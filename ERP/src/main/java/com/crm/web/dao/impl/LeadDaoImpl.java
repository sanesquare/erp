package com.crm.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crm.web.dao.LeadDao;
import com.crm.web.entities.Lead;
import com.crm.web.entities.LeadContact;
import com.crm.web.vo.LeadContactVo;
import com.crm.web.vo.LeadsVo;
import com.erp.web.dao.CompanyDao;
import com.erp.web.entities.Company;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CountryDao;
import com.hrms.web.entities.Country;
import com.hrms.web.exceptions.ItemNotFoundException;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-August-2015
 */
@Repository
public class LeadDaoImpl extends AbstractDao implements LeadDao {

	@Autowired
	private CompanyDao companyDao;
	
	@Autowired
	private CountryDao countryDao;

	public Long saveOrUpdateLead(LeadsVo leadVo) {
		// TODO vendor code generation && Account creation
		Long id = null;
		Lead lead = null;
		if (leadVo.getLeadId() != null) {
			id = leadVo.getLeadId();
			lead = this.findlead(id);
			lead = setValuesToLead(leadVo, lead);
			this.sessionFactory.getCurrentSession().merge(lead);
		} else {
			lead = new Lead();
			lead.setCode(this.generateleadCode());
			lead = setValuesToLead(leadVo, lead);
			id = (Long) this.sessionFactory.getCurrentSession().save(lead);
		}
		return id;
	}

	private Lead setValuesToLead(LeadsVo leadVo, Lead lead) {
		Company company = companyDao.findCompany(leadVo.getCompanyId());
		if(company == null)
			throw new ItemNotFoundException("Company Not Found");
		Country country = countryDao.findCountry(leadVo.getCountryId());
		if(country == null)
			throw new ItemNotFoundException("Country Not Found");
		lead.setCompany(company);
		lead.setCountry(country);
		lead.setName(leadVo.getName());
		lead.setAddress(leadVo.getAddress());
		lead.setDeliveryAddress(leadVo.getDeliveryAddress());
		lead.setMobile(leadVo.getMobile());
		lead.setPhone(leadVo.getLandPhone());
		lead.setEmail(leadVo.getEmail());
		return lead;
	}

	public void deletelead(Long leadId) {
		Lead lead = this.findlead(leadId);
		if (lead == null)
			throw new ItemNotFoundException("Lead Does Not Exist.");
		this.sessionFactory.getCurrentSession().delete(lead);
	}

	public void deletelead(String leadCode) {
		Lead lead = this.findlead(leadCode);
		if (lead == null)
			throw new ItemNotFoundException("Lead Does Not Exist.");
		this.sessionFactory.getCurrentSession().delete(lead);
	}

	public Lead findlead(Long leadId) {
		return (Lead) this.sessionFactory.getCurrentSession().createCriteria(Lead.class)
				.add(Restrictions.eq("id", leadId)).uniqueResult();
	}

	public Lead findlead(String leadCode) {
		return (Lead) this.sessionFactory.getCurrentSession().createCriteria(Lead.class)
				.add(Restrictions.eq("code", leadCode)).uniqueResult();
	}

	private LeadsVo creatLeadVo(Lead lead) {
		LeadsVo vo = new LeadsVo();
		vo.setName(lead.getName());
		vo.setLeadCode(lead.getCode());
		vo.setLeadId(lead.getId());
		vo.setMobile(lead.getMobile());
		vo.setAddress(lead.getAddress());
		vo.setDeliveryAddress(lead.getDeliveryAddress());
		vo.setLandPhone(lead.getPhone());
		vo.setEmail(lead.getEmail());
		Country country = lead.getCountry();
		vo.setCountryId(country.getId());
		vo.setCountry(country.getName());
		if (!lead.getContacts().isEmpty()) {
			for (LeadContact contacts : lead.getContacts()) {
				vo.getContactVos().add(createleadContactVo(contacts, lead));
			}
		}
		return vo;
	}

	private LeadContactVo createleadContactVo(LeadContact contact, Lead lead) {
		LeadContactVo vo = new LeadContactVo();
		vo.setLeadId(lead.getId());
		vo.setContactId(contact.getId());
		vo.setLeadCode(lead.getCode());
		vo.setName(contact.getFirstName());
		vo.setWorkPhone(contact.getPhone());
		vo.setEmail(contact.getEmail());
		vo.setAddress(contact.getAddress());
		return vo;
	}

	public LeadsVo findleadById(Long id) {
		Lead lead = this.findlead(id);
		if (lead == null)
			throw new ItemNotFoundException("Lead Does Not Exist.");
		return creatLeadVo(lead);
	}

	public LeadsVo findleadByCode(String leadCode) {
		Lead lead = this.findlead(leadCode);
		if (lead == null)
			throw new ItemNotFoundException("Lead Does Not Exist.");
		return creatLeadVo(lead);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<LeadsVo> findAllleads() {
		List<LeadsVo> leadsVos = new ArrayList<LeadsVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Lead.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Lead> leads = criteria.list();
		for (Lead lead : leads) {
			leadsVos.add(creatLeadVo(lead));
		}
		return leadsVos;
	}

	public void updateleadContact(LeadContactVo contactVo) {
		LeadContact contact = null;
		Lead lead = this.findlead(contactVo.getLeadId());
		if (lead == null)
			throw new ItemNotFoundException("Lead Does Not Exist.");

		if (contactVo.getContactId() != null) {
			contact = this.findleadContact(contactVo.getContactId());
			if (contact == null)
				throw new ItemNotFoundException("Contact Does Not Exist.");
			contact = setContactValues(contactVo, contact);
		} else {
			contact = new LeadContact();
			contact = setContactValues(contactVo, contact);
			contact.setLead(lead);
		}
		lead.getContacts().add(contact);
		this.sessionFactory.getCurrentSession().merge(lead);
	}

	private LeadContact setContactValues(LeadContactVo contactVo, LeadContact contact) {
		contact.setFirstName(contactVo.getName());
		contact.setPhone(contactVo.getWorkPhone());
		contact.setEmail(contactVo.getEmail());
		contact.setAddress(contactVo.getAddress());
		return contact;
	}

	public LeadContact findleadContact(Long id) {
		return (LeadContact) this.sessionFactory.getCurrentSession().createCriteria(LeadContact.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public LeadContactVo findleadContactById(Long id) {
		LeadContact contacts = this.findleadContact(id);
		if (contacts == null)
			throw new ItemNotFoundException("Contact Does Not Exist.");
		return createleadContactVo(contacts, contacts.getLead());
	}

	public void deleteleadContact(Long id) {
		LeadContact contacts = this.findleadContact(id);
		if (contacts == null)
			throw new ItemNotFoundException("Contact Does Not Exist.");
		Lead lead = contacts.getLead();
		lead.getContacts().remove(contacts);
		this.sessionFactory.getCurrentSession().merge(lead);
	}

	public String generateleadCode() {
		String code = "LEAD-";
		DetachedCriteria maxId = DetachedCriteria.forClass(Lead.class).setProjection(Projections.max("id"));
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Lead.class);
		criteria.add(Property.forName("id").eq(maxId));
		Lead lead = (Lead) criteria.uniqueResult();
		if (lead != null) {
			code = code + String.valueOf(lead.getId() + 1);
		} else {
			code = code + 1;
		}

		return code;
	}

	public List<LeadContactVo> findleadContacts(Long leadId) {
		List<LeadContactVo> contactVos = new ArrayList<LeadContactVo>();
		Lead lead = this.findlead(leadId);
		if (lead == null)
			throw new ItemNotFoundException("Lead Does Not Exist.");
		if (!lead.getContacts().isEmpty()) {
			for (LeadContact contact : lead.getContacts()) {
				contactVos.add(createleadContactVo(contact, lead));
			}
		}
		return contactVos;
	}

	public List<LeadContactVo> findleadContacts(String leadCode) {
		List<LeadContactVo> contactVos = new ArrayList<LeadContactVo>();
		Lead lead = this.findlead(leadCode);
		if (lead == null)
			throw new ItemNotFoundException("Lead Does Not Exist.");
		if (!lead.getContacts().isEmpty()) {
			for (LeadContact contact : lead.getContacts()) {
				contactVos.add(createleadContactVo(contact, lead));
			}
		}
		return contactVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<LeadsVo> findLeadsForCompany(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if(company == null)
			throw new ItemNotFoundException("Company Not Found");
		
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Lead.class);
		criteria.add(Restrictions.eq("company", company));
		List<Lead> leads = new ArrayList<Lead>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		leads = criteria.list();		
		List<LeadsVo> leadVos = new ArrayList<LeadsVo>();
		for(Lead lead : leads){
			leadVos.add(creatLeadVo(lead));
		}
		return leadVos;
	}

}
