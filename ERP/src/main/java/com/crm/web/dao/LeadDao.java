package com.crm.web.dao;

import java.util.List;

import com.crm.web.entities.Lead;
import com.crm.web.entities.LeadContact;
import com.crm.web.vo.LeadContactVo;
import com.crm.web.vo.LeadsVo;
import com.sales.web.entities.Customer;
import com.sales.web.entities.CustomerContact;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-August-2015
 */
public interface LeadDao {
	/**
	 * method to save or update lead
	 * 
	 * @param leadVo
	 * @return
	 */
	public Long saveOrUpdateLead(LeadsVo leadVo);

	/**
	 * method to delete lead
	 * 
	 * @param leadId
	 */
	public void deletelead(Long leadId);

	/**
	 * method to delete lead
	 * 
	 * @param leadCode
	 */
	public void deletelead(String leadCode);

	/**
	 * method to find and return lead Object by id
	 * 
	 * @param leadId
	 * @return
	 */
	public Lead findlead(Long leadId);

	/**
	 * method to find and return lead object by lead code
	 * 
	 * @param leadCode
	 * @return
	 */
	public Lead findlead(String leadCode);

	/**
	 * method to find lead by id
	 * 
	 * @param id
	 * @return
	 */
	public LeadsVo findleadById(Long id);

	/**
	 * method to find lead by code
	 * 
	 * @param leadCode
	 * @return
	 */
	public LeadsVo findleadByCode(String leadCode);

	/**
	 * method to find all leads
	 * 
	 * @return
	 */
	public List<LeadsVo> findAllleads();

	/**
	 * method to update lead contact
	 * 
	 * @param contactVo
	 * @return
	 */
	public void updateleadContact(LeadContactVo contactVo);

	/**
	 * method to find leadVContact object by id
	 * 
	 * @param id
	 * @return
	 */
	public LeadContact findleadContact(Long id);

	/**
	 * method to find lead contact by id
	 * 
	 * @param id
	 * @return
	 */
	public LeadContactVo findleadContactById(Long id);

	/**
	 * method to delete lead contact
	 * 
	 * @param id
	 */
	public void deleteleadContact(Long id);

	/**
	 * method to generate lead code
	 * 
	 * @return
	 */
	public String generateleadCode();

	/**
	 * method to find lead's conacts
	 * 
	 * @param leadId
	 * @return
	 */
	public List<LeadContactVo> findleadContacts(Long leadId);

	/**
	 * method to find lead's conacts
	 * 
	 * @param leadCode
	 * @return
	 */
	public List<LeadContactVo> findleadContacts(String leadCode);
	
	/**
	 * Method to find leads for company
	 * @param companyId
	 * @return
	 */
	public List<LeadsVo> findLeadsForCompany(Long companyId);
}
