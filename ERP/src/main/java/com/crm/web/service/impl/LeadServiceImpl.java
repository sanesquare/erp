package com.crm.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crm.web.dao.LeadDao;
import com.crm.web.service.LeadService;
import com.crm.web.vo.LeadContactVo;
import com.crm.web.vo.LeadsVo;
import com.hrms.web.dao.AbstractDao;
import com.sales.web.entities.Customer;
import com.sales.web.entities.CustomerContact;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-August-2015
 */
@Service
public class LeadServiceImpl extends AbstractDao implements LeadService {

	@Autowired
	private LeadDao dao;

	public Long saveOrUpdateLead(LeadsVo leadVo) {
		return dao.saveOrUpdateLead(leadVo);
	}

	public void deletelead(Long leadId) {
		dao.deletelead(leadId);
	}

	public void deletelead(String leadCode) {
		dao.deletelead(leadCode);
	}

	public LeadsVo findleadById(Long id) {
		return dao.findleadById(id);
	}

	public LeadsVo findleadByCode(String leadCode) {
		return dao.findleadByCode(leadCode);
	}

	public List<LeadsVo> findAllleads() {
		return dao.findAllleads();
	}

	public void updateleadContact(LeadContactVo contactVo) {
		dao.updateleadContact(contactVo);
	}

	public LeadContactVo findleadContactById(Long id) {
		return dao.findleadContactById(id);
	}

	public void deleteleadContact(Long id) {
		dao.deleteleadContact(id);
	}

	public String generateleadCode() {
		return dao.generateleadCode();
	}

	public List<LeadContactVo> findleadContacts(Long leadId) {
		return dao.findleadContacts(leadId);
	}

	public List<LeadContactVo> findleadContacts(String leadCode) {
		return dao.findleadContacts(leadCode);
	}

	public List<LeadsVo> findLeadsForCompany(Long companyId) {
		return dao.findLeadsForCompany(companyId);
	}

}
