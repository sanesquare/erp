package com.crm.web.vo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-August-2015
 */
public class LeadContactVo {

	private Long leadId;

	private Long contactId;

	private String leadCode;

	private String name;

	private String address;

	private String workPhone;
	
	private String email;

	public Long getLeadId() {
		return leadId;
	}

	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public String getLeadCode() {
		return leadCode;
	}

	public void setLeadCode(String leadCode) {
		this.leadCode = leadCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
