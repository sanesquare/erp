package com.crm.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.erp.web.entities.OpportunityProbability;
import com.erp.web.entities.OpportunityStage;
import com.erp.web.entities.OpportunityStatus;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.sales.web.entities.Customer;
import com.sales.web.entities.CustomerContact;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 01-August-2015
 *
 */

@Entity
@Table(name = "opportunity")
public class Opportunity implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4225772688010928468L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Lead lead;
	
	@ManyToOne
	private OpportunityStage opportunityStage;
	
	@Column(name="name")
	private String name;
	
	@ManyToOne
	private OpportunityStatus status;
	
	@ManyToOne
	private LeadContact  leadContact;
	
	@Column(name="expecting_closing_date")
	private Date expectingClosingDate;
	
	@Column(name="amount")
	private BigDecimal amount;
	
	@Column(name="note",length=1000)
	private String note;
	
	@ManyToOne
	private OpportunityProbability probability;
	
	@OneToMany(mappedBy="opportunity")
	private Set<OpportunityItems> items;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Lead getLead() {
		return lead;
	}

	public void setLead(Lead lead) {
		this.lead = lead;
	}

	public OpportunityStage getOpportunityStage() {
		return opportunityStage;
	}

	public void setOpportunityStage(OpportunityStage opportunityStage) {
		this.opportunityStage = opportunityStage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OpportunityStatus getStatus() {
		return status;
	}

	public void setStatus(OpportunityStatus status) {
		this.status = status;
	}

	public LeadContact getLeadContact() {
		return leadContact;
	}

	public void setLeadContact(LeadContact leadContact) {
		this.leadContact = leadContact;
	}

	public Date getExpectingClosingDate() {
		return expectingClosingDate;
	}

	public void setExpectingClosingDate(Date expectingClosingDate) {
		this.expectingClosingDate = expectingClosingDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public OpportunityProbability getProbability() {
		return probability;
	}

	public void setProbability(OpportunityProbability probability) {
		this.probability = probability;
	}

	public Set<OpportunityItems> getItems() {
		return items;
	}

	public void setItems(Set<OpportunityItems> items) {
		this.items = items;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
