package com.crm.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.crm.web.constants.CrmPageNameConstants;
import com.crm.web.constants.CrmRequestConstants;
import com.crm.web.service.LeadService;
import com.crm.web.vo.LeadContactVo;
import com.crm.web.vo.LeadsVo;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.CountryService;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.vo.VendorContactVo;
import com.purchase.web.vo.VendorVo;

/**
 * 
 * @author Shamsheer & Vinutha 3-August-2015
 */
@Controller
@SessionAttributes({ "companyId" })
public class LeadsController {

	@Autowired
	private CountryService countryService;

	@Autowired
	private LeadService leadService;

	/**
	 * method to reach leads home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = CrmRequestConstants.LEADS_URL, method = RequestMethod.GET)
	public String leadsHome(final Model model, HttpServletRequest request , @ModelAttribute("companyId")Long companyId) {
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String errorMessage = request.getParameter(MessageConstants.ERROR);
		try {
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
			model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("leads", leadService.findLeadsForCompany(companyId));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, e);
		}
		return CrmPageNameConstants.LEADS_PAGE;
	}

	/**
	 * method to reach leads add page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = CrmRequestConstants.LEADS_ADD, method = RequestMethod.GET)
	public String addLeadsPage(final Model model, @ModelAttribute LeadsVo leadsVo) {
		try {
			model.addAttribute("country", countryService.findAllCountry());
			model.addAttribute("leadsVo", leadsVo);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, e);
		}
		return CrmPageNameConstants.ADD_LEADS;
	}

	/**
	 * method to save or update leads
	 * 
	 * @param model
	 * @param leadsVo
	 * @param bindingResult
	 * @return
	 */
	@RequestMapping(value = CrmRequestConstants.LEADS_SAVE, method = RequestMethod.POST)
	public String saveLeads(final Model model, @ModelAttribute LeadsVo leadsVo, BindingResult bindingResult,
			@ModelAttribute("companyId") Long companyId) {
		Long id = null;
		try {
			leadsVo.setCompanyId(companyId);
			id = leadService.saveOrUpdateLead(leadsVo);
			model.addAttribute(MessageConstants.SUCCESS, "Leads Details Saved Successfully.");
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
			e.printStackTrace();
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		if (id != null)
			return "redirect:" + CrmRequestConstants.LEADS_EDIT + "?id=" + id;
		else
			return CrmPageNameConstants.ADD_LEADS;
	}

	/**
	 * method to edit leads
	 * 
	 * @param model
	 * @param id
	 * @param request
	 * @return
	 */
	@RequestMapping(value = CrmRequestConstants.LEADS_EDIT, method = RequestMethod.GET)
	public String editLead(final Model model, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request) {
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String errorMessage = request.getParameter(MessageConstants.ERROR);
		try {
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
			model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("leadsVo", leadService.findleadById(id));
			model.addAttribute("country", countryService.findAllCountry());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
			e.printStackTrace();
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return CrmPageNameConstants.ADD_LEADS;
	}

	/**
	 * method to delete lead
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = CrmRequestConstants.LEADS_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deleteLead(@RequestParam(value = "id", required = true) Long id) {
		try {
			leadService.deletelead(id);
		} catch (ItemNotFoundException e) {
			return e.getMessage();
		} catch (Exception e) {
			return MessageConstants.WRONG;
		}
		return "ok";
	}

	/**
	 * method to list all leads
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = CrmRequestConstants.LEADS_LIST, method = RequestMethod.GET)
	public String leadsList(final Model model) {
		try {
			model.addAttribute("leads", leadService.findAllleads());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, e);
		}
		return CrmPageNameConstants.LEADS_LIST;
	}

	/**
	 * method to list leads contact
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = CrmRequestConstants.LEADS_CONTACT_LIST, method = RequestMethod.GET)
	public String leadsContactList(final Model model, @RequestParam(value = "id", required = true) Long id) {
		try {
			model.addAttribute("contacts", leadService.findleadContacts(id));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, e);
		}
		return CrmPageNameConstants.LEADS_CONTACT_LIST;
	}

	/**
	 * method to save lead contact
	 * 
	 * @param contactVo
	 * @return
	 */
	@RequestMapping(value = CrmRequestConstants.LEADS_CONTACT_SAVE, method = RequestMethod.POST)
	public @ResponseBody String saveLeadContactDetails(@RequestBody LeadContactVo contactVo) {
		try {
			leadService.updateleadContact(contactVo);
			return "ok";
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return MessageConstants.WRONG;
		}
	}

	/**
	 * method to delete lead contact
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = CrmRequestConstants.LEADS_CONTACT_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deleteLeadContactDetails(@RequestParam(value = "id", required = true) Long id) {
		try {
			leadService.deleteleadContact(id);
			return "ok";
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return MessageConstants.WRONG;
		}

	}
}
