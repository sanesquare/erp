package com.crm.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.crm.web.constants.CrmPageNameConstants;
import com.crm.web.constants.CrmRequestConstants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
@Controller
public class CrmController {

	@RequestMapping(value = CrmRequestConstants.HOME , method = RequestMethod.GET)
	public String crmHome(final Model model){
		
		return CrmPageNameConstants.HOME_PAGE;
	}
	
}
