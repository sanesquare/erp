package com.crm.web.constants;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface CrmPageNameConstants {

	String HOME_PAGE = "crmHome";
	
	String LEADS_PAGE ="leads";
	String ADD_LEADS="addLeads";
	String LEADS_LIST="leadsList";
	String LEADS_CONTACT_LIST="leadContactList";
}
