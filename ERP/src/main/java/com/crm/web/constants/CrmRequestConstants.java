package com.crm.web.constants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface CrmRequestConstants {

	String HOME = "crmHome.do";

	String LEADS_URL = "leads.do";
	String LEADS_ADD = "addLeads.do";
	String LEADS_SAVE = "saveLeads.do";
	String LEADS_EDIT = "editLeads.do";
	String LEADS_DELETE = "deleteLeads.do";
	String LEADS_CONTACT_SAVE = "leadsContactSave.do";
	String LEADS_CONTACT_LIST = "leadsContactList.do";
	String LEADS_CONTACT_DELETE = "deleteLeadsContact.do";
	String LEADS_LIST = "leadsList.do";
}
