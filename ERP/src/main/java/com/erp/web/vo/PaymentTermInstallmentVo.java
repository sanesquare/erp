package com.erp.web.vo;

import java.math.BigDecimal;


/**
 * @author Shamsheer & Vinutha
 * @since Sep 28, 2015
 */
public class PaymentTermInstallmentVo {

	private BigDecimal percentage;
	
	private Integer nofdays;
	
	private Long paymentMethodId;
	
	private String paymentMethod;
	
	private BigDecimal amount;
	
	private String date;
	
	private Long id;

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public Integer getNofdays() {
		return nofdays;
	}

	public void setNofdays(Integer nofdays) {
		this.nofdays = nofdays;
	}

	public Long getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(Long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
