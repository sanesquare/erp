package com.erp.web.vo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 26, 2015
 */
public class PaymentMethodVo {

	private Long companyId;
	
	private Long id;
	
	private String name;

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
