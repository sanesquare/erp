package com.erp.web.vo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Dec 14, 2015
 */
/**
 * @author Shamsheer & Vinutha
 * @since Dec 15, 2015
 */
public class DeliveryMethodVo {

	private Long companyId;

	private Long deliveryMethodId;

	private String method;

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getDeliveryMethodId() {
		return deliveryMethodId;
	}

	public void setDeliveryMethodId(Long deliveryMethodId) {
		this.deliveryMethodId = deliveryMethodId;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
}
