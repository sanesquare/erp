package com.erp.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 26, 2015
 */
public class ChargeVo {

	private Boolean selected=false;
	
	private List<Long> ids = new ArrayList<Long>();
	
	private List<String> idsAndAmt = new ArrayList<String>();
	
	private Long chargeId;
	
	private BigDecimal amount=new BigDecimal("0.00");
	
	private Long companyId;
	
	private String name;
	
	private Long ledgerId;

	public Long getChargeId() {
		return chargeId;
	}

	public void setChargeId(Long chargeId) {
		this.chargeId = chargeId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(Long ledgerId) {
		this.ledgerId = ledgerId;
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public List<String> getIdsAndAmt() {
		return idsAndAmt;
	}

	public void setIdsAndAmt(List<String> idsAndAmt) {
		this.idsAndAmt = idsAndAmt;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
