package com.erp.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha 
 * @since 12-August-2015
 */
public class ErpCurrencyVo {

	private String date;
	
	private Long baseCurrencyId;
	
	private Long erpCurrencyId;
	
	private BigDecimal rate;

	private String currency;
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getBaseCurrencyId() {
		return baseCurrencyId;
	}

	public void setBaseCurrencyId(Long baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}

	public Long getErpCurrencyId() {
		return erpCurrencyId;
	}

	public void setErpCurrencyId(Long erpCurrencyId) {
		this.erpCurrencyId = erpCurrencyId;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
