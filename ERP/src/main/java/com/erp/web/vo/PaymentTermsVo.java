package com.erp.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 28, 2015
 */
public class PaymentTermsVo {

	private List<PaymentTermInstallmentVo> installmentVos = new ArrayList<PaymentTermInstallmentVo>();
	
	private Long id;
	
	private String name;
	
	private Long companyId;

	public List<PaymentTermInstallmentVo> getInstallmentVos() {
		return installmentVos;
	}

	public void setInstallmentVos(List<PaymentTermInstallmentVo> installmentVos) {
		this.installmentVos = installmentVos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
}
