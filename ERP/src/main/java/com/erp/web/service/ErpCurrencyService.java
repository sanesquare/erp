package com.erp.web.service;

import java.util.List;

import com.erp.web.entities.ErPCurrency;
import com.erp.web.vo.ErpCurrencyVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
public interface ErpCurrencyService {

	/**
	 * method to save or update currency
	 * @param currencyVo
	 */
	public void saveOrUpdateCurrency(ErpCurrencyVo currencyVo);
	
	/**
	 * method to find recent currency by base currency
	 * @param baseCurrencyId
	 * @return
	 */
	public ErpCurrencyVo findRecentCurrencyVoByBaseCurrency(Long baseCurrencyId);
	
	/**
	 * method to find currency by id
	 * @param id
	 * @return
	 */
	public ErpCurrencyVo findCurrencyById(Long id);
	
	/**
	 * method to find all currencies
	 * @return
	 */
	public List<ErpCurrencyVo> findAllCurrencies(String date);
}
