package com.erp.web.service;

import java.util.List;

import com.erp.web.vo.ChargeVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 26, 2015
 */
public interface ChargeService {

	/**
	 * method to save or update charge
	 * @param chargeVo
	 */
	public void saveCharge(ChargeVo chargeVo);
	
	/**
	 * method to delete charge
	 * @param chargeId
	 */
	public void deleteCharge(Long chargeId);
	
	/**
	 * method to find charge by id
	 * @param chargeId
	 * @return
	 */
	public ChargeVo findChargeById(Long chargeId);
	
	/**
	 * method to find charge y name
	 * @param charge
	 * @param companyId
	 * @return
	 */
	public ChargeVo findChargeByName(String charge,Long companyId);
	

	/**
	 * method to find all charges
	 * @param companyId
	 * @return
	 */
	public List<ChargeVo> findAllCharges(Long companyId);
	
	/**
	 * method to find products
	 * @param ids
	 * @return
	 */
	public List<ChargeVo> findCharges(List<Long> ids);
}
