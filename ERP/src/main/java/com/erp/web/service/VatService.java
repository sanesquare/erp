package com.erp.web.service;

import java.util.List;

import com.erp.web.entities.VAT;
import com.erp.web.vo.VatVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 25, 2015
 */
public interface VatService {

	
	/**
	 * Method to save or  update vat		
	 * @param vatVo
	 */
	public void saveOrUpdateTax(VatVo vatVo);
	
	/**
	 * Method to delete tax
	 * @param id
	 */
	public void deleteTax(Long id);
	
	/**
	 * Method to find tax
	 * @param id
	 * @return
	 */
	public VatVo findTax(Long id);
	
	/**
	 * Method to list all taxes for company
	 * @param companyId
	 * @return
	 */
	public List<VatVo> listAllTaxesForCompany(Long companyId);
}
