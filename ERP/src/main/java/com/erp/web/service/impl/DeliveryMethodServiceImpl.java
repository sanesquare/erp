package com.erp.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.web.dao.DeliveryMethodDao;
import com.erp.web.service.DeliveryMethodService;
import com.erp.web.vo.DeliveryMethodVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Dec 15, 2015
 */
@Service
public class DeliveryMethodServiceImpl implements DeliveryMethodService {

	@Autowired
	private DeliveryMethodDao dao;
	
	public void saveDeliveryMethod(DeliveryMethodVo deliveryMethodVo) {
		dao.saveDeliveryMethod(deliveryMethodVo);
	}

	public void deleteDeliveryMethod(Long id) {
		dao.deleteDeliveryMethod(id);	
	}

	public List<DeliveryMethodVo> listAllDeliveryMethods(Long companyId) {
		return dao.listAllDeliveryMethods(companyId);
	}

	public DeliveryMethodVo findDeliveryMethod(Long id) {
		return dao.findDeliveryMethod(id);
	}

}
