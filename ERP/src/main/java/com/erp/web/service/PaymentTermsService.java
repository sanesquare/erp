package com.erp.web.service;

import java.util.List;

import com.erp.web.vo.PaymentTermsVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 28, 2015
 */
public interface PaymentTermsService {

	/**
	 * method to save payment terms
	 * @param vo
	 */
	public void savePaymentTerms(PaymentTermsVo vo);
	
	/**
	 * method to delete payment terms
	 * @param id
	 */
	public void deletePaymentTerms(Long id);
	
	/**
	 * method to delete payment term
	 * @param companyId
	 * @param paymentTermName
	 */
	public void deletePaymentTerms(Long companyId,String paymentTermName);
	
	/**
	 * method to find payment terms
	 * @param id
	 * @return
	 */
	public PaymentTermsVo findPaymentTermById(Long id);
	
	/**
	 * method to find payment terms
	 * @param companyId
	 * @param paymentTermName
	 * @return
	 */
	public PaymentTermsVo findPaymentTermByName(Long companyId,String paymentTermName);
	
	/**
	 * method to find all payment terms
	 * @param companyId
	 * @return
	 */
	public List<PaymentTermsVo> findAllPaymentTerms(Long companyId);
}

