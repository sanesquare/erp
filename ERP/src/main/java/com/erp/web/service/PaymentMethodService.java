package com.erp.web.service;

import java.util.List;
import com.erp.web.vo.PaymentMethodVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 26, 2015
 */
public interface PaymentMethodService {

	/**
	 * method to save payment method
	 * @param vo
	 */
	public void savePaymentMethod(PaymentMethodVo vo);
	
	/**
	 * method to delete payment method
	 * @param id
	 */
	public void deletePaymentMethod(Long id);
	
	/**
	 * method to delete payment method
	 * @param companyId
	 * @param name
	 */
	public void deletePaymentMethod(String name,Long companyId);
	
	/**
	 * method to find payment method
	 * @param id
	 * @return
	 */
	public PaymentMethodVo findPaymentMethodById(Long id);
	
	/**
	 * method to find payment method
	 * @param name
	 * @param companyId
	 * @return
	 */
	public PaymentMethodVo findPaymentMethodByName(String name,Long companyId);
	
	/**
	 * method to find all payment methods
	 * @param companyId
	 * @return
	 */
	public List<PaymentMethodVo> findAllPaymentMethods(Long companyId);
}
