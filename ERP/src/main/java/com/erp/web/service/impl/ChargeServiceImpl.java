package com.erp.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.web.dao.ChargeDao;
import com.erp.web.service.ChargeService;
import com.erp.web.vo.ChargeVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 26, 2015
 */
@Service
public class ChargeServiceImpl implements ChargeService{

	@Autowired
	private ChargeDao dao;

	public void saveCharge(ChargeVo chargeVo) {
		dao.saveCharge(chargeVo);
	}

	public void deleteCharge(Long chargeId) {
		dao.deleteCharge(chargeId);
	}

	public ChargeVo findChargeById(Long chargeId) {
		return dao.findChargeById(chargeId);
	}

	public ChargeVo findChargeByName(String charge, Long companyId) {
		return dao.findChargeByName(charge, companyId);
	}

	public List<ChargeVo> findAllCharges(Long companyId) {
		return dao.findAllCharges(companyId);
	}

	public List<ChargeVo> findCharges(List<Long> ids) {
		return dao.findCharges(ids);
	}
}
