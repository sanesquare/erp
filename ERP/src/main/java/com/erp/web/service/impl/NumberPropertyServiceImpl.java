package com.erp.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.service.NumberPropertyService;
import com.erp.web.vo.NumberPropertyVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 30, 2015
 */
@Service
public class NumberPropertyServiceImpl implements NumberPropertyService{

	@Autowired
	private NumberPropertyDao dao;

	public NumberPropertyVo findNumberProperty(String category, Long companyId) {
		return dao.findNumberProperty(category, companyId);
	}

	public NumberPropertyVo findNumberProperty(String category) {
		return dao.findNumberProperty(category);
	}

	public void incrementNumberProperty(String category, Company company) {
		dao.incrementNumberProperty(category, company);
	}

	public void incrementNumberProperty(String category) {
		dao.incrementNumberProperty(category);
	}

	public void saveNumberProperty(NumberPropertyVo vo) {
		dao.saveNumberProperty(vo);
	}
}
