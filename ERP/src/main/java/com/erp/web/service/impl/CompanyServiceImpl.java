package com.erp.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.web.dao.CompanyDao;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyDao dao;

	public void saveCompany(CompanyVo companyVo) {
		dao.saveCompany(companyVo);
	}

	public CompanyVo findActiveCompanyByParent(Long parentId) {
		return dao.findActiveCompanyByParent(parentId);
	}

	public void deleteCompany(Long id) {
		dao.deleteCompany(id);
	}

	public CompanyVo findCompanyById(Long id) {
		return dao.findCompanyById(id);
	}

	public CompanyVo findCompanyForUser(String userName) {
		return dao.findCompanyForUser(userName);
	}

	public List<CompanyVo> findAllCompanies() {
		return dao.findAllCompanies();
	}

	public Boolean updateDefaultCompanyFOrUser(String userName, Long companyId) {
		 return dao.updateDefaultCompanyFOrUser(userName, companyId);
	}

	public List<CompanyVo> findAllCompaniesOfBranches(List<Long> branchIds, Long companyId) {
		return dao.findAllCompaniesOfBranches(branchIds,companyId);
	}

	public List<CompanyVo> findChildCompanies(Long companyId) {
		return dao.findChildCompanies(companyId);
	}
}
