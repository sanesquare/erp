package com.erp.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.web.dao.PaymentMethodDao;
import com.erp.web.service.PaymentMethodService;
import com.erp.web.vo.PaymentMethodVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 26, 2015
 */
@Service
public class PaymentMethodServiceImpl implements PaymentMethodService {

	@Autowired
	private PaymentMethodDao dao;
	
	/* (non-Javadoc)
	 * @see com.erp.web.service.PaymentMethodService#savePaymentMethod(com.erp.web.vo.PaymentMethodVo)
	 */
	public void savePaymentMethod(PaymentMethodVo vo) {
		dao.savePaymentMethod(vo);
	}

	/* (non-Javadoc)
	 * @see com.erp.web.service.PaymentMethodService#deletePaymentMethod(java.lang.Long)
	 */
	public void deletePaymentMethod(Long id) {
		dao.deletePaymentMethod(id);
	}

	/* (non-Javadoc)
	 * @see com.erp.web.service.PaymentMethodService#deletePaymentMethod(java.lang.String, java.lang.Long)
	 */
	public void deletePaymentMethod(String name, Long companyId) {
		dao.deletePaymentMethod(name, companyId);
	}

	/* (non-Javadoc)
	 * @see com.erp.web.service.PaymentMethodService#findPaymentMethodById(java.lang.Long)
	 */
	public PaymentMethodVo findPaymentMethodById(Long id) {
		return dao.findPaymentMethodById(id);
	}

	/* (non-Javadoc)
	 * @see com.erp.web.service.PaymentMethodService#findPaymentMethodByName(java.lang.String, java.lang.Long)
	 */
	public PaymentMethodVo findPaymentMethodByName(String name, Long companyId) {
		return dao.findPaymentMethodByName(name, companyId);
	}

	/* (non-Javadoc)
	 * @see com.erp.web.service.PaymentMethodService#findAllPaymentMethods(java.lang.Long)
	 */
	public List<PaymentMethodVo> findAllPaymentMethods(Long companyId) {
		return dao.findAllPaymentMethods(companyId);
	}

}
