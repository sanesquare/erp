package com.erp.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.web.dao.VatDao;
import com.erp.web.service.VatService;
import com.erp.web.vo.VatVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 25, 2015
 */
@Service
public class VatServiceImpl implements VatService{

	@Autowired
	private VatDao  dao;
	
	public void saveOrUpdateTax(VatVo vatVo) {
		dao.saveOrUpdateTax(vatVo);
	}

	public void deleteTax(Long id) {
		dao.deleteTax(id);
	}

	public VatVo findTax(Long id) {
		return dao.findTax(id);
	}

	public List<VatVo> listAllTaxesForCompany(Long companyId) {
		return dao.listAllTaxesForCompany(companyId);
	}

}
