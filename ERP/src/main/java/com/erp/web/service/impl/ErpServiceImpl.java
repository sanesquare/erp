package com.erp.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.vo.ErpCurrencyVo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
@Service
public class ErpServiceImpl implements ErpCurrencyService {

	@Autowired
	private ErpCurrencyDao dao;
	
	public void saveOrUpdateCurrency(ErpCurrencyVo currencyVo) {
		dao.saveOrUpdateCurrency(currencyVo);
	}

	public ErpCurrencyVo findRecentCurrencyVoByBaseCurrency(Long baseCurrencyId) {
		return dao.findRecentCurrencyVoByBaseCurrency(baseCurrencyId);
	}

	public ErpCurrencyVo findCurrencyById(Long id) {
		return dao.findCurrencyById(id);
	}

	public List<ErpCurrencyVo> findAllCurrencies(String date) {
		return dao.findAllCurrencies(date);
	}

}
