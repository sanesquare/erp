package com.erp.web.service;

import com.erp.web.entities.Company;
import com.erp.web.vo.NumberPropertyVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 30, 2015
 */
public interface NumberPropertyService {
	
	/**
	 * method to save number property
	 * @param vo
	 */
	public void saveNumberProperty(NumberPropertyVo vo);
	/**
	 * method to find number property for given category and company
	 * 
	 * @param category
	 * @param companyId
	 * @return
	 */
	public NumberPropertyVo findNumberProperty(String category, Long companyId);
	
	/**
	 * method to find number property for given category and company
	 * @param category
	 * @return
	 */
	public NumberPropertyVo findNumberProperty(String category);

	/**
	 * method to increment number property
	 * 
	 * @param category
	 * 
	 * @param companyId
	 */
	public void incrementNumberProperty(String category, Company company);
	
	/**
	 * method to increment number property
	 * @param category
	 */
	public void incrementNumberProperty(String category);
}
