package com.erp.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.web.dao.PaymentTermsDao;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.vo.PaymentTermsVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 28, 2015
 */
@Service
public class PaymentTermsServiceImpl implements PaymentTermsService{

	@Autowired
	private PaymentTermsDao dao;
	
	public void savePaymentTerms(PaymentTermsVo vo) {
		dao.savePaymentTerms(vo);
	}

	public void deletePaymentTerms(Long id) {
		dao.deletePaymentTerms(id);
	}

	public void deletePaymentTerms(Long companyId, String paymentTermName) {
		dao.deletePaymentTerms(companyId, paymentTermName);
	}

	public PaymentTermsVo findPaymentTermById(Long id) {
		return dao.findPaymentTermById(id);
	}

	public PaymentTermsVo findPaymentTermByName(Long companyId, String paymentTermName) {
		return dao.findPaymentTermByName(companyId, paymentTermName);
	}

	public List<PaymentTermsVo> findAllPaymentTerms(Long companyId) {
		return dao.findAllPaymentTerms(companyId);
	}

}
