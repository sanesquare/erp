package com.erp.web.service;

import java.util.List;

import com.erp.web.entities.DeliveryMethod;
import com.erp.web.vo.DeliveryMethodVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Dec 14, 2015
 */
public interface DeliveryMethodService {


	/**
	 * Method to save delivery method
	 * @param deliveryMethodVo
	 * @return
	 */
	public void saveDeliveryMethod(DeliveryMethodVo deliveryMethodVo);
	/**
	 * Method to delete delivery method
	 * @param id
	 */
	public void deleteDeliveryMethod(Long id);
	
	/**
	 * Method to list all delivery methods
	 * @param companyId
	 * @return
	 */
	public List<DeliveryMethodVo> listAllDeliveryMethods(Long companyId);
	
	/**
	 * Method to find delivery method using id
	 * @param id
	 * @return
	 */
	public DeliveryMethodVo findDeliveryMethod(Long id);
}
