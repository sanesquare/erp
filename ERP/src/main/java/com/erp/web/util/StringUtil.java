package com.erp.web.util;

public class StringUtil {

	public static String getFormattedNumberWithLeadingZeroes(String beginsFrom, Long current) {
		String prefix = "";
		int length = beginsFrom.length();
		String nextString = String.valueOf(current);
		int nextLength = nextString.length();
		if (length > nextLength) {
			int diff = length - nextLength;
			for (int i = 0; i < diff; i++) {
				prefix = prefix.concat("0");
			}
			prefix = prefix.concat(nextString);
		}else{
			prefix = nextString;
		}
		return prefix;
	}
}
