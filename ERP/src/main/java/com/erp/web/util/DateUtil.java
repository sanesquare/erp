package com.erp.web.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.erp.web.constants.DateFormatConstants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 17-September-2015
 *
 */
public class DateUtil {

	public static String convertStringToLongDateString(String format, Date date) {
		String dateAsString = null;
		try {
			DateFormat outputformat = new SimpleDateFormat(format);
			dateAsString = outputformat.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dateAsString;
	}

	public static String convertDateToDetailedString(Date date) {

		SimpleDateFormat dd = new SimpleDateFormat("dd");
		SimpleDateFormat mmyyyy = new SimpleDateFormat("MMMM, yyyy");

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String formattedDate = dd.format(date) + getDayOfMonthSuffix(cal.get(Calendar.DATE)) + " "
				+ mmyyyy.format(date);
		return formattedDate;
		//

		/*
		 * String dateAsString = null; try { DateFormat outputformat = new
		 * SimpleDateFormat(format); dateAsString = outputformat.format(date); }
		 * catch (Exception e) { e.printStackTrace(); } return dateAsString;
		 */
	}

	private static String getDayOfMonthSuffix(final int n) {
		if (n < 1 || n > 31) {
			throw new IllegalArgumentException("Illegal day of month");
		}

		if (n >= 11 && n <= 13) {
			return "th";
		}

		switch (n % 10) {
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
		}
	}
}
