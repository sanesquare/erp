package com.erp.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.LedgerGroupDao;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.LedgerGroup;
import com.erp.web.dao.ChargeDao;
import com.erp.web.dao.CompanyDao;
import com.erp.web.entities.Charge;
import com.erp.web.entities.Company;
import com.erp.web.vo.ChargeVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.inventory.web.entities.Product;
import com.inventory.web.vo.ProductVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 26, 2015
 */
@Repository
public class ChargeDaoImpl extends AbstractDao implements ChargeDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private LedgerGroupDao ledgerGroupDao;

	public void saveCharge(ChargeVo chargeVo) {
		chargeVo.setName(chargeVo.getName().trim());
		Charge charge = this.findCharge(chargeVo.getChargeId());
		Company company = null;
		Ledger ledger = null;
		if(charge==null){
			company=companyDao.findCompany(chargeVo.getCompanyId());
			if(this.findCharge(chargeVo.getName(), company)!=null)
				throw new DuplicateItemException("Charge Already Exists.");
			if(company==null)
				throw new ItemNotFoundException("Company Not Found.");
			ledger = new Ledger();
			ledger.setCompany(company);
			ledger.setIsEditable(false);
			ledger.setBalance(BigDecimal.ZERO);
			LedgerGroup ledgerGroup = ledgerGroupDao.findLedgerGroup(AccountsConstants.DIRECT_EXPENSE, chargeVo.getCompanyId());
			if(ledgerGroup==null)
				throw new ItemNotFoundException("Ledger Group Not Found.");
			ledger.setLedgerGroup(ledgerGroup);
			charge=new Charge();
			charge.setName(chargeVo.getName());
			charge.setLedger(ledger);
			charge.setCompany(company);
		}else{
			Charge chargeWithSameName = this.findCharge(chargeVo.getName(), company);
			if(chargeWithSameName!=null&& !charge.equals(chargeWithSameName))
				throw new DuplicateItemException("Charge Already Exists.");
			company=charge.getCompany();
			ledger=charge.getLedger();
			charge.setName(chargeVo.getName());
		}
		ledger.setLedgerName(chargeVo.getName());
		ledger.setOpeningBalance(BigDecimal.ZERO);
		ledger.setStartDate(company.getPeriodStart());
		ledger.setCharge(charge);
		charge.setLedger(ledger);
		this.sessionFactory.getCurrentSession().saveOrUpdate(charge);
	}

	public void deleteCharge(Long chargeId) {
		Charge charge = this.findCharge(chargeId);
		if (charge == null)
			throw new ItemNotFoundException("Charge not exist.");
		this.sessionFactory.getCurrentSession().delete(charge);

	}

	public Charge findCharge(Long chargeId) {
		return (Charge) this.sessionFactory.getCurrentSession().createCriteria(Charge.class)
				.add(Restrictions.eq("id", chargeId)).uniqueResult();
	}

	public Charge findCharge(String charge, Company company) {
		return (Charge) this.sessionFactory.getCurrentSession().createCriteria(Charge.class)
				.add(Restrictions.eq("name", charge)).add(Restrictions.eq("company", company)).uniqueResult();
	}

	/**
	 * method to create charge vo
	 * @param charge
	 * @return
	 */
	private ChargeVo createChargeVo(Charge charge){
		ChargeVo vo = new ChargeVo();
		vo.setChargeId(charge.getId());
		vo.setName(charge.getName());
		vo.setCompanyId(charge.getCompany().getId());
		vo.setLedgerId(charge.getLedger().getId());
		return vo;
	}
	
	public ChargeVo findChargeById(Long chargeId) {
		Charge charge = this.findCharge(chargeId);
		if (charge == null)
			throw new ItemNotFoundException("Charge not exist.");
		return createChargeVo(charge);
	}

	public ChargeVo findChargeByName(String chargeName, Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if(company==null)
			throw new ItemNotFoundException("Company Not Found.");
		Charge charge = this.findCharge(chargeName,company);
		if (charge == null)
			throw new ItemNotFoundException("Charge not exist.");
		return createChargeVo(charge);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ChargeVo> findAllCharges(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if(company==null)
			throw new ItemNotFoundException("Company Not Found");
		List<ChargeVo> chargeVos = new ArrayList<ChargeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Charge.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Charge> charges = criteria.list();
		for(Charge charge :charges)
			chargeVos.add(createChargeVo(charge));
		return chargeVos;
	}

	public List<ChargeVo> findCharges(List<Long> ids) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Charge.class);
		criteria.add(Restrictions.in("id", ids));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Charge> charges = criteria.list();
		List<ChargeVo> chargeVos = new ArrayList<ChargeVo>();
		for (Charge charge : charges) {
			chargeVos.add(createChargeVo(charge));
		}
		return chargeVos;
	}

}
