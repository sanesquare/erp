package com.erp.web.dao;

import java.util.List;

import com.erp.web.entities.Company;
import com.erp.web.vo.CompanyVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
public interface CompanyDao {

	/**
	 * method to save company
	 * @param companyVo
	 */
	public void saveCompany(CompanyVo companyVo);
	
	/**
	 * method to find active company by parent
	 * @param parentId
	 */
	public Company findActiveCompany(Long parentId);
	
	/**
	 * method to find active company by parent
	 * @param parentId
	 * @return
	 */
	public CompanyVo findActiveCompanyByParent(Long parentId);
	
	/**
	 * method to delete company
	 * @param id
	 */
	public void deleteCompany(Long id);
	
	/**
	 * method to find company
	 * @param id
	 */
	public Company findCompany(Long id);
	
	/**
	 * method to find company by id 
	 * @param id
	 * @return
	 */
	public CompanyVo findCompanyById(Long id);
	
	/**
	 * method to find company for user
	 * @param userId
	 * @return
	 */
	public CompanyVo findCompanyForUser(String userName);
	
	/**
	 * method to find all companies
	 * @return
	 */
	public List<CompanyVo> findAllCompanies();
	
	/**
	 * method to update default company of user
	 * @param userName
	 * @param companyId
	 * @return 
	 */
	public Boolean updateDefaultCompanyFOrUser(String userName, Long companyId);
	
	/**
	 * method to find companies of given branches
	 * @param branchIds
	 * @param companyId 
	 * @return
	 */
	public List<CompanyVo> findAllCompaniesOfBranches(List<Long> branchIds, Long companyId);
	
	/**
	 * method to find child companies
	 * @param companyId
	 * @return
	 */
	public List<CompanyVo> findChildCompanies(Long companyId); 
}
