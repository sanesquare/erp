package com.erp.web.dao.impl;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.erp.web.dao.CompanyStatusDao;
import com.erp.web.entities.CompanyStatus;
import com.hrms.web.dao.AbstractDao;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
@Repository
public class CompanyStatusDaoImpl extends AbstractDao implements CompanyStatusDao{

	public CompanyStatus findStatus(String status) {
		return (CompanyStatus) this.sessionFactory.getCurrentSession().createCriteria(CompanyStatus.class).add(Restrictions.eq("status", status)).uniqueResult();
	}

	public CompanyStatus findStatus(Long id) {
		return (CompanyStatus) this.sessionFactory.getCurrentSession().createCriteria(CompanyStatus.class).add(Restrictions.eq("id", id)).uniqueResult();
	}

}
