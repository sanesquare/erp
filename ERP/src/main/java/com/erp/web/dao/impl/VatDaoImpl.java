package com.erp.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bouncycastle.asn1.isismtt.x509.Restriction;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.internal.CriteriaImpl.CriterionEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.LedgerGroupDao;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.LedgerGroup;
import com.accounts.web.vo.LedgerGroupVo;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.VatDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.VAT;
import com.erp.web.vo.VatVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 25, 2015
 */
@Repository
public class VatDaoImpl extends AbstractDao implements VatDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private LedgerGroupDao ledgerGroupDao;

	public void saveOrUpdateTax(VatVo vatVo) {
		vatVo.setTaxName(vatVo.getTaxName().trim());
		Ledger ledger = null;
		Company company = companyDao.findCompany(vatVo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		VAT vatComparison = findTAxObject(vatVo.getTaxName(), company);		
		VAT vat = findTaxObject(vatVo.getId());
		
		if (vat == null) {				
			if (vatComparison!=null)
				throw new DuplicateItemException("Tax Name Already Exists");			
			vat = new VAT();
			vat.setName(vatVo.getTaxName());
			vat.setPercentage(vatVo.getRate());
			vat.setCompany(company);
			ledger = new Ledger();
			ledger.setCompany(company);
			ledger.setIsEditable(false);
			ledger.setBalance(BigDecimal.ZERO);
			LedgerGroup ledgerGroup = ledgerGroupDao.findLedgerGroup(AccountsConstants.DUTIES_AND_TAXES,
					vatVo.getCompanyId());
			if (ledgerGroup == null)
				throw new ItemNotFoundException("Ledger Group Not Found.");
			ledger.setLedgerGroup(ledgerGroup);
		} else {
			if (vatComparison!= null && vat.equals(vatComparison))
				throw new DuplicateItemException("Tax Name Already Exists");
			vat.setName(vatVo.getTaxName());
			vat.setPercentage(vatVo.getRate());
			vat.setCompany(company);
			ledger = vat.getLedger();
		}
		ledger.setLedgerName(vatVo.getTaxName());
		ledger.setOpeningBalance(BigDecimal.ZERO);
		ledger.setStartDate(company.getPeriodStart());
		ledger.setVat(vat);
		vat.setLedger(ledger);
		this.sessionFactory.getCurrentSession().saveOrUpdate(vat);
	}

	public void deleteTax(Long id) {
		VAT vat = findTaxObject(id);
		if (vat == null)
			throw new ItemNotFoundException("Tax Not Found");
		else {
			this.sessionFactory.getCurrentSession().delete(vat);
		}
	}

	public VatVo findTax(Long id) {
		VAT vat = findTaxObject(id);
		if (vat == null)
			throw new ItemNotFoundException("Tax Not Found");
		return createVo(vat);
	}

	private VatVo createVo(VAT vat) {
		VatVo vo = new VatVo();
		vo.setId(vat.getId());
		vo.setCompanyId(vat.getCompany().getId());
		vo.setTaxName(vat.getName());
		vo.setRate(vat.getPercentage());
		return vo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<VatVo> listAllTaxesForCompany(Long companyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(VAT.class);
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<VAT> vats = criteria.list();
		List<VatVo> vos = new ArrayList<VatVo>();
		for (VAT vat : vats) {
			vos.add(createVo(vat));
		}
		return vos;
	}

	public VAT findTaxObject(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(VAT.class);
		criteria.add(Restrictions.eq("id", id));
		return (VAT) criteria.uniqueResult();
	}

	private VAT findTAxObject(String taxName, Company company) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(VAT.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("name", taxName));
		return (VAT) criteria.uniqueResult();
	}

	public VAT findTAxObject(String taxName, Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company not found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(VAT.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("name", taxName));
		return (VAT) criteria.uniqueResult();
	}

	public VatVo findTaxByName(String taxName, Long companyId) {
		VAT vat = findTAxObject(taxName, companyId);
		return createVo(vat);
	}

}
