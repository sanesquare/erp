package com.erp.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.LedgerGroupDao;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.LedgerGroup;
import com.accounts.web.service.LedgerService;
import com.accounts.web.vo.LedgerVo;
import com.erp.web.constants.ErpConstants;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.CompanyStatusDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.CompanyStatus;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.Godown;
import com.erp.web.entities.NumberProperty;
import com.erp.web.entities.Unit;
import com.erp.web.vo.CompanyVo;
import com.erp.web.vo.ErpCurrencyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.UserDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.User;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.BranchService;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryConstants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
@Repository
public class CompanyDaoImpl extends AbstractDao implements CompanyDao {

	@Autowired
	private BranchDao branchDao;

	@Autowired
	private ErpCurrencyDao currencyDao;

	@Autowired
	private CompanyStatusDao statusDao;

	@Autowired
	private UserDao userDao;

	public void saveCompany(CompanyVo companyVo) {
		Company company = findCompany(companyVo.getCompanyId());
		if (company == null) {
			company = new Company();
			company = this.setLedgerGroupToCompany(company);
			company = this.setLedgersToCompany(company, companyVo);
			company = this.setNumberPropertiesToCompany(company);
			company = this.setUnitsToCompany(company);
			Godown godown = new Godown();
			godown.setCompany(company);
			godown.setName("Main Godown");
			company.getGodowns().add(godown);
			Godown toBeIssued = new Godown();
			toBeIssued.setCompany(company);
			toBeIssued.setName(InventoryConstants.TO_BE_ISSUED_GODOWN);
			company.getGodowns().add(toBeIssued);
		}
		Date periodEndDate = DateFormatter.convertStringToDate(companyVo.getPeriodEnd());
		CompanyStatus status = null;
		/*
		 * if (periodEndDate.compareTo(new Date()) >= 0) status =
		 * statusDao.findStatus(ErpConstants.ACTIVE_STATUS); else status =
		 * statusDao.findStatus(ErpConstants.EXPIRED_STATUS);
		 */
		status = statusDao.findStatus(ErpConstants.ACTIVE_STATUS);
		Branch parent = branchDao.findAndReturnBranchObjectById(companyVo.getParentId());
		ErPCurrency currency = currencyDao.findCurrencyApplicableForDate(companyVo.getCurrencyId(),
				DateFormatter.convertStringToDate(companyVo.getPeriodStart()));
		if (currency == null) {
			ErpCurrencyVo currencyVo = new ErpCurrencyVo();
			// currencyVo.setDate(DateFormatter.convertDateToString(company.getPeriodStart()));
			currencyVo.setDate(companyVo.getPeriodStart());
			currencyVo.setRate(new BigDecimal(1));
			currencyVo.setBaseCurrencyId(companyVo.getCurrencyId());
			currency = currencyDao.saveDefaultCurrency(currencyVo);
		}
		company.setStatus(status);
		company.setParent(parent);
		parent.getCompanies().add(company);
		company.setCurrency(currency);
		company = setCompany(company, companyVo);
		this.sessionFactory.getCurrentSession().saveOrUpdate(company);
	}

	/**
	 * method to set units to company
	 * 
	 * @param company
	 * @return
	 */
	private Company setUnitsToCompany(Company company) {
		for (Unit ut : findDefaultUnits()) {
			Unit unit = new Unit();
			unit.setIsDefault(false);
			unit.setName(ut.getName());
			unit.setSymbol(ut.getSymbol());
			unit.setDecimalPlaces(ut.getDecimalPlaces());
			unit.setCompany(company);
			company.getUnits().add(unit);
		}
		return company;
	}

	/**
	 * method to set number properties to company
	 * 
	 * @param company
	 * @return
	 */
	private Company setNumberPropertiesToCompany(Company company) {
		for (NumberProperty numberProperty : findDefaultNumberProperties()) {
			if (!numberProperty.getCategory().equalsIgnoreCase(NumberPropertyConstants.EMPLOYEE)) {
				NumberProperty property = new NumberProperty();
				property.setCompany(company);
				property.setCategory(numberProperty.getCategory());
				property.setPrefix(numberProperty.getPrefix());
				property.setNumber(numberProperty.getNumber());
				company.getNumberProperties().add(property);
			}
		}
		return company;
	}

	/**
	 * method to set ledger groups to company
	 * 
	 * @param company
	 * @return
	 */
	private Company setLedgerGroupToCompany(Company company) {
		for (LedgerGroup lg : findDefaultGroups()) {
			LedgerGroup group = new LedgerGroup();
			group.setAccountType(lg.getAccountType());
			group.setIsEditable(false);
			group.setIsInitial(false);
			group.setCompany(company);
			group.setGroupName(lg.getGroupName());
			company.getLedgerGroups().add(group);
		}
		return company;
	}

	/**
	 * method to set ledgers to company
	 * 
	 * @param company
	 * @param companyVo
	 * @return
	 */
	private Company setLedgersToCompany(Company company, CompanyVo companyVo) {
		Set<LedgerGroup> ledgerGroups = company.getLedgerGroups();
		Map<String, LedgerGroup> map = new HashMap<String, LedgerGroup>();
		for (LedgerGroup ledgerGroup : ledgerGroups)
			map.put(ledgerGroup.getGroupName(), ledgerGroup);
		for (Ledger ledger : findDefaultLedgers()) {
			Ledger newLedger = new Ledger();
			newLedger.setBalance(ledger.getBalance());
			newLedger.setOpeningBalance(ledger.getOpeningBalance());
			newLedger.setIsEditable(false);
			newLedger.setLedgerName(ledger.getLedgerName());
			newLedger.setStartDate(DateFormatter.convertStringToDate(companyVo.getPeriodStart()));
			newLedger.setLedgerGroup(map.get(ledger.getLedgerGroup().getGroupName()));
			newLedger.setCompany(company);
			company.getLedgers().add(newLedger);
		}
		return company;
	}

	@SuppressWarnings("unchecked")
	private List<LedgerGroup> findDefaultGroups() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LedgerGroup.class);
		criteria.add(Restrictions.eq("isInitial", true));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<LedgerGroup> groups = criteria.list();
		return groups;
	}

	@SuppressWarnings("unchecked")
	private List<Unit> findDefaultUnits() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Unit.class);
		criteria.add(Restrictions.eq("isDefault", true));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Unit> units = criteria.list();
		return units;
	}

	/**
	 * method to find default ledgers
	 * 
	 * @return 
	 */
	@SuppressWarnings({ "unchecked" })
	private List<Ledger> findDefaultLedgers() {
		List<Ledger> ledgers = new ArrayList<Ledger>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Ledger.class);
		criteria.add(Restrictions.eq("isInitial", true));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		ledgers = criteria.list();
		return ledgers;
	}

	/**
	 * method to find default number properties
	 * 
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	private List<NumberProperty> findDefaultNumberProperties() {
		List<NumberProperty> numberProperties = new ArrayList<NumberProperty>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(NumberProperty.class);
		criteria.add(Restrictions.isNull("company"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		numberProperties = criteria.list();
		return numberProperties;
	}

	/**
	 * method to set company
	 * 
	 * @param company
	 * @param companyVo
	 * @return
	 */
	private Company setCompany(Company company, CompanyVo companyVo) {
		company.setName(companyVo.getCompanyName());
		company.setAddress(companyVo.getAddress());
		company.setAccountName(companyVo.getAccountName());
		company.setAccountNumber(companyVo.getAccountNumber());
		company.setBankName(companyVo.getBank());
		company.setBankBranch(companyVo.getBankBranch());
		company.setPhone(companyVo.getPhone());
		company.setEmail(companyVo.getEmail());
		company.setSalesTaxNumber(companyVo.getSalesTaxNumber());
		company.setPeriodEnd(DateFormatter.convertStringToDate(companyVo.getPeriodEnd()));
		company.setPeriodStart(DateFormatter.convertStringToDate(companyVo.getPeriodStart()));
		return company;
	}

	/**
	 * method to create company vo
	 * 
	 * @param company
	 * @return
	 */
	private CompanyVo createCompanyVo(Company company) {
		CompanyVo vo = new CompanyVo();
		vo.setCompanyId(company.getId());
		vo.setCurrencyId(company.getCurrency().getBaseCurrency().getId());
		vo.setParentId(company.getParent().getId());
		vo.setCompanyName(company.getName());
		vo.setParent(company.getParent().getBranchName());
		vo.setAddress(company.getAddress());
		vo.setAccountName(company.getAccountName());
		vo.setAccountNumber(company.getAccountNumber());
		vo.setBank(company.getBankName());
		vo.setBankBranch(company.getBankBranch());
		vo.setPhone(company.getPhone());
		vo.setEmail(company.getEmail());
		vo.setSalesTaxNumber(company.getSalesTaxNumber());
		vo.setPeriodEnd(DateFormatter.convertDateToString(company.getPeriodEnd()));
		vo.setPeriodStart(DateFormatter.convertDateToString(company.getPeriodStart()));
		vo.setStatus(company.getStatus().getStatus());
		vo.setStatusId(company.getStatus().getId());
		return vo;
	}

	public Company findActiveCompany(Long parentId) {
		CompanyStatus status = statusDao.findStatus(ErpConstants.ACTIVE_STATUS);
		Branch branch = branchDao.findAndReturnBranchObjectById(parentId);
		return (Company) this.sessionFactory.getCurrentSession().createCriteria(Company.class)
				.add(Restrictions.eq("status", status)).add(Restrictions.eq("parent", branch)).uniqueResult();
	}

	public CompanyVo findActiveCompanyByParent(Long parentId) {
		Company company = findActiveCompany(parentId);
		if (company == null)
			return null;
		else
			return createCompanyVo(company);
	}

	public void deleteCompany(Long id) {
		Company company = findCompany(id);
		Set<User> users = company.getUsers();
		for (User user : users) {
			user.setCompany(null);
			this.sessionFactory.getCurrentSession().update(user);
		}
		if (company != null)
			this.sessionFactory.getCurrentSession().delete(company);
	}

	public Company findCompany(Long id) {
		return (Company) this.sessionFactory.getCurrentSession().createCriteria(Company.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public CompanyVo findCompanyById(Long id) {
		Company company = findCompany(id);
		if (company == null)
			return null;
		else
			return createCompanyVo(company);
	}

	public CompanyVo findCompanyForUser(String userName) {
		User user = userDao.findUserByUsername(userName);
		if (user == null)
			throw new ItemNotFoundException("User not exist.");
		Branch branch = user.getEmployee().getBranch();
		return findActiveCompanyByParent(branch.getId());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<CompanyVo> findAllCompanies() {
		List<CompanyVo> companyVos = new ArrayList<CompanyVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Company.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Company> companies = criteria.list();
		for (Company company : companies) {
			companyVos.add(createCompanyVo(company));
		}
		return companyVos;
	}

	public Boolean updateDefaultCompanyFOrUser(String userName, Long companyId) {
		Company company = findCompany(companyId);
		User user = userDao.findUserByUsername(userName);
		company.getUsers().add(user);
		user.setCompany(company);
		this.sessionFactory.getCurrentSession().update(user);
		return true;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<CompanyVo> findAllCompaniesOfBranches(List<Long> branchIds, Long companyId) {
		List<CompanyVo> companyVos = new ArrayList<CompanyVo>();
		if (branchIds.isEmpty())
			branchIds.add(0L);
		Criterion branches = Restrictions.in("parent.id", branchIds);
		Criterion id = Restrictions.eq("id", companyId);

		LogicalExpression expression = Restrictions.or(branches, id);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Company.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(expression);
		List<Company> companies = criteria.list();
		for (Company company : companies) {
			companyVos.add(createCompanyVo(company));
		}
		return companyVos;
	}

	public List<CompanyVo> findChildCompanies(Long companyId) {
		Company company = this.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company not exist.");
		List<Company> companies = new ArrayList<Company>();
		companies.add(company);
		Branch branch = company.getParent();
		Set<Branch> subBranches = branch.getBranches();
		if (!subBranches.isEmpty()) {
			for (Branch subBranch : subBranches)
				companies.add(this.findActiveCompany(subBranch.getId()));
		}
		List<CompanyVo> companyVos = new ArrayList<CompanyVo>();
		for (Company companyObj : companies) {
			if (companyObj != null)
				companyVos.add(createCompanyVo(companyObj));
		}
		return companyVos;
	}

}
