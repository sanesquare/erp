package com.erp.web.dao;

import java.util.List;

import com.erp.web.entities.VAT;
import com.erp.web.vo.VatVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 25, 2015
 */
public interface VatDao {
	
	/**
	 * Method to save or  update vat		
	 * @param vatVo
	 */
	public void saveOrUpdateTax(VatVo vatVo);
	
	/**
	 * Method to delete tax
	 * @param id
	 */
	public void deleteTax(Long id);
	
	/**
	 * Method to find tax
	 * @param id
	 * @return
	 */
	public VatVo findTax(Long id);
	
	/**
	 * Method to list all taxes for company
	 * @param companyId
	 * @return
	 */
	public List<VatVo> listAllTaxesForCompany(Long companyId);
	
	/**
	 * Method to find tax object
	 * @param id
	 * @return
	 */
	public VAT findTaxObject(Long id);
	
	/**
	 * Method to find tax object by name and companyId
	 * @param taxName
	 * @param companyId
	 * @return
	 */
	public VAT findTAxObject(String taxName , Long companyId);
	
	/**
	 * Method to find tax by name and company id
	 * @param taxName
	 * @param companyId
	 * @return
	 */
	public VatVo findTaxByName(String taxName , Long companyId);

}
