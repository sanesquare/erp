package com.erp.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.DeliveryMethodDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.vo.DeliveryMethodVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Dec 14, 2015
 */
@Repository
public class DeliveryMethodDaoImpl extends AbstractDao implements DeliveryMethodDao {

	@Autowired
	private CompanyDao companyDao;

	public void saveDeliveryMethod(DeliveryMethodVo deliveryMethodVo) {
		Long id = deliveryMethodVo.getDeliveryMethodId();
		Company company = companyDao.findCompany(deliveryMethodVo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		DeliveryMethod method = null;
		if (id == null) {
			method = findDeliveryMethodByMethod(deliveryMethodVo.getMethod());
			if (method != null)
				throw new ItemNotFoundException("Delivery Term Already Exists");
			method = new DeliveryMethod();
			method.setCompany(company);
			method.setMethod(deliveryMethodVo.getMethod());
			this.sessionFactory.getCurrentSession().save(method);
		} else {
			method = findDeliveryMethodObject(id);
			method.setMethod(deliveryMethodVo.getMethod());
			this.sessionFactory.getCurrentSession().merge(method);
		}
	}

	public void deleteDeliveryMethod(Long id) {
		DeliveryMethod method = findDeliveryMethodObject(id);
		if (method == null)
			throw new ItemNotFoundException("Delivery Term Not Found");
		this.sessionFactory.getCurrentSession().delete(method);
	}

	private DeliveryMethodVo createVo(DeliveryMethod method) {
		DeliveryMethodVo vo = new DeliveryMethodVo();
		vo.setDeliveryMethodId(method.getId());
		vo.setMethod(method.getMethod());
		return vo;
	}

	@SuppressWarnings("unchecked")
	public List<DeliveryMethodVo> listAllDeliveryMethods(Long companyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(DeliveryMethod.class);
		criteria.add(Restrictions.eq("company.id", companyId));
		List<DeliveryMethod> methods = new ArrayList<DeliveryMethod>();
		List<DeliveryMethodVo> vos = new ArrayList<DeliveryMethodVo>();
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		methods = criteria.list();
		for (DeliveryMethod method : methods) {
			vos.add(this.createVo(method));
		}
		return vos;
	}

	public DeliveryMethod findDeliveryMethodObject(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(DeliveryMethod.class);
		criteria.add(Restrictions.eq("id", id));
		return (DeliveryMethod) criteria.uniqueResult();
	}

	public DeliveryMethodVo findDeliveryMethod(Long id) {
		DeliveryMethod method = findDeliveryMethodObject(id);
		if (method == null)
			throw new ItemNotFoundException("Delivery Method Not Found");
		return this.createVo(method);
	}

	public DeliveryMethod findDeliveryMethodByMethod(String method) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(DeliveryMethod.class);
		criteria.add(Restrictions.eq("method", method));
		return (DeliveryMethod) criteria.uniqueResult();
	}

}
