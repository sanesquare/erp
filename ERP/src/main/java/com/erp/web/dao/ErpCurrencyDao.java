package com.erp.web.dao;

import java.util.Date;
import java.util.List;

import com.erp.web.entities.ErPCurrency;
import com.erp.web.vo.ErpCurrencyVo;
import com.hrms.web.entities.BaseCurrency;
import com.hrms.web.vo.CurrencyVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
public interface ErpCurrencyDao {

	/**
	 * method to save or update currency
	 * @param currencyVo
	 */
	public void saveOrUpdateCurrency(ErpCurrencyVo currencyVo);
	
	/**
	 * method to save default currency
	 * @param currencyVo
	 * @return
	 */
	public ErPCurrency saveDefaultCurrency(ErpCurrencyVo currencyVo);
	
	/**
	 * method to find currency by id
	 * @param id
	 * @return
	 */
	public ErPCurrency findCurrency(Long id);
	
	/**
	 * method to find currency by base currency and date
	 * @param baseCurrencyId
	 * @param date
	 * @return
	 */
	public ErPCurrency findCurrencyByBaseCurrencyAndDate(Long baseCurrencyId , String date);
	
	/**
	 * method to find recent currency by base currency
	 * @param baseCurrencyId
	 * @return
	 */
	public ErPCurrency findRecentCurrencyByBaseCurrency(Long baseCurrencyId);
	
	/**
	 * find urrency applicable for date
	 * @param baseCurrencyId
	 * @param date
	 * @return
	 */
	public ErPCurrency findCurrencyApplicableForDate(Long baseCurrencyId,Date date);
	
	/**
	 * method to find recent currency by base currency
	 * @param baseCurrencyId
	 * @return
	 */
	public ErpCurrencyVo findRecentCurrencyVoByBaseCurrency(Long baseCurrencyId);
	
	/**
	 * method to find currency by id
	 * @param id
	 * @return
	 */
	public ErpCurrencyVo findCurrencyById(Long id);
	
	/**
	 * method to find all currencies
	 * @return
	 */
	public List<ErpCurrencyVo> findAllCurrencies(String date);
	/**
	 * 
	 * @param date
	 * @return
	 */
	public ErpCurrencyVo findCurrencyForDate(Date date , BaseCurrency baseCurrency);
	
}
