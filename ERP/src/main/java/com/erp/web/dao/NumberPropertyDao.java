package com.erp.web.dao;

import com.erp.web.entities.Company;
import com.erp.web.entities.NumberProperty;
import com.erp.web.vo.NumberPropertyVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-09-2015
 */
public interface NumberPropertyDao {

	/**
	 * method to save number property
	 * @param vo
	 */
	public void saveNumberProperty(NumberPropertyVo vo);
	
	/**
	 * method to find number property for given category and company
	 * 
	 * @param category
	 * @param companyId
	 * @return
	 */
	public NumberPropertyVo findNumberProperty(String category, Long companyId);
	
	public NumberPropertyVo findNumberProperty(String category, Company company);
	
	public String getRefernceNumber(String category,Long companyId);
	
	public String getRefernceNumber(String category,Company company);
	
	/**
	 * method to find number property for given category and company
	 * @param category
	 * @return
	 */
	public NumberPropertyVo findNumberProperty(String category);

	/**
	 * method to increment number property
	 * 
	 * @param category
	 * 
	 * @param companyId
	 */
	public void incrementNumberProperty(String category, Company company);
	
	/**
	 * method to increment number property
	 * @param category
	 */
	public void incrementNumberProperty(String category);
	
	public NumberProperty findNumberProperty(Company company,String prefix);
}
