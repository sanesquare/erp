package com.erp.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.vo.PaymentItemsVo;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.PaymentMethodDao;
import com.erp.web.dao.PaymentTermsDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.PaymentMethod;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.PaymentTermsInstallments;
import com.erp.web.vo.PaymentTermInstallmentVo;
import com.erp.web.vo.PaymentTermsVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 28, 2015
 */
@Repository
public class PaymentTermsDaoImpl extends AbstractDao implements PaymentTermsDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private PaymentMethodDao paymentMethodDao;

	public void savePaymentTerms(PaymentTermsVo vo) {
		vo.setName(vo.getName().trim());
		Company company = companyDao.findCompany(vo.getCompanyId());
		PaymentTerms paymentTerms = this.findPaymentTerms(vo.getId());
		PaymentTerms paymentTermsWithSameNameCompany = this.findPaymentTerms(company, vo.getName());
		if (paymentTerms == null) {
			if (paymentTermsWithSameNameCompany != null)
				throw new DuplicateItemException("Payment Term Already Exists.");
			paymentTerms = new PaymentTerms();
			paymentTerms.setCompany(company);
			paymentTerms.setTerm(vo.getName());
			company.getPaymentTerms().add(paymentTerms);
			paymentTerms = this.setAttributes(paymentTerms, vo);
		} else {
			if (paymentTermsWithSameNameCompany != null && !paymentTerms.equals(paymentTermsWithSameNameCompany))
				throw new DuplicateItemException("Payment Term Already Exists.");
			paymentTerms.setTerm(vo.getName());
			paymentTerms = this.setAttributes(paymentTerms, vo);
		}
		this.sessionFactory.getCurrentSession().saveOrUpdate(paymentTerms);
	}

	/**
	 * method to set attributes to payment
	 * 
	 * @param paymentTerms
	 * @param vo
	 * @return
	 */
	private PaymentTerms setAttributes(PaymentTerms paymentTerms, PaymentTermsVo vo) {
		paymentTerms.getInstallments().clear();
		for (PaymentTermInstallmentVo installmentVo : vo.getInstallmentVos()) {
			PaymentMethod paymentMethod = paymentMethodDao.findPaymentMethod(installmentVo.getPaymentMethodId());
			PaymentTermsInstallments installments = (PaymentTermsInstallments) this.sessionFactory.getCurrentSession()
					.createCriteria(PaymentTermsInstallments.class).add(Restrictions.eq("id", installmentVo.getId()))
					.uniqueResult();
			if(installments==null)
			{
				installments=new PaymentTermsInstallments();
				installments.setTerms(paymentTerms);
				paymentTerms.getInstallments().add(installments);
			}
			installments.setMethod(paymentMethod);
			installments.setPercentage(installmentVo.getPercentage());
			installments.setNoOfDays(installmentVo.getNofdays());
		}
		return paymentTerms;
	}

	public void deletePaymentTerms(Long id) {
		PaymentTerms paymentTerms = this.findPaymentTerms(id);
		if (paymentTerms == null)
			throw new ItemNotFoundException("Payment Term Not Found");
		this.sessionFactory.getCurrentSession().delete(paymentTerms);
	}

	public void deletePaymentTerms(Long companyId, String paymentTermName) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		PaymentTerms paymentTerms = this.findPaymentTerms(company, paymentTermName);
		if (paymentTerms == null)
			throw new ItemNotFoundException("Payment Term Not Found");
		this.sessionFactory.getCurrentSession().delete(paymentTerms);
	}

	public PaymentTerms findPaymentTerms(Long id) {
		return (PaymentTerms) this.sessionFactory.getCurrentSession().createCriteria(PaymentTerms.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public PaymentTerms findPaymentTerms(Company company, String paymentTermName) {
		return (PaymentTerms) this.sessionFactory.getCurrentSession().createCriteria(PaymentTerms.class)
				.add(Restrictions.eq("company", company)).add(Restrictions.eq("term", paymentTermName)).uniqueResult();
	}

	public PaymentTermsVo findPaymentTermById(Long id) {
		PaymentTerms paymentTerms = this.findPaymentTerms(id);
		if (paymentTerms == null)
			throw new ItemNotFoundException("Payment Term Not Found");
		return createPaymentTermsVo(paymentTerms);
	}

	public PaymentTermsVo findPaymentTermByName(Long companyId, String paymentTermName) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		PaymentTerms paymentTerms = this.findPaymentTerms(company, paymentTermName);
		if (paymentTerms == null)
			throw new ItemNotFoundException("Payment Term Not Found");
		return createPaymentTermsVo(paymentTerms);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PaymentTermsVo> findAllPaymentTerms(Long companyId) {
		List<PaymentTermsVo> paymentTermsVos = new ArrayList<PaymentTermsVo>();
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaymentTerms.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("id"));
		List<PaymentTerms> paymentTerms = criteria.list();
		for (PaymentTerms terms : paymentTerms)
			paymentTermsVos.add(createPaymentTermsVo(terms));
		return paymentTermsVos;
	}

	/**
	 * method to create payment terms vo
	 * 
	 * @param paymentTerms
	 * @return
	 */
	private PaymentTermsVo createPaymentTermsVo(PaymentTerms paymentTerms) {
		PaymentTermsVo vo = new PaymentTermsVo();
		vo.setId(paymentTerms.getId());
		vo.setName(paymentTerms.getTerm());
		for (PaymentTermsInstallments installments : paymentTerms.getInstallments()) {
			PaymentTermInstallmentVo installmentVo = new PaymentTermInstallmentVo();
			installmentVo.setId(installments.getId());
			installmentVo.setNofdays(installments.getNoOfDays());
			installmentVo.setPercentage(installments.getPercentage());
			PaymentMethod method = installments.getMethod();
			installmentVo.setPaymentMethodId(method.getId());
			installmentVo.setPaymentMethod(method.getMethod());
			vo.getInstallmentVos().add(installmentVo);
		}
		return vo;
	}
}
