package com.erp.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.vo.ErpCurrencyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BaseCurrencyDao;
import com.hrms.web.entities.BaseCurrency;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.CurrencyVo;
import com.itextpdf.text.pdf.fonts.cmaps.CidLocation;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
@Repository
public class ErpCurrencyDaoImpl extends AbstractDao implements ErpCurrencyDao {

	@Autowired
	private BaseCurrencyDao baseCurrencyDao;

	public void saveOrUpdateCurrency(ErpCurrencyVo currencyVo) {
		BaseCurrency baseCurrency = baseCurrencyDao.findBaseCurrency(currencyVo.getBaseCurrencyId());
		ErPCurrency currency = findCurrencyByBaseCurrencyAndDate(currencyVo.getBaseCurrencyId(), currencyVo.getDate());
		if (currency == null)
			currency = new ErPCurrency();
		currency.setDate(DateFormatter.convertStringToDate(currencyVo.getDate()));
		currency.setRate(currencyVo.getRate());
		currency.setBaseCurrency(baseCurrency);
		this.sessionFactory.getCurrentSession().saveOrUpdate(currency);
	}

	public ErPCurrency findCurrency(Long id) {
		return (ErPCurrency) this.sessionFactory.getCurrentSession().createCriteria(ErPCurrency.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public ErPCurrency findCurrencyByBaseCurrencyAndDate(Long baseCurrencyId, String date) {
		return (ErPCurrency) this.sessionFactory.getCurrentSession().createCriteria(ErPCurrency.class)
				.add(Restrictions.eq("baseCurrency", baseCurrencyDao.findBaseCurrency(baseCurrencyId)))
				.add(Restrictions.eq("date", DateFormatter.convertStringToDate(date))).uniqueResult();
	}

	public ErPCurrency findRecentCurrencyByBaseCurrency(Long baseCurrencyId) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ErPCurrency.class)
				.add(Restrictions.eq("baseCurrency", baseCurrencyDao.findBaseCurrency(baseCurrencyId)))
				.setProjection(Projections.max("id"));
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ErPCurrency.class);
		criteria.add(Property.forName("id").eq(detachedCriteria));
		return (ErPCurrency) criteria.uniqueResult();
	}

	public ErpCurrencyVo findRecentCurrencyVoByBaseCurrency(Long baseCurrencyId) {
		ErPCurrency currency = findRecentCurrencyByBaseCurrency(baseCurrencyId);
		if (currency == null)
			throw new ItemNotFoundException("Currency Not Exist.");
		return createCurrencyVo(currency);
	}

	/**
	 * method to create currency vo
	 * 
	 * @param currency
	 * @return
	 */
	private ErpCurrencyVo createCurrencyVo(ErPCurrency currency) {
		ErpCurrencyVo vo = new ErpCurrencyVo();
		vo.setCurrency(currency.getBaseCurrency().getBaseCurrency());
		vo.setBaseCurrencyId(currency.getBaseCurrency().getId());
		vo.setRate(currency.getRate());
		vo.setErpCurrencyId(currency.getId());
		vo.setDate(DateFormatter.convertDateToString(currency.getDate()));
		return vo;
	}

	public ErpCurrencyVo findCurrencyById(Long id) {
		ErPCurrency currency = findCurrency(id);
		if (currency == null)
			throw new ItemNotFoundException("Currency Not Exist.");
		return createCurrencyVo(currency);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ErpCurrencyVo> findAllCurrencies(String date) {
		List<ErpCurrencyVo> currencyVos = new ArrayList<ErpCurrencyVo>();
		/*Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ErPCurrency.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<ErPCurrency> currencies = criteria.list();
		
		for(ErPCurrency currency : currencies){
			currencyVos.add(createCurrencyVo(currency));
		}*/
		
		String query= "select DISTINCT baseCurrency from ErPCurrency";
		Query que=this.sessionFactory.getCurrentSession().createQuery(query);
		List<BaseCurrency> baseCurrencies = que.list();
		for(BaseCurrency currency : baseCurrencies){
			ErpCurrencyVo vo = this.findCurrencyForDate(DateFormatter.convertStringToDate(date), currency);
			if(vo!=null)
				currencyVos.add(vo);
		}
		return currencyVos;
	}

	public ErPCurrency saveDefaultCurrency(ErpCurrencyVo currencyVo) {
		BaseCurrency baseCurrency = baseCurrencyDao.findBaseCurrency(currencyVo.getBaseCurrencyId());
		ErPCurrency currency = new ErPCurrency();
		currency.setDate(DateFormatter.convertStringToDate(currencyVo.getDate()));
		currency.setRate(currencyVo.getRate());
		currency.setBaseCurrency(baseCurrency);
		Long id = (Long) this.sessionFactory.getCurrentSession().save(currency);
		return findCurrency(id);
	}
	
	public ErpCurrencyVo findCurrencyForDate(Date date , BaseCurrency baseCurrency){
		ErPCurrency currency =null;
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ErPCurrency.class);
		detachedCriteria.add(Restrictions.eq("baseCurrency", baseCurrency));
		detachedCriteria.add(Restrictions.le("date", date));
		detachedCriteria.setProjection(Projections.max("id"));
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ErPCurrency.class);
		criteria.add(Property.forName("id").eq(detachedCriteria));
		currency=(ErPCurrency) criteria.uniqueResult();
		if(currency!=null)
			return createCurrencyVo(currency);
		return null;
	}

	public ErPCurrency findCurrencyApplicableForDate(Long baseCurrencyId, Date date) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ErPCurrency.class)
				.add(Restrictions.eq("baseCurrency", baseCurrencyDao.findBaseCurrency(baseCurrencyId)))
				.add(Restrictions.le("date", date))
				.setProjection(Projections.max("id"));
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ErPCurrency.class);
		criteria.add(Property.forName("id").eq(detachedCriteria));
		return (ErPCurrency) criteria.uniqueResult();
	}
}
