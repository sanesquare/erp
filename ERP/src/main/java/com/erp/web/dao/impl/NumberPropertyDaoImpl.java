package com.erp.web.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.NumberProperty;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-09-2015
 */
@Repository
public class NumberPropertyDaoImpl extends AbstractDao implements NumberPropertyDao {

	@Autowired
	private CompanyDao companyDao;

	public NumberPropertyVo findNumberProperty(String category, Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Exists.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(NumberProperty.class);
		if (!category.equalsIgnoreCase(NumberPropertyConstants.EMPLOYEE))
			criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("category", category));
		return createNumberPropertyVo((NumberProperty) criteria.uniqueResult());
	}

	public void incrementNumberProperty(String category, Company company) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(NumberProperty.class);
		if (!category.equalsIgnoreCase(NumberPropertyConstants.EMPLOYEE))
			criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("category", category));
		NumberProperty numberProperty = (NumberProperty) criteria.uniqueResult();
		if (numberProperty == null)
			throw new ItemNotFoundException("Number Property Not Exists.");
		numberProperty.setNumber(numberProperty.getNumber() + 1);
		this.sessionFactory.getCurrentSession().merge(numberProperty);
	}

	/**
	 * method to create number property vo
	 * 
	 * @param numberProperty
	 * @return
	 */
	private NumberPropertyVo createNumberPropertyVo(NumberProperty numberProperty) {
		NumberPropertyVo vo = new NumberPropertyVo();
		vo.setId(numberProperty.getId());
		vo.setCategory(numberProperty.getCategory());
		vo.setPrefix(numberProperty.getPrefix());
		vo.setNumber(numberProperty.getNumber());
		vo.setPreview(numberProperty.getPrefix() + numberProperty.getNumber());
		return vo;
	}

	public NumberPropertyVo findNumberProperty(String category) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(NumberProperty.class);
		criteria.add(Restrictions.eq("category", category));
		return createNumberPropertyVo((NumberProperty) criteria.uniqueResult());
	}

	public void incrementNumberProperty(String category) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(NumberProperty.class);
		criteria.add(Restrictions.eq("category", category));
		NumberProperty numberProperty = (NumberProperty) criteria.uniqueResult();
		if (numberProperty == null)
			throw new ItemNotFoundException("Number Property Not Exists.");
		numberProperty.setNumber(numberProperty.getNumber() + 1);
		this.sessionFactory.getCurrentSession().merge(numberProperty);
	}

	public void saveNumberProperty(NumberPropertyVo vo) {
		Company company = companyDao.findCompany(vo.getCompanyId());
		if(company==null)
			throw new ItemNotFoundException("Company Not Found");
		NumberProperty numberProperty = this.fincNumberProperty(vo.getId());
		if (numberProperty == null)
			throw new ItemNotFoundException("Number Property Not Found");
		NumberProperty numberPropertyWithSamePrefix = this.findNumberProperty(company,vo.getPrefix());
		if(numberPropertyWithSamePrefix!=null && !numberPropertyWithSamePrefix.equals(numberProperty))
			throw new DuplicateItemException("Prefix "+vo.getPrefix()+" already exists.");
		Long number = numberProperty.getNumber();
		if (vo.getNumber() != null && vo.getNumber().compareTo(number) > 0)
			numberProperty.setNumber(vo.getNumber());
		numberProperty.setPrefix(vo.getPrefix());
		this.sessionFactory.getCurrentSession().merge(numberProperty);
	}

	private NumberProperty fincNumberProperty(Long id) {
		return (NumberProperty) this.sessionFactory.getCurrentSession().createCriteria(NumberProperty.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public NumberProperty findNumberProperty(Company company, String prefix) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(NumberProperty.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("prefix", prefix));
		return (NumberProperty) criteria.uniqueResult();
	}

	public String getRefernceNumber(String category, Long companyId) {
		NumberPropertyVo numberPropertyVo = this.findNumberProperty(category, companyId);
		return numberPropertyVo.getPrefix()+numberPropertyVo.getNumber();
	}

	public String getRefernceNumber(String category, Company company) {
		NumberPropertyVo numberPropertyVo = this.findNumberProperty(category, company);
		return numberPropertyVo.getPrefix()+numberPropertyVo.getNumber();
	}

	public NumberPropertyVo findNumberProperty(String category, Company company) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(NumberProperty.class);
		if (!category.equalsIgnoreCase(NumberPropertyConstants.EMPLOYEE))
			criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("category", category));
		return createNumberPropertyVo((NumberProperty) criteria.uniqueResult());
	}
}
