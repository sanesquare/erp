package com.erp.web.dao;

import com.erp.web.entities.CompanyStatus;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
public interface CompanyStatusDao {

	public CompanyStatus findStatus(String status);
	
	public CompanyStatus findStatus(Long id);
}
