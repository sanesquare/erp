package com.erp.web.dao.impl;

import java.util.ArrayList;
import java.util.DuplicateFormatFlagsException;
import java.util.List;

import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.PaymentMethodDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.PaymentMethod;
import com.erp.web.vo.PaymentMethodVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 26, 2015
 */
@Repository
public class PaymentMethodDaoImpl extends AbstractDao implements PaymentMethodDao {

	@Autowired
	private CompanyDao companyDao;

	public void savePaymentMethod(PaymentMethodVo vo) {
		vo.setName(vo.getName().trim());
		PaymentMethod method = this.findPaymentMethod(vo.getId());
		Company company = null;
		if (method == null) {
			method = new PaymentMethod();
			company = companyDao.findCompany(vo.getCompanyId());
			if (company == null)
				throw new ItemNotFoundException("Company Not Found.");
			if (this.findPaymentMethod(vo.getName(), company) != null)
				throw new DuplicateItemException("Payment Method Already Exist.");
			company.getPaymentMethods().add(method);
			method.setCompany(company);
		} else {
			company = method.getCompany();
			PaymentMethod methodWithSameName = this.findPaymentMethod(vo.getName(), company);
			if (methodWithSameName != null && !method.equals(methodWithSameName))
				throw new DuplicateItemException("Payment Method Already Exist.");
		}
		method.setMethod(vo.getName());
		this.sessionFactory.getCurrentSession().saveOrUpdate(method);
	}

	public void deletePaymentMethod(Long id) {
		PaymentMethod paymentMethod = this.findPaymentMethod(id);
		if (paymentMethod == null)
			throw new ItemNotFoundException("Payment Method Not Found.");
		this.sessionFactory.getCurrentSession().delete(paymentMethod);
	}

	public void deletePaymentMethod(String name, Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		PaymentMethod paymentMethod = this.findPaymentMethod(name, company);
		if (paymentMethod == null)
			throw new ItemNotFoundException("Payment Method Not Found.");
		this.sessionFactory.getCurrentSession().delete(paymentMethod);
	}

	public PaymentMethod findPaymentMethod(Long id) {
		return (PaymentMethod) this.sessionFactory.getCurrentSession().createCriteria(PaymentMethod.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public PaymentMethod findPaymentMethod(String name, Company company) {
		return (PaymentMethod) this.sessionFactory.getCurrentSession().createCriteria(PaymentMethod.class)
				.add(Restrictions.eq("method", name)).add(Restrictions.eq("company", company)).uniqueResult();
	}

	public PaymentMethodVo findPaymentMethodById(Long id) {
		PaymentMethod paymentMethod = this.findPaymentMethod(id);
		if (paymentMethod == null)
			throw new ItemNotFoundException("Payment Method Not Found.");
		return createPaymentMethodVo(paymentMethod);
	}

	public PaymentMethodVo findPaymentMethodByName(String name, Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		PaymentMethod paymentMethod = this.findPaymentMethod(name, company);
		if (paymentMethod == null)
			throw new ItemNotFoundException("Payment Method Not Found.");
		return createPaymentMethodVo(paymentMethod);
	}

	@SuppressWarnings("unchecked")
	public List<PaymentMethodVo> findAllPaymentMethods(Long companyId) {
		List<PaymentMethodVo> methodVos = new ArrayList<PaymentMethodVo>();
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaymentMethod.class);
		criteria.add(Restrictions.eq("company", company)).setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PaymentMethod> paymentMethods = criteria.list();
		for (PaymentMethod paymentMethod : paymentMethods)
			methodVos.add(createPaymentMethodVo(paymentMethod));
		return methodVos;
	}

	/**
	 * method to create payment method vo
	 * 
	 * @param method
	 * @return
	 */
	private PaymentMethodVo createPaymentMethodVo(PaymentMethod method) {
		PaymentMethodVo vo = new PaymentMethodVo();
		vo.setId(method.getId());
		vo.setName(method.getMethod());
		vo.setCompanyId(method.getCompany().getId());
		return vo;
	}
}
