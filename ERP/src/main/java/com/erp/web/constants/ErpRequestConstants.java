package com.erp.web.constants;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface ErpRequestConstants {

	String HOME="erpHome.do";
	String SETTINGS_URL="settings.do";
	String GET_SELECTED_CURRENCY_DETAILS="getThisCurrencyDetails.do";
	String SAVE_CURRENCY="saveCurrency.do";
	String SAVE_COMPANY="saveCompany.do";
	String GET_ACTIVE_COMPANY="getActiveCompany.do";
	String LIST_ALL_COMPANIES="listAllCompanies.do";
	String DELETE_COMPANY="deleteCompany.do";
	String UPDATE_DEFAULT_COMPANY="updateDefaultCompany.do";
	
	String SAVE_TAX="saveTax.do";
	String TAX_LIST="listTaxes.do";
	String TAX_DELETE="deleteTax.do";
	
	String SAVE_CHARGE="saveCharge.do";
	String CHARGE_LIST="listCharge.do";
	String CHARGE_DELETE="deleteCharge.do";
	
	String PAYMENT_METHOD_SAVE="savePaymentMethod.do";
	String PAYMENT_METHOD_DELETE="deletePaymentMethod.do";
	String PAYMENT_METHOD_LIST="listPaymentMethod.do";
	
	String PAYMENT_TERM_ADD="addPaymentTerm.do";
	String PAYMENT_TERM_ADD_ROW="addPaymentTermRow.do";
	String PAYMENT_TERMS_SAVE="savePaymentTerms.do";
	String PAYMENT_TERM_LIST="paymentTermsList.do";
	String PAYMENT_TERM_DELETE="deletePaymentTerms.do";
	String PAYMENT_TERM_ROW_COUNT="getPaymentTermsRowCount.do";
	
	String NUMBER_PROPERTY_GET="getNumberProperty.do";
	String NUMBER_PROPERTY_SAVE="saveNumberProperty.do";
	
	String DELIVERY_TERMS_LIST="listDeliveryTerms.do";
	String DELIVERY_TERMS_SAVE="saveDeliveryterms.do";
	String DELIVERY_TERM_DELETE="deleteDeliveryTerm.do";
	
	
}
