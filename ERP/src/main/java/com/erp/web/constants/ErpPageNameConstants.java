package com.erp.web.constants;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface ErpPageNameConstants {

	String HOME_PAGE="erpHome";
	String SETTINGS_PAGE="settings";
	String COMPANY_LIST="companyList";
	
	String TAX_LIST="taxList";
	String CHARGE_LIST="chargeList";
	
	String PAYMENT_TERM_ADD="addPaymentTerm";
	String PAYMENT_TERM_ADD_ROW="addPaymentTermRow";
	String PAYMENT_TERM_LIST="paymentTermsList";
	
	String DELIVERY_TERMS_LIST="deliveryTermsList";
}
