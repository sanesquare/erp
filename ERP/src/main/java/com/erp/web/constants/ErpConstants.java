package com.erp.web.constants;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface ErpConstants {
	String ACTIVE_STATUS="Active";
	String EXPIRED_STATUS="Expired";
	
	String LOGO_PATH="/WEB-INF/jrxml/Accounts/2mLogo.jpg";
}
