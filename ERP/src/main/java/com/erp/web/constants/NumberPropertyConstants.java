package com.erp.web.constants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Sep 30, 2015
 */
public interface NumberPropertyConstants {

	String JOURNAL = "JOURNAL";
	String PAYMENT = "PAYMENT";
	String RECEIPT = "RECEIPT";
	String CONTRA = "CONTRA";
	String EMPLOYEE = "EMPLOYEECODE";
	String LEADS = "LEAD";
	String CUSTOMER = "CUSTOMER";
	String VENDOR = "VENDOR";
	String BILL = "BILL";
	String VENDOR_QUOTE = "VENDOR_QUOTE";
	String PURCHASE_ORDER = "PURCHASE_ORDER";
	String PURCHASE_RETURN = "PURCHASE_RETURN";
	String VENDOR_CREDIT_MEMO = "VENDOR_CREDIT_MEMO";
	String BILL_PAYMENT = "BILL_PAYMENT";
	String CUSTOMER_QUOTE = "CUSTOMER_QUOTE";
	String SALES_ORDER = "SALES_ORDER";
	String INVOICE = "INVOICE";
	String CUSTOMER_CREDIT_MEMO = "CUSTOMER_CREDIT_MEMO";
	String CUSTOMER_PAYMENT = "CUSTOMER_PAYMENT";
	String SALES_RETURN = "SALES_RETURN";
	String OPPORTUNITIES = "OPPORTUNITIES";
	String LEADS_QUOTE = "LEADS_QUOTE";
	String PACKING_LIST = "PACKING_LIST";
	String DELIVERY_NOTES = "DELIVERY_NOTES";
	String RECEIVING_NOTES = "RECEIVING_NOTES";
	String INVENTORY_ENTRY="INVENTORY ENTRY";
	String INVENTORY_WITHDRAWAL="INVENTORY WITHDRAWAL";
}
