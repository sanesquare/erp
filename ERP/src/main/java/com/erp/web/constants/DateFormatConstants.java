package com.erp.web.constants;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 17-September-2015
 *
 */
public interface DateFormatConstants {

	String MMM_DD_YYYY="MMMM dd,yyy";
}
