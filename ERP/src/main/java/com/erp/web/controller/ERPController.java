package com.erp.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.erp.web.constants.ErpPageNameConstants;
import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.entities.Company;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 29-July-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles", "companyId", "currencyName", "startDate",
		"currencyId", "endDate", "companyName", "branchIds", "companies","autoApprove" })
public class ERPController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private UserService userService;

	@Autowired
	private CompanyService companyService;

	@Log
	private static Logger logger;

	/**
	 * method to reach erp home page
	 * 
	 * @param model
	 * @param jsession
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.HOME, method = RequestMethod.GET)
	public String erpHome(final Model model, @RequestParam(value = "jsession", defaultValue = "SA") String jsession,
			HttpServletRequest request) {
		String errorMesage = request.getParameter(MessageConstants.ERROR);
		model.addAttribute(MessageConstants.ERROR, errorMesage);
		String companyName = "No Active Company!!";
		List<Long> branchIds = new ArrayList<Long>();
		List<CompanyVo> companyVos = new ArrayList<CompanyVo>();
		try {
			Boolean autoApprove = false;
			String currencyName = "-NIL-";
			Long currencyId = 0L;
			String startDate = DateFormatter.convertDateToString(new Date());
			String endDate = DateFormatter.convertDateToString(new Date());
			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			com.hrms.web.entities.User erpUser = userService.findUserByUsername(user.getUsername());
			Employee employee = erpUser.getEmployee();
			Long companyId = 0L;
			if (employee != null) {
				if (employee.getIsAutoApprove() != null)
					autoApprove = employee.getIsAutoApprove();
				EmployeeVo employeeVo = employeeService.getEmployeesDetails(employee.getId());
				model.addAttribute("authEmployeeDetails", employeeVo);
				model.addAttribute("thisUser", user.getUsername());
				model.addAttribute("authEmpCode", employeeVo.getEmployeeCode());
				//model.addAttribute("roles", employeeService.getEmployeeRoles(employee.getId()));
				SessionRolesVo sessionRolesVo = employeeService.getEmployeeRoles(employee.getId());
				model.addAttribute("roles", sessionRolesVo);
				model.addAttribute("rolesMap", sessionRolesVo);
				if (employee.getIsAdmin()) {
					for (Branch branch : employee.getBranches())
						branchIds.add(branch.getId());
					Company company = erpUser.getCompany();
					if (company != null) {
						companyId = company.getId();
						if (company.getName() != null)
							companyName = company.getName();
						startDate = DateFormatter.convertDateToString(company.getPeriodStart());
						endDate = DateFormatter.convertDateToString(company.getPeriodEnd());
						ErPCurrency currency = company.getCurrency();
						if (currency != null) {
							currencyName = currency.getBaseCurrency().getBaseCurrency();
							currencyId = currency.getId();
						}
					}

				} else {
					CompanyVo company = companyService.findActiveCompanyByParent(employee.getBranch().getId());
					companyId = company.getCompanyId();
					if (company.getCompanyName() != null)
						companyName = company.getCompanyName();
				}
				if (companyId != 0)
					companyVos = companyService.findChildCompanies(companyId);
			} else {
				model.addAttribute("authEmployeeDetails", new EmployeeVo());
			}
			model.addAttribute("companyId", companyId);
			model.addAttribute("companyName", companyName);
			model.addAttribute("companies", companyVos);
			model.addAttribute("branchIds", branchIds);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("startDate", startDate);
			model.addAttribute("endDate", endDate);
			model.addAttribute("currencyName", currencyName);
			model.addAttribute("authEmployee", new Employee());
			model.addAttribute("autoApprove", autoApprove);
		} catch (Exception e) {
			logger.info("HOME: ", e);
		}
		return ErpPageNameConstants.HOME_PAGE;
	}
}
