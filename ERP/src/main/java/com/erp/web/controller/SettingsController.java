package com.erp.web.controller;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import com.erp.web.constants.ErpPageNameConstants;
import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.service.ChargeService;
import com.erp.web.service.CompanyService;
import com.erp.web.service.DeliveryMethodService;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.service.NumberPropertyService;
import com.erp.web.service.PaymentMethodService;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.service.VatService;
import com.erp.web.vo.ChargeVo;
import com.erp.web.vo.CompanyVo;
import com.erp.web.vo.DeliveryMethodVo;
import com.erp.web.vo.ErpCurrencyVo;
import com.erp.web.vo.NumberPropertyVo;
import com.erp.web.vo.PaymentMethodVo;
import com.erp.web.vo.PaymentTermInstallmentVo;
import com.erp.web.vo.PaymentTermsVo;
import com.erp.web.vo.VatVo;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BaseCurrencyService;
import com.hrms.web.service.BranchService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.EmployeeVo;
import com.purchase.web.service.StockGroupService;

/**
 * 
 * @author Shamsheer & Vinutha 3-August-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles", "companyId", "branchIds", "companyName" })
public class SettingsController {

	@Autowired
	private BranchService branchService;

	@Autowired
	private BaseCurrencyService baseCurrencyService;

	@Autowired
	private ErpCurrencyService currencyService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private VatService vatService;

	@Autowired
	private ChargeService chargeService;

	@Autowired
	private PaymentMethodService paymentMethodService;

	@Autowired
	private StockGroupService stockGroupService;

	@Log
	private static Logger logger;

	private static Long paymentTermRowId = 1L;

	@Autowired
	private PaymentTermsService paymentTermsService;

	@Autowired
	private NumberPropertyService numberPropertyService;

	@Autowired
	private DeliveryMethodService deliveryMethodService;

	/**
	 * method to reach setting page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.SETTINGS_URL, method = RequestMethod.GET)
	public String getSettingsRequesetHandler(final Model model,
			@ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("branchIds") List<Long> branchIds) {
		try {
			paymentTermRowId = 1L;
			model.addAttribute("branches", branchService.findBranchesById(branchIds));
			model.addAttribute("currencies", baseCurrencyService.findAllBaseCurrency());
			model.addAttribute("companies", companyService.findAllCompaniesOfBranches(branchIds, companyId));
			if (companyId != 0) {
				model.addAttribute("taxes", vatService.listAllTaxesForCompany(companyId));
				model.addAttribute("charges", chargeService.findAllCharges(companyId));
				model.addAttribute("paymentTerms", paymentTermsService.findAllPaymentTerms(companyId));
				model.addAttribute("paymentMethods", paymentMethodService.findAllPaymentMethods(companyId));
				model.addAttribute("stockGroups", stockGroupService.listStockGroupsForCompany(companyId));
				model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
			}
		} catch (Exception e) {
			logger.info("SETTINGS_URL: ", e);
		}
		if (employeeVo.getIsAdmin())
			return ErpPageNameConstants.SETTINGS_PAGE;
		else
			return "redirect:" + ErpRequestConstants.HOME;
	}

	/**
	 * method to get selected currency's details
	 * 
	 * @param id
	 */
	@RequestMapping(value = ErpRequestConstants.GET_SELECTED_CURRENCY_DETAILS, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getCurrencyDetails(
			@RequestParam(value = "baseCurrencyId", required = true) Long id) {
		JsonResponse jsonResponse = new JsonResponse();
		ErpCurrencyVo vo = new ErpCurrencyVo();
		try {
			vo = currencyService.findRecentCurrencyVoByBaseCurrency(id);
		} catch (Exception e) {
			vo.setRate(new BigDecimal(1));
			vo.setDate(DateFormatter.convertDateToString(new Date()));
		}
		jsonResponse.setResult(vo);
		return jsonResponse;
	}

	/**
	 * method to save currency
	 * 
	 * @param currencyVo
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.SAVE_CURRENCY, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveCurrency(@RequestBody ErpCurrencyVo currencyVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			currencyService.saveOrUpdateCurrency(currencyVo);
		} catch (Exception e) {
			logger.info("SAVE_CURRENCY: ", e);
		}
		return jsonResponse;
	}

	/**
	 * method to save company
	 * 
	 * @param companyVo
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.SAVE_COMPANY, method = RequestMethod.POST)
	public String saveCompany(@ModelAttribute CompanyVo companyVo) {
		// ModelAndView modelAndView = new ModelAndView(new
		// RedirectView(ErpRequestConstants.LIST_ALL_COMPANIES));
		try {
			companyService.saveCompany(companyVo);
		} catch (Exception e) {
			logger.info("SAVE_COMPANY: ", e);
		}
		return "redirect:" + ErpRequestConstants.SETTINGS_URL;
	}

	/**
	 * method to get active company details
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.GET_ACTIVE_COMPANY, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getComapnyDetails(@RequestParam(value = "parentId", required = true) Long id) {
		JsonResponse jsonResponse = new JsonResponse();
		CompanyVo vo = new CompanyVo();
		try {
			vo = companyService.findActiveCompanyByParent(id);
		} catch (Exception e) {
			vo.setCompanyId(null);
			logger.info("GET_ACTIVE_COMPANY: ", e);
		}
		jsonResponse.setResult(vo);
		return jsonResponse;
	}

	/**
	 * method to list all companies
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.LIST_ALL_COMPANIES, method = RequestMethod.GET)
	public String listAllCompanies(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("branchIds") List<Long> branchIds) {
		try {
			model.addAttribute("companies", companyService.findAllCompaniesOfBranches(branchIds, companyId));
		} catch (Exception e) {
			logger.info("LIST_ALL_COMPANIES: ", e);
		}
		return ErpPageNameConstants.COMPANY_LIST;
	}

	/**
	 * method to delete company
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.DELETE_COMPANY, method = RequestMethod.GET)
	public @ResponseBody JsonResponse deleteCompany(final Model model,
			@RequestParam(value = "id", required = true) Long id, @ModelAttribute("companyId") Long companyId) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			companyService.deleteCompany(id);
			if (id == companyId)
				model.addAttribute("companyId", 0L);
			jsonResponse.setStatus("Deleted Successfully.");
		} catch (Exception e) {
			logger.info("DELETE_COMPANY: ", e);
			jsonResponse.setStatus("Company Delete Failed");
		}
		return jsonResponse;
	}

	/**
	 * method to update default company of user
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.UPDATE_DEFAULT_COMPANY, method = RequestMethod.GET)
	public @ResponseBody JsonResponse updateDefaultCompany(final Model model,
			@RequestParam(value = "companyId", required = true) Long id,
			@ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			Boolean flag = companyService.updateDefaultCompanyFOrUser(employeeVo.getUserName(), id);
			CompanyVo companyVo = companyService.findCompanyById(id);
			if (flag) {
				model.addAttribute("companyId", id);
				model.addAttribute("companyName", companyVo.getCompanyName());
			}
		} catch (Exception e) {
			logger.info("UPDATE_DEFAULT_COMPANY: ", e);
			jsonResponse.setStatus(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save tax
	 * 
	 * @param vatVo
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.SAVE_TAX, method = RequestMethod.POST)
	public @ResponseBody String saveTax(@RequestBody VatVo vatVo, @ModelAttribute("companyId") Long companyId) {
		try {
			vatVo.setCompanyId(companyId);
			vatService.saveOrUpdateTax(vatVo);
		} catch (Exception ex) {
			logger.info("SAVE_TAX: ", ex);
			return ex.getMessage();
		}
		return "Tax Saved Successfuly";
	}

	/**
	 * method to return tac list
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.TAX_LIST, method = RequestMethod.GET)
	public String listTaxes(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("taxes", vatService.listAllTaxesForCompany(companyId));
		} catch (Exception ex) {
			logger.info("TAX_LIST: ", ex);
			return ex.getMessage();
		}
		return ErpPageNameConstants.TAX_LIST;
	}

	/**
	 * method to delete tax
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.TAX_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deleteTax(@RequestParam(value = "id", required = true) Long id) {
		try {
			vatService.deleteTax(id);
		} catch (Exception ex) {
			logger.info("TAX_DELETE: ", ex);
			return ex.getMessage();
		}
		return "Tax Deleted Successfuly";
	}

	/**
	 * method to save charge
	 * 
	 * @param chargeVo
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.SAVE_CHARGE, method = RequestMethod.POST)
	public @ResponseBody String saveCharge(@RequestBody ChargeVo chargeVo,
			@ModelAttribute("companyId") Long companyId) {
		try {
			chargeVo.setCompanyId(companyId);
			chargeService.saveCharge(chargeVo);
		} catch (Exception ex) {
			logger.info("SAVE_CHARGE: ", ex);
			return ex.getMessage();
		}
		return "Charge Saved Successfuly";
	}

	/**
	 * method to return charge list
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.CHARGE_LIST, method = RequestMethod.GET)
	public String listCharges(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("charges", chargeService.findAllCharges(companyId));
		} catch (Exception ex) {
			logger.info("CHARGE_LIST: ", ex);
			return ex.getMessage();
		}
		return ErpPageNameConstants.CHARGE_LIST;
	}

	/**
	 * method to delete charge
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.CHARGE_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deleteCharge(@RequestParam(value = "id", required = true) Long id) {
		try {
			chargeService.deleteCharge(id);
		} catch (Exception ex) {
			logger.info("CHARGE_DELETE: ", ex);
		}
		return "Charge Deleted Successfuly";
	}

	/**
	 * method to save payment method
	 * 
	 * @param model
	 * @param companyId
	 * @param vo
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.PAYMENT_METHOD_SAVE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse savePaymentMethod(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestBody PaymentMethodVo vo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			vo.setCompanyId(companyId);
			paymentMethodService.savePaymentMethod(vo);
			jsonResponse.setResult(paymentMethodService.findAllPaymentMethods(companyId));
			jsonResponse.setStatus("Payment Method Saved Successfully.");
		} catch (Exception e) {
			logger.info("PAYMENT_METHOD_SAVE: ", e);
			jsonResponse.setStatus(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to delete payment method
	 * 
	 * @param model
	 * @param companyId
	 * @param id
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.PAYMENT_METHOD_DELETE, method = RequestMethod.GET)
	public @ResponseBody JsonResponse deletePaymentMethod(final Model model,
			@ModelAttribute("companyId") Long companyId, @RequestParam(value = "id", required = true) Long id) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			paymentMethodService.deletePaymentMethod(id);
			jsonResponse.setStatus("Payment Method Deleted Successfully.");
		} catch (Exception e) {
			logger.info("PAYMENT_METHOD_DELETE: ", e);
			jsonResponse.setStatus(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to list all payment methods
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.PAYMENT_METHOD_LIST, method = RequestMethod.GET)
	public @ResponseBody JsonResponse listPaymentMethod(final Model model,
			@ModelAttribute("companyId") Long companyId) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(paymentMethodService.findAllPaymentMethods(companyId));
		} catch (Exception e) {
			logger.info("PAYMENT_METHOD_LIST: ", e);
			jsonResponse.setStatus(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to add or edit payment term
	 * 
	 * @param model
	 * @param id
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.PAYMENT_TERM_ADD, method = RequestMethod.GET)
	public String addPaymentTerm(final Model model, @RequestParam(value = "id", required = true) Long id,
			@ModelAttribute("companyId") Long companyId) {
		try {
			PaymentTermsVo paymentTermsVo = new PaymentTermsVo();
			if (id > 0) {
				paymentTermsVo = paymentTermsService.findPaymentTermById(id);
				paymentTermRowId = (long) (paymentTermsVo.getInstallmentVos().size() + 1);
			} else
				paymentTermsVo.getInstallmentVos().add(new PaymentTermInstallmentVo());
			model.addAttribute("paymentTermsVo", paymentTermsVo);
			model.addAttribute("rowId", paymentTermRowId);
			paymentTermRowId++;
			model.addAttribute("paymentMethods", paymentMethodService.findAllPaymentMethods(companyId));
		} catch (Exception e) {
			logger.info("PAYMENT_TERM_ADD: ", e);
		}
		return ErpPageNameConstants.PAYMENT_TERM_ADD;
	}

	/**
	 * method to add payment term row
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.PAYMENT_TERM_ADD_ROW, method = RequestMethod.GET)
	public String addPaymentTermRow(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("rowId", paymentTermRowId);
			paymentTermRowId++;
			model.addAttribute("paymentMethods", paymentMethodService.findAllPaymentMethods(companyId));
		} catch (Exception e) {
			logger.info("PAYMENT_TERM_ADD_ROW: ", e);
		}
		return ErpPageNameConstants.PAYMENT_TERM_ADD_ROW;
	}

	/**
	 * method to save payment terms
	 * 
	 * @param model
	 * @param companyId
	 * @param paymentTermsVo
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.PAYMENT_TERMS_SAVE, method = RequestMethod.POST)
	public @ResponseBody String savePaymentTermRow(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestBody PaymentTermsVo paymentTermsVo) {
		try {
			paymentTermsVo.setCompanyId(companyId);
			paymentTermsService.savePaymentTerms(paymentTermsVo);
		} catch (Exception e) {
			logger.info("PAYMENT_TERMS_SAVE: ", e);
			return e.getMessage();
		}
		return "Payment Term Saved Successfully";
	}

	/**
	 * method to list all payment terms
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.PAYMENT_TERM_LIST, method = RequestMethod.GET)
	public String paymentTerms(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("paymentTerms", paymentTermsService.findAllPaymentTerms(companyId));
		} catch (Exception e) {
			logger.info("PAYMENT_TERM_LIST: ", e);
			return e.getMessage();
		}
		return ErpPageNameConstants.PAYMENT_TERM_LIST;
	}

	/**
	 * method to delete payment term
	 * 
	 * @param model
	 * @param companyId
	 * @param id
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.PAYMENT_TERM_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deletePaymentTerms(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "id", required = true) Long id) {
		try {
			paymentTermsService.deletePaymentTerms(id);
		} catch (Exception e) {
			logger.info("PAYMENT TERM DELET: ", e);
			return e.getMessage();
		}
		return "Payment Term Deleted Successfully";
	}

	@RequestMapping(value = ErpRequestConstants.PAYMENT_TERM_ROW_COUNT, method = RequestMethod.GET)
	public @ResponseBody Long getPaymentTermsRowCount() {
		return paymentTermRowId;
	}

	/**
	 * method to get number property
	 * 
	 * @param companyId
	 * @param property
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.NUMBER_PROPERTY_GET, method = RequestMethod.GET)
	public @ResponseBody NumberPropertyVo getNumberProperty(@ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "property", required = true) String property) {
		NumberPropertyVo numberPropertyVo = new NumberPropertyVo();
		try {
			numberPropertyVo = numberPropertyService.findNumberProperty(property, companyId);
		} catch (Exception r) {
			logger.info("NUMBER PROPERTY GET: ", r);
		}
		return numberPropertyVo;
	}

	/**
	 * method to save number property
	 * 
	 * @param companyId
	 * @param numberPropertyVo
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.NUMBER_PROPERTY_SAVE, method = RequestMethod.POST)
	public @ResponseBody String saveNumberProperty(@ModelAttribute("companyId") Long companyId,
			@RequestBody NumberPropertyVo numberPropertyVo) {
		try {
			numberPropertyVo.setCompanyId(companyId);
			numberPropertyService.saveNumberProperty(numberPropertyVo);
		} catch (Exception r) {
			logger.info("NUMBER PROPERTY SAVE: ", r);
			return r.getMessage();
		}
		return "Number Property Saved Successfully.";
	}

	/**
	 * Method to save delivery method
	 * 
	 * @param companyId
	 * @param deliveryMethodVo
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.DELIVERY_TERMS_SAVE, method = RequestMethod.POST)
	public @ResponseBody String saveDeliveryMethod(@ModelAttribute("companyId") Long companyId,
			@RequestBody DeliveryMethodVo deliveryMethodVo) {
		try {
			deliveryMethodVo.setCompanyId(companyId);
			deliveryMethodService.saveDeliveryMethod(deliveryMethodVo);
		} catch (Exception e) {
			logger.info("SAVE DELIVERY METHOD : ", e);
			return e.getMessage();
		}
		return "Delivery Method Saved Successfully";
	}

	/**
	 * Method to list all delivery terms
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = ErpRequestConstants.DELIVERY_TERMS_LIST, method = RequestMethod.GET)
	public String listAllDeliveryMethods(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
		} catch (Exception e) {
			logger.info("DELIVERY_TERMS_LIST : ", e);
		}
		return ErpPageNameConstants.DELIVERY_TERMS_LIST;
	}

	/**
	 * Method to delete delivery term
	 * 
	 * @param model
	 * @param companyId
	 * @param did
	 */
	@RequestMapping(value = ErpRequestConstants.DELIVERY_TERM_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deleteDeliveryMethod(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "did", required = true) Long did) {
		try {
			deliveryMethodService.deleteDeliveryMethod(did);
		} catch (Exception e) {
			logger.info("DELIVERY_TERM_DELETE : ", e);
			return e.getMessage();
		}
		return "Delivery Term Deleted Successfully";
	}
}
