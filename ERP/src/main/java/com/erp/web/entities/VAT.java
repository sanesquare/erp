package com.erp.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.accounts.web.entities.Ledger;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.DeliveryNote;
import com.inventory.web.entities.DeliveryNoteItems;
import com.inventory.web.entities.PickingListItems;
import com.inventory.web.entities.ReceivingNote;
import com.inventory.web.entities.ReceivingNoteItems;
import com.purchase.web.entities.BillItems;
import com.purchase.web.entities.PurchaseOrder;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.PurchaseOrderItems;
import com.purchase.web.entities.PurchaseReturnItems;
import com.purchase.web.entities.VendorCreditMemo;
import com.purchase.web.entities.VendorQuoteItems;
import com.purchase.web.entities.VendorQuotes;
import com.sales.web.entities.CustomerCreditMemoItems;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.InvoiceItems;
import com.sales.web.entities.InvoiceProducts;
import com.sales.web.entities.SalesOrder;
import com.sales.web.entities.SalesOrderItems;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */

@Entity
@Table(name ="vat")
public class VAT implements AuditEntity,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6845033659683899213L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name ="percentage")
	private BigDecimal percentage;
	
	@Column(name="name")
	private String name;
	
	@ManyToMany(mappedBy="vat")
	private Set<Bill> bills;
	
	@OneToMany(mappedBy="vat")
	private Set<BillItems>billItems = new HashSet<BillItems>();
	
	@ManyToMany(mappedBy="vat")
	private Set<VendorQuotes> vendorQuotes;
	
	@OneToMany(mappedBy="vat")
	private Set<VendorQuoteItems> vendorQuoteItems;
	
	@ManyToMany(mappedBy="vat")
	private Set<PurchaseOrder> purchaseOrders;
	
	@OneToMany(mappedBy="vat")
	private Set<PurchaseOrderItems> orderItems;
	
	@OneToMany(mappedBy="vat")
	private Set<VendorCreditMemo> vendorCreditMemos;
	
	@OneToMany(mappedBy="vat")
	private Set<VendorCreditMemoItems> vendorCreditMemoItems;
	
	@OneToMany(mappedBy="vat")
	private Set<VendorCreditMemoExpense> vendorCreditMemoExpenses;
	
	@ManyToMany(mappedBy="vat")
	private Set<SalesOrder> salesOrders=new HashSet<SalesOrder>();
	
	@OneToMany(mappedBy="vat")
	private Set<SalesOrderItems> salesOrderItems;
	
	@ManyToMany(mappedBy="vat")
	private Set<Invoice>invoices=new HashSet<Invoice>();
	
	@OneToMany(mappedBy="vat")
	private Set<InvoiceProducts> invoiceProducts;
	
	@OneToMany(mappedBy="vat")
	private Set<CustomerCreditMemoItems> customerCreditMemoItems;
	
	@OneToMany(mappedBy="vat")
	private Set<PickingListItems> pickingListItems;
	
	@ManyToOne
	private Company company;
	
	@OneToOne(mappedBy="vat",cascade=CascadeType.ALL)
	private Ledger ledger;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@OneToMany(mappedBy="vat",cascade=CascadeType.ALL,orphanRemoval=true)
	private Set<InvoiceItems> invoiceItems = new HashSet<InvoiceItems>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

	public Set<BillItems> getBillItems() {
		return billItems;
	}

	public void setBillItems(Set<BillItems> billItems) {
		this.billItems = billItems;
	}

	public Set<VendorQuotes> getVendorQuotes() {
		return vendorQuotes;
	}

	public void setVendorQuotes(Set<VendorQuotes> vendorQuotes) {
		this.vendorQuotes = vendorQuotes;
	}

	public Set<VendorQuoteItems> getVendorQuoteItems() {
		return vendorQuoteItems;
	}

	public void setVendorQuoteItems(Set<VendorQuoteItems> vendorQuoteItems) {
		this.vendorQuoteItems = vendorQuoteItems;
	}

	public Set<PurchaseOrder> getPurchaseOrders() {
		return purchaseOrders;
	}

	public void setPurchaseOrders(Set<PurchaseOrder> purchaseOrders) {
		this.purchaseOrders = purchaseOrders;
	}

	public Set<PurchaseOrderItems> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(Set<PurchaseOrderItems> orderItems) {
		this.orderItems = orderItems;
	}

	public Set<VendorCreditMemo> getVendorCreditMemos() {
		return vendorCreditMemos;
	}

	public void setVendorCreditMemos(Set<VendorCreditMemo> vendorCreditMemos) {
		this.vendorCreditMemos = vendorCreditMemos;
	}

	public Set<VendorCreditMemoItems> getVendorCreditMemoItems() {
		return vendorCreditMemoItems;
	}

	public void setVendorCreditMemoItems(Set<VendorCreditMemoItems> vendorCreditMemoItems) {
		this.vendorCreditMemoItems = vendorCreditMemoItems;
	}

	public Set<VendorCreditMemoExpense> getVendorCreditMemoExpenses() {
		return vendorCreditMemoExpenses;
	}

	public void setVendorCreditMemoExpenses(Set<VendorCreditMemoExpense> vendorCreditMemoExpenses) {
		this.vendorCreditMemoExpenses = vendorCreditMemoExpenses;
	}

	public Set<SalesOrder> getSalesOrders() {
		return salesOrders;
	}

	public void setSalesOrders(Set<SalesOrder> salesOrders) {
		this.salesOrders = salesOrders;
	}

	public Set<SalesOrderItems> getSalesOrderItems() {
		return salesOrderItems;
	}

	public void setSalesOrderItems(Set<SalesOrderItems> salesOrderItems) {
		this.salesOrderItems = salesOrderItems;
	}

	public Set<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(Set<Invoice> invoices) {
		this.invoices = invoices;
	}

	public Set<InvoiceProducts> getInvoiceProducts() {
		return invoiceProducts;
	}

	public void setInvoiceProducts(Set<InvoiceProducts> invoiceProducts) {
		this.invoiceProducts = invoiceProducts;
	}

	public Set<CustomerCreditMemoItems> getCustomerCreditMemoItems() {
		return customerCreditMemoItems;
	}

	public void setCustomerCreditMemoItems(Set<CustomerCreditMemoItems> customerCreditMemoItems) {
		this.customerCreditMemoItems = customerCreditMemoItems;
	}

	public Set<PickingListItems> getPickingListItems() {
		return pickingListItems;
	}

	public void setPickingListItems(Set<PickingListItems> pickingListItems) {
		this.pickingListItems = pickingListItems;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<InvoiceItems> getInvoiceItems() {
		return invoiceItems;
	}

	public void setInvoiceItems(Set<InvoiceItems> invoiceItems) {
		this.invoiceItems = invoiceItems;
	}


}
