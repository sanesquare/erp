package com.erp.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.DeliveryNote;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.VendorQuotes;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.SalesOrder;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name="delivery_method")
public class DeliveryMethod implements AuditEntity , Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5435014813690207895L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name ="method")
	private String method;

	@OneToMany(mappedBy="deliveryMethod")
	private Set<DeliveryNote> deliveryNotes;
	
	@ManyToOne
	private Company company;
	
	@OneToMany(mappedBy = "deliveryTerm")
	private Set<VendorQuotes> vendorQuotes = new  HashSet<VendorQuotes>();
	
	@OneToMany(mappedBy = "deliveryTerm")
	private Set<SalesOrder> salesOrder = new  HashSet<SalesOrder>();
	
	@OneToMany(mappedBy = "deliveryTerm")
	private Set<Bill> bill = new  HashSet<Bill>();
	
	@OneToMany(mappedBy = "deliveryTerm")
	private Set<Invoice> invoice = new  HashSet<Invoice>();	
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Set<DeliveryNote> getDeliveryNotes() {
		return deliveryNotes;
	}

	public void setDeliveryNotes(Set<DeliveryNote> deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Set<VendorQuotes> getVendorQuotes() {
		return vendorQuotes;
	}

	public void setVendorQuotes(Set<VendorQuotes> vendorQuotes) {
		this.vendorQuotes = vendorQuotes;
	}

	public Set<Bill> getBill() {
		return bill;
	}

	public void setBill(Set<Bill> bill) {
		this.bill = bill;
	}

	public Set<SalesOrder> getSalesOrder() {
		return salesOrder;
	}

	public void setSalesOrder(Set<SalesOrder> salesOrder) {
		this.salesOrder = salesOrder;
	}

	public Set<Invoice> getInvoice() {
		return invoice;
	}

	public void setInvoice(Set<Invoice> invoice) {
		this.invoice = invoice;
	}
}
