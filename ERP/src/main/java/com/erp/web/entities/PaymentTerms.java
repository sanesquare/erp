package com.erp.web.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.DeliveryNote;
import com.inventory.web.entities.ReceivingNote;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.VendorQuotes;
import com.sales.web.entities.CustomerCreditMemoPaymentTerms;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.SalesOrder;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name = "payment_terms")
public class PaymentTerms implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6266422421364585299L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "term")
	private String term;

	@OneToMany(mappedBy = "terms", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<PaymentTermsInstallments> installments = new ArrayList<PaymentTermsInstallments>();

	@OneToMany(mappedBy = "paymentTerms")
	private Set<Bill> bills;

	@OneToMany(mappedBy = "paymentTerms",cascade=CascadeType.ALL)
	private Set<SalesOrder> salesOrders=new HashSet<SalesOrder>();

	@OneToMany(mappedBy = "paymentTerms")
	private Set<Invoice> invoices;

	@OneToMany(mappedBy = "paymentTerms")
	private Set<CustomerCreditMemoPaymentTerms> customerCreditMemoPaymentTerms;

	@OneToMany(mappedBy = "paymentTerm")
	private Set<VendorQuotes> vendorQuotes = new  HashSet<VendorQuotes>();
	
	
	@ManyToOne
	private Company company;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public List<PaymentTermsInstallments> getInstallments() {
		return installments;
	}

	public void setInstallments(List<PaymentTermsInstallments> installments) {
		this.installments = installments;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

	public Set<SalesOrder> getSalesOrders() {
		return salesOrders;
	}

	public void setSalesOrders(Set<SalesOrder> salesOrders) {
		this.salesOrders = salesOrders;
	}

	public Set<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(Set<Invoice> invoices) {
		this.invoices = invoices;
	}

	public Set<CustomerCreditMemoPaymentTerms> getCustomerCreditMemoPaymentTerms() {
		return customerCreditMemoPaymentTerms;
	}

	public void setCustomerCreditMemoPaymentTerms(Set<CustomerCreditMemoPaymentTerms> customerCreditMemoPaymentTerms) {
		this.customerCreditMemoPaymentTerms = customerCreditMemoPaymentTerms;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<VendorQuotes> getVendorQuotes() {
		return vendorQuotes;
	}

	public void setVendorQuotes(Set<VendorQuotes> vendorQuotes) {
		this.vendorQuotes = vendorQuotes;
	}
}
