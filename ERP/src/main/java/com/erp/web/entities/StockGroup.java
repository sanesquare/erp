package com.erp.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.Product;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name ="stock_group")
public class StockGroup  implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8870277525501446481L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name ="group_name")
	private String groupName;
	
	@ManyToOne
	@JoinColumn(name="parent_group")
	private StockGroup parentGroup;
	
	@OneToMany(mappedBy = "parentGroup" , cascade ={CascadeType.PERSIST , CascadeType.MERGE} )
	private Set<StockGroup> subGroups = new HashSet<StockGroup>();
	
	@OneToMany(mappedBy="stockGroup")
	private Set<Product>products = new HashSet<Product>();

	@ManyToOne
	private Company company;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@OneToOne(mappedBy="stockGroup",cascade=CascadeType.ALL)
	private NumberProperty numberProperty;
	
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public StockGroup getParentGroup() {
		return parentGroup;
	}

	public void setParentGroup(StockGroup parentGroup) {
		this.parentGroup = parentGroup;
	}

	public Set<StockGroup> getSubGroups() {
		return subGroups;
	}

	public void setSubGroups(Set<StockGroup> subGroups) {
		this.subGroups = subGroups;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public NumberProperty getNumberProperty() {
		return numberProperty;
	}

	public void setNumberProperty(NumberProperty numberProperty) {
		this.numberProperty = numberProperty;
	}
	
	
}
