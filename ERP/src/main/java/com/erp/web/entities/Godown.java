package com.erp.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.DeliveryNoteItems;
import com.inventory.web.entities.InventoryCountSheet;
import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.entities.InventoryTransaction;
import com.inventory.web.entities.InventoryTransfer;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.PickingListItems;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ReceivingNoteItems;
import com.sales.web.entities.SalesOrderItems;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name = "godown")
public class Godown implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4604198218713381054L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@OneToMany(mappedBy = "receivingLocation")
	private Set<Product> receivingProducts;

	@OneToMany(mappedBy = "deliveryLocation")
	private Set<Product> deliveredProducts;

	@OneToMany(mappedBy = "godown")
	private Set<SalesOrderItems> salesOrderItems;

	@OneToMany(mappedBy = "godown")
	private Set<InventoryEntry> inventoryEntries;

	@OneToMany(mappedBy = "godown")
	private Set<InventoryWithdrawal> inventoryWithdrawals;

	@OneToMany(mappedBy = "fromGodown")
	private Set<InventoryTransfer> transfersFrom;

	@OneToMany(mappedBy = "toGodown")
	private Set<InventoryTransfer> transfersTo;

	@OneToMany(mappedBy = "godown")
	private Set<InventoryCountSheet> countSheets;
	@OneToMany(mappedBy = "godown")
	private Set<PickingListItems> pickingListItems;
	
	@OneToMany(mappedBy="godown")
	private Set<DeliveryNoteItems> deliveryNoteItems;
	
	@OneToMany(mappedBy="godown")
	private Set<ReceivingNoteItems> receivingNoteItems;
	
	@ManyToOne
	private Company company;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@OneToMany(mappedBy="godown",cascade=CascadeType.ALL,orphanRemoval=true)
	private Set<InventoryTransaction> inventoryTransactions = new HashSet<InventoryTransaction>();
	
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Product> getReceivingProducts() {
		return receivingProducts;
	}

	public void setReceivingProducts(Set<Product> receivingProducts) {
		this.receivingProducts = receivingProducts;
	}

	public Set<Product> getDeliveredProducts() {
		return deliveredProducts;
	}

	public void setDeliveredProducts(Set<Product> deliveredProducts) {
		this.deliveredProducts = deliveredProducts;
	}

	public Set<SalesOrderItems> getSalesOrderItems() {
		return salesOrderItems;
	}

	public void setSalesOrderItems(Set<SalesOrderItems> salesOrderItems) {
		this.salesOrderItems = salesOrderItems;
	}

	public Set<InventoryEntry> getInventoryEntries() {
		return inventoryEntries;
	}

	public void setInventoryEntries(Set<InventoryEntry> inventoryEntries) {
		this.inventoryEntries = inventoryEntries;
	}

	public Set<InventoryWithdrawal> getInventoryWithdrawals() {
		return inventoryWithdrawals;
	}

	public void setInventoryWithdrawals(Set<InventoryWithdrawal> inventoryWithdrawals) {
		this.inventoryWithdrawals = inventoryWithdrawals;
	}

	public Set<InventoryTransfer> getTransfersFrom() {
		return transfersFrom;
	}

	public void setTransfersFrom(Set<InventoryTransfer> transfersFrom) {
		this.transfersFrom = transfersFrom;
	}

	public Set<InventoryTransfer> getTransfersTo() {
		return transfersTo;
	}

	public void setTransfersTo(Set<InventoryTransfer> transfersTo) {
		this.transfersTo = transfersTo;
	}

	public Set<InventoryCountSheet> getCountSheets() {
		return countSheets;
	}

	public void setCountSheets(Set<InventoryCountSheet> countSheets) {
		this.countSheets = countSheets;
	}

	public Set<PickingListItems> getPickingListItems() {
		return pickingListItems;
	}

	public void setPickingListItems(Set<PickingListItems> pickingListItems) {
		this.pickingListItems = pickingListItems;
	}

	public Set<DeliveryNoteItems> getDeliveryNoteItems() {
		return deliveryNoteItems;
	}

	public void setDeliveryNoteItems(Set<DeliveryNoteItems> deliveryNoteItems) {
		this.deliveryNoteItems = deliveryNoteItems;
	}

	public Set<ReceivingNoteItems> getReceivingNoteItems() {
		return receivingNoteItems;
	}

	public void setReceivingNoteItems(Set<ReceivingNoteItems> receivingNoteItems) {
		this.receivingNoteItems = receivingNoteItems;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Set<InventoryTransaction> getInventoryTransactions() {
		return inventoryTransactions;
	}

	public void setInventoryTransactions(Set<InventoryTransaction> inventoryTransactions) {
		this.inventoryTransactions = inventoryTransactions;
	}
}
