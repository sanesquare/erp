package com.erp.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.sales.web.entities.CustomerCreditMemoPaymentTerms;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name = "payment_method")
public class PaymentMethod implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6102080207670617175L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "method")
	private String method;

	@OneToMany(mappedBy = "method")
	private Set<PaymentTermsInstallments> installments=new HashSet<PaymentTermsInstallments>();

	@OneToMany(mappedBy = "paymentMethod")
	private Set<CustomerCreditMemoPaymentTerms> customerCreditMemoPaymentTerms=new HashSet<CustomerCreditMemoPaymentTerms>();
	
	@ManyToOne
	private Company company;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Set<PaymentTermsInstallments> getInstallments() {
		return installments;
	}

	public void setInstallments(Set<PaymentTermsInstallments> installments) {
		this.installments = installments;
	}

	public Set<CustomerCreditMemoPaymentTerms> getCustomerCreditMemoPaymentTerms() {
		return customerCreditMemoPaymentTerms;
	}

	public void setCustomerCreditMemoPaymentTerms(Set<CustomerCreditMemoPaymentTerms> customerCreditMemoPaymentTerms) {
		this.customerCreditMemoPaymentTerms = customerCreditMemoPaymentTerms;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
