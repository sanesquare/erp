package com.erp.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import javax.persistence.Entity;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.ProductAdvanceDetails;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name ="unit")
public class Unit implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9093864014682982329L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name="unit_name")
	private String name;
	
	@Column(name="symbol")
	private String symbol;
	
	@Column(name="decimal_places")
	private Integer decimalPlaces;
	
	@OneToMany(mappedBy ="unit")
	private Set<PackUnit> packUnits;

	@OneToMany(mappedBy="unit")
	private Set<ProductAdvanceDetails> productAdvanceDetails;
	
	@Column(name="is_default")
	private Boolean isDefault;
	
	@ManyToOne
	private Company company;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Integer getDecimalPlaces() {
		return decimalPlaces;
	}

	public void setDecimalPlaces(Integer decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}

	public Set<PackUnit> getPackUnits() {
		return packUnits;
	}

	public void setPackUnits(Set<PackUnit> packUnits) {
		this.packUnits = packUnits;
	}

	public Set<ProductAdvanceDetails> getProductAdvanceDetails() {
		return productAdvanceDetails;
	}

	public void setProductAdvanceDetails(Set<ProductAdvanceDetails> productAdvanceDetails) {
		this.productAdvanceDetails = productAdvanceDetails;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}
