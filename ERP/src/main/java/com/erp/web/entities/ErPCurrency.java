package com.erp.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.entities.BaseCurrency;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.DeliveryNote;
import com.inventory.web.entities.ReceivingNote;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.VendorQuotes;
import com.sales.web.entities.SalesOrder;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name="erp_currency")
public class ErPCurrency implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7333768952501423268L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@ManyToOne
	private User createdBy;
	
	@ManyToOne
	private User updatedBy;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name ="name")
	private String name;
	
	@Column(name ="date")
	private Date date;
	
	@ManyToOne
	private BaseCurrency baseCurrency;
	
	@Column(name ="rate")
	private BigDecimal rate;
	
	@OneToMany(mappedBy="currency")
	private Set<Company> companies;

	@OneToMany(mappedBy="currency")
	private Set<Bill> bills=new HashSet<Bill>();
	
	@OneToMany(mappedBy="currency")
	private Set<VendorQuotes> vendorQuotes=new HashSet<VendorQuotes>();
	
	@OneToMany(mappedBy="currency")
	private Set<SalesOrder> salesOrders = new HashSet<SalesOrder>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BaseCurrency getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(BaseCurrency baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Set<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(Set<Company> companies) {
		this.companies = companies;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

	public Set<VendorQuotes> getVendorQuotes() {
		return vendorQuotes;
	}

	public void setVendorQuotes(Set<VendorQuotes> vendorQuotes) {
		this.vendorQuotes = vendorQuotes;
	}

}
