package com.erp.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.accounts.web.entities.Contra;
import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.LedgerGroup;
import com.accounts.web.entities.Payment;
import com.accounts.web.entities.Receipts;
import com.accounts.web.entities.Transactions;
import com.crm.web.entities.Lead;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.PaySalary;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.DeliveryNote;
import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.entities.InventoryTransaction;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.PackingKind;
import com.inventory.web.entities.PackingList;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ReceivingNote;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.PurchaseOrder;
import com.purchase.web.entities.PurchaseReturn;
import com.purchase.web.entities.Vendor;
import com.purchase.web.entities.VendorCreditMemo;
import com.purchase.web.entities.VendorQuotes;
import com.sales.web.entities.Customer;
import com.sales.web.entities.CustomerCreditMemo;
import com.sales.web.entities.CustomerPayment;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.SalesOrder;
import com.sales.web.entities.SalesReturn;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name = "company")
public class Company implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7811675997398359789L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "account_name")
	private String accountName;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "bank_branch")
	private String bankBranch;

	@Column(name = "address", length = 1000)
	private String address;

	@Column(name = "phone")
	private String phone;

	@Column(name = "email")
	private String email;

	@Column(name = "period_start")
	private Date periodStart;

	@Column(name = "period_end")
	private Date periodEnd;

	@Column(name = "sales_tax_number")
	private String salesTaxNumber;

	@ManyToOne
	private ErPCurrency currency;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<InventoryEntry> inventoryEntries = new HashSet<InventoryEntry>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Godown> godowns = new HashSet<Godown>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Payment> payments;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Receipts> receipts;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Journal> journals;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Contra> contras;

	@ManyToOne(fetch=FetchType.LAZY)
	private Branch parent;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<ReceivingNote> receivingNotes = new HashSet<ReceivingNote>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Bill> bills;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<PurchaseOrder> purchaseOrders;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<PurchaseReturn> purchaseReturns;
	
	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<SalesReturn> salesReturns;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<VendorQuotes> vendorQuotes;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Vendor> vendors;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<VendorCreditMemo> vendorCreditMemos;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Customer> customers;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Lead> leads;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<CustomerCreditMemo> customerCreditMemos;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Invoice> invoices;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<CustomerPayment> customerPayments;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<SalesOrder> salesOrders;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<DeliveryNote> deliveryNotes = new HashSet<DeliveryNote>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Ledger> ledgers = new HashSet<Ledger>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<LedgerGroup> ledgerGroups = new HashSet<LedgerGroup>();

	@ManyToOne
	private CompanyStatus status;

	@OneToMany(mappedBy = "company",fetch=FetchType.LAZY)
	private Set<User> users = new HashSet<User>(0);

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Transactions> transactions = new HashSet<Transactions>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<NumberProperty> numberProperties = new HashSet<NumberProperty>();

	@OneToMany(mappedBy = "company", fetch = FetchType.LAZY)
	private Set<PaySalary> paySalaries = new HashSet<PaySalary>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Unit> units = new HashSet<Unit>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<StockGroup> stockGroups = new HashSet<StockGroup>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Product> products = new HashSet<Product>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<VAT> vat = new HashSet<VAT>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Charge> charges = new HashSet<Charge>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<PaymentMethod> paymentMethods = new HashSet<PaymentMethod>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<PaymentTerms> paymentTerms = new HashSet<PaymentTerms>();
	
	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<DeliveryMethod> deliveryMethod = new HashSet<DeliveryMethod>();

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne(fetch = FetchType.LAZY)
	private User updatedBy;

	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<InventoryTransaction> inventoryTransactions = new HashSet<InventoryTransaction>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<InventoryWithdrawal> inventoryWithdrawals = new HashSet<InventoryWithdrawal>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<PackingKind> packingKinds = new HashSet<PackingKind>();
	
	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<PackingList> packingLists=new HashSet<PackingList>();
	
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Set<PaySalary> getPaySalaries() {
		return paySalaries;
	}

	public void setPaySalaries(Set<PaySalary> paySalaries) {
		this.paySalaries = paySalaries;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(Date periodStart) {
		this.periodStart = periodStart;
	}

	public Date getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodEnd(Date periodEnd) {
		this.periodEnd = periodEnd;
	}

	public String getSalesTaxNumber() {
		return salesTaxNumber;
	}

	public void setSalesTaxNumber(String salesTaxNumber) {
		this.salesTaxNumber = salesTaxNumber;
	}

	public ErPCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(ErPCurrency currency) {
		this.currency = currency;
	}

	public Set<Payment> getPayments() {
		return payments;
	}

	public void setPayments(Set<Payment> payments) {
		this.payments = payments;
	}

	public Set<Receipts> getReceipts() {
		return receipts;
	}

	public void setReceipts(Set<Receipts> receipts) {
		this.receipts = receipts;
	}

	public Set<Journal> getJournals() {
		return journals;
	}

	public void setJournals(Set<Journal> journals) {
		this.journals = journals;
	}

	public Set<Contra> getContras() {
		return contras;
	}

	public void setContras(Set<Contra> contras) {
		this.contras = contras;
	}

	public Branch getParent() {
		return parent;
	}

	public void setParent(Branch parent) {
		this.parent = parent;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

	public Set<PurchaseOrder> getPurchaseOrders() {
		return purchaseOrders;
	}

	public void setPurchaseOrders(Set<PurchaseOrder> purchaseOrders) {
		this.purchaseOrders = purchaseOrders;
	}

	public Set<PurchaseReturn> getPurchaseReturns() {
		return purchaseReturns;
	}

	public void setPurchaseReturns(Set<PurchaseReturn> purchaseReturns) {
		this.purchaseReturns = purchaseReturns;
	}

	public Set<VendorQuotes> getVendorQuotes() {
		return vendorQuotes;
	}

	public void setVendorQuotes(Set<VendorQuotes> vendorQuotes) {
		this.vendorQuotes = vendorQuotes;
	}

	public Set<Vendor> getVendors() {
		return vendors;
	}

	public void setVendors(Set<Vendor> vendors) {
		this.vendors = vendors;
	}

	public Set<VendorCreditMemo> getVendorCreditMemos() {
		return vendorCreditMemos;
	}

	public void setVendorCreditMemos(Set<VendorCreditMemo> vendorCreditMemos) {
		this.vendorCreditMemos = vendorCreditMemos;
	}

	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	public Set<CustomerCreditMemo> getCustomerCreditMemos() {
		return customerCreditMemos;
	}

	public void setCustomerCreditMemos(Set<CustomerCreditMemo> customerCreditMemos) {
		this.customerCreditMemos = customerCreditMemos;
	}

	public Set<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(Set<Invoice> invoices) {
		this.invoices = invoices;
	}

	public Set<CustomerPayment> getCustomerPayments() {
		return customerPayments;
	}

	public void setCustomerPayments(Set<CustomerPayment> customerPayments) {
		this.customerPayments = customerPayments;
	}

	public Set<SalesOrder> getSalesOrders() {
		return salesOrders;
	}

	public void setSalesOrders(Set<SalesOrder> salesOrders) {
		this.salesOrders = salesOrders;
	}

	public Set<DeliveryNote> getDeliveryNotes() {
		return deliveryNotes;
	}

	public void setDeliveryNotes(Set<DeliveryNote> deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}

	public Set<Ledger> getLedgers() {
		return ledgers;
	}

	public void setLedgers(Set<Ledger> ledgers) {
		this.ledgers = ledgers;
	}

	public CompanyStatus getStatus() {
		return status;
	}

	public void setStatus(CompanyStatus status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<LedgerGroup> getLedgerGroups() {
		return ledgerGroups;
	}

	public void setLedgerGroups(Set<LedgerGroup> ledgerGroups) {
		this.ledgerGroups = ledgerGroups;
	}

	public Set<Transactions> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transactions> transactions) {
		this.transactions = transactions;
	}

	public Set<NumberProperty> getNumberProperties() {
		return numberProperties;
	}

	public void setNumberProperties(Set<NumberProperty> numberProperties) {
		this.numberProperties = numberProperties;
	}

	public Set<Unit> getUnits() {
		return units;
	}

	public void setUnits(Set<Unit> units) {
		this.units = units;
	}

	public Set<StockGroup> getStockGroups() {
		return stockGroups;
	}

	public void setStockGroups(Set<StockGroup> stockGroups) {
		this.stockGroups = stockGroups;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	public Set<VAT> getVat() {
		return vat;
	}

	public void setVat(Set<VAT> vat) {
		this.vat = vat;
	}

	public Set<Charge> getCharges() {
		return charges;
	}

	public void setCharges(Set<Charge> charges) {
		this.charges = charges;
	}

	public Set<PaymentMethod> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(Set<PaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public Set<PaymentTerms> getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(Set<PaymentTerms> paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public Set<Lead> getLeads() {
		return leads;
	}

	public void setLeads(Set<Lead> leads) {
		this.leads = leads;
	}

	public Set<Godown> getGodowns() {
		return godowns;
	}

	public void setGodowns(Set<Godown> godowns) {
		this.godowns = godowns;
	}

	public Set<ReceivingNote> getReceivingNotes() {
		return receivingNotes;
	}

	public void setReceivingNotes(Set<ReceivingNote> receivingNotes) {
		this.receivingNotes = receivingNotes;
	}

	public Set<InventoryEntry> getInventoryEntries() {
		return inventoryEntries;
	}

	public void setInventoryEntries(Set<InventoryEntry> inventoryEntries) {
		this.inventoryEntries = inventoryEntries;
	}

	public Set<InventoryTransaction> getInventoryTransactions() {
		return inventoryTransactions;
	}

	public void setInventoryTransactions(Set<InventoryTransaction> inventoryTransactions) {
		this.inventoryTransactions = inventoryTransactions;
	}

	public Set<InventoryWithdrawal> getInventoryWithdrawals() {
		return inventoryWithdrawals;
	}

	public void setInventoryWithdrawals(Set<InventoryWithdrawal> inventoryWithdrawals) {
		this.inventoryWithdrawals = inventoryWithdrawals;
	}

	public Set<SalesReturn> getSalesReturns() {
		return salesReturns;
	}

	public void setSalesReturns(Set<SalesReturn> salesReturns) {
		this.salesReturns = salesReturns;
	}

	public Set<PackingKind> getPackingKinds() {
		return packingKinds;
	}

	public void setPackingKinds(Set<PackingKind> packingKinds) {
		this.packingKinds = packingKinds;
	}

	public Set<PackingList> getPackingLists() {
		return packingLists;
	}

	public void setPackingLists(Set<PackingList> packingLists) {
		this.packingLists = packingLists;
	}

	public Set<DeliveryMethod> getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(Set<DeliveryMethod> deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}
}
