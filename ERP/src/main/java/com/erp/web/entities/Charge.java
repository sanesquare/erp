package com.erp.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.accounts.web.entities.Ledger;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.BillCharges;
import com.purchase.web.entities.PurchaseOrderCharges;
import com.sales.web.entities.InvoiceCharges;
import com.sales.web.entities.InvoiceItems;
import com.sales.web.entities.SalesOrderCharges;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 25, 2015
 */
@Entity
@Table(name = "charge")
public class Charge implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1977656279334992231L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@OneToMany(mappedBy = "charge")
	private Set<Bill> bills;

	@ManyToOne
	private Company company;

	@OneToOne(mappedBy = "charge", cascade = CascadeType.ALL)
	private Ledger ledger;

	@OneToMany(mappedBy = "charge")
	private Set<BillCharges> billCharges = new HashSet<BillCharges>();

	@OneToMany(mappedBy = "charge")
	private Set<InvoiceCharges> invoiceCharges = new HashSet<InvoiceCharges>();

	@OneToMany(mappedBy = "charge")
	private Set<PurchaseOrderCharges> purchaseOrderCharges = new HashSet<PurchaseOrderCharges>();

	@OneToMany(mappedBy = "charge")
	private Set<SalesOrderCharges> salesOrderharges = new HashSet<SalesOrderCharges>();

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<BillCharges> getBillCharges() {
		return billCharges;
	}

	public void setBillCharges(Set<BillCharges> billCharges) {
		this.billCharges = billCharges;
	}

	public Set<InvoiceCharges> getInvoiceCharges() {
		return invoiceCharges;
	}

	public void setInvoiceCharges(Set<InvoiceCharges> invoiceCharges) {
		this.invoiceCharges = invoiceCharges;
	}

	public Set<PurchaseOrderCharges> getPurchaseOrderCharges() {
		return purchaseOrderCharges;
	}

	public void setPurchaseOrderCharges(Set<PurchaseOrderCharges> purchaseOrderCharges) {
		this.purchaseOrderCharges = purchaseOrderCharges;
	}

	public Set<SalesOrderCharges> getSalesOrderharges() {
		return salesOrderharges;
	}

	public void setSalesOrderharges(Set<SalesOrderCharges> salesOrderharges) {
		this.salesOrderharges = salesOrderharges;
	}

}
