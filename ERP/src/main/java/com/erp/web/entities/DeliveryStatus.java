package com.erp.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.purchase.web.entities.VendorCreditMemo;
import com.sales.web.entities.CustomerCreditMemo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name = "delivery_status")
public class DeliveryStatus implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1507358939986160292L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@OneToMany(mappedBy = "deliveryStatus")
	private Set<VendorCreditMemo> vendorCreditMemos;
	
	@OneToMany(mappedBy="deliveryStatus")
	private Set<CustomerCreditMemo> customerCreditMemos;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<VendorCreditMemo> getVendorCreditMemos() {
		return vendorCreditMemos;
	}

	public void setVendorCreditMemos(Set<VendorCreditMemo> vendorCreditMemos) {
		this.vendorCreditMemos = vendorCreditMemos;
	}

	public Set<CustomerCreditMemo> getCustomerCreditMemos() {
		return customerCreditMemos;
	}

	public void setCustomerCreditMemos(Set<CustomerCreditMemo> customerCreditMemos) {
		this.customerCreditMemos = customerCreditMemos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
