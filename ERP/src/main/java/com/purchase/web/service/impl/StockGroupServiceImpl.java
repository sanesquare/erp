package com.purchase.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.purchase.web.dao.StockGroupDao;
import com.purchase.web.service.StockGroupService;
import com.purchase.web.vo.StockGroupVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 23, 2015
 */
@Service
public class StockGroupServiceImpl implements StockGroupService {

	@Autowired
	private StockGroupDao dao;

	public void saveOrUpdateStockGroup(StockGroupVo groupVo) {
		dao.saveOrUpdateStockGroup(groupVo);
	}

	public void deleteStockGroup(Long id) {
		dao.deleteStockGroup(id);
	}

	public StockGroupVo findStockGroup(Long id) {		
		return dao.findStockGroup(id);
	}

	public List<StockGroupVo> listStockGroupsForCompany(Long companyId) {
		return dao.listStockGroupsForCompany(companyId);
	}
}
