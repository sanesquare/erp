package com.purchase.web.service;

import java.util.List;

import com.purchase.web.entities.Bill;
import com.purchase.web.vo.BillVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 7, 2015
 */
public interface BillService {

	/**
	 * Method to save or update bill
	 * @param billVo
	 * @return 
	 */
	public Long saveOrUpdateBill(BillVo billVo);
	
	/**
	 * Method to delete bill
	 * @param id
	 */
	public void deleteBill(Long id);
	
	/**
	 * Method to find bill by id
	 * @param id
	 * @return
	 */
	public BillVo findBill(Long id);
	
	/**
	 * Method to list bills for company
	 * @param companyId
	 * @return
	 */
	public List<BillVo> listBillsForCompany(Long companyId);
	
	/**
	 * method to list bills by vendor
	 * @param vendorId
	 * @return
	 */
	public List<BillVo> listBillsByVendor(Long vendorId);
	
	/**
	 * method to pay bill installment
	 * @param installmentId
	 */
	public void payBillInstallment(Long installmentId);
}
