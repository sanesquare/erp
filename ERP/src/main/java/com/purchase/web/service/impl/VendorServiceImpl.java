package com.purchase.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.purchase.web.dao.VendorDao;
import com.purchase.web.service.VendorService;
import com.purchase.web.vo.VendorContactVo;
import com.purchase.web.vo.VendorVo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 03-August-2015
 */
@Service
public class VendorServiceImpl implements VendorService {

	@Autowired
	private VendorDao dao;
	
	public Long saveOrUpdateVendor(VendorVo vendorVo) {
		return dao.saveOrUpdateVendor(vendorVo);
	}

	public void deleteVendor(Long vendorId) {
		dao.deleteVendor(vendorId);
	}

	public void deleteVendor(String vendorCode) {
		dao.deleteVendor(vendorCode);
	}

	public VendorVo findVendorById(Long id) {
		return dao.findVendorById(id);
	}

	public VendorVo findVendorByCode(String vendorCode) {
		return dao.findVendorByCode(vendorCode);
	}

	public List<VendorVo> findAllVendors() {
		return dao.findAllVendors();
	}

	public void updateVendorContact(VendorContactVo contactVo) {
		 dao.updateVendorContact(contactVo);
	}

	public VendorContactVo findVendorContactById(Long id) {
		return dao.findVendorContactById(id);
	}

	public void deleteVendorContact(Long id) {
		dao.deleteVendorContact(id);
	}

	public String getGeneratedVendorCode() {
		return dao.generateVendorCode();
	}

	public List<VendorContactVo> findVendorContacts(Long vendorId) {
		return dao.findVendorContacts(vendorId);
	}

	public List<VendorContactVo> findVendorContacts(String vendorCode) {
		return dao.findVendorContacts(vendorCode);
	}

	public List<VendorVo> findVendorsForCompany(Long companyId) {
		return dao.findVendorsForCompany(companyId);
	}

}
