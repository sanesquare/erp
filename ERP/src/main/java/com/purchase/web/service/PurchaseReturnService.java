package com.purchase.web.service;

import java.util.List;

import com.purchase.web.vo.PurchaseReturnVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 11, 2015
 */
public interface PurchaseReturnService {

	/**
	 * method to save purchase return
	 * @param vo
	 * @return
	 */
	public Long savePurchaseReturn(PurchaseReturnVo vo);
	
	/**
	 * method to delete purchase return
	 * @param id
	 * @return
	 */
	public void deletePurchaseReturn(Long id);
	
	/**
	 * method to find purchase returns by company
	 * @param companyId
	 * @return
	 */
	public List<PurchaseReturnVo> findPurchaseReturns(Long companyId);
	
	/**
	 * method to find purchase return by id
	 * @param id
	 * @return
	 */
	public PurchaseReturnVo findPurchaseReturnById(Long id);
	
	public PurchaseReturnVo convertBillToReturn(Long billId);
}
