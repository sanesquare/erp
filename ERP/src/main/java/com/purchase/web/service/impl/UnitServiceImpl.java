package com.purchase.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.purchase.web.dao.UnitDao;
import com.purchase.web.service.UnitService;
import com.purchase.web.vo.UnitVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 22, 2015
 */
@Service
public class UnitServiceImpl implements UnitService{

	@Autowired
	private UnitDao dao;
	
	public void saveOrUpdateUnit(UnitVo unitVo) {
		dao.saveOrUpdateUnit(unitVo);		
	}

	public void deleteUnit(Long id) {
		dao.deleteUnit(id);		
	}

	public UnitVo findUnitById(Long id) {
		return dao.findUnitById(id);
	}

	public UnitVo findUnitByName(String name) {
		return dao.findUnitByName(name);
	}

	public UnitVo findUnitByNameAndSymbol(String name, String symbol) {
		return dao.findUnitByNameAndSymbol(name, symbol);
	}

	public List<UnitVo> findAllUnitsByCompany(Long companyId) {
		return dao.findAllUnitsByCompany(companyId);
	}

}
