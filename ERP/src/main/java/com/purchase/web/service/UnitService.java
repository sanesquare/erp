package com.purchase.web.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.purchase.web.vo.UnitVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 22, 2015
 */
public interface UnitService {


	/**
	 * methodt to save unit
	 * @param unitVo
	 */
	public void saveOrUpdateUnit(UnitVo unitVo);
	
	/**
	 * method to delete unit
	 * @param id
	 */
	public void deleteUnit(Long id);
	
	/**
	 * method to find unit by id
	 * @param id
	 * @return
	 */
	public UnitVo findUnitById(Long id);
	
	/**
	 * method to find unit by unit name
	 * @param name
	 * @return
	 */
	public UnitVo findUnitByName(String name);
	
	/**
	 * method to find unit by name & symbol
	 * @param name
	 * @param symbol
	 * @return
	 */
	public UnitVo findUnitByNameAndSymbol(String name,String symbol);
	
	/**
	 * method to list all units by company
	 * @param companyId
	 * @return
	 */
	public List<UnitVo> findAllUnitsByCompany(Long companyId);
	


}
