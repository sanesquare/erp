package com.purchase.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.purchase.web.dao.BillDao;
import com.purchase.web.service.BillService;
import com.purchase.web.vo.BillVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 7, 2015
 */
@Service
public class BillServiceImpl implements BillService{

	@Autowired
	private BillDao dao;

	public Long saveOrUpdateBill(BillVo billVo) {
		return dao.saveOrUpdateBill(billVo);
	}

	public void deleteBill(Long id) {
		dao.deleteBill(id);
	}

	public BillVo findBill(Long id) {
		return dao.findBill(id);
	}

	public List<BillVo> listBillsForCompany(Long companyId) {
		return dao.listBillsForCompany(companyId);
	}

	public void payBillInstallment(Long installmentId) {
		dao.updateBillInstallment(installmentId);
	}

	public List<BillVo> listBillsByVendor(Long vendorId) {
		return dao.listBillsByVendor(vendorId);
	}
}
