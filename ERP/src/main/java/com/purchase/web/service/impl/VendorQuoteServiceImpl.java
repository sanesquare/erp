package com.purchase.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.purchase.web.dao.VendorQuoteDao;
import com.purchase.web.service.VendorQuoteService;
import com.purchase.web.vo.VendorQuoteVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 17, 2015
 */
@Service
public class VendorQuoteServiceImpl  implements VendorQuoteService{

	@Autowired
	private VendorQuoteDao dao;
	
	public Long saveOrUpdateVendorQuote(VendorQuoteVo quoteVo) {
		return dao.saveOrUpdateVendorQuote(quoteVo);
	}

	public void deleteVendorQuote(Long id) {
		dao.deleteVendorQuote(id);
	}

	public VendorQuoteVo findVendorQuote(Long id) {
		return dao.findVendorQuote(id);
	}

	public List<VendorQuoteVo> listVendorQuotesForCompany(Long companyId) {
		return dao.listVendorQuotesForCompany(companyId);
	}

	public Long saveOrUpdatePurchaseOrder(VendorQuoteVo quoteVo) {
		return dao.saveOrUpdatePurchaseOrder(quoteVo);
	}

	public void updateVendorQuoteToPurchaseOrder(Long id) {
		dao.updateVendorQuoteToPurchaseOrder(id);
	}

	public List<VendorQuoteVo> listPurchaseOrdersForCompany(Long companyId) {
		return dao.listPurchaseOrdersForCompany(companyId);
	}

}
