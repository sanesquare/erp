package com.purchase.web.service;

import java.util.List;

import com.purchase.web.vo.StockGroupVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 23, 2015
 */
public interface StockGroupService {

	/**
	 * Method to save or update stock group
	 * @param groupVo
	 */
	public void saveOrUpdateStockGroup(StockGroupVo groupVo);
	
	/**
	 * Method to delete stock group
	 * @param id
	 */
	public void deleteStockGroup(Long id);

	/**
	 * Method to find stock group by id
	 * @param id
	 * @return
	 */
	public StockGroupVo findStockGroup(Long id);
	
	/**
	 * Method to list all stock group for company
	 * @param companyId
	 * @return
	 */
	public List<StockGroupVo> listStockGroupsForCompany(Long companyId);
	
}
