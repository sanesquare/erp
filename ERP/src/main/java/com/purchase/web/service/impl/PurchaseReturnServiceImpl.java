package com.purchase.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.purchase.web.dao.PurchaseReturnDao;
import com.purchase.web.service.PurchaseReturnService;
import com.purchase.web.vo.PurchaseReturnVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 11, 2015
 */
@Service
public class PurchaseReturnServiceImpl implements PurchaseReturnService{

	@Autowired
	private PurchaseReturnDao dao;
	
	public Long savePurchaseReturn(PurchaseReturnVo vo) {
		return dao.savePurchaseReturn(vo);
	}

	public void deletePurchaseReturn(Long id) {
		dao.deletePurchaseReturn(id);
	}

	public List<PurchaseReturnVo> findPurchaseReturns(Long companyId) {
		return dao.findPurchaseReturns(companyId);
	}

	public PurchaseReturnVo findPurchaseReturnById(Long id) {
		return dao.findPurchaseReturnById(id);
	}

	public PurchaseReturnVo convertBillToReturn(Long billId) {
		return dao.convertBillToReturn(billId);
	}

}
