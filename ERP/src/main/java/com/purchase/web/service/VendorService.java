package com.purchase.web.service;

import java.util.List;

import com.purchase.web.vo.VendorContactVo;
import com.purchase.web.vo.VendorVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 03-August-2015
 */
public interface VendorService {

	/**
	 * method to save or update vendor
	 * 
	 * @param vendorVo
	 * @return
	 */
	public Long saveOrUpdateVendor(VendorVo vendorVo);

	/**
	 * method to delete vendor
	 * 
	 * @param vendorId
	 */
	public void deleteVendor(Long vendorId);

	/**
	 * method to delete vendor
	 * 
	 * @param vendorCode
	 */
	public void deleteVendor(String vendorCode);

	/**
	 * method to find vendor by id
	 * 
	 * @param id
	 * @return
	 */
	public VendorVo findVendorById(Long id);

	/**
	 * method to find vendor by code
	 * 
	 * @param vendorCode
	 * @return
	 */
	public VendorVo findVendorByCode(String vendorCode);

	/**
	 * method to find all vendors
	 * 
	 * @return
	 */
	public List<VendorVo> findAllVendors();

	/**
	 * method to update vendor contact
	 * 
	 * @param contactVo
	 * @return
	 */
	public void updateVendorContact(VendorContactVo contactVo);

	/**
	 * method to find vendor contact by id
	 * 
	 * @param id
	 * @return
	 */
	public VendorContactVo findVendorContactById(Long id);

	/**
	 * method to delete vendor contact
	 * 
	 * @param id
	 */
	public void deleteVendorContact(Long id);
	
	/**
	 * method to get getnerated vendor code
	 * @return
	 */
	public String getGeneratedVendorCode();
	
	/**
	 * method to find vendor's conacts
	 * 
	 * @param vendorId
	 * @return
	 */
	public List<VendorContactVo> findVendorContacts(Long vendorId);

	/**
	 * method to find vendor's conacts
	 * 
	 * @param vendorCode
	 * @return
	 */
	public List<VendorContactVo> findVendorContacts(String vendorCode);
	/**
	 * Method to find vendors for company
	 * @param companyId
	 * @return
	 */
	public List<VendorVo> findVendorsForCompany(Long companyId);
}
