package com.purchase.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Ledger;
import com.erp.web.entities.Charge;
import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.ReceivingNote;
import com.purchase.web.entities.BillItems;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name = "bill")
public class Bill implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3381769752487512007L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Vendor vendor;

	@ManyToOne
	private VendorContacts contacts;

	@Column(name = "reference_number")
	private String referenceNumber;

	@ManyToOne
	private PaymentTerms paymentTerms;
	
	@ManyToOne
	private DeliveryMethod deliveryTerm;

	@Column(name = "date")
	private Date date;
	
	@Column(name="is_archive")
	private Boolean isArchive;

	@Column(name = "is_settled")
	private Boolean isSettled;

	@Column(name = "sub_total")
	private BigDecimal subTotal;

	@Column(name = "notes")
	private String notes;

	@Column(name = "discount")
	private BigDecimal discount;

	@Column(name = "net_total")
	private BigDecimal netTotal;

	@Column(name = "net_amount")
	private BigDecimal netAmount;

	@Column(name = "tax_total")
	private BigDecimal taxTotal;

	@Column(name = "grand_total")
	private BigDecimal grandTotal;

	@Column(name = "discount_amount")
	private BigDecimal discountAmount;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "bill")
	private Journal journal;

	@ManyToOne
	private Ledger ledger;

	@ManyToOne
	private ErPCurrency currency;

	@ManyToMany
	private Set<VAT> vat = new HashSet<VAT>(0);

	@Column(name = "total")
	private BigDecimal total;

	@OneToMany(mappedBy = "bill", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<BillItems> billItems = new HashSet<BillItems>();

	@OneToMany(mappedBy = "bill", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<BillCharges> billCharges = new HashSet<BillCharges>();

	@OneToMany(mappedBy = "bill", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<BillInstallments> billInstallments = new ArrayList<BillInstallments>();

	@OneToMany(mappedBy = "bill",cascade=CascadeType.ALL,orphanRemoval=true)
	private Set<PurchaseReturn> purchaseReturns=new HashSet<PurchaseReturn>();

	@ManyToOne
	private Company company;

	@ManyToOne
	private Charge charge;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@Column(name = "voucher_number")
	private String voucherNumber;
	
	@OneToMany(cascade=CascadeType.PERSIST,orphanRemoval=false)
	private Set<ReceivingNote> receivingNote=new HashSet<ReceivingNote>();

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public VendorContacts getContacts() {
		return contacts;
	}

	public void setContacts(VendorContacts contacts) {
		this.contacts = contacts;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public PaymentTerms getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(PaymentTerms paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public Boolean getIsSettled() {
		return isSettled;
	}

	public void setIsSettled(Boolean isSettled) {
		this.isSettled = isSettled;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Set<VAT> getVat() {
		return vat;
	}

	public void setVat(Set<VAT> vat) {
		this.vat = vat;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Set<BillItems> getBillItems() {
		return billItems;
	}

	public void setBillItems(Set<BillItems> billItems) {
		this.billItems = billItems;
	}

	public Set<BillCharges> getBillCharges() {
		return billCharges;
	}

	public void setBillCharges(Set<BillCharges> billCharges) {
		this.billCharges = billCharges;
	}

	public Set<PurchaseReturn> getPurchaseReturns() {
		return purchaseReturns;
	}

	public void setPurchaseReturns(Set<PurchaseReturn> purchaseReturns) {
		this.purchaseReturns = purchaseReturns;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Charge getCharge() {
		return charge;
	}

	public void setCharge(Charge charge) {
		this.charge = charge;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<BillInstallments> getBillInstallments() {
		return billInstallments;
	}

	public void setBillInstallments(List<BillInstallments> billInstallments) {
		this.billInstallments = billInstallments;
	}

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public ErPCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(ErPCurrency currency) {
		this.currency = currency;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}
	
	public void clearItems(){
		this.billItems.clear();
	}
	
	public void clearCharges(){
		this.billCharges.clear();
	}
	
	public void clearInstallments(){
		this.billInstallments.clear();
	}

	public Boolean getIsArchive() {
		return isArchive;
	}

	public void setIsArchive(Boolean isArchive) {
		this.isArchive = isArchive;
	}

	public Set<ReceivingNote> getReceivingNote() {
		return receivingNote;
	}

	public void setReceivingNote(Set<ReceivingNote> receivingNote) {
		this.receivingNote = receivingNote;
	}

	public DeliveryMethod getDeliveryTerm() {
		return deliveryTerm;
	}

	public void setDeliveryTerm(DeliveryMethod deliveryTerm) {
		this.deliveryTerm = deliveryTerm;
	}
}
