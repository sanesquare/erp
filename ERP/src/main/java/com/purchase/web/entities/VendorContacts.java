package com.purchase.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.ReceivingNote;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name="vendor_contacts")
public class VendorContacts implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5132473224564079515L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@ManyToOne
	private Vendor vendor;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="address",length=1000)
	private String address;
	
	@Column(name ="email")
	private String email;
	
	@Column(name="phone")
	private String phone;
	
	@OneToMany(mappedBy="contacts")
	private Set<Bill> bills;
	
	@OneToMany(mappedBy="contacts")
	private Set<VendorQuotes> quotes;
	
	@OneToMany(mappedBy="contacts")
	private Set<PurchaseOrder> purchaseOrders;
	
	@OneToMany(mappedBy="vendorContacts")
	private Set<PurchaseReturn>purchaseReturns;
	
	@OneToMany(mappedBy="vendorContacts")
	private Set<VendorCreditMemo> creditMemos;
	
	@OneToMany(mappedBy="vendorContacts")
	private Set<ReceivingNote> receivingNotes;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

	public Set<VendorQuotes> getQuotes() {
		return quotes;
	}

	public void setQuotes(Set<VendorQuotes> quotes) {
		this.quotes = quotes;
	}

	public Set<PurchaseOrder> getPurchaseOrders() {
		return purchaseOrders;
	}

	public void setPurchaseOrders(Set<PurchaseOrder> purchaseOrders) {
		this.purchaseOrders = purchaseOrders;
	}

	public Set<PurchaseReturn> getPurchaseReturns() {
		return purchaseReturns;
	}

	public void setPurchaseReturns(Set<PurchaseReturn> purchaseReturns) {
		this.purchaseReturns = purchaseReturns;
	}

	public Set<VendorCreditMemo> getCreditMemos() {
		return creditMemos;
	}

	public void setCreditMemos(Set<VendorCreditMemo> creditMemos) {
		this.creditMemos = creditMemos;
	}

	public Set<ReceivingNote> getReceivingNotes() {
		return receivingNotes;
	}

	public void setReceivingNotes(Set<ReceivingNote> receivingNotes) {
		this.receivingNotes = receivingNotes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
