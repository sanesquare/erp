package com.purchase.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Receipts;
import com.erp.web.entities.Company;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name = "purchase_return")
public class PurchaseReturn implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1446175676455444147L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Vendor vendor;

	@Column(name = "return_date")
	private Date returnDate;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "shipping_date")
	private Date shippingDate;

	@Column(name = "reference_number")
	private String referenceNumber;

	@ManyToOne
	private VendorContacts vendorContacts;

	@Column(name = "shipping_address", length = 1000)
	private String shippingAddress;

	@ManyToOne
	private Bill bill;

	@OneToMany(mappedBy = "purchaseReturn", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<PurchaseReturnItems> purchaseReturnItems = new HashSet<PurchaseReturnItems>();

	@ManyToOne
	private Company company;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private Receipts receipt;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private Journal journal;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(Date shippingDate) {
		this.shippingDate = shippingDate;
	}

	public VendorContacts getVendorContacts() {
		return vendorContacts;
	}

	public void setVendorContacts(VendorContacts vendorContacts) {
		this.vendorContacts = vendorContacts;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Set<PurchaseReturnItems> getPurchaseReturnItems() {
		return purchaseReturnItems;
	}

	public void setPurchaseReturnItems(Set<PurchaseReturnItems> purchaseReturnItems) {
		this.purchaseReturnItems = purchaseReturnItems;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Receipts getReceipt() {
		return receipt;
	}

	public void setReceipt(Receipts receipt) {
		this.receipt = receipt;
	}

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}
}
