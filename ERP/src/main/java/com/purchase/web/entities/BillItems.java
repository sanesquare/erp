package com.purchase.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.entities.Product;
import com.purchase.web.entities.Bill;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-july-2015
 *
 */
@Entity
@Table(name="bill_items")
public class BillItems implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8228950079788601323L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@ManyToOne
	private Product product;
	
	@ManyToOne
	private Bill bill;
	
	@Column(name = "quantity")
	private BigDecimal quantity;
	
	@Column(name="price")
	private BigDecimal price;
	
	@Column(name="discount_percentage")
	private BigDecimal discountPercentage;
	
	@Column(name="amount")
	private BigDecimal netAmount;
	
	@Column(name="amount_excluding_tax")
	private BigDecimal amountExcludingTax;
	
	@Column(name="returned_quantity")
	private BigDecimal returnedQuantity;
	
	@ManyToOne
	private VAT vat;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@OneToOne(mappedBy="billItems",cascade=CascadeType.ALL,orphanRemoval=true)
	private InventoryEntry inventoryEntry;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(BigDecimal discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public VAT getVat() {
		return vat;
	}

	public void setVat(VAT vat) {
		this.vat = vat;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getAmountExcludingTax() {
		return amountExcludingTax;
	}

	public void setAmountExcludingTax(BigDecimal amountExcludingTax) {
		this.amountExcludingTax = amountExcludingTax;
	}

	public InventoryEntry getInventoryEntry() {
		return inventoryEntry;
	}

	public void setInventoryEntry(InventoryEntry inventoryEntry) {
		this.inventoryEntry = inventoryEntry;
	}

	public BigDecimal getReturnedQuantity() {
		return returnedQuantity;
	}

	public void setReturnedQuantity(BigDecimal returnedQuantity) {
		this.returnedQuantity = returnedQuantity;
	}
}
