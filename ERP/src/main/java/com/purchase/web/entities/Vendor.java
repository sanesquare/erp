package com.purchase.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import com.accounts.web.entities.Ledger;
import com.erp.web.entities.Company;
import com.hrms.web.entities.Country;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.ReceivingNote;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-july-2015
 *
 */
@Entity
@Table(name = "vendor")
public class Vendor implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5642655471651590669L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "is_company")
	private Boolean isCompany;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "address", length = 1000)
	private String address;

	@Column(name = "delivery_address", length = 1000)
	private String deliveryAddress;

	@ManyToOne
	private Country country;

	@Column(name = "phone")
	private String phone;

	@Column(name = "mobile")
	private String mobile;

	@Column(name = "email")
	private String Email;

	@OneToMany(mappedBy = "vendor", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<VendorContacts> contacts;

	@OneToMany(mappedBy = "vendor")
	private Set<Bill> bills;

	@OneToMany(mappedBy = "vendor")
	private Set<VendorQuotes> quotes;

	@OneToMany(mappedBy = "vendor")
	private Set<PurchaseOrder> purchaseOrders;

	@OneToMany(mappedBy = "vendor")
	private Set<PurchaseReturn> purchaseReturns;

	@OneToMany(mappedBy = "vendor")
	private Set<VendorCreditMemo> creditMemos;

	@OneToMany(mappedBy = "vendor")
	private Set<ReceivingNote> receivingNotes;

	@OneToOne(mappedBy="vendor",cascade=CascadeType.ALL)
	private Ledger ledger;
	
	@ManyToOne
	private Company company;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Boolean getIsCompany() {
		return isCompany;
	}

	public void setIsCompany(Boolean isCompany) {
		this.isCompany = isCompany;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public Set<VendorContacts> getContacts() {
		return contacts;
	}

	public void setContacts(Set<VendorContacts> contacts) {
		this.contacts = contacts;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

	public Set<VendorQuotes> getQuotes() {
		return quotes;
	}

	public void setQuotes(Set<VendorQuotes> quotes) {
		this.quotes = quotes;
	}

	public Set<PurchaseOrder> getPurchaseOrders() {
		return purchaseOrders;
	}

	public void setPurchaseOrders(Set<PurchaseOrder> purchaseOrders) {
		this.purchaseOrders = purchaseOrders;
	}

	public Set<PurchaseReturn> getPurchaseReturns() {
		return purchaseReturns;
	}

	public void setPurchaseReturns(Set<PurchaseReturn> purchaseReturns) {
		this.purchaseReturns = purchaseReturns;
	}

	public Set<VendorCreditMemo> getCreditMemos() {
		return creditMemos;
	}

	public void setCreditMemos(Set<VendorCreditMemo> creditMemos) {
		this.creditMemos = creditMemos;
	}

	public Set<ReceivingNote> getReceivingNotes() {
		return receivingNotes;
	}

	public void setReceivingNotes(Set<ReceivingNote> receivingNotes) {
		this.receivingNotes = receivingNotes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	
}
