package com.purchase.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.ReceivingNote;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name = "vendor_quotes")
public class VendorQuotes implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4499806053944968904L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Vendor vendor;

	@Column(name = "date")
	private Date date;

	@Column(name = "address", length = 1000)
	private String address;

	@ManyToOne
	private VendorContacts contacts;

	@Column(name = "expiration_date")
	private Date expirationDate;

	@Column(name = "sub_total")
	private BigDecimal subTotal;

	@Column(name = "discount_rate")
	private BigDecimal discountRate;

	@Column(name = "discount_amount")
	private BigDecimal discountAmount;

	@ManyToMany
	private Set<VAT> vat = new HashSet<VAT>(0);

	@Column(name = "total")
	private BigDecimal total;

	@OneToMany(mappedBy = "vendorQuote", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<VendorQuoteItems> items = new HashSet<VendorQuoteItems>();

	@OneToOne(mappedBy = "vendorQuote")
	private VendorQuoteAdvanced vendorQuoteAdvanced;

	@OneToMany(mappedBy = "purchaseOrder", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<PurchaseOrderCharges> charges = new LinkedHashSet<PurchaseOrderCharges>();

	@ManyToOne
	private Company company;

	@ManyToOne
	private PaymentTerms paymentTerm;
	
	@ManyToOne
	private DeliveryMethod deliveryTerm;

	@Column(name = "net_total")
	private BigDecimal netTotal;

	@Column(name = "net_amount")
	private BigDecimal netAmount;

	@Column(name = "tax_total")
	private BigDecimal taxTotal;

	@ManyToOne
	private ErPCurrency currency;

	@Column(name = "is_quote")
	private Boolean isQuote;
	
	@Column(name = "has_received")
	private Boolean hasReceived;

	@Column(name = "quote_code")
	private String quoteCode;

	@Column(name = "order_code")
	private String orderCode;

	@Column(name = "note_code")
	private String noteCode;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@OneToMany(mappedBy = "purchaseOrder", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<ReceivingNote> receivingNote = new HashSet<ReceivingNote>();

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public VendorContacts getContacts() {
		return contacts;
	}

	public void setContacts(VendorContacts contacts) {
		this.contacts = contacts;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Set<VendorQuoteItems> getItems() {
		return items;
	}

	public void setItems(Set<VendorQuoteItems> items) {
		this.items = items;
	}

	public VendorQuoteAdvanced getVendorQuoteAdvanced() {
		return vendorQuoteAdvanced;
	}

	public void setVendorQuoteAdvanced(VendorQuoteAdvanced vendorQuoteAdvanced) {
		this.vendorQuoteAdvanced = vendorQuoteAdvanced;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<VAT> getVat() {
		return vat;
	}

	public void setVat(Set<VAT> vat) {
		this.vat = vat;
	}

	public PaymentTerms getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(PaymentTerms paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public ErPCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(ErPCurrency currency) {
		this.currency = currency;
	}

	public Boolean getIsQuote() {
		return isQuote;
	}

	public void setIsQuote(Boolean isQuote) {
		this.isQuote = isQuote;
	}

	public String getQuoteCode() {
		return quoteCode;
	}

	public void setQuoteCode(String quoteCode) {
		this.quoteCode = quoteCode;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getNoteCode() {
		return noteCode;
	}

	public void setNoteCode(String noteCode) {
		this.noteCode = noteCode;
	}

	public Set<PurchaseOrderCharges> getCharges() {
		return charges;
	}

	public void setCharges(Set<PurchaseOrderCharges> charges) {
		this.charges = charges;
	}

	public DeliveryMethod getDeliveryTerm() {
		return deliveryTerm;
	}

	public void setDeliveryTerm(DeliveryMethod deliveryTerm) {
		this.deliveryTerm = deliveryTerm;
	}

	public Set<ReceivingNote> getReceivingNote() {
		return receivingNote;
	}

	public void setReceivingNote(Set<ReceivingNote> receivingNote) {
		this.receivingNote = receivingNote;
	}

	public Boolean getHasReceived() {
		return hasReceived;
	}

	public void setHasReceived(Boolean hasReceived) {
		this.hasReceived = hasReceived;
	}
}
