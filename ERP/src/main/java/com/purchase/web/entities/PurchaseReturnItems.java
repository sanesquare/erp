package com.purchase.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.Godown;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.Product;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name="purchase_return_items")
public class PurchaseReturnItems implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2140358305907998175L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@ManyToOne
	private PurchaseReturn purchaseReturn;
	
	@ManyToOne
	private Product product;
	
	@Column(name="description" , length=1000)
	private String description;

	@Column(name = "quantity")
	private BigDecimal quantity;
	
	@Column(name="price")
	private BigDecimal price;
	
	@Column(name="total")
	private BigDecimal total;
	
	@OneToOne(cascade = CascadeType.ALL , orphanRemoval = true)
	private InventoryWithdrawal inventoryWithdrawal;
	
	@ManyToOne
	private Godown godown;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PurchaseReturn getPurchaseReturn() {
		return purchaseReturn;
	}

	public void setPurchaseReturn(PurchaseReturn purchaseReturn) {
		this.purchaseReturn = purchaseReturn;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public InventoryWithdrawal getInventoryWithdrawal() {
		return inventoryWithdrawal;
	}

	public void setInventoryWithdrawal(InventoryWithdrawal inventoryWithdrawal) {
		this.inventoryWithdrawal = inventoryWithdrawal;
	}

	public Godown getGodown() {
		return godown;
	}

	public void setGodown(Godown godown) {
		this.godown = godown;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
