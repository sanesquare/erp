package com.purchase.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryStatus;
import com.erp.web.entities.InvoiceStatus;
import com.erp.web.entities.PurchaseTypes;
import com.erp.web.entities.VAT;
import com.erp.web.entities.VendorCreditMemoExpense;
import com.erp.web.entities.VendorCreditMemoItems;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 01-August-2015
 *
 */
@Entity
@Table(name = "vendor_credit_memo")
public class VendorCreditMemo implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8342865848584662362L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Vendor vendor;

	@Column(name = "date")
	private Date date;

	@Column(name = "description", length = 1000)
	private String description;

	@ManyToOne
	private VendorContacts vendorContacts;

	@Column(name = "billing_address", length = 1000)
	private String billingAddres;
	
	@ManyToOne
	private PurchaseTypes purchaseType;
	
	@ManyToOne
	private InvoiceStatus invoiceStatus;
	
	@ManyToOne
	private DeliveryStatus deliveryStatus;
	
	@Column(name="total")
	private BigDecimal total;
	
	@Column(name="total_discount_rate")
	private BigDecimal totalDiscountRate;
	
	@Column(name="total_discount_amount")
	private BigDecimal totalDiscountAmount;
	
	@Column(name="net_amount")
	private BigDecimal netAmount;
	
	@Column(name="total_cash_discount_rate")
	private BigDecimal totalCashDiscountRate;
	
	@Column(name="net_after_cash_discount")
	private BigDecimal netAfterCashDiscount;
	
	@Column(name="net_total_inclusive_vat")
	private BigDecimal netTotalInclusiveVat;
	
	@ManyToOne
	private VAT vat;
	
	@OneToMany(mappedBy="vendorCreditMemo")
	private Set<VendorCreditMemoItems> items; 
	
	@OneToMany(mappedBy="vendorCreditMemo")
	private Set<VendorCreditMemoExpense > creditMemoExpenses;
	
	@ManyToOne
	private Company company;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
