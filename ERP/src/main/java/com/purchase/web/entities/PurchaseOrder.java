package com.purchase.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name="purchase_order")
public class PurchaseOrder implements AuditEntity, Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1733147762749025498L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@ManyToOne
	private Vendor vendor;
	
	@ManyToOne
	private VendorContacts contacts;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="code")
	private String code;
	
	@OneToMany(mappedBy="purchaseOrder")
	private Set<PurchaseOrderItems>orderItems = new HashSet<PurchaseOrderItems>();
	
	@ManyToOne
	private Company company;
	
	@Column(name = "reference_number")
	private String referenceNumber;

	@ManyToOne
	private PaymentTerms paymentTerms;
	
	@Column(name = "sub_total")
	private BigDecimal subTotal;
	
	@Column(name = "discount")
	private BigDecimal discount;

	@Column(name = "net_total")
	private BigDecimal netTotal;

	@Column(name = "net_amount")
	private BigDecimal netAmount;

	@Column(name = "tax_total")
	private BigDecimal taxTotal;

	@Column(name = "grand_total")
	private BigDecimal grandTotal;

	@Column(name = "discount_amount")
	private BigDecimal discountAmount;
	
	@ManyToOne
	private ErPCurrency currency;

	@ManyToMany
	private Set<VAT> vat = new HashSet<VAT>(0);

	@Column(name = "total")
	private BigDecimal total;
	
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public VendorContacts getContacts() {
		return contacts;
	}

	public void setContacts(VendorContacts contacts) {
		this.contacts = contacts;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<PurchaseOrderItems> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(Set<PurchaseOrderItems> orderItems) {
		this.orderItems = orderItems;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public PaymentTerms getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(PaymentTerms paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public ErPCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(ErPCurrency currency) {
		this.currency = currency;
	}

	public Set<VAT> getVat() {
		return vat;
	}

	public void setVat(Set<VAT> vat) {
		this.vat = vat;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
