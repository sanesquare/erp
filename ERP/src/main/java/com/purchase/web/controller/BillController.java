package com.purchase.web.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.service.LedgerService;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.service.ChargeService;
import com.erp.web.service.DeliveryMethodService;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.service.VatService;
import com.erp.web.vo.ErpCurrencyVo;
import com.erp.web.vo.PaymentTermInstallmentVo;
import com.erp.web.vo.PaymentTermsVo;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.BaseCurrencyService;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.service.ProductService;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.service.BillService;
import com.purchase.web.service.VendorService;
import com.purchase.web.vo.BillVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 26, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class BillController {

	@Autowired
	private VendorService vendorService;

	@Autowired
	private ProductService productService;

	@Autowired
	private VatService taxService;

	@Autowired
	private ChargeService chargeService;

	@Autowired
	private PaymentTermsService paymentTermService;

	@Log
	private Logger logger;

	@Autowired
	private ErpCurrencyService erpCurrencyService;

	@Autowired
	private BillService billService;

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private DeliveryMethodService deliveryMethodService;
	
	@Autowired
	private static Long billProductId = 1L;

	@RequestMapping(value = PurchaseRequestConstants.BILLS_HOME, method = RequestMethod.GET)
	public String billsHomePageHandler(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("bills", billService.listBillsForCompany(companyId));
		} catch (Exception ex) {
			logger.info("BILLS_HOME: ", ex);
		}
		return PurchasePageNameConstants.BILLS_HOME;
	}

	@RequestMapping(value = PurchaseRequestConstants.ADD_BILL, method = RequestMethod.GET)
	public String addBillPageHandler(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId) {
		try {
			BillVo billVo = new BillVo();
			billVo.setIsEdit(false);
			model.addAttribute("bill", billVo);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
			model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
			model.addAttribute("accounts", ledgerService.contraLedgers(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("charges", chargeService.findAllCharges(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
			model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
		} catch (Exception ex) {
			logger.info("ADD_BILL: ", ex);
		}
		return PurchasePageNameConstants.ADD_BILL;
	}

	@RequestMapping(value = PurchaseRequestConstants.GET_CURRENCIES_FOR_BILL, method = RequestMethod.GET)
	public @ResponseBody List<ErpCurrencyVo> findCurrenciesForBill(
			@RequestParam(value = "date", required = true) String date) {
		List<ErpCurrencyVo> currencyVos = null;
		try {
			currencyVos = erpCurrencyService.findAllCurrencies(date);
		} catch (Exception e) {
			logger.info("GET_CURRENCIES_FOR_BILL : ", e);
		}
		return currencyVos;
	}

	@RequestMapping(value = PurchaseRequestConstants.PAYMENT_INSTALLMENTS, method = RequestMethod.GET)
	public String getPaymentInstallments(@ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "amount", required = true) BigDecimal amount,
			@RequestParam(value = "paymentTermId", required = true) Long id, final Model model,
			@RequestParam(value = "date", required = true) String date) {
		try {
			PaymentTermsVo paymentTermsVo = paymentTermService.findPaymentTermById(id);
			List<PaymentTermInstallmentVo> installmentVos = new ArrayList<PaymentTermInstallmentVo>();
			Date billDate = DateFormatter.convertStringToDate(date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(billDate);
			Long i = 1L;
			for (PaymentTermInstallmentVo installmentVo : paymentTermsVo.getInstallmentVos()) {
				calendar.add(Calendar.DATE, installmentVo.getNofdays());
				Date installmentdate = calendar.getTime();
				installmentVo.setAmount(amount.multiply(installmentVo.getPercentage()).divide(new BigDecimal(100))
						.setScale(2, RoundingMode.HALF_EVEN));
				installmentVo.setDate(DateFormatter.convertDateToString(installmentdate));
				installmentVo.setId(i);
				i++;
				installmentVos.add(installmentVo);
			}
			model.addAttribute("paymentTerm", installmentVos);
			model.addAttribute("amount", amount);
		} catch (Exception e) {
			logger.info("PAYMENT_INSTALLMENTS: ", e);
		}
		return PurchasePageNameConstants.PAYMENT_INSTALLMENTS;
	}

	@RequestMapping(value = PurchaseRequestConstants.BILL_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveBill(@ModelAttribute("companyId") Long companyId, @RequestBody BillVo billVo) {
		Long id = null;
		try {
			billVo.setCompanyId(companyId);
			id = billService.saveOrUpdateBill(billVo);
		} catch (Exception e) {
			logger.info("SAVE BILL: ", e);
		}
		if (id == null)
			id = 0L;
		return id;
	}

	@RequestMapping(value = PurchaseRequestConstants.VIEW_BILL, method = RequestMethod.GET)
	public String viewBill(final Model model, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request, @ModelAttribute("companyId") Long companyId) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			BillVo billVo = billService.findBill(id);
			model.addAttribute("bill", billVo);
			if (!billVo.getIsEdit()) {
				model.addAttribute("currencyId", billVo.getErpCurrencyId());
				model.addAttribute("date", billVo.getBillDate());
				model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
				model.addAttribute("accounts", ledgerService.contraLedgers(companyId));
				model.addAttribute("products", productService.findAllProducts(companyId));
				model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
				model.addAttribute("charges", chargeService.findAllCharges(companyId));
				model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
				model.addAttribute("currencies",
						erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
				model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
			}
		} catch (Exception e) {
			logger.info("VIEW BILL: ", e);
		}
		return PurchasePageNameConstants.ADD_BILL;
	}

	@RequestMapping(value = PurchaseRequestConstants.BILL_LIST, method = RequestMethod.GET)
	public String billList(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			List<BillVo> bills = billService.listBillsForCompany(companyId);
			model.addAttribute("bills", bills);
		} catch (Exception e) {
			logger.info("BILL_LIST : ", e);
		}
		return PurchasePageNameConstants.BILL_LIST;
	}

	@RequestMapping(value = PurchaseRequestConstants.DELETE_BILL, method = RequestMethod.GET)
	public @ResponseBody String deleteBill(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "id", required = true) Long id) {
		try {
			billService.deleteBill(id);
		} catch (ItemNotFoundException ex) {
			return ex.getMessage();
		} catch (Exception e) {
			logger.info("DELETE_BILL : ", e);
			return "Could Not Delete Bill";
		}
		return "Bill Deleted Successfully";
	}

	@RequestMapping(value = PurchaseRequestConstants.PAY_BILL_INSTALLMENT, method = RequestMethod.GET)
	public @ResponseBody Boolean payBillInstallment(@RequestParam(value = "id", required = true) Long installmentId) {
		try {
			billService.payBillInstallment(installmentId);
		} catch (Exception e) {
			logger.info("PAY BILL INSTALLMENT: ", e);
			return false; 
		}
		return true;
	}
}
