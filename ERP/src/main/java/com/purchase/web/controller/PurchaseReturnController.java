package com.purchase.web.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.service.LedgerService;
import com.erp.web.service.ChargeService;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.service.VatService;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.service.GodownService;
import com.inventory.web.service.ProductService;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.service.BillService;
import com.purchase.web.service.PurchaseReturnService;
import com.purchase.web.service.VendorService;
import com.purchase.web.vo.BillVo;
import com.purchase.web.vo.PurchaseReturnVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 6, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class PurchaseReturnController {

	@Log
	private Logger logger;

	@Autowired
	private BillService billService;

	@Autowired
	private VendorService vendorService;

	@Autowired
	private PurchaseReturnService purchaseReturnService;

	@RequestMapping(value = PurchaseRequestConstants.PURCHASE_RETURN_HOME, method = RequestMethod.GET)
	public String purchaseReturnHomeHandler(final Model model, @ModelAttribute("companyId") Long companyId,HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("returns", purchaseReturnService.findPurchaseReturns(companyId));
		} catch (Exception e) {
			logger.info("", e);
		}
		return PurchasePageNameConstants.PURCHASE_RETURN_HOME;
	}
	
	@RequestMapping(value = PurchaseRequestConstants.PURCHASE_RETURN_LIST, method = RequestMethod.GET)
	public String purchaseReturnList(final Model model, @ModelAttribute("companyId") Long companyId,HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("returns", purchaseReturnService.findPurchaseReturns(companyId));
		} catch (Exception e) {
			logger.info("", e);
		}
		return PurchasePageNameConstants.PURCHASE_RETURN_LIST;
	}
	

	@RequestMapping(value = PurchaseRequestConstants.PURCHASE_RETURN_ADD, method = RequestMethod.GET)
	public String addReturnHandler(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute PurchaseReturnVo purchaseReturnVo, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("showFilter", true);
			model.addAttribute("bill", new PurchaseReturnVo());
			model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
			model.addAttribute("bills", billService.listBillsForCompany(companyId));
		} catch (Exception e) {
			logger.info("", e);
		}
		return PurchasePageNameConstants.PURCHASE_RETURN_ADD;
	}

	@RequestMapping(value = PurchaseRequestConstants.RETURN_THIS_BILL, method = RequestMethod.POST)
	public String returnThisBill(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute PurchaseReturnVo purchaseReturnVo, @ModelAttribute("currencyId") Long currencyId) {
		try {
			model.addAttribute("showFilter", false);
			if (purchaseReturnVo.getBillId() != null) {
				PurchaseReturnVo billVo = purchaseReturnService.convertBillToReturn(purchaseReturnVo.getBillId());
				if (billVo.getIsReturned())
					return "redirect:" + PurchaseRequestConstants.PURCHASE_RETURN_ADD
							+ "?msg=Oops.. Selected Bill Has Nothing to Return";
				model.addAttribute("bill", billVo);
				model.addAttribute("currencyId", currencyId);
				model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
			} else {
				return "redirect:" + PurchaseRequestConstants.PURCHASE_RETURN_ADD + "?msg=Select Bill";
			}
		} catch (Exception e) {
			logger.info("", e);
		}
		return PurchasePageNameConstants.PURCHASE_RETURN_ADD;
	}

	@RequestMapping(value = PurchaseRequestConstants.RETURN_THIS_BILL, method = RequestMethod.GET)
	public String returnThisBillget(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "billId") Long billId, @ModelAttribute("currencyId") Long currencyId) {
		try {
			model.addAttribute("showFilter", false);
			PurchaseReturnVo billVo = purchaseReturnService.convertBillToReturn(billId);
			model.addAttribute("bill", billVo);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
		} catch (Exception e) {
			logger.info("", e);
		}
		return PurchasePageNameConstants.PURCHASE_RETURN_ADD;
	}

	@RequestMapping(value = PurchaseRequestConstants.PURCHASE_RETURN_VIEW, method = RequestMethod.GET)
	public String viewPurchaseReturn(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "id") Long returnId, @ModelAttribute("currencyId") Long currencyId) {
		try {
			model.addAttribute("showFilter", false);
			model.addAttribute("bill", purchaseReturnService.findPurchaseReturnById(returnId));
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
		} catch (Exception e) {
			logger.info("", e);
		}
		return PurchasePageNameConstants.PURCHASE_RETURN_ADD;
	}

	@RequestMapping(value = PurchaseRequestConstants.PURCHASE_RETURN_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long savePurchaseReturn(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestBody PurchaseReturnVo purchaseReturnVo) {
		Long id = 0L;
		try {
			id = purchaseReturnService.savePurchaseReturn(purchaseReturnVo);
		} catch (Exception e) {
			logger.info("", e);
		}
		return id;
	}

	@RequestMapping(value = PurchaseRequestConstants.BILLS_BY_VENDOR, method = RequestMethod.GET)
	public @ResponseBody JsonResponse filterBillsBYVendor(final Model model,
			@ModelAttribute("companyId") Long companyId, @RequestParam(value = "vendorId") Long vendorId) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (vendorId != null)
				jsonResponse.setResult(billService.listBillsByVendor(vendorId));
			else
				jsonResponse.setResult(billService.listBillsForCompany(companyId));
			jsonResponse.setStatus("ok");
		} catch (Exception e) {
			jsonResponse.setStatus("error");
			logger.info("", e);
		}
		return jsonResponse;
	}
	
	@RequestMapping(value=PurchaseRequestConstants.PURCHASE_RETURN_DELETE,method=RequestMethod.GET)
	public @ResponseBody ModelAndView deletePurchaseReturn(@RequestParam(value="id",required=true)Long id){
		String url=PurchaseRequestConstants.PURCHASE_RETURN_LIST;
		try{
			purchaseReturnService.deletePurchaseReturn(id);
			url=url+"?msg=Purchase Return Deleted Successfully.";
		}catch(ItemNotFoundException e){
			url=url+"?msg="+e.getMessage();
		}catch (Exception e) {
			logger.info("",e);
			url=url+"?msg=Purchase Return Delete Failed.";
		}
		return new ModelAndView(new RedirectView(url));
	}
}
