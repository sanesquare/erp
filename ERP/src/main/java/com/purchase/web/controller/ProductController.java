package com.purchase.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.service.ChargeService;
import com.erp.web.service.VatService;
import com.erp.web.vo.ChargeVo;
import com.hrms.web.logger.Log;
import com.inventory.web.service.ProductService;
import com.inventory.web.vo.ProductVo;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.service.StockGroupService;
import com.purchase.web.service.UnitService;

/**
 * 
 * @author Shamsheer & Vinutha 3-August-2015
 */
@Controller
@SessionAttributes({ "companyId", "startDate", "endDate" })
public class ProductController {

	@Autowired
	private UnitService unitService;

	@Autowired
	private StockGroupService groupService;

	@Autowired
	private ProductService productService;

	@Autowired
	private VatService vatService;

	@Autowired
	private ChargeService chargeService;

	@Log
	private Logger logger;

	/**
	 * method to reach product home page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.PRODUCTS, method = RequestMethod.GET)
	public String productsHome(final Model model, @ModelAttribute("companyId") Long companyId) {
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME;
		else {
			try {
				model.addAttribute("products", productService.findAllProducts(companyId));
			} catch (Exception e) {
				logger.info("PRODUCTS: ", e);
			}
			return PurchasePageNameConstants.PRODUCTS_PAGE;
		}
	}

	/**
	 * method to reach product add page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.ADD_PRODUCT, method = RequestMethod.GET)
	public String productsAddPage(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute ProductVo productVo, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("units", unitService.findAllUnitsByCompany(companyId));
			model.addAttribute("stockGroups", groupService.listStockGroupsForCompany(companyId));
		} catch (Exception ex) {
			logger.info("ADD_PRODUCT: ", ex);
		}

		return PurchasePageNameConstants.PRODUCTS_ADD;
	}

	/**
	 * method to save product
	 * 
	 * @param model
	 * @param companyId
	 * @param productVo
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.PRODUCT_SAVE, method = RequestMethod.POST)
	public String productSave(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute ProductVo productVo, BindingResult bindingResult) {
		Long id = null;
		try {
			productVo.setCompanyId(companyId);
			id = productService.saveOrUpdateProduct(productVo);
		} catch (Exception ex) {
			logger.info("PRODUCT_SAVE: ", ex);
		}
		if (id != null)
			return "redirect:" + PurchaseRequestConstants.PRODUCT_EDIT + "?id=" + id
					+ "&msg=Product Saved Successfully";
		else
			return "redirect:" + PurchaseRequestConstants.ADD_PRODUCT + "?msg=Product Save Failed";
	}

	/**
	 * Method to edit product
	 * 
	 * @param model
	 * @param companyId
	 * @param productVo
	 * @param id
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.PRODUCT_EDIT, method = RequestMethod.GET)
	public String productEdit(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute ProductVo productVo, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			productVo = productService.findProductById(id);
			model.addAttribute("productVo", productVo);
			model.addAttribute("units", unitService.findAllUnitsByCompany(companyId));
			model.addAttribute("stockGroups", groupService.listStockGroupsForCompany(companyId));
		} catch (Exception ex) {
			logger.info("PRODUCT_EDIT: ", ex);
		}

		return PurchasePageNameConstants.PRODUCTS_ADD;
	}

	/**
	 * Method to delete product
	 * 
	 * @param model
	 * @param companyId
	 * @param id
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.PRODUCT_DELETE, method = RequestMethod.GET)
	public @ResponseBody String productDelete(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "id", required = true) Long id) {
		try {
			productService.deleteProduct(id);
		} catch (Exception ex) {
			logger.info("PRODUCT_DELETE: ", ex);
			return "Could Not Delete Product";
		}

		return "ok";
	}

	@RequestMapping(value = PurchaseRequestConstants.PRODUCT_LIST, method = RequestMethod.GET)
	public String productList(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("products", productService.findAllProducts(companyId));
		} catch (Exception ex) {
			logger.info("PRODUCT_LIST: ", ex);
			return ex.getMessage();
		}

		return PurchasePageNameConstants.PRODUCT_LIST;
	}

	/**
	 * function to get product pop up
	 * 
	 * @param model
	 * @param companyId
	 * @param productVo
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.PRODUCT_POPUP, method = RequestMethod.POST)
	public String productPopup(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestBody ProductVo productVo) {
		try {
			List<ProductVo> productVos = productService.findAllProducts(companyId);
			List<Long> ids = productVo.getIds();
			List<String> idsAndQty = productVo.getIdsAndQuantities();
			if (idsAndQty.isEmpty()) {
				for (ProductVo vo : productVos) {
					if (ids.contains(vo.getProductId()))
						vo.setSelected(true);
				}
				model.addAttribute("products", productVos);
			} else {
				Map<Long, BigDecimal> idMap = new HashMap<Long, BigDecimal>();
				List<ProductVo> alteredProducts = new ArrayList<ProductVo>();
				for (String id : idsAndQty) {
					String[] value = id.split("!!!");
					Long val = Long.parseLong(value[0]);
					if (value.length > 1)
						idMap.put(val, new BigDecimal(value[1]));
					else
						idMap.put(val, BigDecimal.ZERO);
				}
				for (ProductVo vo : productVos) {
					if (idMap.containsKey(vo.getProductId())) {
						vo.setSelected(true);
						vo.setAvailableQty(idMap.get(vo.getProductId()));
					}
					alteredProducts.add(vo);
				}
				model.addAttribute("products", alteredProducts);
			}
			model.addAttribute("isSales", productVo.getIsSales());
		} catch (

		Exception ex)

		{
			logger.info("PRODUCT_POPUP: ", ex);
			return ex.getMessage();
		}

		return PurchasePageNameConstants.PRODUCT_POP_UP;

	}

	/**
	 * method to find product
	 * 
	 * @param model
	 * @param productVo
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.PROUCTS_FIND, method = RequestMethod.POST)
	public String findProducts(final Model model, @RequestBody ProductVo productVo,
			@ModelAttribute("companyId") Long companyId) {
		try {
			if (!productVo.getIdsAndQuantities().isEmpty()) {
				model.addAttribute("isSales", productVo.getIsSales());
				List<String> idsAndQty = productVo.getIdsAndQuantities();
				List<Long> ids = new ArrayList<Long>();
				Map<Long, BigDecimal> idMap = new HashMap<Long, BigDecimal>();
				for (String id : idsAndQty) {
					String[] value = id.split("!!!");
					Long val = Long.parseLong(value[0]);
					ids.add(val);
					if (value.length > 1)
						idMap.put(val, new BigDecimal(value[1]));
					else
						idMap.put(val, BigDecimal.ZERO);
				}
				List<ProductVo> products = productService.findProducts(ids);
				List<ProductVo> alteredProducts = new ArrayList<ProductVo>();
				for (ProductVo vo : products) {
					vo.setAvailableQty(idMap.get(vo.getProductId()));
					alteredProducts.add(vo);
				}
				model.addAttribute("products", alteredProducts);
				model.addAttribute("taxes", vatService.listAllTaxesForCompany(companyId));
			}
		} catch (Exception ex) {
			logger.info("PROUCTS_FIND: ", ex);
			return ex.getMessage();
		}

		return PurchasePageNameConstants.PRODUCT_ROWS;
	}

	@RequestMapping(value = PurchaseRequestConstants.CHARGE_POPUP, method = RequestMethod.POST)
	public String chargePopup(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestBody ChargeVo chargeVo) {
		try {
			List<ChargeVo> chargeVos = chargeService.findAllCharges(companyId);
			if (chargeVo.getIdsAndAmt().isEmpty()) {
				for (ChargeVo vo : chargeVos) {
					if (chargeVo.getIds().contains(vo.getChargeId()))
						vo.setSelected(true);
				}
				model.addAttribute("charges", chargeVos);
			} else {
				Map<Long, BigDecimal> idMap = new HashMap<Long, BigDecimal>();
				List<ChargeVo> alteredCharges = new ArrayList<ChargeVo>();
				for (String id : chargeVo.getIdsAndAmt()) {
					String[] value = id.split("!!!");
					Long val = Long.parseLong(value[0]);
					if (value.length > 1)
						idMap.put(val, new BigDecimal(value[1]));
					else
						idMap.put(val, BigDecimal.ZERO);
				}
				for (ChargeVo vo : chargeVos) {
					if (idMap.containsKey(vo.getChargeId())) {
						vo.setSelected(true);
						vo.setAmount(idMap.get(vo.getChargeId()));
					}
					alteredCharges.add(vo);
				}
				model.addAttribute("charges", alteredCharges);
			}
		} catch (Exception ex) {
			logger.info("CHARGE_POPUP: ", ex);
			return ex.getMessage();
		}

		return PurchasePageNameConstants.CHARGE_POP_UP;
	}

	@RequestMapping(value = PurchaseRequestConstants.CHARGE_FIND, method = RequestMethod.POST)
	public String findCharges(final Model model, @RequestBody ChargeVo chargeVo,
			@ModelAttribute("companyId") Long companyId) {
		try {
			Map<Long, BigDecimal> idMap = new LinkedHashMap<Long, BigDecimal>();
			List<Long> ids = new ArrayList<Long>();
			for (String idQty : chargeVo.getIdsAndAmt()) {
				String[] value = idQty.split("!!!");
				Long val = Long.parseLong(value[0]);
				ids.add(val);
				if (value.length > 1)
					idMap.put(val, new BigDecimal(value[1]));
				else
					idMap.put(val, BigDecimal.ZERO);
			}
			if (!ids.isEmpty()) {
				List<ChargeVo> chargeVos = chargeService.findCharges(ids);
				List<ChargeVo> sortedVos = new ArrayList<ChargeVo>();
				for (ChargeVo vo : chargeVos) {
					vo.setAmount(idMap.get(vo.getChargeId()));
					sortedVos.add(vo);
				}
				model.addAttribute("charges", sortedVos);
			}

		} catch (Exception ex) {
			logger.info("CHARGE_FIND: ", ex);
			return ex.getMessage();
		}

		return PurchasePageNameConstants.CHARGE_ROWS;
	}
}
