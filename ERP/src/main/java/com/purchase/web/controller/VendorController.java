package com.purchase.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.CountryService;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.service.VendorService;
import com.purchase.web.vo.VendorContactVo;
import com.purchase.web.vo.VendorVo;

/**
 * 
 * @author Shamsheer & Vinutha 3-August-2015
 */
@Controller
@SessionAttributes({ "companyId" })
public class VendorController {

	@Autowired
	private VendorService vendorService;

	@Autowired
	private CountryService countryService;
	
	@Log
	private Logger logger;

	/**
	 * method to reach vendors home page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.VENDORS, method = RequestMethod.GET)
	public String vendorsHome(final Model model, HttpServletRequest request,
			@ModelAttribute("companyId") Long companyId) {
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String errorMessage = request.getParameter(MessageConstants.ERROR);
		try {
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
			model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
		} catch (Exception e) {
			logger.info("VENDORS: ",e);
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PurchasePageNameConstants.VENDOR_PAGE;
	}

	/**
	 * method to reach vendor add page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.VENDORS_ADD, method = RequestMethod.GET)
	public String vendorsAddPage(final Model model, @ModelAttribute VendorVo vendorVo) {
		try {
			model.addAttribute("country", countryService.findAllCountry());
			// model.addAttribute("code",
			// vendorService.getGeneratedVendorCode());
		} catch (Exception e) {
			logger.info("VENDORS_ADD: ",e);
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PurchasePageNameConstants.VENDOR_ADD;
	}

	/**
	 * method to save new vendor
	 * 
	 * @param model
	 * @param vendorVo
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.VENDORS_SAVE, method = RequestMethod.POST)
	public String saveVendor(final Model model, @ModelAttribute VendorVo vendorVo, BindingResult bindingResult,
			@ModelAttribute("companyId") Long companyId) {
		Long id = null;
		try {
			vendorVo.setCompanyId(companyId);
			id = vendorService.saveOrUpdateVendor(vendorVo);
			model.addAttribute(MessageConstants.SUCCESS, "Vendor Details Saved Successfully.");
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
			logger.info("VENDORS_SAVE: ",e);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info("VENDORS_SAVE: ",e);
		}
		if (id != null)
			return "redirect:" + PurchaseRequestConstants.VENDORS_EDIT + "?id=" + id;
		else
			return PurchasePageNameConstants.VENDOR_ADD;
	}

	/**
	 * method to edit vendor
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.VENDORS_EDIT, method = RequestMethod.GET)
	public String editVendor(final Model model, @RequestParam(value = "id") Long id, HttpServletRequest request) {
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String errorMessage = request.getParameter(MessageConstants.ERROR);
		try {
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
			model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("vendorVo", vendorService.findVendorById(id));
			model.addAttribute("country", countryService.findAllCountry());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
			logger.info("VENDORS_EDIT: ",e);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info("VENDORS_EDIT: ",e);
		}
		return PurchasePageNameConstants.VENDOR_ADD;
	}

	/**
	 * method to delete vendor
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.VENDOR_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deleteVendor(@RequestParam(value = "id") Long id) {
		try {
			vendorService.deleteVendor(id);
		} catch (ItemNotFoundException e) {
			logger.info("VENDOR_DELETE: ",e);
			return e.getMessage();
		} catch (Exception e) {
			logger.info("VENDOR_DELETE: ",e);
			return MessageConstants.WRONG;
		}
		return "ok";
	}

	/**
	 * method to list vendors
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.VENDOR_LIST, method = RequestMethod.GET)
	public String vendorList(final Model model) {
		try {
			model.addAttribute("vendors", vendorService.findAllVendors());
		} catch (Exception e) {
			logger.info("VENDOR_LIST: ",e);
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PurchasePageNameConstants.VENDOR_LIST;
	}

	/**
	 * method to get vendor contacts
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.VENDOR_CONTACT_LIST, method = RequestMethod.GET)
	public String vendorContactList(final Model model, @RequestParam(value = "id", required = true) Long id) {
		try {
			model.addAttribute("contacts", vendorService.findVendorContacts(id));
		} catch (Exception e) {
			logger.info("VENDOR_CONTACT_LIST: ",e);
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PurchasePageNameConstants.VENDOR_CONTACT_LIST;
	}

	/**
	 * method to save vendor contact
	 * 
	 * @param contactVo
	 */
	@RequestMapping(value = PurchaseRequestConstants.VENDOR_CONTACT_SAVE, method = RequestMethod.POST)
	public @ResponseBody String saveVendorContactDetails(@RequestBody VendorContactVo contactVo) {
		try {
			vendorService.updateVendorContact(contactVo);
			return "ok";
		} catch (ItemNotFoundException e) {
			logger.info("VENDOR_CONTACT_SAVE: ",e);
			return e.getMessage();
		} catch (Exception e) {
			logger.info("VENDOR_CONTACT_SAVE: ",e);
			return MessageConstants.WRONG;
		}
	}

	/**
	 * method to delete vendor contact
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.VENDOR_CONTACT_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deleteVendorContactDetails(@RequestParam(value = "id", required = true) Long id) {
		try {
			vendorService.deleteVendorContact(id);
			return "ok";
		} catch (ItemNotFoundException e) {
			logger.info("VENDOR_CONTACT_DELETE: ",e);
			return e.getMessage();
		} catch (Exception e) {
			logger.info("VENDOR_CONTACT_DELETE: ",e);
			return MessageConstants.WRONG;
		}
	}

}
