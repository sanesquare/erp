package com.purchase.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.service.LedgerService;
import com.erp.web.service.ChargeService;
import com.erp.web.service.DeliveryMethodService;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.service.VatService;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.service.GodownService;
import com.inventory.web.service.ProductService;
import com.inventory.web.vo.ReceivingNoteProductVo;
import com.inventory.web.vo.ReceivingNoteVo;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.service.VendorQuoteService;
import com.purchase.web.service.VendorService;
import com.purchase.web.vo.VendorQuoteVo;
import com.purchase.web.vo.VendorquoteProductVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 29, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class PurchaseOrderController {

	@Log
	private Logger logger;

	@Autowired
	private VendorService vendorService;

	@Autowired
	private ProductService productService;

	@Autowired
	private VatService taxService;

	@Autowired
	private ChargeService chargeService;

	@Autowired
	private PaymentTermsService paymentTermService;

	@Autowired
	private ErpCurrencyService erpCurrencyService;

	@Autowired
	private GodownService godownService;

	@Autowired
	private VendorQuoteService vendorQuoteService;

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private DeliveryMethodService deliveryMethodService;

	@RequestMapping(value = PurchaseRequestConstants.PURCHASE_ORDERS_HOME, method = RequestMethod.GET)
	public String purchaseOrderHomePageHandler(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("orders", vendorQuoteService.listPurchaseOrdersForCompany(companyId));
		} catch (Exception ex) {
			logger.info("PURCHASE_ORDERS_HOME: ", ex);
		}
		return PurchasePageNameConstants.PURCHASE_ORDERS_HOME;
	}

	@RequestMapping(value = PurchaseRequestConstants.PURCHASE_ORDERS_LIST, method = RequestMethod.GET)
	public String purchaseOrderList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("orders", vendorQuoteService.listPurchaseOrdersForCompany(companyId));
		} catch (Exception ex) {
			logger.info("PURCHASE_ORDERS_LIST: ", ex);
		}
		return PurchasePageNameConstants.PURCHASE_OREDER_LIST;
	}

	@RequestMapping(value = PurchaseRequestConstants.ADD_PURCHASE_ORDER, method = RequestMethod.GET)
	public String addPurchaseOrder(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId) {
		try {
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("order", new VendorQuoteVo());
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
			model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
			model.addAttribute("accounts", ledgerService.contraLedgers(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("charges", chargeService.findAllCharges(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
			model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
		} catch (Exception ex) {

		}
		return PurchasePageNameConstants.ADD_PURCHASE_ORDER;
	}

	@RequestMapping(value = PurchaseRequestConstants.PURCHASE_ORDER_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long savePurchaseOrder(final Model model, @ModelAttribute("companyId") Long compnyId,
			@RequestBody VendorQuoteVo orderVo) {
		Long id = 0L;
		orderVo.setCompanyId(compnyId);
		try {
			id = vendorQuoteService.saveOrUpdatePurchaseOrder(orderVo);
		} catch (Exception e) {
			logger.info("PURCHASE_ORDER_SAVE: ", e);
		}
		return id;
	}

	@RequestMapping(value = PurchaseRequestConstants.PURCHASE_ORDER_VIEW, method = RequestMethod.GET)
	public String viewPurchaseOrder(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			VendorQuoteVo orderVo = vendorQuoteService.findVendorQuote(id);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", orderVo.getQuoteDate());
			model.addAttribute("order", orderVo);
			if (!orderVo.getIsView()) {
				model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
				model.addAttribute("accounts", ledgerService.contraLedgers(companyId));
				model.addAttribute("products", productService.findAllProducts(companyId));
				model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
				model.addAttribute("charges", chargeService.findAllCharges(companyId));
				model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
				model.addAttribute("currencies",
						erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
				model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
			}
		} catch (Exception ex) {
			logger.info("PURCHASE_ORDER_VIEW: ", ex);
		}
		return PurchasePageNameConstants.ADD_PURCHASE_ORDER;
	}

	@RequestMapping(value = PurchaseRequestConstants.PURCHASE_ORDER_DELETE, method = RequestMethod.GET)
	public @ResponseBody ModelAndView deletePurchaseOrder(final Model model,
			@RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = null;
		try {
			vendorQuoteService.deleteVendorQuote(id);
			modelAndView = new ModelAndView(new RedirectView(
					PurchaseRequestConstants.PURCHASE_ORDERS_LIST + "?msg=Purchase order deleted successfully"));
		} catch (Exception e) {
			modelAndView = new ModelAndView(new RedirectView(
					PurchaseRequestConstants.PURCHASE_ORDERS_LIST + "?msg=Purchase order delete failed"));
			logger.info("PURCHASE_ORDER_DELETE: ", e);
		}
		return modelAndView;
	}

	@RequestMapping(value = PurchaseRequestConstants.CONVERT_PURCHASE_ORDER_TO_RECEIVING_NOTE, method = RequestMethod.GET)
	public String viewRecivingNote(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			VendorQuoteVo orderVo = vendorQuoteService.findVendorQuote(id);
			ReceivingNoteVo noteVo = new ReceivingNoteVo();
			model.addAttribute("note", this.createNoteVo(orderVo, noteVo));
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", noteVo.getDate());
			model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
		} catch (Exception e) {
			logger.info("RECEIVING_NOTE_VIEW- ", e);
		}
		return InventoryPageNameConstants.RECEIVING_NOTES_ADD;
	}

	private ReceivingNoteVo createNoteVo(VendorQuoteVo quoteVo, ReceivingNoteVo vo) {
		vo.setCompanyId(quoteVo.getCompanyId());
		vo.setOrderCode(quoteVo.getOrderCode());
		vo.setOrderId(quoteVo.getQuoteId());
		vo.setVendor(quoteVo.getVendor());
		vo.setVendorId(quoteVo.getVendorId());
		vo.setDate(quoteVo.getQuoteDate());
		vo.setPaymentTerm(quoteVo.getPaymentTerm());
		vo.setPaymentTermId(quoteVo.getPaymentTermId());
		vo.setDeliveryTerm(quoteVo.getDeliveryTerm());
		vo.setDeliveryTermId(quoteVo.getDeliveryTermId());
		vo.setErpCurrencyId(quoteVo.getErpCurrencyId());
		vo.setCurrency(quoteVo.getCurrency());
		vo.setSubTotal(quoteVo.getSubTotal());
		vo.setDiscountRate(quoteVo.getDiscountRate());
		vo.setDiscountTotal(quoteVo.getDiscountTotal());
		vo.setNetTotal(quoteVo.getNetTotal());
		vo.setTaxTotal(quoteVo.getTaxTotal());
		vo.setGrandTotal(quoteVo.getGrandTotal());
		vo.setTaxes(quoteVo.getTaxes());
		vo.setTaxesWithRate(quoteVo.getTaxesWithRate());
		vo = this.createNoteProductVo(vo, quoteVo);
		return vo;
	}

	private ReceivingNoteVo createNoteProductVo(ReceivingNoteVo vo, VendorQuoteVo quoteVo) {
		List<ReceivingNoteProductVo> productVos = new ArrayList<ReceivingNoteProductVo>();
		for (VendorquoteProductVo items : quoteVo.getProductVos()) {
			BigDecimal toBeReceived = BigDecimal.ZERO;
			if(items.getReceivedQuantity()!=null)
			{
				toBeReceived = items.getQuantity().subtract(items.getReceivedQuantity());
			}else{
				toBeReceived = items.getQuantity();
			}
			ReceivingNoteProductVo productVo = new ReceivingNoteProductVo();
			productVo.setQuantityToReceive(toBeReceived);
			productVo.setAmountExcludingTax(items.getAmountExcludingTax());
			vo.setNetAmountHidden(vo.getNetAmountHidden().add(items.getAmountExcludingTax()));
			productVo.setProductCode(items.getProductCode());
			productVo.setProductName(items.getProductName());
			productVo.setProductId(items.getProductId());
			productVo.setQuantity(toBeReceived);
			productVo.setUnitPrice(items.getUnitPrice());
			productVo.setDiscount(items.getDiscount());
			productVo.setNetAmount(items.getNetAmount());
			productVo.setTax(items.getTax());
			productVos.add(productVo);
		}
		vo.setProductsTaxAmount(quoteVo.getProductsTaxAmount());
		vo.setProductVos(productVos);
		return vo;
	}
}
