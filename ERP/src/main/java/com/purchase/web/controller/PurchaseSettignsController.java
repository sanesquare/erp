package com.purchase.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.constants.AccountsRequestConstants;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.service.StockGroupService;
import com.purchase.web.service.UnitService;
import com.purchase.web.vo.StockGroupVo;
import com.purchase.web.vo.UnitVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 22-September-2015
 *
 */
@Controller
@SessionAttributes({ "companyId" })
public class PurchaseSettignsController {

	@Log
	private Logger logger;

	@Autowired
	private UnitService unitService;

	@Autowired
	private StockGroupService groupService;

	@RequestMapping(value = PurchaseRequestConstants.SETTINGS, method = RequestMethod.GET)
	public String purchaseSettings(final Model model, @ModelAttribute("companyId") Long companyId) {
		if (companyId == 0)
			return "redirect:" + AccountsRequestConstants.HOME;
		else {
			try {
				model.addAttribute("units", unitService.findAllUnitsByCompany(companyId));
				model.addAttribute("stockGroups", groupService.listStockGroupsForCompany(companyId));
			} catch (Exception e) {
				logger.info("SETTINGS: ",e);
			}
			return PurchasePageNameConstants.SETTINGS;
		}
	}

	/**
	 * method to save/update unit
	 * 
	 * @param unitVo
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.SAVE_UNIT, method = RequestMethod.POST)
	public @ResponseBody String saveUnit(@RequestBody UnitVo unitVo, @ModelAttribute("companyId") Long companyId) {
		try {
			unitVo.setCompanyId(companyId);
			unitService.saveOrUpdateUnit(unitVo);
		} catch (Exception e) {
			logger.info("SAVE_UNIT: ",e);
			return e.getMessage();
		}
		return "Unit Saved Successfully";
	}

	/**
	 * method to return units table
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.UNIT_LIST, method = RequestMethod.GET)
	public String unitTable(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("units", unitService.findAllUnitsByCompany(companyId));
		} catch (Exception e) {
			logger.info("UNIT_LIST: ",e);
		}
		return PurchasePageNameConstants.UNIT_TABLE;
	}

	/**
	 * method to delete unit
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.UNIT_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deleteUnit(final Model model, @RequestParam(value = "id", required = true) Long id) {
		try {
			unitService.deleteUnit(id);
		} catch (Exception e) {
			logger.info("UNIT_DELETE: ",e);
		}
		return "Unit Deleted Successfully.";
	}

	/**
	 * Method to save stock group
	 * 
	 * @param stockGroupVo
	 * @param companyid
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.SAVE_STOCK_GROUP, method = RequestMethod.POST)
	public @ResponseBody List<StockGroupVo> saveStockGroup(@RequestBody StockGroupVo stockGroupVo,
			@ModelAttribute("companyId") Long companyId) {
		List<StockGroupVo> groups = new ArrayList<StockGroupVo>();
		try {
			groups = groupService.listStockGroupsForCompany(companyId);
			stockGroupVo.setCompanyId(companyId);
			groupService.saveOrUpdateStockGroup(stockGroupVo);
			groups = groupService.listStockGroupsForCompany(companyId);

		} catch (Exception ex) {
			logger.info("SAVE_STOCK_GROUP: ",ex);
		}
		return groups;
	}

	@RequestMapping(value = PurchaseRequestConstants.DELETE_STOCK_GROUP , method = RequestMethod.GET)
	public @ResponseBody List<StockGroupVo> deleteStockGroup(@RequestParam(value = "id", required = true) Long id,
			@ModelAttribute("companyId") Long companyId) {
		List<StockGroupVo> groups = new ArrayList<StockGroupVo>();
		try {
			groupService.deleteStockGroup(id);
			groups = groupService.listStockGroupsForCompany(companyId);
		}catch (Exception ex) {
			logger.info("DELETE_STOCK_GROUP: ",ex);
		}
		return groups;
	}
	
	@RequestMapping(value = PurchaseRequestConstants.LIST_STOCK_GROUPS , method = RequestMethod.GET)
	public String getStockGroupsTable(final Model model , @ModelAttribute("companyId")Long companyId){
		try{
			model.addAttribute("stockGroups" , groupService.listStockGroupsForCompany(companyId));
		}catch(Exception ex){
			logger.info("LIST_STOCK_GROUPS: ",ex);	
		}
		return PurchasePageNameConstants.STOCK_GROUP_TABLE;
	}
}
