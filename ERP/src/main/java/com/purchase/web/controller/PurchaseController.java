package com.purchase.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.responses.JsonResponse;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
@Controller
public class PurchaseController {

	/**
	 * method to reach purchase home page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = PurchaseRequestConstants.HOME , method = RequestMethod.GET)
	public String purchaseHome(final Model model ){
		
		return PurchasePageNameConstants.HOME_PAGE;
	}
	
	
}
