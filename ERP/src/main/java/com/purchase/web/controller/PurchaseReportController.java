package com.purchase.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.erp.web.constants.ErpConstants;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.logger.Log;
import com.purchase.web.constants.PurchaseReportConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.service.BillService;
import com.purchase.web.service.VendorQuoteService;
import com.purchase.web.service.VendorService;
import com.purchase.web.vo.BillProductVo;
import com.purchase.web.vo.BillVo;
import com.purchase.web.vo.VendorQuoteVo;
import com.purchase.web.vo.VendorVo;
import com.purchase.web.vo.VendorquoteProductVo;
import com.sales.web.constants.SalesReportConstants;
import com.sales.web.vo.CustomerVo;
import com.sales.web.vo.InvoiceProductVo;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 * 
 * @author Shamsheer & Vinutha
 * @since Jan 4, 2016
 */
@Controller
public class PurchaseReportController {

	@Log
	private Logger logger;
	
	@Autowired
	private BillService billService;
	
	@Autowired
	private VendorQuoteService quoteService;
	
	@Autowired
	private VendorService vendorService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private ServletContext context;
	
	@RequestMapping(value =PurchaseRequestConstants.GENERATE_BILL_PDF , method=RequestMethod.GET)
	public String generatePurchaseInvoice(final Model model , @RequestParam(value = "id")Long id){
		try{
			BillVo billVo = billService.findBill(id);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LOGO", context.getRealPath(ErpConstants.LOGO_PATH));
			params.put("DATE", billVo.getBillDate());
			params.put("INVOICE", billVo.getBillCode());
			params.put("CUSTOMER", billVo.getVendor());
			params.put("subTotal", billVo.getSubTotal());
			params.put("discountRate", billVo.getDiscountRate());
			params.put("discountTotal", billVo.getDiscountTotal());
			params.put("netTotal",billVo.getNetTotal());
			params.put("taxTotal", billVo.getTaxTotal());
			params.put("grandTotal", billVo.getGrandTotal());
			params.put("PAYMENT_TERM", billVo.getPaymentTerm());
			params.put("DELIVERY_TERM", billVo.getDeliveryTerm());
			params.put("CURRENCY", billVo.getCurrecny());
			VendorVo vendorVo = vendorService.findVendorById(billVo.getVendorId());
			params.put("CUSTOMER_ADDRESS", vendorVo.getAddress());
			CompanyVo company = companyService.findCompanyById(billVo.getCompanyId());
			if (company != null)
				params.put("COMPANY", company.getParent());
			params.put("datasource", new JREmptyDataSource());
			
			List<String> taxes = billVo.getTaxes();
			if(taxes!=null)
				params.put("TAXES", new JRBeanCollectionDataSource(taxes));
			List<BillProductVo> products = billVo.getProductVos();
			if (products != null)
				params.put("ITEMS", new JRBeanCollectionDataSource(products));
			model.addAllAttributes(params);
		}catch(Exception e){
			logger.info("GENERATE_BILL_PDF : ",e);
		}
		
		return PurchaseReportConstants.BILL_REPORT;
	}
	
	@RequestMapping(value =PurchaseRequestConstants.GENERATE_PURCHASE_ORDER_PDF , method=RequestMethod.GET)
	public String generatePurchaseOrder(final Model model , @RequestParam(value = "id")Long id){
		try{
			VendorQuoteVo orderVo = quoteService.findVendorQuote(id);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LOGO", context.getRealPath(ErpConstants.LOGO_PATH));
			params.put("DATE", orderVo.getQuoteDate());
			params.put("INVOICE", orderVo.getOrderCode());
			params.put("CUSTOMER", orderVo.getVendor());
			params.put("subTotal", orderVo.getSubTotal());
			params.put("discountRate", orderVo.getDiscountRate());
			params.put("discountTotal", orderVo.getDiscountTotal());
			params.put("netTotal",orderVo.getNetTotal());
			params.put("taxTotal", orderVo.getTaxTotal());
			params.put("grandTotal", orderVo.getGrandTotal());
			params.put("PAYMENT_TERM", orderVo.getPaymentTerm());
			params.put("DELIVERY_TERM", orderVo.getDeliveryTerm());
			params.put("CURRENCY", orderVo.getCurrency());
			VendorVo vendorVo = vendorService.findVendorById(orderVo.getVendorId());
			params.put("CUSTOMER_ADDRESS", vendorVo.getAddress());
			CompanyVo company = companyService.findCompanyById(orderVo.getCompanyId());
			if (company != null)
				params.put("COMPANY", company.getParent());
			params.put("datasource", new JREmptyDataSource());			
			List<String> taxes = orderVo.getTaxes();
			if(taxes!=null)
				params.put("TAXES", new JRBeanCollectionDataSource(taxes));
			List<VendorquoteProductVo> products = orderVo.getProductVos();
			if (products != null)
				params.put("ITEMS", new JRBeanCollectionDataSource(products));
			model.addAllAttributes(params);
		}catch(Exception e){
			logger.info("GENERATE_PURCHASE_ORDER_PDF : ",e);
		}
		
		return PurchaseReportConstants.BILL_REPORT;
	}
	
	@RequestMapping(value =PurchaseRequestConstants.GENERATE_VENDOR_QUOTE_PDF , method=RequestMethod.GET)
	public String generateVendorQuotePdf(final Model model , @RequestParam(value = "id")Long id){
		try{
			VendorQuoteVo orderVo = quoteService.findVendorQuote(id);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LOGO", context.getRealPath(ErpConstants.LOGO_PATH));
			params.put("DATE", orderVo.getQuoteDate());
			params.put("INVOICE", orderVo.getQuoteCode());
			params.put("CUSTOMER", orderVo.getVendor());
			params.put("subTotal", orderVo.getSubTotal());
			params.put("discountRate", orderVo.getDiscountRate());
			params.put("discountTotal", orderVo.getDiscountTotal());
			params.put("netTotal",orderVo.getNetTotal());
			params.put("taxTotal", orderVo.getTaxTotal());
			params.put("grandTotal", orderVo.getGrandTotal());
			params.put("PAYMENT_TERM", orderVo.getPaymentTerm());
			params.put("DELIVERY_TERM", orderVo.getDeliveryTerm());
			params.put("CURRENCY", orderVo.getCurrency());
			VendorVo vendorVo = vendorService.findVendorById(orderVo.getVendorId());
			params.put("CUSTOMER_ADDRESS", vendorVo.getAddress());
			CompanyVo company = companyService.findCompanyById(orderVo.getCompanyId());
			if (company != null)
				params.put("COMPANY", company.getParent());
			params.put("datasource", new JREmptyDataSource());			
			List<String> taxes = orderVo.getTaxes();
			if(taxes!=null)
				params.put("TAXES", new JRBeanCollectionDataSource(taxes));
			List<VendorquoteProductVo> products = orderVo.getProductVos();
			if (products != null)
				params.put("ITEMS", new JRBeanCollectionDataSource(products));
			model.addAllAttributes(params);
		}catch(Exception e){
			logger.info("GENERATE_PURCHASE_ORDER_PDF : ",e);
		}
		
		return PurchaseReportConstants.BILL_REPORT;
	}
}
