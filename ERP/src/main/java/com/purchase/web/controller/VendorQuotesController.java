package com.purchase.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.service.LedgerService;
import com.erp.web.service.ChargeService;
import com.erp.web.service.DeliveryMethodService;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.service.VatService;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.service.ProductService;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.service.BillService;
import com.purchase.web.service.VendorQuoteService;
import com.purchase.web.service.VendorService;
import com.purchase.web.vo.VendorQuoteVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 29, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class VendorQuotesController {

	@Autowired
	private VendorService vendorService;

	@Autowired
	private ProductService productService;

	@Autowired
	private VatService taxService;

	@Autowired
	private ChargeService chargeService;

	@Autowired
	private PaymentTermsService paymentTermService;

	@Log
	private Logger logger;

	@Autowired
	private ErpCurrencyService erpCurrencyService;

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private VendorQuoteService vendorQuoteService;

	@Autowired
	private DeliveryMethodService deliveryMethodService;

	@RequestMapping(value = PurchaseRequestConstants.VENDOR_QUOTES_HOME, method = RequestMethod.GET)
	public String vendorQuotesHomePageHandler(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			List<VendorQuoteVo> quoteVos = vendorQuoteService.listVendorQuotesForCompany(companyId);
			model.addAttribute("quotes", quoteVos);
		} catch (Exception ex) {
			logger.info("VENDOR_QUOTES_HOME: ", ex);
		}
		return PurchasePageNameConstants.VENDOR_QUOTES_HOME;
	}

	@RequestMapping(value = PurchaseRequestConstants.VENDOR_QUOTES_LIST, method = RequestMethod.GET)
	public String vendorQuotesList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			List<VendorQuoteVo> quoteVos = vendorQuoteService.listVendorQuotesForCompany(companyId);
			model.addAttribute("quotes", quoteVos);
		} catch (Exception ex) {
			logger.info("VENDOR_QUOTES_HOME: ", ex);
		}
		return PurchasePageNameConstants.VENDOR_QUOTE_TEMPLATE;
	}

	@RequestMapping(value = PurchaseRequestConstants.ADD_VENDOR_QUOTE, method = RequestMethod.GET)
	public String addVendorQuote(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId) {
		try {
			model.addAttribute("quote", new VendorQuoteVo());
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
			model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
			model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
		} catch (Exception ex) {
			logger.info("ADD_VENDOR_QUOTE: ", ex);
		}
		return PurchasePageNameConstants.ADD_VENDOR_QUOTES;
	}

	@RequestMapping(value = PurchaseRequestConstants.VENDOR_QUOTE_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveVendorQuote(final Model model, @ModelAttribute("companyId") Long compnyId,
			@RequestBody VendorQuoteVo quoteVo) {
		Long id = 0L;
		quoteVo.setCompanyId(compnyId);
		try {
			id = vendorQuoteService.saveOrUpdateVendorQuote(quoteVo);
		} catch (Exception e) {
			logger.info("VENDOR_QUOTE_SAVE: ", e);
		}
		return id;
	}

	@RequestMapping(value = PurchaseRequestConstants.VENDOR_QUOTE_VIEW, method = RequestMethod.GET)
	public String viewVendorQuote(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			VendorQuoteVo quoteVo = vendorQuoteService.findVendorQuote(id);
			model.addAttribute("quote", quoteVo);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", quoteVo.getQuoteDate());
			if (!quoteVo.getIsView()) {
				model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
				model.addAttribute("products", productService.findAllProducts(companyId));
				model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
				model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
				model.addAttribute("currencies",
						erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
				model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
			}
		} catch (Exception ex) {
			logger.info("VENDOR_QUOTE_VIEW: ", ex);
		}
		return PurchasePageNameConstants.ADD_VENDOR_QUOTES;
	}

	@RequestMapping(value = PurchaseRequestConstants.CONVERT_VENDOR_QUOTE_TO_ORDER, method = RequestMethod.GET)
	public @ResponseBody ModelAndView convertVendorQuoteToOrder(final Model model,
			@RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = null;
		try {
			vendorQuoteService.updateVendorQuoteToPurchaseOrder(id);
			modelAndView = new ModelAndView(new RedirectView(
					PurchaseRequestConstants.VENDOR_QUOTES_LIST + "?msg=Vendor quote converted successfully"));
		} catch (Exception e) {
			modelAndView = new ModelAndView(new RedirectView(
					PurchaseRequestConstants.VENDOR_QUOTES_LIST + "?msg=Vendor quote convertion failed"));
			logger.info("CONVERT_VENDOR_QUOTE_TO_ORDER: ", e);
		}
		return modelAndView;
	}

	@RequestMapping(value = PurchaseRequestConstants.VENDOR_QUOTE_DELETE, method = RequestMethod.GET)
	public @ResponseBody ModelAndView deleteVendorQuote(final Model model,
			@RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = null;
		try {
			vendorQuoteService.deleteVendorQuote(id);
			modelAndView = new ModelAndView(new RedirectView(
					PurchaseRequestConstants.VENDOR_QUOTES_LIST + "?msg=Vendor quote deleted successfully"));
		} catch (Exception e) {
			modelAndView = new ModelAndView(
					new RedirectView(PurchaseRequestConstants.VENDOR_QUOTES_LIST + "?msg=Vendor quote delete failed"));
			logger.info("VENDOR_QUOTE_DELETE: ", e);
		}
		return modelAndView;
	}
}
