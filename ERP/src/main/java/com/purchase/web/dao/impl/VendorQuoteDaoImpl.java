package com.purchase.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.ChargeDao;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.DeliveryMethodDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.dao.PaymentTermsDao;
import com.erp.web.dao.VatDao;
import com.erp.web.entities.Charge;
import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ReceivingNote;
import com.purchase.web.dao.VendorDao;
import com.purchase.web.dao.VendorQuoteDao;
import com.purchase.web.entities.PurchaseOrderCharges;
import com.purchase.web.entities.Vendor;
import com.purchase.web.entities.VendorQuoteItems;
import com.purchase.web.entities.VendorQuotes;
import com.purchase.web.vo.PurchaseOrderChargeVo;
import com.purchase.web.vo.VendorQuoteVo;
import com.purchase.web.vo.VendorquoteProductVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 17, 2015
 */
@Repository
public class VendorQuoteDaoImpl extends AbstractDao implements VendorQuoteDao {

	@Autowired
	private ProductDao productDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private VendorDao vendorDao;

	@Autowired
	private PaymentTermsDao paymentTermsDao;

	@Autowired
	private VatDao vatDao;

	@Autowired
	private ErpCurrencyDao currencyDao;

	@Autowired
	private ChargeDao chargeDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private DeliveryMethodDao deliveryMethodDao;

	public Long saveOrUpdateVendorQuote(VendorQuoteVo quoteVo) {
		Boolean newQuote = false;
		Long id = quoteVo.getQuoteId();
		Company company = companyDao.findCompany(quoteVo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		Vendor vendor = vendorDao.findVendor(quoteVo.getVendorId());
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Not Found");
		VendorQuotes quotes = null;
		if (id == null) {
			quotes = new VendorQuotes();
			quotes.setIsQuote(true);
			newQuote = true;
			NumberPropertyVo numberPropertyVo = numberPropertyDao
					.findNumberProperty(NumberPropertyConstants.VENDOR_QUOTE, quoteVo.getCompanyId());
			quotes.setQuoteCode(numberPropertyVo.getPrefix() + numberPropertyVo.getNumber());
		} else {
			quotes = findVendorQuoteObject(id);
			if (quotes == null)
				throw new ItemNotFoundException("Vendor Quote Not Found");
		}
		PaymentTerms paymentTerms = paymentTermsDao.findPaymentTerms(quoteVo.getPaymentTermId());
		if (paymentTerms == null)
			throw new ItemNotFoundException("Payment term not found");
		quotes.setPaymentTerm(paymentTerms);
		DeliveryMethod deliveryMethod = deliveryMethodDao.findDeliveryMethodObject(quoteVo.getDeliveryTermId());
		if (deliveryMethod != null)
			quotes.setDeliveryTerm(deliveryMethod);
		ErPCurrency currency = currencyDao.findCurrency(quoteVo.getErpCurrencyId());
		quotes.setCompany(company);
		quotes.setVendor(vendor);
		quotes.setCurrency(currency);
		quotes = this.setAttributesForVendorQuote(quotes, quoteVo);
		quotes = this.setProductsForQuotes(quotes, quoteVo);
		id = (Long) this.sessionFactory.getCurrentSession().save(quotes);
		if (newQuote)
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.VENDOR_QUOTE, company);
		return id;
	}

	private VendorQuotes setAttributesForVendorQuote(VendorQuotes quotes, VendorQuoteVo quoteVo) {

		quotes.setDate(DateFormatter.convertStringToDate(quoteVo.getQuoteDate()));
		quotes.setDiscountAmount(quoteVo.getDiscountTotal());
		if (quoteVo.getExpiryDate() != null)
			quotes.setExpirationDate(DateFormatter.convertStringToDate(quoteVo.getExpiryDate()));
		quotes.setSubTotal(quoteVo.getSubTotal());
		quotes.setTotal(quoteVo.getGrandTotal());
		quotes.getVat().clear();
		quotes.setNetAmount(quoteVo.getNetAmountHidden());
		quotes.setSubTotal(quoteVo.getSubTotal());
		quotes.setDiscountRate(quoteVo.getDiscountRate());
		quotes.setNetTotal(quoteVo.getNetTotal());
		quotes.setTaxTotal(quoteVo.getTaxTotal());
		List<VAT> taxes = new ArrayList<VAT>();
		for (Long id : quoteVo.getTsxIds()) {
			VAT vat = vatDao.findTaxObject(id);
			taxes.add(vat);
		}
		quotes.getVat().addAll(taxes);
		return quotes;
	}

	private VendorQuotes setProductsForQuotes(VendorQuotes quotes, VendorQuoteVo quoteVo) {
		quotes.getItems().clear();
		List<VendorQuoteItems> items = new ArrayList<VendorQuoteItems>();
		Boolean hasReceived = true;
		for (VendorquoteProductVo vo : quoteVo.getProductVos()) {
			VendorQuoteItems item = new VendorQuoteItems();
			VAT vat = vatDao.findTaxObject(vo.getTaxId());
			item.setVat(vat);
			Product product = productDao.findProduct(vo.getProductCode(), quoteVo.getCompanyId());
			item.setProduct(product);
			product.getVendorQuoteItems().add(item);
			item.setQuantity(vo.getQuantity());
			item.setReceivedQuantity(vo.getReceivedQuantity());
			item.setPrice(vo.getUnitPrice());
			item.setDicscountRate(vo.getDiscount());
			item.setTotal(vo.getNetAmount());
			item.setAmountExcludingTax(vo.getAmountExcludingTax());
			item.setVendorQuote(quotes);
			if (item.getQuantity().compareTo(item.getReceivedQuantity()) != 0)
				hasReceived = false;
			items.add(item);
		}
		quotes.setHasReceived(hasReceived);
		quotes.getItems().addAll(items);
		return quotes;
	}

	public void deleteVendorQuote(Long id) {
		VendorQuotes quotes = findVendorQuoteObject(id);
		if (quotes == null)
			throw new ItemNotFoundException("Vendor Quote Not Found");
		if (quotes.getIsQuote())
			this.sessionFactory.getCurrentSession().delete(quotes);
		else if (quotes.getQuoteCode() == null)
			this.sessionFactory.getCurrentSession().delete(quotes);
		else {
			quotes.setIsQuote(true);
			this.sessionFactory.getCurrentSession().merge(quotes);
		}

	}

	public VendorQuotes findVendorQuoteObject(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(VendorQuotes.class);
		criteria.add(Restrictions.eq("id", id));
		return (VendorQuotes) criteria.uniqueResult();
	}

	public VendorQuoteVo findVendorQuote(Long id) {
		VendorQuotes quotes = findVendorQuoteObject(id);
		if (quotes == null)
			throw new ItemNotFoundException("Vendor Quote Not Found");
		return this.createVendorQuoteVo(quotes);
	}

	private VendorQuoteVo createVendorQuoteVo(VendorQuotes quotes) {
		VendorQuoteVo vo = new VendorQuoteVo();
		vo.setQuoteId(quotes.getId());
		Set<ReceivingNote> receivingNote = quotes.getReceivingNote();
		if (receivingNote != null) {
			Map<Long, String> notesMap = new HashMap<Long, String>();
			for (ReceivingNote note : receivingNote) {
				notesMap.put(note.getId(), note.getReferenceNumber());
			}
			vo.setReceivingNoteIdCode(notesMap);
		}
		Vendor vendor = quotes.getVendor();
		vo.setVendor(vendor.getName());
		vo.setVendorId(vendor.getId());
		if (quotes.getExpirationDate() != null)
			vo.setExpiryDate(DateFormatter.convertDateToString(quotes.getExpirationDate()));
		if (quotes.getDate() != null)
			vo.setQuoteDate(DateFormatter.convertDateToString(quotes.getDate()));
		ErPCurrency currency = quotes.getCurrency();
		vo.setCurrency(currency.getBaseCurrency().getBaseCurrency());
		Company company = quotes.getCompany();
		vo.setCompanyId(company.getId());
		vo.setDiscountRate(quotes.getDiscountRate());
		vo.setGrandTotal(quotes.getTotal());
		vo.setNetAmountHidden(quotes.getNetAmount());
		vo.setNetTotal(quotes.getNetTotal());
		vo.setSubTotal(quotes.getSubTotal());
		PaymentTerms paymentTerms = quotes.getPaymentTerm();
		vo.setPaymentTerm(paymentTerms.getTerm());
		vo.setPaymentTermId(paymentTerms.getId());
		DeliveryMethod deliveryMethod = quotes.getDeliveryTerm();
		if (deliveryMethod != null) {
			vo.setDeliveryTerm(deliveryMethod.getMethod());
			vo.setDeliveryTermId(deliveryMethod.getId());
		}
		vo.setTaxTotal(quotes.getTaxTotal());
		vo.setDiscountTotal(quotes.getDiscountAmount());
		for (VAT tax : quotes.getVat()) {
			vo.getTaxes().add(tax.getName());
			vo.getTsxIds().add(tax.getId());
			vo.getTaxesWithRate().add(tax.getId() + "!!!" + tax.getPercentage());
		}

		List<VendorquoteProductVo> items = new ArrayList<VendorquoteProductVo>();
		for (VendorQuoteItems item : quotes.getItems()) {
			VendorquoteProductVo productVo = new VendorquoteProductVo();
			productVo.setAmountExcludingTax(item.getAmountExcludingTax());
			productVo.setDiscount(item.getDicscountRate());
			productVo.setNetAmount(item.getTotal());
			productVo.setQuantity(item.getQuantity());
			productVo.setReceivedQuantity(item.getReceivedQuantity());
			productVo.setQuoteId(quotes.getId());
			Product product = item.getProduct();
			productVo.setProductCode(product.getProductCode());
			productVo.setProductId(product.getId());
			productVo.setProductName(product.getName());
			productVo.setUnitPrice(item.getPrice());
			VAT vat = item.getVat();
			if (vat != null) {
				productVo.setTax(vat.getName());
				productVo.setTaxId(vat.getId());
			}
			items.add(productVo);
		}
		vo.setHasReceived(quotes.getHasReceived());
		vo.setProductVos(items);
		if (!quotes.getIsQuote()) {
			vo = createPurchaseOrderChargeVo(vo, quotes);
		}
		vo.setIsQuote(quotes.getIsQuote());
		vo.setQuoteCode(quotes.getQuoteCode());
		vo.setOrderCode(quotes.getOrderCode());
		return vo;
	}

	private VendorQuoteVo createPurchaseOrderChargeVo(VendorQuoteVo vo, VendorQuotes quotes) {
		List<PurchaseOrderChargeVo> chargeVos = new ArrayList<PurchaseOrderChargeVo>();
		for (PurchaseOrderCharges charge : quotes.getCharges()) {
			PurchaseOrderChargeVo chargeVo = new PurchaseOrderChargeVo();
			chargeVo.setChargeId(charge.getId());
			chargeVo.setCharge(charge.getCharge().getName());
			chargeVo.setAmount(charge.getAmount());
			chargeVos.add(chargeVo);
		}
		vo.setChargeVos(chargeVos);
		return vo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<VendorQuoteVo> listVendorQuotesForCompany(Long companyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(VendorQuotes.class);
		criteria.add(Restrictions.eq("company.id", companyId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.isNotNull("quoteCode"));
		List<VendorQuotes> quotes = criteria.list();
		List<VendorQuoteVo> list = new ArrayList<VendorQuoteVo>();
		if (quotes != null) {
			for (VendorQuotes quote : quotes)
				list.add(this.createVendorQuoteVo(quote));
		}
		return list;
	}

	public Long saveOrUpdatePurchaseOrder(VendorQuoteVo quoteVo) {
		Long id = quoteVo.getQuoteId();
		Boolean isNew = false;
		Company company = companyDao.findCompany(quoteVo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		Vendor vendor = vendorDao.findVendor(quoteVo.getVendorId());
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Not Found");
		VendorQuotes quotes = null;
		if (id == null) {
			isNew = true;
			quotes = new VendorQuotes();
			quotes.setIsQuote(false);
			NumberPropertyVo numberPropertyVo = numberPropertyDao
					.findNumberProperty(NumberPropertyConstants.PURCHASE_ORDER, quoteVo.getCompanyId());
			quotes.setOrderCode(numberPropertyVo.getPrefix() + numberPropertyVo.getNumber());
		} else {
			quotes = findVendorQuoteObject(id);
			if (quotes == null)
				throw new ItemNotFoundException("Purchase Order Not Found");
		}
		PaymentTerms paymentTerms = paymentTermsDao.findPaymentTerms(quoteVo.getPaymentTermId());
		if (paymentTerms == null)
			throw new ItemNotFoundException("Payment term not found");
		quotes.setPaymentTerm(paymentTerms);
		DeliveryMethod deliveryMethod = deliveryMethodDao.findDeliveryMethodObject(quoteVo.getDeliveryTermId());
		if (deliveryMethod != null)
			 quotes.setDeliveryTerm(deliveryMethod);
		ErPCurrency currency = currencyDao.findCurrency(quoteVo.getErpCurrencyId());
		quotes.setCompany(company);
		quotes.setVendor(vendor);
		quotes.setCurrency(currency);
		quotes = this.setAttributesForVendorQuote(quotes, quoteVo);
		quotes = this.setProductsForQuotes(quotes, quoteVo);
		quotes = this.setChargesToPurchaseOrder(quotes, quoteVo);
		id = (Long) this.sessionFactory.getCurrentSession().save(quotes);
		if (isNew)
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PURCHASE_ORDER, company);
		return id;

	}

	private VendorQuotes setChargesToPurchaseOrder(VendorQuotes quotes, VendorQuoteVo vo) {
		quotes.getCharges().clear();
		Set<PurchaseOrderCharges> purchaseOrderCharges = new HashSet<PurchaseOrderCharges>();
		for (PurchaseOrderChargeVo chargeVo : vo.getChargeVos()) {
			PurchaseOrderCharges charges = new PurchaseOrderCharges();
			Charge charge = chargeDao.findCharge(chargeVo.getChargeId());
			charges.setCharge(charge);
			charges.setAmount(chargeVo.getAmount());
			charge.getPurchaseOrderCharges().add(charges);
			charges.setPurchaseOrder(quotes);
			purchaseOrderCharges.add(charges);
		}
		quotes.getCharges().addAll(purchaseOrderCharges);
		return quotes;
	}

	public void updateVendorQuoteToPurchaseOrder(Long id) {
		VendorQuotes quotes = findVendorQuoteObject(id);
		quotes.setIsQuote(false);
		NumberPropertyVo vo = numberPropertyDao.findNumberProperty(NumberPropertyConstants.PURCHASE_ORDER,
				quotes.getCompany().getId());
		quotes.setOrderCode(vo.getPrefix() + vo.getNumber());
		this.sessionFactory.getCurrentSession().merge(quotes);
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PURCHASE_ORDER, quotes.getCompany());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<VendorQuoteVo> listPurchaseOrdersForCompany(Long companyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(VendorQuotes.class);
		criteria.add(Restrictions.eq("company.id", companyId));
		criteria.add(Restrictions.eq("isQuote", false));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<VendorQuotes> quotes = criteria.list();
		List<VendorQuoteVo> list = new ArrayList<VendorQuoteVo>();
		if (quotes != null) {
			for (VendorQuotes quote : quotes)
				list.add(this.createVendorQuoteVo(quote));
		}
		return list;
	}

}
