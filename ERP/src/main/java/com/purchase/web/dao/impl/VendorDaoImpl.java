package com.purchase.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.LedgerGroupDao;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.LedgerGroup;
import com.erp.web.dao.CompanyDao;
import com.erp.web.entities.Company;
import com.hrms.web.constants.CommonConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CountryDao;
import com.hrms.web.entities.Country;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.purchase.web.dao.VendorDao;
import com.purchase.web.entities.Vendor;
import com.purchase.web.entities.VendorContacts;
import com.purchase.web.vo.VendorContactVo;
import com.purchase.web.vo.VendorVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 03-August-2015
 */
@Repository
public class VendorDaoImpl extends AbstractDao implements VendorDao {

	@Autowired
	private CountryDao countryDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private LedgerGroupDao ledgerGroupDao;

	public Long saveOrUpdateVendor(VendorVo vendorVo) {
		// TODO vendor code generation && Account creation
		Long id = null;
		Vendor vendor = findVendor(vendorVo.getVendorId());
		Ledger ledger = null;
		Company company = companyDao.findCompany(vendorVo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		if (vendor != null) {
			id = vendorVo.getVendorId();
			vendor.setCompany(company);
			vendor = setValuesToVendor(vendorVo, vendor);
			ledger = vendor.getLedger();
			ledger.setLedgerName(vendorVo.getName());			
			ledger.setVendor(vendor);
			vendor.setLedger(ledger);
			this.sessionFactory.getCurrentSession().merge(vendor);
		} else {
			vendor = new Vendor();
			LedgerGroup ledgerGroup = ledgerGroupDao.findLedgerGroup(AccountsConstants.SUNDRY_CREDITOR,
					vendorVo.getCompanyId());
			if (ledgerGroup == null)
				throw new ItemNotFoundException("Ledger Group Not Found.");
			ledger = new Ledger();
			ledger.setCompany(company);
			ledger.setIsEditable(false);
			ledger.setBalance(BigDecimal.ZERO);
			ledger.setLedgerGroup(ledgerGroup);
			ledger.setOpeningBalance(BigDecimal.ZERO);
			ledger.setStartDate(company.getPeriodStart());
			ledger.setLedgerName(vendorVo.getName());			
			ledger.setVendor(vendor);
			vendor.setLedger(ledger);
			vendor.setCompany(company);
			vendor.setCode(this.generateVendorCode());
			vendor = setValuesToVendor(vendorVo, vendor);
			id = (Long) this.sessionFactory.getCurrentSession().save(vendor);
		}

		return id;
	}

	/**
	 * method to generate vendor code
	 * 
	 * @return
	 */
	public String generateVendorCode() {
		String code = "VEND-";
		DetachedCriteria maxId = DetachedCriteria.forClass(Vendor.class).setProjection(Projections.max("id"));
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Vendor.class);
		criteria.add(Property.forName("id").eq(maxId));
		Vendor vendor = (Vendor) criteria.uniqueResult();
		if (vendor != null) {
			code = code + String.valueOf(vendor.getId() + 1);
		} else {
			code = code + 1;
		}

		return code;
	}

	/**
	 * method to set values to vendor
	 * 
	 * @param vendorVo
	 * @param vendor
	 * @return
	 */
	private Vendor setValuesToVendor(VendorVo vendorVo, Vendor vendor) {
		Country country = countryDao.findCountry(vendorVo.getCountryId());
		if (country == null)
			throw new ItemNotFoundException("Country Not Found");
		vendor.setCountry(country);
		vendor.setName(vendorVo.getName());
		vendor.setAddress(vendorVo.getAddress());
		vendor.setDeliveryAddress(vendorVo.getDeliveryAddress());
		vendor.setMobile(vendorVo.getMobile());
		vendor.setPhone(vendorVo.getLandPhone());
		vendor.setEmail(vendorVo.getEmail());
		return vendor;
	}

	public void deleteVendor(Long vendorId) {
		Vendor vendor = this.findVendor(vendorId);
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Does Not Exist.");
		this.sessionFactory.getCurrentSession().delete(vendor);
	}

	public void deleteVendor(String vendorCode) {
		Vendor vendor = this.findVendor(vendorCode);
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Does Not Exist.");
		this.sessionFactory.getCurrentSession().delete(vendor);
	}

	public Vendor findVendor(Long vendorId) {
		return (Vendor) this.sessionFactory.getCurrentSession().createCriteria(Vendor.class)
				.add(Restrictions.eq("id", vendorId)).uniqueResult();
	}

	public Vendor findVendor(String vendorCode) {
		return (Vendor) this.sessionFactory.getCurrentSession().createCriteria(Vendor.class)
				.add(Restrictions.eq("code", vendorCode)).uniqueResult();
	}

	public VendorVo findVendorById(Long id) {
		Vendor vendor = this.findVendor(id);
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Does Not Exist.");
		return creatVendorVo(vendor);
	}

	/**
	 * method to create vendor vo
	 * 
	 * @param vendor
	 * @return
	 */
	private VendorVo creatVendorVo(Vendor vendor) {
		VendorVo vo = new VendorVo();
		vo.setName(vendor.getName());
		vo.setVendorCode(vendor.getCode());
		vo.setVendorId(vendor.getId());
		vo.setMobile(vendor.getMobile());
		vo.setAddress(vendor.getAddress());
		vo.setDeliveryAddress(vendor.getDeliveryAddress());
		vo.setLandPhone(vendor.getPhone());
		vo.setEmail(vendor.getEmail());
		Country country = vendor.getCountry();
		vo.setCountryId(country.getId());
		vo.setCountry(country.getName());
		if (!vendor.getContacts().isEmpty()) {
			for (VendorContacts contacts : vendor.getContacts()) {
				vo.getContactVos().add(createVendorContactVo(contacts, vendor));
			}
		}
		if (vendor.getCompany() != null)
			vo.setCompanyId(vendor.getCompany().getId());
		return vo;
	}

	/**
	 * method to create vendor contact vo
	 * 
	 * @param contact
	 * @return
	 */
	private VendorContactVo createVendorContactVo(VendorContacts contact, Vendor vendor) {
		VendorContactVo vo = new VendorContactVo();
		vo.setVendorId(vendor.getId());
		vo.setContactId(contact.getId());
		vo.setVendorCode(vendor.getCode());
		vo.setName(contact.getFirstName());
		vo.setWorkPhone(contact.getPhone());
		vo.setEmail(contact.getEmail());
		vo.setAddress(contact.getAddress());
		return vo;
	}

	public VendorVo findVendorByCode(String vendorCode) {
		Vendor vendor = this.findVendor(vendorCode);
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Does Not Exist.");
		return creatVendorVo(vendor);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<VendorVo> findAllVendors() {
		List<VendorVo> vendorVos = new ArrayList<VendorVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Vendor.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Vendor> vendors = criteria.list();
		for (Vendor vendor : vendors) {
			vendorVos.add(creatVendorVo(vendor));
		}
		return vendorVos;
	}

	public void updateVendorContact(VendorContactVo contactVo) {
		VendorContacts contact = null;
		Vendor vendor = this.findVendor(contactVo.getVendorId());
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Does Not Exist.");

		if (contactVo.getContactId() != null) {
			contact = this.findVendorContact(contactVo.getContactId());
			if (contact == null)
				throw new ItemNotFoundException("Contact Does Not Exist.");
			contact = setContactValues(contactVo, contact);
		} else {
			contact = new VendorContacts();
			contact = setContactValues(contactVo, contact);
			contact.setVendor(vendor);
		}
		vendor.getContacts().add(contact);
		this.sessionFactory.getCurrentSession().merge(vendor);
	}

	/**
	 * method to set values into vendor contact
	 * 
	 * @param contactVo
	 * @param contact
	 * @return
	 */
	private VendorContacts setContactValues(VendorContactVo contactVo, VendorContacts contact) {
		contact.setFirstName(contactVo.getName());
		contact.setPhone(contactVo.getWorkPhone());
		contact.setEmail(contactVo.getEmail());
		contact.setAddress(contactVo.getAddress());
		return contact;
	}

	public VendorContacts findVendorContact(Long id) {
		return (VendorContacts) this.sessionFactory.getCurrentSession().createCriteria(VendorContacts.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public VendorContactVo findVendorContactById(Long id) {
		VendorContacts contacts = this.findVendorContact(id);
		if (contacts == null)
			throw new ItemNotFoundException("Contact Does Not Exist.");
		return createVendorContactVo(contacts, contacts.getVendor());
	}

	public void deleteVendorContact(Long id) {
		VendorContacts contacts = this.findVendorContact(id);
		if (contacts == null)
			throw new ItemNotFoundException("Contact Does Not Exist.");
		Vendor vendor = contacts.getVendor();
		vendor.getContacts().remove(contacts);
		this.sessionFactory.getCurrentSession().merge(vendor);
	}

	public List<VendorContactVo> findVendorContacts(Long vendorId) {
		List<VendorContactVo> contactVos = new ArrayList<VendorContactVo>();
		Vendor vendor = this.findVendor(vendorId);
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Does Not Exist.");
		if (!vendor.getContacts().isEmpty()) {
			for (VendorContacts contact : vendor.getContacts()) {
				contactVos.add(createVendorContactVo(contact, vendor));
			}
		}
		return contactVos;
	}

	public List<VendorContactVo> findVendorContacts(String vendorCode) {
		List<VendorContactVo> contactVos = new ArrayList<VendorContactVo>();
		Vendor vendor = this.findVendor(vendorCode);
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Does Not Exist.");
		if (vendor.getContacts() != null) {
			for (VendorContacts contact : vendor.getContacts()) {
				contactVos.add(createVendorContactVo(contact, vendor));
			}
		}
		return contactVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<VendorVo> findVendorsForCompany(Long companyId) {

		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Vendor.class);
		criteria.add(Restrictions.eq("company", company));
		List<Vendor> vendors = new ArrayList<Vendor>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		vendors = criteria.list();

		List<VendorVo> vendorVos = new ArrayList<VendorVo>();
		for (Vendor vendor : vendors) {
			vendorVos.add(creatVendorVo(vendor));
		}
		return vendorVos;
	}

}
