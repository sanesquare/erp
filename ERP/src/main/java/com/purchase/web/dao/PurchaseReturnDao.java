package com.purchase.web.dao;

import java.util.List;

import com.purchase.web.entities.PurchaseReturn;
import com.purchase.web.vo.PurchaseReturnVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 11, 2015
 */
public interface PurchaseReturnDao {

	/**
	 * method to save purchase return
	 * @param vo
	 * @return
	 */
	public Long savePurchaseReturn(PurchaseReturnVo vo);
	
	/**
	 * method to find purchase return by id
	 * @param id
	 * @return
	 */
	public PurchaseReturn findPurchaseReturn(Long id);
	
	/**
	 * method to delete purchase return
	 * @param id
	 * @return
	 */
	public void deletePurchaseReturn(Long id);
	
	/**
	 * method to find purchase returns by company
	 * @param companyId
	 * @return
	 */
	public List<PurchaseReturnVo> findPurchaseReturns(Long companyId);
	
	/**
	 * method to find purchase return by id
	 * @param id
	 * @return
	 */
	public PurchaseReturnVo findPurchaseReturnById(Long id);
	
	public PurchaseReturnVo convertBillToReturn(Long billId);
}
