package com.purchase.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.JournalDao;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.PaymentDao;
import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.Payment;
import com.accounts.web.vo.JournalItemsVo;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.PaymentItemsVo;
import com.accounts.web.vo.PaymentVo;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.ChargeDao;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.DeliveryMethodDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.dao.PaymentTermsDao;
import com.erp.web.dao.VatDao;
import com.erp.web.entities.Charge;
import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.Godown;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.erp.web.service.DeliveryMethodService;
import com.erp.web.vo.ErpCurrencyVo;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.dao.GodownDao;
import com.inventory.web.dao.InventoryEntryDao;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.dao.ReceivingNotesDao;
import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.entities.InventoryEntryItems;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.InventoryWithdrawalItems;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ReceivingNote;
import com.inventory.web.entities.ReceivingNoteItems;
import com.inventory.web.vo.InventoryEntryItemsVo;
import com.inventory.web.vo.InventoryEntryVo;
import com.purchase.web.dao.BillDao;
import com.purchase.web.dao.VendorDao;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.BillCharges;
import com.purchase.web.entities.BillInstallments;
import com.purchase.web.entities.BillItems;
import com.purchase.web.entities.Vendor;
import com.purchase.web.vo.BillChargeVo;
import com.purchase.web.vo.BillInstallmentVo;
import com.purchase.web.vo.BillProductVo;
import com.purchase.web.vo.BillVo;
import com.sales.web.vo.InvoiceProductVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 6, 2015
 */
@Repository
public class BillDaoImpl extends AbstractDao implements BillDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private ErpCurrencyDao currencyDao;

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private GodownDao godownDao;

	@Autowired
	private InventoryEntryDao inventoryEntryDao;

	@Autowired
	private VatDao vatDao;

	@Autowired
	private JournalDao journalDao;

	@Autowired
	private VendorDao vendorDao;

	@Autowired
	private PaymentTermsDao paymentTermsDao;

	@Autowired
	private PaymentDao paymentDao;

	@Autowired
	private ChargeDao chargeDao;

	@Autowired
	private ReceivingNotesDao receivingNotesDao;

	@Autowired
	private DeliveryMethodDao deliveryMethodDao;

	public Long saveOrUpdateBill(BillVo billVo) {
		Boolean exists = true;
		Long id = billVo.getBillId();
		Bill bill = this.findBillObject(id);
		if (bill == null) {
			bill = new Bill();
			bill.setIsArchive(false);
			exists = false;
			for (Long receivingNoteId : billVo.getReceivingNoteIds()) {
				ReceivingNote note = receivingNotesDao.findReceivingNoteObject(receivingNoteId);
				bill.getReceivingNote().add(note);
			}
			bill.setVoucherNumber(this.generateBillCode(NumberPropertyConstants.BILL, billVo.getCompanyId()));
		}
		Company company = companyDao.findCompany(billVo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		Vendor vendor = vendorDao.findVendor(billVo.getVendorId());
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Not Found.");
		// to account for payment
		Ledger paymentToAccount = ledgerDao.findLedgerObjectById(billVo.getLedgerId());
		if (paymentToAccount == null)
			throw new ItemNotFoundException("Ledger Not Found.");
		bill.setLedger(paymentToAccount);
		// to account for journal
		Ledger billsPayableLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.BILLS_PAYABLE_LEDGER,
				company.getId());
		if (billsPayableLedger == null)
			throw new ItemNotFoundException("Ledger Not Found.");
		Ledger journalByAccount = ledgerDao.findLedgerObjectByName(AccountsConstants.PURCHASE_LEDGER, company.getId());

		if (journalByAccount == null)
			throw new ItemNotFoundException("Ledger Not Found.");

		Ledger taxReceivable = ledgerDao.findLedgerObjectByName(AccountsConstants.TAX_RECEIVABLE_LEDGER,
				company.getId());
		if (taxReceivable == null)
			throw new ItemNotFoundException("Tax Receivable Ledger Not Found");

		Ledger discountReceived = ledgerDao.findLedgerObjectByName(AccountsConstants.DISCOUNT_RECEIVED_LEDGER,
				company.getId());
		if (discountReceived == null)
			throw new ItemNotFoundException("Discount Received Ledger Not Found");
		PaymentTerms paymentTerm = paymentTermsDao.findPaymentTerms(billVo.getPaymentTermId());
		if (paymentTerm == null)
			throw new ItemNotFoundException("Payment Term Not Found.");
		DeliveryMethod deliveryMethod = deliveryMethodDao.findDeliveryMethodObject(billVo.getDeliveryTermId());
		if (deliveryMethod != null)
			bill.setDeliveryTerm(deliveryMethod);
		ErPCurrency currency = currencyDao.findCurrency(billVo.getErpCurrencyId());

		bill.setCompany(company);
		bill.setVendor(vendor);
		bill.setPaymentTerms(paymentTerm);
		bill.setCurrency(currency);
		bill.setDate(DateFormatter.convertStringToDate(billVo.getBillDate()));
		bill = this.setAttributesToBill(bill, billVo);
		// setting journal
		bill = this.setJournalToBill(bill, billVo, journalByAccount, billsPayableLedger, taxReceivable,
				discountReceived);
		bill = this.setProductsToBill(bill, billVo);
		bill = this.setChargesToBill(bill, billVo);
		bill = this.setInstallments(bill, billVo, billsPayableLedger, paymentToAccount);

		id = (Long) this.sessionFactory.getCurrentSession().save(bill);
		Set<ReceivingNote> notes = bill.getReceivingNote();
		if (notes != null) {
			for (ReceivingNote note : notes) {
				note.setBill(bill);
			}
			this.sessionFactory.getCurrentSession().merge(bill);
		}
		if (!exists) {
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.JOURNAL, bill.getCompany());
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.BILL, bill.getCompany());
		}
		return id;
	}

	private Bill setInstallments(Bill bill, BillVo billVo, Ledger byLedger, Ledger toLedger) {
		bill.clearInstallments();
		Boolean isSettled = true;
		if (billVo.getIsSettled()) {
			for (BillInstallmentVo installmentVo : billVo.getInstallmentVos()) {
				BillInstallments billInstallments = this.findBillInstallment(installmentVo.getInstallmentId());
				if (billInstallments == null)
					billInstallments = new BillInstallments();
				billInstallments.setIsPaid(true);
				billInstallments.setAmount(installmentVo.getAmount());
				billInstallments.setDate(DateFormatter.convertStringToDate(installmentVo.getDate()));
				billInstallments.setPercentage(installmentVo.getPercentage());
				billInstallments.setBill(bill);
				billInstallments.setPayment(this.setAttributesToPayment(bill, billInstallments, installmentVo,
						byLedger, toLedger));
				bill.getBillInstallments().add(billInstallments);
			}
		} else {
			for (BillInstallmentVo installmentVo : billVo.getInstallmentVos()) {
				BillInstallments billInstallments = this.findBillInstallment(installmentVo.getInstallmentId());
				if (billInstallments == null)
					billInstallments = new BillInstallments();
				billInstallments.setDate(DateFormatter.convertStringToDate(installmentVo.getDate()));
				if (installmentVo.getIsPaid()) {
					billInstallments.setIsPaid(true);
					billInstallments.setPayment(this.setAttributesToPayment(bill, billInstallments, installmentVo,
							byLedger, toLedger));
				} else {
					isSettled = false;
					billInstallments.setIsPaid(false);
				}
				billInstallments.setAmount(installmentVo.getAmount());
				billInstallments.setPercentage(installmentVo.getPercentage());
				billInstallments.setBill(bill);
				bill.getBillInstallments().add(billInstallments);
			}
		}
		bill.setIsSettled(isSettled);
		return bill;
	}

	private Payment setAttributesToPayment(Bill bill, BillInstallments billInstallments,
			BillInstallmentVo billInstallmentVo, Ledger byLedger, Ledger toLedger) {
		ErpCurrencyVo currencyVo = currencyDao
				.findCurrencyForDate(bill.getDate(), bill.getCurrency().getBaseCurrency());
		if (currencyVo != null) {
			billInstallmentVo.setAmount(billInstallmentVo.getAmount().divide(currencyVo.getRate(), 2,
					RoundingMode.HALF_EVEN));
		}
		Payment payment = new Payment();
		payment.setCompany(bill.getCompany());
		payment.setPaymentDate(billInstallments.getDate());
		payment.setPaymentNumber(paymentDao.getPaymentNumberForCompany(bill.getCompany().getId()));
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PAYMENT, bill.getCompany());
		PaymentVo paymentVo = new PaymentVo();
		paymentVo.setDate(billInstallmentVo.getDate());
		paymentVo.setAmount(billInstallmentVo.getAmount());
		paymentVo.setNarration("Payment for Bill Installment " + bill.getVoucherNumber());
		PaymentItemsVo byItem = new PaymentItemsVo();
		PaymentItemsVo toItem = new PaymentItemsVo();
		byItem.setAccTypeId(byLedger.getId());
		byItem.setDebitAmnt(billInstallmentVo.getAmount());
		toItem.setCreditAmnt(billInstallmentVo.getAmount());
		toItem.setAccTypeId(toLedger.getId());
		paymentVo.getEntries().add(toItem);
		paymentVo.getEntries().add(byItem);
		payment.setIsDependant(true);
		payment = paymentDao.setAttributesForPayment(payment, paymentVo, false);
		payment.setBillInstallments(billInstallments);
		return payment;
	}

	/**
	 * method to set attributes to bill
	 * 
	 * @param bill
	 * @param billVo
	 * @return
	 */
	private Bill setAttributesToBill(Bill bill, BillVo billVo) {
		bill.getVat().clear();
		bill.setIsArchive(false);
		bill.setNetAmount(billVo.getNetAmountHidden());
		bill.setSubTotal(billVo.getSubTotal());
		bill.setDiscount(billVo.getDiscountRate());
		bill.setDiscountAmount(billVo.getDiscountTotal());
		bill.setNetTotal(billVo.getNetTotal());
		bill.setTaxTotal(billVo.getTaxTotal());
		bill.setGrandTotal(billVo.getGrandTotal());
		List<VAT> taxes = new ArrayList<VAT>();
		for (Long id : billVo.getTsxIds()) {
			VAT vat = vatDao.findTaxObject(id);
			taxes.add(vat);
		}
		bill.getVat().addAll(taxes);
		return bill;
	}

	/**
	 * method to set products to bill
	 * 
	 * @param bill
	 * @param billVo
	 * @return
	 */
	private Bill setProductsToBill(Bill bill, BillVo billVo) {
		Set<BillItems> oldItems = bill.getBillItems();
		Map<Product, BillItems> oldItemsMap = new HashMap<Product, BillItems>();
		for (BillItems billItem : oldItems)
			oldItemsMap.put(billItem.getProduct(), billItem);
		bill.clearItems();
		List<BillItems> billItems = new ArrayList<BillItems>();
		for (BillProductVo productVo : billVo.getProductVos()) {
			BillItems item = new BillItems();
			Product product = productDao.findProduct(productVo.getProductCode(), bill.getCompany().getId());
			BillItems oldItem = oldItemsMap.get(product);
			item.setProduct(product);
			Company company = bill.getCompany();
			item.setAmountExcludingTax(productVo.getAmountExcludingTax());
			item.setQuantity(productVo.getQuantity());
			item.setDiscountPercentage(productVo.getDiscount());
			item.setPrice(productVo.getUnitPrice());
			item.setNetAmount(productVo.getNetAmount());
			if (productVo.getTaxId() != null) {
				VAT vat = vatDao.findTaxObject(productVo.getTaxId());
				if (vat == null)
					throw new ItemNotFoundException("Tax Not Found");
				item.setVat(vat);
				vat.getBillItems().add(item);
			}
			item.setBill(bill);

			Godown godown = godownDao.findGodownObject(productVo.getGodownId());
			if (godown == null)
				throw new ItemNotFoundException("Godown not found.");
			// set inventory entry
			InventoryEntry entry = new InventoryEntry();
			InventoryEntryVo entryVo = new InventoryEntryVo();
			entryVo.setDate(billVo.getBillDate());
			InventoryEntryItemsVo entryItemsVo = new InventoryEntryItemsVo();
			entryItemsVo.setProductCode(productVo.getProductCode());
			entryItemsVo.setUnitPrice(productVo.getUnitPrice());
			entryItemsVo.setTotal(productVo.getNetAmount());
			entryItemsVo.setQuantity(productVo.getQuantity());
			entryVo.getItemsVos().add(entryItemsVo);
			entryVo.setNarration("Inventory Entry For Bill " + bill.getVoucherNumber());
			entry.setCode(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.INVENTORY_ENTRY, company.getId()));
			entry.setCompany(company);
			entry.setType("Receipt");
			entry.setGodown(godown);
			entry = inventoryEntryDao.setAttributesToInventoryEntry(entry, entryVo, false);
			entry.setBillItems(item);
			item.setInventoryEntry(entry);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.INVENTORY_ENTRY, company);
			if (oldItem != null) {
				item.setReturnedQuantity(oldItem.getReturnedQuantity());
			} else {
				item.setReturnedQuantity(BigDecimal.ZERO);
			}
			product.getBillItems().add(item);
			billItems.add(item);
		}
		bill.getBillItems().addAll(billItems);
		return bill;
	}

	/**
	 * method to set charges to bill
	 * 
	 * @param bill
	 * @param billVo
	 * @return
	 */
	private Bill setChargesToBill(Bill bill, BillVo billVo) {
		bill.clearCharges();
		Set<BillCharges> billCharges = new HashSet<BillCharges>();
		for (BillChargeVo chargeVo : billVo.getChargeVos()) {
			BillCharges charges = new BillCharges();
			Charge charge = chargeDao.findCharge(chargeVo.getChargeId());
			charges.setCharge(charge);
			charges.setAmount(chargeVo.getAmount());
			charge.getBillCharges().add(charges);
			charges.setBill(bill);
			billCharges.add(charges);
		}
		bill.getBillCharges().addAll(billCharges);
		return bill;
	}

	/**
	 * method to set journal to bill
	 * 
	 * @param bill
	 * @param billVo
	 * @param byLedger
	 * @param toLedger
	 * @return
	 */
	private Bill setJournalToBill(Bill bill, BillVo billVo, Ledger byLedger, Ledger toLedger, Ledger taxReceivable,
			Ledger discountReceived) {
		ErpCurrencyVo currencyVo = currencyDao
				.findCurrencyForDate(bill.getDate(), bill.getCurrency().getBaseCurrency());
		BigDecimal grandTotal = billVo.getGrandTotal();
		if (currencyVo != null) {
			grandTotal = grandTotal.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
			billVo.setNetAmountHidden(billVo.getNetAmountHidden().divide(currencyVo.getRate(), 2,
					RoundingMode.HALF_EVEN));
			billVo.setProductTaxTotal(billVo.getProductTaxTotal().divide(currencyVo.getRate(), 2,
					RoundingMode.HALF_EVEN));
			billVo.setDiscountTotal(billVo.getDiscountTotal().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
		}
		Journal journal = bill.getJournal();
		if (journal == null) {
			journal = new Journal();
			journal.setJournalNumber(journalDao.getJournalNumberForCompany(bill.getCompany().getId()));
			journal.setCompany(bill.getCompany());
			journal.setIsDependant(true);
		} else {
			journal.getEntries().clear();
		}

		JournalVo journalVo = new JournalVo();
		journalVo.setDate(billVo.getBillDate());
		journalVo.setAmount(grandTotal.add(billVo.getDiscountTotal()));
		journalVo.setNarration("Journal for Bill " + bill.getVoucherNumber());
		JournalItemsVo byItem = new JournalItemsVo();
		byItem.setAccTypeId(byLedger.getId());
		byItem.setDebitAmnt(billVo.getNetAmountHidden());
		journalVo.getEntries().add(byItem);

		Set<VAT> vats = bill.getVat();
		Ledger taxLedger = null;
		if (billVo.getProductTaxTotal().compareTo(BigDecimal.ZERO) != 0) {
			for (VAT vat : vats) {
				taxLedger = vat.getLedger();
				JournalItemsVo taxByItem = new JournalItemsVo();
				taxByItem.setAccTypeId(taxLedger.getId());
				taxByItem.setDebitAmnt(billVo.getNetTotal().multiply(vat.getPercentage())
						.divide(new BigDecimal(100), 2, RoundingMode.HALF_EVEN));
				journalVo.getEntries().add(taxByItem);
			}
		}

		/*
		 * if (billVo.getProductTaxTotal().compareTo(BigDecimal.ZERO) != 0) {
		 * JournalItemsVo taxByItem = new JournalItemsVo();
		 * taxByItem.setAccTypeId(taxReceivable.getId());
		 * taxByItem.setDebitAmnt(billVo.getProductTaxTotal());
		 * journalVo.getEntries().add(taxByItem); }
		 */
		if (billVo.getDiscountTotal().compareTo(BigDecimal.ZERO) != 0) {
			JournalItemsVo discount = new JournalItemsVo();
			discount.setAccTypeId(discountReceived.getId());
			discount.setCreditAmnt(billVo.getDiscountTotal());
			journalVo.getEntries().add(discount);
		}
		
		JournalItemsVo toItem = new JournalItemsVo();
		toItem.setAccTypeId(toLedger.getId());
		toItem.setCreditAmnt(grandTotal);
		journalVo.getEntries().add(toItem);

		
		journal = journalDao.setAttributesForJournal(journal, journalVo, false);

		journal.setBill(bill);
		bill.setJournal(journal);
		return bill;
	}

	public void deleteBill(Long id) {
		Bill bill = findBillObject(id);
		if (bill == null)
			throw new ItemNotFoundException("Bill Not Found");
		/*
		 * Boolean delete = true; List<String> payments = new
		 * ArrayList<String>(); for (BillInstallments billInstallments :
		 * bill.getBillInstallments()) { if (billInstallments.getIsPaid()) {
		 * payments.add(billInstallments.getPayment().getPaymentNumber());
		 * delete = false; } } if (!delete) throw new ItemNotFoundException(
		 * "Please Delete Payment(s) -> " + payments);
		 * bill.getJournal().setIsArchive(true); bill.setIsArchive(true);
		 * this.sessionFactory.getCurrentSession().merge(bill);
		 */
		Set<ReceivingNote> receivingNote = bill.getReceivingNote();

		if (receivingNote != null) {
			for (ReceivingNote note : receivingNote) {
				note.setBill(null);
				this.sessionFactory.getCurrentSession().merge(note);
			}
		}

		this.sessionFactory.getCurrentSession().delete(bill);
	}

	public BillVo findBill(Long id) {
		Bill bill = this.findBillObject(id);
		if (bill == null)
			throw new ItemNotFoundException("Bill Not Found.");
		BillVo vo = new BillVo();
		return createBillVo(vo, bill);
	}

	private BillVo createBillVo(BillVo vo, Bill bill) {
		Company company = bill.getCompany();
		if (company != null)
			vo.setCompanyId(company.getId());
		vo.setBillId(bill.getId());
		vo.setBillCode(bill.getVoucherNumber());
		Vendor vendor = bill.getVendor();
		Ledger ledger = bill.getLedger();
		vo.setAccount(ledger.getLedgerName());
		vo.setLedgerId(ledger.getId());
		vo.setVendor(vendor.getName());
		vo.setVendorId(vendor.getId());
		vo.setBillDate(DateFormatter.convertDateToString(bill.getDate()));
		vo.setIsSettled(bill.getIsSettled());
		if (bill.getIsSettled())
			vo.setIsEdit(true);
		PaymentTerms paymentTerm = bill.getPaymentTerms();
		vo.setPaymentTerm(paymentTerm.getTerm());
		vo.setPaymentTermId(paymentTerm.getId());
		DeliveryMethod deliveryMethod = bill.getDeliveryTerm();
		if (deliveryMethod != null) {
			vo.setDeliveryTerm(deliveryMethod.getMethod());
			vo.setDeliveryTermId(deliveryMethod.getId());
		}
		ErPCurrency currency = bill.getCurrency();
		vo.setErpCurrencyId(currency.getId());
		vo.setCurrecny(currency.getBaseCurrency().getBaseCurrency());
		vo.setIsEdit(true);
		vo.setSubTotal(bill.getSubTotal());
		vo.setDiscountRate(bill.getDiscount());
		vo.setDiscountTotal(bill.getDiscountAmount());
		vo.setNetTotal(bill.getNetTotal());
		vo.setTaxTotal(bill.getTaxTotal());
		vo.setGrandTotal(bill.getGrandTotal());
		Set<VAT> taxes = bill.getVat();
		for (VAT tax : taxes) {
			vo.getTaxes().add(tax.getName());
			vo.getTaxesWithRate().add(tax.getId() + "!!!" + tax.getPercentage());
		}
		vo = this.createBillProductVo(vo, bill);
		vo = this.createBillChargeVo(vo, bill);
		vo = this.createBillInstallmentVo(vo, bill);
		return vo;
	}

	private BillVo createBillProductVo(BillVo vo, Bill bill) {
		List<BillProductVo> productVos = new ArrayList<BillProductVo>();
		BigDecimal taxAmount = new BigDecimal("0.00");
		Boolean isReturned = true;
		for (BillItems items : bill.getBillItems()) {
			BillProductVo productVo = new BillProductVo();
			Product product = items.getProduct();
			productVo.setAmountExcludingTax(items.getAmountExcludingTax());
			vo.setNetAmountHidden(vo.getNetAmountHidden().add(items.getAmountExcludingTax()));
			productVo.setProductCode(product.getProductCode());
			productVo.setProductName(product.getName());
			productVo.setProductId(product.getId());
			productVo.setBillProductId(items.getId());
			taxAmount = taxAmount.add(items.getNetAmount().subtract(items.getAmountExcludingTax()));
			productVo.setQuantity(items.getQuantity());
			productVo.setUnitPrice(items.getPrice());
			productVo.setReturnedQuantity(items.getReturnedQuantity());
			BigDecimal quantityToReturn = items.getQuantity().subtract(items.getReturnedQuantity());
			productVo.setQuantityToReturn(quantityToReturn);
			if (quantityToReturn.compareTo(BigDecimal.ZERO) == 0)
				productVo.setIsReturned(true);
			else
				isReturned = false;
			productVo.setDiscount(items.getDiscountPercentage());
			productVo.setNetAmount(items.getNetAmount());
			VAT tax = items.getVat();
			if (tax != null) {
				productVo.setTax(tax.getName());
			}
			InventoryEntry entry = items.getInventoryEntry();
			Set<InventoryEntryItems> entryItems = entry.getInventoryEntryItems();
			InventoryEntryItems entryItem = null;
			for (InventoryEntryItems item : entryItems) {
				entryItem = item;
			}
			Godown godown = entryItem.getTransaction().getGodown();
			productVo.setGodownId(godown.getId());
			productVo.setGodown(godown.getName());
			productVos.add(productVo);
		}
		vo.setIsReturned(isReturned);
		vo.setProductsTaxAmount(taxAmount);
		vo.setProductVos(productVos);
		return vo;
	}

	private BillVo createBillChargeVo(BillVo vo, Bill bill) {
		List<BillChargeVo> chargeVos = new ArrayList<BillChargeVo>();
		for (BillCharges charge : bill.getBillCharges()) {
			BillChargeVo chargeVo = new BillChargeVo();
			Charge chargeObj = charge.getCharge();
			chargeVo.setChargeId(chargeObj.getId());
			chargeVo.setBillChargeId(charge.getId());
			chargeVo.setCharge(charge.getCharge().getName());
			chargeVo.setAmount(charge.getAmount());
			chargeVos.add(chargeVo);
		}
		vo.setChargeVos(chargeVos);
		return vo;
	}

	private BillVo createBillInstallmentVo(BillVo vo, Bill bill) {
		Boolean settled = false;
		BigDecimal outStanding = new BigDecimal("0.00");
		List<BillInstallmentVo> installmentVos = new ArrayList<BillInstallmentVo>();
		for (BillInstallments installment : bill.getBillInstallments()) {
			if (!installment.getIsPaid()) {
				outStanding = outStanding.add(installment.getAmount());
			} else
				settled = true;
			BillInstallmentVo installmentVo = new BillInstallmentVo();
			installmentVo.setId(installment.getId());
			installmentVo.setDate(DateFormatter.convertDateToString(installment.getDate()));
			installmentVo.setAmount(installment.getAmount());
			installmentVo.setPercentage(installment.getPercentage());
			installmentVo.setIsPaid(installment.getIsPaid());
			installmentVo.setInstallmentId(installment.getId());
			installmentVos.add(installmentVo);
		}
		vo.setIsEdit(settled);
		vo.setOutStanding(outStanding);
		vo.setInstallmentVos(installmentVos);
		return vo;
	}

	public Bill findBillObject(Long id) {
		return (Bill) this.sessionFactory.getCurrentSession().createCriteria(Bill.class).add(Restrictions.eq("id", id))
				.add(Restrictions.eq("isArchive", false)).uniqueResult();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<BillVo> listBillsForCompany(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Bill.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("company", company)).add(Restrictions.eq("isArchive", false));
		List<Bill> bills = criteria.list();
		List<BillVo> billVos = new ArrayList<BillVo>();
		for (Bill bill : bills) {
			BillVo vo = new BillVo();
			billVos.add(createBillVo(vo, bill));
		}
		return billVos;
	}

	public String generateBillCode(String item, Long companyId) {
		NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(NumberPropertyConstants.BILL,
				companyId);
		return numberPropertyVo.getPrefix() + String.valueOf(numberPropertyVo.getNumber());
	}

	public void updateBillInstallment(Long installmentId) {
		BillInstallments installment = this.findBillInstallment(installmentId);
		if (installment == null)
			throw new ItemNotFoundException("Installment Not Found.");
		Bill bill = installment.getBill();
		Company company = bill.getCompany();
		Ledger byLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.BILLS_PAYABLE_LEDGER, company.getId());
		Ledger toLedger = bill.getLedger();
		BillInstallmentVo billInstallmentVo = new BillInstallmentVo();
		billInstallmentVo.setAmount(installment.getAmount());
		installment.setIsPaid(true);
		billInstallmentVo.setDate(DateFormatter.convertDateToString(installment.getDate()));
		installment.setPayment(this.setAttributesToPayment(bill, installment, billInstallmentVo, byLedger, toLedger));
		bill.getBillInstallments().add(installment);
		Boolean isSettled = true;
		for (BillInstallments installments : bill.getBillInstallments()) {
			if (!installments.getIsPaid())
				isSettled = false;
		}
		bill.setIsSettled(isSettled);
		this.sessionFactory.getCurrentSession().merge(bill);
	}

	private BillInstallments findBillInstallment(Long installmentId) {
		return (BillInstallments) this.sessionFactory.getCurrentSession().createCriteria(BillInstallments.class)
				.add(Restrictions.eq("id", installmentId)).uniqueResult();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<BillVo> listBillsByVendor(Long vendorId) {
		Vendor vendor = vendorDao.findVendor(vendorId);
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Not Found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Bill.class);
		criteria.add(Restrictions.eq("vendor", vendor));
		List<Bill> bills = criteria.list();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<BillVo> billVos = new ArrayList<BillVo>();
		for (Bill bill : bills) {
			BillVo billVo = new BillVo();
			billVos.add(this.createBillVo(billVo, bill));
		}
		return billVos;
	}

}
