package com.purchase.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.NumberProperty;
import com.erp.web.entities.StockGroup;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.purchase.web.dao.StockGroupDao;
import com.purchase.web.vo.StockGroupVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 23, 2015
 */
@Repository
public class StockGroupDaoImpl extends AbstractDao implements StockGroupDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	public void saveOrUpdateStockGroup(StockGroupVo groupVo) {
		groupVo.setGroupName(groupVo.getGroupName().trim());
		groupVo.setGroupName(groupVo.getGroupName().trim());
		StockGroup group = findStockGroupsObject(groupVo.getId());
		StockGroup parentGroup = findStockGroupsObject(groupVo.getParentGroupId());
		Company company = companyDao.findCompany(groupVo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");

		StockGroup groupCompare = this.findStockGroupByNameAndParentGroup(groupVo.getGroupName(), parentGroup, company);
		if (group == null) {

			if (groupCompare != null)
				throw new DuplicateItemException("Stock Group Already Exists");
			group = new StockGroup();
			NumberProperty numberProperty = new NumberProperty();
			String prefix = "";
			numberProperty.setCompany(company);
			if (groupVo.getGroupName().length() > 3)
				prefix = groupVo.getGroupName().substring(0, 3);
			else
				prefix = groupVo.getGroupName();
			NumberProperty numberPropertyWithSamePrefix = numberPropertyDao.findNumberProperty(company, prefix);
			int i = 4;
			while (numberPropertyWithSamePrefix != null) {
				if (groupVo.getGroupName().length() > i)
					prefix = groupVo.getGroupName().substring(0, i);
				else
					prefix = prefix + "0";
				numberPropertyWithSamePrefix = numberPropertyDao.findNumberProperty(company, prefix);
				i++;
			}
			numberProperty.setPrefix(prefix);
			numberProperty.setNumber(1L);
			numberProperty.setCategory(groupVo.getGroupName());
			numberProperty.setStockGroup(group);
			group.setNumberProperty(numberProperty);
			group.setGroupName(groupVo.getGroupName());
			group.setCompany(company);
			group.setParentGroup(parentGroup);
		} else {
			if (groupCompare != null && !group.equals(groupCompare))
				throw new DuplicateItemException("Stock Group Already Exists");
			group.setGroupName(groupVo.getGroupName());
			NumberProperty numberProperty = group.getNumberProperty();
			numberProperty.setCategory(group.getGroupName());
			String prefix = "";
			numberProperty.setCompany(company);
			if (groupVo.getGroupName().length() > 3)
				prefix = groupVo.getGroupName().substring(0, 3);
			else
				prefix = groupVo.getGroupName();
			NumberProperty numberPropertyWithSamePrefix = numberPropertyDao.findNumberProperty(company, prefix);
			int i = 4;
			while (numberPropertyWithSamePrefix != null) {
				if (groupVo.getGroupName().length() > i)
					prefix = groupVo.getGroupName().substring(0, i);
				else
					prefix = prefix + "0";
				numberPropertyWithSamePrefix = numberPropertyDao.findNumberProperty(company, prefix);
				i++;
			}
			numberProperty.setPrefix(prefix);
			group.setNumberProperty(numberProperty);
			group.setCompany(company);
			group.setParentGroup(parentGroup);
		}

		if (parentGroup != null) {
			parentGroup.getSubGroups().add(group);
			this.sessionFactory.getCurrentSession().saveOrUpdate(group);
			this.sessionFactory.getCurrentSession().saveOrUpdate(parentGroup);
		} else {
			this.sessionFactory.getCurrentSession().saveOrUpdate(group);
		}

	}

	private StockGroup findStockGroupByNameAndParentGroup(String stockGroup, StockGroup parentGroup, Company company) {
		StockGroup group = null;

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(StockGroup.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("groupName", stockGroup));

		if (parentGroup == null)
			criteria.add(Restrictions.isNull("parentGroup"));
		else
			criteria.add(Restrictions.eq("parentGroup", parentGroup));
		group = (StockGroup) criteria.uniqueResult();
		return group;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.purchase.web.dao.StockGroupDao#deleteStockGroup(java.lang.Long)
	 */
	public void deleteStockGroup(Long id) {
		StockGroup group = findStockGroupsObject(id);
		if (group == null)
			throw new ItemNotFoundException("Stock Group Not Found");
		else {

			/*
			 * if(!group.getProducts().isEmpty()) throw new
			 * ItemNotFoundException("Stock Group Is Not Empty");
			 */
			StockGroup parentGroup = group.getParentGroup();
			if (parentGroup == null)
				this.sessionFactory.getCurrentSession().delete(group);
			else {
				parentGroup.getSubGroups().remove(group);
				this.sessionFactory.getCurrentSession().delete(group);
				this.sessionFactory.getCurrentSession().merge(parentGroup);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.purchase.web.dao.StockGroupDao#findStockGroupsObject(java.lang.Long)
	 */
	public StockGroup findStockGroupsObject(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(StockGroup.class);
		criteria.add(Restrictions.eq("id", id));
		return (StockGroup) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.purchase.web.dao.StockGroupDao#findStockGroup(java.lang.Long)
	 */
	public StockGroupVo findStockGroup(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	private StockGroupVo createVo(StockGroup group) {
		StockGroupVo vo = new StockGroupVo();
		vo.setGroupName(group.getGroupName());
		vo.setId(group.getId());
		vo.setCompanyId(group.getCompany().getId());
		StockGroup parentGroup = group.getParentGroup();
		if (parentGroup != null) {
			vo.setParentGroupId(parentGroup.getId());
			vo.setParentGroup(parentGroup.getGroupName());
		}

		vo.setSubGroups(getSetOfStockGroupVos(group.getSubGroups()));
		vo.setSubGroupIds(getSubGroupIds(group.getSubGroups()));
		return vo;

	}

	private Set<StockGroupVo> getSetOfStockGroupVos(Set<StockGroup> stockGroups) {
		Set<StockGroupVo> vos = new HashSet<StockGroupVo>();
		for (StockGroup group : stockGroups) {
			vos.add(createVo(group));
		}
		return vos;
	}

	private Set<Long> getSubGroupIds(Set<StockGroup> stockGroups) {
		Set<Long> vos = new HashSet<Long>();
		for (StockGroup group : stockGroups) {
			vos.add(group.getId());
		}
		return vos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.purchase.web.dao.StockGroupDao#listStockGroupsForCompany(java.lang.
	 * Long)
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<StockGroupVo> listStockGroupsForCompany(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company not found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(StockGroup.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<StockGroup> groups = criteria.list();
		List<StockGroupVo> vos = new ArrayList<StockGroupVo>();
		for (StockGroup group : groups) {
			vos.add(createVo(group));
		}
		return vos;
	}

	public List<StockGroup> findStockGroupWithNoParent(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company not found");
		return findStockGroupWithNoParent(company);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<StockGroup> findStockGroupWithNoParent(Company company) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(StockGroup.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.isNull("parentGroup"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

}
