package com.purchase.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.JournalDao;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.ReceiptDao;
import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.Receipts;
import com.accounts.web.vo.JournalItemsVo;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.ReceiptItemsVo;
import com.accounts.web.vo.ReceiptVo;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.Godown;
import com.erp.web.vo.ErpCurrencyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.dao.GodownDao;
import com.inventory.web.dao.InventoryWithdrawalDao;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.Product;
import com.inventory.web.vo.InventoryWithdrawalItemsVo;
import com.inventory.web.vo.InventoryWithdrawalVo;
import com.purchase.web.dao.BillDao;
import com.purchase.web.dao.PurchaseReturnDao;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.BillInstallments;
import com.purchase.web.entities.BillItems;
import com.purchase.web.entities.PurchaseReturn;
import com.purchase.web.entities.PurchaseReturnItems;
import com.purchase.web.entities.Vendor;
import com.purchase.web.vo.PurchaseReturnItemsVo;
import com.purchase.web.vo.PurchaseReturnVo;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.InvoiceInstallments;
import com.sales.web.vo.InvoiceInstallmentVo;
import com.sales.web.vo.SalesReturnItemsVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 11, 2015
 */
@Repository
public class PurchaseReturnDaoImpl extends AbstractDao implements PurchaseReturnDao {

	@Autowired
	private BillDao billDao;

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private InventoryWithdrawalDao withdrawalDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private GodownDao godownDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private ErpCurrencyDao currencyDao;

	@Autowired
	private ReceiptDao receiptDao;

	@Autowired
	private JournalDao journalDao;

	public Long savePurchaseReturn(PurchaseReturnVo vo) {
		Long id = vo.getReturnId();
		PurchaseReturn purchaseReturn = this.findPurchaseReturn(vo.getReturnId());
		Bill bill = null;
		Boolean isNew = false;
		Company company = null;
		if (purchaseReturn == null) {
			bill = billDao.findBillObject(vo.getBillId());
			if (bill == null)
				throw new ItemNotFoundException("Bill Not Found");
			company = bill.getCompany();
			purchaseReturn = new PurchaseReturn();
			isNew = true;
			purchaseReturn.setCompany(company);
			purchaseReturn.setBill(bill);
			purchaseReturn.setReferenceNumber(numberPropertyDao.getRefernceNumber(
					NumberPropertyConstants.PURCHASE_RETURN, company));
		} else {
			bill = purchaseReturn.getBill();
			company = bill.getCompany();
		}
		Ledger purchaseReturnLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.PURCHASE_RETURN_LEDGER,
				company.getId());
		if (bill.getIsSettled()) {
			purchaseReturn = this.setAccountTransactionForCashPurchase(purchaseReturn, vo, purchaseReturnLedger);
		} else {
			purchaseReturn = this.setAccountTransactionForCreditPurchase(purchaseReturn, vo, purchaseReturnLedger);
		}

		purchaseReturn.setReturnDate(DateFormatter.convertStringToDate(vo.getDate()));
		purchaseReturn.setVendor(bill.getVendor());
		purchaseReturn = this.setProductsToReturn(purchaseReturn, vo, bill, company);
		if (isNew) {
			id = (Long) this.sessionFactory.getCurrentSession().save(purchaseReturn);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PURCHASE_RETURN, company);
			bill.getPurchaseReturns().add(purchaseReturn);
			this.sessionFactory.getCurrentSession().merge(bill);
		} else {
			bill.getPurchaseReturns().add(purchaseReturn);
			this.sessionFactory.getCurrentSession().merge(bill);
		}
		return id;
	}

	private PurchaseReturn setAccountTransactionForCreditPurchase(PurchaseReturn purchaseReturn, PurchaseReturnVo vo,
			Ledger returnLedger) {
		BigDecimal totalPaid = BigDecimal.ZERO;
		BigDecimal pendingAmount = BigDecimal.ZERO;
		for (BillInstallments installment : purchaseReturn.getBill().getBillInstallments()) {
			if (installment.getIsPaid()) {
				totalPaid = totalPaid.add(installment.getAmount());
			} else {
				pendingAmount = pendingAmount.add(installment.getAmount());
			}
		}

		// Receipt cash a/c Dr to Purchase Return
		Company company = purchaseReturn.getCompany();
		BigDecimal total = BigDecimal.ZERO;
		for (PurchaseReturnItemsVo itemsVo : vo.getProductVos()) {
			total = total.add(itemsVo.getTotal());
		}
		ErpCurrencyVo currencyVo = currencyDao.findCurrencyForDate(purchaseReturn.getReturnDate(), purchaseReturn
				.getBill().getCurrency().getBaseCurrency());
		if (currencyVo != null) {
			total = total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
		}

		BigDecimal journalAmount = total.subtract(totalPaid);
		if (totalPaid.compareTo(total) != -1) {
			Receipts receipts = purchaseReturn.getReceipt();
			if (receipts == null)
				receipts = new Receipts();
			else
				receipts.getEntries().clear();
			receipts.setCompany(company);
			receipts.setReceiptDate(purchaseReturn.getReturnDate());
			receipts.setReceiptNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.RECEIPT,
					company.getId()));
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.RECEIPT, company);
			ReceiptVo receiptVo = new ReceiptVo();
			receiptVo.setDate(vo.getDate());
			receiptVo.setAmount(total);
			ReceiptItemsVo byItem = new ReceiptItemsVo();
			ReceiptItemsVo toItem = new ReceiptItemsVo();
			Ledger byLedger = purchaseReturn.getBill().getLedger();
			byItem.setAccTypeId(byLedger.getId());
			byItem.setDebitAmnt(total);
			toItem.setCreditAmnt(total);
			toItem.setAccTypeId(returnLedger.getId());
			receiptVo.getEntries().add(toItem);
			receiptVo.getEntries().add(byItem);
			receiptVo.setNarration("Receipt For Purchase Return " + purchaseReturn.getReferenceNumber());
			receipts.setIsDependant(true);
			receipts = receiptDao.setAttributesForReceipt(receipts, receiptVo, false);
			receipts.setPurchaseReturn(purchaseReturn);
			purchaseReturn.setReceipt(receipts);
		} else {
			if (totalPaid.compareTo(BigDecimal.ZERO) != 0) {
				Receipts receipts = purchaseReturn.getReceipt();
				if (receipts == null)
					receipts = new Receipts();
				else
					receipts.getEntries().clear();
				receipts.setCompany(company);
				receipts.setReceiptDate(purchaseReturn.getReturnDate());
				receipts.setReceiptNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.RECEIPT,
						company.getId()));
				numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.RECEIPT, company);
				ReceiptVo receiptVo = new ReceiptVo();
				receiptVo.setDate(vo.getDate());
				receiptVo.setAmount(totalPaid);
				ReceiptItemsVo byItem = new ReceiptItemsVo();
				ReceiptItemsVo toItem = new ReceiptItemsVo();
				Ledger byLedger = purchaseReturn.getBill().getLedger();
				byItem.setAccTypeId(byLedger.getId());
				byItem.setDebitAmnt(totalPaid);
				toItem.setCreditAmnt(totalPaid);
				toItem.setAccTypeId(returnLedger.getId());
				receiptVo.getEntries().add(toItem);
				receiptVo.getEntries().add(byItem);
				receiptVo.setNarration("Receipt For Purchase Return " + purchaseReturn.getReferenceNumber());
				receipts.setIsDependant(true);
				receipts = receiptDao.setAttributesForReceipt(receipts, receiptVo, false);
				receipts.setPurchaseReturn(purchaseReturn);
				purchaseReturn.setReceipt(receipts);
			}
			if (journalAmount.compareTo(BigDecimal.ZERO) != 0 && journalAmount.compareTo(BigDecimal.ZERO) != -1) {
				Ledger byLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.BILLS_PAYABLE_LEDGER,
						company.getId());
				Journal journal = purchaseReturn.getJournal();
				if (journal == null)
					journal = new Journal();
				else
					journal.getEntries().clear();
				journal.setCompany(company);
				journal.setJournalDate(purchaseReturn.getReturnDate());
				journal.setJournalNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.JOURNAL,
						company.getId()));
				numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.JOURNAL, company);
				JournalVo journalVo = new JournalVo();
				journalVo.setDate(vo.getDate());
				journalVo.setAmount(journalAmount);
				JournalItemsVo byItem = new JournalItemsVo();
				JournalItemsVo toItem = new JournalItemsVo();
				byItem.setAccTypeId(byLedger.getId());
				byItem.setDebitAmnt(journalAmount);
				toItem.setCreditAmnt(journalAmount);
				toItem.setAccTypeId(returnLedger.getId());
				journalVo.getEntries().add(toItem);
				journalVo.getEntries().add(byItem);
				journalVo.setNarration("Journal For Purchase Return " + purchaseReturn.getReferenceNumber());
				journal.setIsDependant(true);
				journal = journalDao.setAttributesForJournal(journal, journalVo, false);
				journal.setPurchaseReturn(purchaseReturn);
				purchaseReturn.setJournal(journal);
			}
		}
		return purchaseReturn;
	}

	private PurchaseReturn setAccountTransactionForCashPurchase(PurchaseReturn purchaseReturn, PurchaseReturnVo vo,
			Ledger returnLedger) {

		// Receipt cash a/c Dr to Purchase Return
		Company company = purchaseReturn.getCompany();
		BigDecimal total = BigDecimal.ZERO;
		for (PurchaseReturnItemsVo itemsVo : vo.getProductVos()) {
			total = total.add(itemsVo.getTotal());
		}
		ErpCurrencyVo currencyVo = currencyDao.findCurrencyForDate(purchaseReturn.getReturnDate(), purchaseReturn
				.getBill().getCurrency().getBaseCurrency());
		if (currencyVo != null) {
			total = total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
		}

		Receipts receipts = purchaseReturn.getReceipt();
		if (receipts == null)
			receipts = new Receipts();
		receipts.setCompany(company);
		receipts.setReceiptDate(purchaseReturn.getReturnDate());
		receipts.setReceiptNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.RECEIPT, company.getId()));
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.RECEIPT, company);
		ReceiptVo receiptVo = new ReceiptVo();
		receiptVo.setDate(vo.getDate());
		receiptVo.setAmount(total);
		ReceiptItemsVo byItem = new ReceiptItemsVo();
		ReceiptItemsVo toItem = new ReceiptItemsVo();
		Ledger byLedger = purchaseReturn.getBill().getLedger();
		byItem.setAccTypeId(byLedger.getId());
		byItem.setDebitAmnt(total);
		toItem.setCreditAmnt(total);
		toItem.setAccTypeId(returnLedger.getId());
		receiptVo.getEntries().add(toItem);
		receiptVo.getEntries().add(byItem);
		receiptVo.setNarration("Receipt For Purchase Return " + purchaseReturn.getReferenceNumber());
		receipts.setIsDependant(true);
		receipts = receiptDao.setAttributesForReceipt(receipts, receiptVo, false);
		receipts.setPurchaseReturn(purchaseReturn);
		purchaseReturn.setReceipt(receipts);
		return purchaseReturn;
	}

	private PurchaseReturn setProductsToReturn(PurchaseReturn purchaseReturn, PurchaseReturnVo vo, Bill bill,
			Company company) {
		Set<PurchaseReturnItems> oldItems = purchaseReturn.getPurchaseReturnItems();
		Map<Product, PurchaseReturnItems> oldItemsMap = new HashMap<Product, PurchaseReturnItems>();
		for (PurchaseReturnItems purchaseReturnItem : oldItems)
			oldItemsMap.put(purchaseReturnItem.getProduct(), purchaseReturnItem);
		purchaseReturn.getPurchaseReturnItems().clear();
		Set<BillItems> billItems = bill.getBillItems();
		Map<Product, BillItems> map = new HashMap<Product, BillItems>();
		for (BillItems billItem : billItems)
			map.put(billItem.getProduct(), billItem);
		BigDecimal total = BigDecimal.ZERO;
		for (PurchaseReturnItemsVo itemsVo : vo.getProductVos()) {
			Product product = productDao.findProduct(itemsVo.getProductCode(), company.getId());
			PurchaseReturnItems item = new PurchaseReturnItems();
			item.setProduct(product);
			Godown godown = godownDao.findGodownObject(itemsVo.getGodownId());
			item.setGodown(godown);
			total = total.add(itemsVo.getTotal());
			item.setQuantity(itemsVo.getQuantity());
			item.setPrice(itemsVo.getUnitPrice());
			item.setTotal(itemsVo.getTotal());

			InventoryWithdrawal withdrawal = new InventoryWithdrawal();
			InventoryWithdrawalVo withdrawalVo = new InventoryWithdrawalVo();
			withdrawalVo.setDate(vo.getDate());
			InventoryWithdrawalItemsVo withdrawalItemsVo = new InventoryWithdrawalItemsVo();
			withdrawalItemsVo.setProductCode(product.getProductCode());
			withdrawalItemsVo.setUnitPrice(itemsVo.getUnitPrice());
			withdrawalItemsVo.setQuantity(itemsVo.getQuantity());
			withdrawalItemsVo.setTotal(item.getTotal());
			withdrawalVo.getItemsVos().add(withdrawalItemsVo);
			withdrawalVo
					.setNarration("Inventory Withdrawal For Purchase Return " + purchaseReturn.getReferenceNumber());
			withdrawal.setCode(withdrawalDao.getNumber(company.getId(), NumberPropertyConstants.INVENTORY_WITHDRAWAL));
			withdrawal.setCompany(company);
			withdrawal.setType("Receipt");
			withdrawal.setGodown(godown);
			withdrawal = withdrawalDao.setAttributesToInventoryWithdrawal(withdrawal, withdrawalVo, false);
			withdrawal.setPurchaseReturnItems(item);

			BillItems billItem = map.get(product);
			PurchaseReturnItems oldItem = oldItemsMap.get(product);
			if (oldItem != null)
				billItem.setReturnedQuantity(billItem.getReturnedQuantity().subtract(oldItem.getQuantity()));

			billItem.setReturnedQuantity(billItem.getReturnedQuantity().add(item.getQuantity()));
			bill.getBillItems().add(billItem);

			item.setInventoryWithdrawal(withdrawal);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.INVENTORY_WITHDRAWAL, company);
			item.setPurchaseReturn(purchaseReturn);
			purchaseReturn.getPurchaseReturnItems().add(item);
		}
		purchaseReturn.setBill(bill);
		return purchaseReturn;
	}

	public PurchaseReturn findPurchaseReturn(Long id) {
		return (PurchaseReturn) this.sessionFactory.getCurrentSession().createCriteria(PurchaseReturn.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public void deletePurchaseReturn(Long id) {
		PurchaseReturn purchaseReturn = this.findPurchaseReturn(id);
		if (purchaseReturn == null)
			throw new ItemNotFoundException("Purchase Return Not Found");
		Bill bill = purchaseReturn.getBill();
		Set<BillItems> billItems = bill.getBillItems();
		Map<Product, BillItems> map = new HashMap<Product, BillItems>();
		for (BillItems billItem : billItems)
			map.put(billItem.getProduct(), billItem);
		for (PurchaseReturnItems pri : purchaseReturn.getPurchaseReturnItems()) {
			BillItems billIt = map.get(pri.getProduct());
			billIt.setReturnedQuantity(billIt.getReturnedQuantity().subtract(pri.getQuantity()));
			bill.getBillItems().add(billIt);
		}
		bill.getPurchaseReturns().remove(purchaseReturn);
		this.sessionFactory.getCurrentSession().merge(bill);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PurchaseReturnVo> findPurchaseReturns(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PurchaseReturn.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PurchaseReturn> purchaseReturns = criteria.list();
		List<PurchaseReturnVo> vos = new ArrayList<PurchaseReturnVo>();
		for (PurchaseReturn purchaseReturn : purchaseReturns) {
			vos.add(createPurchaseReturnVo(purchaseReturn));
		}
		return vos;
	}

	public PurchaseReturnVo findPurchaseReturnById(Long id) {
		PurchaseReturn purchaseReturn = this.findPurchaseReturn(id);
		if (purchaseReturn == null)
			throw new ItemNotFoundException("Purchase Return Not Found.");
		return createPurchaseReturnVo(purchaseReturn);
	}

	private PurchaseReturnVo createPurchaseReturnVo(PurchaseReturn purchaseReturn) {
		PurchaseReturnVo vo = new PurchaseReturnVo();
		Bill bill = purchaseReturn.getBill();
		Ledger ledger = bill.getLedger();
		Vendor vendor = purchaseReturn.getVendor();
		ErPCurrency currency = bill.getCurrency();
		vo.setReturnId(purchaseReturn.getId());
		vo.setBillId(bill.getId());
		vo.setCode(purchaseReturn.getReferenceNumber());
		vo.setDate(DateFormatter.convertDateToString(purchaseReturn.getReturnDate()));
		vo.setVendor(vendor.getName());
		vo.setAccount(ledger.getLedgerName());
		vo.setCurrecny(currency.getBaseCurrency().getBaseCurrency());
		vo.setIsSettled(bill.getIsSettled());

		Set<BillItems> billItems = bill.getBillItems();
		Map<Product, BillItems> map = new HashMap<Product, BillItems>();
		for (BillItems billItem : billItems)
			map.put(billItem.getProduct(), billItem);

		for (PurchaseReturnItems item : purchaseReturn.getPurchaseReturnItems()) {
			PurchaseReturnItemsVo itemsVo = new PurchaseReturnItemsVo();
			Product product = item.getProduct();
			BillItems billItem = map.get(product);
			Godown godown = item.getGodown();
			itemsVo.setProductCode(product.getProductCode());
			itemsVo.setProductName(product.getName());
			itemsVo.setGodown(godown.getName());
			itemsVo.setGodownId(godown.getId());
			itemsVo.setQuantity(item.getQuantity());
			itemsVo.setUnitPrice(item.getPrice());
			itemsVo.setQuantityToReturn(billItem.getQuantity().subtract(billItem.getReturnedQuantity())
					.add(item.getQuantity()));
			itemsVo.setTotal(item.getTotal());
			vo.getProductVos().add(itemsVo);
		}
		return vo;
	}

	public PurchaseReturnVo convertBillToReturn(Long billId) {
		PurchaseReturnVo vo = new PurchaseReturnVo();
		Bill bill = billDao.findBillObject(billId);
		if (bill == null)
			throw new ItemNotFoundException("Bill Not Found");
		Ledger ledger = bill.getLedger();
		Vendor vendor = bill.getVendor();
		ErPCurrency currency = bill.getCurrency();
		vo.setBillId(bill.getId());
		vo.setDate(DateFormatter.convertDateToString(bill.getDate()));
		vo.setVendor(vendor.getName());
		vo.setAccount(ledger.getLedgerName());
		vo.setCurrecny(currency.getBaseCurrency().getBaseCurrency());
		vo.setIsSettled(bill.getIsSettled());
		Boolean isReturned = true;
		for (BillItems item : bill.getBillItems()) {
			BigDecimal quantityToReturn = item.getQuantity().subtract(item.getReturnedQuantity());
			if (quantityToReturn.compareTo(BigDecimal.ZERO) != 0) {
				isReturned = false;
				PurchaseReturnItemsVo itemsVo = new PurchaseReturnItemsVo();
				Product product = item.getProduct();
				InventoryEntry inventoryEntry = item.getInventoryEntry();
				Godown godown = inventoryEntry.getGodown();
				itemsVo.setProductCode(product.getProductCode());
				itemsVo.setProductName(product.getName());
				itemsVo.setGodown(godown.getName());
				itemsVo.setGodownId(godown.getId());
				itemsVo.setQuantity(quantityToReturn);
				itemsVo.setUnitPrice(item.getPrice());
				itemsVo.setQuantityToReturn(quantityToReturn);
				itemsVo.setTotal(quantityToReturn.multiply(item.getPrice()));
				vo.getProductVos().add(itemsVo);
			}
		}
		vo.setIsReturned(isReturned);
		return vo;
	}
}
