package com.purchase.web.dao;

import java.util.List;

import com.erp.web.entities.Company;
import com.erp.web.entities.StockGroup;
import com.purchase.web.vo.StockGroupVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 23, 2015
 */
public interface StockGroupDao {

	/**
	 * Method to save or update stock group
	 * @param groupVo
	 */
	public void saveOrUpdateStockGroup(StockGroupVo groupVo);
	
	/**
	 * Method to delete stock group
	 * @param id
	 */
	public void deleteStockGroup(Long id);

	/**
	 * Method to find stock group object by id
	 * @param id
	 * @return
	 */
	public StockGroup findStockGroupsObject(Long id);
	
	/**
	 * Method to find stock group by id
	 * @param id
	 * @return
	 */
	public StockGroupVo findStockGroup(Long id);
	
	/**
	 * Method to list all stock group for company
	 * @param companyId
	 * @return
	 */
	public List<StockGroupVo> listStockGroupsForCompany(Long companyId);
	
	public List<StockGroup> findStockGroupWithNoParent(Long companyId);
	
	public List<StockGroup> findStockGroupWithNoParent(Company company);
	
}
