package com.purchase.web.dao;

import java.util.List;

import com.purchase.web.entities.VendorQuotes;
import com.purchase.web.vo.VendorQuoteVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 17, 2015
 */
public interface VendorQuoteDao {

	/**
	 * Method to save or update vendor quote
	 * @param quoteVo
	 * @return
	 */
	public Long saveOrUpdateVendorQuote(VendorQuoteVo quoteVo);
	
	/**
	 * Method to save or update purchase order
	 * @param quoteVo
	 * @return
	 */
	public Long saveOrUpdatePurchaseOrder(VendorQuoteVo quoteVo);
	
	/**
	 * Method to delete vendor quote
	 * @param id
	 */
	public void deleteVendorQuote(Long id);
	
	/**
	 * Method to find vendor quote object
	 * @param id
	 * @return
	 */
	public VendorQuotes findVendorQuoteObject(Long id);
	
	/**
	 * Method to find vendor quote
	 * @param id
	 * @return
	 */
	public VendorQuoteVo findVendorQuote(Long id);
	
	/**
	 * Method to list vendor quotes for company
	 * @param companyId
	 * @return
	 */
	public List<VendorQuoteVo> listVendorQuotesForCompany(Long companyId);
	/**
	 * Method to list vendor quotes for company
	 * @param companyId
	 * @return
	 */
	public List<VendorQuoteVo> listPurchaseOrdersForCompany(Long companyId);
	
	/**
	 * Method to update vendor quotes to purchase Order
	 * @param id
	 */
	public void updateVendorQuoteToPurchaseOrder(Long id);
}
