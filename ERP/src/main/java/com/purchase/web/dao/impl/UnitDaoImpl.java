package com.purchase.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.dao.CompanyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.Unit;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.purchase.web.dao.UnitDao;
import com.purchase.web.vo.UnitVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 22, 2015
 */
@Repository
public class UnitDaoImpl extends AbstractDao implements UnitDao {

	@Autowired
	private CompanyDao companyDao;

	public void saveOrUpdateUnit(UnitVo unitVo) {
		unitVo.setName(unitVo.getName().trim());
		Unit unit = null;
		Company company = null;
		if (unitVo.getId() != null) {
			unit = this.findUnit(unitVo.getId());
			Unit unitWithSameNameAndSymbol = this.findUnit(unitVo.getName(), unitVo.getSymbol());
			if (unitWithSameNameAndSymbol != null && !unit.equals(unitWithSameNameAndSymbol)) {
				throw new DuplicateItemException("Unit Already Exists.");
			} else {
				unit = this.setAttributesToUnit(unit, unitVo);
			}
		} else if (this.findUnit(unitVo.getName(), unitVo.getSymbol()) == null) {
			unit = new Unit();
			company = companyDao.findCompany(unitVo.getCompanyId());
			if (company == null)
				throw new ItemNotFoundException("Company Not Exists.");
			unit.setCompany(company);
			unit = this.setAttributesToUnit(unit, unitVo);
		} else {
			throw new DuplicateItemException("Unit Already Exists.");
		}
		this.sessionFactory.getCurrentSession().saveOrUpdate(unit);
	}

	private Unit setAttributesToUnit(Unit unit, UnitVo unitVo) {
		unit.setDecimalPlaces(unitVo.getDecimalPlaces());
		unit.setIsDefault(false);
		unit.setName(unitVo.getName());
		unit.setSymbol(unitVo.getSymbol());
		return unit;
	}

	public void deleteUnit(Long id) {
		Unit unit = findUnit(id);
		if (unit == null)
			throw new ItemNotFoundException("Unit Not Found");
		else {
			this.sessionFactory.getCurrentSession().delete(unit);
		}

	}

	public Unit findUnit(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Unit.class);
		criteria.add(Restrictions.eq("id", id));
		return (Unit) criteria.uniqueResult();
	}

	public Unit findUnit(String name) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Unit.class);
		criteria.add(Restrictions.eq("name", name));
		return (Unit) criteria.uniqueResult();
	}

	public Unit findUnit(String name, String symbol) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Unit.class);
		criteria.add(Restrictions.eq("name", name));
		criteria.add(Restrictions.eq("symbol", symbol));
		return (Unit) criteria.uniqueResult();
	}

	public UnitVo findUnitById(Long id) {
		Unit unit = this.findUnit(id);
		if (unit == null)
			throw new ItemNotFoundException("Unit Not Exist");
		return createUnitVo(unit);
	}

	private UnitVo createUnitVo(Unit unit) {
		UnitVo unitVo = new UnitVo();
		unitVo.setId(unit.getId());
		unitVo.setName(unit.getName());
		unitVo.setSymbol(unit.getSymbol());
		unitVo.setDecimalPlaces(unit.getDecimalPlaces());
		return unitVo;
	}

	public UnitVo findUnitByName(String name) {
		Unit unit = this.findUnit(name);
		if (unit == null)
			throw new ItemNotFoundException("Unit Not Exist");
		return createUnitVo(unit);
	}

	public UnitVo findUnitByNameAndSymbol(String name, String symbol) {
		Unit unit = this.findUnit(name, symbol);
		if (unit == null)
			throw new ItemNotFoundException("Unit Not Exist");
		return createUnitVo(unit);
	}

	public List<UnitVo> findAllUnitsByCompany(Long companyId) {
		List<UnitVo> unitVos = new ArrayList<UnitVo>();
		List<Unit> units = new ArrayList<Unit>();
		units = this.findAllUnitsObjectByCompany(companyId);
		for (Unit unit : units) {
			unitVos.add(createUnitVo(unit));
		}
		return unitVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<Unit> findAllUnitsObjectByCompany(Long companyId) {
		List<Unit> units = new ArrayList<Unit>();
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Exists.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Unit.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		units = criteria.list();
		return units;
	}

}
