package com.purchase.web.dao;

import java.util.List;

import com.purchase.web.entities.Bill;
import com.purchase.web.vo.BillVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 3, 2015
 */
public interface BillDao {

	/**
	 * Method to save or update bill
	 * @param billVo
	 * @return 
	 */
	public Long saveOrUpdateBill(BillVo billVo);
	
	/**
	 * Method to delete bill
	 * @param id
	 */
	public void deleteBill(Long id);
	
	/**
	 * Method to find bill by id
	 * @param id
	 * @return
	 */
	public BillVo findBill(Long id);
	
	/**
	 * Method to find bill object by id
	 * @param id
	 * @return
	 */
	public Bill findBillObject(Long id);
	
	/**
	 * method to list bills by vendor
	 * @param vendorId
	 * @return
	 */
	public List<BillVo> listBillsByVendor(Long vendorId);
	
	/**
	 * Method to list bills for company
	 * @param companyId
	 * @return
	 */
	public List<BillVo> listBillsForCompany(Long companyId);
	
	/**
	 * method to pay bill installment
	 * @param installmentId
	 */
	public void updateBillInstallment(Long installmentId);
}
