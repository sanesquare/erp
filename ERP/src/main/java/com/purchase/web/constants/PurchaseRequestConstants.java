package com.purchase.web.constants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface PurchaseRequestConstants {

	String HOME = "purchaseHome.do";

	String VENDORS = "vendors.do";
	String VENDOR_LIST = "vendorList.do";
	String VENDORS_ADD = "addVendors.do";
	String VENDORS_SAVE = "saveVendor.do";
	String VENDORS_EDIT = "editVendor.do";
	String VENDOR_DELETE = "deleteVendor.do";
	String VENDOR_CONTACT_SAVE = "vendorContactSave.do";
	String VENDOR_CONTACT_LIST = "vendorContactList.do";
	String VENDOR_CONTACT_DELETE = "deleteVendorContact.do";

	String PRODUCTS = "products.do";
	String ADD_PRODUCT = "addProduct.do";
	String PRODUCT_SAVE = "saveProduct.do";
	String PRODUCT_EDIT = "editProduct.do";
	String PRODUCT_DELETE = "deleteProduct.do";
	String PRODUCT_LIST = "productList.do";
	String PRODUCT_POPUP = "productPopUp.do";
	String PROUCTS_FIND = "findProducts.do";

	String CHARGE_POPUP = "chargePopUp.do";
	String CHARGE_FIND = "findCharges.do";

	String PAYMENT_INSTALLMENTS = "getInstallmentsForBill.do";

	String SETTINGS = "purchaseSettings.do";

	String SAVE_UNIT = "saveUnit.do";
	String EDIT_UNIT = "editUnit.do";
	String DELETE_UNIT = "deleteUnit.do";
	String UNIT_LIST = "listUnits.do";
	String UNIT_DELETE = "unitDelete.do";

	String SAVE_STOCK_GROUP = "saveStockGroup.do";
	String DELETE_STOCK_GROUP = "deleteStockGroup.do";
	String LIST_STOCK_GROUPS = "listStockGroups.do";

	String BILLS_HOME = "billsHome.do";
	String ADD_BILL = "addBill.do";
	String CONVERT_RECEIVING_NOTE_TO_BILL = "convertReceivingNoteToBill.do";
	String GET_CURRENCIES_FOR_BILL = "getCurrenciesForBill.do";
	String BILL_SAVE = "saveBill.do";
	String VIEW_BILL = "viewBill.do";
	String DELETE_BILL = "deletebill.do";
	String BILL_LIST = "billList.do";
	String PAY_BILL_INSTALLMENT = "payBillInstallment.do";
	String BILLS_BY_VENDOR = "getBillsByVendor.do";

	String VENDOR_QUOTES_HOME = "vendorQuotesHome.do";
	String VENDOR_QUOTES_LIST = "vendorQuotesList.do";
	String ADD_VENDOR_QUOTE = "addVendorQuote.do";
	String VENDOR_QUOTE_SAVE = "saveVendorQoutes.do";
	String VENDOR_QUOTE_VIEW = "viewVendorQuote.do";
	String VENDOR_QUOTE_DELETE = "deleteVendorQuote.do";
	String CONVERT_VENDOR_QUOTE_TO_ORDER = "convertVendorQouteToOrder.do";

	String PURCHASE_ORDERS_HOME = "purchaseOrderHome.do";
	String PURCHASE_ORDERS_LIST = "purchaseOrderList.do";
	String ADD_PURCHASE_ORDER = "addPurchaseOrder.do";
	String PURCHASE_ORDER_SAVE = "savePurchaseOrder.do";
	String PURCHASE_ORDER_VIEW = "viewPurchaseOrder.do";
	String PURCHASE_ORDER_DELETE = "deletePurchaseOrder.do";
	String CONVERT_PURCHASE_ORDER_TO_RECEIVING_NOTE = "purchaseOrderToReceivingNote.do";

	String PURCHASE_RETURN_HOME = "purchaseReturnHome.do";
	String PURCHASE_RETURN_LIST = "purchaseReturnList.do";
	String PURCHASE_RETURN_ADD = "addPurchaseReturn.do";
	String RETURN_THIS_BILL = "returnThisBill.do";
	String PURCHASE_RETURN_SAVE="savePurchaseReturn.do";
	String PURCHASE_RETURN_VIEW="viewPurchaseReturn.do";
	String PURCHASE_RETURN_DELETE="deletePurchaseReturn.do";
	
	String GENERATE_BILL_PDF="generateBillPdf.do";
	String GENERATE_PURCHASE_ORDER_PDF="purchaseOrderPdf.do";
	String GENERATE_VENDOR_QUOTE_PDF="vendorQuotePdf.do";
	
}
