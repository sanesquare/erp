package com.purchase.web.constants;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface PurchasePageNameConstants {

	String HOME_PAGE ="purchaseHome";
	
	String VENDOR_PAGE="vendors";
	String VENDOR_ADD="addVendors";
	String VENDOR_LIST="vendorList";
	String VENDOR_CONTACT_LIST="vendorContactList";
	
	String PRODUCTS_PAGE="products";
	String PRODUCTS_ADD="addProduct";
	String PRODUCT_LIST="productsTable";
	String PRODUCT_POP_UP="productPopUp";
	String PRODUCT_ROWS="productRows";
	
	String CHARGE_POP_UP="chargePopUp";
	String CHARGE_ROWS="chargeRows";
	
	String PAYMENT_INSTALLMENTS="paymentInstallments";
	
	String SETTINGS="purchaseSettings";
	String UNIT_TABLE="unitTable";
	
	String STOCK_GROUP_TABLE="stockGroupsTable";
	
	String BILLS_HOME="billsHome";
	String ADD_BILL="addBill";
	String BILL_LIST="billList";
	
	String VENDOR_QUOTES_HOME="vendorQuotesHome";
	String ADD_VENDOR_QUOTES="addVendorQuote";
	String VENDOR_QUOTE_TEMPLATE="vendorQuotesTemplate";
	
	
	String PURCHASE_ORDERS_HOME="purchaseOrderHome";
	String ADD_PURCHASE_ORDER="addPurchaseOrder";
	String PURCHASE_OREDER_LIST="purchaseOrderList";
	
	String PURCHASE_RETURN_HOME="purchaseReturnHome";
	String PURCHASE_RETURN_ADD="addPurchaseReturn";
	String PURCHASE_RETURN_LIST="purchaseReturnList";
	
}
