package com.purchase.web.vo;

import java.math.BigDecimal;

/**
 * @author Shamsheer & Vinutha
 * @since Dec 8, 2015
 */
public class PurchaseOrderChargeVo {

	private Long poChargeId;

	private Long orderId;

	private String charge;

	private BigDecimal amount;

	private Long chargeId;

	public Long getPoChargeId() {
		return poChargeId;
	}

	public void setPoChargeId(Long poChargeId) {
		this.poChargeId = poChargeId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getChargeId() {
		return chargeId;
	}

	public void setChargeId(Long chargeId) {
		this.chargeId = chargeId;
	}
}
