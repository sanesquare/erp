package com.purchase.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.purchase.web.entities.PurchaseOrderCharges;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 6, 2015
 */
public class BillChargeVo {

	private Long billChargeId;

	private Long billId;

	private String charge;

	private BigDecimal amount = new BigDecimal("0.00");

	private Long chargeId;

	private Set<PurchaseOrderCharges> purchaseOrderCharges = new HashSet<PurchaseOrderCharges>();

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getChargeId() {
		return chargeId;
	}

	public void setChargeId(Long chargeId) {
		this.chargeId = chargeId;
	}

	public Long getBillChargeId() {
		return billChargeId;
	}

	public void setBillChargeId(Long billChargeId) {
		this.billChargeId = billChargeId;
	}

	public Set<PurchaseOrderCharges> getPurchaseOrderCharges() {
		return purchaseOrderCharges;
	}

	public void setPurchaseOrderCharges(Set<PurchaseOrderCharges> purchaseOrderCharges) {
		this.purchaseOrderCharges = purchaseOrderCharges;
	}
}
