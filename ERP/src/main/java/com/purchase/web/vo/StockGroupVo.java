package com.purchase.web.vo;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 23, 2015
 */
public class StockGroupVo {

	private Long id;
	
	private String groupName;
	
	private Set<StockGroupVo> subGroups = new HashSet<StockGroupVo>();
	
	private Long companyId;
	
	private Long parentGroupId;

	private String parentGroup;
	
	private Set<Long> subGroupIds = new HashSet<Long>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Set<StockGroupVo> getSubGroups() {
		return subGroups;
	}

	public void setSubGroups(Set<StockGroupVo> subGroups) {
		this.subGroups = subGroups;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getParentGroupId() {
		return parentGroupId;
	}

	public void setParentGroupId(Long parentGroupId) {
		this.parentGroupId = parentGroupId;
	}

	public String getParentGroup() {
		return parentGroup;
	}

	public void setParentGroup(String parentGroup) {
		this.parentGroup = parentGroup;
	}

	public Set<Long> getSubGroupIds() {
		return subGroupIds;
	}

	public void setSubGroupIds(Set<Long> subGroupIds) {
		this.subGroupIds = subGroupIds;
	}
	
	
}
