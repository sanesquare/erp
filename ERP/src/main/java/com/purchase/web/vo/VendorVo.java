package com.purchase.web.vo;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author Shamsheer & Vinutha
 * @since 03-August-2015
 */
public class VendorVo {

	private Long companyId;
	
	private Long vendorId;
	
	private String vendorCode;
	
	private String name;
	
	private String address;
	
	private Long countryId;
	
	private String deliveryAddress;
	
	private String mobile;
	
	private String email;
	
	private String landPhone;
	
	private String country;
	
	private List<VendorContactVo> contactVos = new ArrayList<VendorContactVo>();

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLandPhone() {
		return landPhone;
	}

	public void setLandPhone(String landPhone) {
		this.landPhone = landPhone;
	}

	public List<VendorContactVo> getContactVos() {
		return contactVos;
	}

	public void setContactVos(List<VendorContactVo> contactVos) {
		this.contactVos = contactVos;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
}
