package com.purchase.web.vo;

import java.math.BigDecimal;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 6, 2015
 */
public class BillProductVo {
	
	private Long billProductId;

	private Long productId;
	
	private Long godownId;

	private String godown;
	
	private String productCode;

	private String productName;

	private BigDecimal quantity = BigDecimal.ZERO;
	
	private BigDecimal returnedQuantity = BigDecimal.ZERO;
	
	private BigDecimal quantityToReturn = BigDecimal.ZERO;
	
	private Boolean isReturned=false;

	private BigDecimal unitPrice = BigDecimal.ZERO;

	private BigDecimal discount = BigDecimal.ZERO;

	private BigDecimal netAmount = BigDecimal.ZERO;
	
	private BigDecimal amountExcludingTax = BigDecimal.ZERO;

	private Long taxId;

	private String tax;
	
	private Long billId;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public Long getTaxId() {
		return taxId;
	}

	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public Long getBillProductId() {
		return billProductId;
	}

	public void setBillProductId(Long billProductId) {
		this.billProductId = billProductId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public BigDecimal getAmountExcludingTax() {
		return amountExcludingTax;
	}

	public void setAmountExcludingTax(BigDecimal amountExcludingTax) {
		this.amountExcludingTax = amountExcludingTax;
	}

	public Long getGodownId() {
		return godownId;
	}

	public void setGodownId(Long godownId) {
		this.godownId = godownId;
	}

	public BigDecimal getReturnedQuantity() {
		return returnedQuantity;
	}

	public void setReturnedQuantity(BigDecimal returnedQuantity) {
		this.returnedQuantity = returnedQuantity;
	}

	public BigDecimal getQuantityToReturn() {
		return quantityToReturn;
	}

	public void setQuantityToReturn(BigDecimal quantityToReturn) {
		this.quantityToReturn = quantityToReturn;
	}

	public Boolean getIsReturned() {
		return isReturned;
	}

	public void setIsReturned(Boolean isReturned) {
		this.isReturned = isReturned;
	}

	public String getGodown() {
		return godown;
	}

	public void setGodown(String godown) {
		this.godown = godown;
	}

}
