package com.purchase.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 3, 2015
 */
public class BillVo {

	private Long receivingNoteId;
	
	private Long returnId;
	
	private List<Long> receivingNoteIds=new ArrayList<Long>();
	
	private BigDecimal productsTaxAmount = new BigDecimal("0.00");
	
	private BigDecimal outStanding = new BigDecimal("0.00");

	private String account;
	
	private List<String> taxes = new ArrayList<String>();
	
	private List<String> taxesWithRate = new ArrayList<String>();

	private String vendor;

	private BigDecimal productTaxTotal = new BigDecimal("0.00");

	private BigDecimal netAmountHidden = new BigDecimal("0.00");

	private List<Long> tsxIds = new ArrayList<Long>();

	private BigDecimal discountRate = new BigDecimal("0.00");

	private Long ledgerId;

	private Boolean isEdit;
	
	private Boolean isReturned=false;

	private Long billId;

	private Long companyId;

	private String billCode;

	private Long vendorId;

	private String billDate;

	private Long paymentTermId;

	private Long erpCurrencyId;

	private String paymentTerm;

	private String currecny;

	private String status;

	private Boolean isSettled;

	private List<BillProductVo> productVos = new ArrayList<BillProductVo>();

	private List<BillChargeVo> chargeVos = new ArrayList<BillChargeVo>();

	private BigDecimal subTotal = new BigDecimal("0.00");

	private BigDecimal discountTotal = new BigDecimal("0.00");

	private BigDecimal netTotal = new BigDecimal("0.00");

	private BigDecimal taxTotal = new BigDecimal("0.00");

	private BigDecimal grandTotal = new BigDecimal("0.00");

	private List<BillInstallmentVo> installmentVos = new ArrayList<BillInstallmentVo>();

	private Long deliveryTermId ;
	
	private String deliveryTerm;
	
	public Boolean getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getBillCode() {
		return billCode;
	}

	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public Long getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(Long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public Long getErpCurrencyId() {
		return erpCurrencyId;
	}

	public void setErpCurrencyId(Long erpCurrencyId) {
		this.erpCurrencyId = erpCurrencyId;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getCurrecny() {
		return currecny;
	}

	public void setCurrecny(String currecny) {
		this.currecny = currecny;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getIsSettled() {
		return isSettled;
	}

	public void setIsSettled(Boolean isSettled) {
		this.isSettled = isSettled;
	}

	public List<BillProductVo> getProductVos() {
		return productVos;
	}

	public void setProductVos(List<BillProductVo> productVos) {
		this.productVos = productVos;
	}

	public List<BillChargeVo> getChargeVos() {
		return chargeVos;
	}

	public void setChargeVos(List<BillChargeVo> chargeVos) {
		this.chargeVos = chargeVos;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getDiscountTotal() {
		return discountTotal;
	}

	public void setDiscountTotal(BigDecimal discountTotal) {
		this.discountTotal = discountTotal;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public List<BillInstallmentVo> getInstallmentVos() {
		return installmentVos;
	}

	public void setInstallmentVos(List<BillInstallmentVo> installmentVos) {
		this.installmentVos = installmentVos;
	}

	public Long getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(Long ledgerId) {
		this.ledgerId = ledgerId;
	}

	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	public List<Long> getTsxIds() {
		return tsxIds;
	}

	public void setTsxIds(List<Long> tsxIds) {
		this.tsxIds = tsxIds;
	}

	public BigDecimal getNetAmountHidden() {
		return netAmountHidden;
	}

	public void setNetAmountHidden(BigDecimal netAmountHidden) {
		this.netAmountHidden = netAmountHidden;
	}

	public BigDecimal getProductTaxTotal() {
		return productTaxTotal;
	}

	public void setProductTaxTotal(BigDecimal productTaxTotal) {
		this.productTaxTotal = productTaxTotal;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public List<String> getTaxes() {
		return taxes;
	}

	public void setTaxes(List<String> taxes) {
		this.taxes = taxes;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public BigDecimal getOutStanding() {
		return outStanding;
	}

	public void setOutStanding(BigDecimal outStanding) {
		this.outStanding = outStanding;
	}

	public List<String> getTaxesWithRate() {
		return taxesWithRate;
	}

	public void setTaxesWithRate(List<String> taxesWithRate) {
		this.taxesWithRate = taxesWithRate;
	}

	public BigDecimal getProductsTaxAmount() {
		return productsTaxAmount;
	}

	public void setProductsTaxAmount(BigDecimal productsTaxAmount) {
		this.productsTaxAmount = productsTaxAmount;
	}

	public Long getReceivingNoteId() {
		return receivingNoteId;
	}

	public void setReceivingNoteId(Long receivingNoteId) {
		this.receivingNoteId = receivingNoteId;
	}

	public List<Long> getReceivingNoteIds() {
		return receivingNoteIds;
	}

	public void setReceivingNoteIds(List<Long> receivingNoteIds) {
		this.receivingNoteIds = receivingNoteIds;
	}

	public Boolean getIsReturned() {
		return isReturned;
	}

	public void setIsReturned(Boolean isReturned) {
		this.isReturned = isReturned;
	}

	public Long getReturnId() {
		return returnId;
	}

	public void setReturnId(Long returnId) {
		this.returnId = returnId;
	}

	public Long getDeliveryTermId() {
		return deliveryTermId;
	}

	public void setDeliveryTermId(Long deliveryTermId) {
		this.deliveryTermId = deliveryTermId;
	}

	public String getDeliveryTerm() {
		return deliveryTerm;
	}

	public void setDeliveryTerm(String deliveryTerm) {
		this.deliveryTerm = deliveryTerm;
	}

}
