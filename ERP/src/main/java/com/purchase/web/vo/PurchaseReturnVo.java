package com.purchase.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 6, 2015
 */
public class PurchaseReturnVo {

	private Long billId;
	
	private String code;
	
	private Boolean isReturned = false;
	
	private Long returnId;
	
	private Long vendorId;
	
	private String date;
	
	private String vendor;
	
	private String account;
	
	private Boolean isSettled;
	
	private String referenceNumber;
	
	private String currecny;
	
	private List<PurchaseReturnItemsVo> productVos = new ArrayList<PurchaseReturnItemsVo>();
	
	public Long getReturnId() {
		return returnId;
	}

	public void setReturnId(Long returnId) {
		this.returnId = returnId;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Boolean getIsSettled() {
		return isSettled;
	}

	public void setIsSettled(Boolean isSettled) {
		this.isSettled = isSettled;
	}

	public String getCurrecny() {
		return currecny;
	}

	public void setCurrecny(String currecny) {
		this.currecny = currecny;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public List<PurchaseReturnItemsVo> getProductVos() {
		return productVos;
	}

	public void setProductVos(List<PurchaseReturnItemsVo> productVos) {
		this.productVos = productVos;
	}

	public Boolean getIsReturned() {
		return isReturned;
	}

	public void setIsReturned(Boolean isReturned) {
		this.isReturned = isReturned;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}
