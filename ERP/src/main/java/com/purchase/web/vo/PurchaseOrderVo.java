package com.purchase.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 17, 2015
 */
public class PurchaseOrderVo {

	private BigDecimal productsTaxAmount = new BigDecimal("0.00");

	private List<String> taxes = new ArrayList<String>();

	private List<String> taxesWithRate = new ArrayList<String>();

	private String vendor;

	private BigDecimal productTaxTotal = new BigDecimal("0.00");

	private BigDecimal netAmountHidden = new BigDecimal("0.00");

	private List<Long> tsxIds = new ArrayList<Long>();

	private BigDecimal discountRate = new BigDecimal("0.00");

	private Boolean isEdit;

	private Long orderId;

	private Long companyId;

	private String orderCode;

	private Long vendorId;

	private String orderDate;

	private Long paymentTermId;

	private Long erpCurrencyId;

	private String paymentTerm;

	private String currency;

	private String status;

	private List<PurchaseOrderProductVo> productVos = new ArrayList<PurchaseOrderProductVo>();
	
	private List<PurchaseOrderChargeVo> chargeVos = new ArrayList<PurchaseOrderChargeVo>();

	private BigDecimal subTotal = new BigDecimal("0.00");

	private BigDecimal discountTotal = new BigDecimal("0.00");

	private BigDecimal netTotal = new BigDecimal("0.00");

	private BigDecimal taxTotal = new BigDecimal("0.00");

	private BigDecimal grandTotal = new BigDecimal("0.00");

	public BigDecimal getProductsTaxAmount() {
		return productsTaxAmount;
	}

	public void setProductsTaxAmount(BigDecimal productsTaxAmount) {
		this.productsTaxAmount = productsTaxAmount;
	}

	public List<String> getTaxes() {
		return taxes;
	}

	public void setTaxes(List<String> taxes) {
		this.taxes = taxes;
	}

	public List<String> getTaxesWithRate() {
		return taxesWithRate;
	}

	public void setTaxesWithRate(List<String> taxesWithRate) {
		this.taxesWithRate = taxesWithRate;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public BigDecimal getProductTaxTotal() {
		return productTaxTotal;
	}

	public void setProductTaxTotal(BigDecimal productTaxTotal) {
		this.productTaxTotal = productTaxTotal;
	}

	public BigDecimal getNetAmountHidden() {
		return netAmountHidden;
	}

	public void setNetAmountHidden(BigDecimal netAmountHidden) {
		this.netAmountHidden = netAmountHidden;
	}

	public List<Long> getTsxIds() {
		return tsxIds;
	}

	public void setTsxIds(List<Long> tsxIds) {
		this.tsxIds = tsxIds;
	}

	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	public Boolean getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public Long getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(Long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public Long getErpCurrencyId() {
		return erpCurrencyId;
	}

	public void setErpCurrencyId(Long erpCurrencyId) {
		this.erpCurrencyId = erpCurrencyId;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<PurchaseOrderProductVo> getProductVos() {
		return productVos;
	}

	public void setProductVos(List<PurchaseOrderProductVo> productVos) {
		this.productVos = productVos;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getDiscountTotal() {
		return discountTotal;
	}

	public void setDiscountTotal(BigDecimal discountTotal) {
		this.discountTotal = discountTotal;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public List<PurchaseOrderChargeVo> getChargeVos() {
		return chargeVos;
	}

	public void setChargeVos(List<PurchaseOrderChargeVo> chargeVos) {
		this.chargeVos = chargeVos;
	}

	
}
