package com.inventory.web.constants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface InventoryRequestConstants {

	String HOME = "inventoryHome.do";

	String RECEIVING_NOTE = "receivingNotes.do";

	String INVENTORY_SETTINGS_URL = "inventorySettings.do";
	String GODOWN_SAVE = "saveGoDown.do";
	String GODOWN_DELETE = "deleteGoDown.do";
	
	String RECEIVING_NOTES="receivingNotes.do";
	String RECEIVING_NOTES_ADD="addReceivingNote.do";
	String RECEIVING_NOTE_SAVE="saveReceivingNote.do";
	String RECEIVING_NOTE_VIEW="viewReceivingNote.do";
	String RECEIVING_NOTE_DELETE="deleteReceivingNOte.do";
	String RECEIVING_NOTE_LIST="listReceivingNOte.do";
	
	String DELIVERY_NOTES="deliveryNotes.do";
	String DELIVERY_NOTES_ADD="addDeliveryNote.do";
	String DELIVERY_NOTE_SAVE="saveDeliveryNote.do";
	String DELIVERY_NOTE_VIEW="viewDeliveryNote.do";
	String DELIVERY_NOTE_DELETE="deleteDeliveryNOte.do";
	String DELIVERY_NOTE_LIST="listDeliveryNOte.do";
	String CONVERT_DELIVERY_NOTE_TO_INVOICE="convertDeliveryNotToInvoice.do";
	
	String PRODUCT_POP="inventoryProductPop.do";
	String PRODUCT_ROW="inventoryProductRow.do";
	
	String INVENTORY_ENTRY="inventoryEntry.do";
	String INVENTORY_ENTRY_ADD="addInventoryEntry.do";
	String INVENTORY_ENTRY_VIEW="viewInventoryEntry.do";
	String INVENTORY_ENTRY_LIST="listInventoryEntry.do";
	String INVENTORY_ENTRY_DELETE="deleteInventoryEntry.do";
	String IE_PRODUCT_POP="invntr_entryProductPop.do";
	String IE_PRODUCT_ROW="invntr_entryProductRow.do";
	String INVENTORY_ENTRY_SAVE="saveInventoryEntry.do";
	
	String INVENTORY_WITHDRAWAL="inventoryWithdrawal.do";
	String INVENTORY_WITHDRAWAL_ADD="addInventoryWithdrawal.do";
	String INVENTORY_WITHDRAWAL_VIEW="viewInventoryWithdrawal.do";
	String INVENTORY_WITHDRAWAL_LIST="listInventoryWithdrawal.do";
	String INVENTORY_WITHDRAWAL_DELETE="deleteInventoryWithdrawal.do";
	String INVENTORY_WITHDRAWAL_SAVE="saveInventoryWithdrawal.do";
	
	String PACKING_KINDS_LIST="listPackingKinds.do";
	String PACKING_KINDS_SAVE="savePackingKinds.do";
	String PACKING_KINDS_DELETE="deletePackingKinds.do";
	
	String PACKING_LIST="packingList.do";
	String PACKING_LIST_ADD="addPackingList.do";
	String PACKING_LIST_SAVE="savePackingList.do";
	String PACKING_LIST_VIEW="viewPackingList.do";
	String PACKING_LIST_DELETE="deletePackingList.do";
	String PACKING_LIST_LIST="listPackingList.do";
	String CONVERT_INVOICE_TO_PACKING_LIST="packThisInvoice.do";
	
	String STOCK_SUMMARY="stockSummary.do";
	String STOCK_SUMMARY_FILTER="stockSummaryFilter.do";
	String STOCK_SUMMARY_FILTER_TEMPLATE="stockSummaryFilterTemplate.do";
	String STOCK_SUMMARY_DETAILED="getDetailedStockView.do";
	String STOCK_SUMMARY_DETAILED_PREVIOUS="getDetailedStockViewPrevious.do";
	String STOCK_SUMMARY_MONTHLY_DETAILS="stockSummaryMonthViseProductDetails.do";
	String PACKING_LIST_PDF = "packingListPdf.do";
	String LOAD_VOUCHER="loadVoucher.do";
}
