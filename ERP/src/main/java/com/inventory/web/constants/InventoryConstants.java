package com.inventory.web.constants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface InventoryConstants {

	String IE_MANUAL = "Manual";
	String IE_RECEIPT = "Receipt";

	String TO_BE_ISSUED_GODOWN = "To Be Issued";

	String PURCHASE = "Purchase";
	String PURCHASE_RETURN = "Purchase Return";
	String SALES = "Sales";
	String SALES_RETURN = "Sales Return";
	String INVENTORY_ENTRY = "Inventory Entry";
	String INVENTORY_WITHDRAWAL = "Inventory Withdrawal";
}
