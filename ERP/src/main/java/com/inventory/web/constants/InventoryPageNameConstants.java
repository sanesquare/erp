package com.inventory.web.constants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface InventoryPageNameConstants {

	String HOME_PAGE = "inventoryHome";

	String SETTINGS_PAGE="inventorySettings";
	
	String RECEIVING_NOTES="receivingNotes";
	String RECEIVING_NOTES_ADD="addReceivingNote";
	String RECEIVNG_NOTE_LIST="receivingNoteList";
	
	String DELIVERY_NOTES="deliveryNotes";
	String DELIVERY_NOTES_ADD="addDeliveryNote";
	String DELIVERY_NOTE_LIST="deliveryNoteList";
	
	String PRODUCT_POP="inventoryProductPop";
	String PRODUCT_ROW="inventoryProductRow";

	String INVENTORY_ENTRY="inventoryEntry";
	String INVENTORY_ENTRY_LIST="invntr_entryList";
	String INVENTORY_ENTRY_ADD="addInventoryEntry";
	
	String IE_PRODUCT_POP="invntr_entryProductPop";
	String IE_PRODUCT_ROW="invntr_entryProductRow";
	
	String INVENTORY_WITHDRAWAL="inventoryWithdrawal";
	String INVENTORY_WITHDRAWAL_LIST="inventoryWithdrawalList";
	String INVENTORY_WITHDRAWAL_ADD="addInventoryWithdrawal";
	
	String PACKING_KIND_LIST="packingKindList";
	
	String PACKING_LISTS="packingLists";
	String PACKING_LISTS_ADD="addPackingList";
	String PACKING_LISTS_LIST="packingListList";
	
	String STOCK_SUMMARY="stockSummary";
	String STOCK_SUMMARY_HOME_POP="stockSummaryHomePop";
	String STOCK_SUMMARY_DETAILED="stockSummaryDetailed";
	String STOCK_SUMMARY_PRODUCT_DETAILS="stockSummaryProductDetails";
	String STOCK_SUMMARY_MONTHLY_DETAILS="stockSummaryMonthViseProductDetails";
	String BILL_VOUCHER_POP="billVoucherPop";
	String INVOICE_VOUCHER_POP="invoiceVoucherPop";
	String SALES_RETURN_VOUCHER_POP="salesReturnVoucherPop";
	String PURCHASE_RETURN_VOUCHER_POP="purchaseReturnVoucherPop";
	String ENTRY_VOUCHER_POP="inventoryEntryVoucherPop";
	String WITHDRAWAL_VOUCHER_POP="inventoryWithdrawalVoucherPop";
	
	String PACKING_LIST_PDF = "packingListPdf";
}
