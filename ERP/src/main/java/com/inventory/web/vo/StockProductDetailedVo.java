package com.inventory.web.vo;

import java.math.BigDecimal;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 19, 2015
 */
public class StockProductDetailedVo {

	private String ledger;

	private String voucher;
	
	private Long voucherId;
	
	private String date;

	private String voucherNumber;

	private String unit;

	private BigDecimal inwardsQuantity=new BigDecimal("0.00");
	
	private BigDecimal toBeIssuedQuantity=new BigDecimal("0.00");

	private BigDecimal inwardsRate=new BigDecimal("0.00");

	private BigDecimal inwardsValue=new BigDecimal("0.00");

	private BigDecimal outwardsQuantity=new BigDecimal("0.00");

	private BigDecimal outwardsRate=new BigDecimal("0.00");

	private BigDecimal outwardsValue=new BigDecimal("0.00");

	private BigDecimal closingQuantity=new BigDecimal("0.00");

	private BigDecimal closingRate=new BigDecimal("0.00");

	private BigDecimal closingValue=new BigDecimal("0.00");

	public String getLedger() {
		return ledger;
	}

	public void setLedger(String ledger) {
		this.ledger = ledger;
	}

	public String getVoucher() {
		return voucher;
	}

	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public BigDecimal getInwardsQuantity() {
		return inwardsQuantity;
	}

	public void setInwardsQuantity(BigDecimal inwardsQuantity) {
		this.inwardsQuantity = inwardsQuantity;
	}

	public BigDecimal getInwardsRate() {
		return inwardsRate;
	}

	public void setInwardsRate(BigDecimal inwardsRate) {
		this.inwardsRate = inwardsRate;
	}

	public BigDecimal getInwardsValue() {
		return inwardsValue;
	}

	public void setInwardsValue(BigDecimal inwardsValue) {
		this.inwardsValue = inwardsValue;
	}

	public BigDecimal getOutwardsQuantity() {
		return outwardsQuantity;
	}

	public void setOutwardsQuantity(BigDecimal outwardsQuantity) {
		this.outwardsQuantity = outwardsQuantity;
	}

	public BigDecimal getOutwardsRate() {
		return outwardsRate;
	}

	public void setOutwardsRate(BigDecimal outwardsRate) {
		this.outwardsRate = outwardsRate;
	}

	public BigDecimal getOutwardsValue() {
		return outwardsValue;
	}

	public void setOutwardsValue(BigDecimal outwardsValue) {
		this.outwardsValue = outwardsValue;
	}

	public BigDecimal getClosingQuantity() {
		return closingQuantity;
	}

	public void setClosingQuantity(BigDecimal closingQuantity) {
		this.closingQuantity = closingQuantity;
	}

	public BigDecimal getClosingRate() {
		return closingRate;
	}

	public void setClosingRate(BigDecimal closingRate) {
		this.closingRate = closingRate;
	}

	public BigDecimal getClosingValue() {
		return closingValue;
	}

	public void setClosingValue(BigDecimal closingValue) {
		this.closingValue = closingValue;
	}

	public Long getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(Long voucherId) {
		this.voucherId = voucherId;
	}

	public BigDecimal getToBeIssuedQuantity() {
		return toBeIssuedQuantity;
	}

	public void setToBeIssuedQuantity(BigDecimal toBeIssuedQuantity) {
		this.toBeIssuedQuantity = toBeIssuedQuantity;
	}
}
