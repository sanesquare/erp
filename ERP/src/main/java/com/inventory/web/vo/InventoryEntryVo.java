package com.inventory.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 23, 2015
 */
public class InventoryEntryVo {

	private Long companyId;
	
	private List<InventoryEntryItemsVo> itemsVos = new ArrayList<InventoryEntryItemsVo>();
	
	private String date;
	
	private String narration;
	
	private Long godownId;
	
	private String godown;
	
	private String code;
	
	private Long inventoryEntryId;
	
	private String type;

	private Boolean editable=true;
	
	public List<InventoryEntryItemsVo> getItemsVos() {
		return itemsVos;
	}

	public void setItemsVos(List<InventoryEntryItemsVo> itemsVos) {
		this.itemsVos = itemsVos;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public Long getGodownId() {
		return godownId;
	}

	public void setGodownId(Long godownId) {
		this.godownId = godownId;
	}

	public String getGodown() {
		return godown;
	}

	public void setGodown(String godown) {
		this.godown = godown;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getInventoryEntryId() {
		return inventoryEntryId;
	}

	public void setInventoryEntryId(Long inventoryEntryId) {
		this.inventoryEntryId = inventoryEntryId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}
}
