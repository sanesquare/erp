package com.inventory.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
public class PackingListVo {

	private Long invoiceId;
	
	private Long packingListId;
	
	private String invoiceCode;
	
	private String code;
	
	private String date;
	
	private Boolean isPacked=false;
	
	private BigDecimal netWeight = new BigDecimal("0.00");
	
	private BigDecimal grossWeight = new BigDecimal("0.00");
	
	private Long numberOfPacks;
	
	private List<PackingListItemsVo> itemsVos = new ArrayList<PackingListItemsVo>();
	
	private Long companyId;

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Long getPackingListId() {
		return packingListId;
	}

	public void setPackingListId(Long packingListId) {
		this.packingListId = packingListId;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public BigDecimal getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}

	public BigDecimal getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Long getNumberOfPacks() {
		return numberOfPacks;
	}

	public void setNumberOfPacks(Long numberOfPacks) {
		this.numberOfPacks = numberOfPacks;
	}

	public List<PackingListItemsVo> getItemsVos() {
		return itemsVos;
	}

	public void setItemsVos(List<PackingListItemsVo> itemsVos) {
		this.itemsVos = itemsVos;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Boolean getIsPacked() {
		return isPacked;
	}

	public void setIsPacked(Boolean isPacked) {
		this.isPacked = isPacked;
	}
}
