package com.inventory.web.vo;

import java.math.BigDecimal;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 27, 2015
 */
public class InventoryWithdrawalItemsVo {

private String productCode;
	
	private String productName;
	
	private BigDecimal quantity=new BigDecimal("0.00");
	
	private BigDecimal unitPrice=new BigDecimal("0.00");
	
	private BigDecimal total=new BigDecimal("0.00");
	
	private Long iventoryWithdrawalId;
	
	private Long iventoryWithdrawalItemId;
	
	private Long productId;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Long getIventoryWithdrawalId() {
		return iventoryWithdrawalId;
	}

	public void setIventoryWithdrawalId(Long iventoryWithdrawalId) {
		this.iventoryWithdrawalId = iventoryWithdrawalId;
	}

	public Long getIventoryWithdrawalItemId() {
		return iventoryWithdrawalItemId;
	}

	public void setIventoryWithdrawalItemId(Long iventoryWithdrawalItemId) {
		this.iventoryWithdrawalItemId = iventoryWithdrawalItemId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}
}
