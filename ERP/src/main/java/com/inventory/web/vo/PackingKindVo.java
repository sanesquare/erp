package com.inventory.web.vo;

import java.math.BigDecimal;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
public class PackingKindVo {

	private Long id;
	
	private String name;
	
	private BigDecimal weight;
	
	private Long companyId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
}
