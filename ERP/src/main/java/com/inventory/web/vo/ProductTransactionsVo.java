package com.inventory.web.vo;

import java.math.BigDecimal;

import com.erp.web.entities.Godown;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 19, 2015
 */
public class ProductTransactionsVo {

	private String date;
	
	private String voucher;
	
	private Godown godown;
	
	private Long voucherId;
	
	private String voucherNumber;
	
	private String ledger;
	
	private BigDecimal quantity;
	
	private BigDecimal toBeIssuedquantity=new BigDecimal("0.00");
	
	private BigDecimal unitPrice;
	
	private BigDecimal amount;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getVoucher() {
		return voucher;
	}

	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getLedger() {
		return ledger;
	}

	public void setLedger(String ledger) {
		this.ledger = ledger;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(Long voucherId) {
		this.voucherId = voucherId;
	}

	public Godown getGodown() {
		return godown;
	}

	public void setGodown(Godown godown) {
		this.godown = godown;
	}

	public BigDecimal getToBeIssuedquantity() {
		return toBeIssuedquantity;
	}

	public void setToBeIssuedquantity(BigDecimal toBeIssuedquantity) {
		this.toBeIssuedquantity = toBeIssuedquantity;
	}
}
