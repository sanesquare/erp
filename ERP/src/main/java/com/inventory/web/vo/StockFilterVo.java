package com.inventory.web.vo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 20, 2015
 */
public class StockFilterVo {
	
	private Long companyId;
	
	private String startDate;
	
	private String endDate;

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
