package com.inventory.web.vo;

import java.math.*;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 19, 2015
 */
public class PackingListPdfVo {

	private String itemCode;
	
	private String itemDescription;
	
	private Long noOfPack;
	
	private String kindOfPack;
	
	private BigDecimal contentOfPack;
	
	private BigDecimal netWeight;
	
	private BigDecimal grossWeight;

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public Long getNoOfPack() {
		return noOfPack;
	}

	public void setNoOfPack(Long noOfPack) {
		this.noOfPack = noOfPack;
	}

	public String getKindOfPack() {
		return kindOfPack;
	}

	public void setKindOfPack(String kindOfPack) {
		this.kindOfPack = kindOfPack;
	}

	public BigDecimal getContentOfPack() {
		return contentOfPack;
	}

	public void setContentOfPack(BigDecimal contentOfPack) {
		this.contentOfPack = contentOfPack;
	}

	public BigDecimal getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}

	public BigDecimal getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}
	
	
}
