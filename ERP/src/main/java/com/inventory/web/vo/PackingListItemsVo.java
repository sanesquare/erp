package com.inventory.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
public class PackingListItemsVo {

	private String productCode;

	private String productName;
	
	private Boolean isPacked=false;

	private BigDecimal weight = new BigDecimal("0.00");

	private BigDecimal netWeight = new BigDecimal("0.00");

	private BigDecimal grossWeight = new BigDecimal("0.00");

	private BigDecimal quantity = new BigDecimal("0.00");

	private BigDecimal quantityToPack = new BigDecimal("0.00");

	private Long numberOfPack;
	
	private String selectedKind;

	private Long packingKindId;

	private String packingKind;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}

	public BigDecimal getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Long getNumberOfPack() {
		return numberOfPack;
	}

	public void setNumberOfPack(Long numberOfPack) {
		this.numberOfPack = numberOfPack;
	}

	public Long getPackingKindId() {
		return packingKindId;
	}

	public void setPackingKindId(Long packingKindId) {
		this.packingKindId = packingKindId;
	}

	public String getPackingKind() {
		return packingKind;
	}

	public void setPackingKind(String packingKind) {
		this.packingKind = packingKind;
	}

	public BigDecimal getQuantityToPack() {
		return quantityToPack;
	}

	public void setQuantityToPack(BigDecimal quantityToPack) {
		this.quantityToPack = quantityToPack;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public String getSelectedKind() {
		return selectedKind;
	}

	public void setSelectedKind(String selectedKind) {
		this.selectedKind = selectedKind;
	}

	public Boolean getIsPacked() {
		return isPacked;
	}

	public void setIsPacked(Boolean isPacked) {
		this.isPacked = isPacked;
	}
}
