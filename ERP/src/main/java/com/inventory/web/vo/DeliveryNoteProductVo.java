package com.inventory.web.vo;

import java.math.BigDecimal;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 28, 2015
 */
public class DeliveryNoteProductVo {

	private Long productId;

	private String productCode;

	private String productName;

	private BigDecimal quantity = BigDecimal.ZERO;
	
	private BigDecimal quantityToDeliver = BigDecimal.ZERO;

	private BigDecimal unitPrice = BigDecimal.ZERO;

	private BigDecimal discount = BigDecimal.ZERO;

	private BigDecimal netAmount = BigDecimal.ZERO;
	
	private BigDecimal amountExcludingTax = BigDecimal.ZERO;

	private Long taxId;

	private String tax;
	
	private Long deliveryNoteId;
	
	private String godown;
	
	private Long godownId;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getAmountExcludingTax() {
		return amountExcludingTax;
	}

	public void setAmountExcludingTax(BigDecimal amountExcludingTax) {
		this.amountExcludingTax = amountExcludingTax;
	}

	public Long getTaxId() {
		return taxId;
	}

	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public Long getDeliveryNoteId() {
		return deliveryNoteId;
	}

	public void setDeliveryNoteId(Long deliveryNoteId) {
		this.deliveryNoteId = deliveryNoteId;
	}

	public String getGodown() {
		return godown;
	}

	public void setGodown(String godown) {
		this.godown = godown;
	}

	public Long getGodownId() {
		return godownId;
	}

	public void setGodownId(Long godownId) {
		this.godownId = godownId;
	}

	public BigDecimal getQuantityToDeliver() {
		return quantityToDeliver;
	}

	public void setQuantityToDeliver(BigDecimal quantityToDeliver) {
		this.quantityToDeliver = quantityToDeliver;
	}
}
