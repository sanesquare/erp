package com.inventory.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 27, 2015
 */
public class InventoryWithdrawalVo {

	private Long companyId;

	private List<InventoryWithdrawalItemsVo> itemsVos = new ArrayList<InventoryWithdrawalItemsVo>();

	private String date;

	private String narration;

	private Long godownId;

	private String godown;

	private String code;

	private Long inventoryWithdrawalId;

	private String type;
	
	private Boolean editable=true;

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public List<InventoryWithdrawalItemsVo> getItemsVos() {
		return itemsVos;
	}

	public void setItemsVos(List<InventoryWithdrawalItemsVo> itemsVos) {
		this.itemsVos = itemsVos;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public Long getGodownId() {
		return godownId;
	}

	public void setGodownId(Long godownId) {
		this.godownId = godownId;
	}

	public String getGodown() {
		return godown;
	}

	public void setGodown(String godown) {
		this.godown = godown;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getInventoryWithdrawalId() {
		return inventoryWithdrawalId;
	}

	public void setInventoryWithdrawalId(Long inventoryWithdrawalId) {
		this.inventoryWithdrawalId = inventoryWithdrawalId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

}
