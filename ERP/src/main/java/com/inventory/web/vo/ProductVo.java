package com.inventory.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 23, 2015
 */
public class ProductVo {

	private Boolean isSales;
	
	private Boolean selected=false;
	
	private List<Long > ids ;
	
	private List<String> idsAndQuantities = new ArrayList<String>();
 	
	private Long companyId;
	
	private Long productId;
	
	private Long productAdditionalInfoId;
	
	private Long unitId;
	
	private Long stockGroupId;
	
	private String name;
	
	private BigDecimal salesPrice;
	
	private BigDecimal purchasePrice;
	
	private BigDecimal openingBalance;
	
	private BigDecimal openingQuantity;
	
	private BigDecimal weight;
	
	private String productCode;
	
	private String unit;
	
	private String stockGroup;
	
	private BigDecimal availableQty;

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getProductAdditionalInfoId() {
		return productAdditionalInfoId;
	}

	public void setProductAdditionalInfoId(Long productAdditionalInfoId) {
		this.productAdditionalInfoId = productAdditionalInfoId;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(Long unitId) {
		this.unitId = unitId;
	}

	public Long getStockGroupId() {
		return stockGroupId;
	}

	public void setStockGroupId(Long stockGroupId) {
		this.stockGroupId = stockGroupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSalesPrice() {
		return salesPrice;
	}

	public void setSalesPrice(BigDecimal salesPrice) {
		this.salesPrice = salesPrice;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}

	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}

	public BigDecimal getOpeningQuantity() {
		return openingQuantity;
	}

	public void setOpeningQuantity(BigDecimal openingQuantity) {
		this.openingQuantity = openingQuantity;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getStockGroup() {
		return stockGroup;
	}

	public void setStockGroup(String stockGroup) {
		this.stockGroup = stockGroup;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public BigDecimal getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(BigDecimal availableQty) {
		this.availableQty = availableQty;
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Boolean getIsSales() {
		return isSales;
	}

	public void setIsSales(Boolean isSales) {
		this.isSales = isSales;
	}

	public List<String> getIdsAndQuantities() {
		return idsAndQuantities;
	}

	public void setIdsAndQuantities(List<String> idsAndQuantities) {
		this.idsAndQuantities = idsAndQuantities;
	}
}
