package com.inventory.web.vo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 21, 2015
 */
public class GodownVo {

	private Long godownId;

	private Long companyId;

	private String name;

	public Long getGodownId() {
		return godownId;
	}

	public void setGodownId(Long godownId) {
		this.godownId = godownId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

}
