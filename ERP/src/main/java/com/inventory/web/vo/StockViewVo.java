package com.inventory.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 5, 2015
 */
public class StockViewVo {

	private Integer hashCode = this.hashCode();

	private String name;

	private BigDecimal quantity = new BigDecimal("0.00");

	private String unit;

	private Boolean isSameUnit;

	private Boolean isProduct;

	private BigDecimal amount = new BigDecimal("0.00");

	private BigDecimal totalQuantity = new BigDecimal("0.00");

	private BigDecimal totalAmount = new BigDecimal("0.00");

	private List<StockViewVo> items = new ArrayList<StockViewVo>();
	
	private BigDecimal inwardsQuantity= new BigDecimal("0.00");

	private BigDecimal inwardsRate= new BigDecimal("0.00");

	private BigDecimal inwardsValue= new BigDecimal("0.00");

	private BigDecimal outwardsQuantity= new BigDecimal("0.00");

	private BigDecimal outwardsRate= new BigDecimal("0.00");

	private BigDecimal outwardsValue= new BigDecimal("0.00");

	private BigDecimal closingQuantity= new BigDecimal("0.00");

	private BigDecimal closingRate= new BigDecimal("0.00");

	private BigDecimal closingValue= new BigDecimal("0.00");
	
	private BigDecimal openingQuantity= new BigDecimal("0.00");
	
	private BigDecimal openingValue= new BigDecimal("0.00");
	
	private List<StockProductDetailedVo> productDetailedVos = new ArrayList<StockProductDetailedVo>();
	
	private List<StockMonthlyVo> monthlyVos = new ArrayList<StockMonthlyVo>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Boolean getIsSameUnit() {
		return isSameUnit;
	}

	public void setIsSameUnit(Boolean isSameUnit) {
		this.isSameUnit = isSameUnit;
	}

	public Boolean getIsProduct() {
		return isProduct;
	}

	public void setIsProduct(Boolean isProduct) {
		this.isProduct = isProduct;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(BigDecimal totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public List<StockViewVo> getItems() {
		return items;
	}

	public void setItems(List<StockViewVo> items) {
		this.items = items;
	}

	public Integer getHashCode() {
		return hashCode;
	}

	public BigDecimal getInwardsQuantity() {
		return inwardsQuantity;
	}

	public void setInwardsQuantity(BigDecimal inwardsQuantity) {
		this.inwardsQuantity = inwardsQuantity;
	}

	public BigDecimal getInwardsRate() {
		return inwardsRate;
	}

	public void setInwardsRate(BigDecimal inwardsRate) {
		this.inwardsRate = inwardsRate;
	}

	public BigDecimal getInwardsValue() {
		return inwardsValue;
	}

	public void setInwardsValue(BigDecimal inwardsValue) {
		this.inwardsValue = inwardsValue;
	}

	public BigDecimal getOutwardsQuantity() {
		return outwardsQuantity;
	}

	public void setOutwardsQuantity(BigDecimal outwardsQuantity) {
		this.outwardsQuantity = outwardsQuantity;
	}

	public BigDecimal getOutwardsRate() {
		return outwardsRate;
	}

	public void setOutwardsRate(BigDecimal outwardsRate) {
		this.outwardsRate = outwardsRate;
	}

	public BigDecimal getOutwardsValue() {
		return outwardsValue;
	}

	public void setOutwardsValue(BigDecimal outwardsValue) {
		this.outwardsValue = outwardsValue;
	}

	public BigDecimal getClosingQuantity() {
		return closingQuantity;
	}

	public void setClosingQuantity(BigDecimal closingQuantity) {
		this.closingQuantity = closingQuantity;
	}

	public BigDecimal getClosingRate() {
		return closingRate;
	}

	public void setClosingRate(BigDecimal closingRate) {
		this.closingRate = closingRate;
	}

	public BigDecimal getClosingValue() {
		return closingValue;
	}

	public void setClosingValue(BigDecimal closingValue) {
		this.closingValue = closingValue;
	}

	public List<StockProductDetailedVo> getProductDetailedVos() {
		return productDetailedVos;
	}

	public void setProductDetailedVos(List<StockProductDetailedVo> productDetailedVos) {
		this.productDetailedVos = productDetailedVos;
	}

	public void setHashCode(Integer hashCode) {
		this.hashCode = hashCode;
	}

	public List<StockMonthlyVo> getMonthlyVos() {
		return monthlyVos;
	}

	public void setMonthlyVos(List<StockMonthlyVo> monthlyVos) {
		this.monthlyVos = monthlyVos;
	}

	public BigDecimal getOpeningQuantity() {
		return openingQuantity;
	}

	public void setOpeningQuantity(BigDecimal openingQuantity) {
		this.openingQuantity = openingQuantity;
	}

	public BigDecimal getOpeningValue() {
		return openingValue;
	}

	public void setOpeningValue(BigDecimal openingValue) {
		this.openingValue = openingValue;
	}
}
