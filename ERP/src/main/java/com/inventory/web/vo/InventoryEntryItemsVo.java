package com.inventory.web.vo;

import java.math.BigDecimal;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 23, 2015
 */
public class InventoryEntryItemsVo {

	private String productCode;
	
	private String productName;
	
	private BigDecimal quantity=new BigDecimal("0.00");
	
	private BigDecimal unitPrice=new BigDecimal("0.00");
	
	private BigDecimal total=new BigDecimal("0.00");
	
	private Long iventoryEntryId;
	
	private Long iventoryEntryItemId;
	
	private Long productId;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Long getIventoryEntryId() {
		return iventoryEntryId;
	}

	public void setIventoryEntryId(Long iventoryEntryId) {
		this.iventoryEntryId = iventoryEntryId;
	}

	public Long getIventoryEntryItemId() {
		return iventoryEntryItemId;
	}

	public void setIventoryEntryItemId(Long iventoryEntryItemId) {
		this.iventoryEntryItemId = iventoryEntryItemId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}
}
