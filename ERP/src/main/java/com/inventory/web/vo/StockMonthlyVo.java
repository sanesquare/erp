package com.inventory.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 21, 2015
 */
public class StockMonthlyVo {

	private String date;

	private Integer hashCode = this.hashCode();

	private BigDecimal openingQuantity = new BigDecimal("0.00");

	private BigDecimal openingValue = new BigDecimal("0.00");

	private BigDecimal openingRate = new BigDecimal("0.00");

	private String name;

	private List<StockProductDetailedVo> productDetailedVos = new ArrayList<StockProductDetailedVo>();

	private BigDecimal inwardsQuantity = new BigDecimal("0.00");

	private BigDecimal inwardsRate = new BigDecimal("0.00");

	private BigDecimal inwardsValue = new BigDecimal("0.00");

	private BigDecimal outwardsQuantity = new BigDecimal("0.00");

	private BigDecimal outwardsRate = new BigDecimal("0.00");

	private BigDecimal outwardsValue = new BigDecimal("0.00");

	private BigDecimal closingQuantity = new BigDecimal("0.00");

	private BigDecimal closingRate = new BigDecimal("0.00");

	private BigDecimal closingValue = new BigDecimal("0.00");

	private String unit;

	public Integer getHashCode() {
		return hashCode;
	}

	public void setHashCode(Integer hashCode) {
		this.hashCode = hashCode;
	}

	public List<StockProductDetailedVo> getProductDetailedVos() {
		return productDetailedVos;
	}

	public void setProductDetailedVos(List<StockProductDetailedVo> productDetailedVos) {
		this.productDetailedVos = productDetailedVos;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public BigDecimal getInwardsQuantity() {
		return inwardsQuantity;
	}

	public void setInwardsQuantity(BigDecimal inwardsQuantity) {
		this.inwardsQuantity = inwardsQuantity;
	}

	public BigDecimal getInwardsRate() {
		return inwardsRate;
	}

	public void setInwardsRate(BigDecimal inwardsRate) {
		this.inwardsRate = inwardsRate;
	}

	public BigDecimal getInwardsValue() {
		return inwardsValue;
	}

	public void setInwardsValue(BigDecimal inwardsValue) {
		this.inwardsValue = inwardsValue;
	}

	public BigDecimal getOutwardsQuantity() {
		return outwardsQuantity;
	}

	public void setOutwardsQuantity(BigDecimal outwardsQuantity) {
		this.outwardsQuantity = outwardsQuantity;
	}

	public BigDecimal getOutwardsRate() {
		return outwardsRate;
	}

	public void setOutwardsRate(BigDecimal outwardsRate) {
		this.outwardsRate = outwardsRate;
	}

	public BigDecimal getOutwardsValue() {
		return outwardsValue;
	}

	public void setOutwardsValue(BigDecimal outwardsValue) {
		this.outwardsValue = outwardsValue;
	}

	public BigDecimal getClosingQuantity() {
		return closingQuantity;
	}

	public void setClosingQuantity(BigDecimal closingQuantity) {
		this.closingQuantity = closingQuantity;
	}

	public BigDecimal getClosingRate() {
		return closingRate;
	}

	public void setClosingRate(BigDecimal closingRate) {
		this.closingRate = closingRate;
	}

	public BigDecimal getClosingValue() {
		return closingValue;
	}

	public void setClosingValue(BigDecimal closingValue) {
		this.closingValue = closingValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public BigDecimal getOpeningQuantity() {
		return openingQuantity;
	}

	public void setOpeningQuantity(BigDecimal openingQuantity) {
		this.openingQuantity = openingQuantity;
	}

	public BigDecimal getOpeningValue() {
		return openingValue;
	}

	public void setOpeningValue(BigDecimal openingValue) {
		this.openingValue = openingValue;
	}

	public BigDecimal getOpeningRate() {
		return openingRate;
	}

	public void setOpeningRate(BigDecimal openingRate) {
		this.openingRate = openingRate;
	}
}
