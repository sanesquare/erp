package com.inventory.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.erp.web.entities.Godown;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 27, 2015
 */
@Entity
@Table(name = "inventory_transactions")
public class InventoryTransaction implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -391599627415197479L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@OneToOne
	private InventoryEntryItems entryItems;
	
	@OneToOne
	private InventoryWithdrawalItems withdrawalItems;
	
	@ManyToOne
	private Product product;
	
	@ManyToOne
	private Company company;
	
	@ManyToOne
	private Godown godown;
	
	@Column(name="quantity_plus")
	private BigDecimal quantityPlus;
	
	@Column(name="quantity_minus")
	private BigDecimal quantityMinus;
	
	@Column(name="unitPrice")
	private BigDecimal unitPrice;
	
	@Column(name="total_amount")
	private BigDecimal totalAmount;
	
	@Column(name="date")
	private Date date;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public InventoryEntryItems getEntryItems() {
		return entryItems;
	}

	public void setEntryItems(InventoryEntryItems entryItems) {
		this.entryItems = entryItems;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Godown getGodown() {
		return godown;
	}

	public void setGodown(Godown godown) {
		this.godown = godown;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getQuantityPlus() {
		return quantityPlus;
	}

	public void setQuantityPlus(BigDecimal quantityPlus) {
		this.quantityPlus = quantityPlus;
	}

	public BigDecimal getQuantityMinus() {
		return quantityMinus;
	}

	public void setQuantityMinus(BigDecimal quantityMinus) {
		this.quantityMinus = quantityMinus;
	}

	public InventoryWithdrawalItems getWithdrawalItems() {
		return withdrawalItems;
	}

	public void setWithdrawalItems(InventoryWithdrawalItems withdrawalItems) {
		this.withdrawalItems = withdrawalItems;
	}
}
