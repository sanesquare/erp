package com.inventory.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.erp.web.entities.Godown;
import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-August-2015
 *
 */
@Entity
@Table(name="picking_list_items")
public class PickingListItems implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4025641130515572197L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private PickingList pickingList;
	
	@Column(name="description" , length=1000)
	private String description;
	
	@Column(name="packing_quantity")
	private BigDecimal packingQuantity;
	
	@Column(name="quantity")
	private BigDecimal quantity;
	
	@Column(name="price")
	private BigDecimal price;
	
	@Column(name="discount_percentage")
	private BigDecimal discountPercentage;
	
	@ManyToOne
	private Product product;
	
	@Column(name="total")
	private BigDecimal total;
	
	@ManyToOne
	private VAT vat;
	
	@ManyToOne
	private Godown godown;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
