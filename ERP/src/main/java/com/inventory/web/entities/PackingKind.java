package com.inventory.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
@Entity
@Table(name="packing_kind")
public class PackingKind implements AuditEntity,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3231740160926629732L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	private Company company;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@Column(name="packing_name")
	private String name;
	
	@Column(name="weight")
	private BigDecimal weight;
	
	@OneToMany(mappedBy="packingKind")
	private Set<PackingListItems> packingListItems = new HashSet<PackingListItems>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<PackingListItems> getPackingListItems() {
		return packingListItems;
	}

	public void setPackingListItems(Set<PackingListItems> packingListItems) {
		this.packingListItems = packingListItems;
	}
}
