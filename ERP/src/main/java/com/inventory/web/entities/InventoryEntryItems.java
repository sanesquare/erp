package com.inventory.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-August-2015
 *
 */
@Entity
@Table(name = "inventory_entry_items")
public class InventoryEntryItems implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3294404638677707263L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private InventoryEntry inventoryEntry;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@OneToOne(mappedBy="entryItems",cascade=CascadeType.ALL)
	private InventoryTransaction transaction;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public InventoryEntry getInventoryEntry() {
		return inventoryEntry;
	}

	public void setInventoryEntry(InventoryEntry inventoryEntry) {
		this.inventoryEntry = inventoryEntry;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public InventoryTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(InventoryTransaction transaction) {
		this.transaction = transaction;
	}
}
