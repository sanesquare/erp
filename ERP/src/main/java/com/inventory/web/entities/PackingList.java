package com.inventory.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Repository;

import com.erp.web.entities.Company;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.sales.web.entities.Invoice;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
@Entity
@Table(name="packing_list")
public class PackingList implements AuditEntity,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -50909369417252196L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@ManyToOne
	private Invoice invoice;
	
	@ManyToOne
	private Company company;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="gross_weight")
	private BigDecimal grossWeight;
	
	@Column(name="number_of_packs")
	private Long numberOfPacks;
	
	@Column(name="net_weight")
	private BigDecimal netWeight;
	
	@Column(name="reference_number")
	private String referenceNumber;
	
	@OneToMany(mappedBy="packingList",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<PackingListItems> packingListItems = new ArrayList<PackingListItems>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}

	public BigDecimal getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public List<PackingListItems> getPackingListItems() {
		return packingListItems;
	}

	public void setPackingListItems(List<PackingListItems> packingListItems) {
		this.packingListItems = packingListItems;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getNumberOfPacks() {
		return numberOfPacks;
	}

	public void setNumberOfPacks(Long numberOfPacks) {
		this.numberOfPacks = numberOfPacks;
	}
}
