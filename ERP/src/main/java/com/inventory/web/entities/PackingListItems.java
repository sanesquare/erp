package com.inventory.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Repository;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
@Entity
@Table(name="packing_list_items")
public class PackingListItems implements AuditEntity,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4478012896670945707L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@ManyToOne
	private Product product;
	
	@Column(name="quantity")
	private BigDecimal quantity;
	
	@Column(name="gross_weight")
	private BigDecimal grossWeight;
	
	@Column(name="number_of_pack")
	private Long numberOfPack;
	
	@Column(name="net_weight")
	private BigDecimal netWeight;
	
	@ManyToOne
	private PackingList packingList;
	
	@ManyToOne
	private PackingKind packingKind;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}

	public BigDecimal getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}

	public PackingList getPackingList() {
		return packingList;
	}

	public void setPackingList(PackingList packingList) {
		this.packingList = packingList;
	}

	public PackingKind getPackingKind() {
		return packingKind;
	}

	public void setPackingKind(PackingKind packingKind) {
		this.packingKind = packingKind;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getNumberOfPack() {
		return numberOfPack;
	}

	public void setNumberOfPack(Long numberOfPack) {
		this.numberOfPack = numberOfPack;
	}
}
