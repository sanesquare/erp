package com.inventory.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-August-2015
 *
 */
@Entity
@Table(name="inventory_count_sheet_items")
public class InventoryCountSheetItems implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4716691898700027879L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private InventoryCountSheet countSheet;
	
	@ManyToOne
	private Product product;
	
	@Column(name="theoretical_inventory")
	private BigDecimal theoreticalInventory;
	
	@Column(name="correct_inventory")
	private BigDecimal correctInventory;
	
	@Column(name="wap")
	private BigDecimal wap;
	
	@Column(name="adjusted_wap")
	private BigDecimal adjustedWap;
	
	@Column(name="gap")
	private BigDecimal gap;
	
	@Column(name="total_wap")
	private BigDecimal totalWap;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
