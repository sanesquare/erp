package com.inventory.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.sales.web.entities.Customer;
import com.sales.web.entities.CustomerContact;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.SalesOrder;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-August-2015
 *
 */
@Entity
@Table(name = "delivery_note")
public class DeliveryNote implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5946468150571625334L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Customer customer;

	@ManyToOne
	private CustomerContact customerContact;

	@ManyToOne
	private DeliveryMethod deliveryMethod;

	@Column(name = "reference_number")
	private String referenceNumber;
	
	@Column(name = "date")
	private Date date;

	@Column(name = "notes", length = 1000)
	private String notees;

	@OneToMany(mappedBy="deliveryNote",cascade=CascadeType.ALL,orphanRemoval=true)
	private Set<DeliveryNoteItems> items=new HashSet<DeliveryNoteItems>();
	
	@ManyToOne
	private Company company;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@ManyToOne
	private Invoice invoice;

	@OneToOne
	private SalesOrder salesOrder;
	
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public CustomerContact getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(CustomerContact customerContact) {
		this.customerContact = customerContact;
	}

	public DeliveryMethod getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(DeliveryMethod deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNotees() {
		return notees;
	}

	public void setNotees(String notees) {
		this.notees = notees;
	}


	public Set<DeliveryNoteItems> getItems() {
		return items;
	}

	public void setItems(Set<DeliveryNoteItems> items) {
		this.items = items;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}


	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SalesOrder getSalesOrder() {
		return salesOrder;
	}

	public void setSalesOrder(SalesOrder salesOrder) {
		this.salesOrder = salesOrder;
	}
}
