package com.inventory.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.crm.web.entities.OpportunityItems;
import com.erp.web.entities.Company;
import com.erp.web.entities.Godown;
import com.erp.web.entities.StockGroup;
import com.erp.web.entities.VendorCreditMemoItems;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.purchase.web.entities.BillItems;
import com.purchase.web.entities.PurchaseOrderItems;
import com.purchase.web.entities.PurchaseReturnItems;
import com.purchase.web.entities.VendorQuoteItems;
import com.sales.web.entities.CustomerCreditMemoItems;
import com.sales.web.entities.InvoiceItems;
import com.sales.web.entities.InvoiceProducts;
import com.sales.web.entities.SalesOrderItems;
import com.sales.web.entities.SalesReturnItems;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name = "product")
public class Product implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7269440042318448063L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "selling_price")
	private BigDecimal sellingPrice;

	@Column(name = "purchase_price")
	private BigDecimal purchasePrice;

	@Column(name = "product_code")
	private String productCode;

	@ManyToOne
	private StockGroup stockGroup;

	@Column(name = "min_staock_quantity")
	private BigDecimal minimumStockQuantity;

	@ManyToOne
	private Godown receivingLocation;

	@ManyToOne
	private Godown deliveryLocation;

	@OneToMany(mappedBy = "product")
	private Set<BillItems> billItems = new HashSet<BillItems>();

	@OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
	private ProductAdvanceDetails advanceDetails;

	@OneToMany(mappedBy = "product")
	private Set<VendorQuoteItems> vendorQuoteItems=new HashSet<VendorQuoteItems>();

	@OneToMany(mappedBy = "product")
	private Set<PurchaseOrderItems> purcahzseOrderItems;

	@OneToMany(mappedBy = "product")
	private Set<PurchaseReturnItems> purchaseReturnItems;
	
	@OneToMany(mappedBy = "product")
	private Set<SalesReturnItems> salesReturnItems;

	@OneToMany(mappedBy = "product")
	private Set<VendorCreditMemoItems> vendorCreditMemoItems;

	@OneToMany(mappedBy = "product")
	private Set<SalesOrderItems> salesOrderItems;

	@OneToMany(mappedBy = "product")
	private Set<InvoiceProducts> invoiceProducts;

	@OneToMany(mappedBy = "product")
	private Set<CustomerCreditMemoItems> customerCreditMemoItems;

	@OneToMany(mappedBy = "product")
	private Set<InventoryTransferItems> transferItems;

	@OneToMany(mappedBy = "countSheet")
	private Set<InventoryCountSheetItems> countSheetItems;

	@OneToMany(mappedBy = "product")
	private Set<PickingListItems> pickingListItems;

	@OneToMany(mappedBy = "product")
	private Set<DeliveryNoteItems> deliveryNoteItems;

	@OneToMany(mappedBy = "product")
	private Set<ReceivingNoteItems> receivingNoteItems;

	@OneToMany(mappedBy = "product")
	private Set<OpportunityItems> opportunityItems;

	@OneToMany(mappedBy="product",cascade=CascadeType.ALL,orphanRemoval=true)
	private Set<InventoryTransaction> inventoryTransactions=new HashSet<InventoryTransaction>();
	
	@OneToMany(mappedBy="product")
	private Set<InvoiceItems> invoiceItems = new HashSet<InvoiceItems>();
	
	@ManyToOne
	private Company company;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@OneToMany(mappedBy="product",cascade=CascadeType.ALL,orphanRemoval=true)
	private Set<PackingListItems> packingListItems = new HashSet<PackingListItems>();
	
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public StockGroup getStockGroup() {
		return stockGroup;
	}

	public void setStockGroup(StockGroup stockGroup) {
		this.stockGroup = stockGroup;
	}

	public BigDecimal getMinimumStockQuantity() {
		return minimumStockQuantity;
	}

	public void setMinimumStockQuantity(BigDecimal minimumStockQuantity) {
		this.minimumStockQuantity = minimumStockQuantity;
	}

	public Godown getReceivingLocation() {
		return receivingLocation;
	}

	public void setReceivingLocation(Godown receivingLocation) {
		this.receivingLocation = receivingLocation;
	}

	public Godown getDeliveryLocation() {
		return deliveryLocation;
	}

	public void setDeliveryLocation(Godown deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}

	public Set<BillItems> getBillItems() {
		return billItems;
	}

	public void setBillItems(Set<BillItems> billItems) {
		this.billItems = billItems;
	}

	public ProductAdvanceDetails getAdvanceDetails() {
		return advanceDetails;
	}

	public void setAdvanceDetails(ProductAdvanceDetails advanceDetails) {
		this.advanceDetails = advanceDetails;
	}

	public Set<VendorQuoteItems> getVendorQuoteItems() {
		return vendorQuoteItems;
	}

	public void setVendorQuoteItems(Set<VendorQuoteItems> vendorQuoteItems) {
		this.vendorQuoteItems = vendorQuoteItems;
	}

	public Set<PurchaseOrderItems> getPurcahzseOrderItems() {
		return purcahzseOrderItems;
	}

	public void setPurcahzseOrderItems(Set<PurchaseOrderItems> purcahzseOrderItems) {
		this.purcahzseOrderItems = purcahzseOrderItems;
	}

	public Set<PurchaseReturnItems> getPurchaseReturnItems() {
		return purchaseReturnItems;
	}

	public void setPurchaseReturnItems(Set<PurchaseReturnItems> purchaseReturnItems) {
		this.purchaseReturnItems = purchaseReturnItems;
	}

	public Set<VendorCreditMemoItems> getVendorCreditMemoItems() {
		return vendorCreditMemoItems;
	}

	public void setVendorCreditMemoItems(Set<VendorCreditMemoItems> vendorCreditMemoItems) {
		this.vendorCreditMemoItems = vendorCreditMemoItems;
	}

	public Set<SalesOrderItems> getSalesOrderItems() {
		return salesOrderItems;
	}

	public void setSalesOrderItems(Set<SalesOrderItems> salesOrderItems) {
		this.salesOrderItems = salesOrderItems;
	}

	public Set<InvoiceProducts> getInvoiceProducts() {
		return invoiceProducts;
	}

	public void setInvoiceProducts(Set<InvoiceProducts> invoiceProducts) {
		this.invoiceProducts = invoiceProducts;
	}

	public Set<CustomerCreditMemoItems> getCustomerCreditMemoItems() {
		return customerCreditMemoItems;
	}

	public void setCustomerCreditMemoItems(Set<CustomerCreditMemoItems> customerCreditMemoItems) {
		this.customerCreditMemoItems = customerCreditMemoItems;
	}


	public Set<InventoryTransferItems> getTransferItems() {
		return transferItems;
	}

	public void setTransferItems(Set<InventoryTransferItems> transferItems) {
		this.transferItems = transferItems;
	}

	public Set<InventoryCountSheetItems> getCountSheetItems() {
		return countSheetItems;
	}

	public void setCountSheetItems(Set<InventoryCountSheetItems> countSheetItems) {
		this.countSheetItems = countSheetItems;
	}

	public Set<PickingListItems> getPickingListItems() {
		return pickingListItems;
	}

	public void setPickingListItems(Set<PickingListItems> pickingListItems) {
		this.pickingListItems = pickingListItems;
	}

	public Set<DeliveryNoteItems> getDeliveryNoteItems() {
		return deliveryNoteItems;
	}

	public void setDeliveryNoteItems(Set<DeliveryNoteItems> deliveryNoteItems) {
		this.deliveryNoteItems = deliveryNoteItems;
	}

	public Set<ReceivingNoteItems> getReceivingNoteItems() {
		return receivingNoteItems;
	}

	public void setReceivingNoteItems(Set<ReceivingNoteItems> receivingNoteItems) {
		this.receivingNoteItems = receivingNoteItems;
	}

	public Set<OpportunityItems> getOpportunityItems() {
		return opportunityItems;
	}

	public void setOpportunityItems(Set<OpportunityItems> opportunityItems) {
		this.opportunityItems = opportunityItems;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<InventoryTransaction> getInventoryTransactions() {
		return inventoryTransactions;
	}

	public void setInventoryTransactions(Set<InventoryTransaction> inventoryTransactions) {
		this.inventoryTransactions = inventoryTransactions;
	}

	public Set<InvoiceItems> getInvoiceItems() {
		return invoiceItems;
	}

	public void setInvoiceItems(Set<InvoiceItems> invoiceItems) {
		this.invoiceItems = invoiceItems;
	}

	public Set<SalesReturnItems> getSalesReturnItems() {
		return salesReturnItems;
	}

	public void setSalesReturnItems(Set<SalesReturnItems> salesReturnItems) {
		this.salesReturnItems = salesReturnItems;
	}

	public Set<PackingListItems> getPackingListItems() {
		return packingListItems;
	}

	public void setPackingListItems(Set<PackingListItems> packingListItems) {
		this.packingListItems = packingListItems;
	}
}
