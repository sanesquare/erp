package com.inventory.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.erp.web.entities.Godown;
import com.erp.web.entities.InventoryWithdrawalType;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.purchase.web.entities.PurchaseReturnItems;
import com.sales.web.entities.InvoiceItems;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-August-2015
 *
 */
@Entity
@Table(name = "inventory_withdrawal")
public class InventoryWithdrawal implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3558864163864182288L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="date")
	private Date date;
	
	@ManyToOne
	private Godown godown;
	
	@Column(name="label")
	private String label;
	
	@ManyToOne
	private InventoryWithdrawalType withdrawalType;
	
	@Column(name = "description")
	private String description;
	
	@ManyToOne
	private Company company;

	@Column(name = "type")
	private String type;

	@Column(name = "code")
	private String code;
	
	@OneToMany(mappedBy="inventoryWithdrawal",cascade=CascadeType.ALL,orphanRemoval=true)
	private Set<InventoryWithdrawalItems> withdrawalItems=new HashSet<InventoryWithdrawalItems>();
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@OneToOne
	private InvoiceItems invoiceItems;
	
	@OneToOne
	private PurchaseReturnItems purchaseReturnItems;
	
	/**
	 * 
	 */
	@OneToOne
	private DeliveryNoteItems deliveryNoteItems;
	

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Godown getGodown() {
		return godown;
	}

	public void setGodown(Godown godown) {
		this.godown = godown;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public InventoryWithdrawalType getWithdrawalType() {
		return withdrawalType;
	}

	public void setWithdrawalType(InventoryWithdrawalType withdrawalType) {
		this.withdrawalType = withdrawalType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<InventoryWithdrawalItems> getWithdrawalItems() {
		return withdrawalItems;
	}

	public void setWithdrawalItems(Set<InventoryWithdrawalItems> withdrawalItems) {
		this.withdrawalItems = withdrawalItems;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public InvoiceItems getInvoiceItems() {
		return invoiceItems;
	}

	public void setInvoiceItems(InvoiceItems invoiceItems) {
		this.invoiceItems = invoiceItems;
	}

	public DeliveryNoteItems getDeliveryNoteItems() {
		return deliveryNoteItems;
	}

	public void setDeliveryNoteItems(DeliveryNoteItems deliveryNoteItems) {
		this.deliveryNoteItems = deliveryNoteItems;
	}

	public PurchaseReturnItems getPurchaseReturnItems() {
		return purchaseReturnItems;
	}

	public void setPurchaseReturnItems(PurchaseReturnItems purchaseReturnItems) {
		this.purchaseReturnItems = purchaseReturnItems;
	}

}
