package com.inventory.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.erp.web.entities.Godown;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.purchase.web.entities.BillItems;
import com.sales.web.entities.SalesReturnItems;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-August-2015
 *
 */
@Entity
@Table(name = "inventory_entry")
public class InventoryEntry implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8924029383821582160L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "date")
	private Date date;

	@ManyToOne
	private Godown godown;

	@Column(name = "description")
	private String description;

	@OneToMany(mappedBy = "inventoryEntry",cascade=CascadeType.ALL,orphanRemoval=true)
	private Set<InventoryEntryItems> inventoryEntryItems=new HashSet<InventoryEntryItems>();

	@ManyToOne
	private Company company;

	@Column(name = "type")
	private String type;

	@Column(name = "code")
	private String code;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@OneToOne
	private BillItems billItems;
	
	@OneToOne
	private SalesReturnItems salesReturnItems;
	
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Godown getGodown() {
		return godown;
	}

	public void setGodown(Godown godown) {
		this.godown = godown;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<InventoryEntryItems> getInventoryEntryItems() {
		return inventoryEntryItems;
	}

	public void setInventoryEntryItems(Set<InventoryEntryItems> inventoryEntryItems) {
		this.inventoryEntryItems = inventoryEntryItems;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public BillItems getBillItems() {
		return billItems;
	}

	public void setBillItems(BillItems billItems) {
		this.billItems = billItems;
	}

	public SalesReturnItems getSalesReturnItems() {
		return salesReturnItems;
	}

	public void setSalesReturnItems(SalesReturnItems salesReturnItems) {
		this.salesReturnItems = salesReturnItems;
	}
}
