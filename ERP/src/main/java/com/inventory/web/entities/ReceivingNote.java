package com.inventory.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.PurchaseTypes;
import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.Vendor;
import com.purchase.web.entities.VendorContacts;
import com.purchase.web.entities.VendorQuotes;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 01-August-2015
 *
 */

@Entity
@Table(name = "receiving_note")
public class ReceivingNote implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6777967403244322746L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Vendor vendor;

	@ManyToOne
	private VendorContacts vendorContacts;

	@Column(name = "date")
	private Date date;

	@Column(name = "reference_number")
	private String referenceNumber;

	@Column(name = "notes", length = 1000)
	private String notees;

	@Column(name = "delivery_date")
	private Date deliveryDate;

	@Column(name = "expected_delivery_date")
	private Date expectedDeliveryDate;

	@ManyToOne
	private PurchaseTypes purchaseTypes;

	@OneToMany(mappedBy = "receivingNote", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<ReceivingNoteItems> items = new HashSet<ReceivingNoteItems>();

	@ManyToOne
	private Company company;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@ManyToOne
	private Bill bill;

	@ManyToOne
	private VendorQuotes purchaseOrder;
	
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public VendorContacts getVendorContacts() {
		return vendorContacts;
	}

	public void setVendorContacts(VendorContacts vendorContacts) {
		this.vendorContacts = vendorContacts;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNotees() {
		return notees;
	}

	public void setNotees(String notees) {
		this.notees = notees;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Date getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public PurchaseTypes getPurchaseTypes() {
		return purchaseTypes;
	}

	public void setPurchaseTypes(PurchaseTypes purchaseTypes) {
		this.purchaseTypes = purchaseTypes;
	}

	public Set<ReceivingNoteItems> getItems() {
		return items;
	}

	public void setItems(Set<ReceivingNoteItems> items) {
		this.items = items;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public VendorQuotes getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(VendorQuotes purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}
}
