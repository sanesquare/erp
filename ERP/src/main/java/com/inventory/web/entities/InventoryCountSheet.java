package com.inventory.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.erp.web.entities.Godown;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-August_2015
 *
 */
@Entity
@Table(name="inventory_count_sheet")
public class InventoryCountSheet implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2021915197518254074L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="inventory_name")
	private String inventoryName;
	
	@Column(name="date")
	private Date date;
	
	@ManyToOne
	private Godown godown;

	@OneToMany(mappedBy="countSheet")
	private Set<InventoryCountSheetItems>countSheetItems;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
