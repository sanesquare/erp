package com.inventory.web.service;

import java.util.List;

import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.vo.InventoryWithdrawalVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 27, 2015
 */
public interface InventoryWithdrawalService {

	/**
	 * method to save inventory withdrawal
	 * 
	 * @param vo
	 * @return
	 */
	public Long saveInventoryWithdrawal(InventoryWithdrawalVo vo);


	/**
	 * method to delete inventory withdrawal
	 * 
	 * @param id
	 */
	public void deleteInventoryWithdrawal(Long id);

	/**
	 * method to list all inventory withdrawals
	 * 
	 * @param companyId
	 * @return
	 */
	public List<InventoryWithdrawalVo> listAllInventoryWithdrawals(Long companyId);

	public String getNumber(Long companyId, String category);

	/**
	 * method to find inventory withdrawal
	 * 
	 * @param id
	 * @return
	 */
	public InventoryWithdrawalVo findInventoryWithdrawalById(Long id);

	/**
	 * method to find all withdrawals
	 * 
	 * @param companyId
	 * @return
	 */
	public List<InventoryWithdrawalVo> findAllWithdrawals(Long companyId);

}
