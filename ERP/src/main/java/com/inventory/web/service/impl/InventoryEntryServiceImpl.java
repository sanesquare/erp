package com.inventory.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.web.dao.InventoryEntryDao;
import com.inventory.web.service.InventoryEntryService;
import com.inventory.web.vo.InventoryEntryVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 26, 2015
 */
@Service
public class InventoryEntryServiceImpl implements InventoryEntryService {

	@Autowired
	private InventoryEntryDao dao;

	public Long saveInventoryEntry(InventoryEntryVo vo) {
		return dao.saveInventoryEntry(vo);
	}

	public void deleteInventoryEntry(Long id) {
		dao.deleteInventoryEntry(id);
	}

	public List<InventoryEntryVo> listAllInventoryEntries(Long companyId) {
		return dao.listAllInventoryEntries(companyId);
	}

	public String getNumber(Long companyId, String category) {
		return dao.getNumber(companyId, category);
	}

	public InventoryEntryVo findInventoryEntryById(Long id) {
		return dao.findInventoryEntryById(id);
	}

	public List<InventoryEntryVo> findAllEntries(Long companyId) {
		return dao.findAllEntries(companyId);
	}

}
