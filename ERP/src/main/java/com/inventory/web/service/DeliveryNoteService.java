package com.inventory.web.service;

import java.util.List;

import com.inventory.web.vo.DeliveryNoteVo;
import com.sales.web.vo.InvoiceVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 29, 2015
 */
public interface DeliveryNoteService {

	/**
	 * Method to save or update delivery note
	 * @param noteVo
	 */
	public Long saveOrUpdateDeliveryNotes(DeliveryNoteVo noteVo);
	
	/**
	 * Method to delete delivery note
	 * @param id
	 */
	public void deleteDeliveryNote(Long id);
	
	/**
	 * method to save delivery note
	 * @param noteVo
	 * @return
	 */
	public Long saveOrUpdateDeliveryNotesFromInvoice(DeliveryNoteVo noteVo);
	
	/**
	 * Method to find delivery note
	 * @param id
	 * @return
	 */
	public DeliveryNoteVo findDeliveryNote(Long id);
	
	/**
	 * Method to list delivery notes for company
	 * @param companyId
	 * @return
	 */
	public List<DeliveryNoteVo> listDeliveryNotesForCompany(Long companyId);
	
	/**
	 * Method to list delivery notes for company within date range
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<DeliveryNoteVo> listDeliveryNotesForCompanyWithinDateRange(Long companyId , String startDate , String endDate);
	
	/**
	 * method to convert delivery notes to invoice
	 * @param deliveryNoteIds
	 * @return
	 */
	public InvoiceVo convertDeliveryNotesToInvoice(List<Long> deliveryNoteIds);
}
