package com.inventory.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.web.dao.ReceivingNotesDao;
import com.inventory.web.service.ReceivingNotesService;
import com.inventory.web.vo.ReceivingNoteVo;
import com.purchase.web.vo.BillVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 22, 2015
 */
@Service
public class ReceivingNotesServiceImpl implements ReceivingNotesService {

	@Autowired
	private ReceivingNotesDao dao;
	
	public Long saveOrUpdateReceivingNotes(ReceivingNoteVo noteVo) {
		return dao.saveOrUpdateReceivingNotes(noteVo);
	}

	public void deleteReceivingNote(Long id) {

		dao.deleteReceivingNote(id);
	}

	public ReceivingNoteVo findReceivingNote(Long id) {
		return dao.findReceivingNote(id);
	}

	public List<ReceivingNoteVo> listReceivingNotesForCompany(Long companyId) {
		return dao.listReceivingNotesForCompany(companyId);
	}

	public List<ReceivingNoteVo> listReceivingNotesForCompanyWithinDateRange(Long companyId, String startDate,
			String endDate) {
		return dao.listReceivingNotesForCompanyWithinDateRange(companyId, startDate, endDate);
	}

	public BillVo convertReceivingNoteToBill(List<Long> receivingNoteIds) {
		return dao.convertReceivingNoteToBill(receivingNoteIds);
	}

}
