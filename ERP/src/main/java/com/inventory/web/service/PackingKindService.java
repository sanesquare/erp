package com.inventory.web.service;

import java.util.List;

import com.inventory.web.vo.PackingKindVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
public interface PackingKindService {

	/**
	 * method to save packing kind
	 * @param vo
	 */
	public void savePackingKing(PackingKindVo vo);
	
	/**
	 * method to delete packing kind
	 * @param id
	 */
	public void deletePackingKing(Long id);
	
	/**
	 * method to find packing kinds
	 * @param companyId
	 * @return
	 */
	public List<PackingKindVo> findAllPackingKinds(Long companyId);
	
	/**
	 * method to find packing kinds
	 * @param id
	 * @return
	 */
	public PackingKindVo findPackingKind(Long id);
}
