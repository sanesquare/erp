package com.inventory.web.service;

import java.util.List;

import com.inventory.web.vo.GodownVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 21, 2015
 */
public interface GodownService {

	/**
	 * Method to save or upate godown
	 * 
	 * @param godownVo
	 * @return
	 */
	public void saveOrUpdateGodown(GodownVo godownVo);

	/**
	 * Method to delete godown
	 * 
	 * @param id
	 */
	public void deleteGodown(Long id);

	/**
	 * Method to list all godowns for company
	 * 
	 * @param companyId
	 * @return
	 */
	public List<GodownVo> listAllGodownsForCompany(Long companyId);

	/**
	 * Method to find godown
	 * 
	 * @param id
	 * @return
	 */
	public GodownVo findGodown(Long id);

}
