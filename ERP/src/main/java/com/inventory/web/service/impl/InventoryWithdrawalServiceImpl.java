package com.inventory.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.web.dao.InventoryWithdrawalDao;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.service.InventoryWithdrawalService;
import com.inventory.web.vo.InventoryWithdrawalVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 27, 2015
 */
@Service
public class InventoryWithdrawalServiceImpl implements InventoryWithdrawalService {

	@Autowired
	private InventoryWithdrawalDao dao;

	public Long saveInventoryWithdrawal(InventoryWithdrawalVo vo) {
		return dao.saveInventoryWithdrawal(vo);
	}

	public void deleteInventoryWithdrawal(Long id) {
		dao.deleteInventoryWithdrawal(id);
	}

	public List<InventoryWithdrawalVo> listAllInventoryWithdrawals(Long companyId) {
		return dao.listAllInventoryWithdrawals(companyId);
	}

	public String getNumber(Long companyId, String category) {
		return dao.getNumber(companyId, category);
	}

	public InventoryWithdrawalVo findInventoryWithdrawalById(Long id) {
		return dao.findInventoryWithdrawalById(id);
	}

	public List<InventoryWithdrawalVo> findAllWithdrawals(Long companyId) {
		return dao.findAllWithdrawals(companyId);
	}

}
