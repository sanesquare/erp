package com.inventory.web.service;

import java.util.List;

import com.inventory.web.vo.ReceivingNoteVo;
import com.purchase.web.vo.BillVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 22, 2015
 */
public interface ReceivingNotesService {

	/**
	 * Method to save or update receiving note
	 * @param noteVo
	 */
	public Long saveOrUpdateReceivingNotes(ReceivingNoteVo noteVo);
	
	/**
	 * Method to delete receiving note
	 * @param id
	 */
	public void deleteReceivingNote(Long id);
	
	/**
	 * Method to find receiving note
	 * @param id
	 * @return
	 */
	public ReceivingNoteVo findReceivingNote(Long id);
	
	/**
	 * Method to list receiving notes for company
	 * @param companyId
	 * @return
	 */
	public List<ReceivingNoteVo> listReceivingNotesForCompany(Long companyId);
	
	/**
	 * Method to list receiving notes for company within date range
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReceivingNoteVo> listReceivingNotesForCompanyWithinDateRange(Long companyId , String startDate , String endDate);
	
	/**
	 * method to convert receiving notes to bill
	 * @param receivingNoteIds
	 * @return
	 */
	public BillVo convertReceivingNoteToBill(List<Long> receivingNoteIds);
	
}
