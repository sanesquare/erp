package com.inventory.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.web.dao.PackingListdao;
import com.inventory.web.service.PackingListService;
import com.inventory.web.vo.PackingListVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
@Service
public class PackingListServiceImpl implements PackingListService {

	@Autowired
	private PackingListdao listdao;

	public Long savePackingList(PackingListVo vo) {
		return listdao.savePackingList(vo);
	}

	public void deletePackingList(Long id) {
		listdao.deletePackingList(id);
	}

	public List<PackingListVo> findAllPackingLists(Long companyId) {
		return listdao.findAllPackingLists(companyId);
	}

	public PackingListVo findPackingListById(Long id) {
		return listdao.findPackingListById(id);
	}

	public PackingListVo convertInvoiceToPackingList(Long invoiceId) {
		return listdao.convertInvoiceToPackingList(invoiceId);
	}

}
