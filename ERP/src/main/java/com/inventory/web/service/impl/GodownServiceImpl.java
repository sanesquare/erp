package com.inventory.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.web.dao.GodownDao;
import com.inventory.web.service.GodownService;
import com.inventory.web.vo.GodownVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 21, 2015
 */
@Service
public class GodownServiceImpl implements GodownService {

	@Autowired
	private GodownDao dao;

	public void saveOrUpdateGodown(GodownVo godownVo) {
		 dao.saveOrUpdateGodown(godownVo);
	}

	public void deleteGodown(Long id) {
		dao.deleteGodown(id);
	}

	public List<GodownVo> listAllGodownsForCompany(Long companyId) {
		return dao.listAllGodownsForCompany(companyId);
	}

	public GodownVo findGodown(Long id) {
		return dao.findGodown(id);
	}

}
