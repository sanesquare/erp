package com.inventory.web.service;

import java.util.List;

import com.inventory.web.vo.StockViewVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 5, 2015
 */
public interface InventoryTransactionService {

	/**
	 * method to find stock summary
	 * @param companyId
	 * @return
	 */
	public List<StockViewVo> findStockSummary(Long companyId,String startDate,String endDate);
}
