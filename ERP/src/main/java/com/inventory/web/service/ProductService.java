package com.inventory.web.service;

import java.util.List;

import com.inventory.web.entities.Product;
import com.inventory.web.vo.ProductVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 23, 2015
 */
public interface ProductService {

	/**
	 * method to save or update product
	 * @param productVo
	 */
	public Long saveOrUpdateProduct(ProductVo productVo);
	
	/**
	 * method to delete product
	 * @param id
	 */
	public void deleteProduct(Long id);
	
	/**
	 * method to delete product
	 * @param code
	 */
	public void deleteProduct(String code,Long companyId);
	
	
	/**
	 * method to find product by id
	 * @param id
	 * @return
	 */
	public ProductVo findProductById(Long id);
	
	/**
	 * method to find product by code
	 * @param code
	 * @return
	 */
	public ProductVo findProductByCode(String code,Long companyId);
	
	/**
	 * method to find all products
	 * @param companyId
	 * @return
	 */
	public List<ProductVo> findAllProducts(Long companyId);
	
	public List<ProductVo> findProducts(List<Long> ids);
}
