package com.inventory.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.web.dao.InventoryTransactionDao;
import com.inventory.web.service.InventoryTransactionService;
import com.inventory.web.vo.StockViewVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 5, 2015
 */
@Service
public class InventoryTransactionServiceImpl implements InventoryTransactionService{

	@Autowired
	private InventoryTransactionDao dao;

	public List<StockViewVo> findStockSummary(Long companyId,String startDate,String endDate) {
		return dao.findStockSummary(companyId,startDate,endDate);
	}
}
