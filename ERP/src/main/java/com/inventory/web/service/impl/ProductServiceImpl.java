package com.inventory.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.web.dao.ProductDao;
import com.inventory.web.service.ProductService;
import com.inventory.web.vo.ProductVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 23, 2015
 */
@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductDao dao;
	
	public Long saveOrUpdateProduct(ProductVo productVo) {
		return dao.saveOrUpdateProduct(productVo);
	}

	public void deleteProduct(Long id) {
		dao.deleteProduct(id);
	}

	public void deleteProduct(String code, Long companyId) {
		dao.deleteProduct(code,companyId);
	}

	public ProductVo findProductById(Long id) {
		return dao.findProductById(id);
	}

	public ProductVo findProductByCode(String code, Long companyId) {
		return dao.findProductByCode(code, companyId);
	}

	public List<ProductVo> findAllProducts(Long companyId) {
		return dao.findAllProducts(companyId);
	}

	public List<ProductVo> findProducts(List<Long> ids) {
		return dao.findProducts(ids);
	}

}
