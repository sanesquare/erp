package com.inventory.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.web.dao.PackingKindDao;
import com.inventory.web.service.PackingKindService;
import com.inventory.web.vo.PackingKindVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
@Service
public class PackingKindServiceImpl implements PackingKindService {

	@Autowired
	private PackingKindDao dao;

	public void savePackingKing(PackingKindVo vo) {
		dao.savePackingKing(vo);
	}

	public void deletePackingKing(Long id) {
		dao.deletePackingKing(id);
	}

	public List<PackingKindVo> findAllPackingKinds(Long companyId) {
		return dao.findAllPackingKinds(companyId);
	}

	public PackingKindVo findPackingKind(Long id) {
		return dao.findPackingKind(id);
	}

}
