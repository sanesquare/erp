package com.inventory.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.web.dao.DeliveryNoteDao;
import com.inventory.web.service.DeliveryNoteService;
import com.inventory.web.vo.DeliveryNoteVo;
import com.sales.web.vo.InvoiceVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 29, 2015
 */
@Service
public class DeliveryNoteServiceImpl implements DeliveryNoteService{

	@Autowired
	private DeliveryNoteDao dao;

	public Long saveOrUpdateDeliveryNotes(DeliveryNoteVo noteVo) {
		return dao.saveOrUpdateDeliveryNotes(noteVo);
	}

	public void deleteDeliveryNote(Long id) {
		dao.deleteDeliveryNote(id);
	}

	public DeliveryNoteVo findDeliveryNote(Long id) {
		return dao.findDeliveryNote(id);
	}

	public List<DeliveryNoteVo> listDeliveryNotesForCompany(Long companyId) {
		return dao.listDeliveryNotesForCompany(companyId);
	}

	public List<DeliveryNoteVo> listDeliveryNotesForCompanyWithinDateRange(Long companyId, String startDate,
			String endDate) {
		return dao.listDeliveryNotesForCompanyWithinDateRange(companyId, startDate, endDate);
	}

	public InvoiceVo convertDeliveryNotesToInvoice(List<Long> deliveryNoteIds) {
		return dao.convertDeliveryNotesToInvoice(deliveryNoteIds);
	}

	public Long saveOrUpdateDeliveryNotesFromInvoice(DeliveryNoteVo noteVo) {
		return dao.saveOrUpdateDeliveryNotesFromInvoice(noteVo);
	}
}
