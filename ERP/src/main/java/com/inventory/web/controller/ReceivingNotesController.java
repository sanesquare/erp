package com.inventory.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.entities.Ledger;
import com.accounts.web.service.LedgerService;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.erp.web.service.ChargeService;
import com.erp.web.service.DeliveryMethodService;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.service.VatService;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.constants.InventoryRequestConstants;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ReceivingNote;
import com.inventory.web.service.GodownService;
import com.inventory.web.service.ProductService;
import com.inventory.web.service.ReceivingNotesService;
import com.inventory.web.vo.ProductVo;
import com.inventory.web.vo.ReceivingNoteProductVo;
import com.inventory.web.vo.ReceivingNoteVo;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.BillItems;
import com.purchase.web.entities.Vendor;
import com.purchase.web.service.VendorService;
import com.purchase.web.vo.BillProductVo;
import com.purchase.web.vo.BillVo;
import com.purchase.web.vo.VendorQuoteVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 17, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class ReceivingNotesController {

	@Autowired
	private VendorService vendorService;

	@Autowired
	private ProductService productService;

	@Autowired
	private VatService taxService;

	@Autowired
	private ChargeService chargeService;

	@Autowired
	private VatService vatService;

	@Autowired
	private PaymentTermsService paymentTermService;

	@Autowired
	private GodownService godownService;

	@Log
	private Logger logger;

	@Autowired
	private ErpCurrencyService erpCurrencyService;

	@Autowired
	private ReceivingNotesService receivingNoteService;

	@Autowired
	private LedgerService ledgerService;
	
	@Autowired
	private DeliveryMethodService deliveryMethodService;

	@RequestMapping(value = InventoryRequestConstants.RECEIVING_NOTE, method = RequestMethod.GET)
	public String receivingNotesHome(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @ModelAttribute BillVo billVo) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("notes", receivingNoteService.listReceivingNotesForCompany(companyId));
		} catch (Exception e) {
			logger.info("RECEIVING_NOTE- ", e);
		}
		return InventoryPageNameConstants.RECEIVING_NOTES;
	}

	@RequestMapping(value = InventoryRequestConstants.RECEIVING_NOTE_LIST, method = RequestMethod.GET)
	public String receivingNotesList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @ModelAttribute BillVo billVo) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("notes", receivingNoteService.listReceivingNotesForCompany(companyId));
		} catch (Exception e) {
			logger.info("RECEIVING_NOTE- ", e);
		}
		return InventoryPageNameConstants.RECEIVNG_NOTE_LIST;
	}

	@RequestMapping(value = InventoryRequestConstants.RECEIVING_NOTES_ADD, method = RequestMethod.GET)
	public String receivingNotesAdd(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @ModelAttribute("currencyId") Long currencyId) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("note", new ReceivingNoteVo());
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
			model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
		} catch (Exception e) {
			logger.info("RECEIVING_NOTE- ", e);
		}
		return InventoryPageNameConstants.RECEIVING_NOTES_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.PRODUCT_POP, method = RequestMethod.POST)
	public String productPopup(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestBody ProductVo productVo) {
		try {
			List<ProductVo> productVos = productService.findAllProducts(companyId);
			List<Long> ids = productVo.getIds();
			List<String> idsAndQty = productVo.getIdsAndQuantities();
			if (idsAndQty.isEmpty()) {
				for (ProductVo vo : productVos) {
					if (ids.contains(vo.getProductId()))
						vo.setSelected(true);
				}
				model.addAttribute("products", productVos);
			} else {
				Map<Long, BigDecimal> idMap = new HashMap<Long, BigDecimal>();
				List<ProductVo> alteredProducts = new ArrayList<ProductVo>();
				for (String id : idsAndQty) {
					String[] value = id.split("!!!");
					Long val = Long.parseLong(value[0]);
					if (value.length > 1)
						idMap.put(val, new BigDecimal(value[1]));
					else
						idMap.put(val, BigDecimal.ZERO);
				}
				for (ProductVo vo : productVos) {
					if (idMap.containsKey(vo.getProductId())) {
						vo.setSelected(true);
						vo.setAvailableQty(idMap.get(vo.getProductId()));
					}
					alteredProducts.add(vo);
				}
				model.addAttribute("products", alteredProducts);
			}	
		} catch (Exception ex) {
			logger.info("PRODUCT_POPUP: ", ex);
			return ex.getMessage();
		}

		return InventoryPageNameConstants.PRODUCT_POP;
	}

	@RequestMapping(value = InventoryRequestConstants.PRODUCT_ROW, method = RequestMethod.POST)
	public String findProducts(final Model model, @RequestBody ProductVo productVo,
			@ModelAttribute("companyId") Long companyId) {
		try {

			if (!productVo.getIdsAndQuantities().isEmpty()) {
				List<String> idsAndQty = productVo.getIdsAndQuantities();
				List<Long> ids = new ArrayList<Long>();
				Map<Long, BigDecimal> idMap = new HashMap<Long, BigDecimal>();
				for (String id : idsAndQty) {
					String[] value = id.split("!!!");
					Long val = Long.parseLong(value[0]);
					ids.add(val);
					if (value.length > 1)
						idMap.put(val, new BigDecimal(value[1]));
					else
						idMap.put(val, BigDecimal.ZERO);
				}
				List<ProductVo> products = productService.findProducts(ids);
				List<ProductVo> alteredProducts = new ArrayList<ProductVo>();
				for (ProductVo vo : products) {
					vo.setAvailableQty(idMap.get(vo.getProductId()));
					alteredProducts.add(vo);
				}
				model.addAttribute("products", alteredProducts);
				model.addAttribute("taxes", vatService.listAllTaxesForCompany(companyId));
				model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
			}
		} catch (Exception ex) {
			logger.info("PROUCTS_FIND: ", ex);
			return ex.getMessage();
		}

		return InventoryPageNameConstants.PRODUCT_ROW;
	}

	@RequestMapping(value = InventoryRequestConstants.RECEIVING_NOTE_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveReceivingNote(final Model model, @ModelAttribute("companyId") Long compnyId,
			@RequestBody ReceivingNoteVo receivingNoteVo) {
		Long id = 0L;
		receivingNoteVo.setCompanyId(compnyId);
		try {
			id = receivingNoteService.saveOrUpdateReceivingNotes(receivingNoteVo);
		} catch (Exception e) {
			logger.info("RECEIVING_NOTE_SAVE: ", e);
		}
		return id;
	}

	@RequestMapping(value = InventoryRequestConstants.RECEIVING_NOTE_VIEW, method = RequestMethod.GET)
	public String viewRecivingNote(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			ReceivingNoteVo noteVo = receivingNoteService.findReceivingNote(id);
			model.addAttribute("note", noteVo);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", noteVo.getDate());
			model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
		} catch (Exception e) {
			logger.info("RECEIVING_NOTE_VIEW- ", e);
		}
		return InventoryPageNameConstants.RECEIVING_NOTES_ADD;
	}

	@RequestMapping(value = PurchaseRequestConstants.CONVERT_RECEIVING_NOTE_TO_BILL, method = RequestMethod.POST)
	public String addBillPageHandler(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId, @ModelAttribute BillVo billVo) {
		if (billVo.getReceivingNoteIds() != null && !billVo.getReceivingNoteIds().isEmpty()) {
			try {
				model.addAttribute("bill",
						receivingNoteService.convertReceivingNoteToBill(billVo.getReceivingNoteIds()));
				model.addAttribute("currencyId", currencyId);
				model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
				model.addAttribute("vendors", vendorService.findVendorsForCompany(companyId));
				model.addAttribute("accounts", ledgerService.contraLedgers(companyId));
				model.addAttribute("products", productService.findAllProducts(companyId));
				model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
				model.addAttribute("charges", chargeService.findAllCharges(companyId));
				model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
				model.addAttribute("currencies",
						erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
				model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
			} catch (Exception ex) {
				logger.info("ADD_BILL: ", ex);
			}
			return PurchasePageNameConstants.ADD_BILL;
		} else {
			return "redirect:" + InventoryRequestConstants.RECEIVING_NOTE + "?msg=Please Select Any Receiving Note.";
		}
	}

	@RequestMapping(value = InventoryRequestConstants.RECEIVING_NOTE_DELETE, method = RequestMethod.GET)
	public ModelAndView deleteReceivingNote(@RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = null;
		try {
			receivingNoteService.deleteReceivingNote(id);
			modelAndView = new ModelAndView(new RedirectView(
					InventoryRequestConstants.RECEIVING_NOTE_LIST + "?msg=Receiving Note Deleted Successfully"));
		} catch (Exception e) {
			logger.info("RECEIVING_NOTE_DELETE: ", e);
			modelAndView = new ModelAndView(new RedirectView(
					InventoryRequestConstants.RECEIVING_NOTE_LIST + "?msg=Receiving Note Delete Failed"));
		}
		return modelAndView;
	}
}
