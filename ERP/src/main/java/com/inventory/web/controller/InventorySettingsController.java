package com.inventory.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.constants.InventoryRequestConstants;
import com.inventory.web.service.GodownService;
import com.inventory.web.service.PackingKindService;
import com.inventory.web.vo.GodownVo;
import com.inventory.web.vo.PackingKindVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 21, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate", "companies", "companyName" })
public class InventorySettingsController {

	@Log
	private Logger logger;

	@Autowired
	private GodownService godownService;

	@Autowired
	private PackingKindService packingKindService;

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_SETTINGS_URL, method = RequestMethod.GET)
	public String settingsHome(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("godowns", godownService.listAllGodownsForCompany(companyId));
			model.addAttribute("packings", packingKindService.findAllPackingKinds(companyId));
		} catch (Exception e) {
			logger.info("INVENTORY_SETTINGS_URL ", e);
		}
		return InventoryPageNameConstants.SETTINGS_PAGE;
	}

	@RequestMapping(value = InventoryRequestConstants.PACKING_KINDS_LIST, method = RequestMethod.GET)
	public String packingKindList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("packings", packingKindService.findAllPackingKinds(companyId));
		} catch (Exception e) {
			logger.info("INVENTORY_SETTINGS_URL ", e);
		}
		return InventoryPageNameConstants.PACKING_KIND_LIST;
	}

	@RequestMapping(value = InventoryRequestConstants.GODOWN_SAVE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveGodown(final Model model, @RequestBody GodownVo godownVo,
			@ModelAttribute("companyId") Long companyId) {
		godownVo.setCompanyId(companyId);
		JsonResponse jsonResponse = new JsonResponse();
		String status = "";
		try {
			godownService.saveOrUpdateGodown(godownVo);
			jsonResponse.setResult(godownService.listAllGodownsForCompany(companyId));
			status = "Godown saved successfully.";
		} catch (ItemNotFoundException e) {
			status = e.getMessage();
		} catch (Exception e) {
			logger.info("GODOWN_SAVE ", e);
			status = "Godown save failed.";
		}
		jsonResponse.setStatus(status);
		return jsonResponse;
	}

	@RequestMapping(value = InventoryRequestConstants.GODOWN_DELETE, method = RequestMethod.GET)
	public @ResponseBody JsonResponse deleteGodown(final Model model,
			@RequestParam(value = "id", required = true) Long id, @ModelAttribute("companyId") Long companyId) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			godownService.deleteGodown(id);
			jsonResponse.setResult(godownService.listAllGodownsForCompany(companyId));
			jsonResponse.setStatus("Godown deleted successfully.");
		} catch (ItemNotFoundException e) {
			jsonResponse.setStatus(e.getMessage());
		} catch (Exception e) {
			logger.info("GODOWN_SAVE ", e);
			jsonResponse.setStatus("Godown delete failed.");
		}
		return jsonResponse;
	}

	@RequestMapping(value = InventoryRequestConstants.PACKING_KINDS_SAVE, method = RequestMethod.POST)
	public @ResponseBody ModelAndView savePackingKind(final Model model, @RequestBody PackingKindVo vo,
			@ModelAttribute("companyId") Long companyId) {
		ModelAndView modelAndView = null;
		String url = "";
		try {
			vo.setCompanyId(companyId);
			packingKindService.savePackingKing(vo);
			url = InventoryRequestConstants.PACKING_KINDS_LIST + "?msg=Packing Kind Saved Successfully";
		} catch (ItemNotFoundException e) {
			url = InventoryRequestConstants.PACKING_KINDS_LIST + "?msg=" + e.getMessage();
		} catch (DuplicateItemException e) {
			url = InventoryRequestConstants.PACKING_KINDS_LIST + "?msg=" + e.getMessage();
		} catch (Exception e) {
			url = InventoryRequestConstants.PACKING_KINDS_LIST + "?msg=Packing Kind Save Failed";
			logger.info("packing kind save ", e);
		}
		modelAndView = new ModelAndView(new RedirectView(url));
		return modelAndView;
	}

	@RequestMapping(value = InventoryRequestConstants.PACKING_KINDS_DELETE, method = RequestMethod.GET)
	public @ResponseBody ModelAndView deletePackingKind(final Model model,
			@RequestParam(value = "id", required = true) Long id, @ModelAttribute("companyId") Long companyId) {
		ModelAndView modelAndView = null;
		String url = "";
		try {
			packingKindService.deletePackingKing(id);
			url = InventoryRequestConstants.PACKING_KINDS_LIST + "?msg=Packing Kind Deleted Successfully";
		} catch (ItemNotFoundException e) {
			url = InventoryRequestConstants.PACKING_KINDS_LIST + "?msg=" + e.getMessage();
		} catch (DuplicateItemException e) {
			url = InventoryRequestConstants.PACKING_KINDS_LIST + "?msg=" + e.getMessage();
		} catch (Exception e) {
			url = InventoryRequestConstants.PACKING_KINDS_LIST + "?msg=Packing Kind Delete Failed";
			logger.info("packing kind save ", e);
		}
		modelAndView = new ModelAndView(new RedirectView(url));
		return modelAndView;
	}
}
