package com.inventory.web.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.constants.InventoryRequestConstants;
import com.inventory.web.service.PackingKindService;
import com.inventory.web.service.PackingListService;
import com.inventory.web.vo.PackingListVo;
import com.sales.web.constants.SalesPageNameConstants;
import com.sales.web.constants.SalesRequestConstants;
import com.sales.web.service.InvoiceService;
import com.sales.web.vo.SalesReturnVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class PackingListController {

	@Log
	private Logger logger;

	@Autowired
	private PackingListService packingListService;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private PackingKindService packingKindService;

	@RequestMapping(value = InventoryRequestConstants.PACKING_LIST, method = RequestMethod.GET)
	public String packingListHome(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("showFilter", false);
			model.addAttribute("packingLists", packingListService.findAllPackingLists(companyId));
		} catch (Exception e) {
			logger.info("", e);
		}
		return InventoryPageNameConstants.PACKING_LISTS;
	}

	@RequestMapping(value = InventoryRequestConstants.PACKING_LIST_LIST, method = RequestMethod.GET)
	public String packingListList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("showFilter", false);
			model.addAttribute("packingLists", packingListService.findAllPackingLists(companyId));
		} catch (Exception e) {
			logger.info("", e);
		}
		return InventoryPageNameConstants.PACKING_LISTS_LIST;
	}

	@RequestMapping(value = InventoryRequestConstants.PACKING_LIST_ADD, method = RequestMethod.GET)
	public String packingListAdd(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute PackingListVo packingListVo, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("invoices", invoiceService.listInvoicesForCompany(companyId));
			model.addAttribute("showFilter", true);
		} catch (Exception e) {
			logger.info("", e);
		}
		return InventoryPageNameConstants.PACKING_LISTS_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.PACKING_LIST_VIEW, method = RequestMethod.GET)
	public String packingListView(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "id") Long id, @ModelAttribute PackingListVo packingListVo,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("showFilter", false);
			PackingListVo invoiceVo = packingListService.findPackingListById(id);
			model.addAttribute("invoice", invoiceVo);
			model.addAttribute("packingKinds", packingKindService.findAllPackingKinds(companyId));
			model.addAttribute("date", invoiceVo.getDate());
		} catch (Exception e) {
			logger.info("", e);
		}
		return InventoryPageNameConstants.PACKING_LISTS_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.CONVERT_INVOICE_TO_PACKING_LIST, method = RequestMethod.GET)
	public String packThisInvoiceGet(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "invoiceId") Long invoiceId, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("showFilter", false);
			PackingListVo packingListVo = packingListService.convertInvoiceToPackingList(invoiceId);
			model.addAttribute("invoice", packingListVo);
			model.addAttribute("packingKinds", packingKindService.findAllPackingKinds(companyId));
			model.addAttribute("date", packingListVo.getDate());
		} catch (Exception e) {
			logger.info("", e);
		}
		return InventoryPageNameConstants.PACKING_LISTS_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.CONVERT_INVOICE_TO_PACKING_LIST, method = RequestMethod.POST)
	public String packThisInvoicePost(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute PackingListVo packingListVo, @ModelAttribute("currencyId") Long currencyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("showFilter", false);
			if (packingListVo.getInvoiceId() != null) {
				PackingListVo invoiceVo = packingListService.convertInvoiceToPackingList(packingListVo.getInvoiceId());
				if (invoiceVo.getIsPacked())
					return "redirect:" + InventoryRequestConstants.PACKING_LIST_ADD
							+ "?msg=Oops.. Selected Invoice Has Nothing to Pack";
				model.addAttribute("invoice", invoiceVo);
				model.addAttribute("packingKinds", packingKindService.findAllPackingKinds(companyId));
				model.addAttribute("date", invoiceVo.getDate());

			} else {
				return "redirect:" + InventoryRequestConstants.PACKING_LIST_ADD + "?msg=Select Invoice";
			}
		} catch (Exception e) {
			logger.info("", e);
		}
		return InventoryPageNameConstants.PACKING_LISTS_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.PACKING_LIST_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long savePackingList(final Model model, @RequestBody PackingListVo packingListVo) {
		Long id = 0L;
		try {
			id = packingListService.savePackingList(packingListVo);
		} catch (Exception e) {
			logger.info("", e);
		}
		return id;
	}

	@RequestMapping(value = InventoryRequestConstants.PACKING_LIST_DELETE, method = RequestMethod.GET)
	public @ResponseBody ModelAndView deletePackingList(@RequestParam(value = "id", required = true) Long id) {
		String url = InventoryRequestConstants.PACKING_LIST_LIST;
		try {
			packingListService.deletePackingList(id);
			url = url + "?msg=Packing List Deleted Successfully";
		} catch (ItemNotFoundException e) {
			url = url + "?msg=" + e.getMessage();
		} catch (Exception e) {
			logger.info("", e);
			url = url + "?msg=Packing List Delete Failed";
		}
		return new ModelAndView(new RedirectView(url));
	}
}
