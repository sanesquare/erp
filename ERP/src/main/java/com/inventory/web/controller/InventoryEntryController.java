package com.inventory.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.constants.InventoryRequestConstants;
import com.inventory.web.service.GodownService;
import com.inventory.web.service.InventoryEntryService;
import com.inventory.web.service.ProductService;
import com.inventory.web.vo.InventoryEntryVo;
import com.inventory.web.vo.ProductVo;
import com.inventory.web.vo.ReceivingNoteVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 23, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class InventoryEntryController {

	@Log
	private Logger logger;

	@Autowired
	private GodownService godownService;

	@Autowired
	private ProductService productService;

	@Autowired
	private InventoryEntryService inventoryEntryService;

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_ENTRY, method = RequestMethod.GET)
	public String inventoryEntryHome(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("entries", inventoryEntryService.findAllEntries(companyId));
		} catch (Exception e) {
			logger.info("INVENTORY_ENTRY - ", e);
		}
		return InventoryPageNameConstants.INVENTORY_ENTRY;
	}

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_ENTRY_LIST, method = RequestMethod.GET)
	public String inventoryEntryList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("entries", inventoryEntryService.findAllEntries(companyId));
		} catch (Exception e) {
			logger.info("INVENTORY_ENTRY - ", e);
		}
		return InventoryPageNameConstants.INVENTORY_ENTRY_LIST;
	}

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_ENTRY_ADD, method = RequestMethod.GET)
	public String addInventoryEntry(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			InventoryEntryVo vo = new InventoryEntryVo();
			vo.setDate(DateFormatter.convertDateToString(new Date()));
			model.addAttribute("vo", vo);
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
		} catch (Exception e) {
			logger.info("INVENTORY_ENTRY - ", e);
		}
		return InventoryPageNameConstants.INVENTORY_ENTRY_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.IE_PRODUCT_POP, method = RequestMethod.POST)
	public String productPopup(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestBody ProductVo productVo) {
		try {
			List<ProductVo> productVos = productService.findAllProducts(companyId);
			List<Long> ids = productVo.getIds();
			List<String> idsAndQty = productVo.getIdsAndQuantities();
			if (idsAndQty.isEmpty()) {
				for (ProductVo vo : productVos) {
					if (ids.contains(vo.getProductId()))
						vo.setSelected(true);
				}
				model.addAttribute("products", productVos);
			} else {
				Map<Long, BigDecimal> idMap = new HashMap<Long, BigDecimal>();
				List<ProductVo> alteredProducts = new ArrayList<ProductVo>();
				for (String id : idsAndQty) {
					String[] value = id.split("!!!");
					Long val = Long.parseLong(value[0]);
					if (value.length > 1)
						idMap.put(val, new BigDecimal(value[1]));
					else
						idMap.put(val, BigDecimal.ZERO);
				}
				for (ProductVo vo : productVos) {
					if (idMap.containsKey(vo.getProductId())) {
						vo.setSelected(true);
						vo.setAvailableQty(idMap.get(vo.getProductId()));
					}
					alteredProducts.add(vo);
				}
				model.addAttribute("products", alteredProducts);
			}
		} catch (Exception ex) {
			logger.info("PRODUCT_POPUP: ", ex);
			return ex.getMessage();
		}

		return InventoryPageNameConstants.IE_PRODUCT_POP;
	}

	@RequestMapping(value = InventoryRequestConstants.IE_PRODUCT_ROW, method = RequestMethod.POST)
	public String findProducts(final Model model, @RequestBody ProductVo productVo,
			@ModelAttribute("companyId") Long companyId) {
		try {
			if (!productVo.getIdsAndQuantities().isEmpty()) {
				List<String> idsAndQty = productVo.getIdsAndQuantities();
				List<Long> ids = new ArrayList<Long>();
				Map<Long, BigDecimal> idMap = new HashMap<Long, BigDecimal>();
				for (String id : idsAndQty) {
					String[] value = id.split("!!!");
					Long val = Long.parseLong(value[0]);
					ids.add(val);
					if (value.length > 1)
						idMap.put(val, new BigDecimal(value[1]));
					else
						idMap.put(val, BigDecimal.ZERO);
				}
				List<ProductVo> products = productService.findProducts(ids);
				List<ProductVo> alteredProducts = new ArrayList<ProductVo>();
				for (ProductVo vo : products) {
					vo.setAvailableQty(idMap.get(vo.getProductId()));
					alteredProducts.add(vo);
				}
				model.addAttribute("products", alteredProducts);
			}
		} catch (Exception ex) {
			logger.info("PROUCTS_FIND: ", ex);
			return ex.getMessage();
		}

		return InventoryPageNameConstants.IE_PRODUCT_ROW;
	}

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_ENTRY_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveInventoryEntry(final Model model, @ModelAttribute("companyId") Long compnyId,
			@RequestBody InventoryEntryVo inventoryEntryVo) {
		Long id = 0L;
		inventoryEntryVo.setCompanyId(compnyId);
		try {
			id = inventoryEntryService.saveInventoryEntry(inventoryEntryVo);
		} catch (Exception e) {
			logger.info("INVENTORY_ENTRY_SAVE: ", e);
		}
		return id;
	}

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_ENTRY_VIEW, method = RequestMethod.GET)
	public String viewInventoryEntry(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @RequestParam(value = "id", required = true) Long id) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			InventoryEntryVo vo = inventoryEntryService.findInventoryEntryById(id);
			model.addAttribute("vo", vo);
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
		} catch (Exception e) {
			logger.info("INVENTORY_ENTRY - ", e);
		}
		return InventoryPageNameConstants.INVENTORY_ENTRY_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_ENTRY_DELETE, method = RequestMethod.GET)
	public ModelAndView deleteInventoryEntry(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = null;
		try {
			inventoryEntryService.deleteInventoryEntry(id);
			modelAndView = new ModelAndView(new RedirectView(
					InventoryRequestConstants.INVENTORY_ENTRY_LIST + "?msg=Inventory Entry Deleted Successfully"));
		} catch (Exception e) {
			modelAndView = new ModelAndView(new RedirectView(
					InventoryRequestConstants.INVENTORY_ENTRY_LIST + "?msg=Inventory Entry Delete Failed"));
			logger.info("INVENTORY_ENTRY - ", e);
		}
		return modelAndView;
	}
}
