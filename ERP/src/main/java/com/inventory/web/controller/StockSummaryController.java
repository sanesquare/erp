package com.inventory.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.erp.web.vo.CompanyVo;
import com.hrms.web.logger.Log;
import com.inventory.web.constants.InventoryConstants;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.constants.InventoryRequestConstants;
import com.inventory.web.service.InventoryEntryService;
import com.inventory.web.service.InventoryTransactionService;
import com.inventory.web.service.InventoryWithdrawalService;
import com.inventory.web.vo.InventoryEntryVo;
import com.inventory.web.vo.InventoryWithdrawalVo;
import com.inventory.web.vo.StockFilterVo;
import com.inventory.web.vo.StockMonthlyVo;
import com.inventory.web.vo.StockViewVo;
import com.purchase.web.service.BillService;
import com.purchase.web.service.PurchaseReturnService;
import com.purchase.web.vo.BillVo;
import com.purchase.web.vo.PurchaseReturnVo;
import com.sales.web.service.InvoiceService;
import com.sales.web.service.SalesReturnService;
import com.sales.web.vo.InvoiceVo;
import com.sales.web.vo.SalesReturnVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 5, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate", "companies", "companyName" })
public class StockSummaryController {

	@Log
	private Logger logger;

	@Autowired
	private BillService billService;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private PurchaseReturnService purchaseReturnService;

	@Autowired
	private SalesReturnService salesReturnService;

	@Autowired
	private InventoryEntryService entryService;

	@Autowired
	private InventoryWithdrawalService withdrawalService;

	@Autowired
	private InventoryTransactionService inventoryTransactionService;

	private Map<Integer, StockViewVo> map = new HashMap<Integer, StockViewVo>();

	private Map<Integer, StockMonthlyVo> monthlyMap = new HashMap<Integer, StockMonthlyVo>();

	private List<Integer> ids = new ArrayList<Integer>();

	private List<StockViewVo> vos = new ArrayList<StockViewVo>();

	@RequestMapping(value = InventoryRequestConstants.STOCK_SUMMARY, method = RequestMethod.GET)
	public String stockSummary(@ModelAttribute("companyId") Long companyId, final Model model,
			@ModelAttribute("companies") List<CompanyVo> companies, @ModelAttribute("companyName") String companyName,
			@ModelAttribute("currencyName") String currency, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		try {
			map.clear();
			ids.clear();
			BigDecimal totalAmount = new BigDecimal("0.00");
			List<StockViewVo> stockViewVos = inventoryTransactionService
					.findStockSummary(companyId, startDate, endDate);
			vos = stockViewVos;
			for (StockViewVo vo : stockViewVos) {
				map.put(vo.getHashCode(), vo);
				if (vo.getIsProduct()) {
					for (StockMonthlyVo monthlyVo : vo.getMonthlyVos())
						monthlyMap.put(monthlyVo.getHashCode(), monthlyVo);
				}
				totalAmount = totalAmount.add(vo.getTotalAmount()).add(vo.getClosingValue());
				testPrintProduct(vo.getItems());
			}
			model.addAttribute("totalAmount", totalAmount);
			model.addAttribute("currencyName", currency);
			model.addAttribute("companies", companies);
			model.addAttribute("startDate", startDate);
			model.addAttribute("endDate", endDate);
			model.addAttribute("stock", stockViewVos);
			model.addAttribute("zero", new BigDecimal("0.00"));
		} catch (Exception e) {
			logger.info("", e);
		}
		return InventoryPageNameConstants.STOCK_SUMMARY;
	}

	@RequestMapping(value = InventoryRequestConstants.STOCK_SUMMARY_FILTER, method = RequestMethod.POST)
	public String stockSummaryFilter(final Model model, @ModelAttribute("companies") List<CompanyVo> companies,
			@ModelAttribute("companyName") String companyName, @ModelAttribute("currencyName") String currency,
			@ModelAttribute StockFilterVo filterVo) {
		try {
			map.clear();
			ids.clear();
			BigDecimal totalAmount = new BigDecimal("0.00");
			List<StockViewVo> stockViewVos = inventoryTransactionService.findStockSummary(filterVo.getCompanyId(),
					filterVo.getStartDate(), filterVo.getEndDate());
			vos = stockViewVos;
			for (StockViewVo vo : stockViewVos) {
				map.put(vo.getHashCode(), vo);
				totalAmount = totalAmount.add(vo.getTotalAmount()).add(vo.getClosingValue());
				testPrintProduct(vo.getItems());
			}
			model.addAttribute("totalAmount", totalAmount);
			model.addAttribute("currencyName", currency);
			model.addAttribute("companies", companies);
			model.addAttribute("startDate", filterVo.getStartDate());
			model.addAttribute("endDate", filterVo.getEndDate());
			model.addAttribute("stock", stockViewVos);
			model.addAttribute("zero", new BigDecimal("0.00"));
		} catch (Exception e) {
			logger.info("", e);
		}
		return InventoryPageNameConstants.STOCK_SUMMARY;
	}
	
	@RequestMapping(value = InventoryRequestConstants.STOCK_SUMMARY_FILTER_TEMPLATE, method = RequestMethod.POST)
	public String stockSummaryFilterTemplate(final Model model, @ModelAttribute("companies") List<CompanyVo> companies,
			@ModelAttribute("companyName") String companyName, @ModelAttribute("currencyName") String currency,
			@RequestBody StockFilterVo filterVo) {
		try {
			map.clear();
			ids.clear();
			BigDecimal totalAmount = new BigDecimal("0.00");
			List<StockViewVo> stockViewVos = inventoryTransactionService.findStockSummary(filterVo.getCompanyId(),
					filterVo.getStartDate(), filterVo.getEndDate());
			vos = stockViewVos;
			for (StockViewVo vo : stockViewVos) {
				map.put(vo.getHashCode(), vo);
				totalAmount = totalAmount.add(vo.getTotalAmount()).add(vo.getClosingValue());
				testPrintProduct(vo.getItems());
			}
			model.addAttribute("totalAmount", totalAmount);
			model.addAttribute("currencyName", currency);
			model.addAttribute("companies", companies);
			model.addAttribute("startDate", filterVo.getStartDate());
			model.addAttribute("endDate", filterVo.getEndDate());
			model.addAttribute("stock", stockViewVos);
			model.addAttribute("zero", new BigDecimal("0.00"));
		} catch (Exception e) {
			logger.info("", e);
		}
		return InventoryPageNameConstants.STOCK_SUMMARY_HOME_POP;
	}

	@RequestMapping(value = InventoryRequestConstants.STOCK_SUMMARY_DETAILED, method = RequestMethod.GET)
	public String stockSummaryDetailedView(@ModelAttribute("companyId") Long companyId, final Model model,
			@RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "prevId", required = true) Integer prevId) {
		Boolean isProduct = null;
		try {
			ids.add(prevId);
			StockViewVo stockViewVo = map.get(id);
			isProduct = stockViewVo.getIsProduct();
			List<StockViewVo> stockViewVos = stockViewVo.getItems();
			BigDecimal totalAmount = new BigDecimal("0.00");
			for (StockViewVo vo : stockViewVos) {
				totalAmount = totalAmount.add(vo.getTotalAmount()).add(vo.getClosingValue());
			}
			model.addAttribute("totalAmount", totalAmount);
			if (isProduct)
				model.addAttribute("stock", stockViewVo);
			else
				model.addAttribute("stock", stockViewVos);
			model.addAttribute("show", true);
			model.addAttribute("prevId", prevId);
			model.addAttribute("id", id);
			model.addAttribute("zero", new BigDecimal("0.00"));
		} catch (Exception e) {
			logger.info("", e);
		}
		if (isProduct)
			return InventoryPageNameConstants.STOCK_SUMMARY_PRODUCT_DETAILS;
		else
			return InventoryPageNameConstants.STOCK_SUMMARY_DETAILED;
	}

	@RequestMapping(value = InventoryRequestConstants.STOCK_SUMMARY_DETAILED_PREVIOUS, method = RequestMethod.GET)
	public String stockSummaryPreviousDetails(@ModelAttribute("companyId") Long companyId, final Model model) {
		Boolean isProduct = false;
		try {
			int index = ids.size() - 1;
			int prevId = 0;
			if (index >= 0)
				prevId = ids.get(index);
			int id = prevId;
			ids.remove(ids.indexOf(prevId));
			StockViewVo stockViewVo = map.get(prevId);
			List<StockViewVo> stockViewVos = null;
			if (prevId != 0) {
				index = ids.size() - 1;
				if (index >= 0)
					prevId = ids.get(index);
			} else {
				stockViewVos = vos;
				isProduct = false;
			}
			if (stockViewVo != null) {
				isProduct = stockViewVo.getIsProduct();
				stockViewVos = stockViewVo.getItems();
			}
			BigDecimal totalAmount = new BigDecimal("0.00");
			for (StockViewVo vo : stockViewVos) {
				totalAmount = totalAmount.add(vo.getTotalAmount()).add(vo.getClosingValue());
			}
			model.addAttribute("totalAmount", totalAmount);
			if (isProduct)
				model.addAttribute("stock", stockViewVo);
			else
				model.addAttribute("stock", stockViewVos);
			if (id != 0) {
				model.addAttribute("show", true);
			} else {
				model.addAttribute("show", false);
			}
			model.addAttribute("prevId", prevId);
			model.addAttribute("id", id);
			model.addAttribute("zero", new BigDecimal("0.00"));
		} catch (Exception e) {
			logger.info("", e);
		}
		if (isProduct)
			return InventoryPageNameConstants.STOCK_SUMMARY_PRODUCT_DETAILS;
		else
			return InventoryPageNameConstants.STOCK_SUMMARY_DETAILED;
	}

	@RequestMapping(value = InventoryRequestConstants.STOCK_SUMMARY_MONTHLY_DETAILS, method = RequestMethod.GET)
	public String stockSummaryMonthwiseDetailedView(@ModelAttribute("companyId") Long companyId, final Model model,
			@RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "prevId", required = true) Integer prevId) {
		try {
			ids.remove(id);
			ids.add(prevId);
			StockMonthlyVo monthlyVo = monthlyMap.get(id);
			model.addAttribute("stock", monthlyVo);
			model.addAttribute("show", true);
			model.addAttribute("prevId", ids.get(ids.size()-1));
			model.addAttribute("id", id);
			model.addAttribute("zero", new BigDecimal("0.00"));
		} catch (Exception e) {
			logger.info("", e);
		}
		return InventoryPageNameConstants.STOCK_SUMMARY_MONTHLY_DETAILS;
	}

	private void testPrintProduct(List<StockViewVo> vos) {
		for (StockViewVo vo : vos) {
			map.put(vo.getHashCode(), vo);
			if (vo.getIsProduct()) {
				for (StockMonthlyVo monthlyVo : vo.getMonthlyVos())
					monthlyMap.put(monthlyVo.getHashCode(), monthlyVo);
			}
			testPrintProduct(vo.getItems());
		}
	}

	@RequestMapping(value = InventoryRequestConstants.LOAD_VOUCHER, method = RequestMethod.GET)
	public String loadVoucher(@RequestParam(value = "voucher", required = true) String voucher,
			@RequestParam(value = "voucherId", required = true) Long voucherId, final Model model) {
		try {
			if (voucher.equalsIgnoreCase(InventoryConstants.PURCHASE)) {
				BillVo vo = billService.findBill(voucherId);
				model.addAttribute("bill", vo);
				return InventoryPageNameConstants.BILL_VOUCHER_POP;
			} else if (voucher.equalsIgnoreCase(InventoryConstants.PURCHASE_RETURN)) {
				PurchaseReturnVo vo = purchaseReturnService.findPurchaseReturnById(voucherId);
				model.addAttribute("bill", vo);
				model.addAttribute("date", vo.getDate());
				return InventoryPageNameConstants.PURCHASE_RETURN_VOUCHER_POP;
			} else if (voucher.equalsIgnoreCase(InventoryConstants.SALES)) {
				InvoiceVo vo = invoiceService.findInvoiceObject(voucherId);
				model.addAttribute("invoice", vo);
				return InventoryPageNameConstants.INVOICE_VOUCHER_POP;
			} else if (voucher.equalsIgnoreCase(InventoryConstants.SALES_RETURN)) {
				SalesReturnVo vo = salesReturnService.findSalesReturnById(voucherId);
				model.addAttribute("invoice", vo);
				model.addAttribute("date", vo.getDate());
				return InventoryPageNameConstants.SALES_RETURN_VOUCHER_POP;
			} else if (voucher.equalsIgnoreCase(InventoryConstants.INVENTORY_ENTRY)) {
				InventoryEntryVo vo = entryService.findInventoryEntryById(voucherId);
				model.addAttribute("vo", vo);
				return InventoryPageNameConstants.ENTRY_VOUCHER_POP;
			} else if (voucher.equalsIgnoreCase(InventoryConstants.INVENTORY_WITHDRAWAL)) {
				InventoryWithdrawalVo vo = withdrawalService.findInventoryWithdrawalById(voucherId);
				model.addAttribute("vo", vo);
				return InventoryPageNameConstants.WITHDRAWAL_VOUCHER_POP;
			}
		} catch (Exception e) {
			logger.info("", e);
		}
		return null;
	}
}
