package com.inventory.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.constants.InventoryRequestConstants;
import com.inventory.web.service.GodownService;
import com.inventory.web.service.InventoryWithdrawalService;
import com.inventory.web.service.ProductService;
import com.inventory.web.vo.InventoryEntryVo;
import com.inventory.web.vo.InventoryWithdrawalVo;
import com.inventory.web.vo.ProductVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 27, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class InventoryWithdrawalController {

	@Log
	private Logger logger;

	@Autowired
	private GodownService godownService;

	@Autowired
	private ProductService productService;

	@Autowired
	private InventoryWithdrawalService inventoryWithdrawalService;

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_WITHDRAWAL, method = RequestMethod.GET)
	public String inventoryEntryHome(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("entries", inventoryWithdrawalService.findAllWithdrawals(companyId));
		} catch (Exception e) {
			logger.info("INVENTORY_ENTRY - ", e);
		}
		return InventoryPageNameConstants.INVENTORY_WITHDRAWAL;
	}

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_WITHDRAWAL_LIST, method = RequestMethod.GET)
	public String inventoryEntryList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("entries", inventoryWithdrawalService.findAllWithdrawals(companyId));
		} catch (Exception e) {
			logger.info("INVENTORY_ENTRY - ", e);
		}
		return InventoryPageNameConstants.INVENTORY_WITHDRAWAL_LIST;
	}

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_WITHDRAWAL_ADD, method = RequestMethod.GET)
	public String addInventoryEntry(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			InventoryWithdrawalVo vo = new InventoryWithdrawalVo();
			vo.setDate(DateFormatter.convertDateToString(new Date()));
			model.addAttribute("vo", vo);
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
		} catch (Exception e) {
			logger.info("INVENTORY_ENTRY - ", e);
		}
		return InventoryPageNameConstants.INVENTORY_WITHDRAWAL_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_WITHDRAWAL_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveInventoryEntry(final Model model, @ModelAttribute("companyId") Long compnyId,
			@RequestBody InventoryWithdrawalVo inventoryWithdrawalVo) {
		Long id = 0L;
		inventoryWithdrawalVo.setCompanyId(compnyId);
		try {
			 id = inventoryWithdrawalService.saveInventoryWithdrawal(inventoryWithdrawalVo);
		} catch (Exception e) {
			logger.info("INVENTORY_WITHDRAWAL_SAVE: ", e);
		}
		return id;
	}

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_WITHDRAWAL_VIEW, method = RequestMethod.GET)
	public String viewInventoryEntry(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @RequestParam(value = "id", required = true) Long id) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			InventoryWithdrawalVo vo = inventoryWithdrawalService.findInventoryWithdrawalById(id);
			model.addAttribute("vo", vo);
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
		} catch (Exception e) {
			logger.info("INVENTORY_WITHDRAWAL_VIEW - ", e);
		}
		return InventoryPageNameConstants.INVENTORY_WITHDRAWAL_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.INVENTORY_WITHDRAWAL_DELETE, method = RequestMethod.GET)
	public ModelAndView deleteInventoryEntry(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = null;
		try {
			inventoryWithdrawalService.deleteInventoryWithdrawal(id);
			modelAndView = new ModelAndView(new RedirectView(InventoryRequestConstants.INVENTORY_WITHDRAWAL_LIST
					+ "?msg=Inventory Withdrawal Deleted Successfully"));
		} catch (Exception e) {
			modelAndView = new ModelAndView(new RedirectView(InventoryRequestConstants.INVENTORY_WITHDRAWAL_LIST
					+ "?msg=Inventory Withdrawal Delete Failed"));
			logger.info("INVENTORY_WITHDRAWAL_DELETE - ", e);
		}
		return modelAndView;
	}
}
