package com.inventory.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.logger.Log;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.constants.InventoryRequestConstants;
import com.inventory.web.dao.PackingKindDao;
import com.inventory.web.service.PackingListService;
import com.inventory.web.vo.PackingListItemsVo;
import com.inventory.web.vo.PackingListPdfVo;
import com.inventory.web.vo.PackingListVo;
import com.sales.web.service.CustomerService;
import com.sales.web.service.InvoiceService;
import com.sales.web.vo.CustomerVo;
import com.sales.web.vo.InvoiceVo;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 17, 2015
 */
@Controller
@SessionAttributes({ "companyId" })
public class InventoryReportController {

	@Log
	private Logger logger;

	@Autowired
	private PackingListService packingListService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = InventoryRequestConstants.PACKING_LIST_PDF, method = RequestMethod.GET)
	public String packingListViewHandler(final Model model, @RequestParam(value = "pid") Long pid) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			PackingListVo packingListVo = packingListService.findPackingListById(pid);
			CompanyVo companyVo = companyService.findCompanyById(packingListVo.getCompanyId());
			if (companyVo != null) {
				params.put("companyName", companyVo.getParent());
				params.put("companyAddress", companyVo.getAddress());
			}
			InvoiceVo invoiceVo = invoiceService.findInvoiceObject(packingListVo.getInvoiceId());
			if (invoiceVo != null) {
				CustomerVo customerVo = customerService.findCustomerById(invoiceVo.getCustomerId());
				params.put("customerName", customerVo.getName());
				params.put("customerAddress", customerVo.getAddress());
			}
			params.put("invoiceNumber", packingListVo.getCode());
			params.put("billDate", packingListVo.getDate());
			params.put("grossWeight", packingListVo.getGrossWeight());
			params.put("netWeight", packingListVo.getNetWeight());
			params.put("totalNumberOfPackages", packingListVo.getNumberOfPacks());
			List<PackingListPdfVo> pdfVos = this.createPdfListVos(packingListVo);
			String casesList = "";
			for (PackingListPdfVo itemsVo : pdfVos) {
				if (!casesList.isEmpty()) {
					if (!casesList.contains(itemsVo.getKindOfPack())) {
						casesList = casesList + " , " + itemsVo.getKindOfPack();
					}
				} else {
					casesList = casesList + itemsVo.getKindOfPack();
				}
			}
			params.put("casesList", casesList);
			params.put("datasource", new JRBeanCollectionDataSource(pdfVos));
			model.addAllAttributes(params);
			return InventoryPageNameConstants.PACKING_LIST_PDF;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("PACKING LIST PDF : " + e);
		}
		return "redirect:" + InventoryRequestConstants.PACKING_LIST;
	}

	private List<PackingListPdfVo> createPdfListVos(PackingListVo listVo) {
		List<PackingListPdfVo> listPdfVos = new ArrayList<PackingListPdfVo>();
		for (PackingListItemsVo itemsVo : listVo.getItemsVos()) {
			PackingListPdfVo vo = new PackingListPdfVo();
			vo.setItemCode(itemsVo.getProductCode());
			vo.setItemDescription(itemsVo.getProductName());
			vo.setGrossWeight(itemsVo.getGrossWeight());
			vo.setKindOfPack(itemsVo.getPackingKind());
			vo.setNetWeight(itemsVo.getNetWeight());
			vo.setNoOfPack(itemsVo.getNumberOfPack());
			vo.setContentOfPack(itemsVo.getQuantity());
			listPdfVos.add(vo);
		}
		return listPdfVos;
	}
}
