package com.inventory.web.controller;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.service.LedgerService;
import com.erp.web.service.ChargeService;
import com.erp.web.service.DeliveryMethodService;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.service.VatService;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.constants.InventoryRequestConstants;
import com.inventory.web.service.DeliveryNoteService;
import com.inventory.web.service.GodownService;
import com.inventory.web.service.ProductService;
import com.inventory.web.vo.DeliveryNoteVo;
import com.sales.web.constants.SalesPageNameConstants;
import com.sales.web.service.CustomerService;
import com.sales.web.vo.InvoiceVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 29, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class DeliveryNoteController {

	@Autowired
	private DeliveryNoteService deliveryNoteService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private ChargeService chargeService;

	@Autowired
	private ProductService productService;

	@Autowired
	private VatService taxService;

	@Autowired
	private PaymentTermsService paymentTermService;

	@Autowired
	private GodownService godownService;

	@Log
	private Logger logger;

	@Autowired
	private ErpCurrencyService erpCurrencyService;
	
	@Autowired
	private DeliveryMethodService deliveryMethodService;

	@RequestMapping(value = InventoryRequestConstants.DELIVERY_NOTES, method = RequestMethod.GET)
	public String deliveryNotesHome(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @ModelAttribute InvoiceVo invoiceVo) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("notes", deliveryNoteService.listDeliveryNotesForCompany(companyId));
		} catch (Exception e) {
			logger.info("DELIVERY_NOTE- ", e);
		}
		return InventoryPageNameConstants.DELIVERY_NOTES;
	}

	@RequestMapping(value = InventoryRequestConstants.DELIVERY_NOTE_LIST, method = RequestMethod.GET)
	public String deliveryNotesList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @ModelAttribute InvoiceVo invoiceVo) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("notes", deliveryNoteService.listDeliveryNotesForCompany(companyId));
		} catch (Exception e) {
			logger.info("DELIVERY_NOTE- ", e);
		}
		return InventoryPageNameConstants.DELIVERY_NOTE_LIST;
	}

	@RequestMapping(value = InventoryRequestConstants.DELIVERY_NOTES_ADD, method = RequestMethod.GET)
	public String deliveryNotesAdd(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @ModelAttribute("currencyId") Long currencyId) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("note", new DeliveryNoteVo());
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
			model.addAttribute("customers", customerService.findCustomersForCompany(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
		} catch (Exception e) {
			logger.info("DELIVERY_NOTE- ", e);
		}
		return InventoryPageNameConstants.DELIVERY_NOTES_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.DELIVERY_NOTE_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveDeliveryNote(final Model model, @ModelAttribute("companyId") Long compnyId,
			@RequestBody DeliveryNoteVo deliveryNoteVo) {
		Long id = 0L;
		deliveryNoteVo.setCompanyId(compnyId);
		try {
			if (deliveryNoteVo.getInvoiceId() != null)
				id = deliveryNoteService.saveOrUpdateDeliveryNotesFromInvoice(deliveryNoteVo);
			else
				id = deliveryNoteService.saveOrUpdateDeliveryNotes(deliveryNoteVo);
		} catch (Exception e) {
			logger.info("DELIVERY_NOTE_SAVE: ", e);
		}
		return id;
	}

	@RequestMapping(value = InventoryRequestConstants.DELIVERY_NOTE_VIEW, method = RequestMethod.GET)
	public String viewRecivingNote(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			DeliveryNoteVo noteVo = deliveryNoteService.findDeliveryNote(id);
			model.addAttribute("note", noteVo);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", noteVo.getDate());
			model.addAttribute("customers", customerService.findCustomersForCompany(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
		} catch (Exception e) {
			logger.info("DELIVERY_NOTE_VIEW- ", e);
		}
		return InventoryPageNameConstants.DELIVERY_NOTES_ADD;
	}

	@RequestMapping(value = InventoryRequestConstants.DELIVERY_NOTE_DELETE, method = RequestMethod.GET)
	public ModelAndView deleteDeliveryNote(@RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = null;
		try {
			deliveryNoteService.deleteDeliveryNote(id);
			modelAndView = new ModelAndView(new RedirectView(InventoryRequestConstants.DELIVERY_NOTE_LIST
					+ "?msg=Delivery Note Deleted Successfully"));
		} catch (Exception e) {
			logger.info("DELIVERY_NOTE_DELETE: ", e);
			modelAndView = new ModelAndView(new RedirectView(InventoryRequestConstants.DELIVERY_NOTE_LIST
					+ "?msg=Delivery Note Delete Failed"));
		}
		return modelAndView;
	}

	@RequestMapping(value = InventoryRequestConstants.CONVERT_DELIVERY_NOTE_TO_INVOICE, method = RequestMethod.POST)
	public String convertDeliveryNoteToInvoice(final Model model, @ModelAttribute InvoiceVo invoiceVo,
			HttpServletRequest request, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId) {
		model.addAttribute("msg", request.getParameter("msg"));
		if (invoiceVo.getDeliveryNoteIds() != null && !invoiceVo.getDeliveryNoteIds().isEmpty()) {
			try {
				InvoiceVo vo = deliveryNoteService.convertDeliveryNotesToInvoice(invoiceVo.getDeliveryNoteIds());
				model.addAttribute("invoice", vo);
				model.addAttribute("fromDN", true);
				model.addAttribute("currencyId", currencyId);
				model.addAttribute("date", vo.getInvoiceDate());
				model.addAttribute("customer", customerService.findCustomersForCompany(companyId));
				model.addAttribute("accounts", ledgerService.contraLedgers(companyId));
				model.addAttribute("products", productService.findAllProducts(companyId));
				model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
				model.addAttribute("charges", chargeService.findAllCharges(companyId));
				model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
				model.addAttribute("currencies",
						erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
				model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
			} catch (Exception e) {
				logger.info("CONVERT_DELIVERY_NOTE_TO_INVOICE: ", e);
			}
			return SalesPageNameConstants.INVOICE_ADD;
		} else {
			return "redirect:" + InventoryRequestConstants.DELIVERY_NOTES + "?msg=Please Select Any Delivery Note";
		}
	}

}
