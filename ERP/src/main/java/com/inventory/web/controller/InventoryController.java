package com.inventory.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.constants.InventoryRequestConstants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
@Controller
public class InventoryController {

	@RequestMapping(value = InventoryRequestConstants.HOME , method = RequestMethod.GET)
	public String inventoryHome(final Model model){
		return InventoryPageNameConstants.HOME_PAGE;
	}
}
