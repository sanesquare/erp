package com.inventory.web.dao;

import java.util.List;

import com.erp.web.entities.Company;
import com.erp.web.entities.Godown;
import com.inventory.web.vo.GodownVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 21, 2015
 */
public interface GodownDao {

	/**
	 * Method to save or upate godown
	 * @param godownVo
	 * @return
	 */
	public void saveOrUpdateGodown(GodownVo godownVo);
	
	/**
	 * Method to delete godown
	 * @param id
	 */
	public void deleteGodown(Long id);
	
	/**
	 * Method to list all godowns for company
	 * @param companyId
	 * @return
	 */
	public List<GodownVo> listAllGodownsForCompany(Long companyId);
	
	/**
	 * Method to find godown
	 * @param id
	 * @return
	 */
	public GodownVo findGodown(Long id);
	
	/**
	 * Method to find godown object
	 * @param id
	 * @return
	 */
	public Godown findGodownObject(Long id);
	
	/**
	 * Method to find godown by name
	 * @param name
	 * @param companyId
	 * @return
	 */
	public GodownVo findGodownByName(String name , Long companyId);
	
	/**
	 * Method to find godown object by name
	 * @param name
	 * @param companyId
	 * @return
	 */
	public Godown findGodownObjectByName(String name , Long companyId);
	
	public Godown findGodownObjectByName(String name , Company company);
}
