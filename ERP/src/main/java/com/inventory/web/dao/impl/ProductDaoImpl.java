package com.inventory.web.dao.impl;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.StockGroup;
import com.erp.web.entities.Unit;
import com.erp.web.vo.ErpCurrencyVo;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ProductAdvanceDetails;
import com.inventory.web.vo.ProductVo;
import com.purchase.web.dao.StockGroupDao;
import com.purchase.web.dao.UnitDao;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 23, 2015
 */
@Repository
public class ProductDaoImpl extends AbstractDao implements ProductDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private UnitDao unitDao;

	@Autowired
	private StockGroupDao stockGroupDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private ErpCurrencyDao currencyDao;

	public Long saveOrUpdateProduct(ProductVo productVo) {
		Product product = this.findProduct(productVo.getProductId());
		Company company = null;
		ProductAdvanceDetails advanceDetails = null;
		Unit unit = unitDao.findUnit(productVo.getUnitId());
		Long id = null;
		StockGroup stockGroup = stockGroupDao.findStockGroupsObject(productVo.getStockGroupId());
		if (product == null) {
			product = new Product();
			company = companyDao.findCompany(productVo.getCompanyId());
			if (company == null)
				throw new ItemNotFoundException("Company Not Found");
			advanceDetails = new ProductAdvanceDetails();
			company.getProducts().add(product);
			product.setCompany(company);
			product = setAttributesToProduct(product, productVo, unit, stockGroup, advanceDetails);
			// updates
			product.setProductCode(this.getProductCoderForCompany(productVo.getCompanyId(), stockGroup.getGroupName()));
			id = (Long) this.sessionFactory.getCurrentSession().save(product);
			numberPropertyDao.incrementNumberProperty(stockGroup.getGroupName(), company);

		} else {
			Boolean changedGroup = false;
			company = product.getCompany();
			advanceDetails = product.getAdvanceDetails();
			if (!product.getStockGroup().equals(stockGroup)) {
				changedGroup = true;
				product.setProductCode(this.getProductCoderForCompany(productVo.getCompanyId(),
						stockGroup.getGroupName()));
			}
			product = setAttributesToProduct(product, productVo, unit, stockGroup, advanceDetails);
			id = product.getId();
			this.sessionFactory.getCurrentSession().saveOrUpdate(product);
			if (changedGroup)
				numberPropertyDao.incrementNumberProperty(stockGroup.getGroupName(), company);
		}
		return id;
	}

	public String getProductCoderForCompany(Long companyId, String stockGroup) {
		NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(stockGroup, companyId);
		String code = numberPropertyVo.getPrefix() + String.valueOf(numberPropertyVo.getNumber());
		return code;
	}

	private Product setAttributesToProduct(Product product, ProductVo productVo, Unit unit, StockGroup stockGroup,
			ProductAdvanceDetails advanceDetails) {

		ErpCurrencyVo currencyVo = currencyDao.findCurrencyForDate(new Date(), product.getCompany().getCurrency()
				.getBaseCurrency());
		if (currencyVo != null) {
			/*
			 * advanceDetails.setOpeningBalance(
			 * productVo.getOpeningBalance().divide(currencyVo.getRate(), 2,
			 * RoundingMode.HALF_EVEN));
			 */
			product.setSellingPrice(productVo.getSalesPrice().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
			product.setPurchasePrice(productVo.getPurchasePrice().divide(currencyVo.getRate(), 2,
					RoundingMode.HALF_EVEN));
		} else {
			/* advanceDetails.setOpeningBalance(productVo.getOpeningBalance()); */
			product.setSellingPrice(productVo.getSalesPrice());
			product.setPurchasePrice(productVo.getPurchasePrice());
		}
		advanceDetails.setUnit(unit);
		advanceDetails.setOpeningQuantity(productVo.getOpeningQuantity());
		advanceDetails.setWeight(productVo.getWeight());
		advanceDetails.setProduct(product);
		product.setName(productVo.getName());
		product.setAdvanceDetails(advanceDetails);
		product.setStockGroup(stockGroup);
		// product.setProductCode(productVo.getProductCode());
		return product;
	}

	public void deleteProduct(Long id) {
		Product product = this.findProduct(id);
		if (product == null)
			throw new ItemNotFoundException("Product Not Found.");
		this.sessionFactory.getCurrentSession().delete(product);
	}

	public void deleteProduct(String code, Long companyId) {
		Product product = this.findProduct(code, companyId);
		if (product == null)
			throw new ItemNotFoundException("Product Not Found.");
		this.sessionFactory.getCurrentSession().delete(product);
	}

	public Product findProduct(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("id", id));
		return (Product) criteria.uniqueResult();
	}

	public Product findProduct(String code, Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("productCode", code));
		criteria.add(Restrictions.eq("company", company));
		return (Product) criteria.uniqueResult();
	}

	public ProductVo findProductById(Long id) {
		Product product = this.findProduct(id);
		if (product == null)
			throw new ItemNotFoundException("Product Not Found.");
		return createProductVo(product);
	}

	private ProductVo createProductVo(Product product) {

		ProductVo vo = new ProductVo();
		vo.setProductId(product.getId());
		vo.setAvailableQty(product.getAdvanceDetails().getOpeningQuantity());
		vo.setName(product.getName());
		vo.setProductCode(product.getProductCode());
		StockGroup stockGroup = product.getStockGroup();
		ProductAdvanceDetails advanceDetails = product.getAdvanceDetails();
		Unit unit = advanceDetails.getUnit();
		vo.setStockGroup(stockGroup.getGroupName());
		vo.setStockGroupId(stockGroup.getId());
		vo.setWeight(advanceDetails.getWeight());
		vo.setOpeningQuantity(advanceDetails.getOpeningQuantity());
		vo.setProductAdditionalInfoId(advanceDetails.getId());
		vo.setUnit(unit.getName());
		vo.setUnitId(unit.getId());

		ErpCurrencyVo currencyVo = currencyDao.findCurrencyForDate(new Date(), product.getCompany().getCurrency()
				.getBaseCurrency());

		if (currencyVo != null) {
			vo.setSalesPrice(product.getSellingPrice().multiply(currencyVo.getRate())
					.setScale(2, RoundingMode.HALF_EVEN));
			vo.setPurchasePrice(product.getPurchasePrice().multiply(currencyVo.getRate())
					.setScale(2, RoundingMode.HALF_EVEN));
			/*
			 * vo.setOpeningBalance(advanceDetails.getOpeningBalance().multiply(
			 * currencyVo.getRate()).setScale(2, RoundingMode.HALF_EVEN));
			 */
		} else {
			vo.setSalesPrice(product.getSellingPrice());
			vo.setPurchasePrice(product.getPurchasePrice());
			/* vo.setOpeningBalance(advanceDetails.getOpeningBalance()); */
		}

		return vo;
	}

	public ProductVo findProductByCode(String code, Long companyId) {
		Product product = this.findProduct(code, companyId);
		if (product == null)
			throw new ItemNotFoundException("Product Not Found.");
		return createProductVo(product);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ProductVo> findAllProducts(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Product> products = criteria.list();
		List<ProductVo> productVos = new ArrayList<ProductVo>();
		for (Product product : products) {
			productVos.add(createProductVo(product));
		}
		return productVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ProductVo> findProducts(List<Long> ids) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Product.class);
		criteria.add(Restrictions.in("id", ids));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Product> products = criteria.list();
		List<ProductVo> productVos = new ArrayList<ProductVo>();
		for (Product product : products) {
			productVos.add(createProductVo(product));
		}
		return productVos;
	}

}
