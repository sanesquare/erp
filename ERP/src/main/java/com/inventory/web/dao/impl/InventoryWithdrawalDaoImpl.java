package com.inventory.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.Godown;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryConstants;
import com.inventory.web.dao.GodownDao;
import com.inventory.web.dao.InventoryWithdrawalDao;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.entities.InventoryEntryItems;
import com.inventory.web.entities.InventoryTransaction;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.InventoryWithdrawalItems;
import com.inventory.web.entities.Product;
import com.inventory.web.vo.InventoryEntryItemsVo;
import com.inventory.web.vo.InventoryEntryVo;
import com.inventory.web.vo.InventoryWithdrawalItemsVo;
import com.inventory.web.vo.InventoryWithdrawalVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 27, 2015
 */
@Repository
public class InventoryWithdrawalDaoImpl extends AbstractDao implements InventoryWithdrawalDao {

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private GodownDao godownDao;

	public Long saveInventoryWithdrawal(InventoryWithdrawalVo vo) {

		Long id = vo.getInventoryWithdrawalId();
		InventoryWithdrawal withdrawal = this.findInventoryWithdrawal(vo.getInventoryWithdrawalId());
		Godown godown = godownDao.findGodownObject(vo.getGodownId());
		if (withdrawal == null) {
			withdrawal = new InventoryWithdrawal();
			withdrawal.setType(InventoryConstants.IE_MANUAL);
			Company company = companyDao.findCompany(vo.getCompanyId());
			withdrawal.setCode(this.getNumber(company.getId(), NumberPropertyConstants.INVENTORY_WITHDRAWAL));
			withdrawal.setCompany(company);
			withdrawal.setType(vo.getType());
			withdrawal.setGodown(godown);
			withdrawal = this.setAttributesToInventoryWithdrawal(withdrawal, vo, true);
			id = (Long) this.sessionFactory.getCurrentSession().save(withdrawal);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.INVENTORY_WITHDRAWAL, company);
		} else {
			withdrawal.setGodown(godown);
			withdrawal = this.setAttributesToInventoryWithdrawal(withdrawal, vo, true);
			this.sessionFactory.getCurrentSession().merge(withdrawal);
		}
		return id;

	}

	public InventoryWithdrawal setAttributesToInventoryWithdrawal(InventoryWithdrawal withdrawal,
			InventoryWithdrawalVo vo, Boolean isDirect) {

		withdrawal.getWithdrawalItems().clear();

		withdrawal.setDate(DateFormatter.convertStringToDate(vo.getDate()));
		withdrawal.setDescription(vo.getNarration());

		// setting items
		for (InventoryWithdrawalItemsVo itemsVo : vo.getItemsVos()) {
			InventoryWithdrawalItems withdrawalItems = new InventoryWithdrawalItems();
			withdrawalItems.setInventoryWithdrawal(withdrawal);
			InventoryTransaction inventoryTransaction = new InventoryTransaction();
			Product product = productDao.findProduct(itemsVo.getProductCode(), withdrawal.getCompany().getId());
			inventoryTransaction.setProduct(product);
			inventoryTransaction.setCompany(withdrawal.getCompany());
			inventoryTransaction.setGodown(withdrawal.getGodown());
			inventoryTransaction.setQuantityMinus(itemsVo.getQuantity());
			if (isDirect) {
				inventoryTransaction.setUnitPrice(product.getPurchasePrice());
				inventoryTransaction.setTotalAmount(inventoryTransaction.getQuantityMinus().multiply(
						inventoryTransaction.getUnitPrice()));
			} else {
				inventoryTransaction.setUnitPrice(itemsVo.getUnitPrice());
				inventoryTransaction.setTotalAmount(itemsVo.getTotal());
			}
			inventoryTransaction.setDate(withdrawal.getDate());
			inventoryTransaction.setWithdrawalItems(withdrawalItems);
			withdrawalItems.setTransaction(inventoryTransaction);
			withdrawal.getWithdrawalItems().add(withdrawalItems);
		}
		return withdrawal;
	}

	public void deleteInventoryWithdrawal(Long id) {
		InventoryWithdrawal withdrawal = this.findInventoryWithdrawal(id);
		if (withdrawal == null)
			throw new ItemNotFoundException("Inventory Withdrawal Not Found");
		this.sessionFactory.getCurrentSession().delete(withdrawal);

	}

	public List<InventoryWithdrawalVo> listAllInventoryWithdrawals(Long companyId) {
		// TODO Auto-generated method stub
		return null;
	}

	public InventoryWithdrawal findInventoryWithdrawal(Long id) {
		return (InventoryWithdrawal) this.sessionFactory.getCurrentSession().createCriteria(InventoryWithdrawal.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public String getNumber(Long companyId, String category) {
		String code = "";
		NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(category, companyId);
		code = numberPropertyVo.getPrefix() + numberPropertyVo.getNumber();
		return code;
	}

	public InventoryWithdrawalVo findInventoryWithdrawalById(Long id) {
		InventoryWithdrawal withdrawal = this.findInventoryWithdrawal(id);
		if (withdrawal == null)
			throw new ItemNotFoundException("Inventory withdrawal not found");
		return createVo(withdrawal);
	}

	private InventoryWithdrawalVo createVo(InventoryWithdrawal withdrawal) {
		InventoryWithdrawalVo vo = new InventoryWithdrawalVo();
		vo.setInventoryWithdrawalId(withdrawal.getId());
		vo.setCode(withdrawal.getCode());
		vo.setDate(DateFormatter.convertDateToString(withdrawal.getDate()));
		vo.setNarration(withdrawal.getDescription());
		if (withdrawal.getType().equalsIgnoreCase(InventoryConstants.IE_MANUAL))
			vo.setEditable(true);
		else
			vo.setEditable(false);
		Godown godown = withdrawal.getGodown();
		vo.setGodown(godown.getName());
		vo.setGodownId(godown.getId());
		vo.setType(withdrawal.getType());
		for (InventoryWithdrawalItems item : withdrawal.getWithdrawalItems()) {
			InventoryTransaction transaction = item.getTransaction();
			Product product = transaction.getProduct();
			InventoryWithdrawalItemsVo itemsVo = new InventoryWithdrawalItemsVo();
			itemsVo.setProductCode(product.getProductCode());
			itemsVo.setProductName(product.getName());
			itemsVo.setQuantity(transaction.getQuantityMinus());
			itemsVo.setUnitPrice(transaction.getUnitPrice());
			itemsVo.setTotal(transaction.getTotalAmount());
			vo.getItemsVos().add(itemsVo);
		}
		return vo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<InventoryWithdrawalVo> findAllWithdrawals(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company not found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryWithdrawal.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<InventoryWithdrawal> entries = criteria.list();
		List<InventoryWithdrawalVo> entryVos = new ArrayList<InventoryWithdrawalVo>();
		for (InventoryWithdrawal entry : entries)
			entryVos.add(createVo(entry));
		return entryVos;
	}

}
