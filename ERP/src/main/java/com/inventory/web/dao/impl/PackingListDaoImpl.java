package com.inventory.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.dao.PackingKindDao;
import com.inventory.web.dao.PackingListdao;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.entities.PackingKind;
import com.inventory.web.entities.PackingList;
import com.inventory.web.entities.PackingListItems;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ProductAdvanceDetails;
import com.inventory.web.vo.PackingListItemsVo;
import com.inventory.web.vo.PackingListVo;
import com.sales.web.dao.InvoiceDao;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.InvoiceItems;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
@Repository
public class PackingListDaoImpl extends AbstractDao implements PackingListdao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private InvoiceDao invoiceDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private PackingKindDao packingKindDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	public Long savePackingList(PackingListVo vo) {
		Long id = vo.getPackingListId();
		Invoice invoice = null;
		PackingList packingList = this.findPackingList(id);
		Boolean isNew = false;
		Company company = null;
		if (packingList == null) {
			invoice = invoiceDao.findInvoice(vo.getInvoiceId());
			if (invoice == null)
				throw new ItemNotFoundException("Invoice Not Found");
			company = invoice.getCompany();
			packingList = new PackingList();
			isNew = true;
			packingList.setCompany(company);
			packingList.setInvoice(invoice);
			packingList.setReferenceNumber(
					numberPropertyDao.getRefernceNumber(NumberPropertyConstants.PACKING_LIST, company));
		} else {
			invoice = packingList.getInvoice();
			company = packingList.getCompany();
		}
		packingList.setDate(DateFormatter.convertStringToDate(vo.getDate()));
		packingList.setGrossWeight(vo.getGrossWeight());
		packingList.setNetWeight(vo.getNetWeight());
		packingList.setNumberOfPacks(vo.getNumberOfPacks());
		packingList = this.setItemsToPackingList(packingList, vo, invoice, company);
		if (isNew) {
			id = (Long) this.sessionFactory.getCurrentSession().save(packingList);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PACKING_LIST, company);
			invoice.getPackingLists().add(packingList);
			this.sessionFactory.getCurrentSession().merge(invoice);
		} else {
			invoice.getPackingLists().add(packingList);
			this.sessionFactory.getCurrentSession().merge(invoice);
		}
		return id;
	}

	private PackingList setItemsToPackingList(PackingList packingList, PackingListVo vo, Invoice invoice,
			Company company) {
		List<PackingListItems> oldItems = packingList.getPackingListItems();
		Map<Product, PackingListItems> oldMap = new HashMap<Product, PackingListItems>();
		for (PackingListItems listItem : oldItems)
			oldMap.put(listItem.getProduct(), listItem);
		packingList.getPackingListItems().clear();
		Set<InvoiceItems> invoiceItems = invoice.getInvoiceItems();
		Map<Product, InvoiceItems> map = new HashMap<Product, InvoiceItems>();
		for (InvoiceItems invoiceItem : invoiceItems)
			map.put(invoiceItem.getProduct(), invoiceItem);

		for (PackingListItemsVo itemsVo : vo.getItemsVos()) {
			Product product = productDao.findProduct(itemsVo.getProductCode(), company.getId());
			PackingListItems item = new PackingListItems();
			item.setProduct(product);
			item.setQuantity(itemsVo.getQuantity());
			item.setGrossWeight(itemsVo.getGrossWeight());
			item.setNetWeight(itemsVo.getNetWeight());
			item.setNumberOfPack(itemsVo.getNumberOfPack());
			PackingKind packingKind = packingKindDao.findObject(itemsVo.getPackingKindId());
			if (packingKind == null)
				throw new ItemNotFoundException("Packing Kind Not Found");
			item.setPackingKind(packingKind);

			InvoiceItems invoiceItem = map.get(product);
			PackingListItems oldItem = oldMap.get(product);
			if (oldItem != null) {
				invoiceItem.setPackedQuantity(invoiceItem.getPackedQuantity().subtract(oldItem.getQuantity()));
			}
			invoiceItem.setPackedQuantity(invoiceItem.getPackedQuantity().add(item.getQuantity()));
			invoice.getInvoiceItems().add(invoiceItem);
			item.setPackingList(packingList);
			packingList.getPackingListItems().add(item);
		}
		packingList.setInvoice(invoice);
		return packingList;
	}

	public void deletePackingList(Long id) {
		PackingList packingList = this.findPackingList(id);
		if(packingList==null)
			throw new ItemNotFoundException("Packing List Not Found");
		Invoice invoice = packingList.getInvoice();
		Set<InvoiceItems> invoiceItems = invoice.getInvoiceItems();
		Map<Product, InvoiceItems> map = new HashMap<Product, InvoiceItems>();
		for(InvoiceItems invoiceItem : invoiceItems)
			map.put(invoiceItem.getProduct(), invoiceItem);
		for(PackingListItems packingListItem : packingList.getPackingListItems()){
			InvoiceItems invoiceIt = map.get(packingListItem.getProduct());
			invoiceIt.setPackedQuantity(invoiceIt.getPackedQuantity().subtract(packingListItem.getQuantity()));
			invoice.getInvoiceItems().add(invoiceIt);
		}
		invoice.getPackingLists().remove(packingList);
		this.sessionFactory.getCurrentSession().merge(invoice);
	}

	public List<PackingListVo> findAllPackingLists(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PackingList.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PackingList> packingLists = criteria.list();
		List<PackingListVo> vos = new ArrayList<PackingListVo>();
		for (PackingList packingList : packingLists)
			vos.add(createPackingListVo(packingList));
		return vos;
	}

	public PackingList findPackingList(Long id) {
		return (PackingList) this.sessionFactory.getCurrentSession().createCriteria(PackingList.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public PackingListVo findPackingListById(Long id) {
		PackingList packingList = this.findPackingList(id);
		if (packingList == null)
			throw new ItemNotFoundException("Packing List Not Found");
		return createPackingListVo(packingList);
	}

	private PackingListVo createPackingListVo(PackingList packingList) {
		PackingListVo vo = new PackingListVo();
		vo.setCompanyId(packingList.getCompany().getId());
		vo.setDate(DateFormatter.convertDateToString(packingList.getDate()));
		vo.setPackingListId(packingList.getId());
		Invoice invoice = packingList.getInvoice();
		vo.setInvoiceCode(invoice.getReferenceNumber());
		vo.setCode(packingList.getReferenceNumber());
		vo.setInvoiceId(invoice.getId());
		vo.setGrossWeight(packingList.getGrossWeight());
		vo.setNetWeight(packingList.getNetWeight());
		vo.setNumberOfPacks(packingList.getNumberOfPacks());

		Set<InvoiceItems> invoiceItems = invoice.getInvoiceItems();
		Map<Product, InvoiceItems> map = new HashMap<Product, InvoiceItems>();
		for (InvoiceItems invoiceItem : invoiceItems)
			map.put(invoiceItem.getProduct(), invoiceItem);
		Boolean isPacked = true;
		for (PackingListItems items : packingList.getPackingListItems()) {
			Product product = items.getProduct();
			PackingListItemsVo itemsVo = new PackingListItemsVo();
			itemsVo.setProductCode(product.getProductCode());
			itemsVo.setProductName(product.getName());
			itemsVo.setQuantity(items.getQuantity());
			itemsVo.setWeight(product.getAdvanceDetails().getWeight());
			itemsVo.setGrossWeight(items.getGrossWeight());
			itemsVo.setNetWeight(items.getNetWeight());
			itemsVo.setNumberOfPack(items.getNumberOfPack());
			PackingKind packingKind = items.getPackingKind();
			itemsVo.setSelectedKind(packingKind.getId() + "!!!" + packingKind.getWeight());
			itemsVo.setPackingKind(packingKind.getName());
			InvoiceItems invoiceIte = map.get(product);
			BigDecimal quantityToPack = invoiceIte.getDeliveredQuantity().subtract(invoiceIte.getPackedQuantity());
			if (quantityToPack.compareTo(BigDecimal.ZERO) != 0)
				isPacked = false;
			itemsVo.setQuantityToPack(quantityToPack.add(items.getQuantity()));
			vo.getItemsVos().add(itemsVo);
		}
		vo.setIsPacked(isPacked);
		return vo;
	}

	public PackingListVo convertInvoiceToPackingList(Long invoiceId) {
		PackingListVo vo = new PackingListVo();
		Invoice invoice = invoiceDao.findInvoice(invoiceId);
		if (invoice == null)
			throw new ItemNotFoundException("Invoice not found");
		vo.setInvoiceId(invoiceId);
		vo.setDate(DateFormatter.convertDateToString(invoice.getDate()));
		Long i = 1L;
		BigDecimal grossWeight = BigDecimal.ZERO;
		Boolean isPacked = true;
		for (InvoiceItems invoiceItems : invoice.getInvoiceItems()) {
			BigDecimal quantityToPack = invoiceItems.getDeliveredQuantity().subtract(invoiceItems.getPackedQuantity());
			if (quantityToPack.compareTo(BigDecimal.ZERO) != 0) {
				isPacked = false;
				PackingListItemsVo itemsVo = new PackingListItemsVo();
				Product product = invoiceItems.getProduct();
				ProductAdvanceDetails productAdvanceDetails = product.getAdvanceDetails();
				itemsVo.setQuantityToPack(quantityToPack);
				itemsVo.setQuantity(itemsVo.getQuantityToPack());
				itemsVo.setWeight(productAdvanceDetails.getWeight());
				itemsVo.setNetWeight(
						itemsVo.getQuantity().multiply(itemsVo.getWeight()).setScale(2, RoundingMode.HALF_EVEN));
				itemsVo.setProductCode(product.getProductCode());
				itemsVo.setProductName(product.getName());
				itemsVo.setGrossWeight(itemsVo.getNetWeight());
				grossWeight = grossWeight.add(itemsVo.getGrossWeight());
				itemsVo.setNumberOfPack(i);
				vo.getItemsVos().add(itemsVo);
				i++;
			}
		}
		vo.setIsPacked(isPacked);
		vo.setGrossWeight(grossWeight);
		vo.setNetWeight(grossWeight);
		vo.setNumberOfPacks((long) vo.getItemsVos().size());
		return vo;
	}

}
