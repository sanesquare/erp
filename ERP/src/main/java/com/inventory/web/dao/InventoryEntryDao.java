package com.inventory.web.dao;

import java.util.List;

import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.vo.InventoryEntryVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 26, 2015
 */
public interface InventoryEntryDao {

	/**
	 * method to save inventory entry
	 * 
	 * @param vo
	 * @return
	 */
	public Long saveInventoryEntry(InventoryEntryVo vo);

	/**
	 * method to set attributes to inventory entry
	 * 
	 * @param entry
	 * @param vo
	 * @return
	 */
	public InventoryEntry setAttributesToInventoryEntry(InventoryEntry entry, InventoryEntryVo vo,Boolean isDirect);

	/**
	 * method to delete inventory entry
	 * 
	 * @param id
	 */
	public void deleteInventoryEntry(Long id);

	/**
	 * method to list all inventory entries
	 * 
	 * @param companyId
	 * @return
	 */
	public List<InventoryEntryVo> listAllInventoryEntries(Long companyId);

	/**
	 * method to find inventory entry
	 * 
	 * @param id
	 * @return
	 */
	public InventoryEntry findInventoryEntry(Long id);

	public String getNumber(Long companyId, String category);

	/**
	 * method to find inventory entry
	 * @param id
	 * @return
	 */
	public InventoryEntryVo findInventoryEntryById(Long id);
	
	/**
	 * method to find all entries
	 * @param companyId
	 * @return
	 */
	public List<InventoryEntryVo> findAllEntries(Long companyId);
}
