package com.inventory.web.dao;

import java.util.List;

import com.erp.web.entities.Company;
import com.inventory.web.entities.PackingKind;
import com.inventory.web.vo.PackingKindVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
public interface PackingKindDao {

	/**
	 * method to save packing kind
	 * @param vo
	 */
	public void savePackingKing(PackingKindVo vo);
	
	/**
	 * method to delete packing kind
	 * @param id
	 */
	public void deletePackingKing(Long id);
	
	/**
	 * method to find packing kind object by id
	 * @param id
	 * @return
	 */
	public PackingKind findObject(Long id);
	
	/**
	 * method to find packing kind object by name
	 * @param name
	 * @return
	 */
	public PackingKind findObject(String name,Company company);
	
	/**
	 * method to find packing kinds
	 * @param companyId
	 * @return
	 */
	public List<PackingKindVo> findAllPackingKinds(Long companyId);
	
	/**
	 * method to find packing kinds
	 * @param id
	 * @return
	 */
	public PackingKindVo findPackingKind(Long id);
}
