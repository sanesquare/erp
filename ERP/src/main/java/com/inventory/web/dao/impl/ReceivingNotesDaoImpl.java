package com.inventory.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Charge;
import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.Godown;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.dao.GodownDao;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.dao.ReceivingNotesDao;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ReceivingNote;
import com.inventory.web.entities.ReceivingNoteItems;
import com.inventory.web.vo.ReceivingNoteProductVo;
import com.inventory.web.vo.ReceivingNoteVo;
import com.purchase.web.dao.VendorDao;
import com.purchase.web.dao.VendorQuoteDao;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.PurchaseOrderCharges;
import com.purchase.web.entities.Vendor;
import com.purchase.web.entities.VendorQuoteItems;
import com.purchase.web.entities.VendorQuotes;
import com.purchase.web.vo.BillChargeVo;
import com.purchase.web.vo.BillProductVo;
import com.purchase.web.vo.BillVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 22, 2015
 */
@Repository
public class ReceivingNotesDaoImpl extends AbstractDao implements ReceivingNotesDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private GodownDao godownDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private VendorQuoteDao vendorQuoteDao;

	@Autowired
	private VendorDao vendorDao;

	public Long saveOrUpdateReceivingNotes(ReceivingNoteVo noteVo) {
		VendorQuotes purchaseOrder = vendorQuoteDao.findVendorQuoteObject(noteVo.getOrderId());
		Boolean newNote = false;
		Long id = noteVo.getNoteId();
		Company company = companyDao.findCompany(noteVo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found !!!");
		Vendor vendor = vendorDao.findVendor(noteVo.getVendorId());
		if (vendor == null)
			throw new ItemNotFoundException("Vendor Not Found");
		ReceivingNote note = null;
		if (id == null) {
			note = new ReceivingNote();
			newNote = true;
			NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(
					NumberPropertyConstants.RECEIVING_NOTES, noteVo.getCompanyId());
			if (numberPropertyVo != null) {
				String refNumber = numberPropertyVo.getPrefix() + numberPropertyVo.getNumber();
				note.setReferenceNumber(refNumber);
			}

		} else {
			note = findReceivingNoteObject(id);
		}
		note.setPurchaseOrder(purchaseOrder);
		note.setCompany(company);
		company.getReceivingNotes().add(note);
		note.setVendor(vendor);
		note = this.setAttributesForReceivingNote(note, noteVo);
		note = this.setProductsForReceivingNote(note, noteVo);
		if (newNote) {
			if (purchaseOrder == null)
				id = (Long) this.sessionFactory.getCurrentSession().save(note);
			else {
				purchaseOrder.setNoteCode(note.getReferenceNumber());
				// note.setPurchaseOrder(purchaseOrder);
				purchaseOrder.getReceivingNote().add(note);
				this.sessionFactory.getCurrentSession().merge(purchaseOrder);
				// id = purchaseOrder.getReceivingNote().getId();
			}
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.RECEIVING_NOTES, company);
		} else {
			this.sessionFactory.getCurrentSession().saveOrUpdate(note);
		}
		id = note.getId();
		return id;
	}

	private ReceivingNote setAttributesForReceivingNote(ReceivingNote note, ReceivingNoteVo noteVo) {
		note.setDate(DateFormatter.convertStringToDate(noteVo.getDate()));
		/*
		 * note.setDiscountPercentage(noteVo.getDiscountRate());
		 * note.setDiscountAmount(noteVo.getDiscountRate());
		 * note.setGrandTotal(noteVo.getGrandTotal());
		 * note.setNetAmount(noteVo.getNetAmountHidden());
		 * note.setNetTotal(noteVo.getNetTotal());
		 * note.setSubTotal(noteVo.getSubTotal());
		 * note.setTaxTotal(noteVo.getTaxTotal()); note.getVat().clear();
		 * List<VAT> taxes = new ArrayList<VAT>(); for (Long id :
		 * noteVo.getTsxIds()) { VAT vat = vatDao.findTaxObject(id);
		 * taxes.add(vat); } note.getVat().addAll(taxes);
		 */
		return note;
	}

	private ReceivingNote setProductsForReceivingNote(ReceivingNote note, ReceivingNoteVo noteVo) {
		VendorQuotes order = note.getPurchaseOrder();
		List<ReceivingNoteItems> items = new ArrayList<ReceivingNoteItems>();
		Set<ReceivingNoteItems> oldItems = note.getItems();
		Map<Product, ReceivingNoteItems> oldItemMap = new HashMap<Product, ReceivingNoteItems>();
		for (ReceivingNoteItems item : oldItems)
			oldItemMap.put(item.getProduct(), item);
		Set<VendorQuoteItems> orderItems = new HashSet<VendorQuoteItems>();
		if (order != null) {
			orderItems = order.getItems();
		}
		Map<Product, VendorQuoteItems> map = new HashMap<Product, VendorQuoteItems>();
		for (VendorQuoteItems item : orderItems)
			map.put(item.getProduct(), item);
		note.getItems().clear();
		for (ReceivingNoteProductVo vo : noteVo.getProductVos()) {
			Godown godown = godownDao.findGodownObject(vo.getGodownId());
			if (godown == null)
				throw new ItemNotFoundException("Godown not found.");
			ReceivingNoteItems item = new ReceivingNoteItems();

			/*
			 * // set inventory entry InventoryEntry entry = new
			 * InventoryEntry(); InventoryEntryVo entryVo = new
			 * InventoryEntryVo(); entryVo.setDate(noteVo.getDate());
			 * InventoryEntryItemsVo entryItemsVo = new InventoryEntryItemsVo();
			 * entryItemsVo.setProductCode(vo.getProductCode());
			 * entryItemsVo.setUnitPrice(vo.getUnitPrice());
			 * entryItemsVo.setTotal(vo.getNetAmount());
			 * entryItemsVo.setQuantity(vo.getQuantity());
			 * entryVo.getItemsVos().add(entryItemsVo); entryVo.setNarration(
			 * "Inventory Entry For Receiving Note " +
			 * note.getReferenceNumber());
			 * entry.setCode(inventoryEntryDao.getNumber
			 * (note.getCompany().getId(),
			 * NumberPropertyConstants.INVENTORY_ENTRY));
			 * entry.setCompany(note.getCompany()); entry.setType("Receipt");
			 * entry.setGodown(godown); entry =
			 * inventoryEntryDao.setAttributesToInventoryEntry(entry, entryVo);
			 * entry.setReceivingNoteItems(item); item.setInventoryEntry(entry);
			 */
			Product product = productDao.findProduct(vo.getProductCode(), noteVo.getCompanyId());
			/*
			 * VAT tax = vatDao.findTaxObject(vo.getTaxId()); if (tax != null)
			 * item.setVat(tax);
			 */
			item.setGodown(godown);
			item.setProduct(product);
			product.getReceivingNoteItems().add(item);
			item.setQuantity(vo.getQuantity());
			/*
			 * item.setPrice(vo.getUnitPrice());
			 * item.setDiscountPercentage(vo.getDiscount());
			 * item.setTotal(vo.getNetAmount());
			 * item.setAmountExcludingTax(vo.getAmountExcludingTax());
			 */
			item.setReceivingNote(note);
			items.add(item);

			if (order != null) {
				VendorQuoteItems orderItem = map.get(product);
				if (orderItem != null) {
					BigDecimal receivedQuantity = orderItem.getReceivedQuantity();
					ReceivingNoteItems oldItem = oldItemMap.get(product);
					if (oldItem != null) {
						if (receivedQuantity != null)
							orderItem.setReceivedQuantity(receivedQuantity.subtract(oldItem.getQuantity()));
						else
							orderItem.setReceivedQuantity(oldItem.getQuantity());
					}
					if (receivedQuantity != null)
						orderItem.setReceivedQuantity(receivedQuantity.add(item.getQuantity()));
					else
						orderItem.setReceivedQuantity(item.getQuantity());

					order.getItems().add(orderItem);
				}
			}
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.INVENTORY_ENTRY, note.getCompany());
		}
		if (order != null) {
			Boolean hasReceived = true;
			for (VendorQuoteItems orderItem : order.getItems()) {
				if (orderItem.getReceivedQuantity() != null) {
					if (orderItem.getReceivedQuantity().compareTo(orderItem.getQuantity()) != 0)
						hasReceived = false;
				} else
					hasReceived = false;
			}
			order.setHasReceived(hasReceived);
		}
		note.setPurchaseOrder(order);
		note.getItems().addAll(items);
		return note;
	}

	public void deleteReceivingNote(Long id) {
		ReceivingNote note = findReceivingNoteObject(id);
		if (note == null)
			throw new ItemNotFoundException("Receiving Note Not Found");
		VendorQuotes order = note.getPurchaseOrder();
		if (order != null) {
			Boolean hasReceived = true;
			Set<VendorQuoteItems> orderItems = order.getItems();
			Map<Product, VendorQuoteItems> map = new HashMap<Product, VendorQuoteItems>();
			for (VendorQuoteItems orderItem : orderItems)
				map.put(orderItem.getProduct(), orderItem);
			for (ReceivingNoteItems item : note.getItems()) {
				VendorQuoteItems orderItem = map.get(item.getProduct());
				orderItem.setReceivedQuantity(orderItem.getReceivedQuantity().subtract(item.getQuantity()));
				order.getItems().add(orderItem);
				if (orderItem.getQuantity().compareTo(orderItem.getReceivedQuantity()) != 0)
					hasReceived = false;
			}
			order.setHasReceived(hasReceived);
			order.getReceivingNote().remove(note);
			this.sessionFactory.getCurrentSession().merge(order);

		} else
			this.sessionFactory.getCurrentSession().delete(note);

	}

	public ReceivingNoteVo findReceivingNote(Long id) {
		ReceivingNote note = findReceivingNoteObject(id);
		if (note == null)
			throw new ItemNotFoundException("Receiving Note Not Found");
		return this.createVo(note);
	}

	private ReceivingNoteVo createVo(ReceivingNote note) {
		ReceivingNoteVo vo = new ReceivingNoteVo();
		vo.setNoteId(note.getId());
		vo.setNoteCode(note.getReferenceNumber());
		Company company = note.getCompany();
		Bill bill = note.getBill();
		if (bill != null) {
			vo.setIsConverted(true);
			vo.setBillCode(bill.getVoucherNumber());
			vo.setIsView(true);
			vo.setBillId(bill.getId());
		}
		VendorQuotes vendorQuotes = note.getPurchaseOrder();
		if (vendorQuotes != null) {
			vo.setOrderId(vendorQuotes.getId());
			vo.setOrderCode(vendorQuotes.getOrderCode());
			vo.setQuoteCode(vendorQuotes.getQuoteCode());
		}
		vo.setCompanyId(company.getId());
		vo.setNoteCode(note.getReferenceNumber());
		Vendor vendor = note.getVendor();
		vo.setVendor(vendor.getName());
		vo.setVendorId(vendor.getId());
		vo.setDate((DateFormatter.convertDateToString(note.getDate())));
		/*
		 * vo.setSubTotal(note.getSubTotal());
		 * vo.setDiscountRate(note.getDiscountPercentage());
		 * vo.setDiscountTotal(note.getDiscountAmount());
		 * vo.setNetTotal(note.getNetTotal());
		 * vo.setTaxTotal(note.getTaxTotal());
		 * vo.setGrandTotal(note.getGrandTotal()); Set<VAT> taxes =
		 * note.getVat(); for (VAT tax : taxes) {
		 * vo.getTaxes().add(tax.getName());
		 * vo.getTaxesWithRate().add(tax.getId() + "!!!" + tax.getPercentage());
		 * }
		 */
		vo = this.createNoteProductVo(vo, note);
		return vo;
	}

	private ReceivingNoteVo createNoteProductVo(ReceivingNoteVo vo, ReceivingNote note) {

		VendorQuotes order = note.getPurchaseOrder();
		Set<VendorQuoteItems> orderItems = new HashSet<VendorQuoteItems>();
		Map<Product, VendorQuoteItems> map = new HashMap<Product, VendorQuoteItems>();
		if (order != null) {
			orderItems = order.getItems();
		}
		for (VendorQuoteItems item : orderItems) {
			map.put(item.getProduct(), item);
		}
		List<ReceivingNoteProductVo> productVos = new ArrayList<ReceivingNoteProductVo>();
		for (ReceivingNoteItems items : note.getItems()) {
			BigDecimal toBeReceived = BigDecimal.ZERO;
			ReceivingNoteProductVo productVo = new ReceivingNoteProductVo();
			Product product = items.getProduct();
			// productVo.setAmountExcludingTax(items.getAmountExcludingTax());
			// vo.setNetAmountHidden(vo.getNetAmountHidden().add(items.getAmountExcludingTax()));
			productVo.setProductCode(product.getProductCode());
			productVo.setProductName(product.getName());
			productVo.setProductId(product.getId());
			productVo.setProductId(items.getId());
			// taxAmount =
			// taxAmount.add(items.getTotal().subtract(items.getAmountExcludingTax()));
			productVo.setQuantity(items.getQuantity());
			VendorQuoteItems orderItem = map.get(product);
			if (orderItem != null) {
				if (orderItem.getReceivedQuantity() != null)
					toBeReceived = orderItem.getQuantity().subtract(orderItem.getReceivedQuantity())
							.add(items.getQuantity());
				else
					toBeReceived = orderItem.getQuantity();
			}
			productVo.setQuantityToReceive(toBeReceived);
			Godown godown = items.getGodown();
			productVo.setGodown(godown.getName());
			productVo.setGodownId(godown.getId());
			/*
			 * productVo.setUnitPrice(items.getPrice());
			 * productVo.setDiscount(items.getDiscountPercentage());
			 * productVo.setNetAmount(items.getTotal()); VAT tax =
			 * items.getVat(); if (tax != null) {
			 * productVo.setTax(tax.getName()); }
			 */
			productVos.add(productVo);
		}
		vo.setProductVos(productVos);
		return vo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ReceivingNoteVo> listReceivingNotesForCompany(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		List<ReceivingNoteVo> list = new ArrayList<ReceivingNoteVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReceivingNote.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<ReceivingNote> notes = criteria.list();
		if (notes != null) {
			for (ReceivingNote note : notes) {
				list.add(this.createVo(note));
			}
		}
		return list;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ReceivingNoteVo> listReceivingNotesForCompanyWithinDateRange(Long companyId, String startDate,
			String endDate) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		List<ReceivingNoteVo> list = new ArrayList<ReceivingNoteVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReceivingNote.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.between("date", DateFormatter.convertStringToDate(startDate),
				DateFormatter.convertStringToDate(endDate)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<ReceivingNote> notes = criteria.list();
		if (notes != null) {
			for (ReceivingNote note : notes) {
				list.add(this.createVo(note));
			}
		}
		return list;
	}

	public ReceivingNote findReceivingNoteObject(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReceivingNote.class);
		criteria.add(Restrictions.eq("id", id));
		return (ReceivingNote) criteria.uniqueResult();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public BillVo convertReceivingNoteToBill(List<Long> receivingNoteIds) {
		BillVo billVo = new BillVo();
		List<ReceivingNote> receivingNotes = new ArrayList<ReceivingNote>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReceivingNote.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.in("id", receivingNoteIds));
		receivingNotes = criteria.list();
		ReceivingNoteVo noteVo = new ReceivingNoteVo();
		Boolean isFirst = true;
		List<ReceivingNoteProductVo> productVos = new ArrayList<ReceivingNoteProductVo>();
		Map<String, BillChargeVo> chargeMap = new HashMap<String, BillChargeVo>();
		for (ReceivingNote note : receivingNotes) {
			billVo.getReceivingNoteIds().add(note.getId());
			VendorQuotes order = note.getPurchaseOrder();
			if (order != null) {
				Charge charge = null;
				String chargeName = null;
				for (PurchaseOrderCharges charges : order.getCharges()) {
					charge = charges.getCharge();
					chargeName = charge.getName();
					BillChargeVo chargeVo = chargeMap.get(chargeName);
					if (chargeVo == null) {
						chargeVo = new BillChargeVo();
					}
					chargeVo.setCharge(charge.getName());
					chargeVo.setChargeId(charge.getId());
					if (!chargeVo.getPurchaseOrderCharges().contains(charges))
						chargeVo.setAmount(chargeVo.getAmount().add(charges.getAmount()));
					chargeVo.getPurchaseOrderCharges().add(charges);
					chargeMap.put(chargeName, chargeVo);
				}
			}
			if (isFirst) {
				Vendor vendor = note.getVendor();
				noteVo.setVendor(vendor.getName());
				noteVo.setVendorId(vendor.getId());
				if (order != null) {
					PaymentTerms paymentTerms = order.getPaymentTerm();
					ErPCurrency currency = order.getCurrency();
					noteVo.setPaymentTerm(paymentTerms.getTerm());
					noteVo.setPaymentTermId(paymentTerms.getId());
					DeliveryMethod deliveryMethod = order.getDeliveryTerm();
					if (deliveryMethod != null) {
						noteVo.setDeliveryTerm(deliveryMethod.getMethod());
						noteVo.setDeliveryTermId(deliveryMethod.getId());
					}
					noteVo.setCurrency(currency.getBaseCurrency().getBaseCurrency());
					noteVo.setErpCurrencyId(currency.getId());
					isFirst = false;
				}
			}
			if (order != null) {
				noteVo.setSubTotal(noteVo.getSubTotal().add(order.getSubTotal()));
				noteVo.setDiscountRate(noteVo.getDiscountRate().add(order.getDiscountRate()));
				noteVo.setDiscountTotal(noteVo.getDiscountTotal().add(order.getDiscountAmount()));
				noteVo.setNetTotal(noteVo.getNetTotal().add(order.getNetTotal()));
				noteVo.setGrandTotal(noteVo.getGrandTotal().add(order.getTotal()));
				for (VAT tax : order.getVat()) {
					noteVo.getTaxes().add(tax.getName());
					noteVo.getTsxIds().add(tax.getId());
					noteVo.getTaxesWithRate().add(tax.getId() + "!!!" + tax.getPercentage());
				}
			}
			for (ReceivingNoteItems items : note.getItems()) {
				ReceivingNoteProductVo productVo = new ReceivingNoteProductVo();
				Product product = items.getProduct();
				if (order != null) {
					VendorQuoteItems orderItem = (VendorQuoteItems) this.sessionFactory.getCurrentSession()
							.createCriteria(VendorQuoteItems.class).add(Restrictions.eq("vendorQuote", order))
							.add(Restrictions.eq("product", product)).uniqueResult();
					productVo.setUnitPrice(orderItem.getPrice());
					productVo.setDiscount(orderItem.getDicscountRate());
					BigDecimal netAmount = items.getQuantity().multiply(orderItem.getPrice())
							.setScale(2, RoundingMode.HALF_EVEN);
					BigDecimal discountAmount = netAmount.multiply(orderItem.getDicscountRate()).divide(
							new BigDecimal(100), 2, RoundingMode.HALF_EVEN);
					netAmount = netAmount.subtract(discountAmount);
					productVo.setNetAmount(netAmount);
					productVo.setAmountExcludingTax(netAmount);
					VAT tax = orderItem.getVat();
					if (tax != null) {
						productVo.setTax(tax.getName());
						productVo.setNetAmount(netAmount.add(productVo.getNetAmount().multiply(tax.getPercentage())
								.divide(new BigDecimal(100), 2, RoundingMode.HALF_EVEN)));
					}
				} else {
					productVo.setUnitPrice(BigDecimal.ZERO);
					productVo.setDiscount(BigDecimal.ZERO);
					productVo.setNetAmount(BigDecimal.ZERO);
				}
				productVo.setProductCode(product.getProductCode());
				productVo.setProductName(product.getName());
				productVo.setProductId(product.getId());
				productVo.setProductId(items.getId());
				productVo.setQuantity(items.getQuantity());
				Godown godown = items.getGodown();
				productVo.setGodown(godown.getName());
				productVo.setGodownId(godown.getId());
				productVos.add(productVo);
			}
		}
		for (Map.Entry<String, BillChargeVo> entry : chargeMap.entrySet()) {
			billVo.getChargeVos().add(entry.getValue());
		}
		noteVo.setProductVos(sortProductVos(productVos));
		billVo = createBillVo(billVo, noteVo);
		return billVo;
	}

	private List<ReceivingNoteProductVo> sortProductVos(List<ReceivingNoteProductVo> list) {
		Map<String, ReceivingNoteProductVo> map = new HashMap<String, ReceivingNoteProductVo>();
		List<ReceivingNoteProductVo> vos = new ArrayList<ReceivingNoteProductVo>();
		for (ReceivingNoteProductVo vo : list) {
			String key = vo.getGodown() + "!!!" + vo.getProductCode();
			ReceivingNoteProductVo newVo = map.get(key);
			if (newVo != null) {
				newVo.setQuantity(newVo.getQuantity().add(vo.getQuantity()));
				newVo.setNetAmount(newVo.getNetAmount().add(vo.getNetAmount()));
				newVo.setAmountExcludingTax(newVo.getAmountExcludingTax().add(vo.getAmountExcludingTax()));
				map.put(key, newVo);
			} else {
				map.put(key, vo);
			}
		}
		for (Map.Entry<String, ReceivingNoteProductVo> entry : map.entrySet()) {
			vos.add(entry.getValue());
		}
		return vos;
	}

	private BillVo createBillVo(BillVo billVo, ReceivingNoteVo noteVo) {
		billVo.setVendor(noteVo.getVendor());
		billVo.setVendorId(noteVo.getVendorId());
		billVo.setBillDate(DateFormatter.convertDateToString(new Date()));
		billVo.setPaymentTermId(noteVo.getPaymentTermId());
		billVo.setErpCurrencyId(noteVo.getErpCurrencyId());
		billVo.setDeliveryTermId(noteVo.getDeliveryTermId());
		billVo.setDeliveryTerm(noteVo.getDeliveryTerm());
		billVo.setCurrecny(noteVo.getCurrency());
		billVo.setSubTotal(noteVo.getSubTotal());
		billVo.setDiscountRate(noteVo.getDiscountRate());
		billVo.setDiscountTotal(noteVo.getDiscountTotal());
		billVo.setNetTotal(noteVo.getNetTotal());
		billVo.setTaxTotal(noteVo.getTaxTotal());
		billVo.setGrandTotal(noteVo.getGrandTotal());
		billVo.setTaxesWithRate(noteVo.getTaxesWithRate());
		billVo.setTaxes(noteVo.getTaxes());
		billVo = this.createBillProductVo(billVo, noteVo);
		return billVo;
	}

	private BillVo createBillProductVo(BillVo vo, ReceivingNoteVo noteVo) {
		List<BillProductVo> productVos = new ArrayList<BillProductVo>();
		BigDecimal taxAmount = new BigDecimal("0.00");
		for (ReceivingNoteProductVo items : noteVo.getProductVos()) {
			BillProductVo productVo = new BillProductVo();
			productVo.setGodownId(items.getGodownId());
			productVo.setAmountExcludingTax(items.getAmountExcludingTax());
			vo.setNetAmountHidden(vo.getNetAmountHidden().add(items.getAmountExcludingTax()));
			productVo.setProductCode(items.getProductCode());
			productVo.setProductName(items.getProductName());
			productVo.setProductId(items.getProductId());
			taxAmount = taxAmount.add(items.getNetAmount().subtract(items.getAmountExcludingTax()));
			productVo.setQuantity(items.getQuantity());
			productVo.setUnitPrice(items.getUnitPrice());
			productVo.setDiscount(items.getDiscount());
			productVo.setNetAmount(items.getNetAmount());
			productVo.setTax(items.getTax());
			productVos.add(productVo);
		}
		vo.setProductsTaxAmount(taxAmount);
		vo.setProductVos(productVos);
		return vo;
	}
}
