package com.inventory.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Charge;
import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.Godown;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.dao.DeliveryNoteDao;
import com.inventory.web.dao.GodownDao;
import com.inventory.web.dao.InventoryWithdrawalDao;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.entities.DeliveryNote;
import com.inventory.web.entities.DeliveryNoteItems;
import com.inventory.web.entities.InventoryTransaction;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.InventoryWithdrawalItems;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ReceivingNoteItems;
import com.inventory.web.vo.DeliveryNoteProductVo;
import com.inventory.web.vo.DeliveryNoteVo;
import com.inventory.web.vo.InventoryWithdrawalItemsVo;
import com.inventory.web.vo.InventoryWithdrawalVo;
import com.purchase.web.entities.VendorQuoteItems;
import com.sales.web.dao.CustomerDao;
import com.sales.web.dao.SalesOrderDao;
import com.sales.web.entities.Customer;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.InvoiceItems;
import com.sales.web.entities.SalesOrder;
import com.sales.web.entities.SalesOrderCharges;
import com.sales.web.entities.SalesOrderItems;
import com.sales.web.vo.InvoiceChargeVo;
import com.sales.web.vo.InvoiceProductVo;
import com.sales.web.vo.InvoiceVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 29, 2015
 */
@Repository
public class DeliveryNoteDaoImpl extends AbstractDao implements DeliveryNoteDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private GodownDao godownDao;

	@Autowired
	private InventoryWithdrawalDao inventoryWithdrawalDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private CustomerDao customerDao;

	@Autowired
	private SalesOrderDao salesOrderDao;

	public Long saveOrUpdateDeliveryNotes(DeliveryNoteVo noteVo) {
		SalesOrder salesOrder = salesOrderDao.findSalesOrder(noteVo.getOrderId());
		Boolean newNote = false;
		Long id = noteVo.getNoteId();
		Company company = companyDao.findCompany(noteVo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found !!!");
		Customer customer = customerDao.findCustomerObjectById(noteVo.getCustomerId());
		if (customer == null)
			throw new ItemNotFoundException("Customer Not Found.");
		DeliveryNote note = null;
		if (id == null) {
			note = new DeliveryNote();
			newNote = true;
			NumberPropertyVo numberPropertyVo = numberPropertyDao
					.findNumberProperty(NumberPropertyConstants.DELIVERY_NOTES, noteVo.getCompanyId());
			if (numberPropertyVo != null) {
				String refNumber = numberPropertyVo.getPrefix() + numberPropertyVo.getNumber();
				note.setReferenceNumber(refNumber);
			}
		} else {
			note = this.findDeliveryNoteObject(id);
		}
		note.setSalesOrder(salesOrder);
		note.setCustomer(customer);
		note.setCompany(company);
		note.setDate(DateFormatter.convertStringToDate(noteVo.getDate()));
		note = this.setProductsToDeliverNote(note, noteVo);
		company.getDeliveryNotes().add(note);
		if (newNote) {
			if (salesOrder == null) {
				id = (Long) this.sessionFactory.getCurrentSession().save(note);
			} else {
				salesOrder.setNoteCode(note.getReferenceNumber());
				//note.setSalesOrder(salesOrder);
				//salesOrder.setDeliveryNote(note);
				salesOrder.getDeliveryNote().add(note);
				id = (Long) this.sessionFactory.getCurrentSession().save(salesOrder);
				salesOrder = salesOrderDao.findSalesOrder(id);
				//id = salesOrder.getDeliveryNote().getId();
			}
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.DELIVERY_NOTES, company);
		} else {
			this.sessionFactory.getCurrentSession().merge(note);
		}
		id= note.getId();
		return id;
	}

	public Long saveOrUpdateDeliveryNotesFromInvoice(DeliveryNoteVo noteVo) {
		Invoice invoice = (Invoice) this.sessionFactory.getCurrentSession().createCriteria(Invoice.class)
				.add(Restrictions.eq("id", noteVo.getInvoiceId())).uniqueResult();
		if (invoice == null)
			throw new ItemNotFoundException("Invoice Not Found");
		Boolean newNote = false;
		Long id = noteVo.getNoteId();
		Company company = invoice.getCompany();
		if (company == null)
			throw new ItemNotFoundException("Company Not Found !!!");
		Customer customer = invoice.getCustomer();
		if (customer == null)
			throw new ItemNotFoundException("Customer Not Found.");
		DeliveryNote note = null;
		if (id == null) {
			note = new DeliveryNote();
			newNote = true;
			NumberPropertyVo numberPropertyVo = numberPropertyDao
					.findNumberProperty(NumberPropertyConstants.DELIVERY_NOTES, noteVo.getCompanyId());
			if (numberPropertyVo != null) {
				String refNumber = numberPropertyVo.getPrefix() + numberPropertyVo.getNumber();
				note.setReferenceNumber(refNumber);
			}
		} else {
			note = this.findDeliveryNoteObject(id);
		}
		note.setCustomer(customer);
		note.setCompany(company);
		note.setDate(DateFormatter.convertStringToDate(noteVo.getDate()));
		note.setInvoice(invoice);
		invoice.getDeliveryNotes().add(note);
		note = this.setProductsToDeliverNoteFromInvoice(note, noteVo, invoice);
		company.getDeliveryNotes().add(note);
		if (newNote) {
			id = (Long) this.sessionFactory.getCurrentSession().save(note);
			this.sessionFactory.getCurrentSession().merge(invoice);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.DELIVERY_NOTES, company);
		} else {
			this.sessionFactory.getCurrentSession().merge(note);
		}
		return id;
	}

	private DeliveryNote setProductsToDeliverNote(DeliveryNote note, DeliveryNoteVo noteVo) {
		
		SalesOrder order = note.getSalesOrder();
		Set<DeliveryNoteItems> oldItems = note.getItems();
		Map<Product, DeliveryNoteItems> oldItemMap = new HashMap<Product, DeliveryNoteItems>();
		for (DeliveryNoteItems item : oldItems)
			oldItemMap.put(item.getProduct(), item);
		Set<SalesOrderItems> orderItems = new HashSet<SalesOrderItems>();
		if (order != null) {
			orderItems = order.getOrderItems();
		}
		Map<Product, SalesOrderItems> map = new HashMap<Product, SalesOrderItems>();
		for (SalesOrderItems item : orderItems)
			map.put(item.getProduct(), item);
		
		
		note.getItems().clear();
		List<DeliveryNoteItems> items = new ArrayList<DeliveryNoteItems>();
		for (DeliveryNoteProductVo vo : noteVo.getProductVos()) {
			Godown godown = godownDao.findGodownObject(vo.getGodownId());
			if (godown == null)
				throw new ItemNotFoundException("Godown not found.");
			DeliveryNoteItems item = new DeliveryNoteItems();

			Product product = productDao.findProduct(vo.getProductCode(), noteVo.getCompanyId());
			item.setGodown(godown);
			item.setProduct(product);
			product.getDeliveryNoteItems().add(item);
			item.setQuantity(vo.getQuantity());
			item.setDeliveryNote(note);
			items.add(item);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.INVENTORY_WITHDRAWAL, note.getCompany());
			
			if (order != null) {
				SalesOrderItems orderItem = map.get(product);
				if (orderItem != null) {
					BigDecimal deliveredQuantity = orderItem.getDeliveredQuantity();
 					DeliveryNoteItems oldItem = oldItemMap.get(product);
					if (oldItem != null) {
						if(deliveredQuantity!=null)
							orderItem.setDeliveredQuantity(deliveredQuantity.subtract(oldItem.getQuantity()));
						else
							orderItem.setDeliveredQuantity(oldItem.getQuantity());
					}
					if(deliveredQuantity!=null)
						orderItem.setDeliveredQuantity(deliveredQuantity.add(item.getQuantity()));
					else
						orderItem.setDeliveredQuantity(item.getQuantity());

					order.getOrderItems().add(orderItem);
				}
			}
		}
		if (order != null) {
			Boolean hasDelivered = true;
			for (SalesOrderItems orderItem : order.getOrderItems()) {
				if (orderItem.getDeliveredQuantity() != null) {
					if (orderItem.getDeliveredQuantity().compareTo(orderItem.getQuantity()) != 0)
						hasDelivered = false;
				} else
					hasDelivered = false;
			}
			order.setHasDelivered(hasDelivered);
		}
		note.setSalesOrder(order);
		note.getItems().addAll(items);
		return note;
	}

	private DeliveryNote setProductsToDeliverNoteFromInvoice(DeliveryNote note, DeliveryNoteVo noteVo,
			Invoice invoice) {
		note.getItems().clear();
		List<DeliveryNoteItems> items = new ArrayList<DeliveryNoteItems>();
		Company company = invoice.getCompany();
		Set<InvoiceItems> invoiceItems = invoice.getInvoiceItems();
		Map<Product, InvoiceItems> invoicItemMap = new HashMap<Product, InvoiceItems>();
		for (InvoiceItems item : invoiceItems)
			invoicItemMap.put(item.getProduct(), item);
		for (DeliveryNoteProductVo vo : noteVo.getProductVos()) {
			Godown godown = godownDao.findGodownObject(vo.getGodownId());
			if (godown == null)
				throw new ItemNotFoundException("Godown not found.");
			DeliveryNoteItems item = new DeliveryNoteItems();

			Product product = productDao.findProduct(vo.getProductCode(), noteVo.getCompanyId());
			InvoiceItems invoiceItem = invoicItemMap.get(product);
			invoiceItem.setDeliveredQuantity(invoiceItem.getDeliveredQuantity().add(vo.getQuantity()));
			InventoryWithdrawal inventoryWithdrawal = invoiceItem.getInventoryWithdrawal();
			InventoryWithdrawalItems withdrawalIt = null;
			for (InventoryWithdrawalItems withdrawalItem : inventoryWithdrawal.getWithdrawalItems())
				withdrawalIt = withdrawalItem;
			InventoryTransaction inventoryTransaction = withdrawalIt.getTransaction();
			inventoryTransaction.setQuantityMinus(inventoryTransaction.getQuantityMinus().subtract(vo.getQuantity()));
			withdrawalIt.setTransaction(inventoryTransaction);
			if (inventoryTransaction.getQuantityMinus().compareTo(BigDecimal.ZERO) != 0) {
				inventoryWithdrawal.getWithdrawalItems().clear();
				inventoryWithdrawal.getWithdrawalItems().add(withdrawalIt);
				invoiceItem.setInventoryWithdrawal(inventoryWithdrawal);
			} else
				invoiceItem.setInventoryWithdrawal(null);
			invoice.getInvoiceItems().add(invoiceItem);

			InventoryWithdrawal withdrawal = new InventoryWithdrawal();
			InventoryWithdrawalVo withdrawalVo = new InventoryWithdrawalVo();
			withdrawalVo.setDate(noteVo.getDate());
			InventoryWithdrawalItemsVo withdrawalItemsVo = new InventoryWithdrawalItemsVo();
			withdrawalItemsVo.setProductCode(product.getProductCode());
			withdrawalItemsVo.setUnitPrice(inventoryTransaction.getUnitPrice());
			withdrawalItemsVo.setQuantity(vo.getQuantity());
			withdrawalItemsVo.setTotal(vo.getQuantity().multiply(inventoryTransaction.getUnitPrice()));
			withdrawalVo.getItemsVos().add(withdrawalItemsVo);
			withdrawalVo.setNarration("Inventory Withdrawal For Invoice " + invoice.getReferenceNumber());
			withdrawal.setCode(
					inventoryWithdrawalDao.getNumber(company.getId(), NumberPropertyConstants.INVENTORY_WITHDRAWAL));
			withdrawal.setCompany(company);
			withdrawal.setType("Receipt");
			withdrawal.setGodown(godown);
			withdrawal = inventoryWithdrawalDao.setAttributesToInventoryWithdrawal(withdrawal, withdrawalVo, false);
			withdrawal.setDeliveryNoteItems(item);
			item.setInventoryWithdrawal(withdrawal);

			item.setGodown(godown);
			item.setProduct(product);
			// product.getDeliveryNoteItems().add(item);
			item.setQuantity(vo.getQuantity());
			item.setDeliveryNote(note);
			items.add(item);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.INVENTORY_WITHDRAWAL, note.getCompany());
		}
		note.getItems().addAll(items);
		return note;
	}

	public void deleteDeliveryNote(Long id) {
		DeliveryNote note = this.findDeliveryNoteObject(id);
		if (note == null)
			throw new ItemNotFoundException("Delivery Note Not Found");
		SalesOrder order = note.getSalesOrder();
		/*
		 * Invoice invoice = note.getInvoice(); if (invoice == null) {
		 */
		if (order != null) {
			Boolean hasDelivered = true;
			Set<SalesOrderItems> orderItems = order.getOrderItems();
			Map<Product, SalesOrderItems> map = new HashMap<Product, SalesOrderItems>();
			for (SalesOrderItems orderItem : orderItems)
				map.put(orderItem.getProduct(), orderItem);
			for (DeliveryNoteItems item : note.getItems()) {
				SalesOrderItems orderItem = map.get(item.getProduct());
				orderItem.setDeliveredQuantity(orderItem.getDeliveredQuantity().subtract(item.getQuantity()));
				order.getOrderItems().add(orderItem);
				if(orderItem.getQuantity().compareTo(orderItem.getDeliveredQuantity()) != 0)
					hasDelivered = false;
			}
			order.setHasDelivered(hasDelivered);
			order.getDeliveryNote().remove(note);
			this.sessionFactory.getCurrentSession().merge(order);
		} else
			this.sessionFactory.getCurrentSession().delete(note);
		/*
		 * } else { if (order != null) { note.setSalesOrder(null); }
		 * Set<InvoiceItems> invoiceItems = invoice.getInvoiceItems();
		 * Set<DeliveryNoteItems> noteItems = note.getItems(); Map<Product,
		 * InvoiceItems> map = new HashMap<Product, InvoiceItems>(); for
		 * (InvoiceItems invoiceItem : invoiceItems)
		 * map.put(invoiceItem.getProduct(), invoiceItem); for
		 * (DeliveryNoteItems deliveryNoteItem : noteItems) { InvoiceItems
		 * invoiceIt = map.get(deliveryNoteItem.getProduct());
		 * invoiceIt.setDeliveredQuantity(invoiceIt.getDeliveredQuantity()
		 * .subtract(deliveryNoteItem.getQuantity())); InventoryWithdrawal
		 * inventoryWithdrawal = invoiceIt.getInventoryWithdrawal(); if
		 * (inventoryWithdrawal != null) {
		 * inventoryWithdrawal.setGodown(godownDao.findGodownObjectByName(
		 * InventoryConstants.TO_BE_ISSUED_GODOWN, invoice.getCompany())); }
		 * invoiceIt.setInventoryWithdrawal(inventoryWithdrawal);
		 * invoice.getInvoiceItems().add(invoiceIt); }
		 * invoice.getDeliveryNotes().remove(note);
		 * this.sessionFactory.getCurrentSession().delete(note);
		 * this.sessionFactory.getCurrentSession().merge(invoice);
		 * 
		 * }
		 */
	}

	public DeliveryNoteVo findDeliveryNote(Long id) {
		DeliveryNote note = this.findDeliveryNoteObject(id);
		if (note == null)
			throw new ItemNotFoundException("Delivery Note Not Found");
		return createVo(note);
	}

	private DeliveryNoteVo createVo(DeliveryNote note) {
		DeliveryNoteVo vo = new DeliveryNoteVo();
		vo.setNoteId(note.getId());
		vo.setNoteCode(note.getReferenceNumber());
		Company company = note.getCompany();
		Invoice invoice = note.getInvoice();
		if (invoice != null) {
			vo.setIsConverted(true);
			vo.setInvoiceCode(invoice.getReferenceNumber());
			vo.setInvoiceId(invoice.getId());
		}
		vo.setCompanyId(company.getId());
		vo.setNoteCode(note.getReferenceNumber());
		Customer customer = note.getCustomer();
		vo.setCustomer(customer.getName());
		vo.setCustomerId(customer.getId());
		vo.setDate((DateFormatter.convertDateToString(note.getDate())));
		vo = this.createNoteProductVo(vo, note);
		return vo;
	}

	private DeliveryNoteVo createNoteProductVo(DeliveryNoteVo vo, DeliveryNote note) {
		List<DeliveryNoteProductVo> productVos = new ArrayList<DeliveryNoteProductVo>();
		for (DeliveryNoteItems items : note.getItems()) {
			DeliveryNoteProductVo productVo = new DeliveryNoteProductVo();
			Product product = items.getProduct();
			productVo.setProductCode(product.getProductCode());
			productVo.setProductName(product.getName());
			productVo.setProductId(product.getId());
			productVo.setProductId(items.getId());
			productVo.setQuantity(items.getQuantity());
			Godown godown = items.getGodown();
			productVo.setGodown(godown.getName());
			productVo.setGodownId(godown.getId());
			productVos.add(productVo);
		}
		vo.setProductVos(productVos);
		return vo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<DeliveryNoteVo> listDeliveryNotesForCompany(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		List<DeliveryNoteVo> list = new ArrayList<DeliveryNoteVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(DeliveryNote.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<DeliveryNote> notes = criteria.list();
		if (notes != null) {
			for (DeliveryNote note : notes) {
				list.add(this.createVo(note));
			}
		}
		return list;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<DeliveryNoteVo> listDeliveryNotesForCompanyWithinDateRange(Long companyId, String startDate,
			String endDate) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		List<DeliveryNoteVo> list = new ArrayList<DeliveryNoteVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(DeliveryNote.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.between("date", DateFormatter.convertStringToDate(startDate),
				DateFormatter.convertStringToDate(endDate)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<DeliveryNote> notes = criteria.list();
		if (notes != null) {
			for (DeliveryNote note : notes) {
				list.add(this.createVo(note));
			}
		}
		return list;
	}

	public DeliveryNote findDeliveryNoteObject(Long id) {
		return (DeliveryNote) this.sessionFactory.getCurrentSession().createCriteria(DeliveryNote.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public InvoiceVo convertDeliveryNotesToInvoice(List<Long> deliveryNoteIds) {
		InvoiceVo invoiceVo = new InvoiceVo();
		List<DeliveryNote> deliveryNotes = new ArrayList<DeliveryNote>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(DeliveryNote.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.in("id", deliveryNoteIds));
		deliveryNotes = criteria.list();
		DeliveryNoteVo noteVo = new DeliveryNoteVo();
		Boolean isFirst = true;
		List<DeliveryNoteProductVo> productVos = new ArrayList<DeliveryNoteProductVo>();
		Map<String, InvoiceChargeVo> chargeMap = new HashMap<String, InvoiceChargeVo>();
		for (DeliveryNote note : deliveryNotes) {
			invoiceVo.getDeliveryNoteIds().add(note.getId());
			SalesOrder order = note.getSalesOrder();
			if (order != null) {
				Charge charge = null;
				String chargeName = null;
				for (SalesOrderCharges charges : order.getCharges()) {
					charge = charges.getCharge();
					chargeName = charge.getName();
					InvoiceChargeVo chargeVo = chargeMap.get(charge);
					if (chargeVo == null)
						chargeVo = new InvoiceChargeVo();
					chargeVo.setAmount(chargeVo.getAmount().add(charges.getAmount()));
					chargeVo.setCharge(chargeName);
					chargeVo.setChargeId(charge.getId());
					chargeMap.put(chargeName, chargeVo);
				}
			}
			if (isFirst) {
				Customer customer = note.getCustomer();
				noteVo.setCustomer(customer.getName());
				noteVo.setCustomerId(customer.getId());
				if (order != null) {
					PaymentTerms paymentTerms = order.getPaymentTerm();
					ErPCurrency currency = order.getCurrency();
					noteVo.setPaymentTerm(paymentTerms.getTerm());
					noteVo.setPaymentTermId(paymentTerms.getId());
					DeliveryMethod deliveryMethod = order.getDeliveryTerm();
					if (deliveryMethod != null) {
						noteVo.setDeliveryTerm(deliveryMethod.getMethod());
						noteVo.setDeliveryTermId(deliveryMethod.getId());
					}
					noteVo.setCurrency(currency.getBaseCurrency().getBaseCurrency());
					noteVo.setErpCurrencyId(currency.getId());
					isFirst = false;
				}
			}
			if (order != null) {
				noteVo.setSubTotal(noteVo.getSubTotal().add(order.getSubTotal()));
				noteVo.setDiscountRate(noteVo.getDiscountRate().add(order.getDiscountPercentage()));
				noteVo.setDiscountTotal(noteVo.getDiscountTotal().add(order.getDiscountAmount()));
				noteVo.setNetTotal(noteVo.getNetTotal().add(order.getNetTotal()));
				noteVo.setGrandTotal(noteVo.getGrandTotal().add(order.getTotal()));
				for (VAT tax : order.getVat()) {
					noteVo.getTaxes().add(tax.getName());
					noteVo.getTsxIds().add(tax.getId());
					noteVo.getTaxesWithRate().add(tax.getId() + "!!!" + tax.getPercentage());
				}
			}
			for (DeliveryNoteItems items : note.getItems()) {
				DeliveryNoteProductVo productVo = new DeliveryNoteProductVo();
				Product product = items.getProduct();
				if (order != null) {
					SalesOrderItems orderItem = (SalesOrderItems) this.sessionFactory.getCurrentSession()
							.createCriteria(SalesOrderItems.class).add(Restrictions.eq("salesOrder", order))
							.add(Restrictions.eq("product", product)).uniqueResult();
					productVo.setUnitPrice(orderItem.getPrice());
					productVo.setDiscount(orderItem.getDicscountRate());
					productVo.setNetAmount(orderItem.getTotal());
					VAT tax = orderItem.getVat();
					if (tax != null)
						productVo.setTax(tax.getName());
				} else {
					productVo.setUnitPrice(BigDecimal.ZERO);
					productVo.setDiscount(BigDecimal.ZERO);
					productVo.setNetAmount(BigDecimal.ZERO);
				}
				productVo.setProductCode(product.getProductCode());
				productVo.setProductName(product.getName());
				productVo.setProductId(product.getId());
				productVo.setProductId(items.getId());
				productVo.setQuantity(items.getQuantity());
				Godown godown = items.getGodown();
				productVo.setGodown(godown.getName());
				productVo.setGodownId(godown.getId());
				productVos.add(productVo);
			}
		}
		for (Map.Entry<String, InvoiceChargeVo> entry : chargeMap.entrySet()) {
			invoiceVo.getChargeVos().add(entry.getValue());
		}
		noteVo.setProductVos(sortProductVos(productVos));
		invoiceVo = createInvoiceVo(invoiceVo, noteVo);
		return invoiceVo;
	}

	private List<DeliveryNoteProductVo> sortProductVos(List<DeliveryNoteProductVo> list) {
		Map<String, DeliveryNoteProductVo> map = new HashMap<String, DeliveryNoteProductVo>();
		List<DeliveryNoteProductVo> vos = new ArrayList<DeliveryNoteProductVo>();
		for (DeliveryNoteProductVo vo : list) {
			String key = vo.getGodown() + "!!!" + vo.getProductCode();
			DeliveryNoteProductVo newVo = map.get(key);
			if (newVo != null) {
				newVo.setQuantity(newVo.getQuantity().add(vo.getQuantity()));
				newVo.setNetAmount(newVo.getNetAmount().add(vo.getNetAmount()));
				map.put(key, newVo);
			} else {
				map.put(key, vo);
			}
		}
		for (Map.Entry<String, DeliveryNoteProductVo> entry : map.entrySet()) {
			vos.add(entry.getValue());
		}
		return vos;
	}

	private InvoiceVo createInvoiceVo(InvoiceVo vo, DeliveryNoteVo noteVo) {
		vo.setDeliveryNoteId(noteVo.getNoteId());
		vo.setCustomer(noteVo.getCustomer());
		vo.setCustomerId(noteVo.getCustomerId());
		vo.setInvoiceDate(DateFormatter.convertDateToString(new Date()));
		vo.setIsSettled(false);
		vo.setIsEdit(false);
		vo.setPaymentTerm(noteVo.getPaymentTerm());
		vo.setPaymentTermId(noteVo.getPaymentTermId());
		vo.setDeliveryTermId(noteVo.getDeliveryTermId());
		vo.setDeliveryTerm(noteVo.getDeliveryTerm());
		vo.setErpCurrencyId(noteVo.getErpCurrencyId());
		vo.setCurrecny(noteVo.getCurrency());
		vo.setSubTotal(noteVo.getSubTotal());
		vo.setDiscountRate(noteVo.getDiscountRate());
		vo.setDiscountTotal(noteVo.getDiscountTotal());
		vo.setNetTotal(noteVo.getNetTotal());
		vo.setTaxTotal(noteVo.getTaxTotal());
		vo.setGrandTotal(noteVo.getGrandTotal());
		vo.setTaxes(noteVo.getTaxes());
		vo.setTaxesWithRate(noteVo.getTaxesWithRate());
		vo = this.createInvoiceProductVo(vo, noteVo);
		return vo;
	}

	private InvoiceVo createInvoiceProductVo(InvoiceVo vo, DeliveryNoteVo noteVo) {
		List<InvoiceProductVo> productVos = new ArrayList<InvoiceProductVo>();
		BigDecimal taxAmount = new BigDecimal("0.00");
		for (DeliveryNoteProductVo items : noteVo.getProductVos()) {
			InvoiceProductVo productVo = new InvoiceProductVo();
			productVo.setAmountExcludingTax(items.getAmountExcludingTax());
			vo.setNetAmountHidden(vo.getNetAmountHidden().add(items.getAmountExcludingTax()));
			productVo.setProductCode(items.getProductCode());
			productVo.setProductName(items.getProductName());
			productVo.setProductId(items.getProductId());
			taxAmount = taxAmount.add(items.getNetAmount().subtract(items.getAmountExcludingTax()));
			productVo.setQuantity(items.getQuantity());
			productVo.setGodownId(items.getGodownId());
			productVo.setUnitPrice(items.getUnitPrice());
			productVo.setDiscount(items.getDiscount());
			productVo.setNetAmount(items.getNetAmount());
			productVo.setTax(items.getTax());
			productVos.add(productVo);
		}
		vo.setProductsTaxAmount(taxAmount);
		vo.setProductVos(productVos);
		return vo;
	}
}
