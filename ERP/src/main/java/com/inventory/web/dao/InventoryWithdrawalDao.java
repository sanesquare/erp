package com.inventory.web.dao;

import java.util.List;

import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.vo.InventoryEntryVo;
import com.inventory.web.vo.InventoryWithdrawalVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 27, 2015
 */
public interface InventoryWithdrawalDao {

	/**
	 * method to save inventory withdrawal
	 * 
	 * @param vo
	 * @return
	 */
	public Long saveInventoryWithdrawal(InventoryWithdrawalVo vo);

	/**
	 * method to set attributes to inventory withdrawal
	 * 
	 * @param entry
	 * @param vo
	 * @return
	 */
	public InventoryWithdrawal setAttributesToInventoryWithdrawal(InventoryWithdrawal withdrawal, InventoryWithdrawalVo vo,Boolean isDirect);

	/**
	 * method to delete inventory withdrawal
	 * 
	 * @param id
	 */
	public void deleteInventoryWithdrawal(Long id);

	/**
	 * method to list all inventory withdrawals
	 * 
	 * @param companyId
	 * @return
	 */
	public List<InventoryWithdrawalVo> listAllInventoryWithdrawals(Long companyId);

	/**
	 * method to find inventory withdrawal
	 * 
	 * @param id
	 * @return
	 */
	public InventoryWithdrawal findInventoryWithdrawal(Long id);

	public String getNumber(Long companyId, String category);

	/**
	 * method to find inventory withdrawal
	 * 
	 * @param id
	 * @return
	 */
	public InventoryWithdrawalVo findInventoryWithdrawalById(Long id);

	/**
	 * method to find all withdrawals
	 * 
	 * @param companyId
	 * @return
	 */
	public List<InventoryWithdrawalVo> findAllWithdrawals(Long companyId);

}
