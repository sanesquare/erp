package com.inventory.web.dao;

import java.util.List;

import com.inventory.web.entities.PackingList;
import com.inventory.web.vo.PackingListVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
public interface PackingListdao {

	/**
	 * method to save packing list
	 * @param vo
	 * @return
	 */
	public Long savePackingList(PackingListVo vo);
	
	/**
	 * method to delete packing list
	 * @param id
	 */
	public void deletePackingList(Long id);
	
	/**
	 * method to find packing lists
	 * @param companyId
	 * @return
	 */
	public List<PackingListVo> findAllPackingLists(Long companyId);
	
	/**
	 * method to find packing list
	 * @param id
	 * @return
	 */
	public PackingList findPackingList(Long id);
	
	/**
	 * method to find packing list
	 * @param id
	 * @return
	 */
	public PackingListVo findPackingListById(Long id);
	
	/**
	 * method to convert invoice to packing list
	 * @param invoiceId
	 * @return
	 */
	public PackingListVo convertInvoiceToPackingList(Long invoiceId);
	
}
