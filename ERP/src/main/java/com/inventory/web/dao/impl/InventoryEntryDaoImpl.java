package com.inventory.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.constants.ErpPageNameConstants;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.Godown;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryConstants;
import com.inventory.web.dao.GodownDao;
import com.inventory.web.dao.InventoryEntryDao;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.entities.InventoryEntryItems;
import com.inventory.web.entities.InventoryTransaction;
import com.inventory.web.entities.Product;
import com.inventory.web.vo.InventoryEntryItemsVo;
import com.inventory.web.vo.InventoryEntryVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 26, 2015
 */
@Repository
public class InventoryEntryDaoImpl extends AbstractDao implements InventoryEntryDao {

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private GodownDao godownDao;

	public Long saveInventoryEntry(InventoryEntryVo vo) {
		Long id = vo.getInventoryEntryId();
		InventoryEntry entry = this.findInventoryEntry(vo.getInventoryEntryId());
		Godown godown = godownDao.findGodownObject(vo.getGodownId());
		if (entry == null) {
			entry = new InventoryEntry();
			Company company = companyDao.findCompany(vo.getCompanyId());
			entry.setCode(this.getNumber(company.getId(), NumberPropertyConstants.INVENTORY_ENTRY));
			entry.setCompany(company);
			entry.setType(vo.getType());
			entry.setGodown(godown);
			entry = this.setAttributesToInventoryEntry(entry, vo, true);
			id = (Long) this.sessionFactory.getCurrentSession().save(entry);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.INVENTORY_ENTRY, company);
		} else {
			entry.setGodown(godown);
			entry = this.setAttributesToInventoryEntry(entry, vo, true);
			this.sessionFactory.getCurrentSession().merge(entry);
		}
		return id;
	}

	public InventoryEntry setAttributesToInventoryEntry(InventoryEntry entry, InventoryEntryVo vo, Boolean isDirect) {

		entry.getInventoryEntryItems().clear();

		entry.setDate(DateFormatter.convertStringToDate(vo.getDate()));
		entry.setDescription(vo.getNarration());

		// setting items
		for (InventoryEntryItemsVo itemsVo : vo.getItemsVos()) {
			InventoryEntryItems entryItems = new InventoryEntryItems();
			entryItems.setInventoryEntry(entry);
			InventoryTransaction inventoryTransaction = new InventoryTransaction();
			Product product = productDao.findProduct(itemsVo.getProductCode(), entry.getCompany().getId());
			inventoryTransaction.setProduct(product);
			inventoryTransaction.setCompany(entry.getCompany());
			inventoryTransaction.setGodown(entry.getGodown());
			inventoryTransaction.setQuantityPlus(itemsVo.getQuantity());
			if (isDirect) {
				inventoryTransaction.setUnitPrice(product.getPurchasePrice());
				inventoryTransaction.setTotalAmount(inventoryTransaction.getQuantityPlus().multiply(
						inventoryTransaction.getUnitPrice()));
			} else {
				inventoryTransaction.setUnitPrice(itemsVo.getUnitPrice());
				inventoryTransaction.setTotalAmount(itemsVo.getTotal());
			}
			inventoryTransaction.setDate(entry.getDate());
			inventoryTransaction.setEntryItems(entryItems);
			entryItems.setTransaction(inventoryTransaction);
			entry.getInventoryEntryItems().add(entryItems);
		}

		return entry;
	}

	public void deleteInventoryEntry(Long id) {
		InventoryEntry entry = this.findInventoryEntry(id);
		if (entry == null)
			throw new ItemNotFoundException("Entry Not Found");
		this.sessionFactory.getCurrentSession().delete(entry);
	}

	public List<InventoryEntryVo> listAllInventoryEntries(Long companyId) {
		// TODO Auto-generated method stub
		return null;
	}

	public InventoryEntry findInventoryEntry(Long id) {
		return (InventoryEntry) this.sessionFactory.getCurrentSession().createCriteria(InventoryEntry.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public String getNumber(Long companyId, String category) {
		String code = "";
		NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(category, companyId);
		code = numberPropertyVo.getPrefix() + numberPropertyVo.getNumber();
		return code;
	}

	public InventoryEntryVo findInventoryEntryById(Long id) {
		InventoryEntry entry = this.findInventoryEntry(id);
		if (entry == null)
			throw new ItemNotFoundException("Inventory entry not found");
		return createVo(entry);
	}

	private InventoryEntryVo createVo(InventoryEntry entry) {
		InventoryEntryVo vo = new InventoryEntryVo();
		vo.setInventoryEntryId(entry.getId());
		vo.setCode(entry.getCode());
		vo.setDate(DateFormatter.convertDateToString(entry.getDate()));
		vo.setNarration(entry.getDescription());
		Godown godown = entry.getGodown();
		vo.setGodown(godown.getName());
		if (entry.getType().equalsIgnoreCase(InventoryConstants.IE_MANUAL))
			vo.setEditable(true);
		else
			vo.setEditable(false);
		vo.setGodownId(godown.getId());
		vo.setType(entry.getType());
		for (InventoryEntryItems item : entry.getInventoryEntryItems()) {
			InventoryTransaction transaction = item.getTransaction();
			Product product = transaction.getProduct();
			InventoryEntryItemsVo itemsVo = new InventoryEntryItemsVo();
			itemsVo.setProductCode(product.getProductCode());
			itemsVo.setProductName(product.getName());
			itemsVo.setQuantity(transaction.getQuantityPlus());
			itemsVo.setUnitPrice(transaction.getUnitPrice());
			itemsVo.setTotal(transaction.getTotalAmount());
			vo.getItemsVos().add(itemsVo);
		}
		return vo;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<InventoryEntryVo> findAllEntries(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company not found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryEntry.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<InventoryEntry> entries = criteria.list();
		List<InventoryEntryVo> entryVos = new ArrayList<InventoryEntryVo>();
		for (InventoryEntry entry : entries)
			entryVos.add(createVo(entry));
		return entryVos;
	}
}
