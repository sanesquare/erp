package com.inventory.web.dao.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transaction;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.dao.CompanyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.Godown;
import com.erp.web.entities.StockGroup;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryConstants;
import com.inventory.web.dao.InventoryEntryDao;
import com.inventory.web.dao.InventoryTransactionDao;
import com.inventory.web.dao.InventoryWithdrawalDao;
import com.inventory.web.entities.DeliveryNoteItems;
import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.entities.InventoryEntryItems;
import com.inventory.web.entities.InventoryTransaction;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.InventoryWithdrawalItems;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ReceivingNoteItems;
import com.inventory.web.vo.ProductTransactionsVo;
import com.inventory.web.vo.StockMonthlyVo;
import com.inventory.web.vo.StockProductDetailedVo;
import com.inventory.web.vo.StockViewVo;
import com.purchase.web.dao.StockGroupDao;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.BillItems;
import com.purchase.web.entities.PurchaseReturn;
import com.purchase.web.entities.PurchaseReturnItems;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.InvoiceItems;
import com.sales.web.entities.SalesReturn;
import com.sales.web.entities.SalesReturnItems;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 5, 2015
 */
@Repository
public class InventoryTransactionDaoImpl extends AbstractDao implements InventoryTransactionDao {

	@Autowired
	private StockGroupDao stockGroupDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private InventoryEntryDao inventoryEntryDao;

	@Autowired
	private InventoryWithdrawalDao inventoryWithdrawalDao;

	public List<StockViewVo> findStockSummary(Long companyId, String start, String end) {
		List<StockViewVo> stockViewVos = new ArrayList<StockViewVo>();
		Date startDate = DateFormatter.convertStringToDate(start);
		Date endDate = DateFormatter.convertStringToDate(end);
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");

		// fetching stock groups
		List<StockGroup> stockGroups = stockGroupDao.findStockGroupWithNoParent(company);
		// iterate stock group
		for (StockGroup stockGroup : stockGroups) {
			StockViewVo vo = new StockViewVo();
			vo.setName(stockGroup.getGroupName());
			vo.setIsProduct(false);

			// fetching products from current stockGroup
			vo = this.setProducts(stockGroup, vo, startDate, endDate);

			// fetching child groups group from current stock group
			vo = this.setChildGroups(stockGroup, vo, startDate, endDate);
			stockViewVos.add(vo);
		}
		List<StockViewVo> finalList = new ArrayList<StockViewVo>();
		for (StockViewVo viewVo : stockViewVos) {
			if (!viewVo.getIsProduct()) {
				if (viewVo.getItems().size() > 0)
					finalList.add(viewVo);
			}
		}

		return finalList;
	}

	private StockViewVo setProducts(StockGroup stockGroup, StockViewVo vo, Date startDate, Date endDate) {
		Set<Product> products = stockGroup.getProducts();
		String sameUnit = null;
		Boolean isSameUnit = true;
		for (Product product : products) {
			List<String> months = DateFormatter.getMonthYearList(startDate, endDate);
			Map<String, StockMonthlyVo> monthlyVos = new LinkedHashMap<String, StockMonthlyVo>();
			for (String month : months) {
				StockMonthlyVo monthlyVo = new StockMonthlyVo();
				monthlyVo.setDate(month);
				monthlyVo.setName(product.getName());
				monthlyVos.put(month.trim(), monthlyVo);
			}
			String unit = product.getAdvanceDetails().getUnit().getSymbol();
			if (sameUnit == null)
				sameUnit = unit;
			else {
				if (!sameUnit.equalsIgnoreCase(unit))
					isSameUnit = false;
			}
			StockViewVo productVo = new StockViewVo();
			productVo.setName(product.getName());
			productVo.setIsProduct(true);
			productVo.setUnit(unit);

			List<InventoryTransaction> transactions = this.findTransactions(product, startDate, endDate);
			List<ProductTransactionsVo> productTransactionsVos = new ArrayList<ProductTransactionsVo>();
			BigDecimal openingBalance = new BigDecimal("0.00");
			BigDecimal openingRate = new BigDecimal("0.00");
			BigDecimal openingQuantity = new BigDecimal("0.00");
			for (InventoryTransaction transaction : transactions) {
				if (transaction.getDate().compareTo(startDate) >= 0) {
					Godown godown = transaction.getGodown();
					InventoryEntryItems entryItems = transaction.getEntryItems();
					InventoryWithdrawalItems withdrawalItems = transaction.getWithdrawalItems();
					BillItems billItems = null;
					SalesReturnItems salesReturnItems = null;
					InvoiceItems invoiceItems = null;
					Invoice invoiceObj = null;
					PurchaseReturnItems purchaseReturnItems = null;
					InventoryEntry entry = null;
					InventoryWithdrawal withdrawal = null;
					DeliveryNoteItems deliveryNoteItems = null;
					if (entryItems != null) {
						entry = entryItems.getInventoryEntry();
						billItems = entry.getBillItems();
						salesReturnItems = entryItems.getInventoryEntry().getSalesReturnItems();
					} else {
						withdrawal = withdrawalItems.getInventoryWithdrawal();
						deliveryNoteItems = withdrawal.getDeliveryNoteItems();
						if (deliveryNoteItems != null) {
							invoiceObj = deliveryNoteItems.getDeliveryNote().getInvoice();
						}
						invoiceItems = withdrawal.getInvoiceItems();
						purchaseReturnItems = withdrawalItems.getInventoryWithdrawal().getPurchaseReturnItems();
					}

					ProductTransactionsVo productTransactionsVo = new ProductTransactionsVo();
					productTransactionsVo.setGodown(godown);
					productTransactionsVo.setDate(DateFormatter.convertDateToString(transaction.getDate()));
					if (billItems != null) {
						Bill bill = billItems.getBill();
						productTransactionsVo.setVoucher("Purchase");
						productTransactionsVo.setQuantity(transaction.getQuantityPlus());
						productTransactionsVo.setVoucherId(bill.getId());
						productTransactionsVo.setVoucherNumber(bill.getVoucherNumber());
						productTransactionsVo.setLedger(bill.getLedger().getLedgerName());
					} else if (salesReturnItems != null) {
						SalesReturn salesReturn = salesReturnItems.getSalesReturn();
						Invoice invoice = salesReturn.getInvoice();
						productTransactionsVo.setQuantity(transaction.getQuantityPlus());
						productTransactionsVo.setVoucherId(salesReturn.getId());
						productTransactionsVo.setVoucher("Sales Return");
						productTransactionsVo.setVoucherNumber(salesReturn.getReferenceNumber());
						productTransactionsVo.setLedger(invoice.getLedger().getLedgerName());
					} else if (invoiceItems != null) {
						Invoice invoice = invoiceItems.getInvoice();
						productTransactionsVo.setQuantity(transaction.getQuantityMinus());
						productTransactionsVo.setVoucher("Sales");
						productTransactionsVo.setVoucherId(invoice.getId());
						productTransactionsVo.setVoucherNumber(invoice.getReferenceNumber());
						productTransactionsVo.setLedger(invoice.getLedger().getLedgerName());
					} else if (invoiceObj != null) {
						productTransactionsVo.setQuantity(transaction.getQuantityMinus());
						productTransactionsVo.setVoucher("Sales");
						productTransactionsVo.setVoucherId(invoiceObj.getId());
						productTransactionsVo.setVoucherNumber(invoiceObj.getReferenceNumber());
						productTransactionsVo.setLedger(invoiceObj.getLedger().getLedgerName());
					} else if (purchaseReturnItems != null) {
						PurchaseReturn purchaseReturn = purchaseReturnItems.getPurchaseReturn();
						productTransactionsVo.setQuantity(transaction.getQuantityMinus());
						Bill bill = purchaseReturn.getBill();
						productTransactionsVo.setVoucherId(purchaseReturn.getId());
						productTransactionsVo.setVoucher("Purchase Return");
						productTransactionsVo.setVoucherNumber(purchaseReturn.getReferenceNumber());
						productTransactionsVo.setLedger(bill.getLedger().getLedgerName());
					} else if (entry != null) {
						productTransactionsVo.setQuantity(transaction.getQuantityPlus());
						productTransactionsVo.setVoucher("Inventory Entry");
						productTransactionsVo.setVoucherId(entry.getId());
						productTransactionsVo.setVoucherNumber(entry.getCode());
						productTransactionsVo.setLedger("--");
					} else if (withdrawal != null) {
						productTransactionsVo.setQuantity(transaction.getQuantityMinus());
						productTransactionsVo.setVoucher("Inventory Withdrawal");
						productTransactionsVo.setVoucherId(withdrawal.getId());
						productTransactionsVo.setVoucherNumber(withdrawal.getCode());
						productTransactionsVo.setLedger("--");
					}
					productTransactionsVo.setAmount(transaction.getTotalAmount());
					productTransactionsVo.setUnitPrice(transaction.getUnitPrice());
					productTransactionsVos.add(productTransactionsVo);
				} else {
					openingBalance = openingBalance.add(transaction.getTotalAmount());
					if (transaction.getQuantityPlus() != null)
						openingQuantity = openingQuantity.add(transaction.getQuantityPlus());
					if (transaction.getQuantityMinus() != null)
						openingQuantity = openingQuantity.subtract(transaction.getQuantityMinus());
				}
				StockMonthlyVo monthlyVo = monthlyVos.get(months.get(0));
				monthlyVos.put(months.get(0), monthlyVo);
			}

			if (openingQuantity.compareTo(BigDecimal.ZERO) < 0)
				openingBalance = BigDecimal.ZERO.subtract(openingBalance);
			if (openingQuantity.compareTo(BigDecimal.ZERO) != 0)
				openingRate = openingBalance.abs().divide(openingQuantity.abs(), 2, RoundingMode.HALF_EVEN);

			BigDecimal inwardsQuantity = new BigDecimal("0.00");
			BigDecimal inwardsRate = new BigDecimal("0.00");
			BigDecimal inwardsValue = new BigDecimal("0.00");
			BigDecimal outwardsQuantity = new BigDecimal("0.00");
			BigDecimal outwardsRate = new BigDecimal("0.00");
			BigDecimal outwardsValue = new BigDecimal("0.00");
			BigDecimal closingQuantity = new BigDecimal("0.00");
			BigDecimal closingRate = new BigDecimal("0.00");
			BigDecimal closingValue = new BigDecimal("0.00");

			List<StockProductDetailedVo> productDetailedVos = new ArrayList<StockProductDetailedVo>();

			for (ProductTransactionsVo productTransactionsVo : productTransactionsVos) {

				Godown godown = productTransactionsVo.getGodown();

				StockProductDetailedVo detailedVo = new StockProductDetailedVo();
				detailedVo.setVoucher(productTransactionsVo.getVoucher());
				detailedVo.setVoucherId(productTransactionsVo.getVoucherId());
				detailedVo.setDate(productTransactionsVo.getDate());
				detailedVo.setLedger(productTransactionsVo.getLedger());
				detailedVo.setVoucherNumber(productTransactionsVo.getVoucherNumber());
				detailedVo.setUnit(unit);

				if (godown.getName().equalsIgnoreCase(InventoryConstants.TO_BE_ISSUED_GODOWN))
					detailedVo.setToBeIssuedQuantity(productTransactionsVo.getQuantity());

				if (productTransactionsVos.indexOf(productTransactionsVo) == 0) {
					if (productTransactionsVo.getVoucher().equalsIgnoreCase(InventoryConstants.PURCHASE)) {
						detailedVo.setInwardsQuantity(productTransactionsVo.getQuantity());
						detailedVo.setInwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setInwardsValue(productTransactionsVo.getAmount());

						inwardsQuantity = inwardsQuantity.add(detailedVo.getInwardsQuantity());
						inwardsValue = inwardsValue.add(detailedVo.getInwardsValue());
						inwardsRate = inwardsValue.divide(inwardsQuantity);

						detailedVo.setClosingQuantity(productTransactionsVo.getQuantity());
						detailedVo.setClosingRate(productTransactionsVo.getUnitPrice());
						detailedVo.setClosingValue(productTransactionsVo.getAmount());

					} else if (productTransactionsVo.getVoucher().equalsIgnoreCase(InventoryConstants.SALES)) {
						detailedVo.setOutwardsQuantity(productTransactionsVo.getQuantity());
						detailedVo.setOutwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setOutwardsValue(productTransactionsVo.getAmount());

						outwardsQuantity = outwardsQuantity.add(detailedVo.getOutwardsQuantity());
						outwardsValue = outwardsValue.add(detailedVo.getOutwardsValue());
						outwardsRate = outwardsValue.divide(outwardsQuantity);

						detailedVo.setClosingQuantity(detailedVo.getClosingQuantity().subtract(
								productTransactionsVo.getQuantity()));
						detailedVo.setClosingRate(productTransactionsVo.getUnitPrice());
						detailedVo.setClosingValue(detailedVo.getClosingQuantity()
								.multiply(detailedVo.getClosingRate()).setScale(2, RoundingMode.HALF_EVEN));

					} else if (productTransactionsVo.getVoucher().equalsIgnoreCase(InventoryConstants.SALES_RETURN)) {

						detailedVo.setOutwardsQuantity(detailedVo.getOutwardsQuantity().subtract(
								productTransactionsVo.getQuantity()));
						detailedVo.setOutwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setOutwardsValue(detailedVo.getOutwardsQuantity()
								.multiply(detailedVo.getOutwardsRate()).setScale(2, RoundingMode.HALF_EVEN));

						outwardsQuantity = outwardsQuantity.subtract(detailedVo.getOutwardsQuantity());
						outwardsValue = outwardsValue.add(detailedVo.getOutwardsValue());
						outwardsRate = outwardsValue.divide(outwardsQuantity);

						detailedVo.setClosingQuantity(detailedVo.getOutwardsQuantity());
						detailedVo.setClosingRate(productTransactionsVo.getUnitPrice());
						detailedVo.setClosingValue(detailedVo.getOutwardsValue());

					} else if (productTransactionsVo.getVoucher().equalsIgnoreCase(InventoryConstants.PURCHASE_RETURN)) {

						detailedVo.setInwardsQuantity(detailedVo.getInwardsQuantity().subtract(
								productTransactionsVo.getQuantity()));
						detailedVo.setInwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setInwardsValue(detailedVo.getInwardsQuantity()
								.multiply(detailedVo.getInwardsRate()).setScale(2, RoundingMode.HALF_EVEN));

						inwardsQuantity = inwardsQuantity.subtract(detailedVo.getInwardsQuantity());
						inwardsValue = inwardsValue.add(detailedVo.getInwardsValue());
						inwardsRate = inwardsValue.divide(inwardsQuantity);

						detailedVo.setClosingQuantity(detailedVo.getInwardsQuantity());
						detailedVo.setClosingRate(productTransactionsVo.getUnitPrice());
						detailedVo.setClosingValue(detailedVo.getInwardsValue());
					} else if (productTransactionsVo.getVoucher().equalsIgnoreCase(
							InventoryConstants.INVENTORY_WITHDRAWAL)) {

						detailedVo.setOutwardsQuantity(productTransactionsVo.getQuantity());
						detailedVo.setOutwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setOutwardsValue(productTransactionsVo.getAmount());

						outwardsQuantity = outwardsQuantity.add(detailedVo.getOutwardsQuantity());
						outwardsValue = outwardsValue.add(detailedVo.getOutwardsValue());
						outwardsRate = outwardsValue.divide(outwardsQuantity);

						detailedVo.setClosingQuantity(detailedVo.getClosingQuantity().subtract(
								productTransactionsVo.getQuantity()));
						detailedVo.setClosingRate(productTransactionsVo.getUnitPrice());
						detailedVo.setClosingValue(detailedVo.getClosingQuantity()
								.multiply(detailedVo.getClosingRate()).setScale(2, RoundingMode.HALF_EVEN));

					} else if (productTransactionsVo.getVoucher().equalsIgnoreCase(InventoryConstants.INVENTORY_ENTRY)) {
						detailedVo.setInwardsQuantity(productTransactionsVo.getQuantity());
						detailedVo.setInwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setInwardsValue(productTransactionsVo.getAmount());

						inwardsQuantity = inwardsQuantity.add(detailedVo.getInwardsQuantity());
						inwardsValue = inwardsValue.add(detailedVo.getInwardsValue());
						inwardsRate = inwardsValue.divide(inwardsQuantity);

						detailedVo.setClosingQuantity(productTransactionsVo.getQuantity());
						detailedVo.setClosingRate(productTransactionsVo.getUnitPrice());
						detailedVo.setClosingValue(productTransactionsVo.getAmount());
					}

					closingQuantity = detailedVo.getClosingQuantity();
					closingRate = detailedVo.getClosingValue().divide(closingQuantity, 2, RoundingMode.HALF_EVEN);
					closingValue = detailedVo.getClosingValue();
				} else {
					StockProductDetailedVo oldVo = productDetailedVos.get(productTransactionsVos
							.indexOf(productTransactionsVo) - 1);
					if (productTransactionsVo.getVoucher().equalsIgnoreCase(InventoryConstants.PURCHASE)) {

						detailedVo.setInwardsQuantity(productTransactionsVo.getQuantity());
						detailedVo.setInwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setInwardsValue(productTransactionsVo.getAmount());

						detailedVo.setClosingQuantity(oldVo.getClosingQuantity().add(detailedVo.getInwardsQuantity()));
						detailedVo.setClosingValue(oldVo.getClosingValue().add(detailedVo.getInwardsValue()));
						detailedVo.setClosingRate(detailedVo.getClosingValue().divide(detailedVo.getClosingQuantity(),
								2, RoundingMode.HALF_EVEN));

						inwardsQuantity = inwardsQuantity.add(detailedVo.getInwardsQuantity());
						inwardsValue = inwardsValue.add(detailedVo.getInwardsValue());
						inwardsRate = inwardsValue.divide(inwardsQuantity, 2, RoundingMode.HALF_EVEN);

					} else if (productTransactionsVo.getVoucher().equalsIgnoreCase(InventoryConstants.SALES)) {

						detailedVo.setOutwardsQuantity(productTransactionsVo.getQuantity());
						detailedVo.setOutwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setOutwardsValue(productTransactionsVo.getAmount());
						detailedVo.setClosingQuantity(oldVo.getClosingQuantity().subtract(
								detailedVo.getOutwardsQuantity()));
						detailedVo.setClosingRate(oldVo.getClosingRate());
						detailedVo.setClosingValue(detailedVo.getClosingQuantity()
								.multiply(detailedVo.getClosingRate()).setScale(2, RoundingMode.HALF_EVEN));

						outwardsQuantity = outwardsQuantity.add(detailedVo.getOutwardsQuantity());
						outwardsValue = outwardsValue.add(detailedVo.getOutwardsValue());
						outwardsRate = outwardsValue.divide(outwardsQuantity, 2, RoundingMode.HALF_EVEN);

					} else if (productTransactionsVo.getVoucher().equalsIgnoreCase(InventoryConstants.SALES_RETURN)) {

						detailedVo.setOutwardsQuantity(detailedVo.getOutwardsQuantity().subtract(
								productTransactionsVo.getQuantity()));
						detailedVo.setOutwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setOutwardsValue(detailedVo.getOutwardsQuantity()
								.multiply(detailedVo.getOutwardsRate()).setScale(2, RoundingMode.HALF_EVEN));
						detailedVo.setClosingQuantity(oldVo.getClosingQuantity().add(detailedVo.getOutwardsQuantity()));
						detailedVo.setClosingRate(oldVo.getClosingRate());
						detailedVo.setClosingValue(detailedVo.getClosingQuantity()
								.multiply(detailedVo.getClosingRate()).setScale(2, RoundingMode.HALF_EVEN));

						outwardsQuantity = outwardsQuantity.add(detailedVo.getOutwardsQuantity());
						outwardsValue = outwardsValue.add(detailedVo.getOutwardsValue());
						outwardsRate = outwardsValue.divide(outwardsQuantity, 2, RoundingMode.HALF_EVEN);

					} else if (productTransactionsVo.getVoucher().equalsIgnoreCase(InventoryConstants.PURCHASE_RETURN)) {

						detailedVo.setInwardsQuantity(detailedVo.getInwardsQuantity().subtract(
								productTransactionsVo.getQuantity()));
						detailedVo.setInwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setInwardsValue(detailedVo.getInwardsQuantity()
								.multiply(detailedVo.getInwardsRate()).setScale(2, RoundingMode.HALF_EVEN));

						detailedVo.setClosingQuantity(oldVo.getClosingQuantity().add(detailedVo.getInwardsQuantity()));
						detailedVo.setClosingValue(oldVo.getClosingValue().add(detailedVo.getInwardsValue()));
						detailedVo.setClosingRate(detailedVo.getClosingValue().divide(detailedVo.getClosingQuantity(),
								2, RoundingMode.HALF_EVEN));

						inwardsQuantity = inwardsQuantity.add(detailedVo.getInwardsQuantity());
						inwardsValue = inwardsValue.add(detailedVo.getInwardsValue());
						inwardsRate = inwardsValue.divide(inwardsQuantity, 2, RoundingMode.HALF_EVEN);

					} else if (productTransactionsVo.getVoucher().equalsIgnoreCase(
							InventoryConstants.INVENTORY_WITHDRAWAL)) {
						detailedVo.setOutwardsQuantity(productTransactionsVo.getQuantity());
						detailedVo.setOutwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setOutwardsValue(productTransactionsVo.getAmount());
						detailedVo.setClosingQuantity(oldVo.getClosingQuantity().subtract(
								detailedVo.getOutwardsQuantity()));
						detailedVo.setClosingRate(oldVo.getClosingRate());
						detailedVo.setClosingValue(detailedVo.getClosingQuantity()
								.multiply(detailedVo.getClosingRate()).setScale(2, RoundingMode.HALF_EVEN));

						outwardsQuantity = outwardsQuantity.add(detailedVo.getOutwardsQuantity());
						outwardsValue = outwardsValue.add(detailedVo.getOutwardsValue());
						outwardsRate = outwardsValue.divide(outwardsQuantity, 2, RoundingMode.HALF_EVEN);
					} else if (productTransactionsVo.getVoucher().equalsIgnoreCase(InventoryConstants.INVENTORY_ENTRY)) {
						detailedVo.setInwardsQuantity(productTransactionsVo.getQuantity());
						detailedVo.setInwardsRate(productTransactionsVo.getUnitPrice());
						detailedVo.setInwardsValue(productTransactionsVo.getAmount());

						detailedVo.setClosingQuantity(oldVo.getClosingQuantity().add(detailedVo.getInwardsQuantity()));
						detailedVo.setClosingValue(oldVo.getClosingValue().add(detailedVo.getInwardsValue()));
						detailedVo.setClosingRate(detailedVo.getClosingValue().divide(detailedVo.getClosingQuantity(),
								2, RoundingMode.HALF_EVEN));

						inwardsQuantity = inwardsQuantity.add(detailedVo.getInwardsQuantity());
						inwardsValue = inwardsValue.add(detailedVo.getInwardsValue());
						inwardsRate = inwardsValue.divide(inwardsQuantity, 2, RoundingMode.HALF_EVEN);
					}

				}
				productDetailedVos.add(detailedVo);
			}

			List<StockProductDetailedVo> newVos = new ArrayList<StockProductDetailedVo>();
			Map<Long, StockProductDetailedVo> map = new HashMap<Long, StockProductDetailedVo>();
			for (StockProductDetailedVo detailedVo : productDetailedVos) {
				if (detailedVo.getVoucher().equalsIgnoreCase("Sales")) {
					StockProductDetailedVo oldVo = map.get(detailedVo.getVoucherId());
					if (oldVo == null) {
						oldVo = detailedVo;
						newVos.add(detailedVo);
						map.put(detailedVo.getVoucherId(), oldVo);
					} else {
						StockProductDetailedVo previousVo = productDetailedVos
								.get(productDetailedVos.indexOf(oldVo) - 1);
						oldVo.setOutwardsQuantity(oldVo.getOutwardsQuantity().add(detailedVo.getOutwardsQuantity()));
						oldVo.setOutwardsValue(oldVo.getOutwardsValue().add(detailedVo.getOutwardsValue()));
						oldVo.setClosingQuantity(previousVo.getClosingQuantity().subtract(oldVo.getOutwardsQuantity()));
						oldVo.setClosingValue(oldVo.getClosingQuantity().multiply(oldVo.getClosingRate())
								.setScale(2, RoundingMode.HALF_EVEN));
						map.put(detailedVo.getVoucherId(), oldVo);
					}
				} else {
					newVos.add(detailedVo);
				}
			}

			List<StockProductDetailedVo> finalVos = new ArrayList<StockProductDetailedVo>();
			for (StockProductDetailedVo detailedVo : newVos) {
				if (detailedVo.getVoucher().equalsIgnoreCase("Sales")) {
					StockProductDetailedVo oldVo = map.get(detailedVo.getVoucherId());
					detailedVo.setClosingQuantity(oldVo.getClosingQuantity());
					detailedVo.setClosingRate(oldVo.getClosingRate());
					detailedVo.setClosingValue(oldVo.getClosingValue());
					detailedVo.setToBeIssuedQuantity(oldVo.getToBeIssuedQuantity());
					detailedVo.setOutwardsQuantity(oldVo.getOutwardsQuantity());
					detailedVo.setOutwardsRate(oldVo.getOutwardsRate());
					detailedVo.setOutwardsValue(oldVo.getOutwardsValue());
				}
				finalVos.add(detailedVo);
			}

			List<StockProductDetailedVo> originalFinalVos = new ArrayList<StockProductDetailedVo>();
			for (StockProductDetailedVo detailedVo : finalVos) {
				int index = finalVos.indexOf(detailedVo) - 1;
				if (index >= 0) {
					StockProductDetailedVo oldVo = finalVos.get(index);
					if (detailedVo.getOutwardsQuantity().compareTo(BigDecimal.ZERO) == 0) {
						detailedVo.setClosingQuantity(oldVo.getClosingQuantity().add(detailedVo.getInwardsQuantity()));
						detailedVo.setClosingValue(detailedVo.getInwardsValue().add(oldVo.getClosingValue()));
						detailedVo.setClosingRate(detailedVo.getClosingValue().divide(detailedVo.getClosingQuantity(),
								2, RoundingMode.HALF_EVEN));
					} else {
						detailedVo.setClosingQuantity(oldVo.getClosingQuantity().subtract(
								detailedVo.getOutwardsQuantity()));
						detailedVo.setClosingRate(oldVo.getClosingRate());
						detailedVo.setClosingValue(detailedVo.getClosingQuantity()
								.multiply(detailedVo.getClosingRate()).setScale(2, RoundingMode.HALF_EVEN));
					}
				}
				originalFinalVos.add(detailedVo);
			}
			for (StockProductDetailedVo detailedVo : originalFinalVos) {
				String date = DateFormatter.getMonthYesr(DateFormatter.convertStringToDate(detailedVo.getDate()));
				StockMonthlyVo monthlyVo = monthlyVos.get(date.trim());
				if (monthlyVo != null) {
					monthlyVo.getProductDetailedVos().add(detailedVo);
					monthlyVos.put(date, monthlyVo);
				}
			}

			StockMonthlyVo openingBalanceVo = new StockMonthlyVo();
			openingBalanceVo.setDate("Opening Balance");
			openingBalanceVo.setClosingQuantity(openingQuantity);
			openingBalanceVo.setUnit(unit);
			openingBalanceVo.setClosingValue(openingBalance);
			productVo.getMonthlyVos().add(openingBalanceVo);
			for (Map.Entry<String, StockMonthlyVo> entry : monthlyVos.entrySet()) {
				StockMonthlyVo monthlyVo = entry.getValue();
				productVo.getMonthlyVos().add(monthlyVo);
			}

			if (!productDetailedVos.isEmpty()) {
				StockProductDetailedVo lastVo = originalFinalVos.get(originalFinalVos.size() - 1);
				closingQuantity = lastVo.getClosingQuantity();
				closingValue = lastVo.getClosingValue();
				closingRate = closingValue.divide(closingQuantity, 2, RoundingMode.HALF_EVEN);
			} else {
				closingQuantity = openingQuantity;
				closingValue = openingBalance;
				closingRate = openingRate;
			}
			productVo.setClosingQuantity(closingQuantity);
			productVo.setClosingRate(closingRate);
			productVo.setClosingValue(closingValue);
			productVo.setInwardsQuantity(inwardsQuantity);
			productVo.setInwardsRate(inwardsRate.setScale(2));
			productVo.setInwardsValue(inwardsValue);
			productVo.setOutwardsQuantity(outwardsQuantity);
			productVo.setOutwardsRate(outwardsRate);
			productVo.setOutwardsValue(outwardsValue);
			productVo.setUnit(unit);
			int i = 1;
			for (StockMonthlyVo monthlyVo : productVo.getMonthlyVos()) {
				if (!monthlyVo.equals(openingBalanceVo)) {
					if (i <= 12) {
						String[] dateSplit = monthlyVo.getDate().split(" ");
						monthlyVo.setDate(dateSplit[0]);
					}
					if (i == 1) {
						monthlyVo.setOpeningQuantity(openingQuantity);
						monthlyVo.setOpeningValue(openingBalance);
						monthlyVo.setOpeningRate(openingRate);
						monthlyVo.setUnit(unit);
					} else {
						StockMonthlyVo oldMonthlyVo = productVo.getMonthlyVos().get(
								productVo.getMonthlyVos().indexOf(monthlyVo) - 1);
						monthlyVo.setOpeningQuantity(oldMonthlyVo.getClosingQuantity());
						monthlyVo.setOpeningValue(oldMonthlyVo.getClosingValue());
						if (monthlyVo.getOpeningQuantity().compareTo(BigDecimal.ZERO) != 0)
							monthlyVo.setOpeningRate(monthlyVo.getOpeningValue().abs()
									.divide(monthlyVo.getOpeningQuantity().abs(), 2, RoundingMode.HALF_EVEN));
					}
					inwardsQuantity = new BigDecimal("0.00");
					inwardsRate = new BigDecimal("0.00");
					inwardsValue = new BigDecimal("0.00");
					outwardsQuantity = new BigDecimal("0.00");
					outwardsRate = new BigDecimal("0.00");
					outwardsValue = new BigDecimal("0.00");
					closingQuantity = new BigDecimal("0.00");
					closingRate = new BigDecimal("0.00");
					closingValue = new BigDecimal("0.00");
					for (StockProductDetailedVo detailedVo : monthlyVo.getProductDetailedVos()) {
						inwardsQuantity = inwardsQuantity.add(detailedVo.getInwardsQuantity());
						inwardsValue = inwardsValue.add(detailedVo.getInwardsValue());
						outwardsQuantity = outwardsQuantity.add(detailedVo.getOutwardsQuantity());
						outwardsValue = outwardsValue.add(detailedVo.getOutwardsValue());
					}
					if (inwardsQuantity.compareTo(BigDecimal.ZERO) != 0)
						inwardsRate = inwardsValue.divide(inwardsQuantity, 2, RoundingMode.HALF_EVEN);
					if (outwardsQuantity.compareTo(BigDecimal.ZERO) != 0)
						outwardsRate = outwardsValue.divide(outwardsQuantity, 2, RoundingMode.HALF_EVEN);
					int index = monthlyVo.getProductDetailedVos().size() - 1;
					if (index >= 0) {
						StockProductDetailedVo lastVo = monthlyVo.getProductDetailedVos().get(index);
						closingQuantity = lastVo.getClosingQuantity();
						closingRate = lastVo.getClosingRate();
						closingValue = lastVo.getClosingValue();
					} else {
						closingQuantity = monthlyVo.getOpeningQuantity();
						closingValue = monthlyVo.getOpeningValue();
						closingRate = monthlyVo.getOpeningRate();
					}
					monthlyVo.setUnit(unit);
					monthlyVo.setInwardsQuantity(inwardsQuantity);
					monthlyVo.setInwardsRate(inwardsRate);
					monthlyVo.setInwardsValue(inwardsValue);
					monthlyVo.setOutwardsQuantity(outwardsQuantity);
					monthlyVo.setOutwardsRate(outwardsRate);
					monthlyVo.setOutwardsValue(outwardsValue);
					monthlyVo.setClosingQuantity(closingQuantity);
					monthlyVo.setClosingRate(closingRate);
					monthlyVo.setClosingValue(closingValue);
					i++;
				}
			}
			vo.getItems().add(productVo);
			vo.setTotalAmount(vo.getTotalAmount().add(productVo.getClosingValue()));
			vo.setTotalQuantity(vo.getTotalQuantity().add(productVo.getClosingQuantity()));
		}
		vo.setUnit(sameUnit);
		vo.setIsSameUnit(isSameUnit);
		return vo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	private List<InventoryTransaction> findTransactions(Product product, Date startDate, Date endDate) {
		List<InventoryTransaction> inventoryTransactions = new ArrayList<InventoryTransaction>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryTransaction.class);
		criteria.add(Restrictions.eq("product", product));
		criteria.add(Restrictions.le("date", endDate))/*
													 * .add(Restrictions.ge("date"
													 * , startDate))
													 */;
		criteria.addOrder(Order.asc("date"));
		criteria.addOrder(Order.asc("id"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		inventoryTransactions = criteria.list();
		return inventoryTransactions;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	private List<InventoryTransaction> findInwardTransactions(Product product, Date startDate, Date endDate) {
		List<InventoryTransaction> inventoryTransactions = new ArrayList<InventoryTransaction>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryTransaction.class);
		criteria.add(Restrictions.eq("product", product));
		criteria.add(Restrictions.le("date", endDate)).add(Restrictions.ge("date", startDate));
		criteria.addOrder(Order.asc("date"));
		criteria.add(Restrictions.isNull("withdrawalItems"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		inventoryTransactions = criteria.list();
		return inventoryTransactions;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	private List<InventoryTransaction> findOutWardTransactions(Product product, Date startDate, Date endDate) {
		List<InventoryTransaction> inventoryTransactions = new ArrayList<InventoryTransaction>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryTransaction.class);
		criteria.add(Restrictions.eq("product", product));
		criteria.add(Restrictions.le("date", endDate)).add(Restrictions.ge("date", startDate));
		criteria.addOrder(Order.asc("date"));
		criteria.add(Restrictions.isNull("entryItems"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		inventoryTransactions = criteria.list();
		return inventoryTransactions;
	}

	private StockViewVo setChildGroups(StockGroup stockGroup, StockViewVo vo, Date startDate, Date endDate) {
		Set<StockGroup> childGroups = stockGroup.getSubGroups();
		for (StockGroup childGroup : childGroups) {
			StockViewVo childGroupVo = new StockViewVo();
			childGroupVo.setName(childGroup.getGroupName());
			childGroupVo.setIsProduct(false);

			// fetching products
			childGroupVo = this.setProducts(childGroup, childGroupVo, startDate, endDate);

			// setting child groups
			childGroupVo = this.setChildGroups(childGroup, childGroupVo, startDate, endDate);
			vo.getItems().add(childGroupVo);
			vo.setTotalAmount(vo.getTotalAmount().add(childGroupVo.getTotalAmount()));
			vo.setTotalQuantity(vo.getTotalQuantity().add(childGroupVo.getTotalQuantity()));
			vo.setIsSameUnit(childGroupVo.getIsSameUnit());
			vo.setUnit(childGroupVo.getUnit());
		}
		return vo;
	}

	private void findOpeningBalance(Product product, Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -1);
		date = calendar.getTime();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryTransaction.class);
		criteria.add(Restrictions.eq("product", product));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.le("date", date));
		List<InventoryTransaction> inventoryTransactions = criteria.list();
		BigDecimal quantity = new BigDecimal("0.00");
		BigDecimal amount = new BigDecimal("0.00");
		for (InventoryTransaction transaction : inventoryTransactions) {

		}
	}
}
