package com.inventory.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.dao.CompanyDao;
import com.erp.web.entities.Company;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.inventory.web.dao.PackingKindDao;
import com.inventory.web.entities.PackingKind;
import com.inventory.web.vo.PackingKindVo;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 13, 2015
 */
@Repository
public class PackingKindDaoImpl extends AbstractDao implements PackingKindDao {

	@Autowired
	private CompanyDao companyDao;

	public void savePackingKing(PackingKindVo vo) {
		vo.setName(vo.getName().trim());
		Company company = companyDao.findCompany(vo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		PackingKind packingKind = this.findObject(vo.getId());
		PackingKind packingKindWithSameName = this.findObject(vo.getName(), company);
		if (packingKind == null) {
			packingKind = new PackingKind();
			if (packingKindWithSameName != null)
				throw new DuplicateItemException("Packing Kind " + vo.getName() + " Already Exists.");
		} else {
			if (packingKindWithSameName != null && !packingKind.equals(packingKindWithSameName))
				throw new DuplicateItemException("Packing Kind " + vo.getName() + " Already Exists.");
		}
		packingKind.setName(vo.getName());
		packingKind.setWeight(vo.getWeight());
		packingKind.setCompany(company);
		company.getPackingKinds().add(packingKind);
		this.sessionFactory.getCurrentSession().merge(company);
	}

	public void deletePackingKing(Long id) {
		PackingKind packingKind = this.findObject(id);
		if (packingKind == null)
			throw new ItemNotFoundException("Packing Kind Not Found.");
		this.sessionFactory.getCurrentSession().delete(packingKind);
	}

	public PackingKind findObject(Long id) {
		return (PackingKind) this.sessionFactory.getCurrentSession().createCriteria(PackingKind.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public PackingKind findObject(String name, Company company) {
		return (PackingKind) this.sessionFactory.getCurrentSession().createCriteria(PackingKind.class)
				.add(Restrictions.eq("company", company)).add(Restrictions.eq("name", name)).uniqueResult();
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PackingKindVo> findAllPackingKinds(Long companyId) {
		List<PackingKindVo> vos = new ArrayList<PackingKindVo>();
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PackingKind.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("company", company));
		List<PackingKind> packingKinds = criteria.list();
		for (PackingKind packingKind : packingKinds)
			vos.add(createVo(packingKind));
		return vos;
	}

	private PackingKindVo createVo(PackingKind packingKind) {
		PackingKindVo vo = new PackingKindVo();
		vo.setId(packingKind.getId());
		vo.setName(packingKind.getName());
		vo.setWeight(packingKind.getWeight());
		return vo;
	}

	public PackingKindVo findPackingKind(Long id) {
		PackingKind packingKind = this.findObject(id);
		if (packingKind == null)
			throw new ItemNotFoundException("Packing Kind Not Found.");
		return createVo(packingKind);
	}

}
