package com.inventory.web.dao;

import java.util.List;

import com.inventory.web.entities.Product;
import com.inventory.web.vo.ProductVo;

/**
 * @author Shamsheer & Vinutha
 * @since Sep 23, 2015
 */
public interface ProductDao {

	/**
	 * method to save or update product
	 * @param productVo
	 * @return 
	 */
	public Long saveOrUpdateProduct(ProductVo productVo);
	
	/**
	 * method to delete product
	 * @param id
	 */
	public void deleteProduct(Long id);
	
	/**
	 * method to delete product
	 * @param code
	 */
	public void deleteProduct(String code,Long companyId);
	
	/**
	 * method to find product
	 * @param id
	 * @return
	 */
	public Product findProduct(Long id);
	
	/**
	 * method to find product
	 * @param code
	 * @return
	 */
	public Product findProduct(String code,Long companyId);
	
	/**
	 * method to find product by id
	 * @param id
	 * @return
	 */
	public ProductVo findProductById(Long id);
	
	/**
	 * method to find product by code
	 * @param code
	 * @return
	 */
	public ProductVo findProductByCode(String code,Long companyId);
	
	/**
	 * method to find all products
	 * @param companyId
	 * @return
	 */
	public List<ProductVo> findAllProducts(Long companyId);
	
	/**
	 * 
	 * @param ids
	 * @return
	 */
	public List<ProductVo> findProducts(List<Long> ids);
	
}
