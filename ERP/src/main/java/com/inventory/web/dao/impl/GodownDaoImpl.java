package com.inventory.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.dao.CompanyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.Godown;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.inventory.web.constants.InventoryConstants;
import com.inventory.web.dao.GodownDao;
import com.inventory.web.vo.GodownVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 21, 2015
 */
@Repository
public class GodownDaoImpl extends AbstractDao implements GodownDao {

	@Autowired
	private CompanyDao companyDao;
	
	public void saveOrUpdateGodown(GodownVo godownVo) {
		godownVo.setName(godownVo.getName().trim());
		Long id = godownVo.getGodownId();
		Company company = companyDao.findCompany(godownVo.getCompanyId());
		if(company == null)
			throw new ItemNotFoundException("Company Not Found");
		Godown compareGodown = findGodownObjectByName(godownVo.getName(), company.getId());
		Godown godown = null;
		if(id == null){
			if(compareGodown != null)
				throw new ItemNotFoundException("Godown Name Already Exists !!! ");
			godown = new Godown();
		}else{
			godown = findGodownObject(id);
			if(compareGodown != null && !compareGodown.equals(godown))
				throw new ItemNotFoundException("Godown Name Already Exists !!! ");
		}
		godown.setCompany(company);
		godown.setName(godownVo.getName());
		this.sessionFactory.getCurrentSession().saveOrUpdate(godown);
	}

	public void deleteGodown(Long id) {

		Godown godown = findGodownObject(id);
		if(godown == null)
			throw new ItemNotFoundException("Godown Not Found");
		this.sessionFactory.getCurrentSession().delete(godown);
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<GodownVo> listAllGodownsForCompany(Long companyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Godown.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY)		;
		criteria.add(Restrictions.eq("company.id", companyId));
		criteria.add(Restrictions.not(Restrictions.eq("name", InventoryConstants.TO_BE_ISSUED_GODOWN)));
		List<Godown> godowns = criteria.list();
		List<GodownVo> vos =  new ArrayList<GodownVo>();
		if(godowns != null){
			for(Godown godown : godowns){
				vos.add(this.createGodownVo(godown));
			}
		}
		return vos;
	}

	private GodownVo createGodownVo(Godown godown){
		GodownVo godownVo = new GodownVo();
		godownVo.setGodownId(godown.getId());
		godownVo.setName(godown.getName());
		Company company = godown.getCompany();
		if(company!=null)
			godownVo.setCompanyId(company.getId());
		return godownVo;
	}
	public GodownVo findGodown(Long id) {
		Godown godown = findGodownObject(id);
		if(godown == null)
			throw new ItemNotFoundException("Godown Not Found");
		return this.createGodownVo(godown);
	}

	public Godown findGodownObject(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Godown.class);
		criteria.add(Restrictions.eq("id", id));
		return (Godown)criteria.uniqueResult();
	}

	public GodownVo findGodownByName(String name, Long companyId) {
		Godown godown = findGodownObjectByName(name, companyId);
		return this.createGodownVo(godown);
	}

	public Godown findGodownObjectByName(String name, Long companyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Godown.class);
		criteria.add(Restrictions.eq("name", name));
		criteria.add(Restrictions.eq("company.id", companyId));
		return (Godown)criteria.uniqueResult();
	}

	public Godown findGodownObjectByName(String name, Company company) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Godown.class);
		criteria.add(Restrictions.eq("name", name));
		criteria.add(Restrictions.eq("company", company));
		return (Godown)criteria.uniqueResult();
	}

}
