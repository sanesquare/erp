package com.sales.web.constants;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface SalesRequestConstants {

	String HOME = "salesHome.do";
	String CUSTOMER = "customer.do";
	String CUSTOMER_ADD="addCustomer.do";
	String SAVE_CUSTOMER="saveCustomer.do";
	String EDIT_CUSTOMER="editCustomer.do";
	String DELETE_CUSTOMER="deleteCustomer.do";
	String CUSTOMER_LIST="customerList.do";
	String CUSTOMER_CONTACT_SAVE="saveCustomerContact.do";
	String CUSTOMER_CONTACT_LIST ="customContacts.do";
	String GET_CONTACT = "getContact.do";
	String DELETE_CONTACT="deleteContacts.do";
	
	String CUSTOMER_QUOTES_HOME="customerQuotesHome.do";
	String CUSTOMER_QUOTES_LIST="customerQuotesList.do";
	String ADD_CUSTOMER_QUOTE="addCustomerQuote.do";
	String CUSTOMER_QUOTE_SAVE="saveCustomerQoutes.do";
	String CUSTOMER_QUOTE_VIEW="viewCustomerQuote.do";
	String CUSTOMER_QUOTE_DELETE="deleteCustomerQuote.do";
	String CONVERT_CUSTOMER_QUOTE_TO_ORDER="convertCustomerQouteToOrder.do";
	
	String SALES_ORDERS_HOME="salesOrderHome.do";
	String SALES_ORDERS_LIST="salesOrderList.do";
	String ADD_SALES_ORDER="addSalesOrder.do";
	String SALES_ORDER_SAVE="saveSalesOrder.do";
	String SALES_ORDER_VIEW="viewSalesOrder.do";
	String SALES_ORDER_DELETE="deleteSalesOrder.do";
	String CONVERT_SALES_ORDER_TO_DELIVERY_NOTE="salesOrderToDeliveryNote.do";
	
	String INVOICE_HOME="invoiceHome.do";
	String INVOICE_ADD="invoiceAdd.do";
	String CONVERT_DELIVERY_NOTE_TO_INVOICE="convertDeliveryNotToInvoice.do";
	String INVOICE_SAVE="saveInvoice.do";
	String INVOICE_VIEW="viewInvoice.do";
	String INVOICE_DELETE="deleteInvoice.do";
	String INVOICE_LIST="invoiceList.do";
	String PAY_INVOICE_INSTALLMENT="payInvoiceInstallment.do";
	String RECEIPT_INSTALLMENTS="getInstallmentsForInvoice.do";
	String CONVERT_INVOICE_TO_DELIVERY_NOTE="convertInvoiceToDeliveryNote.do";
	String INVOICE_BY_CUSTOMER = "getInvoicesByCustomer.do";
	
	String SALES_RETURN_HOME = "salesReturnHome.do";
	String SALES_RETURN_ADD = "addSalesReturn.do";
	String RETURN_THIS_INVOICE = "returnThisInvoice.do";
	String SALES_RETURN_SAVE="saveSalesReturn.do";
	String SALES_RETURN_VIEW="viewSalesReturn.do";
	String SALES_RETURN_DELETE="deleteSalesReturn.do";
	String SALES_RETURN_LIST="listSalesReturn.do";
	
	String GENERATE_INVOICE_PDF="generateInvoice.do";
	String GENERATE_SALES_ORDER_PDF="generateSalesOrderPdf.do";
	String GENERATE_CUSTOMER_QUOTE_PDF="generateCustomerQuotePdf.do";
}
