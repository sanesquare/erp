package com.sales.web.constants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface SalesPageNameConstants {

	String HOME_PAGE = "salesHome";
	String CUSTOMERS_PAGE = "customers";
	String CUSTOMER_ADD_PAGE = "addCustomer";
	String CUSTOMER_LIST_TEMP = "customersList";
	String CUSTOMER_CONTACTS_LIST = "customerContactsList";

	String CUSTOMER_QUOTES_HOME = "customerQuotesHome";
	String ADD_CUSTOMER_QUOTES = "addCustomerQuote";
	String CUSTOMER_QUOTE_TEMPLATE = "customerQuotesTemplate";

	String SALES_ORDERS_HOME = "salesOrderHome";
	String ADD_SALES_ORDER = "addSalesOrder";
	String SALES_OREDER_LIST = "salesOrderList";

	String INVOICE = "invoices";
	String INVOICE_ADD = "addInvoice";
	String INVOICE_LIST = "invoiceList";
	String RECEIPT_INSTALLMENTS = "receiptInstallments";

	String SALES_RETURN_HOME = "salesReturnHome";
	String SALES_RETURN_ADD = "salesReturnAdd";
	String SALES_RETURN_LIST="salesReturnList";
}
