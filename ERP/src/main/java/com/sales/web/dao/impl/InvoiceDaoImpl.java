package com.sales.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.JournalDao;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.PaymentDao;
import com.accounts.web.dao.ReceiptDao;
import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.Receipts;
import com.accounts.web.vo.JournalItemsVo;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.ReceiptItemsVo;
import com.accounts.web.vo.ReceiptVo;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.ChargeDao;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.DeliveryMethodDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.dao.PaymentTermsDao;
import com.erp.web.dao.VatDao;
import com.erp.web.entities.Charge;
import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.Godown;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.erp.web.vo.ErpCurrencyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryConstants;
import com.inventory.web.dao.DeliveryNoteDao;
import com.inventory.web.dao.GodownDao;
import com.inventory.web.dao.InventoryWithdrawalDao;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.entities.DeliveryNote;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.InventoryWithdrawalItems;
import com.inventory.web.entities.Product;
import com.inventory.web.vo.DeliveryNoteProductVo;
import com.inventory.web.vo.DeliveryNoteVo;
import com.inventory.web.vo.InventoryWithdrawalItemsVo;
import com.inventory.web.vo.InventoryWithdrawalVo;
import com.sales.web.dao.CustomerDao;
import com.sales.web.dao.InvoiceDao;
import com.sales.web.entities.Customer;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.InvoiceCharges;
import com.sales.web.entities.InvoiceInstallments;
import com.sales.web.entities.InvoiceItems;
import com.sales.web.vo.InvoiceChargeVo;
import com.sales.web.vo.InvoiceInstallmentVo;
import com.sales.web.vo.InvoiceProductVo;
import com.sales.web.vo.InvoiceVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
@Repository
public class InvoiceDaoImpl extends AbstractDao implements InvoiceDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private ErpCurrencyDao currencyDao;

	@Autowired
	private GodownDao godownDao;

	@Autowired
	private InventoryWithdrawalDao inventoryWithdrawalDao;

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private VatDao vatDao;

	@Autowired
	private JournalDao journalDao;

	@Autowired
	private CustomerDao customerDao;

	@Autowired
	private PaymentTermsDao paymentTermsDao;

	@Autowired
	private PaymentDao paymentDao;

	@Autowired
	private ReceiptDao receiptDao;

	@Autowired
	private ChargeDao chargeDao;

	@Autowired
	private DeliveryNoteDao deliveryNoteDao;

	@Autowired
	private DeliveryMethodDao deliveryMethodDao;

	public Long saveOrUpdateInvoice(InvoiceVo vo) {
		Boolean exists = true;
		Long id = vo.getInvoiceId();
		Invoice invoice = this.findInvoice(id);
		Boolean isDirect = true;
		Company company = companyDao.findCompany(vo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		if (invoice == null) {
			invoice = new Invoice();
			invoice.setIsArchive(false);
			exists = false;
			invoice.setReferenceNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.INVOICE,
					vo.getCompanyId()));
			for (Long deliveryNoteId : vo.getDeliveryNoteIds()) {
				if (deliveryNoteId != null) {
					isDirect = false;
					DeliveryNote deliveryNote = deliveryNoteDao.findDeliveryNoteObject(deliveryNoteId);
					invoice.getDeliveryNotes().add(deliveryNote);
				}
			}
		}
		Customer customer = customerDao.findCustomerObjectById(vo.getCustomerId());
		if (customer == null)
			throw new ItemNotFoundException("Customer Not Found.");
		Ledger paymentToAccount = ledgerDao.findLedgerObjectById(vo.getLedgerId());
		if (paymentToAccount == null)
			throw new ItemNotFoundException("Ledger Not Found.");
		invoice.setLedger(paymentToAccount);

		Ledger billsReceivableLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.BILLS_RECIEVABLE_LEDGER,
				company.getId());
		if (billsReceivableLedger == null)
			throw new ItemNotFoundException("Ledger Not Found.");
		Ledger journalByAccount = ledgerDao.findLedgerObjectByName(AccountsConstants.SALES_LEDGER, company.getId());

		if (journalByAccount == null)
			throw new ItemNotFoundException("Ledger Not Found.");

		Ledger taxPayable = ledgerDao.findLedgerObjectByName(AccountsConstants.TAX_PAYABLE_LEDGER, company.getId());
		if (taxPayable == null)
			throw new ItemNotFoundException("Tax Receivable Ledger Not Found");

		Ledger discountPayable = ledgerDao.findLedgerObjectByName(AccountsConstants.DISCOUNT_PAYABLE_LEDGER,
				company.getId());
		if (discountPayable == null)
			throw new ItemNotFoundException("Discount Received Ledger Not Found");
		PaymentTerms paymentTerm = paymentTermsDao.findPaymentTerms(vo.getPaymentTermId());
		if (paymentTerm == null)
			throw new ItemNotFoundException("Payment Term Not Found.");
		DeliveryMethod method = deliveryMethodDao.findDeliveryMethodObject(vo.getDeliveryTermId());
		if (method != null) {
			invoice.setDeliveryTerm(method);
		}
		ErPCurrency currency = currencyDao.findCurrency(vo.getErpCurrencyId());
		invoice.setCompany(company);
		invoice.setCustomer(customer);
		invoice.setPaymentTerms(paymentTerm);
		invoice.setCurrency(currency);
		invoice.setDate(DateFormatter.convertStringToDate(vo.getInvoiceDate()));
		invoice = this.setAttributesToInvoice(invoice, vo);
		invoice = this.setJournalToInvoice(invoice, vo, journalByAccount, billsReceivableLedger, taxPayable,
				discountPayable);
		invoice = this.setProductsToInvoice(invoice, vo, isDirect);
		invoice = this.setChargesToInvoice(invoice, vo);
		invoice = this.setInstallments(invoice, vo, billsReceivableLedger, paymentToAccount);
		id = (Long) this.sessionFactory.getCurrentSession().save(invoice);

		Set<DeliveryNote> notes = invoice.getDeliveryNotes();
		if (notes != null) {
			for (DeliveryNote note : notes) {
				note.setInvoice(invoice);
			}
			this.sessionFactory.getCurrentSession().merge(invoice);
		}

		if (!exists) {
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.JOURNAL, company);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.INVOICE, company);
		}
		return id;
	}

	/**
	 * method to set payment installments to invoice
	 * 
	 * @param invoice
	 * @param invoiceVo
	 * @return
	 */
	private Invoice setInstallments(Invoice invoice, InvoiceVo invoiceVo, Ledger billReceivable, Ledger receiptToAccount) {
		invoice.getInvoiceInstallments().clear();
		Boolean isSettled = true;
		if (invoiceVo.getIsSettled()) {
			for (InvoiceInstallmentVo installmentVo : invoiceVo.getInstallmentVos()) {
				InvoiceInstallments invoiceInstallments = this.findInstallment(installmentVo.getInstallmentId());
				if (invoiceInstallments == null)
					invoiceInstallments = new InvoiceInstallments();
				invoiceInstallments.setIsPaid(true);
				invoiceInstallments.setAmount(installmentVo.getAmount());
				invoiceInstallments.setDate(DateFormatter.convertStringToDate(installmentVo.getDate()));
				invoiceInstallments.setPercentage(installmentVo.getPercentage());
				invoiceInstallments.setInvoice(invoice);
				invoiceInstallments.setReceipts(this.setAttributesToReceipt(invoice, invoiceInstallments,
						installmentVo, billReceivable, receiptToAccount));
				invoice.getInvoiceInstallments().add(invoiceInstallments);
			}
		} else {
			for (InvoiceInstallmentVo installmentVo : invoiceVo.getInstallmentVos()) {
				InvoiceInstallments invoiceInstallments = this.findInstallment(installmentVo.getInstallmentId());
				if (invoiceInstallments == null)
					invoiceInstallments = new InvoiceInstallments();
				invoiceInstallments.setDate(DateFormatter.convertStringToDate(installmentVo.getDate()));
				if (installmentVo.getIsPaid()) {
					invoiceInstallments.setIsPaid(true);
					invoiceInstallments.setReceipts(this.setAttributesToReceipt(invoice, invoiceInstallments,
							installmentVo, billReceivable, receiptToAccount));
				} else {
					isSettled = false;
					invoiceInstallments.setIsPaid(false);
				}
				invoiceInstallments.setAmount(installmentVo.getAmount());
				invoiceInstallments.setPercentage(installmentVo.getPercentage());
				invoiceInstallments.setInvoice(invoice);
				invoice.getInvoiceInstallments().add(invoiceInstallments);
			}
		}
		invoice.setIsSetteld(isSettled);
		return invoice;
	}

	private Receipts setAttributesToReceipt(Invoice invoice, InvoiceInstallments invoiceInstallments,
			InvoiceInstallmentVo invoiceInstallmentVo, Ledger toLedger, Ledger byLedger) {
		Company company = invoice.getCompany();
		ErpCurrencyVo currencyVo = currencyDao.findCurrencyForDate(invoice.getDate(), invoice.getCurrency()
				.getBaseCurrency());
		if (currencyVo != null) {
			invoiceInstallmentVo.setAmount(invoiceInstallmentVo.getAmount().divide(currencyVo.getRate(), 2,
					RoundingMode.HALF_EVEN));
		}
		Receipts receipts = new Receipts();
		receipts.setCompany(company);
		receipts.setReceiptDate(invoiceInstallments.getDate());
		receipts.setReceiptNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.RECEIPT, company.getId()));
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.RECEIPT, company);
		ReceiptVo receiptVo = new ReceiptVo();
		receiptVo.setDate(invoiceInstallmentVo.getDate());
		receiptVo.setAmount(invoiceInstallmentVo.getAmount());
		ReceiptItemsVo byItem = new ReceiptItemsVo();
		ReceiptItemsVo toItem = new ReceiptItemsVo();
		byItem.setAccTypeId(byLedger.getId());
		byItem.setDebitAmnt(invoiceInstallmentVo.getAmount());
		toItem.setCreditAmnt(invoiceInstallmentVo.getAmount());
		toItem.setAccTypeId(toLedger.getId());
		receiptVo.getEntries().add(toItem);
		receiptVo.getEntries().add(byItem);
		receiptVo.setNarration("Receipt For Invoice " + invoice.getReferenceNumber());
		receipts.setIsDependant(true);
		receipts = receiptDao.setAttributesForReceipt(receipts, receiptVo, false);
		receipts.setInvoiceInstallments(invoiceInstallments);
		return receipts;
	}

	private InvoiceInstallments findInstallment(Long id) {
		return (InvoiceInstallments) this.sessionFactory.getCurrentSession().createCriteria(InvoiceInstallments.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * method to set charges to invoice
	 * 
	 * @param invoice
	 * @param invoiceVo
	 * @return
	 */
	private Invoice setChargesToInvoice(Invoice invoice, InvoiceVo invoiceVo) {
		invoice.getInvoiceCharges().clear();
		Set<InvoiceCharges> invoiceCharges = new HashSet<InvoiceCharges>();
		for (InvoiceChargeVo chargeVo : invoiceVo.getChargeVos()) {
			InvoiceCharges charges = new InvoiceCharges();
			Charge charge = chargeDao.findCharge(chargeVo.getChargeId());
			charges.setCharge(charge);
			charges.setAmount(chargeVo.getAmount());
			charge.getInvoiceCharges().add(charges);
			charges.setInvoice(invoice);
			invoiceCharges.add(charges);
		}
		invoice.getInvoiceCharges().addAll(invoiceCharges);
		return invoice;
	}

	/**
	 * method to set attributes to invoice
	 * 
	 * @param invoice
	 * @param invoiceVo
	 * @return
	 */
	private Invoice setProductsToInvoice(Invoice invoice, InvoiceVo invoiceVo, Boolean isDirect) {
		Set<InvoiceItems> oldItems = invoice.getInvoiceItems();
		Map<Product, InvoiceItems> map = new HashMap<Product, InvoiceItems>();
		for (InvoiceItems invoiceItems : oldItems)
			map.put(invoiceItems.getProduct(), invoiceItems);
		invoice.getInvoiceItems().clear();
		List<InvoiceItems> invoiceItems = new ArrayList<InvoiceItems>();
		for (InvoiceProductVo productVo : invoiceVo.getProductVos()) {
			InvoiceItems item = new InvoiceItems();
			Company company = invoice.getCompany();
			Product product = productDao.findProduct(productVo.getProductCode(), company.getId());
			InvoiceItems oldItem = map.get(product);
			Godown godown = null;
			if (isDirect) {
				godown = godownDao.findGodownObjectByName(InventoryConstants.TO_BE_ISSUED_GODOWN, company);
				if (godown == null) {
					Godown toBeIssued = new Godown();
					toBeIssued.setCompany(company);
					toBeIssued.setName(InventoryConstants.TO_BE_ISSUED_GODOWN);
					company.getGodowns().add(toBeIssued);
					this.sessionFactory.getCurrentSession().save(toBeIssued);
					godown = toBeIssued;
				}
			} else
				godown = godownDao.findGodownObject(productVo.getGodownId());
			if (godown == null)
				throw new ItemNotFoundException("Godown not found.");
			// set inventory entry
			InventoryWithdrawal withdrawal = new InventoryWithdrawal();
			InventoryWithdrawalVo withdrawalVo = new InventoryWithdrawalVo();
			withdrawalVo.setDate(invoiceVo.getInvoiceDate());
			InventoryWithdrawalItemsVo withdrawalItemsVo = new InventoryWithdrawalItemsVo();
			withdrawalItemsVo.setProductCode(productVo.getProductCode());
			withdrawalItemsVo.setUnitPrice(productVo.getUnitPrice());
			withdrawalItemsVo.setQuantity(productVo.getQuantity());
			withdrawalItemsVo.setTotal(productVo.getNetAmount());
			withdrawalVo.getItemsVos().add(withdrawalItemsVo);
			withdrawalVo.setNarration("Inventory Withdrawal For Invoice " + invoice.getReferenceNumber());
			withdrawal.setCode(inventoryWithdrawalDao.getNumber(company.getId(),
					NumberPropertyConstants.INVENTORY_WITHDRAWAL));
			withdrawal.setCompany(company);
			withdrawal.setType("Receipt");
			withdrawal.setGodown(godown);
			withdrawal = inventoryWithdrawalDao.setAttributesToInventoryWithdrawal(withdrawal, withdrawalVo, false);
			withdrawal.setInvoiceItems(item);
			item.setInventoryWithdrawal(withdrawal);

			item.setProduct(product);
			item.setAmountExcludingTax(productVo.getAmountExcludingTax());
			item.setQuantity(productVo.getQuantity());
			item.setDiscountPercentage(productVo.getDiscount());
			item.setPrice(productVo.getUnitPrice());
			item.setNetAmount(productVo.getNetAmount());
			if (productVo.getTaxId() != null) {
				VAT vat = vatDao.findTaxObject(productVo.getTaxId());
				if (vat == null)
					throw new ItemNotFoundException("Tax Not Found");
				item.setVat(vat);
				vat.getInvoiceItems().add(item);
			}
			if (oldItem != null) {
				item.setDeliveredQuantity(oldItem.getDeliveredQuantity());
				item.setPackedQuantity(oldItem.getPackedQuantity());
				item.setReturnedQuantity(oldItem.getReturnedQuantity());
			} else if (!isDirect) {
				item.setDeliveredQuantity(item.getQuantity());
				item.setReturnedQuantity(BigDecimal.ZERO);
				item.setPackedQuantity(BigDecimal.ZERO);
			} else {
				item.setDeliveredQuantity(BigDecimal.ZERO);
				item.setReturnedQuantity(BigDecimal.ZERO);
				item.setPackedQuantity(BigDecimal.ZERO);
			}
			item.setInvoice(invoice);
			product.getInvoiceItems().add(item);
			invoiceItems.add(item);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.INVENTORY_WITHDRAWAL, company);
		}
		invoice.getInvoiceItems().addAll(invoiceItems);
		return invoice;
	}

	/**
	 * method to set attributes to invoice
	 * 
	 * @param invoice
	 * @param invoiceVo
	 * @return
	 */
	private Invoice setAttributesToInvoice(Invoice invoice, InvoiceVo invoiceVo) {
		invoice.getVat().clear();
		invoice.setIsArchive(false);
		invoice.setNetAmount(invoiceVo.getNetAmountHidden());
		invoice.setSubTotal(invoiceVo.getSubTotal());
		invoice.setDiscountPercentage(invoiceVo.getDiscountRate());
		invoice.setDiscountAmount(invoiceVo.getDiscountTotal());
		invoice.setNetTotal(invoiceVo.getNetTotal());
		invoice.setTaxTotal(invoiceVo.getTaxTotal());
		invoice.setGrandTotal(invoiceVo.getGrandTotal());
		List<VAT> taxes = new ArrayList<VAT>();
		for (Long id : invoiceVo.getTsxIds()) {
			VAT vat = vatDao.findTaxObject(id);
			taxes.add(vat);
		}
		invoice.getVat().addAll(taxes);
		return invoice;
	}

	/**
	 * method to set journal to invoice
	 * 
	 * @param invoice
	 * @param invoiceVo
	 * @param byLedger
	 * @param toLedger
	 * @param taxPayable
	 * @param discountPayable
	 * @return
	 */
	private Invoice setJournalToInvoice(Invoice invoice, InvoiceVo invoiceVo, Ledger byLedger, Ledger toLedger,
			Ledger taxPayable, Ledger discountPayable) {
		ErpCurrencyVo currencyVo = currencyDao.findCurrencyForDate(invoice.getDate(), invoice.getCurrency()
				.getBaseCurrency());
		BigDecimal grandTotal = invoiceVo.getGrandTotal();
		if (currencyVo != null) {
			grandTotal = grandTotal.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
			invoiceVo.setNetAmountHidden(invoiceVo.getNetAmountHidden().divide(currencyVo.getRate(), 2,
					RoundingMode.HALF_EVEN));
			invoiceVo.setProductTaxTotal(invoiceVo.getProductTaxTotal().divide(currencyVo.getRate(), 2,
					RoundingMode.HALF_EVEN));
			invoiceVo.setDiscountTotal(invoiceVo.getDiscountTotal().divide(currencyVo.getRate(), 2,
					RoundingMode.HALF_EVEN));
		}
		Journal journal = invoice.getJournal();
		if (journal == null) {
			journal = new Journal();
			journal.setJournalNumber(journalDao.getJournalNumberForCompany(invoice.getCompany().getId()));
			journal.setCompany(invoice.getCompany());
			journal.setIsDependant(true);
		} else {
			journal.getEntries().clear();
		}

		JournalVo journalVo = new JournalVo();
		journalVo.setDate(invoiceVo.getInvoiceDate());
		journalVo.setAmount(grandTotal.add(invoiceVo.getDiscountTotal()));

		JournalItemsVo byItem = new JournalItemsVo();
		byItem.setAccTypeId(byLedger.getId());
		byItem.setCreditAmnt(invoiceVo.getNetAmountHidden());
		journalVo.getEntries().add(byItem);

		Set<VAT> vats = invoice.getVat();
		Ledger taxLedger = null;
		if (invoiceVo.getProductTaxTotal().compareTo(BigDecimal.ZERO) != 0) {
			for (VAT vat : vats) {
				taxLedger = vat.getLedger();
				JournalItemsVo taxByItem = new JournalItemsVo();
				taxByItem.setAccTypeId(taxLedger.getId());
				taxByItem.setCreditAmnt(invoiceVo.getNetTotal().multiply(vat.getPercentage())
						.divide(new BigDecimal(100), 2, RoundingMode.HALF_EVEN));
				journalVo.getEntries().add(taxByItem);
			}
		}
		JournalItemsVo toItem = new JournalItemsVo();
		toItem.setAccTypeId(toLedger.getId());
		toItem.setDebitAmnt(grandTotal);
		journalVo.getEntries().add(toItem);

		if (invoiceVo.getDiscountTotal().compareTo(BigDecimal.ZERO) != 0) {
			JournalItemsVo discount = new JournalItemsVo();
			discount.setAccTypeId(discountPayable.getId());
			discount.setDebitAmnt(invoiceVo.getDiscountTotal());
			journalVo.getEntries().add(discount);
		}
		journalVo.setNarration("Journal For Invoice " + invoice.getReferenceNumber());
		journal = journalDao.setAttributesForJournal(journal, journalVo, false);

		journal.setInvoice(invoice);
		invoice.setJournal(journal);
		return invoice;
	}

	public void deleteInvoice(Long id) {
		Invoice invoice = this.findInvoice(id);
		if (invoice == null)
			throw new ItemNotFoundException("Invoice Not Found");
		Set<DeliveryNote> deliveryNotes = invoice.getDeliveryNotes();
		for (DeliveryNote deliveryNote : deliveryNotes) {
			if (deliveryNote.getSalesOrder() != null) {
				deliveryNote.setInvoice(null);
				this.sessionFactory.getCurrentSession().merge(deliveryNote);
			}
		}
		this.sessionFactory.getCurrentSession().delete(invoice);
	}

	public Invoice findInvoice(Long id) {
		return (Invoice) this.sessionFactory.getCurrentSession().createCriteria(Invoice.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public InvoiceVo findInvoiceObject(Long id) {
		Invoice invoice = this.findInvoice(id);
		if (invoice == null)
			throw new ItemNotFoundException("Invoice Not Found");
		return createInvoiceVo(invoice);
	}

	/**
	 * method to create invoice vo
	 * 
	 * @param invoice
	 * @return
	 */
	private InvoiceVo createInvoiceVo(Invoice invoice) {
		Company company = invoice.getCompany();
		InvoiceVo vo = new InvoiceVo();
		vo.setCompanyId(company.getId());
		vo.setInvoiceId(invoice.getId());
		vo.setInvoiceCode(invoice.getReferenceNumber());
		Customer customer = invoice.getCustomer();
		Ledger ledger = invoice.getLedger();
		vo.setAccount(ledger.getLedgerName());
		vo.setLedgerId(ledger.getId());
		vo.setCustomer(customer.getName());
		vo.setCustomerId(customer.getId());
		vo.setInvoiceDate(DateFormatter.convertDateToString(invoice.getDate()));
		vo.setIsSettled(invoice.getIsSetteld());
		if (invoice.getIsSetteld())
			vo.setIsEdit(true);
		PaymentTerms paymentTerm = invoice.getPaymentTerms();
		vo.setPaymentTerm(paymentTerm.getTerm());
		vo.setPaymentTermId(paymentTerm.getId());
		ErPCurrency currency = invoice.getCurrency();
		vo.setErpCurrencyId(currency.getId());
		vo.setCurrecny(currency.getBaseCurrency().getBaseCurrency());
		vo.setCurrency(currency.getBaseCurrency().getBaseCurrency());
		vo.setSubTotal(invoice.getSubTotal());
		vo.setDiscountRate(invoice.getDiscountPercentage());
		vo.setDiscountTotal(invoice.getDiscountAmount());
		vo.setNetTotal(invoice.getNetTotal());
		vo.setTaxTotal(invoice.getTaxTotal());
		vo.setGrandTotal(invoice.getGrandTotal());
		Set<VAT> taxes = invoice.getVat();
		for (VAT tax : taxes) {
			vo.getTaxes().add(tax.getName());
			vo.getTaxesWithRate().add(tax.getId() + "!!!" + tax.getPercentage());
		}
		vo = this.createInvoiceProductVo(vo, invoice);
		vo = this.createInvoiceChargeVo(vo, invoice);
		vo = this.createInvoiceInstallmentVo(vo, invoice);
		return vo;
	}

	private InvoiceVo createInvoiceProductVo(InvoiceVo vo, Invoice invoice) {
		List<InvoiceProductVo> productVos = new ArrayList<InvoiceProductVo>();
		BigDecimal taxAmount = new BigDecimal("0.00");
		Boolean isDelivered = true;
		Boolean isReturned = true;
		Boolean isPacked = true;
		for (InvoiceItems items : invoice.getInvoiceItems()) {
			InvoiceProductVo productVo = new InvoiceProductVo();
			InventoryWithdrawal withdrawal = items.getInventoryWithdrawal();
			if (withdrawal != null) {
				Set<InventoryWithdrawalItems> withdrawalItems = withdrawal.getWithdrawalItems();
				InventoryWithdrawalItems withdrawalItem = null;
				for (InventoryWithdrawalItems item : withdrawalItems) {
					withdrawalItem = item;
				}
				Godown godown = withdrawalItem.getTransaction().getGodown();
				productVo.setGodownId(godown.getId());
			}
			Product product = items.getProduct();
			productVo.setAmountExcludingTax(items.getAmountExcludingTax());
			vo.setNetAmountHidden(vo.getNetAmountHidden().add(items.getAmountExcludingTax()));
			productVo.setProductCode(product.getProductCode());
			productVo.setProductName(product.getName());
			if (items.getQuantity().compareTo(items.getDeliveredQuantity()) != 0)
				isDelivered = false;
			BigDecimal quantityToReturn = items.getQuantity().subtract(items.getReturnedQuantity());
			productVo.setQuantityToReturn(quantityToReturn);
			if (quantityToReturn.compareTo(BigDecimal.ZERO) == 0)
				productVo.setIsReturned(true);
			else
				isReturned = false;
			BigDecimal quantityToPack = items.getQuantity().subtract(items.getPackedQuantity());
			if (quantityToPack.compareTo(BigDecimal.ZERO) != 0)
				isPacked = false;
			productVo.setProductId(product.getId());
			productVo.setInvoiceProductId(items.getId());
			taxAmount = taxAmount.add(items.getNetAmount().subtract(items.getAmountExcludingTax()));
			productVo.setQuantity(items.getQuantity());
			productVo.setUnitPrice(items.getPrice());
			productVo.setDeliveredQuantity(items.getDeliveredQuantity());
			productVo.setReturnedQuantity(items.getReturnedQuantity());
			productVo.setDiscount(items.getDiscountPercentage());
			productVo.setNetAmount(items.getNetAmount());
			VAT tax = items.getVat();
			if (tax != null) {
				productVo.setTax(tax.getName());
			}
			productVos.add(productVo);

		}
		vo.setIsPacked(isPacked);
		vo.setIsReturned(isReturned);
		vo.setIsDelivered(isDelivered);
		vo.setProductsTaxAmount(taxAmount);
		vo.setProductVos(productVos);
		return vo;
	}

	private InvoiceVo createInvoiceChargeVo(InvoiceVo vo, Invoice invoice) {
		List<InvoiceChargeVo> chargeVos = new ArrayList<InvoiceChargeVo>();
		for (InvoiceCharges charge : invoice.getInvoiceCharges()) {
			InvoiceChargeVo chargeVo = new InvoiceChargeVo();
			chargeVo.setChargeId(charge.getId());
			chargeVo.setCharge(charge.getCharge().getName());
			chargeVo.setAmount(charge.getAmount());
			chargeVos.add(chargeVo);
		}
		vo.setChargeVos(chargeVos);
		return vo;
	}

	private InvoiceVo createInvoiceInstallmentVo(InvoiceVo vo, Invoice invoice) {
		Boolean settled = false;
		BigDecimal outStanding = new BigDecimal("0.00");
		List<InvoiceInstallmentVo> installmentVos = new ArrayList<InvoiceInstallmentVo>();
		for (InvoiceInstallments installment : invoice.getInvoiceInstallments()) {
			if (!installment.getIsPaid()) {
				outStanding = outStanding.add(installment.getAmount());
			} else
				settled = true;
			InvoiceInstallmentVo installmentVo = new InvoiceInstallmentVo();
			installmentVo.setId(installment.getId());
			installmentVo.setDate(DateFormatter.convertDateToString(installment.getDate()));
			installmentVo.setAmount(installment.getAmount());
			installmentVo.setPercentage(installment.getPercentage());
			installmentVo.setIsPaid(installment.getIsPaid());
			installmentVo.setInstallmentId(installment.getId());
			installmentVos.add(installmentVo);
		}
		vo.setIsEdit(settled);
		vo.setOutStanding(outStanding);
		vo.setInstallmentVos(installmentVos);
		return vo;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<InvoiceVo> listInvoicesForCompany(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Invoice.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("company", company)).add(Restrictions.eq("isArchive", false));
		List<Invoice> invoices = criteria.list();
		List<InvoiceVo> invoiceVos = new ArrayList<InvoiceVo>();
		for (Invoice invoice : invoices) {
			invoiceVos.add(createInvoiceVo(invoice));
		}
		return invoiceVos;
	}

	public void updateBillInstallmentPay(Long installmentId) {
		InvoiceInstallments installment = this.findInstallment(installmentId);
		if (installment == null)
			throw new ItemNotFoundException("Installment Not Found.");
		Invoice invoice = installment.getInvoice();
		Company company = invoice.getCompany();
		Ledger byLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.BILLS_RECIEVABLE_LEDGER, company.getId());
		Ledger toLedger = invoice.getLedger();
		InvoiceInstallmentVo installmentVo = new InvoiceInstallmentVo();
		installmentVo.setAmount(installment.getAmount());
		installment.setIsPaid(true);
		installmentVo.setDate(DateFormatter.convertDateToString(installment.getDate()));
		installment.setReceipts(this.setAttributesToReceipt(invoice, installment, installmentVo, byLedger, toLedger));
		invoice.getInvoiceInstallments().add(installment);
		Boolean isSettled = true;
		for (InvoiceInstallments installments : invoice.getInvoiceInstallments()) {
			if (!installments.getIsPaid())
				isSettled = false;
		}
		invoice.setIsSetteld(isSettled);
		this.sessionFactory.getCurrentSession().merge(invoice);
	}

	public DeliveryNoteVo convertINvoiceToDeliveryNote(Long id) {
		DeliveryNoteVo noteVo = new DeliveryNoteVo();
		Invoice invoice = this.findInvoice(id);
		if (invoice == null)
			throw new ItemNotFoundException("Invoice Not Found.");
		noteVo.setIsFromInvoice(true);
		noteVo = createDeliveryNoteVo(noteVo, invoice);
		return noteVo;
	}

	private DeliveryNoteVo createDeliveryNoteVo(DeliveryNoteVo noteVo, Invoice invoice) {
		Customer customer = invoice.getCustomer();
		noteVo.setCustomerId(customer.getId());
		noteVo.setCustomer(customer.getName());
		noteVo.setDate(DateFormatter.convertDateToString(invoice.getDate()));
		noteVo.setInvoiceId(invoice.getId());
		List<DeliveryNoteProductVo> productVos = new ArrayList<DeliveryNoteProductVo>();
		for (InvoiceItems items : invoice.getInvoiceItems()) {
			BigDecimal quantity = items.getQuantity().subtract(items.getDeliveredQuantity());
			if (quantity.compareTo(BigDecimal.ZERO) != 0) {
				DeliveryNoteProductVo productVo = new DeliveryNoteProductVo();
				Product product = items.getProduct();
				productVo.setProductCode(product.getProductCode());
				productVo.setProductName(product.getName());
				productVo.setProductId(product.getId());
				productVo.setProductId(items.getId());
				productVo.setQuantity(quantity);
				productVos.add(productVo);
			}
		}
		noteVo.setProductVos(productVos);
		return noteVo;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<InvoiceVo> listInvoicesByCustomer(Long customerId) {
		Customer customer = customerDao.findCustomerObjectById(customerId);
		if (customer == null)
			throw new ItemNotFoundException("Customer Not Found.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Invoice.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("customer", customer)).add(Restrictions.eq("isArchive", false));
		List<Invoice> invoices = criteria.list();
		List<InvoiceVo> invoiceVos = new ArrayList<InvoiceVo>();
		for (Invoice invoice : invoices) {
			invoiceVos.add(createInvoiceVo(invoice));
		}
		return invoiceVos;
	}
}
