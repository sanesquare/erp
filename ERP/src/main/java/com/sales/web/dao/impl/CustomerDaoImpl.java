/**
 * 
 */
package com.sales.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.hssf.record.aggregates.CustomViewSettingsRecordAggregate;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CountryDao;
import com.hrms.web.entities.Country;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.purchase.web.entities.Vendor;
import com.purchase.web.vo.VendorVo;
import com.sales.web.dao.CustomerDao;
import com.sales.web.entities.Customer;
import com.sales.web.entities.CustomerContact;
import com.sales.web.vo.CustomerContactVo;
import com.sales.web.vo.CustomerVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-August-2015
 */
@Repository
public class CustomerDaoImpl extends AbstractDao implements CustomerDao {

	@Autowired
	private CountryDao countryDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	/**
	 * Method to save or update customer
	 * 
	 * @param customerVo
	 * @return
	 */
	public Long saveOrUpdateCustomer(CustomerVo customerVo) {
		Long id = null;
		Customer customer = null;
		Company company = null;
		if (customerVo.getCustomerId() == null) {
			customer = new Customer();
			company = companyDao.findCompany(customerVo.getCompanyId());
			if (company == null)
				throw new ItemNotFoundException("Company Not Found.");
			customer.setCompany(company);
			customer.setCode(this.generateCustomerCode(NumberPropertyConstants.CUSTOMER, company.getId()));
			customer = this.setAttributesForCustomer(customer, customerVo);
			id = (Long) this.sessionFactory.getCurrentSession().save(customer);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.CUSTOMER, company);
		} else {
			id = customerVo.getCustomerId();
			customer = this.findCustomerObjectById(id);
			customer = this.setAttributesForCustomer(customer, customerVo);
			this.sessionFactory.getCurrentSession().merge(customer);
		}
		return id;
	}

	public String generateCustomerCode(String category, Long companyId) {
		String code = "";
		NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(category, companyId);
		code = numberPropertyVo.getPrefix() + numberPropertyVo.getNumber();
		return code;
	}

	/**
	 * Method to set values to customer
	 * 
	 * @param customer
	 * @param customerVo
	 * @return
	 */
	private Customer setAttributesForCustomer(Customer customer, CustomerVo customerVo) {
		Company company = companyDao.findCompany(customerVo.getCompanyId());
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		Country country = countryDao.findCountry(customerVo.getCountryId());
		if (country == null)
			throw new ItemNotFoundException("Country Not Found");
		customer.setCompany(company);
		customer.setName(customerVo.getName());
		customer.setPhone(customerVo.getPhone());
		customer.setEmail(customerVo.getEmail());
		customer.setMobile(customerVo.getMobile());
		customer.setAddress(customerVo.getAddress());
		customer.setDeliveryAddress(customerVo.getDeliveryAddress());
		customer.setCountry(country);
		customer.setIsCompany(customerVo.getIsCompany());
		customer.setIsLead(customerVo.getIsLead());
		return customer;
	}

	/**
	 * Method to find customer by id
	 * 
	 * @param id
	 * @return
	 */
	public Customer findCustomerObjectById(Long id) {
		return (Customer) this.sessionFactory.getCurrentSession().createCriteria(Customer.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * Method to find customer by code
	 * 
	 * @param id
	 * @return
	 */
	private Customer findCustomerObjectByCode(String code) {
		return (Customer) this.sessionFactory.getCurrentSession().createCriteria(Customer.class)
				.add(Restrictions.eq("code", code)).uniqueResult();
	}

	/**
	 * Method to delete customer
	 * 
	 * @param Id
	 */
	public void deleteCustomerById(Long id) {
		Customer customer = this.findCustomerObjectById(id);
		if (customer == null)
			throw new ItemNotFoundException("Customer does not exist");
		else
			this.sessionFactory.getCurrentSession().delete(customer);
	}

	/**
	 * Method to find all customers
	 * 
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<CustomerVo> findAllCustomers() {
		List<CustomerVo> list = new ArrayList<CustomerVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Customer.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Customer> customers = criteria.list();
		for (Customer customer : customers) {
			list.add(this.createCustomerVo(customer));
		}
		return list;
	}

	/**
	 * Method to create customer vo
	 * 
	 * @param customer
	 * @return
	 */
	private CustomerVo createCustomerVo(Customer customer) {
		CustomerVo vo = new CustomerVo();
		vo.setCustomerId(customer.getId());
		vo.setCountryId(customer.getId());
		vo.setName(customer.getName());
		vo.setCustomerCode(customer.getCode());
		vo.setAddress(customer.getAddress());
		vo.setCountryId(customer.getCountry().getId());
		vo.setDeliveryAddress(customer.getDeliveryAddress());
		vo.setEmail(customer.getEmail());
		vo.setIsCompany(customer.getIsCompany());
		vo.setIsLead(customer.getIsLead());
		vo.setMobile(customer.getMobile());
		vo.setPhone(customer.getPhone());
		Company company = customer.getCompany();
		if (company != null)
			vo.setCompanyId(company.getId());
		List<CustomerContactVo> contactVos = new ArrayList<CustomerContactVo>();
		for (CustomerContact contact : customer.getContacts()) {
			contactVos.add(this.createCustomerContactVo(contact));
		}
		vo.setContactVos(contactVos);
		return vo;
	}

	/**
	 * Method to find customer by id
	 * 
	 * @param id
	 * @return
	 */
	public CustomerVo findCustomerById(Long id) {
		Customer customer = this.findCustomerObjectById(id);
		if (customer == null)
			throw new ItemNotFoundException("Customer does not exist ");
		else
			return this.createCustomerVo(customer);
	}

	/**
	 * Method to find customer by code
	 * 
	 * @param Code
	 * @return
	 */
	public CustomerVo findCustomerByCode(String code) {
		Customer customer = this.findCustomerObjectByCode(code);
		if (customer == null)
			throw new ItemNotFoundException("Customer does not exist ");
		else
			return this.createCustomerVo(customer);
	}

	/**
	 * Method to save or update customer contact
	 * 
	 * @param contactVo
	 * @return
	 */
	public void saveOrUpdateCustomerContact(CustomerContactVo contactVo) {
		CustomerContact contact = null;
		Customer customer = null;
		customer = this.findCustomerObjectById(contactVo.getCustomerId());
		if (customer == null)
			throw new ItemNotFoundException("Customer Does Not Exist");
		else {
			if (contactVo.getCustomerContactId() != null) {
				contact = this.findContactbyId(contactVo.getCustomerContactId());
				if (contact == null)
					throw new ItemNotFoundException("Customer Contact Does Not Exist");
				else {
					contact = this.setAttributesForContact(contact, contactVo);
				}
			} else {
				contact = new CustomerContact();
				contact.setCustomer(customer);
				contact = this.setAttributesForContact(contact, contactVo);
			}
			customer.getContacts().add(contact);
			this.sessionFactory.getCurrentSession().merge(customer);
		}
	}

	/**
	 * Method to set attributes for contact
	 * 
	 * @param contact
	 * @param contactVo
	 * @return
	 */
	private CustomerContact setAttributesForContact(CustomerContact contact, CustomerContactVo contactVo) {
		contact.setName(contactVo.getName());
		contact.setEmail(contactVo.getEmail());
		contact.setWorkPhone(contactVo.getPhone());
		contact.setAddress(contactVo.getAddress());
		return contact;
	}

	/**
	 * Method to find customer contact by id
	 * 
	 * @param id
	 * @return
	 */
	private CustomerContact findContactbyId(Long id) {
		return (CustomerContact) this.sessionFactory.getCurrentSession().createCriteria(CustomerContact.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * Method to delete customer contact
	 * 
	 * @param id
	 */
	public void deleteCustomerContactbyId(Long id) {

		CustomerContact contact = this.findContactbyId(id);
		if (contact == null)
			throw new ItemNotFoundException("Customer Contact Not Found ");
		else
			this.sessionFactory.getCurrentSession().delete(contact);
	}

	/**
	 * Method to find customer contact by id
	 * 
	 * @param id
	 * @return
	 */
	public CustomerContactVo findCustomerContactById(Long id) {
		CustomerContact contact = (CustomerContact) this.sessionFactory.getCurrentSession()
				.createCriteria(CustomerContact.class).add(Restrictions.eq("id", id)).uniqueResult();
		return this.createCustomerContactVo(contact);
	}

	/**
	 * Method to create customer contact vo
	 * 
	 * @param contact
	 * @return
	 */
	private CustomerContactVo createCustomerContactVo(CustomerContact contact) {
		CustomerContactVo contactVo = new CustomerContactVo();
		contactVo.setCustomerContactId(contact.getId());
		contactVo.setName(contact.getName());
		contactVo.setCustomerContactCode(contact.getCode());
		contactVo.setCustomerId(contact.getCustomer().getId());
		contactVo.setEmail(contact.getEmail());
		contactVo.setPhone(contact.getWorkPhone());
		contactVo.setAddress(contact.getAddress());
		return contactVo;
	}

	/**
	 * Method to find customer contact by code
	 * 
	 * @param code
	 * @return
	 */
	public CustomerContactVo findCustomerContactByCode(String code) {
		CustomerContact contact = (CustomerContact) this.sessionFactory.getCurrentSession()
				.createCriteria(CustomerContact.class).add(Restrictions.eq("code", code)).uniqueResult();
		return this.createCustomerContactVo(contact);
	}

	/**
	 * Method to finds contacts of customer
	 * 
	 * @param id
	 * @return
	 */
	public Set<CustomerContactVo> findContactsForCustomerById(Long id) {
		Set<CustomerContactVo> contactVos = new HashSet<CustomerContactVo>();
		Customer customer = this.findCustomerObjectById(id);
		if (customer == null)
			throw new ItemNotFoundException("Customer Not Found");
		else {
			Set<CustomerContact> contacts = customer.getContacts();
			for (CustomerContact contact : contacts) {
				contactVos.add(this.createCustomerContactVo(contact));
			}
		}
		return contactVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<CustomerVo> findCustomersForCompany(Long companyId) {

		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Customer.class);
		criteria.add(Restrictions.eq("company", company));
		List<Customer> customers = new ArrayList<Customer>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		customers = criteria.list();
		List<CustomerVo> customerVos = new ArrayList<CustomerVo>();
		for (Customer customer : customers) {
			customerVos.add(createCustomerVo(customer));
		}
		return customerVos;
	}
}
