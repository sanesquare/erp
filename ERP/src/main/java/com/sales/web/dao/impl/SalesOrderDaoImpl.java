package com.sales.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.ChargeDao;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.DeliveryMethodDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.dao.PaymentTermsDao;
import com.erp.web.dao.VatDao;
import com.erp.web.entities.Charge;
import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.entities.DeliveryNote;
import com.inventory.web.entities.Product;
import com.inventory.web.entities.ReceivingNote;
import com.purchase.web.dao.VendorDao;
import com.purchase.web.entities.BillCharges;
import com.purchase.web.entities.Vendor;
import com.purchase.web.entities.VendorQuoteItems;
import com.purchase.web.entities.VendorQuotes;
import com.purchase.web.vo.BillChargeVo;
import com.purchase.web.vo.VendorQuoteVo;
import com.purchase.web.vo.VendorquoteProductVo;
import com.sales.web.dao.CustomerDao;
import com.sales.web.dao.SalesOrderDao;
import com.sales.web.entities.Customer;
import com.sales.web.entities.SalesOrder;
import com.sales.web.entities.SalesOrderCharges;
import com.sales.web.entities.SalesOrderItems;
import com.sales.web.vo.SalesOrderChargeVo;
import com.sales.web.vo.SalesOrderProductVo;
import com.sales.web.vo.SalesOrderVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
@Repository
public class SalesOrderDaoImpl extends AbstractDao implements SalesOrderDao {

	@Autowired
	private ProductDao productDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private PaymentTermsDao paymentTermsDao;

	@Autowired
	private VatDao vatDao;

	@Autowired
	private ChargeDao chargeDao;

	@Autowired
	private ErpCurrencyDao currencyDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private CustomerDao customerDao;

	@Autowired
	private DeliveryMethodDao deliveryMethodDao;

	public Long saveOrUpdateSalesOrder(SalesOrderVo vo) {
		Long id = vo.getOrderId();
		Boolean isNew = false;
		Boolean isQuote = vo.getIsQuote();
		SalesOrder order = this.findSalesOrder(id);
		Company company = null;
		if (order == null) {
			order = new SalesOrder();
			company = companyDao.findCompany(vo.getCompanyId());
			isNew = true;
			if (isQuote) {
				order.setQuoteCode(
						numberPropertyDao.getRefernceNumber(NumberPropertyConstants.CUSTOMER_QUOTE, vo.getCompanyId()));
				order.setIsQuote(true);
			} else {
				order.setOrderCode(
						numberPropertyDao.getRefernceNumber(NumberPropertyConstants.SALES_ORDER, vo.getCompanyId()));
				order.setIsQuote(false);
			}
		} else {
			company = order.getCompany();
		}
		order.setCompany(company);
		PaymentTerms paymentTerms = paymentTermsDao.findPaymentTerms(vo.getPaymentTermId());
		if (paymentTerms == null)
			throw new ItemNotFoundException("Payment term not found");
		order.setPaymentTerm(paymentTerms);
		DeliveryMethod deliveryMethod = deliveryMethodDao.findDeliveryMethodObject(vo.getDeliveryTermId());
		if (deliveryMethod != null) {
			order.setDeliveryTerm(deliveryMethod);
		}
		ErPCurrency currency = currencyDao.findCurrency(vo.getErpCurrencyId());
		order.setCurrency(currency);
		Customer customer = customerDao.findCustomerObjectById(vo.getCustomerId());
		if (customer == null)
			throw new ItemNotFoundException("Customer Not Found.");
		order.setCustomer(customer);
		if (!isQuote)
			order = this.setChargesToSalesORder(order, vo);
		order = this.setAttributesToOrder(order, vo);
		order = this.setProductsToSalesOrder(order, vo);
		if (isNew) {
			id = (Long) this.sessionFactory.getCurrentSession().save(order);
			if (isQuote)
				numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.CUSTOMER_QUOTE, company);
			else
				numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.SALES_ORDER, company);
		} else
			this.sessionFactory.getCurrentSession().merge(order);
		return id;
	}

	private SalesOrder setChargesToSalesORder(SalesOrder order, SalesOrderVo vo) {
		order.getCharges().clear();
		Set<SalesOrderCharges> salesOrderCharges = new HashSet<SalesOrderCharges>();
		for (SalesOrderChargeVo chargeVo : vo.getChargeVos()) {
			SalesOrderCharges charges = new SalesOrderCharges();
			Charge charge = chargeDao.findCharge(chargeVo.getChargeId());
			charges.setCharge(charge);
			charges.setAmount(chargeVo.getAmount());
			charge.getSalesOrderharges().add(charges);
			charges.setOrder(order);
			salesOrderCharges.add(charges);
		}
		order.getCharges().addAll(salesOrderCharges);
		return order;
	}

	/**
	 * method to set attributes to sales order
	 * 
	 * @param order
	 * @param vo
	 * @return
	 */
	private SalesOrder setAttributesToOrder(SalesOrder order, SalesOrderVo vo) {
		order.setDate(DateFormatter.convertStringToDate(vo.getOrderDate()));
		order.setDiscountAmount(vo.getDiscountTotal());
		if (vo.getExpiryDate() != null)
			order.setExpirationDate(DateFormatter.convertStringToDate(vo.getExpiryDate()));
		order.setSubTotal(vo.getSubTotal());
		order.setTotal(vo.getGrandTotal());
		order.getVat().clear();
		order.setNetAmount(vo.getNetAmountHidden());
		order.setSubTotal(vo.getSubTotal());
		order.setDiscountPercentage(vo.getDiscountRate());
		order.setNetTotal(vo.getNetTotal());
		order.setTaxTotal(vo.getTaxTotal());
		List<VAT> taxes = new ArrayList<VAT>();
		for (Long id : vo.getTsxIds()) {
			VAT vat = vatDao.findTaxObject(id);
			taxes.add(vat);
		}
		order.getVat().addAll(taxes);
		return order;
	}

	/**
	 * method to set products to sales order
	 * 
	 * @param order
	 * @param vo
	 * @return
	 */
	private SalesOrder setProductsToSalesOrder(SalesOrder order, SalesOrderVo orderVo) {
		order.getOrderItems().clear();
		List<SalesOrderItems> items = new ArrayList<SalesOrderItems>();
		Boolean hasDelivered = true;
		for (SalesOrderProductVo vo : orderVo.getProductVos()) {
			SalesOrderItems item = new SalesOrderItems();
			VAT vat = vatDao.findTaxObject(vo.getTaxId());
			item.setVat(vat);
			Product product = productDao.findProduct(vo.getProductCode(), orderVo.getCompanyId());
			item.setProduct(product);
			product.getSalesOrderItems().add(item);
			item.setQuantity(vo.getQuantity());
			item.setDeliveredQuantity(vo.getDeliveredQuantity());
			item.setPrice(vo.getUnitPrice());
			item.setDicscountRate(vo.getDiscount());
			item.setTotal(vo.getNetAmount());
			item.setAmountExcludingTax(vo.getAmountExcludingTax());
			item.setSalesOrder(order);
			items.add(item);
			if (item.getQuantity().compareTo(item.getDeliveredQuantity()) != 0)
				hasDelivered = false;
		}
		order.setHasDelivered(hasDelivered);
		order.getOrderItems().addAll(items);
		return order;
	}

	public SalesOrder findSalesOrder(Long id) {
		return (SalesOrder) this.sessionFactory.getCurrentSession().createCriteria(SalesOrder.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public void deleteSalesOrder(Long id) {
		SalesOrder order = this.findSalesOrder(id);
		if (order == null)
			throw new ItemNotFoundException("Sales Order Not Found");
		if (order.getIsQuote())
			this.sessionFactory.getCurrentSession().delete(order);
		else if (order.getQuoteCode() == null)
			this.sessionFactory.getCurrentSession().delete(order);
		else {
			order.setIsQuote(true);
			this.sessionFactory.getCurrentSession().merge(order);
		}
	}

	public SalesOrderVo findSalesOrderById(Long id) {
		SalesOrder order = this.findSalesOrder(id);
		if (order == null)
			throw new ItemNotFoundException("Sales Order Not Found");
		return createSalesOrderVo(order);
	}

	private SalesOrderVo createSalesOrderVo(SalesOrder order) {
		SalesOrderVo vo = new SalesOrderVo();
		vo.setOrderId(order.getId());
		Customer customer = order.getCustomer();
		vo.setCustomer(customer.getName());
		vo.setCustomerId(customer.getId());
		if (order.getExpirationDate() != null)
			vo.setExpiryDate(DateFormatter.convertDateToString(order.getExpirationDate()));
		Set<DeliveryNote> deliveryNote = order.getDeliveryNote();
		if (deliveryNote != null) {
			Map<Long, String> noteIdCode = new HashMap<Long, String>();
			for (DeliveryNote note : deliveryNote) {
				noteIdCode.put(note.getId(), note.getReferenceNumber());
			}
			vo.setDeliveryNoteIdCode(noteIdCode);
		}
		vo.setOrderDate(DateFormatter.convertDateToString(order.getDate()));
		ErPCurrency currency = order.getCurrency();
		vo.setCurrency(currency.getBaseCurrency().getBaseCurrency());
		Company company = order.getCompany();
		vo.setCompanyId(company.getId());
		vo.setDiscountRate(order.getDiscountPercentage());
		vo.setGrandTotal(order.getTotal());
		vo.setNetAmountHidden(order.getNetAmount());
		vo.setNetTotal(order.getNetTotal());
		vo.setSubTotal(order.getSubTotal());
		PaymentTerms paymentTerms = order.getPaymentTerm();
		vo.setPaymentTerm(paymentTerms.getTerm());
		vo.setPaymentTermId(paymentTerms.getId());
		vo.setHasDelivered(order.getHasDelivered());
		DeliveryMethod deliveryMethod = order.getDeliveryTerm();
		if (deliveryMethod != null) {
			vo.setDeliveryTerm(deliveryMethod.getMethod());
			vo.setDeliveryTermId(deliveryMethod.getId());
		}
		vo.setTaxTotal(order.getTaxTotal());
		vo.setDiscountTotal(order.getDiscountAmount());
		for (VAT tax : order.getVat()) {
			vo.getTaxes().add(tax.getName());
			vo.getTsxIds().add(tax.getId());
			vo.getTaxesWithRate().add(tax.getId() + "!!!" + tax.getPercentage());
		}

		List<SalesOrderProductVo> items = new ArrayList<SalesOrderProductVo>();
		for (SalesOrderItems item : order.getOrderItems()) {
			SalesOrderProductVo productVo = new SalesOrderProductVo();
			productVo.setAmountExcludingTax(item.getAmountExcludingTax());
			productVo.setDiscount(item.getDicscountRate());
			productVo.setNetAmount(item.getTotal());
			productVo.setQuantity(item.getQuantity());
			productVo.setDeliveredQuantity(item.getDeliveredQuantity());
			productVo.setOrderId(order.getId());
			Product product = item.getProduct();
			productVo.setProductCode(product.getProductCode());
			productVo.setProductId(product.getId());
			productVo.setProductName(product.getName());
			productVo.setUnitPrice(item.getPrice());
			VAT vat = item.getVat();
			if (vat != null) {
				productVo.setTax(vat.getName());
				productVo.setTaxId(vat.getId());
			}
			items.add(productVo);
		}
		vo.setProductVos(items);
		if (!order.getIsQuote())
			vo = this.creatChargeVo(order, vo);
		vo.setIsQuote(order.getIsQuote());
		vo.setQuoteCode(order.getQuoteCode());
		vo.setOrderCode(order.getOrderCode());
		return vo;
	}

	private SalesOrderVo creatChargeVo(SalesOrder order, SalesOrderVo vo) {
		Charge charge = null;
		for (SalesOrderCharges charges : order.getCharges()) {
			charge = charges.getCharge();
			SalesOrderChargeVo chargeVo = new SalesOrderChargeVo();
			chargeVo.setCharge(charge.getName());
			chargeVo.setChargeId(charge.getId());
			chargeVo.setAmount(charges.getAmount());
			vo.getChargeVos().add(chargeVo);
		}
		return vo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<SalesOrderVo> listSalesOrders(Long companyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalesOrder.class);
		criteria.add(Restrictions.eq("company.id", companyId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("isQuote", false));
		List<SalesOrder> orders = criteria.list();
		List<SalesOrderVo> list = new ArrayList<SalesOrderVo>();
		if (orders != null) {
			for (SalesOrder order : orders)
				list.add(this.createSalesOrderVo(order));
		}
		return list;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<SalesOrderVo> listCustomerQuotes(Long companyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalesOrder.class);
		criteria.add(Restrictions.eq("company.id", companyId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.isNotNull("quoteCode"));
		List<SalesOrder> orders = criteria.list();
		List<SalesOrderVo> list = new ArrayList<SalesOrderVo>();
		if (orders != null) {
			for (SalesOrder order : orders)
				list.add(this.createSalesOrderVo(order));
		}
		return list;
	}

	public void updateCustomerQuoteToSalesOrder(Long id) {
		SalesOrder order = findSalesOrder(id);
		if (order == null)
			throw new ItemNotFoundException("Customer Quote Not Found.");
		Company company = order.getCompany();
		order.setIsQuote(false);
		order.setOrderCode(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.SALES_ORDER, company.getId()));
		this.sessionFactory.getCurrentSession().merge(order);
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.SALES_ORDER, company);
	}
}
