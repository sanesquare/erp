package com.sales.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.JournalDao;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.PaymentDao;
import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.Payment;
import com.accounts.web.vo.JournalItemsVo;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.PaymentItemsVo;
import com.accounts.web.vo.PaymentVo;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.Godown;
import com.erp.web.vo.ErpCurrencyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.dao.GodownDao;
import com.inventory.web.dao.InventoryEntryDao;
import com.inventory.web.dao.ProductDao;
import com.inventory.web.entities.InventoryEntry;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.Product;
import com.inventory.web.vo.InventoryEntryItemsVo;
import com.inventory.web.vo.InventoryEntryVo;
import com.sales.web.dao.InvoiceDao;
import com.sales.web.dao.SalesReturnDao;
import com.sales.web.entities.Customer;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.InvoiceInstallments;
import com.sales.web.entities.InvoiceItems;
import com.sales.web.entities.SalesReturn;
import com.sales.web.entities.SalesReturnItems;
import com.sales.web.vo.SalesReturnItemsVo;
import com.sales.web.vo.SalesReturnVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 12, 2015
 */
@Repository
public class SalesReturnDaoImpl extends AbstractDao implements SalesReturnDao {

	@Autowired
	private InvoiceDao invoiceDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private GodownDao godownDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private InventoryEntryDao inventoryEntryDao;

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private ErpCurrencyDao currencyDao;

	@Autowired
	private PaymentDao paymentDao;

	@Autowired
	private JournalDao journalDao;

	public Long saveSalesReturn(SalesReturnVo vo) {
		Long id = vo.getReturnId();
		SalesReturn salesReturn = this.findSalesReturnObjectById(id);
		Invoice invoice = null;
		Boolean isNew = false;
		Company company = null;
		if (salesReturn == null) {
			invoice = invoiceDao.findInvoice(vo.getInvoiceId());
			if (invoice == null)
				throw new ItemNotFoundException("Invoice Not Found");
			company = invoice.getCompany();
			salesReturn = new SalesReturn();
			isNew = true;
			salesReturn.setCompany(company);
			salesReturn.setInvoice(invoice);
			salesReturn.setReferenceNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.SALES_RETURN,
					company));
		} else {
			invoice = salesReturn.getInvoice();
			company = invoice.getCompany();
		}
		Ledger salesReturnLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.SALES_RETURN_LEDGER,
				company.getId());
		if (invoice.getIsSetteld()) {
			salesReturn = this.setAccountTransactionForCashSales(salesReturn, vo, salesReturnLedger);
		} else {
			salesReturn = this.setAccountTransactionForCreditSales(salesReturn, vo, salesReturnLedger);
		}
		salesReturn.setReturnDate(DateFormatter.convertStringToDate(vo.getDate()));
		salesReturn.setCustomer(invoice.getCustomer());
		salesReturn = this.setProductsToReturn(salesReturn, vo, invoice, company);
		if (isNew) {
			id = (Long) this.sessionFactory.getCurrentSession().save(salesReturn);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.SALES_RETURN, company);
			invoice.getSalesReturns().add(salesReturn);
			this.sessionFactory.getCurrentSession().merge(invoice);
		} else {
			invoice.getSalesReturns().add(salesReturn);
			this.sessionFactory.getCurrentSession().merge(invoice);
		}
		return id;
	}

	private SalesReturn setAccountTransactionForCreditSales(SalesReturn salesReturn, SalesReturnVo vo,
			Ledger returnLedger) {
		BigDecimal totalPaid = BigDecimal.ZERO;
		BigDecimal pendingAmount = BigDecimal.ZERO;
		for (InvoiceInstallments installment : salesReturn.getInvoice().getInvoiceInstallments()) {
			if (installment.getIsPaid()) {
				totalPaid = totalPaid.add(installment.getAmount());
			} else {
				pendingAmount = pendingAmount.add(installment.getAmount());
			}
		}

		// Receipt cash a/c Dr to Purchase Return
		Company company = salesReturn.getCompany();
		BigDecimal total = BigDecimal.ZERO;
		for (SalesReturnItemsVo itemsVo : vo.getProductVos()) {
			total = total.add(itemsVo.getTotal());
		}
		ErpCurrencyVo currencyVo = currencyDao.findCurrencyForDate(salesReturn.getReturnDate(), salesReturn
				.getInvoice().getCurrency().getBaseCurrency());
		if (currencyVo != null) {
			total = total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
		}

		BigDecimal journalAmount = total.subtract(totalPaid);
		if (totalPaid.compareTo(total) != -1) {
			Payment payment = salesReturn.getPayment();
			if (payment == null)
				payment = new Payment();
			else {
				payment.getEntries().clear();
			}
			payment.setCompany(company);
			payment.setPaymentDate(salesReturn.getReturnDate());
			payment.setPaymentNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.PAYMENT,
					company.getId()));
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PAYMENT, company);
			PaymentVo paymentVo = new PaymentVo();
			paymentVo.setDate(vo.getDate());
			paymentVo.setAmount(total);
			PaymentItemsVo byItem = new PaymentItemsVo();
			PaymentItemsVo toItem = new PaymentItemsVo();
			Ledger toLedger = salesReturn.getInvoice().getLedger();
			byItem.setAccTypeId(returnLedger.getId());
			byItem.setDebitAmnt(total);
			toItem.setCreditAmnt(total);
			toItem.setAccTypeId(toLedger.getId());
			paymentVo.getEntries().add(toItem);
			paymentVo.getEntries().add(byItem);
			paymentVo.setNarration("Payment For Sales Return " + salesReturn.getReferenceNumber());
			payment.setIsDependant(true);
			payment = paymentDao.setAttributesForPayment(payment, paymentVo, false);
			payment.setSalesReturn(salesReturn);
			salesReturn.setPayment(payment);
		} else {
			if (totalPaid.compareTo(BigDecimal.ZERO) != 0) {
				Payment payment = salesReturn.getPayment();
				if (payment == null)
					payment = new Payment();
				else
					payment.getEntries().clear();
				payment.setCompany(company);
				payment.setPaymentDate(salesReturn.getReturnDate());
				payment.setPaymentNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.PAYMENT,
						company.getId()));
				numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PAYMENT, company);
				PaymentVo paymentVo = new PaymentVo();
				paymentVo.setDate(vo.getDate());
				paymentVo.setAmount(totalPaid);
				PaymentItemsVo byItem = new PaymentItemsVo();
				PaymentItemsVo toItem = new PaymentItemsVo();
				Ledger toLedger = salesReturn.getInvoice().getLedger();
				byItem.setAccTypeId(returnLedger.getId());
				byItem.setDebitAmnt(totalPaid);
				toItem.setCreditAmnt(totalPaid);
				toItem.setAccTypeId(toLedger.getId());
				paymentVo.getEntries().add(toItem);
				paymentVo.getEntries().add(byItem);
				paymentVo.setNarration("Payment For Sales Return " + salesReturn.getReferenceNumber());
				payment.setIsDependant(true);
				payment = paymentDao.setAttributesForPayment(payment, paymentVo, false);
				payment.setSalesReturn(salesReturn);
				salesReturn.setPayment(payment);
			}
			if (journalAmount.compareTo(BigDecimal.ZERO) >= 0) {
				Ledger byLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.BILLS_RECIEVABLE_LEDGER,
						company.getId());
				Journal journal = salesReturn.getJournal();
				if (journal == null)
					journal = new Journal();
				else
					journal.getEntries().clear();
				journal.setCompany(company);
				journal.setJournalDate(salesReturn.getReturnDate());
				journal.setJournalNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.JOURNAL,
						company.getId()));
				numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.JOURNAL, company);
				JournalVo journalVo = new JournalVo();
				journalVo.setDate(vo.getDate());
				journalVo.setAmount(journalAmount);
				JournalItemsVo byItem = new JournalItemsVo();
				JournalItemsVo toItem = new JournalItemsVo();
				byItem.setAccTypeId(byLedger.getId());
				byItem.setDebitAmnt(journalAmount);
				toItem.setCreditAmnt(journalAmount);
				toItem.setAccTypeId(returnLedger.getId());
				journalVo.getEntries().add(toItem);
				journalVo.getEntries().add(byItem);
				journalVo.setNarration("Journal For Sales Return " + salesReturn.getReferenceNumber());
				journal.setIsDependant(true);
				journal = journalDao.setAttributesForJournal(journal, journalVo, false);
				journal.setSalesReturn(salesReturn);
				salesReturn.setJournal(journal);
			}
		}
		return salesReturn;
	}

	private SalesReturn setAccountTransactionForCashSales(SalesReturn salesReturn, SalesReturnVo vo, Ledger returnLedger) {
		Company company = salesReturn.getCompany();
		BigDecimal total = BigDecimal.ZERO;
		for (SalesReturnItemsVo itemsVo : vo.getProductVos()) {
			total = total.add(itemsVo.getTotal());
		}
		ErpCurrencyVo currencyVo = currencyDao.findCurrencyForDate(salesReturn.getReturnDate(), salesReturn
				.getInvoice().getCurrency().getBaseCurrency());
		if (currencyVo != null) {
			total = total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
		}
		Payment payment = salesReturn.getPayment();
		if (payment == null)
			payment = new Payment();
		payment.setCompany(company);
		payment.setPaymentDate(salesReturn.getReturnDate());
		payment.setPaymentNumber(numberPropertyDao.getRefernceNumber(NumberPropertyConstants.PAYMENT, company.getId()));
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PAYMENT, company);
		PaymentVo paymentVo = new PaymentVo();
		paymentVo.setDate(vo.getDate());
		paymentVo.setAmount(total);
		PaymentItemsVo byItem = new PaymentItemsVo();
		PaymentItemsVo toItem = new PaymentItemsVo();
		Ledger toLedger = salesReturn.getInvoice().getLedger();
		byItem.setAccTypeId(returnLedger.getId());
		byItem.setDebitAmnt(total);
		toItem.setCreditAmnt(total);
		toItem.setAccTypeId(toLedger.getId());
		paymentVo.getEntries().add(toItem);
		paymentVo.getEntries().add(byItem);
		paymentVo.setNarration("Payment For Sales Return " + salesReturn.getReferenceNumber());
		payment.setIsDependant(true);
		payment = paymentDao.setAttributesForPayment(payment, paymentVo, false);
		payment.setSalesReturn(salesReturn);
		salesReturn.setPayment(payment);
		return salesReturn;
	}

	private SalesReturn setProductsToReturn(SalesReturn salesReturn, SalesReturnVo vo, Invoice invoice, Company company) {
		Set<SalesReturnItems> oldItems = salesReturn.getSalesReturnItems();
		Map<Product, SalesReturnItems> oldItemsMap = new HashMap<Product, SalesReturnItems>();
		for (SalesReturnItems salesReturnItems : oldItems)
			oldItemsMap.put(salesReturnItems.getProduct(), salesReturnItems);
		salesReturn.getSalesReturnItems().clear();
		Set<InvoiceItems> invoiceItems = invoice.getInvoiceItems();
		Map<Product, InvoiceItems> map = new HashMap<Product, InvoiceItems>();
		for (InvoiceItems invoiceItem : invoiceItems)
			map.put(invoiceItem.getProduct(), invoiceItem);
		BigDecimal total = BigDecimal.ZERO;
		for (SalesReturnItemsVo itemsVo : vo.getProductVos()) {
			Product product = productDao.findProduct(itemsVo.getProductCode(), company.getId());
			SalesReturnItems item = new SalesReturnItems();
			item.setProduct(product);
			Godown godown = godownDao.findGodownObject(itemsVo.getGodownId());
			item.setGodown(godown);
			total = total.add(itemsVo.getTotal());
			item.setQuantity(itemsVo.getQuantity());
			item.setPrice(itemsVo.getUnitPrice());
			item.setTotal(itemsVo.getTotal());

			InventoryEntry entry = new InventoryEntry();
			InventoryEntryVo entryVo = new InventoryEntryVo();
			entryVo.setDate(vo.getDate());
			InventoryEntryItemsVo entryItemsVo = new InventoryEntryItemsVo();
			entryItemsVo.setProductCode(product.getProductCode());
			entryItemsVo.setUnitPrice(itemsVo.getUnitPrice());
			entryItemsVo.setQuantity(itemsVo.getQuantity());
			entryItemsVo.setTotal(item.getTotal());
			entryVo.getItemsVos().add(entryItemsVo);
			entryVo.setNarration("Inventory Entry For Sales Return " + salesReturn.getReferenceNumber());
			entry.setCode(inventoryEntryDao.getNumber(company.getId(), NumberPropertyConstants.INVENTORY_ENTRY));
			entry.setCompany(company);
			entry.setType("Receipt");
			entry.setGodown(godown);
			entry = inventoryEntryDao.setAttributesToInventoryEntry(entry, entryVo, false);
			entry.setSalesReturnItems(item);

			InvoiceItems invoiceItem = map.get(product);
			SalesReturnItems oldItem = oldItemsMap.get(product);
			if (oldItem != null)
				invoiceItem.setReturnedQuantity(invoiceItem.getReturnedQuantity().subtract(oldItem.getQuantity()));

			invoiceItem.setReturnedQuantity(invoiceItem.getReturnedQuantity().add(item.getQuantity()));
			invoice.getInvoiceItems().add(invoiceItem);

			item.setInventoryEntry(entry);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.INVENTORY_ENTRY, company);
			item.setSalesReturn(salesReturn);
			salesReturn.getSalesReturnItems().add(item);
		}

		salesReturn.setInvoice(invoice);
		return salesReturn;
	}

	public void deleteSalesReturn(Long id) {
		SalesReturn salesReturn = this.findSalesReturnObjectById(id);
		if (salesReturn == null)
			throw new ItemNotFoundException("Sales Return Not Found");
		Invoice invoice = salesReturn.getInvoice();
		Set<InvoiceItems> invoiceItems = invoice.getInvoiceItems();
		Map<Product, InvoiceItems> map = new HashMap<Product, InvoiceItems>();
		for (InvoiceItems invoiceItem : invoiceItems)
			map.put(invoiceItem.getProduct(), invoiceItem);
		for (SalesReturnItems sri : salesReturn.getSalesReturnItems()) {
			InvoiceItems invoiceIt = map.get(sri.getProduct());
			invoiceIt.setReturnedQuantity(invoiceIt.getReturnedQuantity().subtract(sri.getQuantity()));
			invoice.getInvoiceItems().add(invoiceIt);
		}
		invoice.getSalesReturns().remove(salesReturn);
		this.sessionFactory.getCurrentSession().merge(invoice);
	}

	public List<SalesReturnVo> findSalesReturns(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalesReturn.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<SalesReturn> salesReturns = criteria.list();
		List<SalesReturnVo> vos = new ArrayList<SalesReturnVo>();
		for (SalesReturn salesReturn : salesReturns) {
			vos.add(createSalesReturnVo(salesReturn));
		}
		return vos;
	}

	public SalesReturnVo findSalesReturnById(Long id) {
		SalesReturn salesReturn = this.findSalesReturnObjectById(id);
		if (salesReturn == null)
			throw new ItemNotFoundException("Sales Return Not Found.");
		return createSalesReturnVo(salesReturn);
	}

	private SalesReturnVo createSalesReturnVo(SalesReturn salesReturn) {
		SalesReturnVo vo = new SalesReturnVo();
		Invoice invoice = salesReturn.getInvoice();
		Ledger ledger = invoice.getLedger();
		Customer customer = salesReturn.getCustomer();
		ErPCurrency currency = invoice.getCurrency();
		vo.setReturnId(salesReturn.getId());
		vo.setInvoiceId(invoice.getId());
		vo.setCode(salesReturn.getReferenceNumber());
		vo.setDate(DateFormatter.convertDateToString(salesReturn.getReturnDate()));
		vo.setCustomer(customer.getName());
		vo.setAccount(ledger.getLedgerName());
		vo.setCurrecny(currency.getBaseCurrency().getBaseCurrency());
		vo.setIsSettled(invoice.getIsSetteld());

		Set<InvoiceItems> invoiceItems = invoice.getInvoiceItems();
		Map<Product, InvoiceItems> map = new HashMap<Product, InvoiceItems>();
		for (InvoiceItems invoiceItem : invoiceItems)
			map.put(invoiceItem.getProduct(), invoiceItem);

		for (SalesReturnItems item : salesReturn.getSalesReturnItems()) {
			SalesReturnItemsVo itemsVo = new SalesReturnItemsVo();
			Product product = item.getProduct();
			InvoiceItems invoiceItem = map.get(product);
			Godown godown = item.getGodown();
			itemsVo.setProductCode(product.getProductCode());
			itemsVo.setProductName(product.getName());
			if (godown != null) {
				itemsVo.setGodown(godown.getName());
				itemsVo.setGodownId(godown.getId());
			}
			itemsVo.setQuantity(item.getQuantity());
			itemsVo.setUnitPrice(item.getPrice());
			itemsVo.setQuantityToReturn(invoiceItem.getQuantity().subtract(invoiceItem.getReturnedQuantity())
					.add(item.getQuantity()));
			itemsVo.setTotal(item.getTotal());
			vo.getProductVos().add(itemsVo);
		}
		return vo;
	}

	public SalesReturnVo convertInvoiceToReturn(Long invoiceId) {
		SalesReturnVo vo = new SalesReturnVo();
		Invoice invoice = invoiceDao.findInvoice(invoiceId);
		if (invoice == null)
			throw new ItemNotFoundException("Invoice Not Found");
		Ledger ledger = invoice.getLedger();
		Customer customer = invoice.getCustomer();
		ErPCurrency currency = invoice.getCurrency();
		vo.setInvoiceId(invoice.getId());
		vo.setDate(DateFormatter.convertDateToString(invoice.getDate()));
		vo.setCustomer(customer.getName());
		vo.setAccount(ledger.getLedgerName());
		vo.setCurrecny(currency.getBaseCurrency().getBaseCurrency());
		vo.setIsSettled(invoice.getIsSetteld());
		Boolean isReturned = true;
		for (InvoiceItems item : invoice.getInvoiceItems()) {
			BigDecimal quantityToReturn = item.getQuantity().subtract(item.getReturnedQuantity());
			if (quantityToReturn.compareTo(BigDecimal.ZERO) != 0) {
				isReturned = false;
				SalesReturnItemsVo itemsVo = new SalesReturnItemsVo();
				Product product = item.getProduct();
				InventoryWithdrawal inventoryWithdrawal = item.getInventoryWithdrawal();
				if (inventoryWithdrawal != null) {
					Godown godown = inventoryWithdrawal.getGodown();
					itemsVo.setGodown(godown.getName());
					itemsVo.setGodownId(godown.getId());
				}
				itemsVo.setProductCode(product.getProductCode());
				itemsVo.setProductName(product.getName());
				itemsVo.setQuantity(quantityToReturn);
				itemsVo.setUnitPrice(item.getPrice());
				itemsVo.setQuantityToReturn(quantityToReturn);
				itemsVo.setTotal(quantityToReturn.multiply(item.getPrice()));
				vo.getProductVos().add(itemsVo);
			}
		}
		vo.setIsReturned(isReturned);
		return vo;
	}

	public SalesReturn findSalesReturnObjectById(Long id) {
		return (SalesReturn) this.sessionFactory.getCurrentSession().createCriteria(SalesReturn.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

}
