package com.sales.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sales.web.dao.SalesReturnDao;
import com.sales.web.service.SalesReturnService;
import com.sales.web.vo.SalesReturnVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 12, 2015
 */
@Service
public class SalesReturnServiceImpl implements SalesReturnService {

	@Autowired
	private SalesReturnDao dao;

	public Long saveSalesReturn(SalesReturnVo vo) {
		return dao.saveSalesReturn(vo);
	}

	public void deleteSalesReturn(Long id) {
		dao.deleteSalesReturn(id);
	}

	public List<SalesReturnVo> findSalesReturns(Long companyId) {
		return dao.findSalesReturns(companyId);
	}

	public SalesReturnVo findSalesReturnById(Long id) {
		return dao.findSalesReturnById(id);
	}

	public SalesReturnVo convertInvoiceToReturn(Long billId) {
		return dao.convertInvoiceToReturn(billId);
	}

}
