package com.sales.web.service;

import java.util.List;

import com.inventory.web.vo.DeliveryNoteVo;
import com.sales.web.vo.InvoiceVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
public interface InvoiceService {

	/**
	 * method to save invoice
	 * @param vo
	 * @return
	 */
	public Long saveOrUpdateInvoice(InvoiceVo vo);
	
	/**
	 * method to delete invoice
	 * @param id
	 */
	public void deleteInvoice(Long id);
	
	/**
	 * method to find invoice by id
	 * @param id
	 * @return
	 */
	public InvoiceVo findInvoiceObject(Long id);
	
	/**
	 * method to list invoices for company
	 * @param companyId
	 * @return
	 */
	public List<InvoiceVo> listInvoicesForCompany(Long companyId);
	
	public List<InvoiceVo> listInvoicesByCustomer(Long customerId);
	
	/**
	 * method to pay bill installment
	 * @param installmentId
	 */
	public void updateBillInstallmentPay(Long installmentId);
	
	/**
	 * method to convert invoice to delivery note
	 * @param id
	 * @return
	 */
	public DeliveryNoteVo convertINvoiceToDeliveryNote(Long id);
}
