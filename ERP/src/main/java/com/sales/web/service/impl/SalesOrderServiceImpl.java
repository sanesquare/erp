package com.sales.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sales.web.dao.SalesOrderDao;
import com.sales.web.service.SalesOrderService;
import com.sales.web.vo.SalesOrderVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
@Service
public class SalesOrderServiceImpl implements SalesOrderService{

	@Autowired
	private SalesOrderDao dao;

	public Long saveOrUpdateSalesOrder(SalesOrderVo vo) {
		return dao.saveOrUpdateSalesOrder(vo);
	}

	public void deleteSalesOrder(Long id) {
		dao.deleteSalesOrder(id);
	}

	public SalesOrderVo findSalesOrderById(Long id) {
		return dao.findSalesOrderById(id);
	}

	public List<SalesOrderVo> listSalesOrders(Long companyId) {
		return dao.listSalesOrders(companyId);
	}

	public List<SalesOrderVo> listCustomerQuotes(Long companyId) {
		return dao.listCustomerQuotes(companyId);
	}

	public void updateCustomerQuoteToSalesOrder(Long id) {
		dao.updateCustomerQuoteToSalesOrder(id);
	}
}
