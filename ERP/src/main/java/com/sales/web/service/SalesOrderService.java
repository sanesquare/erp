package com.sales.web.service;

import java.util.List;

import com.sales.web.vo.SalesOrderVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
public interface SalesOrderService {

	/**
	 * method to save or update order
	 * @param vo
	 * @return
	 */
	public Long saveOrUpdateSalesOrder(SalesOrderVo vo);
	
	/**
	 * method to delete sales order
	 * @param id
	 */
	public void deleteSalesOrder(Long id);
	
	/**
	 * method to find sales order by id
	 * @param id
	 * @return
	 */
	public SalesOrderVo findSalesOrderById(Long id);
	
	/**
	 * method to list sales orders for company
	 * @param companyId
	 * @return
	 */
	public List<SalesOrderVo> listSalesOrders(Long companyId);
	
	/**
	 * method to list customer quotes for company
	 * @param companyId
	 * @return
	 */
	public List<SalesOrderVo> listCustomerQuotes(Long companyId);
	
	/**
	 * method to convert customer quote to sales order
	 * @param id
	 */
	public void updateCustomerQuoteToSalesOrder(Long id);
}
