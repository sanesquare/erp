package com.sales.web.service;

import java.util.List;
import java.util.Set;

import com.sales.web.vo.CustomerContactVo;
import com.sales.web.vo.CustomerVo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-August-2015
 */
public interface CustomerService {

	/**
	 * Method to save or update customer
	 * @param customerVo
	 * @return
	 */
	public Long saveOrUpdateCustomer(CustomerVo customerVo) ;
	/**
	 * Method to delete customer
	 * @param Id
	 */
	public void deleteCustomerById(Long Id);
	/**
	 * Method to find all cusotmers
	 * @return
	 */
	public List<CustomerVo> findAllCustomers();
	/**
	 * Method to find customer by id
	 * @param id
	 * @return
	 */
	public CustomerVo findCustomerById(Long id);
	/**
	 * Method to find customer by code
	 * @param Code
	 * @return
	 */
	public CustomerVo findCustomerByCode(String Code);
	/**
	 * Method to save or update customer contact
	 * @param contactVo
	 * @return
	 */
	public void saveOrUpdateCustomerContact(CustomerContactVo contactVo);
	/**
	 * Method to delete customer contact
	 * @param id
	 */
	public void deleteCustomerContactbyId(Long id);
	/**
	 * Method to find customer contact by id 
	 * @param id
	 * @return
	 */
	public CustomerContactVo findCustomerContactById(Long id);
	/**
	 * Method to find customer contact by code
	 * @param code
	 * @return
	 */
	public CustomerContactVo findCustomerContactByCode(String code);
	/**
	 * Method to finds contacts of customer
	 * @param id
	 * @return
	 */
	public Set<CustomerContactVo> findContactsForCustomerById(Long id);

	/**
	 * Method to get customer code 
	 * @return
	 */
	public String generateCustomerCode();
	
	/**
	 * Method to find customers for company
	 * @param companyId
	 * @return
	 */
	public List<CustomerVo> findCustomersForCompany(Long companyId);
}
