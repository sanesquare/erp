package com.sales.web.service;

import java.util.List;

import com.purchase.web.vo.PurchaseReturnVo;
import com.sales.web.vo.SalesReturnVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 12, 2015
 */
public interface SalesReturnService {

	/**
	 * method to save sales return
	 * 
	 * @param vo
	 * @return
	 */
	public Long saveSalesReturn(SalesReturnVo vo);

	/**
	 * method to delete sales return
	 * 
	 * @param id
	 * @return
	 */
	public void deleteSalesReturn(Long id);

	/**
	 * method to find sales returns by company
	 * 
	 * @param companyId
	 * @return
	 */
	public List<SalesReturnVo> findSalesReturns(Long companyId);

	/**
	 * method to find sales return by id
	 * 
	 * @param id
	 * @return
	 */
	public SalesReturnVo findSalesReturnById(Long id);

	public SalesReturnVo convertInvoiceToReturn(Long billId);
}
