package com.sales.web.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sales.web.dao.CustomerDao;
import com.sales.web.service.CustomerService;
import com.sales.web.vo.CustomerContactVo;
import com.sales.web.vo.CustomerVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-August-2015
 */
@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDao customerDao;

	public Long saveOrUpdateCustomer(CustomerVo customerVo) {
		return customerDao.saveOrUpdateCustomer(customerVo);
	}

	public void deleteCustomerById(Long Id) {
		customerDao.deleteCustomerById(Id);
	}

	public List<CustomerVo> findAllCustomers() {
		return customerDao.findAllCustomers();
	}

	public CustomerVo findCustomerById(Long id) {
		return customerDao.findCustomerById(id);
	}

	public CustomerVo findCustomerByCode(String Code) {
		return customerDao.findCustomerByCode(Code);
	}

	public void saveOrUpdateCustomerContact(CustomerContactVo contactVo) {
		  customerDao.saveOrUpdateCustomerContact(contactVo);
	}

	public void deleteCustomerContactbyId(Long id) {
		customerDao.deleteCustomerContactbyId(id);
	}

	public CustomerContactVo findCustomerContactById(Long id) {
		return customerDao.findCustomerContactById(id);
	}

	public CustomerContactVo findCustomerContactByCode(String code) {
		return customerDao.findCustomerContactByCode(code);
	}
	/**
	 * Method to finds contacts of customer
	 * @param id
	 * @return
	 */
	public Set<CustomerContactVo> findContactsForCustomerById(Long id){
		return customerDao.findContactsForCustomerById(id);
	}
	/**
	 * Method to get customer code 
	 * @return
	 */
	public String generateCustomerCode(){
		return null;
	}

	public List<CustomerVo> findCustomersForCompany(Long companyId) {
		return customerDao.findCustomersForCompany(companyId);
	}
}
