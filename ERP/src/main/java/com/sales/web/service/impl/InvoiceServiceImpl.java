package com.sales.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.web.vo.DeliveryNoteVo;
import com.sales.web.dao.InvoiceDao;
import com.sales.web.service.InvoiceService;
import com.sales.web.vo.InvoiceVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceDao dao;

	public Long saveOrUpdateInvoice(InvoiceVo vo) {
		return dao.saveOrUpdateInvoice(vo);
	}

	public void deleteInvoice(Long id) {
		dao.deleteInvoice(id);
	}

	public InvoiceVo findInvoiceObject(Long id) {
		return dao.findInvoiceObject(id);
	}

	public List<InvoiceVo> listInvoicesForCompany(Long companyId) {
		return dao.listInvoicesForCompany(companyId);
	}

	public void updateBillInstallmentPay(Long installmentId) {
		dao.updateBillInstallmentPay(installmentId);
	}

	public DeliveryNoteVo convertINvoiceToDeliveryNote(Long id) {
		return dao.convertINvoiceToDeliveryNote(id);
	}

	public List<InvoiceVo> listInvoicesByCustomer(Long customerId) {
		return dao.listInvoicesByCustomer(customerId);
	}
}
