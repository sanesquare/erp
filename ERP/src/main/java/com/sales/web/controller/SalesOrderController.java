package com.sales.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.erp.web.service.DeliveryMethodService;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.service.VatService;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.service.GodownService;
import com.inventory.web.service.ProductService;
import com.inventory.web.vo.DeliveryNoteProductVo;
import com.inventory.web.vo.DeliveryNoteVo;
import com.sales.web.constants.SalesPageNameConstants;
import com.sales.web.constants.SalesRequestConstants;
import com.sales.web.service.CustomerService;
import com.sales.web.service.SalesOrderService;
import com.sales.web.vo.SalesOrderProductVo;
import com.sales.web.vo.SalesOrderVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class SalesOrderController {

	@Autowired
	private SalesOrderService salesOrderService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private ProductService productService;

	@Autowired
	private VatService taxService;

	@Autowired
	private PaymentTermsService paymentTermService;

	@Log
	private Logger logger;

	@Autowired
	private GodownService godownService;

	@Autowired
	private ErpCurrencyService erpCurrencyService;

	@Autowired
	private DeliveryMethodService deliveryMethodService;

	@RequestMapping(value = SalesRequestConstants.SALES_ORDERS_HOME, method = RequestMethod.GET)
	public String customerQuotesHomePageHandler(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			List<SalesOrderVo> quoteVos = salesOrderService.listSalesOrders(companyId);
			model.addAttribute("quotes", quoteVos);
		} catch (Exception ex) {
			logger.info("SALES_ORDERS_HOME: ", ex);
		}
		return SalesPageNameConstants.SALES_ORDERS_HOME;
	}

	@RequestMapping(value = SalesRequestConstants.SALES_ORDERS_LIST, method = RequestMethod.GET)
	public String customerQuotesList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			List<SalesOrderVo> quoteVos = salesOrderService.listSalesOrders(companyId);
			model.addAttribute("quotes", quoteVos);
		} catch (Exception ex) {
			logger.info("SALES_ORDERS_HOME: ", ex);
		}
		return SalesPageNameConstants.SALES_OREDER_LIST;
	}

	@RequestMapping(value = SalesRequestConstants.ADD_SALES_ORDER, method = RequestMethod.GET)
	public String addCustomerQuote(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId) {
		try {
			model.addAttribute("order", new SalesOrderVo());
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
			model.addAttribute("customers", customerService.findCustomersForCompany(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
			model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
		} catch (Exception ex) {
			logger.info("ADD_SALES_ORDER: ", ex);
		}
		return SalesPageNameConstants.ADD_SALES_ORDER;
	}

	@RequestMapping(value = SalesRequestConstants.SALES_ORDER_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveCustomerQuote(final Model model, @ModelAttribute("companyId") Long compnyId,
			@RequestBody SalesOrderVo quoteVo) {
		Long id = 0L;
		quoteVo.setCompanyId(compnyId);
		try {
			quoteVo.setIsQuote(false);
			id = salesOrderService.saveOrUpdateSalesOrder(quoteVo);
		} catch (Exception e) {
			logger.info("SALES_ORDER_SAVE: ", e);
		}
		return id;
	}

	@RequestMapping(value = SalesRequestConstants.SALES_ORDER_VIEW, method = RequestMethod.GET)
	public String viewCustomerQuote(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			SalesOrderVo quoteVo = salesOrderService.findSalesOrderById(id);
			model.addAttribute("order", quoteVo);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", quoteVo.getOrderDate());
			if (!quoteVo.getIsView()) {
				model.addAttribute("customers", customerService.findCustomersForCompany(companyId));
				model.addAttribute("products", productService.findAllProducts(companyId));
				model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
				model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
				model.addAttribute("currencies",
						erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
				model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
			}
		} catch (Exception ex) {
			logger.info("SALES_ORDER_VIEW: ", ex);
		}
		return SalesPageNameConstants.ADD_SALES_ORDER;
	}

	@RequestMapping(value = SalesRequestConstants.SALES_ORDER_DELETE, method = RequestMethod.GET)
	public @ResponseBody ModelAndView deleteCustomerQuote(final Model model,
			@RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = null;
		try {
			salesOrderService.deleteSalesOrder(id);
			modelAndView = new ModelAndView(new RedirectView(
					SalesRequestConstants.SALES_ORDERS_LIST + "?msg=Sales Order deleted successfully"));
		} catch (Exception e) {
			modelAndView = new ModelAndView(
					new RedirectView(SalesRequestConstants.SALES_ORDERS_LIST + "?msg=Sales Order delete failed"));
			logger.info("SALES_ORDER_DELETE: ", e);
		}
		return modelAndView;
	}

	@RequestMapping(value = SalesRequestConstants.CONVERT_SALES_ORDER_TO_DELIVERY_NOTE, method = RequestMethod.GET)
	public String convertSalesOrderToDeliveryNote(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			SalesOrderVo orderVo = salesOrderService.findSalesOrderById(id);
			DeliveryNoteVo noteVo = new DeliveryNoteVo();
			model.addAttribute("note", this.createNoteVo(orderVo, noteVo));
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", noteVo.getDate());
			model.addAttribute("customers", customerService.findCustomersForCompany(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
			model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
		} catch (Exception e) {
			logger.info("RECEIVING_NOTE_VIEW- ", e);
		}
		return InventoryPageNameConstants.DELIVERY_NOTES_ADD;
	}

	/**
	 * method to create delivery note vo
	 * 
	 * @param orderVo
	 * @param noteVo
	 * @return
	 */
	private DeliveryNoteVo createNoteVo(SalesOrderVo noteVo, DeliveryNoteVo vo) {
		vo.setCompanyId(noteVo.getCompanyId());
		vo.setOrderCode(noteVo.getOrderCode());
		vo.setOrderId(noteVo.getOrderId());
		vo.setCustomer(noteVo.getCustomer());
		vo.setCustomerId(noteVo.getCustomerId());
		vo.setDate(noteVo.getOrderDate());
		vo.setPaymentTerm(noteVo.getPaymentTerm());
		vo.setPaymentTermId(noteVo.getPaymentTermId());
		vo.setDeliveryTerm(noteVo.getDeliveryTerm());
		vo.setDeliveryTermId(noteVo.getDeliveryTermId());
		vo.setErpCurrencyId(noteVo.getErpCurrencyId());
		vo.setCurrency(noteVo.getCurrency());
		vo.setSubTotal(noteVo.getSubTotal());
		vo.setDiscountRate(noteVo.getDiscountRate());
		vo.setDiscountTotal(noteVo.getDiscountTotal());
		vo.setNetTotal(noteVo.getNetTotal());
		vo.setTaxTotal(noteVo.getTaxTotal());
		vo.setGrandTotal(noteVo.getGrandTotal());
		vo.setTaxes(noteVo.getTaxes());
		vo.setTaxesWithRate(noteVo.getTaxesWithRate());
		vo = this.creatProductVo(vo, noteVo);
		return vo;
	}

	private DeliveryNoteVo creatProductVo(DeliveryNoteVo vo, SalesOrderVo orderVo) {
		List<DeliveryNoteProductVo> productVos = new ArrayList<DeliveryNoteProductVo>();
		for (SalesOrderProductVo items : orderVo.getProductVos()) {
			BigDecimal toBeDelivered = BigDecimal.ZERO;
			if(items.getDeliveredQuantity()!=null)
			{
				toBeDelivered = items.getQuantity().subtract(items.getDeliveredQuantity());
			}else{
				toBeDelivered = items.getQuantity();
			}
			DeliveryNoteProductVo productVo = new DeliveryNoteProductVo();
			productVo.setQuantityToDeliver(toBeDelivered);
			productVo.setAmountExcludingTax(items.getAmountExcludingTax());
			vo.setNetAmountHidden(vo.getNetAmountHidden().add(items.getAmountExcludingTax()));
			productVo.setProductCode(items.getProductCode());
			productVo.setProductName(items.getProductName());
			productVo.setProductId(items.getProductId());
			productVo.setQuantity(toBeDelivered);
			productVo.setUnitPrice(items.getUnitPrice());
			productVo.setDiscount(items.getDiscount());
			productVo.setNetAmount(items.getNetAmount());
			productVo.setTax(items.getTax());
			productVos.add(productVo);
		}
		vo.setProductsTaxAmount(orderVo.getProductsTaxAmount());
		vo.setProductVos(productVos);
		vo.setIsFromInvoice(true);
		return vo;
	}
}
