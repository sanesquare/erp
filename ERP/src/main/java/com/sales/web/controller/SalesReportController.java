package com.sales.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.erp.web.constants.ErpConstants;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.logger.Log;
import com.sales.web.constants.SalesReportConstants;
import com.sales.web.constants.SalesRequestConstants;
import com.sales.web.service.CustomerService;
import com.sales.web.service.InvoiceService;
import com.sales.web.service.SalesOrderService;
import com.sales.web.vo.CustomerVo;
import com.sales.web.vo.InvoiceItemsPdfVo;
import com.sales.web.vo.InvoiceProductVo;
import com.sales.web.vo.InvoiceVo;
import com.sales.web.vo.SalesOrderProductVo;
import com.sales.web.vo.SalesOrderVo;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 2, 2015
 */
@Controller
public class SalesReportController {

	@Log
	private Logger logger;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private SalesOrderService salesOrderService;
	
	@Autowired
	private ServletContext context;

	@RequestMapping(value = SalesRequestConstants.GENERATE_INVOICE_PDF, method = RequestMethod.GET)
	public String generateInvoice(final Model model, @RequestParam(value = "iid", required = true) Long id) {
		try {
			InvoiceVo invoiceVo = invoiceService.findInvoiceObject(id);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LOGO", context.getRealPath(ErpConstants.LOGO_PATH));
			params.put("DATE", invoiceVo.getInvoiceDate());
			params.put("INVOICE", invoiceVo.getInvoiceCode());
			params.put("CUSTOMER", invoiceVo.getCustomer());
			params.put("subTotal", invoiceVo.getSubTotal());
			params.put("discountRate", invoiceVo.getDiscountRate());
			params.put("discountTotal", invoiceVo.getDiscountTotal());
			params.put("netTotal",invoiceVo.getNetTotal());
			params.put("taxTotal", invoiceVo.getTaxTotal());
			params.put("grandTotal", invoiceVo.getGrandTotal());
			params.put("PAYMENT_TERM", invoiceVo.getPaymentTerm());
			params.put("DELIVERY_TERM", invoiceVo.getDeliveryTerm());
			params.put("CURRENCY", invoiceVo.getCurrecny());
			CustomerVo customerVo = customerService.findCustomerById(invoiceVo.getCustomerId());
			params.put("CUSTOMER_ADDRESS", customerVo.getAddress());
			CompanyVo company = companyService.findCompanyById(invoiceVo.getCompanyId());
			if (company != null)
				params.put("COMPANY", company.getParent());
			params.put("datasource", new JREmptyDataSource());
			
			List<String> taxes = invoiceVo.getTaxes();
			if(taxes!=null)
				params.put("TAXES", new JRBeanCollectionDataSource(taxes));
			List<InvoiceProductVo> products = invoiceVo.getProductVos();
			if (products != null)
				params.put("ITEMS", new JRBeanCollectionDataSource(products));
			model.addAllAttributes(params);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return SalesReportConstants.INVOICE_REPORT;
	}
	
	@RequestMapping(value = SalesRequestConstants.GENERATE_SALES_ORDER_PDF, method = RequestMethod.GET)
	public String generateSalesOrder(final Model model, @RequestParam(value = "id", required = true) Long id) {
		try {
			SalesOrderVo invoiceVo = salesOrderService.findSalesOrderById(id);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LOGO", context.getRealPath(ErpConstants.LOGO_PATH));
			params.put("DATE", invoiceVo.getOrderDate());
			params.put("INVOICE", invoiceVo.getOrderCode());
			params.put("CUSTOMER", invoiceVo.getCustomer());
			params.put("subTotal", invoiceVo.getSubTotal());
			params.put("discountRate", invoiceVo.getDiscountRate());
			params.put("discountTotal", invoiceVo.getDiscountTotal());
			params.put("netTotal",invoiceVo.getNetTotal());
			params.put("taxTotal", invoiceVo.getTaxTotal());
			params.put("grandTotal", invoiceVo.getGrandTotal());
			params.put("PAYMENT_TERM", invoiceVo.getPaymentTerm());
			params.put("DELIVERY_TERM", invoiceVo.getDeliveryTerm());
			params.put("CURRENCY", invoiceVo.getCurrency());
			CustomerVo customerVo = customerService.findCustomerById(invoiceVo.getCustomerId());
			params.put("CUSTOMER_ADDRESS", customerVo.getAddress());
			CompanyVo company = companyService.findCompanyById(invoiceVo.getCompanyId());
			if (company != null)
				params.put("COMPANY", company.getParent());
			params.put("datasource", new JREmptyDataSource());
			
			List<String> taxes = invoiceVo.getTaxes();
			if(taxes!=null)
				params.put("TAXES", new JRBeanCollectionDataSource(taxes));
			List<SalesOrderProductVo> products = invoiceVo.getProductVos();
			if (products != null)
				params.put("ITEMS", new JRBeanCollectionDataSource(products));
			model.addAllAttributes(params);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return SalesReportConstants.INVOICE_REPORT;
	}
	
	@RequestMapping(value = SalesRequestConstants.GENERATE_CUSTOMER_QUOTE_PDF, method = RequestMethod.GET)
	public String generateCustomerQuotePdf(final Model model, @RequestParam(value = "id", required = true) Long id) {
		try {
			SalesOrderVo invoiceVo = salesOrderService.findSalesOrderById(id);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LOGO", context.getRealPath(ErpConstants.LOGO_PATH));
			params.put("DATE", invoiceVo.getOrderDate());
			params.put("INVOICE", invoiceVo.getQuoteCode());
			params.put("CUSTOMER", invoiceVo.getCustomer());
			params.put("subTotal", invoiceVo.getSubTotal());
			params.put("discountRate", invoiceVo.getDiscountRate());
			params.put("discountTotal", invoiceVo.getDiscountTotal());
			params.put("netTotal",invoiceVo.getNetTotal());
			params.put("taxTotal", invoiceVo.getTaxTotal());
			params.put("grandTotal", invoiceVo.getGrandTotal());
			params.put("PAYMENT_TERM", invoiceVo.getPaymentTerm());
			params.put("DELIVERY_TERM", invoiceVo.getDeliveryTerm());
			params.put("CURRENCY", invoiceVo.getCurrency());
			CustomerVo customerVo = customerService.findCustomerById(invoiceVo.getCustomerId());
			params.put("CUSTOMER_ADDRESS", customerVo.getAddress());
			CompanyVo company = companyService.findCompanyById(invoiceVo.getCompanyId());
			if (company != null)
				params.put("COMPANY", company.getParent());
			params.put("datasource", new JREmptyDataSource());
			
			List<String> taxes = invoiceVo.getTaxes();
			if(taxes!=null)
				params.put("TAXES", new JRBeanCollectionDataSource(taxes));
			List<SalesOrderProductVo> products = invoiceVo.getProductVos();
			if (products != null)
				params.put("ITEMS", new JRBeanCollectionDataSource(products));
			model.addAllAttributes(params);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return SalesReportConstants.INVOICE_REPORT;
	}
}
