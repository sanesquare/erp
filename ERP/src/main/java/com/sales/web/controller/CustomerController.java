package com.sales.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.CountryService;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.sales.web.constants.SalesPageNameConstants;
import com.sales.web.constants.SalesRequestConstants;
import com.sales.web.service.CustomerService;
import com.sales.web.vo.CustomerContactVo;
import com.sales.web.vo.CustomerVo;

/**
 * 
 * @author Shamsheer & Vinutha 3-August-2015
 */
@Controller
@SessionAttributes({ "companyId" })
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CountryService countryService;

	/**
	 * method to reach customer home page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = SalesRequestConstants.CUSTOMER, method = RequestMethod.GET)
	public String customerHome(final Model model, @ModelAttribute("companyId") Long companyId) {
		model.addAttribute("customers", customerService.findCustomersForCompany(companyId));
		return SalesPageNameConstants.CUSTOMERS_PAGE;
	}

	/**
	 * method to reach customer add page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = SalesRequestConstants.CUSTOMER_ADD, method = RequestMethod.GET)
	public String customerAddPage(final Model model, @ModelAttribute CustomerVo customerVo) {
		try {
			// model.addAttribute("code",
			// customerService.generateCustomerCode());
			model.addAttribute("countries", countryService.findAllCountry());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SalesPageNameConstants.CUSTOMER_ADD_PAGE;
	}

	/**
	 * Method to handle save customer request
	 * 
	 * @param model
	 * @param customerVo
	 * @return
	 */
	@RequestMapping(value = SalesRequestConstants.SAVE_CUSTOMER, method = RequestMethod.POST)
	public String customerSaveRequestHandler(final Model model, @ModelAttribute CustomerVo customerVo,
			@ModelAttribute("companyId") Long companyId) {
		Long id = null;
		try {
			customerVo.setCompanyId(companyId);
			id = customerService.saveOrUpdateCustomer(customerVo);
			customerVo.setCustomerId(id);
			model.addAttribute(MessageConstants.SUCCESS, "Customer saved successfully");
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		if (id != null)
			return "redirect:" + SalesRequestConstants.EDIT_CUSTOMER + "?id=" + id;
		return SalesPageNameConstants.CUSTOMERS_PAGE;
	}

	/**
	 * Method to handle request for edit customer
	 * 
	 * @param model
	 * @param id
	 * @param request
	 * @return
	 */
	@RequestMapping(value = SalesRequestConstants.EDIT_CUSTOMER, method = RequestMethod.GET)
	public String customerEditHandler(final Model model, @RequestParam(value = "id") Long id,
			HttpServletRequest request) {
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String errorMessage = request.getParameter(MessageConstants.ERROR);
		try {
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
			model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("customerVo", customerService.findCustomerById(id));
			model.addAttribute("countries", countryService.findAllCountry());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return SalesPageNameConstants.CUSTOMER_ADD_PAGE;
	}

	/**
	 * Method to handle request for delete customer
	 * 
	 * @param model
	 * @param id
	 * @param request
	 * @return
	 */
	@RequestMapping(value = SalesRequestConstants.DELETE_CUSTOMER, method = RequestMethod.GET)
	public @ResponseBody String customerDeleteRequestHandler(final Model model, @RequestParam(value = "id") Long id,
			HttpServletRequest request) {
		try {
			customerService.deleteCustomerById(id);
			model.addAttribute("customers", customerService.findAllCustomers());
		} catch (ItemNotFoundException ex) {
			ex.printStackTrace();
			return ex.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return "ok";
	}

	/**
	 * Method to handle request for
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = SalesRequestConstants.CUSTOMER_LIST, method = RequestMethod.GET)
	public String customerList(final Model model) {
		try {
			model.addAttribute("customers", customerService.findAllCustomers());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SalesPageNameConstants.CUSTOMER_LIST_TEMP;
	}

	/**
	 * Method to save customer contact
	 * 
	 * @param contactVo
	 * @return
	 */
	@RequestMapping(value = SalesRequestConstants.CUSTOMER_CONTACT_SAVE, method = RequestMethod.POST)
	public @ResponseBody String saveOrUpdateCustomerContact(@RequestBody CustomerContactVo contactVo) {
		try {
			customerService.saveOrUpdateCustomerContact(contactVo);
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return "ok";
	}

	/**
	 * Method to get customer contacts
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = SalesRequestConstants.CUSTOMER_CONTACT_LIST, method = RequestMethod.GET)
	public String customerContactList(final Model model, @RequestParam(value = "id") Long id) {
		try {
			model.addAttribute("customerContacts", customerService.findContactsForCustomerById(id));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SalesPageNameConstants.CUSTOMER_CONTACTS_LIST;
	}

	/**
	 * Method to delete Customer Contact
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = SalesRequestConstants.DELETE_CONTACT, method = RequestMethod.GET)
	public @ResponseBody String deleteContact(final Model model, @RequestParam(value = "id", required = true) Long id) {
		try {
			customerService.deleteCustomerContactbyId(id);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "ok";
	}
}
