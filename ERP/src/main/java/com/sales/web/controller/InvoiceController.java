package com.sales.web.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.service.LedgerService;
import com.erp.web.service.ChargeService;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.service.VatService;
import com.erp.web.vo.PaymentTermInstallmentVo;
import com.erp.web.vo.PaymentTermsVo;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.constants.InventoryPageNameConstants;
import com.inventory.web.service.GodownService;
import com.inventory.web.service.ProductService;
import com.inventory.web.vo.DeliveryNoteVo;
import com.purchase.web.constants.PurchasePageNameConstants;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.purchase.web.vo.BillVo;
import com.sales.web.constants.SalesPageNameConstants;
import com.sales.web.constants.SalesRequestConstants;
import com.sales.web.service.CustomerService;
import com.sales.web.service.InvoiceService;
import com.sales.web.vo.InvoiceVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class InvoiceController {

	@Log
	private Logger logger;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private ProductService productService;

	@Autowired
	private VatService taxService;
	
	@Autowired
	private GodownService godownService;

	@Autowired
	private ChargeService chargeService;

	@Autowired
	private PaymentTermsService paymentTermService;

	@Autowired
	private ErpCurrencyService erpCurrencyService;

	@RequestMapping(value = SalesRequestConstants.INVOICE_HOME, method = RequestMethod.GET)
	public String invoiceHomePageHandler(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("invoice", invoiceService.listInvoicesForCompany(companyId));
		} catch (Exception ex) {
			logger.info("INVOICE_HOME: ", ex);
		}
		return SalesPageNameConstants.INVOICE;
	}

	@RequestMapping(value = SalesRequestConstants.INVOICE_ADD, method = RequestMethod.GET)
	public String addBillPageHandler(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId) {
		try {
			InvoiceVo billVo = new InvoiceVo();
			billVo.setIsEdit(false);
			model.addAttribute("invoice", billVo);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("fromDN", false);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
			model.addAttribute("customer", customerService.findCustomersForCompany(companyId));
			model.addAttribute("accounts", ledgerService.contraLedgers(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("charges", chargeService.findAllCharges(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
		} catch (Exception ex) {
			logger.info("INVOICE_ADD: ", ex);
		}
		return SalesPageNameConstants.INVOICE_ADD;
	}

	@RequestMapping(value = SalesRequestConstants.RECEIPT_INSTALLMENTS, method = RequestMethod.GET)
	public String getPaymentInstallments(@ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "amount", required = true) BigDecimal amount,
			@RequestParam(value = "paymentTermId", required = true) Long id, final Model model,
			@RequestParam(value = "date", required = true) String date) {
		try {
			PaymentTermsVo paymentTermsVo = paymentTermService.findPaymentTermById(id);
			List<PaymentTermInstallmentVo> installmentVos = new ArrayList<PaymentTermInstallmentVo>();
			Date billDate = DateFormatter.convertStringToDate(date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(billDate);
			Long i = 1L;
			for (PaymentTermInstallmentVo installmentVo : paymentTermsVo.getInstallmentVos()) {
				calendar.add(Calendar.DATE, installmentVo.getNofdays());
				Date installmentdate = calendar.getTime();
				installmentVo.setAmount(amount.multiply(installmentVo.getPercentage()).divide(new BigDecimal(100))
						.setScale(2, RoundingMode.HALF_EVEN));
				installmentVo.setDate(DateFormatter.convertDateToString(installmentdate));
				installmentVo.setId(i);
				i++;
				installmentVos.add(installmentVo);
			}
			model.addAttribute("paymentTerm", installmentVos);
			model.addAttribute("amount", amount);
		} catch (Exception e) {
			logger.info("RECEIPT_INSTALLMENTS: ", e);
		}
		return SalesPageNameConstants.RECEIPT_INSTALLMENTS;
	}

	@RequestMapping(value = SalesRequestConstants.INVOICE_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveBill(@ModelAttribute("companyId") Long companyId, @RequestBody InvoiceVo invoiceVo) {
		Long id = null;
		try {
			invoiceVo.setCompanyId(companyId);
			id = invoiceService.saveOrUpdateInvoice(invoiceVo);
		} catch (Exception e) {
			logger.info("INVOICE_SAVE:", e);
		}
		if (id == null)
			id = 0L;
		return id;
	}

	@RequestMapping(value = SalesRequestConstants.INVOICE_VIEW, method = RequestMethod.GET)
	public String viewBill(final Model model, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request, @ModelAttribute("companyId") Long companyId) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			InvoiceVo invoiceVo = invoiceService.findInvoiceObject(id);
			model.addAttribute("invoice", invoiceVo);
			if (!invoiceVo.getIsEdit()) {
				model.addAttribute("fromDN", false);
				model.addAttribute("currencyId", invoiceVo.getErpCurrencyId());
				model.addAttribute("date", invoiceVo.getInvoiceDate());
				model.addAttribute("customer", customerService.findCustomersForCompany(companyId));
				model.addAttribute("accounts", ledgerService.contraLedgers(companyId));
				model.addAttribute("products", productService.findAllProducts(companyId));
				model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
				model.addAttribute("charges", chargeService.findAllCharges(companyId));
				model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
				model.addAttribute("currencies",
						erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
			}
		} catch (Exception e) {
			logger.info("INVOICE_VIEW: ", e);
		}
		return SalesPageNameConstants.INVOICE_ADD;
	}

	@RequestMapping(value = SalesRequestConstants.INVOICE_LIST, method = RequestMethod.GET)
	public String billList(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			List<InvoiceVo> invoices = invoiceService.listInvoicesForCompany(companyId);
			model.addAttribute("invoice", invoices);
		} catch (Exception e) {
			logger.info("INVOICE_LIST : ", e);
		}
		return SalesPageNameConstants.INVOICE_LIST;
	}

	@RequestMapping(value = SalesRequestConstants.INVOICE_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deleteBill(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "id", required = true) Long id) {
		try {
			invoiceService.deleteInvoice(id);
		} catch (ItemNotFoundException ex) {
			return ex.getMessage();
		} catch (Exception e) {
			logger.info("INVOICE_DELETE : ", e);
			return "Could Not Delete Invoice";
		}
		return "Invoice Deleted Successfully";
	}

	@RequestMapping(value = SalesRequestConstants.PAY_INVOICE_INSTALLMENT, method = RequestMethod.GET)
	public @ResponseBody Boolean payBillInstallment(@RequestParam(value = "id", required = true) Long installmentId) {
		try {
			invoiceService.updateBillInstallmentPay(installmentId);
		} catch (Exception e) {
			logger.info("RECEIPT_INSTALLMENTS: ", e);
			return false;
		}
		return true;
	}

	@RequestMapping(value = SalesRequestConstants.CONVERT_INVOICE_TO_DELIVERY_NOTE, method = RequestMethod.GET)
	public String convertInvoiceToDeliveryNote(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "id", required = true) Long id, @ModelAttribute("currencyId") Long currencyId) {
		try {
			DeliveryNoteVo noteVo=invoiceService.convertINvoiceToDeliveryNote(id);
			model.addAttribute("note",noteVo );
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", noteVo.getDate());
			model.addAttribute("customers", customerService.findCustomersForCompany(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("godown", godownService.listAllGodownsForCompany(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
		} catch (Exception e) {
			logger.info("CONVERT_INVOICE_TO_DELIVERY_NOTE: ", e);
		}
		return InventoryPageNameConstants.DELIVERY_NOTES_ADD;
	}
}
