package com.sales.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sales.web.constants.SalesPageNameConstants;
import com.sales.web.constants.SalesRequestConstants;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
@Controller
public class SalesController {

	/**
	 * method to reach sales home page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = SalesRequestConstants.HOME , method = RequestMethod.GET)
	public String salesHome(final Model model){
		return SalesPageNameConstants.HOME_PAGE;
	}
	
}
