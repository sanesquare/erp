package com.sales.web.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.util.DateFormatter;
import com.purchase.web.constants.PurchaseRequestConstants;
import com.sales.web.constants.SalesPageNameConstants;
import com.sales.web.constants.SalesRequestConstants;
import com.sales.web.vo.SalesReturnVo;
import com.sales.web.service.CustomerService;
import com.sales.web.service.InvoiceService;
import com.sales.web.service.SalesReturnService;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 12, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class SalesReturnController {

	@Log
	private Logger logger;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private SalesReturnService salesReturnService;

	@RequestMapping(value = SalesRequestConstants.SALES_RETURN_HOME, method = RequestMethod.GET)
	public String salesReturnHomeHandler(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("returns", salesReturnService.findSalesReturns(companyId));
		} catch (Exception e) {
			logger.info("", e);
		}
		return SalesPageNameConstants.SALES_RETURN_HOME;
	}

	@RequestMapping(value = SalesRequestConstants.SALES_RETURN_LIST, method = RequestMethod.GET)
	public String salesReturnList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("returns", salesReturnService.findSalesReturns(companyId));
		} catch (Exception e) {
			logger.info("", e);
		}
		return SalesPageNameConstants.SALES_RETURN_LIST;
	}

	@RequestMapping(value = SalesRequestConstants.SALES_RETURN_ADD, method = RequestMethod.GET)
	public String addReturnHandler(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute SalesReturnVo salesReturnVo, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("showFilter", true);
			model.addAttribute("invoice", new SalesReturnVo());
			model.addAttribute("customer", customerService.findCustomersForCompany(companyId));
			model.addAttribute("invoices", invoiceService.listInvoicesForCompany(companyId));
		} catch (Exception e) {
			logger.info("", e);
		}
		return SalesPageNameConstants.SALES_RETURN_ADD;
	}

	@RequestMapping(value = SalesRequestConstants.RETURN_THIS_INVOICE, method = RequestMethod.POST)
	public String returnThisBill(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute SalesReturnVo salesReturnVo, @ModelAttribute("currencyId") Long currencyId) {
		try {
			model.addAttribute("showFilter", false);
			if (salesReturnVo.getInvoiceId() != null) {
				SalesReturnVo invoiceVo = salesReturnService.convertInvoiceToReturn(salesReturnVo.getInvoiceId());
				if (invoiceVo.getIsReturned())
					return "redirect:" + SalesRequestConstants.SALES_RETURN_ADD
							+ "?msg=Oops.. Selected Invoice Has Nothing to Return";
				model.addAttribute("invoice", invoiceVo);
				model.addAttribute("currencyId", currencyId);
				model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
			} else {
				return "redirect:" + SalesRequestConstants.SALES_RETURN_ADD + "?msg=Select Invoice";
			}
		} catch (Exception e) {
			logger.info("", e);
		}
		return SalesPageNameConstants.SALES_RETURN_ADD;
	}

	@RequestMapping(value = SalesRequestConstants.RETURN_THIS_INVOICE, method = RequestMethod.GET)
	public String returnThisBillget(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "invoiceId") Long invoiceId, @ModelAttribute("currencyId") Long currencyId) {
		try {
			model.addAttribute("showFilter", false);
			SalesReturnVo invoiceVo = salesReturnService.convertInvoiceToReturn(invoiceId);
			model.addAttribute("invoice", invoiceVo);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
		} catch (Exception e) {
			logger.info("", e);
		}
		return SalesPageNameConstants.SALES_RETURN_ADD;
	}

	@RequestMapping(value = SalesRequestConstants.SALES_RETURN_VIEW, method = RequestMethod.GET)
	public String viewSalesReturn(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "id") Long returnId, @ModelAttribute("currencyId") Long currencyId) {
		try {
			model.addAttribute("showFilter", false);
			model.addAttribute("invoice", salesReturnService.findSalesReturnById(returnId));
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
		} catch (Exception e) {
			logger.info("", e);
		}
		return SalesPageNameConstants.SALES_RETURN_ADD;
	}

	@RequestMapping(value = SalesRequestConstants.SALES_RETURN_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveSalesReturn(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestBody SalesReturnVo salesReturnVo) {
		Long id = 0L;
		try {
			id = salesReturnService.saveSalesReturn(salesReturnVo);
		} catch (Exception e) {
			logger.info("", e);
		}
		return id;
	}

	@RequestMapping(value = SalesRequestConstants.INVOICE_BY_CUSTOMER, method = RequestMethod.GET)
	public @ResponseBody JsonResponse filterInvoicesByCustomer(final Model model,
			@ModelAttribute("companyId") Long companyId, @RequestParam(value = "customerId") Long customerId) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (customerId != null)
				jsonResponse.setResult(invoiceService.listInvoicesByCustomer(customerId));
			else
				jsonResponse.setResult(invoiceService.listInvoicesForCompany(companyId));
			jsonResponse.setStatus("ok");
		} catch (Exception e) {
			jsonResponse.setStatus("error");
			logger.info("", e);
		}
		return jsonResponse;
	}

	@RequestMapping(value = SalesRequestConstants.SALES_RETURN_DELETE, method = RequestMethod.GET)
	public @ResponseBody ModelAndView deleteSalesReturn(@RequestParam(value = "id", required = true) Long id) {
		String url = SalesRequestConstants.SALES_RETURN_LIST;
		try {
			salesReturnService.deleteSalesReturn(id);
			url = url + "?msg=Sales Return Deleted Successfully.";
		} catch (ItemNotFoundException e) {
			url = url + "?msg=" + e.getMessage();
		} catch (Exception e) {
			logger.info("", e);
			url = url + "?msg=Sales Return Delete Failed.";
		}
		return new ModelAndView(new RedirectView(url));
	}
}
