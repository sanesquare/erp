package com.sales.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.service.LedgerService;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.service.ChargeService;
import com.erp.web.service.DeliveryMethodService;
import com.erp.web.service.ErpCurrencyService;
import com.erp.web.service.PaymentTermsService;
import com.erp.web.service.VatService;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.service.ProductService;
import com.sales.web.constants.SalesPageNameConstants;
import com.sales.web.constants.SalesRequestConstants;
import com.sales.web.service.CustomerService;
import com.sales.web.service.SalesOrderService;
import com.sales.web.vo.SalesOrderVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyId" })
public class CustomerQuoteController {

	@Autowired
	private SalesOrderService salesOrderService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private ProductService productService;

	@Autowired
	private VatService taxService;

	@Autowired
	private ChargeService chargeService;

	@Autowired
	private PaymentTermsService paymentTermService;

	@Log
	private Logger logger;

	@Autowired
	private ErpCurrencyService erpCurrencyService;

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private DeliveryMethodService deliveryMethodService;

	@RequestMapping(value = SalesRequestConstants.CUSTOMER_QUOTES_HOME, method = RequestMethod.GET)
	public String customerQuotesHomePageHandler(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			List<SalesOrderVo> quoteVos = salesOrderService.listCustomerQuotes(companyId);
			model.addAttribute("quotes", quoteVos);
		} catch (Exception ex) {
			logger.info("CUSTOMER_QUOTES_HOME: ", ex);
		}
		return SalesPageNameConstants.CUSTOMER_QUOTES_HOME;
	}

	@RequestMapping(value = SalesRequestConstants.CUSTOMER_QUOTES_LIST, method = RequestMethod.GET)
	public String customerQuotesList(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			List<SalesOrderVo> quoteVos = salesOrderService.listCustomerQuotes(companyId);
			model.addAttribute("quotes", quoteVos);
		} catch (Exception ex) {
			logger.info("CUSTOMER_QUOTES_HOME: ", ex);
		}
		return SalesPageNameConstants.CUSTOMER_QUOTE_TEMPLATE;
	}

	@RequestMapping(value = SalesRequestConstants.ADD_CUSTOMER_QUOTE, method = RequestMethod.GET)
	public String addCustomerQuote(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId) {
		try {
			SalesOrderVo orderVo = new SalesOrderVo();
			orderVo.setOrderDate(DateFormatter.convertDateToString(new Date()));
			orderVo.setExpiryDate(DateFormatter.convertDateToString(new Date()));
			model.addAttribute("quote", orderVo);
			model.addAttribute("currencyId", currencyId);
			model.addAttribute("customers", customerService.findCustomersForCompany(companyId));
			model.addAttribute("products", productService.findAllProducts(companyId));
			model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
			model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
			model.addAttribute("currencies",
					erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
			model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
		} catch (Exception ex) {
			logger.info("ADD_CUSTOMER_QUOTE: ", ex);
		}
		return SalesPageNameConstants.ADD_CUSTOMER_QUOTES;
	}

	@RequestMapping(value = SalesRequestConstants.CUSTOMER_QUOTE_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveCustomerQuote(final Model model, @ModelAttribute("companyId") Long compnyId,
			@RequestBody SalesOrderVo quoteVo) {
		Long id = 0L;
		quoteVo.setCompanyId(compnyId);
		try {
			quoteVo.setIsQuote(true);
			id = salesOrderService.saveOrUpdateSalesOrder(quoteVo);
		} catch (Exception e) {
			logger.info("CUSTOMER_QUOTE_SAVE: ", e);
		}
		return id;
	}

	@RequestMapping(value = SalesRequestConstants.CUSTOMER_QUOTE_VIEW, method = RequestMethod.GET)
	public String viewCustomerQuote(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("currencyId") Long currencyId, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			SalesOrderVo quoteVo = salesOrderService.findSalesOrderById(id);
			model.addAttribute("quote", quoteVo);
			model.addAttribute("currencyId", currencyId);
			if (!quoteVo.getIsView()) {
				model.addAttribute("customers", customerService.findCustomersForCompany(companyId));
				model.addAttribute("products", productService.findAllProducts(companyId));
				model.addAttribute("taxes", taxService.listAllTaxesForCompany(companyId));
				model.addAttribute("terms", paymentTermService.findAllPaymentTerms(companyId));
				model.addAttribute("currencies",
						erpCurrencyService.findAllCurrencies(DateFormatter.convertDateToString(new Date())));
				model.addAttribute("deliveryTerms", deliveryMethodService.listAllDeliveryMethods(companyId));
			}
		} catch (Exception ex) {
			logger.info("CUSTOMER_QUOTE_VIEW: ", ex);
		}
		return SalesPageNameConstants.ADD_CUSTOMER_QUOTES;
	}

	@RequestMapping(value = SalesRequestConstants.CONVERT_CUSTOMER_QUOTE_TO_ORDER, method = RequestMethod.GET)
	public @ResponseBody ModelAndView convertCustomerQuoteToOrder(final Model model,
			@RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = null;
		try {
			salesOrderService.updateCustomerQuoteToSalesOrder(id);
			modelAndView = new ModelAndView(new RedirectView(
					SalesRequestConstants.CUSTOMER_QUOTES_LIST + "?msg=Customer quote converted successfully"));
		} catch (Exception e) {
			modelAndView = new ModelAndView(new RedirectView(
					SalesRequestConstants.CUSTOMER_QUOTES_LIST + "?msg=Customer quote convertion failed"));
			logger.info("CONVERT_CUSTOMER_QUOTE_TO_ORDER: ", e);
		}
		return modelAndView;
	}

	@RequestMapping(value = SalesRequestConstants.CUSTOMER_QUOTE_DELETE, method = RequestMethod.GET)
	public @ResponseBody ModelAndView deleteCustomerQuote(final Model model,
			@RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = null;
		try {
			salesOrderService.deleteSalesOrder(id);
			modelAndView = new ModelAndView(new RedirectView(
					SalesRequestConstants.CUSTOMER_QUOTES_LIST + "?msg=Customer quote deleted successfully"));
		} catch (Exception e) {
			modelAndView = new ModelAndView(
					new RedirectView(SalesRequestConstants.CUSTOMER_QUOTES_LIST + "?msg=Customer quote delete failed"));
			logger.info("CUSTOMER_QUOTE_DELETE: ", e);
		}
		return modelAndView;
	}
}
