package com.sales.web.vo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-August-2015
 */
public class CustomerContactVo {

	private Long customerContactId;
	
	private String customerContactCode;
	
	private String name;
	
	private String email;
	
	private String phone;
	
	private Long customerId;
	
	private String address;

	/**
	 * @return the customerContactId
	 */
	public Long getCustomerContactId() {
		return customerContactId;
	}

	/**
	 * @param customerContactId the customerContactId to set
	 */
	public void setCustomerContactId(Long customerContactId) {
		this.customerContactId = customerContactId;
	}

	/**
	 * @return the customerContactCode
	 */
	public String getCustomerContactCode() {
		return customerContactCode;
	}

	/**
	 * @param customerContactCode the customerContactCode to set
	 */
	public void setCustomerContactCode(String customerContactCode) {
		this.customerContactCode = customerContactCode;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	
}
