package com.sales.web.vo;

import java.math.BigDecimal;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
public class InvoiceProductVo {

	private Long godownId;
	
	private Boolean isReturned=false;
	
	private String productName;

	private String productCode;

	private BigDecimal quantity = BigDecimal.ZERO;
	
	private BigDecimal deliveredQuantity = BigDecimal.ZERO;
	
	private BigDecimal returnedQuantity = BigDecimal.ZERO;
	
	private BigDecimal quantityToReturn = BigDecimal.ZERO;

	private BigDecimal unitPrice = BigDecimal.ZERO;

	private BigDecimal discount = BigDecimal.ZERO;

	private BigDecimal netAmount = BigDecimal.ZERO;

	private BigDecimal amountExcludingTax = BigDecimal.ZERO;

	private Long taxId;

	private String tax;

	private Long invoiceId;

	private Long invoiceProductId;

	private Long productId;

	public Long getInvoiceProductId() {
		return invoiceProductId;
	}

	public void setInvoiceProductId(Long invoiceProductId) {
		this.invoiceProductId = invoiceProductId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getAmountExcludingTax() {
		return amountExcludingTax;
	}

	public void setAmountExcludingTax(BigDecimal amountExcludingTax) {
		this.amountExcludingTax = amountExcludingTax;
	}

	public Long getTaxId() {
		return taxId;
	}

	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Long getGodownId() {
		return godownId;
	}

	public void setGodownId(Long godownId) {
		this.godownId = godownId;
	}

	public BigDecimal getDeliveredQuantity() {
		return deliveredQuantity;
	}

	public void setDeliveredQuantity(BigDecimal deliveredQuantity) {
		this.deliveredQuantity = deliveredQuantity;
	}

	public BigDecimal getReturnedQuantity() {
		return returnedQuantity;
	}

	public void setReturnedQuantity(BigDecimal returnedQuantity) {
		this.returnedQuantity = returnedQuantity;
	}

	public BigDecimal getQuantityToReturn() {
		return quantityToReturn;
	}

	public void setQuantityToReturn(BigDecimal quantityToReturn) {
		this.quantityToReturn = quantityToReturn;
	}

	public Boolean getIsReturned() {
		return isReturned;
	}

	public void setIsReturned(Boolean isReturned) {
		this.isReturned = isReturned;
	}
}
