package com.sales.web.vo;

import java.math.BigDecimal;

/**
 * @author Shamsheer & Vinutha
 * @since Dec 8, 2015
 */
public class SalesOrderChargeVo {

	private Long soChargeId;

	private Long orderId;

	private String charge;

	private BigDecimal amount = new BigDecimal("0.00");

	private Long chargeId;

	public Long getSoChargeId() {
		return soChargeId;
	}

	public void setSoChargeId(Long soChargeId) {
		this.soChargeId = soChargeId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getChargeId() {
		return chargeId;
	}

	public void setChargeId(Long chargeId) {
		this.chargeId = chargeId;
	}
}
