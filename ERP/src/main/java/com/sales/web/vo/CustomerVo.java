package com.sales.web.vo;

import java.util.ArrayList;
import java.util.List;

import com.purchase.web.vo.VendorContactVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-August-2015
 */
public class CustomerVo {

	private Long companyId;
	
	private Long customerId;
	
	private String customerCode;
	
	private String name;
	
	private String address;
	
	private String deliveryAddress;
	
	private String mobile;
	
	private String email;
	
	private String phone;

	private Boolean isCompany;
	
	private Boolean isLead;
	
	private Long countryId;
	
	private List<CustomerContactVo>contactVos = new ArrayList<CustomerContactVo>();
	
	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the customerCode
	 */
	public String getCustomerCode() {
		return customerCode;
	}

	/**
	 * @param customerCode the customerCode to set
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the deliveryAddress
	 */
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress the deliveryAddress to set
	 */
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the isCompany
	 */
	public Boolean getIsCompany() {
		return isCompany;
	}

	/**
	 * @param isCompany the isCompany to set
	 */
	public void setIsCompany(Boolean isCompany) {
		this.isCompany = isCompany;
	}

	/**
	 * @return the isLead
	 */
	public Boolean getIsLead() {
		return isLead;
	}

	/**
	 * @param isLead the isLead to set
	 */
	public void setIsLead(Boolean isLead) {
		this.isLead = isLead;
	}

	/**
	 * @return the countryId
	 */
	public Long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the contactVos
	 */
	public List<CustomerContactVo> getContactVos() {
		return contactVos;
	}

	/**
	 * @param contactVos the contactVos to set
	 */
	public void setContactVos(List<CustomerContactVo> contactVos) {
		this.contactVos = contactVos;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	
	
	
}
