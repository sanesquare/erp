package com.sales.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
public class SalesOrderVo {

	private Long noteId;

	private String orderCode;

	private String noteCode;

	private Boolean isQuote = true;

	private BigDecimal productsTaxAmount = new BigDecimal("0.00");

	private List<String> taxes = new ArrayList<String>();

	private List<String> taxesWithRate = new ArrayList<String>();

	private String customer;

	private String expiryDate;

	private BigDecimal productTaxTotal = new BigDecimal("0.00");

	private BigDecimal netAmountHidden = new BigDecimal("0.00");

	private List<Long> tsxIds = new ArrayList<Long>();

	private BigDecimal discountRate = new BigDecimal("0.00");

	private Boolean isView = false;

	private Long orderId;

	private Long companyId;

	private String quoteCode;

	private Long customerId;

	private String orderDate;

	private Long paymentTermId;

	private Long erpCurrencyId;

	private String paymentTerm;

	private String currency;

	private String status;

	private List<SalesOrderProductVo> productVos = new ArrayList<SalesOrderProductVo>();
	
	private List<SalesOrderChargeVo> chargeVos = new ArrayList<SalesOrderChargeVo>();

	private BigDecimal subTotal = new BigDecimal("0.00");

	private BigDecimal discountTotal = new BigDecimal("0.00");

	private BigDecimal netTotal = new BigDecimal("0.00");

	private BigDecimal taxTotal = new BigDecimal("0.00");

	private BigDecimal grandTotal = new BigDecimal("0.00");

	private Long deliveryTermId ;
	
	private String deliveryTerm;
	
	private Map<Long, String> deliveryNoteIdCode = new HashMap<Long, String>();

	private Boolean hasDelivered = false;

	public Long getNoteId() {
		return noteId;
	}

	public void setNoteId(Long noteId) {
		this.noteId = noteId;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getNoteCode() {
		return noteCode;
	}

	public void setNoteCode(String noteCode) {
		this.noteCode = noteCode;
	}

	public Boolean getIsQuote() {
		return isQuote;
	}

	public void setIsQuote(Boolean isQuote) {
		this.isQuote = isQuote;
	}

	public BigDecimal getProductsTaxAmount() {
		return productsTaxAmount;
	}

	public void setProductsTaxAmount(BigDecimal productsTaxAmount) {
		this.productsTaxAmount = productsTaxAmount;
	}

	public List<String> getTaxes() {
		return taxes;
	}

	public void setTaxes(List<String> taxes) {
		this.taxes = taxes;
	}

	public List<String> getTaxesWithRate() {
		return taxesWithRate;
	}

	public void setTaxesWithRate(List<String> taxesWithRate) {
		this.taxesWithRate = taxesWithRate;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getProductTaxTotal() {
		return productTaxTotal;
	}

	public void setProductTaxTotal(BigDecimal productTaxTotal) {
		this.productTaxTotal = productTaxTotal;
	}

	public BigDecimal getNetAmountHidden() {
		return netAmountHidden;
	}

	public void setNetAmountHidden(BigDecimal netAmountHidden) {
		this.netAmountHidden = netAmountHidden;
	}

	public List<Long> getTsxIds() {
		return tsxIds;
	}

	public void setTsxIds(List<Long> tsxIds) {
		this.tsxIds = tsxIds;
	}

	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	public Boolean getIsView() {
		return isView;
	}

	public void setIsView(Boolean isView) {
		this.isView = isView;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getQuoteCode() {
		return quoteCode;
	}

	public void setQuoteCode(String quoteCode) {
		this.quoteCode = quoteCode;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public Long getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(Long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public Long getErpCurrencyId() {
		return erpCurrencyId;
	}

	public void setErpCurrencyId(Long erpCurrencyId) {
		this.erpCurrencyId = erpCurrencyId;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SalesOrderProductVo> getProductVos() {
		return productVos;
	}

	public void setProductVos(List<SalesOrderProductVo> productVos) {
		this.productVos = productVos;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getDiscountTotal() {
		return discountTotal;
	}

	public void setDiscountTotal(BigDecimal discountTotal) {
		this.discountTotal = discountTotal;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public List<SalesOrderChargeVo> getChargeVos() {
		return chargeVos;
	}

	public void setChargeVos(List<SalesOrderChargeVo> chargeVos) {
		this.chargeVos = chargeVos;
	}

	public Long getDeliveryTermId() {
		return deliveryTermId;
	}

	public void setDeliveryTermId(Long deliveryTermId) {
		this.deliveryTermId = deliveryTermId;
	}

	public String getDeliveryTerm() {
		return deliveryTerm;
	}

	public void setDeliveryTerm(String deliveryTerm) {
		this.deliveryTerm = deliveryTerm;
	}

	public Map<Long, String> getDeliveryNoteIdCode() {
		return deliveryNoteIdCode;
	}

	public void setDeliveryNoteIdCode(Map<Long, String> deliveryNoteIdCode) {
		this.deliveryNoteIdCode = deliveryNoteIdCode;
	}

	public Boolean getHasDelivered() {
		return hasDelivered;
	}

	public void setHasDelivered(Boolean hasDelivered) {
		this.hasDelivered = hasDelivered;
	}
}
