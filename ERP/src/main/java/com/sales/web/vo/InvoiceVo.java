package com.sales.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.purchase.web.vo.BillChargeVo;
import com.purchase.web.vo.BillInstallmentVo;
import com.purchase.web.vo.BillProductVo;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
public class InvoiceVo {

	private Long deliveryNoteId;

	private Boolean isDelivered;

	private Boolean isReturned = false;

	private Boolean isPacked = false;

	private List<Long> deliveryNoteIds = new ArrayList<Long>();

	private BigDecimal productsTaxAmount = new BigDecimal("0.00");

	private BigDecimal outStanding = new BigDecimal("0.00");

	private String account;

	private List<String> taxes = new ArrayList<String>();

	private List<String> taxesWithRate = new ArrayList<String>();

	private String customer;

	private BigDecimal productTaxTotal = new BigDecimal("0.00");

	private BigDecimal netAmountHidden = new BigDecimal("0.00");

	private List<Long> tsxIds = new ArrayList<Long>();

	private BigDecimal discountRate = new BigDecimal("0.00");

	private Long ledgerId;

	private Boolean isEdit;

	private Long invoiceId;

	private Long companyId;

	private String invoiceCode;

	private Long customerId;

	private String invoiceDate;

	private Long paymentTermId;

	private Long erpCurrencyId;

	private String paymentTerm;

	private String currecny;
	
	private String currency;

	private String status;

	private Boolean isSettled;

	private List<InvoiceProductVo> productVos = new ArrayList<InvoiceProductVo>();

	private List<InvoiceChargeVo> chargeVos = new ArrayList<InvoiceChargeVo>();

	private BigDecimal subTotal = new BigDecimal("0.00");

	private BigDecimal discountTotal = new BigDecimal("0.00");

	private BigDecimal netTotal = new BigDecimal("0.00");

	private BigDecimal taxTotal = new BigDecimal("0.00");

	private BigDecimal grandTotal = new BigDecimal("0.00");

	private List<InvoiceInstallmentVo> installmentVos = new ArrayList<InvoiceInstallmentVo>();

	private Long deliveryTermId;

	private String deliveryTerm;

	public Long getDeliveryNoteId() {
		return deliveryNoteId;
	}

	public void setDeliveryNoteId(Long deliveryNoteId) {
		this.deliveryNoteId = deliveryNoteId;
	}

	public BigDecimal getProductsTaxAmount() {
		return productsTaxAmount;
	}

	public void setProductsTaxAmount(BigDecimal productsTaxAmount) {
		this.productsTaxAmount = productsTaxAmount;
	}

	public BigDecimal getOutStanding() {
		return outStanding;
	}

	public void setOutStanding(BigDecimal outStanding) {
		this.outStanding = outStanding;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public List<String> getTaxes() {
		return taxes;
	}

	public void setTaxes(List<String> taxes) {
		this.taxes = taxes;
	}

	public List<String> getTaxesWithRate() {
		return taxesWithRate;
	}

	public void setTaxesWithRate(List<String> taxesWithRate) {
		this.taxesWithRate = taxesWithRate;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public BigDecimal getProductTaxTotal() {
		return productTaxTotal;
	}

	public void setProductTaxTotal(BigDecimal productTaxTotal) {
		this.productTaxTotal = productTaxTotal;
	}

	public BigDecimal getNetAmountHidden() {
		return netAmountHidden;
	}

	public void setNetAmountHidden(BigDecimal netAmountHidden) {
		this.netAmountHidden = netAmountHidden;
	}

	public List<Long> getTsxIds() {
		return tsxIds;
	}

	public void setTsxIds(List<Long> tsxIds) {
		this.tsxIds = tsxIds;
	}

	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	public Long getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(Long ledgerId) {
		this.ledgerId = ledgerId;
	}

	public Boolean getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Long getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(Long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public Long getErpCurrencyId() {
		return erpCurrencyId;
	}

	public void setErpCurrencyId(Long erpCurrencyId) {
		this.erpCurrencyId = erpCurrencyId;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getCurrecny() {
		return currecny;
	}

	public void setCurrecny(String currecny) {
		this.currecny = currecny;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getIsSettled() {
		return isSettled;
	}

	public void setIsSettled(Boolean isSettled) {
		this.isSettled = isSettled;
	}

	public List<InvoiceProductVo> getProductVos() {
		return productVos;
	}

	public void setProductVos(List<InvoiceProductVo> productVos) {
		this.productVos = productVos;
	}

	public List<InvoiceChargeVo> getChargeVos() {
		return chargeVos;
	}

	public void setChargeVos(List<InvoiceChargeVo> chargeVos) {
		this.chargeVos = chargeVos;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getDiscountTotal() {
		return discountTotal;
	}

	public void setDiscountTotal(BigDecimal discountTotal) {
		this.discountTotal = discountTotal;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public List<InvoiceInstallmentVo> getInstallmentVos() {
		return installmentVos;
	}

	public void setInstallmentVos(List<InvoiceInstallmentVo> installmentVos) {
		this.installmentVos = installmentVos;
	}

	public List<Long> getDeliveryNoteIds() {
		return deliveryNoteIds;
	}

	public void setDeliveryNoteIds(List<Long> deliveryNoteIds) {
		this.deliveryNoteIds = deliveryNoteIds;
	}

	public Boolean getIsDelivered() {
		return isDelivered;
	}

	public void setIsDelivered(Boolean isDelivered) {
		this.isDelivered = isDelivered;
	}

	public Boolean getIsReturned() {
		return isReturned;
	}

	public void setIsReturned(Boolean isReturned) {
		this.isReturned = isReturned;
	}

	public Boolean getIsPacked() {
		return isPacked;
	}

	public void setIsPacked(Boolean isPacked) {
		this.isPacked = isPacked;
	}

	public Long getDeliveryTermId() {
		return deliveryTermId;
	}

	public void setDeliveryTermId(Long deliveryTermId) {
		this.deliveryTermId = deliveryTermId;
	}

	public String getDeliveryTerm() {
		return deliveryTerm;
	}

	public void setDeliveryTerm(String deliveryTerm) {
		this.deliveryTerm = deliveryTerm;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
