package com.sales.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 6, 2015
 */
public class SalesReturnItemsVo {

	private Long returnId;

	private String productCode;

	private String productName;

	private BigDecimal quantity = new BigDecimal("0.00");

	private BigDecimal quantityToReturn = new BigDecimal("0.00");

	private BigDecimal unitPrice = new BigDecimal("0.00");

	private BigDecimal total = new BigDecimal("0.00");

	private Long godownId;

	private String godown;

	private Long itemId;

	public Long getReturnId() {
		return returnId;
	}

	public void setReturnId(Long returnId) {
		this.returnId = returnId;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Long getGodownId() {
		return godownId;
	}

	public void setGodownId(Long godownId) {
		this.godownId = godownId;
	}

	public String getGodown() {
		return godown;
	}

	public void setGodown(String godown) {
		this.godown = godown;
	}

	public BigDecimal getQuantityToReturn() {
		return quantityToReturn;
	}

	public void setQuantityToReturn(BigDecimal quantityToReturn) {
		this.quantityToReturn = quantityToReturn;
	}
}
