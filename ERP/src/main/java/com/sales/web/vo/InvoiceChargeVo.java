package com.sales.web.vo;

import java.math.BigDecimal;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
public class InvoiceChargeVo {

	private Long invoiceChargeId;

	private Long invoiceId;

	private String charge;

	private BigDecimal amount = new BigDecimal("0.00");

	private Long chargeId;

	public Long getInvoiceChargeId() {
		return invoiceChargeId;
	}

	public void setInvoiceChargeId(Long invoiceChargeId) {
		this.invoiceChargeId = invoiceChargeId;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getChargeId() {
		return chargeId;
	}

	public void setChargeId(Long chargeId) {
		this.chargeId = chargeId;
	}
}
