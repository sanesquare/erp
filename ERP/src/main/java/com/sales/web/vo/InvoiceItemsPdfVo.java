package com.sales.web.vo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 3, 2015
 */
public class InvoiceItemsPdfVo {

	private Integer slNo;
	
	private String name;

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
