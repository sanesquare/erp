package com.sales.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 6, 2015
 */
public class SalesReturnVo {

	private Long invoiceId;
	
	private String code;
	
	private Boolean isReturned = false;
	
	private Long returnId;
	
	private Long customerId;
	
	private String date;
	
	private String customer;
	
	private String account;
	
	private Boolean isSettled;
	
	private String referenceNumber;
	
	private String currecny;
	
	private List<SalesReturnItemsVo> productVos = new ArrayList<SalesReturnItemsVo>();

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getIsReturned() {
		return isReturned;
	}

	public void setIsReturned(Boolean isReturned) {
		this.isReturned = isReturned;
	}

	public Long getReturnId() {
		return returnId;
	}

	public void setReturnId(Long returnId) {
		this.returnId = returnId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Boolean getIsSettled() {
		return isSettled;
	}

	public void setIsSettled(Boolean isSettled) {
		this.isSettled = isSettled;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getCurrecny() {
		return currecny;
	}

	public void setCurrecny(String currecny) {
		this.currecny = currecny;
	}

	public List<SalesReturnItemsVo> getProductVos() {
		return productVos;
	}

	public void setProductVos(List<SalesReturnItemsVo> productVos) {
		this.productVos = productVos;
	}

}
