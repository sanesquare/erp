package com.sales.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Ledger;
import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.DeliveryNote;
import com.inventory.web.entities.PackingList;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-August-2015
 *
 */
@Entity
@Table(name = "invoice")
public class Invoice implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5434849527975391121L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Customer customer;

	@Column(name = "date")
	private Date date;

	@ManyToOne
	private CustomerContact customerContact;

	@ManyToOne
	private PaymentTerms paymentTerms;

	@ManyToOne
	private DeliveryMethod deliveryTerm;

	@Column(name = "is_settled")
	private Boolean isSetteld;

	@Column(name = "notes", length = 1000)
	private String notes;

	@Column(name = "sub_total")
	private BigDecimal subTotal;

	@Column(name = "discount_percentage")
	private BigDecimal discountPercentage;

	@Column(name = "discount_amount")
	private BigDecimal discountAmount;

	@Column(name = "net_total")
	private BigDecimal netTotal;

	@Column(name = "net_amount")
	private BigDecimal netAmount;

	@Column(name = "tax_total")
	private BigDecimal taxTotal;

	@Column(name = "grand_total")
	private BigDecimal grandTotal;

	@ManyToOne
	private Ledger ledger;

	@ManyToOne
	private ErPCurrency currency;

	@ManyToMany
	private Set<VAT> vat = new HashSet<VAT>(0);

	@Column(name = "total")
	private BigDecimal total;

	@Column(name = "reference_number")
	private String referenceNumber;

	@OneToMany(mappedBy = "invoice")
	private Set<InvoiceProducts> invoiceProducts;

	@OneToMany(mappedBy = "invoice")
	private Set<CustomerPayment> customerPayments;

	@Column(name = "is_archive")
	private Boolean isArchive;

	@OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<InvoiceItems> invoiceItems = new HashSet<InvoiceItems>();

	@OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<InvoiceCharges> invoiceCharges = new HashSet<InvoiceCharges>();

	@OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<InvoiceInstallments> invoiceInstallments = new ArrayList<InvoiceInstallments>();

	@OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<SalesReturn> salesReturns = new HashSet<SalesReturn>();

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "invoice")
	private Journal journal;

	@ManyToOne
	private Company company;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@OneToMany(mappedBy = "invoice", cascade = CascadeType.PERSIST, orphanRemoval = false)
	private Set<DeliveryNote> deliveryNotes = new HashSet<DeliveryNote>();

	@OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<PackingList> packingLists = new HashSet<PackingList>();

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public CustomerContact getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(CustomerContact customerContact) {
		this.customerContact = customerContact;
	}

	public PaymentTerms getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(PaymentTerms paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public Boolean getIsSetteld() {
		return isSetteld;
	}

	public void setIsSetteld(Boolean isSetteld) {
		this.isSetteld = isSetteld;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(BigDecimal discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	public ErPCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(ErPCurrency currency) {
		this.currency = currency;
	}

	public Set<VAT> getVat() {
		return vat;
	}

	public void setVat(Set<VAT> vat) {
		this.vat = vat;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Set<InvoiceProducts> getInvoiceProducts() {
		return invoiceProducts;
	}

	public void setInvoiceProducts(Set<InvoiceProducts> invoiceProducts) {
		this.invoiceProducts = invoiceProducts;
	}

	public Set<CustomerPayment> getCustomerPayments() {
		return customerPayments;
	}

	public void setCustomerPayments(Set<CustomerPayment> customerPayments) {
		this.customerPayments = customerPayments;
	}

	public Boolean getIsArchive() {
		return isArchive;
	}

	public void setIsArchive(Boolean isArchive) {
		this.isArchive = isArchive;
	}

	public Set<InvoiceItems> getInvoiceItems() {
		return invoiceItems;
	}

	public void setInvoiceItems(Set<InvoiceItems> invoiceItems) {
		this.invoiceItems = invoiceItems;
	}

	public Set<InvoiceCharges> getInvoiceCharges() {
		return invoiceCharges;
	}

	public void setInvoiceCharges(Set<InvoiceCharges> invoiceCharges) {
		this.invoiceCharges = invoiceCharges;
	}

	public List<InvoiceInstallments> getInvoiceInstallments() {
		return invoiceInstallments;
	}

	public void setInvoiceInstallments(List<InvoiceInstallments> invoiceInstallments) {
		this.invoiceInstallments = invoiceInstallments;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}

	public Set<DeliveryNote> getDeliveryNotes() {
		return deliveryNotes;
	}

	public void setDeliveryNotes(Set<DeliveryNote> deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}

	public Set<SalesReturn> getSalesReturns() {
		return salesReturns;
	}

	public void setSalesReturns(Set<SalesReturn> salesReturns) {
		this.salesReturns = salesReturns;
	}

	public Set<PackingList> getPackingLists() {
		return packingLists;
	}

	public void setPackingLists(Set<PackingList> packingLists) {
		this.packingLists = packingLists;
	}

	public DeliveryMethod getDeliveryTerm() {
		return deliveryTerm;
	}

	public void setDeliveryTerm(DeliveryMethod deliveryTerm) {
		this.deliveryTerm = deliveryTerm;
	}
}
