package com.sales.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryStatus;
import com.erp.web.entities.InvoiceStatus;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-August-2015
 *
 */
@Entity
@Table(name = "customer_credit_memo")
public class CustomerCreditMemo implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 275852303909929120L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Customer customer;

	@Column(name = "description")
	private String description;

	@ManyToOne
	private CustomerContact customerContact;

	@ManyToOne
	private DeliveryStatus deliveryStatus;

	@ManyToOne
	private InvoiceStatus invoiceStatus;

	@Column(name = "sum")
	private BigDecimal sum;

	@Column(name = "discount_percentage")
	private BigDecimal discountPercentage;

	@Column(name = "discount_amount")
	private BigDecimal discountAmount;

	@Column(name = "vat_amount")
	private BigDecimal vatAmount;

	@Column(name = "total_including_vat")
	private BigDecimal totalIncludingVat;

	@OneToMany(mappedBy = "creditMemo")
	private Set<CustomerCreditMemoItems> items;

	@ManyToOne
	private Company company;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
