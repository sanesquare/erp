package com.sales.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.InventoryWithdrawal;
import com.inventory.web.entities.Product;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 30, 2015
 */
@Entity
@Table(name="invoice_items")
public class InvoiceItems implements AuditEntity,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3539568908565996358L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@ManyToOne
	private Invoice invoice;
	
	@ManyToOne
	private Product product;
	
	@Column(name = "quantity")
	private BigDecimal quantity;
	
	@Column(name = "delivered_quantity")
	private BigDecimal deliveredQuantity;
	
	@Column(name = "returned_quantity")
	private BigDecimal returnedQuantity;
	
	@Column(name = "packed_quantity")
	private BigDecimal packedQuantity;
	
	@Column(name="price")
	private BigDecimal price;
	
	@Column(name="discount_percentage")
	private BigDecimal discountPercentage;
	
	@Column(name="amount")
	private BigDecimal netAmount;
	
	@Column(name="amount_excluding_tax")
	private BigDecimal amountExcludingTax;
	
	@ManyToOne
	private VAT vat;
	
	@OneToOne(mappedBy="invoiceItems",cascade=CascadeType.ALL,orphanRemoval=true)
	private InventoryWithdrawal inventoryWithdrawal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(BigDecimal discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getAmountExcludingTax() {
		return amountExcludingTax;
	}

	public void setAmountExcludingTax(BigDecimal amountExcludingTax) {
		this.amountExcludingTax = amountExcludingTax;
	}

	public VAT getVat() {
		return vat;
	}

	public void setVat(VAT vat) {
		this.vat = vat;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public InventoryWithdrawal getInventoryWithdrawal() {
		return inventoryWithdrawal;
	}

	public void setInventoryWithdrawal(InventoryWithdrawal inventoryWithdrawal) {
		this.inventoryWithdrawal = inventoryWithdrawal;
	}

	public BigDecimal getDeliveredQuantity() {
		return deliveredQuantity;
	}

	public void setDeliveredQuantity(BigDecimal deliveredQuantity) {
		this.deliveredQuantity = deliveredQuantity;
	}

	public BigDecimal getReturnedQuantity() {
		return returnedQuantity;
	}

	public void setReturnedQuantity(BigDecimal returnedQuantity) {
		this.returnedQuantity = returnedQuantity;
	}

	public BigDecimal getPackedQuantity() {
		return packedQuantity;
	}

	public void setPackedQuantity(BigDecimal packedQuantity) {
		this.packedQuantity = packedQuantity;
	}
}
