package com.sales.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.erp.web.entities.DeliveryMethod;
import com.erp.web.entities.ErPCurrency;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.SalesOrderStatus;
import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.DeliveryNote;
import com.inventory.web.entities.PickingList;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 01-August-2015
 *
 */

@Entity
@Table(name = "sales_order")
public class SalesOrder implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3318361272779750107L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Customer customer;

	@ManyToOne
	private CustomerContact customerContact;

	@Column(name = "po_number")
	private String poNumber;

	@Column(name = "date")
	private Date date;

	@ManyToOne
	private SalesOrderStatus status;

	@Column(name = "delivery_date")
	private Date deliveryDate;

	@ManyToOne
	private PaymentTerms paymentTerms;

	@Column(name = "sub_total")
	private BigDecimal subTotal;

	@Column(name = "discount_percentage")
	private BigDecimal discountPercentage;

	@Column(name = "discount_amount")
	private BigDecimal discountAmount;

	@ManyToMany
	private Set<VAT> vat = new HashSet<VAT>();
	
	@OneToMany(mappedBy="order",cascade=CascadeType.ALL,orphanRemoval=true)
	private Set<SalesOrderCharges> charges = new LinkedHashSet<SalesOrderCharges>();

	@Column(name = "total")
	private BigDecimal total;

	@OneToMany(mappedBy = "salesOrder", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<SalesOrderItems> orderItems = new HashSet<SalesOrderItems>();

	@OneToMany(mappedBy = "salesOrder")
	private Set<PickingList> pickingLists;

	@Column(name = "expiration_date")
	private Date expirationDate;

	@ManyToOne
	private Company company;

	@ManyToOne
	private PaymentTerms paymentTerm;
	
	@ManyToOne
	private DeliveryMethod deliveryTerm;

	@Column(name = "net_total")
	private BigDecimal netTotal;

	@Column(name = "net_amount")
	private BigDecimal netAmount;

	@Column(name = "tax_total")
	private BigDecimal taxTotal;

	@ManyToOne
	private ErPCurrency currency;

	@Column(name = "is_quote")
	private Boolean isQuote;

	@Column(name = "quote_code")
	private String quoteCode;

	@Column(name = "order_code")
	private String orderCode;

	@Column(name = "note_code")
	private String noteCode;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@Column(name = "has_delivered")
	private Boolean hasDelivered;

	@OneToMany(mappedBy = "salesOrder", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<DeliveryNote> deliveryNote = new HashSet<DeliveryNote>();

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public CustomerContact getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(CustomerContact customerContact) {
		this.customerContact = customerContact;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public SalesOrderStatus getStatus() {
		return status;
	}

	public void setStatus(SalesOrderStatus status) {
		this.status = status;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public PaymentTerms getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(PaymentTerms paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(BigDecimal discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Set<VAT> getVat() {
		return vat;
	}

	public void setVat(Set<VAT> vat) {
		this.vat = vat;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Set<SalesOrderItems> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(Set<SalesOrderItems> orderItems) {
		this.orderItems = orderItems;
	}

	public Set<PickingList> getPickingLists() {
		return pickingLists;
	}

	public void setPickingLists(Set<PickingList> pickingLists) {
		this.pickingLists = pickingLists;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public PaymentTerms getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(PaymentTerms paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public ErPCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(ErPCurrency currency) {
		this.currency = currency;
	}

	public Boolean getIsQuote() {
		return isQuote;
	}

	public void setIsQuote(Boolean isQuote) {
		this.isQuote = isQuote;
	}

	public String getQuoteCode() {
		return quoteCode;
	}

	public void setQuoteCode(String quoteCode) {
		this.quoteCode = quoteCode;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getNoteCode() {
		return noteCode;
	}

	public void setNoteCode(String noteCode) {
		this.noteCode = noteCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<SalesOrderCharges> getCharges() {
		return charges;
	}

	public void setCharges(Set<SalesOrderCharges> charges) {
		this.charges = charges;
	}

	public DeliveryMethod getDeliveryTerm() {
		return deliveryTerm;
	}

	public void setDeliveryTerm(DeliveryMethod deliveryTerm) {
		this.deliveryTerm = deliveryTerm;
	}

	public Set<DeliveryNote> getDeliveryNote() {
		return deliveryNote;
	}

	public void setDeliveryNote(Set<DeliveryNote> deliveryNote) {
		this.deliveryNote = deliveryNote;
	}

	public Boolean getHasDelivered() {
		return hasDelivered;
	}

	public void setHasDelivered(Boolean hasDelivered) {
		this.hasDelivered = hasDelivered;
	}
}
