package com.sales.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.erp.web.entities.Godown;
import com.erp.web.entities.VAT;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.Product;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-July-2015
 *
 */
@Entity
@Table(name="sales_order_items")
public class SalesOrderItems implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8704829461229071946L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@ManyToOne
	private SalesOrder salesOrder;
	
	@Column(name="description" , length=1000)
	private String description;
	
	@Column(name="packing_quantity")
	private BigDecimal packingQuantity;
	
	@Column(name="quantity")
	private BigDecimal quantity;
	
	@Column(name="delivered_quantity")
	private BigDecimal deliveredQuantity ;
	
	@Column(name="price")
	private BigDecimal price;
	
	@Column(name="discount_rate")
	private BigDecimal dicscountRate;
	
	@Column(name="amount_excluding_tax")
	private BigDecimal amountExcludingTax;
	
	@ManyToOne
	private Product product;
	
	@Column(name="total")
	private BigDecimal total;
	
	@ManyToOne
	private VAT vat;
	
	@ManyToOne
	private Godown godown;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SalesOrder getSalesOrder() {
		return salesOrder;
	}

	public void setSalesOrder(SalesOrder salesOrder) {
		this.salesOrder = salesOrder;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPackingQuantity() {
		return packingQuantity;
	}

	public void setPackingQuantity(BigDecimal packingQuantity) {
		this.packingQuantity = packingQuantity;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDicscountRate() {
		return dicscountRate;
	}

	public void setDicscountRate(BigDecimal dicscountRate) {
		this.dicscountRate = dicscountRate;
	}

	public BigDecimal getAmountExcludingTax() {
		return amountExcludingTax;
	}

	public void setAmountExcludingTax(BigDecimal amountExcludingTax) {
		this.amountExcludingTax = amountExcludingTax;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public VAT getVat() {
		return vat;
	}

	public void setVat(VAT vat) {
		this.vat = vat;
	}

	public Godown getGodown() {
		return godown;
	}

	public void setGodown(Godown godown) {
		this.godown = godown;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getDeliveredQuantity() {
		return deliveredQuantity;
	}

	public void setDeliveredQuantity(BigDecimal deliveredQuantity) {
		this.deliveredQuantity = deliveredQuantity;
	}
}
