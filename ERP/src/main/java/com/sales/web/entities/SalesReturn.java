package com.sales.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Payment;
import com.erp.web.entities.Company;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Nov 12, 2015
 */
@Entity
@Table(name = "sales_return")
public class SalesReturn implements AuditEntity , Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4345816931822938867L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Customer customer;

	@Column(name = "return_date")
	private Date returnDate;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "shipping_date")
	private Date shippingDate;

	@Column(name = "reference_number")
	private String referenceNumber;

	@ManyToOne
	private CustomerContact customerContacts;

	@Column(name = "shipping_address", length = 1000)
	private String shippingAddress;

	@ManyToOne
	private Invoice invoice;

	@OneToMany(mappedBy = "salesReturn", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<SalesReturnItems> salesReturnItems = new HashSet<SalesReturnItems>();

	@OneToOne(cascade=CascadeType.ALL , orphanRemoval=true)
	private Payment payment;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private Journal journal;
	
	@ManyToOne
	private Company company;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(Date shippingDate) {
		this.shippingDate = shippingDate;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public CustomerContact getCustomerContacts() {
		return customerContacts;
	}

	public void setCustomerContacts(CustomerContact customerContacts) {
		this.customerContacts = customerContacts;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Set<SalesReturnItems> getSalesReturnItems() {
		return salesReturnItems;
	}

	public void setSalesReturnItems(Set<SalesReturnItems> salesReturnItems) {
		this.salesReturnItems = salesReturnItems;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}

}
