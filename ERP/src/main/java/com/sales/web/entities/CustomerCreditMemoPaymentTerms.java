package com.sales.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.erp.web.entities.PaymentMethod;
import com.erp.web.entities.PaymentTerms;
import com.erp.web.entities.PaymentTermsRules;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-August-2015
 *
 */
@Entity
@Table(name = "customer_credit_memo_payment_terms")
public class CustomerCreditMemoPaymentTerms implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3111757989660301162L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private PaymentTerms paymentTerms;
	
	@Column(name="installment_number")
	private Integer installmentNo;
	
	@Column(name="due_date")
	private Date dueDate;
	
	@Column(name="portion")
	private BigDecimal portion;
	
	@ManyToOne
	private PaymentMethod paymentMethod;
	
	@Column(name="no_of_days")
	private Long noOfDays;
	
	@Column(name="xDays")
	private Long xDays;
	
	@Column(name="total_amount")
	private BigDecimal totalAmount;
	
	@Column(name="received_amount")
	private BigDecimal receivedAmount;
	
	@Column(name="balance_due")
	private BigDecimal balanceDue;
	
	@ManyToOne
	private PaymentTermsRules rules;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
