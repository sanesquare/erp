package com.sales.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.inventory.web.entities.DeliveryNote;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 01-August-2015
 *
 */

@Entity
@Table(name = "customer_contact")
public class CustomerContact implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7104196476112707162L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Customer customer;

	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	@Column(name = "email")
	private String email;

	@Column(name = "work_phone")
	private String workPhone;

	@Column(name = "address")
	private String address;

	@OneToMany(mappedBy = "customerContact")
	private Set<SalesOrder> salesOrders;

	@OneToMany(mappedBy = "customerContact")
	private Set<Invoice> invoices;

	@OneToMany(mappedBy = "customerContact")
	private Set<CustomerCreditMemo> customerCreditMemos;

	@OneToMany(mappedBy = "customerContact")
	private Set<DeliveryNote> deliveryNotes;

	@OneToMany(mappedBy = "customerContacts")
	private Set<SalesReturn> salesReturns;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer
	 *            the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the workPhone
	 */
	public String getWorkPhone() {
		return workPhone;
	}

	/**
	 * @param workPhone
	 *            the workPhone to set
	 */
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the salesOrders
	 */
	public Set<SalesOrder> getSalesOrders() {
		return salesOrders;
	}

	/**
	 * @param salesOrders
	 *            the salesOrders to set
	 */
	public void setSalesOrders(Set<SalesOrder> salesOrders) {
		this.salesOrders = salesOrders;
	}

	/**
	 * @return the invoices
	 */
	public Set<Invoice> getInvoices() {
		return invoices;
	}

	/**
	 * @param invoices
	 *            the invoices to set
	 */
	public void setInvoices(Set<Invoice> invoices) {
		this.invoices = invoices;
	}

	/**
	 * @return the customerCreditMemos
	 */
	public Set<CustomerCreditMemo> getCustomerCreditMemos() {
		return customerCreditMemos;
	}

	/**
	 * @param customerCreditMemos
	 *            the customerCreditMemos to set
	 */
	public void setCustomerCreditMemos(Set<CustomerCreditMemo> customerCreditMemos) {
		this.customerCreditMemos = customerCreditMemos;
	}

	/**
	 * @return the deliveryNotes
	 */
	public Set<DeliveryNote> getDeliveryNotes() {
		return deliveryNotes;
	}

	/**
	 * @param deliveryNotes
	 *            the deliveryNotes to set
	 */
	public void setDeliveryNotes(Set<DeliveryNote> deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<SalesReturn> getSalesReturns() {
		return salesReturns;
	}

	public void setSalesReturns(Set<SalesReturn> salesReturns) {
		this.salesReturns = salesReturns;
	}

}
