package com.hrms.web.constants;
/**
 * 
 * @author Shamsheer
 * @since 13-May-2015
 */
public class AccountTypeConstants {

	public static final String SB = "SB";
	public static final String PF = "PF";
	public static final String ESI = "ESI";
}
