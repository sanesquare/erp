package com.hrms.web.constants;

/**
 * 
 * @author sangeeth 12/03/2015
 */
public class CommonConstants {

	public static final String SFTP_DOCUMENT_ROOT_PATH = "/home/2mftp/documents";
	public static final String SFTP_DOCUMENT_USER = "2mftp";
	public static final String SFTP_DOCUMENT_IP_ADDRESS = "128.199.249.169";
	public static final Integer SFTP_DOCUMENT_PORT = 22;
	public static final String SFTP_DOCUMENT_RSA = "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAqfpIstMyzx9wVP8cb/LX+cadzWWzZeKMQsdmbyDm5E0a6bXM\nhNYXpbOS81hu2CA2RPw4p3SmwMQyotXE3XFy0cqVHvblvWcxbbpPR/32EMEo+aRA\nNLjrWTmDmKAhZ6QN4EUscKl4Xe9robdR8Mb3DSWGNr7/qRJzwvY6J6g6CHaewl8f\niuQeH44cRcXp/EvDDNY1xAoRta+lN5zbcPTazLYr7uJcqnT9knBwsy3Ql4SwufUB\nQAkMJiYpKOQf1b0efjyvnQI1Vtlj+HMN7DGc1hnt9wgG9Y7Eq/+xdRCSNtEqhGb1\nV4ykZYAaZ+N1B3ZsRlgKDhUSl16lH+VzfsQGCQIDAQABAoIBAHe1+bVX4RTWg3qZ\nISIG2ezj4QdjdPFwmG//lzj05ygq1aV7kEqNraHZ0sjno0k7hn0XVZ86HKbuhur/\nuuJqn/74ugVQxYSbRMiOyn9dcQtjvsPy1fVLtxTj2LaJMH2ZoNwuW5oNAOTHVfpS\n7baxqhTsNJYh9lTQ1g6c82Ayrox+N65bZfZuRUeGyzqWxA+zoO2qrbdv9/wYMOrF\nYM5cJJVZszQcSlZMUO1JBiW93UlfaRhFde9rCswCWne306Xu/2+RCEbbS//nhtld\nn5jHslZa1DFOFv9oqPOXqhsjwOcijb+JwvE7Avuxbhnng1nJCt0btQTzRiiGHIF4\nunPjWfECgYEA4PvP5FRDd8z5wt/0P/D5U+0mUQz6nfDaC2TGae+tzmSyyr7NgjN5\nplpX4IwxIP0LR35b/fkMOR2rB63TlLoBDf9RxaRjJMchgADjAlFD5kKs9jL5zpKM\n/tJqlfIvw16SDNwcvyKNnRIq/pImR5StctR9QZXxWQ/N3Ho1KfLAP18CgYEAwWkv\nbpXNd14o9JnJ83y26MhYLMMkZg7M4FXUwnY5TJptsKs8mLE0UVS/vCc7JNYmVZms\nPEocP8Up1DJZfkZ4vuH+/ikkyKmF2Lx1IPqTIlehAE57McRhrekFLgr6CGaA+ucw\nWgra17jrDSyFLI3CtuXqY9TlY1bpNcIrCnKUe5cCgYEAr9oDJ6v5RDWR7xEPn77h\nkwjlDyr+5N6x/3/YWks95fntie1TQnarmiw5TckmSWugGlSY4M1+EVKwMQNcQn6U\nfnVXXA1edQu0kpGe3lvYn6zz9kfn29/Olwquxt8TGDjeHXEagwcl+2SM+IgRpZd/\nbybJdK8Xgw6IUNGwpkYYoC8CgYAwR/m3OV4ZNkAzD1ECn1dptNmOEwi7EWbDeamr\nAag8HzDSsWxnEZVl2Cok5gY1o9/d7oa4Zie3I5jVh9wfUahCvlfRSqQME4V+HU1/\n9g1UvVtLoEvQmJ1ptIm+fK79+dioyZ2gXKVUxKyolTyJarn4vlxMn5DDw6Zj7Ryh\ndwjp8QKBgQCE1m9C2FW0Rm9YfL8PznpDsl78rzjUeUvFrteTiGughLPJj4ISnWet\nx2+ZnhMui5no6CBnbz4XOnDhBTypuWimzVkK67izvBLB+sFWchIbjkJ3Tu83qTpi\nSFTjRxoka7uMvLy8ongzeyFRo0ms4pReii3htbm185sPxtrITVXkpw==\n-----END RSA PRIVATE KEY-----";

	public static final String SFTP_PATH = "http://2merp.cdn.snogol.net";
	public static final String SFTP_CDN_ROOT_PATH = "/home/2mftp/cdn";
	public static final String SFTP_CDN_USER = "2mftp";
	public static final String SFTP_CDN_IP_ADDRESS = "128.199.249.169";
	public static final Integer SFTP_CDN_PORT = 22;
	public static final String SFTP_CDN_RSA = "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAqfpIstMyzx9wVP8cb/LX+cadzWWzZeKMQsdmbyDm5E0a6bXM\nhNYXpbOS81hu2CA2RPw4p3SmwMQyotXE3XFy0cqVHvblvWcxbbpPR/32EMEo+aRA\nNLjrWTmDmKAhZ6QN4EUscKl4Xe9robdR8Mb3DSWGNr7/qRJzwvY6J6g6CHaewl8f\niuQeH44cRcXp/EvDDNY1xAoRta+lN5zbcPTazLYr7uJcqnT9knBwsy3Ql4SwufUB\nQAkMJiYpKOQf1b0efjyvnQI1Vtlj+HMN7DGc1hnt9wgG9Y7Eq/+xdRCSNtEqhGb1\nV4ykZYAaZ+N1B3ZsRlgKDhUSl16lH+VzfsQGCQIDAQABAoIBAHe1+bVX4RTWg3qZ\nISIG2ezj4QdjdPFwmG//lzj05ygq1aV7kEqNraHZ0sjno0k7hn0XVZ86HKbuhur/\nuuJqn/74ugVQxYSbRMiOyn9dcQtjvsPy1fVLtxTj2LaJMH2ZoNwuW5oNAOTHVfpS\n7baxqhTsNJYh9lTQ1g6c82Ayrox+N65bZfZuRUeGyzqWxA+zoO2qrbdv9/wYMOrF\nYM5cJJVZszQcSlZMUO1JBiW93UlfaRhFde9rCswCWne306Xu/2+RCEbbS//nhtld\nn5jHslZa1DFOFv9oqPOXqhsjwOcijb+JwvE7Avuxbhnng1nJCt0btQTzRiiGHIF4\nunPjWfECgYEA4PvP5FRDd8z5wt/0P/D5U+0mUQz6nfDaC2TGae+tzmSyyr7NgjN5\nplpX4IwxIP0LR35b/fkMOR2rB63TlLoBDf9RxaRjJMchgADjAlFD5kKs9jL5zpKM\n/tJqlfIvw16SDNwcvyKNnRIq/pImR5StctR9QZXxWQ/N3Ho1KfLAP18CgYEAwWkv\nbpXNd14o9JnJ83y26MhYLMMkZg7M4FXUwnY5TJptsKs8mLE0UVS/vCc7JNYmVZms\nPEocP8Up1DJZfkZ4vuH+/ikkyKmF2Lx1IPqTIlehAE57McRhrekFLgr6CGaA+ucw\nWgra17jrDSyFLI3CtuXqY9TlY1bpNcIrCnKUe5cCgYEAr9oDJ6v5RDWR7xEPn77h\nkwjlDyr+5N6x/3/YWks95fntie1TQnarmiw5TckmSWugGlSY4M1+EVKwMQNcQn6U\nfnVXXA1edQu0kpGe3lvYn6zz9kfn29/Olwquxt8TGDjeHXEagwcl+2SM+IgRpZd/\nbybJdK8Xgw6IUNGwpkYYoC8CgYAwR/m3OV4ZNkAzD1ECn1dptNmOEwi7EWbDeamr\nAag8HzDSsWxnEZVl2Cok5gY1o9/d7oa4Zie3I5jVh9wfUahCvlfRSqQME4V+HU1/\n9g1UvVtLoEvQmJ1ptIm+fK79+dioyZ2gXKVUxKyolTyJarn4vlxMn5DDw6Zj7Ryh\ndwjp8QKBgQCE1m9C2FW0Rm9YfL8PznpDsl78rzjUeUvFrteTiGughLPJj4ISnWet\nx2+ZnhMui5no6CBnbz4XOnDhBTypuWimzVkK67izvBLB+sFWchIbjkJ3Tu83qTpi\nSFTjRxoka7uMvLy8ongzeyFRo0ms4pReii3htbm185sPxtrITVXkpw==\n-----END RSA PRIVATE KEY-----";

	public static final String UNIQUE_TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
	public static final String[] MONTH_NAMES = { "January", "February", "March", "April", "May", "Jun", "July",
			"August", "September", "November", "December" };
	public static final String USER_ACTIVITY_LIST_FORMAT = "EEEE yyyy/MMM/dd 'at' hh:mma zzz";

	public static final String NOTIFICATION_SENDON_TYPE = "SEND_ON";

	public static final String TERMINATION_MAIL = "termination";
	public static final String MEETING_MAIL="meeting";
	public static final String INTERVIEW_INVITAION_MAIL = "Invitation For Interview";
	public static final String INTERVIEW_NOTIFICATION_MAIL = "Interview Details";
	public static final String OFFER_LETTER="Offer Letter";
	
	public static final String EMPLOYEE_CODE="EMP/2M/";

	public static final String PRESENT="Present";
	public static final String ABSENT="Absent";
	public static final String HALF_DAY="Half Day";
}
