package com.hrms.web.constants;
/**
 * 
 * @author Shamsheer
 * @since 05-May-2015
 */
public class PaySlipConstants {

	public static final String DEDUCTIONS="DEDUCTIONS";
	public static final String TAX="TAX";	
	public static final String BONUSES="BONUSES";
	public static final String COMMISSIONS="INCENTIVES";
	public static final String ADJUSTMENT_INCREMENT="ADJUSTMENTS (INCREMENT)";
	public static final String ADJUSTMENT_ENCASHMENT="ADJUSTMENTS (ENCASHMENT)";
	public static final String ADJUSTMENT_DECREMENT="ADJUSTMENTS (DECREMENT)";
	public static final String REIMBURSEMENTS="REIMBURSEMENTS";
	public static final String ESI="ESI";
	public static final String OVERTIME="OVERTIME";
	public static final String ADVANCE_SALARY="ADVANCE SALARY";
	public static final String LOAN="LOAN";
	public static final String LWF="LWF";
	public static final String PROVIDENT_FUND="PROVIDENT FUND";
	public static final String INSURANCE="INSURANCE";
	public static final String PROFESSIONAL_TAX="Professional Tax";
	public static final String PF_EMPLOYER="PF Employer Contribution";
	public static final String ESI_EMPLOYER="ESI Employer Contribution";
	public static final String LOP_COUNT="LOP Count";
	public static final String LOP_DEDUCTION="LOP Deduction";
	
	public static final String SALARY_TYPE_MONTHLY="Monthly";
	public static final String SALARY_TYPE_DAILY="Daily";
	public static final String SALARY_TYPE_HOURLY="Hourly";
}
