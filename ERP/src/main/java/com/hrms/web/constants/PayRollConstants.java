package com.hrms.web.constants;

/**
 * 
 * @author Jithin Mohan
 * @since 23-April-2015
 *
 */
public class PayRollConstants {

	public static final String GROSS_SALARY = "GROSS_SALARY";
	public static final String BASIC_SALARY = "BASIC_SALARY";
	public static final String DA = "DA";
	public static final String HRA = "HRA";
	public static final String CONVEYANCE = "CONVEYANCE";
	public static final String CCA = "CCA";
}
