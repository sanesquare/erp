package com.hrms.web.constants;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
public class RequestConstants {

	public static final String BASE_URL = "signin";
	public static final String SIGN_OUT_URL = "logout";
	public static final String HOME_URL = "hrmsDashboard.do";
	public static final String SETTINGS_URL = "hrmsSettings.do";
	public static final String CHANGE_PASSWORD = "changePassword.do";

	public static final String PROJECTS_URL = "projects.do";

	public static final String SAVE_PROJECT_URL = "saveProject.do";
	public static final String SAVE_PROJECT_INFO_URL = "saveProjectInfo.do";

	public static final String SALARY_CALCULATOR_URL = "salaryCalculator.do";
	public static final String SALARY_CALCULATOR_CALCULATE = "salaryCalculatorCalculate.do";

	public static final String GET_NOTIFICATIONS = "getNotifications.do";
	public static final String DELETE_NOTIFICATION = "deleteNotification.do";

	public static final String PAYSALARY_URL="paysalary.do";
	public static final String PAYSALARY_ADD_URL="addPaySalary.do";
	public static final String PAYSALARY_CALCULATE="calculatePaySalary.do";
	public static final String PAYSALARY_SAVE_HOURLY_SALARY="savePayHourly.do";
	public static final String PAYSALARY_SAVE_DAILY_SALARY="savePayDaily.do";
	public static final String PAYSALARY_EDIT_URL="editPaysalary.do";
	public static final String PAYSALARY_DELETE_URL="deletPaySalary.do";
	public static final String PAYSALARY_SAVE_MONTHLY="saveMontlhyPaysalary.do";
	public static final String PAYSALARY_SMS_GROUP="sendPaysalarySmsForGroup.do";
	public static final String PAYSALARY_SMS_INDIVIDUAL="sendPaysalarySmsForIndividual.do";
	
	public static final String SALARY_URL = "salary.do";
	public static final String SALARY_DELETE="deleteSalary.do";
	public static final String SALARY_ADD_URL="addSalary.do";
	public static final String SALARY_SAVE = "saveSalary.do";
	public static final String GET_PAYROLL_RESULT = "getPayrollResult.do";
	public static final String GET_EXTRA_PAYSLIP_ITEMS = "getExtraPayslipsItems.do";
	public static final String SAVE_MONTHLY_SALARY = "saveEmployeeMonthlySalary.do";
	public static final String GET_EXTRA_PAYSLIP_ITEM_CALCULATION = "getExtraPayslipItemCalculation.do";

	public static final String PAYSLIP_URL = "payslip.do";
	public static final String PAYSLIP_ADD_URL = "addPayslip.do";
	public static final String PAYSLIP_VIEW_URL = "viewPayslip.do";
	public static final String PAYSLIP_SAVE_URL = "savePayslip.do";
	public static final String PAYSLIP_EDIT_URL = "editPayslip.do";
	public static final String PAYSLIP_DELETE_URL = "deletePayslip.do";
	public static final String PAYSLIP_GET_DETAILS_BY_EMPLOYEE = "getPayslipDetailsByEmployee.do";
	public static final String UPDATE_PAYSLIP_STATUS = "updatePayslipStatus.do";

	public static final String ADD_PROJECTS_URL = "addProjects.do";
	public static final String UPDATE_PROJECT_DOCUMENT = "updateProjectDocument.do";
	public static final String UPDATE_PROJECT_INFORMATION = "updateProjectinformation.do";
	public static final String UPDATE_PROJECT_STATUS = "updateProjectStatus.do";
	public static final String EDIT_PROJECT_URL = "editProject.do";
	public static final String VIEW_PROJECT_DETAILS_URL = "viewProject.do";
	public static final String DELETE_PROJECT_URL = "deleteProject.do";
	public static final String UPDATE_PROJECT_URL = "updateProject.do";
	public static final String VIEW_PROJECT_DOCUMENT = "viewProjectDocument.do";
	public static final String DELETE_PROJECT_DOCUMENT = "deleteProjectDocument.do";
	public static final String READ_ALL_REMINDERS_JSON_URL = "readAllReminders.do";
	public static final String DELETE_REMINDER_URL = "DeleteReminder.do";
	
	public static final String VIEW_POLICY_DOCUMENT = "viewPolicyDocument.do";

	public static final String SETTINGS_SECOND_LEVELS_URL = "GetPayrollEmployees.do";
	public static final String SAVE_SECOND_LEVEL_FORWARDER_URL = "SaveSecondLevelForwarders.do";

	public static final String NEW_COUNTRY_URL = "NewCountry.do";
	public static final String DEPARTMENT_URL = "department.do";
	public static final String ADD_DEPARTMENT_URL = "add_department.do";
	public static final String SAVE_DEPARTMENT_URL = "saveDepartment.do";
	public static final String SAVE_DEPARTMENT_DOCUMENT_URL = "saveDepartmentDocument.do";

	public static final String VIEW_DEPARTMENT_URL = "view_department.do";
	public static final String EDIT_DEPARTMENT_URL = "editDepartment.do";
	public static final String DELETE_DEPARTMENT_URL = "deleteDepartment.do";
	public static final String UPDATE_DEPARTMENT_URL = "updateDepartment.do";
	public static final String VIEW_DEPARTMENT_EMPLOYEE_URL = "viewDepartment.do";
	public static final String DELETE_DEPARTMENT_DOCUMENT = "deleteDepartmentDocument.do";
	public static final String VIEW_DEPARTMENT_DOCUMENT = "viewDepartmentDocument.do";

	public static final String PROMOTION_URL = "promotion.do";
	public static final String ADD_PROMOTION_URL = "add_promotion.do";
	public static final String SAVE_PROMOTION_URL = "savePromotion.do";
	public static final String DELETE_PROMOTION_URL = "deletePromotion.do";
	public static final String EDIT_PROMOTION_URL = "editPromotion.do";
	public static final String UPDATE_PROMOTION_URL = "updatePromotion.do";
	public static final String GET_SUPERIORS_LIST_URL = "getEmployeeSuperiors.do";
	public static final String DELETE_PROMOTION_DOCUMENT = "deletePromotionDocument.do";
	public static final String VIEW_PROMOTION_DOCUMENT = "viewPromotionDocument.do";

	public static final String COMPLAINTS_URL = "complaints.do";
	public static final String ADD_COMPLAINTS_URL = "add_complaints.do";
	public static final String SAVE_COMPLAINTS_URL = "saveComplaints.do";
	public static final String DELETE_COMPLAINTS_URL = "deleteComplaints.do";
	public static final String EDIT_COMPLAINTS_URL = "editComplaints.do";
	public static final String UPDATE_COMPLAINTS_URL = "updateComplaints.do";
	public static final String DELETE_COMPLAINTS_DOCUMENT = "deleteComplaintsDocument.do";
	public static final String VIEW_COMPLAINTS_DOCUMENT = "viewComplaintsDocument.do";

	public static final String EMPLOYEE_HOME_URL = "employees.do";
	public static final String EMPLOYEE_ROLES = "employee_rules.do";
	public static final String SAVE_EMPLOYEE_ROLES = "saveEmployeeRoles.do";
	public static final String EMPLOYEE_ADD_NEW_URL = "addEmployee.do";
	public static final String EMPLOYEE_NEW_DESIGNATION = "empNewDesignation.do";
	public static final String EMPLOYEE_NEW_GRADE_ADD = "empNewGrade.do";
	public static final String EMPLOYEE_SAVE_INFO = "saveEmployeeInfo.do";
	public static final String EMPLOYEE_SAVE_USER_INFO = "empNewUser.do";
	public static final String EMPLOYEE_SAVE_PERSONAL_DETAILS = "empNewUserProfile.do";
	public static final String EMPLOYEE_SAVE_ADDITIONAL_INFO = "empAdditionalInfoSave.do";
	public static final String EMPLOYEE_SAVE_ADDRESS = "empAddressSave.do";
	public static final String EMPLOYEE_SAVE_PHONE = "empPhoneSave.do";
	public static final String EMPLOYEE_SAVE_NOTES = "empNotesSave.do";
	public static final String EMPLOYEE_EMERGENCY_CONTACT_SAVE = "empEmergencyContact.do";
	public static final String EMPLOYEE_QUALIFICATION_SAVE = "empQualfctionSave.do";
	public static final String EMPLOYEE_LANGUAGE_SAVE = "empLanguageSave.do";
	public static final String EMPLOYEE_EXPRNCE_SAVE = "empExprenceSave.do";
	public static final String EMPLOYEE_BANK_SAVE = "empBankAccount.do";
	public static final String EMPLOYEE_BENEFIT_SAVE = "empBeneftDetails.do";
	public static final String EMPLOYEE_NEXTOFKIN_SAVE = "empNextOfKIn.do";
	public static final String EMPLOYEE_DEPENDANTS_SAVE = "empDependants.do";
	public static final String EMPLOYEE_REFERENCE_SAVE = "emprrReferenceSave.do";
	public static final String EMPLOYEE_SKILL_SAVE = "emprrSkillSave.do";
	public static final String EMPLOYEE_UNIFORM_SAVE = "emprUnfrmSve.do";
	public static final String EMPLOYEE_ASSETS_SAVE = "emprAssetsSve.do";
	public static final String EMPLOYEE_DOC_UPLD = "uplodEmpDocs.do";
	public static final String ADD_NEW_RELEGION = "newRelgn.do";
	public static final String EMPLOYEE_STATUS_UPDATE = "empStatusUpdate.do";
	public static final String EMLOYEE_NOTIFY_EMAIL_UPDATE = "empNotifyByEmailUpdate.do";
	public static final String EMPLOYEE_SUPERIORS_SUBORDINATE_SAVE = "empSpriosSubdntsSave.do";
	public static final String VIEW_EMPLOYEE_DETAILS = "viewEmployee.do";
	public static final String EDIT_EMPLOYEE_DETAILS = "editEmployee.do";
	public static final String DELETE_EMPLOYEE_DETAILS = "deleteEmployee.do";
	public static final String DOWNLOAD_EMPLOYEE_DOCUMENT = "downloadEmpDoc.do";
	public static final String DELETE_EMPLOYEE_DOCUMENT = "deleteEmpDoc.do";
	public static final String SAVE_EMPLOYEE_LEAVE = "empLeaveSave.do";
	public static final String GET_EMPLOYEE_LEAVE = "empLeaveGet.do";
	public static final String GET_EMPLOYEE_QUALIFICATION = "empQualfctionGet.do";
	public static final String GET_EMPLOYEE_LANGUAGE = "empLanguageGet.do";
	public static final String GET_EMPLOYEE_ASSET = "emprAssetsGet.do";
	public static final String GET_EMPLOYEE_SKILL = "empSkillGet.do";
	public static final String GET_SUPERIOR_SUBORDINATE = "getSuperiorSubordinate.do";
	public static final String GET_EMPLOYEE_BY_CODE = "getEmployeeByCode.do";
	public static final String GET_EMPLOYEE_BANK_ACCOUNT = "getEmployeeBankAccount.do";
	public static final String EXPORT_TO_EXCEL_EMPLOYEES="exportToexcel.do";

	public static final String ADD_EMPLOYEE_JOINING = "addEmployeejoining.do";
	public static final String GET_EMPLOYEE_BY_BRANCH_AND_DEPT = "getEmployeeByBranchAndDepartment.do";
	public static final String GET_EMPLOYEE_DETAILS_BY_ID = "getEmployeeDetailsById.do";
	public static final String GET_EMPLOYEE_DETAILS_BY_ID_FOR_POP="getEmployeeDetailsForPopupById.do";
	public static final String SAVE_EMPLOYEE_JOINING = "saveEmployeeJoining.do";

	public static final String CONTRACT_URL = "contracts.do";
	public static final String CONTRACT_ADD_URL = "addContract.do";
	public static final String SAVE_CONTRACT = "saveContrct.do";
	public static final String UPLOAD_CONTRACT_DOCS = "uploadContrctDocs.do";
	public static final String EDIT_CONTRACT = "editContract.do";
	public static final String DELETE_CONTRACT = "deleteContract.do";
	public static final String VIEW_CONTRACT = "viewContract.do";
	public static final String DOWNLOAD_CONTRACT_DOC = "viewDocCOntrct.do";
	public static final String DELET_CONTRACT_DOC = "deleteDocCOntrct.do";

	public static final String ASSIGNMENTS_URL = "assignments.do";
	public static final String ADD_ASSIGNMENT_URL = "add_assignment.do";
	public static final String EDIT_ASSIGNMENT = "editAssignment.do";
	public static final String DELETE_ASSIGNMENT = "deleteAssignment.do";
	public static final String VIEW_ASSIGNMENT = "viewAssignment.do";
	public static final String SAVE_ASSIGNMENT = "saveAssignment.do";
	public static final String UPLOAD_ASSIGNMENT_DOC = "uploadAssignmntDoc.do";
	public static final String DELETE_ASSIGNMENT_DOC = "deleteAssignmentDoc.do";
	public static final String DOWNLOAD_ASSIGNMENT_DOC = "downloadAssignmentDoc.do";

	public static final String EMPOYEE_JOINING = "employeeJoining.do";
	public static final String UPLOAD_EMPLOYEE_JOINING_DOC = "uploadEmployeeJoinDocs.do";
	public static final String EDIT_JOINING_URL = "editJoining.do";
	public static final String DELETE_JOININ_URL = "deleteJoining.do";
	public static final String DOWNLOAD_JOINIG_DOCS = "viewJoinDoc.do";
	public static final String DELETE_JOINING_DOC = "deleteJoinDoc.do";
	public static final String VIEW_JOINING = "viewJoining.do";

	public static final String NEW_LANGUAGE_URL = "SaveLanguage.do";
	public static final String NEW_SKILL_URL = "SaveSkill.do";
	public static final String NEW_QUALIFICATION_URL = "SaveQualification.do";

	public static final String NEW_CONTRACT_TYPE_URL = "SaveContractType.do";

	public static final String NEW_JOB_TYPE_URL = "SaveJobType.do";

	public static final String VIEW_ANNOUNCEMENTS_DETAILS_URL = "viewAnnouncements.do";
	public static final String EDIT_ANNOUNCEMENTS_URL = "edit_announcement.do";
	public static final String SAVE_ANNOUNCEMENTS_URL = "saveAnnouncements.do";
	public static final String ANNOUNCEMENTS_URL = "announcements.do";
	public static final String ADD_ANNOUNCEMENTS_URL = "add_announcements.do";
	public static final String ADD_ANNOUNCEMENTS_STATUS_URL = "addAnnouncementsStatus.do";
	public static final String UPDATE_ANNOUNCEMENTS_URL = "updateAnnouncements.do";
	public static final String DELETE_ANNOUNCEMENTS_URL = "delete_announcement.do";

	public static final String NEW_JOB_FIELD_URL = "SaveJobField.do";
	public static final String NEW_BASE_CURRENCY_URL = "SaveBaseCurrency.do";
	public static final String NEW_CURRENCY_URL = "SaveCurrency.do";
	public static final String NEW_TIME_ZONE_URL = "SaveTimeZone.do";
	public static final String NEW_POLICY_TYPE_URL = "SavePolicyType.do";
	public static final String NEW_EMPLOYEE_TYPE_URL = "SaveEmployeeType.do";
	public static final String NEW_EMPLOYEE_CATEGORY_URL = "SaveEmployeeCategory.do";
	public static final String NEW_INSURANCE_TYPE_URL = "SaveInsuranceType.do";
	public static final String NEW_BRANCH_TYPE_URL = "SaveBranchType.do";
	public static final String NEW_TRAINING_TYPE_URL = "SaveTrainingType.do";
	public static final String NEW_TRAINING_EVENT_TYPE_URL = "SaveTrainingEventType.do";
	public static final String NEW_REIMBURSEMENT_CATEGORY_URL = "SaveReimbursementCategory.do";
	public static final String NEW_REMINDER_STATUS_URL = "SaveReminderStatus.do";
	public static final String NEW_REMINDERS_URL = "SaveReminder.do";
	public static final String NEW_ORGANISATION_URL = "SaveOrganisation.do";
	public static final String UPDATE_MANAGER_REPT_NOTIFY_URL = "UpdateManagerReportingNotification.do";
	public static final String FETCH_BASE_CURRENCY_URL = "readBaseCurrencyDetial.do";

	public static final String BRANCH_URL = "branches.do";
	public static final String ADD_BRANCH_URL = "addBranches.do";
	public static final String SAVE_BRANCH_URL = "saveBranch.do";
	public static final String VIEW_BRANCH_DETAILS_URL = "viewBranch.do";
	public static final String EDIT_BRANCH_URL = "editBranch.do";
	public static final String DELETE_BRANCH_URL = "deleteBranch.do";
	public static final String UPDATE_BRANCH_URL = "updateBranch.do";
	public static final String SAVE_BRANCH_INFO_URL = "saveBranchInfo.do";
	public static final String UPDATE_BRANCH_INFORMATION = "updateBranchInfo.do";
	public static final String VIEW_BRANCH_DOCUMENT = "viewBranchDocument.do";
	public static final String DELETE_BRANCH_DOCUMENT = "deleteBranchDocument.do";

	public static final String MEETINGS_URL = "meetings.do";
	public static final String ADD_MEETINGS_URL = "add_meeting.do";
	public static final String GET_DEPARTMENTS_LIST_URL = "getdepartments.do";
	public static final String GET_EMPLOYEE_BY_DEPARTMENTS = "getEmployeeesByDepartment.do";
	public static final String GET_EMPLOYEE_BY_BRANCHES = "getEmployeeesByBranch.do";
	public static final String GET_EMPLOYEE_BY_DEPART_AND_BRANCH = "getEmployeesByBranchAndDepartment.do";
	public static final String SAVE_MEETING = "saveMeeting.do";
	public static final String UPLOAD_MEETING_DOCS = "uploadMeetingsDocs.do";
	public static final String EDIT_MEETING = "editMeeting.do";
	public static final String DELETE_MEETING = "deleteMeeting.do";
	public static final String VIEW_MEETING = "viewMeeting.do";
	public static final String DOWNLOAD_MEETING_DOC = "downloadMeetingDoc.do";
	public static final String DELETE_MEETING_DOC = "deleteMeetingDoc.do";

	public static final String EMPLOYES_BY_BRANCH_ID="employeesByBranchId.do";
	
	public static final String NOTIFICATION_URL = "notificationList.do";
	public static final String ADD_NOTIFICATION_URL = "addNotification.do";
	public static final String SAVE_NOTIFICATION_URL = "saveNotification.do";
	public static final String DELETE_NOTIFICATION_URL = "deleteNotification.do";

	public static final String SAVE_BLOOD_GROUP_URL = "SaveBloodGroup.do";
	public static final String SAVE_RELEGION_URL = "SaveRelegion.do";
	public static final String SAVE_ACCOUNT_TYPE_URL = "SaveAccountType.do";
	public static final String SAVE_WORK_SHIFT_URL = "SaveWorkShift.do";
	public static final String SAVE_EMPLOYEE_STATUS_URL = "SaveEmployeeStatus.do";
	public static final String SAVE_ASSETS_URL = "SaveAsset.do";
	public static final String SAVE_TERMINATION_STATUS_URL = "SaveTerminationStatus.do";
	public static final String SAVE_PROMOTION_STATUS_URL = "SavePromotionStatus.do";

	public static final String SAVE_EMPLOYEE_EXIT_TYPE_URL = "SaveEmployeeExitType.do";
	public static final String SAVE_LEAVET_TYPE_URL = "SaveLaveType.do";
	public static final String SAVE_EMPLOYEE_GRADE_URL = "SaveEmployeeGrade.do";
	public static final String SAVE_EMPLOYEE_DESIGNATION_URL = "SaveEmployeeDesignation.do";

	public static final String SAVE_SHIFT_DAY_URL = "SaveShiftDay.do";
	public static final String SAVE_ADVANCE_SALARY_STATUS_URL = "SaveAdvanceSalaryStatus.do";
	public static final String SAVE_OVERTIME_STATUS_URL = "SaveOvertimeStatus.do";
	public static final String SAVE_LOAN_STATUS_URL = "SaveLoanStatus.do";

	public static final String LIST_ALL_REMINDER_URL = "ReminderList.do";

	public static final String USER_ACTIVITIES_URL = "userActivities.do";
	public static final String LIST_USER_ACTIVITIES_URL = "listUserActivities.do";

	public static final String EMPLOYEE_EXIT_LIST_URL = "EmployeeExit.do";
	public static final String EMPLOYEE_EXIT_SAVE_URL = "SaveEmployeeExit.do";
	public static final String EMPLOYEE_EXIT_CREATE_PAGE_URL = "AddEmployeeExit.do";

	public static final String RECRUITMENT_JOB_POSTS_URL = "jobPosts.do";
	public static final String RECRUITMENT_JOB_CANDIDATES_URL = "jobCandidates.do";
	public static final String RECRUITMENT_JOB_INTERVIEWS_URL = "jobInterviews.do";
	public static final String RECRUITMENT_ADD_JOB_POSTS_URL = "addJobPosts.do";
	public static final String SAVE_JOB_POST_URL = "saveJobPost.do";
	public static final String SAVE_JOB_POST_INFO_URL = "saveJobPostInfo.do";
	public static final String EDIT_POST_INFO_URL = "editJobPostInfo.do";
	public static final String UPDATE_JOB_POST_URL = "editJobPosts.do";
	public static final String DELETE_JOB_POST_URL = "deleteJobPosts.do";
	public static final String JOB_POST_SAVE_BASIC_URL = "saveJobPostBasicInfo.do";
	public static final String JOB_POST_SAVE_QUALIFICATION_URL = "saveJobPostQual.do";
	public static final String JOB_POST_SAVE_EXPERIENCE_URL = "saveJobPostExp.do";
	public static final String JOB_POST_SAVE_ADDITIONAL_URL = "savePostAdditionalInfo.do";
	public static final String JOB_POST_SAVE_DESCRIPTION_URL = "savePostDescription.do";
	public static final String JOB_POST_VIEW_URL = "viewJobPost.do";
	public static final String JOB_POST_UPLOAD_DOCS = "uploadPostDocs.do";
	public static final String JOB_POST_DELETE_DOCS = "deletePostDoc.do";
	public static final String JOB_POST_DWNLD_DOCS = "downloadPostDoc.do";

	public static final String ADD_JOB_CANDIDATES_URL = "addJobCandidates.do";
	public static final String SAVE_CANDIDATES_INFO_URL = "saveCandidateBasicInfo.do";
	public static final String SAVE_CANDIDATES_ACHIVE_URL = "saveCandidatesAchievement.do";
	public static final String SAVE_CANDIDATES_ADDINFO_URL = "saveCandidatesAddInfo.do";
	public static final String SAVE_CANDIATES_INT_URL = "saveCandidatesInterests.do";
	public static final String SAVE_CANDIDATES_CONTACT_URL = "saveCandidateContact.do";
	public static final String SAVE_CANDIDATES_QUALIFICATION_URL = "saveCandidateQual.do";
	public static final String SAVE_CANDIDATES_WORK_EXPERIENCE_URL = "saveCandidatesExperience.do";
	public static final String SAVE_CANDIDATES_LANGUAGE_URL = "saveCandidatesLanguage.do";
	public static final String SAVE_CANDIDATES_SKILLS_URL = "saveCandidatesSkills.do";
	public static final String SAVE_CANDIDATES_STATUS_URL = "saveCandidatesStatus.do";
	public static final String SAVE_CANDIDATES_REFERENCE_URL = "saveCandidatesReferences.do";
	public static final String JOB_CANDIDATE_VIEW_URL = "viewJobCandidate.do";
	public static final String JOB_CANDIDATE_EDIT_URL = "editJobCandidate.do";
	public static final String JOB_CANDIDATE_DELETE_URL = "deleteJobCandidate.do";
	public static final String JOB_CANDIDATE_UPLOAD_RESUME = "uploadCandResume.do";
	public static final String JOB_CANDIDATE_UPLOAD_DOCS = "uploadCandDocs.do";
	public static final String JOB_CANDIDATE_DWNLD_DOCS = "downloadCandDoc.do";
	public static final String JOB_CANDIDATE_DELETE_DOCS = "deleteCandDoc.do";
	public static final String JOB_CANDIDATE_DELETE_RESUME = "deleteCandResume.do";
	public static final String JOB_CANDIDATE_GET_QUALIFICATION = "getCandQualifications.do";
	public static final String JOB_CANDIDATE_GET_SKILLS = "getCandSkills.do";
	public static final String JOB_CANDIDATE_GET_LANGUAGE = "getCandLanguage.do";

	public static final String ADD_JOB_INTERVIEWS_URL = "addJobInterviews.do";
	public static final String JOB_INTERVIEW_SAVE_BASIC_INFO_URL = "saveIntBasicInfo.do";
	public static final String JOB_INTERVIEW_SAVE_ADD_INFO_URL = "saveAdditionalInfo.do";
	public static final String JOB_INTERVIEW_SAVEDESCRIPTION_URL = "saveDescription.do";
	public static final String JOB_INTERVIEW_VIEW_URL = "viewInterview.do";
	public static final String JOB_INTERVIEW_EDIT_URL = "editJobInterview.do";
	public static final String JOB_INTERVIEW_UPLOAD_DOCS = "uploadDocs.do";
	public static final String JOB_INTERVIEW_DWNLD_DOCS = "downloadIntDoc.do";
	public static final String JOB_INTERVIEW_DELETE_DOCS = "deleteIntDoc.do";
	public static final String JOB_INTERVIEW_DELETE_URL = "deleteJobInterview.do";

	public static final String EMPLOYEE_EXIT_EDIT_URL = "EditEmployeeExit.do";
	public static final String EMPLOYEE_EXIT_VIEW_URL = "ViewEmployeeExit.do";
	public static final String EMPLOYEE_EXIT_SAVE_DOC_URL = "SaveEmployeeExitDocument.do";
	public static final String EMPLOYEE_EXIT_GET_ALL_DOC_URL = "readThisEmployeeExitDocuments.do";
	public static final String EMPLOYEE_EXIT_DELETE_DOC_URL = "deleteThisEmployeeExitDocument.do";
	public static final String EMPLOYEE_EXIT_DELETE_URL = "deleteThisEmployeeExit.do";

	public static final String EMPLOYEE_TERMINATION_PAGE_URL = "Termination.do";
	public static final String EMPLOYEE_TERMINATION_CREATE_URL = "NewTermination.do";
	public static final String EMPLOYEE_TERMINATION_EDIT_URL = "EditTermination.do";
	public static final String EMPLOYEE_TERMINATION_VIEW_URL = "ViewTermination.do";
	public static final String EMPLOYEE_TERMINATION_DELETE_URL = "deleteThisTermination.do";
	public static final String ANNOUNCEMENT_DOC_SAVE_URL = "UploadAnnouncementDocument.do";

	public static final String GET_EMPLOYEE_SUPERIOR_LIST_URL = "getMySuperiors.do";

	public static final String EMPLOYEE_TERMINATION_DOC_SAVE_URL = "UploadTerminationDocument.do";
	public static final String EMPLOYEE_TERMINATION_DOC_LIST_URL = "readAllTerminationDocuments.do";
	public static final String EMPLOYEE_TERMINATION_DOC_DELETE_URL = "deleteThisTerminationDocument.do";

	public static final String WARNING_PAGE_URL = "Warnings.do";
	public static final String WARNING_CREATE_PAGE_URL = "CreateWarning.do";
	public static final String WARNING_LIST_PAGE_URL = "warningList.do";
	public static final String WARNING_DOC_LIST_PAGE_URL = "warningDocumentsList.do";
	public static final String WARNING_UPDATE_PAGE_URL = "UpdateWarning.do";
	public static final String WARNING_VIEW_PAGE_URL = "ViewWarning.do";
	public static final String WARNING_DELETE_PAGE_URL = "deleteThisWarning.do";
	public static final String WARNING_DOC_UPLOAD_URL = "UploadWarningDocument.do";
	public static final String WARNING_DOC_DELETE_URL = "warningDocumentDelete.do";

	public static final String ANNOUNCEMENT_DOC_LIST_PAGE_URL = "AnnouncementDocumentsList.do";
	public static final String ANNOUNCEMENT_DOC_DELETE_URL = "AnnouncementDocumentDelete.do";

	public static final String MEMO_PAGE_URL = "Memos.do";
	public static final String MEMO_INFORMATION_URL = "MemoInformation.do";
	public static final String MEMO_INFORMATION_SAVE_URL = "SaveMemoInformation.do";
	public static final String MEMO_INFORMATION_UPDATE_URL = "UpdateMemoInformation.do";
	public static final String MEMO_INFORMATION_VIEW_URL = "ViewMemoInformation.do";
	public static final String MEMO_INFORMATION_LIST_URL = "memoList.do";
	public static final String MEMO_INFORMATION_DELETE_URL = "deleteThisMemo.do";
	public static final String MEMO_DOCUEMENT_LIST_URL = "memoDocumentsList.do";
	public static final String MEMO_DOCUEMENT_UPLOAD_URL = "UploadMemoDocument.do";
	public static final String MEMO_DOCUEMENT_DELETE_URL = "memoDocumentDelete.do";

	public static final String HOLIDAY_PAGE_URL = "Holidays.do";
	public static final String CREATE_HOLIDAY_PAGE_URL = "CreateHoliday.do";
	public static final String VIEW_HOLIDAY_PAGE_URL = "ViewHoliday.do";
	public static final String SAVE_HOLIDAY_PAGE_URL = "SaveHoliday.do";
	public static final String UPDATE_HOLIDAY_PAGE_URL = "UpdateHoliday.do";
	public static final String DELETE_HOLIDAY_PAGE_URL = "DeleteHoliday.do";
	public static final String LIST_ALL_HOLIDAY_PAGE_URL = "HolidayList.do";

	public static final String WORK_SHIFT_PAGE_URL = "WorkShift.do";
	public static final String CREATE_WORK_SHIFT_PAGE_URL = "SaveWorkShift.do";
	public static final String WORK_SHIFT_SAVE_PAGE_URL = "saveWorkShiftDetails.do";
	public static final String GET_WORK_SHIFT_DETAILS_PAGE_URL = "GetEmployeeWorkShifts.do";

	public static final String ATTENDANCE_URL = "attendance.do";
	public static final String ADD_ATTENDANCE_URL = "add_attendance.do";
	public static final String SAVE_ATTENDANCE_URL = "saveAttendance.do";
	public static final String DELETE_ATTENDANCE = "deleteAttendance.do";
	public static final String EDIT_ATTENDANCE = "editAttendance.do";
	public static final String VIEW_ATTENDANCE = "viewAttendance.do";
	public static final String READ_ATTENDANCE_FROM_BIOMETRIC_REPORT="readAttendanceFromReport.do";

	public static final String LEAVE_URL = "leaves.do";
	public static final String LEAVE_ADD_URL = "add_leave.do";
	public static final String SAVE_LEAVE = "saveLeave.do";
	public static final String EDIT_LEAVE = "editLeave.do";
	public static final String DELETE_LEAVE = "deletLeave.do";
	public static final String VIEW_LEAVE = "viewLeave.do";
	public static final String UPLOAD_LEAV_DOCUMENT = "uploadLeaveDocument.do";
	public static final String DOWNLOAD_LEAVE_DOCUMENT = "downloadLeaveDocument.do";
	public static final String DELETE_LEAVE_DOCUMENT = "deleteLeaveDocument.do";
	public static final String SAVE_LEAVE_STATUS = "saveLeaveStatus.do";

	public static final String BONUS_URL = "bonuses.do";
	public static final String BONUS_VIEW_URL = "viewBonus.do";
	public static final String BONUS_EDIT_URL = "editBonus.do";
	public static final String BONUS_DELETE_URL = "deleteBonus.do";
	public static final String BONUS_ADD_URL = "addBonus.do";
	public static final String BONUS_SAVE_URL = "saveBonus.do";
	public static final String BONUS_TITLE_SAVE_URL = "saveBonusTitle.do";
	public static final String GET_BONUS_BY_EMPLOYEE_AND_TITLE = "getBonusByEmployeeAndTitle.do";
	public static final String GET_AVAILABLE_BONUS_TITLES = "getAllAvailableBonusTitles.do";
	public static final String GET_BONUS_AMOUNT = "getBonusAmountOnPeriod.do";

	public static final String DEDUCTION_URL = "deductions.do";
	public static final String DEDUCTION_VIEW_URL = "viewDeduction.do";
	public static final String DEDUCTION_EDIT_URL = "editDeduction.do";
	public static final String DEDUCTION_DELETE_URL = "deleteDeduction.do";
	public static final String DEDUCTION_ADD_URL = "addDeduction.do";
	public static final String DEDUCTION_SAVE_URL = "saveDeduction.do";
	public static final String DEDUCTION_TITLE_SAVE = "saveDeductionTitle.do";
	public static final String GET_DEDUCTION_BY_TITLE_AND_EMPLOYEE = "getDeductionByEmployeeAndTitle.do";
	public static final String GET_AVAILABLE_DEDUCTION_TITLES = "getAllAvailableDeductionTitles.do";

	public static final String COMMISSION_URL = "commissions.do";
	public static final String COMMISSION_VIEW_URL = "viewCommission.do";
	public static final String COMMISSION_EDIT_URL = "editCommission.do";
	public static final String COMMISSION_DELETE_URL = "deleteCommission.do";
	public static final String COMMISSION_ADD_URL = "addCommission.do";
	public static final String COMMISSION_SAVE_URL = "saveCommission.do";
	public static final String COMMISSION_TITLE_SAVE = "saveCommissionTitle.do";
	public static final String GET_COMMISSION_BY_TITLE_AND_EMPLOYEE = "getCommissionByEmployeeAndTitle.do";
	public static final String COMMISSION_UPDATE_STATUS = "updateCommissionStatus.do";
	public static final String GET_AVAILABLE_COMMISSION_TITLES = "getAllAvailableCommissionTitles.do";

	public static final String ADJUSTMENT_URL = "adjustments.do";
	public static final String ADJUSTMENT_VIEW_URL = "viewAdjustment.do";
	public static final String ADJUSTMENT_EDIT_URL = "editAdjustment.do";
	public static final String ADJUSTMENT_DELETE_URL = "deleteAdjustment.do";
	public static final String ADJUSTMENT_ADD_URL = "addAdjustment.do";
	public static final String ADJUSTMENT_SAVE_URL = "saveAdjustment.do";
	public static final String ADJUSTMENT_TITLE_SAVE = "saveAdjustmentTitle.do";
	public static final String GET_ADJUSTMENT_BY_TITLE_AND_EMPLOYEE = "getAdjustmentByEmployeeAndTitle.do";
	public static final String GET_AVAILABLE_ADJUSTMENT_TITLES = "getAllAvailableAdjustmentTitles.do";

	public static final String REIMBURSEMENTS_URL = "reimbursementsHome.do";
	public static final String REIMBURSEMENTS_VIEW_URL = "viewReimbursements.do";
	public static final String REIMBURSEMENTS_EDIT_URL = "editReimbursements.do";
	public static final String REIMBURSEMENTS_DELETE_URL = "deleteReimbursements.do";
	public static final String REIMBURSEMENTS_ADD_URL = "addReimbursements.do";
	public static final String REIMBURSEMENTS_SAVE_URL = "saveReimbursements.do";
	public static final String REIMBURSEMENTS_STATUS_UPDATE = "updateReimbursementsStatus.do";
	public static final String REIMBURSEMENTS_DOCUMENT_UPDATE = "updateReimbursementsDocument.do";
	public static final String DOWNLOAD_REIMBURSEMENT_DOCUMENT = "downloadReimbursementDocument.do";
	public static final String DELETE_REIMBURSEMENT_DOCUMENT = "deleteReimbursementDocument.do";

	public static final String DAILY_WAGES_URL = "dailyWages.do";
	public static final String DAILY_WAGES_ADD = "dailyWagesAdd.do";
	public static final String DAILY_WAGES_EDIT = "dailyWagesEdit.do";
	public static final String DAILY_WAGES_SAVE = "dailyWagesSave.do";
	public static final String DAILY_WAGES_DELETE = "dailyWagesDelete.do";
	public static final String DAILY_WAGES_VIEW = "dailyWagesView.do";
	public static final String DAILY_WAGES_GET_EMPLOYEE_WAGES = "dailyWagesGetEmployeeWages.do";

	public static final String HOURLY_WAGES_URL = "hourlyWages.do";
	public static final String HOURLY_WAGES_ADD = "hourlyWagesAdd.do";
	public static final String HOURLY_WAGES_EDIT = "hourlyWagesEdit.do";
	public static final String HOURLY_WAGES_SAVE = "hourlyWagesSave.do";
	public static final String HOURLY_WAGES_DELETE = "hourlyWagesDelete.do";
	public static final String HOURLY_WAGES_VIEW = "hourlyWagesView.do";
	public static final String HOURLY_WAGES_GET_EMPLOYEE_WAGES = "hourlyWagesGetEmployeeWages.do";

	public static final String ACHIEVEMENTS_URL = "achievements.do";
	public static final String ADD_ACHIEVEMENTS_URL = "add_achievements.do";

	public static final String ORGANIZATION_POLICY_URL = "OrganizationPolicy.do";
	public static final String ORGANIZATION_POLICY_LIST_URL = "OrganizationPolicyList.do";
	public static final String SAVE_ORGANIZATION_POLICY_URL = "AddOrganizationPolicy.do";
	public static final String VIEW_ORGANIZATION_POLICY_URL = "ViewOrganizationPolicy.do";
	public static final String EDIT_ORGANIZATION_POLICY_URL = "EditOrganizationPolicy.do";
	public static final String DELETE_ORGANIZATION_POLICY_URL = "DeleteOrganizationPolicy.do";
	public static final String ORGANIZATION_POLICY_DOC_SAVE_URL = "UploadOrgPolicyDocument.do";
	public static final String ORGANIZATION_POLICY_DOC_LIST_URL = "readAllOrgPolicyDocuments.do";
	public static final String ORGANIZATION_POLICY_DOC_DELETE_URL = "DeleteOrganizationPolicyDocument.do";

	public static final String INSURANCE_URL = "Insurance.do";
	public static final String ADD_INSURANCE_URL = "AddInsurance.do";
	public static final String DELETE_INSURANCE_URL = "deleteInsurance.do";
	public static final String LIST_ALL_INSURANCE_URL = "listAllInsurance.do";
	public static final String EDIT_INSURANCE_URL = "EditInsurance.do";
	public static final String VIEW_INSURANCE_URL = "ViewInsurance.do";
	public static final String GET_ALL_ORG_BRANCHES_URL = "getAllBranches.do";
	public static final String GET_ALL_ORG_BRANCH_DEPARTMENTS_URL = "getAllBranchDepartments.do";
	public static final String GET_ALL_ORG_BRANCH_DEPARTMENTS_EMPLOYEE_URL = "getAllBranchDepartmentsEmployee.do";
	public static final String SET_SELECTED_EMPLOYEE_TO_VIEW_URL = "setSelectedEmployeeToView.do";
	public static final String GET_EMPLOYEE_BASIC_INFO_POPUP_URL = "getEmployeePopupDetails.do";

	public static final String ESI_URL = "Esi.do";
	public static final String ADD_ESI_URL = "AddEsi.do";
	public static final String EDIT_ESI_URL = "EditEsi.do";
	public static final String VIEW_ESI_URL = "ViewEsi.do";
	public static final String LIST_ALL_ESI_URL = "listAllEsi.do";
	public static final String DELETE_ESI_URL = "deleteEsi.do";

	public static final String PAYROLL_PAYSLIP_ITEM_TAB_URL = "PaySlipItemTab.do";
	public static final String PAYROLL_TAXRULE_ITEM_TAB_URL = "TaxRuleItemTab.do";
	public static final String PAYROLL_PAYSLIP_ITEM_LIST_URL = "PaySlipItemList.do";
	public static final String PAYROLL_TAXRULE_ITEM_LIST_URL = "TaxRuleItemList.do";
	public static final String PAYROLL_EXTRA_PAYSLIP_ITEM_LIST_URL = "ExtraPaySlipItemList.do";
	public static final String BONUS_DATE_LIST_URL = "BonusDateList.do";
	public static final String PF_SYNTAX_LIST_URL = "PFSyntaxList.do";
	public static final String ESI_SYNTAX_LIST_URL = "ESISyntaxList.do";
	public static final String PAYROLL_PAYSLIP_ITEM_EMPTY_URL = "PaySlipItemEmpty.do";
	public static final String PAYROLL_STRUCTURE_URL = "PayrollStructure.do";
	public static final String PAYROLL_OPTIONS_SAVE_URL = "SavePayrollOption.do";
	public static final String PAYSLIP_ITEM_SAVE_URL = "SavePaySlipItem.do";
	public static final String TAXEXCLUDE_EMPLOYEES_SAVE_URL = "SaveTaxExcludeEmployees.do";
	public static final String EXTRA_PAYSLIP_ITEM_SAVE_URL = "SaveExtraPaySlipItem.do";
	public static final String BONUS_DATE_SAVE_URL = "SaveBonusDateInfo.do";
	public static final String PF_SYNTAX_SAVE_URL = "SavePFSyntaxInfo.do";
	public static final String ESI_SYNTAX_SAVE_URL = "SaveESISyntaxInfo.do";
	public static final String LWF_SAVE_URL = "SaveLWFInfo.do";
	public static final String PROFESIONAL_TAX_SAVE_URL = "SaveProfesionalTaxInfo.do";
	public static final String PAYSLIP_ITEM_UPDATE_URL = "UpdatePaySlipItem.do";
	public static final String EXTRA_PAYSLIP_ITEM_UPDATE_URL = "UpdateExtraPaySlipItem.do";
	public static final String GET_TAX_WITH_SALARY_TO_URL = "GetTaxWithSalaryTo.do";
	public static final String SAVE_TAX_RULE_ITEM_URL = "SaveTaxRuleItem.do";
	public static final String GET_EMPLOYEE_PF_SHARE_URL = "getMyProvidentFund.do";
	public static final String GET_EMPLOYEE_ESI_SHARE_URL = "getMyESI.do";

	public static final String OVERTIME_URL = "Overtime.do";
	public static final String ADD_OVERTIME_URL = "AddOvertime.do";
	public static final String EDIT_OVERTIME_URL = "EditOvertime.do";
	public static final String VIEW_OVERTIME_URL = "ViewOvertime.do";
	public static final String UPDATE_OVERTIME_STATUS_URL = "OvertimeStatusUpdate.do";
	public static final String LIST_ALL_OVERTIME_URL = "listAllOvertime.do";
	public static final String DELETE_OVERTIME_URL = "deleteOvertime.do";

	public static final String ADVANCE_SALARY_URL = "AdvanceSalary.do";
	public static final String ADD_ADVANCE_SALARY_URL = "AddAdvanceSalary.do";
	public static final String EDIT_ADVANCE_SALARY_URL = "EditAdvanceSalary.do";
	public static final String VIEW_ADVANCE_SALARY_URL = "ViewAdvanceSalary.do";
	public static final String UPDATE_ADVANCE_SALARY_STATUSURL = "AdvanceSalaryStatusUpdate.do";
	public static final String LIST_ALL_ADVANCE_SALARY_URL = "listAllAdvanceSalary.do";
	public static final String DELETE_ADVANCE_SALARY_URL = "deleteAdvanceSalary.do";

	public static final String PROFIDENT_FUND_URL = "ProfidentFunds.do";
	public static final String ADD_PROFIDENT_FUND_URL = "AddProfidentFund.do";
	public static final String VIEW_PROFIDENT_FUND_URL = "ViewProfidentFund.do";
	public static final String EDIT_PROFIDENT_FUND_URL = "EditProfidentFund.do";
	public static final String LIST_ALL_PROFIDENT_FUND_URL = "listAllProfidentFund.do";
	public static final String DELETE_PROFIDENT_FUND_URL = "deleteProfidentFund.do";

	public static final String LOAN_URL = "Loan.do";
	public static final String ADD_LOAN_URL = "AddLoan.do";
	public static final String VIEW_LOAN_URL = "ViewLoan.do";
	public static final String LOAN_LIST_PAGE_URL = "loanList.do";
	public static final String LOAN_DELETE_PAGE_URL = "deleteThisLoan.do";
	public static final String UPDATE_LOAN_STATUS_URL = "LoanStatusUpdate.do";
	public static final String EDIT_LOAN_URL = "EditLoan.do";
	public static final String LOAN_DOCUMENT_LIST_URL = "ListLoanAllDocuments.do";
	public static final String LOAN_DOCUMENTS_UPLOAD_URL = "UploadLoanDocuments.do";
	public static final String LOAN_DOC_DELETE_URL = "loanDocumentDelete.do";

	public static final String LIST_OF_ADMINS_URL = "getAllAdmins.do";
	public static final String UPDATE_ADMIN_STATUS_URL = "updateEmployeeAdminStatus.do";
	public static final String CHANGE_ADMIN_STATUS_URL = "changeAdminStatus.do";

	public static final String GET_ALL_AVAILABLE_LANGUAGES_URL = "getAllAvailableLanguages.do";
	public static final String GET_ALL_AVAILABLE_SKILLS_URL = "getAllAvailableSkills.do";
	public static final String GET_ALL_AVAILABLE_Q_DEGREES_URL = "getAllAvailableQDegrees.do";
	public static final String GET_ALL_AVAILABLE_CONTRACT_TYPE_URL = "getAllAvailableCTypes.do";
	public static final String GET_ALL_AVAILABLE_JOB_TYPE_URL = "getAllAvailableJTypes.do";
	public static final String GET_ALL_AVAILABLE_JOB_FIELD_URL = "getAllAvailableJFields.do";
	public static final String GET_ALL_AVAILABLE_BRANCH_TYPE_URL = "getAllAvailableBTypes.do";
	public static final String GET_ALL_AVAILABLE_POLICY_TYPE_URL = "getAllAvailablePTypes.do";
	public static final String GET_ALL_AVAILABLE_EMPLOYEE_TYPE_URL = "getAllAvailableETypes.do";
	public static final String GET_ALL_AVAILABLE_EMPLOYEE_CATEGORY_URL = "getAllAvailableECategories.do";
	public static final String GET_ALL_AVAILABLE_REIMBURSEMENT_CATEGORY_URL = "getAllAvailableReimbCategories.do";
	public static final String GET_ALL_AVAILABLE_COUNTRY_URL = "getAllAvailableCountries.do";
	public static final String GET_ALL_AVAILABLE_ACCOUNT_TYPE_URL = "getAllAvailableAccountTypes.do";
	public static final String GET_ALL_AVAILABLE_WORK_SHIFTS_URL = "getAllAvailableWorkShifts.do";
	public static final String GET_ALL_AVAILABLE_EMPLOYEE_STATUS_URL = "getAllAvailableEStatus.do";
	public static final String GET_ALL_AVAILABLE_TERMINATION_STATUS_URL = "getAllAvailableTStatus.do";
	public static final String GET_ALL_AVAILABLE_LEAVE_TYPE_URL = "getAllAvailableLTypes.do";
	public static final String GET_ALL_AVAILABLE_REMINDER_STATUS_URL = "getAllAvailableRStatus.do";
	public static final String GET_ALL_AVAILABLE_EMPLOYEE_GRADE_URL = "getAllAvailableEGrades.do";
	public static final String GET_ALL_AVAILABLE_ADVANCE_SALARY_URL = "getAllAvailableAsvncStatus.do";
	public static final String GET_ALL_AVAILABLE_OVERTIME_STATUS_URL = "getAllAvailableOStatus.do";
	public static final String GET_ALL_AVAILABLE_LOAN_STATUS_URL = "getAllAvailableLStatus.do";
	public static final String GET_ALL_AVAILABLE_EMPLOYEE_DESIGNATION_URL = "getAllEmployeeDesignation.do";
	public static final String GET_ALL_AVAILABLE_ORGANIZATION_ASSETS_URL = "getAllOrganizationAssets.do";

	public static final String DELETE_EMPLOYEE_DESIGNATION_URL = "DeleteEmployeeDesignation.do";
	public static final String DELETE_ORGANIZATION_ASSET_URL = "DeleteOrganizationAsset.do";
	public static final String DELETE_POLICY_TYPE_URL = "DeletePolicyType.do";
	public static final String DELETE_LANGUAGE_URL = "DeleteLanguage.do";
	public static final String DELETE_SKILL_URL = "DeleteSkill.do";
	public static final String DELETE_QD_URL = "DeleteQD.do";
	public static final String DELETE_CONTRACT_TYPE_URL = "DeleteContractType.do";
	public static final String DELETE_JOB_TYPE_URL = "DeleteJobType.do";
	public static final String DELETE_JOB_FIELD_URL = "DeleteJobField.do";
	public static final String DELETE_BRANCH_TYPE_URL = "DeleteBranchType.do";
	public static final String DELETE_EMPLOYEE_TYPE_URL = "DeleteEmployeeType.do";
	public static final String DELETE_EMPLOYEE_CATEGORY_URL = "DeleteEmployeeCategory.do";
	public static final String DELETE_REIMBURSEMENT_CATEGORY_URL = "DeleteReimbursementCategory.do";
	public static final String DELETE_COUNTRY_URL = "DeleteCountry.do";
	public static final String DELETE_ACCOUNT_TYPE_URL = "DeleteAccountType.do";
	public static final String DELETE_WORK_SHIFT_URL = "DeleteWorkShift.do";
	public static final String DELETE_EMPLOYEE_STATUS_URL = "DeleteEmployeeStatus.do";
	public static final String DELETE_TERMINATION_STATUS_URL = "DeleteTerminationStatus.do";
	public static final String DELETE_LEAVE_TYPE_URL = "DeleteLeaveType.do";
	public static final String DELETE_REMINDER_STATUS_URL = "DeleteReminderStatus.do";
	public static final String DELETE_EMPLOYEE_GRADE_URL = "DeleteEmployeeGrade.do";
	public static final String DELETE_ADVANCE_SALARY_STATUS_URL = "DeleteAdvanceSalaryStatus.do";
	public static final String DELETE_OVERTIME_STATUS_URL = "DeleteOvertimeStatus.do";
	public static final String DELETE_LOAN_STATUS_URL = "DeleteLoanStatus.do";

	public static final String TRAVEL_URL = "travel.do";
	public static final String ADD_TRAVEL_URL = "add_travel.do";
	public static final String TRAVEL_EDIT = "editTravel.do";
	public static final String TRAVEL_DELETE = "deleteTravel.do";
	public static final String TRAVEL_SAVE = "saveTravel.do";
	public static final String TRAVEL_DOCUMENT_DELETE = "deleteTravelDocument.do";
	public static final String TRAVEL_DOCUMENT_DOWNLOAD = "downloadTravelDocument.do";
	public static final String TRAVEL_VIEW = "viewTravel.do";
	public static final String TRAVEL_UPDATE_STATUS = "updateTravelStatus.do";
	public static final String TRAVEL_DOCUMENT_UPLOAD = "uploadTravelDocument.do";

	public static final String TRANSFER_URL = "transfer.do";
	public static final String ADD_TRANSFER_URL = "add_transfer.do";
	public static final String TRANSFER_EDIT = "editTreansfer.do";
	public static final String TRANSFER_DELETE = "deleteTransfer.do";
	public static final String TRANSFER_SAVE = "saveTransfer.do";
	public static final String TRANSFER_DOCUMENT_DELETE = "deleteTransferDocument.do";
	public static final String TRANSFER_DOCUMENT_DOWNLOAD = "downloadTransferDocument.do";
	public static final String TRANSFER_VIEW = "viewTransfer.do";
	public static final String TRANSFER_UPDATE_STATUS = "updateTransferStatus.do";
	public static final String TRANSFER_DOCUMENT_UPLOAD = "uploadTransferDocument.do";

	public static final String RESIGNATION_URL = "resignation.do";
	public static final String ADD_RESIGNATION_URL = "add_resignation.do";
	public static final String RESIGNATION_EDIT = "editResignation.do";
	public static final String RESIGNATION_DELETE = "deleteResignation.do";
	public static final String RESIGNATION_SAVE = "saveResignation.do";
	public static final String RESIGNATION_DOCUMENT_DELETE = "deleteResignationDocument.do";
	public static final String RESIGNATION_DOCUMENT_DOWNLOAD = "downloadResignationDocument.do";
	public static final String RESIGNATION_VIEW = "viewResignation.do";
	public static final String RESIGNATION_UPDATE_STATUS = "updateResignationStatus.do";
	public static final String RESIGNATION_DOCUMENT_UPLOAD = "uploadResignationDocument.do";

	public static final String PERFORMANCE_EVALUATION_PAGE_URL = "PerformanceEvaluation.do";
	public static final String PERFORMANCE_EVALUATION_CREATE_PAGE_URL = "CreatePerformanceEvaluation.do";
	public static final String PERFORMANCE_EVALUATION_VIEW_PAGE_URL = "ViewPerformanceEvaluation.do";
	public static final String PERFORMANCE_EVALUATION_EDIT_PAGE_URL = "UpdatePerformanceEvaluation.do";
	public static final String PERFORMANCE_EVALUATION_DELETE_URL = "PerformanceEvaluationDelete.do";
	public static final String PERFORMANCE_EVALUATION_QUESTION_UPLOAD_URL = "UploadPerformanceEvaluationQuestion.do";
	public static final String PERFORMANCE_EVALUATION_LIST_URL = "PerformanceEvaluationList.do";
	public static final String PERFORMANCE_EVALUATION_Q_LIST_URL = "PerformanceEvaluationQuesList.do";
	public static final String PERFORMANCE_EVALUATION_Q_DELETE_URL = "PerformanceEvaluationQuesDelete.do";
	public static final String PERFORMANCE_EVALUATION_GET_DETAILS = "getEmployeePerformance.do";
	public static final String PERFORMANCE_EMPLOYEE_DETAIL_DELETE = "EmployeePerformanceEvaluationDelete.do";

	public static final String WORKSHEETS_PAGE_URL = "worksheets.do";
	public static final String WORKSHEETS_ADD_PAGE_URL = "addWorkSheet.do";
	public static final String WORKSHEETS_EDIT_PAGE_URL = "editWorkSheet.do";
	public static final String WORKSHEETS_DELETE_PAGE_URL = "deleteWorkSheet.do";
	public static final String WORKSHEETS_VIEW_PAGE_URL = "viewWorkSheets.do";
	public static final String WORKSHEETS_INFORMATION_SAVE = "saveWorksheetsInfo.do";
	public static final String WORKSHEETS_PROJECT_SAVE = "saveWorksheetProjectInfo.do";
	public static final String WORKSHEETS_DESCRIPTION_SAVE = "saveWorksheetsDesc.do";
	public static final String WORKSHEETS_ADDINFO_SAVE = "saveWorksheetsAddInfo.do";
	public static final String WORKSHEETS_UPLOAD_DOCS = "uploadWorksheetDocs.do";
	public static final String WORKSHEETS_DELETE_DOCS = "deleteWorksheetDoc.do";
	public static final String WORKSHEETS_VIEW_DOCS = "viewWorksheetDoc.do";

	public static final String REPORTS_HR_HOME_URL = "hrReports.do";
	public static final String GENERATE_HR_EMP_REPORT = "generateEmpRep.do";

	public static final String PDF_DWNLOAD_URL = "pdfRequest.do";
	public static final String VIEW_HR_SUMMARY = "viewHrSummary.do";
	public static final String EXPORT_HR_SUMMARY = "exportHrSummary.do";
	public static final String VIEW_PAYROLL_SUMMARY = "viewPaySummary.do";

	public static final String GENERATE_HOLIDAY_REPORT = "generateHolidayReport.do";
	public static final String GENERATE_LEAVES_REPORT = "generateLeavesReport.do";
	public static final String GENERATE_BRANCH_REPORT = "generateBranchReport.do";

	public static final String REC_REPORT_HOME_URL = "recReports.do";
	public static final String VIEW_REC_POST_REPORT = "viewPostReport.do";
	public static final String VIEW_REC_CAND_REPORT = "viewCandReport.do";
	public static final String VIEW_REC_INTERVIEW_REPORT = "viewInterviewReport.do";

	public static final String GENERATE_PRO_EMP_REPORT_URL = "viewEmpProjectReport.do";

	public static final String EMP_REPORTS_HOME_URL = "empReports.do";
	public static final String EMP_SUMMARY_URL = "viewEmpSummary.do";
	public static final String VIEW_JOIN_REPORT = "viewEmpJoinReport.do";
	public static final String VIEW_PROJECT_REPORT = "viewEmpProReport.do";
	public static final String VIEW_TRANSFERS_REPORT = "viewTransfersReport.do";
	public static final String VIEW_EMP_ASSGMNT_REPORT = "viewAssigmentReport.do";
	public static final String VIEW_EMP_RESIGN_REPORT = "viewEmpResignReport.do";
	public static final String VIEW_EMP_TRAVEL_REPORT = "viewEmpTravelReport.do";
	public static final String VIEW_EMP_EVALUATION_REPORT = "viewEmpEvaluationReport.do";
	public static final String EMP_EXP_EVALUATION_REPORT = "exportEvaluationReport.do";

	public static final String PAY_REPORTS_HOME_URL = "payReports.do";
	public static final String PAY_LOAN_REPORT_URL = "payLoan.do";
	public static final String PAY_ADVSALARY_REPORT_URL = "payAdvSalary.do";
	public static final String PAY_ESCI_REPORT_URL = "viewEsiReport.do";
	public static final String PAY_PF_REPORT_URL = "viewPFReport.do";
	public static final String PAY_OVERTIME_URL = "viewOTReport.do";
	public static final String PAY_REIMBURSE_URL = "viewReimbReport.do";
	public static final String PAY_COMMISSION_URL = "viewCommReport.do";
	public static final String PAY_DEDUCTION_URL = "viewDeductReport.do";
	public static final String PAY_ADJUSTMENTS_URL = "viewAdjstmntsReport.do";
	public static final String PAY_BONUSES_URL = "viewBonusesReport.do";
	public static final String PAY_DAILYW_URL = "viewDailyWageReport.do";
	public static final String PAY_HOURLYW_URL = "viewHourlyWageReport.do";
	public static final String PAY_SALARY_REPORT_URL = "viewSalaryReport.do";
	public static final String PAY_SALARY_SPLIT = "viewSalarySplit.do";
	public static final String PAY_PAYSLIPS_URL = "viewPaySlips.do";
	public static final String PAY_PAYROLL_REPORT_URL = "viewPayrollReport.do";

	public static final String HR_EXP_LEAVE_REPORT = "exportHrLeave.do";
	public static final String HR_EXP_HOLIDAYS_REPORT = "exportHrHolidays.do";
	public static final String HR_EXP_PROJECTS_REPORT = "exportHrprojects.do";

	public static final String TIME_REPORTS_URL = "timeReports.do";
	public static final String TIME_VIEW_WORKSHEET = "viewWorkSheetReport.do";
	public static final String TIME_VIEW_EMP_WORKSHEET = "viewEmpWorkSheet.do";
	public static final String TIME_VIEW_EMP_TIMESHEET = "viewEmpTimeReport.do";

	public static final String REC_EXP_POST_REPORT = "exportRecPosts.do";
	public static final String REC_EXP_CAND_REPORT = "exportRecCand.do";
	public static final String REC_EXP_INT_REPORT = "exportRecInt.do";

	public static final String TIME_VIEW_LEAVE_SUMMARY = "viewEmpLeaves.do";
	public static final String TIME_VIEW_ABSENT_EMPLOYEES = "viewAbsentEmployees.do";
	public static final String TIME_EMP_WORKSHIFT = "viewEmpAttendance.do";

	public static final String TIM_EXP_EMP_TIMESHEET = "exportEmplTimeSheet.do";

	public static final String EMP_EXP_JOIN = "exportEmpJoinReport.do";

	public static final String REC_LETTERS_URL = "addRecLeters.do";

	public static final String REC_FIND_CANDIDATES = "getCandidatesByInterviewDateAndPost.do";

	public static final String REC_FIND_INTERVIEWS_BY_DATE = "getInterviewsDate.do";

	public static final String EMP_EXP_ASSIGNEMENTS = "exportEmpAsigns.do";
	public static final String EMP_EXP_PROJECT = "exportEmpProjects.do";
	public static final String EMP_EXP_RESIGNATIONS = "exportEmpResignations.do";
	public static final String EMP_EXP_TRANSFER = "exportEmpTransfer.do";
	public static final String EMP_EXP_TRAVEL = "exportEmpTravel.do";
	public static final String TIM_EXP_ABSENT = "exportTimAbsent.do";
	public static final String TIM_EXP_WORKSHIFT = "exportWorkshift.do";
	public static final String TIM_EXP_WORKSHEET = "exportWorkSheet.do";
	public static final String TIM_EXP_EMP_WORKSHEET = "exportEmpWS.do";

	public static final String PAY_EXP_ADJUSTMENT = "exportPayAdjustment.do";
	public static final String PAY_EXP_ADVANCE = "exportPayAdvance.do";
	public static final String PAY_EXP_BONUSE = "exportPayBonus.do";
	public static final String PAY_EXP_COMMISSIONS = "exportPayCommissions.do";
	public static final String PAY_EXP_DAILY = "exportPayDaily.do";
	public static final String PAY_EXP_DEDUCTIONS = "exportPayDeductions.do";
	public static final String PAY_EXP_ESI = "exportPayEsi.do";
	public static final String PAY_EXP_HOURLY = "exportHourly.do";
	public static final String PAY_EXP_LOAN = "exportLoanReport.do";
	public static final String PAY_EXP_OT = "exportOTReport.do";
	public static final String PAY_EXP_PF = "exportPfReport.do";
	public static final String PAY_EXP_REIMB = "exportReimbursement.do";
	public static final String PAY_EXP_PAYSLIP = "exportPaySlip.do";

	public static final String WORKSHEET_GET_TASK = "getWorksheetTask.do";
	public static final String WORKSHEET_DELETE_TASK = "deleteWorkSheetTask.do";

	public static final String ADD_ATTENDANCE_STATUS_URL = "addAttenStatus.do";
	public static final String SAVE_ATTENDANCE_STATUS = "saveAttendanceStatus.do";
	public static final String EDIT_ATTENDANCE_STATUS = "editAttendanceStatus.do";

	public static final String HR_EXP_BRANCH_REPORT = "exportBranchReport.do";
	public static final String SEND_OFFER_LETTER = "sendOfferLetter.do";
	public static final String SEND_APPOINTMENT_LETTER="sendAppointmentLetter.do";

	public static final String APPROVED_SALARIES="approvedSalaries.do";
	public static final String VIEW_SALARY="viewSalaryHrms.do";
	public static final String FILTER_PAY_SALARIES="filterHrmsSalary.do.do";
	
	public static final String POP_UP="popUp.do";
	public static final String POP_UP_EMPLOYEES="popUpemployees.do";

	public static final String GET_AUTO_APPROVE_EMPLOYEES="getAutoApproveEmployees";
	public static final String CHANGE_AUTO_APPROVAL="changeAutoApproval";
	public static final String UPDATE_AUTO_APPROVAL_STATUS="updateAutoApproval";
	
	public static final String BANK_STATEMENT="bankStatement";
	public static final String EXPORT_BANK_STATEMENT="exportBankStatement";
	public static final String BANK_HOME="bankHome";
	
	public static final String DETAILED_EMP_REPORT_VIEW="viewDetailedEmpreport.do";
}
