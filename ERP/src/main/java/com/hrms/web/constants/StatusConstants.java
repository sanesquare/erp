package com.hrms.web.constants;
/**
 * 
 * @author Shamsheer
 * @since 05-May-2015
 */
public class StatusConstants {

	public static final String REQUESTED="Requested";
	public static final String APPROVED="Approved";
	public static final String REJECTED="Rejected";
	public static final String SEND_BACK="Send Back";
	public static final String FORWARD="Forward";
	public static final String CLOSED="Closed";
}
