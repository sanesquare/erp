package com.hrms.web.constants;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
public class PageNameConstatnts {

	public static final String INDEX_PAGE = "index";
	public static final String LOGIN_PAGE = "signin";
	public static final String SETTINGS_PAGE = "hrms_settings";
	public static final String REMINDER_LIST_PAGE = "reminder_list";

	public static final String PROJECTS_PAGE = "hrms_projects";
	public static final String PAYROLL_EMPLOYEES_LIST_PAGE = "second_levels_list";

	public static final String DEPARTMENT_PAGE = "hrms_departments";
	public static final String ADD_DEPARTMENT_PAGE = "hrms_add_departments";
	public static final String EDIT_DEPARTMENT_PAGE = "hrms_edit_department";
	public static final String VIEW_DEPART_EMPLOYEE_PAGE = "viewDepartEmployee";

	public static final String ANNOUNCEMENTS_PAGE = "hrms_announcements";
	public static final String ADD_ANNOUNCEMENTS_PAGE = "hrms_add_announcements";
	public static final String ADD_PROJECTS_PAGE = "hrms_add_projects";
	public static final String VIEW_PROJECT_DETAILS_PAGE = "hrms_view_project";

	public static final String VIEW_ANNOUNCEMENTS_DETAILS_PAGE = "hrms_view_announcements";
	public static final String EDIT_ANNOUNCEMENTS_PAGE = "hrms_edit_announcements";

	public static final String BRANCHES_PAGE = "hrms_branches";
	public static final String ADD_BRANCHES_PAGE = "hrms_add_branches";
	public static final String VIEW_BRANCHES_DETAILS_PAGE = "hrms_view_branches";

	public static final String MEETINGS_PAGE = "hrms_meetings";
	public static final String ADD_MEETINGS_PAGE = "hrms_add_meetings";
	public static final String VIEW_MEETING = "hrms_view_meetings";

	public static final String SALARY_CALCULATOR_PAGES = "salary_calculator";
	public static final String CALUCLATOR_TEMPLATE="salaryCalculatoTemplate";

	public static final String PROMOTION_PAGE = "hrms_promotion";
	public static final String SUPERIORS_LIST_PAGE = "superiors_list";
	public static final String ADD_PROMOTION_PAGE = "hrms_add_promotion";
	public static final String EDIT_PROMOTION_PAGE = "hrms_edit_promotion";

	public static final String PAY_SALARY = "pay_salary";
	public static final String ADD_PAY_SALARY = "add_paysalary";

	public static final String COMPLAINTS_PAGE = "hrms_complaints";
	public static final String ADD_COMPLAINTS_PAGE = "hrms_add_complaints";
	public static final String EDIT_COMPLAINTS_PAGE = "hrms_edit_complaints";

	public static final String EMPLOYEE_PAGE = "hrms_employee";
	public static final String EMPLOYEE_ROLES_PAGE = "hrms_employee_roles";
	public static final String EMPLOYEE_ADD_PAGE = "hrms_add_employee";
	public static final String EMPLOYEE_VIEW_PAGE = "hrms_view_employee";

	public static final String EMPLOYEE_JOINING_ADD_PAGE = "hrms_add_employee_joining";
	public static final String EDIT_EMPLOYEE_JOINING = "hrms_edit_employee_joining";
	public static final String EMPLOYEE_JOINING_PAGE = "hrms_employee_joining";
	public static final String EMPLOYEE_JOININ_VIEW = "hrms_view_employee_joining";

	public static final String USER_ACTIVITIES_PAGE = "hrms_user_activities";
	public static final String LIST_USER_ACTIVITIES_PAGE = "hrms_list_user_activities";

	public static final String ADD_NOTIFICATION_PAGE = "hrms_add_notification";
	public static final String NOTIFICATION_PAGE = "hrms_list_notification";

	public static final String EMPLOYEE_EXIT_LIST_PAGE = "employee_exit";
	public static final String EMPLOYEE_EXIT_ADD_PAGE = "add_employee_exit";
	public static final String EMPLOYEE_EXIT_VIEW_PAGE = "view_employee_exit";

	public static final String RECRUITMENT_JOB_POSTS_PAGE = "hrms_job_posts";
	public static final String RECRUITMENT_JOB_CANDIDATES_PAGE = "hrms_job_candidates";
	public static final String RECRUITMENT_JOB_INTERVIEWS_PAGE = "hrms_job_interviews";

	public static final String RECRUITMENT_ADD_JOB_POSTS_PAGE = "hrms_add_job_posts";
	public static final String RECRUITMENT_VIEW_JOB_POST_PAGE = "hrms_view_job_posts";

	public static final String ADD_JOB_CANDIDATES_PAGE = "hrms_add_job_candidates";
	public static final String VIEW_JOB_CANDIDATE_PAGE = "hrms_view_job_candidates";

	public static final String ADD_JOB_INTERVIEWS_PAGE = "hrms_add_job_interviews";
	public static final String VIEW_JOB_INTERVIEWS_PAGE = "hrms_view_job_interviews";

	public static final String TERMINATION_PAGE = "termination";
	public static final String TERMINATION_CREATE_PAGE = "new_termination";
	public static final String TERMINATION_VIEW_PAGE = "view_termination";

	public static final String EMPLOYEE_SUPERIOR_LIST_PAGE = "mySuperiorList";

	public static final String WARNING_PAGE = "warnings";
	public static final String WARNING_LIST_PAGE = "warning_list";
	public static final String WARNING_DOC_LIST_PAGE = "warning_doc_list";
	public static final String WARNING_CREATE_PAGE = "new_warning";
	public static final String WARNING_VIEW_PAGE = "view_warning";

	public static final String ANNOUNCEMENT_DOC_LIST_PAGE = "announcement_doc_list";

	public static final String MEMO_PAGE = "memos";
	public static final String MEMO_CREATE_PAGE = "new_memo";
	public static final String MEMO_VIEW_PAGE = "view_memo";

	public static final String HOLIDAYS_PAGE = "holidays";
	public static final String CREATE_HOLIDAY_PAGE = "new_holiday";
	public static final String HOLIDAY_LIST_PAGE = "holiday_list";
	public static final String VIEW_HOLIDAY_PAGE = "view_holiday";

	public static final String WORK_SHIFT_SAVE_PAGE = "new_workshift";
	public static final String WORK_SHIFT_PAGE = "work_shift";
	public static final String EMPLOYEE_WORK_SHIFT_INFO_PAGE = "employee_work_shift_info";

	public static final String MEMO_LIST_S_PAGE = "memo_list";
	public static final String MEMO_DOC_LIST_PAGE = "memo_doc_list";

	public static final String CONTRACT_PAGE = "hrms_contract";
	public static final String CONTRACT_ADD_PAGE = "hrms_add_contract";
	public static final String CONTRACT_VIEW_PAGE = "hrms_view_contract";
	public static final String CONTRACT_EDIT_PAGE = "hrms_edit_contract";

	public static final String ASSIGNMENTS_PAGE = "hrms_assignments";
	public static final String ADD_ASSIGNMENTS_PAGE = "hrms_add_assignments";
	public static final String VIEW_ASSIGNMENTS_PAGE = "hrms_view_assignments";

	public static final String ACHIEVEMENTS_PAGE = "hrms_achievements";
	public static final String ADD_ACHIEVEMENTS_PAGE = "hrms_add_achievements";

	public static final String ORGANIZATION_POLICY_PAGE = "organization_policy";
	public static final String ORGANIZATION_POLICY_LIST_PAGE = "organization_policy_list";
	public static final String SAVE_ORGANIZATION_POLICY_PAGE = "new_organization_policy";
	public static final String VIEW_ORGANIZATION_POLICY_PAGE = "view_organization_policy";
	public static final String ORGANIZATION_POLICY_DOC_LIST_PAGE = "organization_policy_doc_list";

	public static final String GET_INSURANCE_PAGE = "insurance";
	public static final String ADD_INSURANCE_PAGE = "add_insurance";
	public static final String VIEW_INSURANCE_PAGE = "view_insurance";
	public static final String INSURANCE_LIST_PAGE = "insuranceList";

	public static final String GET_ESI_PAGE = "esi";
	public static final String ADD_ESI_PAGE = "add_esi";
	public static final String VIEW_ESI_PAGE = "view_esi";
	public static final String ESI_LIST_PAGE = "esiList";

	public static final String GET_PAYROLL_PAYSLIP_ITEM_TAB_PAGE = "pay_slip_item_tab";
	public static final String GET_PAYROLL_TAXRULE_ITEM_TAB_PAGE = "tax_rule_item_tab";
	public static final String GET_PAYROLL_PAYSLIP_ITEM_LIST_PAGE = "pay_slip_item_list";
	public static final String GET_PAYROLL_TAXRULE_ITEM_LIST_PAGE = "tax_rule_item_list";
	public static final String GET_PAYROLL_EXTRA_PAYSLIP_ITEM_LIST_PAGE = "extra_pay_slip_item_list";
	public static final String GET_BONUS_DATE_LIST_PAGE = "bonusDateList";
	public static final String GET_PF_SYNTAX_LIST_PAGE = "pfSyntaxList";
	public static final String GET_ESI_SYNTAX_LIST_PAGE = "esiSyntaxList";
	public static final String GET_PAYROLL_PAYSLIP_ITEM_EMPTY_LIST_PAGE = "empty_pay_slip_item_list";
	public static final String GET_PAYROLL_STRUCTURE_PAGE = "payroll_structure";

	public static final String GET_OVERTIME_PAGE = "overtime";
	public static final String ADD_OVERTIME_PAGE = "add_overtime";
	public static final String VIEW_OVERTIME_PAGE = "view_overtime";
	public static final String OVERTIME_LIST_PAGE = "overtimeList";

	public static final String GET_ADVANCE_SALARY_PAGE = "advance_salary";
	public static final String ADD_ADVANCE_SALARY_PAGE = "add_advance_salary";
	public static final String VIEW_ADVANCE_SALARY_PAGE = "view_advance_salary";
	public static final String ADVANCE_SALARY_LIST_PAGE = "advance_salaryList";

	public static final String GET_PROFIDENT_FUNDS_PAGE = "pf";
	public static final String ADD_PROFIDENT_FUND_PAGE = "add_pf";
	public static final String VIEW_PROFIDENT_FUND_PAGE = "view_pf";
	public static final String PROFIDENT_FUND_LIST_PAGE = "pfList";

	public static final String GET_LOAN_PAGE = "loan";
	public static final String ADD_LOAN_PAGE = "add_loan";
	public static final String VIEW_LOAN_PAGE = "view_loan";
	public static final String LOAN_DOCUMENTS_LIST_PAGE = "loanDocumentList";
	public static final String LOAN_LIST_PAGE = "loanList";

	public static final String ORG_BRACNH_LIST_PAGE = "orgBranchList";
	public static final String ORG_BRACNH_DEPARTMENT_LIST_PAGE = "orgBranchDeptmntList";
	public static final String ORG_BRACNH_DEPARTMENT_EMP_LIST_PAGE = "orgBranchDeptmntEmpList";
	public static final String ORG_BRACNH_DEPARTMENT_SEL_EMP_PAGE = "orgBranchDeptmntSelEmp";
	public static final String EMPLOYEE_BASIC_INFO_POPUP_PAGE = "employeePopupPage";

	public static final String LIST_OF_ADMINS_PAGE = "admin_list";

	public static final String TRAVEL_PAGE = "hrms_travel";
	public static final String ADD_TRAVEL_PAGE = "hrms_add_travel";
	public static final String TRAVEL_VIEW_PAGE = "hrms_view_travel";

	public static final String TRANSFER_PAGE = "hrms_transfer";
	public static final String ADD_TRANSFER_PAGE = "hrms_add_transfer";
	public static final String TRANSFER_VIEW_PAGE = "hrms_view_transfer";

	public static final String RESIGNATION_PAGE = "hrms_resignation";
	public static final String ADD_RESIGNATION_PAGE = "hrms_add_resignation";
	public static final String RESIGNATION_VIEW_PAGE = "hrms_view_resignation";

	public static final String PERFORMANCE_EVALUATION_PAGE = "performance_evaluation";
	public static final String PERFORMANCE_EVALUATION_CREATE_PAGE = "new_performance_evaluation";
	public static final String PERFORMANCE_EVALUATION_VIEW_PAGE = "view_performance_evaluation";
	public static final String PERFORMANCE_EVALUATION_LIST_PAGE = "performance_evaluation_list";
	public static final String PERFORMANCE_EVALUATION_Q_LIST_PAGE = "performance_evaluation_q_list";

	public static final String WORKSHEETS_PAGE = "hrms_worksheet";
	public static final String ADD_WORKSHEETS_PAGE = "hrms_add_worksheet";
	public static final String VIEW_WORKSHEETS_PAGE = "hrms_view_worksheet";

	public static final String LEAVES_PAGE = "hrms_leaves";
	public static final String LEAVES_VIEW_PAGE = "hrms_view_leaves";
	public static final String LEAVES_ADD_PAGE = "hrms_add_leaves";

	public static final String PAYSLIP_PAGE = "payslip";
	public static final String PAYSLIP_ADD_PAGE = "payslip_add";
	public static final String PAYSLIP_VIEW_PAGE = "payslip_view";

	public static final String HOURLY_WAGES_PAGE = "hourlyWages";
	public static final String HOURLY_WAGES_ADD_PAGE = "add_hourlyWages";
	public static final String HOURLY_WAGES_VIEW_PAGE = "view_hourlyWages";

	public static final String BONUS_PAGE = "bonuses";
	public static final String BONUS_ADD_PAGE = "add_bonuses";
	public static final String BONUS_VIEW_PAGE = "view_bonuses";

	public static final String DEDUCTION_PAGE = "deduction";
	public static final String DEDUCTION_ADD_PAGE = "add_deduction";
	public static final String DEDUCTION_VIEW_PAGE = "view_deduction";

	public static final String COMMISSION_PAGE = "commission";
	public static final String COMMISSION_ADD_PAGE = "add_commission";
	public static final String COMMISSION_VIEW_PAGE = "view_commission";

	public static final String ADJUSTMENT_PAGE = "adjustments";
	public static final String ADJUSTMENT_ADD_PAGE = "add_adjustments";
	public static final String ADJUSTMENT_VIEW_PAGE = "view_adjustments";

	public static final String REIMBURSEMENTS_PAGE = "reimbursements";
	public static final String REIMBURSEMENTS_ADD_PAGE = "add_reimbursements";
	public static final String REIMBURSEMENTS_VIEW_PAGE = "view_reimbursements";

	public static final String DAILY_WAGES_PAGE = "dailyWages";
	public static final String DAILY_WAGES_ADD_PAGE = "add_dailyWages";
	public static final String DAILY_WAGES_VIEW_PAGE = "view_dailyWages";

	public static final String ATTENDANCE_PAGE = "hrms_attendance";
	public static final String ATTENDANCE_ADD_PAGE = "hrms_add_attendance";
	public static final String ATTENDANCE_VIEW_PAGE = "hrms_view_attendance";

	public static final String REPORTS_HOME_PAGE = "hrReportHome";
	public static final String HR_REPORT_VIEW = "hrReportView";

	public static final String HR_SUMMARY_VIEW = "hrSummaryView";

	public static final String PAYROLL_SUMMARY_VIEW = "payrollSummaryView";

	public static final String HOLIDAYS_REPORT_VIEW = "hrHolidaysReport";

	public static final String LEAVES_REPORT_VIEW = "hrLeavesReport";

	public static final String BRANCH_REPORT = "hrBranchReport";

	public static final String SALRY_PAGE = "salary";

	public static final String REC_REPORT_HOME = "recReportHome";
	public static final String REC_POST_REPORT = "recJobPostReport";
	public static final String REC_CANDIDATE_REPORT = "recJobCandReport";
	public static final String REC_INTERVIEW_REPORT = "recJobIntReport";

	public static final String PROJECT_EMP_REPORT_PAGE = "hrProEmpReport";

	public static final String EMP_REPORTS_HOME_PAGE = "empReportHome";
	public static final String EMP_SUMMARY_PAGE = "empSummary";
	public static final String EMP_JOINING_REPORT_PAGE = "empJoiningReport";
	public static final String EMP_PROJECT_REPORT_PAGE = "empProjectReport";
	public static final String EMP_TRANSFERS_REPORT = "empTransferReport";
	public static final String EMP_ASSGMNT_REPORT = "empAssignmentReport";
	public static final String EMP_RESIGN_REPORT = "empResignReport";
	public static final String EMP_TRAVEL_REPORT = "empTravelReport";
	public static final String EMP_EVALUATION_REPORT = "empEvaluationReport";

	public static final String PAY_REPORTS_HOME = "payReportHome";
	public static final String PAY_LOANS_REPORT_PAGE = "payLoansReport";
	public static final String PAY_ADVSALARY_REPORT_PAGE = "payAdvSalReport";
	public static final String PAY_ESI_REPORT_PAGE = "payEsiReport";

	public static final String SALRY_HOME_PAGE="salaryHome";
	
	public static final String PAY_PF_REPORT_PAGE = "payPFReport";
	public static final String PAY_OT_PAGE = "payOTReport";
	public static final String PAY_REIMB_PAGE = "payReimReport";
	public static final String PAY_COMM_PAGE = "payCommReport";
	public static final String PAY_DEDUCT_PAGE = "payDeductReport";
	public static final String PAY_ADJST_PAGE = "payAdjstReport";
	public static final String PAY_BONUSES_PAGE = "payBonusesReport";
	public static final String PAY_DAILYWAGE_PAGE = "payDailyWReport";
	public static final String PAY_HOURLYWAGE_PAGE = "payHourlyWReport";
	public static final String PAY_SALARY_REPORT = "paySalaryReport";
	public static final String PAY_SPLIT_SALARY_PAGE = "paySalarySplit";
	public static final String PAY_PAYSLIP_PAGE = "payPaySlip";
	public static final String PAY_PAYROLL_REPORT_PAGE = "payPayrollReport";

	public static final String TIME_REPORTS_HOME_PAGE = "timeReportHome";
	public static final String TIME_WORKSHEET_REPORT = "timeWorkSheetReport";
	public static final String TIME_EMP_WORK_REPORT = "timeEmpWorkSheetReport";
	public static final String TIME_EMP_TIMESHEET_REPORT = "timeEmpTimeSheetReport";
	public static final String TIME_EMP_LEAVE_SUMMARY = "timeEmpLeaveSummary";
	public static final String TIME_ABSENT_EMP = "timeAbsentEmp";
	public static final String TIME_EMP_ATTENDANCE = "timeEmpAttendance";

	public static final String REC_LETTERS_PAGE = "recLetters";

	public static final String ADD_ATTENDANCE_STATUS_PAGE = "hrms_add_attenStatus";

	public static final String APPROVED_SALARIES = "approvedSalaries";
	public static final String VIEW_SALARY="viewSalary_hrms";
	public static final String SALARIES_LIST="salariesList";
	
	public static final String POP_UP="popUp";
	public static final String POP_UP_EMPLOYEES="popUpemployees";
	
	public static final String AUTO_APPROVE_EMPLOYEES="autoApproveEmployees";
	
	public static final String DETAILED_EMPLOYEE_REPORT="detailedEmpreport";
	public static final String BANK_STATEMENT="bankStatement";
	public static final String BANK_HOME="bankReportHome";
}
