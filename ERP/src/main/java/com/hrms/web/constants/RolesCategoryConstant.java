package com.hrms.web.constants;
/**
 * 
 * @author Shamsheer
 * @since 06-May-2015
 */
public class RolesCategoryConstant {

	public static final String HOME="Home";
	public static final String ERP="Erp";
	public static final String ORGANIZATION="Organization";
	public static final String RECRUITMENT="Recruitment";
	public static final String EMPLOYEES="Employees";
	public static final String TIME_SHEET="Time Sheet";
	public static final String PAYROLL="Payroll";
	public static final String REPORTS="Reports";
	public static final String MISCELLANEOUS="Miscellaneous";
}
