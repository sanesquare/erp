package com.hrms.web.constants;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March_2015
 *
 */
public class MessageConstants {

	public static final String SUCCESS = "success_msg";
	public static final String ERROR = "error_msg";

	public static final String EMPLOYEE_EXIT_MODULE = "EmployeeExit";
	public static final String TERMINATION_MODULE = "Termination";
	public static final String ORGANIZATION_POLICY_MODULE = "OrganizationPolicy";
	public static final String ANNOUNCEMENT_MODULE = "Announcement";
	public static final String WARNING_MODULE = "Warning";
	public static final String PERFORMANCE_EVALUATION_MODULE = "PerformanceEvaluationQuestion";
	public static final String LOAN_DOCUMENT_MODULE = "LoanDocument";
	public static final String MEMO_MODULE = "Memo";

	public static final String MEETING_MODULE = "Meetings";

	public static final String TRAVEL_MODULE = "Travel";

	public static final String TRANSFER_MODULE = "Transfer";

	public static final String RESIGNATION_MODULE = "Resignation";

	public static final String LEAVE_MODULE = "Leave";

	public static final String EMPLOYEE_MODULE = "Employee";
	
	public static final String DOCUMENT_UPLOAD_SUCCESS="Document Uploaded Successfully";
	public static final String DOCUMENT_UPLOAD_FAILED="Document Upload Failed";

	public static final String REIMBURSEMENTS_MODULE = "Reimbursements";

	public static final String EMPLOYEE_JOINING_MODULE = "EmployeeJoining";

	public static final String CONTRACT_MODULE = "Contract";
	public static final String ASSIGNMENT_MODULE = "Assignment";

	public static final String EMPLOYEE_JOINING_SAVE_SUCCESS = "Employee Joining Saved Successfully.";
	public static final String EMPLOYEE_JOINING_UPDATE_SUCCESS = "Employee Joining Updated Successfully.";

	public static final String EMPLOYEE_JOINING_SAVE_FAILED = "Employee Joining Save Failed.";
	public static final String EMPLOYEE_JOINING_UPDATE_FAILED = "Employee Joining Update Failed.";
	public static final String EMPLOYEE_JOINING_DELETE_SUCCESS = "Employee Joining Deleted Successfully.";
	public static final String EMPLOYEE_JOINING_DELETE_FAILED = "Employee Joining Delete Failed.";

	
	public static final String EMP_EXIT_BUTTON_LABEL = "buttonLabel";
	public static final String EMP_EXIT_BUTTON_1 = "Save";
	public static final String EMP_EXIT_BUTTON_2 = "Update";

	public static final String BUTTON_LABEL = "buttonLabel";
	public static final String SAVE_BUTTON = "Save";
	public static final String UPDATE_BUTTON = "Update";

	public static final String EMPLOYEE_SAVE_SUCCESS = "Employee Saved Successfully.";
	public static final String EMPLOYEE_SAVE_FAILED = "Employee Save Failed.";
	public static final String EMPLOYEE_UPDATE_SUCCESS = "Employee Updated Successfully.";
	public static final String EMPLOYEE_EXISTS = "Employee Code Exists.";

	public static final String CREATED_ON = "created_on";

	public static final String COUNTRY_SUCCESS = "Countries Successfully Saved.";
	public static final String SECOND_LEVEL_FORWARDER_SUCCESS = "Second Level Forwarder Successfully Added.";
	public static final String LANG_SUCCESS = "Languages Successfully Saved.";
	public static final String SKILL_SUCCESS = "Skills Successfully Saved.";
	public static final String QUALIFICATION_SUCCESS = "Qualifications Successfully Saved.";
	public static final String CONTRACT_TYPE_SUCCESS = "Contract Types Successfully Saved.";
	public static final String JOB_TYPE_SUCCESS = "Job Types Successfully Saved.";
	public static final String JOB_FIELD_SUCCESS = "Job Fields Successfully Saved.";
	public static final String BASE_CURRENCY_SUCCESS = "Base Currency Successfully Saved.";
	public static final String CURRENCY_SUCCESS = "Currency Successfully Saved.";
	public static final String TIME_ZONE_SUCCESS = "TimeZone Successfully Saved.";
	public static final String POLICY_TYPE_SUCCESS = "Policy Type Successfully Saved.";
	public static final String EMPLOYEE_TYPE_SUCCESS = "Employee Type Successfully Saved.";
	public static final String EMPLOYEE_CATEGORY_SUCCESS = "Employee Category Successfully Saved.";
	public static final String INSURANCE_TYPE_SUCCESS = "Insurance Type Successfully Saved.";
	public static final String BRANCH_TYPE_SUCCESS = "Branch Type Successfully Saved.";
	public static final String TRAINING_TYPE_SUCCESS = "Training Type Successfully Saved.";
	public static final String TRAINING_EVENT_TYPE_SUCCESS = "Training Event Type Successfully Saved.";
	public static final String REIMBURSEMENT_CATEGORY_SUCCESS = "Reimbursement Category Successfully Saved.";
	public static final String REMINDER_STATUS_SUCCESS = "Reminder Status Successfully Saved.";
	public static final String BLOOD_GROUP_SUCCESS = "Blood Group Successfully Saved.";
	public static final String RELEGION_SUCCESS = "Relegion Successfully Saved.";
	public static final String ACCOUNT_TYPE_SUCCESS = "Account Type Successfully Saved.";
	public static final String WORK_SHIFT_SUCCESS = "Work Shift Successfully Saved.";
	public static final String EMPLOYEE_STATUS_SUCCESS = "Employee Status Successfully Saved.";
	public static final String ASSETS_SUCCESS = "Asset Successfully Saved.";
	public static final String TERMINATION_STATUS_SUCCESS = "Termination Status Successfully Saved.";
	public static final String PROMOTION_STATUS_SUCCESS = "Promotion Status Successfully Saved.";
	public static final String EMPLOYEE_EXIT_TYPE_SUCCESS = "Employee Exit Type Successfully Saved.";
	public static final String LEAVE_TYPE_SUCCESS = "Leave Type Successfully Saved.";
	public static final String EMPLOYEE_GRADE_SUCCESS = "Employee Grade Successfully Saved.";
	public static final String EMPLOYEE_DESIGNATION_SUCCESS = "Employee Designation Successfully Saved.";
	public static final String SHIFT_DAY_SUCCESS = "Shift Days Successfully Saved.";
	public static final String ADVANCE_SALARY_STATUS_SUCCESS = "Advance Salary Status Successfully Saved.";
	public static final String OVERTIME_STATUS_SUCCESS = "Overtime Status Successfully Saved.";
	public static final String LOAN_STATUS_SUCCESS = "Loan Status Successfully Saved.";
	public static final String ADMIN_EMPLOYEE_SUCCESS = "Employee Admin Status Successfully Updated.";
	public static final String REMINDER_SUCCESS = "Reminder Successfully Saved.";
	public static final String ORGANISATION_SUCCESS = "Organisation Successfully Saved.";
	public static final String MANAGER_REPRT_NOTIFY_SUCCESS = "Manager/Supervisor Reporting Notification Successfully Updated.";
	public static final String WRONG = "Oops! Something went wrong!";

	public static final String PROJECT_MODULE = "Project";
	
	public static final String SAVE_SUCCESS="Saved Successfully.";
	public static final String UPDATED_SUCCESS="Updated Successfully.";
	public static final String DELETE_SUCCESS="Deleted Successfully.";
	
	public static final String SAVE_FAILED="Save Failed.";
	public static final String UPDATED_FAILED="Update Failed.";
	public static final String DELETE_FAILED="Delete Failed.";

	public static final String PROJECT_SAVE_SUCCESS = "Project Successfully Saved.";
	public static final String PROJECT_SAVE_FAILED = "Project Save Failed.";
	public static final String PROJECT_UPDATE_SUCCESS = "Project Successfully Updated.";
	public static final String PROJECT_DELETE_SUCCESS = "Project Successfully Deleted.";
	public static final String PROJECT_TITLE_EXISTS = "Project Title Already Exists..";
	public static final String FILE_UPLOAD_SUCCESS = "File Uploaded Successfully";
	public static final String FILE_UPLOAD_FAILED = "File Upload Failed";
	public static final String STATUS_UPDATE_SUCCESS = "Status Successfully Updated.";
	public static final String STATUS_UPDATE_FAILED = "Status Update Failed.";
	public static final String PROJECT_STATUS_SAVE_SUCCESS = "Project Status Saved Successfully";

	public static final String CONTRCT_DELETE_SUCCESS = "Contract Deleted Successfully.";
	public static final String CONTRCT_DELETE_FAILED = "Contract Delete Failed.";

	public static final String ASSIGNMENT_DELETE_SUCCESS = "Assignment Deleted Successfully.";
	public static final String ASSIGNMENT_DELETE_FAILED = "Assignment Delete Failed.";

	public static final String ANNOUNCEMENTS_SAVE_SUCCESS = "Announcement Successfully Saved.";
	public static final String ANNOUNCEMENTS_UPDATE_SUCCESS = "Announcement Successfully Updated.";
	public static final String ANNOUNCEMENTS_DELETE_SUCCESS = "Announcement Successfully Deleted.";
	public static final String ANNOUNCEMENTS_TITLE_EXISTS = "Announcement Already Exist.";
	public static final String JSON_SUCCESS = "SUCCESS";
	public static final String DEFAULT_CODE = "NULL";
	public static final String ANNOUNCEMENTS_INVALID_DATE = "Invalid Date Given";

	public static final String DOCUMENT_DELETED_SUCCESSFULLY = "Document Deleted Successfully.";
	public static final String DOCUMENT_DELETED_FAILED = "Document Delete Failed.";

	public static final String BRANCH_SAVE_SUCCESS = "Branch Successfully Saved.";
	public static final String BRANCH_DELETE_SUCCESS = "Branch Successfully Deleted.";
	public static final String BRANCH_UPDATE_SUCCESS = "Branch Successfully Updated.";
	public static final String BRANCH_SAVE_FAILED = "Branch Save Failed.";
	public static final String BRANCH_UPDATE_FAILED = "Branch Updation Failed.";

	public static final String DEPARTMENT_DELETE_SUCCESS = "Department Successfully Deleted.";
	public static final String DEPARTMENT_UPDATE_SUCCESS = "Department Successfully Updated.";
	public static final String DEPARTMENT_SAVE_SUCCESS = "Department Successfully Saved.";
	public static final String DEPARTMENT_DOCUMENT_DELETE_SUCCESS = "Department Document Deleted Successfully Saved.";
	public static final String DEPARTMENT_DOCUMENT_DELETE_FAIL = "Could not remove the department document";
	
	public static final String PROMOTION_DELETE_SUCCESS = "Promotion Successfully Deleted.";
	public static final String PROMOTION_UPDATE_SUCCESS = "Promotion Successfully Updated.";
	public static final String PROMOTION_SAVE_SUCCESS = "Promotion Successfully Saved.";
	public static final String PROMOTION_DOCUMENT_DELETE_SUCCESS = "Promotion Document Deleted Successfully Saved.";
	public static final String PROMOTION_DOCUMENT_DELETE_FAIL = "Could not remove the Promotion document";
	
	public static final String COMPLAINT_DELETE_SUCCESS = "Complaint Successfully Deleted.";
	public static final String COMPLAINT_UPDATE_SUCCESS = "Complaint Successfully Updated.";
	public static final String COMPLAINT_SAVE_SUCCESS = "Complaint Successfully Saved.";
	public static final String COMPLAINT_DOCUMENT_DELETE_SUCCESS = "Complaint Document Deleted Successfully Saved.";
	public static final String COMPLAINT_DOCUMENT_DELETE_FAIL = "Could not remove the Complaint document";

	public static final String NOTIFICATION_SUCCESS = "Notification Successfully Saved.";

	public static final String EMPLOYEE_EXIT_SUCCESS = "Employee's Exit Information Successfully Saved.";
	public static final String EMPLOYEE_EXIT_UPDATE_SUCCESS = "Employee's Exit Information Successfully Updated.";
	public static final String EMPLOYEE_EXIT_NOT_FOUND = "Employee's Exit Information Not Found.";

	public static final String NEW_TERMINATION_SUCCESS = "Termination Successfully Saved.";

	public static final String JOB_POST_SAVE_SUCCESS = "Job Post Successfully Saved.";
	public static final String JOB_POST_SAVE_FAILED = "Job Post Save Failed.";

	public static final String JOB_POST_UPDATE_SUCCESS = "Job Post Successfully Updated.";
	public static final String JOB_POST_UPDATE_FAILED = "Job Post Update Failed.";
	public static final String JOB_POST_DELETE_SUCCESS = "Job Post Successfully Deleted.";
	public static final String JOB_POST_DELETE_FAILED = "Job Post Delete Failed.";
	public static final String JOB_POST_MODULE = "Job Post";
	public static final String POST_DOCUMENT_SAVE_SUCCESS = "Job Post Document Saved Successfully.";
	public static final String POST_DOCUMENT_SAVE_FAILED = "Job Post Document Save Failed.";
	public static final String POST_DOCUMENT_DELETE_SUCCESS = "Job Post Document Deleted Successfully.";
	public static final String POST_DOCUMENT_DELETE_FAILED = "Job Post Document Deletion Failed.";

	public static final String JOB_INTERVIEW_SAVE_SUCCESS = "Interview Successfully Saved.";
	public static final String JOB_INTERVIEW_SAVE_FAILED = "Interview Save Failed.";
	public static final String JOB_INTERVIEW_UPDATE_SUCCESS = "Interview Updated Successfully.";
	public static final String JOB_INTERVIEW_UPDATE_FAILED = "Interview Update Failed.";
	public static final String JOB_INTERVIEW_DELETE_SUCCESS = "Interview Deleted Successfully.";
	public static final String JOB_INTERVIEW_DELETE_FAILED = "Interview Delete Failed.";
	public static final String INTERVIEW_DOCUMENT_SAVE_SUCCESS = "Interview Document Successfully Saved.";
	public static final String INTERVIEW_DOCUMENT_SAVE_FAILED = "Interview Document Save Failed.";
	public static final String INTERVIEW_DOCUMENT_DELETE_SUCCESS = "Interview Document Successfully Deleted.";
	public static final String INTERVIEW_DOCUMENT_DELETE_FAILED = "Interview Document Deletion Failed.";
	public static final String INTERVIEW_MODULE = "Interview";

	public static final String JOB_CANDIDATES_SAVE_SUCCESS = "Job Candidate Successfully Saved.";
	public static final String JOB_CANDIDATES_SAVE_FAILED = "Job Candidate Save Failed.";
	public static final String JOB_CANDIDATES_UPDATE_SUCCESS = "Job Candidate Updated Successfully.";
	public static final String JOB_CANDIDATES_UPDATE_FAILED = "Job Candidate Update Failed.";
	public static final String JOB_CANDIDATE_DELETE_SUCCES = "Job Candidate Deleted Successfully.";
	public static final String JOB_CANDIDATE_DELETE_FAILED = "Job Candidate Delete Failed.";
	public static final String CANDIDATE_DOC_SAVE_SUCCESS = "Job Candidate Document Saved Successfully.";
	public static final String CANDIDATE_DOC_SAVE_FAIL = "Job Candidate Document Save Failed.";
	public static final String CANDIDATE_RES_SAVE_SUCCESS = "Candidate Resume Saved Successfully.";
	public static final String CANDIDATE_RES_SAVE_FAIL = "Candidate Resume Save Failed.";
	public static final String CANDIDATE_DOC_DEL_SUCCESS = "Candidate Document Deleted Successfully.";
	public static final String CANDIDATE_DOC_DEL_FAIL = "Candidate Document Delete Failed.";
	public static final String CANDIDATE_RES_DEL_SUCCESS = "Candidate Resume Deleted Successfully.";
	public static final String CANDIDATE_RES_DEL_FAIL = "Candidate Resume Delete Failed.";
	public static final String CANDIDATE_MODULE = "Candidates";

	public static final String CANDIDATE_DOCUMENT_DELETE_SUCCESS = "Candidate Document Deleted Successfully.";
	public static final String CANDIDATE_DOCUMENT_DELETE_FAILED = "Candidate Document Deletion Failed.";

	public static final String SAVE_TERMINATION_DOC_SUCCESS = "Termination Documents Successfully Saved.";

	public static final String NEW_WARNING_SUCCESS = "Warning Successfully Saved.";
	public static final String NEW_PERFORMANCE_EVALUATION_SUCCESS = "Performance Evaluation Successfully Saved.";

	public static final String NO_WARNING_DETAILS = "No Warning Details Found.";
	public static final String NO_PERFORMANCE_EVALUATION_DETAILS = "No Performance Evaluation Details Found.";

	public static final String NEW_HOLIDAY_SUCCESS = "Holiday Successfully Saved.";

	public static final String EMPLOYEE_WORK_SHIFT_SUCCESS = "Work Shift Successfully Saved.";

	public static final String MEETING_SAVE_SUCCESS = "Meeting Saved Successfully.";
	public static final String MEETING_UPDATE_SUCCESS = "Meeting Updated Successfully.";
	public static final String MEETING_DOC_SUCCESS = "Document Updloaded Successfully.";
	public static final String MEETING_DOC_DELETE_SUCCESS = "Document Deleted Successfully.";
	public static final String MEETING_SAVE_FAILED = "Meeting Save Failed.";
	public static final String MEETING_DELETE_SUCCESS = "Meeting Deleted Successfully.";
	public static final String MEETING_DELETE_FAILED = "Meeting Delete Filed";

	public static final String LEAVE_SAVE_SUCCESS = "Leave Saved Successfully.";
	public static final String LEAVE_UPDATE_SUCCESS = "Leave Updated Successfully.";
	public static final String LEAVE_DOC_SUCCESS = "Document Updloaded Successfully.";
	public static final String LEAVE_DOC_DELETE_SUCCESS = "Document Deleted Successfully.";
	public static final String LEAVE_SAVE_FAILED = "Leave Save Failed.";
	public static final String LEAVE_DELETE_SUCCESS = "Leave Deleted Successfully.";
	public static final String LEAVE_DELETE_FAILED = "Leave Delete Filed";

	public static final String HOURLY_WAGES_SAVE_SUCCESS = "Hourly Wages Saved Successfully.";
	public static final String HOURLY_WAGES_UPDATE_SUCCESS = "Hourly Wages Updated Successfully.";
	public static final String HOURLY_WAGES_SAVE_FAILED = "Hourly Wages Save Failed.";
	public static final String HOURLY_WAGES_DELETE_SUCCESS = "Hourly Wages Deleted Successfully.";
	public static final String HOURLY_WAGES_DELETE_FAILED = "Hourly Wages Delete Failed";

	public static final String BONUS_SAVE_SUCCESS = "Bonus Saved Successfully.";
	public static final String BONUS_TITLE_SAVE_SUCCESS = "Bonus Title Saved Successfully.";
	public static final String BONUS_UPDATE_SUCCESS = "Bonus Updated Successfully.";
	public static final String BONUS_SAVE_FAILED = "Bonus Save Failed.";
	public static final String BONUS_TITLE_SAVE_FAILED = "Bonus Title Save Failed.";
	public static final String BONUS_DELETE_SUCCESS = "Bonus Deleted Successfully.";
	public static final String BONUS_DELETE_FAILED = "Bonus Delete Failed";

	public static final String DEDUCTION_SAVE_SUCCESS = "Deduction Saved Successfully.";
	public static final String DEDUCTION_TITLE_SAVE_SUCCESS = "Deduction Title Saved Successfully.";
	public static final String DEDUCTION_UPDATE_SUCCESS = "Deduction Updated Successfully.";
	public static final String DEDUCTION_SAVE_FAILED = "Deduction Save Failed.";
	public static final String DEDUCTION_TITLE_SAVE_FAILED = "Deduction Title Save Failed.";
	public static final String DEDUCTION_DELETE_SUCCESS = "Deduction Deleted Successfully.";
	public static final String DEDUCTION_DELETE_FAILED = "Deduction Delete Failed";

	public static final String COMMISSION_SAVE_SUCCESS = "Commission Saved Successfully.";
	public static final String COMMISSION_TITLE_SAVE_SUCCESS = "Commission Title Saved Successfully.";
	public static final String COMMISSION_UPDATE_SUCCESS = "Commission Updated Successfully.";
	public static final String COMMISSION_SAVE_FAILED = "Commission Save Failed.";
	public static final String COMMISSION_TITLE_SAVE_FAILED = "Commission Title Save Failed.";
	public static final String COMMISSION_DELETE_SUCCESS = "Commission Deleted Successfully.";
	public static final String COMMISSION_DELETE_FAILED = "Commission Delete Failed";

	public static final String ADJUSTMENT_SAVE_SUCCESS = "Adjustment Saved Successfully.";
	public static final String ADJUSTMENT_TITLE_SAVE_SUCCESS = "Adjustment Title Saved Successfully.";
	public static final String ADJUSTMENT_UPDATE_SUCCESS = "Adjustment Updated Successfully.";
	public static final String ADJUSTMENT_SAVE_FAILED = "Adjustment Save Failed.";
	public static final String ADJUSTMENT_TITLE_SAVE_FAILED = "Adjustment Title Save Failed.";
	public static final String ADJUSTMENT_DELETE_SUCCESS = "Adjustment Deleted Successfully.";
	public static final String ADJUSTMENT_DELETE_FAILED = "Adjustment Delete Failed";

	public static final String REIMBURSEMENTS_SAVE_SUCCESS = "Reimbursement Saved Successfully.";
	public static final String REIMBURSEMENTS_TITLE_SAVE_SUCCESS = "Reimbursement Title Saved Successfully.";
	public static final String REIMBURSEMENTS_UPDATE_SUCCESS = "Reimbursement Updated Successfully.";
	public static final String REIMBURSEMENTS_SAVE_FAILED = "Reimbursement Save Failed.";
	public static final String REIMBURSEMENTS_TITLE_SAVE_FAILED = "Reimbursement Title Save Failed.";
	public static final String REIMBURSEMENTS_DELETE_SUCCESS = "Reimbursement Deleted Successfully.";
	public static final String REIMBURSEMENTS_DELETE_FAILED = "Reimbursement Delete Failed";
	public static final String REIMBURSEMENTS_STATUS_SUCCESS = "Reimbursement Status Saved Successfully.";
	public static final String REIMBURSEMENTS_STATUS_FAILED = "Reimbursement Status Save Failed.";
	public static final String REIMBURSEMENTS_DOCUMENTS_SUCCESS = "Reimbursement Documents Upload Successfully.";
	public static final String REIMBURSEMENTS_DOCUMENTS_FAILED = "Reimbursement Documents Upload Failed.";

	public static final String TRAVEL_SAVE_SUCCESS = "Travel Saved Successfully.";
	public static final String TRAVEL_UPDATE_SUCCESS = "Travel Updated Successfully.";
	public static final String TRAVEL_SAVE_FAILED = "Travel Save Failed.";
	public static final String TRAVEL_DELETE_SUCCESS = "Travel Deleted Successfully.";
	public static final String TRAVEL_DELETE_FAILED = "Travel Delete Failed";
	public static final String TRAVEL_STATUS_SUCCESS = "Travel Status Saved Successfully.";
	public static final String TRAVEL_STATUS_FAILED = "Travel Status Save Failed.";
	public static final String TRAVEL_DOCUMENTS_SUCCESS = "Travel Documents Upload Successfully.";
	public static final String TRAVEL_DOCUMENTS_FAILED = "Travel Documents Upload Failed.";
	public static final String TRAVEL_DOCUMENTS_DELETE_SUCCESS = "Travel Documents Delete Successfully.";
	public static final String TRAVEL_DOCUMENTS_DELETE_FAILED = "Travel Documents Delete Failed.";

	public static final String TRANSFER_SAVE_SUCCESS = "Transfer Saved Successfully.";
	public static final String TRANSFER_UPDATE_SUCCESS = "Transfer Updated Successfully.";
	public static final String TRANSFER_SAVE_FAILED = "Transfer Save Failed.";
	public static final String TRANSFER_DELETE_SUCCESS = "Transfer Deleted Successfully.";
	public static final String TRANSFER_DELETE_FAILED = "Transfer Delete Failed";
	public static final String TRANSFER_STATUS_SUCCESS = "Transfer Status Saved Successfully.";
	public static final String TRANSFER_STATUS_FAILED = "Transfer Status Save Failed.";
	public static final String TRANSFER_DOCUMENTS_SUCCESS = "Transfer Documents Upload Successfully.";
	public static final String TRANSFER_DOCUMENTS_FAILED = "Transfer Documents Upload Failed.";
	public static final String TRANSFER_DOCUMENTS_DELETE_SUCCESS = "Transfer Documents Delete Successfully.";
	public static final String TRANSFER_DOCUMENTS_DELETE_FAILED = "Transfer Documents Delete Failed.";

	public static final String RESIGNATION_SAVE_SUCCESS = "Resignation Saved Successfully.";
	public static final String RESIGNATION_UPDATE_SUCCESS = "Resignation Updated Successfully.";
	public static final String RESIGNATION_SAVE_FAILED = "Resignation Save Failed.";
	public static final String RESIGNATION_DELETE_SUCCESS = "Resignation Deleted Successfully.";
	public static final String RESIGNATION_DELETE_FAILED = "Resignation Delete Failed";
	public static final String RESIGNATION_STATUS_SUCCESS = "Resignation Status Saved Successfully.";
	public static final String RESIGNATION_STATUS_FAILED = "Resignation Status Save Failed.";
	public static final String RESIGNATION_DOCUMENTS_SUCCESS = "Resignation Documents Upload Successfully.";
	public static final String RESIGNATION_DOCUMENTS_FAILED = "Resignation Documents Upload Failed.";
	public static final String RESIGNATION_DOCUMENTS_DELETE_SUCCESS = "Resignation Documents Delete Successfully.";
	public static final String RESIGNATION_DOCUMENTS_DELETE_FAILED = "Resignation Documents Delete Failed.";

	public static final String REIMBURSEMENTS_DOCUMENTS_DELETE_SUCCESS = "Reimbursement Documents Deleted Successfully.";
	public static final String REIMBURSEMENTS_DOCUMENTS_DELETE_FAILED = "Reimbursement Documents Delete Failed.";

	public static final String ATTENDANCE_SAVE_SUCCESS = "Attendance Saved Successfully.";
	public static final String ATTENDANCE_UPDATE_SUCCESS = "Attendance Updated Successfully.";
	public static final String ATTENDANCE_DELETE_SUCCESS = "Attendance Deleted Successfully.";
	public static final String ATTENDANCE_DELETE_FAILED = "Attendance Delete Failed.";
	public static final String ATTENDANCE_SAVE_FAILED = "Attendance Save Failed.";

	public static final String DAILY_WAGES_SAVE_SUCCESS = "Daily Wages Saved Successfully.";
	public static final String DAILY_WAGES_UPDATE_SUCCESS = "Daily Wages Updated Successfully.";
	public static final String DAILY_WAGES_SAVE_FAILED = "Daily Wages Save Failed.";
	public static final String DAILY_WAGES_DELETE_SUCCESS = "Daily Wages Deleted Successfully.";
	public static final String DAILY_WAGES_DELETE_FAILED = "Daily Wages Delete Failed";

	public static final String NEW_MEMO_SUCCESS = "Memo Successfully Saved.";
	public static final String NO_MEMO_DETAILS = "No Memo Details Found.";

	public static final String NEW_PAYROLL_OPTIONS_SUCCESS = "Payroll Options Successfully Saved.";

	public static final String NEW_ESI_SUCCESS = "ESI Successfully Saved.";
	public static final String NEW_PROFIDENT_FUND_SUCCESS = "Provident Fund Successfully Saved.";
	public static final String NEW_ADVANCE_SALARY_SUCCESS = "Advance Salary Successfully Saved.";
	public static final String UPDATE_ADVANCE_SALARY_SUCCESS = "Advance Salary Status Successfully Updated.";
	public static final String UPDATE_OVERTIME_SUCCESS = "Overtime Status Successfully Updated.";
	public static final String UPDATE_LOAN_SUCCESS = "Loan Status Successfully Updated.";
	public static final String NEW_OVERTIME_SUCCESS = "Overtime Successfully Saved.";
	public static final String NEW_LOAN_SUCCESS = "Loan Successfully Saved.";

	public static final String NEW_INSURANCE_SUCCESS = "Insurance Successfully Saved.";
	public static final String NO_INSURANCE_DETAILS = "No Insurance Details Found.";

	public static final String WORKSHEET_SAVE_SUCCESS = "Work Sheet Successfully Saved.";
	public static final String WORKSHEET_SAVE_FAILED = "WorkSheet Save Failed.";
	public static final String WORKSHEET_UPDATE_SUCCESS = "Work SheetSuccessfully Updated.";
	public static final String WORKSHEET_UPDATE_FAILED = "Work Sheet Update Failed .";
	public static final String WORKSHEET_DELETE_SUCCESS = "Work Sheet Successfully Deleted.";
	public static final String WORKSHEET_DELETE_FAILED = "Work Sheet Delete Failed.";
	public static final String WORKSHEET_DOC_SAVE_SUCCESS = "Work Sheet Document Successfully Saved.";
	public static final String WORKSHEET_DOC_SAVE_FAILED = "Work Sheet Document Save Failed.";
	public static final String WORKSHEET_DOC_DEL_SUCCESS = "Work Sheet Document Successfully Deleted.";
	public static final String WORKSHEET_DOC_DEL_FAILED = "Work Sheet Document Deletion Failed.";
	public static final String WORKSHEET_MODULE = "WorkSheet";
	
	public static final String BRANCH_MODULE="Branch";
	
	public static final String NEW_ORGANIZATION_POLICY_SUCCESS = "Organization Policy Successfully Saved.";
	public static final String AUTO_APPROVAL_SUCCESS = "Employee Auto Approval Status Successfully Updated.";
}
