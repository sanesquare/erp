package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.lucene.analysis.CharArrayMap.EntrySet;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.PayRollConstants;
import com.hrms.web.constants.PaySlipConstants;
import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AdjustmentDao;
import com.hrms.web.dao.AdvanceSalaryDao;
import com.hrms.web.dao.AttendanceDao;
import com.hrms.web.dao.BonusDao;
import com.hrms.web.dao.CommissionDao;
import com.hrms.web.dao.DeductionDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.EsiDao;
import com.hrms.web.dao.ForwardApplicationStatusDao;
import com.hrms.web.dao.InsuranceDao;
import com.hrms.web.dao.LoanDao;
import com.hrms.web.dao.LwfDao;
import com.hrms.web.dao.OvertimeDao;
import com.hrms.web.dao.PayRollOptionDao;
import com.hrms.web.dao.PayslipDao;
import com.hrms.web.dao.ProfesionalTaxDao;
import com.hrms.web.dao.ProfidentFundDao;
import com.hrms.web.dao.ReimbursementDao;
import com.hrms.web.dao.SalaryDao;
import com.hrms.web.dto.EsiShare;
import com.hrms.web.dto.PFShare;
import com.hrms.web.entities.Adjustments;
import com.hrms.web.entities.AdvanceSalary;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.EmployeeLeave;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.entities.Loan;
import com.hrms.web.entities.Payslip;
import com.hrms.web.entities.PayslipSavedItems;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EsiSyntaxService;
import com.hrms.web.service.LeaveService;
import com.hrms.web.service.PaySlipItemService;
import com.hrms.web.service.ProfidentFundSyntaxService;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.PayRollCalculator;
import com.hrms.web.vo.AdjustmentVo;
import com.hrms.web.vo.AdvanceSalaryVo;
import com.hrms.web.vo.BonusVo;
import com.hrms.web.vo.CommissionVo;
import com.hrms.web.vo.DeductionVo;
import com.hrms.web.vo.EmployeeLeaveVo;
import com.hrms.web.vo.EsiSyntaxVo;
import com.hrms.web.vo.EsiVo;
import com.hrms.web.vo.InsuranceVo;
import com.hrms.web.vo.LeavesVo;
import com.hrms.web.vo.LoanVo;
import com.hrms.web.vo.LwfVo;
import com.hrms.web.vo.OvertimeVo;
import com.hrms.web.vo.PayRollOptionVo;
import com.hrms.web.vo.PayRollResultVo;
import com.hrms.web.vo.PaySlipItemVo;
import com.hrms.web.vo.PaysSlipVo;
import com.hrms.web.vo.PayslipEmployeeItemsVo;
import com.hrms.web.vo.PayslipEmployeeItemsVoListVo;
import com.hrms.web.vo.ProfesionalTaxVo;
import com.hrms.web.vo.ProfidentFundSyntaxVo;
import com.hrms.web.vo.ProfidentFundVo;
import com.hrms.web.vo.ReimbursementsVo;
import com.hrms.web.vo.SalaryMonthlyVo;
import com.hrms.web.vo.SalaryPayslipItemsVo;
import com.hrms.web.vo.PayslipReportItemsVo;

/**
 * 
 * @author Shamsheer
 * @since 29-April-2015
 */
@Repository
public class PayslipDaoImpl extends AbstractDao implements PayslipDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private SalaryDao salaryDao;
	
	@Autowired
	private AttendanceDao attendanceDao;

	@Autowired
	private DeductionDao deductionDao;

	@Autowired
	private BonusDao bonusDao;

	@Autowired
	private LeaveService leaveService;

	@Autowired
	private PaySlipItemService paySlipItemService;

	@Autowired
	private ProfidentFundSyntaxService profidentFundSyntaxService;

	@Autowired
	private EsiSyntaxService esiSyntaxService;

	@Autowired
	private CommissionDao commissionDao;

	@Autowired
	private AdjustmentDao adjustmentDao;

	@Autowired
	private ReimbursementDao reimbursementDao;

	@Autowired
	private PayRollOptionDao payRollOptionsDao;

	@Autowired
	private EsiDao esiDao;

	@Autowired
	private OvertimeDao overtimeDao;

	@Autowired
	private LoanDao loanDao;

	@Autowired
	private AdvanceSalaryDao advanceSalaryDao;

	@Autowired
	private ProfidentFundDao pfDao;

	@Autowired
	private InsuranceDao insuranceDao;

	@Autowired
	private LwfDao lwfDao;

	@Autowired
	private ProfesionalTaxDao professionalTaxDao;

	@Log
	private Logger logger;

	@Autowired
	private ForwardApplicationStatusDao statusDao;

	/**
	 * method to get employees payslip details
	 */
	public List<PaysSlipVo> getPayslipDetailsByEmployee(String employeeCode, String dateFrom, String dateTo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);

		List<PaysSlipVo> paysSlipVos = new ArrayList<PaysSlipVo>();
		Date startDate = DateFormatter.convertStringToDate(dateFrom);
		Date endDate = DateFormatter.convertStringToDate(dateTo);
		Calendar fromCalendar = Calendar.getInstance();
		Calendar toCalendar = Calendar.getInstance();
		fromCalendar.setTime(startDate);
		toCalendar.setTime(endDate);
		int diffYear = toCalendar.get(Calendar.YEAR) - fromCalendar.get(Calendar.YEAR);
		int diffMonth = diffYear * 12 + toCalendar.get(Calendar.MONTH) - fromCalendar.get(Calendar.MONTH);
		int year = fromCalendar.get(Calendar.YEAR);
		List<String> dates = new ArrayList<String>();
		int k = 0;
		int d = fromCalendar.get(Calendar.MONTH);
		for (int i = 0; i <= diffMonth; i++) {
			if (d > 11) {
				year++;
				d = 0;
			}
			if (k != 0) {
				String newdate = "01/" + String.valueOf(d + 1) + "/" + year;
				dates.add(newdate);
			} else {
				dates.add(dateFrom);
			}
			k++;
			d++;
		}

		for (int i = 0; i < dates.size(); i++) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(DateFormatter.convertStringToDate(dates.get(i)));
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date todate = calendar.getTime();
			Date fromdate = null;
			if (i == 0) {
				fromdate = DateFormatter.convertStringToDate(dateFrom);
			} else {
				fromdate = DateFormatter.convertStringToDate(dates.get(i));
			}

			PayslipReportItemsVo payslipReportItemsVo = findSlary(employee, fromdate, todate);
			for (PaysSlipVo paysSlipVo : payslipReportItemsVo.getPaysSlipVos()) {
				if (paysSlipVo != null)
					paysSlipVos.add(paysSlipVo);
			}
		}

		return paysSlipVos;
	}

	/**
	 * 
	 * @param employeeCode
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings("static-access")
	private PayslipReportItemsVo findSlary(Employee employee, Date fromDate, Date toDate) {
		PayslipReportItemsVo payslipReportItemsVo = new PayslipReportItemsVo();
		// PaysSlipVo paysSlipVo = null;
		SalaryMonthlyVo salaryVo = salaryDao.findSalaryForDate(DateFormatter.convertDateToString(fromDate),
				employee.getEmployeeCode());
		if (salaryVo != null) {
			Date withEffectFromFromForDate = DateFormatter.convertStringToDate(salaryVo.getWithEffectFrom());
			Date withEffectFromList = null;

			List<SalaryMonthlyVo> salaryMonthlyVos = salaryDao.getMonthlySalariesByEmployeeStartDateEndDate(
					employee.getId(), fromDate, toDate);
			if (salaryMonthlyVos.size() != 0) {
				if (withEffectFromFromForDate.compareTo(DateFormatter.convertStringToDate(salaryMonthlyVos.get(0)
						.getWithEffectFrom())) != 0)
					withEffectFromList = DateFormatter.convertStringToDate(salaryMonthlyVos.get(0).getWithEffectFrom());
			}

			List<Date> dates = new ArrayList<Date>();
			dates.add(fromDate);
			for (SalaryMonthlyVo salaryMonthlyVo : salaryMonthlyVos) {
				dates.add(DateFormatter.convertStringToDate(salaryMonthlyVo.getWithEffectFrom()));
			}
			dates.add(toDate);

			for (int i = 1; i < dates.size(); i++) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(dates.get(i));
				calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
				int month = calendar.get(Calendar.MONTH);
				Date monthEndDate = calendar.getTime();
				Calendar cal = Calendar.getInstance();
				cal.setTime(dates.get(i - 1));
				cal.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));
				Date monthStartDate = cal.getTime();

				if (dates.get(i - 1).compareTo(dates.get(i)) != 0) {
					if (withEffectFromFromForDate.compareTo(dates.get(i)) == -1) {
						PaysSlipVo paysSlipVo = new PaysSlipVo();
						PayRollOptionVo payRollOptionVo = payRollOptionsDao.findPayRollOption();
						SalaryMonthlyVo firstSalary = salaryDao.findSalaryForDate(
								DateFormatter.convertDateToString(dates.get(i - 1)), employee.getEmployeeCode());
						// esi & pf starts
						PFShare pfShare = null;
						EsiShare esiShare = null;

						List<PaySlipItemVo> paySlipItemVos = paySlipItemService.findAllPaySlipItem();
						if (paySlipItemVos != null) {
							PayRollCalculator calculator = new PayRollCalculator();
							Map<String, String> components = calculator.getPayRollComponents(paySlipItemVos);
							ProfidentFundSyntaxVo profidentFundSyntaxVo = profidentFundSyntaxService
									.findProfidentFundSyntax();
							if (profidentFundSyntaxVo != null) {
								pfShare = PayRollCalculator.getPFShare(
										new BigDecimal(firstSalary.getMonthlyGrossSalary()),
										profidentFundSyntaxVo.getEmployeeSyntax(),
										profidentFundSyntaxVo.getEmployerSyntax(), components);
							}
						}

						EsiSyntaxVo esiSyntaxVo = esiSyntaxService.findEsiSyntax();
						if (esiSyntaxVo != null) {
							esiShare = PayRollCalculator.getESIShare(
									new BigDecimal(firstSalary.getMonthlyGrossSalary()),
									esiSyntaxVo.getEmployeeSyntax(), esiSyntaxVo.getEmployerSyntax(), esiSyntaxVo);
						}

						LwfVo lwfVo = null;
						lwfVo = lwfDao.findLwf();

						ProfesionalTaxVo profesionalTaxVo = null;
						profesionalTaxVo = professionalTaxDao.findProfesionalTax();

						// esi & pf ends

						List<PayslipEmployeeItemsVo> items = new ArrayList<PayslipEmployeeItemsVo>();
						BigDecimal totall = new BigDecimal(0);

						// gross salary
						PayslipEmployeeItemsVo gross = new PayslipEmployeeItemsVo();
						gross.setAmount(new BigDecimal(firstSalary.getMonthlyGrossSalary()).setScale(2,
								RoundingMode.HALF_UP));
						gross.setTitle(PayRollConstants.GROSS_SALARY);
						items.add(gross);

						// Tax
						PayslipEmployeeItemsVo taxMonthly = new PayslipEmployeeItemsVo();
						taxMonthly.setAmount(new BigDecimal(firstSalary.getMonthlyTaxDeduction()).setScale(2,
								RoundingMode.HALF_UP));
						taxMonthly.setTitle(PaySlipConstants.TAX);
						items.add(taxMonthly);

						// per day calculation ... fetching days
						BigDecimal totalDays = new BigDecimal(30);
						BigDecimal grossForDay = new BigDecimal(0);
						if (payRollOptionVo != null
								&& payRollOptionVo.getPerDaySalaryCalculation().equalsIgnoreCase("true")
								&& (payRollOptionVo.getPerDaySalaryCalculationDays() != 0)) {
							totalDays = new BigDecimal(payRollOptionVo.getPerDaySalaryCalculationDays());
							grossForDay = new BigDecimal(firstSalary.getMonthlyGrossSalary()).divide(totalDays, 2,
									RoundingMode.HALF_UP);
						} else {
							Calendar monthdays = Calendar.getInstance();
							monthdays.setTime(dates.get(i - 1));
							totalDays = new BigDecimal(monthdays.getActualMaximum(Calendar.DAY_OF_MONTH));
							grossForDay = new BigDecimal(firstSalary.getMonthlyGrossSalary()).divide(totalDays, 2,
									RoundingMode.HALF_UP);
						}
						
						paysSlipVo.setPerDayCalculation(grossForDay);
						paysSlipVo.setTotalWorkingDays((long) totalDays.intValue());
						// lop calculation
						Map<BigDecimal, BigDecimal> lopCalculation = lopCalculation(employee,
								DateFormatter.convertDateToString(dates.get(i - 1)),
								DateFormatter.convertDateToString(dates.get(i)), grossForDay);
						for (Map.Entry<BigDecimal, BigDecimal> entry : lopCalculation.entrySet()) {
							// lop count vo
							PayslipEmployeeItemsVo lopCountVo = new PayslipEmployeeItemsVo();
							lopCountVo.setAmount(entry.getKey());
							lopCountVo.setTitle(PaySlipConstants.LOP_COUNT);
							items.add(lopCountVo);

							// lop deduction vo
							PayslipEmployeeItemsVo lopDeductionVo = new PayslipEmployeeItemsVo();
							lopDeductionVo.setAmount(entry.getValue());
							lopDeductionVo.setTitle(PaySlipConstants.LOP_DEDUCTION);
							items.add(lopDeductionVo);
						}

						if (firstSalary.getItemsVos() != null) {
							if (monthStartDate.compareTo(dates.get(i - 1)) != 0
									|| monthEndDate.compareTo(dates.get(i)) != 0) {
								Long diffInMilli = dates.get(i).getTime() - dates.get(i - 1).getTime();
								Long daysDiff = TimeUnit.DAYS.convert(diffInMilli, TimeUnit.MILLISECONDS);

								BigDecimal days = new BigDecimal(daysDiff);

								PayslipEmployeeItemsVo itemm = new PayslipEmployeeItemsVo();
								BigDecimal tax = new BigDecimal(firstSalary.getMonthlyTaxDeduction()).divide(totalDays,
										2, RoundingMode.HALF_UP);
								tax = tax.multiply(days);
								itemm.setAmount(tax);
								itemm.setTitle(PaySlipConstants.TAX);
								items.add(itemm);
								BigDecimal sum = new BigDecimal(firstSalary.getMonthlyTaxDeduction()).divide(totalDays,
										2, RoundingMode.HALF_UP);
								sum = sum.multiply(days);
								totall = totall.subtract(sum);
								PayslipEmployeeItemsVoListVo employeeItemsVoListVo = findItemsByEmployeeBetweenDates(
										employee, dates.get(i - 1), dates.get(i));
								for (SalaryPayslipItemsVo vo : firstSalary.getItemsVos()) {
									PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
									BigDecimal amount = vo.getAmount().divide(totalDays, 2, RoundingMode.HALF_UP);
									amount = amount.multiply(days);
									// item.setAmount(vo.getAmount().divide(days));
									item.setAmount(amount);
									BigDecimal summ = vo.getAmount().divide(totalDays, 2, RoundingMode.HALF_UP);
									summ = summ.multiply(days);
									totall = totall.add(summ.setScale(2, RoundingMode.HALF_UP));
									item.setTitle(vo.getItem());
									items.add(item);
								}

								// setting esi & pf
								if (esiShare != null) {
									PayslipEmployeeItemsVo pfEmployer = new PayslipEmployeeItemsVo();
									BigDecimal employerAmount = esiShare.getEmployerShare().divide(totalDays, 2,
											RoundingMode.HALF_UP);
									employerAmount = employerAmount.multiply(days);
									pfEmployer.setAmount(employerAmount.setScale(2, RoundingMode.HALF_UP));
									pfEmployer.setTitle(PaySlipConstants.ESI_EMPLOYER);
									items.add(pfEmployer);

									PayslipEmployeeItemsVo vo = new PayslipEmployeeItemsVo();
									vo.setTitle(PaySlipConstants.ESI);
									BigDecimal amount = esiShare.getEmployeeShare().divide(totalDays, 2,
											RoundingMode.HALF_UP);
									amount = amount.multiply(days);
									vo.setAmount(amount.setScale(2, RoundingMode.HALF_UP));
									totall = totall.subtract(amount);
									items.add(vo);
								}
								if (pfShare != null) {
									PayslipEmployeeItemsVo pfEmployer = new PayslipEmployeeItemsVo();
									BigDecimal amount1 = pfShare.getEmployerShare().divide(totalDays, 2,
											RoundingMode.HALF_UP);
									amount1 = amount1.multiply(days);
									pfEmployer.setAmount(amount1.setScale(2, RoundingMode.HALF_UP));
									pfEmployer.setTitle(PaySlipConstants.PF_EMPLOYER);
									items.add(pfEmployer);

									PayslipEmployeeItemsVo vo = new PayslipEmployeeItemsVo();
									vo.setTitle(PaySlipConstants.PROVIDENT_FUND);
									BigDecimal amount = pfShare.getEmployeeShare().divide(totalDays, 2,
											RoundingMode.HALF_UP);
									amount = amount.multiply(days);

									vo.setAmount(amount.setScale(2, RoundingMode.HALF_UP));
									totall = totall.subtract(amount);
									items.add(vo);
								}

								// esi & pf ends

								// lwf
								if (lwfVo != null) {
									PayslipEmployeeItemsVo vo = new PayslipEmployeeItemsVo();
									vo.setTitle(PaySlipConstants.LWF);
									BigDecimal amount = new BigDecimal(lwfVo.getAmount()).divide(totalDays, 2,
											RoundingMode.HALF_UP);
									amount = amount.multiply(days);
									vo.setAmount(amount.setScale(2, RoundingMode.HALF_UP));
									totall = totall.subtract(amount);
									items.add(vo);
								}

								// professional tax
								if (month == 2 || month == 8) {
									if (profesionalTaxVo != null) {
										PayslipEmployeeItemsVo vo = new PayslipEmployeeItemsVo();
										vo.setTitle(PaySlipConstants.PROFESSIONAL_TAX);
										BigDecimal amount = new BigDecimal(profesionalTaxVo.getAmount()).divide(
												totalDays, 2, RoundingMode.HALF_UP);
										amount = amount.multiply(days);
										vo.setAmount(amount.setScale(2, RoundingMode.HALF_UP));
										totall = totall.subtract(amount);
										items.add(vo);
									}
								}

								// lop calculation
								/*
								 * Map<BigDecimal, BigDecimal> lopCalculation =
								 * lopCalculation(employee,
								 * DateFormatter.convertDateToString(fromDate),
								 * DateFormatter.convertDateToString(toDate),
								 * grossForDay); for (Map.Entry<BigDecimal,
								 * BigDecimal> entry :
								 * lopCalculation.entrySet()) { // lop count vo
								 * PayslipEmployeeItemsVo lopCountVo = new
								 * PayslipEmployeeItemsVo();
								 * lopCountVo.setAmount(entry.getKey());
								 * lopCountVo
								 * .setTitle(PaySlipConstants.LOP_COUNT);
								 * items.add(lopCountVo);
								 * 
								 * // lop deduction vo PayslipEmployeeItemsVo
								 * lopDeductionVo = new
								 * PayslipEmployeeItemsVo();
								 * lopDeductionVo.setAmount(entry.getValue());
								 * lopDeductionVo
								 * .setTitle(PaySlipConstants.LOP_DEDUCTION);
								 * items.add(lopDeductionVo); }
								 */

								paysSlipVo.setItems(items);
								paysSlipVo.setFromDate(DateFormatter.convertDateToString(dates.get(i - 1)));
								paysSlipVo.setToDate(DateFormatter.convertDateToString(dates.get(i)));
								totall = totall.add(employeeItemsVoListVo.getTotal());
								totall.setScale(2, RoundingMode.HALF_EVEN);
								paysSlipVo.setTotal(totall);
								payslipReportItemsVo.getPaysSlipVos().add(paysSlipVo);
							} else {
								for (SalaryPayslipItemsVo vo : firstSalary.getItemsVos()) {
									PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
									item.setAmount(vo.getAmount().setScale(2, RoundingMode.HALF_UP));
									totall = totall.add(vo.getAmount());
									item.setTitle(vo.getItem());
									items.add(item);
								}
								PayslipEmployeeItemsVo itemm = new PayslipEmployeeItemsVo();
								itemm.setAmount(new BigDecimal(firstSalary.getMonthlyTaxDeduction()));
								itemm.setTitle(PaySlipConstants.TAX);
								items.add(itemm);
								totall = totall.subtract(new BigDecimal(firstSalary.getMonthlyTaxDeduction()));
								PayslipEmployeeItemsVoListVo employeeItemsVoListVo = findItemsByEmployeeBetweenDates(
										employee, dates.get(i - 1), dates.get(i));
								for (PayslipEmployeeItemsVo vo : employeeItemsVoListVo.getItemsVos()) {
									items.add(vo);
								}
								// setting esi & pf
								if (esiShare != null) {
									PayslipEmployeeItemsVo pfEmployer = new PayslipEmployeeItemsVo();
									pfEmployer.setAmount(esiShare.getEmployerShare().setScale(2, RoundingMode.HALF_UP));
									pfEmployer.setTitle(PaySlipConstants.ESI_EMPLOYER);
									items.add(pfEmployer);

									PayslipEmployeeItemsVo vo = new PayslipEmployeeItemsVo();
									vo.setTitle(PaySlipConstants.ESI);
									vo.setAmount(esiShare.getEmployeeShare().setScale(2, RoundingMode.HALF_UP));
									totall = totall.subtract(esiShare.getEmployeeShare());
									items.add(vo);
								}
								if (pfShare != null) {
									PayslipEmployeeItemsVo pfEmployer = new PayslipEmployeeItemsVo();
									pfEmployer.setAmount(pfShare.getEmployerShare().setScale(2, RoundingMode.HALF_UP));
									pfEmployer.setTitle(PaySlipConstants.PF_EMPLOYER);
									items.add(pfEmployer);

									PayslipEmployeeItemsVo vo = new PayslipEmployeeItemsVo();
									vo.setTitle(PaySlipConstants.PROVIDENT_FUND);
									vo.setAmount(pfShare.getEmployeeShare().setScale(2, RoundingMode.HALF_UP));
									totall = totall.subtract(pfShare.getEmployeeShare());
									items.add(vo);
								}

								// lwf
								if (lwfVo != null) {
									PayslipEmployeeItemsVo vo = new PayslipEmployeeItemsVo();
									vo.setTitle(PaySlipConstants.LWF);
									vo.setAmount(new BigDecimal(lwfVo.getAmount()).setScale(2, RoundingMode.HALF_UP));
									totall = totall.subtract(new BigDecimal(lwfVo.getAmount()));
									items.add(vo);
								}

								// professional tax
								if (month == 2 || month == 8) {
									if (profesionalTaxVo != null) {
										PayslipEmployeeItemsVo vo = new PayslipEmployeeItemsVo();
										vo.setTitle(PaySlipConstants.PROFESSIONAL_TAX);
										vo.setAmount(new BigDecimal(profesionalTaxVo.getAmount()).setScale(2,
												RoundingMode.HALF_UP));
										totall = totall.subtract(new BigDecimal(profesionalTaxVo.getAmount()));
										items.add(vo);
									}
								}

								// esi & pf ends

								/*
								 * // lop calculation Map<BigDecimal,
								 * BigDecimal> lopCalculation =
								 * lopCalculation(employee,
								 * DateFormatter.convertDateToString(fromDate),
								 * DateFormatter.convertDateToString(toDate),
								 * grossForDay); for (Map.Entry<BigDecimal,
								 * BigDecimal> entry :
								 * lopCalculation.entrySet()) { // lop count vo
								 * PayslipEmployeeItemsVo lopCountVo = new
								 * PayslipEmployeeItemsVo();
								 * lopCountVo.setAmount(entry.getKey());
								 * lopCountVo
								 * .setTitle(PaySlipConstants.LOP_COUNT);
								 * items.add(lopCountVo);
								 * 
								 * // lop deduction vo PayslipEmployeeItemsVo
								 * lopDeductionVo = new
								 * PayslipEmployeeItemsVo();
								 * lopDeductionVo.setAmount(entry.getValue());
								 * lopDeductionVo
								 * .setTitle(PaySlipConstants.LOP_DEDUCTION);
								 * items.add(lopDeductionVo); }
								 */

								paysSlipVo.setItems(items);
								paysSlipVo.setFromDate(DateFormatter.convertDateToString(dates.get(i - 1)));
								paysSlipVo.setToDate(DateFormatter.convertDateToString(dates.get(i)));
								totall = totall.add(employeeItemsVoListVo.getTotal());
								totall.setScale(2, RoundingMode.HALF_EVEN);
								paysSlipVo.setTotal(totall.setScale(2, RoundingMode.HALF_UP));
								payslipReportItemsVo.getPaysSlipVos().add(paysSlipVo);
							}
						}
					}
				}
			}
		}
		return payslipReportItemsVo;
	}

	/**
	 * method to calculate lop
	 * 
	 * @param employee
	 * @param fromDate
	 * @param toDate
	 */
	private Map<BigDecimal, BigDecimal> lopCalculation(Employee employee, String fromDate, String toDate,
			BigDecimal grossForDay) {
		Map<BigDecimal, BigDecimal> lopCalculation = new HashMap<BigDecimal, BigDecimal>();
		List<LeavesVo> leaves = leaveService.findApprovedLeavesBetweenDates(employee.getId(), fromDate, toDate);
		Double lopCount = (double) 0;
		if (leaves.size() > 0) {
			LeavesVo leav = leaves.get(leaves.size() - 1);
			if (leav.getLeaveBalance() < 0) {
				/*
				 * String lop = String.valueOf(leav.getLeaveBalance()); lopCount
				 * = Integer.parseInt(lop); lopCount = Math.abs(lopCount);
				 */
				lopCount = Double.parseDouble(String.valueOf(leav.getLeaveBalance()));
			}
			// List<EmployeeLeaveVo> empLeaves = new
			// ArrayList<EmployeeLeaveVo>();
			Set<EmployeeLeave> empLeaves = employee.getLeaves();
			Set<String> leavTypes = new HashSet<String>();
			if (empLeaves != null) {

				for (EmployeeLeave employeeLeaveVo : empLeaves) {
					if (employeeLeaveVo.getType().equalsIgnoreCase("Unpaid")) {
						leavTypes.add(employeeLeaveVo.getTitle().getType());
					}
				}
			}
			for (LeavesVo leavesVo : leaves) {
				if (leavTypes.contains(leavesVo.getLeaveType())) {
					lopCount += leavesVo.getDuration();
				} else if (leavesVo.getLeaveBalance() < 0) {
					//
				}
			}
		}
		Double absentCount = this.attendanceDao.getLopCount(employee, DateFormatter.convertStringToDate(fromDate),
				DateFormatter.convertStringToDate(toDate));
		lopCount = lopCount + absentCount;
		BigDecimal lopDeduction = grossForDay.multiply(new BigDecimal(lopCount)).setScale(2, RoundingMode.HALF_UP);
		lopCalculation.put(new BigDecimal(lopCount), lopDeduction);
		return lopCalculation;
	}

	/**
	 * method to find payslip items
	 * 
	 * @param employeeId
	 * @param fromDate
	 * @param endDate
	 * @return
	 */
	private PayslipEmployeeItemsVoListVo findItemsByEmployeeBetweenDates(Employee employee, Date fromDate, Date endDate) {
		PayslipEmployeeItemsVoListVo employeeItemsVoListVo = new PayslipEmployeeItemsVoListVo();
		List<PayslipEmployeeItemsVo> items = new ArrayList<PayslipEmployeeItemsVo>();
		BigDecimal totall = new BigDecimal(0);

		// setting deduction
		List<DeductionVo> deductionVos = deductionDao.listDeductionByEmployeeStartDateEndDate(employee, fromDate,
				endDate);

		if (!deductionVos.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.DEDUCTIONS);
			BigDecimal amount = new BigDecimal(0);
			for (DeductionVo deductionVo : deductionVos) {
				amount = amount.add(new BigDecimal(deductionVo.getAmount()));
			}
			totall = totall.subtract(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// setting bonuses
		List<BonusVo> bonusVos = bonusDao.listBonusesByEmployeeStartDateEndDate(employee, fromDate, endDate);
		if (!bonusVos.isEmpty()) {
			PayslipEmployeeItemsVo vo = new PayslipEmployeeItemsVo();
			vo.setTitle(PaySlipConstants.BONUSES);
			BigDecimal amount = new BigDecimal(0);
			for (BonusVo bonusVo : bonusVos) {
				amount = amount.add(new BigDecimal(bonusVo.getAmount()));
			}
			totall = totall.add(amount);
			vo.setAmount(amount);
			items.add(vo);
		}

		// setting commission
		List<CommissionVo> commissionVos = commissionDao.listCommissionesByEmployeeStartDateEndDate(employee, fromDate,
				endDate);
		if (!commissionVos.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.COMMISSIONS);
			BigDecimal amount = new BigDecimal(0);
			for (CommissionVo commissionVo : commissionVos) {
				amount = amount.add(new BigDecimal(commissionVo.getAmount()));
			}
			totall = totall.add(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// increment adjustments
		List<AdjustmentVo> adjustmentVos = adjustmentDao
				.listIncrementAdjustmentsByEmployee(employee, fromDate, endDate);
		if (!adjustmentVos.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.ADJUSTMENT_INCREMENT);
			BigDecimal amount = new BigDecimal(0);
			for (AdjustmentVo adjustments : adjustmentVos) {
				amount = amount.add(new BigDecimal(adjustments.getAmount()));
			}
			totall = totall.add(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// decrement adjustments
		List<AdjustmentVo> adjustmentVosDecre = adjustmentDao.listDecrementAdjustmentsByEmployee(employee, fromDate,
				endDate);
		if (!adjustmentVosDecre.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.ADJUSTMENT_DECREMENT);
			BigDecimal amount = new BigDecimal(0);
			for (AdjustmentVo adjustments : adjustmentVosDecre) {
				amount = amount.add(new BigDecimal(adjustments.getAmount()));
			}
			totall = totall.subtract(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// encashment adjustments
		List<AdjustmentVo> adjustmentVosEncahsments = adjustmentDao.listEncashmentAdjustmentsByEmployee(employee,
				fromDate, endDate);
		if (!adjustmentVosEncahsments.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.ADJUSTMENT_ENCASHMENT);
			BigDecimal amount = new BigDecimal(0);
			for (AdjustmentVo adjustments : adjustmentVosEncahsments) {
				amount = amount.add(new BigDecimal(adjustments.getAmount()));
			}
			totall = totall.add(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// reimbursements
		List<ReimbursementsVo> reimbursementsVos = reimbursementDao.listReimbursementsByEmployeeStartDateEndDate(
				employee, fromDate, endDate);
		if (!reimbursementsVos.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.REIMBURSEMENTS);
			BigDecimal amount = new BigDecimal(0);
			for (ReimbursementsVo vo : reimbursementsVos) {
				amount = amount.add(new BigDecimal(vo.getAmount()));
			}
			totall = totall.add(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// esi
		List<EsiVo> esiVos = esiDao.findAllEsiByEmployee(employee, fromDate, endDate);
		if (!esiVos.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.ESI);
			BigDecimal amount = new BigDecimal(0);
			for (EsiVo vo : esiVos) {
				amount = amount.add(vo.getEmployeeShare());
			}
			totall = totall.subtract(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// over time
		List<OvertimeVo> overtimeVos = overtimeDao.findOvertimeBetweenDates(employee.getId(), fromDate, endDate);
		if (!overtimeVos.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.OVERTIME);
			BigDecimal amount = new BigDecimal(0);
			for (OvertimeVo vo : overtimeVos) {
				amount = amount.add(vo.getOvertimeAmount());
			}
			totall = totall.add(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// advance salary
		List<AdvanceSalary> advanceSalaryVos = advanceSalaryDao.findApprovedAdvanceSalaryForSalary(
				DateFormatter.convertDateToString(endDate), employee);
		if (!overtimeVos.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.ADVANCE_SALARY);
			BigDecimal amount = new BigDecimal("0.00");
			for (AdvanceSalary vo : advanceSalaryVos) {
				// amount = amount.add(vo.getAdvanceAmount());
				BigDecimal balance = vo.getBalanceAmount();
				BigDecimal repaymentAmount = vo.getRepaymentAmount();
				BigDecimal remaining = balance.subtract(repaymentAmount);
				if (remaining.compareTo(new BigDecimal(0)) > 0) {
					amount = amount.add(repaymentAmount);
				} else {
					// pay balance ... change status
					amount = amount.add(balance);
				}
			}
			totall = totall.add(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// loan
		List<Loan> loanVos = loanDao.findApprovedLoanForSalary(DateFormatter.convertDateToString(endDate), employee);
		if (!loanVos.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.LOAN);
			BigDecimal amount = new BigDecimal(0);
			for (Loan vo : loanVos) {
				BigDecimal balance = vo.getBalanceAmount();
				BigDecimal repaymentAmount = vo.getRepaymentAmount();
				BigDecimal remaining = balance.subtract(repaymentAmount);
				if (remaining.compareTo(new BigDecimal(0)) > 0) {
					amount = amount.add(repaymentAmount);
				} else {
					// pay balance ... change status
					amount = amount.add(balance);
				}
			}
			totall = totall.subtract(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// p f
		List<ProfidentFundVo> profidentFundVos = pfDao
				.findProfidentFundBetweenDate(employee.getId(), fromDate, endDate);
		if (!overtimeVos.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.PROVIDENT_FUND);
			BigDecimal amount = new BigDecimal(0);
			for (ProfidentFundVo vo : profidentFundVos) {
				amount = amount.add(vo.getEmployeeShare());
			}
			totall = totall.subtract(amount);
			item.setAmount(amount);
			items.add(item);
		}

		// insurance
		List<InsuranceVo> insuranceVos = insuranceDao.findAllInsuranceByEmployee(employee, fromDate, endDate);
		if (!overtimeVos.isEmpty()) {
			PayslipEmployeeItemsVo item = new PayslipEmployeeItemsVo();
			item.setTitle(PaySlipConstants.INSURANCE);
			BigDecimal amount = new BigDecimal(0);
			for (InsuranceVo vo : insuranceVos) {
				amount = amount.add(vo.getEmployeeShare());
			}
			totall = totall.subtract(amount);
			item.setAmount(amount);
			items.add(item);
		}

		employeeItemsVoListVo.setItemsVos(items);
		employeeItemsVoListVo.setTotal(totall);
		return employeeItemsVoListVo;
	}

	/**
	 * method to save payslip
	 */
	public Long savePayslip(PaysSlipVo paysSlipVo) {
		Payslip payslip = new Payslip();
		Employee employee = employeeDao.findAndReturnEmployeeByCode(paysSlipVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		payslip.setEmployee(employee);
		payslip.setSalaryDate(DateFormatter.convertStringToDate(paysSlipVo.getSalaryDate()));
		payslip.setStartDate(DateFormatter.convertStringToDate(paysSlipVo.getFromDate()));
		payslip.setEndDate(DateFormatter.convertStringToDate(paysSlipVo.getToDate()));

		ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.REQUESTED);
		payslip.setStatus(status);
		/*
		 * payslip.setNotes(paysSlipVo.getNotes()); for (PayslipEmployeeItemsVo
		 * itemsVo : paysSlipVo.getItems()) { if (itemsVo.getAmount() != null)
		 * payslip.getItems().add(setPayslipItems(itemsVo, payslip)); }
		 * payslip.setTotalAmount(paysSlipVo.getTotal());
		 */
		userActivitiesService.saveUserActivity("Added new Payslip " + paysSlipVo.getEmployeeCode());
		return (Long) this.sessionFactory.getCurrentSession().save(payslip);
	}

	/**
	 * method to set items
	 * 
	 * @param itemsVos
	 * @param payslip
	 * @return
	 */
	/*
	 * private PayslipSavedItems setPayslipItems(PayslipEmployeeItemsVo itemsVo,
	 * Payslip payslip) { PayslipSavedItems item = new PayslipSavedItems();
	 * item.setAmount(itemsVo.getAmount()); item.setItem(itemsVo.getTitle());
	 * item.setPayslip(payslip); return item; }
	 */
	public void updatePayslip(PaysSlipVo paysSlipVo) {
		Payslip payslip = getPayslip(paysSlipVo.getId());
		Employee employee = employeeDao.findAndReturnEmployeeByCode(paysSlipVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		payslip.setEmployee(employee);
		payslip.setSalaryDate(DateFormatter.convertStringToDate(paysSlipVo.getSalaryDate()));
		payslip.setStartDate(DateFormatter.convertStringToDate(paysSlipVo.getStartDate()));
		payslip.setEndDate(DateFormatter.convertStringToDate(paysSlipVo.getEndDate()));
		/*
		 * payslip.setNotes(paysSlipVo.getNotes()); payslip.getItems().clear();
		 * for (PayslipEmployeeItemsVo itemsVo : paysSlipVo.getItems()) { if
		 * (itemsVo.getAmount() != null)
		 * payslip.getItems().add(setPayslipItems(itemsVo, payslip)); }
		 * payslip.setTotalAmount(paysSlipVo.getTotal());
		 */
		this.sessionFactory.getCurrentSession().merge(payslip);
		userActivitiesService.saveUserActivity("Updated Payslip " + paysSlipVo.getEmployeeCode());
	}

	/**
	 * method to get payslip
	 */
	public Payslip getPayslip(Long id) {
		return (Payslip) this.sessionFactory.getCurrentSession().createCriteria(Payslip.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * method to list all payslips
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<PaysSlipVo> listAllPayslips() {
		List<PaysSlipVo> paysSlipVos = new ArrayList<PaysSlipVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Payslip.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Payslip> payslips = criteria.list();
		for (Payslip payslip : payslips) {
			paysSlipVos.add(createPayslipVo(payslip));
		}
		return paysSlipVos;
	}

	/**
	 * method to create payslip vo
	 * 
	 * @param payslip
	 * @return
	 */
	private PaysSlipVo createPayslipVo(Payslip payslip) {
		PaysSlipVo vo = new PaysSlipVo();
		vo.setId(payslip.getId());
		vo.setStatus(payslip.getStatus().getName());
		vo.setStatusdescription(payslip.getStatusDescription());
		vo.setBranch(payslip.getEmployee().getBranch().getBranchName());
		vo.setDepartment(payslip.getEmployee().getDepartmentEmployees().getName());
		vo.setDesignation(payslip.getEmployee().getEmployeeDesignation().getDesignation());
		// vo.setTotal(payslip.getTotalAmount());
		vo.setEmployeeCode(payslip.getEmployee().getEmployeeCode());
		if (payslip.getEmployee().getUserProfile() != null)
			vo.setEmployee(payslip.getEmployee().getUserProfile().getFirstName() + " "
					+ payslip.getEmployee().getUserProfile().getLastname());
		vo.setSalaryDate(DateFormatter.convertDateToString(payslip.getSalaryDate()));
		vo.setCreatedBy(payslip.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(payslip.getCreatedOn()));
		vo.setEndDate(DateFormatter.convertDateToString(payslip.getEndDate()));
		vo.setStartDate(DateFormatter.convertDateToString(payslip.getStartDate()));
		// vo.setNotes(payslip.getNotes());
		// vo.setItems(createItemsVo(payslip.getItems()));
		return vo;
	}

	/**
	 * method to create items vo
	 * 
	 * @param items
	 * @return
	 */
	private List<PayslipEmployeeItemsVo> createItemsVo(Set<PayslipSavedItems> items) {
		List<PayslipEmployeeItemsVo> employeeItemsVos = new ArrayList<PayslipEmployeeItemsVo>();
		for (PayslipSavedItems item : items) {
			PayslipEmployeeItemsVo vo = new PayslipEmployeeItemsVo();
			vo.setAmount(item.getAmount());
			vo.setTitle(item.getItem());
			employeeItemsVos.add(vo);
		}
		return employeeItemsVos;
	}

	/**
	 * method to delete payslip
	 */
	public void deletePayslip(Long id) {
		Payslip payslip = getPayslip(id);
		if (payslip == null)
			throw new ItemNotFoundException("PaySlip Not Exist.");
		this.sessionFactory.getCurrentSession().delete(payslip);
	}

	/**
	 * method to get payslip by id
	 */
	public PaysSlipVo getPayslipById(Long id) {
		Payslip payslip = getPayslip(id);
		if (payslip == null)
			throw new ItemNotFoundException("PaySlip Not Exist.");
		return createPayslipVo(payslip);
	}

	/**
	 * method to find employees payslips between dates
	 * 
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PaysSlipVo> findPayslipOfEmployeeBetweenDates(Long employeeId, Date startDate, Date endDate) {
		ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.APPROVED);
		List<PaysSlipVo> paysSlipVos = new ArrayList<PaysSlipVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Payslip.class);
		criteria.add(Restrictions.eq("employee.id", employeeId));
		// criteria.add(Restrictions.ge("salaryDate",
		// startDate)).add(Restrictions.le("salaryDate", endDate));
		criteria.add(Restrictions.ge("startDate", startDate));
		criteria.add(Restrictions.le("endDate", endDate));
		criteria.add(Restrictions.eq("status", status));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Payslip> payslips = criteria.list();
		for (Payslip payslip : payslips) {
			// paysSlipVos.add(createPayslipVo(payslip));
			List<PaysSlipVo> list = getPayslipDetailsByEmployee(payslip.getEmployee().getEmployeeCode(),
					DateFormatter.convertDateToString(payslip.getStartDate()),
					DateFormatter.convertDateToString(payslip.getEndDate()));
			for (PaysSlipVo vo : list) {
				vo.setStatus(payslip.getStatus().getName());
				vo.setStatusdescription(payslip.getStatusDescription());
				paysSlipVos.add(vo);
			}
		}
		return paysSlipVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PaysSlipVo> findAllPayslipsByEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<PaysSlipVo> paysSlipVos = new ArrayList<PaysSlipVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Payslip.class);
		Criterion byEmployee = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression logicalExpression = Restrictions.or(byEmployee, createdBy);
		criteria.add(logicalExpression);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Payslip> payslips = criteria.list();
		for (Payslip payslip : payslips) {
			paysSlipVos.add(createPayslipVo(payslip));
		}
		return paysSlipVos;
	}

	public void updatePayslipStatus(PaysSlipVo paysSlipVo) {
		Payslip payslip = getPayslip(paysSlipVo.getId());
		if (payslip == null)
			throw new ItemNotFoundException("Payslip Not Exist.");
		ForwardApplicationStatus status = statusDao.getStatus(paysSlipVo.getStatus());
		payslip.setStatus(status);
		payslip.setStatusDescription(paysSlipVo.getStatusdescription());
		this.sessionFactory.getCurrentSession().merge(payslip);
	}

}
