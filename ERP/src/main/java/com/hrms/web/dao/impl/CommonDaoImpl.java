package com.hrms.web.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CommonDao;
import com.hrms.web.entities.MstReference;

@Repository
public class CommonDaoImpl extends AbstractDao implements CommonDao {

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<MstReference> fectMstreferneceByType(String refernceType) {
		Query query = this.sessionFactory.getCurrentSession().createQuery("from MstReference where reference_type=:refernceType");
		query.setParameter("refernceType",refernceType);
		List<MstReference> mstReferences= query.list();	
		return mstReferences;
	}

}
