package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.DepartmentDao;
import com.hrms.web.dao.EmployeeDesignationDao;
import com.hrms.web.dao.JobPostDao;
import com.hrms.web.dao.JobTypeDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.CandidateDocuments;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.ExperienceRequired;
import com.hrms.web.entities.JobCandidates;
import com.hrms.web.entities.JobPost;
import com.hrms.web.entities.JobPostDocuments;
import com.hrms.web.entities.WorkSheet;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.CandidateDocumentsVo;
import com.hrms.web.vo.JobPostDocumentsVo;
import com.hrms.web.vo.JobPostVo;
import com.hrms.web.vo.WorkSheetVo;

/**
 * 
 * @author Vinutha
 * @since 19-March-2015
 *
 */
@Repository
public class JobPostDaoImpl extends AbstractDao  implements JobPostDao  {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private DepartmentDao departmentDao;
	
	@Autowired
	private JobTypeDao jobTypeDao;
	
	@Autowired
	private BranchDao branchDao;
	
	
	
	/**
	 * method to save complete job post details
	 * @param jobPostVo
	 */
	public void saveJobPost(JobPostVo jobPostVo) {
		
		JobPost jobPost = new JobPost();
		jobPost.setDepartment(departmentDao.findDepartment(jobPostVo.getDepartmentId()));
		jobPost.setJobType(jobTypeDao.findJobType(jobPostVo.getJobTypeId()));
		jobPost.setJobTitle(jobPostVo.getJobTitle());
		if(jobPostVo.getPositions()!=null)
			jobPost.setPositions(Integer.parseInt(jobPostVo.getPositions()));
		if(jobPostVo.getAgeStart()!=null)
			jobPost.setAgeStart(Integer.parseInt(jobPostVo.getAgeStart()));
		if(jobPostVo.getAgeEnd()!=null)
			jobPost.setAgeEnd(Integer.parseInt(jobPostVo.getAgeEnd()));
		if(jobPostVo.getBranchIds()!=null)
			jobPost.setBranch(createBranches(jobPostVo.getBranchIds()));
		if(jobPostVo.getStartSalary()!=null)
			jobPost.setSalaryStartRange(Double.parseDouble(jobPostVo.getStartSalary()));
		if(jobPostVo.getEndSalary()!=null)
			jobPost.setSalaryEndRange(Double.parseDouble(jobPostVo.getEndSalary()));
		if(jobPostVo.getJobPostEndDate()!=null)
			jobPost.setClosingDate(DateFormatter.convertStringToDate(jobPostVo.getJobPostEndDate()));
		this.sessionFactory.getCurrentSession().save(jobPost);
		
	}
	/**
	 * Method to find branches
	 * @param ids
	 * @return
	 */
	private Set<Branch> createBranches(List<Long> ids)
	{
		Set<Branch> branches = new HashSet<Branch>();
		for(Long id : ids)
		{
			Branch branch = branchDao.findAndReturnBranchObjectById(id);
			if(branch!=null)	
				branches.add(branch);
		}
		return branches;
	}
	/**
	 * method to update job post
	 * @param jobPostVo 
	 */
	public void updateJobPost(JobPostVo jobPostVo) {
		JobPost jobPost= this.findAndReturnJobPostObjectById(jobPostVo.getJobPostId());
		jobPost.setJobType(jobTypeDao.findJobType(jobPostVo.getJobTypeId()));
		jobPost.setJobTitle(jobPostVo.getJobTitle());
		jobPost.setJobType(jobTypeDao.findJobType(jobPostVo.getJobTypeId()));
		if(jobPostVo.getPositions()!=null)
			jobPost.setPositions(Integer.parseInt(jobPostVo.getPositions()));
		if(jobPostVo.getAgeStart()!=null)
			jobPost.setAgeStart(Integer.parseInt(jobPostVo.getAgeStart()));
		if(jobPostVo.getAgeEnd()!=null)
			jobPost.setAgeEnd(Integer.parseInt(jobPostVo.getAgeEnd()));
		if(jobPostVo.getBranchIds()!=null)
			jobPost.setBranch(createBranches(jobPostVo.getBranchIds()));
		if(jobPostVo.getStartSalary()!=null)
			jobPost.setSalaryStartRange(Double.parseDouble(jobPostVo.getStartSalary()));
		if(jobPostVo.getEndSalary()!=null)
			jobPost.setSalaryEndRange(Double.parseDouble(jobPostVo.getEndSalary()));
		if(jobPostVo.getJobPostEndDate()!=null)
			jobPost.setClosingDate(DateFormatter.convertStringToDate(jobPostVo.getJobPostEndDate()));
		this.sessionFactory.getCurrentSession().merge(jobPost);
		
	}
	/**
	 * method to delete jobPost
	 * @param jobPostVo
	 */
	public void deleteJobPost(JobPostVo jobPostVo) {
		this.sessionFactory.getCurrentSession().delete(this.findAndReturnJobPostObjectById(jobPostVo.getJobPostId()));
		
	}
	/**
	 * method to delete jobPost by id 
	 * @param id
	 */
	public void deleteJobPostById(Long id) {
		this.sessionFactory.getCurrentSession().delete(this.findJobPostById(id));
		
	}
	/**
	 * method to list all job posts
	 * @return List<JobPostVo> 
	 */
	@SuppressWarnings("unchecked")
	public List<JobPostVo> listAllJobPost() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobPost.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobPost>jobPosts = criteria.list();
		List<JobPostVo>jobPostVos=new ArrayList<JobPostVo>();
		for(JobPost jobPost : jobPosts)
		{
			jobPostVos.add(createJobPostVo(jobPost));
		}
		return jobPostVos;
	}
	/**
	 * method to find job post Vo by id
	 * @param id
	 * @return jobPostVo
	 */
	public JobPostVo findJobPostById(Long jobPostId) {
		JobPost jobPost = (JobPost)this.sessionFactory.getCurrentSession().createCriteria(JobPost.class).add(Restrictions.eq("id", jobPostId)).uniqueResult();
		JobPostVo jobPostVo=createJobPostVo(jobPost);
		return jobPostVo;
	}
	/**
	 * method to find job post object by id
	 * @param id
	 * @return JobPost
	 */
	public JobPost findAndReturnJobPostObjectById(Long jobPostId) {
		
		return (JobPost)this.sessionFactory.getCurrentSession().createCriteria(JobPost.class).add(Restrictions.eq("id",jobPostId)).uniqueResult();
	}
	/**
	 * method to find job post by department id
	 * @param departmentId
	 * @return List<JobPostVo>
	 */
	public List<JobPostVo> findListOfJobPostByDepartmentId(Long departmentId) {
		List<JobPostVo> resultVoList = new ArrayList<JobPostVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobPost.class)
				.add(Restrictions.eq("department.id", departmentId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("closingDate"));
		List<JobPost> jobPosts = criteria.list();
		for(JobPost post : jobPosts)
		{
			resultVoList.add(createJobPostVo(post));
		}
		return resultVoList;
	}
	/**
	 * method to find list of job posts based on branch
	 * @param departmentId
	 * @return list<JobPostVo>
	 */
	public List<JobPostVo> findListOfJobPostByBranchId(Long branch)
	{
		List<JobPostVo> result = new ArrayList<JobPostVo>();
		List<JobPostVo> jobPosts = listAllJobPost();
		for(JobPostVo post : jobPosts)
		{
			for (Long branchId : post.getBranchIds()) {
				
				if(branchId==branch)
				{
					result.add(post);
					break;
				}
				
			}
		}
		return result;
	}
	/**
	 * method to create jobPostVo
	 * @param jobPost
	 * @return
	 */
	private JobPostVo createJobPostVo(JobPost jobPost){
		JobPostVo jobPostVo = new JobPostVo();
		jobPostVo.setJobPostId(jobPost.getId());
		jobPostVo.setJobTitle(jobPost.getJobTitle());
		jobPostVo.setAgeEnd(String.valueOf(jobPost.getAgeEnd()));
		jobPostVo.setAgeStart(String.valueOf(jobPost.getAgeStart()));
		jobPostVo.setJobType(jobPost.getJobType().getType());
		jobPostVo.setJobTypeId(jobPost.getJobType().getId());
		if(jobPost.getDepartment()!=null)
		{
			jobPostVo.setDepartmentId(jobPost.getDepartment().getId());
			jobPostVo.setDepartment(jobPost.getDepartment().getName());
		}
		jobPostVo.setStartSalary(String.valueOf(jobPost.getSalaryStartRange()));
		jobPostVo.setEndSalary(String.valueOf(jobPost.getSalaryEndRange()));
		//jobPostVo.setLocation(jobPost.getBranch().getBranchName());
		jobPostVo.setLocations(createBranchesVo(jobPost.getBranch()));
		jobPostVo.setBranchIds(createBranchIds(jobPost.getBranch()));
		jobPostVo.setCreatedBy(jobPost.getCreatedBy().getUserName());
		
		if(jobPost.getCreatedOn()!=null)
			jobPostVo.setCreatedOn(DateFormatter.convertDateToString(jobPost.getCreatedOn()));
		
		jobPostVo.setPositions(String.valueOf(jobPost.getPositions()));
		
		if(jobPost.getClosingDate()!=null)
			jobPostVo.setJobPostEndDate(DateFormatter.convertDateToString(jobPost.getClosingDate()));
		
		jobPostVo.setQualification(jobPost.getQualificationRequired());
		
		if(jobPost.getExperienceRequired()!=null){
			jobPostVo.setExperienceMonths(jobPost.getExperienceRequired().getMonths());
			jobPostVo.setExperienceYears(jobPost.getExperienceRequired().getYears());
		}
		jobPostVo.setPostAdditionalInfo(jobPost.getAdditionalInformation());
		jobPostVo.setPostDescription(jobPost.getDescription());
		
		if(jobPost.getJobPostDocuments()!=null)
		{
			Set<JobPostDocuments> documents= jobPost.getJobPostDocuments();
			Set<JobPostDocumentsVo> documentVos =new HashSet<JobPostDocumentsVo>();
			for(JobPostDocuments document : documents)
			{
				documentVos.add(createDocumentsVo(document));
			}
			jobPostVo.setJobPostDocumentsVos(documentVos);
		}
		
		return jobPostVo;
		
	}
	/**
	 * Method to create locations vo
	 * @param branches
	 * @return
	 */
	private List<String> createBranchesVo(Set<Branch> branches)
	{
		List<String> locations=new ArrayList<String>();
		for(Branch branch:branches)
		{
			locations.add(branch.getBranchName());
		}
		return locations;
	}
	/**
	 * Method to get set of branch ids
	 * @param branches
	 * @return
	 */
	private List<Long> createBranchIds(Set<Branch> branches)
	{
		List<Long> branchIds=new ArrayList<Long>();
		for(Branch branch:branches)
		{
			branchIds.add(branch.getId());
		}
		return branchIds;
	}
	
	/**
	 * method to create document vo
	 * @param documents
	 * @return
	 */
	private JobPostDocumentsVo createDocumentsVo(JobPostDocuments documents){
		JobPostDocumentsVo vo = new JobPostDocumentsVo();
		vo.setDocumentId(documents.getId());
		vo.setDocumentUrl(documents.getDocumentPath());
		vo.setFileName(documents.getFileName());
		return vo;
	}
	/**
	 * method to create experience required object
	 * @param jobPostVo
	 * @param jobPost
	 * @return
	 */
	private ExperienceRequired createAndReturnExperience(JobPostVo jobPostVo, JobPost jobPost ){
		ExperienceRequired expRequired=jobPost.getExperienceRequired();
		if(expRequired==null)
			expRequired=new ExperienceRequired();
		expRequired.setYears(jobPostVo.getExperienceYears());
		expRequired.setMonths(jobPostVo.getExperienceMonths());
		expRequired.setJobPost(jobPost);
		return expRequired;
	}
	/**
	 * method to save basic info
	 * @param jobPostVo
	 * @return Long id
	 */
	public Long saveBasicInfo(JobPostVo jobPostVo) {
		JobPost jobPost=new JobPost(); 
		jobPost.setDepartment(departmentDao.findDepartment(jobPostVo.getDepartmentId()));
		jobPost.setJobType(jobTypeDao.findJobType(jobPostVo.getJobTypeId()));
		jobPost.setJobTitle(jobPostVo.getJobTitle());
		if(jobPostVo.getPositions()!=null)
			jobPost.setPositions(Integer.parseInt(jobPostVo.getPositions()));
		if(jobPostVo.getAgeStart()!=null)
			jobPost.setAgeStart(Integer.parseInt(jobPostVo.getAgeStart()));
		if(jobPostVo.getAgeEnd()!=null)
			jobPost.setAgeEnd(Integer.parseInt(jobPostVo.getAgeEnd()));
		jobPost.setBranch(createBranches(jobPostVo.getBranchIds()));
		jobPost.setSalaryStartRange(Double.parseDouble(jobPostVo.getStartSalary()));
		jobPost.setSalaryEndRange(Double.parseDouble(jobPostVo.getEndSalary()));
		jobPost.setClosingDate(DateFormatter.convertStringToDate(jobPostVo.getJobPostEndDate()));
		Long id=(Long)this.sessionFactory.getCurrentSession().save(jobPost);
		return id;
	}
	/**
	 * method to save qualification
	 * @param jobPostVo
	 * 
	 */
	public void saveQualificationInfo(JobPostVo jobPostVo) {
		JobPost jobPost=findAndReturnJobPostObjectById(jobPostVo.getJobPostId());
		jobPost.setQualificationRequired(jobPostVo.getQualification());
		this.sessionFactory.getCurrentSession().merge(jobPost);
		
	}
	/**
	 * method to save experience required
	 * @param jobPostVo
	 * 
	 */
	public void saveExperienceRequired(JobPostVo jobPostVo)
	{
		JobPost jobPost=findAndReturnJobPostObjectById(jobPostVo.getJobPostId());
		
		jobPost.setExperienceRequired(createAndReturnExperience(jobPostVo , jobPost));
		this.sessionFactory.getCurrentSession().merge(jobPost);
	}
	/**
	 * method to save additional info
	 * @param JobPostVo
	 * 
	 */
	public void saveAdditionalInfo(JobPostVo jobPostVo) {
		JobPost jobPost=findAndReturnJobPostObjectById(jobPostVo.getJobPostId());
		jobPost.setAdditionalInformation(jobPostVo.getPostAdditionalInfo());
		this.sessionFactory.getCurrentSession().merge(jobPost);
	}
	/**
	 * method to save post description
	 * @param JobPOstVo
	 */
	public void savePostDescription(JobPostVo jobPostVo){
		JobPost jobPost=findAndReturnJobPostObjectById(jobPostVo.getJobPostId());
		jobPost.setDescription(jobPostVo.getPostDescription());
		this.sessionFactory.getCurrentSession().merge(jobPost);
	}
	/**
	 * method to update documents
	 * @param jobPostVo
	 */
	public void updateDocuments(JobPostVo jobPostVo) {
		JobPost post= (JobPost) this.sessionFactory.getCurrentSession().createCriteria(JobPost.class).add(Restrictions.eq("id",jobPostVo.getJobPostId())).uniqueResult();
		post.setJobPostDocuments(setDocuments(jobPostVo.getJobPostDocumentsVos(), post));
		this.sessionFactory.getCurrentSession().merge(post);
		
	}
	/**
	 * method to convert documentvos to document objects
	 * @param documentVos
	 * @param jobPost
	 * @return
	 */
	private Set<JobPostDocuments> setDocuments(Set<JobPostDocumentsVo> documentVos, JobPost jobPost) {
		Set<JobPostDocuments> postDocuments = new HashSet<JobPostDocuments>();
		for (JobPostDocumentsVo documentsVo : documentVos) {
			JobPostDocuments documents = new JobPostDocuments();
			documents.setFileName(documentsVo.getFileName());
			documents.setDocumentPath(documentsVo.getDocumentUrl());
			documents.setJobPost(jobPost);
			postDocuments.add(documents);
		}
		return postDocuments;

	}
	/**
	 * method to delete job post document
	 * @param url
	 */
	public void deleteDocument(String url) {
		JobPostDocuments document = (JobPostDocuments) this.sessionFactory.getCurrentSession().createCriteria(JobPostDocuments.class)
				.add(Restrictions.eq("documentPath",url)).uniqueResult();
		JobPost post= document.getJobPost();
		
		post.getJobPostDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(post);
		
	}


		

}
