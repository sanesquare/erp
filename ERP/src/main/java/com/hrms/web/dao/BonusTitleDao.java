package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.BonusTitles;
import com.hrms.web.vo.BonusTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
public interface BonusTitleDao {

	/**
	 * method to save bonus title
	 * @param titleVo
	 */
	public void saveBonusTitle(BonusTitleVo titleVo); 
	
	/**
	 * method to update bonus title
	 * @param titleVo
	 */
	public void updateBonusTitle(BonusTitleVo titleVo);
	
	/**
	 * method to delete bonus title
	 * @param bonusTitleId
	 */
	public void deleteBonusTitle(Long bonusTitleId);
	
	/**
	 * method to delete bonus title
	 * @param bonusTitle
	 */
	public void deleteBonusTitle(String bonusTitle);
	
	/**
	 * method to find bonus title
	 * @param bonusTitleId
	 * @return
	 */
	public BonusTitles findBonusTitle(Long bonusTitleId);
	
	/**
	 * method to find bonus title
	 * @param bonusTitleId
	 * @return
	 */
	public BonusTitles findBonusTitle(String bonusTitle);
	
	/**
	 * method to list all bonus titles
	 * @return
	 */
	public List<BonusTitleVo> listAllBonusTitles();
}
