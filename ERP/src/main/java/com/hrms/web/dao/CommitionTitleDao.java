package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.CommissionTitles;
import com.hrms.web.vo.CommissionTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
public interface CommitionTitleDao {

	/**
	 * method to save Commission title
	 * @param titleVo
	 */
	public void saveCommissionTitle(CommissionTitleVo titleVo); 
	
	/**
	 * method to update Commission title
	 * @param titleVo
	 */
	public void updateCommissionTitle(CommissionTitleVo titleVo);
	
	/**
	 * method to delete Commission title
	 * @param CommissionTitleId
	 */
	public void deleteCommissionTitle(Long commissionTitleId);
	
	/**
	 * method to delete Commission title
	 * @param CommissionTitle
	 */
	public void deleteCommissionTitle(String commissionTitle);
	
	/**
	 * method to find Commission title
	 * @param CommissionTitleId
	 * @return
	 */
	public CommissionTitles findCommissionTitle(Long commissionTitleId);
	
	/**
	 * method to find Commission title
	 * @param CommissionTitleId
	 * @return
	 */
	public CommissionTitles findCommissionTitle(String commissionTitle);
	
	/**
	 * method to list all Commission titles
	 * @return
	 */
	public List<CommissionTitleVo> listAllCommissionTitles();
}
