package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeGradeDao;
import com.hrms.web.entities.EmployeeGrade;
import com.hrms.web.entities.EmployeeWorkShift;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.EmployeeGradeVo;

/**
 * 
 * @author shamsheer
 * @since 18-March-2015
 */
@Repository
public class EmployeeGradeDaoImpl extends AbstractDao implements EmployeeGradeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@SuppressWarnings("unchecked")
	public List<EmployeeGrade> listAllGrades() {
		return this.sessionFactory.getCurrentSession().createCriteria(EmployeeGrade.class).list();
	}

	@SuppressWarnings("unchecked")
	public List<EmployeeWorkShift> listWorkShift() {
		return this.sessionFactory.getCurrentSession().createCriteria(EmployeeWorkShift.class).list();
	}

	public void saveGrade(EmployeeGradeVo gradeVo) {
		for (String grade : StringSplitter.splitWithComma(gradeVo.getGrade())) {
			if (findGrade(grade.trim()) != null)
				throw new DuplicateItemException("Duplicate Grade Value : " + grade.trim());
			EmployeeGrade employeeGrade = findGrade(gradeVo.getTempGrade());
			if (employeeGrade == null)
				employeeGrade = new EmployeeGrade();
			employeeGrade.setGrade(grade.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(employeeGrade);
			userActivitiesService.saveUserActivity("Added new Employee Grade " + grade.trim());
		}
	}

	public void updateGrade(EmployeeGradeVo gradeVo) {
		EmployeeGrade employeeGrade = findGrade(gradeVo.getGradeId());
		if (employeeGrade == null)
			throw new ItemNotFoundException("Employee Grade Details Not Found.");
		employeeGrade.setGrade(gradeVo.getGrade());
		this.sessionFactory.getCurrentSession().merge(employeeGrade);
		userActivitiesService.saveUserActivity("Updated Grade as " + gradeVo.getGrade());
	}

	public void deleteGrade(Long gradeId) {
		EmployeeGrade employeeGrade = findGrade(gradeId);
		if (employeeGrade == null)
			throw new ItemNotFoundException("Employee Grade Details Not Found.");
		String name = employeeGrade.getGrade();
		this.sessionFactory.getCurrentSession().delete(employeeGrade);
		userActivitiesService.saveUserActivity("Deleted Employee Grade " + name);
	}

	public EmployeeGrade findGrade(Long gradeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeGrade.class)
				.add(Restrictions.eq("id", gradeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeGrade) criteria.uniqueResult();
	}

	public EmployeeGrade findGrade(String grade) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeGrade.class)
				.add(Restrictions.eq("grade", grade));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeGrade) criteria.uniqueResult();
	}

	/**
	 * method to create EmployeeGradeVo object
	 * 
	 * @param employeeGrade
	 * @return employeeGradeVo
	 */
	private EmployeeGradeVo createEmployeeGradeVo(EmployeeGrade employeeGrade) {
		EmployeeGradeVo employeeGradeVo = new EmployeeGradeVo();
		employeeGradeVo.setGrade(employeeGrade.getGrade());
		employeeGradeVo.setGradeId(employeeGrade.getId());
		return employeeGradeVo;
	}

	public EmployeeGradeVo findGradeById(Long gradeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeGrade.class)
				.add(Restrictions.eq("id", gradeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeGradeVo((EmployeeGrade) criteria.uniqueResult());
	}

	public EmployeeGradeVo findGradeByType(String gradeType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeGrade.class)
				.add(Restrictions.eq("grade", gradeType));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeGradeVo((EmployeeGrade) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeGradeVo> findAllGrade() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeGrade.class);
		List<EmployeeGradeVo> employeeGradeVos = new ArrayList<EmployeeGradeVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeGrade> employeeGrades = criteria.list();
		for (EmployeeGrade employeeGrade : employeeGrades)
			employeeGradeVos.add(createEmployeeGradeVo(employeeGrade));
		return employeeGradeVos;
	}

}
