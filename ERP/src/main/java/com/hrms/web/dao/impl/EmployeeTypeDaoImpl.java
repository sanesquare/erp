package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeTypeDao;
import com.hrms.web.entities.EmployeeType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.EmployeeTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Repository
public class EmployeeTypeDaoImpl extends AbstractDao implements EmployeeTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveEmployeeType(EmployeeTypeVo employeeTypeVo) {
		for (String employeeType : StringSplitter.splitWithComma(employeeTypeVo.getEmployeeType())) {
			if (findEmployeeType(employeeType.trim()) != null)
				throw new DuplicateItemException("Duplicate Employee Type : " + employeeType.trim());
			EmployeeType type = findEmployeeType(employeeTypeVo.getTempEmployeeType());
			if (type == null)
				type = new EmployeeType();
			type.setType(employeeType.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(type);
			userActivitiesService.saveUserActivity("Added new Employee Type " + employeeTypeVo.getEmployeeType());
		}
	}

	public void updateEmployeeType(EmployeeTypeVo employeeTypeVo) {
		EmployeeType employeeType = findEmployeeType(employeeTypeVo.getEmployeeTypeId());
		if (employeeType == null)
			throw new ItemNotFoundException("Employee type not exist");
		employeeType.setType(employeeTypeVo.getEmployeeType());
		this.sessionFactory.getCurrentSession().merge(employeeType);
		userActivitiesService.saveUserActivity("Updated Employee Type as " + employeeTypeVo.getEmployeeType());
	}

	public void deleteEmployeeType(Long employeeTypeId) {
		EmployeeType employeeType = findEmployeeType(employeeTypeId);
		if (employeeType == null)
			throw new ItemNotFoundException("Employee type not exist");
		this.sessionFactory.getCurrentSession().delete(employeeType);
	}

	public EmployeeType findEmployeeType(Long employeeTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeType.class)
				.add(Restrictions.eq("id", employeeTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeType) criteria.uniqueResult();
	}

	public EmployeeType findEmployeeType(String employeeType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeType.class)
				.add(Restrictions.eq("type", employeeType));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeType) criteria.uniqueResult();
	}

	/**
	 * method to create employeetypevo object
	 * 
	 * @param employeeType
	 * @return employeetypevo
	 */
	private EmployeeTypeVo createEmployeeTypeVo(EmployeeType employeeType) {
		EmployeeTypeVo employeeTypeVo = new EmployeeTypeVo();
		employeeTypeVo.setEmployeeType(employeeType.getType());
		employeeTypeVo.setEmployeeTypeId(employeeType.getId());
		return employeeTypeVo;
	}

	public EmployeeTypeVo findEmployeeTypeById(Long employeeTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeType.class)
				.add(Restrictions.eq("id", employeeTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeTypeVo((EmployeeType) criteria.uniqueResult());
	}

	public EmployeeTypeVo findEmployeeTypeByType(String employeeType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeType.class)
				.add(Restrictions.eq("type", employeeType));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeTypeVo((EmployeeType) criteria.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<EmployeeTypeVo> findAllEmployeeType() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeType.class);
		List<EmployeeTypeVo> employeeTypeVos = new ArrayList<EmployeeTypeVo>();
		List<EmployeeType> employeeTypes = criteria.list();
		for (EmployeeType employeeType : employeeTypes)
			employeeTypeVos.add(createEmployeeTypeVo(employeeType));
		return employeeTypeVos;
	}

}
