package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.BranchType;
import com.hrms.web.vo.BranchTypeVo;
import com.hrms.web.vo.BranchVo;

/**
 * 
 * @author Vintha
 * @since 12-March-2015
 *
 */

public interface BranchTypeDao {
	
	/**
	 * method to save new BranchType
	 */
	public void saveBranchType(BranchTypeVo branchTypeVo);

	/**
	 * method to return BranchType object found by name
	 * 
	 * @param branchTypeName
	 * @return
	 */
	public BranchType findAndReturnBranchTypeByName(String branchTypeName);

	/**
	 * method to return branchType object
	 * 
	 * @param branchTypeVo
	 * @return
	 */
	public BranchType findAndReturnBranchTypeById(Long branchTypeId);

	/**
	 * 
	 * @param branchTypeVo
	 */
	public void updateBranchType(BranchTypeVo branchTypeVo);

	/**
	 * 
	 * @param branchTypeId
	 */
	public void deleteBranchType(Long branchTypeId);

	/**
	 * 
	 * @param branchTypeId
	 * @return
	 */
	public BranchTypeVo findBranchType(Long branchTypeId);

	/**
	 * 
	 * @param branchType
	 * @return
	 */
	public BranchTypeVo findBranchType(String branchType);

	/**
	 * 
	 * @return
	 */
	List<BranchTypeVo> findAllBranchType();
	/**
	 * method to create branchType Vo from branch vo
	 * @param branchVo
	 * @return
	 */
	public BranchTypeVo createBranchTypeVoFromBranchVo(BranchVo branchVo);

}
