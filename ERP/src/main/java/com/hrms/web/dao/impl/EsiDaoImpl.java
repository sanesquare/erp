package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.EsiDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Esi;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.EsiVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-April-2015
 *
 */
@Repository
public class EsiDaoImpl extends AbstractDao implements EsiDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	public void saveOrUpdateEsi(EsiVo esiVo) {
		Esi esi = findEsi(esiVo.getEsiId());
		if (esi == null)
			esi = new Esi();
		esi.setEmployee(employeeDao.findAndReturnEmployeeByCode(esiVo.getEmployeeCode()));
		esi.setEmployeeShare(esiVo.getEmployeeShare());
		esi.setOrganizationShare(esiVo.getOrganizationShare());
		esi.setDescription(esiVo.getDescription());
		esi.setNotes(esiVo.getNotes());
		this.sessionFactory.getCurrentSession().saveOrUpdate(esi);
		esiVo.setEsiId(esi.getId());
		esiVo.setRecordedBy(esi.getCreatedBy().getUserName());
		esiVo.setRecordedOn(DateFormatter.convertDateToDetailedString(esi.getCreatedOn()));
	}

	public void deleteEsi(Long esiId) {
		Esi esi = findEsi(esiId);
		if (esi == null)
			throw new ItemNotFoundException("Esi Information not found.");
		this.sessionFactory.getCurrentSession().delete(esi);
	}

	public Esi findEsi(Long esiId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Esi.class)
				.add(Restrictions.eq("id", esiId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Esi) criteria.uniqueResult();
	}

	/**
	 * method to create EsiVo object
	 * 
	 * @param esi
	 * @return esiVo
	 */
	private EsiVo createEsiVo(Esi esi) {
		EsiVo esiVo = new EsiVo();
		esiVo.setEsiId(esi.getId());
		esiVo.setEmployee(esi.getEmployee().getUserProfile().getFirstName() + " "
				+ esi.getEmployee().getUserProfile().getLastname());
		esiVo.setEmployeeCode(esi.getEmployee().getEmployeeCode());
		esiVo.setEmployeeShare(esi.getEmployeeShare());
		esiVo.setOrganizationShare(esi.getOrganizationShare());
		esiVo.setDescription(esi.getDescription());
		esiVo.setNotes(esi.getNotes());
		esiVo.setRecordedBy(esi.getCreatedBy().getUserName());
		esiVo.setRecordedOn(DateFormatter.convertDateToDetailedString(esi.getCreatedOn()));
		return esiVo;
	}

	public EsiVo findEsiById(Long esiId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Esi.class)
				.add(Restrictions.eq("id", esiId));
		if (criteria.uniqueResult() == null)
			return null;
		return createEsiVo((Esi) criteria.uniqueResult());
	}

	public EsiVo findEsiByEmployee(Employee employee) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Esi.class)
				.add(Restrictions.eq("employee", employee));
		if (criteria.uniqueResult() == null)
			return null;
		return createEsiVo((Esi) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EsiVo> findAllEsi() {
		List<EsiVo> esiVos = new ArrayList<EsiVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Esi.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Esi> esis = criteria.list();
		for (Esi esi : esis)
			esiVos.add(createEsiVo(esi));
		return esiVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EsiVo> findAllEsiByEmployee(Employee employee, Date startDate, Date endDate) {
		List<EsiVo> esiVos = new ArrayList<EsiVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Esi.class)
				.add(Restrictions.eq("employee", employee)).add(Restrictions.ge("createdOn", startDate))
				.add(Restrictions.le("createdOn", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Esi> esis = criteria.list();
		for (Esi esi : esis)
			esiVos.add(createEsiVo(esi));
		return esiVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EsiVo> findAllEsiByEmployee(String employeeCode) {
		List<EsiVo> esiVos = new ArrayList<EsiVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Esi.class);
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("createdBy", employee.getUser());
		Criterion receiver = Restrictions.eq("employee", employee);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Esi> esis = criteria.list();
		for (Esi esi : esis)
			esiVos.add(createEsiVo(esi));
		return esiVos;
	}

}
