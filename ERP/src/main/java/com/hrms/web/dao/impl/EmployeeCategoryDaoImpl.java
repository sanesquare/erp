package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeCategoryDao;
import com.hrms.web.entities.EmployeeCategory;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.EmployeeCategoryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Repository
public class EmployeeCategoryDaoImpl extends AbstractDao implements EmployeeCategoryDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveEmployeeCategory(EmployeeCategoryVo employeeCategoryVo) {
		for (String employeeCategory : StringSplitter.splitWithComma(employeeCategoryVo.getEmployeeType())) {
			if (findEmployeeCategory(employeeCategory.trim()) != null)
				throw new DuplicateItemException("Duplicate Employee Category : " + employeeCategory.trim());
			EmployeeCategory category = findEmployeeCategory(employeeCategoryVo.getTempEmployeeCategory());
			if (category == null)
				category = new EmployeeCategory();
			category.setCategory(employeeCategory.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(category);
			userActivitiesService.saveUserActivity("Added new Employee Category " + employeeCategory.trim());
		}
	}

	public void updateEmployeeCategory(EmployeeCategoryVo employeeCategoryVo) {
		EmployeeCategory employeeCategory = findEmployeeCategory(employeeCategoryVo.getEmployeeCategoryId());
		if (employeeCategory == null)
			throw new ItemNotFoundException("Employee Category Not Found");
		employeeCategory.setCategory(employeeCategoryVo.getEmployeeType());
		this.sessionFactory.getCurrentSession().merge(employeeCategory);
		userActivitiesService.saveUserActivity("Updated Employee Category " + employeeCategoryVo.getEmployeeType());
	}

	public void deleteEmployeeCategory(Long employeeCategoryId) {
		EmployeeCategory employeeCategory = findEmployeeCategory(employeeCategoryId);
		if (employeeCategory == null)
			throw new ItemNotFoundException("Employee Category Not Found");
		this.sessionFactory.getCurrentSession().delete(employeeCategory);
		userActivitiesService.saveUserActivity("Deleted Employee Category");
	}

	public EmployeeCategory findEmployeeCategory(Long employeeCategoryId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeCategory.class)
				.add(Restrictions.eq("id", employeeCategoryId));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeCategory) criteria.uniqueResult();
	}

	public EmployeeCategory findEmployeeCategory(String employeeCategory) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeCategory.class)
				.add(Restrictions.eq("category", employeeCategory));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeCategory) criteria.uniqueResult();
	}

	/**
	 * method to create EmployeeCategoryVo object
	 * 
	 * @param employeeCategory
	 * @return categoryVo
	 */
	private EmployeeCategoryVo createEmployeeCategoryVo(EmployeeCategory employeeCategory) {
		EmployeeCategoryVo categoryVo = new EmployeeCategoryVo();
		categoryVo.setEmployeeCategory(employeeCategory.getCategory());
		categoryVo.setEmployeeCategoryId(employeeCategory.getId());
		return categoryVo;
	}

	public EmployeeCategoryVo findEmployeeCategoryById(Long employeeCategoryId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeCategory.class)
				.add(Restrictions.eq("id", employeeCategoryId));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeCategoryVo((EmployeeCategory) criteria.uniqueResult());
	}

	public EmployeeCategoryVo findEmployeeCategoryByCategory(String employeeCategory) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeCategory.class)
				.add(Restrictions.eq("category", employeeCategory));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeCategoryVo((EmployeeCategory) criteria.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<EmployeeCategoryVo> findAllEmployeeCategory() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeCategory.class);
		List<EmployeeCategoryVo> employeeCategoryVos = new ArrayList<EmployeeCategoryVo>();
		List<EmployeeCategory> employeeCategories = criteria.list();
		for (EmployeeCategory employeeCategory : employeeCategories)
			employeeCategoryVos.add(createEmployeeCategoryVo(employeeCategory));
		return employeeCategoryVos;
	}

}
