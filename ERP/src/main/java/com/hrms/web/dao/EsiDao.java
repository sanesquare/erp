package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Esi;
import com.hrms.web.vo.EsiVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-April-2015
 *
 */
public interface EsiDao {

	/**
	 * 
	 * @param esiVo
	 */
	public void saveOrUpdateEsi(EsiVo esiVo);

	/**
	 * 
	 * @param esiId
	 */
	public void deleteEsi(Long esiId);

	/**
	 * 
	 * @param esiId
	 * @return
	 */
	public Esi findEsi(Long esiId);

	/**
	 * 
	 * @param esiId
	 * @return
	 */
	public EsiVo findEsiById(Long esiId);

	/**
	 * 
	 * @param employee
	 * @return
	 */
	public EsiVo findEsiByEmployee(Employee employee);

	/**
	 * 
	 * @return
	 */
	public List<EsiVo> findAllEsi();

	/**
	 * method to find all esi of an employee and between dates
	 * 
	 * @param employee
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<EsiVo> findAllEsiByEmployee(Employee employee, Date startDate, Date endDate);

	/**
	 * 
	 * @return
	 */
	public List<EsiVo> findAllEsiByEmployee(String employeeCode);
}
