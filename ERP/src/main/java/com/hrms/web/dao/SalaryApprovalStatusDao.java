package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.SalaryApprovalStatus;
import com.hrms.web.vo.SalaryApprovalStatusVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-09-2015
 */
public interface SalaryApprovalStatusDao {

	public SalaryApprovalStatus findStatus(String status);
	
	public SalaryApprovalStatus findStatus(Long id);
	
	public List<SalaryApprovalStatusVo> findAllStatus();
	
}
