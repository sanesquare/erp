package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.DepartmentDao;
import com.hrms.web.dao.OrganizationPolicyDao;
import com.hrms.web.dao.PolicyTypeDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.OrganizationPolicy;
import com.hrms.web.entities.OrganizationPolicyDocument;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.OrganizationPolicyDocumentsVo;
import com.hrms.web.vo.OrganizationPolicyVo;

/**
 * 
 * @author Jithin Mohan
 * @since 15-May-2015
 *
 */
@Repository
public class OrganizationPolicyDaoImpl extends AbstractDao implements OrganizationPolicyDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private BranchDao branchDao;

	@Autowired
	private DepartmentDao departmentDao;

	@Autowired
	private PolicyTypeDao policyTypeDao;

	public void saveOrUpdateOrganizationPolicy(OrganizationPolicyVo organizationPolicyVo) {
		OrganizationPolicy organizationPolicy = findOrganizationPolicy(organizationPolicyVo.getOrg_policy_id());
		if (organizationPolicy == null)
			organizationPolicy = new OrganizationPolicy();
		organizationPolicy.setPolicyType(policyTypeDao.findPolicyType(organizationPolicyVo.getPolicy_type_id()));
		organizationPolicy.setTitle(organizationPolicyVo.getTitle());
		organizationPolicy.setBranch(setBranches(organizationPolicyVo.getBranch_id(), organizationPolicy));
		organizationPolicy.setDepartment(setDepartments(organizationPolicyVo.getDepartment_id(), organizationPolicy));
		organizationPolicy.setDescription(organizationPolicyVo.getDescription());
		organizationPolicy.setInformation(organizationPolicyVo.getInformation());
		this.sessionFactory.getCurrentSession().saveOrUpdate(organizationPolicy);
		organizationPolicyVo.setOrg_policy_id(organizationPolicy.getId());
		organizationPolicyVo.setRecordedBy(organizationPolicy.getCreatedBy().getUserName());
		organizationPolicyVo
				.setRecordedOn(DateFormatter.convertDateToDetailedString(organizationPolicy.getCreatedOn()));
	}

	/**
	 * 
	 * @param branchIdList
	 * @param organizationPolicy
	 * @return
	 */
	private Set<Branch> setBranches(List<Long> branchIdList, OrganizationPolicy organizationPolicy) {
		Set<Branch> available = organizationPolicy.getBranch();
		if (available == null || available.isEmpty())
			available = new HashSet<Branch>();

		Set<Branch> availableTemp = new HashSet<Branch>();
		if (available != null)
			availableTemp.addAll(available);

		Set<Branch> newAvailable = new HashSet<Branch>();
		for (Long branchId : branchIdList) {
			Branch forwader = branchDao.findAndReturnBranchObjectById(branchId);
			newAvailable.add(forwader);
			if (!available.contains(forwader)) {
				available.add(forwader);
			}
		}

		availableTemp.removeAll(newAvailable);
		Iterator<Branch> iterator = availableTemp.iterator();
		while (iterator.hasNext()) {
			Branch branch = (Branch) iterator.next();
			available.remove(branch);
		}
		return available;
	}

	/**
	 * 
	 * @param departmentIdList
	 * @param organizationPolicy
	 * @return
	 */
	private Set<Department> setDepartments(List<Long> departmentIdList, OrganizationPolicy organizationPolicy) {
		Set<Department> available = organizationPolicy.getDepartment();
		if (available == null || available.isEmpty())
			available = new HashSet<Department>();

		Set<Department> availableTemp = new HashSet<Department>();
		if (available != null)
			availableTemp.addAll(available);

		Set<Department> newAvailable = new HashSet<Department>();
		for (Long departmentId : departmentIdList) {
			Department forwader = departmentDao.findDepartment(departmentId);
			newAvailable.add(forwader);
			if (!available.contains(forwader)) {
				available.add(forwader);
			}
		}

		availableTemp.removeAll(newAvailable);
		Iterator<Department> iterator = availableTemp.iterator();
		while (iterator.hasNext()) {
			Department department = (Department) iterator.next();
			available.remove(department);
		}
		return available;
	}

	public void saveOrganizationPolicyDocument(OrganizationPolicyVo organizationPolicyVo) {
		OrganizationPolicy organizationPolicy = findOrganizationPolicy(organizationPolicyVo.getOrg_policy_id());
		if (organizationPolicy == null)
			throw new ItemNotFoundException("Organization Policy Details Not Found");
		organizationPolicy.setDocuments(setOrganizationPolicyDocuments(organizationPolicyVo.getDocuments(),
				organizationPolicy));
		this.sessionFactory.getCurrentSession().merge(organizationPolicy);
	}

	/**
	 * 
	 * @param documentsVos
	 * @param organizationPolicy
	 * @return
	 */
	private Set<OrganizationPolicyDocument> setOrganizationPolicyDocuments(
			List<OrganizationPolicyDocumentsVo> documentsVos, OrganizationPolicy organizationPolicy) {
		Set<OrganizationPolicyDocument> documents = new HashSet<OrganizationPolicyDocument>();
		for (OrganizationPolicyDocumentsVo documentsVo : documentsVos) {
			OrganizationPolicyDocument document = new OrganizationPolicyDocument();
			document.setDocumentName(documentsVo.getDocumentName());
			document.setDocumentUrl(documentsVo.getDocumentUrl());
			document.setOrganizationPolicy(organizationPolicy);
			documents.add(document);
		}
		return documents;
	}

	public void deleteOrganizationPolicy(Long organizationPolicyId) {
		OrganizationPolicy organizationPolicy = findOrganizationPolicy(organizationPolicyId);
		if (organizationPolicy == null)
			throw new ItemNotFoundException("Organization Policy Details Not Found");
		organizationPolicy.getBranch().clear();
		organizationPolicy.getDepartment().clear();
		this.sessionFactory.getCurrentSession().delete(organizationPolicy);
	}

	public OrganizationPolicy findOrganizationPolicy(Long organizationPolicyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicy.class)
				.add(Restrictions.eq("id", organizationPolicyId));
		if (criteria.uniqueResult() == null)
			return null;
		return (OrganizationPolicy) criteria.uniqueResult();
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<OrganizationPolicy> findOrganizationPolicy(Branch branch) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicy.class)
				.add(Restrictions.eq("branch", branch));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<OrganizationPolicy> findOrganizationPolicy(Branch branch, Department department) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicy.class)
				.add(Restrictions.eq("branch", branch)).add(Restrictions.eq("department", department));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	/**
	 * 
	 * @param organizationPolicy
	 * @return
	 */
	private OrganizationPolicyVo createOrganizationPolicyVo(OrganizationPolicy organizationPolicy) {
		OrganizationPolicyVo organizationPolicyVo = new OrganizationPolicyVo();
		organizationPolicyVo.setOrg_policy_id(organizationPolicy.getId());
		organizationPolicyVo.setPolicy_type_id(organizationPolicy.getPolicyType().getId());
		organizationPolicyVo.setPolicyType(organizationPolicy.getPolicyType().getType());
		organizationPolicyVo.setTitle(organizationPolicy.getTitle());
		organizationPolicyVo.setBranch_id(getBranch(organizationPolicy.getBranch()));
		organizationPolicyVo.setBranches(getBranchNames(organizationPolicy.getBranch()));
		organizationPolicyVo.setDepartment_id(getDepartment(organizationPolicy.getDepartment()));
		organizationPolicyVo.setDepartments(getDepartmentNames(organizationPolicy.getDepartment()));
		organizationPolicyVo.setDescription(organizationPolicy.getDescription());
		organizationPolicyVo.setInformation(organizationPolicy.getInformation());
		organizationPolicyVo.setRecordedBy(organizationPolicy.getCreatedBy().getUserName());
		organizationPolicyVo
				.setRecordedOn(DateFormatter.convertDateToDetailedString(organizationPolicy.getCreatedOn()));
		organizationPolicyVo.setDocuments(getDocuments(organizationPolicy.getDocuments()));
		return organizationPolicyVo;
	}

	/**
	 * 
	 * @param documents
	 * @return
	 */
	private List<OrganizationPolicyDocumentsVo> getDocuments(Set<OrganizationPolicyDocument> documents) {
		List<OrganizationPolicyDocumentsVo> documentsVos = new ArrayList<OrganizationPolicyDocumentsVo>();
		Iterator<OrganizationPolicyDocument> iterator = documents.iterator();
		while (iterator.hasNext()) {
			OrganizationPolicyDocumentsVo documentsVo = new OrganizationPolicyDocumentsVo();
			OrganizationPolicyDocument policyDocument = (OrganizationPolicyDocument) iterator.next();
			documentsVo.setDocumentName(policyDocument.getDocumentName());
			documentsVo.setDocumentUrl(policyDocument.getDocumentUrl());
			documentsVo.setDocumentId(policyDocument.getId());
			documentsVos.add(documentsVo);
		}
		return documentsVos;
	}

	/**
	 * 
	 * @param branchs
	 * @return
	 */
	private List<Long> getBranch(Set<Branch> branchs) {
		List<Long> branchIds = new ArrayList<Long>();
		Iterator<Branch> iterator = branchs.iterator();
		while (iterator.hasNext()) {
			Branch branch = (Branch) iterator.next();
			branchIds.add(branch.getId());
		}
		return branchIds;
	}

	/**
	 * 
	 * @param branchs
	 * @return
	 */
	private List<String> getBranchNames(Set<Branch> branchs) {
		List<String> branchNames = new ArrayList<String>();
		Iterator<Branch> iterator = branchs.iterator();
		while (iterator.hasNext()) {
			Branch branch = (Branch) iterator.next();
			branchNames.add(branch.getBranchName());
		}
		return branchNames;
	}

	/**
	 * 
	 * @param departments
	 * @return
	 */
	private List<Long> getDepartment(Set<Department> departments) {
		List<Long> departmentIds = new ArrayList<Long>();
		Iterator<Department> iterator = departments.iterator();
		while (iterator.hasNext()) {
			Department department = (Department) iterator.next();
			departmentIds.add(department.getId());
		}
		return departmentIds;
	}

	/**
	 * 
	 * @param departments
	 * @return
	 */
	private List<String> getDepartmentNames(Set<Department> departments) {
		List<String> departmentNames = new ArrayList<String>();
		Iterator<Department> iterator = departments.iterator();
		while (iterator.hasNext()) {
			Department department = (Department) iterator.next();
			departmentNames.add(department.getName());
		}
		return departmentNames;
	}

	public OrganizationPolicyVo findOrganizationPolicyById(Long organizationPolicyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicy.class)
				.add(Restrictions.eq("id", organizationPolicyId));
		if (criteria.uniqueResult() == null)
			return null;
		return createOrganizationPolicyVo((OrganizationPolicy) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<OrganizationPolicyVo> findOrganizationPolicyByBranch(Long branchId) {
		List<OrganizationPolicyVo> organizationPolicyVos = new ArrayList<OrganizationPolicyVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicy.class)
				.add(Restrictions.eq("branch", branchDao.findAndReturnBranchObjectById(branchId)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<OrganizationPolicy> organizationPolicies = criteria.list();
		for (OrganizationPolicy organizationPolicy : organizationPolicies)
			organizationPolicyVos.add(createOrganizationPolicyVo(organizationPolicy));
		return organizationPolicyVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<OrganizationPolicyVo> findOrganizationPolicyByBranchAndDepartment(Long branchId, Long departmentId) {
		List<OrganizationPolicyVo> organizationPolicyVos = new ArrayList<OrganizationPolicyVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicy.class)
				.add(Restrictions.eq("branch", branchDao.findAndReturnBranchObjectById(branchId)))
				.add(Restrictions.eq("department", departmentDao.findDepartment(departmentId)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<OrganizationPolicy> organizationPolicies = criteria.list();
		for (OrganizationPolicy organizationPolicy : organizationPolicies)
			organizationPolicyVos.add(createOrganizationPolicyVo(organizationPolicy));
		return organizationPolicyVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<OrganizationPolicyVo> findOrganizationPolicyByType(String policyType) {
		List<OrganizationPolicyVo> organizationPolicyVos = new ArrayList<OrganizationPolicyVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicy.class)
				.add(Restrictions.eq("policyType", policyTypeDao.findPolicyType(policyType)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<OrganizationPolicy> organizationPolicies = criteria.list();
		for (OrganizationPolicy organizationPolicy : organizationPolicies)
			organizationPolicyVos.add(createOrganizationPolicyVo(organizationPolicy));
		return organizationPolicyVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<OrganizationPolicyVo> findAllOrganizationPolicy() {
		List<OrganizationPolicyVo> organizationPolicyVos = new ArrayList<OrganizationPolicyVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicy.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<OrganizationPolicy> organizationPolicies = criteria.list();
		for (OrganizationPolicy organizationPolicy : organizationPolicies)
			organizationPolicyVos.add(createOrganizationPolicyVo(organizationPolicy));
		return organizationPolicyVos;
	}

	public void deleteOrganizationPolicyDocument(Long orgPolicyDocId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicyDocument.class)
				.add(Restrictions.eq("id", orgPolicyDocId));
		OrganizationPolicyDocument document = (OrganizationPolicyDocument) criteria.uniqueResult();
		OrganizationPolicy organizationPolicy = findOrganizationPolicy(document.getOrganizationPolicy().getId());
		organizationPolicy.getDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(organizationPolicy);
	}

}
