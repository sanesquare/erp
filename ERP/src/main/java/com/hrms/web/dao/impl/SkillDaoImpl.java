package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.SkillDao;
import com.hrms.web.entities.Skills;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.SkillVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-march-2015
 *
 */
@Repository
public class SkillDaoImpl extends AbstractDao implements SkillDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	/**
	 * method to save new skill
	 * 
	 * @param skillVo
	 */
	public void saveSkill(SkillVo skillVo) {
		logger.info("Inside Skill DAO >>> saveSkill.....");
		for (String skillName : StringSplitter.splitWithComma(skillVo.getSkillName())) {
			if (findSkill(skillName.trim()) != null)
				throw new DuplicateItemException("Duplicate Skill : " + skillName);
			Skills skills = findSkill(skillVo.getTempSkill());
			if (skills == null)
				skills = new Skills();
			skills.setSkill(skillName.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(skills);
		}
	}

	/**
	 * method to update skill information
	 * 
	 * @param skillVo
	 */
	public void updateSkill(SkillVo skillVo) {
		logger.info("Inside Skill DAO >>> updateSkill.....");
		Skills skills = findSkill(skillVo.getSkillId());
		if (skills == null)
			throw new ItemNotFoundException("Skill Not Exist.");
		skills.setSkill(skillVo.getSkillName());
		this.sessionFactory.getCurrentSession().merge(skills);
	}

	/**
	 * method to delete skill information
	 * 
	 * @param skillId
	 */
	public void deleteSkill(Long skillId) {
		logger.info("Inside Skill DAO >>> deleteSkill.....");
		Skills skills = findSkill(skillId);
		if (skills == null)
			throw new ItemNotFoundException("Skill Not Exist.");
		this.sessionFactory.getCurrentSession().delete(skills);
	}

	/**
	 * method to find skill by id
	 * 
	 * @param skillId
	 * @return skills
	 */
	public Skills findSkill(Long skillId) {
		logger.info("Inside Skill DAO >>> findSkill.....");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Skills.class)
				.add(Restrictions.eq("id", skillId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Skills) criteria.uniqueResult();
	}

	/**
	 * method to create skillvo object
	 * 
	 * @param skill
	 * @return skillvo
	 */
	private SkillVo createSkillVo(Skills skill) {
		logger.info("Inside Skill DAO >>> createSkillVo.....");
		SkillVo skillVo = new SkillVo();
		skillVo.setSkillName(skill.getSkill());
		skillVo.setSkillId(skill.getId());
		return skillVo;
	}

	/**
	 * method to find skill by name
	 * 
	 * @param skillId
	 * @return skillvo
	 */
	public SkillVo findSkillById(Long skillId) {
		logger.info("Inside Skill DAO >>> findSkillById.....");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Skills.class)
				.add(Restrictions.eq("id", skillId));
		if (criteria.uniqueResult() == null)
			return null;
		return createSkillVo((Skills) criteria.uniqueResult());
	}

	/**
	 * method to find skill by name
	 * 
	 * @param skillName
	 * @return skillvo
	 */
	public SkillVo findSkillByName(String skillName) {
		logger.info("Inside Skill DAO >>> findSkillByName.....");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Skills.class)
				.add(Restrictions.eq("skill", skillName));
		if (criteria.uniqueResult() == null)
			return null;
		return createSkillVo((Skills) criteria.uniqueResult());
	}

	/**
	 * method to find skill by name
	 * 
	 * @param skillName
	 * @return skill
	 */
	public Skills findSkill(String skillName) {
		logger.info("Inside Skill DAO >>> findSkill.....");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Skills.class)
				.add(Restrictions.eq("skill", skillName));
		if (criteria.uniqueResult() == null)
			return null;
		return (Skills) criteria.uniqueResult();
	}

	/**
	 * method to find all skills
	 * 
	 * @return skillVos
	 */
	@SuppressWarnings("unchecked")
	public List<SkillVo> findAllSkills() {
		logger.info("Inside Skill DAO >>> findAllSkills.....");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Skills.class);
		List<SkillVo> skillVos = new ArrayList<SkillVo>();

		List<Skills> skills = criteria.list();
		for (Skills skill : skills)
			skillVos.add(createSkillVo(skill));
		return skillVos;
	}

}
