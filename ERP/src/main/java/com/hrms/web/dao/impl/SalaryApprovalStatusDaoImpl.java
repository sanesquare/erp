package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.SalaryApprovalStatusDao;
import com.hrms.web.entities.SalaryApprovalStatus;
import com.hrms.web.vo.SalaryApprovalStatusVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-09-2015
 */
@Repository
public class SalaryApprovalStatusDaoImpl extends AbstractDao implements SalaryApprovalStatusDao {

	public SalaryApprovalStatus findStatus(String status) {
		return (SalaryApprovalStatus) this.sessionFactory.getCurrentSession()
				.createCriteria(SalaryApprovalStatus.class).add(Restrictions.eq("status", status)).uniqueResult();
	}

	public SalaryApprovalStatus findStatus(Long id) {
		return (SalaryApprovalStatus) this.sessionFactory.getCurrentSession()
				.createCriteria(SalaryApprovalStatus.class).add(Restrictions.eq("id", id)).uniqueResult();
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<SalaryApprovalStatusVo> findAllStatus() {
		List<SalaryApprovalStatusVo> vos = new ArrayList<SalaryApprovalStatusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryApprovalStatus.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.not(Restrictions.eq("status", AccountsConstants.PAID_STATUS)));
		List<SalaryApprovalStatus> approvalStatus = criteria.list();
		for (SalaryApprovalStatus status : approvalStatus) {
			vos.add(createVo(status));
		}
		return vos;
	}

	private SalaryApprovalStatusVo createVo(SalaryApprovalStatus status) {
		SalaryApprovalStatusVo vo = new SalaryApprovalStatusVo();
		vo.setId(status.getId());
		vo.setStatus(status.getStatus());
		return vo;
	}
}
