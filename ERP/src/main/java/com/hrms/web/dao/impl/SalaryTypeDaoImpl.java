package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.hibernate.search.backend.impl.jgroups.SlaveNodeSelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.entities.SalaryType;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.SalaryTypeVo;

/**
 * 
 * @author Shamsheer
 * @since 18-April-2015
 */
@Repository
public class SalaryTypeDaoImpl extends AbstractDao implements SalaryTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveSalaryType(SalaryTypeVo salaryTypeVo) {
		// TODO Auto-generated method stub

	}

	public void updateSalaryType(SalaryTypeVo salaryTypeVo) {
		// TODO Auto-generated method stub

	}

	public void deleteSalaryType(Long id) {
		// TODO Auto-generated method stub

	}

	public SalaryType getSalaryType(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public SalaryType getSalaryType(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<SalaryTypeVo> listAllSalaryTypes() {
		List<SalaryTypeVo> vos = new ArrayList<SalaryTypeVo>();
		@SuppressWarnings("unchecked")
		List<SalaryType> salaryTypes = this.sessionFactory.getCurrentSession().createCriteria(SalaryType.class).list();
		for (SalaryType salaryType : salaryTypes) {
			vos.add(createSalaryTypeVo(salaryType));
		}
		return vos;
	}

	private SalaryTypeVo createSalaryTypeVo(SalaryType salaryType) {
		SalaryTypeVo vo = new SalaryTypeVo();
		vo.setId(salaryType.getId());
		vo.setType(salaryType.getType());
		return vo;
	}

	public SalaryTypeVo findSalaryType(Long id) {
		SalaryType salaryType = findSalaryTypeObject(id);
		if (salaryType != null)
			return createSalaryTypeVo(salaryType);
		else
			return null;
	}

	public SalaryTypeVo findSalaryType(String type) {
		SalaryType salaryType = findSalaryTypeObject(type);
		if (salaryType != null)
			return createSalaryTypeVo(salaryType);
		else
			return null;
	}

	public SalaryType findSalaryTypeObject(Long id) {
		return (SalaryType) this.sessionFactory.getCurrentSession().createCriteria(SalaryType.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public SalaryType findSalaryTypeObject(String type) {
		return (SalaryType) this.sessionFactory.getCurrentSession().createCriteria(SalaryType.class)
				.add(Restrictions.eq("type", type)).uniqueResult();
	}
}
