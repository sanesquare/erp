package com.hrms.web.dao;

import com.hrms.web.entities.ProfesionalTax;
import com.hrms.web.vo.ProfesionalTaxVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-March-2015
 *
 */
public interface ProfesionalTaxDao {

	/**
	 * 
	 * @param profesionalTaxVo
	 */
	public void saveOrUpdate(ProfesionalTaxVo profesionalTaxVo);

	/**
	 * 
	 * @param id
	 * @return
	 */
	public ProfesionalTax findProfesionalTax(Long id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	public ProfesionalTaxVo findProfesionalTaxById(Long id);

	/**
	 * 
	 * @return
	 */
	public ProfesionalTaxVo findProfesionalTax();
}
