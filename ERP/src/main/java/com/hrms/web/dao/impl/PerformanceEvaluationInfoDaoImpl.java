package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.PerformanceEvaluationInfoDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.IndividualEvaluation;
import com.hrms.web.entities.PerformanceEvaluationInfo;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.IndividualEvaluationVo;
import com.hrms.web.vo.PerformanceVo;

/**
 * 
 * @author Vinutha
 * @since 23-May-2015
 *
 */
@Repository
public class PerformanceEvaluationInfoDaoImpl extends AbstractDao implements PerformanceEvaluationInfoDao {

	@Autowired
	private EmployeeDao empDao;

	/**
	 * Method to save performance evaluation
	 * 
	 * @param performanceVo
	 * @return
	 */
	public Long savePerformanceEvaluation(PerformanceVo performanceVo) {
		PerformanceEvaluationInfo performance = new PerformanceEvaluationInfo();
		performance.setTitle(performanceVo.getTitle());
		performance.setDate(DateFormatter.convertStringToDate(performanceVo.getDate()));
		performance.setNotes(performanceVo.getNotes());
		Long id = (Long) this.sessionFactory.getCurrentSession().save(performance);
		if(performanceVo.getEmployeeCode()!=null)
		{
			
			IndividualEvaluation individual = new IndividualEvaluation();
			
			Employee emp = empDao.findAndReturnEmployeeByCode(performanceVo.getEmployeeCode());
			if(emp!=null)
			{	
				individual.setEmployee(emp);
				individual.setBranch(emp.getBranch().getBranchName());
				individual.setDesignation(emp.getEmployeeDesignation().getDesignation());
				if(performanceVo.getMaxMark()!=null)
					individual.setMaxMark(Double.parseDouble(performanceVo.getMaxMark()));
				if(performanceVo.getScore()!=null)
					individual.setScore(Double.parseDouble(performanceVo.getScore()));
				individual.setEvaluationInfo(performance);
			 	this.sessionFactory.getCurrentSession().save(individual);
			}
		}
		 
		return id;
	}

	/**
	 * Method to update performance evaluation
	 * 
	 * @param performanceVo
	 */
	public void updatePerformanceEvaluation(PerformanceVo performanceVo) {
		PerformanceEvaluationInfo performance = findAndReturnEvaluationById(performanceVo.getEvaluationId());
		performance.setTitle(performanceVo.getTitle());
		performance.setDate(DateFormatter.convertStringToDate(performanceVo.getDate()));
		performance.setNotes(performanceVo.getNotes());
		this.sessionFactory.getCurrentSession().save(performance);
		if(performanceVo.getEmployeeCode()!=null)
		{
			IndividualEvaluation individual = new IndividualEvaluation();
			if(performanceVo.getIndividualId()!=null)
				individual.setId(performanceVo.getIndividualId());
			Employee emp = empDao.findAndReturnEmployeeByCode(performanceVo.getEmployeeCode());
			if(emp!=null)
			{	
				individual.setEmployee(emp);
				individual.setBranch(emp.getBranch().getBranchName());
				if(emp.getEmployeeDesignation()!=null)
					individual.setDesignation(emp.getEmployeeDesignation().getDesignation());
				individual.setMaxMark(Double.parseDouble(performanceVo.getMaxMark()));
				individual.setScore(Double.parseDouble(performanceVo.getScore()));
				individual.setEvaluationInfo(performance);
				if(performanceVo.getIndividualId()!=null)
					this.sessionFactory.getCurrentSession().merge(individual);
				else
					this.sessionFactory.getCurrentSession().save(individual);
			}
		}
	}

	/**
	 * Method to delete performance evaluation
	 * 
	 * @param id
	 */
	public void deletePerformanceEvaluation(Long id) {
		PerformanceEvaluationInfo performance = findAndReturnEvaluationById(id);
		this.sessionFactory.getCurrentSession().delete(performance);
	}

	/**
	 * Method to list performance evaluations
	 * 
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<PerformanceVo> listPerformanceEvaluations() {

		List<PerformanceVo> performanceEvaluations = new ArrayList<PerformanceVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluationInfo.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PerformanceEvaluationInfo> evaluations = criteria.list();
		for (PerformanceEvaluationInfo performanceEvaluationInfo : evaluations) {
			performanceEvaluations.add(createPerformanceVo(performanceEvaluationInfo));
		}
		return performanceEvaluations;
	}
	/**
	 * Method to create performance evaluation vo
	 * @param performance
	 * @return
	 */
	private PerformanceVo createPerformanceVo(PerformanceEvaluationInfo performance) {
		PerformanceVo vo = new PerformanceVo();
		vo.setEvaluationId(performance.getId());
		vo.setDate(DateFormatter.convertDateToString(performance.getDate()));
		vo.setTitle(performance.getTitle());
		vo.setNotes(performance.getNotes());
		Set<IndividualEvaluationVo> individualVos = new HashSet<IndividualEvaluationVo>();
		Set<IndividualEvaluation> individuals = new  HashSet<IndividualEvaluation>();
		individuals = performance.getIndividualEvaluation();
		for (IndividualEvaluation individualEvaluation : individuals) {
			individualVos.add(createIndividualVo(individualEvaluation));
		}
		vo.setIndividualVos(individualVos);
		if(performance.getCreatedOn()!=null)
			vo.setCreatedOn(DateFormatter.convertDateToString(performance.getCreatedOn()));
		vo.setCreatedBy(performance.getCreatedBy().getUserName());
		vo.setNotes(performance.getNotes());
		return vo;
	}
	/**
	 * Method to create individual evaluation vo
	 * @param individual
	 * @return
	 */
	private IndividualEvaluationVo createIndividualVo(IndividualEvaluation individual)
	{
		IndividualEvaluationVo vo = new IndividualEvaluationVo();
		vo.setIndividualId( individual.getId());
		if(individual.getEmployee().getUserProfile()!=null)
			vo.setEmployee(individual.getEmployee().getUserProfile().getFirstName()+" "+individual.getEmployee().getUserProfile().getLastname());
		vo.setBranch(individual.getBranch());
		vo.setDesignation(individual.getDesignation());
		vo.setMaxMark(String.valueOf(individual.getMaxMark()));
		vo.setScore(String.valueOf(individual.getScore()));
		return vo;
	}
	/**
	 * method to return performance evaluation info object by id
	 * @param id
	 * @return
	 */
	public PerformanceEvaluationInfo findAndReturnEvaluationById(Long id){
		
		return (PerformanceEvaluationInfo)this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluationInfo.class)
							.add(Restrictions.eq("id", id)).uniqueResult();
	}
	/**
	 * method to return performance evaluation info Vo object by id
	 * @param id
	 * @return
	 */
	public PerformanceVo findEvaluationById(Long id){
		PerformanceEvaluationInfo info = findAndReturnEvaluationById(id);
		return createPerformanceVo(info);
	}
	/**
	 * Method to find employee evaluation details
	 * @param employeeCode
	 * @param id
	 * @return
	 */
	public IndividualEvaluationVo finddEmployeeEvaluationInfo(String employeeCode , Long id){
		
		IndividualEvaluationVo evaluationVo = new IndividualEvaluationVo();
		PerformanceEvaluationInfo performance = findAndReturnEvaluationById(id);
		if(id==null)
		{
			Employee emp = empDao.findAndReturnEmployeeByCode(employeeCode);
			evaluationVo.setBranch(emp.getBranch().getBranchName());
			evaluationVo.setDesignation(emp.getEmployeeDesignation().getDesignation());
		}
		else if(performance==null)
			throw new ItemNotFoundException("Performance Evaluation Not Found");
		else
		{
			Set<IndividualEvaluation> individuals = new HashSet<IndividualEvaluation>();
			individuals = performance.getIndividualEvaluation();
			for (IndividualEvaluation individualEvaluation : individuals) {
				if(individualEvaluation.getEmployee().getEmployeeCode().equalsIgnoreCase(employeeCode))
				{
					evaluationVo = createIndividualVo(individualEvaluation);
					break;
				}
			}
			if(evaluationVo.getEmployee()==null)
			{
				Employee emp = empDao.findAndReturnEmployeeByCode(employeeCode);
				evaluationVo.setBranch(emp.getBranch().getBranchName());
				evaluationVo.setDesignation(emp.getEmployeeDesignation().getDesignation());
			}
		}
		
		return evaluationVo;
	}
	/**
	 * Method to delete individual evaluation record
	 * @param id
	 */
	public void deleteEmployeeEvaluation(Long id , Long individualId){
		
		IndividualEvaluation individual = (IndividualEvaluation) this.sessionFactory.getCurrentSession().createCriteria(IndividualEvaluation.class)
				.add(Restrictions.eq("id", individualId)).uniqueResult();
		this.sessionFactory.getCurrentSession().delete(individual);
				
		PerformanceEvaluationInfo performance = findAndReturnEvaluationById(id);
		this.sessionFactory.getCurrentSession().merge(performance);
		
	}
	/**
	 * Method to find performance evaluations within date range
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PerformanceVo> findEvaluationWithinDateRange(String startDate , String endDate){
		
		Date start = DateFormatter.convertStringToDate(startDate);
		Date end = DateFormatter.convertStringToDate(endDate);
		List<PerformanceVo> performanceEvaluations = new ArrayList<PerformanceVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluationInfo.class)
				.add(Restrictions.between("date", start, end));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PerformanceEvaluationInfo> evaluations = criteria.list();
		for (PerformanceEvaluationInfo performanceEvaluationInfo : evaluations) {
			performanceEvaluations.add(createPerformanceVo(performanceEvaluationInfo));
		}
		return performanceEvaluations;
	}
}
