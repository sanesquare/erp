package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CandidateStatusDao;
import com.hrms.web.entities.CandidateStatus;
import com.hrms.web.entities.JobInterview;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.CandidatesStatusVo;

/**
 * 
 * @author Vinutha
 * @since 24-March-2015
 *
 */
@Repository
public class CandidateStatusDaoImpl extends AbstractDao implements CandidateStatusDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/**
	 * method to return candidate status object by id
	 * @param Long id
	 */
	public CandidateStatus findAndReturnStatusObjectById(Long id) {
		
		return (CandidateStatus) this.sessionFactory.getCurrentSession().createCriteria(CandidateStatus.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}
	/**
	 * method to save new status
	 * @param candidatesStatusVo 
	 */
	public void saveCandidatesStatus(CandidatesStatusVo candidatesStatusVo) {
		this.sessionFactory.getCurrentSession().save(candidatesStatusVo);
		
	}
	/**
	 * method to delete existing status
	 * @param candidatesStatusVo
	 */
	public void deleteCandidateStatus(CandidatesStatusVo candidatesStatusVo) {
		this.sessionFactory.getCurrentSession().delete(candidatesStatusVo);
		
	}
	/**
	 * method to list all status
	 * @return List<CandidatesStatusVo>
	 */
	@SuppressWarnings("unchecked")
	public List<CandidatesStatusVo> listAllStatus() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(CandidateStatus.class);
		  criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		  List<CandidateStatus> statusList= criteria.list();
		  
		//List<CandidateStatus> statusList=this.sessionFactory.getCurrentSession().createCriteria(CandidateStatus.class).list(); 
		List<CandidatesStatusVo> statusVoList= new ArrayList<CandidatesStatusVo>();
		for(CandidateStatus status : statusList)
		{
			statusVoList.add(createStatusVo(status));
		}
		return statusVoList;
	}
	/**
	 * method to find status by id
	 * @param Long id
	 * @return CandidatesStatusVo
	 */
	public CandidatesStatusVo findAndReturnStatusById(Long id) {
		
		CandidateStatus status = (CandidateStatus)this.sessionFactory.getCurrentSession().createCriteria(CandidateStatus.class).add(Restrictions.eq("id", id));
		return createStatusVo(status);
	}
	/**
	 * method to create status vo
	 * @param staus
	 * @return CandidatesStatusVo
	 */
	private CandidatesStatusVo createStatusVo(CandidateStatus status)
	{
		CandidatesStatusVo statusVo=new CandidatesStatusVo();
		statusVo.setStatusId(status.getId());
		statusVo.setStatus(status.getStatus());
		return statusVo;
	}
	/**
	 * Method to return status bject by name
	 * @param status
	 * @return CandidatesStatusVo
	 */
	public CandidateStatus findAndReturnStatusByName(String status) {
		CandidateStatus statusResult = (CandidateStatus)this.sessionFactory.getCurrentSession().createCriteria(CandidateStatus.class).add(Restrictions.eq("status", status));
		return statusResult;
	}

	
}
