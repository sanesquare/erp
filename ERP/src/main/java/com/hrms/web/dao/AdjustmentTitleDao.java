package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.AdjustmentTitle;
import com.hrms.web.vo.AdjustmentTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 14-April-2015
 */
public interface AdjustmentTitleDao {

	/**
	 * method to save Adjustment title
	 * @param titleVo
	 */
	public void saveAdjustmentTitle(AdjustmentTitleVo titleVo); 
	
	/**
	 * method to update Adjustment title
	 * @param titleVo
	 */
	public void updateAdjustmentTitle(AdjustmentTitleVo titleVo);
	
	/**
	 * method to delete Adjustment title
	 * @param AdjustmentTitleId
	 */
	public void deleteAdjustmentTitle(Long adjustmentTitleId);
	
	/**
	 * method to delete Adjustment title
	 * @param AdjustmentTitle
	 */
	public void deleteAdjustmentTitle(String adjustmentTitle);
	
	/**
	 * method to find Adjustment title
	 * @param AdjustmentTitleId
	 * @return
	 */
	public AdjustmentTitle findAdjustmentTitle(Long adjustmentTitleId);
	
	/**
	 * method to find Adjustment title
	 * @param AdjustmentTitleId
	 * @return
	 */
	public AdjustmentTitle findAdjustmentTitle(String adjustmentTitle);
	
	/**
	 * method to list all Adjustment titles
	 * @return
	 */
	public List<AdjustmentTitleVo> listAllAdjustmentTitles();
}
