package com.hrms.web.dao;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.User;
import com.hrms.web.vo.UserVo;

/**
 * 
 * @author shamsheer
 * @since 19-March-2015
 */
public interface UserDao {

	/**
	 * method to save new user
	 * @param userVo
	 */
	public void saveUser(UserVo userVo);
	
	/**
	 * method to update existing user
	 * @param userVo
	 */
	public void updateUser(UserVo userVo);
	
	/**
	 * method to delete user
	 * @param id
	 */
	public void deleteUser(Long id);
	
	/**
	 * method to find user by id
	 * @param id
	 * @return
	 */
	public User findUserById(Long id);
	
	/**
	 * method to find user by userName
	 * @param userName
	 * @return
	 */
	public User findUserByUsername(String userName);
	
	/**
	 * method to find user
	 * @param userName
	 * @return
	 */
	public UserVo findUser(String userName);
	
	
	/**
	 * method to find user by employee id
	 * @param employeeId
	 * @return
	 */
	public User findUserByEmployeeId(Long employeeId);
	
	/**
	 * method to change password
	 * @param userName
	 * @param newPassword
	 * @return
	 */
	public boolean updatePassword(String userName , String password, String newPassword);
}
