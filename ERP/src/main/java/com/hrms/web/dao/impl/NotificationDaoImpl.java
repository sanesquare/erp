package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.NotificationDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Notification;
import com.hrms.web.entities.NotificationType;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.NotificationVo;

/**
 * 
 * @author Vips
 *
 */
@Repository
public class NotificationDaoImpl extends AbstractDao implements NotificationDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/**
	 * 
	 */
	public NotificationVo saveNotification(NotificationVo notificationVo) {
		Notification notification = createNotification(notificationVo);
		if (notification.getId() != null && notification.getId() > 0) {
			this.sessionFactory.getCurrentSession().update(notification);
		} else {
			this.sessionFactory.getCurrentSession().save(notification);
		}
		notificationVo.setId(notification.getId());
		return notificationVo;
	}
	

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<NotificationType> fetchAllNotificationType() {
		Query query = this.sessionFactory.getCurrentSession().createQuery("from NotificationType");
		List<NotificationType> notificationTypes = new ArrayList<NotificationType>();
		notificationTypes = query.list();
		return notificationTypes;
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<Notification> fetchAllNotifications() {
		Query query = this.sessionFactory.getCurrentSession().createQuery("from Notification");
		List<Notification> notificationList = new ArrayList<Notification>();
		notificationList = query.list();
		return notificationList;
	}

	/**
	 * 
	 */
	public int deleteNotificationById(Long notificationId) {
		Notification notification = new Notification();
		notification.setId(notificationId);
		this.sessionFactory.getCurrentSession().delete(notification);
		return  1;
	}

	/**
	 * 
	 */
	public NotificationVo fetchNotificationById(Long notificationId) {
		NotificationVo notificationVo = null;
		Notification notification = (Notification) this.sessionFactory.getCurrentSession().load(Notification.class, notificationId);
		if (notification != null) {
			 notificationVo = createNotificationVo(notification);
		}
		return  notificationVo;
	}

	
	/**
	 * 
	 * @param notificationVo
	 * @return
	 */
	private Notification createNotification(NotificationVo notificationVo){
		Notification notification =  new Notification();
		if (notificationVo.getId() != null && notificationVo.getId() > 0) {
			notification.setId(notificationVo.getId());
		}
		notification.setMstReference(notificationVo.getMstReference());
		notification.setSendToSelf(notificationVo.getIsStoS());
		notification.setNotificationType(notificationVo.getNotificationType());
		for (Long employeeeId : notificationVo.getEmployeeIds()) {
			Employee employee =  new Employee();
			employee.setId(employeeeId);
			notification.getEmployees().add(employee);
		}
		
	
		return notification;
	}
	
	/**
	 * 
	 * @param notificationVo
	 * @return
	 */
	private NotificationVo createNotificationVo(Notification notification){
		NotificationVo notificationVo =  new NotificationVo();
		notificationVo.setId(notification.getId());
		notificationVo.setMstReference(notification.getMstReference());
		notificationVo.setIsStoS(notification.getSendToSelf());
		notificationVo.setNotificationType(notification.getNotificationType());
		for (Employee employee : notification.getEmployees()) {
			notificationVo.getEmployeeIds().add(employee.getId());
		}
		return notificationVo;
	}
}
