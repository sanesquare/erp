package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.BonusTitles;
import com.hrms.web.entities.DeductionTitles;
import com.hrms.web.vo.BonusTitleVo;
import com.hrms.web.vo.DeductionTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
public interface DeductionTitleDao {

	/**
	 * method to save Deduction title
	 * @param titleVo
	 */
	public void saveDeductionTitle(DeductionTitleVo titleVo); 
	
	/**
	 * method to update Deduction title
	 * @param titleVo
	 */
	public void updateDeductionTitle(DeductionTitleVo titleVo);
	
	/**
	 * method to delete Deduction title
	 * @param DeductionTitleId
	 */
	public void deleteDeductionTitle(Long deductionTitleId);
	
	/**
	 * method to delete Deduction title
	 * @param DeductionTitle
	 */
	public void deleteDeductionTitle(String deductionTitle);
	
	/**
	 * method to find Deduction title
	 * @param DeductionTitleId
	 * @return
	 */
	public DeductionTitles findDeductionTitle(Long deductionTitleId);
	
	/**
	 * method to find Deduction title
	 * @param DeductionTitleId
	 * @return
	 */
	public DeductionTitles findDeductionTitle(String deductionTitle);
	
	/**
	 * method to list all Deduction titles
	 * @return
	 */
	public List<DeductionTitleVo> listAllDeductionTitles();
}
