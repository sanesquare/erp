package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.Employee;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.ProjectVo;

/**
 * 
 * @author Sangeeth and Bibin
 *
 */

public interface DepartmentDao {

	/**
	 * Method to save new Department
	 * 
	 * @param departmentVo
	 * @return void
	 */
	public void saveDepartment(DepartmentVo departmentVo);
	
	
	/**
	 * method to update department document
	 * @param projectVo
	 */
	public DepartmentVo updateDepartmentDocument(DepartmentVo departmentVo);
	

	/**
	 * method to update existing department
	 * 
	 * @param departmentVo
	 * @return void
	 */
	public void updateDepartment(DepartmentVo departmentVo);

	/**
	 * method to list all employees
	 * 
	 * @return List<Employee>
	 */
	/**
	 * method to findAllBranches
	 * 
	 * @return
	 */
	public List<DepartmentVo> findAllDepartments();

	/**
	 * method to findAlldepartment details
	 * 
	 * @return
	 */
	public List<DepartmentVo> findAllDepartmentDetails();

	/**
	 * method to find department by using department Id
	 * 
	 * @param id
	 * @return DepartmentVo
	 */
	public DepartmentVo findDepartmentById(Long departmentId);

	/**
	 * method to delete existing department
	 * 
	 * @param departmentVo
	 * @return void
	 */
	public void deleteDepartment(Long id);

	/**
	 * method to delete department document
	 * 
	 * @param path
	 */
	public void deleteDepartmentDocument(Long path);

	/**
	 * method to find department object by id
	 * 
	 * @param id
	 * @return
	 */
	public Department findDepartment(Long id);

	/**
	 * 
	 * @param branch
	 * @return
	 */
	public List<DepartmentVo> findDepartmentByBranch(Branch branch);

}
