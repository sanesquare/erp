package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.LoanStatusDao;
import com.hrms.web.entities.LoanStatus;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.LoanStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Repository
public class LoanStatusDaoImpl extends AbstractDao implements LoanStatusDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveLoanStatus(LoanStatusVo loanStatusVo) {
		for (String status : StringSplitter.splitWithComma(loanStatusVo.getLoanStatus())) {
			if (findLoanStatus(status.trim()) != null)
				throw new DuplicateItemException("Duplicate Loan Status : " + status.trim());
			LoanStatus loanStatus = new LoanStatus();
			loanStatus.setStatus(status.trim());
			this.sessionFactory.getCurrentSession().save(loanStatus);
		}
	}

	public void updateLoanStatus(LoanStatusVo loanStatusVo) {
		// TODO Auto-generated method stub

	}

	public void deleteLoanStatus(Long statusId) {
		LoanStatus loanStatus = findLoanStatus(statusId);
		if (loanStatus == null)
			throw new ItemNotFoundException("Loan Status Details Not Found");
		this.sessionFactory.getCurrentSession().delete(loanStatus);
	}

	public LoanStatus findLoanStatus(Long statusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LoanStatus.class)
				.add(Restrictions.eq("id", statusId));
		if (criteria.uniqueResult() == null)
			return null;
		return (LoanStatus) criteria.uniqueResult();
	}

	public LoanStatus findLoanStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LoanStatus.class)
				.add(Restrictions.eq("status", status));
		if (criteria.uniqueResult() == null)
			return null;
		return (LoanStatus) criteria.uniqueResult();
	}

	/**
	 * method to create loanStatusVo object
	 * 
	 * @param loanStatus
	 * @return loanStatusVo
	 */
	private LoanStatusVo createLoanStatusVo(LoanStatus loanStatus) {
		LoanStatusVo loanStatusVo = new LoanStatusVo();
		loanStatusVo.setLoanStatus(loanStatus.getStatus());
		loanStatusVo.setLoanStatusId(loanStatus.getId());
		return loanStatusVo;
	}

	public LoanStatusVo findLoanStatusById(Long statusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LoanStatus.class)
				.add(Restrictions.eq("id", statusId));
		if (criteria.uniqueResult() == null)
			return null;
		return createLoanStatusVo((LoanStatus) criteria.uniqueResult());
	}

	public LoanStatusVo findLoanStatusByStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LoanStatus.class)
				.add(Restrictions.eq("status", status));
		if (criteria.uniqueResult() == null)
			return null;
		return createLoanStatusVo((LoanStatus) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<LoanStatusVo> findAllLoanStatus() {
		List<LoanStatusVo> loanStatusVos = new ArrayList<LoanStatusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LoanStatus.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<LoanStatus> loanStatuses = criteria.list();
		for (LoanStatus loanStatus : loanStatuses)
			loanStatusVos.add(createLoanStatusVo(loanStatus));
		return loanStatusVos;
	}

}
