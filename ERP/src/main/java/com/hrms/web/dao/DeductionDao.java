package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Deductions;
import com.hrms.web.entities.Employee;
import com.hrms.web.vo.DeductionVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
public interface DeductionDao {

	/**
	 * method to save deductions
	 * @param deductionVo
	 * @return
	 */
	public Long saveDeduction(DeductionVo deductionVo);
	
	/**
	 * method to update deductions
	 * @param deductionVo
	 */
	public void updateDeduction(DeductionVo deductionVo);
	
	/**
	 * mehod to delete deductions
	 * @param deduction
	 */
	public void deleteDeduction(Long deduction);
	
	/**
	 * method to find and return Deduction object by id
	 * @param deductionId
	 * @return
	 */
	public Deductions getDeduction(Long deductionId);
	
	/**
	 * method to get deduction by id
	 * @param deductionId
	 * @return
	 */
	public DeductionVo getDeductionById(Long deductionId);
	
	/**
	 * method to list all deductions
	 * @return
	 */
	public List<DeductionVo> listAllDeduction();
	
	/**
	 * method to list deductions by employee
	 * @param employeeId
	 * @return
	 */
	public List<DeductionVo> listDeductionByEmployee(String employeeCode);
	
	/**
	 * list deduction by employee
	 * @param employee
	 * @return
	 */
	public List<DeductionVo> listDeductionByEmployee(Employee employee);
	
	/**
	 * method to list deductions by status
	 * @param statusId
	 * @return
	 */
	public List<DeductionVo> listDeductionByStatus(Long statusId);
	
	/**
	 * method to get deduction by employee and title
	 * @param deductionId
	 * @param employeeId
	 * @return
	 */
	public DeductionVo getDeductionByEmployeeAndTitle(Long titleId , String employeeCode);
	
	/**
	 * method to list deduction by employee , start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<DeductionVo> listDeductionByEmployeeStartDateEndDate(Long employeeId , Date startDate , Date endDate);
	
	/**
	 *  method to list deduction by employee , start date & end date
	 * @param employee
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<DeductionVo> listDeductionByEmployeeStartDateEndDate(Employee employee , Date startDate , Date endDate);
	
	/**
	 * method to list deduction by start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<DeductionVo> listDeductionByStartDateEndDate(Date startDate , Date endDate);
}
