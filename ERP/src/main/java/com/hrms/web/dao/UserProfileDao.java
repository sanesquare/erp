package com.hrms.web.dao;

import com.hrms.web.entities.UserProfile;

/**
 * 
 * @author shamsheer
 * @since 19-March-2015
 */
public interface UserProfileDao {

	/**
	 * method to find userProfile by employee id
	 * @param EmployeeId
	 * @return
	 */
	public UserProfile findUserProfilByEmployeeId(Long EmployeeId);
}
