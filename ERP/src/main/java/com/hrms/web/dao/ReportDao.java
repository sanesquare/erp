package com.hrms.web.dao;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.entities.Attendance;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Leaves;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AttendanceVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.HolidayVo;
import com.hrms.web.vo.LeavesVo;
import com.hrms.web.vo.SalaryDailyWagesVo;
import com.hrms.web.vo.SalaryHourlyWageVo;

/**
 * 
 * @author Vinutha
 * @since 17-April-2015
 *
 */
public interface ReportDao {
	
	/**
	 * Method to get number of departments
	 * @return
	 */
	public Long deparmentCount();
	/**
	 * Method to get number of projects
	 * @return
	 */
	public Long projectCount();
	/**
	 * Method to list all holidays for a branch within given date range
	 * @param branch
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<HolidayVo> findHolidaysForBranchWithinDateRange(Branch branch,Date startDate , Date endDate);
	/**
	 * Method to list all holidays within date range
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<HolidayVo> findHolidayBetweenDate(Date startDate, Date endDate) ;
	/**
	 * Method to get employeeVo by code
	 * @param code
	 * @return
	 */
	public EmployeeVo findEmployeeByCode(String code) ;
	/**
	 * Method to get distinct days on which employee was present
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<Timestamp>  listAttendanceObjectByEmployeeAndDate(Long employeeId, String startDate, String endDate);
	/**
	 * Method to get hourly wage for a partcular day
	 * @param employeeCode
	 * @param startDate
	 * @return
	 */
	public SalaryHourlyWageVo getHourlyWageForDay(String employeeCode ,Date startDate);
	/**
	 * Method to get daily wage for a day
	 * @param employeeCode
	 * @param date
	 * @return
	 */
	public SalaryDailyWagesVo getDailyWageForDay(String employeeCode ,Date date);
	/**
	 * Method to get all leaves taken by employee
	 * @param employeeId
	 * @return
	 */
	public List<LeavesVo> getAllLeavesTakenByEmployee(Long employeeId , String startDate , String endDate);
}
