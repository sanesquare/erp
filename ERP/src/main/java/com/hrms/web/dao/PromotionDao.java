package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.PromotionVo;

public interface PromotionDao {
	
	/**
	 * Method to save new Promotion
	 * @param promotionVo
	 * @return void
	 */
	public void savePromotion(PromotionVo promotionVo);
	
	
	/**
	 * method to findAllpromotion details
	 * @return
	 */
	public List<PromotionVo> findAllPromotionDetails();
	
	
	/**
	 * method to delete existing promotion
	 * @param promotioVo
	 * @return void
	 */
	public void deletePromotion(Long id);
	
	/**
	 * method to update promotion document
	 * @param promotionVo
	 */
	public PromotionVo updatePromotionDocument(PromotionVo promotionVo);
	
	
	/**
	 * method to find department by using department Id
	 * @param id
	 * @return DepartmentVo
	 */
	public PromotionVo findPromotionById(Long promotionId);
	
	/**
	 * method to update existing promotion
	 * @param promotionVo
	 * @return void
	 */
	public void updatePromotion(PromotionVo promotionVo);
	
	/**
	 * method to delete department document
	 * 
	 * @param path
	 */
	public void deletePromotionDocument(Long path);

}
