package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.WorkShiftDao;
import com.hrms.web.entities.EmployeeWorkShift;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.WorkShiftVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Repository
public class WorkShiftDaoImpl extends AbstractDao implements WorkShiftDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveWorkShift(WorkShiftVo workShiftVo) {
		for (String shift : StringSplitter.splitWithComma(workShiftVo.getWorkShift())) {
			if (findWorkShift(shift.trim()) != null)
				throw new DuplicateItemException("Duplicate Work Shift : " + shift.trim());
			EmployeeWorkShift workShift = findWorkShift(workShiftVo.getTempWorkShift());
			if (workShift == null)
				workShift = new EmployeeWorkShift();
			workShift.setWorkShift(shift.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(workShift);
		}

	}

	public void updateWorkShift(WorkShiftVo workShiftVo) {
		EmployeeWorkShift shift = findWorkShift(workShiftVo.getWorkShiftId());
		if (shift == null)
			throw new ItemNotFoundException("Work Shift Details Not Found.");
		shift.setWorkShift(workShiftVo.getWorkShift());
		this.sessionFactory.getCurrentSession().merge(shift);
	}

	public void deleteWorkShift(Long workShiftId) {
		EmployeeWorkShift shift = findWorkShift(workShiftId);
		if (shift == null)
			throw new ItemNotFoundException("Work Shift Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(shift);
	}

	public EmployeeWorkShift findWorkShift(Long workShiftId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeWorkShift.class)
				.add(Restrictions.eq("id", workShiftId));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeWorkShift) criteria.uniqueResult();
	}

	public EmployeeWorkShift findWorkShift(String shiftType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeWorkShift.class)
				.add(Restrictions.eq("workShift", shiftType));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeWorkShift) criteria.uniqueResult();
	}

	/**
	 * method to create WorkShiftVo object
	 * 
	 * @param workShift
	 * @return
	 */
	private WorkShiftVo createWorkShiftVo(EmployeeWorkShift workShift) {
		WorkShiftVo workShiftVo = new WorkShiftVo();
		workShiftVo.setWorkShift(workShift.getWorkShift());
		workShiftVo.setWorkShiftId(workShift.getId());
		return workShiftVo;
	}

	public WorkShiftVo findWorkShiftById(Long workShiftId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeWorkShift.class)
				.add(Restrictions.eq("id", workShiftId));
		if (criteria.uniqueResult() == null)
			return null;
		return createWorkShiftVo((EmployeeWorkShift) criteria.uniqueResult());
	}

	public WorkShiftVo findWorkShiftByType(String shiftType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeWorkShift.class)
				.add(Restrictions.eq("workShift", shiftType));
		if (criteria.uniqueResult() == null)
			return null;
		return createWorkShiftVo((EmployeeWorkShift) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<WorkShiftVo> findAllWorkShift() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeWorkShift.class);
		List<WorkShiftVo> workShiftVos = new ArrayList<WorkShiftVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeWorkShift> workShifts = criteria.list();
		for (EmployeeWorkShift workShift : workShifts)
			workShiftVos.add(createWorkShiftVo(workShift));
		return workShiftVos;
	}

}
