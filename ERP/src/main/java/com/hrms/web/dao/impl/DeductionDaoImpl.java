package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.DeductionDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.entities.DeductionTitles;
import com.hrms.web.entities.Deductions;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.DeductionVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
@Repository
public class DeductionDaoImpl extends AbstractDao implements DeductionDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;
	
	/**
	 * method to save deduction
	 */
	public Long saveDeduction(DeductionVo deductionVo) {
		Deductions deductions = new Deductions();

		Employee employee = employeeDao.findAndReturnEmployeeByCode(deductionVo.getEmployeeCode());
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		deductions.setEmployee(employee);

		DeductionTitles titles = new DeductionTitles();
		titles.setId(deductionVo.getTitleId());
		deductions.setTitles(titles);
		
		deductions.setAmount(new BigDecimal(deductionVo.getAmount()));
		deductions.setDescription(deductionVo.getDescription());
		if(!deductionVo.getDate().isEmpty())
			deductions.setDate(DateFormatter.convertStringToDate(deductionVo.getDate()));
		deductions.setNotes(deductionVo.getNotes());

		return (Long) this.sessionFactory.getCurrentSession().save(deductions);
	}

	/**
	 * method to update deductions
	 */
	public void updateDeduction(DeductionVo deductionVo) {
		Deductions deductions = getDeduction(deductionVo.getDeductionId());

		Employee employee = employeeDao.findAndReturnEmployeeByCode(deductionVo.getEmployeeCode());
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		deductions.setEmployee(employee);

		DeductionTitles titles = new DeductionTitles();
		titles.setId(deductionVo.getTitleId());
		deductions.setTitles(titles);
		
		deductions.setAmount(new BigDecimal(deductionVo.getAmount()));
		deductions.setDescription(deductionVo.getDescription());
		if(!deductionVo.getDate().isEmpty())
			deductions.setDate(DateFormatter.convertStringToDate(deductionVo.getDate()));
		deductions.setNotes(deductionVo.getNotes());
		
		this.sessionFactory.getCurrentSession().merge(deductions);
	}

	/**
	 * method to delete deductions
	 */
	public void deleteDeduction(Long deduction) {
		Deductions deductions = getDeduction(deduction);
		if(deduction==null)
			throw new ItemNotFoundException("Deduction Not Exist.");
		this.sessionFactory.getCurrentSession().delete(deductions);
	}

	/**
	 * method to get deduction object by id
	 */
	public Deductions getDeduction(Long deductionId) {
		return (Deductions) this.sessionFactory.getCurrentSession().createCriteria(Deductions.class)
				.add(Restrictions.eq("id", deductionId)).uniqueResult();
	}

	/**
	 * method to get deduction by id
	 */
	public DeductionVo getDeductionById(Long deductionId) {
		Deductions deductions = getDeduction(deductionId);
		if(deductions==null)
			throw new ItemNotFoundException("Deduction Not Exist.");
		return createDeductionVo(deductions);
	}

	/**
	 * method to create deduction vo
	 * @param deductions
	 * @return
	 */
	private DeductionVo createDeductionVo(Deductions deductions){
		DeductionVo vo = new DeductionVo();
		vo.setDeductionId(deductions.getId());
		vo.setId(deductions.getId());
		vo.setEmployeeId(deductions.getEmployee().getId());
		vo.setEmployeeCode(deductions.getEmployee().getEmployeeCode());
		vo.setEmployee(deductions.getEmployee().getUserProfile().getFirstName()+" "+deductions.getEmployee().getUserProfile().getLastname());
		vo.setAmount(deductions.getAmount().toString());
		vo.setTitle(deductions.getTitles().getTitle());
		vo.setTitleId(deductions.getTitles().getId());
		vo.setDescription(deductions.getDescription());
		if(deductions.getDate()!=null)
			vo.setDate(DateFormatter.convertDateToString(deductions.getDate()));
		vo.setNotes(deductions.getNotes());
		vo.setCreatedBy(deductions.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(deductions.getCreatedOn()));
		
		return vo;
	}
	
	/**
	 * method to list all deductions
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<DeductionVo> listAllDeduction() {
		List<DeductionVo> vos = new ArrayList<DeductionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Deductions.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Deductions> deductions = criteria.list();
		for(Deductions deduction : deductions){
			vos.add(createDeductionVo(deduction));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<DeductionVo> listDeductionByEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<DeductionVo> vos = new ArrayList<DeductionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Deductions.class);
		Criterion byEmployee = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression logicalExpression = Restrictions.or(byEmployee, createdBy);
		criteria.add(logicalExpression);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Deductions> deductions = criteria.list();
		for(Deductions deduction : deductions){
			vos.add(createDeductionVo(deduction));
		}
		return vos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<DeductionVo> listDeductionByStatus(Long statusId) {
		List<DeductionVo> vos = new ArrayList<DeductionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Deductions.class)
				.add(Restrictions.eq("deductionStatus.id", statusId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Deductions> deductions = criteria.list();
		for(Deductions deduction : deductions){
			vos.add(createDeductionVo(deduction));
		}
		return vos;
	}

	/**
	 * method to get deduction by empoyee and title
	 */
	public DeductionVo getDeductionByEmployeeAndTitle(Long titleId, String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Deductions deduction = (Deductions) this.sessionFactory.getCurrentSession().createCriteria(Deductions.class)
				.add(Restrictions.eq("employee", employee)).add(Restrictions.eq("titles.id", titleId)).uniqueResult();
		return createDeductionVo(deduction);
	}

	/**
	 * method to list deduction by employee , start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<DeductionVo> listDeductionByEmployeeStartDateEndDate(Long employeeId, Date startDate, Date endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<DeductionVo> vos = new ArrayList<DeductionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Deductions.class);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Deductions> deductions = criteria.list();
		for(Deductions deduction : deductions){
			vos.add(createDeductionVo(deduction));
		}
		return vos;
	}

	/**
	 * method to list deduction by start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<DeductionVo> listDeductionByStartDateEndDate(Date startDate, Date endDate) {
		List<DeductionVo> vos = new ArrayList<DeductionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Deductions.class);
		criteria.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Deductions> deductions = criteria.list();
		for(Deductions deduction : deductions){
			vos.add(createDeductionVo(deduction));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<DeductionVo> listDeductionByEmployee(Employee employee) {
		List<DeductionVo> vos = new ArrayList<DeductionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Deductions.class)
				.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Deductions> deductions = criteria.list();
		for(Deductions deduction : deductions){
			vos.add(createDeductionVo(deduction));
		}
		return vos;
	}

	/**
	 *  method to list deduction by employee , start date & end date
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<DeductionVo> listDeductionByEmployeeStartDateEndDate(Employee employee, Date startDate, Date endDate) {
		List<DeductionVo> vos = new ArrayList<DeductionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Deductions.class);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Deductions> deductions = criteria.list();
		for(Deductions deduction : deductions){
			vos.add(createDeductionVo(deduction));
		}
		return vos;
	}

}
