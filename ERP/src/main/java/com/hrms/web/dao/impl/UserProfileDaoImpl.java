package com.hrms.web.dao.impl;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.UserProfileDao;
import com.hrms.web.entities.UserProfile;
import com.hrms.web.service.UserActivitiesService;

/**
 * 
 * @author shamsheer
 * @since 19-March-2015
 */
@Repository
public class UserProfileDaoImpl extends AbstractDao implements UserProfileDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/** 
	 * method to find userProfile by employeeId
	 */
	public UserProfile findUserProfilByEmployeeId(Long EmployeeId) {
		return (UserProfile) this.sessionFactory.getCurrentSession().createCriteria(UserProfile.class)
				.add(Restrictions.eq("employee.id", EmployeeId)).uniqueResult();
	}

}
