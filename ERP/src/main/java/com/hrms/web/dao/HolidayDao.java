package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Holiday;
import com.hrms.web.vo.HolidayVo;

/**
 * 
 * @author Jithin Mohan
 * @since 29-March-2015
 *
 */
public interface HolidayDao {

	/**
	 * method to save Holiday
	 * 
	 * @param holidayVo
	 */
	public void saveHoliday(HolidayVo holidayVo);

	/**
	 * method to update Holiday
	 * 
	 * @param holidayVo
	 */
	public void updateHoliday(HolidayVo holidayVo);

	/**
	 * method to delete Holiday
	 * 
	 * @param holidayId
	 */
	public void deleteHoliday(Long holidayId);

	/**
	 * method to find Holiday by id
	 * 
	 * @param holidayId
	 * @return Holiday
	 */
	public Holiday findHoliday(Long holidayId);

	/**
	 * method to find Holiday by id
	 * 
	 * @param holidayId
	 * @return HolidayVo
	 */
	public HolidayVo findHolidayById(Long holidayId);

	/**
	 * method to find Holiday by branch
	 * 
	 * @param branch
	 * @return List<HolidayVo>
	 */
	public List<HolidayVo> findHolidayByBranch(Branch branch);

	/**
	 * method to find Holidays by start date
	 * 
	 * @param startDate
	 * @return List<HolidayVo>
	 */
	public List<HolidayVo> findHolidayByStartDate(Date startDate);

	/**
	 * method to find Holidays by end date
	 * 
	 * @param endDate
	 * @return List<HolidayVo>
	 */
	public List<HolidayVo> findHolidayByEndDate(Date endDate);

	/**
	 * method to find Holidays by between two dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return List<HolidayVo>
	 */
	public List<HolidayVo> findHolidayBetweenDate(Date startDate, Date endDate);

	/**
	 * method to find all Holidays
	 * 
	 * @return List<HolidayVo>
	 */
	public List<HolidayVo> findAllHoliday();
}
