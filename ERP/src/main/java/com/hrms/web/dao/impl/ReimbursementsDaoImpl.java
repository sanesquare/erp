package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ForwardApplicationStatusDao;
import com.hrms.web.dao.ReimbursementCategoryDao;
import com.hrms.web.dao.ReimbursementDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.entities.ReimbursementCategory;
import com.hrms.web.entities.Reimbursements;
import com.hrms.web.entities.ReimbursementsDocuments;
import com.hrms.web.entities.ReimbursementsItems;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.ReimbursementsDocumentVo;
import com.hrms.web.vo.ReimbursementsItemsVo;
import com.hrms.web.vo.ReimbursementsVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Shamsheer
 * @since 17-April-2015
 */
@Repository
public class ReimbursementsDaoImpl extends AbstractDao implements ReimbursementDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private ForwardApplicationStatusDao statusDao;

	@Autowired
	private ReimbursementCategoryDao categoryDao;

	@Autowired
	private TempNotificationDao notificationDao;

	/**
	 * method to save reimbursements
	 */
	public Long saveReimbursement(ReimbursementsVo vo) {
		Reimbursements reimbursements = new Reimbursements();

		Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		reimbursements.setEmployee(employee);

		for (ReimbursementsItemsVo itemsVo : vo.getItemsVos()) {
			ReimbursementsItems items = setItems(itemsVo, reimbursements);
			if (items != null)
				reimbursements.getItems().add(items);
		}
		ForwardApplicationStatus status = null;
		if (vo.getSuperiorIds() != null && !vo.getSuperiorIds().contains(0L)) {
			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject("A Reimbursement Request Has Been Received From "
					+ employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname() + ". ");
			notificationVo.getEmployeeIds().addAll(vo.getSuperiorIds());
			notificationDao.saveNotification(notificationVo);

			reimbursements.setForwardApplicationTo(setForwardApplicationTo(vo.getSuperiorIds()));
			status = statusDao.getStatus(StatusConstants.REQUESTED);// Requested
		} else {
			if (vo.getStatusId() != null) {
				status = statusDao.getStatus(vo.getStatus());// As per use
																// selection
			} else {
				status = statusDao.getStatus(StatusConstants.APPROVED);// Approved
			}
		}
		reimbursements.setStatus(status);
		reimbursements.setNotes(vo.getNotes());
		reimbursements.setDescription(vo.getDescription());
		reimbursements.setTitle(vo.getTitle());

		if (vo.getDate() != "" || vo.getDate() != null)
			reimbursements.setDate(DateFormatter.convertStringToDate(vo.getDate()));

		reimbursements.setAmount(new BigDecimal(vo.getAmount()));
		return (Long) this.sessionFactory.getCurrentSession().save(reimbursements);
	}

	/**
	 * method to set items
	 * 
	 * @param itemsVos
	 * @param reimbursements
	 * @return
	 */
	private ReimbursementsItems setItems(ReimbursementsItemsVo itemsVo, Reimbursements reimbursements) {
		if (itemsVo.getCategoryId() != null) {
			ReimbursementsItems reimbursementsItems = (ReimbursementsItems) this.sessionFactory.getCurrentSession()
					.createCriteria(ReimbursementsItems.class).add(Restrictions.eq("id", itemsVo.getId()))
					.uniqueResult();
			if (reimbursementsItems == null)
				reimbursementsItems = new ReimbursementsItems();
			if (itemsVo.getCategoryId() != null) {
				ReimbursementCategory category = categoryDao.findReimbursementCategory(itemsVo.getCategoryId());
				if (category == null)
					throw new ItemNotFoundException("Category Not Exist.");
				reimbursementsItems.setCategory(category);
			}
			reimbursementsItems.setReimbursements(reimbursements);
			if (itemsVo.getDate() != "")
				reimbursementsItems.setDate(DateFormatter.convertStringToDate(itemsVo.getDate()));
			reimbursementsItems.setItem(itemsVo.getItem());
			reimbursementsItems.setReceipt(itemsVo.getReceipt());
			reimbursementsItems.setCountry(itemsVo.getCountry());
			if (!itemsVo.getAmount().isEmpty())
				reimbursementsItems.setAmount(new BigDecimal(itemsVo.getAmount()));
			return reimbursementsItems;
		}
		return null;
	}

	/**
	 * method to set superiors
	 * 
	 * @param ids
	 * @return
	 */
	private Set<Employee> setForwardApplicationTo(List<Long> ids) {
		Set<Employee> superiors = new HashSet<Employee>();
		for (Long id : ids) {
			Employee superior = employeeDao.findAndReturnEmployeeById(id);
			if (superior == null)
				throw new ItemNotFoundException("Employee Not Exist.");

			superiors.add(superior);
		}
		return superiors;
	}

	/**
	 * method to update reimbursements
	 */
	public void updateReimbursement(ReimbursementsVo vo) {

		Reimbursements reimbursements = getReimbursements(vo.getReimbursementId());
		if (reimbursements == null)
			throw new ItemNotFoundException("Reimbursements Not Exist.");
		reimbursements.getItems().clear();
		Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		reimbursements.setEmployee(employee);

		for (ReimbursementsItemsVo itemsVo : vo.getItemsVos()) {
			ReimbursementsItems items = setItems(itemsVo, reimbursements);
			if (items != null)
				reimbursements.getItems().add(items);
		}

		ForwardApplicationStatus status = null;
		if (vo.getSuperiorIds() != null) {
			reimbursements.setForwardApplicationTo(setForwardApplicationTo(vo.getSuperiorIds()));
			status = statusDao.getStatus(StatusConstants.REQUESTED);// Requested
		} else {
			reimbursements.getForwardApplicationTo().clear();
			if (vo.getStatusId() != null) {
				status = statusDao.getStatus(vo.getStatus());// As per use
																// selection
			} else {
				status = statusDao.getStatus(StatusConstants.APPROVED);// Approved
			}
		}
		reimbursements.setStatus(status);
		reimbursements.setNotes(vo.getNotes());
		reimbursements.setDescription(vo.getDescription());
		reimbursements.setTitle(vo.getTitle());

		if (vo.getDate() != "")
			reimbursements.setDate(DateFormatter.convertStringToDate(vo.getDate()));

		reimbursements.setAmount(new BigDecimal(vo.getAmount()));

		this.sessionFactory.getCurrentSession().merge(reimbursements);
	}

	/**
	 * method to delete reimbursements
	 */
	public void deleteReimbursements(Long reimbursementId) {
		Reimbursements reimbursements = getReimbursements(reimbursementId);
		if (reimbursements == null)
			throw new ItemNotFoundException("Reimbursements Not Exist.");
		this.sessionFactory.getCurrentSession().delete(reimbursements);
	}

	/**
	 * method to get reimbursements object
	 */
	public Reimbursements getReimbursements(Long reimbursementId) {
		return (Reimbursements) this.sessionFactory.getCurrentSession().createCriteria(Reimbursements.class)
				.add(Restrictions.eq("id", reimbursementId)).uniqueResult();
	}

	/**
	 * method to update documents
	 */
	public void updateDocuments(ReimbursementsVo vo) {
		Reimbursements reimbursements = getReimbursements(vo.getReimbursementId());
		if (reimbursements == null)
			throw new ItemNotFoundException("Reimbursements Not Exist.");
		reimbursements.setDocuments(setDocuments(vo.getDocumentVos(), reimbursements));
		this.sessionFactory.getCurrentSession().merge(reimbursements);
	}

	/**
	 * method to set documents
	 * 
	 * @param vos
	 * @param reimbursements
	 * @return
	 */
	private Set<ReimbursementsDocuments> setDocuments(List<ReimbursementsDocumentVo> vos, Reimbursements reimbursements) {
		Set<ReimbursementsDocuments> documents = new HashSet<ReimbursementsDocuments>();
		for (ReimbursementsDocumentVo vo : vos) {
			ReimbursementsDocuments document = new ReimbursementsDocuments();
			document.setReimbursements(reimbursements);
			document.setUrl(vo.getUrl());
			document.setFileName(vo.getFileName());
			documents.add(document);
		}
		return documents;
	}

	/**
	 * method to delete documents
	 */
	public void deleteDocuments(String url) {
		ReimbursementsDocuments documents = (ReimbursementsDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(ReimbursementsDocuments.class).add(Restrictions.eq("url", url)).uniqueResult();
		if (documents == null)
			throw new ItemNotFoundException("Document Not Exist.");
		Reimbursements reimbursements = documents.getReimbursements();
		if (reimbursements == null)
			throw new ItemNotFoundException("Reimbursements Not Exist.");
		reimbursements.getDocuments().remove(documents);
		this.sessionFactory.getCurrentSession().merge(reimbursements);
		this.sessionFactory.getCurrentSession().delete(documents);
	}

	/**
	 * method to list all reimbursements
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ReimbursementsVo> listAllReimbursements() {
		List<ReimbursementsVo> vos = new ArrayList<ReimbursementsVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reimbursements.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Reimbursements> reimbursements = criteria.list();
		for (Reimbursements reimbursement : reimbursements) {
			vos.add(createReimbursementsVo(reimbursement));
		}
		return vos;
	}

	/**
	 * method to create reimbursements Vo
	 * 
	 * @param reimbursements
	 * @return
	 */
	private ReimbursementsVo createReimbursementsVo(Reimbursements reimbursements) {
		ReimbursementsVo vo = new ReimbursementsVo();
		vo.setReimbursementId(reimbursements.getId());
		vo.setEmployeeId(reimbursements.getEmployee().getId());
		vo.setEmployee(reimbursements.getEmployee().getUserProfile().getFirstName() + " "
				+ reimbursements.getEmployee().getUserProfile().getLastname());
		vo.setEmployeeCode(reimbursements.getEmployee().getEmployeeCode());
		vo.setTitle(reimbursements.getTitle());
		if (reimbursements.getItems() != null)
			vo.setItemsVos(creteItemsVo(reimbursements.getItems()));
		vo.setDescription(reimbursements.getDescription());
		vo.setNotes(reimbursements.getNotes());
		if (reimbursements.getAmount() != null)
			vo.setAmount(reimbursements.getAmount().toString());
		if (reimbursements.getDate() != null)
			vo.setDate(DateFormatter.convertDateToString(reimbursements.getDate()));
		vo.setCreatedBy(reimbursements.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(reimbursements.getCreatedOn()));

		if (reimbursements.getForwardApplicationTo() != null) {
			List<Long> superiorIds = new ArrayList<Long>();
			List<Long> superiorIdsAjax = new ArrayList<Long>();
			List<String> superiorName = new ArrayList<String>();
			List<String> name = new ArrayList<String>();
			for (Employee superior : reimbursements.getForwardApplicationTo()) {
				superiorIds.add(superior.getId());
				superiorIdsAjax.add(superior.getId());
				superiorName.add(superior.getEmployeeCode());
				if (superior.getUserProfile() != null)
					name.add(superior.getUserProfile().getFirstName() + " " + superior.getUserProfile().getLastname());
			}
			vo.setSuperiorAjaxIds(superiorIdsAjax);
			vo.setSuperiorIds(superiorIds);
			vo.setSuperiorCodes(superiorName);
			vo.setSuperiorName(name);
		}

		if (reimbursements.getDocuments() != null) {
			vo.setDocumentVos(createDocumentsVo(reimbursements.getDocuments()));
		}

		vo.setStatus(reimbursements.getStatus().getName());
		vo.setStatusId(reimbursements.getStatus().getId());
		vo.setStatusDescription(reimbursements.getStatusDescription());
		return vo;
	}

	/**
	 * method to create document vos
	 * 
	 * @param documents
	 * @return
	 */
	private List<ReimbursementsDocumentVo> createDocumentsVo(Set<ReimbursementsDocuments> documents) {
		List<ReimbursementsDocumentVo> vos = new ArrayList<ReimbursementsDocumentVo>();
		for (ReimbursementsDocuments document : documents) {
			ReimbursementsDocumentVo vo = new ReimbursementsDocumentVo();
			vo.setUrl(document.getUrl());
			vo.setFileName(document.getFileName());
			vos.add(vo);
		}
		return vos;
	}

	/**
	 * method to create items vo
	 * 
	 * @param items
	 * @return
	 */
	private List<ReimbursementsItemsVo> creteItemsVo(Set<ReimbursementsItems> items) {
		List<ReimbursementsItemsVo> vos = new ArrayList<ReimbursementsItemsVo>();
		for (ReimbursementsItems item : items) {
			ReimbursementsItemsVo vo = new ReimbursementsItemsVo();
			vo.setId(item.getId());
			if (item.getAmount() != null)
				vo.setAmount(item.getAmount().toString());
			vo.setCountry(item.getCountry());
			vo.setCategory(item.getCategory().getCategory());
			vo.setCategoryId(item.getCategory().getId());
			vo.setReceipt(item.getReceipt());
			vo.setItem(item.getItem());
			if (item.getDate() != null)
				vo.setDate(DateFormatter.convertDateToString(item.getDate()));
			vos.add(vo);
		}
		return vos;
	}

	/**
	 * method to list reimbursements by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ReimbursementsVo> listReimbursementsByEmployee(Long employeeId) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<ReimbursementsVo> vos = new ArrayList<ReimbursementsVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reimbursements.class);
		Criterion byEmployee = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression logicalExpression = Restrictions.or(byEmployee, createdBy);
		criteria.add(logicalExpression);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Reimbursements> reimbursements = criteria.list();
		for (Reimbursements reimbursement : reimbursements) {
			vos.add(createReimbursementsVo(reimbursement));
		}
		return vos;
	}

	/**
	 * method to update status
	 */
	public void updateStatus(ReimbursementsVo vo) {
		Reimbursements reimbursements = getReimbursements(vo.getReimbursementId());
		if (reimbursements == null)
			throw new ItemNotFoundException("Reimbursements Not Exist.");
		ForwardApplicationStatus status = statusDao.getStatus(vo.getStatus());
		reimbursements.setStatus(status);
		if (vo.getSuperiorCodes() != null)
			reimbursements.setForwardApplicationTo(setSuperiorsByCodes(vo.getSuperiorCodes()));
		else
			reimbursements.getForwardApplicationTo().clear();
		reimbursements.setStatusDescription(vo.getStatusDescription());
		TempNotificationVo notificationVo = new TempNotificationVo();
		notificationVo.getEmployeeIds().add(reimbursements.getEmployee().getId());
		notificationVo.setSubject(reimbursements.getEmployee().getEmployeeCode() + "'s Reimbursement Request "
				+ vo.getStatus());
		notificationDao.saveNotification(notificationVo);
		this.sessionFactory.getCurrentSession().merge(reimbursements);
	}

	/**
	 * method to get reimbursements by id
	 */
	public ReimbursementsVo getReimbursementsById(Long reimbursementsId) {
		Reimbursements reimbursements = getReimbursements(reimbursementsId);
		if (reimbursements == null)
			throw new ItemNotFoundException("Reimbursements Not Exist.");
		return createReimbursementsVo(reimbursements);
	}

	/**
	 * method to get reimbursements by employee , startDate & endDate
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ReimbursementsVo> listReimbursementsByEmployeeStartDateEndDate(Long employeeId, Date startDate,
			Date endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<ReimbursementsVo> vos = new ArrayList<ReimbursementsVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reimbursements.class)
				.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate))
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Reimbursements> reimbursements = criteria.list();
		for (Reimbursements reimbursement : reimbursements) {
			vos.add(createReimbursementsVo(reimbursement));
		}
		return vos;
	}

	/**
	 * method to get reimbursements by startDate & endDate
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ReimbursementsVo> listReimbursementsByStartDateEndDate(Date startDate, Date endDate) {
		List<ReimbursementsVo> vos = new ArrayList<ReimbursementsVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reimbursements.class)
				.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Reimbursements> reimbursements = criteria.list();
		for (Reimbursements reimbursement : reimbursements) {
			vos.add(createReimbursementsVo(reimbursement));
		}
		return vos;
	}

	private Set<Employee> setSuperiorsByCodes(List<String> superiorCodes) {
		Set<Employee> superiors = new HashSet<Employee>();
		for (String coded : superiorCodes) {
			Employee superior = employeeDao.findAndReturnEmployeeByCode(coded);
			if (superior != null)
				superiors.add(superior);
		}
		return superiors;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ReimbursementsVo> listReimbursementsByEmployeeStartDateEndDate(Employee employee, Date startDate,
			Date endDate) {
		List<ReimbursementsVo> vos = new ArrayList<ReimbursementsVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reimbursements.class)
				.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate))
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Reimbursements> reimbursements = criteria.list();
		for (Reimbursements reimbursement : reimbursements) {
			vos.add(createReimbursementsVo(reimbursement));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ReimbursementsVo> findAllReimbursementsByEmployee(String employeeCode) {
		List<ReimbursementsVo> vos = new ArrayList<ReimbursementsVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reimbursements.class);
		criteria.createAlias("forwardApplicationTo", "employee");
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("employee", employee);
		Criterion receiver = Restrictions.eq("employee.employeeCode", employeeCode);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Reimbursements> reimbursements = criteria.list();
		for (Reimbursements reimbursement : reimbursements) {
			vos.add(createReimbursementsVo(reimbursement));
		}
		return vos;
	}
}
