package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BaseCurrencyDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.BranchTypeDao;
import com.hrms.web.dao.CountryDao;
import com.hrms.web.dao.TimeZoneDao;
import com.hrms.web.entities.BranchAddress;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.BranchDocuments;
import com.hrms.web.entities.BranchType;
import com.hrms.web.entities.CandidateDocuments;
import com.hrms.web.entities.GeographicalLocation;
import com.hrms.web.entities.JobCandidates;
import com.hrms.web.entities.Project;
import com.hrms.web.entities.ProjectDocuments;
import com.hrms.web.logger.Log;
import com.hrms.web.service.OrganisationService;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.BranchDocumentVo;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.CandidateDocumentsVo;
import com.hrms.web.vo.ProjectDocumentsVo;
/**
 * 
 * @author Vinutha
 * @since 10-March-2015
 *
 */
@Repository
public class BranchDaoImpl extends AbstractDao implements BranchDao{

	@Log
	private static Logger logger;

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private BranchTypeDao branchTypeDao;
	
	@Autowired
	private CountryDao countryDao;
	
	@Autowired
	private BaseCurrencyDao baseCurrencyDao;
	
	@Autowired
	private TimeZoneDao timeZoneDao;
	
	/**
	 * method to save new Branch
	 * @param branchVo
	 */
	public Long saveBranch(BranchVo branchVo) {
		logger.info("Inside BranchDAO >>>>>>>>>> save branch");
		Branch branch = new Branch();
		branch.setBranchName(branchVo.getBranchName());
		if(branchVo.getStartDate()!=null)
			branch.setStartDate(DateFormatter.convertStringToDate(branchVo.getStartDate()));
		branch.setBranchType(branchTypeDao.findAndReturnBranchTypeByName(branchVo.getBranchType()));
		branch.setParentBranch(findAndReturnBranchObjectById(branchVo.getParentBranchId()));
		branch.setAdditionalInformation(branchVo.getNotes());
		//branch.setCurrency();
		branch.setCurrency(baseCurrencyDao.findBaseCurrency(branchVo.getCurrency()));
		branch.setTimeZone(timeZoneDao.findTimeZone(branchVo.getTimeZoneLocation()));
		branch.setAddressField(createAddress(branchVo,branch));
		branch.setGeographicalLocation(createGeographicalLocation(branchVo,branch));
		branch.setBranchDocuments(createAndReturnSetOfBranchDocuments(branchVo,branch));
		Long id = (Long)this.sessionFactory.getCurrentSession().save(branch);
		userActivitiesService.saveUserActivity("Added new Branch "+branchVo.getBranchName());
		return id;
		
		
	}
	/**
	 * method to delete branch
	 * @param branchVo
	 */
	public void deleteBranch(BranchVo branchVo) {
		
		logger.info("Inside BranchDaoImpl>>>>>>deleteBranch");
		String name=branchVo.getBranchName();
		this.sessionFactory.getCurrentSession().delete
		(findAndReturnBranchObjectById(branchVo.getBranchId()));
		userActivitiesService.saveUserActivity("Deleted Announcement "+name);
	}
	/**
	 * method to update existing branch
	 * @param branchVo
	 * 
	 */
	public void updateBranch(BranchVo branchVo) {
		logger.info("Inside BranchDaoImpl>>>>>>updateBranch");
		Branch branch = findAndReturnBranchObjectById(branchVo.getBranchId());
		branch.setBranchName(branchVo.getBranchName());
		if(branchVo.getStartDate()!=null)
			branch.setStartDate(DateFormatter.convertStringToDate(branchVo.getStartDate()));
		branch.setBranchType(branchTypeDao.findAndReturnBranchTypeByName(branchVo.getBranchType()));
		branch.setParentBranch(findAndReturnBranchObjectById(branchVo.getParentBranchId()));
		branch.setAdditionalInformation(branchVo.getNotes());
		branch.setCurrency(baseCurrencyDao.findBaseCurrency(branchVo.getCurrency()));
		branch.setAddressField(createAddress(branchVo,branch));
		branch.setGeographicalLocation(createGeographicalLocation(branchVo,branch));
		branch.setBranchDocuments(createAndReturnSetOfBranchDocuments(branchVo,branch));
		branch.setAdditionalInformation(branchVo.getNotes());
		this.sessionFactory.getCurrentSession().merge(branch);
		userActivitiesService.saveUserActivity("Updated Branch "+branchVo.getBranchName());
	}
	/**
	 * method to findAllBranches
	 * @param 
	 * @return List<BranchVo>
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<BranchVo> findAllBranches() {
		logger.info("Inside BranchDaoImpl>>>>>>findAllBranches");
		List<BranchVo> branchVos=new ArrayList<BranchVo>();
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Branch.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Branch> branches=criteria.list();
		for(Branch branch:branches){
			branchVos.add(createBranchNameIdVo(branch));
		}
		return branchVos;
	}
	/**
	 * Method to find all branches other than one with specified id
	 * @param id
	 * @return
	 */
	public List<BranchVo> findRemainingBranches(Long id)
	{
		logger.info("Inside BranchDaoImpl>>>>>>findRemainingBranches");
		List<BranchVo> branchVos=new ArrayList<BranchVo>();
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Branch.class).add(Restrictions.ne("id",id));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Branch> branches=criteria.list();
		for(Branch branch:branches){
			branchVos.add(createBranchNameIdVo(branch));
		}
		return branchVos;
	}
	/**
	 * method to find branchById
	 * @param long id
	 * @return branchVo
	 */
	public BranchVo findBranchById(Long id) {
		logger.info("Inside BranchDaoImpl>>>>>>findBranchById");
		Branch branch=(Branch)this.sessionFactory.getCurrentSession().createCriteria(Branch.class)
			.add(Restrictions.eq("id", id)).uniqueResult();
		BranchVo branchVo = createBranchVo(branch);
		return branchVo;
	}
	/**
	 * method to find branch by name
	 * @param String branchName
	 * @return branchVo
	 */
	public BranchVo findBranchByName(String branchName) {
		logger.info("Inside BranchDaoImpl>>>>>>findBranchByName");
		Branch branch=(Branch)this.sessionFactory.getCurrentSession().createCriteria(Branch.class)
				.add(Restrictions.eq("branchName",branchName)).uniqueResult();
		BranchVo branchVo = createBranchVo(branch);
		return branchVo;
	}
	/**
	 * method to find branch by branch type
	 * @param branchType
	 * @return List<BranchVo>
	 */
	@SuppressWarnings("unchecked")
	public List<BranchVo> findBranchesByBranchType(BranchType branchType) {
		logger.info("Inside BranchDaoImpl>>>>>>findBranchesByBranchType");
		List<BranchVo> branchVos=new ArrayList<BranchVo>();
		List<Branch> branches=this.sessionFactory.getCurrentSession().createCriteria(Branch.class)
				.add(Restrictions.eq("branchType", branchType)).list();
		for(Branch branch:branches){
			branchVos.add(createBranchVo(branch));
		}
		return branchVos;
	}
	/**
	 * method to find branches by parent station
	 * @param parentBranch
	 * @return List<BranchVo>
	 */
	@SuppressWarnings("unchecked")
	public List<BranchVo> findBranchesByParentStation(String parentBranch) {
		logger.info("Inside BranchDaoImpl>>>>>>findBranchesByParentStation");
		List<BranchVo> branchVos= new ArrayList<BranchVo>();
		List<Branch>branches=this.sessionFactory.getCurrentSession().createCriteria(Branch.class)
				.add(Restrictions.eq("parentStation", parentBranch)).list();
		for(Branch branch:branches){
			branchVos.add(createBranchVo(branch));
		}
		return branchVos;
	}
	/**
	 * method to find branches based on creation date 
	 * @param date
	 * @return List<BranchVo>
	 */
	public List<BranchVo> findBranchesBasedOnCreationDate(String start , String end) {
		logger.info("Inside BranchDaoImpl>>>>>>findBranchesBasedOnCreationDate");
		Date startDate = DateFormatter.convertStringToDate(start);
		Date endDate = DateFormatter.convertStringToDate(end);
		List<BranchVo> branchVos = new ArrayList<BranchVo>();
		@SuppressWarnings("unchecked")
		List<Branch> branches=this.sessionFactory.getCurrentSession().createCriteria(Branch.class)
				.add(Restrictions.between("startDate", startDate, endDate)).list();
		for(Branch branch:branches){
			branchVos.add(createBranchVo(branch));
		}
		return branchVos;
	}
	/**
	 * method to return branch object
	 * @param branchId
	 * @return Branch
	 */
	public Branch findAndReturnBranchObjectById(Long branchId) {
		logger.info("Inside BranchDaoImpl>>>>>>findAndReturnBranchObjectById");
		return (Branch) this.sessionFactory.getCurrentSession().createCriteria(Branch.class)
				.add(Restrictions.eq("id", branchId)).uniqueResult();
		
	}
	/**
	 * method to find and return branch object by name
	 * @param branvh name
	 * @return Branch
	 */
	public Branch findAndReturnBranchObjectByName(String branchName) {
		logger.info("Inside findAndReturnBranchObjectById");
		return (Branch) this.sessionFactory.getCurrentSession().createCriteria(Branch.class)
				.add(Restrictions.eq("branchName", branchName)).uniqueResult();
		
	}
	/**
	 * method to create geographicalLocation 
	 * @param branchVo
	 * @return
	 */
	private GeographicalLocation createGeographicalLocation(BranchVo branchVo,Branch branch)
	{
		logger.info("Inside BranchDaoImpl>>>>>>createGeographicalLocation");
		GeographicalLocation geographicalLocation=null;
		if(branchVo.getBranchId()!=null){
			geographicalLocation=branch.getGeographicalLocation();
		}
		else
		{
			geographicalLocation = new GeographicalLocation();
		}
		geographicalLocation.setLatitude(branchVo.getLatitude());
		geographicalLocation.setLongitude(branchVo.getLongitude());
		geographicalLocation.setBranch(branch);
		return geographicalLocation;
	}
	/**
	 * method to create address
	 * @param branchVo
	 * @return
	 */
	private BranchAddress createAddress(BranchVo branchVo,Branch branch)
	{
		logger.info("Inside BranchDaoImpl>>>>>>createAddress");
		BranchAddress address= null;
		if(branchVo.getBranchId()!=null){
			address=branch.getAddressField();
		}else{
			address=new BranchAddress();
		}
		address.setAddress(branchVo.getAddress());
		address.setCity(branchVo.getCity());
		address.setState(branchVo.getState());
		address.setZipCode(branchVo.getZipCode());
		address.setCountry(countryDao.findCountry(branchVo.getCountry()));
		address.setPhoneNumber(branchVo.getPhoneNumber());
		address.setFaxNumber(branchVo.getFaxNumber());
		address.seteMail(branchVo.geteMail());
		address.setWebSite(branchVo.getWebSite());
		address.setBranch(branch);
		
		return address;
	}
	/**
	 * method to create branchVo from branch to return name and id only
	 * @param branch
	 * @return
	 */
	private BranchVo createBranchNameIdVo(Branch branch)
	{
		BranchVo branchVo = new BranchVo();
		branchVo.setBranchName(branch.getBranchName());
		branchVo.setBranchId(branch.getId());
		if(branch.getBranchType()!=null)
				branchVo.setBranchType(branch.getBranchType().getType());
		if(branch.getAddressField()!=null)
				branchVo.setCity(branch.getAddressField().getCity());
		return branchVo;
	}
	
	/**
	 * method to create branchVo from branch object
	 * @param branch
	 * @return
	 */
	private BranchVo createBranchVo(Branch branch)
	{
		logger.info("Inside BranchDaoImpl>>>>>>createBranchVo");
		BranchVo branchVo = new BranchVo();
		branchVo.setBranchId(branch.getId());
		branchVo.setCreatedBy(branch.getCreatedBy().getUserName());
		if(branch.getStartDate()!=null)
			branchVo.setStartDate(DateFormatter.convertDateToString(branch.getStartDate()));
		if(branch.getCreatedOn()!=null)
			branchVo.setCreatedOn(DateFormatter.convertDateToString(branch.getCreatedOn()));
		branchVo.setBranchName(branch.getBranchName());
		if(branch.getParentBranch()!=null)
		{
			branchVo.setParentBranchId(branch.getParentBranch().getId());
			branchVo.setParentBranch(branch.getParentBranch().getBranchName());
		}
		branchVo.setBranchType(branch.getBranchType().getType());
		branchVo.setNotes(branch.getAdditionalInformation());
		if(branch.getTimeZone()!=null)
		{
			branchVo.setTimeZoneOffset(branch.getTimeZone().getOffSet());
			branchVo.setTimeZoneLocation(branch.getTimeZone().getLocation());
			branchVo.setTimeZoneComplete(branch.getTimeZone().getOffSet()+" "+branch.getTimeZone().getLocation());
		}
		if(branch.getCurrency()!=null)
			branchVo.setCurrency(branch.getCurrency().getBaseCurrency());
		BranchAddress address=branch.getAddressField();
		if(address!=null)
		{
			branchVo.setAddress(address.getAddress());
			branchVo.setCity(address.getCity());
			branchVo.setState(address.getState());
			branchVo.setZipCode(address.getZipCode());
			branchVo.setCountry(address.getCountry().getName());
			branchVo.setPhoneNumber(address.getPhoneNumber());
			branchVo.setFaxNumber(address.getFaxNumber());
			branchVo.seteMail(address.geteMail());
			branchVo.setWebSite(address.getWebSite());
		}
		if(branch.getGeographicalLocation()!=null)
		{
			branchVo.setLatitude(branch.getGeographicalLocation().getLatitude());
			branchVo.setLongitude(branch.getGeographicalLocation().getLongitude());
		}
		if(branch.getBranchDocuments()!=null)
		{
			Set<BranchDocuments> documents= branch.getBranchDocuments();
			Set<BranchDocumentVo> documentVos =new HashSet<BranchDocumentVo>();
			for(BranchDocuments document : documents)
			{
				documentVos.add(createDocumentsVo(document));
			}
			branchVo.setBranchDocuments(documentVos);
			
		}
		
		return branchVo;
	}
	/**
	 * method to create document vo
	 * @param documents
	 * @return
	 */
	private BranchDocumentVo createDocumentsVo(BranchDocuments documents){
		BranchDocumentVo vo = new BranchDocumentVo();
		vo.setDocumentId(documents.getId());
		vo.setDocumentUrl(documents.getDocumentPath());
		vo.setName(documents.getName());
		return vo;
	}

	/**
	 * method to delete branch by id
	 * @param branchId
	 * 
	 */
	public void deleteBranchById(Long branchId) {
		logger.info("Inside deleteBranch");
		this.sessionFactory.getCurrentSession().delete
		(findAndReturnBranchObjectById(branchId));
		
	}
	/**
	 * method to create and return set of branch documents
	 * @param branchVo
	 * 
	 */
	private Set<BranchDocuments> createAndReturnSetOfBranchDocuments(BranchVo branchVo, Branch branch)
	{
		logger.info("Inside BranchDaoImpl>>>>>>createAndReturnSetOfBranchDocuments");
		Set<BranchDocuments> branchDocuments = null;
		if(branchVo.getBranchId()!=null)
		{
			branchDocuments=branch.getBranchDocuments();
		}
		else
		{
			branchDocuments = new HashSet<BranchDocuments>();
		}
		for(BranchDocuments branchDocuments2 : branchDocuments)
		{
			branchDocuments2 = new BranchDocuments();
			branchDocuments2.setBranch(branch);
			branchDocuments2.setDocumentPath(branchVo.getDocumentsPath());
			branchDocuments.add(branchDocuments2);
		}
		return branchDocuments;
	}
	/**
	 * method to update branch document
	 * @param branchVo
	 * @return BranchVo
	 */
	public BranchVo updateBranchDocument(BranchVo branchVo) {
		Branch branch=this.findAndReturnBranchObjectById(branchVo.getBranchId());
		branch.setBranchDocuments(setDocuments(branchVo.getBranchDocuments(), branch));
		this.sessionFactory.getCurrentSession().merge(branch);
		return createBranchVo(branch);
	}

	/**
	 * method to update documents
	 * @param documentsVos
	 * @param project
	 * @return
	 */
	private Set<BranchDocuments> setDocuments(Set<BranchDocumentVo> documentsVos , Branch branch){
		Set<BranchDocuments> documents = new HashSet<BranchDocuments>();
		for(BranchDocumentVo vo : documentsVos){
			BranchDocuments document = new BranchDocuments();
			document.setBranch(branch);
			document.setName(vo.getName());
			document.setDocumentPath(vo.getDocumentUrl());
			documents.add(document);
		}
		return documents;
	}
	/**
	 * method to delete branch document
	 * @param path
	 */
	public void deleteBranchDocument(String path) {
		BranchDocuments branchDocument = (BranchDocuments) this.sessionFactory.getCurrentSession().createCriteria(BranchDocuments.class)
				.add(Restrictions.eq("documentPath", path)).uniqueResult();
		
		Branch branch= branchDocument.getBranch();
		branch.getBranchDocuments().remove(branchDocument);
		this.sessionFactory.getCurrentSession().delete(branchDocument);
		this.sessionFactory.getCurrentSession().merge(branch);
		
	}
	/**
	 * Method to get number of branches
	 * @return int
	 */
	public Long getbranchCount() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Branch.class);
		criteria.setProjection(Projections.rowCount());
		return (Long) (criteria.uniqueResult());
		
	}
	public List<Branch> findBranchesById(List<Long> ids) {
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Branch.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.in("id", ids));
		List<Branch> branches=criteria.list();
		return branches;
	}
	public List<BranchVo> findBranchesByIds(List<Long> ids) {
		List<Branch> branchs = this.findBranchesById(ids);
		List<BranchVo> branchVos = new ArrayList<BranchVo>();
		for(Branch branch: branchs)
			branchVos.add(createBranchVo(branch));
		return branchVos;
	}
	
	

}
