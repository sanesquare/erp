package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.TrainingTypeDao;
import com.hrms.web.entities.TrainingType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.TrainingTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Repository
public class TrainingTypeDaoImpl extends AbstractDao implements TrainingTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveTrainingType(TrainingTypeVo trainingTypeVo) {
		for (String trainingType : StringSplitter.splitWithComma(trainingTypeVo.getTrainingType())) {
			if (findTrainingType(trainingType.trim()) != null)
				throw new DuplicateItemException("Duplicate Training Type : " + trainingType.trim());
			TrainingType type = new TrainingType();
			type.setType(trainingType.trim());
			this.sessionFactory.getCurrentSession().save(type);
		}
	}

	public void updateTrainingType(TrainingTypeVo trainingTypeVo) {
		TrainingType trainingType = findTrainingType(trainingTypeVo.getTrainingTypeId());
		if (trainingType == null)
			throw new ItemNotFoundException("Training Type Not Found.");
		trainingType.setType(trainingTypeVo.getTrainingType());
		this.sessionFactory.getCurrentSession().merge(trainingType);
	}

	public void deleteTrainingType(Long trainingTypeId) {
		TrainingType trainingType = findTrainingType(trainingTypeId);
		if (trainingType == null)
			throw new ItemNotFoundException("Training Type Not Found.");
		this.sessionFactory.getCurrentSession().delete(trainingType);
	}

	public TrainingType findTrainingType(Long trainingTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TrainingType.class)
				.add(Restrictions.eq("id", trainingTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (TrainingType) criteria.uniqueResult();
	}

	public TrainingType findTrainingType(String trainingType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TrainingType.class)
				.add(Restrictions.eq("type", trainingType));
		if (criteria.uniqueResult() == null)
			return null;
		return (TrainingType) criteria.uniqueResult();
	}

	/**
	 * method to create trainingtypevo object
	 * 
	 * @param trainingType
	 * @return trainingtypevo
	 */
	private TrainingTypeVo createTrainingTypeVo(TrainingType trainingType) {
		TrainingTypeVo trainingTypeVo = new TrainingTypeVo();
		trainingTypeVo.setTrainingType(trainingType.getType());
		trainingTypeVo.setTrainingTypeId(trainingType.getId());
		return trainingTypeVo;
	}

	public TrainingTypeVo findTrainingTypeById(Long trainingTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TrainingType.class)
				.add(Restrictions.eq("id", trainingTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createTrainingTypeVo((TrainingType) criteria.uniqueResult());
	}

	public TrainingTypeVo findTrainingTypeByType(String trainingType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TrainingType.class)
				.add(Restrictions.eq("type", trainingType));
		if (criteria.uniqueResult() == null)
			return null;
		return createTrainingTypeVo((TrainingType) criteria.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<TrainingTypeVo> findAllTrainingType() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TrainingType.class);
		List<TrainingTypeVo> trainingTypeVos = new ArrayList<TrainingTypeVo>();
		List<TrainingType> trainingTypes = criteria.list();
		for (TrainingType trainingType : trainingTypes)
			trainingTypeVos.add(createTrainingTypeVo(trainingType));
		return trainingTypeVos;
	}

}
