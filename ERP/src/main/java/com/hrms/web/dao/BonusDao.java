package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Bonuses;
import com.hrms.web.entities.Employee;
import com.hrms.web.vo.BonusVo;

/**
 * @author Shamsheer
 * @since 11-April-2015
 */
public interface BonusDao {

	/**
	 * method to save bonus
	 * @param bonusVo
	 * @return
	 */
	public Long saveBonus(BonusVo bonusVo);
	
	/**
	 * method to update bonus
	 * @param bonusVo
	 */
	public void updateBonus(BonusVo bonusVo);
	
	/**
	 * method to delete bonus
	 * @param bonusId
	 */
	public void deleteBonus(Long bonusId);
	
	/**
	 * method to get Bonus Object
	 * @param bonusId
	 * @return
	 */
	public Bonuses getBonus(Long bonusId);
	
	/**
	 * method to list all bonuses
	 * @return
	 */
	public List<BonusVo> listAllBonuses();
	
	/**
	 * method to list bonuses by employee
	 * @param employeeId
	 * @return
	 */
	public List<BonusVo> listBonusesByEmployee(Long employeeId);
	
	/**
	 * method to list bonuses by employee
	 * @param employee
	 * @return
	 */
	public List<BonusVo> listBonusesByEmployee(Employee employee);
	
	/**
	 * method to list bonuses by title
	 * @param bonusTitleId
	 * @return
	 */
	public List<BonusVo> listBonusesByTitle(Long bonusTitleId);
	
	/** 
	 * method to get bonus details
	 * @param bonusId
	 * @return
	 */
	public BonusVo getBonusById(Long bonusId);
	
	/**
	 * method to get bonus by employee and bonus title
	 * @param employeeId
	 * @param BonusTitleId
	 * @return
	 */
	public BonusVo getBonusByEmployeeAndTitle(String employeeCode, Long BonusTitleId);
	
	/**
	 * method to list bonus by employee , start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<BonusVo> listBonusesByEmployeeStartDateEndDate(Long employeeId , Date startDate , Date endDate);
	
	/**
	 * method to list bonus by employee , start date & end date
	 * @param employee
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<BonusVo> listBonusesByEmployeeStartDateEndDate(Employee employee , Date startDate , Date endDate);
	
	/**
	 * method to list bonus bystart date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<BonusVo> listBonusesByStartDateEndDate(Date startDate , Date endDate);
}
