package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.EmployeeExitDao;
import com.hrms.web.dao.EmployeeExitTypeDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.EmployeeExit;
import com.hrms.web.entities.EmployeeExitDocuments;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.EmployeeExitDocumentsVo;
import com.hrms.web.vo.EmployeeExitVo;

/**
 * 
 * @author Jithin Mohan
 * @since 18-March-2015
 *
 */
@Repository
public class EmployeeExitDaoImpl extends AbstractDao implements EmployeeExitDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;
	@Autowired
	private EmployeeExitTypeDao employeeExitTypeDao;

	public Long saveEmployeeExit(EmployeeExitVo employeeExitVo) {
		if (employeeExitVo.getEmployeeExitId() == null
				&& findEmployeeExitByEmployee(employeeExitVo.getEmployeeCode()) != null) {
			throw new DuplicateItemException("Employee already exit");
		} else {
			EmployeeExit employeeExit = findEmployeeExit(employeeExitVo.getEmployeeExitId());
			if (employeeExit == null)
				employeeExit = new EmployeeExit();
			employeeExit.setEmployee(employeeDao.findAndReturnEmployeeByCode(employeeExitVo.getEmployeeCode()));
			employeeExit.setExitDate(DateFormatter.convertStringToDate(employeeExitVo.getExitDate()));
			employeeExit.setExitType(employeeExitTypeDao.findEmployeeExitType(employeeExitVo.getExitType()));
			employeeExit.setExitInterview(employeeExitVo.getExitInterview());
			employeeExit.setReason(employeeExitVo.getExitReason());
			employeeExit.setAdditionalInformation(employeeExitVo.getExitNotes());
			this.sessionFactory.getCurrentSession().saveOrUpdate(employeeExit);
			userActivitiesService.saveUserActivity("Added new Employee Exit "+employeeExitVo.getEmployeeCode());
			return employeeExit.getId();
		}
	}

	public void saveEmployeeExitDocuments(EmployeeExitVo employeeExitVo) {
		EmployeeExit employeeExit = findEmployeeExit(employeeExitVo.getEmployeeExitId());
		if (employeeExit == null)
			throw new ItemNotFoundException("Employee Exit Not Exist.");
		employeeExit.setEmployeeExitDocuments(setEmployeeExitDocuments(employeeExitVo.getEmployeeExitDocuments(),
				employeeExit));
		this.sessionFactory.getCurrentSession().merge(employeeExit);
	}

	public void updateEmployeeExit(EmployeeExitVo employeeExitVo) {
		EmployeeExit employeeExit = findEmployeeExit(employeeExitVo.getEmployeeExitId());
		if (employeeExit == null)
			throw new ItemNotFoundException("Employee Exit Not Exist.");
		employeeExit.setEmployee(employeeDao.findAndReturnEmployeeByCode(employeeExitVo.getEmployeeCode()));
		employeeExit.setExitDate(DateFormatter.convertStringToDate(employeeExitVo.getExitDate()));
		employeeExit.setExitType(employeeExitTypeDao.findEmployeeExitType(employeeExitVo.getExitType()));
		employeeExit.setExitInterview(employeeExitVo.getExitInterview());
		employeeExit.setReason(employeeExitVo.getExitReason());
		employeeExit.setAdditionalInformation(employeeExitVo.getExitNotes());
		this.sessionFactory.getCurrentSession().merge(employeeExit);
		userActivitiesService.saveUserActivity("Updated Employee Exit "+employeeExitVo.getEmployeeCode());
	}

	/**
	 * method to set employeeExit documents to employeeExit object
	 * 
	 * @param documentsVos
	 * @param employeeExit
	 * @return documents
	 */
	private Set<EmployeeExitDocuments> setEmployeeExitDocuments(List<EmployeeExitDocumentsVo> documentsVos,
			EmployeeExit employeeExit) {
		Set<EmployeeExitDocuments> documents = new HashSet<EmployeeExitDocuments>();
		for (EmployeeExitDocumentsVo documentsVo : documentsVos) {
			EmployeeExitDocuments exitDocuments = new EmployeeExitDocuments();
			exitDocuments.setName(documentsVo.getDocumentName());
			exitDocuments.setDocumentPath(documentsVo.getDocumentUrl());
			exitDocuments.setEmployeeExit(employeeExit);
			documents.add(exitDocuments);
		}
		return documents;
	}

	public void deleteEmployeeExit(Long employeeExitId) {
		EmployeeExit employeeExit = findEmployeeExit(employeeExitId);
		if (employeeExit == null)
			throw new ItemNotFoundException("Employee Exit Not Exist.");
		this.sessionFactory.getCurrentSession().delete(employeeExit);
		userActivitiesService.saveUserActivity("Deleted Employee Exit ");
	}

	public EmployeeExit findEmployeeExit(Long employeeExitId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExit.class)
				.add(Restrictions.eq("id", employeeExitId));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeExit) criteria.uniqueResult();
	}

	public EmployeeExit findEmployeeExit(Date exitDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExit.class)
				.add(Restrictions.eq("exitDate", exitDate));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeExit) criteria.uniqueResult();
	}

	public EmployeeExit findEmployeeExit(Date startDate, Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExit.class)
				.add(Restrictions.ge("exitDate", startDate)).add(Restrictions.le("exitDate", endDate));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeExit) criteria.uniqueResult();
	}

	/**
	 * method to create employeeExitVo object
	 * 
	 * @param employeeExit
	 * @return employeeExitVo
	 */
	private EmployeeExitVo createEmployeeExitVo(EmployeeExit employeeExit) {
		EmployeeExitVo employeeExitVo = new EmployeeExitVo();
		employeeExitVo.setEmployee(employeeExit.getEmployee().getUserProfile().getFirstName() + " "
				+ employeeExit.getEmployee().getUserProfile().getLastname());
		employeeExitVo.setEmployeeCode(employeeExit.getEmployee().getEmployeeCode());
		employeeExitVo.setEmployeeExitId(employeeExit.getId());
		employeeExitVo.setExitStringDate(DateFormatter.convertDateToDetailedString(employeeExit.getExitDate()));
		employeeExitVo.setExitDate(DateFormatter.convertDateToString(employeeExit.getExitDate()));
		employeeExitVo.setExitType(employeeExit.getExitType().getExitType());
		employeeExitVo.setExitInterview(employeeExit.getExitInterview());
		employeeExitVo.setExitNotes(employeeExit.getAdditionalInformation());
		employeeExitVo.setExitReason(employeeExit.getReason());
		employeeExitVo.setRecordedBy(employeeExit.getCreatedBy().getUserName());
		employeeExitVo.setRecordedOn(DateFormatter.convertDateToDetailedString(employeeExit.getCreatedOn()));
		employeeExitVo.setEmployeeExitDocuments(createEmployeeExitDocumentsVo(employeeExit));
		return employeeExitVo;
	}

	/**
	 * method to get all the documents of a given employeeExit record
	 * 
	 * @param employeeExit
	 * @return documentsVos
	 */
	private List<EmployeeExitDocumentsVo> createEmployeeExitDocumentsVo(EmployeeExit employeeExit) {
		List<EmployeeExitDocumentsVo> documentsVos = new ArrayList<EmployeeExitDocumentsVo>();
		Iterator<EmployeeExitDocuments> iterator = employeeExit.getEmployeeExitDocuments().iterator();
		while (iterator.hasNext()) {
			EmployeeExitDocumentsVo documentsVo = new EmployeeExitDocumentsVo();
			EmployeeExitDocuments employeeExitDocuments = (EmployeeExitDocuments) iterator.next();
			documentsVo.setDocumentName(employeeExitDocuments.getName());
			documentsVo.setDocumentUrl(employeeExitDocuments.getDocumentPath());
			documentsVo.setEmp_ext_doc_id(employeeExitDocuments.getId());
			documentsVos.add(documentsVo);
		}
		return documentsVos;
	}

	public EmployeeExitVo findEmployeeExitById(Long employeeExitId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExit.class)
				.add(Restrictions.eq("id", employeeExitId));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeExitVo((EmployeeExit) criteria.uniqueResult());
	}

	public EmployeeExitVo findEmployeeExitByDate(Date exitDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExit.class)
				.add(Restrictions.eq("exitDate", exitDate));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeExitVo((EmployeeExit) criteria.uniqueResult());
	}

	public EmployeeExitVo findEmployeeExitBetweenDates(Date startDate, Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExit.class)
				.add(Restrictions.ge("exitDate", startDate)).add(Restrictions.le("exitDate", endDate));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeExitVo((EmployeeExit) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeExitVo> findAllEmployeeExit() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExit.class);
		List<EmployeeExitVo> employeeExitVos = new ArrayList<EmployeeExitVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeExit> employeeExits = criteria.list();
		for (EmployeeExit employeeExit : employeeExits)
			employeeExitVos.add(createEmployeeExitVo(employeeExit));
		return employeeExitVos;
	}

	public void deleteEmployeeExitDocument(Long employeeExitDocumentId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExitDocuments.class)
				.add(Restrictions.eq("id", employeeExitDocumentId));
		EmployeeExitDocuments document = (EmployeeExitDocuments) criteria.uniqueResult();

		EmployeeExit employeeExit = findEmployeeExit(document.getEmployeeExit().getId());
		employeeExit.getEmployeeExitDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);

		this.sessionFactory.getCurrentSession().merge(employeeExit);
	}

	public EmployeeExitVo findEmployeeExitByEmployee(String employeeCode) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExit.class)
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeByCode(employeeCode)));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeExitVo((EmployeeExit) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeExitVo> findAllEmployeeExitByEmployee(String employeeCode) {
		List<EmployeeExitVo> employeeExitVos = new ArrayList<EmployeeExitVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExit.class);
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("createdBy", employee.getUser());
		Criterion receiver = Restrictions.eq("employee", employee);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeExit> employeeExits = criteria.list();
		for (EmployeeExit employeeExit : employeeExits)
			employeeExitVos.add(createEmployeeExitVo(employeeExit));
		return employeeExitVos;
	}

}
