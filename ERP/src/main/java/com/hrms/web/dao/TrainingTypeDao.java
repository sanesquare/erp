package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.TrainingType;
import com.hrms.web.vo.TrainingTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public interface TrainingTypeDao {

	/**
	 * method to save TrainingType
	 * 
	 * @param trainingTypeVo
	 */
	public void saveTrainingType(TrainingTypeVo trainingTypeVo);

	/**
	 * method to update TrainingType
	 * 
	 * @param trainingTypeVo
	 */
	public void updateTrainingType(TrainingTypeVo trainingTypeVo);

	/**
	 * method to delete TrainingType
	 * 
	 * @param trainingTypeId
	 */
	public void deleteTrainingType(Long trainingTypeId);

	/**
	 * method to find TrainingType by id
	 * 
	 * @param trainingTypeId
	 * @return TrainingType
	 */
	public TrainingType findTrainingType(Long trainingTypeId);

	/**
	 * method to find TrainingType by type
	 * 
	 * @param trainingType
	 * @return TrainingType
	 */
	public TrainingType findTrainingType(String trainingType);

	/**
	 * method to find TrainingType by id
	 * 
	 * @param trainingTypeId
	 * @return TrainingTypeVo
	 */
	public TrainingTypeVo findTrainingTypeById(Long trainingTypeId);

	/**
	 * method to find TrainingType by type
	 * 
	 * @param trainingType
	 * @return TrainingTypeVo
	 */
	public TrainingTypeVo findTrainingTypeByType(String trainingType);

	/**
	 * method to find all TrainingType
	 * 
	 * @return List<TrainingTypeVo>
	 */
	public List<TrainingTypeVo> findAllTrainingType();
}
