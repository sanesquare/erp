package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Overtime;
import com.hrms.web.entities.OvertimeStatus;
import com.hrms.web.vo.OvertimeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public interface OvertimeDao {

	/**
	 * 
	 * @param advanceSalaryVo
	 */
	public void saveOrUpdateOvertime(OvertimeVo overtimeVo);

	/**
	 * 
	 * @param advanceSalaryVo
	 */
	public void updateOvertimeStatus(OvertimeVo overtimeVo);

	/**
	 * 
	 * @param advanceSalaryId
	 */
	public void deleteOvertime(Long overtimeId);

	/**
	 * 
	 * @param advanceSalaryId
	 * @return
	 */
	public Overtime findOvertime(Long overtimeId);

	/**
	 * 
	 * @param advanceSalaryId
	 * @return
	 */
	public OvertimeVo findOvertimeById(Long overtimeId);

	/**
	 * 
	 * @param employee
	 * @return
	 */
	public OvertimeVo findOvertimeByEmployee(Employee employee);

	/**
	 * 
	 * @param requestedDate
	 * @return
	 */
	public List<OvertimeVo> findOvertimeByDate(Date requestedDate);

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<OvertimeVo> findOvertimeBetweenDates(Date startDate, Date endDate);

	/**
	 * 
	 * @param status
	 * @return
	 */
	public List<OvertimeVo> findOvertimeByStatus(OvertimeStatus status);

	/**
	 * 
	 * @param timeIn
	 * @return
	 */
	public List<OvertimeVo> findOvertimeByTimeIn(Date timeIn);

	/**
	 * 
	 * @param timeOut
	 * @return
	 */
	public List<OvertimeVo> findOvertimeByTimeOut(Date timeOut);

	/**
	 * 
	 * @param timeIn
	 * @param timeOut
	 * @return
	 */
	public List<OvertimeVo> findOvertimeBetweenInAndOut(Date timeIn, Date timeOut);

	/**
	 * 
	 * @return
	 */
	public List<OvertimeVo> findAllOvertime();

	/**
	 * 
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<OvertimeVo> findOvertimeBetweenDates(Long employeeId, Date startDate, Date endDate);

	/**
	 * 
	 * @return
	 */
	public List<OvertimeVo> findAllOvertimeByEmployee(String employeeCode);

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @param status
	 * @return
	 */
	public List<OvertimeVo> findAllOvertimeBetweenDatesAndStatus(String employeeCode, String status, Date startDate,
			Date endDate);
}
