package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AnnouncementsDao;
import com.hrms.web.dao.AnnouncementsStatusDao;
import com.hrms.web.entities.Announcements;
import com.hrms.web.entities.AnnouncementsDocuments;
import com.hrms.web.entities.AnnouncementsStatus;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AnnouncementDocumentsVo;
import com.hrms.web.vo.AnnouncementsVo;

/**
 * 
 * @author Shimil Babu
 * @since 10-March-2015
 *
 */
@Repository
public class AnnouncementsDaoImpl extends AbstractDao implements AnnouncementsDao {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private AnnouncementsStatusDao announcementsStatusDao;

	/**
	 * method to save announcement
	 * 
	 * @param AnnouncementsVo
	 * @return
	 */
	public void saveOrUpdateAnnouncements(AnnouncementsVo announcementsVo) {
		Announcements announcements = findAndReturnAnnouncementsObjectById(announcementsVo.getAnnouncementsId());
		if (announcements == null)
			announcements = new Announcements();

		announcements.setTitle(announcementsVo.getAnnouncementsTitle());
		announcements.setStartDate(DateFormatter.convertStringToDate(announcementsVo.getStartDate()));
		announcements.setEndDate(DateFormatter.convertStringToDate(announcementsVo.getEndDate()));
		announcements.setMessage(announcementsVo.getMessage());
		announcements.setNotes(announcementsVo.getNotes());
		announcements.setSendEmail(announcementsVo.isSendMail());
		// if (announcementsVo.getAnnouncementsStatus() != null)
		announcements.setAnnouncementsStatus(announcementsStatusDao
				.findAnnouncementsStatusByAnnouncementsStatusName("Active"));
		announcements.setStatusNotes(announcementsVo.getAnnouncementsStatusNotes());

		this.sessionFactory.getCurrentSession().saveOrUpdate(announcements);
		userActivitiesService.saveUserActivity("Added new Announcement "+announcementsVo.getAnnouncementsTitle());
		announcementsVo.setAnnouncementsId(announcements.getId());
		announcementsVo.setCreatedBy(announcements.getCreatedBy().getUserName());
		announcementsVo.setCreatedOn(DateFormatter.convertDateToDetailedString(announcements.getCreatedOn()));
	}

	/**
	 * method to delete announcement
	 * 
	 * @param announcements
	 *            id
	 * @return
	 */
	public void deleteAnnouncements(Long id) {
		this.sessionFactory.getCurrentSession().delete(findAndReturnAnnouncementsObjectById(id));
		userActivitiesService.saveUserActivity("Deleted Announcement");
	}

	/**
	 * method to update announcement
	 * 
	 * @param AnnouncementsVo
	 * @return
	 */
	public void updateAnnouncements(AnnouncementsVo announcementsVo) {
		Announcements announcements = findAndReturnAnnouncementsObjectById(announcementsVo.getAnnouncementsId());
		announcements.setTitle(announcementsVo.getAnnouncementsTitle());
		announcements.setStartDate(DateFormatter.convertStringToDate(announcementsVo.getStartDate()));
		announcements.setEndDate(DateFormatter.convertStringToDate(announcementsVo.getEndDate()));
		announcements.setMessage(announcementsVo.getMessage());
		announcements.setNotes(announcementsVo.getNotes());
		announcements.setSendEmail(announcementsVo.isSendMail());
		announcements.setAnnouncementsStatus(announcementsStatusDao
				.findAnnouncementsStatusByAnnouncementsStatusNotes(announcementsVo.getAnnouncementsStatusNotes()));
		this.sessionFactory.getCurrentSession().merge(announcements);
		userActivitiesService.saveUserActivity("Updated Announcement "+announcementsVo.getAnnouncementsTitle());
	}

	/**
	 * method to find announcements by announcements id
	 * 
	 * @param announcement
	 *            id
	 * @return
	 */

	public AnnouncementsVo findAnnouncementsById(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Announcements.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return createAnnouncementsVo((Announcements) criteria.uniqueResult());

	}

	/**
	 * method to find announcements by announcements title
	 */
	public AnnouncementsVo findAnnouncementsByAnnouncementsTitle(String title) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Announcements.class)
				.add(Restrictions.eq("title", title));
		if (criteria.uniqueResult() == null)
			return null;
		return createAnnouncementsVo((Announcements) criteria.uniqueResult());
	}

	/**
	 * method to find announcements by date
	 */
	public AnnouncementsVo findAnnouncementsByDate(Date date) {
		return null;
	}

	/**
	 * method to find all announcements
	 * 
	 * @return announcementsVos
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<AnnouncementsVo> findAllAnnouncements() {
		List<AnnouncementsVo> announcementsVos = new ArrayList<AnnouncementsVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Announcements.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Announcements> announcements = criteria.list();
		for (Announcements announcementsObj : announcements) {
			announcementsVos.add(createAnnouncementsVo(announcementsObj));
		}
		return announcementsVos;
	}

	/**
	 * method to create announcementVo
	 * 
	 * @param announsment
	 * @return Announcement
	 */
	private AnnouncementsVo createAnnouncementsVo(Announcements announcements) {
		AnnouncementsVo announcementsVo = new AnnouncementsVo();
		announcementsVo.setAnnouncementsId(announcements.getId());
		announcementsVo.setAnnouncementsTitle(announcements.getTitle());
		announcementsVo.setStartDate(DateFormatter.convertDateToString(announcements.getStartDate()));
		announcementsVo.setEndDate(DateFormatter.convertDateToString(announcements.getEndDate()));
		announcementsVo.setMessage(announcements.getMessage());
		announcementsVo.setNotes(announcements.getNotes());
		announcementsVo.setSendMail(announcements.isSendEmail());
		if (announcements.getAnnouncementsStatus() != null) {
			announcementsVo.setAnnouncementsStatus(announcements.getAnnouncementsStatus().getStatus());
			announcementsVo.setAnnouncementsStatusId(announcements.getAnnouncementsStatus().getId());
			announcementsVo.setAnnouncementsStatusNotes(announcements.getAnnouncementsStatus().getNotes());
		}
		announcementsVo.setCreatedBy(announcements.getCreatedBy().getUserName());
		announcementsVo.setCreatedOn(DateFormatter.convertDateToDetailedString(announcements.getCreatedOn()));
		announcementsVo.setAnnouncementDocumentsVos(getDocuments(announcements.getAnnouncementsDocuments()));
		return announcementsVo;
	}

	/**
	 * method to get all documents
	 * 
	 * @param documents
	 * @return documentsVos
	 */
	private List<AnnouncementDocumentsVo> getDocuments(Set<AnnouncementsDocuments> documents) {
		List<AnnouncementDocumentsVo> documentsVos = new ArrayList<AnnouncementDocumentsVo>();
		Iterator<AnnouncementsDocuments> iterator = documents.iterator();
		while (iterator.hasNext()) {
			AnnouncementDocumentsVo documentsVo = new AnnouncementDocumentsVo();
			AnnouncementsDocuments announcementDocument = (AnnouncementsDocuments) iterator.next();
			documentsVo.setDocumentName(announcementDocument.getDocumentName());
			documentsVo.setDocumentUrl(announcementDocument.getDocumentUrl());
			documentsVo.setAnnr_doc_id(announcementDocument.getId());
			documentsVos.add(documentsVo);
		}
		return documentsVos;
	}

	/**
	 * method to find announcement object by announcement id
	 * 
	 * @param announsmentId
	 * @return Announcement
	 */
	public Announcements findAndReturnAnnouncementsObjectById(Long announcementsId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Announcements.class)
				.add(Restrictions.eq("id", announcementsId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Announcements) criteria.uniqueResult();
	}

	/**
	 * method to find announcement object by title
	 * 
	 * @param announsment
	 *            title
	 * @return Announcement
	 */
	public Announcements findAndReturnAnnouncementsObjectByTitle(String title) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Announcements.class)
				.add(Restrictions.eq("title", title));
		if (criteria.uniqueResult() == null)
			return null;
		return (Announcements) criteria.uniqueResult();
	}

	/**
	 * method to update announcement status
	 * 
	 * @param announsmentvo
	 * @return
	 */
	/*
	 * public void updateAnnouncementStatus(AnnouncementsVo announcementsVo) {
	 * Date dateobj = new Date(); for (int i = 0; i < announcementsVo.size();
	 * i++) { Date announcemnetDate =
	 * DateFormatter.convertStringToDate(announcementsVo.get(i).getEndDate());
	 * if (dateobj.compareTo(announcemnetDate) > 0) { Announcements
	 * announcements =
	 * findAndReturnAnnouncementsObjectById(announcementsVo.get(i)
	 * .getAnnouncementsId());
	 * announcements.setAnnouncementsStatus(announcementsStatusDao
	 * .findAnnouncementsStatusByAnnouncementsStatusName("inactive"));
	 * this.sessionFactory.getCurrentSession().merge(announcements);
	 * 
	 * } } }
	 */

	/**
	 * method to find all active announcements
	 * 
	 * @return announcementsVos
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AnnouncementsVo> findAllActiveAnnouncements() {
		List<AnnouncementsVo> announcementsVos = new ArrayList<AnnouncementsVo>();
		AnnouncementsStatus announcementsStatus = announcementsStatusDao
				.findAnnouncementsStatusByAnnouncementsStatusName("Active");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Announcements.class);
		Criterion betweenDates = Restrictions.ge("endDate", new Date());
		Criterion isActive = Restrictions.eq("announcementsStatus", announcementsStatus);
		LogicalExpression andExp = Restrictions.and(betweenDates, isActive);
		criteria.add(andExp);
		/*
		 * Criteria criteria =
		 * this.sessionFactory.getCurrentSession().createCriteria
		 * (Announcements.class) .add(Restrictions.eq("announcementsStatus",
		 * announcementsStatus));
		 */
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Announcements> announcements = criteria.list();
		for (Announcements announcementsObj : announcements) {
			announcementsVos.add(createAnnouncementsVo(announcementsObj));
		}
		return announcementsVos;
	}

	/**
	 * method to add announcements status
	 * 
	 * @return announcementsVos
	 */
	public void updateAnnouncementStatus(AnnouncementsVo announcementsVo) {
		Announcements announcements = findAndReturnAnnouncementsObjectByTitle(announcementsVo.getAnnouncementsTitle());
		// Announcements announcements=new Announcements();
		announcements.setStatusNotes(announcementsVo.getAnnouncementsStatusNotes());
		AnnouncementsStatus status = announcementsStatusDao
				.findAnnouncementsStatusByAnnouncementsStatusId(announcementsVo.getAnnouncementsStatusId());
		announcements.setAnnouncementsStatus(status);
		announcements.setStatusNotes(announcementsVo.getStatusNotes());
		try {
			this.sessionFactory.getCurrentSession().merge(announcements);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveAnnouncementDocuments(AnnouncementsVo announcementsVo) {
		Announcements announcements = findAndReturnAnnouncementsObjectById(announcementsVo.getAnnouncementsId());
		if (announcements == null)
			throw new ItemNotFoundException("Announcement Details Not Found.");
		announcements.setAnnouncementsDocuments(setAnnouncementDocuments(announcementsVo.getAnnouncementDocumentsVos(),
				announcements));
		this.sessionFactory.getCurrentSession().merge(announcements);
	}

	/**
	 * method to set documents to announcement object
	 * 
	 * @param documentsVos
	 * @param announcements
	 * @return
	 */
	private Set<AnnouncementsDocuments> setAnnouncementDocuments(List<AnnouncementDocumentsVo> documentsVos,
			Announcements announcements) {
		Set<AnnouncementsDocuments> announcementDocuments = new HashSet<AnnouncementsDocuments>();
		for (AnnouncementDocumentsVo documentsVo : documentsVos) {
			AnnouncementsDocuments documents = new AnnouncementsDocuments();
			documents.setDocumentName(documentsVo.getDocumentName());
			documents.setDocumentUrl(documentsVo.getDocumentUrl());
			documents.setAnnouncements(announcements);
			announcementDocuments.add(documents);
		}
		return announcementDocuments;
	}

	public void deleteAnnouncementDocument(Long documentId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AnnouncementsDocuments.class)
				.add(Restrictions.eq("id", documentId));
		AnnouncementsDocuments document = (AnnouncementsDocuments) criteria.uniqueResult();
		Announcements announcements = document.getAnnouncements();
		announcements.getAnnouncementsDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(announcements);
	}

}
