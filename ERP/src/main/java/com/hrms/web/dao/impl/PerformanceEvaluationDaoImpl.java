package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.PerformanceEvaluationDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.PerformanceEvaluation;
import com.hrms.web.entities.PerformanceEvaluationDocument;
import com.hrms.web.entities.PerformanceEvaluationQuestion;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.PerformanceEvaluationDocumentVo;
import com.hrms.web.vo.PerformanceEvaluationQuestionVo;
import com.hrms.web.vo.PerformanceEvaluationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 24-March-2015
 *
 */
@Repository
public class PerformanceEvaluationDaoImpl extends AbstractDao implements PerformanceEvaluationDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private BranchDao branchDao;

	public void savePerformanceEvaluation(PerformanceEvaluationVo evaluationVo) {
		PerformanceEvaluation evaluation = findPerformanceEvaluation(evaluationVo.getEvaluationId());
		if (evaluation == null)
			evaluation = new PerformanceEvaluation();

		evaluation.setTitle(evaluationVo.getEvaluationTitle());
		evaluation.setStartDate(DateFormatter.convertStringToDate(evaluationVo.getStartDate()));
		evaluation.setEndDate(DateFormatter.convertStringToDate(evaluationVo.getEndDate()));
		if (!evaluationVo.getBranches().contains(Long.parseLong("0")))
			evaluation.setBranch(setBranch(evaluationVo.getBranches()));
		evaluation.setDescription(evaluationVo.getDescription());
		evaluation.setNotes(evaluationVo.getNotes());
		this.sessionFactory.getCurrentSession().save(evaluation);
		evaluationVo.setEvaluationId(evaluation.getId());
		evaluationVo.setRecordedBy(evaluation.getCreatedBy().getUserName());
		evaluationVo.setRecordedOn(DateFormatter.convertDateToDetailedString(evaluation.getCreatedOn()));
	}

	public void updatePerformanceEvaluation(PerformanceEvaluationVo evaluationVo) {
		PerformanceEvaluation evaluation = findPerformanceEvaluation(evaluationVo.getEvaluationId());
		if (evaluation == null)
			throw new ItemNotFoundException("Performance Evaluation Details Not Found.");
		evaluation.setTitle(evaluationVo.getEvaluationTitle());
		if (!evaluationVo.getBranches().contains(Long.parseLong("0")))
			evaluation.setBranch(setBranch(evaluationVo.getBranches()));
		evaluation.setStartDate(DateFormatter.convertStringToDate(evaluationVo.getStartDate()));
		evaluation.setEndDate(DateFormatter.convertStringToDate(evaluationVo.getEndDate()));
		evaluation.setDescription(evaluationVo.getDescription());
		evaluation.setNotes(evaluationVo.getNotes());
		this.sessionFactory.getCurrentSession().merge(evaluation);
	}

	/**
	 * method to set branch set to performance evaluation
	 * 
	 * @param branchIds
	 * @return
	 */
	private Set<Branch> setBranch(List<Long> branchIds) {
		Set<Branch> branchs = new HashSet<Branch>();
		for (Long id : branchIds)
			branchs.add(branchDao.findAndReturnBranchObjectById(id));
		return branchs;
	}

	public void deletePerformanceEvaluation(Long evaluationId) {
		PerformanceEvaluation evaluation = findPerformanceEvaluation(evaluationId);
		if (evaluation == null)
			throw new ItemNotFoundException("Performance Evaluation Details Not Found.");
		evaluation.getBranch().clear();
		this.sessionFactory.getCurrentSession().delete(evaluation);
	}

	public PerformanceEvaluation findPerformanceEvaluation(Long evaluationId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluation.class)
				.add(Restrictions.eq("id", evaluationId));
		if (criteria.uniqueResult() == null)
			return null;
		return (PerformanceEvaluation) criteria.uniqueResult();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<PerformanceEvaluation> findPerformanceEvaluationByStartDate(Date startDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluation.class)
				.add(Restrictions.eq("startDate", startDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<PerformanceEvaluation> findPerformanceEvaluationByEndDate(Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluation.class)
				.add(Restrictions.eq("endDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<PerformanceEvaluation> findPerformanceEvaluationBetweenDates(Date startDate, Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluation.class)
				.add(Restrictions.ge("startDate", startDate)).add(Restrictions.le("endDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	/**
	 * method to create PerformanceEvaluationVo object
	 * 
	 * @param evaluation
	 * @return
	 */
	private PerformanceEvaluationVo createPerformanceEvaluationVo(PerformanceEvaluation evaluation) {
		PerformanceEvaluationVo evaluationVo = new PerformanceEvaluationVo();
		evaluationVo.setEvaluationTitle(evaluation.getTitle());
		evaluationVo.setBranches(getBranches(evaluation.getBranch()));
		evaluationVo.setStartDate(DateFormatter.convertDateToString(evaluation.getStartDate()));
		evaluationVo.setEndDate(DateFormatter.convertDateToString(evaluation.getEndDate()));
		evaluationVo.setDescription(evaluation.getDescription());
		evaluationVo.setNotes(evaluation.getNotes());
		evaluationVo.setEvaluationId(evaluation.getId());
		evaluationVo.setRecordedOn(DateFormatter.convertDateToString(evaluation.getCreatedOn()));
		evaluationVo.setRecordedBy(evaluation.getCreatedBy().getUserName());
		evaluationVo.setEvaluationQuestionVos(getPerformanceEvaluationQuestions(evaluation.getQuestions()));
		evaluationVo.setEvaluationDocuments(getPerformanceEvaluationDocuments(evaluation.getDocuments()));
		return evaluationVo;
	}

	/**
	 * method to get branches from Performance evaluation
	 * 
	 * @param branchs
	 * @return
	 */
	private List<Long> getBranches(Set<Branch> branchs) {
		List<Long> branches = new ArrayList<Long>();
		if (branchs == null) {
			branches.add(Long.valueOf("0"));
		} else {
			Iterator<Branch> iterator = branchs.iterator();
			while (iterator.hasNext()) {
				Branch branch = (Branch) iterator.next();
				branches.add(branch.getId());
			}
		}
		return branches;
	}

	/**
	 * method to get performance evaluation question list
	 * 
	 * @param questions
	 * @return
	 */
	private List<PerformanceEvaluationQuestionVo> getPerformanceEvaluationQuestions(
			Set<PerformanceEvaluationQuestion> questions) {
		List<PerformanceEvaluationQuestionVo> questionVos = new ArrayList<PerformanceEvaluationQuestionVo>();
		Iterator<PerformanceEvaluationQuestion> iterator = questions.iterator();
		while (iterator.hasNext()) {
			PerformanceEvaluationQuestionVo questionVo = new PerformanceEvaluationQuestionVo();
			PerformanceEvaluationQuestion performanceEvaluationQuestion = (PerformanceEvaluationQuestion) iterator
					.next();
			questionVo.setDocumentName(performanceEvaluationQuestion.getDocumentName());
			questionVo.setDocumentUrl(performanceEvaluationQuestion.getDocumentUrl());
			questionVo.setQuestionId(performanceEvaluationQuestion.getId());
			questionVos.add(questionVo);
		}
		return questionVos;
	}

	/**
	 * method to get performance evaluation document list
	 * 
	 * @param questions
	 * @return
	 */
	private List<PerformanceEvaluationDocumentVo> getPerformanceEvaluationDocuments(
			Set<PerformanceEvaluationDocument> documents) {
		List<PerformanceEvaluationDocumentVo> documentVos = new ArrayList<PerformanceEvaluationDocumentVo>();
		Iterator<PerformanceEvaluationDocument> iterator = documents.iterator();
		while (iterator.hasNext()) {
			PerformanceEvaluationDocumentVo documentVo = new PerformanceEvaluationDocumentVo();
			PerformanceEvaluationDocument performanceEvaluationDocument = (PerformanceEvaluationDocument) iterator
					.next();
			documentVo.setDocumentName(performanceEvaluationDocument.getDocumentName());
			documentVo.setDocumentUrl(performanceEvaluationDocument.getDocumentUrl());
			documentVo.setDocumentId(performanceEvaluationDocument.getId());
			documentVos.add(documentVo);
		}
		return documentVos;
	}

	public PerformanceEvaluationVo findPerformanceEvaluationById(Long evaluationId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluation.class)
				.add(Restrictions.eq("id", evaluationId));
		if (criteria.uniqueResult() == null)
			return null;
		return createPerformanceEvaluationVo((PerformanceEvaluation) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<PerformanceEvaluationVo> findPerformanceEvaluationVoByStartDate(Date startDate) {
		List<PerformanceEvaluationVo> evaluationVos = new ArrayList<PerformanceEvaluationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluation.class)
				.add(Restrictions.eq("startDate", startDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PerformanceEvaluation> evaluations = criteria.list();
		for (PerformanceEvaluation evaluation : evaluations)
			evaluationVos.add(createPerformanceEvaluationVo(evaluation));
		return evaluationVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<PerformanceEvaluationVo> findPerformanceEvaluationVoByEndDate(Date endDate) {
		List<PerformanceEvaluationVo> evaluationVos = new ArrayList<PerformanceEvaluationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluation.class)
				.add(Restrictions.eq("endDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PerformanceEvaluation> evaluations = criteria.list();
		for (PerformanceEvaluation evaluation : evaluations)
			evaluationVos.add(createPerformanceEvaluationVo(evaluation));
		return evaluationVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<PerformanceEvaluationVo> findPerformanceEvaluationVoBetweenDates(Date startDate, Date endDate) {
		List<PerformanceEvaluationVo> evaluationVos = new ArrayList<PerformanceEvaluationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluation.class)
				.add(Restrictions.ge("startDate", startDate)).add(Restrictions.le("endDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PerformanceEvaluation> evaluations = criteria.list();
		for (PerformanceEvaluation evaluation : evaluations)
			evaluationVos.add(createPerformanceEvaluationVo(evaluation));
		return evaluationVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<PerformanceEvaluationVo> findAllPerformanceEvaluation() {
		List<PerformanceEvaluationVo> evaluationVos = new ArrayList<PerformanceEvaluationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluation.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PerformanceEvaluation> evaluations = criteria.list();
		for (PerformanceEvaluation evaluation : evaluations)
			evaluationVos.add(createPerformanceEvaluationVo(evaluation));
		return evaluationVos;
	}

	/**
	 * method to set performance evaluation questions
	 * 
	 * @param evaluationQuestionVos
	 * @return
	 */
	private Set<PerformanceEvaluationQuestion> setPerformanceEvaluationQuestion(
			List<PerformanceEvaluationQuestionVo> evaluationQuestionVos, PerformanceEvaluation evaluation) {
		Set<PerformanceEvaluationQuestion> evaluationQuestions = new HashSet<PerformanceEvaluationQuestion>();
		for (PerformanceEvaluationQuestionVo evaluationQuestionVo : evaluationQuestionVos) {
			PerformanceEvaluationQuestion evaluationQuestion = new PerformanceEvaluationQuestion();
			evaluationQuestion.setDocumentName(evaluationQuestionVo.getDocumentName());
			evaluationQuestion.setDocumentUrl(evaluationQuestionVo.getDocumentUrl());
			evaluationQuestion.setEvaluation(evaluation);
			evaluationQuestions.add(evaluationQuestion);
		}
		return evaluationQuestions;
	}

	/**
	 * method to set performance evaluation questions
	 * 
	 * @param evaluationQuestionVos
	 * @return
	 */
	private Set<PerformanceEvaluationDocument> setPerformanceEvaluationDocuments(
			List<PerformanceEvaluationDocumentVo> evaluationDocumentVos, PerformanceEvaluation evaluation) {
		Set<PerformanceEvaluationDocument> evaluationDocuments = new HashSet<PerformanceEvaluationDocument>();
		for (PerformanceEvaluationDocumentVo evaluationQuestionVo : evaluationDocumentVos) {
			PerformanceEvaluationDocument evaluationDocument = new PerformanceEvaluationDocument();
			evaluationDocument.setDocumentName(evaluationQuestionVo.getDocumentName());
			evaluationDocument.setDocumentUrl(evaluationQuestionVo.getDocumentUrl());
			evaluationDocument.setEvaluation(evaluation);
			evaluationDocuments.add(evaluationDocument);
		}
		return evaluationDocuments;
	}

	public void savePerformanceEvaluationDocuments(PerformanceEvaluationVo evaluationVo) {
		PerformanceEvaluation evaluation = findPerformanceEvaluation(evaluationVo.getEvaluationId());
		if (evaluation == null)
			throw new ItemNotFoundException("Performance Evaulation Details Not Found.");
		evaluation.setDocuments(setPerformanceEvaluationDocuments(evaluationVo.getEvaluationDocuments(), evaluation));
		this.sessionFactory.getCurrentSession().merge(evaluation);
	}

	public void deletePerformanceEvaluationDocument(Long documentId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluationDocument.class)
				.add(Restrictions.eq("id", documentId));
		PerformanceEvaluationDocument document = (PerformanceEvaluationDocument) criteria.uniqueResult();
		PerformanceEvaluation evaluation = document.getEvaluation();
		evaluation.getDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(evaluation);
	}

	public void savePerformanceEvaluationQuestions(PerformanceEvaluationVo evaluationVo) {
		PerformanceEvaluation evaluation = findPerformanceEvaluation(evaluationVo.getEvaluationId());
		if (evaluation == null)
			throw new ItemNotFoundException("Performance Evaulation Details Not Found.");
		evaluation.setQuestions(setPerformanceEvaluationQuestion(evaluationVo.getEvaluationQuestionVos(), evaluation));
		this.sessionFactory.getCurrentSession().merge(evaluation);
	}

	public void deletePerformanceEvaluationQuestion(Long documentId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PerformanceEvaluationQuestion.class)
				.add(Restrictions.eq("id", documentId));
		PerformanceEvaluationQuestion question = (PerformanceEvaluationQuestion) criteria.uniqueResult();
		PerformanceEvaluation evaluation = question.getEvaluation();
		evaluation.getQuestions().remove(question);
		this.sessionFactory.getCurrentSession().delete(question);
		this.sessionFactory.getCurrentSession().merge(evaluation);
	}

}
