package com.hrms.web.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BonusDateDao;
import com.hrms.web.entities.BonusDate;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.BonusDateVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Repository
public class BonusDateDaoImpl extends AbstractDao implements BonusDateDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveOrUpdateBonusDate(BonusDateVo bonusDateVo) {
		BonusDate bonusDate = findBonusDate(bonusDateVo.getBonusDateId());
		if (bonusDate == null)
			bonusDate = new BonusDate();
		bonusDate.setDay(bonusDateVo.getDay());
		bonusDate.setMonth(bonusDateVo.getMonth());
		bonusDate.setYear(bonusDateVo.getYear());
		this.sessionFactory.getCurrentSession().saveOrUpdate(bonusDate);
		userActivitiesService.saveUserActivity("Added new Bonus Date "+bonusDateVo.getDay()+"/"+bonusDateVo.getMonth()+"/"+bonusDateVo.getYear());
	}

	public void deleteBonusDate(Long bonusDateId) {
		BonusDate bonusDate = findBonusDate(bonusDateId);
		if (bonusDate == null)
			throw new ItemNotFoundException("Bonus Date Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(bonusDate);
		userActivitiesService.saveUserActivity("Deleted Bonus Date");
	}

	public BonusDate findBonusDate(Long bonusDateId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BonusDate.class)
				.add(Restrictions.eq("id", bonusDateId));
		if (criteria.uniqueResult() == null)
			return null;
		return (BonusDate) criteria.uniqueResult();
	}

	/**
	 * method to create BonusDateVo object
	 * 
	 * @param bonusDate
	 * @return bonusDateVo
	 */
	private BonusDateVo createBonusDateVo(BonusDate bonusDate) {
		BonusDateVo bonusDateVo = new BonusDateVo();
		bonusDateVo.setBonusDateId(bonusDate.getId());
		bonusDateVo.setDay(bonusDate.getDay());
		bonusDateVo.setMonth(bonusDate.getMonth());
		bonusDateVo.setYear(bonusDate.getYear());
		return bonusDateVo;
	}

	public BonusDateVo findBonusDateById(Long bonusDateId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BonusDate.class)
				.add(Restrictions.eq("id", bonusDateId));
		if (criteria.uniqueResult() == null)
			return null;
		return createBonusDateVo((BonusDate) criteria.uniqueResult());
	}

	public BonusDateVo findBonusDate() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BonusDate.class);
		if (criteria.uniqueResult() == null)
			return null;
		return createBonusDateVo((BonusDate) criteria.uniqueResult());
	}

}
