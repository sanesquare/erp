package com.hrms.web.dao.impl;

import java.math.BigDecimal;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ProfesionalTaxDao;
import com.hrms.web.entities.ProfesionalTax;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.ProfesionalTaxVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-May-2015
 *
 */
@Repository
public class ProfesionalTaxDaoImpl extends AbstractDao implements ProfesionalTaxDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveOrUpdate(ProfesionalTaxVo profesionalTaxVo) {
		ProfesionalTax profesionalTax = findProfesionalTax(profesionalTaxVo.getPtId());
		if (profesionalTax == null)
			profesionalTax = new ProfesionalTax();
		profesionalTax.setAmount(new BigDecimal(profesionalTaxVo.getAmount()));
		this.sessionFactory.getCurrentSession().saveOrUpdate(profesionalTax);
		profesionalTaxVo.setPtId(profesionalTax.getId());
	}

	public ProfesionalTax findProfesionalTax(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfesionalTax.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return (ProfesionalTax) criteria.uniqueResult();
	}

	/**
	 * method to create ProfesionalTaxVo object
	 * 
	 * @param profesionalTax
	 * @return profesionalTaxVo
	 */
	private ProfesionalTaxVo createProfesionalTaxVo(ProfesionalTax profesionalTax) {
		ProfesionalTaxVo profesionalTaxVo = new ProfesionalTaxVo();
		profesionalTaxVo.setPtId(profesionalTax.getId());
		profesionalTaxVo.setAmount(profesionalTax.getAmount().doubleValue());
		return profesionalTaxVo;
	}

	public ProfesionalTaxVo findProfesionalTaxById(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfesionalTax.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return createProfesionalTaxVo((ProfesionalTax) criteria.uniqueResult());
	}

	public ProfesionalTaxVo findProfesionalTax() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfesionalTax.class);
		if (criteria.uniqueResult() == null)
			return null;
		return createProfesionalTaxVo((ProfesionalTax) criteria.uniqueResult());
	}

}
