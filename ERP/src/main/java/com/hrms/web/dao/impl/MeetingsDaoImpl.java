package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.DepartmentDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.MeetingsDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.LeaveDocuments;
import com.hrms.web.entities.MeetingDocuments;
import com.hrms.web.entities.MeetingEmployee;
import com.hrms.web.entities.MeetingStatus;
import com.hrms.web.entities.Meetings;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.util.SmsServiceUtil;
import com.hrms.web.vo.MeetingDocumentsVo;
import com.hrms.web.vo.MeetingsVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Shamsheer
 * @since 12-March-2015
 *
 */
@Repository
public class MeetingsDaoImpl extends AbstractDao implements MeetingsDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private BranchDao branchDao;

	@Autowired
	private DepartmentDao departmentDao;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private TempNotificationDao notificationDao;

	/**
	 * method to save new meetings
	 * 
	 * @param meetingsVo
	 * @throws Exception
	 */
	public Long saveMeetings(MeetingsVo meetingsVo) throws Exception {
		Meetings meetings = new Meetings();
		meetings.setAdditionalInformation(meetingsVo.getAdditionalInformation());
		meetings.setTime(DateFormatter.convertStringTimeToSqlTime(meetingsVo.getTime()));

		MeetingStatus status = new MeetingStatus();
		status.setId(meetingsVo.getStatusId());
		meetings.setMeetingStatus(status);

		meetings.setSendEmail(meetingsVo.isSendEmail());
		meetings.setSendNotification(meetingsVo.isSendNotification());
		meetings.setAgenda(meetingsVo.getAgenda());
		meetings.setStatusNotes(meetingsVo.getStatusDescription());
		meetings.setMeetingStartDate(DateFormatter.convertStringToDate(meetingsVo.getMeetingStartDate()));
		meetings.setMeetingEndDate(DateFormatter.convertStringToDate(meetingsVo.getMeetingEndDate()));
		meetings.setTitle(meetingsVo.getMeetingsTitle());
		meetings.setVenue(meetingsVo.getVenue());
		TempNotificationVo notificationVo = new TempNotificationVo();
		notificationVo.setSubject("A Meeting Has Been Scheduled.  ");
		for (Long id : meetingsVo.getEmployeeIds()) {
			notificationVo.getEmployeeIds().add(id);
			meetings.getMeetingEmployees().add(setMeetingEmployee(id, meetings));
		}
		if (notificationVo.getEmployeeCodes().size() != 0 || notificationVo.getEmployeeIds().size() != 0)
			notificationDao.saveNotification(notificationVo);
		sendSms(meetings);
		return (Long) this.sessionFactory.getCurrentSession().save(meetings);
	}

	/**
	 * method to send sms to meeting participants
	 * 
	 * @param meetings
	 * @throws Exception
	 */
	private void sendSms(Meetings meetings) throws Exception {
		List<String> toNumbers = new ArrayList<String>();
		if (meetings.getMeetingEmployees() != null) {
			for (MeetingEmployee meetingEmployee : meetings.getMeetingEmployees()) {
				Employee employee = meetingEmployee.getEmployee();
				try {
					if (employee.getUserProfile() != null && employee.getUserProfile().getMobile() != null)
						toNumbers.add(employee.getUserProfile().getMobile());
				} catch (Exception e) {
				}
			}
			String msg = "MEETING (" + meetings.getTitle() + ") on "
					+ DateFormatter.convertDateToString(meetings.getMeetingStartDate()) + " "
					+ DateFormatter.convertSqlTimeToString(meetings.getTime()) + " at " + meetings.getVenue();
			SmsServiceUtil.sendSMS(toNumbers, msg);
		}
	}

	/**
	 * method to update an existing meeting
	 * 
	 * @param meetingsVo
	 */
	public void updateMeetings(MeetingsVo meetingsVo) {
		Query query = this.sessionFactory.getCurrentSession().createQuery(
				"DELETE FROM MeetingEmployee WHERE meetingEmployeeId.meetings.id=?");
		query.setLong(0, meetingsVo.getMeetingId());
		query.executeUpdate();
		Meetings meetings = findAndReturnMeetingsObjectById(meetingsVo.getMeetingId());

		meetings.setAdditionalInformation(meetingsVo.getAdditionalInformation());
		meetings.setTime(DateFormatter.convertStringTimeToSqlTime(meetingsVo.getTime()));

		MeetingStatus status = new MeetingStatus();
		status.setId(meetingsVo.getStatusId());
		meetings.setMeetingStatus(status);

		meetings.setSendEmail(meetingsVo.isSendEmail());
		meetings.setSendNotification(meetingsVo.isSendNotification());
		meetings.setAgenda(meetingsVo.getAgenda());
		meetings.setStatusNotes(meetingsVo.getStatusDescription());
		meetings.setMeetingStartDate(DateFormatter.convertStringToDate(meetingsVo.getMeetingStartDate()));
		meetings.setMeetingEndDate(DateFormatter.convertStringToDate(meetingsVo.getMeetingEndDate()));
		meetings.setTitle(meetingsVo.getMeetingsTitle());
		meetings.setVenue(meetingsVo.getVenue());
		if (meetingsVo.getEmployeeIds() != null) {
			for (Long id : meetingsVo.getEmployeeIds()) {
				meetings.getMeetingEmployees().add(setMeetingEmployee(id, meetings));
			}
		}
		try {
			sendSms(meetings);
		} catch (Exception e) {
		}
		this.sessionFactory.getCurrentSession().merge(meetings);
	}

	/**
	 * method to set meeting employee
	 * 
	 * @param ids
	 * @return
	 */
	private MeetingEmployee setMeetingEmployee(Long id, Meetings meetings) {
		MeetingEmployee employee = new MeetingEmployee();
		Employee employeeObj = employeeDao.findAndReturnEmployeeById(id);
		employee.setEmployee(employeeObj);
		employee.setBranch(employeeObj.getBranch());
		employee.setDepartment(employeeObj.getDepartmentEmployees());
		employee.setMeetings(meetings);
		return employee;
	}

	/**
	 * method to delete an existing meeting
	 * 
	 * @param meetingsId
	 */
	public void deleteMeeting(Long meetingsId) {
		Query query = this.sessionFactory.getCurrentSession().createQuery(
				"DELETE FROM MeetingEmployee WHERE meetingEmployeeId.meetings.id=?");
		query.setLong(0, meetingsId);
		query.executeUpdate();
		Meetings meeting = findAndReturnMeetingsObjectById(meetingsId);
		if (meeting.getMeetingDocuments() != null) {
			SFTPOperation operation = new SFTPOperation();
			for (MeetingDocuments documents : meeting.getMeetingDocuments()) {
				operation.removeFileFromSFTP(documents.getDocumentPath());
			}
		}
		this.sessionFactory.getCurrentSession().delete(meeting);
	}

	/**
	 * method to list all meetings
	 * 
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<MeetingsVo> listAllMeetings() {
		List<MeetingsVo> meetingsVos = new ArrayList<MeetingsVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Meetings.class)
				.addOrder(Order.desc("id"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Meetings> meetings = criteria.list();
		for (Meetings meeting : meetings) {
			meetingsVos.add(createMeatingVo(meeting));
		}
		return meetingsVos;
	}

	/**
	 * method to find meetings by id
	 * 
	 * @param id
	 * @return
	 */
	public MeetingsVo findMeetingsById(Long id) {
		return createDetailedVo((Meetings) this.sessionFactory.getCurrentSession().createCriteria(Meetings.class)
				.add(Restrictions.eq("id", id)).uniqueResult());
	}

	/**
	 * method to find meetings by title
	 * 
	 * @param title
	 * @return
	 */
	public MeetingsVo findMeetingsByTitle(String title) {
		return createMeatingVo((Meetings) this.sessionFactory.getCurrentSession().createCriteria(Meetings.class)
				.add(Restrictions.eq("title", title)).uniqueResult());
	}

	/**
	 * method to find and return meetings Object by id
	 * 
	 * @param id
	 * @return
	 */
	public Meetings findAndReturnMeetingsObjectById(Long id) {
		return (Meetings) this.sessionFactory.getCurrentSession().createCriteria(Meetings.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * method to find and return meetings Object by title
	 * 
	 * @param id
	 * @return
	 */
	public Meetings findAndReturnMeetingsObjectByTitle(String title) {
		return (Meetings) this.sessionFactory.getCurrentSession().createCriteria(Meetings.class)
				.add(Restrictions.eq("title", title)).uniqueResult();
	}

	/**
	 * methd to create meetingVo
	 * 
	 * @param meetings
	 * @return
	 */
	private MeetingsVo createMeatingVo(Meetings meetings) {
		MeetingsVo meetingsVo = new MeetingsVo();
		meetingsVo.setMeetingId(meetings.getId());
		meetingsVo.setMeetingsTitle(meetings.getTitle());
		meetingsVo.setMeetingEndDate(DateFormatter.convertDateToString(meetings.getMeetingEndDate()));
		meetingsVo.setMeetingStartDate(DateFormatter.convertDateToString(meetings.getMeetingStartDate()));
		meetingsVo.setStatus(meetings.getMeetingStatus().getName());
		meetingsVo.setVenue(meetings.getVenue());
		return meetingsVo;
	}

	/**
	 * method to create detailed meeting vo
	 * 
	 * @param meetings
	 * @return
	 */
	private MeetingsVo createDetailedVo(Meetings meetings) {
		MeetingsVo vo = new MeetingsVo();
		vo.setMeetingId(meetings.getId());
		vo.setMeetingsTitle(meetings.getTitle());
		vo.setMeetingEndDate(DateFormatter.convertDateToString(meetings.getMeetingEndDate()));
		vo.setMeetingStartDate(DateFormatter.convertDateToString(meetings.getMeetingStartDate()));
		vo.setStatus(meetings.getMeetingStatus().getName());
		vo.setStatusId(meetings.getMeetingStatus().getId());
		vo.setStatusDescription(meetings.getStatusNotes());
		vo.setCreatedBy(meetings.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToDetailedString(meetings.getCreatedOn()));
		vo.setSendNotification(meetings.isSendNotification());
		vo.setVenue(meetings.getVenue());
		vo.setTime(DateFormatter.convertSqlTimeToString(meetings.getTime()));
		vo.setSendEmail(meetings.isSendEmail());
		vo.setAdditionalInformation(meetings.getAdditionalInformation());
		vo.setAgenda(meetings.getAgenda());
		if (meetings.getMeetingEmployees() != null) {
			Set<Long> ids = new HashSet<Long>();
			for (MeetingEmployee employee : meetings.getMeetingEmployees()) {
				Long id = employee.getEmployee().getId();
				ids.add(id);
			}
			vo.setEmployeeIds(ids);
		}
		if (meetings.getMeetingDocuments() != null) {
			List<MeetingDocumentsVo> vos = new ArrayList<MeetingDocumentsVo>();
			for (MeetingDocuments documents : meetings.getMeetingDocuments()) {
				MeetingDocumentsVo documentsVo = new MeetingDocumentsVo();
				documentsVo.setFilename(documents.getName());
				documentsVo.setUrl(documents.getDocumentPath());
				vos.add(documentsVo);
			}
			vo.setDocumentsVos(vos);
		}
		if (meetings.getMeetingEmployees() != null) {
			List<String> employees = new ArrayList<String>();
			for (MeetingEmployee employee : meetings.getMeetingEmployees()) {
				String employe = null;
				if (employee.getEmployee().getUserProfile() != null)
					employe = employee.getEmployee().getUserProfile().getFirstName() + " "
							+ employee.getEmployee().getUserProfile().getLastname();
				employees.add(employe);
			}
			vo.setEmployees(employees);
		}
		return vo;
	}

	/**
	 * method to update document
	 */
	public void updateDocument(MeetingsVo meetingsVo) {
		Meetings meetings = findAndReturnMeetingsObjectById(meetingsVo.getMeetingId());
		meetings.setMeetingDocuments(setDocuments(meetingsVo.getDocumentsVos(), meetings));
		this.sessionFactory.getCurrentSession().merge(meetings);
	}

	/**
	 * method to set documents
	 * 
	 * @param vos
	 * @param meetingsm
	 * @return
	 */
	private Set<MeetingDocuments> setDocuments(List<MeetingDocumentsVo> vos, Meetings meetings) {
		Set<MeetingDocuments> documentss = new HashSet<MeetingDocuments>();
		for (MeetingDocumentsVo documentsVo : vos) {
			MeetingDocuments documents = new MeetingDocuments();
			documents.setName(documentsVo.getFilename());
			documents.setDocumentPath(documentsVo.getUrl());
			documents.setMeetings(meetings);
			documentss.add(documents);
		}
		return documentss;
	}

	/**
	 * method to delete document
	 */
	public void deleteDocument(String url) {
		MeetingDocuments documents = (MeetingDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(MeetingDocuments.class).add(Restrictions.eq("documentPath", url)).uniqueResult();
		Meetings meetings = documents.getMeetings();
		if (meetings.getMeetingDocuments() != null) {
			meetings.getMeetingDocuments().remove(documents);
			this.sessionFactory.getCurrentSession().merge(meetings);
		}
		this.sessionFactory.getCurrentSession().delete(documents);
	}
}
