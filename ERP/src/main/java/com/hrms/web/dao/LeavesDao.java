package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.Leaves;
import com.hrms.web.vo.LeavesVo;

/**
 * 
 * @author Shamsheer
 * @since 6-April-2015
 */
public interface LeavesDao {

	/**
	 * method to save new leave
	 * 
	 * @param leavesVo
	 * @return
	 */
	public Long saveLeave(LeavesVo leavesVo);

	/**
	 * method to update leave
	 * 
	 * @param leavesVo
	 */
	public void updateLeave(LeavesVo leavesVo);

	/**
	 * method to delete leave
	 * 
	 * @param leaveId
	 */
	public void deleteLeave(Long leaveId);

	/**
	 * method to get leave by id
	 * 
	 * @param id
	 * @return
	 */
	public Leaves getLeave(Long id);

	/**
	 * method to get leave by type
	 * 
	 * @param typeId
	 * @return
	 */
	public List<Leaves> getLeaveByType(Long typeId);

	/**
	 * method to get leave by id
	 * 
	 * @param leaveId
	 * @return
	 */
	public LeavesVo getleaveById(Long leaveId);

	/**
	 * method to list all status
	 * 
	 * @return
	 */
	public List<LeavesVo> listStatus();

	/**
	 * method to get leaves by employee
	 * 
	 * @param employeeId
	 * @return
	 */
	public List<LeavesVo> getLeavesByEmployee(Long employeeId);

	/**
	 * method to get leaves by employee
	 * 
	 * @param employeeId
	 * @return
	 */
	public List<LeavesVo> getLeavesTakenByEmployee(Long employeeId);

	/**
	 * method to get leaves by type
	 * 
	 * @param typeId
	 * @return
	 */
	public List<LeavesVo> getLeavesByType(Long typeId);

	/**
	 * method to get all leaves
	 * 
	 * @return
	 */
	public List<LeavesVo> listAllLeaves();

	/**
	 * method to update document
	 * 
	 * @param leavesVo
	 */
	public void updateDocuments(LeavesVo leavesVo);

	/**
	 * method to delete document
	 * 
	 * @param url
	 */
	public void deleteDocument(String url);

	/**
	 * method to update status
	 * 
	 * @param vo
	 */
	public void updateStatus(LeavesVo vo);

	/**
	 * method to find all leaves by employee
	 * 
	 * @param employeeId
	 * @return
	 */
	public List<LeavesVo> findAllLeaveByEmployee(String employeeCode);
	
	/**
	 * method to find approved leaves of employee between dates
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<LeavesVo> findApprovedLeavesBetweenDates(Long employeeId , String startDate , String endDate);
}
