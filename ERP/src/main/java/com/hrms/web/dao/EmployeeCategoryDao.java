package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.EmployeeCategory;
import com.hrms.web.vo.EmployeeCategoryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public interface EmployeeCategoryDao {

	/**
	 * method to save new EmployeeCategory
	 * 
	 * @param employeeCategoryVo
	 */
	public void saveEmployeeCategory(EmployeeCategoryVo employeeCategoryVo);

	/**
	 * method to update EmployeeCategory
	 * 
	 * @param employeeCategoryVo
	 */
	public void updateEmployeeCategory(EmployeeCategoryVo employeeCategoryVo);

	/**
	 * method to delete EmployeeCategory
	 * 
	 * @param employeeCategoryId
	 */
	public void deleteEmployeeCategory(Long employeeCategoryId);

	/**
	 * method to find EmployeeCategory by id
	 * 
	 * @param employeeCategoryId
	 * @return EmployeeCategory
	 */
	public EmployeeCategory findEmployeeCategory(Long employeeCategoryId);

	/**
	 * method to find EmployeeCategory by type
	 * 
	 * @param employeeCategory
	 * @return EmployeeCategory
	 */
	public EmployeeCategory findEmployeeCategory(String employeeCategory);

	/**
	 * method to find EmployeeCategory by id
	 * 
	 * @param employeeCategoryId
	 * @return EmployeeCategoryVo
	 */
	public EmployeeCategoryVo findEmployeeCategoryById(Long employeeCategoryId);

	/**
	 * method to find EmployeeCategory by type
	 * 
	 * @param employeeCategory
	 * @return EmployeeCategoryVo
	 */
	public EmployeeCategoryVo findEmployeeCategoryByCategory(String employeeCategory);

	/**
	 * method to find all EmployeeCategory
	 * 
	 * @return List<EmployeeCategoryVo>
	 */
	public List<EmployeeCategoryVo> findAllEmployeeCategory();
}
