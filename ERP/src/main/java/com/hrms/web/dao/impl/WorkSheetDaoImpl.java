package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ProjectDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.dao.WorkSheetDao;
import com.hrms.web.dao.WorkSheetTaskDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.entities.JobInterviewDocuments;
import com.hrms.web.entities.Project;
import com.hrms.web.entities.Travel;
import com.hrms.web.entities.UserProfile;
import com.hrms.web.entities.WorkSheet;
import com.hrms.web.entities.WorkSheetDocuments;
import com.hrms.web.entities.WorkSheetTasks;
import com.hrms.web.service.TempNotificationService;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.TempNotificationVo;
import com.hrms.web.vo.WorkSheetDocumentVo;
import com.hrms.web.vo.WorkSheetVo;
import com.hrms.web.vo.WorksheetTasksVo;

/**
 * 
 * @author Vinutha
 * @since 1-April-2015
 *
 */
@Repository
public class WorkSheetDaoImpl extends AbstractDao implements WorkSheetDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private ProjectDao projectDao;

	@Autowired
	private WorkSheetTaskDao taskDao;
	
	@Autowired
	private TempNotificationDao  notificationDao;

	/**
	 * method to save basic info
	 * 
	 * @param worksheetVo
	 * @return
	 */
	public Long saveBasicInfo(WorkSheetVo worksheetVo) {
		WorkSheet workSheet = new WorkSheet();
		Employee employee = employeeDao.findAndReturnEmployeeByCode(worksheetVo.getEmployeeCode());
		workSheet.setEmployee(employee);
		if (worksheetVo.getDate() != null)
			workSheet.setDate(DateFormatter.convertStringToDate(worksheetVo.getDate()));
		if(worksheetVo.getReportingCode()!=null)
		{
			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject("Work Sheet Report  Has Been Received From "+employee.getUserProfile().getFirstName()
					+" "+employee.getUserProfile().getLastname()+".");
			notificationVo.getEmployeeCodes().addAll(worksheetVo.getReportingCode());
			notificationDao.saveNotification(notificationVo);
			Set<Employee> reportingTo = new HashSet<Employee>();
			for(String code : worksheetVo.getReportingCode())
			{
				reportingTo.add(employeeDao.findAndReturnEmployeeByCode(code));
			}
			workSheet.setReportTo(reportingTo);
		}
		Long id = (Long) this.sessionFactory.getCurrentSession().save(workSheet);
		return id;
	}

	/**
	 * method to update basic info
	 * 
	 * @param worksheetVo
	 */
	public void updateBasicInfo(WorkSheetVo worksheetVo) {
		WorkSheet workSheet = findAndReturnObjectById(worksheetVo.getWorkSheetId());
		Employee employee = employeeDao.findAndReturnEmployeeByCode(worksheetVo.getEmployeeCode());
		workSheet.setEmployee(employee);
		if (worksheetVo.getDate() != null)
			workSheet.setDate(DateFormatter.convertStringToDate(worksheetVo.getDate()));
		if(worksheetVo.getReportingCode()!=null)
		{
			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject("Work Sheet Report  Has Been Received From "+employee.getUserProfile().getFirstName()
					+" "+employee.getUserProfile().getLastname()+".");
			notificationVo.getEmployeeCodes().addAll(worksheetVo.getReportingCode());
			notificationDao.saveNotification(notificationVo);
			Set<Employee> reportingTo = new HashSet<Employee>();
			for(String code : worksheetVo.getReportingCode())
			{
				reportingTo.add(employeeDao.findAndReturnEmployeeByCode(code));
			}
			workSheet.setReportTo(reportingTo);
		}
		this.sessionFactory.getCurrentSession().merge(workSheet);
	}

	/**
	 * method to save project and task
	 * 
	 * @param worksheetVo
	 */
	public void saveProjectTask(WorkSheetVo worksheetVo) {
		WorkSheet workSheet = findAndReturnObjectById(worksheetVo.getWorkSheetId());
		/*
		 * workSheet.setProject(projectDao.findAndReturnProjectObjectById(
		 * worksheetVo.getProjectId()));
		 * workSheet.setTask(worksheetVo.getTask());
		 */
		WorkSheetTasks task = new WorkSheetTasks();
		Project project = new Project();
		task.setTask(worksheetVo.getTask());
		task.setStartTime(DateFormatter.convertStringTimeToSqlTime(worksheetVo.getStartTime()));
		task.setEndTime(DateFormatter.convertStringTimeToSqlTime(worksheetVo.getEndTime()));
		task.setWorkSheet(workSheet);
		this.sessionFactory.getCurrentSession().save(task);
		this.sessionFactory.getCurrentSession().merge(workSheet);
	}

	/**
	 * Method to update tasks
	 * 
	 * @param worksheetVo
	 */
	public void updateProjectTask(WorkSheetVo worksheetVo) {
		WorkSheet workSheet = findAndReturnObjectById(worksheetVo.getWorkSheetId());
		/*
		 * workSheet.setProject(projectDao.findAndReturnProjectObjectById(
		 * worksheetVo.getProjectId()));
		 * workSheet.setTask(worksheetVo.getTask());
		 */
		WorkSheetTasks task = taskDao.findTaskObject(worksheetVo.getTaskId());
		Project project = new Project();
		task.setTask(worksheetVo.getTask());
		task.setStartTime(DateFormatter.convertStringTimeToSqlTime(worksheetVo.getStartTime()));
		task.setEndTime(DateFormatter.convertStringTimeToSqlTime(worksheetVo.getEndTime()));
		task.setWorkSheet(workSheet);
		this.sessionFactory.getCurrentSession().merge(task);
		this.sessionFactory.getCurrentSession().merge(workSheet);
	}

	/**
	 * method to save description
	 * 
	 * @param worksheetVo
	 */
	public void saveDesc(WorkSheetVo worksheetVo) {
		WorkSheet workSheet = findAndReturnObjectById(worksheetVo.getWorkSheetId());
		workSheet.setDescription(worksheetVo.getDescription());
		this.sessionFactory.getCurrentSession().merge(workSheet);
	}

	/**
	 * method to save additional information
	 * 
	 * @param worksheetVo
	 */
	public void saveAddInfo(WorkSheetVo worksheetVo) {
		WorkSheet workSheet = findAndReturnObjectById(worksheetVo.getWorkSheetId());
		workSheet.setAdditionalInfo(worksheetVo.getAddInfo());
		this.sessionFactory.getCurrentSession().merge(workSheet);
	}

	/**
	 * method to return object by id
	 * 
	 * @param id
	 * @return
	 */
	public WorkSheet findAndReturnObjectById(long id) {
		WorkSheet workSheet = (WorkSheet) this.sessionFactory.getCurrentSession().createCriteria(WorkSheet.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
		return workSheet;
	}

	/**
	 * method to update documents
	 * 
	 * @param workSheetVo
	 */
	public void updateDocuments(WorkSheetVo workSheetVo) {
		WorkSheet workSheet = (WorkSheet) this.sessionFactory.getCurrentSession().createCriteria(WorkSheet.class)
				.add(Restrictions.eq("id", workSheetVo.getWorkSheetId())).uniqueResult();
		workSheet.setWorkSheetDocuments(setDocuments(workSheetVo, workSheet));
		this.sessionFactory.getCurrentSession().merge(workSheet);
	}

	/**
	 * method to set document to work sheet
	 * 
	 * @param sheetVo
	 * @param sheet
	 * @return
	 */
	private Set<WorkSheetDocuments> setDocuments(WorkSheetVo sheetVo, WorkSheet sheet) {
		Set<WorkSheetDocuments> documents = new HashSet<WorkSheetDocuments>();
		Set<WorkSheetDocumentVo> documentVos = sheetVo.getDocumentsVo();
		for (WorkSheetDocumentVo docVo : documentVos) {
			WorkSheetDocuments doc = new WorkSheetDocuments();
			doc.setDocumentName(docVo.getDocumentName());
			doc.setDocumentPath(docVo.getDocumentUrl());
			doc.setWorkSheet(sheet);
			documents.add(doc);
		}
		return documents;
	}

	/**
	 * method to list all worksheets
	 * 
	 * @return
	 */
	public List<WorkSheetVo> listAllWorksheets() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(WorkSheet.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<WorkSheet> workSheets = criteria.list();
		List<WorkSheetVo> workSheetVos = new ArrayList<WorkSheetVo>();
		for (WorkSheet sheet : workSheets) {
			workSheetVos.add(createWorkSheetVo(sheet));
		}
		return workSheetVos;
	}

	/**
	 * method to list employee worksheets
	 * 
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<WorkSheetVo> listEmployeeWorkSheets(String employeeCode)
	{
		List<WorkSheetVo> workSheetVos = new ArrayList<WorkSheetVo>();
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(WorkSheet.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		 List<WorkSheet> workSheets= criteria.add(Restrictions.eq("employee", employee)).list();
		List<WorkSheet> workSheetsNew = new ArrayList<WorkSheet>();
		 workSheetsNew = workSheets;
		 workSheets = listWorkSheetObjects();
		 Set<Employee> reportingEmployee = new HashSet<Employee>(); 
		 for (WorkSheet workSheet : workSheets) {
			 reportingEmployee.addAll(workSheet.getReportTo());
			 for (Employee employee2 : reportingEmployee) {
				if(employee2.getEmployeeCode().equalsIgnoreCase(employeeCode))
				{
					if(!workSheets.contains(workSheet))
					{
						workSheets.add(workSheet);
					}
				}
			}
		 } 
		 for(WorkSheet sheet: workSheets)
		 {
			 workSheetVos.add(createWorkSheetVo(sheet));
		 }
		return workSheetVos;
		
	}
	
	private List<WorkSheet> listWorkSheetObjects()
	{
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(WorkSheet.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		 List<WorkSheet> workSheets= criteria.list();
		 return workSheets;
	}

	/**
	 * method to find WorkSheetVo by id
	 * 
	 * @param id
	 * @return
	 */
	public WorkSheetVo findWorksheetVoById(Long id) {
		WorkSheet workSheet = (WorkSheet) this.sessionFactory.getCurrentSession().createCriteria(WorkSheet.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
		return createWorkSheetVo(workSheet);
	}

	/**
	 * method to delete documents
	 * 
	 * @param workSheetVo
	 */
	public void deleteDocument(String url) {
		WorkSheetDocuments document = (WorkSheetDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(WorkSheetDocuments.class).add(Restrictions.eq("documentPath", url)).uniqueResult();
		WorkSheet workSheet = document.getWorkSheet();
		workSheet.getWorkSheetDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(workSheet);
	}

	/**
	 * method to create worksheet vo
	 * 
	 * @param workSheet
	 * @return
	 */
	private WorkSheetVo createWorkSheetVo(WorkSheet workSheet) {
		WorkSheetVo sheetVo = new WorkSheetVo();
		sheetVo.setWorkSheetId(workSheet.getId());
		sheetVo.setCreatedBy(workSheet.getCreatedBy().getUserName());
		if (workSheet.getCreatedOn() != null)
			sheetVo.setCreatedOn(DateFormatter.convertDateToString(workSheet.getCreatedOn()));
		if (workSheet.getDate() != null)
			sheetVo.setDate(DateFormatter.convertDateToString(workSheet.getDate()));
		sheetVo.setAddInfo(workSheet.getAdditionalInfo());
		sheetVo.setDescription(workSheet.getDescription());
		if (workSheet.getWorkSheetTasks() != null) {
			sheetVo.setTasksVo(createTasksVo(workSheet));
		}
		if (workSheet.getEmployee() != null) {
			sheetVo.setEmployeeId(workSheet.getEmployee().getId());
			UserProfile profile = workSheet.getEmployee().getUserProfile();
			sheetVo.setEmployee(profile.getFirstName() + "  " + profile.getLastname());
			sheetVo.setEmployeeCode(workSheet.getEmployee().getEmployeeCode());
		}
		if (workSheet.getWorkSheetDocuments() != null) {
			sheetVo.setDocumentsVo(createDocumentVos(workSheet));
		}
		return sheetVo;
	}

	/**
	 * method to create WorksheetTasksVo tasks vo
	 * 
	 * @param sheet
	 * @return
	 */
	private Set<WorksheetTasksVo> createTasksVo(WorkSheet sheet) {
		Set<WorksheetTasksVo> tasksVos = new HashSet<WorksheetTasksVo>();
		Set<WorkSheetTasks> tasks = sheet.getWorkSheetTasks();
		for (WorkSheetTasks task : tasks) {
			WorksheetTasksVo taskVo = new WorksheetTasksVo();
			taskVo.setWorkSheetTasksId(task.getId());
			taskVo.setTask(task.getTask());
			taskVo.setStartTime(DateFormatter.convertSqlTimeToString(task.getStartTime()));
			taskVo.setEndTime(DateFormatter.convertSqlTimeToString(task.getEndTime()));
			tasksVos.add(taskVo);
		}
		return tasksVos;
	}

	/**
	 * method to create document vos
	 * 
	 * @param workSheet
	 * @return
	 */
	private Set<WorkSheetDocumentVo> createDocumentVos(WorkSheet workSheet) {
		Set<WorkSheetDocuments> documents = workSheet.getWorkSheetDocuments();
		Set<WorkSheetDocumentVo> documentVos = new HashSet<WorkSheetDocumentVo>();
		for (WorkSheetDocuments document : documents) {
			WorkSheetDocumentVo documentVo = new WorkSheetDocumentVo();
			documentVo.setDoc_id(document.getId());
			documentVo.setDocumentName(document.getDocumentName());
			documentVo.setDocumentUrl(document.getDocumentPath());
			documentVos.add(documentVo);
		}
		return documentVos;
	}

	/**
	 * method to delete work sheet by id
	 */
	public void deleteWorkSheetById(Long id) {
		WorkSheet workSheet = findAndReturnObjectById(id);
		Set<WorkSheetDocuments> documents = workSheet.getWorkSheetDocuments();
		SFTPOperation operation = new SFTPOperation();
		for (WorkSheetDocuments document : documents) {
			operation.removeFileFromSFTP(document.getDocumentPath());

		}
		this.sessionFactory.getCurrentSession().delete(workSheet);
	}

	/**
	 * Method to list workSheet withtin given date range
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<WorkSheetVo> listWorkSheetWithinDateRange(String startDate, String endDate) {

		List<WorkSheetVo> sheetVos = new ArrayList<WorkSheetVo>();
		Date start = DateFormatter.convertStringToDate(startDate);
		Date end = DateFormatter.convertStringToDate(endDate);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(WorkSheet.class);
		criteria.add(Restrictions.between("date", start, end));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<WorkSheet> workSheets = criteria.list();
		for (WorkSheet sheet : workSheets) {
			sheetVos.add(createWorkSheetVo(sheet));
		}
		return sheetVos;

	}

	/**
	 * Method to list work sheet of an employee within date range
	 * 
	 * @param empCode
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<WorkSheetVo> listWorkSheetWithEmployeeCodeWithinDateRange(String empCode, String startDate,
			String endDate) {

		List<WorkSheetVo> workSheetVos = new ArrayList<WorkSheetVo>();
		Date start = DateFormatter.convertStringToDate(startDate);
		Date end = DateFormatter.convertStringToDate(endDate);
		Employee employee = employeeDao.findAndReturnEmployeeByCode(empCode);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(WorkSheet.class);
		criteria.add(Restrictions.eq("employee", employee)).add(Restrictions.between("date", start, end));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<WorkSheet> workSheets = criteria.list();
		for (WorkSheet sheet : workSheets) {
			workSheetVos.add(createWorkSheetVo(sheet));
		}
		return workSheetVos;
	}

}
