package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.DeductionTitleDao;
import com.hrms.web.entities.BonusTitles;
import com.hrms.web.entities.DeductionTitles;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.BonusTitleVo;
import com.hrms.web.vo.DeductionTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
@Repository
public class DeductionTitlesDaoImpl extends AbstractDao implements DeductionTitleDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveDeductionTitle(DeductionTitleVo titleVo) {
		for(String title : StringSplitter.splitWithComma(titleVo.getTitle().trim())){
			if(findDeductionTitle(title.trim())!=null)
				throw new DuplicateItemException("Duplicate Entry : "+title.trim());
			DeductionTitles deductionTitles = new DeductionTitles();
			deductionTitles.setTitle(title.trim());
			this.sessionFactory.getCurrentSession().save(deductionTitles);
		}
	}

	public void updateDeductionTitle(DeductionTitleVo titleVo) {
		// TODO Auto-generated method stub
		
	}

	public void deleteDeductionTitle(Long deductionTitleId) {
		DeductionTitles deductionTitles = findDeductionTitle(deductionTitleId);
		if(deductionTitles==null)
			throw new ItemNotFoundException("Deduction Title Not Found.  ");
		else
			this.sessionFactory.getCurrentSession().delete(deductionTitles);
	}

	public void deleteDeductionTitle(String deductionTitle) {
		DeductionTitles deductionTitles = findDeductionTitle(deductionTitle);
		if(deductionTitles==null)
			throw new ItemNotFoundException("Deduction Title Not Found.  ");
		else
			this.sessionFactory.getCurrentSession().delete(deductionTitles);
	}

	public DeductionTitles findDeductionTitle(Long deductionTitleId) {
		return (DeductionTitles) this.sessionFactory.getCurrentSession().createCriteria(DeductionTitles.class)
				.add(Restrictions.eq("id", deductionTitleId)).uniqueResult();
	}

	public DeductionTitles findDeductionTitle(String deductionTitle) {
		return (DeductionTitles) this.sessionFactory.getCurrentSession().createCriteria(DeductionTitles.class)
				.add(Restrictions.eq("title", deductionTitle)).uniqueResult();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<DeductionTitleVo> listAllDeductionTitles() {
		List<DeductionTitleVo> vos = new ArrayList<DeductionTitleVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(DeductionTitles.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<DeductionTitles> titles = criteria.list();
		for(DeductionTitles title : titles){
			vos.add(createDeductionTitleVo(title));
		}
		return vos;
	}
	
	private DeductionTitleVo createDeductionTitleVo(DeductionTitles titles){
		DeductionTitleVo vo = new DeductionTitleVo();
		vo.setId(titles.getId());
		vo.setTitle(titles.getTitle());
		return vo;
	}

}
