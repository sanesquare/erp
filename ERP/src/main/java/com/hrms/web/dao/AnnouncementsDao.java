package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Announcements;
import com.hrms.web.vo.AnnouncementsVo;

/**
 * 
 * @author Shimil Babu
 * @since 10-March-2015
 *
 */
public interface AnnouncementsDao {

	/**
	 * method to save announcements
	 * 
	 * @param announcementsVo
	 * @return boolean
	 */
	public void saveOrUpdateAnnouncements(AnnouncementsVo announcementsVo);

	/**
	 * method to delete announcements
	 * 
	 * @param announcementsVo
	 * @return boolean
	 */
	public void deleteAnnouncements(Long id);
	
	/**
	 * method to delete announcement Document
	 * 
	 * @param documentId
	 */
	public void deleteAnnouncementDocument(Long documentId);

	/**
	 * method to update announcements
	 * 
	 * @param announcementsvo
	 * @return boolean
	 */
	public void updateAnnouncements(AnnouncementsVo announcementsVo);

	/**
	 * method to find announcements by announcements id
	 * 
	 * @param id
	 * @return announcementsVo
	 */
	public AnnouncementsVo findAnnouncementsById(Long id);

	/**
	 * method to find announcements by announcements title
	 * 
	 * @param title
	 * @return announcementsVo
	 */
	public AnnouncementsVo findAnnouncementsByAnnouncementsTitle(String title);

	/**
	 * method to find announcements by announcements date
	 * 
	 * @param date
	 * @return announcementsVo
	 */
	public AnnouncementsVo findAnnouncementsByDate(Date date);

	/**
	 * method to find all announcements
	 * 
	 * @return List<AnnouncementsVo>
	 */
	public List<AnnouncementsVo> findAllAnnouncements();

	/**
	 * method to find announcements object by announcement title
	 * 
	 * @return Announcements
	 */
	public Announcements findAndReturnAnnouncementsObjectByTitle(String title);

	/**
	 * method to update announcements status
	 * 
	 * @return
	 */
	// public void updateAnnouncementStatus(List<AnnouncementsVo>
	// announcementsVo);

	/**
	 * method to find all active announcements
	 * 
	 * @return List<AnnouncementsVo>
	 */
	public List<AnnouncementsVo> findAllActiveAnnouncements();

	/**
	 * method to add announcements status
	 * 
	 * @return Announcements
	 */
	public void updateAnnouncementStatus(AnnouncementsVo announcementsVo);

	/**
	 * 
	 * @param terminationVo
	 */
	public void saveAnnouncementDocuments(AnnouncementsVo announcementsVo);
}
