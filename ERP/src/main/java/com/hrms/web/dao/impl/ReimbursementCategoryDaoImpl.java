package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ReimbursementCategoryDao;
import com.hrms.web.entities.ReimbursementCategory;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.ReimbursementCategoryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Repository
public class ReimbursementCategoryDaoImpl extends AbstractDao implements ReimbursementCategoryDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveReimbursementCategory(ReimbursementCategoryVo reimbursementCategoryVo) {
		for (String reimbursementCategory : StringSplitter.splitWithComma(reimbursementCategoryVo
				.getReimbursementCategory())) {
			if (findReimbursementCategory(reimbursementCategory.trim()) != null)
				throw new DuplicateItemException("Duplicate Reimbursement Category : " + reimbursementCategory.trim());
			ReimbursementCategory category = findReimbursementCategory(reimbursementCategoryVo
					.getTempReimbursementCategory());
			if (category == null)
				category = new ReimbursementCategory();
			category.setCategory(reimbursementCategory.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(category);
		}
	}

	public void updateReimbursementCategory(ReimbursementCategoryVo reimbursementCategoryVo) {
		ReimbursementCategory reimbursementCategory = findReimbursementCategory(reimbursementCategoryVo
				.getReimbursementCategoryId());
		if (reimbursementCategory == null)
			throw new ItemNotFoundException("Reimbursement category not exist");
		reimbursementCategory.setCategory(reimbursementCategoryVo.getReimbursementCategory());
		this.sessionFactory.getCurrentSession().merge(reimbursementCategory);
	}

	public void deleteReimbursementCategory(Long reimbursementCategoryId) {
		ReimbursementCategory reimbursementCategory = findReimbursementCategory(reimbursementCategoryId);
		if (reimbursementCategory == null)
			throw new ItemNotFoundException("Reimbursement category not exist");
		this.sessionFactory.getCurrentSession().delete(reimbursementCategory);
	}

	public ReimbursementCategory findReimbursementCategory(Long reimbursementCategoryId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReimbursementCategory.class)
				.add(Restrictions.eq("id", reimbursementCategoryId));
		if (criteria.uniqueResult() == null)
			return null;
		return (ReimbursementCategory) criteria.uniqueResult();
	}

	public ReimbursementCategory findReimbursementCategory(String reimbursementCategory) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReimbursementCategory.class)
				.add(Restrictions.eq("category", reimbursementCategory));
		if (criteria.uniqueResult() == null)
			return null;
		return (ReimbursementCategory) criteria.uniqueResult();
	}

	/**
	 * method to create reimbursementcategoryVo object
	 * 
	 * @param category
	 * @return categoryVo
	 */
	private ReimbursementCategoryVo createReimbursementCategoryVo(ReimbursementCategory category) {
		ReimbursementCategoryVo categoryVo = new ReimbursementCategoryVo();
		categoryVo.setReimbursementCategory(category.getCategory());
		categoryVo.setReimbursementCategoryId(category.getId());
		return categoryVo;
	}

	public ReimbursementCategoryVo findReimbursementCategoryById(Long reimbursementCategoryId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReimbursementCategory.class)
				.add(Restrictions.eq("id", reimbursementCategoryId));
		if (criteria.uniqueResult() == null)
			return null;
		return createReimbursementCategoryVo((ReimbursementCategory) criteria.uniqueResult());
	}

	public ReimbursementCategoryVo findReimbursementCategoryByCategory(String reimbursementCategory) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReimbursementCategory.class)
				.add(Restrictions.eq("category", reimbursementCategory));
		if (criteria.uniqueResult() == null)
			return null;
		return createReimbursementCategoryVo((ReimbursementCategory) criteria.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<ReimbursementCategoryVo> findAllReimbursementCategory() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReimbursementCategory.class);
		List<ReimbursementCategoryVo> reimbursementCategoryVos = new ArrayList<ReimbursementCategoryVo>();
		List<ReimbursementCategory> reimbursementCategories = criteria.list();
		for (ReimbursementCategory reimbursementCategory : reimbursementCategories)
			reimbursementCategoryVos.add(createReimbursementCategoryVo(reimbursementCategory));
		return reimbursementCategoryVos;
	}

}
