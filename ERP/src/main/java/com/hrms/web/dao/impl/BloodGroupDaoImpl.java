package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BloodGroupDao;
import com.hrms.web.entities.BloodGroup;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.BloodGroupVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Repository
public class BloodGroupDaoImpl extends AbstractDao implements BloodGroupDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveBloodGroup(BloodGroupVo bloodGroupVo) {
		for (String group : StringSplitter.splitWithComma(bloodGroupVo.getGroupName())) {
			if (findBloodGroup(group.trim()) != null)
				throw new DuplicateItemException("Duplicate Blood Group : " + group.trim());
			BloodGroup bloodGroup = new BloodGroup();
			bloodGroup.setGroup(group.trim());
			this.sessionFactory.getCurrentSession().save(bloodGroup);
			userActivitiesService.saveUserActivity("Added new Blood Group "+group.trim());
		}
	}

	public void updateBloodGroup(BloodGroupVo bloodGroupVo) {
		BloodGroup bloodGroup = findBloodGroup(bloodGroupVo.getBloodGroupId());
		if (bloodGroup == null)
			throw new ItemNotFoundException("Blood Group Details Not Found.");
		bloodGroup.setGroup(bloodGroupVo.getGroupName());
		this.sessionFactory.getCurrentSession().merge(bloodGroup);
		userActivitiesService.saveUserActivity("Updated Blood Group as "+bloodGroupVo.getGroupName());
	}

	public void deleteBloodGroup(Long groupId) {
		BloodGroup bloodGroup = findBloodGroup(groupId);
		if (bloodGroup == null)
			throw new ItemNotFoundException("Blood Group Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(bloodGroup);
		userActivitiesService.saveUserActivity("Deleted Blood Group");
	}

	public BloodGroup findBloodGroup(Long groupId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BloodGroup.class)
				.add(Restrictions.eq("id", groupId));
		if (criteria.uniqueResult() == null)
			return null;
		return (BloodGroup) criteria.uniqueResult();
	}

	public BloodGroup findBloodGroup(String groupName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BloodGroup.class)
				.add(Restrictions.eq("group", groupName));
		if (criteria.uniqueResult() == null)
			return null;
		return (BloodGroup) criteria.uniqueResult();
	}

	/**
	 * method to create BloodGroupVo object
	 * 
	 * @param bloodGroup
	 * @return
	 */
	private BloodGroupVo createBloodGroupVo(BloodGroup bloodGroup) {
		BloodGroupVo bloodGroupVo = new BloodGroupVo();
		bloodGroupVo.setBloodGroupId(bloodGroup.getId());
		bloodGroupVo.setGroupName(bloodGroup.getGroup());
		return bloodGroupVo;
	}

	public BloodGroupVo findBloodGroupById(Long groupId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BloodGroup.class)
				.add(Restrictions.eq("id", groupId));
		if (criteria.uniqueResult() == null)
			return null;
		return createBloodGroupVo((BloodGroup) criteria.uniqueResult());
	}

	public BloodGroupVo findBloodGroupByGroup(String groupName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BloodGroup.class)
				.add(Restrictions.eq("group", groupName));
		if (criteria.uniqueResult() == null)
			return null;
		return createBloodGroupVo((BloodGroup) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<BloodGroupVo> findAllBloodGroup() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BloodGroup.class);
		List<BloodGroupVo> bloodGroupVos = new ArrayList<BloodGroupVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<BloodGroup> bloodGroups = criteria.list();
		for (BloodGroup bloodGroup : bloodGroups)
			bloodGroupVos.add(createBloodGroupVo(bloodGroup));
		return bloodGroupVos;
	}

}
