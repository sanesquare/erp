package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.PerformanceEvaluation;
import com.hrms.web.vo.PerformanceEvaluationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 24-March-2015
 *
 */
public interface PerformanceEvaluationDao {

	/**
	 * method to save PerformanceEvaluation
	 * 
	 * @param evaluationVo
	 */
	public void savePerformanceEvaluation(PerformanceEvaluationVo evaluationVo);

	/**
	 * method to save PerformanceEvaluation documents
	 * 
	 * @param evaluationVo
	 */
	public void savePerformanceEvaluationDocuments(PerformanceEvaluationVo evaluationVo);

	/**
	 * method to save PerformanceEvaluation questions
	 * 
	 * @param evaluationVo
	 */
	public void savePerformanceEvaluationQuestions(PerformanceEvaluationVo evaluationVo);

	/**
	 * method to update PerformanceEvaluation
	 * 
	 * @param evaluationVo
	 */
	public void updatePerformanceEvaluation(PerformanceEvaluationVo evaluationVo);

	/**
	 * method to delete PerformanceEvaluation
	 * 
	 * @param evaluationId
	 */
	public void deletePerformanceEvaluation(Long evaluationId);

	/**
	 * method to delete PerformanceEvaluation document
	 * 
	 * @param documentId
	 */
	public void deletePerformanceEvaluationDocument(Long documentId);

	/**
	 * method to delete PerformanceEvaluation question
	 * 
	 * @param documentId
	 */
	public void deletePerformanceEvaluationQuestion(Long documentId);

	/**
	 * method to find PerformanceEvaluation by id
	 * 
	 * @param evaluationId
	 * @return PerformanceEvaluation
	 */
	public PerformanceEvaluation findPerformanceEvaluation(Long evaluationId);

	/**
	 * method to find PerformanceEvaluation by start date
	 * 
	 * @param startDate
	 * @return List<PerformanceEvaluation>
	 */
	public List<PerformanceEvaluation> findPerformanceEvaluationByStartDate(Date startDate);

	/**
	 * method to find PerformanceEvaluation by end date
	 * 
	 * @param endDate
	 * @return List<PerformanceEvaluation>
	 */
	public List<PerformanceEvaluation> findPerformanceEvaluationByEndDate(Date endDate);

	/**
	 * method to find PerformanceEvaluation between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return List<PerformanceEvaluation>
	 */
	public List<PerformanceEvaluation> findPerformanceEvaluationBetweenDates(Date startDate, Date endDate);

	/**
	 * method to find PerformanceEvaluation by id
	 * 
	 * @param evaluationId
	 * @return List<PerformanceEvaluationVo>
	 */
	public PerformanceEvaluationVo findPerformanceEvaluationById(Long evaluationId);

	/**
	 * method to find PerformanceEvaluation by start date
	 * 
	 * @param startDate
	 * @return List<PerformanceEvaluationVo>
	 */
	public List<PerformanceEvaluationVo> findPerformanceEvaluationVoByStartDate(Date startDate);

	/**
	 * method to find PerformanceEvaluation by end date
	 * 
	 * @param endDate
	 * @return List<PerformanceEvaluationVo>
	 */
	public List<PerformanceEvaluationVo> findPerformanceEvaluationVoByEndDate(Date endDate);

	/**
	 * method to find PerformanceEvaluation between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return List<PerformanceEvaluationVo>
	 */
	public List<PerformanceEvaluationVo> findPerformanceEvaluationVoBetweenDates(Date startDate, Date endDate);

	/**
	 * method to find all PerformanceEvaluation
	 * 
	 * @return List<PerformanceEvaluationVo>
	 */
	public List<PerformanceEvaluationVo> findAllPerformanceEvaluation();
}
