package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Payslip;
import com.hrms.web.vo.PaysSlipVo;

/**
 * 
 * @author Shamsheer
 * @since 29-April-2015
 */
public interface PayslipDao {

	/**
	 * method to get details of payslip items
	 * 
	 * @param employeeCode
	 * @return
	 */
	public List<PaysSlipVo> getPayslipDetailsByEmployee(String employeeCode, String dateFrom, String dateTo);

	/**
	 * method to save payslip
	 * 
	 * @param paysSlipVo
	 * @return
	 */
	public Long savePayslip(PaysSlipVo paysSlipVo);

	/**
	 * method to update payslip
	 * 
	 * @param paysSlipVo
	 * @return
	 */
	public void updatePayslip(PaysSlipVo paysSlipVo);

	/**
	 * method to get payslip
	 * 
	 * @param id
	 * @return
	 */
	public Payslip getPayslip(Long id);

	/**
	 * method to list all payslips
	 * 
	 * @return
	 */
	public List<PaysSlipVo> listAllPayslips();

	/**
	 * method to delete payslip
	 * 
	 * @param id
	 */
	public void deletePayslip(Long id);

	/**
	 * method to get payslip by id
	 * 
	 * @param id
	 * @return
	 */
	public PaysSlipVo getPayslipById(Long id);

	/**
	 * method to find employees payslips between dates
	 * 
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PaysSlipVo> findPayslipOfEmployeeBetweenDates(Long employeeId, Date startDate, Date endDate);

	/**
	 * method to find all pay slip by employee
	 * 
	 * @return
	 */
	public List<PaysSlipVo> findAllPayslipsByEmployee(String employeeCode);
	
	/**
	 * method to update payslip status
	 * @param paysSlipVo
	 */
	public void updatePayslipStatus(PaysSlipVo paysSlipVo);
}
