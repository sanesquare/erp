package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.TrainingEventTypeDao;
import com.hrms.web.entities.TrainingEventType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.TrainingEventTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Repository
public class TrainingEventTypeDaoImpl extends AbstractDao implements TrainingEventTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveTrainingEventType(TrainingEventTypeVo trainingEventTypeVo) {
		for (String trainingEventType : StringSplitter.splitWithComma(trainingEventTypeVo.getTrainingEventType())) {
			if (findTrainingEventType(trainingEventType.trim()) != null)
				throw new DuplicateItemException("Duplicate Training Event Type : " + trainingEventType.trim());
			TrainingEventType eventType = new TrainingEventType();
			eventType.setType(trainingEventType.trim());
			this.sessionFactory.getCurrentSession().save(eventType);
		}
	}

	public void updateTrainingEventType(TrainingEventTypeVo trainingEventTypeVo) {
		TrainingEventType eventType = findTrainingEventType(trainingEventTypeVo.getTrainingEventTypeId());
		if (eventType == null)
			throw new ItemNotFoundException("Training Event Type Not Exist");
		eventType.setType(trainingEventTypeVo.getTrainingEventType());
		this.sessionFactory.getCurrentSession().merge(eventType);
	}

	public void deleteTrainingEventType(Long trainingEventTypeId) {
		TrainingEventType eventType = findTrainingEventType(trainingEventTypeId);
		if (eventType == null)
			throw new ItemNotFoundException("Training Event Type Not Exist");
		this.sessionFactory.getCurrentSession().delete(eventType);
	}

	public TrainingEventType findTrainingEventType(Long trainingEventTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TrainingEventType.class)
				.add(Restrictions.eq("id", trainingEventTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (TrainingEventType) criteria.uniqueResult();
	}

	public TrainingEventType findTrainingEventType(String trainingEventType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TrainingEventType.class)
				.add(Restrictions.eq("type", trainingEventType));
		if (criteria.uniqueResult() == null)
			return null;
		return (TrainingEventType) criteria.uniqueResult();
	}

	/**
	 * method to create eventTypeVo object
	 * 
	 * @param eventType
	 * @return eventTypeVo
	 */
	private TrainingEventTypeVo createTrainingEventTypeVo(TrainingEventType eventType) {
		TrainingEventTypeVo eventTypeVo = new TrainingEventTypeVo();
		eventTypeVo.setTrainingEventType(eventType.getType());
		eventTypeVo.setTrainingEventTypeId(eventType.getId());
		return eventTypeVo;
	}

	public TrainingEventTypeVo findTrainingEventTypeById(Long trainingEventTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TrainingEventType.class)
				.add(Restrictions.eq("id", trainingEventTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createTrainingEventTypeVo((TrainingEventType) criteria.uniqueResult());
	}

	public TrainingEventTypeVo findTrainingEventTypeBytype(String trainingEventType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TrainingEventType.class)
				.add(Restrictions.eq("type", trainingEventType));
		if (criteria.uniqueResult() == null)
			return null;
		return createTrainingEventTypeVo((TrainingEventType) criteria.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<TrainingEventTypeVo> findAllTrainingEventType() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TrainingEventType.class);
		List<TrainingEventTypeVo> eventTypeVos = new ArrayList<TrainingEventTypeVo>();
		List<TrainingEventType> eventTypes = criteria.list();
		for (TrainingEventType eventType : eventTypes)
			eventTypeVos.add(createTrainingEventTypeVo(eventType));
		return eventTypeVos;
	}

}
