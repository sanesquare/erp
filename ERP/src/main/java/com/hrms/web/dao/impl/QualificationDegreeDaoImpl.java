package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.QualificationDegreeDao;
import com.hrms.web.entities.QualificationDegree;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.QualificationDegreeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
@Repository
public class QualificationDegreeDaoImpl extends AbstractDao implements QualificationDegreeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	/**
	 * method to save new qualification degree
	 * 
	 * @param degreeVo
	 */
	public void saveQualificationDegree(QualificationDegreeVo degreeVo) {
		logger.info("Inside Qualification Degree DAO >>> saveQualificationDegree......");
		for (String degreeName : StringSplitter.splitWithComma(degreeVo.getDegreeName())) {
			if (findQualification(degreeName.trim()) != null)
				throw new DuplicateItemException("Duplicate Degree : " + degreeName.trim());
			QualificationDegree degree = findQualification(degreeVo.getTempQualification());
			if (degree == null)
				degree = new QualificationDegree();
			degree.setDegree(degreeName.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(degree);
		}
	}

	/**
	 * method to update qualification information
	 * 
	 * @param degreeVo
	 */
	public void updateQualificationDegree(QualificationDegreeVo degreeVo) {
		logger.info("Inside Qualification Degree DAO >>> updateQualificationDegree......");
		QualificationDegree degree = findQualification(degreeVo.getDegreeName());
		if (degree == null)
			throw new ItemNotFoundException("Qualification Degree Not exist.");
		degree.setDegree(degreeVo.getDegreeName());
		this.sessionFactory.getCurrentSession().merge(degree);
	}

	/**
	 * method to delete qualification information
	 * 
	 * @param degreeId
	 */
	public void deleteQualificationDegree(Long degreeId) {
		logger.info("Inside Qualification Degree DAO >>> deleteQualificationDegree......");
		QualificationDegree degree = findQualification(degreeId);
		if (degree == null)
			throw new ItemNotFoundException("Qualification Degree Not exist.");
		this.sessionFactory.getCurrentSession().delete(degree);
	}

	/**
	 * method to find qualification by id
	 * 
	 * @param degreeId
	 * @return qualificationdegree
	 */
	public QualificationDegree findQualification(Long degreeId) {
		logger.info("Inside Qualification Degree DAO >>> findQualification......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(QualificationDegree.class)
				.add(Restrictions.eq("id", degreeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (QualificationDegree) criteria.uniqueResult();
	}

	/**
	 * method to create qualification degree vo
	 * 
	 * @param degree
	 * @return degreeVo
	 */
	private QualificationDegreeVo createQualificationDegreeVo(QualificationDegree degree) {
		QualificationDegreeVo degreeVo = new QualificationDegreeVo();
		degreeVo.setDegreeId(degree.getId());
		degreeVo.setDegreeName(degree.getDegree());
		return degreeVo;
	}

	/**
	 * method to find qualification information
	 * 
	 * @param degreeId
	 * @return qualificationdegreevo
	 */
	public QualificationDegreeVo findQialificationDegreeById(Long degreeId) {
		logger.info("Inside Qualification Degree DAO >>> findQialificationDegreeById......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(QualificationDegree.class)
				.add(Restrictions.eq("id", degreeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createQualificationDegreeVo((QualificationDegree) criteria.uniqueResult());
	}

	/**
	 * method to find qualification by name
	 * 
	 * @param degreeName
	 * @return qualificationdegree
	 */
	public QualificationDegree findQualification(String degreeName) {
		logger.info("Inside Qualification Degree DAO >>> findQualification......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(QualificationDegree.class)
				.add(Restrictions.eq("degree", degreeName));
		if (criteria.uniqueResult() == null)
			return null;
		return (QualificationDegree) criteria.uniqueResult();
	}

	/**
	 * method to find qualification by name
	 * 
	 * @param degreeName
	 * @return qualificationdegreevo
	 */
	public QualificationDegreeVo findQualificationDegreeByName(String degreeName) {
		logger.info("Inside Qualification Degree DAO >>> findQualificationDegreeByName......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(QualificationDegree.class)
				.add(Restrictions.eq("degree", degreeName));
		if (criteria.uniqueResult() == null)
			return null;
		return createQualificationDegreeVo((QualificationDegree) criteria.uniqueResult());
	}

	/**
	 * method to find all qualification
	 * 
	 * @return List<QualificationDegree>
	 */
	@SuppressWarnings("unchecked")
	public List<QualificationDegreeVo> findAllQualificationDegree() {
		logger.info("Inside Qualification Degree DAO >>> findAllQualificationDegree......");
		List<QualificationDegreeVo> degreeVos = new ArrayList<QualificationDegreeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(QualificationDegree.class);

		List<QualificationDegree> degrees = criteria.list();
		for (QualificationDegree degree : degrees)
			degreeVos.add(createQualificationDegreeVo(degree));
		return degreeVos;
	}
}
