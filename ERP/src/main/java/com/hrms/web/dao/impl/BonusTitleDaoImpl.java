package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BonusTitleDao;
import com.hrms.web.entities.BonusTitles;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.BonusTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
@Repository
public class BonusTitleDaoImpl extends AbstractDao implements BonusTitleDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveBonusTitle(BonusTitleVo titleVo) {
		for(String title : StringSplitter.splitWithComma(titleVo.getTitle().trim())){
			if(findBonusTitle(title.trim())!=null)
				throw new DuplicateItemException("Duplicate Entry : "+title.trim());
			BonusTitles bonusTitles = new BonusTitles();
			bonusTitles.setTitle(title.trim());
			this.sessionFactory.getCurrentSession().save(bonusTitles);
			userActivitiesService.saveUserActivity("Added new Bonus Title "+title.trim());
		}
	}

	public void updateBonusTitle(BonusTitleVo titleVo) {
		BonusTitles bonusTitles = findBonusTitle(titleVo.getId());
		if(bonusTitles == null)
			throw new ItemNotFoundException("Bonus Title Not Exist.");
		bonusTitles.setTitle(titleVo.getTitle());
		this.sessionFactory.getCurrentSession().merge(bonusTitles);
		userActivitiesService.saveUserActivity("Updated Bonus Title as "+titleVo.getTitle());
	}

	public void deleteBonusTitle(Long bonusTitleId) {
		BonusTitles bonusTitles = findBonusTitle(bonusTitleId);
		if(bonusTitles==null)
			throw new ItemNotFoundException("Bonus Title Not Found.  ");
		else{
			this.sessionFactory.getCurrentSession().delete(bonusTitles);
		    userActivitiesService.saveUserActivity("Deleted Bonus Date");
		}
	}

	public void deleteBonusTitle(String bonusTitle) {
		BonusTitles bonusTitles = findBonusTitle(bonusTitle);
		if(bonusTitles==null)
			throw new ItemNotFoundException("Bonus Title Not Found : "+bonusTitle);
		else
			this.sessionFactory.getCurrentSession().delete(bonusTitles);
	}

	public BonusTitles findBonusTitle(Long bonusTitleId) {
		return (BonusTitles) this.sessionFactory.getCurrentSession().createCriteria(BonusTitles.class)
				.add(Restrictions.eq("id", bonusTitleId)).uniqueResult();
	}

	public BonusTitles findBonusTitle(String bonusTitle) {
		return (BonusTitles) this.sessionFactory.getCurrentSession().createCriteria(BonusTitles.class)
				.add(Restrictions.eq("title", bonusTitle)).uniqueResult();
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<BonusTitleVo> listAllBonusTitles() {
		List<BonusTitleVo> vos = new ArrayList<BonusTitleVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BonusTitles.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<BonusTitles> titles = criteria.list();
		for(BonusTitles title : titles){
			vos.add(createBonusTitleVo(title));
		}
		return vos;
	}

	/**
	 * method to create bonus title vo
	 * @param titles
	 * @return
	 */
	private BonusTitleVo createBonusTitleVo(BonusTitles titles){
		BonusTitleVo vo = new BonusTitleVo();
		vo.setId(titles.getId());
		vo.setTitle(titles.getTitle());
		return vo;
	}
}
