package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CountryDao;
import com.hrms.web.entities.Country;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.CountryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
@Repository
public class CountryDaoImpl extends AbstractDao implements CountryDao {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private UserActivitiesService userActivitiesService;

	/**
	 * method to save new Country
	 * 
	 * @param countryVo
	 * @return
	 */
	public void saveCountry(CountryVo countryVo) {
		logger.info("Inside CountryDao >>> saveCountry......");

		for (String countryName : StringSplitter.splitWithComma(countryVo.getName())) {
			if (findCountrybyName(countryName.trim()) != null)
				throw new DuplicateItemException("Duplicate Value : " + countryName.trim());
			Country country = findCountry(countryVo.getTempName());
			if (country == null)
				country = new Country();
			country.setName(countryName.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(country);

		}
	}

	/**
	 * method to update country information
	 * 
	 * @param countryVo
	 * @return
	 */
	public void updateCountry(CountryVo countryVo) {
		logger.info("Inside CountryDao >>> updateCountry......");

		Country country = findCountry(countryVo.getCountryId());
		if (country == null)
			throw new ItemNotFoundException("Country Not Exixting >>>> Country DAO");

		country.setName(countryVo.getName());
		this.sessionFactory.getCurrentSession().merge(country);
	}

	/**
	 * method to delete country information
	 * 
	 * @param countryId
	 * @return
	 */
	public void deleteCountry(Long countryId) {
		logger.info("Inside CountryDao >>> deleteCountry......");
		Country country = findCountry(countryId);
		if (country == null)
			throw new ItemNotFoundException("Country Not Exixting >>>> Country DAO");

		this.sessionFactory.getCurrentSession().delete(country);
	}

	/**
	 * methos to find country by id and return country object
	 * 
	 * @param countryId
	 * @return Country
	 */
	public Country findCountry(Long countryId) {
		logger.info("Inside CountryDao >>> findCountry......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Country.class)
				.add(Restrictions.eq("id", countryId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Country) criteria.uniqueResult();
	}

	/**
	 * method to find country by id and return countryvo object
	 * 
	 * @param countryId
	 * @return CountryVo
	 */
	public CountryVo findCountryById(Long countryId) {
		logger.info("Inside CountryDao >>> findCountryById......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Country.class)
				.add(Restrictions.eq("id", countryId));
		if (criteria.uniqueResult() == null)
			return null;
		return createCountryVo((Country) criteria.uniqueResult());
	}

	/**
	 * method to create country Vo
	 * 
	 * @param country
	 * @return countryVo
	 */
	private CountryVo createCountryVo(Country country) {
		logger.info("Inside CountryDao >>> createCountryVo......");
		CountryVo countryVo = new CountryVo();
		countryVo.setCountryId(country.getId());
		countryVo.setName(country.getName());
		return countryVo;
	}

	/**
	 * method to find country by name
	 * 
	 * @param countryName
	 * @return countryVo
	 */
	public CountryVo findCountrybyName(String countryName) {
		logger.info("Inside CountryDao >>> findCountrybyName......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Country.class)
				.add(Restrictions.eq("name", countryName));
		if (criteria.uniqueResult() == null)
			return null;
		return createCountryVo((Country) criteria.uniqueResult());
	}

	/**
	 * method to find all country
	 * 
	 * @return List<CountryVo>
	 */
	@SuppressWarnings("unchecked")
	public List<CountryVo> findAllCountry() {
		logger.info("Inside CountryDao >>> findCountrybyName......");
		List<CountryVo> countryVos = new ArrayList<CountryVo>();

		List<Country> countries = this.sessionFactory.getCurrentSession().createCriteria(Country.class).list();
		for (Country country : countries) {
			countryVos.add(createCountryVo(country));
		}
		return countryVos;
	}

	/**
	 * method to find country by name and return country object
	 * 
	 * @param countryName
	 * @return country
	 */
	public Country findCountry(String countryName) {
		logger.info("Inside CountryDao >>> findCountrybyName......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Country.class)
				.add(Restrictions.eq("name", countryName));
		if (criteria.uniqueResult() == null)
			return null;
		return (Country) criteria.uniqueResult();
	}

}
