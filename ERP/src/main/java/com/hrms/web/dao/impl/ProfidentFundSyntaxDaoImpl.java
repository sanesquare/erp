package com.hrms.web.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ProfidentFundSyntaxDao;
import com.hrms.web.entities.ProfidentFundSyntax;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.ProfidentFundSyntaxVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Repository
public class ProfidentFundSyntaxDaoImpl extends AbstractDao implements ProfidentFundSyntaxDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveOrUpdateProfidentFundSyntax(ProfidentFundSyntaxVo profidentFundSyntaxVo) {
		ProfidentFundSyntax profidentFundSyntax = findProfidentFundSyntax(profidentFundSyntaxVo.getPfSyntaxId());
		if (profidentFundSyntax == null)
			profidentFundSyntax = new ProfidentFundSyntax();
		profidentFundSyntax.setEmployeeSyntax(profidentFundSyntaxVo.getEmployeeSyntax());
		profidentFundSyntax.setEmployerSyntax(profidentFundSyntaxVo.getEmployerSyntax());
		this.sessionFactory.getCurrentSession().saveOrUpdate(profidentFundSyntax);
	}

	public void deleteProfidentFundSyntax(Long pfSyntaxId) {
		ProfidentFundSyntax profidentFundSyntax = findProfidentFundSyntax(pfSyntaxId);
		if (profidentFundSyntax == null)
			throw new ItemNotFoundException("Profident Fund Syntax Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(profidentFundSyntax);
	}

	public ProfidentFundSyntax findProfidentFundSyntax(Long pfSyntaxId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfidentFundSyntax.class)
				.add(Restrictions.eq("id", pfSyntaxId));
		if (criteria.uniqueResult() == null)
			return null;
		return (ProfidentFundSyntax) criteria.uniqueResult();
	}

	/**
	 * method to create ProfidentFundSyntaxVo object
	 * 
	 * @param profidentFundSyntax
	 * @return profidentFundSyntaxVo
	 */
	private ProfidentFundSyntaxVo createProfidentFundSyntaxVo(ProfidentFundSyntax profidentFundSyntax) {
		ProfidentFundSyntaxVo profidentFundSyntaxVo = new ProfidentFundSyntaxVo();
		profidentFundSyntaxVo.setPfSyntaxId(profidentFundSyntax.getId());
		profidentFundSyntaxVo.setEmployeeSyntax(profidentFundSyntax.getEmployeeSyntax());
		profidentFundSyntaxVo.setEmployerSyntax(profidentFundSyntax.getEmployerSyntax());
		return profidentFundSyntaxVo;
	}

	public ProfidentFundSyntaxVo findProfidentFundSyntaxById(Long pfSyntaxId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfidentFundSyntax.class)
				.add(Restrictions.eq("id", pfSyntaxId));
		if (criteria.uniqueResult() == null)
			return null;
		return createProfidentFundSyntaxVo((ProfidentFundSyntax) criteria.uniqueResult());
	}

	public ProfidentFundSyntaxVo findProfidentFundSyntax() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfidentFundSyntax.class);
		if (criteria.uniqueResult() == null)
			return null;
		return createProfidentFundSyntaxVo((ProfidentFundSyntax) criteria.uniqueResult());
	}

}
