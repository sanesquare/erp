package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeStatusDao;
import com.hrms.web.entities.EmployeeStatus;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.EmployeeStatusVo;

/**
 * 
 * @author shamsheer, Jithin Mohan
 * @since 21-March-2015
 */
@Repository
public class EmployeeStatsuDaoImpl extends AbstractDao implements EmployeeStatusDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveStatus(EmployeeStatusVo statusVo) {
		for (String status : StringSplitter.splitWithComma(statusVo.getStatus())) {
			if (findStatus(status.trim()) != null)
				throw new DuplicateItemException("Duplicate Employee Status : " + status.trim());
			EmployeeStatus employeeStatus = findStatus(statusVo.getTempEmployeeStatus());
			if (employeeStatus == null)
				employeeStatus = new EmployeeStatus();
			employeeStatus.setName(status.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(employeeStatus);
			userActivitiesService.saveUserActivity("Added new Employee Status " + statusVo.getStatus());
		}
	}

	public void updateStatus(EmployeeStatusVo statusVo) {
		EmployeeStatus employeeStatus = findStatus(statusVo.getId());
		if (employeeStatus == null)
			throw new ItemNotFoundException("Employee Status Details Not Found.");
		employeeStatus.setName(statusVo.getStatus());
		this.sessionFactory.getCurrentSession().merge(employeeStatus);
		userActivitiesService.saveUserActivity("Updated Employee Status as " + statusVo.getStatus());
	}

	public void deleteStatus(Long id) {
		EmployeeStatus employeeStatus = findStatus(id);
		if (employeeStatus == null)
			throw new ItemNotFoundException("Employee Status Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(employeeStatus);
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeStatusVo> listAllStatus() {
		List<EmployeeStatusVo> employeeStatusVos = new ArrayList<EmployeeStatusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeStatus.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeStatus> employeeStatus = criteria.list();
		for (EmployeeStatus status : employeeStatus) {
			employeeStatusVos.add(createStatusVo(status));
		}
		return employeeStatusVos;
	}

	/**
	 * method to create EmployeeStatusVo object
	 * 
	 * @param employeeStatus
	 * @return
	 */
	private EmployeeStatusVo createStatusVo(EmployeeStatus employeeStatus) {
		EmployeeStatusVo employeeStatusVo = new EmployeeStatusVo();
		employeeStatusVo.setId(employeeStatus.getId());
		employeeStatusVo.setStatus(employeeStatus.getName());
		return employeeStatusVo;
	}

	public EmployeeStatusVo findStatusById(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeStatus.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return createStatusVo((EmployeeStatus) criteria.uniqueResult());
	}

	public EmployeeStatusVo findStatusByName(String name) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeStatus.class)
				.add(Restrictions.eq("name", name));
		if (criteria.uniqueResult() == null)
			return null;
		return createStatusVo((EmployeeStatus) criteria.uniqueResult());
	}

	public EmployeeStatus findStatus(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeStatus.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeStatus) criteria.uniqueResult();
	}

	public EmployeeStatus findStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeStatus.class)
				.add(Restrictions.eq("name", status));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeStatus) criteria.uniqueResult();
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeStatusVo> findAllEmployeeStatus() {
		List<EmployeeStatusVo> employeeStatusVos = new ArrayList<EmployeeStatusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeStatus.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeStatus> employeeStatus = criteria.list();
		for (EmployeeStatus status : employeeStatus)
			employeeStatusVos.add(createStatusVo(status));
		return employeeStatusVos;
	}

}
