package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CountryDao;
import com.hrms.web.dao.OrganisationDao;
import com.hrms.web.entities.Organisation;
import com.hrms.web.entities.OrganisationContactPerson;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.OrganisationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-March-2015
 *
 */
@Repository
public class OrganisationDaoImpl extends AbstractDao implements OrganisationDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private CountryDao countryDao;

	public void saveOrganisation(OrganisationVo organisationVo) {
		/*
		 * if (findOrganisation(organisationVo.getOrganisationCode()) != null)
		 * throw new DuplicateItemException("Duplicate Organisation Code : " +
		 * organisationVo.getOrganisationCode()); if
		 * (findOrganisationByName(organisationVo.getOrganisationName()) !=
		 * null) throw new
		 * DuplicateItemException("Duplicate Organisation Name : " +
		 * organisationVo.getOrganisationName()); if
		 * (findOrganisationByUrt(organisationVo.getOrganisationUrt()) != null)
		 * throw new DuplicateItemException("Duplicate Organisation Urt : " +
		 * organisationVo.getOrganisationUrt());
		 */

		Organisation organisation = findOrganisation(organisationVo.getOrganisationId());
		if (organisation == null)
			organisation = new Organisation();
		organisation.setCode(organisationVo.getOrganisationCode());
		organisation.setUrt(organisationVo.getOrganisationUrt());
		organisation.setName(organisationVo.getOrganisationName());
		organisation.setYear(organisationVo.getOrganisationYear());
		organisation.setFiscalMonth(organisationVo.getFiscalMonth());
		organisation.setFiscalDate(organisationVo.getFiscalDay());
		organisation.setContactPerson(setOrganisationContact(organisationVo, organisation));
		this.sessionFactory.getCurrentSession().saveOrUpdate(organisation);
	}

	/**
	 * method to set OrganisationContactPerson
	 * 
	 * @param organisationVo
	 * @return organisationContactPersons
	 */
	private OrganisationContactPerson setOrganisationContact(OrganisationVo organisationVo, Organisation organisation) {
		// /Set<OrganisationContactPerson> organisationContactPersons = new
		// HashSet<OrganisationContactPerson>();
		OrganisationContactPerson contactPerson = organisation.getContactPerson();
		if (contactPerson == null)
			contactPerson = new OrganisationContactPerson();
		contactPerson.setFirstName(organisationVo.getContactFirstName());
		contactPerson.setLastName(organisationVo.getContactLastName());
		contactPerson.setEmail(organisationVo.getContactEmail());
		contactPerson.setContactNumber(organisationVo.getContactNumber());
		contactPerson.setCountry(countryDao.findCountry(organisationVo.getContactCountry()));
		contactPerson.setOrganisation(organisation);
		// organisationContactPersons.add(contactPerson);
		return contactPerson;
	}

	public void updateOrganisation(OrganisationVo organisationVo) {
		Organisation organisation = findOrganisation(organisationVo.getOrganisationId());
		if (organisation == null)
			throw new ItemNotFoundException("Organisation Not Found");

		organisation.setCode(organisationVo.getOrganisationCode());
		organisation.setUrt(organisationVo.getOrganisationUrt());
		organisation.setName(organisationVo.getOrganisationName());
		organisation.setYear(organisationVo.getOrganisationYear());
		organisation.setFiscalMonth(organisationVo.getFiscalMonth());
		organisation.setFiscalDate(organisationVo.getFiscalDay());
		organisation.setContactPerson(setOrganisationContact(organisationVo, organisation));
		this.sessionFactory.getCurrentSession().merge(organisation);
	}

	public void deleteOrganisation(Long organisationId) {
		Organisation organisation = findOrganisation(organisationId);
		if (organisation == null)
			throw new ItemNotFoundException("Organisation Not Found");
		this.sessionFactory.getCurrentSession().delete(organisation);
	}

	public Organisation findOrganisation(Long organisationId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organisation.class)
				.add(Restrictions.eq("id", organisationId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Organisation) criteria.uniqueResult();
	}

	public Organisation findOrganisation(String organisationCode) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organisation.class)
				.add(Restrictions.eq("code", organisationCode));
		if (criteria.uniqueResult() == null)
			return null;
		return (Organisation) criteria.uniqueResult();
	}

	public Organisation findOrganisationByUrt(String organisationUrt) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organisation.class)
				.add(Restrictions.eq("urt", organisationUrt));
		if (criteria.uniqueResult() == null)
			return null;
		return (Organisation) criteria.uniqueResult();
	}

	public Organisation findOrganisationByName(String organisationName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organisation.class)
				.add(Restrictions.eq("name", organisationName));
		if (criteria.uniqueResult() == null)
			return null;
		return (Organisation) criteria.uniqueResult();
	}

	/**
	 * method to create
	 * 
	 * @param organisation
	 *            OrganisationVo object
	 * @return organisationVo
	 */
	private OrganisationVo createOrganisationVo(Organisation organisation) {
		OrganisationVo organisationVo = new OrganisationVo();
		organisationVo.setOrganisationId(organisation.getId());
		organisationVo.setOrganisationCode(organisation.getCode());
		organisationVo.setOrganisationUrt(organisation.getUrt());
		organisationVo.setOrganisationName(organisation.getName());
		organisationVo.setOrganisationYear(organisation.getYear());
		organisationVo.setFiscalMonth(organisation.getFiscalMonth());
		organisationVo.setFiscalDay(organisation.getFiscalDate());
		// for (OrganisationContactPerson contactPerson :
		// getOrganisationContactPerson(organisation.getContactPerson())) {
		if (organisation.getContactPerson() != null) {
			OrganisationContactPerson contactPerson = organisation.getContactPerson();
			organisationVo.setContactFirstName(contactPerson.getFirstName());
			organisationVo.setContactLastName(contactPerson.getLastName());
			organisationVo.setContactEmail(contactPerson.getEmail());
			organisationVo.setContactCountry(contactPerson.getCountry().getName());
			organisationVo.setContactNumber(contactPerson.getContactNumber());
		}
		// }
		// organisationVo.setContactPersons(getOrganisationContactPerson(organisation.getContactPerson()));
		return organisationVo;
	}

	/**
	 * method to get list of OrganisationContactPerson
	 * 
	 * @param contactPersons
	 * @return organisationContactPersons
	 */
	private List<OrganisationContactPerson> getOrganisationContactPerson(Set<OrganisationContactPerson> contactPersons) {
		List<OrganisationContactPerson> organisationContactPersons = new ArrayList<OrganisationContactPerson>();
		Iterator<OrganisationContactPerson> iterator = contactPersons.iterator();
		while (iterator.hasNext()) {
			organisationContactPersons.add((OrganisationContactPerson) iterator.next());
		}
		return organisationContactPersons;
	}

	public OrganisationVo findOrganisationById(Long organisationId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organisation.class)
				.add(Restrictions.eq("id", organisationId));
		if (criteria.uniqueResult() == null)
			return null;
		return createOrganisationVo((Organisation) criteria.uniqueResult());
	}

	public OrganisationVo findOrganisationByCode(String organisationCode) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organisation.class)
				.add(Restrictions.eq("code", organisationCode));
		if (criteria.uniqueResult() == null)
			return null;
		return createOrganisationVo((Organisation) criteria.uniqueResult());
	}

	public OrganisationVo findOrganisationByOrgUrt(String organisationUrt) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organisation.class)
				.add(Restrictions.eq("urt", organisationUrt));
		if (criteria.uniqueResult() == null)
			return null;
		return createOrganisationVo((Organisation) criteria.uniqueResult());
	}

	public OrganisationVo findOrganisationByOrgName(String organisationName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organisation.class)
				.add(Restrictions.eq("name", organisationName));
		if (criteria.uniqueResult() == null)
			return null;
		return createOrganisationVo((Organisation) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<OrganisationVo> findAllOrganisation() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organisation.class);
		List<OrganisationVo> organisationVos = new ArrayList<OrganisationVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Organisation> organisations = criteria.list();
		for (Organisation organisation : organisations)
			organisationVos.add(createOrganisationVo(organisation));
		return organisationVos;
	}

	public OrganisationVo findOrganisation() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organisation.class);
		if (criteria.uniqueResult() == null)
			return null;
		return createOrganisationVo((Organisation) criteria.uniqueResult());
	}

}
