package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AdvanceSalaryDao;
import com.hrms.web.dao.AdvanceSalaryStatusDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.AdvanceSalary;
import com.hrms.web.entities.AdvanceSalaryStatus;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AdvanceSalaryVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Repository
public class AdvanceSalaryDaoImpl extends AbstractDao implements AdvanceSalaryDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private AdvanceSalaryStatusDao advanceSalaryStatusDao;
	
	@Autowired
	private TempNotificationDao notificationDao;

	public void saveOrUpdateAdvanceSalary(AdvanceSalaryVo advanceSalaryVo) {
		AdvanceSalary advanceSalary = findAdvanceSalary(advanceSalaryVo.getAdvanceSalaryId());
		if (advanceSalary == null)
			advanceSalary = new AdvanceSalary();
		advanceSalary.setNoOfInstallments(advanceSalaryVo.getNoOfInstallments());
		advanceSalary.setPendingInstallments(advanceSalaryVo.getNoOfInstallments());
		Employee employee = employeeDao.findAndReturnEmployeeByCode(advanceSalaryVo.getEmployeeCode());
		advanceSalary.setEmployee(employee);
		if (advanceSalaryVo.getForwaders() != null && !advanceSalaryVo.getForwaders().contains("auto")) {
			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject(" An Request For Advance Salary Has Been Received From "
					+ employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname() + ".");
			notificationVo.getEmployeeCodes().addAll(advanceSalaryVo.getForwaders());
			notificationDao.saveNotification(notificationVo);
			advanceSalary.setForwader(setAdvanceSalaryForwadList(advanceSalaryVo.getForwaders(), advanceSalary));
			advanceSalary.setStatus(advanceSalaryStatusDao.findAdvanceSalaryStatus(StatusConstants.REQUESTED));
		} else {
			advanceSalary.setStatus(advanceSalaryStatusDao.findAdvanceSalaryStatus(StatusConstants.APPROVED));
		}
		advanceSalary.setTitle(advanceSalaryVo.getAdvanceSalaryTitle());
		advanceSalary.setAdvanceAmount(advanceSalaryVo.getAdvanceAmount());
		advanceSalary.setAmountRequestedOn(DateFormatter.convertStringToDate(advanceSalaryVo.getRequestedDate()));
		advanceSalary.setDescription(advanceSalaryVo.getDescription());
		advanceSalary.setNotes(advanceSalaryVo.getNotes());
		// modification
		advanceSalary.setRepaymentStartDate(DateFormatter.convertStringToDate(advanceSalaryVo.getRepaymentStartDate()));
		advanceSalary.setRepaymentAmount(advanceSalaryVo.getRepaymentAmount());
		advanceSalary.setBalanceAmount(advanceSalaryVo.getAdvanceAmount());
		this.sessionFactory.getCurrentSession().saveOrUpdate(advanceSalary);
		userActivitiesService.saveUserActivity("Added new Advance Salary " + advanceSalary.getTitle());
		advanceSalaryVo.setAdvanceSalaryId(advanceSalary.getId());
		advanceSalaryVo.setRecordedOn(DateFormatter.convertDateToDetailedString(advanceSalary.getCreatedOn()));
	}

	/**
	 * method to create loan forward list
	 * 
	 * @param employee
	 * @param advanceSalary
	 * @return forwardTo
	 */
	private Set<Employee> setAdvanceSalaryForwadList(List<String> employee, AdvanceSalary advanceSalary) {
		Set<Employee> forwardTo = advanceSalary.getForwader();
		if (forwardTo == null || forwardTo.isEmpty())
			forwardTo = new HashSet<Employee>();

		Set<Employee> forwardToTemp = new HashSet<Employee>();
		if (forwardTo != null)
			forwardToTemp.addAll(forwardTo);

		Set<Employee> newForwardTo = new HashSet<Employee>();
		for (String employeeCode : employee) {
			Employee forwader = employeeDao.findAndReturnEmployeeByCode(employeeCode);
			newForwardTo.add(forwader);
			if (!forwardTo.contains(forwader)) {
				forwader.setAdvanceSalary(advanceSalary);
				forwardTo.add(forwader);
			}
		}

		forwardToTemp.removeAll(newForwardTo);
		Iterator<Employee> iterator = forwardToTemp.iterator();
		while (iterator.hasNext()) {
			Employee employee2 = (Employee) iterator.next();
			forwardTo.remove(employee2);
		}
		return forwardTo;
	}

	public void updateAdvanceSalaryStatus(AdvanceSalaryVo advanceSalaryVo) {
		AdvanceSalary advanceSalary = findAdvanceSalary(advanceSalaryVo.getAdvanceSalaryId());
		if (advanceSalary == null)
			throw new ItemNotFoundException("Advance Salary Details Not Found.");
		advanceSalary.setStatus(advanceSalaryStatusDao.findAdvanceSalaryStatus(advanceSalaryVo.getStatus()));
		advanceSalary.setStatusDescription(advanceSalaryVo.getStatusDescription());
		advanceSalary.setForwader(setAdvanceSalaryForwadList(advanceSalaryVo.getForwaders(), advanceSalary));
		this.sessionFactory.getCurrentSession().merge(advanceSalary);
		userActivitiesService.saveUserActivity("Updated Advance Salary Status "+advanceSalaryVo.getStatus());
	}

	public void deleteAdvanceSalary(Long advanceSalaryId) {
		AdvanceSalary advanceSalary = findAdvanceSalary(advanceSalaryId);
		if (advanceSalary == null)
			throw new ItemNotFoundException("Advance Salary Details Not Found.");
		String title=advanceSalary.getTitle();
		advanceSalary.getForwader().clear();
		this.sessionFactory.getCurrentSession().delete(advanceSalary);
		userActivitiesService.saveUserActivity("Deleted Advance Salary "+title);
	}

	public AdvanceSalary findAdvanceSalary(Long advanceSalaryId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalary.class)
				.add(Restrictions.eq("id", advanceSalaryId));
		if (criteria.uniqueResult() == null)
			return null;
		return (AdvanceSalary) criteria.uniqueResult();
	}

	/**
	 * method to create advanceSalaryVo object
	 * 
	 * @param advanceSalary
	 * @return advanceSalaryVo
	 */
	private AdvanceSalaryVo createAdvanceSalaryVo(AdvanceSalary advanceSalary) {
		AdvanceSalaryVo advanceSalaryVo = new AdvanceSalaryVo();
		advanceSalaryVo.setAdvanceSalaryId(advanceSalary.getId());
		advanceSalaryVo.setEmployee(advanceSalary.getEmployee().getUserProfile().getFirstName() + " "
				+ advanceSalary.getEmployee().getUserProfile().getLastname());
		advanceSalaryVo.setEmployeeCode(advanceSalary.getEmployee().getEmployeeCode());
		advanceSalaryVo.setPendingInstallments(advanceSalary.getPendingInstallments());
		advanceSalaryVo.setNoOfInstallments(advanceSalary.getNoOfInstallments());
		advanceSalaryVo.setBalanceAmount(advanceSalary.getBalanceAmount());
		if (advanceSalary.getNoOfInstallments() != advanceSalary.getPendingInstallments()) {
			advanceSalaryVo.setIsView(true);
		}
		advanceSalaryVo.setForwaders(getAdvanceSalaryForwadersList(advanceSalary.getForwader()));
		advanceSalaryVo.setForwadersName(getAdvanceSalaryForwadersNameList(advanceSalary.getForwader()));
		advanceSalaryVo.setAdvanceSalaryTitle(advanceSalary.getTitle());
		advanceSalaryVo.setAdvanceAmount(advanceSalary.getAdvanceAmount());
		advanceSalaryVo.setRepaymentAmount(advanceSalary.getRepaymentAmount());
		advanceSalaryVo.setRepaymentStartDate(DateFormatter.convertDateToString(advanceSalary.getRepaymentStartDate()));
		advanceSalaryVo.setRequestedDate(DateFormatter.convertDateToString(advanceSalary.getAmountRequestedOn()));
		advanceSalaryVo.setDescription(advanceSalary.getDescription());
		advanceSalaryVo.setNotes(advanceSalary.getNotes());
		if (advanceSalary.getStatus() != null)
			advanceSalaryVo.setStatus(advanceSalary.getStatus().getStatus());
		advanceSalaryVo.setStatusDescription(advanceSalary.getStatusDescription());
		advanceSalaryVo.setRecordedOn(DateFormatter.convertDateToDetailedString(advanceSalary.getCreatedOn()));
		return advanceSalaryVo;
	}

	/**
	 * method to get advance salary forwaders list
	 * 
	 * @param forwaders
	 * @return advanceSalaryFrwdrs
	 */
	private List<String> getAdvanceSalaryForwadersList(Set<Employee> forwaders) {
		List<String> advanceSalaryFrwdrs = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			advanceSalaryFrwdrs.add(employee.getEmployeeCode());
		}
		return advanceSalaryFrwdrs;
	}

	/**
	 * method to get advance salary forwaders name list
	 * 
	 * @param forwaders
	 * @return advanceSalaryFrwdrs
	 */
	private List<String> getAdvanceSalaryForwadersNameList(Set<Employee> forwaders) {
		List<String> advanceSalaryFrwdrs = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			advanceSalaryFrwdrs.add(employee.getUserProfile().getFirstName() + " "
					+ employee.getUserProfile().getLastname());
		}
		return advanceSalaryFrwdrs;
	}

	public AdvanceSalaryVo findAdvanceSalaryById(Long advanceSalaryId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalary.class)
				.add(Restrictions.eq("id", advanceSalaryId));
		if (criteria.uniqueResult() == null)
			return null;
		return createAdvanceSalaryVo((AdvanceSalary) criteria.uniqueResult());
	}

	public AdvanceSalaryVo findAdvanceSalaryByEmployee(Employee employee) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalary.class)
				.add(Restrictions.eq("employee", employee));
		if (criteria.uniqueResult() == null)
			return null;
		return createAdvanceSalaryVo((AdvanceSalary) criteria.uniqueResult());
	}

	public List<AdvanceSalaryVo> findAdvanceSalaryByDate(Date requestedDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalary.class)
				.add(Restrictions.eq("amountRequestedOn", requestedDate));
		return iterateCriteria(criteria);
	}

	public List<AdvanceSalaryVo> findAdvanceSalaryBetweenDates(Date startDate, Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalary.class)
				.add(Restrictions.ge("amountRequestedOn", startDate))
				.add(Restrictions.le("amountRequestedOn", endDate));
		return iterateCriteria(criteria);
	}

	public List<AdvanceSalaryVo> findAdvanceSalaryByStatus(AdvanceSalaryStatus status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalary.class)
				.add(Restrictions.eq("status", status));
		return iterateCriteria(criteria);
	}

	public List<AdvanceSalaryVo> findAllAdvanceSalary() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalary.class);
		return iterateCriteria(criteria);
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	private List<AdvanceSalaryVo> iterateCriteria(Criteria criteria) {
		List<AdvanceSalaryVo> advanceSalaryVos = new ArrayList<AdvanceSalaryVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<AdvanceSalary> advanceSalaries = criteria.list();
		for (AdvanceSalary advanceSalary : advanceSalaries)
			advanceSalaryVos.add(createAdvanceSalaryVo(advanceSalary));
		return advanceSalaryVos;
	}

	public List<AdvanceSalaryVo> findAdvanceSalaryBetweenDate(Long employeeId, Date startDate, Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalary.class)
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeById(employeeId)))
				.add(Restrictions.ge("amountRequestedOn", startDate))
				.add(Restrictions.le("amountRequestedOn", endDate));
		return iterateCriteria(criteria);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdvanceSalaryVo> findAllAdvanceSalaryByEmployee(String employeeCode) {
		List<AdvanceSalaryVo> advanceSalaryVos = new ArrayList<AdvanceSalaryVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalary.class);
		criteria.createAlias("forwader", "employee");
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("createdBy", employee.getUser());
		Criterion receiver = Restrictions.eq("employee.employeeCode", employeeCode);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<AdvanceSalary> advanceSalaries = criteria.list();
		for (AdvanceSalary advanceSalary : advanceSalaries)
			advanceSalaryVos.add(createAdvanceSalaryVo(advanceSalary));
		return advanceSalaryVos;
	}

	public List<AdvanceSalaryVo> findAdvanceSalaryByStatusAndDates(String employeeCode, String status, Date startDate,
			Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalary.class)
				.add(Restrictions.eq("status", advanceSalaryStatusDao.findAdvanceSalaryStatus(status)))
				.add(Restrictions.ge("amountRequestedOn", startDate))
				.add(Restrictions.le("amountRequestedOn", endDate))
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeByCode(employeeCode)));
		return iterateCriteria(criteria);
	}

	public List<AdvanceSalary> findApprovedAdvanceSalaryForSalary(String endDate, Employee employee) {
		Criteria criteria = this.sessionFactory
				.getCurrentSession()
				.createCriteria(AdvanceSalary.class)
				.add(Restrictions.eq("employee", employee))
				.add(Restrictions.le("repaymentStartDate", DateFormatter.convertStringToDate(endDate)))
				.add(Restrictions.eq("status", advanceSalaryStatusDao.findAdvanceSalaryStatus(StatusConstants.APPROVED)));
		List<AdvanceSalary> salaries = new ArrayList<AdvanceSalary>();
		salaries = criteria.list();
		return salaries;
	}

	public void updateAdvanceSalaryObject(AdvanceSalary advanceSalary) {
		this.sessionFactory.getCurrentSession().merge(advanceSalary);
	}
}
