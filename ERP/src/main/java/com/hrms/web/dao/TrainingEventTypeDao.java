package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.TrainingEventType;
import com.hrms.web.vo.TrainingEventTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public interface TrainingEventTypeDao {

	/**
	 * method to save TrainingEventType
	 * 
	 * @param trainingEventTypeVo
	 */
	public void saveTrainingEventType(TrainingEventTypeVo trainingEventTypeVo);

	/**
	 * method to update TrainingEventType
	 * 
	 * @param trainingEventTypeVo
	 */
	public void updateTrainingEventType(TrainingEventTypeVo trainingEventTypeVo);

	/**
	 * method to delete TrainingEventType
	 * 
	 * @param trainingEventTypeId
	 */
	public void deleteTrainingEventType(Long trainingEventTypeId);

	/**
	 * method to find TrainingEventType by id
	 * 
	 * @param trainingEventTypeId
	 * @return TrainingEventType
	 */
	public TrainingEventType findTrainingEventType(Long trainingEventTypeId);

	/**
	 * method to find TrainingEventType by type
	 * 
	 * @param trainingEventType
	 * @return TrainingEventType
	 */
	public TrainingEventType findTrainingEventType(String trainingEventType);

	/**
	 * method to find TrainingEventType by id
	 * 
	 * @param trainingEventTypeId
	 * @return TrainingEventTypeVo
	 */
	public TrainingEventTypeVo findTrainingEventTypeById(Long trainingEventTypeId);

	/**
	 * method to find TrainingEventType by type
	 * 
	 * @param trainingEventType
	 * @return TrainingEventTypeVo
	 */
	public TrainingEventTypeVo findTrainingEventTypeBytype(String trainingEventType);

	/**
	 * method to find all TrainingEventType
	 * 
	 * @return List<TrainingEventTypeVo>
	 */
	public List<TrainingEventTypeVo> findAllTrainingEventType();
}
