package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.PayRollOptionDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.PayRollOption;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.PayRollOptionVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Repository
public class PayRollOptionDaoImpl extends AbstractDao implements PayRollOptionDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	public void saveOrUpdatePayRollOption(PayRollOptionVo payRollOptionVo) {
		PayRollOption payRollOption = findPayRollOption(payRollOptionVo.getPayRollOptionId());
		if (payRollOption == null)
			payRollOption = new PayRollOption();
		payRollOption.setAutoAprovePayroll(Boolean.valueOf(payRollOptionVo.getAutoAprovePayRoll()));
		payRollOption.setAutoEmailPayslip(Boolean.valueOf(payRollOptionVo.getAutoEmailPaySlip()));
		payRollOption.setPerDayCalculation(Boolean.valueOf(payRollOptionVo.getPerDaySalaryCalculation()));
		payRollOption.setPerDayCalculationType(payRollOptionVo.getPerDaySalaryCalculationType());
		payRollOption.setPerDayCalculationDays(payRollOptionVo.getPerDaySalaryCalculationDays());
		payRollOption.setApprovers(setPaySlipApprovers(payRollOptionVo.getApprovers(), payRollOption));
		this.sessionFactory.getCurrentSession().saveOrUpdate(payRollOption);
	}

	/**
	 * method to set pay slip approver employees
	 * 
	 * @param approvers
	 * @param payRollOption
	 * @return
	 */
	private Set<Employee> setPaySlipApprovers(List<String> approvers, PayRollOption payRollOption) {
		Set<Employee> forwardTo = payRollOption.getApprovers();
		if (!payRollOption.getAutoAprovePayroll()) {
			if (forwardTo == null || forwardTo.isEmpty())
				forwardTo = new HashSet<Employee>();

			Set<Employee> forwardToTemp = new HashSet<Employee>();
			if (forwardTo != null)
				forwardToTemp.addAll(forwardTo);

			Set<Employee> newForwardTo = new HashSet<Employee>();
			for (String employeeCode : approvers) {
				Employee forwader = employeeDao.findAndReturnEmployeeByCode(employeeCode);
				newForwardTo.add(forwader);
				if (!forwardTo.contains(forwader)) {
					forwader.setPayRollOption(payRollOption);
					forwardTo.add(forwader);
				}
			}

			forwardToTemp.removeAll(newForwardTo);
			Iterator<Employee> iterator = forwardToTemp.iterator();
			while (iterator.hasNext()) {
				Employee employee2 = (Employee) iterator.next();
				employee2.setPayRollOption(null);
				this.sessionFactory.getCurrentSession().merge(employee2);
				forwardTo.remove(employee2);
			}
		} else {
			Iterator<Employee> iterator = forwardTo.iterator();
			while (iterator.hasNext()) {
				Employee employee2 = (Employee) iterator.next();
				employee2.setPayRollOption(null);
				this.sessionFactory.getCurrentSession().merge(employee2);
				forwardTo.remove(employee2);
			}
		}
		return forwardTo;
	}

	public void deletePayRollOption(Long payRollOptionId) {
		PayRollOption payRollOption = findPayRollOption(payRollOptionId);
		if (payRollOption == null)
			throw new ItemNotFoundException("PayRoll Option Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(payRollOption);
	}

	public PayRollOption findPayRollOption(Long payRollOptionId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PayRollOption.class)
				.add(Restrictions.eq("id", payRollOptionId));
		if (criteria.uniqueResult() == null)
			return null;
		return (PayRollOption) criteria.uniqueResult();
	}

	/**
	 * method to create PayRollOptionVo object
	 * 
	 * @param payRollOption
	 * @return payRollOptionVo
	 */
	private PayRollOptionVo createPayRollOptionVo(PayRollOption payRollOption) {
		PayRollOptionVo payRollOptionVo = new PayRollOptionVo();
		payRollOptionVo.setPayRollOptionId(payRollOption.getId());
		payRollOptionVo.setAutoAprovePayRoll(String.valueOf(payRollOption.getAutoAprovePayroll()));
		payRollOptionVo.setAutoEmailPaySlip(String.valueOf(payRollOption.getAutoEmailPayslip()));
		payRollOptionVo.setPerDaySalaryCalculation(String.valueOf(payRollOption.getPerDayCalculation()));
		payRollOptionVo.setPerDaySalaryCalculationType(payRollOption.getPerDayCalculationType());
		payRollOptionVo.setPerDaySalaryCalculationDays(payRollOption.getPerDayCalculationDays());
		payRollOptionVo.setApprovers(getPaySlipApprovers(payRollOption.getApprovers()));
		return payRollOptionVo;
	}

	/**
	 * method to get all pay slip approvers
	 * 
	 * @param approvers
	 * @return
	 */
	private List<String> getPaySlipApprovers(Set<Employee> approvers) {
		List<String> forwarders = new ArrayList<String>();
		Iterator<Employee> iterator = approvers.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			forwarders.add(employee.getEmployeeCode());
		}
		return forwarders;
	}

	public PayRollOptionVo findPayRollOptionById(Long payRollOptionId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PayRollOption.class)
				.add(Restrictions.eq("id", payRollOptionId));
		if (criteria.uniqueResult() == null)
			return null;
		return createPayRollOptionVo((PayRollOption) criteria.uniqueResult());
	}

	public PayRollOptionVo findPayRollOption() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PayRollOption.class);
		if (criteria.uniqueResult() == null)
			return null;
		return createPayRollOptionVo((PayRollOption) criteria.uniqueResult());
	}

}
