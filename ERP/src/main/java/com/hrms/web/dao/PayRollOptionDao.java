package com.hrms.web.dao;

import com.hrms.web.entities.PayRollOption;
import com.hrms.web.vo.PayRollOptionVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public interface PayRollOptionDao {

	/**
	 * method to save PayRollOption
	 * 
	 * @param payRollOptionVo
	 */
	public void saveOrUpdatePayRollOption(PayRollOptionVo payRollOptionVo);

	/**
	 * method to delete PayRollOption
	 * 
	 * @param payRollOptionId
	 */
	public void deletePayRollOption(Long payRollOptionId);

	/**
	 * method to find PayRollOption by id
	 * 
	 * @param payRollOptionId
	 * @return PayRollOption
	 */
	public PayRollOption findPayRollOption(Long payRollOptionId);

	/**
	 * method to find PayRollOption by id
	 * 
	 * @param payRollOptionId
	 * @return PayRollOptionVo
	 */
	public PayRollOptionVo findPayRollOptionById(Long payRollOptionId);

	/**
	 * method to find PayRollOption
	 * 
	 * @return
	 */
	public PayRollOptionVo findPayRollOption();
}
