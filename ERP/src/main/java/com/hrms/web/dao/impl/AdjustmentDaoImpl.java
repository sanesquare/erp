package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AdjustmentDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.entities.AdjustmentTitle;
import com.hrms.web.entities.Adjustments;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AdjustmentVo;

/**
 * 
 * @author Shamsheer
 * @since 14-April-2015
 */
@Repository
public class AdjustmentDaoImpl extends AbstractDao implements AdjustmentDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	/**
	 * method to save adjustment
	 */
	public Long saveAdjustment(AdjustmentVo vo) {
		Adjustments adjustments =new Adjustments();

		/*Employee employee = new Employee();
		employee.setId(vo.getEmployeeId());*/

		Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
		if(employee==null)
			throw new ItemNotFoundException("Employee Not Exist.");
		adjustments.setEmployee(employee);

		AdjustmentTitle title = new AdjustmentTitle();
		title.setId(vo.getTitleId());
		adjustments.setTitle(title);

		adjustments.setType(vo.getType());
		adjustments.setAmount(new BigDecimal(vo.getAmount()));
		adjustments.setDescription(vo.getDescription());
		adjustments.setNotes(vo.getNotes());

		if(vo.getDate()!="")
			adjustments.setDate(DateFormatter.convertStringToDate(vo.getDate()));

		userActivitiesService.saveUserActivity("Added new Adjustment "+adjustments.getTitle());
		return (Long) this.sessionFactory.getCurrentSession().save(adjustments);
	}

	/**
	 * method to update adjustment
	 */
	public void updateAdjustment(AdjustmentVo vo) {
		Adjustments adjustments = findAdjustment(vo.getAdjustmentId());

		if(adjustments==null)
			throw new ItemNotFoundException("Adjustment Not Exist.");

		/*Employee employee = new Employee();
		employee.setId(vo.getEmployeeId());*/

		Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
		if(employee==null)
			throw new ItemNotFoundException("Employee Not Exist.");
		adjustments.setEmployee(employee);

		AdjustmentTitle title = new AdjustmentTitle();
		title.setId(vo.getTitleId());
		adjustments.setTitle(title);

		adjustments.setType(vo.getType());
		adjustments.setAmount(new BigDecimal(vo.getAmount()));
		adjustments.setDescription(vo.getDescription());
		adjustments.setNotes(vo.getNotes());

		if(vo.getDate()!="")
			adjustments.setDate(DateFormatter.convertStringToDate(vo.getDate()));

		this.sessionFactory.getCurrentSession().merge(adjustments);
		userActivitiesService.saveUserActivity("Updated Adjustment "+vo.getEmployeeCode());
	}

	/**
	 * method to delete adjustment
	 */
	public void deleteAdjustment(Long adjustmentId) {
		Adjustments adjustments = findAdjustment(adjustmentId);
		if(adjustments==null)
			throw new ItemNotFoundException("Adjustment Not Exist.");
		this.sessionFactory.getCurrentSession().delete(adjustments);
		userActivitiesService.saveUserActivity("Deleted Adjustment ");
	}

	/**
	 * method to find adjustment
	 */
	public Adjustments findAdjustment(Long adjustmentId) {
		return (Adjustments) this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.eq("id", adjustmentId)).uniqueResult();
	}

	/**
	 * method to list all adjustments
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listAllAdjustments() {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	/**
	 * method to list adjustments by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listAdjustmentsByEmployee(Long employeeId) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class);
		Criterion byEmployee = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression logicalExpression = Restrictions.or(byEmployee, createdBy);
		criteria.add(logicalExpression);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	/**
	 * method to list adjustment by tile
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listAdjustmentsByTitle(Long titleId) {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.eq("title.id", titleId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	/**
	 * method to list adjustments by type
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listAdjustmentsByType(String type) {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.eq("type", type));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	/**
	 * method to find adjustment by id
	 */
	public AdjustmentVo findAdjustmentById(Long adjustmentId) {
		Adjustments adjustments = findAdjustment(adjustmentId);
		if(adjustments==null)
			throw new ItemNotFoundException("Adjustment Not Exist.");
		return createAdjustmetnVo(adjustments);
	}

	/**
	 * method to create adjustment vo
	 * @param adjustments
	 * @return
	 */
	private AdjustmentVo createAdjustmetnVo(Adjustments adjustments){
		AdjustmentVo vo = new AdjustmentVo();
		vo.setAdjustmentId(adjustments.getId());
		vo.setEmployeeId(adjustments.getEmployee().getId());
		vo.setEmployeeCode(adjustments.getEmployee().getEmployeeCode());
		vo.setEmployee(adjustments.getEmployee().getUserProfile().getFirstName()+" "+adjustments.getEmployee().getUserProfile().getLastname());
		vo.setType(adjustments.getType());
		vo.setTitle(adjustments.getTitle().getTitle());
		vo.setTitleId(adjustments.getTitle().getId());
		vo.setAmount(adjustments.getAmount().toString());
		if(adjustments.getDate()!=null)
			vo.setDate(DateFormatter.convertDateToString(adjustments.getDate()));
		vo.setDescription(adjustments.getDescription());
		vo.setNotes(adjustments.getNotes());
		vo.setCreatedBy(adjustments.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(adjustments.getCreatedOn()));
		return vo;
	}

	/**
	 * method to get adjustment by employee title
	 */
	public AdjustmentVo getAdjustmentByEmployeeAndTitle(String employeeCode, Long titleId) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);

		Adjustments adjustments = (Adjustments) this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.eq("employee", employee)).add(Restrictions.eq("title.id", titleId)).uniqueResult();
		if(adjustments==null)
			throw new ItemNotFoundException("Adjustment Not Exist.");
		return createAdjustmetnVo(adjustments);
	}

	/**
	 * method to list adjustment by start date & enda date
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listAdjustmentsByStartDateEndDate(Date startDate, Date endDate) {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.ge("date", startDate))
				.add(Restrictions.le("date", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	/**
	 * method to list adjustment by employee , start date & enda date
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listAdjustmentsByEmployeeStartDateEndDate(Long employeeId, Date startDate, Date endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.ge("date", startDate))
				.add(Restrictions.le("date", endDate))
				.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listIncrementAdjustmentsByEmployee(Employee employee) {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.eq("type", "Increment"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listDecrementAdjustmentsByEmployee(Employee employee) {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.eq("type", "Decrement"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listAdjustmentsByEmployeeStartDateEndDate(Employee employee, Date startDate,
			Date endDate) {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.ge("date", startDate))
				.add(Restrictions.le("date", endDate))
				.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<AdjustmentVo> listDecrementAdjustmentsByEmployee(Employee employee, Date startDate, Date endDate) {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("date", startDate))
			.add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("type", "Decrement"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listIncrementAdjustmentsByEmployee(Employee employee, Date startDate, Date endDate) {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("date", startDate))
			.add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("type", "Increment"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listEncashmentAdjustmentsByEmployee(Employee employee) {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.eq("type", "Encashment"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AdjustmentVo> listEncashmentAdjustmentsByEmployee(Employee employee, Date startDate, Date endDate) {
		List<AdjustmentVo> vos = new ArrayList<AdjustmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Adjustments.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("date", startDate))
			.add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("type", "Encashment"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Adjustments> adjustments = criteria.list();
		for(Adjustments adjustment : adjustments){
			vos.add(createAdjustmetnVo(adjustment));
		}
		return vos;
	}
}
