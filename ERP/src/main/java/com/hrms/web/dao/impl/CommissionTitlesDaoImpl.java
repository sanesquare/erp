package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CommitionTitleDao;
import com.hrms.web.entities.BonusTitles;
import com.hrms.web.entities.CommissionTitles;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.BonusTitleVo;
import com.hrms.web.vo.CommissionTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
@Repository
public class CommissionTitlesDaoImpl extends AbstractDao implements CommitionTitleDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveCommissionTitle(CommissionTitleVo titleVo) {
		for(String title : StringSplitter.splitWithComma(titleVo.getTitle().trim())){
			if(findCommissionTitle(title.trim())!=null)
				throw new DuplicateItemException("Duplicate Entry : "+title.trim());
			CommissionTitles commissionTitles = new CommissionTitles();
			commissionTitles.setTitle(title.trim());
			this.sessionFactory.getCurrentSession().save(commissionTitles);
			userActivitiesService.saveUserActivity("Added new Commission Title "+title.trim());
		}
	}

	public void updateCommissionTitle(CommissionTitleVo titleVo) {
		// TODO Auto-generated method stub

	}

	public void deleteCommissionTitle(Long commissionTitleId) {
		CommissionTitles commissionTitles = findCommissionTitle(commissionTitleId);
		if(commissionTitles==null)
			throw new ItemNotFoundException("Bonus Title Not Found.  ");
		String name=commissionTitles.getTitle();
		this.sessionFactory.getCurrentSession().delete(commissionTitles);
		userActivitiesService.saveUserActivity("Deleted commission Title "+name);
	}

	public void deleteCommissionTitle(String commissionTitle) {
		CommissionTitles commissionTitles = findCommissionTitle(commissionTitle);
		if(commissionTitles==null)
			throw new ItemNotFoundException("Bonus Title Not Found.  ");
		this.sessionFactory.getCurrentSession().delete(commissionTitles);
	}

	public CommissionTitles findCommissionTitle(Long commissionTitleId) {
		return (CommissionTitles) this.sessionFactory.getCurrentSession().createCriteria(CommissionTitles.class)
				.add(Restrictions.eq("id", commissionTitleId)).uniqueResult();
	}

	public CommissionTitles findCommissionTitle(String commissionTitle) {
		return (CommissionTitles) this.sessionFactory.getCurrentSession().createCriteria(CommissionTitles.class)
				.add(Restrictions.eq("title", commissionTitle)).uniqueResult();
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<CommissionTitleVo> listAllCommissionTitles() {
		List<CommissionTitleVo> vos = new ArrayList<CommissionTitleVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(CommissionTitles.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<CommissionTitles> titles = criteria.list();
		for(CommissionTitles title : titles){
			vos.add(createBonusTitleVo(title));
		}
		return vos;
	}

	private CommissionTitleVo createBonusTitleVo(CommissionTitles titles){
		CommissionTitleVo vo = new CommissionTitleVo();
		vo.setId(titles.getId());
		vo.setTitle(titles.getTitle());
		return vo;
	}
}
