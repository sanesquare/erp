package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ShiftDayDao;
import com.hrms.web.dao.WorkShiftDetailsDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ShiftDay;
import com.hrms.web.entities.WorkShift;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.WorkShiftDetailsVo;

/**
 * 
 * @author Jithin Mohan
 * @since 1-April-2015
 *
 */
@Repository
public class WorkShiftDetailsDaoImpl extends AbstractDao implements WorkShiftDetailsDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private ShiftDayDao shiftDayDao;

	@Autowired
	private EmployeeDao employeeDao;

	public void saveWorkShiftDetails(List<WorkShiftDetailsVo> shiftDetailsVo) {
		for (WorkShiftDetailsVo shiftDetails : shiftDetailsVo) {
			WorkShift workShift = findWorkShiftDetails(shiftDetails.getWorkShiftId());
			if (workShift == null)
				workShift = new WorkShift();
			workShift.setShiftDay(shiftDayDao.findShiftDay(shiftDetails.getShiftDay()));
			workShift.setRegularWorkFrom(shiftDetails.getRegularWorkFrom());
			workShift.setRegularWorkTo(shiftDetails.getRegularWorkTo());
			workShift.setRefreshmentBreakFrom(shiftDetails.getRefreshmentBreakFrom());
			workShift.setRefreshmentBreakTo(shiftDetails.getRefreshmentBreakTo());
			workShift.setAdditionalBreakFrom(shiftDetails.getAdditionalBreakFrom());
			workShift.setAdditionalBreakTo(shiftDetails.getAdditionalBreakTo());
			workShift.setEmployee(employeeDao.findAndReturnEmployeeByCode(shiftDetails.getEmployee()));
			this.sessionFactory.getCurrentSession().saveOrUpdate(workShift);
		}
	}

	public void updateWorkShiftDetails(WorkShiftDetailsVo shiftDetailsVo) {
		// TODO Auto-generated method stub

	}

	public void deleteWorkShiftDetails(Long shiftDetailsId) {
		// TODO Auto-generated method stub

	}

	public WorkShift findWorkShiftDetails(Long shiftDetailsId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(WorkShift.class)
				.add(Restrictions.eq("id", shiftDetailsId));
		if (criteria.uniqueResult() == null)
			return null;
		return (WorkShift) criteria.uniqueResult();
	}

	public List<WorkShift> findWorkShiftDetails(ShiftDay shiftDay) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * method to create WorkShiftDetailsVo object
	 * 
	 * @param workShift
	 * @return shiftDetailsVo
	 */
	private WorkShiftDetailsVo createWorkShiftDetailsVo(WorkShift workShift) {
		WorkShiftDetailsVo shiftDetailsVo = new WorkShiftDetailsVo();
		shiftDetailsVo.setRegularWorkFrom(workShift.getRegularWorkFrom());
		shiftDetailsVo.setRegularWorkTo(workShift.getRegularWorkTo());
		shiftDetailsVo.setRefreshmentBreakFrom(workShift.getRefreshmentBreakFrom());
		shiftDetailsVo.setRefreshmentBreakTo(workShift.getRefreshmentBreakTo());
		shiftDetailsVo.setAdditionalBreakFrom(workShift.getAdditionalBreakFrom());
		shiftDetailsVo.setAdditionalBreakTo(workShift.getAdditionalBreakTo());
		shiftDetailsVo.setShiftDay(workShift.getShiftDay().getDay());
		shiftDetailsVo.setEmployee(workShift.getEmployee().getEmployeeCode());
		shiftDetailsVo.setWorkShiftId(workShift.getId());
		return shiftDetailsVo;
	}

	public WorkShiftDetailsVo findWorkShiftDetailsById(Long shiftDetailsId) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<WorkShiftDetailsVo> findWorkShiftDetailsByEmployee(Employee employee) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(WorkShift.class)
				.add(Restrictions.eq("employee", employee));
		List<WorkShiftDetailsVo> shiftDetailsVos = new ArrayList<WorkShiftDetailsVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<WorkShift> shifts = criteria.list();
		for (WorkShift shift : shifts)
			shiftDetailsVos.add(createWorkShiftDetailsVo(shift));
		return shiftDetailsVos;
	}

	public List<WorkShiftDetailsVo> findAllWorkShiftDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	public WorkShiftDetailsVo findWorkShiftByDay(String day, String employeeCode) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(WorkShift.class)
				.add(Restrictions.eq("shiftDay", shiftDayDao.findShiftDay(day)))
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeByCode(employeeCode)));
		if (criteria.uniqueResult() == null)
			return null;
		return createWorkShiftDetailsVo((WorkShift) criteria.uniqueResult());
	}
}



