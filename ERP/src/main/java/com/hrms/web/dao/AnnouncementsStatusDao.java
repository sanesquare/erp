package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.AnnouncementsStatus;
import com.hrms.web.vo.AnnouncementsStatusVo;

/**
 * 
 * @author Shimil Babu
 * @since 11-March-2015
 *
 */
public interface AnnouncementsStatusDao {
	
	/**
	 * method to save announcements status
	 * @param announcementsStatus 
	 * @return boolean
	 */
	public boolean saveAnnouncementsStatus(AnnouncementsStatusVo announcementsStatusVo);
	
	/**
	 * method to delete announcements status
	 * @param announcementsStatus 
	 * @return boolean
	 */
	public boolean deleteAnnouncementsStatus(AnnouncementsStatusVo announcementsVo);
	
	/**
	 * method to update announcements
	 * @param announcements
	 * @return boolean
	 */
	public boolean updateAnnouncementsStatus(AnnouncementsStatusVo announcementsVo);
	
	/**
	 * method to find all announcements 
	 * 
	 * @return List of AnnouncementsVo 
	 */
	public List<AnnouncementsStatusVo> findAllAnnouncementsStatus();
	
	/**
	 * method to find announcements status by name 
	 * 
	 * @return List of AnnouncementsStatusVo 
	 */                        
	public AnnouncementsStatus findAnnouncementsStatusByAnnouncementsStatusName(String status);
	
	/**
	 * method to find announcements status by name 
	 * 
	 * @return List of AnnouncementsStatusVo 
	 */
	
	public AnnouncementsStatus findAnnouncementsStatusByAnnouncementsStatusNotes(String notes);
	
	public AnnouncementsStatus findAnnouncementsStatusByAnnouncementsStatusId(Long id);
}
