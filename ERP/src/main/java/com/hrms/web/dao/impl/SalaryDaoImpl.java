package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ExtraPaySlipItemDao;
import com.hrms.web.dao.SalaryDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ExtraPaySlipItem;
import com.hrms.web.entities.HourlyWages;
import com.hrms.web.entities.PaySlipItem;
import com.hrms.web.entities.SalaryDailyWage;
import com.hrms.web.entities.SalaryExtraPayslipItem;
import com.hrms.web.entities.SalaryHourlyWage;
import com.hrms.web.entities.SalaryMonthly;
import com.hrms.web.entities.SalaryPayslipItems;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.PaySlipItemService;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.SalaryDailyWagesVo;
import com.hrms.web.vo.SalaryHourlyWageVo;
import com.hrms.web.vo.SalaryMonthlyVo;
import com.hrms.web.vo.SalaryPayslipItemsVo;
import com.hrms.web.vo.SalaryViewVo;
import com.hrms.web.vo.SalaryVo;

/**
 * 
 * @author Shamsheer
 * @since 21-April-2015
 */
@Repository
public class SalaryDaoImpl extends AbstractDao implements SalaryDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private PaySlipItemService paySlipItemService;

	@Autowired
	private ExtraPaySlipItemDao extraPaySlipItemDao;

	/**
	 * method to save daily wage salary
	 */
	public void saveSalaryDailyWage(SalaryVo salaryVo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(salaryVo.getEmployeeCode());
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");

		SalaryDailyWage dailyWage = (SalaryDailyWage) this.sessionFactory.getCurrentSession().createCriteria(SalaryDailyWage.class)
				.add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("withEffectFrom", DateFormatter.convertStringToDate(salaryVo.getWithEffectFrom()))).uniqueResult();
		if(dailyWage == null){
			dailyWage = new SalaryDailyWage();
			dailyWage.setEmployee(employee);
		}
		dailyWage.setWithEffectFrom(DateFormatter.convertStringToDate(salaryVo.getWithEffectFrom()));
		dailyWage.setSalary(salaryVo.getDailyWagesVo().getFinalSalary());
		dailyWage.setTax(new BigDecimal(salaryVo.getDailyWagesVo().getTax()));
		this.sessionFactory.getCurrentSession().saveOrUpdate(dailyWage);
		userActivitiesService.saveUserActivity("Added new Salary Daily Wage "+salaryVo.getEmployeeCode());
	}

	/**
	 * method to save hourly wage salary
	 */
	public void saveSalaryHourlyWage(SalaryVo salaryVo) {
		SalaryHourlyWageVo hourlyWageVo = salaryVo.getHourlyWageVo();
		Employee employee = employeeDao.findAndReturnEmployeeByCode(salaryVo.getEmployeeCode());
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		SalaryHourlyWage hourlyWage = (SalaryHourlyWage) this.sessionFactory.getCurrentSession().createCriteria(SalaryHourlyWage.class)
				.add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("withEffectFrom", DateFormatter.convertStringToDate(salaryVo.getWithEffectFrom()))).uniqueResult();
		if(hourlyWage == null){
			hourlyWage = new SalaryHourlyWage();
			hourlyWage.setEmployee(employee);
		}
		hourlyWage.setWithEffectFrom(DateFormatter.convertStringToDate(salaryVo.getWithEffectFrom()));
		hourlyWage.setRegularHourSalary(new BigDecimal(hourlyWageVo.getFinalRegularHourSalary()));
		hourlyWage.setOverTimeHourSalary(new BigDecimal(hourlyWageVo.getFinalOverTimerHourSalary()));
		hourlyWage.setTax(Double.parseDouble(hourlyWageVo.getTax()));
		this.sessionFactory.getCurrentSession().saveOrUpdate(hourlyWage);
		userActivitiesService.saveUserActivity("Added new Salary Hourly Wage "+salaryVo.getEmployeeCode());
	}

	/**
	 * method to save monthly salary
	 */
	public void saveSalaryMonthly(SalaryVo salaryVo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(salaryVo.getEmployeeCode());
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		SalaryMonthly salaryMonthly = (SalaryMonthly) this.sessionFactory.getCurrentSession().createCriteria(SalaryMonthly.class)
				.add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("withEffectFrom", DateFormatter.convertStringToDate(salaryVo.getWithEffectFrom()))).uniqueResult();
		if(salaryMonthly == null){
			salaryMonthly = new SalaryMonthly();
			salaryMonthly.setEmployee(employee);
		}
		salaryMonthly.setWithEffectFrom(DateFormatter.convertStringToDate(salaryVo.getWithEffectFrom()));

		salaryMonthly.setGrossSalaryAnnually(new BigDecimal(salaryVo.getAnnualGrossSalary()));
		salaryMonthly.setGrossSalaryMonthly(new BigDecimal(salaryVo.getMonthlyGrossSalary()));
		if(salaryVo.getAnnualTaxDeduction()!="")
			salaryMonthly.setTaxDeductionAnnually(new BigDecimal(salaryVo.getAnnualTaxDeduction()));
		if(salaryVo.getMonthlyTaxDeduction()!="")
			salaryMonthly.setTaxDeductionMonthly(new BigDecimal(salaryVo.getMonthlyTaxDeduction()));
		salaryMonthly.setEstimatedAnnualSalary(new BigDecimal(salaryVo.getAnnualEstimatedSalary()));
		salaryMonthly.setEstimatedMonthlySalary(new BigDecimal(salaryVo.getMonthlyEstimatedSalary()));

		setPayslipItems(salaryVo, salaryMonthly);
	}


	/**
	 * method to set salary paysip items
	 * @param salaryVo
	 * @return
	 */
	private void setPayslipItems(SalaryVo salaryVo , SalaryMonthly salaryMonthly){
		Set<SalaryPayslipItems> payslipItems = new HashSet<SalaryPayslipItems>();
		Set<SalaryExtraPayslipItem> salaryExtraPayslipItems =new HashSet<SalaryExtraPayslipItem>();
		for(SalaryPayslipItemsVo vo : salaryVo.getItemsVos()){
			if(vo.getAmount()!=null){
				SalaryPayslipItems items = new SalaryPayslipItems();
				SalaryExtraPayslipItem extraPayslipItem = new SalaryExtraPayslipItem();
				extraPayslipItem.setAmount(vo.getAmount());
				extraPayslipItem.setSalaryMonthly(salaryMonthly);
				items.setAmount(vo.getAmount());
				items.setSalaryMonthly(salaryMonthly);
				PaySlipItem paySlipItem = paySlipItemService.findPaySlipItem(vo.getItem());
				ExtraPaySlipItem item = extraPaySlipItemDao.findExtraPaySlipItem(vo.getItem());
				if(paySlipItem !=null){
					items.setPayslipItem(paySlipItem);
					payslipItems.add(items);
				}else if(item!=null){
					extraPayslipItem.setExtraPayslipItem(item);
					salaryExtraPayslipItems.add(extraPayslipItem);
				}
			}
		}
		salaryMonthly.setExtraPayslipItems(salaryExtraPayslipItems);
		salaryMonthly.setPayslipItems(payslipItems);
		this.sessionFactory.getCurrentSession().saveOrUpdate(salaryMonthly);
	}


	/**
	 * method to get employee's daily wage
	 */
	public SalaryDailyWagesVo getSalaryDailyWage(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		DetachedCriteria withEffectFrom = DetachedCriteria.forClass(SalaryDailyWage.class)
				.setProjection(Projections.max("withEffectFrom"));
		Criteria criteria =  this.sessionFactory.getCurrentSession().createCriteria(SalaryDailyWage.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Property.forName("withEffectFrom").eq(withEffectFrom));
		SalaryDailyWage dailyWage = (SalaryDailyWage) criteria.uniqueResult();
		return createDailyWageSlaryVo(dailyWage);
	}

	/**
	 * method to create daily wage salary vo
	 * @param wage
	 * @return
	 */
	private SalaryDailyWagesVo createDailyWageSlaryVo(SalaryDailyWage wage){
		SalaryDailyWagesVo dailyWagesVo = new SalaryDailyWagesVo();

		if(wage.getEmployee().getUserProfile()!=null)
			dailyWagesVo.setEmployee(wage.getEmployee().getUserProfile().getFirstName()+" "+wage.getEmployee().getUserProfile().getLastname());
		dailyWagesVo.setEmployeeCode(wage.getEmployee().getEmployeeCode());

		dailyWagesVo.setWithEffectFrom(DateFormatter.convertDateToString(wage.getWithEffectFrom()));

		dailyWagesVo.setSalary(wage.getSalary().toString());
		dailyWagesVo.setTax(wage.getTax().toString());
		return dailyWagesVo;
	}

	/**
	 * method to get employee's hourly wage salary
	 */
	public SalaryHourlyWageVo getSalaryHourlyWage(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		DetachedCriteria withEffectFrom = DetachedCriteria.forClass(SalaryHourlyWage.class).setProjection(Projections.max("withEffectFrom"));
		SalaryHourlyWage hourlyWage = (SalaryHourlyWage) this.sessionFactory.getCurrentSession().createCriteria(SalaryHourlyWage.class)
				.add(Restrictions.eq("employee", employee))
				.add(Property.forName("withEffectFrom").eq(withEffectFrom));
		return createHourlyWageSalaryVo(hourlyWage);
	}

	/**
	 * method to create hourly wage salary vo
	 * @param hourlyWage
	 * @return
	 */
	private SalaryHourlyWageVo createHourlyWageSalaryVo(SalaryHourlyWage hourlyWage){
		SalaryHourlyWageVo vo = new SalaryHourlyWageVo();
		if(hourlyWage.getEmployee().getUserProfile()!=null)
			vo.setEmployee(hourlyWage.getEmployee().getUserProfile().getFirstName()+" "+hourlyWage.getEmployee().getUserProfile().getLastname());
		vo.setEmployeeCode(hourlyWage.getEmployee().getEmployeeCode());

		vo.setWithEffectFrom(DateFormatter.convertDateToString(hourlyWage.getWithEffectFrom()));

		vo.setRegularHourSalary(hourlyWage.getRegularHourSalary().toString());
		vo.setOverTimerHourSalary(hourlyWage.getOverTimeHourSalary().toString());
		vo.setTax(hourlyWage.getTax().toString());

		return vo;
	}

	/**
	 * method to list salary daily wage by employee
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<SalaryDailyWagesVo> getSalaryDailyWageListByEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<SalaryDailyWagesVo> dailyWagesVos = new ArrayList<SalaryDailyWagesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryDailyWage.class)
				.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<SalaryDailyWage> dailyWages = criteria.list();
		for(SalaryDailyWage dailyWage : dailyWages){
			dailyWagesVos.add(createDailyWageSlaryVo(dailyWage));
		}
		return dailyWagesVos;
	}

	/**
	 * method to list salary hourly wage by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<SalaryHourlyWageVo> getSalaryHourlyWageListByEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<SalaryHourlyWageVo> hourlyWageVos = new ArrayList<SalaryHourlyWageVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryHourlyWage.class)
				.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<SalaryHourlyWage> hourlyWages = criteria.list();
		for(SalaryHourlyWage hourlyWage : hourlyWages){
			hourlyWageVos.add(createHourlyWageSalaryVo(hourlyWage));
		}
		return hourlyWageVos;
	}

	/**
	 * method to get list daily wages of employee by start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<SalaryDailyWagesVo> getSalaryDailyWageListByEmployeeStartDateEndDate(Long employeeId, Date startDate,
			Date endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<SalaryDailyWagesVo> dailyWagesVos = new ArrayList<SalaryDailyWagesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryDailyWage.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("withEffectFrom", startDate)).add(Restrictions.le("withEffectFrom", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<SalaryDailyWage> dailyWages = criteria.list();
		for(SalaryDailyWage dailyWage : dailyWages){
			dailyWagesVos.add(createDailyWageSlaryVo(dailyWage));
		}
		return dailyWagesVos;
	}

	/**
	 * method to get list daily wages of employee by start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<SalaryHourlyWageVo> getSalaryHourlyWageListByEmployeeStartDateEndDate(Long employeeId, Date startDate,
			Date endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<SalaryHourlyWageVo> hourlyWageVos = new ArrayList<SalaryHourlyWageVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryHourlyWage.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("withEffectFrom", startDate)).add(Restrictions.le("withEffectFrom", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<SalaryHourlyWage> hourlyWages = criteria.list();
		for(SalaryHourlyWage hourlyWage : hourlyWages){
			hourlyWageVos.add(createHourlyWageSalaryVo(hourlyWage));
		}
		return hourlyWageVos;
	}

	/**
	 * method to get list salaries by employee
	 * @param employeeId
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<SalaryMonthlyVo> getSalariesByEmployee(Long employeeId) {
		List<SalaryMonthlyVo> salaryMonthlyVos = new ArrayList<SalaryMonthlyVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryMonthly.class);
		criteria.add(Restrictions.eq("employee.id", employeeId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<SalaryMonthly> salaryMonthlies = criteria.list();
		for(SalaryMonthly salaryMonthly : salaryMonthlies){
			salaryMonthlyVos.add(createSalaryMonthlyVo(salaryMonthly));
		}
		return salaryMonthlyVos;
	}


	/**
	 * method to create salary monthly vo
	 * @param salaryMonthly
	 * @return
	 */
	private SalaryMonthlyVo createSalaryMonthlyVo(SalaryMonthly salary){
		SalaryMonthlyVo vo = new SalaryMonthlyVo();
		vo.setEmployeeCode(salary.getEmployee().getEmployeeCode());
		if(salary.getEmployee().getUserProfile()!=null)
			vo.setEmployee(salary.getEmployee().getUserProfile().getFirstName()+" "+salary.getEmployee().getUserProfile().getLastname());

		vo.setAnnualEstimatedSalary(salary.getEstimatedAnnualSalary().toString());
		vo.setAnnualGrossSalary(salary.getGrossSalaryAnnually().toString());
		vo.setAnnualTaxDeduction(salary.getTaxDeductionAnnually().toString());

		vo.setWithEffectFrom(DateFormatter.convertDateToString(salary.getWithEffectFrom()));

		vo.setMonthlyEstimatedSalary(salary.getEstimatedMonthlySalary().toString());
		vo.setMonthlyGrossSalary(salary.getGrossSalaryMonthly().toString());
		vo.setMonthlyTaxDeduction(salary.getTaxDeductionMonthly().toString());

		if(salary.getPayslipItems()!=null || salary.getExtraPayslipItems()!=null)
			vo.setItemsVos(createPayslipItemsVo(salary));

		return vo;
	}

	/**
	 * method to create pyslipItems vos
	 * @param salary
	 * @return
	 */
	private List<SalaryPayslipItemsVo> createPayslipItemsVo(SalaryMonthly salary){
		List<SalaryPayslipItemsVo> itemsVos = new ArrayList<SalaryPayslipItemsVo>();
		if(salary.getPayslipItems()!=null){
			for(SalaryPayslipItems items : salary.getPayslipItems()){
				SalaryPayslipItemsVo itemsVo = new SalaryPayslipItemsVo();
				itemsVo.setStringAmount(items.getAmount().toString());
				itemsVo.setAmount(items.getAmount());
				itemsVo.setItem(items.getPaySlipItem().getTitle());
				itemsVos.add(itemsVo);
			}
		}
		if(salary.getExtraPayslipItems()!=null){
			for(SalaryExtraPayslipItem item : salary.getExtraPayslipItems()){
				SalaryPayslipItemsVo itemsVo = new SalaryPayslipItemsVo();
				itemsVo.setStringAmount(item.getAmount().toString());
				itemsVo.setAmount(item.getAmount());
				itemsVo.setItem(item.getExtraPaySlipItem().getTitle());
				itemsVos.add(itemsVo);
			}
		}

		return itemsVos;
	}

	/**
	 * method to get salary by employee
	 */
	public SalaryMonthlyVo getMonthlySalaryByEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		DetachedCriteria withEffectFrom = DetachedCriteria.forClass(SalaryMonthly.class)
				.setProjection(Projections.max("withEffectFrom"));
		Criteria criteria =  this.sessionFactory.getCurrentSession().createCriteria(SalaryMonthly.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Property.forName("withEffectFrom").eq(withEffectFrom));
		SalaryMonthly salaryMonthly = (SalaryMonthly) criteria.uniqueResult();
		
		if(salaryMonthly!=null)
			return createSalaryMonthlyVo(salaryMonthly);
		else
			return null;
	}

	/**
	 * method to get salary by employee
	 */
	public SalaryMonthlyVo getMonthlySalaryByEmployee(Employee employee) {
		DetachedCriteria withEffectFrom = DetachedCriteria.forClass(SalaryMonthly.class)
				.setProjection(Projections.max("withEffectFrom"));
		Criteria criteria =  this.sessionFactory.getCurrentSession().createCriteria(SalaryMonthly.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Property.forName("withEffectFrom").eq(withEffectFrom));
		SalaryMonthly salaryMonthly = (SalaryMonthly) criteria.uniqueResult();
		if(salaryMonthly!=null)
			return createSalaryMonthlyVo(salaryMonthly);
		else return null;
	}

	/**
	 * method to get salaries by employee , start date & end date
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<SalaryMonthlyVo> getMonthlySalariesByEmployeeStartDateEndDate(Long employeeId, Date startDate,
			Date endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<SalaryMonthlyVo> monthlyVos = new ArrayList<SalaryMonthlyVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryMonthly.class);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("withEffectFrom", startDate)).add(Restrictions.le("withEffectFrom", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<SalaryMonthly> salaryMonthlies = criteria.list();
		for(SalaryMonthly salaryMonthly : salaryMonthlies){
			monthlyVos.add(createSalaryMonthlyVo(salaryMonthly));
		}
		return monthlyVos;
	}

	/**
	 * Method to get hourly wage for a day
	 * @param employeeCode
	 * @param date
	 * @return SalaryHourlyWageVo
	 */
	@SuppressWarnings("unchecked")
	public SalaryHourlyWageVo getHourlyWageForDay(String employeeCode ,Date date) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<SalaryHourlyWage> hourlyWages = this.sessionFactory.getCurrentSession().createCriteria(SalaryHourlyWage.class)
				.add(Restrictions.eq("employee", employee))
				.add(Restrictions.le("withEffectFrom",date)).list();
		int size = hourlyWages.size();
		if(size>0)
		{
			SalaryHourlyWage hourlyWage = hourlyWages.get(size-1);
			return createHourlyWageSalaryVo(hourlyWage);
		}
		else
		{
			return null;
		}

	}

	/**
	 * Method to get daily wage for a day
	 * @param employeeCode
	 * @param date
	 * @return SalaryHourlyWageVo
	 */
	@SuppressWarnings("unchecked")
	public SalaryDailyWagesVo getDailyWageForDay(String employeeCode ,Date date) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<SalaryDailyWage> dailyWages = this.sessionFactory.getCurrentSession().createCriteria(SalaryDailyWage.class)
				.add(Restrictions.eq("employee", employee))
				.add(Restrictions.le("withEffectFrom",date)).list();
		int size = dailyWages.size();
		if(size>0)
		{
			SalaryDailyWage dailyWage = dailyWages.get(size-1);
			return createDailyWageSlaryVo(dailyWage);
		}
		else
		{
			return null;
		}

	}

	/**
	 * method to find applicable salary for given date
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public SalaryMonthlyVo findSalaryForDate(String date , String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		SalaryMonthlyVo salaryMonthlyVo =null;
		SalaryMonthlyVo	salaryVo =  getMonthlySalaryByEmployee(employeeCode);
		if(salaryVo!=null && salaryVo.getWithEffectFrom().equals(date)){
			salaryMonthlyVo = salaryVo;
		}else{
			Date withEffectFrom = DateFormatter.convertStringToDate(date);
			List<SalaryMonthly> salaryMonthlies = null;
			Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryMonthly.class);
			criteria.add(Restrictions.le("withEffectFrom", withEffectFrom));
			criteria.add(Restrictions.eq("employee", employee));
			criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
			salaryMonthlies = criteria.list();
			if(salaryMonthlies.size()==0){
				Criteria criteria2 = this.sessionFactory.getCurrentSession().createCriteria(SalaryMonthly.class);
				criteria2.add(Restrictions.ge("withEffectFrom", withEffectFrom));
				criteria2.add(Restrictions.eq("employee", employee));
				criteria2.setResultTransformer(criteria2.DISTINCT_ROOT_ENTITY);
				criteria2.setFirstResult(0);
				criteria2.setMaxResults(1);
				salaryMonthlies = criteria2.list();
			}
			List<Date> dates = new ArrayList<Date>();
			for(SalaryMonthly salaryMonthly : salaryMonthlies){
				dates.add(salaryMonthly.getWithEffectFrom());
			}
			Date nearByDate = getNearestDate(dates, withEffectFrom);
			Criteria criteria2 = this.sessionFactory.getCurrentSession().createCriteria(SalaryMonthly.class);
			criteria2.add(Restrictions.eq("employee", employee));
			criteria2.add(Restrictions.eq("withEffectFrom", nearByDate));
			SalaryMonthly salaryMonthly = (SalaryMonthly) criteria2.uniqueResult();
			if(salaryMonthly!=null)
				salaryMonthlyVo = createSalaryMonthlyVo(salaryMonthly);
		}
		return salaryMonthlyVo;
	}

	private  Date getNearestDate(List<Date> dates, Date currentDate) {
		long minDiff = -1, currentTime = currentDate.getTime();
		Date minDate = null;
		for (Date date : dates) {
			long diff = Math.abs(currentTime - date.getTime());
			if ((minDiff == -1) || (diff < minDiff)) {
				minDiff = diff;
				minDate = date;
			}
		}
		return minDate;
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<SalaryViewVo> listAllSalaries() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryMonthly.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<SalaryMonthly> monthlies = criteria.list();
		criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryDailyWage.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<SalaryDailyWage> dailyWages = criteria.list();
		criteria = this.sessionFactory.getCurrentSession().createCriteria(SalaryHourlyWage.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<HourlyWages> hourlyWages = criteria.list();
		List<SalaryViewVo> salaryViewVos = new ArrayList<SalaryViewVo>();
		for (SalaryMonthly monthly : monthlies) {
			Employee employee = monthly.getEmployee();
			SalaryViewVo vo = new SalaryViewVo();
			vo.setEmployeeCode(employee.getEmployeeCode());
			vo.setEmployeeId(employee.getId());
			vo.setSalaryType("Monthly");
			vo.setSalaryId(monthly.getId());
			vo.setWithEffectFrom(DateFormatter.convertDateToString(monthly.getWithEffectFrom()));
			vo.setAmount(monthly.getEstimatedMonthlySalary());
			salaryViewVos.add(vo);
		}
		for (SalaryDailyWage dailyWage : dailyWages) {
			Employee employee = dailyWage.getEmployee();
			SalaryViewVo vo = new SalaryViewVo();
			vo.setEmployeeCode(employee.getEmployeeCode());
			vo.setEmployeeId(employee.getId());
			vo.setSalaryType("Daily");
			vo.setSalaryId(dailyWage.getId());
			vo.setWithEffectFrom(DateFormatter.convertDateToString(dailyWage.getWithEffectFrom()));
			vo.setAmount(dailyWage.getSalary());
			salaryViewVos.add(vo);
		}
		for (HourlyWages hourlyWage : hourlyWages) {
			Employee employee = hourlyWage.getEmployee();
			SalaryViewVo vo = new SalaryViewVo();
			vo.setEmployeeCode(employee.getEmployeeCode());
			vo.setEmployeeId(employee.getId());
			vo.setSalaryType("Hourly");
			vo.setSalaryId(hourlyWage.getId());
			vo.setAmount(hourlyWage.getRegularHourAmount());
			salaryViewVos.add(vo);
		}
		return salaryViewVos;
	}
	
	public void deleteSalary(Long id, String type) {
		if (type.equalsIgnoreCase("Monthly")) {
			SalaryMonthly salary = (SalaryMonthly) this.sessionFactory.getCurrentSession()
					.createCriteria(SalaryMonthly.class).add(Restrictions.eq("id", id)).uniqueResult();
			this.sessionFactory.getCurrentSession().delete(salary);
		} else if (type.equalsIgnoreCase("Daily")) {
			SalaryDailyWage salary = (SalaryDailyWage) this.sessionFactory.getCurrentSession()
					.createCriteria(SalaryDailyWage.class).add(Restrictions.eq("id", id)).uniqueResult();
			this.sessionFactory.getCurrentSession().delete(salary);
		} else if (type.equalsIgnoreCase("Hourly")) {
			SalaryHourlyWage salary = (SalaryHourlyWage) this.sessionFactory.getCurrentSession()
					.createCriteria(SalaryHourlyWage.class).add(Restrictions.eq("id", id)).uniqueResult();
			this.sessionFactory.getCurrentSession().delete(salary);
		}
	}
}
