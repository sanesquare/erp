package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.MeetingStatus;
import com.hrms.web.vo.MeetingsVo;

/**
 * 
 * @author Shamsheer
 * @since 13-March-2015
 *
 */
public interface MeetingsStatusDao {

	/**
	 * method to save meeting Status
	 * @param MeetingsVo
	 */
	public void saveMeetingStatus(MeetingsVo meetingsVo);
	
	/**
	 * method to update projectStatus
	 * @param MeetingsVo
	 */
	public void updateProjectStatus(MeetingsVo meetingsVo);
	
	/**
	 * method to delete meeting Status
	 * @param projectVo
	 */
	public void deleteMeetingStatus(Long id);
	
	/**
	 * method to list all meeting Status
	 * @return
	 */
	public List<MeetingsVo> listAllMeetingStatus();
	
	/**
	 * method to find and return meeting Status object
	 * @param id
	 * @return
	 */
	public MeetingStatus findAndReturnMeetingStatusObjById(Long id);
	
	/**
	 * method to find and return meeting Status object by status
	 * @param status
	 * @return
	 */
	public MeetingStatus findAndReturnMeetingStatusObjectByStatus(String status);
}
