package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.DepartmentDao;
import com.hrms.web.dao.PromotionDao;
import com.hrms.web.entities.Complaints;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.DepartmentDocuments;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.EmployeeDesignation;
import com.hrms.web.entities.Promotion;
import com.hrms.web.entities.PromotionDocuments;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.DepartmentDocumentsVo;
import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.PromotionDocumentVo;
import com.hrms.web.vo.PromotionVo;


@Repository
public class PromotionDaoImpl extends AbstractDao implements PromotionDao{

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeService employeeService;
	
	public void savePromotion(PromotionVo promotionVo) {
		logger.info("Inside >>promotionDao save promotion");
		try{
		
			Promotion promotion=new Promotion();
			promotion.setDescription(promotionVo.getPromotionDescription());
			promotion.setNote(promotionVo.getPromotionNote());
			promotion.setPromotionDate(DateFormatter.convertStringToDate(promotionVo.getPromotionDate()));
			promotion.setEmployee(employeeService.findAndReturnEmployeeById(promotionVo.getEmployeeId()));	
			promotion.setForwardTo(setPromotionForwardTo(employeeService.findAndReturnEmployeeByCode(promotionVo.getSuperiorCode()), promotion));
			EmployeeDesignation employeeDesignation=new EmployeeDesignation();
			employeeDesignation.setId(promotionVo.getDesignationId());
			promotion.setPromotiontoTo(employeeDesignation);
			this.sessionFactory.getCurrentSession().saveOrUpdate(promotion);
			promotionVo.setId(promotion.getId());
			
		
	} catch (Exception e) {
		e.printStackTrace();
		logger.info(e.getMessage());
	}

	}
	
	private Set<Employee> setPromotionForwardTo(Employee employee, Promotion promotion) {
		Set<Employee> againstr = new HashSet<Employee>();
		Set<Promotion> promotions=new HashSet<Promotion>();
		//complaints.add(complaint);
		//employee.setComplaints2(complaints); 
		againstr.add(employee);
		return againstr;
	}
	

	public List<PromotionVo> findAllPromotionDetails() {
		logger.info("Inside findAllpromotion");
		List<PromotionVo> promotionVo = new ArrayList<PromotionVo>();
		List<Promotion> promotions = this.sessionFactory.getCurrentSession().createCriteria(Promotion.class).list();
		for (Promotion promotion : promotions) {
			promotionVo.add(createPromotionVo(promotion));
		}
		return promotionVo;
	}
	
	
	/**
	 * method to create departmentVo from department object
	 * 
	 * @param department
	 * @return
	 */
	private PromotionVo createPromotionVo(Promotion promotion) {
		logger.info("inside>> Deparrment... createPromotionVo");
		PromotionVo promotionVo = new PromotionVo();
		promotionVo.setEmployeeId(promotion.getEmployee().getId());
		promotionVo.setEmployeeCode(promotion.getEmployee().getEmployeeCode());
		promotionVo.setPromotionDate(DateFormatter.convertDateToString(promotion.getPromotionDate()));
		promotionVo.setPromotionDescription(promotion.getDescription());
		promotionVo.setPromotionNote(promotion.getNote());
		promotionVo.setId(promotion.getId());
		promotionVo.setDesignationId(promotion.getPromotiontoTo().getId());
		promotionVo.setDesignationName(promotion.getPromotiontoTo().getDesignation());
		promotionVo.setPromotionDocuments(promotion.getPromotionDocuments());
		return promotionVo;
	}

	public void deletePromotion(Long id) {
		logger.info("inside>> DepartmentDao... deleteDepartment");
		//Promotion promotion = findAndReturnPromotionObjectById(id);
		//promotion.setParentDepartment(null);
		this.sessionFactory.getCurrentSession().delete(findAndReturnPromotionObjectById(id));
		
	}
	
	/**
	 * Method to find and return Promotion Object by using promotion Id
	 * 
	 * @param promotionId
	 * @return Promotion
	 */
	public Promotion findAndReturnPromotionObjectById(Long promotionId) {
		logger.info("inside>> PromotionDao... findAndReturnPromotionObjectById");
		return (Promotion) this.sessionFactory.getCurrentSession().createCriteria(Promotion.class)
				.add(Restrictions.eq("id", promotionId)).uniqueResult();
	}

	public PromotionVo findPromotionById(Long promotionId) {
		logger.info("inside>> PromotionDao... findPromotionById");
		Promotion promotion = (Promotion) this.sessionFactory.getCurrentSession().createCriteria(Promotion.class)
				.add(Restrictions.eq("id", promotionId)).uniqueResult();
		return createPromotionVo(promotion);
	}

	public void updatePromotion(PromotionVo promotionVo) {
		logger.info("inside>> PromotionDao... updatePromotion");
		try {
			Promotion promotion = findAndReturnPromotionObjectById(promotionVo.getId());
			promotion.setDescription(promotionVo.getPromotionDescription());
			promotion.setNote(promotionVo.getPromotionNote());
			promotion.setPromotionDate(DateFormatter.convertStringToDate(promotionVo.getPromotionDate()));
			promotion.setEmployee(employeeService.findAndReturnEmployeeById(promotionVo.getEmployeeId()));	
			promotion.setForwardTo(setPromotionForwardTo(employeeService.findAndReturnEmployeeByCode(promotionVo.getSuperiorCode()), promotion));
			EmployeeDesignation employeeDesignation=new EmployeeDesignation();
			employeeDesignation.setId(promotionVo.getDesignationId());
			promotion.setPromotiontoTo(employeeDesignation);
			this.sessionFactory.getCurrentSession().saveOrUpdate(promotion);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
		
	}

	public void deletePromotionDocument(Long docId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PromotionDocuments.class)
				.add(Restrictions.eq("id", docId));
		PromotionDocuments document = (PromotionDocuments) criteria.uniqueResult();
		Promotion promotion = findPromotion(document.getPromotion().getId());
		promotion.getPromotionDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(promotion);
		
	}
	
	public Promotion findPromotion(Long promotionId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Promotion.class)
				.add(Restrictions.eq("id", promotionId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Promotion) criteria.uniqueResult();
	}

	public PromotionVo updatePromotionDocument(PromotionVo promotionVo) {
		Promotion promotion=this.findAndReturnPromotionObjectById(promotionVo.getId());
		promotion.setPromotionDocuments(setDocuments(promotionVo.getPromotionDocument(), promotion));
		this.sessionFactory.getCurrentSession().merge(promotion);
		return createPromotionVo(promotion);
	}
	
	/**
	 * method to update documents
	 * @param documentsVos
	 * @param project
	 * @return
	 */
	private Set<PromotionDocuments> setDocuments(List<PromotionDocumentVo> documentsVos , Promotion promotion){
		Set<PromotionDocuments> documents = new HashSet<PromotionDocuments>();
		for(PromotionDocumentVo vo : documentsVos){
			PromotionDocuments document = new PromotionDocuments();
			document.setPromotion(promotion);
			document.setDocumentName(vo.getFileName());
			document.setUrl(vo.getUrl());
			System.out.println("vo.getUrl().........................................................................."+vo.getUrl());
			documents.add(document);
		}
		return documents;
	}

}
