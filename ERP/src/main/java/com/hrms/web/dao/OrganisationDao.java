package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.Organisation;
import com.hrms.web.vo.OrganisationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-March-2015
 *
 */
public interface OrganisationDao {

	/**
	 * method to save Organisation
	 * 
	 * @param organisationVo
	 */
	public void saveOrganisation(OrganisationVo organisationVo);

	/**
	 * method to update Organisation
	 * 
	 * @param organisationVo
	 */
	public void updateOrganisation(OrganisationVo organisationVo);

	/**
	 * method to delete Organisation
	 * 
	 * @param organisationId
	 */
	public void deleteOrganisation(Long organisationId);

	/**
	 * method to find Organisation by Id
	 * 
	 * @param organisationId
	 * @return Organisation
	 */
	public Organisation findOrganisation(Long organisationId);

	/**
	 * method to find Organisation by code
	 * 
	 * @param organisationCode
	 * @return Organisation
	 */
	public Organisation findOrganisation(String organisationCode);

	/**
	 * method to find Organisation by urt
	 * 
	 * @param organisationUrt
	 * @return Organisation
	 */
	public Organisation findOrganisationByUrt(String organisationUrt);

	/**
	 * method to find Organisation by name
	 * 
	 * @param organisationName
	 * @return Organisation
	 */
	public Organisation findOrganisationByName(String organisationName);

	/**
	 * method to find Organisation by id
	 * 
	 * @param organisationId
	 * @return OrganisationVo
	 */
	public OrganisationVo findOrganisationById(Long organisationId);

	/**
	 * method to find Organisation by code
	 * 
	 * @param organisationCode
	 * @return OrganisationVo
	 */
	public OrganisationVo findOrganisationByCode(String organisationCode);

	/**
	 * method to find Organisation by urt
	 * 
	 * @param organisationUrt
	 * @return OrganisationVo
	 */
	public OrganisationVo findOrganisationByOrgUrt(String organisationUrt);

	/**
	 * method to find Organisation by name
	 * 
	 * @param organisationName
	 * @return OrganisationVo
	 */
	public OrganisationVo findOrganisationByOrgName(String organisationName);

	/**
	 * method to find all Organisation
	 * 
	 * @return List<OrganisationVo>
	 */
	public List<OrganisationVo> findAllOrganisation();

	/**
	 * method to find Organisation
	 * 
	 * @return OrganisationVo
	 */
	public OrganisationVo findOrganisation();
}
