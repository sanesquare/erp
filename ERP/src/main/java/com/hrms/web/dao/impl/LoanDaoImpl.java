package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.LoanDao;
import com.hrms.web.dao.LoanStatusDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Loan;
import com.hrms.web.entities.LoanDocument;
import com.hrms.web.entities.LoanStatus;
import com.hrms.web.entities.UserProfile;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.LoanDocumentVo;
import com.hrms.web.vo.LoanVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Repository
public class LoanDaoImpl extends AbstractDao implements LoanDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private LoanStatusDao loanStatusDao;

	@Autowired
	private TempNotificationDao notificationDao;

	public void saveOrUpdateLoan(LoanVo loanVo) {
		Loan loan = findLoan(loanVo.getLoanId());
		if (loan == null)
			loan = new Loan();
		loan.setNoOfInstallments(loanVo.getNoOfInstallments());
		loan.setPendingInstallments(loan.getNoOfInstallments());
		Employee employee = employeeDao.findAndReturnEmployeeByCode(loanVo.getEmployeeCode());
		loan.setEmployee(employee);
		if (loanVo.getForwardTo().contains("auto")) {
			loan.setStatus(loanStatusDao.findLoanStatus(StatusConstants.APPROVED));
		} else {
			loan.setStatus(loanStatusDao.findLoanStatus(StatusConstants.REQUESTED));
			loan.setForwardTo(setLoanForwadList(loanVo.getForwardTo(), loan));
		}
		if (loanVo.getForwardTo() != null && !loanVo.getForwardTo().contains("auto")) {
			TempNotificationVo notificationVo = new TempNotificationVo();
			loanVo.getForwardTo().remove("auto");
			notificationVo.getEmployeeCodes().addAll(loanVo.getForwardTo());
			UserProfile profile = employee.getUserProfile();
			notificationVo.setSubject(" A Request For Loan Has Been Received From " + profile.getFirstName() + " "
					+ profile.getLastname() + ".  ");
			notificationDao.saveNotification(notificationVo);
		}
		loan.setTitle(loanVo.getLoanTitle());
		loan.setAmount(loanVo.getLoanAmount());
		loan.setLoanDate(DateFormatter.convertStringToDate(loanVo.getLoanDate()));
		loan.setSalaryType(loanVo.getSalaryType());
		loan.setRepaymentAmount(loanVo.getRepaymentAmount());
		// set balance initially
		loan.setBalanceAmount(loan.getAmount());
		loan.setRepaymentStartDate(DateFormatter.convertStringToDate(loanVo.getRepaymentStartDate()));
		loan.setDescription(loanVo.getDescription());
		loan.setNotes(loanVo.getNotes());
		this.sessionFactory.getCurrentSession().saveOrUpdate(loan);
		loanVo.setLoanId(loan.getId());
		loanVo.setRecordedOn(DateFormatter.convertDateToDetailedString(loan.getCreatedOn()));
	}

	/**
	 * method to create loan forward list
	 * 
	 * @param employee
	 * @return forwardTo
	 */
	private Set<Employee> setLoanForwadList(List<String> employee, Loan loan) {
		Set<Employee> forwardTo = loan.getForwardTo();

		Set<Employee> forwardToTemp = new HashSet<Employee>();
		if (forwardTo != null)
			forwardToTemp.addAll(forwardTo);

		Set<Employee> newForwardTo = new HashSet<Employee>();
		if (forwardTo == null || forwardTo.isEmpty())
			forwardTo = new HashSet<Employee>();
		for (String employeeCode : employee) {
			Employee forwader = employeeDao.findAndReturnEmployeeByCode(employeeCode);
			newForwardTo.add(forwader);
			if (!forwardTo.contains(forwader)) {
				forwader.setLoan(loan);
				forwardTo.add(forwader);
			}
		}

		forwardToTemp.removeAll(newForwardTo);
		Iterator<Employee> iterator = forwardToTemp.iterator();
		while (iterator.hasNext()) {
			Employee employee2 = (Employee) iterator.next();
			forwardTo.remove(employee2);
		}
		return forwardTo;
	}

	public void updateLoanStatus(LoanVo loanVo) {
		Loan loan = findLoan(loanVo.getLoanId());
		if (loan == null)
			throw new ItemNotFoundException("Loan Details Not Found");
		loan.setStatus(loanStatusDao.findLoanStatus(loanVo.getStatus()));
		loan.setStatusDescription(loanVo.getStatusDescription());
		loan.setForwardTo(setLoanForwadList(loanVo.getForwardTo(), loan));
		this.sessionFactory.getCurrentSession().merge(loan);
	}

	public void deleteLoan(Long loanId) {
		Loan loan = findLoan(loanId);
		if (loan == null)
			throw new ItemNotFoundException("Loan Details Not Found");
		loan.getForwardTo().clear();
		this.sessionFactory.getCurrentSession().delete(loan);
	}

	public void saveLoanDocument(LoanVo loanVo) {
		Loan loan = findLoan(loanVo.getLoanId());
		if (loan == null)
			throw new ItemNotFoundException("Loan Details Not Found");
		loan.setDocuments(setLoanDocuemntsToLoan(loanVo.getLoanDocuments(), loan));
		this.sessionFactory.getCurrentSession().merge(loan);
	}

	/**
	 * method to set loan documents to loan object
	 * 
	 * @param documents
	 * @param loan
	 * @return loanDocuments
	 */
	private Set<LoanDocument> setLoanDocuemntsToLoan(List<LoanDocumentVo> documents, Loan loan) {
		Set<LoanDocument> loanDocuments = new HashSet<LoanDocument>();
		for (LoanDocumentVo documentVo : documents) {
			LoanDocument document = new LoanDocument();
			document.setName(documentVo.getDocumentName());
			document.setUrl(documentVo.getDocumentUrl());
			document.setLoan(loan);
			loanDocuments.add(document);
		}
		return loanDocuments;
	}

	public void deleteLoanDocument(Long documentId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LoanDocument.class)
				.add(Restrictions.eq("id", documentId));
		LoanDocument document = (LoanDocument) criteria.uniqueResult();
		Loan loan = document.getLoan();
		loan.getDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(loan);
	}

	public Loan findLoan(Long loanId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class)
				.add(Restrictions.eq("id", loanId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Loan) criteria.uniqueResult();
	}

	/**
	 * method to create LoanVo object
	 * 
	 * @param loan
	 * @return loanVo
	 */
	private LoanVo createLoanVo(Loan loan) {
		LoanVo loanVo = new LoanVo();
		UserProfile userProfile = loan.getEmployee().getUserProfile();
		loanVo.setEmployee(userProfile.getFirstName() + " " + userProfile.getLastname());
		loanVo.setPendingInstallments(loan.getPendingInstallments());
		loanVo.setNoOfInstallments(loan.getNoOfInstallments());
		loanVo.setBalanceAmount(loan.getBalanceAmount());
		if (loan.getNoOfInstallments() != loan.getPendingInstallments()) {
			loanVo.setIsView(true);
		}
		loanVo.setEmployeeCode(loan.getEmployee().getEmployeeCode());
		loanVo.setForwardTo(getLoanForwadersList(loan.getForwardTo()));
		loanVo.setForwardToNames(getLoanForwadersNameList(loan.getForwardTo()));
		loanVo.setLoanTitle(loan.getTitle());
		loanVo.setLoanAmount(loan.getAmount());
		loanVo.setLoanDate(DateFormatter.convertDateToString(loan.getLoanDate()));
		loanVo.setSalaryType(loan.getSalaryType());
		loanVo.setRepaymentAmount(loan.getRepaymentAmount());
		loanVo.setRepaymentStartDate(DateFormatter.convertDateToString(loan.getRepaymentStartDate()));
		loanVo.setDescription(loan.getDescription());
		loanVo.setNotes(loan.getNotes());
		if (loan.getStatus() != null)
			loanVo.setStatus(loan.getStatus().getStatus());
		loanVo.setStatusDescription(loan.getStatusDescription());
		loanVo.setLoanDocuments(getLoanDocuments(loan));
		loanVo.setRecordedOn(DateFormatter.convertDateToDetailedString(loan.getCreatedOn()));
		loanVo.setLoanId(loan.getId());
		loanVo.setBalanceAmount(loan.getBalanceAmount());
		return loanVo;
	}

	/**
	 * method to get advance salary forwaders list
	 * 
	 * @param forwaders
	 * @return advanceSalaryFrwdrs
	 */
	private List<String> getLoanForwadersList(Set<Employee> forwaders) {
		List<String> loanFrwdrs = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			loanFrwdrs.add(employee.getEmployeeCode());
		}
		return loanFrwdrs;
	}

	/**
	 * method to get advance salary forwaders name list
	 * 
	 * @param forwaders
	 * @return advanceSalaryFrwdrs
	 */
	private List<String> getLoanForwadersNameList(Set<Employee> forwaders) {
		List<String> loanFrwdrs = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			loanFrwdrs.add(employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname());
		}
		return loanFrwdrs;
	}

	/**
	 * method to get all documents of a loan
	 * 
	 * @param loan
	 * @return loanDocuments
	 */
	private List<LoanDocumentVo> getLoanDocuments(Loan loan) {
		List<LoanDocumentVo> loanDocuments = new ArrayList<LoanDocumentVo>();
		Iterator<LoanDocument> iterator = loan.getDocuments().iterator();
		while (iterator.hasNext()) {
			LoanDocumentVo documentVo = new LoanDocumentVo();
			LoanDocument loanDocument = (LoanDocument) iterator.next();
			documentVo.setDocumentName(loanDocument.getName());
			documentVo.setDocumentUrl(loanDocument.getUrl());
			documentVo.setLoanDocId(loanDocument.getId());
			loanDocuments.add(documentVo);
		}
		return loanDocuments;
	}

	public LoanVo findLoanById(Long loanId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class)
				.add(Restrictions.eq("id", loanId));
		if (criteria.uniqueResult() == null)
			return null;
		return createLoanVo((Loan) criteria.uniqueResult());
	}

	public List<LoanVo> findLoanByEmployee(Employee employee) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class)
				.add(Restrictions.eq("employee", employee));
		return iterateCriteria(criteria);
	}

	public List<LoanVo> findLoanByDate(Date loanDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class)
				.add(Restrictions.eq("loanDate", loanDate));
		return iterateCriteria(criteria);
	}

	public List<LoanVo> findLoanByRepaymentDate(Date repaymentDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class)
				.add(Restrictions.eq("repaymentStartDate", repaymentDate));
		return iterateCriteria(criteria);
	}

	public List<LoanVo> findLoanByStatus(LoanStatus loanStatus) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class)
				.add(Restrictions.eq("status", loanStatus));
		return iterateCriteria(criteria);
	}

	public List<LoanVo> findAllLoan() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class);
		return iterateCriteria(criteria);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	private List<LoanVo> iterateCriteria(Criteria criteria) {
		List<LoanVo> loanVos = new ArrayList<LoanVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Loan> loans = criteria.list();
		for (Loan loan : loans)
			loanVos.add(createLoanVo(loan));
		return loanVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<LoanVo> findLoanByBetweenDates(Long employeeId, Date startDate, Date endDate) {
		List<LoanVo> loanVos = new ArrayList<LoanVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class)
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeById(employeeId)))
				.add(Restrictions.ge("loanDate", startDate)).add(Restrictions.le("loanDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Loan> loans = criteria.list();
		for (Loan loan : loans)
			loanVos.add(createLoanVo(loan));
		return loanVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<LoanVo> findAllLoanByEmployee(String employeeCode) {
		List<LoanVo> loanVos = new ArrayList<LoanVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class);
		criteria.createAlias("forwardTo", "employee");
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("createdBy", employee.getUser());
		Criterion receiver = Restrictions.eq("employee.employeeCode", employeeCode);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Loan> loans = criteria.list();
		for (Loan loan : loans)
			loanVos.add(createLoanVo(loan));
		return loanVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<LoanVo> findLoanByStatusAndDates(String employeeCode, String status, Date startDate, Date endDate) {
		List<LoanVo> loanVos = new ArrayList<LoanVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class)
				.add(Restrictions.eq("status", loanStatusDao.findLoanStatus(status)))
				.add(Restrictions.ge("loanDate", startDate)).add(Restrictions.le("loanDate", endDate))
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeByCode(employeeCode)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Loan> loans = criteria.list();
		for (Loan loan : loans)
			loanVos.add(createLoanVo(loan));
		return loanVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<Loan> findApprovedLoanForSalary(String endDate, Employee employee) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Loan.class);
		criteria.add(Restrictions.eq("status", loanStatusDao.findLoanStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.le("repaymentStartDate", DateFormatter.convertStringToDate(endDate)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Loan> loans = criteria.list();
		return loans;
	}

	public void updateLoanObject(Loan loan) {
		this.sessionFactory.getCurrentSession().update(loan);
	}
}
