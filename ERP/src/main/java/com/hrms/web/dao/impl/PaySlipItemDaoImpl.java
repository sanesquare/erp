package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.PaySlipItemDao;
import com.hrms.web.entities.PaySlipItem;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.PaySlipItemVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Repository
public class PaySlipItemDaoImpl extends AbstractDao implements PaySlipItemDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveOrUpdatePaySlipItem(PaySlipItemVo paySlipItemVo) {
		PaySlipItemVo slipItemVo = findPaySlipItemByTitle(paySlipItemVo.getTitle());
		PaySlipItem paySlipItem = findPaySlipItem(paySlipItemVo.getPaySlipItemId());
		if (paySlipItem == null) {
			paySlipItem = new PaySlipItem();
			if (slipItemVo != null)
				throw new DuplicateItemException("Item with this title already available");
		} else {
			if (slipItemVo != null && slipItemVo.getPaySlipItemId() != paySlipItemVo.getPaySlipItemId())
				throw new DuplicateItemException("Item with this title already available");
		}
		paySlipItem.setTitle(paySlipItemVo.getTitle());
		paySlipItem.setType(paySlipItemVo.getType());
		paySlipItem.setTaxAllowance(Boolean.valueOf(paySlipItemVo.getTaxAllowance()));
		paySlipItem.setCalculation(paySlipItemVo.getCalculation());
		paySlipItem.setEffectFrom(DateFormatter.convertStringToDate(paySlipItemVo.getEffectFrom()));
		this.sessionFactory.getCurrentSession().saveOrUpdate(paySlipItem);
	}

	public void deletePaySlipItem(Long paySlipItemId) {
		PaySlipItem paySlipItem = findPaySlipItem(paySlipItemId);
		if (paySlipItem == null)
			throw new ItemNotFoundException("PaySlip Item Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(paySlipItem);
	}

	public PaySlipItem findPaySlipItem(Long paySlipItemId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySlipItem.class)
				.add(Restrictions.eq("id", paySlipItemId));
		if (criteria.uniqueResult() == null)
			return null;
		return (PaySlipItem) criteria.uniqueResult();
	}

	/**
	 * method to create PaySlipItemVo object
	 * 
	 * @param paySlipItem
	 * @return paySlipItemVo
	 */
	private PaySlipItemVo createPaySlipItemVo(PaySlipItem paySlipItem) {
		PaySlipItemVo paySlipItemVo = new PaySlipItemVo();
		paySlipItemVo.setPaySlipItemId(paySlipItem.getId());
		paySlipItemVo.setTitle(paySlipItem.getTitle());
		paySlipItemVo.setType(paySlipItem.getType());
		paySlipItemVo.setTaxAllowance(String.valueOf(paySlipItem.getTaxAllowance()));
		paySlipItemVo.setCalculation(paySlipItem.getCalculation());
		paySlipItemVo.setEffectFrom(DateFormatter.convertDateToString(paySlipItem.getEffectFrom()));
		return paySlipItemVo;
	}

	public PaySlipItemVo findPaySlipItemById(Long paySlipItemId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySlipItem.class)
				.add(Restrictions.eq("id", paySlipItemId));
		if (criteria.uniqueResult() == null)
			return null;
		return createPaySlipItemVo((PaySlipItem) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PaySlipItemVo> findAllPaySlipItem() {
		List<PaySlipItemVo> paySlipItemVos = new ArrayList<PaySlipItemVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySlipItem.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PaySlipItem> items = criteria.list();
		for (PaySlipItem item : items)
			paySlipItemVos.add(createPaySlipItemVo(item));
		return paySlipItemVos;
	}

	public PaySlipItemVo findPaySlipItemByTitle(String title) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySlipItem.class)
				.add(Restrictions.eq("title", title));
		if (criteria.uniqueResult() == null)
			return null;
		return createPaySlipItemVo((PaySlipItem) criteria.uniqueResult());
	}

	public PaySlipItem findPaySlipItem(String title) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySlipItem.class)
				.add(Restrictions.eq("title", title));
		if (criteria.uniqueResult() == null)
			return null;
		return (PaySlipItem) criteria.uniqueResult();
	}

}
