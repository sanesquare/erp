package com.hrms.web.dao;

import com.hrms.web.entities.EsiSyntax;
import com.hrms.web.vo.EsiSyntaxVo;

/**
 * 
 * @author Jithin Mohan
 * @since 5-May-2015
 *
 */
public interface EsiSyntaxDao {

	/**
	 * method to save or update EsiSyntax
	 * 
	 * @param EsiSyntaxVo
	 */
	public void saveOrUpdateEsiSyntax(EsiSyntaxVo esiSyntaxVo);

	/**
	 * method to delete EsiSyntax
	 * 
	 * @param esiSyntaxId
	 */
	public void deleteEsiSyntax(Long esiSyntaxId);

	/**
	 * method to find EsiSyntax by id
	 * 
	 * @param esiSyntaxId
	 * @return EsiSyntax
	 */
	public EsiSyntax findEsiSyntax(Long esiSyntaxId);

	/**
	 * method to find EsiSyntax by id
	 * 
	 * @param esiSyntaxId
	 * @return EsiSyntaxVo
	 */
	public EsiSyntaxVo findEsiSyntaxId(Long esiSyntaxId);

	/**
	 * method to find EsiSyntax
	 * 
	 * @return EsiSyntaxVo
	 */
	public EsiSyntaxVo findEsiSyntax();
}
