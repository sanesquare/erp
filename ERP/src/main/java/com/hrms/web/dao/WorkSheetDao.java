package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.WorkSheet;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.WorkSheetVo;

/**
 * 
 * @author Vinutha
 * @since 1-April-2015
 *
 */
public interface WorkSheetDao {
	/**
	 * method to save basic info
	 * @param worksheetVo
	 * @return
	 */
	public Long saveBasicInfo(WorkSheetVo worksheetVo);
	/**
	 * method to update basic info
	 * @param worksheetVo
	 */
	public void updateBasicInfo(WorkSheetVo worksheetVo);
	/**
	 * mehtod to save project and task
	 * @param worksheetVo
	 */
	public void saveProjectTask(WorkSheetVo worksheetVo);
	/**
	 * mehtod to update project and task
	 * @param worksheetVo
	 */
	public void updateProjectTask(WorkSheetVo worksheetVo);
	/**
	 * method to save description
	 * @param worksheetVo
	 */
	public void saveDesc(WorkSheetVo worksheetVo);
	/**
	 * method to save additional information
	 * @param worksheetVo
	 */
	public void saveAddInfo(WorkSheetVo worksheetVo);
	/**
	 * method to return object by id
	 * @param id
	 * @return
	 */
	public WorkSheet findAndReturnObjectById(long id);
	/**
	 * method to update documents
	 * @param workSheetVo
	 */
	public void updateDocuments(WorkSheetVo workSheetVo);
	/**
	 * method to list all worksheets
	 * @return
	 */
	public List<WorkSheetVo> listAllWorksheets();
	/**
	 * method to list employee worksheets
	 * @return
	 */
	public List<WorkSheetVo> listEmployeeWorkSheets(String employeeCode);
	/**
	 * method to find WorkSheetVo by id
	 * @param id
	 * @return
	 */
	public WorkSheetVo  findWorksheetVoById(Long id);
	/**
	 * method to delete documents
	 * @param workSheetVo
	 */
	public void deleteDocument(String url);
	/**
	 * method to delete work sheet by id
	 * @param id
	 */
	public void deleteWorkSheetById(Long id);
	/**
	 * Method to list workSheet withtin given date range
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<WorkSheetVo> listWorkSheetWithinDateRange(String startDate , String endDate);
	/**
	 * Method to list work sheet of an employee within date range
	 * @param empCode
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<WorkSheetVo> listWorkSheetWithEmployeeCodeWithinDateRange(String empCode , String startDate , String endDate );

}
