package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.OvertimeStatusDao;
import com.hrms.web.entities.OvertimeStatus;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.OvertimeStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Repository
public class OvertimeStatusDaoImpl extends AbstractDao implements OvertimeStatusDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveOvertimeStatus(OvertimeStatusVo overtimeStatusVo) {
		for (String status : StringSplitter.splitWithComma(overtimeStatusVo.getStatus())) {
			if (findOvertimeStatus(status.trim()) != null)
				throw new DuplicateItemException("Duplicate Overtime Status : " + status.trim());
			OvertimeStatus overtimeStatus = new OvertimeStatus();
			overtimeStatus.setStatus(status.trim());
			this.sessionFactory.getCurrentSession().save(overtimeStatus);
		}
	}

	public void updateOvertimeStatus(OvertimeStatusVo overtimeStatusVo) {
		// TODO Auto-generated method stub

	}

	public void deleteOvertimeStatus(Long overtimeStatusId) {
		OvertimeStatus overtimeStatus = findOvertimeStatus(overtimeStatusId);
		if (overtimeStatus == null)
			throw new ItemNotFoundException("Overtime Status Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(overtimeStatus);
	}

	public OvertimeStatus findOvertimeStatus(Long overtimeStatusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OvertimeStatus.class)
				.add(Restrictions.eq("id", overtimeStatusId));
		if (criteria.uniqueResult() == null)
			return null;
		return (OvertimeStatus) criteria.uniqueResult();
	}

	public OvertimeStatus findOvertimeStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OvertimeStatus.class)
				.add(Restrictions.eq("status", status));
		if (criteria.uniqueResult() == null)
			return null;
		return (OvertimeStatus) criteria.uniqueResult();
	}

	/**
	 * method to create OvertimeStatusVo object
	 * 
	 * @param overtimeStatus
	 * @return overtimeStatusVo
	 */
	private OvertimeStatusVo createOvertimeStatusVo(OvertimeStatus overtimeStatus) {
		OvertimeStatusVo overtimeStatusVo = new OvertimeStatusVo();
		overtimeStatusVo.setOvertimeStatusId(overtimeStatus.getId());
		overtimeStatusVo.setStatus(overtimeStatus.getStatus());
		return overtimeStatusVo;
	}

	public OvertimeStatusVo findOvertimeStatusById(Long overtimeStatusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OvertimeStatus.class)
				.add(Restrictions.eq("id", overtimeStatusId));
		if (criteria.uniqueResult() == null)
			return null;
		return createOvertimeStatusVo((OvertimeStatus) criteria.uniqueResult());
	}

	public OvertimeStatusVo findOvertimeStatusByStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OvertimeStatus.class)
				.add(Restrictions.eq("status", status));
		if (criteria.uniqueResult() == null)
			return null;
		return createOvertimeStatusVo((OvertimeStatus) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<OvertimeStatusVo> findAllOvertimeStatus() {
		List<OvertimeStatusVo> overtimeStatusVos = new ArrayList<OvertimeStatusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OvertimeStatus.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<OvertimeStatus> overtimeStatuses = criteria.list();
		for (OvertimeStatus overtimeStatus : overtimeStatuses)
			overtimeStatusVos.add(createOvertimeStatusVo(overtimeStatus));
		return overtimeStatusVos;
	}

}
