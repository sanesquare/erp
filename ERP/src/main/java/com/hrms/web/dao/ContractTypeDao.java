package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.ContractType;
import com.hrms.web.vo.ContractTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public interface ContractTypeDao {

	/**
	 * method to save new contractType
	 * 
	 * @param contractTypeVo
	 */
	public void saveContractType(ContractTypeVo contractTypeVo);

	/**
	 * method to update contracttype information
	 * 
	 * @param contractTypeVo
	 */
	public void updateContractType(ContractTypeVo contractTypeVo);

	/**
	 * method to delete contracttype information
	 * 
	 * @param typeId
	 */
	public void deleteContractType(Long typeId);

	/**
	 * method to find contractType by id
	 * 
	 * @param typeId
	 * @return contractType
	 */
	public ContractType findContractType(Long typeId);

	/**
	 * method to find contractType by id
	 * 
	 * @param typeId
	 * @return contractTypevo
	 */
	public ContractTypeVo findContractTypeById(Long typeId);

	/**
	 * method to find contract type by type
	 * 
	 * @param typeName
	 * @return contractType
	 */
	public ContractType findContractType(String typeName);

	/**
	 * method to find contractType by type
	 * 
	 * @param typeName
	 * @return contractType
	 */
	public ContractTypeVo findContractTypeByType(String typeName);

	/**
	 * method to find all contractType
	 * 
	 * @return List<ContractTypeVo>
	 */
	public List<ContractTypeVo> findAllContractType();
}
