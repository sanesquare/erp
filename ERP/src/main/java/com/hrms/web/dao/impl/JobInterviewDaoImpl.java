package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CandidateStatusDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.JobCandidatesDao;
import com.hrms.web.dao.JobInterviewDao;
import com.hrms.web.dao.JobPostDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.CandidateStatus;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.EmployeeDocuments;
import com.hrms.web.entities.JobCandidates;
import com.hrms.web.entities.JobInterview;
import com.hrms.web.entities.JobInterviewDocuments;
import com.hrms.web.entities.JobPost;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.CandidatesStatusVo;
import com.hrms.web.vo.EmployeeDocumentsVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.InterviewDocumentsVo;
import com.hrms.web.vo.JobCandidatesVo;
import com.hrms.web.vo.JobInterviewVo;
import com.hrms.web.vo.JobPostVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Vinutha
 * @since 23-March-2015
 *
 */
@Repository
public class JobInterviewDaoImpl extends AbstractDao implements JobInterviewDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private JobPostDao jobPostDao;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private JobCandidatesDao candDao;

	@Autowired
	private CandidateStatusDao statusDao;
	
	@Autowired
	private TempNotificationDao notificationDao;

	/**
	 * method to save interview basic info
	 * 
	 * @param interviewVo
	 * @return Long id
	 */
	public Long saveJobInterviewInfo(JobInterviewVo interviewVo) {
		JobInterview interview = new JobInterview();
		interview.setJobPost(jobPostDao.findAndReturnJobPostObjectById(interviewVo.getJobPostId()));
		interview.setInterviewDate(DateFormatter.convertStringToDate(interviewVo.getInterviewDate()));
		interview.setInterviewTime(DateFormatter.convertStringTimeToSqlTime(interviewVo.getInterviewTime()));
		if(interviewVo.getInterViewerIds()!=null){
			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject("An Interview Has Been Scheduled on "+interviewVo.getInterviewDate()+" "+interviewVo.getInterviewTime()+".");
			notificationVo.getEmployeeIds().addAll(interviewVo.getInterViewerIds());
			notificationDao.saveNotification(notificationVo);
			interview.setInterviewers(createInterviewers(interviewVo));
		}
		if(interviewVo.getStatusId()!=null && interviewVo.getSelectionNumber()!=null)
			interview.setInterviewee(createInterviewees(interviewVo , interview));
		interview.setPlaceOfInterview(interviewVo.getPlace());
		/*if(interviewVo.getStatusId()!=null)
			interview.setStatus(createStatus(interviewVo , interview));*/
		
		interview.setSelectionNumber(Integer.parseInt(interviewVo.getSelectionNumber()));
		Long saveId = (Long) this.sessionFactory.getCurrentSession().save(interview);
		JobInterview inter  = findAndReturnInterviewObjectById(saveId);
		for(Long id : interviewVo.getStatusId()){
			inter.getStatus().add(statusDao.findAndReturnStatusObjectById(id));
		}
		/*if(interviewVo.getStatusId()!=null)
			interview.setStatus(createStatus(interviewVo , inter));*/
		this.sessionFactory.getCurrentSession().merge(inter);
		return saveId;
	}

	/**
	 * method to update job interview info
	 * 
	 * @param interviewVo
	 */
	public void updateJobInterviewInfo(JobInterviewVo interviewVo) {

		JobInterview interview = (JobInterview) this.sessionFactory.getCurrentSession()
				.createCriteria(JobInterview.class).add(Restrictions.eq("id", interviewVo.getJobInterviewId()))
				.uniqueResult();
		interview.setJobPost(jobPostDao.findAndReturnJobPostObjectById(interviewVo.getJobPostId()));
		interview.setInterviewDate(DateFormatter.convertStringToDate(interviewVo.getInterviewDate()));
		interview.setInterviewTime(DateFormatter.convertStringTimeToSqlTime(interviewVo.getInterviewTime()));
		if(interviewVo.getInterViewerIds()!=null)
			interview.setInterviewers(createInterviewers(interviewVo));
		if(interviewVo.getStatusId()!=null && interviewVo.getSelectionNumber()!=null){
			interview.setInterviewee(createInterviewees(interviewVo , interview));
		}
		if(interviewVo.getStatusId()!=null)
			interview.setStatus(createStatus(interviewVo , interview));
		// interview.setStatus(interviewVo.getStatus());
		interview.setPlaceOfInterview(interviewVo.getPlace());
		interview.setSelectionNumber(Integer.parseInt(interviewVo.getSelectionNumber()));
		this.sessionFactory.getCurrentSession().merge(interview);
	}

	/**
	 * method to delete job interview
	 * 
	 * @param interviewVo
	 */
	public void deleteJobInterview(JobInterviewVo interviewVo) {
		JobInterview interview = (JobInterview) this.sessionFactory.getCurrentSession()
				.createCriteria(JobInterview.class).add(Restrictions.eq("id", interviewVo.getJobInterviewId()))
				.uniqueResult();
		Set<JobCandidates> interviewees = interview.getInterviewee();
		for (JobCandidates jobCandidates : interviewees) {
			jobCandidates.setJobInterview(null);
			this.sessionFactory.getCurrentSession().merge(jobCandidates);
		}
		Set<JobInterviewDocuments> documents = interview.getInterviewDocuments();
		SFTPOperation operation = new SFTPOperation();
		for (JobInterviewDocuments document : documents) {
			operation.removeFileFromSFTP(document.getDocumentPath());

		}
		//interview.getInterviewee().clear();
		this.sessionFactory.getCurrentSession().delete(interview);
	}

	/**
	 * method to delete job interview by id
	 * 
	 * @param id
	 */
	public void deleteJobInterviewById(Long id) {
		JobInterview interview = (JobInterview) this.sessionFactory.getCurrentSession()
				.createCriteria(JobInterview.class).add(Restrictions.eq("id", id)).uniqueResult();
		Set<JobCandidates> interviewees = interview.getInterviewee();
		for (JobCandidates jobCandidates : interviewees) {
			jobCandidates.setJobInterview(null);
			this.sessionFactory.getCurrentSession().merge(jobCandidates);
		}
		Set<JobInterviewDocuments> documents = interview.getInterviewDocuments();
		SFTPOperation operation = new SFTPOperation();
		for (JobInterviewDocuments document : documents) {
			operation.removeFileFromSFTP(document.getDocumentPath());

		}
		this.sessionFactory.getCurrentSession().delete(interview);
	}

	/**
	 * method to list all interviews
	 * 
	 * @return List<JobInterviewVo>
	 */
	@SuppressWarnings("unchecked")
	public List<JobInterviewVo> listAllInterviews() {

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobInterview.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobInterview> interviews = criteria.list();
		List<JobInterviewVo> interviewVos = new ArrayList<JobInterviewVo>();
		for (JobInterview interview : interviews) {
			interviewVos.add(createInterviewVo(interview));
		}
		return interviewVos;
	}

	/**
	 * method to find and return interview object
	 * 
	 * @param id
	 */
	public JobInterview findAndReturnInterviewObjectById(Long id) {

		return (JobInterview) this.sessionFactory.getCurrentSession().createCriteria(JobInterview.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * method to list interviews based on place
	 * 
	 * @param place
	 * @return List<JobInterviewvo>
	 */
	public List<JobInterviewVo> listInterviewsBasedOnPlace(String place) {
		// List<JobInterviewVo>
		return null;
	}

	/**
	 * method to create set of interviewers
	 * 
	 * @param interviewVo
	 * @return
	 */
	private Set<Employee> createInterviewers(JobInterviewVo interviewVo) {
		Set<Employee> interviewers = new HashSet<Employee>();
		for (Long id : interviewVo.getInterViewerIds()) {
			interviewers.add(employeeDao.findAndReturnEmployeeById(id));
		}
		return interviewers;

	}

	/**
	 * method to create set of interviewees
	 * 
	 * @param interviewVo
	 * @return
	 */
	private Set<JobCandidates> createInterviewees(JobInterviewVo interviewVo , JobInterview interview) {
		List<JobCandidates> interviewees = candDao.listCandidatesBasedOnStatusAndNumber(interviewVo);
		Set<JobCandidates> intervieweesSet = new HashSet<JobCandidates>();
		if(interviewees.size()>0){
			for(JobCandidates candidate : interviewees)
			{
				if(checkCandidateAge(candidate, interview.getJobPost()))
				{
					candidate.setJobInterview(interview);
					this.sessionFactory.getCurrentSession().merge(candidate);
					intervieweesSet.add(candidate);
				}
			}
		}
		return intervieweesSet;
	}
	/**
	 * Method to check if candidate belongs to age group for specific job post
	 * @param candidate
	 * @param post
	 * @return
	 */
	public boolean checkCandidateAge(JobCandidates candidate , JobPost post){
		
		int age = DateFormatter.getAge(candidate.getBirthDate());
		if(age >= post.getAgeStart()&& age <= post.getAgeEnd())
			return true;
		else
			return false;
	}

	/**
	 * method to create status
	 * 
	 * @param interviewVo
	 * @return
	 */
	private Set<CandidateStatus> createStatus(JobInterviewVo interviewVo , JobInterview interview) {
		Set<CandidateStatus> candidateStatus = new HashSet<CandidateStatus>();
		for (Long statusId : interviewVo.getStatusId()) {
			CandidateStatus sts = statusDao.findAndReturnStatusObjectById(statusId);
			candidateStatus.add(sts);
		}
		return candidateStatus;
	}

	/**
	 * method to save description
	 * 
	 * @param interviewVo
	 */
	public void saveInterviewDesc(JobInterviewVo interviewVo) {
		JobInterview interview = (JobInterview) this.sessionFactory.getCurrentSession()
				.createCriteria(JobInterview.class).add(Restrictions.eq("id", interviewVo.getJobInterviewId()))
				.uniqueResult();
		interview.setDescription(interviewVo.getDescription());
		this.sessionFactory.getCurrentSession().merge(interview);
	}

	/**
	 * method to save additional info
	 * 
	 * @param interviewVo
	 */
	public void saveInterviewAddInfo(JobInterviewVo interviewVo) {
		JobInterview interview = (JobInterview) this.sessionFactory.getCurrentSession()
				.createCriteria(JobInterview.class).add(Restrictions.eq("id", interviewVo.getJobInterviewId()))
				.uniqueResult();
		interview.setAdditionalInfo(interviewVo.getAdditionalInfo());
		this.sessionFactory.getCurrentSession().merge(interview);
	}

	/**
	 * method to create interviewVo
	 * 
	 * @param interview
	 */
	private JobInterviewVo createInterviewVo(JobInterview interview) {

		JobInterviewVo interviewVo = new JobInterviewVo();
		interviewVo.setJobInterviewId(interview.getId());
		if (interview.getJobPost() != null) {
			interviewVo.setJobPostId(interview.getJobPost().getId());
			interviewVo.setJobPost(interview.getJobPost().getJobTitle());
		}
		interviewVo.setInterviewers(createEmpCodes(interview.getInterviewers()));
		interviewVo.setInterViewerIds(createInterviewerIds(interview.getInterviewers()));
		interviewVo.setStatusses(createStatuses(interview.getStatus()));
		interviewVo.setStatus(createStatusVo(interview.getStatus()));
		interviewVo.setStatusId(createStatusIds(interview.getStatus()));
		interviewVo.setInterviewees(createInterviewees(interview));
		// interviewVo.setInterviewees(interview.getInterviewee());
		interviewVo.setIntervieweeIds(createIntervieweeIds(interview));
		interviewVo.setSelectionNumber(String.valueOf(interview.getSelectionNumber()));
		interviewVo.setPlace(interview.getPlaceOfInterview());
		if (interview.getInterviewDate() != null)
			interviewVo.setInterviewDate(DateFormatter.convertDateToString(interview.getInterviewDate()));
		interviewVo.setInterviewTime(DateFormatter.convertSqlTimeToString(interview.getInterviewTime()));
		interviewVo.setDescription(interview.getDescription());
		interviewVo.setAdditionalInfo(interview.getAdditionalInfo());
		interviewVo.setCreatedBy(interview.getCreatedBy().getUserName());
		if (interview.getCreatedOn() != null)
			interviewVo.setCreatedOn(DateFormatter.convertDateToString(interview.getCreatedOn()));
		if (interview.getInterviewDocuments() != null) {
			List<InterviewDocumentsVo> documentsVos = new ArrayList<InterviewDocumentsVo>();
			for (JobInterviewDocuments documents : interview.getInterviewDocuments()) {
				documentsVos.add(createDocumentsVo(documents));
			}
			interviewVo.setDocumentVos(documentsVos);
		}
		return interviewVo;
	}

	/**
	 * Method to get interviewer id
	 * 
	 * @param employee
	 * @return
	 */
	private Set<Long> createInterviewerIds(Set<Employee> employee) {
		Set<Long> ids = new HashSet<Long>();
		for (Employee emp : employee) {
			ids.add(emp.getId());
		}
		return ids;
	}
	private Set<Long> createIntervieweeIds(JobInterview interview) {
		List<JobCandidatesVo> candidates = candDao.findCandidatesByInterview(interview.getId());
		Set<Long> ids = new HashSet<Long>();
		for (JobCandidatesVo cand : candidates) {
			ids.add(cand.getJobCandidateId());
		}
		return ids;
	}
	private List<String> createInterviewees(JobInterview interview) {
		Set<JobCandidates> candidates = interview.getInterviewee();
		List<String> cands = new ArrayList<String>();
		for (JobCandidates cand : candidates) {
			cands.add(cand.getFirstName()+" "+cand.getLastName());
		}
		return cands;
	}
	/**
	 * Method to create status id
	 * 
	 * @param status
	 * @return
	 */
	private List<Long> createStatusIds(Set<CandidateStatus> status) {
		List<Long> ids = new ArrayList<Long>();
		for (CandidateStatus sts : status) {
			ids.add(sts.getId());
		}
		return ids;
	}

	/**
	 * Method to get employee codes
	 * 
	 * @param employee
	 * @return Set<String>
	 */
	private Set<String> createEmpCodes(Set<Employee> employee) {

		Set<String> empCodes = new HashSet<String>();
		for (Employee emp : employee) {
			empCodes.add(emp.getEmployeeCode());
		}
		return empCodes;

	}

	/**
	 * Method to create set of status
	 * 
	 * @param status
	 * @return Set<CandidatesStatusVo>
	 */
	private Set<CandidatesStatusVo> createStatusVo(Set<CandidateStatus> status) {

		Set<CandidatesStatusVo> result = new HashSet<CandidatesStatusVo>();
		for (CandidateStatus status1 : status) {
			CandidatesStatusVo statusVo = new CandidatesStatusVo();
			statusVo.setStatus(status1.getStatus());
			statusVo.setStatusId(status1.getId());
			result.add(statusVo);
		}
		return result;
	}

	/**
	 * Method to get set of status name
	 * 
	 * @param employee
	 * @return Set<String>
	 */
	private Set<String> createStatuses(Set<CandidateStatus> status) {

		Set<String> statuses = new HashSet<String>();
		for (CandidateStatus sts : status) {
			statuses.add(sts.getStatus());
		}
		return statuses;

	}

	/**
	 * method to find Job Interview by id
	 * 
	 * @param Long id
	 * 
	 */
	public JobInterviewVo findJobInterviewById(Long id) {
		JobInterview interview = (JobInterview) this.sessionFactory.getCurrentSession()
				.createCriteria(JobInterview.class).add(Restrictions.eq("id", id)).uniqueResult();
		return createInterviewVo(interview);
	}

	/**
	 * method to upload interview documents
	 * 
	 * @param interviewVo
	 */
	public void updateDocuments(JobInterviewVo interviewVo) {
		JobInterview interview = findAndReturnInterviewObjectById(interviewVo.getJobInterviewId());
		interview.setInterviewDocuments(setDocuments(interviewVo.getDocumentVos(), interview));
		this.sessionFactory.getCurrentSession().merge(interview);

	}

	/**
	 * method to get set of interview documents
	 * 
	 * @param documentVos
	 * @param interview
	 * @return
	 */
	private Set<JobInterviewDocuments> setDocuments(List<InterviewDocumentsVo> documentVos, JobInterview interview) {
		Set<JobInterviewDocuments> interviewDocuments = new HashSet<JobInterviewDocuments>();
		for (InterviewDocumentsVo documentsVo : documentVos) {
			JobInterviewDocuments documents = new JobInterviewDocuments();
			documents.setFileName(documentsVo.getFileName());
			documents.setDocumentPath(documentsVo.getDocumentUrl());
			documents.setJobInterview(interview);
			interviewDocuments.add(documents);
		}
		return interviewDocuments;

	}

	/**
	 * method to delete document from interview
	 * 
	 * @param String
	 *            url
	 */
	public void deleteDocument(String url) {

		JobInterviewDocuments document = (JobInterviewDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(JobInterviewDocuments.class).add(Restrictions.eq("documentPath", url)).uniqueResult();

		JobInterview interview = findAndReturnInterviewObjectById(document.getJobInterview().getId());

		interview.getInterviewDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(interview);

	}

	/**
	 * method to create InterviewDocumentsvo
	 * 
	 * @param documents
	 * @return
	 */
	private InterviewDocumentsVo createDocumentsVo(JobInterviewDocuments documents) {
		InterviewDocumentsVo vo = new InterviewDocumentsVo();
		vo.setDocumentId(documents.getId());
		vo.setDocumentUrl(documents.getDocumentPath());
		vo.setFileName(documents.getFileName());
		return vo;
	}

	/**
	 * Method to list interviews based on date
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<JobInterviewVo> listInterviewsByDateRange(Date startDate, Date endDate) {
		List<JobInterviewVo> resultVos = new ArrayList<JobInterviewVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobInterview.class)
				.add(Restrictions.ge("interviewDate", startDate)).add(Restrictions.le("interviewDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("interviewDate"));

		List<JobInterview> interviews = criteria.list();
		for (JobInterview interview : interviews) {
			resultVos.add(createInterviewVo(interview));
		}
		return resultVos;
	}

	/**
	 * Method to list interviews based on date and job post id
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<JobInterviewVo> listInterviewsByJobPostDateRange(Long postId, Date startDate, Date endDate) {
		List<JobInterviewVo> resultVos = new ArrayList<JobInterviewVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobInterview.class)
				.add(Restrictions.ge("interviewDate", startDate)).add(Restrictions.le("interviewDate", endDate))
				.add(Restrictions.eq("jobPost.id", postId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("interviewDate"));

		List<JobInterview> interviews = criteria.list();
		for (JobInterview interview : interviews) {
			resultVos.add(createInterviewVo(interview));
		}
		return resultVos;
	}

	/**
	 * Method to list job interviews by branch and date
	 * 
	 * @param branchId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<JobInterviewVo> listInterviewByBranchAndDate(Long branchId, Date startDate, Date endDate) {
		List<JobInterviewVo> resultVos = new ArrayList<JobInterviewVo>();
		List<JobPostVo> posts = jobPostDao.findListOfJobPostByBranchId(branchId);
		for (JobPostVo jobPostVo : posts) {

			resultVos.addAll(listInterviewsByJobPostDateRange(jobPostVo.getJobPostId(), startDate, endDate));
			System.out.println("Inside interview dao listInterviewby branch and date " + resultVos.size() + " \n"
					+ startDate + "\n" + endDate);
		}
		return resultVos;

	}
	/**
	 * Method to Get interview by post and date
	 * @param postId
	 * @param date
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public JobInterviewVo listInterviewByPostAndDate(Long postId , String date){
		Date dateVar = DateFormatter.convertStringToDate(date);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobInterview.class)
				.add(Restrictions.eq("interviewDate", dateVar))
				.add(Restrictions.eq("jobPost.id", postId));
		criteria.addOrder(Order.asc("interviewDate"));
		List<JobInterview> interview = criteria.list();
		if(interview.size()>0)
			return createInterviewVo(interview.get(0));
		else 
			return null;
	}
	/**
	 * Method to getinterviews by date
	 * @param date
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<JobInterviewVo> listInterviewsByDate(String date){
		List<JobInterviewVo> interviewVos = new ArrayList<JobInterviewVo>();
		Date dateVar = DateFormatter.convertStringToDate(date);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobInterview.class)
				.add(Restrictions.eq("interviewDate", dateVar));
		criteria.addOrder(Order.asc("interviewDate"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobInterview> interview =criteria.list();
		for(JobInterview inter : interview)
		{
			interviewVos.add(createInterviewVo(inter));
		}
		return interviewVos;
	}
}
