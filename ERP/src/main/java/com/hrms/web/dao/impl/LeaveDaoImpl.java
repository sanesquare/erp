package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ForwardApplicationStatusDao;
import com.hrms.web.dao.LeaveTypeDao;
import com.hrms.web.dao.LeavesDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.EmployeeLeave;
import com.hrms.web.entities.LeaveDocuments;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.entities.LeaveType;
import com.hrms.web.entities.Leaves;
import com.hrms.web.entities.UserProfile;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.LeaveDocumentsVo;
import com.hrms.web.vo.LeavesVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Shamsheer
 * @since 6-April-2015
 */
@Repository
public class LeaveDaoImpl extends AbstractDao implements LeavesDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Log
	private Logger logger;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private ForwardApplicationStatusDao statusDao;

	@Autowired
	private TempNotificationDao notificationDao;

	@Autowired
	private LeaveTypeDao leaveTypeDao;

	/**
	 * method to save leave
	 */
	public Long saveLeave(LeavesVo vo) {
		Leaves leaves = new Leaves();
		leaves.setReason(vo.getReason());
		if (vo.getFromDate() != "")
			leaves.setFromDate(DateFormatter.convertStringToDate(vo.getFromDate()));
		if (vo.getToDate() != "")
			leaves.setToDate(DateFormatter.convertStringToDate(vo.getToDate()));
		leaves.setNotes(vo.getNotes());
		leaves.setDescription(vo.getDescription());
		leaves.setLeavesBalance(0L);
		leaves.setTotalLeaves(0L);
		leaves.setAlternateContactNumber(vo.getAlternateContactNumber());
		leaves.setAlternateContactPerson(vo.getAlternateContactPerson());

		LeaveType leaveType = leaveTypeDao.findLeaveType(vo.getLeaveTypeId());
		leaves.setLeaveType(leaveType);

		int diffInDays = (int) ((leaves.getToDate().getTime() - leaves.getFromDate().getTime())
				/ (1000 * 60 * 60 * 24));
		leaves.setDuration(diffInDays + 1);

		Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		leaves.setEmployee(employee);
		if (vo.getSuperiorIds() != null && !vo.getSuperiorIds().contains(0L)) {
			ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.REQUESTED);
			leaves.setLeaveStatus(status);
			leaves.setForwardApplicationTo(setSuperiors(vo.getSuperiorIds()));

			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject("A Leave Request Has Been Received From "
					+ employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname() + ".");
			notificationVo.getEmployeeIds().addAll(vo.getSuperiorIds());
			notificationDao.saveNotification(notificationVo);
		} else {
			ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.APPROVED);
			leaves.setLeaveStatus(status);
			EmployeeLeave employeeLeave = employeeDao.findEmployeeLeaveByEMployeeAndTyep(employee,
					leaves.getLeaveType());
			if (employeeLeave != null) {
				Long leavesTaken = employeeLeave.getLeavesTaken();
				if (leavesTaken == null)
					leavesTaken = 0L;
				leavesTaken = leavesTaken + leaves.getDuration();
				employeeLeave.setLeavesTaken(leavesTaken);
				Long leaveBalance = employeeLeave.getLeaves() - leavesTaken;
				employeeLeave.setLeavesBalance(leaveBalance);
				leaves.setLeavesBalance(leaveBalance);
				leaves.setTotalLeaves(employeeLeave.getLeaves());
				this.sessionFactory.getCurrentSession().merge(employeeLeave);
			}
		}
		/*
		 * else if(vo.getSuperiorAjaxIds().size()!=0) {
		 * 
		 * ForwardApplicationStatus status =
		 * statusDao.getStatus(StatusConstants.APPROVED);
		 * leaves.setLeaveStatus(status);
		 * leaves.setForwardApplicationTo(setSuperiors
		 * (vo.getSuperiorAjaxIds())); }
		 */

		return (Long) this.sessionFactory.getCurrentSession().save(leaves);
	}

	/**
	 * method to set superiors
	 * 
	 * @param superiorIds
	 * @return
	 */
	private Set<Employee> setSuperiors(List<Long> superiorIds) {
		Set<Employee> superiors = new HashSet<Employee>();
		for (Long id : superiorIds) {
			if (id != null) {
				Employee superior = new Employee();
				superior.setId(id);
				superiors.add(superior);
			}
		}
		return superiors;
	}

	/**
	 * method to update leave
	 */
	public void updateLeave(LeavesVo vo) {
		Leaves leaves = getLeave(vo.getLeaveId());
		leaves.setReason(vo.getReason());
		leaves.setFromDate(DateFormatter.convertStringToDate(vo.getFromDate()));
		leaves.setToDate(DateFormatter.convertStringToDate(vo.getToDate()));
		leaves.setNotes(vo.getNotes());
		leaves.setDescription(vo.getDescription());
		leaves.setAlternateContactNumber(vo.getAlternateContactNumber());
		leaves.setAlternateContactPerson(vo.getAlternateContactPerson());

		int diffInDays = (int) ((leaves.getToDate().getTime() - leaves.getFromDate().getTime())
				/ (1000 * 60 * 60 * 24));
		leaves.setDuration(diffInDays);

		Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		leaves.setEmployee(employee);

		leaves.setForwardApplicationTo(setSuperiors(vo.getSuperiorIds()));

		LeaveType leaveType = new LeaveType();
		leaveType.setId(vo.getLeaveTypeId());
		leaves.setLeaveType(leaveType);
		// UPDATE LEAVE STATUS UPON EDIT
		if (vo.getSuperiorIds() != null && !vo.getSuperiorIds().contains(0L)) {
			ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.REQUESTED);
			leaves.setLeaveStatus(status);
			leaves.setForwardApplicationTo(setSuperiors(vo.getSuperiorIds()));

			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject("A Leave Request Has Been Received From "
					+ employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname() + ".");
			notificationVo.getEmployeeIds().addAll(vo.getSuperiorIds());
			notificationDao.saveNotification(notificationVo);
		} else {
			ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.APPROVED);
			leaves.setLeaveStatus(status);
			EmployeeLeave employeeLeave = employeeDao.findEmployeeLeaveByEMployeeAndTyep(employee,
					leaves.getLeaveType());
			if (employeeLeave != null) {
				Long leavesTaken = employeeLeave.getLeavesTaken();
				if (leavesTaken == null)
					leavesTaken = 0L;
				leavesTaken = leavesTaken + leaves.getDuration();
				employeeLeave.setLeavesTaken(leavesTaken);
				Long leaveBalance = employeeLeave.getLeaves() - leavesTaken;
				employeeLeave.setLeavesBalance(leaveBalance);
				leaves.setLeavesBalance(leaveBalance);
				leaves.setTotalLeaves(employeeLeave.getLeaves());
				this.sessionFactory.getCurrentSession().merge(employeeLeave);
			}
		}
		//
		this.sessionFactory.getCurrentSession().merge(leaves);
	}

	/**
	 * method to delete leave
	 */
	public void deleteLeave(Long leaveId) {
		Leaves leaves = getLeave(leaveId);
		if (leaves.getDocuments() != null) {
			SFTPOperation operation = new SFTPOperation();
			for (LeaveDocuments documents : leaves.getDocuments()) {
				operation.removeFileFromSFTP(documents.getUrl());
			}
		}
		// updating leave counts
		ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.APPROVED);
		if (leaves.getLeaveStatus().equals(status)) {
			EmployeeLeave employeeLeave = employeeDao.findEmployeeLeaveByEMployeeAndTyep(leaves.getEmployee(),
					leaves.getLeaveType());
			if (employeeLeave != null) {
				Long leavesTaken = employeeLeave.getLeavesTaken();
				if (leavesTaken == null)
					leavesTaken = 0L;
				leavesTaken = leavesTaken - leaves.getDuration();
				employeeLeave.setLeavesTaken(leavesTaken);
				Long leaveBalance = employeeLeave.getLeaves() - leavesTaken;
				employeeLeave.setLeavesBalance(leaveBalance);
				/*
				 * leaves.setLeavesBalance(leaveBalance);
				 * leaves.setTotalLeaves(employeeLeave.getLeaves());
				 */
				this.sessionFactory.getCurrentSession().merge(employeeLeave);
			}
		}
		//
		this.sessionFactory.getCurrentSession().delete(leaves);
	}

	/**
	 * method to get leave by id
	 */
	public Leaves getLeave(Long id) {
		return (Leaves) this.sessionFactory.getCurrentSession().createCriteria(Leaves.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * method to get leave by id
	 */
	public LeavesVo getleaveById(Long leaveId) {
		return createLeavesVo(getLeave(leaveId));
	}

	/**
	 * method to create leavesVo
	 * 
	 * @param leaves
	 * @return
	 */
	private LeavesVo createLeavesVo(Leaves leaves) {
		LeavesVo vo = new LeavesVo();
		vo.setReason(leaves.getReason());
		vo.setEmployeeId(leaves.getEmployee().getId());
		Employee employee = leaves.getEmployee();
		if (employee.getBranch() != null)
			vo.setBranch(employee.getBranch().getBranchName());
		if (employee.getDepartmentEmployees() != null)
			vo.setDepartment(employee.getDepartmentEmployees().getName());
		if (employee.getEmployeeDesignation() != null)
			vo.setDesignation(employee.getEmployeeDesignation().getDesignation());
		vo.setEmployeeCode(employee.getEmployeeCode());
		vo.setNotes(leaves.getNotes());
		vo.setLeaveBalance(leaves.getLeavesBalance());
		vo.setTotalLeaves(leaves.getTotalLeaves());
		UserProfile profile = employee.getUserProfile();
		if (profile != null)
			vo.setEmployee(profile.getFirstName() + " " + profile.getLastname());
		vo.setLeaveId(leaves.getId());
		vo.setDescription(leaves.getDescription());
		if (leaves.getFromDate() != null)
			vo.setFromDate(DateFormatter.convertDateToString(leaves.getFromDate()));
		if (leaves.getToDate() != null)
			vo.setToDate(DateFormatter.convertDateToString(leaves.getToDate()));
		vo.setAlternateContactNumber(leaves.getAlternateContactNumber());
		vo.setAlternateContactPerson(leaves.getAlternateContactPerson());

		vo.setStatus(leaves.getLeaveStatus().getName());
		vo.setStatusId(leaves.getLeaveStatus().getId());
		vo.setStatusDescription(leaves.getStatusDescription());

		List<Long> superiorIds = new ArrayList<Long>();
		List<Long> superiorIdsAjax = new ArrayList<Long>();
		List<String> superiorName = new ArrayList<String>();
		for (Employee superior : leaves.getForwardApplicationTo()) {
			superiorIds.add(superior.getId());
			superiorIdsAjax.add(superior.getId());
			superiorName.add(superior.getEmployeeCode());
		}
		vo.setSuperiorIds(superiorIds);
		vo.setSuperiorAjaxIds(superiorIdsAjax);
		vo.setSuperiorNames(superiorName);

		vo.setLeaveType(leaves.getLeaveType().getType());
		vo.setLeaveTypeId(leaves.getLeaveType().getId());
		vo.setLeaveTypeAjaxId(leaves.getLeaveType().getId());
		vo.setEmployeeId(leaves.getEmployee().getId());
		if (leaves.getDocuments() != null) {
			vo.setDocumentsVos(createDocumentVo(leaves.getDocuments()));
		}
		vo.setCreatedBy(leaves.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(leaves.getCreatedOn()));
		vo.setDuration(leaves.getDuration());
		return vo;
	}

	/**
	 * method to set documents vos
	 * 
	 * @param documents
	 * @return
	 */
	private List<LeaveDocumentsVo> createDocumentVo(Set<LeaveDocuments> documents) {
		List<LeaveDocumentsVo> vos = new ArrayList<LeaveDocumentsVo>();
		for (LeaveDocuments document : documents) {
			LeaveDocumentsVo vo = new LeaveDocumentsVo();
			vo.setFileName(document.getFileName());
			vo.setUrl(document.getUrl());
			vos.add(vo);
		}
		return vos;
	}

	/**
	 * method to list leaves by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<LeavesVo> getLeavesByEmployee(Long employeeId) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<LeavesVo> leavesVos = new ArrayList<LeavesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Leaves.class);
		Criterion byEmployee = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression logicalExpression = Restrictions.or(byEmployee, createdBy);
		criteria.add(logicalExpression);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Leaves> leaves = criteria.list();
		for (Leaves leave : leaves) {
			leavesVos.add(createLeavesVo(leave));
		}
		return leavesVos;
	}

	/**
	 * method to get leave by type
	 */
	public List<LeavesVo> getLeavesByType(Long typeId) {
		List<Leaves> leaves = getLeaveByType(typeId);
		List<LeavesVo> leavesVos = new ArrayList<LeavesVo>();
		for (Leaves leave : leaves) {
			leavesVos.add(createLeavesVo(leave));
		}
		return leavesVos;
	}

	/**
	 * method to list all leaves
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<LeavesVo> listAllLeaves() {
		List<LeavesVo> leavesVos = new ArrayList<LeavesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Leaves.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Leaves> leaves = criteria.list();
		for (Leaves leave : leaves) {
			leavesVos.add(createLeavesVo(leave));
		}
		return leavesVos;
	}

	/**
	 * method to update documents
	 */
	public void updateDocuments(LeavesVo leavesVo) {
		Leaves leaves = getLeave(leavesVo.getLeaveId());
		leaves.setDocuments(createDocuments(leavesVo.getDocumentsVos(), leaves));
		this.sessionFactory.getCurrentSession().merge(leaves);
	}

	/**
	 * method to set documents
	 * 
	 * @param documentsVos
	 * @return
	 */
	private Set<LeaveDocuments> createDocuments(List<LeaveDocumentsVo> documentsVos, Leaves leaves) {
		Set<LeaveDocuments> documents = new HashSet<LeaveDocuments>();
		for (LeaveDocumentsVo documentsVo : documentsVos) {
			LeaveDocuments leaveDocuments = new LeaveDocuments();
			leaveDocuments.setUrl(documentsVo.getUrl());
			leaveDocuments.setFileName(documentsVo.getFileName());
			leaveDocuments.setLeaves(leaves);
			documents.add(leaveDocuments);
		}
		return documents;
	}

	/**
	 * method to delete document
	 */
	public void deleteDocument(String url) {
		LeaveDocuments documents = (LeaveDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(LeaveDocuments.class).add(Restrictions.eq("url", url)).uniqueResult();
		Leaves leaves = documents.getLeaves();
		leaves.getDocuments().remove(documents);
		this.sessionFactory.getCurrentSession().merge(leaves);
		this.sessionFactory.getCurrentSession().delete(documents);
	}

	/**
	 * method to get leave by type
	 */
	@SuppressWarnings("unchecked")
	public List<Leaves> getLeaveByType(Long typeId) {
		return (List<Leaves>) this.sessionFactory.getCurrentSession().createCriteria(Leaves.class)
				.add(Restrictions.eq("leaveType.id", typeId)).list();
	}

	/**
	 * method to list all status
	 */
	@SuppressWarnings("unchecked")
	public List<LeavesVo> listStatus() {
		List<LeavesVo> vos = new ArrayList<LeavesVo>();
		List<ForwardApplicationStatus> leaveStatus = this.sessionFactory.getCurrentSession()
				.createCriteria(ForwardApplicationStatus.class).list();
		for (ForwardApplicationStatus status : leaveStatus) {
			vos.add(createStatusVo(status));
		}
		return vos;
	}

	/**
	 * method to create leaves vo
	 * 
	 * @param status
	 * @return
	 */
	private LeavesVo createStatusVo(ForwardApplicationStatus status) {
		LeavesVo leavesVo = new LeavesVo();
		leavesVo.setStatusId(status.getId());
		leavesVo.setStatus(status.getName());
		return leavesVo;
	}

	/**
	 * method to get leaves taken by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<LeavesVo> getLeavesTakenByEmployee(Long employeeId) {
		List<LeavesVo> leavesVos = new ArrayList<LeavesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Leaves.class);
		criteria.add(Restrictions.eq("leaveStatus", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.eq("employee.id", employeeId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Leaves> leaves = criteria.list();
		for (Leaves leave : leaves) {
			leavesVos.add(createLeavesVo(leave));
		}
		return leavesVos;
	}

	/**
	 * method to update leave status
	 */
	public void updateStatus(LeavesVo vo) {
		Leaves leaves = getLeave(vo.getLeaveId());
		if (leaves == null)
			throw new ItemNotFoundException("Leave Not Exist.");
		ForwardApplicationStatus status = statusDao.getStatus(vo.getStatus());
		if (leaves.getLeaveStatus() != status) {
			if (vo.getStatus().equalsIgnoreCase(StatusConstants.APPROVED)) {
				Employee employee = leaves.getEmployee();
				EmployeeLeave employeeLeave = employeeDao.findEmployeeLeaveByEMployeeAndTyep(employee,
						leaves.getLeaveType());
				if (employeeLeave != null) {
					Long leavesTaken = employeeLeave.getLeavesTaken();
					if (leavesTaken == null)
						leavesTaken = 0L;
					leavesTaken = leavesTaken + leaves.getDuration();
					employeeLeave.setLeavesTaken(leavesTaken);
					Long leaveBalance = employeeLeave.getLeaves() - leavesTaken;
					employeeLeave.setLeavesBalance(leaveBalance);
					leaves.setLeavesBalance(leaveBalance);
					leaves.setTotalLeaves(employeeLeave.getLeaves());
					this.sessionFactory.getCurrentSession().merge(employeeLeave);
				}
			}
		}

		TempNotificationVo notificationVo = new TempNotificationVo();
		notificationVo.getEmployeeIds().add(leaves.getEmployee().getId());
		notificationVo.setSubject(leaves.getEmployee().getEmployeeCode() + " 's Leave Request " + vo.getStatus());
		notificationDao.saveNotification(notificationVo);

		leaves.setLeaveStatus(status);
		if (vo.getSuperiorCode() != null)
			leaves.setForwardApplicationTo(setSuperiorsByCodes(vo.getSuperiorCode()));
		else
			// leaves.getForwardApplicationTo().clear();
			leaves.setStatusDescription(vo.getStatusDescription());
		this.sessionFactory.getCurrentSession().merge(leaves);
	}

	private Set<Employee> setSuperiorsByCodes(List<String> superiorCodes) {
		Set<Employee> superiors = new HashSet<Employee>();
		for (String coded : superiorCodes) {
			Employee superior = employeeDao.findAndReturnEmployeeByCode(coded);
			if (superior != null)
				superiors.add(superior);
		}
		return superiors;
	}

	@SuppressWarnings({ "unchecked" })
	public List<LeavesVo> findAllLeaveByEmployee(String employeeCode) {
		List<LeavesVo> leavesVos = new ArrayList<LeavesVo>();
		Set<Leaves> leaves = new HashSet<Leaves>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Leaves.class);
		criteria.createAlias("forwardApplicationTo", "employee");
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("employee", employee);
		Criterion receiver = Restrictions.eq("employee.employeeCode", employeeCode);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		leaves.addAll(criteria.list());

		Criteria criteria2 = this.sessionFactory.getCurrentSession().createCriteria(Leaves.class);
		Criterion sender2 = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression expression = Restrictions.or(sender2, createdBy);
		criteria2.add(expression);
		criteria2.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		leaves.addAll(criteria2.list());
		for (Leaves leave : leaves) {
			leavesVos.add(createLeavesVo(leave));
		}
		return leavesVos;
	}

	@SuppressWarnings({ "unchecked" })
	public List<LeavesVo> findApprovedLeavesBetweenDates(Long employeeId, String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<LeavesVo> leavesVos = new ArrayList<LeavesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Leaves.class);
		criteria.add(Restrictions.eq("leaveStatus", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("fromDate", startdate));
		criteria.add(Restrictions.le("fromDate", enddate));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Leaves> leaves = criteria.list();
		for (Leaves leave : leaves) {
			leavesVos.add(createLeavesVo(leave));
		}
		return leavesVos;
	}

}
