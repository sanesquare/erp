package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.AccountType;
import com.hrms.web.vo.AccountTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
public interface AccountTypeDao {

	/**
	 * method to save AccountType
	 * 
	 * @param accountTypeVo
	 */
	public void saveAccountType(AccountTypeVo accountTypeVo);

	/**
	 * method to update AccountType
	 * 
	 * @param accountTypeVo
	 */
	public void updateAccountType(AccountTypeVo accountTypeVo);

	/**
	 * method to delete AccountType
	 * 
	 * @param accountTypeId
	 */
	public void deleteAccountType(Long accountTypeId);

	/**
	 * method to find AccountType by id
	 * 
	 * @param accountTypeId
	 * @return AccountType
	 */
	public AccountType findAccountType(Long accountTypeId);

	/**
	 * method to find AccountType by type
	 * 
	 * @param typeName
	 * @return AccountType
	 */
	public AccountType findAccountType(String typeName);

	/**
	 * method to find AccountType by id
	 * 
	 * @param accountTypeId
	 * @return AccountTypeVo
	 */
	public AccountTypeVo findAccountTypeById(Long accountTypeId);

	/**
	 * method to find AccountType by type
	 * 
	 * @param typeName
	 * @return AccountTypeVo
	 */
	public AccountTypeVo findAccountTypeByType(String typeName);

	/**
	 * method to find all AccountType
	 * 
	 * @return List<AccountTypeVo>
	 */
	public List<AccountTypeVo> findAllAccountType();
}
