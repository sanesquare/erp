package com.hrms.web.dao;

import com.hrms.web.entities.SecondLevelForwader;
import com.hrms.web.vo.SecondLevelForwardingVo;

/**
 * 
 * @author Jithin Mohan
 * @since 27-April-2015
 *
 */
public interface SecondLevelForwaderDao {

	/**
	 * method to save SecondLevelForwader
	 * 
	 * @param secondLevelForwardingVo
	 */
	public void saveSecondLevelForwader(SecondLevelForwardingVo secondLevelForwardingVo);

	/**
	 * method to update SecondLevelForwader
	 * 
	 * @param secondLevelForwardingVo
	 */
	public void updateSecondLevelForwader(SecondLevelForwardingVo secondLevelForwardingVo);

	/**
	 * method to delete SecondLevelForwader
	 * 
	 * @param secondLevelForwaderId
	 */
	public void deleteSecondLevelForwader(Long secondLevelForwaderId);

	/**
	 * method to find SecondLevelForwader by id
	 * 
	 * @param secondLevelForwaderId
	 * @return SecondLevelForwader
	 */
	public SecondLevelForwader findSecondLevelForwader(Long secondLevelForwaderId);

	/**
	 * method to find SecondLevelForwader by employee
	 * 
	 * @param secondLevelForwaderCode
	 * @return SecondLevelForwader
	 */
	public SecondLevelForwader findSecondLevelForwader(String secondLevelForwaderCode);

	/**
	 * method to find SecondLevelForwader by id
	 * 
	 * @param secondLevelForwaderId
	 * @return SecondLevelForwardingVo
	 */
	public SecondLevelForwardingVo findSecondLevelForwaderById(Long secondLevelForwaderId);

	/**
	 * method to find SecondLevelForwader by employee
	 * 
	 * @param secondLevelForwaderCode
	 * @return SecondLevelForwardingVo
	 */
	public SecondLevelForwardingVo findSecondLevelForwaderByForwader(String secondLevelForwaderCode);

	/**
	 * method to find all SecondLevelForwader
	 * 
	 * @return List<SecondLevelForwardingVo>
	 */
	public SecondLevelForwardingVo findAllSecondLevelForwader();

	/**
	 * method to find all SecondLevelForwader emails
	 * 
	 * @return
	 */
	public SecondLevelForwardingVo findAllSecondLevelForwaderEmails();
}
