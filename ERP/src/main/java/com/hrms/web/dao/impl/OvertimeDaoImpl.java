package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.OvertimeDao;
import com.hrms.web.dao.OvertimeStatusDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Overtime;
import com.hrms.web.entities.OvertimeStatus;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.OvertimeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Repository
public class OvertimeDaoImpl extends AbstractDao implements OvertimeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private OvertimeStatusDao overtimeStatusDao;

	public void saveOrUpdateOvertime(OvertimeVo overtimeVo) {
		Overtime overtime = findOvertime(overtimeVo.getOvertimeId());
		if (overtime == null)
			overtime = new Overtime();
		overtime.setEmployee(employeeDao.findAndReturnEmployeeByCode(overtimeVo.getEmployeeCode()));
		if (!overtimeVo.getForwaders().contains("auto")){
			overtime.setForwader(setOvertimeForwadList(overtimeVo.getForwaders(), overtime));
			overtime.setStatus(overtimeStatusDao.findOvertimeStatus(StatusConstants.REQUESTED));
		}else
			overtime.setStatus(overtimeStatusDao.findOvertimeStatus(StatusConstants.APPROVED));
		overtime.setTitle(overtimeVo.getOvertimeTitle());
		overtime.setOvertimeAmount(overtimeVo.getOvertimeAmount());
		overtime.setAmountRequestedOn(DateFormatter.convertStringToDate(overtimeVo.getRequestedDate()));
		overtime.setTimeIn(DateFormatter.createUtilDateWithTime(overtimeVo.getTimeIn()));
		overtime.setTimeOut(DateFormatter.createUtilDateWithTime(overtimeVo.getTimeOut()));
		overtime.setDescription(overtimeVo.getDescription());
		overtime.setNotes(overtimeVo.getNotes());
		this.sessionFactory.getCurrentSession().saveOrUpdate(overtime);
		overtimeVo.setOvertimeId(overtime.getId());
		overtimeVo.setRecordedOn(DateFormatter.convertDateToDetailedString(overtime.getCreatedOn()));
	}

	/**
	 * method to create overtime forward list
	 * 
	 * @param employee
	 * @param overtime
	 * @return forwardTo
	 */
	private Set<Employee> setOvertimeForwadList(List<String> employee, Overtime overtime) {
		Set<Employee> forwardTo = overtime.getForwader();
		if (forwardTo == null || forwardTo.isEmpty())
			forwardTo = new HashSet<Employee>();

		Set<Employee> forwardToTemp = new HashSet<Employee>();
		if (forwardTo != null)
			forwardToTemp.addAll(forwardTo);

		Set<Employee> newForwardTo = new HashSet<Employee>();
		for (String employeeCode : employee) {
			Employee forwader = employeeDao.findAndReturnEmployeeByCode(employeeCode);
			newForwardTo.add(forwader);
			if (!forwardTo.contains(forwader)) {
				forwader.setOvertime(overtime);
				forwardTo.add(forwader);
			}
		}

		forwardToTemp.removeAll(newForwardTo);
		Iterator<Employee> iterator = forwardToTemp.iterator();
		while (iterator.hasNext()) {
			Employee employee2 = (Employee) iterator.next();
			forwardTo.remove(employee2);
		}
		return forwardTo;
	}

	public void updateOvertimeStatus(OvertimeVo overtimeVo) {
		Overtime overtime = findOvertime(overtimeVo.getOvertimeId());
		if (overtime == null)
			throw new ItemNotFoundException("Overtime Details Not Found.");
		overtime.setStatus(overtimeStatusDao.findOvertimeStatus(overtimeVo.getStatus()));
		overtime.setStatusDescription(overtimeVo.getStatusDescription());
		overtime.setForwader(setOvertimeForwadList(overtimeVo.getForwaders(), overtime));
		this.sessionFactory.getCurrentSession().merge(overtime);
	}

	public void deleteOvertime(Long overtimeId) {
		Overtime overtime = findOvertime(overtimeId);
		if (overtime == null)
			throw new ItemNotFoundException("Overtime Details Not Found.");
		overtime.getForwader().clear();
		this.sessionFactory.getCurrentSession().delete(overtime);
	}

	public Overtime findOvertime(Long overtimeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.eq("id", overtimeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Overtime) criteria.uniqueResult();
	}

	/**
	 * method to create OvertimeVo object
	 * 
	 * @param overtime
	 * @return overtimeVo
	 */
	private OvertimeVo createOvertimeVo(Overtime overtime) {
		OvertimeVo overtimeVo = new OvertimeVo();
		overtimeVo.setOvertimeId(overtime.getId());
		overtimeVo.setEmployee(overtime.getEmployee().getUserProfile().getFirstName() + " "
				+ overtime.getEmployee().getUserProfile().getLastname());
		overtimeVo.setEmployeeCode(overtime.getEmployee().getEmployeeCode());
		overtimeVo.setForwaders(getOvertimeForwadersList(overtime.getForwader()));
		overtimeVo.setForwadersName(getOvertimeForwadersNameList(overtime.getForwader()));
		overtimeVo.setOvertimeTitle(overtime.getTitle());
		overtimeVo.setOvertimeAmount(overtime.getOvertimeAmount());
		overtimeVo.setRequestedDate(DateFormatter.convertDateToString(overtime.getAmountRequestedOn()));
		overtimeVo.setDescription(overtime.getDescription());
		overtimeVo.setNotes(overtime.getNotes());
		if (overtime.getStatus() != null)
			overtimeVo.setStatus(overtime.getStatus().getStatus());
		overtimeVo.setStatusDescription(overtime.getStatusDescription());
		overtimeVo.setTimeIn(DateFormatter.createUtilDateToString(overtime.getTimeIn()));
		overtimeVo.setTimeOut(DateFormatter.createUtilDateToString(overtime.getTimeOut()));
		overtimeVo.setRecordedBy(overtime.getCreatedBy().getUserName());
		overtimeVo.setRecordedOn(DateFormatter.convertDateToDetailedString(overtime.getCreatedOn()));
		return overtimeVo;
	}

	/**
	 * method to get overtime forwaders list
	 * 
	 * @param forwaders
	 * @return overtimeFrwdrs
	 */
	private List<String> getOvertimeForwadersList(Set<Employee> forwaders) {
		List<String> overtimeFrwdrs = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			overtimeFrwdrs.add(employee.getEmployeeCode());
		}
		return overtimeFrwdrs;
	}

	/**
	 * method to get overtime forwaders name list
	 * 
	 * @param forwaders
	 * @return overtimeFrwdrs
	 */
	private List<String> getOvertimeForwadersNameList(Set<Employee> forwaders) {
		List<String> overtimeFrwdrs = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			overtimeFrwdrs
					.add(employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname());
		}
		return overtimeFrwdrs;
	}

	public OvertimeVo findOvertimeById(Long overtimeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.eq("id", overtimeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createOvertimeVo((Overtime) criteria.uniqueResult());
	}

	public OvertimeVo findOvertimeByEmployee(Employee employee) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.eq("employee", employee));
		if (criteria.uniqueResult() == null)
			return null;
		return createOvertimeVo((Overtime) criteria.uniqueResult());
	}

	public List<OvertimeVo> findOvertimeByDate(Date requestedDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.eq("amountRequestedOn", requestedDate));
		return iterateCriteria(criteria);
	}

	public List<OvertimeVo> findOvertimeBetweenDates(Date startDate, Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.ge("amountRequestedOn", startDate))
				.add(Restrictions.le("amountRequestedOn", endDate));
		return iterateCriteria(criteria);
	}

	public List<OvertimeVo> findOvertimeByStatus(OvertimeStatus status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.eq("status", status));
		return iterateCriteria(criteria);
	}

	public List<OvertimeVo> findOvertimeByTimeIn(Date timeIn) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.eq("timeIn", timeIn));
		return iterateCriteria(criteria);
	}

	public List<OvertimeVo> findOvertimeByTimeOut(Date timeOut) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.eq("timeOut", timeOut));
		return iterateCriteria(criteria);
	}

	public List<OvertimeVo> findOvertimeBetweenInAndOut(Date timeIn, Date timeOut) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.ge("timeIn", timeIn)).add(Restrictions.le("timeOut", timeOut));
		return iterateCriteria(criteria);
	}

	public List<OvertimeVo> findAllOvertime() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class);
		return iterateCriteria(criteria);
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	private List<OvertimeVo> iterateCriteria(Criteria criteria) {
		List<OvertimeVo> overtimeVos = new ArrayList<OvertimeVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Overtime> overtimes = criteria.list();
		for (Overtime overtime : overtimes)
			overtimeVos.add(createOvertimeVo(overtime));
		return overtimeVos;
	}

	/**
	 * 
	 */
	public List<OvertimeVo> findOvertimeBetweenDates(Long employeeId, Date startDate, Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeById(employeeId)))
				.add(Restrictions.ge("amountRequestedOn", startDate))
				.add(Restrictions.le("amountRequestedOn", endDate));
		return iterateCriteria(criteria);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<OvertimeVo> findAllOvertimeByEmployee(String employeeCode) {
		List<OvertimeVo> overtimeVos = new ArrayList<OvertimeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class);
		criteria.createAlias("forwader", "employee");
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("createdBy", employee.getUser());
		Criterion receiver = Restrictions.eq("employee.employeeCode", employeeCode);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Overtime> overtimes = criteria.list();
		for (Overtime overtime : overtimes)
			overtimeVos.add(createOvertimeVo(overtime));
		return overtimeVos;
	}

	public List<OvertimeVo> findAllOvertimeBetweenDatesAndStatus(String employeeCode, String status, Date startDate,
			Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Overtime.class)
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeByCode(employeeCode)))
				.add(Restrictions.ge("amountRequestedOn", startDate))
				.add(Restrictions.le("amountRequestedOn", endDate))
				.add(Restrictions.eq("status", overtimeStatusDao.findOvertimeStatus(status)));
		return iterateCriteria(criteria);
	}

}
