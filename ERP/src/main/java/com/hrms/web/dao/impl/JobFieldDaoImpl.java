package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.JobFieldDao;
import com.hrms.web.entities.JobFields;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.JobFieldVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Repository
public class JobFieldDaoImpl extends AbstractDao implements JobFieldDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	/**
	 * method to save new job field
	 * 
	 * @param jobFieldVo
	 */
	public void saveJobField(JobFieldVo jobFieldVo) {
		for (String fieldName : StringSplitter.splitWithComma(jobFieldVo.getJobField())) {
			if (findJobField(fieldName.trim()) != null)
				throw new DuplicateItemException("Duplicate Job Field : " + fieldName.trim());
			JobFields fields = findJobField(jobFieldVo.getTempJobField());
			if (fields == null)
				fields = new JobFields();
			fields.setField(fieldName.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(fields);
		}
	}

	/**
	 * method to update job field
	 * 
	 * @param jobFieldVo
	 */
	public void updateJobField(JobFieldVo jobFieldVo) {
		JobFields fields = findJobFiled(jobFieldVo.getJobFieldId());
		if (fields == null)
			throw new ItemNotFoundException("Job Field Not Exists.");
		fields.setField(jobFieldVo.getJobField());
		this.sessionFactory.getCurrentSession().merge(fields);
	}

	/**
	 * method to delete jobfield
	 * 
	 * @param fieldId
	 */
	public void deleteJobField(Long fieldId) {
		JobFields fields = findJobFiled(fieldId);
		if (fields == null)
			throw new ItemNotFoundException("Job Field Not Exists.");
		this.sessionFactory.getCurrentSession().delete(fields);
	}

	/**
	 * method to find job field
	 * 
	 * @param fieldId
	 * @return jobfield
	 */
	public JobFields findJobFiled(Long fieldId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobFields.class)
				.add(Restrictions.eq("id", fieldId));
		if (criteria.uniqueResult() == null)
			return null;
		return (JobFields) criteria.uniqueResult();
	}

	/**
	 * method to create jobfieldvo
	 * 
	 * @param fields
	 * @return
	 */
	private JobFieldVo createJobFieldVo(JobFields fields) {
		JobFieldVo fieldVo = new JobFieldVo();
		fieldVo.setJobField(fields.getField());
		fieldVo.setJobFieldId(fields.getId());
		return fieldVo;
	}

	/**
	 * method to find jobfiled
	 * 
	 * @param fieldId
	 * @return jobfieldvo
	 */
	public JobFieldVo findJobFieldById(Long fieldId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobFields.class)
				.add(Restrictions.eq("id", fieldId));
		if (criteria.uniqueResult() == null)
			return null;
		return createJobFieldVo((JobFields) criteria.uniqueResult());
	}

	/**
	 * method to find job field by name
	 * 
	 * @param fieldName
	 * @return job field
	 */
	public JobFields findJobField(String fieldName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobFields.class)
				.add(Restrictions.eq("field", fieldName));
		if (criteria.uniqueResult() == null)
			return null;
		return (JobFields) criteria.uniqueResult();
	}

	/**
	 * method to find job field by name
	 * 
	 * @param fieldName
	 * @return jobfieldvo
	 */
	public JobFieldVo findJobFieldByName(String fieldName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobFields.class)
				.add(Restrictions.eq("field", fieldName));
		if (criteria.uniqueResult() == null)
			return null;
		return createJobFieldVo((JobFields) criteria.uniqueResult());
	}

	/**
	 * method to find all job field
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<JobFieldVo> findAllJobField() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobFields.class);
		List<JobFieldVo> fieldVos = new ArrayList<JobFieldVo>();
		List<JobFields> fields = criteria.list();
		for (JobFields field : fields)
			fieldVos.add(createJobFieldVo(field));
		return fieldVos;
	}
}
