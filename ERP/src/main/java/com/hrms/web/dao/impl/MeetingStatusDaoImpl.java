package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.MeetingsStatusDao;
import com.hrms.web.entities.MeetingStatus;
import com.hrms.web.interfaces.AuditEntity;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.MeetingsVo;
/**
 * 
 * @author Shamsheer
 * @since 13-March-2015
 *
 */
@Repository
public class MeetingStatusDaoImpl extends AbstractDao implements MeetingsStatusDao, AuditEntity {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/**
	 * method to save meeting Status
	 * @param MeetingsVo
	 */
	public void saveMeetingStatus(MeetingsVo meetingsVo) {
		MeetingStatus meetingStatus=new MeetingStatus();
		meetingStatus.setName(meetingsVo.getStatus());
		this.sessionFactory.getCurrentSession().saveOrUpdate(meetingStatus);
	}

	/**
	 * method to update projectStatus
	 * @param MeetingsVo
	 */
	public void updateProjectStatus(MeetingsVo meetingsVo) {
		MeetingStatus meetingStatus=this.findAndReturnMeetingStatusObjById(meetingsVo.getStatusId());
		meetingStatus.setName(meetingsVo.getStatus());
		this.sessionFactory.getCurrentSession().update(meetingStatus);
	}

	/**
	 * method to delete meeting Status
	 * @param projectVo
	 */
	public void deleteMeetingStatus(Long id) {
		this.sessionFactory.getCurrentSession().delete(this.findAndReturnMeetingStatusObjById(id));
	}

	/**
	 * method to list all meeting Status
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<MeetingsVo> listAllMeetingStatus() {
		List<MeetingsVo> meetingsVos=new ArrayList<MeetingsVo>();
		List<MeetingStatus> meetingStatus=this.sessionFactory.getCurrentSession().createCriteria(MeetingStatus.class).list();
		for(MeetingStatus meetingStatusObj:meetingStatus){
			meetingsVos.add(createMeetingVo(meetingStatusObj));
		}
		return meetingsVos;
	}

	/**
	 * method to find and return meeting Status object
	 * @param id
	 * @return
	 */
	public MeetingStatus findAndReturnMeetingStatusObjById(Long id) {
		return (MeetingStatus) this.sessionFactory.getCurrentSession().createCriteria(MeetingStatus.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * method to find and return meeting Status object by status
	 * @param status
	 * @return
	 */
	public MeetingStatus findAndReturnMeetingStatusObjectByStatus(String status) {
		return (MeetingStatus) this.sessionFactory.getCurrentSession().createCriteria(MeetingStatus.class)
				.add(Restrictions.eq("name", status)).uniqueResult();
	}

	/**
	 * method to create meetingVo
	 * @param meetingStatus
	 * @return
	 */
	private MeetingsVo createMeetingVo(MeetingStatus meetingStatus){
		MeetingsVo meetingsVo=new MeetingsVo();
		meetingsVo.setStatus(meetingStatus.getName());
		meetingsVo.setStatusId(meetingStatus.getId());
		return meetingsVo;
	}
}
