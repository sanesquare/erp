package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.JobPost;
import com.hrms.web.vo.JobPostVo;

/**
 * 
 * @author Vinutha
 * @since 19-March-2015
 *
 */

public interface JobPostDao {
	
	/**
	 * method to save new job post
	 * @param jobPostVo
	 */
	public void saveJobPost(JobPostVo jobPostVo);
	/**
	 * method to update existing jobPost
	 * @param jobPostVo
	 */
	public void updateJobPost(JobPostVo jobPostVo);
	/**
	 * method to delete existing jobPost
	 * @param jobPostVo
	 */
	public void deleteJobPost(JobPostVo jobPostVo);
	/**
	 * method to deleteJobPostById
	 * @param id
	 */
	public void deleteJobPostById(Long id);
	/**
	 * method to list all job posts
	 * @return List<JobPost>
	 */
	public List<JobPostVo> listAllJobPost();
	/**
	 * method to find job post based on id
	 * @param jobPostId
	 * @return
	 */
	public JobPostVo findJobPostById(Long jobPostId);
	/**
	 * method to find and return job post object
	 * @param jobPostId
	 * @return
	 */
	public JobPost findAndReturnJobPostObjectById(Long jobPostId);
	/**
	 * method to find list of job posts based on department
	 * @param departmentId
	 * @return list<JobPostVo>
	 */
	public List<JobPostVo> findListOfJobPostByDepartmentId(Long departmentId);
	/**
	 * method to find list of job posts based on branch
	 * @param departmentId
	 * @return list<JobPostVo>
	 */
	public List<JobPostVo> findListOfJobPostByBranchId(Long branch);
	/**
	 * method t save basic info
	 * @param jobPostVo
	 * @return
	 */
	public  Long saveBasicInfo(JobPostVo jobPostVo);
	/**
	 * method to save qualification 
	 * @param jobPostVo
	 */
	public void saveQualificationInfo(JobPostVo jobPostVo);
	/**
	 * method to save experience required
	 * @param jobPostVo
	 */
	public void saveExperienceRequired(JobPostVo jobPostVo);
	/**
	 * method to save additional info
	 * @param jobPostVo
	 */
	public void saveAdditionalInfo(JobPostVo jobPostVo);
	/**
	 * method to save description
	 * @param jobPostVo
	 */
	public void savePostDescription(JobPostVo jobPostVo);
	/**
	 * method to update job post
	 * @param jobPostVo
	 */
	public void updateDocuments(JobPostVo jobPostVo);
	/**
	 * method to delete document
	 * @param url
	 */
	public void deleteDocument(String url);

}
