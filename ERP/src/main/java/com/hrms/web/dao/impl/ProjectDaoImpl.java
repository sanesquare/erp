package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ProjectDao;
import com.hrms.web.dao.ProjectStatusDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Project;
import com.hrms.web.entities.ProjectDocuments;
import com.hrms.web.entities.ProjectStatus;
import com.hrms.web.service.TempNotificationService;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.ProjectDocumentsVo;
import com.hrms.web.vo.ProjectJsonVo;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Shamsheer
 * @since 10-March-2015
 *
 */
@Repository
public class ProjectDaoImpl extends AbstractDao implements ProjectDao {


	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private ProjectStatusDao projectStatusDao;
	
	@Autowired
	private EmployeeDao employeeDao;
	
	@Autowired
	private TempNotificationDao notificationDao;

	/**
	 * Method to save new Project
	 * 
	 * @param projectVo
	 * @return void
	 */
	public Long saveProject(ProjectJsonVo projectVo) {
		Project project = new Project();
		project.setTitle(projectVo.getProjectTitle());
		project.setDescription(projectVo.getProjectDescription());
		project.setAdditionalInformation(projectVo.getProjectAdditionalInformation());
		project.setProjectStartDate(DateFormatter.convertStringToDate(projectVo.getProjectStartDate()));
		project.setProjectEndDate(DateFormatter.convertStringToDate(projectVo.getProjectEndDate()));
		TempNotificationVo notificationVo = new TempNotificationVo();
		notificationVo.setSubject(" A New Project Has Been Added. ");
		if(projectVo.getEmployeesIds()!=null){
			project.setEmployee(setEmployeeList(projectVo.getEmployeesIds()));
			notificationVo.getEmployeeIds().addAll(projectVo.getEmployeesIds());
		}
		if(projectVo.getHeadIds()!=null){
			project.setProjectHeads(setProjectHeadList(projectVo.getHeadIds()));
			for(Long id : projectVo.getHeadIds()){
				if(!notificationVo.getEmployeeIds().contains(id))
					notificationVo.getEmployeeIds().add(id);
			}
			
		}
		notificationDao.saveNotification(notificationVo);

		return (Long) this.sessionFactory.getCurrentSession().save(project);

	}

	/**
	 * method to update existing project
	 * 
	 * @param projectVo
	 * @return void
	 */
	public void updateProject(ProjectJsonVo projectVo) {
		Project project = findAndReturnProjectObjectById(projectVo.getProjectId());
		project.setDescription(projectVo.getProjectDescription());
		project.setTitle(projectVo.getProjectTitle());
		project.setAdditionalInformation(projectVo.getProjectAdditionalInformation());
		project.setProjectStartDate(DateFormatter.convertStringToDate(projectVo.getProjectStartDate()));
		project.setProjectEndDate(DateFormatter.convertStringToDate(projectVo.getProjectEndDate()));
		if(projectVo.getEmployeesIds()!=null)
			project.setEmployee(setEmployeeList(projectVo.getEmployeesIds()));
		if(projectVo.getHeadIds()!=null)
			project.setProjectHeads(setProjectHeadList(projectVo.getHeadIds()));

		this.sessionFactory.getCurrentSession().merge(project);

	}

	/**
	 * method to delete existing project
	 * 
	 * @param projectVo
	 * @return void
	 */
	public void deleteProject(Long id) {
		Project project=findAndReturnProjectObjectById(id);
		if(project.getProjectDocuments()!=null){
			SFTPOperation operation = new SFTPOperation();
			for(ProjectDocuments documents : project.getProjectDocuments()){
				operation.removeFileFromSFTP(documents.getDocumentPath());
			}
		}
		this.sessionFactory.getCurrentSession().delete(project);

	}

	/**
	 * method to find project by using project Id
	 * 
	 * @param id
	 * @return ProjectVo
	 */
	public ProjectVo findProjectById(Long id) {
		Project project = (Project) this.sessionFactory.getCurrentSession().createCriteria(Project.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
		return createProjectVo(project);
	}

	/**
	 * method to find project by using project title
	 * 
	 * @param title
	 * @return ProjectVo
	 */
	public ProjectVo findProjectByTitle(String title) {
		Project project = (Project) this.sessionFactory.getCurrentSession().createCriteria(Project.class)
				.add(Restrictions.eq("title", title)).uniqueResult();
		return createProjectVo(project);
	}

	/**
	 * Method to find and return Project Object by using project Id
	 * 
	 * @param projectId
	 * @return Project
	 */
	public Project findAndReturnProjectObjectById(Long projectId) {
		return (Project) this.sessionFactory.getCurrentSession().createCriteria(Project.class)
				.add(Restrictions.eq("id", projectId)).uniqueResult();
	}

	/**
	 * method to list all projects
	 * 
	 * @return List<ProjectVo>
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<ProjectVo> listAllProjects() {
		List<ProjectVo> projectVos = new ArrayList<ProjectVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Project.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Project> projects =criteria .list();
		for (Project project : projects) {
			projectVos.add(createProjectVo(project));
		}
		return projectVos;
	}


	/**
	 * method to create ProjectVo
	 * 
	 * @param project
	 * @return
	 */
	private ProjectVo createProjectVo(Project project) {

		if(project!=null){
			ProjectVo projectVo = new ProjectVo();
			projectVo.setProjectId(project.getId());
			projectVo.setProjectDescription(project.getDescription());
			projectVo.setCreatedBy(project.getCreatedBy().getUserName());
			projectVo.setCreatedOn(DateFormatter.convertDateToString(project.getCreatedOn()));
			projectVo.setUpdatedBy(project.getUpdatedBy().getUserName());
			if (project.getUpdatedOn() != null)
				projectVo.setUpdatedOn(DateFormatter.convertDateToString(project.getUpdatedOn()));
			projectVo.setProjectTitle(project.getTitle());
			projectVo.setProjectAdditionalInformation(project.getAdditionalInformation());
			projectVo.setProjectEndDate(DateFormatter.convertDateToString(project.getProjectEndDate()));
			projectVo.setProjectStartDate(DateFormatter.convertDateToString(project.getProjectStartDate()));
			if(project.getProjectStatus()!=null){
				projectVo.setProjectStatusId(project.getProjectStatus().getId());
				projectVo.setProjectStatus(project.getProjectStatus().getName());
			}

			if(project.getEmployee()!=null){
				List<String> codes = new ArrayList<String>();
				List<String> names = new ArrayList<String>();
				List<Long> ids = new ArrayList<Long>();
				for (Employee employee : project.getEmployee()) {
					ids.add(employee.getId());
					codes.add(employee.getEmployeeCode());
					if(employee.getUserProfile()!=null)
						names.add(employee.getUserProfile().getFirstName()+" "+employee.getUserProfile().getLastname());
				}
				projectVo.setEmployeesIds(ids);
				projectVo.setEmployeesCodes(codes);
				projectVo.setEmployees(names);
			}
			
			if(project.getProjectHeads()!=null){
				List<Long> ids = new ArrayList<Long>();
				List<String> codes = new ArrayList<String>();
				List<String> names = new ArrayList<String>();
				for (Employee employee : project.getProjectHeads()) {
					ids.add(employee.getId());
					codes.add(employee.getEmployeeCode());
					if(employee.getUserProfile()!=null)
						names.add(employee.getUserProfile().getFirstName()+" "+employee.getUserProfile().getLastname());
				}
				projectVo.setHeadIds(ids);
				projectVo.setHeadCodes(codes);
				projectVo.setHeadNames(names);
			}
			if(project.getProjectDocuments()!=null)
				projectVo.setProjectDocuments(createDocumentVo(project.getProjectDocuments()));
			return projectVo;
		}else{
			return null;
		}
	}

	/**
	 * method to create project documents vo
	 * @param documents
	 * @return
	 */
	private List<ProjectDocumentsVo> createDocumentVo(Set<ProjectDocuments> documents){
		List<ProjectDocumentsVo> documentsVos = new ArrayList<ProjectDocumentsVo>();
		for(ProjectDocuments document : documents){
			ProjectDocumentsVo vo = new ProjectDocumentsVo();
			vo.setFileName(document.getName());
			vo.setUrl(document.getDocumentPath());
			documentsVos.add(vo);
		}
		return documentsVos;
	}

	/**
	 * method to set employees
	 * 
	 * @param employeeId
	 * @return
	 */
	@SuppressWarnings({ })
	private Set<Employee> setEmployeeList(List<Long> employeeIds) {
		Set<Employee> employeesSet = new HashSet<Employee>();
		for (Long employeeId : employeeIds) {
			Employee employeeObj=new Employee();
			employeeObj.setId(employeeId);
			employeesSet.add(employeeObj);
		}
		return employeesSet;
	}

	private Set<Employee> setProjectHeadList(List<Long> employeeIds) {
		Set<Employee> employeesSet = new HashSet<Employee>();
		for (Long employeeId : employeeIds) {
			Employee employeeObj=new Employee();
			employeeObj.setId(employeeId);
			employeesSet.add(employeeObj);
		}
		return employeesSet;
	}

	/**
	 * method to set employees names
	 * 
	 * @return
	 */
	private List<String> setEmployeesName(Set<Employee> employees) {
		List<String> employeeName = new ArrayList<String>();
		for (Employee employee : employees) {
			employeeName.add(employee.getEmployeeCode());
		}
		return employeeName;
	}

	/**
	 * method to set employee ids
	 * 
	 * @param employees
	 * @return
	 */
	private List<Long> setEmployeeIds(Set<Employee> employees) {
		List<Long> ids = new ArrayList<Long>();
		for (Employee employee : employees) {
			ids.add(employee.getId());
		}
		return ids;
	}

	/**
	 * method to find and return Project object by id
	 * 
	 * @return
	 */
	public Project findAndReturnProjectObjByTitle(String title) {
		return (Project) this.sessionFactory.getCurrentSession().createCriteria(Project.class)
				.add(Restrictions.eq("title", title)).uniqueResult();
	}

	/**
	 * method to list project by status
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ProjectVo> listProjectByStatus(String status) {
		List<ProjectVo> projectVos = new ArrayList<ProjectVo>();
		List<Project> projects = this.sessionFactory
				.getCurrentSession()
				.createCriteria(Project.class)
				.add(Restrictions.eq("projectStatus", projectStatusDao.findAndReturnProjectStatusObjectByStatus(status)))
				.list();
		for (Project project : projects) {
			projectVos.add(createProjectVo(project));
		}
		return projectVos;
	}



	/**
	 * method to delete project document
	 * 
	 * @param path
	 */
	public void deleteProjectDocument(String path) {
		ProjectDocuments documents = (ProjectDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(ProjectDocuments.class).add(Restrictions.eq("documentPath", path)).uniqueResult();
		Project project = documents.getProject();
		project.getProjectDocuments().remove(documents);
		this.sessionFactory.getCurrentSession().merge(project);
		this.sessionFactory.getCurrentSession().delete(documents);
	}

	/**
	 * method to update projet document
	 */
	public ProjectVo updateProjectDocument(ProjectVo projectVo) {
		Project project=this.findAndReturnProjectObjectById(projectVo.getProjectId());
		project.setProjectDocuments(setDocuments(projectVo.getProjectDocuments(), project));
		this.sessionFactory.getCurrentSession().merge(project);
		return createProjectVo(project);
	}

	/**
	 * method to update documents
	 * @param documentsVos
	 * @param project
	 * @return
	 */
	private Set<ProjectDocuments> setDocuments(List<ProjectDocumentsVo> documentsVos , Project project){
		Set<ProjectDocuments> documents = new HashSet<ProjectDocuments>();
		for(ProjectDocumentsVo vo : documentsVos){
			ProjectDocuments document = new ProjectDocuments();
			document.setProject(project);
			document.setName(vo.getFileName());
			document.setDocumentPath(vo.getUrl());
			documents.add(document);
		}
		return documents;
	}

	/**
	 * method to update project status
	 * @param projectVo
	 */
	public void updateProjectStatus(ProjectJsonVo projectVo) {
		Project project=findAndReturnProjectObjectById(projectVo.getProjectId());
		ProjectStatus projectStatus=new ProjectStatus();
		projectStatus.setId(projectVo.getProjectStatusId());
		project.setProjectStatus(projectStatus);
		this.sessionFactory.getCurrentSession().merge(project);
	}

}
