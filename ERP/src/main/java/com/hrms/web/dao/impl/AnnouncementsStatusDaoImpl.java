/**
 * 
 */
package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AnnouncementsStatusDao;
import com.hrms.web.entities.AnnouncementsStatus;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.AnnouncementsStatusVo;

/**
 * @author Shimil Babu
 * @since 10-March-2015
 * 
 */
@Repository
public class AnnouncementsStatusDaoImpl extends AbstractDao implements AnnouncementsStatusDao {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/**
	 * method to save announcements status
	 */
	public boolean saveAnnouncementsStatus(AnnouncementsStatusVo announcementsStatusVo) {
		logger.info("inside announcementStatusDaoImpl>> saveAnnouncementsStatus..");
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * method to delete announcements status
	 */
	public boolean deleteAnnouncementsStatus(AnnouncementsStatusVo announcementsVo) {
		logger.info("inside announcementStatusDaoImpl>> deleteAnnouncementsStatus..");
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * method to update announcements status
	 */
	public boolean updateAnnouncementsStatus(AnnouncementsStatusVo announcementsVo) {
		logger.info("inside announcementStatusDaoImpl>> updateAnnouncementsStatus..");
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * method to find all announcements status
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<AnnouncementsStatusVo> findAllAnnouncementsStatus() {
		logger.info("inside announcementStatusDaoImpl>> findAllAnnouncements..");
		List<AnnouncementsStatusVo> announcementsStatusVos = new ArrayList<AnnouncementsStatusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AnnouncementsStatus.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<AnnouncementsStatus> announcementsStatus = criteria.list();
		for (AnnouncementsStatus announcementsStatusObj : announcementsStatus) {
			announcementsStatusVos.add(createAnnouncementsVo(announcementsStatusObj));
		}
		return announcementsStatusVos;
	}

	/**
	 * method to create announcementStatusVo
	 * 
	 * @param announcementsStatus
	 * @return AnnouncementStatusVo
	 */
	private AnnouncementsStatusVo createAnnouncementsVo(AnnouncementsStatus announcementsStatus) {
		AnnouncementsStatusVo announcementsStatusVo = new AnnouncementsStatusVo();
		announcementsStatusVo.setAnnouncementsStatusId(announcementsStatus.getId());
		announcementsStatusVo.setAnnouncementsStatus(announcementsStatus.getStatus());
		announcementsStatusVo.setAnnouncementsStatusNotes(announcementsStatus.getNotes());
		return announcementsStatusVo;
	}

	public AnnouncementsStatus findAnnouncementsStatusByAnnouncementsStatusNotes(String notes) {
		AnnouncementsStatus announcementsStatus = (AnnouncementsStatus) this.sessionFactory.getCurrentSession()
				.createCriteria(AnnouncementsStatus.class).add(Restrictions.eq("notes", notes)).uniqueResult();
		return announcementsStatus;

	}

	public AnnouncementsStatus findAnnouncementsStatusByAnnouncementsStatusName(String status) {
		AnnouncementsStatus announcementsStatus = (AnnouncementsStatus) this.sessionFactory.getCurrentSession()
				.createCriteria(AnnouncementsStatus.class).add(Restrictions.eq("status", status)).uniqueResult();
		return announcementsStatus;

	}

	public AnnouncementsStatus findAnnouncementsStatusByAnnouncementsStatusId(Long id) {
		AnnouncementsStatus announcementsStatus = (AnnouncementsStatus) this.sessionFactory.getCurrentSession()
				.createCriteria(AnnouncementsStatus.class).add(Restrictions.eq("id", id)).uniqueResult();
		return announcementsStatus;

	}

}
