package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AssignmentDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.AssignmentDocuments;
import com.hrms.web.entities.Assignments;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Project;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.AssignmentDocumentsVo;
import com.hrms.web.vo.AssignmentVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Shamsheer
 * @since 26-March-2015
 */
@Repository
public class AssignmentDaoImpl extends AbstractDao implements AssignmentDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private TempNotificationDao notificationDao;
	
	/**
	 * method to save assignment
	 */
	public Long saveAssignment(AssignmentVo assignmentVo) {
		Assignments assignments  =new Assignments();

		Employee employee = employeeDao.findAndReturnEmployeeByCode(assignmentVo.getEmployeeCode());
		if(employee==null)
			throw new ItemNotFoundException("Employee Not Exist.");
		assignments.setEmployee(employee);

		TempNotificationVo notificationVo = new TempNotificationVo();
		notificationVo.setSubject("A New Assignment Has assigned.  ");
		notificationVo.getEmployeeCodes().add(assignmentVo.getEmployeeCode());
		notificationDao.saveNotification(notificationVo);
		
		Project project = new Project();
		project.setId(assignmentVo.getProjectId());
		assignments.setProject(project);

		assignments.setPriority(assignmentVo.getPriority());
		assignments.setNotes(assignmentVo.getNotes());
		assignments.setDescription(assignmentVo.getDescription());
		assignments.setAssignmentName(assignmentVo.getAssignmentName());
		assignments.setStartDate(DateFormatter.convertStringToDate(assignmentVo.getStartDate()));
		assignments.setEndDate(DateFormatter.convertStringToDate(assignmentVo.getEndDate()));

		userActivitiesService.saveUserActivity("Added new Assignment "+assignmentVo.getAssignmentName());
		return (Long) this.sessionFactory.getCurrentSession().save(assignments);
	}

	/**
	 * method to update assignments
	 */
	public void updateAssignment(AssignmentVo assignmentVo) {
		Assignments assignments  = findAssignment(assignmentVo.getAssignmentId());
		assignments.setNotes(assignmentVo.getNotes());
		assignments.setPriority(assignmentVo.getPriority());
		assignments.setDescription(assignmentVo.getDescription());
		assignments.setAssignmentName(assignmentVo.getAssignmentName());
		assignments.setStartDate(DateFormatter.convertStringToDate(assignmentVo.getStartDate()));
		assignments.setEndDate(DateFormatter.convertStringToDate(assignmentVo.getEndDate()));
		this.sessionFactory.getCurrentSession().merge(assignments);
		userActivitiesService.saveUserActivity("Updated Assignment "+assignmentVo.getAssignmentName());
	}

	/**
	 * method to delete assignment
	 */
	public void deleteAssignment(Long id) {
		Assignments assignments  = findAssignment(id);
		if(assignments.getAssignmentDocuments()!=null){
			SFTPOperation operation = new SFTPOperation();
			for(AssignmentDocuments documents : assignments.getAssignmentDocuments()){
				operation.removeFileFromSFTP(documents.getUrl());
			}
		}
		String name=assignments.getAssignmentName();
		this.sessionFactory.getCurrentSession().delete(assignments);
		userActivitiesService.saveUserActivity("Deleted Assignment "+name);
	}

	/**
	 * method to find assignment by id
	 */
	public AssignmentVo findAssignmentById(Long id) {
		return createAssignmentVo(findAssignment(id));
	}

	/**
	 * method to create assignment vo
	 * @param assignments
	 * @return
	 */
	private AssignmentVo createAssignmentVo(Assignments assignments){
		AssignmentVo vo  = new AssignmentVo();
		vo.setAssignmentId(assignments.getId());
		vo.setAssignmentName(assignments.getAssignmentName());
		vo.setEmployeeCode(assignments.getEmployee().getEmployeeCode());
		vo.setProjectTitle(assignments.getProject().getTitle());
		if(assignments.getEmployee().getUserProfile()!=null)
			vo.setEmployee(assignments.getEmployee().getUserProfile().getFirstName()+" "+assignments.getEmployee().getUserProfile().getLastname());
		vo.setNotes(assignments.getNotes());
		vo.setPriority(assignments.getPriority());
		vo.setDescription(assignments.getDescription());
		vo.setStartDate(DateFormatter.convertDateToString(assignments.getStartDate()));
		vo.setEndDate(DateFormatter.convertDateToString(assignments.getEndDate()));
		vo.setCreatedBy(assignments.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(assignments.getCreatedOn()));
		if(assignments.getAssignmentDocuments()!=null){
			List<AssignmentDocumentsVo> documentsVos=new ArrayList<AssignmentDocumentsVo>();
			for(AssignmentDocuments documents : assignments.getAssignmentDocuments()){
				AssignmentDocumentsVo documentsVo = new AssignmentDocumentsVo();
				documentsVo.setFileName(documents.getFileName());
				documentsVo.setUrl(documents.getUrl());
				documentsVos.add(documentsVo);
			}
			vo.setDocumentsVos(documentsVos);
		}
		return vo;
	}

	/**
	 * method to list assignments by name
	 */
	public List<AssignmentVo> findAssignmentByName(String title) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * method to list all assignments
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<AssignmentVo> listAllAssignment() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Assignments.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Assignments> assignments = criteria.list();
		List<AssignmentVo> vos = new ArrayList<AssignmentVo>();
		for(Assignments assignmentsObj : assignments){
			vos.add(createAssignmentVo(assignmentsObj));
		}
		return vos;
	}

	/**
	 * method to list assignments by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<AssignmentVo> listAssignmentByEmployee(Long employeeId) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if(employee==null)
			throw new ItemNotFoundException("Employee Not Exist.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Assignments.class);
		Criterion byEmployee = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression logicalExpression = Restrictions.or(byEmployee, createdBy);
		criteria.add(logicalExpression);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Assignments> assignments = criteria.list();
		List<AssignmentVo> vos = new ArrayList<AssignmentVo>();
		for(Assignments assignmentsObj : assignments){
			vos.add(createAssignmentVo(assignmentsObj));
		}
		return vos;
	}

	/**
	 * method to find assignment by id
	 */
	public Assignments findAssignment(Long id) {
		return (Assignments) this.sessionFactory.getCurrentSession().createCriteria(Assignments.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * method to delete document
	 */
	public void deleteDocument(String url) {
		AssignmentDocuments document = (AssignmentDocuments) this.sessionFactory.getCurrentSession().createCriteria(AssignmentDocuments.class)
				.add(Restrictions.eq("url", url)).uniqueResult();
		Assignments assignment = document.getAssignments();
		assignment.getAssignmentDocuments().remove(document);
		this.sessionFactory.getCurrentSession().merge(assignment);
		this.sessionFactory.getCurrentSession().delete(document);
	}

	/**
	 * method to update document
	 */
	public void updateDocument(AssignmentVo assignmentVo) {
		Assignments assignments=findAssignment(assignmentVo.getAssignmentId());
		assignments.setAssignmentDocuments(setDocuments(assignmentVo.getDocumentsVos(), assignments));
		this.sessionFactory.getCurrentSession().merge(assignments);
	}

	/**
	 * method to setdocuments
	 */
	private Set<AssignmentDocuments> setDocuments(List<AssignmentDocumentsVo> vos , Assignments assignments){
		Set<AssignmentDocuments> documents = new HashSet<AssignmentDocuments>();
		for(AssignmentDocumentsVo vo : vos){
			AssignmentDocuments assignmentDocuments = new AssignmentDocuments();
			assignmentDocuments.setFileName(vo.getFileName());
			assignmentDocuments.setUrl(vo.getUrl());
			assignmentDocuments.setAssignments(assignments);
			documents.add(assignmentDocuments);
		}

		return documents;
	}
}
