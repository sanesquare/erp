package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.LanguageDao;
import com.hrms.web.entities.Language;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.LanguageVo;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
@Repository
public class LanguageDaoImpl extends AbstractDao implements LanguageDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	/**
	 * method to save new language
	 * 
	 * @param languageVo
	 */
	public void saveLanguage(LanguageVo languageVo) {
		logger.info("Inside Language DAO >>> saveLanguage......");
		for (String languageName : StringSplitter.splitWithComma(languageVo.getLanguage().trim())) {
			if (findLanguage(languageName.trim()) != null)
				throw new DuplicateItemException("Duplicate Entry : " + languageName.trim());
			Language language = findLanguage(languageVo.getTempLanguage());
			if (language == null)
				language = new Language();
			language.setName(languageName.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(language);
		}
	}

	/**
	 * method to update language information
	 * 
	 * @param languageVo
	 */
	public void updateLanguage(LanguageVo languageVo) {
		logger.info("Inside Language DAO >>> updateLanguage......");
		Language language = findLanguage(languageVo.getLanguageId());

		if (language == null)
			throw new ItemNotFoundException("Language Not Exist.");
		language.setName(languageVo.getLanguage());
		this.sessionFactory.getCurrentSession().merge(language);
	}

	/**
	 * method to delete language information
	 * 
	 * @param deleteLanguage
	 */
	public void deleteLanguage(Long languageId) {
		logger.info("Inside Language DAO >>> deleteLanguage......");
		Language language = findLanguage(languageId);

		if (language == null)
			throw new ItemNotFoundException("Language Not Exist.");
		this.sessionFactory.getCurrentSession().delete(language);
	}

	/**
	 * method to find language by id and return language object
	 * 
	 * @param languageId
	 * @return language
	 */
	public Language findLanguage(Long languageId) {
		logger.info("Inside Language DAO >>> findLanguage......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Language.class)
				.add(Restrictions.eq("id", languageId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Language) criteria.uniqueResult();
	}

	/**
	 * method to create languageVo
	 * 
	 * @param language
	 * @return
	 */
	private LanguageVo createLanguageVo(Language language) {
		logger.info("Inside Language DAO >>> createLanguageVo......");
		LanguageVo languageVo = new LanguageVo();
		languageVo.setLanguage(language.getName());
		languageVo.setLanguageId(language.getId());
		return languageVo;
	}

	/**
	 * method to find language by id and return language vo
	 * 
	 * @param languageId
	 * @return languagevo
	 */
	public LanguageVo findLanguageById(Long languageId) {
		logger.info("Inside Language DAO >>> findLanguageById......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Language.class)
				.add(Restrictions.eq("id", languageId));
		if (criteria.uniqueResult() == null)
			return null;
		return createLanguageVo((Language) criteria.uniqueResult());
	}

	/**
	 * method to find language by name and return languagevo object
	 * 
	 * @param languageName
	 * @return languagevo
	 */
	public LanguageVo findLanguageByName(String languageName) {
		logger.info("Inside Language DAO >>> findLanguageByName......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Language.class)
				.add(Restrictions.eq("name", languageName));
		if (criteria.uniqueResult() == null)
			return null;
		return createLanguageVo((Language) criteria.uniqueResult());
	}

	/**
	 * method to find all languages
	 * 
	 * @return languageVos
	 */
	@SuppressWarnings("unchecked")
	public List<LanguageVo> findAllLanguage() {
		logger.info("Inside Language DAO >>> findAllLanguage......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Language.class);
		List<LanguageVo> languageVos = new ArrayList<LanguageVo>();

		List<Language> languages = criteria.list();
		for (Language language : languages)
			languageVos.add(createLanguageVo(language));
		return languageVos;
	}

	/**
	 * method to find language by name and return language object
	 * 
	 * @param languageName
	 * @return Language
	 */
	public Language findLanguage(String languageName) {
		logger.info("Inside Language DAO >>> findLanguage......");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Language.class)
				.add(Restrictions.eq("name", languageName));
		if (criteria.uniqueResult() == null)
			return null;
		return (Language) criteria.uniqueResult();
	}
}
