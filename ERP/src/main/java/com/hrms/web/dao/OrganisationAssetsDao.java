package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.OrganisationAssets;
import com.hrms.web.vo.OrganisationAssetsVo;

/**
 * 
 * @author shamsheer
 * @since 21-March-2015
 */
public interface OrganisationAssetsDao {

	/**
	 * method to save new assets
	 * @param assetsVo
	 */
	public void saveAssets(OrganisationAssetsVo assetsVo);
	
	/**
	 * method to update assets
	 * @param assetsVo
	 */
	public void updateAssets(OrganisationAssetsVo assetsVo);
	
	/**
	 * method to delete asset
	 * @param id
	 */
	public void deleteAsset(Long id);
	
	/**
	 * method to find asset by id
	 * @param id
	 * @return
	 */
	public OrganisationAssetsVo findAssetById(Long id);
	
	/**
	 * method to find asset by name
	 * @param name
	 * @return
	 */
	public OrganisationAssetsVo findAssetByName(String name);
	
	/**
	 * method to find asset by code
	 * @param code
	 * @return
	 */
	public OrganisationAssetsVo findAssetByCode(String code);
	
	/**
	 * method to find asset by id
	 * @param id
	 * @return
	 */
	public OrganisationAssets findAssetObjById(Long id);
	
	/**
	 * method to find asset by name
	 * @param name
	 * @return
	 */
	public OrganisationAssets findAssetObjByName(String name);
	
	/**
	 * method to find asset by code
	 * @param code
	 * @return
	 */
	public OrganisationAssets findAssetObjByCode(String code);
	
	/**
	 * method to list all assets
	 * @return
	 */
	public List<OrganisationAssetsVo> listAllAssets();
}
