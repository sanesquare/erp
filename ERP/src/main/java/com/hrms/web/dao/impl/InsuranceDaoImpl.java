package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.InsuranceDao;
import com.hrms.web.dao.InsuranceTypeDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Insurance;
import com.hrms.web.entities.InsuranceType;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.EmployeePopUp;
import com.hrms.web.vo.InsuranceVo;

/**
 * 
 * @author Jithin Mohan
 * @since 10-April-2015
 *
 */
@Repository
public class InsuranceDaoImpl extends AbstractDao implements InsuranceDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private InsuranceTypeDao insuranceTypeDao;

	public void saveOrUpdateInsurance(InsuranceVo insuranceVo) {
		Insurance insurance = findInsurance(insuranceVo.getInsuranceId());
		if (insurance == null)
			insurance = new Insurance();
		insurance.setEmployee(employeeDao.findAndReturnEmployeeByCode(insuranceVo.getEmployeeCode()));
		insurance.setInsuranceType(insuranceTypeDao.findInsuranceType(insuranceVo.getInsuranceType()));
		insurance.setTitle(insuranceVo.getInsuranceTitle());
		insurance.setEmployeeShare(insuranceVo.getEmployeeShare());
		insurance.setOrganizationShare(insuranceVo.getOrganizationShare());
		insurance.setDescription(insuranceVo.getDescription());
		insurance.setNotes(insuranceVo.getNotes());
		this.sessionFactory.getCurrentSession().saveOrUpdate(insurance);
		insuranceVo.setInsuranceId(insurance.getId());
		insuranceVo.setRecordedBy(insurance.getCreatedBy().getUserName());
		insuranceVo.setRecordedOn(DateFormatter.convertDateToDetailedString(insurance.getCreatedOn()));
	}

	public void deleteInsurance(Long insuranceId) {
		Insurance insurance = findInsurance(insuranceId);
		if (insurance == null)
			throw new ItemNotFoundException("Insurance Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(insurance);
	}

	public Insurance findInsurance(Long insuranceId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Insurance.class)
				.add(Restrictions.eq("id", insuranceId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Insurance) criteria.uniqueResult();
	}

	/**
	 * method to create InsuranceVo object
	 * 
	 * @param insurance
	 * @return insuranceVo
	 */
	private InsuranceVo createInsuranceVo(Insurance insurance) {
		InsuranceVo insuranceVo = new InsuranceVo();
		insuranceVo.setInsuranceId(insurance.getId());
		insuranceVo.setEmployeeCode(insurance.getEmployee().getEmployeeCode());
		insuranceVo.setEmployee(insurance.getEmployee().getUserProfile().getFirstName() + " "
				+ insurance.getEmployee().getUserProfile().getLastname());
		insuranceVo.setInsuranceType(insurance.getInsuranceType().getType());
		insuranceVo.setInsuranceTitle(insurance.getTitle());
		insuranceVo.setEmployeeShare(insurance.getEmployeeShare());
		insuranceVo.setOrganizationShare(insurance.getOrganizationShare());
		insuranceVo.setDescription(insurance.getDescription());
		insuranceVo.setNotes(insurance.getNotes());
		insuranceVo.setEmployeePopup(EmployeePopUp.createEmployeePopupVo(insurance.getEmployee()));
		insuranceVo.setRecordedBy(insurance.getCreatedBy().getUserName());
		insuranceVo.setRecordedOn(DateFormatter.convertDateToDetailedString(insurance.getCreatedOn()));
		return insuranceVo;
	}

	public InsuranceVo findInsuranceById(Long insuranceId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Insurance.class)
				.add(Restrictions.eq("id", insuranceId));
		if (criteria.uniqueResult() == null)
			return null;
		return createInsuranceVo((Insurance) criteria.uniqueResult());
	}

	public InsuranceVo findInsuranceByEmployee(Employee employee) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Insurance.class)
				.add(Restrictions.eq("employee", employee));
		if (criteria.uniqueResult() == null)
			return null;
		return createInsuranceVo((Insurance) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<InsuranceVo> findInsuranceByType(InsuranceType insuranceType) {
		List<InsuranceVo> insuranceVos = new ArrayList<InsuranceVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Insurance.class)
				.add(Restrictions.eq("insuranceType", insuranceType));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Insurance> insurances = criteria.list();
		for (Insurance insurance : insurances)
			insuranceVos.add(createInsuranceVo(insurance));
		return insuranceVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<InsuranceVo> findAllInsurance() {
		List<InsuranceVo> insuranceVos = new ArrayList<InsuranceVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Insurance.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Insurance> insurances = criteria.list();
		for (Insurance insurance : insurances)
			insuranceVos.add(createInsuranceVo(insurance));
		return insuranceVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<InsuranceVo> findAllInsuranceByEmployee(Employee employee, Date startDate, Date endDate) {
		List<InsuranceVo> insuranceVos = new ArrayList<InsuranceVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Insurance.class)
				.add(Restrictions.eq("employee", employee)).add(Restrictions.ge("createdOn", startDate))
				.add(Restrictions.le("createdOn", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Insurance> insurances = criteria.list();
		for (Insurance insurance : insurances)
			insuranceVos.add(createInsuranceVo(insurance));
		return insuranceVos;
	}

}
