package com.hrms.web.dao;

import com.hrms.web.entities.WorkSheetTasks;
import com.hrms.web.vo.WorksheetTasksVo;

/**
 * 
 * @author Vinutha
 * @since 19-May-2015
 *
 */
public interface WorkSheetTaskDao {

	/**
	 * Method to find task by id
	 * @param id
	 * @return
	 */
	public WorksheetTasksVo findTaskById(Long id);
	/**
	 * Method to find task object
	 * @param id
	 * @return
	 */
	public WorkSheetTasks findTaskObject(Long id);
	/**
	 * Method to delete task
	 * @param id
	 */
	public void deleteTaskById(Long id);
	
}
