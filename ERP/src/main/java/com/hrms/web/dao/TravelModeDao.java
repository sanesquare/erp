package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.TravelMode;
import com.hrms.web.vo.TravelModeVo;

/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
public interface TravelModeDao {

	public void saveTravelMode(TravelModeVo vo);
	
	public void updateTravelMode(TravelModeVo vo);
	
	public void deleteTravelMode(Long id);
	
	public TravelMode getTravelMode(Long id);
	
	public TravelModeVo getTravelModeById(Long id);
	
	public List<TravelModeVo> listAllTravelModes();
}
