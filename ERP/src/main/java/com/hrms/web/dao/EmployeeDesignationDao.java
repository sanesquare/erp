package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.EmployeeDesignation;
import com.hrms.web.vo.DesignationRankVo;
import com.hrms.web.vo.EmployeeVo;

/**
 * 
 * @author shamsheer
 * @since 18-March-2015
 */
public interface EmployeeDesignationDao {

	/**
	 * method to save new designation
	 * 
	 * @param employeeVo
	 */
	public void saveDesignation(DesignationRankVo designationRankVo);

	/**
	 * method to update designation
	 * 
	 * @param employeeVo
	 */
	public void updateDesignation(EmployeeVo employeeVo);

	/**
	 * method to delete designation
	 * 
	 * @param id
	 */
	public void deleteDesignation(Long id);

	/**
	 * method to list all designations
	 * 
	 * @return
	 */
	public List<EmployeeDesignation> listAllDesignations();

	/**
	 * method to find designation by Id
	 * 
	 * @param id
	 * @return
	 */
	public EmployeeDesignation findDesignationById(Long id);

	/**
	 * method to find designation by designation
	 * 
	 * @param designation
	 * @return
	 */
	public EmployeeDesignation findDesignationByDesignation(String designation);

	/**
	 * 
	 * @return
	 */
	public List<DesignationRankVo> findAllDesignations();

	/**
	 * 
	 * @param designation
	 * @param rank
	 * @return
	 */
	public EmployeeDesignation findEmployeeDesignation(String designation, Long rank);
}
