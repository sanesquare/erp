package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Shamsheer
 * @since 19-May-2015
 */
public interface TempNotificationDao {

	/**
	 * method to save new notification
	 * @param notificationVo
	 */
	public void saveNotification(TempNotificationVo notificationVo);
	
	/**
	 * method to update notification
	 * @param notificationVo
	 */
	public void updateNotification(TempNotificationVo notificationVo);
	
	/**
	 * method to delete notification
	 * @param id
	 */
	public void deleteNotification(Long id, String employeeCode);
	
	/**
	 * method to list notifications for an employee
	 * @param employeeId
	 * @return 
	 */
	public List<TempNotificationVo> listNotificationForEmployee(Long employeeId);
	
	/**
	 * method to list notifications for an employee
	 * @param employeeCode
	 */
	public List<TempNotificationVo> listNotificationForEmployee(String employeeCode);
	
}
