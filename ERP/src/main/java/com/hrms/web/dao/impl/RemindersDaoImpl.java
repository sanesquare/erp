package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ReminderStatusDao;
import com.hrms.web.dao.RemindersDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ReminderStatus;
import com.hrms.web.entities.Reminders;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.RemindersVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Repository
public class RemindersDaoImpl extends AbstractDao implements RemindersDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private ReminderStatusDao reminderStatusDao;

	/**
	 * method to save new reminder
	 * 
	 * @param remindersVo
	 * @return
	 */
	public void saveReminder(RemindersVo remindersVo) {
		Reminders reminders = new Reminders();
		reminders.setTitle(remindersVo.getReminderTitle());
		reminders.setMessage(remindersVo.getReminderMessage());
		reminders.setReminderStatus(getReminderStatus(remindersVo.getReminderStatus()));
		reminders.setReminderTime(getTime(remindersVo.getReminderTime()));
		reminders.setTimeMeridian(remindersVo.getTimeMeridian());

		reminders.setReminderOn(DateFormatter.convert12To24(
				DateFormatter.getCurrentYear() + "-" + remindersVo.getReminderMonth() + "-"
						+ remindersVo.getReminderDay(), remindersVo.getReminderTime(), remindersVo.getTimeMeridian()));

		reminders.setEmployee(getEmployees(remindersVo.getReminderTo()));
		this.sessionFactory.getCurrentSession().save(reminders);
	}

	/**
	 * method to convert time in a format eg: hh.mm
	 * 
	 * @param time
	 * @return reminderTime
	 */
	private String getTime(String time) {
		List<String> reminderTime = Arrays.asList(time.split(":"));
		return reminderTime.get(0) + "." + reminderTime.get(1);
	}

	/**
	 * method to get employee set
	 * 
	 * @param employeeName
	 * @return employees
	 */
	private Set<Employee> getEmployees(List<String> employeeName) {
		Set<Employee> employees = new HashSet<Employee>();

		return employees;
	}

	/**
	 * method to fetch reminderstatus by status
	 * 
	 * @param status
	 * @return reminder status
	 */
	private ReminderStatus getReminderStatus(String status) {
		return reminderStatusDao.findReminderStatus(status);
	}

	public void updateReminder(RemindersVo remindersVo) {
		Reminders reminders = findReminder(remindersVo.getReminderId());
		if (reminders == null)
			throw new ItemNotFoundException("Reminder Not Exist");
		reminders.setTitle(remindersVo.getReminderTitle());
		reminders.setMessage(remindersVo.getReminderMessage());
		reminders.setReminderStatus(getReminderStatus(remindersVo.getReminderStatus()));
		reminders.setReminderTime(remindersVo.getReminderTime());
		reminders.setTimeMeridian(remindersVo.getTimeMeridian());

		reminders.setReminderOn(DateFormatter.convert12To24(
				DateFormatter.getCurrentYear() + "-" + remindersVo.getReminderMonth() + "-"
						+ remindersVo.getReminderDay(), remindersVo.getReminderTime(), remindersVo.getTimeMeridian()));

		reminders.setEmployee(getEmployees(remindersVo.getReminderTo()));
		this.sessionFactory.getCurrentSession().update(reminders);
	}

	public void deleteReminder(Long reminderId) {
		Reminders reminders = findReminder(reminderId);
		if (reminders == null)
			throw new ItemNotFoundException("Reminder Not Exist");
		this.sessionFactory.getCurrentSession().delete(reminders);
	}

	public Reminders findReminder(Long reminderId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reminders.class)
				.add(Restrictions.eq("id", reminderId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Reminders) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Reminders> findReminder(Date reminderDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reminders.class)
				.add(Restrictions.like("reminderOn", reminderDate));
		List<Reminders> reminders = criteria.list();
		return reminders;
	}

	/**
	 * method to create remindersVo object
	 * 
	 * @param reminders
	 * @return remindersVo
	 */
	private RemindersVo createRemindersVo(Reminders reminders) {
		RemindersVo remindersVo = new RemindersVo();
		remindersVo.setReminderId(reminders.getId());
		remindersVo.setReminderDate(DateFormatter.convertDateToDetailedString(reminders.getReminderOn()));
		remindersVo.setReminderMessage(reminders.getMessage());
		remindersVo.setReminderStatus(reminders.getReminderStatus().getStatus());
		remindersVo.setReminderTitle(reminders.getTitle());
		remindersVo.setReminderTo(getReminderReceipients(reminders.getEmployee()));
		return remindersVo;
	}

	/**
	 * method to create list of employee name
	 * 
	 * @param employees
	 * @return employee
	 */
	private List<String> getReminderReceipients(Set<Employee> employees) {
		List<String> employee = new ArrayList<String>();
		Iterator<Employee> iterator = employees.iterator();
		while (iterator.hasNext()) {
			Employee employeeObj = (Employee) iterator.next();
			employee.add(employeeObj.getEmployeeCode());
		}
		return employee;
	}

	public RemindersVo findReminderById(Long reminderId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reminders.class)
				.add(Restrictions.eq("id", reminderId));
		if (criteria.uniqueResult() == null)
			return null;
		return createRemindersVo((Reminders) criteria.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<RemindersVo> findReminderByDate(Date reminderDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reminders.class)
				.add(Restrictions.like("reminderOn", reminderDate));
		List<RemindersVo> remindersVos = new ArrayList<RemindersVo>();
		List<Reminders> reminders = criteria.list();
		for (Reminders reminder : reminders)
			remindersVos.add(createRemindersVo(reminder));
		return remindersVos;
	}

	@SuppressWarnings("unchecked")
	public List<RemindersVo> findAllReminder() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Reminders.class)
				.addOrder(Order.asc("reminderOn"));
		List<RemindersVo> remindersVos = new ArrayList<RemindersVo>();
		List<Reminders> reminders = criteria.list();
		for (Reminders reminder : reminders)
			remindersVos.add(createRemindersVo(reminder));
		return remindersVos;
	}

}
