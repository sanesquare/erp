package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeExitTypeDao;
import com.hrms.web.entities.EmployeeExitType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.EmployeeExitTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 26-March-2015
 *
 */
@Repository
public class EmployeeExitTypeDaoImpl extends AbstractDao implements EmployeeExitTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveEmployeeExitType(EmployeeExitTypeVo employeeExitTypeVo) {
		for (String type : StringSplitter.splitWithComma(employeeExitTypeVo.getExitType())) {
			if (findEmployeeExitType(type.trim()) != null)
				throw new DuplicateItemException("Duplicate Employee Exit Type : " + type.trim());
			EmployeeExitType employeeExitType = new EmployeeExitType();
			employeeExitType.setExitType(type.trim());
			this.sessionFactory.getCurrentSession().save(employeeExitType);
			userActivitiesService.saveUserActivity("Added new Employee Exit Type "+employeeExitTypeVo.getExitType());
		}
	}

	public void updateEmployeeExitType(EmployeeExitTypeVo employeeExitTypeVo) {
		EmployeeExitType employeeExitType = findEmployeeExitType(employeeExitTypeVo.getExitTypeId());
		if (employeeExitType == null)
			throw new ItemNotFoundException("Employee Exit Type Details Not Found.");
		employeeExitType.setExitType(employeeExitTypeVo.getExitType());
		this.sessionFactory.getCurrentSession().merge(employeeExitType);
		userActivitiesService.saveUserActivity("Updated Employee Exit Type as "+employeeExitTypeVo.getExitType());
	}

	public void deleteEmployeeExitType(Long exitTypeId) {
		EmployeeExitType employeeExitType = findEmployeeExitType(exitTypeId);
		if (employeeExitType == null)
			throw new ItemNotFoundException("Employee Exit Type Details Not Found.");
		String name=employeeExitType.getExitType();
		this.sessionFactory.getCurrentSession().delete(employeeExitType);
		userActivitiesService.saveUserActivity("Deleted Employee Exit Type "+name);
	}

	public EmployeeExitType findEmployeeExitType(Long exitTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExitType.class)
				.add(Restrictions.eq("id", exitTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeExitType) criteria.uniqueResult();
	}

	public EmployeeExitType findEmployeeExitType(String exitType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExitType.class)
				.add(Restrictions.eq("exitType", exitType));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeExitType) criteria.uniqueResult();
	}

	/**
	 * method to create EmployeeExitTypeVo object
	 * 
	 * @param employeeExitType
	 * @return employeeExitTypeVo
	 */
	private EmployeeExitTypeVo createEmployeeExitTypeVo(EmployeeExitType employeeExitType) {
		EmployeeExitTypeVo employeeExitTypeVo = new EmployeeExitTypeVo();
		employeeExitTypeVo.setExitType(employeeExitType.getExitType());
		employeeExitTypeVo.setExitTypeId(employeeExitType.getId());
		return employeeExitTypeVo;
	}

	public EmployeeExitTypeVo findEmployeeExitTypeById(Long exitTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExitType.class)
				.add(Restrictions.eq("id", exitTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeExitTypeVo((EmployeeExitType) criteria.uniqueResult());
	}

	public EmployeeExitTypeVo findEmployeeExitTypeByType(String exitType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExitType.class)
				.add(Restrictions.eq("exitType", exitType));
		if (criteria.uniqueResult() == null)
			return null;
		return createEmployeeExitTypeVo((EmployeeExitType) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeExitTypeVo> findAllEmployeeExitType() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeExitType.class);
		List<EmployeeExitTypeVo> employeeExitTypeVos=new ArrayList<EmployeeExitTypeVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeExitType> employeeExitTypes=criteria.list();
		for(EmployeeExitType employeeExitType:employeeExitTypes)
			employeeExitTypeVos.add(createEmployeeExitTypeVo(employeeExitType));
		return employeeExitTypeVos;
	}
}
