package com.hrms.web.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.omg.PortableInterceptor.USER_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Repository;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.UserDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.User;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.UserVo;

/**
 * 
 * @author shamsheer
 * @since 19-March-2015
 */
@Repository
public class UserDaoImpl extends AbstractDao implements UserDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveUser(UserVo userVo) {
		// TODO Auto-generated method stub

	}

	public void updateUser(UserVo userVo) {
		// TODO Auto-generated method stub

	}

	public void deleteUser(Long id) {
		// TODO Auto-generated method stub

	}

	public User findUserById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public User findUserByUsername(String userName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("userName", userName));
		if (criteria.uniqueResult() == null)
			return null;
		return (User) criteria.uniqueResult();
	}

	public User findUserByEmployeeId(Long employeeId) {
		return (User) this.sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("employee.id", employeeId)).uniqueResult();
	}

	public UserVo findUser(String userName) {
		UserVo userVo = new UserVo();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("userName", userName));
		User user = (User) criteria.uniqueResult();
		if (user != null) {
			userVo.setUserName(userName);
			userVo.setEmployeeCode(user.getEmployee().getEmployeeCode());
			if (user.getEmployee().getUserProfile() != null)
				userVo.setEmployee(user.getEmployee().getUserProfile().getFirstName() + " "
						+ user.getEmployee().getUserProfile().getLastname());
		}
		return userVo;
	}

	public boolean updatePassword(String userName, String password, String newPassword) {
		boolean result = false;
		String encryptedPassword = DigestUtils.md5DigestAsHex(password.getBytes());
		String expectedPassword = null;
		User user = findUserByUsername(userName);
		if (user != null) {
			expectedPassword = user.getPassWord();
			if (!StringUtils.hasText(expectedPassword)) {
				throw new BadCredentialsException("No password for " + userName
						+ " set in database, contact administrator");
			}
		} else
			throw new BadCredentialsException("Invalid Username");
		if (!encryptedPassword.equals(expectedPassword))
			throw new BadCredentialsException("Invalid Password");
		else {
			user.setPassWord(DigestUtils.md5DigestAsHex(newPassword.getBytes()));
			this.sessionFactory.getCurrentSession().merge(user);
			result = true;
		}
		return result;
	}


}
