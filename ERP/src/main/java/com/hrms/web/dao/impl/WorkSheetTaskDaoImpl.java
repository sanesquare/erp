package com.hrms.web.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.bouncycastle.asn1.isismtt.x509.Restriction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.WorkSheetTaskDao;
import com.hrms.web.entities.WorkSheet;
import com.hrms.web.entities.WorkSheetTasks;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.WorksheetTasksVo;

/**
 * 
 * @author Vinutha
 * @since 19-May-2015
 *
 */
@Repository
public class WorkSheetTaskDaoImpl extends AbstractDao implements WorkSheetTaskDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/**
	 * Method to find task by id
	 * 
	 * @param id
	 * @return
	 */
	public WorksheetTasksVo findTaskById(Long id) {
		WorkSheetTasks task = (WorkSheetTasks) this.sessionFactory.getCurrentSession()
				.createCriteria(WorkSheetTasks.class).add(Restrictions.eq("id", id)).uniqueResult();
		return createTasksVo(task);
	}

	/**
	 * Method to create tasks vo
	 * 
	 * @param sheet
	 * @return
	 */
	private WorksheetTasksVo createTasksVo(WorkSheetTasks task) {
		WorksheetTasksVo taskVo = new WorksheetTasksVo();
		taskVo.setWorkSheetTasksId(task.getId());
		taskVo.setTask(task.getTask());
		taskVo.setStartTime(DateFormatter.convertSqlTimeToString(task.getStartTime()));
		taskVo.setEndTime(DateFormatter.convertSqlTimeToString(task.getEndTime()));
		return taskVo;
	}
	/**
	 * Method to find task object
	 * @param id
	 * @return
	 */
	public WorkSheetTasks findTaskObject(Long id) {
		WorkSheetTasks task = (WorkSheetTasks) this.sessionFactory.getCurrentSession()
				.createCriteria(WorkSheetTasks.class).add(Restrictions.eq("id", id)).uniqueResult();
		return task;
	}
	/**
	 * Method to delete task
	 * @param id
	 */
	public void deleteTaskById(Long id){
		WorkSheetTasks task = findTaskObject(id);
		WorkSheet sheet = task.getWorkSheet();
		sheet.getWorkSheetTasks().remove(task);
		this.sessionFactory.getCurrentSession().merge(sheet);
		this.sessionFactory.getCurrentSession().delete(task);
	}
}
