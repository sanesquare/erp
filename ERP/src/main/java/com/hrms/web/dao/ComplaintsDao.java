package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.vo.ComplaintStatusVo;
import com.hrms.web.vo.ComplaintsVo;


public interface ComplaintsDao {

	/**
	 * Method to save new Promotion
	 * @param ComplaintVo
	 * @return void
	 */
	public void saveComplaint(ComplaintsVo complaintVo);
	
	
	/**
	 * method to findAllComplaint details
	 * @return
	 */
	public List<ComplaintsVo> findAllComplaintDetails();
	
	
	/**
	 * method to findAllComplaint details
	 * @return
	 */
	public List<ComplaintStatusVo> findAllComplaintStatusDetails();

	
	
	/**
	 * method to delete existing Complaint
	 * @param ComplaintsVo
	 * @return void
	 */
	public void deleteComplaint(Long id);
	
	/**
	 * method to find Complaint by using Complaint Id
	 * @param id
	 * @return ComplaintVo
	 */
	public ComplaintsVo findComplaintById(Long promotionId);
	
	/**
	 * method to update existing Complaint
	 * @param promotionVo
	 * @return void
	 */
	public void updateComplaint(ComplaintsVo complaintsVo);
	
	/**
	 * method to delete complaint document
	 * 
	 * @param path
	 */
	public void deleteComplaintsDocument(Long path);
	
}
