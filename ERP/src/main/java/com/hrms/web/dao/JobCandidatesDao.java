package com.hrms.web.dao;

import java.util.List;
import java.util.Set;

import com.hrms.web.entities.JobCandidates;
import com.hrms.web.vo.CandidatesLanguageVo;
import com.hrms.web.vo.CandidatesQualificationsVo;
import com.hrms.web.vo.CandidatesSkillsVo;
import com.hrms.web.vo.JobCandidatesVo;
import com.hrms.web.vo.JobInterviewVo;

/**
 * 
 * @author Vinutha
 * @since 19-March-2015
 *
 */
public interface JobCandidatesDao {
	/**
	 * method to save job candidates
	 * @param jobCandidatesVo
	 */
	public void saveJobCandidates(JobCandidatesVo candidatesVo);
	/**
	 * method to update existing job candidates
	 * @param candidatesVo
	 */
	public void updateJobCandidates(JobCandidatesVo candidatesVo);
	/**
	 * method to delete existing job candidates
	 * @param candidatesVo
	 */
	public void deleteJobCandidates(JobCandidatesVo candidatesVo);
	/**
	 * method to delete job candidate by id
	 * @param id
	 */
	public void deleteJobCandidateById(Long id);
	/**
	 * method to find job candidate by id
	 * @return
	 */
	public JobCandidatesVo findJobCandidatesById(Long id);
	/**
	 * method to find all candidates with first name
	 * @param firstName
	 * @return
	 */
	public List<JobCandidatesVo> findAllCandidateswithFirstName(String firstName);
	/**
	 * method to find all candidates with last name
	 * @param lastName
	 * @return
	 */
	public List<JobCandidatesVo> findAllCandidateswithLastName(String lastName);
	/**
	 * method find and return job candidate object with id
	 * @param id
	 * @return
	 */
	public JobCandidates findAndReturnJobCandidatesObjectWithId(Long id);
	/**
	 * method to list candidates based on status and required number
	 * @param status
	 * @param number
	 * @return
	 */
	public List<JobCandidates> listCandidatesBasedOnStatusAndNumber(JobInterviewVo interviewVo);
	/**
	 * method to save basic info
	 * @param candidatesVo
	 * @return
	 */
	public Long saveCandidateBasicInfo(JobCandidatesVo candidatesVo);
	/**
	 * method to update candidate basic info
	 * @param candidatesVo
	 */
	public void updateCandidateBasicInfo(JobCandidatesVo candidatesVo);
	/**
	 * method to list all candidates
	 * @return
	 */
	public List<JobCandidatesVo> listAllCandidates();
	/**
	 * method to save achievements
	 * @param candidatesVo
	 */
	public void saveAchievement(JobCandidatesVo candidatesVo);
	/**
	 * method to save interests
	 * @param candidatesVo
	 */
	public void saveInterests(JobCandidatesVo candidatesVo);
	/**
	 * method to save additional info
	 * @param candidatesVo
	 */
	public void saveAdditionalInfo(JobCandidatesVo candidatesVo);
	/**
	 * method to save contact details
	 * @param candidatesVo
	 */
	public void saveContactDetails(JobCandidatesVo candidatesVo);
	/**
	 * method to save qualifications
	 * @param candidatesVo
	 */
	public void saveCandidateQualifications(JobCandidatesVo candidatesVo);
	/**
	 * method to save languages
	 * @param candidatesVo
	 */
	public void saveCandidateLanguages(JobCandidatesVo candidatesVo);
	/**
	 * method to save skills
	 * @param candidatesVo
	 */
	public void saveCandidatesSkills(JobCandidatesVo candidatesVo);
	/**
	 * method to save work experiences
	 * @param candidatesVo
	 */
	public void saveCandidateExperiences(JobCandidatesVo candidatesVo);
	/**
	 * method to save status
	 * @param candidatesVo
	 */
	public void saveCandidateStatus(JobCandidatesVo candidatesVo);
	/**
	 * method to save references
	 * @param candidatesVo
	 */
	public void saveCandidateReferences(JobCandidatesVo candidatesVo);
	/**
	 * method to upload resume
	 * @param candidatesVo
	 */
	public void saveCandidateResume(JobCandidatesVo candidatesVo);
	/**
	 * method to update documents
	 * @param candidatesVo
	 */
	public void updateDocuments(JobCandidatesVo candidatesVo);
	/**
	 * method to delete document
	 * @param candidatesVo
	 */
	public void deleteDocuments(String url);
	/**
	 * method to update resume
	 * @param candidatesVo
	 */
	public void updateResume(JobCandidatesVo candidatesVo);
	/**
	 * method to delete resume
	 * @param candidatesVo
	 * @param candidatesVo
	 */
	public void deleteResume(String url);
	/**
	 * method to find candidate qualification details
	 * @param candidate
	 * @param degree
	 * @return
	 */
	public CandidatesQualificationsVo getCandQualification(Long candidate , Long degree);
	/**
	 * method to find candidate language details
	 * @param candidate
	 * @param skill
	 * @return
	 */
	public CandidatesSkillsVo getCandSkills(Long candidate , Long skill);
	/**
	 * method to find candidate language details
	 * @param candidate
	 * @param language
	 * @return
	 */
	public CandidatesLanguageVo getCandLanguage(Long candidate , Long language);
	/**
	 * Method to list candidates based on field and gender
	 * @param fieldId
	 * @param gender
	 * @return
	 */
	public List<JobCandidatesVo> findCandidtesByPostAndGender(String designation,String gender);
	/**
	 * Method to list candidates based on field 
	 * @param fieldId
	 * @return
	 */
	public List<JobCandidatesVo> findCandidtesByPost(String designation);
	/**
	 * Method to list candidates based on gender
	 * @param gender
	 * @return
	 */
	public List<JobCandidatesVo> findCandidtesByGender(String gender);
	/**
	 * Method to list candidates for report
	 * @param gender
	 * @return
	 */
	public List<JobCandidatesVo> findAllCandidatesForReport() ;
	/**
	 * Method to get candidates by interview
	 * @param interviewId
	 * @return
	 */
	public List<JobCandidatesVo> findCandidatesByInterview(Long interviewId);
}
