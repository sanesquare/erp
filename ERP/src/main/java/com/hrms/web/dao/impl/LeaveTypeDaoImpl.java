package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.LeaveTypeDao;
import com.hrms.web.entities.LeaveType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.LeaveTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 26-March-2015
 *
 */
@Repository
public class LeaveTypeDaoImpl extends AbstractDao implements LeaveTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveLeaveType(LeaveTypeVo leaveTypeVo) {
		for (String type : StringSplitter.splitWithComma(leaveTypeVo.getLeaveType())) {
			if (findLeaveType(type.trim()) != null)
				throw new DuplicateItemException("Duplicate Leave Type : " + type.trim());
			LeaveType leaveType = findLeaveType(leaveTypeVo.getTempLeaveType());
			if (leaveType == null)
				leaveType = new LeaveType();
			leaveType.setType(type.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(leaveType);
		}
	}

	public void updateLeaveType(LeaveTypeVo leaveTypeVo) {
		LeaveType leaveType = findLeaveType(leaveTypeVo.getLeaveTypeId());
		if (leaveType == null)
			throw new ItemNotFoundException("Leave Type Details Not Found");
		leaveType.setType(leaveTypeVo.getLeaveType());
		this.sessionFactory.getCurrentSession().merge(leaveType);
	}

	public void deleteLeaveType(Long leaveTypeId) {
		LeaveType leaveType = findLeaveType(leaveTypeId);
		if (leaveType == null)
			throw new ItemNotFoundException("Leave Type Details Not Found");
		this.sessionFactory.getCurrentSession().delete(leaveType);
	}

	public LeaveType findLeaveType(Long leaveTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LeaveType.class)
				.add(Restrictions.eq("id", leaveTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (LeaveType) criteria.uniqueResult();
	}

	public LeaveType findLeaveType(String leaveType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LeaveType.class)
				.add(Restrictions.eq("type", leaveType));
		if (criteria.uniqueResult() == null)
			return null;
		return (LeaveType) criteria.uniqueResult();
	}

	/**
	 * method to create LeaveTypeVo object
	 * 
	 * @param leaveType
	 * @return leaveTypeVo
	 */
	private LeaveTypeVo createLeaveTypeVo(LeaveType leaveType) {
		LeaveTypeVo leaveTypeVo = new LeaveTypeVo();
		leaveTypeVo.setLeaveType(leaveType.getType());
		leaveTypeVo.setLeaveTypeId(leaveType.getId());
		return leaveTypeVo;
	}

	public LeaveTypeVo findLeaveTypeById(Long leaveTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LeaveType.class)
				.add(Restrictions.eq("id", leaveTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createLeaveTypeVo((LeaveType) criteria.uniqueResult());
	}

	public LeaveTypeVo findLeaveTypeByType(String leaveType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LeaveType.class)
				.add(Restrictions.eq("type", leaveType));
		if (criteria.uniqueResult() == null)
			return null;
		return createLeaveTypeVo((LeaveType) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<LeaveTypeVo> findAllLeaveType() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LeaveType.class);
		List<LeaveTypeVo> leaveTypeVos = new ArrayList<LeaveTypeVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<LeaveType> leaveTypes = criteria.list();
		for (LeaveType leaveType : leaveTypes)
			leaveTypeVos.add(createLeaveTypeVo(leaveType));
		return leaveTypeVos;
	}

}
