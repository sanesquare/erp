package com.hrms.web.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ManagerReportingNotificationDao;
import com.hrms.web.entities.ManagerReportingNotification;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.ManagerReportingNotificationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Repository
public class ManagerReportingNotificationDaoImpl extends AbstractDao implements ManagerReportingNotificationDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void updateManagerReportingNotification(boolean isRepot) {
		ManagerReportingNotification managerReportingNotification = findManagerReportingNotification();

		if (managerReportingNotification == null)
			managerReportingNotification = new ManagerReportingNotification();

		managerReportingNotification.setReport(isRepot);
		this.sessionFactory.getCurrentSession().saveOrUpdate(managerReportingNotification);
	}

	/**
	 * method to create ManagerReportingNotificationVo
	 * 
	 * @param notification
	 * @return notificationVo
	 */
	private ManagerReportingNotificationVo createManagerReportingNotificationVo(
			ManagerReportingNotification notification) {
		ManagerReportingNotificationVo notificationVo = new ManagerReportingNotificationVo();
		notificationVo.setReport(notification.isReport());
		notificationVo.setReportingNotificationId(notification.getId());
		return notificationVo;
	}

	public ManagerReportingNotificationVo findManagerReportingNotification(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ManagerReportingNotification.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return createManagerReportingNotificationVo((ManagerReportingNotification) criteria.uniqueResult());
	}

	public ManagerReportingNotification findManagerReportingNotification() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ManagerReportingNotification.class);
		if (criteria.uniqueResult() == null)
			return null;
		return (ManagerReportingNotification) criteria.uniqueResult();
	}

}
