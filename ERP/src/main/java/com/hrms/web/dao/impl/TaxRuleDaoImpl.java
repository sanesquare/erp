package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.TaxRuleDao;
import com.hrms.web.entities.TaxRule;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.TaxRuleVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Repository
public class TaxRuleDaoImpl extends AbstractDao implements TaxRuleDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveOrUpdateTaxRule(TaxRuleVo taxRuleVo) {
		TaxRule taxRule = findTaxRule(taxRuleVo.getTaxRuleId());
		if (taxRule == null)
			taxRule = new TaxRule();
		taxRule.setSalaryFrom(taxRuleVo.getSalaryFrom());
		taxRule.setSalaryTo(taxRuleVo.getSalaryTo());
		taxRule.setTaxPercentage(taxRuleVo.getTaxPercentage());
		taxRule.setExemptedAmount(taxRuleVo.getExemptedAmount());
		taxRule.setAdditionalAmount(taxRuleVo.getAdditionalAmount());
		taxRule.setIndividual(taxRuleVo.getIndividual());
		taxRule.setRowTax(taxRuleVo.getRowTax());
		this.sessionFactory.getCurrentSession().saveOrUpdate(taxRule);
	}

	public void deleteTaxRule(Long taxRuleId) {
		TaxRule taxRule = findTaxRule(taxRuleId);
		if (taxRule == null)
			throw new ItemNotFoundException("Tax Rule Details Not Found");
		this.sessionFactory.getCurrentSession().delete(taxRule);
	}

	public TaxRule findTaxRule(Long taxRuleId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxRule.class)
				.add(Restrictions.eq("id", taxRuleId));
		if (criteria.uniqueResult() == null)
			return null;
		return (TaxRule) criteria.uniqueResult();
	}

	/**
	 * method to create TaxRuleVo object
	 * 
	 * @param taxRule
	 * @return taxRuleVo
	 */
	private TaxRuleVo createTaxRuleVo(TaxRule taxRule) {
		TaxRuleVo taxRuleVo = new TaxRuleVo();
		taxRuleVo.setTaxRuleId(taxRule.getId());
		taxRuleVo.setSalaryFrom(taxRule.getSalaryFrom());
		taxRuleVo.setSalaryTo(taxRule.getSalaryTo());
		taxRuleVo.setTaxPercentage(taxRule.getTaxPercentage());
		taxRuleVo.setExemptedAmount(taxRule.getExemptedAmount());
		taxRuleVo.setAdditionalAmount(taxRule.getAdditionalAmount());
		taxRuleVo.setRowTax(taxRule.getRowTax());
		taxRuleVo.setIndividual(taxRule.getIndividual());
		return taxRuleVo;
	}

	public TaxRuleVo findTaxRuleById(Long taxRuleId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxRule.class)
				.add(Restrictions.eq("id", taxRuleId));
		if (criteria.uniqueResult() == null)
			return null;
		return createTaxRuleVo((TaxRule) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TaxRuleVo> findAllTaxRule() {
		List<TaxRuleVo> taxRuleVos = new ArrayList<TaxRuleVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxRule.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<TaxRule> rules = criteria.list();
		for (TaxRule rule : rules)
			taxRuleVos.add(createTaxRuleVo(rule));
		return taxRuleVos;
	}

	public TaxRule findTaxRuleBySalaryTo(BigDecimal salaryTo) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxRule.class)
				.add(Restrictions.eq("salaryTo", salaryTo));
		if (criteria.uniqueResult() == null)
			return null;
		return (TaxRule) criteria.uniqueResult();
	}

	public TaxRule findTaxRuleBySalaryFrom(BigDecimal salaryFrom) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxRule.class)
				.add(Restrictions.eq("salaryFrom", salaryFrom));
		if (criteria.uniqueResult() == null)
			return null;
		return (TaxRule) criteria.uniqueResult();
	}

	public TaxRuleVo findTaxRuleVoBySalaryTo(BigDecimal salaryTo) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxRule.class)
				.add(Restrictions.eq("salaryTo", salaryTo));
		if (criteria.uniqueResult() == null)
			return null;
		return createTaxRuleVo((TaxRule) criteria.uniqueResult());
	}

	public TaxRuleVo findTaxRuleVoBySalaryFrom(BigDecimal salaryFrom) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxRule.class)
				.add(Restrictions.eq("salaryFrom", salaryFrom));
		if (criteria.uniqueResult() == null)
			return null;
		return createTaxRuleVo((TaxRule) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TaxRuleVo> findAllTaxRuleByIndividual(String individualCategory) {
		List<TaxRuleVo> taxRuleVos = new ArrayList<TaxRuleVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxRule.class)
				.add(Restrictions.eq("individual", individualCategory));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<TaxRule> rules = criteria.list();
		for (TaxRule rule : rules)
			taxRuleVos.add(createTaxRuleVo(rule));
		return taxRuleVos;
	}

}
