package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.TimeZoneDao;
import com.hrms.web.entities.TimeZone;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.TimeZoneVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Repository
public class TimeZoneDaoImpl extends AbstractDao implements TimeZoneDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/**
	 * method to save new TimeZone
	 * 
	 * @param timeZoneVo
	 */
	public void saveTimeZone(TimeZoneVo timeZoneVo) {
		if (findTimeZone(timeZoneVo.getLocation()) != null)
			throw new DuplicateItemException("Duplicate Time Zone : " + timeZoneVo.getLocation());
		TimeZone timeZone = new TimeZone();
		timeZone.setLocation(timeZoneVo.getLocation());
		timeZone.setOffSet(timeZoneVo.getOffSet());
		this.sessionFactory.getCurrentSession().save(timeZone);
	}

	/**
	 * method to update TimeZone
	 * 
	 * @param timeZoneVo
	 */
	public void updateTimeZone(TimeZoneVo timeZoneVo) {
		TimeZone timeZone = findTimeZone(timeZoneVo.getTimeZoneId());
		if (timeZone == null)
			throw new ItemNotFoundException("Time Zone Not Exist");
		timeZone.setLocation(timeZoneVo.getLocation());
		timeZone.setOffSet(timeZoneVo.getOffSet());
		this.sessionFactory.getCurrentSession().merge(timeZone);
	}

	/**
	 * method to delete TimeZone
	 * 
	 * @param timeZoneId
	 */
	public void deleteTimeZone(Long timeZoneId) {
		TimeZone timeZone = findTimeZone(timeZoneId);
		if (timeZone == null)
			throw new ItemNotFoundException("Time Zone Not Exist");
		this.sessionFactory.getCurrentSession().delete(timeZone);
	}

	/**
	 * method to find TimeZone by id
	 * 
	 * @param timeZoneId
	 * @return TimeZone
	 */
	public TimeZone findTimeZone(Long timeZoneId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TimeZone.class)
				.add(Restrictions.eq("id", timeZoneId));
		if (criteria.uniqueResult() == null)
			return null;
		return (TimeZone) criteria.uniqueResult();
	}

	/**
	 * method to find TimeZone by location
	 * 
	 * @param location
	 * @return TimeZone
	 */
	public TimeZone findTimeZone(String location) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TimeZone.class)
				.add(Restrictions.eq("location", location));
		if (criteria.uniqueResult() == null)
			return null;
		return (TimeZone) criteria.uniqueResult();
	}

	/**
	 * method to create timezonvo object
	 * 
	 * @param timeZone
	 * @return timezonevo
	 */
	private TimeZoneVo createTimeZoneVo(TimeZone timeZone) {
		TimeZoneVo timeZoneVo = new TimeZoneVo();
		timeZoneVo.setLocation(timeZone.getLocation());
		timeZoneVo.setOffSet(timeZone.getOffSet());
		timeZoneVo.setTimeZoneId(timeZone.getId());
		return timeZoneVo;
	}

	/**
	 * method to find TimeZone by id
	 * 
	 * @param timeZoneId
	 * @return TimeZoneVo
	 */
	public TimeZoneVo findTimeZoneById(Long timeZoneId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TimeZone.class)
				.add(Restrictions.eq("id", timeZoneId));
		if (criteria.uniqueResult() == null)
			return null;
		return createTimeZoneVo((TimeZone) criteria.uniqueResult());
	}

	/**
	 * method to find TimeZone by location
	 * 
	 * @param location
	 * @return TimeZoneVo
	 */
	public TimeZoneVo findTimeZoneByLocation(String location) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TimeZone.class)
				.add(Restrictions.eq("location", location));
		if (criteria.uniqueResult() == null)
			return null;
		return createTimeZoneVo((TimeZone) criteria.uniqueResult());
	}

	/**
	 * method to find all TimeZone
	 * 
	 * @return List<TimeZoneVo>
	 */
	@SuppressWarnings("unchecked")
	public List<TimeZoneVo> findAllTimeZone() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TimeZone.class);
		List<TimeZoneVo> timeZoneVos = new ArrayList<TimeZoneVo>();
		List<TimeZone> timeZones = criteria.list();
		for (TimeZone timeZone : timeZones)
			timeZoneVos.add(createTimeZoneVo(timeZone));
		return timeZoneVos;
	}

}
