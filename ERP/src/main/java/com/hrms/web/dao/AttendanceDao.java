package com.hrms.web.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hrms.web.entities.Attendance;
import com.hrms.web.entities.Employee;
import com.hrms.web.vo.AttendanceVo;
import com.hrms.web.vo.BiometricAttendanceVo;
import com.hrms.web.vo.RestBiometricAttendanceVo;

/**
 * 
 * @author Shamsheer
 * @since 7-April-2015
 */
public interface AttendanceDao {

	/**
	 * method to save attendance
	 * @param vo
	 * @return
	 */
	public Long saveAttendance(AttendanceVo vo);
	
	/**
	 * method to update attendance
	 * @param vo
	 */
	public void updateAttendance(AttendanceVo vo);
	
	/**
	 * method to get attendance by id
	 * @param attendanceId
	 * @return
	 */
	public Attendance getAttendanceById(Long attendanceId);
	
	
	/**
	 * method to get attendance by id
	 * @param attendanceId
	 * @return
	 */
	public AttendanceVo getAttendanceByAttendanceId(Long attendanceId);
	
	/**
	 * method to delete attendance
	 * @param attendanceId
	 */
	public void deleteAttendance(Long attendanceId);
	
	/**
	 * method to list all attendances
	 * @return
	 */
	public List<AttendanceVo> listAllAttendance();
	
	/**
	 * method to list attendance by employee
	 * @param employeeId
	 * @return
	 */
	public List<AttendanceVo> listAttendanceByEmployee(Long employeeId);
	
	/**
	 * method to list attendance by employee
	 * @param employeeId
	 * @return
	 */
	public List<AttendanceVo> listAttendanceByEmployee(String employeeCode, String date);
	
	/**
	 * method to list attendance by employee and date
	 * @param employeeId
	 * @param date
	 * @return
	 */
	public List<AttendanceVo> listAttendanceByEmployeeAndDate(Long employeeId , String date);
	
	/**
	 * method to list attendance objects by employee and date
	 * @param employeeId
	 * @param date
	 * @return
	 */
	public List<Attendance> listAttendanceObjectByEmployeeAndDate(Long employeeId , String date);
	
	/**
	 * method to list attendance object by employee and date
	 * @param employeeCode
	 * @param date
	 * @return
	 */
	public List<Attendance> listAttendanceObjectByEmployeeAndDate(String employeeCode , String date);
	
	/**
	 * method to get attendance of employee between dates
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Integer listAttendanceObjectByEmployeeAndDate(Long employeeId , String startDate , String endDate);
	
	/**
	 * method to get total days attended by employee
	 * @param employeeId
	 * @param date
	 * @return
	 */
	public List<AttendanceVo> getTotalDaysAttendedByEmployee(Long employeeId , String date);
	
	/**
	 * method to save attendance from biometric device
	 * @param attendanceVos
	 */
	public void saveAttendanceFromBiometric(Set<BiometricAttendanceVo> attendanceVos);
	
	/**
	 * method to save attendance from rest
	 * @param attendanceMap
	 */
	public void saveAttendanceFromRestService(Map<String, RestBiometricAttendanceVo> attendanceMap);
	
	/**
	 * method to get lop count
	 * @param employee
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Double getLopCount(Employee employee , Date startDate,Date  endDate);
}
