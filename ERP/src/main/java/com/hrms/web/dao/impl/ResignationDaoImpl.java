package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ForwardApplicationStatusDao;
import com.hrms.web.dao.ResignationDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.entities.Resignation;
import com.hrms.web.entities.ResignationDocuments;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.ResignationDocumentsVo;
import com.hrms.web.vo.ResignationVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Shamsheer
 * @since 24-April-2015
 */
@Repository
public class ResignationDaoImpl extends AbstractDao implements ResignationDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private ForwardApplicationStatusDao statusDao;
	
	@Autowired
	private TempNotificationDao notificationDao;

	/**
	 * method to save resignation
	 */
	public Long saveResignation(ResignationVo resignationVo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(resignationVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		Resignation resignation = new Resignation();
		resignation.setFromBranch(employee.getBranch());
		resignation.setFromDepartment(employee.getDepartmentEmployees());
		resignation.setEmployee(employee);
		if (resignationVo.getResignationDate() != "")
			resignation.setResignationDate(DateFormatter.convertStringToDate(resignationVo.getResignationDate()));
		if (resignationVo.getNoticeDate() != "")
			resignation.setNoticeDate(DateFormatter.convertStringToDate(resignationVo.getNoticeDate()));
		ForwardApplicationStatus status=null;
		if (resignationVo.getSuperiorCode() != ""&& !resignationVo.getSuperiorCode().contains("auto")){
			resignation
					.setForwardApplicationTo(employeeDao.findAndReturnEmployeeByCode(resignationVo.getSuperiorCode()));
			TempNotificationVo notificationVo= new TempNotificationVo();
			notificationVo.getEmployeeCodes().add(resignationVo.getSuperiorCode());
			notificationVo.setSubject("A Resignation Request Has Been Received From "+employee.getUserProfile().getFirstName()
					+" "+employee.getUserProfile().getLastname()+". ");
			notificationDao.saveNotification(notificationVo);
			status = statusDao.getStatus(StatusConstants.REQUESTED);
		}else
			status = statusDao.getStatus(StatusConstants.APPROVED);
		if (status == null)
			throw new ItemNotFoundException("Status Not Exist.");
		resignation.setStatus(status);
		resignation.setDescription(resignationVo.getDescription());
		resignation.setNotes(resignationVo.getNotes());
		return (Long) this.sessionFactory.getCurrentSession().save(resignation);
	}

	/**
	 * method to update resignation
	 */
	public void updateResignation(ResignationVo resignationVo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(resignationVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		Resignation resignation = getResignation(resignationVo.getResignationId());
		if (resignation == null)
			throw new ItemNotFoundException("Resignatin Not Exist.");
		resignation.setEmployee(employee);
		resignation.setFromBranch(employee.getBranch());
		resignation.setFromDepartment(employee.getDepartmentEmployees());
		if (resignationVo.getResignationDate() != "")
			resignation.setResignationDate(DateFormatter.convertStringToDate(resignationVo.getResignationDate()));
		if (resignationVo.getNoticeDate() != "")
			resignation.setNoticeDate(DateFormatter.convertStringToDate(resignationVo.getNoticeDate()));
		if (resignationVo.getSuperiorCode() != "")
			resignation
					.setForwardApplicationTo(employeeDao.findAndReturnEmployeeByCode(resignationVo.getSuperiorCode()));
		ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.REQUESTED);
		if (status == null)
			throw new ItemNotFoundException("Status Not Exist.");
		resignation.setStatus(status);
		resignation.setDescription(resignationVo.getDescription());
		resignation.setNotes(resignationVo.getNotes());

		this.sessionFactory.getCurrentSession().merge(resignation);
	}

	/**
	 * method to delete resignation
	 */
	public void deleteResignation(Long resignationId) {
		Resignation resignation = getResignation(resignationId);
		if (resignation == null)
			throw new ItemNotFoundException("Resignatin Not Exist.");
		if (resignation.getDocuments() != null) {
			SFTPOperation operation = new SFTPOperation();
			for (ResignationDocuments documents : resignation.getDocuments()) {
				operation.removeFileFromSFTP(documents.getUrl());
			}
		}
		this.sessionFactory.getCurrentSession().delete(resignation);
	}

	/**
	 * method to get resignation
	 */
	public Resignation getResignation(Long resignationId) {
		return (Resignation) this.sessionFactory.getCurrentSession().createCriteria(Resignation.class)
				.add(Restrictions.eq("id", resignationId)).uniqueResult();
	}

	/**
	 * method to list all resignations
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<ResignationVo> listAllResignation() {
		List<ResignationVo> vos = new ArrayList<ResignationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Resignation.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Resignation> resignations = criteria.list();
		for (Resignation resignation : resignations) {
			vos.add(createResignationVo(resignation));
		}
		return vos;
	}

	/**
	 * method to get resignation by id
	 */
	public ResignationVo getResignationById(Long resignationId) {
		Resignation resignation = getResignation(resignationId);
		if (resignation == null)
			throw new ItemNotFoundException("Resignatin Not Exist.");
		return createResignationVo(resignation);
	}

	/**
	 * method to list resignatin by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ResignationVo> listResignationByEmployee(Long employeeId) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<ResignationVo> vos = new ArrayList<ResignationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Resignation.class);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Resignation> resignations = criteria.list();
		for (Resignation resignation : resignations) {
			vos.add(createResignationVo(resignation));
		}
		return vos;
	}

	/**
	 * method to list resignatin by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ResignationVo> listResignationByEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<ResignationVo> vos = new ArrayList<ResignationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Resignation.class);
		Criterion byEmployee = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression logicalExpression = Restrictions.or(byEmployee, createdBy);
		criteria.add(logicalExpression);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Resignation> resignations = criteria.list();
		for (Resignation resignation : resignations) {
			vos.add(createResignationVo(resignation));
		}
		return vos;
	}

	/**
	 * method to update document
	 */
	public void updateDocument(ResignationVo resignationVo) {
		Resignation resignation = getResignation(resignationVo.getResignationId());
		if (resignation == null)
			throw new ItemNotFoundException("Resignatin Not Exist.");
		resignation.setDocuments(setDocuments(resignationVo.getDocumentsVos(), resignation));
		this.sessionFactory.getCurrentSession().merge(resignation);
	}

	/**
	 * method to delete document
	 */
	public void deleteDocument(String url) {
		ResignationDocuments documents = (ResignationDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(ResignationDocuments.class).add(Restrictions.eq("url", url)).uniqueResult();
		Resignation resignation = documents.getResignation();
		resignation.getDocuments().remove(documents);
		this.sessionFactory.getCurrentSession().merge(resignation);
		this.sessionFactory.getCurrentSession().delete(documents);
	}

	/**
	 * method to update status
	 */
	public void updateStatus(ResignationVo resignationVo) {
		Resignation resignation = getResignation(resignationVo.getResignationId());
		if (resignation == null)
			throw new ItemNotFoundException("Resignatin Not Exist.");
		resignation.setStatusDescription(resignationVo.getStatusDescription());
		ForwardApplicationStatus status = statusDao.getStatus(resignationVo.getStatus());
		if (status == null)
			throw new ItemNotFoundException("Status Not Exist.");
		resignation.setStatus(status);
		if (resignationVo.getSuperiorCode() != "")
			resignation
					.setForwardApplicationTo(employeeDao.findAndReturnEmployeeByCode(resignationVo.getSuperiorCode()));
		else
			resignation.setForwardApplicationTo(null);
		this.sessionFactory.getCurrentSession().merge(resignation);
		
		TempNotificationVo notifyVo = new TempNotificationVo();
		notifyVo.getEmployeeIds().add(resignation.getEmployee().getId());
		notifyVo.setSubject(resignation.getEmployee().getEmployeeCode()+"'s Resignation Request "+resignationVo.getStatus());
		notificationDao.saveNotification(notifyVo);
	}

	/**
	 * method to list resignation between dates
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ResignationVo> listResignationBetweenDates(String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		List<ResignationVo> vos = new ArrayList<ResignationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Resignation.class);
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.between("resignationDate", startdate, enddate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Resignation> resignations = criteria.list();
		for (Resignation resignation : resignations) {
			vos.add(createResignationVo(resignation));
		}
		return vos;
	}

	/**
	 * method to create resignation vo
	 * 
	 * @param resignation
	 * @return
	 */
	private ResignationVo createResignationVo(Resignation resignation) {
		ResignationVo vo = new ResignationVo();
		vo.setResignationId(resignation.getId());
		vo.setEmployeeId(resignation.getEmployee().getId());
		vo.setEmployeeCode(resignation.getEmployee().getEmployeeCode());
		if (resignation.getEmployee().getUserProfile() != null)
			vo.setEmployee(resignation.getEmployee().getUserProfile().getFirstName() + " "
					+ resignation.getEmployee().getUserProfile().getLastname());

		if (resignation.getForwardApplicationTo() != null) {
			vo.setSuperiorId(resignation.getForwardApplicationTo().getId());
			vo.setSuperiorCode(resignation.getForwardApplicationTo().getEmployeeCode());
			vo.setSuperiorCodeAjax(resignation.getForwardApplicationTo().getEmployeeCode());
			if (resignation.getForwardApplicationTo().getUserProfile() != null)
				vo.setSuperior(resignation.getForwardApplicationTo().getUserProfile().getFirstName() + " "
						+ resignation.getForwardApplicationTo().getUserProfile().getLastname());
		}

		if (resignation.getNoticeDate() != null)
			vo.setNoticeDate(DateFormatter.convertDateToString(resignation.getNoticeDate()));
		if (resignation.getResignationDate() != null)
			vo.setResignationDate(DateFormatter.convertDateToString(resignation.getResignationDate()));

		vo.setDescription(resignation.getDescription());
		vo.setNotes(resignation.getNotes());
		vo.setStatus(resignation.getStatus().getName());
		vo.setStatusDescription(resignation.getStatusDescription());
		vo.setStatusId(resignation.getStatus().getId());
		vo.setCreatedBy(resignation.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(resignation.getCreatedOn()));

		if (resignation.getDocuments() != null)
			vo.setDocumentsVos(createDocumentVo(resignation.getDocuments()));
		return vo;
	}

	/**
	 * method to create document vos
	 * 
	 * @param documents
	 * @return
	 */
	private List<ResignationDocumentsVo> createDocumentVo(Set<ResignationDocuments> documents) {
		List<ResignationDocumentsVo> documentsVos = new ArrayList<ResignationDocumentsVo>();
		for (ResignationDocuments document : documents) {
			ResignationDocumentsVo vo = new ResignationDocumentsVo();
			vo.setFileName(document.getName());
			vo.setUrl(document.getUrl());
			documentsVos.add(vo);
		}
		return documentsVos;
	}

	private Set<ResignationDocuments> setDocuments(List<ResignationDocumentsVo> documentsVos, Resignation resignation) {
		Set<ResignationDocuments> documents = new HashSet<ResignationDocuments>();
		for (ResignationDocumentsVo vo : documentsVos) {
			ResignationDocuments document = new ResignationDocuments();
			document.setResignation(resignation);
			document.setUrl(vo.getUrl());
			document.setName(vo.getFileName());
			documents.add(document);
		}
		return documents;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ResignationVo> listResignationsByEmployeeIds(List<Long> employeeIds) {
		List<ResignationVo> vos = new ArrayList<ResignationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Resignation.class);
		criteria.add(Restrictions.in("employee.id", employeeIds));
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Resignation> resignations = criteria.list();
		for (Resignation resignation : resignations) {
			vos.add(createResignationVo(resignation));
		}
		return vos;
	}

	/**
	 * method to find resignation between dates
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ResignationVo> findResignationBetweenDates(String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		List<ResignationVo> vos = new ArrayList<ResignationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Resignation.class);
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.ge("resignationDate", startdate)).add(Restrictions.le("resignationDate", enddate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Resignation> resignations = criteria.list();
		for (Resignation resignation : resignations) {
			vos.add(createResignationVo(resignation));
		}
		return vos;
	}

	/**
	 * method to find resignation between dates & branch
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ResignationVo> findResignationByBranchBetweenDates(Long branchId, String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		List<ResignationVo> vos = new ArrayList<ResignationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Resignation.class);
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.eq("fromBranch.id", branchId));
		criteria.add(Restrictions.ge("resignationDate", startdate)).add(Restrictions.le("resignationDate", enddate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Resignation> resignations = criteria.list();
		for (Resignation resignation : resignations) {
			vos.add(createResignationVo(resignation));
		}
		return vos;
	}

	/**
	 * method to find resignation between dates &department
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ResignationVo> findResignationByDepartmentBetweenDates(Long departmentId, String startDate,
			String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		List<ResignationVo> vos = new ArrayList<ResignationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Resignation.class);
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.ge("resignationDate", startdate)).add(Restrictions.le("resignationDate", enddate));
		criteria.add(Restrictions.eq("fromDepartment.id", departmentId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Resignation> resignations = criteria.list();
		for (Resignation resignation : resignations) {
			vos.add(createResignationVo(resignation));
		}
		return vos;
	}

	/**
	 * method to find resignation between dates , branch & department
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ResignationVo> findResignationByBranchDepartmentBetweenDates(Long branchId, Long departmentId,
			String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		List<ResignationVo> vos = new ArrayList<ResignationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Resignation.class);
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.ge("resignationDate", startdate)).add(Restrictions.le("resignationDate", enddate));
		criteria.add(Restrictions.eq("fromDepartment.id", departmentId));
		criteria.add(Restrictions.eq("fromBranch.id", branchId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Resignation> resignations = criteria.list();
		for (Resignation resignation : resignations) {
			vos.add(createResignationVo(resignation));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ResignationVo> findAllResignationByEmployee(String employeeCode) {
		List<ResignationVo> vos = new ArrayList<ResignationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Resignation.class);
		criteria.createAlias("forwardApplicationTo", "employee");
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("employee", employee);
		Criterion receiver = Restrictions.eq("employee.employeeCode", employeeCode);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Resignation> resignations = criteria.list();
		for (Resignation resignation : resignations) {
			vos.add(createResignationVo(resignation));
		}
		return vos;
	}
}
