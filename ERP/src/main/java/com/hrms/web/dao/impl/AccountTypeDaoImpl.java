package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AccountTypeDao;
import com.hrms.web.entities.AccountType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.AccountTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Repository
public class AccountTypeDaoImpl extends AbstractDao implements AccountTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveAccountType(AccountTypeVo accountTypeVo) {
		for (String type : StringSplitter.splitWithComma(accountTypeVo.getAccountType())) {
			if (findAccountType(type.trim()) != null)
				throw new DuplicateItemException("Duplicate Account Type : " + type.trim());
			AccountType accountType = findAccountType(accountTypeVo.getTempAccountType());
			if (accountType == null)
				accountType = new AccountType();
			accountType.setType(type.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(accountType);
			userActivitiesService.saveUserActivity("Added new Account Type " + accountType.getType());
		}
	}

	public void updateAccountType(AccountTypeVo accountTypeVo) {
		AccountType accountType = findAccountType(accountTypeVo.getAccountTypeId());
		if (accountType == null)
			throw new ItemNotFoundException("Account Type Details Not Found.");
		accountType.setType(accountTypeVo.getAccountType());
		this.sessionFactory.getCurrentSession().merge(accountType);
		userActivitiesService.saveUserActivity("Updated Account Type as " + accountType.getType());
	}

	public void deleteAccountType(Long accountTypeId) {
		AccountType accountType = findAccountType(accountTypeId);
		if (accountType == null)
			throw new ItemNotFoundException("Account Type Details Not Found.");
		String accountTypeString = accountType.getType();
		this.sessionFactory.getCurrentSession().delete(accountType);
		userActivitiesService.saveUserActivity("Deleted Account Type " + accountTypeString);
	}

	public AccountType findAccountType(Long accountTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AccountType.class)
				.add(Restrictions.eq("id", accountTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (AccountType) criteria.uniqueResult();
	}

	public AccountType findAccountType(String typeName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AccountType.class)
				.add(Restrictions.eq("type", typeName));
		if (criteria.uniqueResult() == null)
			return null;
		return (AccountType) criteria.uniqueResult();
	}

	/**
	 * method to create AccountTypeVo object
	 * 
	 * @param accountType
	 * @return
	 */
	private AccountTypeVo createAccountTypeVo(AccountType accountType) {
		AccountTypeVo accountTypeVo = new AccountTypeVo();
		accountTypeVo.setAccountType(accountType.getType());
		accountTypeVo.setAccountTypeId(accountType.getId());
		return accountTypeVo;
	}

	public AccountTypeVo findAccountTypeById(Long accountTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AccountType.class)
				.add(Restrictions.eq("id", accountTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createAccountTypeVo((AccountType) criteria.uniqueResult());
	}

	public AccountTypeVo findAccountTypeByType(String typeName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AccountType.class)
				.add(Restrictions.eq("type", typeName));
		if (criteria.uniqueResult() == null)
			return null;
		return createAccountTypeVo((AccountType) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<AccountTypeVo> findAllAccountType() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AccountType.class);
		List<AccountTypeVo> accountTypeVos = new ArrayList<AccountTypeVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<AccountType> accountTypes = criteria.list();
		for (AccountType accountType : accountTypes)
			accountTypeVos.add(createAccountTypeVo(accountType));
		return accountTypeVos;
	}

}
