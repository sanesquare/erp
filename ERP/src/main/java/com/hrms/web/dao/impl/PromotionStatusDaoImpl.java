package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.PromotionStatusDao;
import com.hrms.web.entities.PromotionStatus;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.PromotionStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 25-March-2015
 *
 */
@Repository
public class PromotionStatusDaoImpl extends AbstractDao implements PromotionStatusDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void savePromotionStatus(PromotionStatusVo promotionStatusVo) {
		for (String status : StringSplitter.splitWithComma(promotionStatusVo.getPromotionStatus())) {
			if (findPromotionStatus(status.trim()) != null)
				throw new DuplicateItemException("Duplicate Promotion Status : " + status.trim());
			PromotionStatus promotionStatus = new PromotionStatus();
			promotionStatus.setStatus(status.trim());
			this.sessionFactory.getCurrentSession().save(promotionStatus);
		}
	}

	public void updatePromotionStatus(PromotionStatusVo promotionStatusVo) {
		PromotionStatus promotionStatus = findPromotionStatus(promotionStatusVo.getPromotionStatusId());
		if (promotionStatus == null)
			throw new ItemNotFoundException("Promotion Status Details Not Found.");
		promotionStatus.setStatus(promotionStatusVo.getPromotionStatus());
		this.sessionFactory.getCurrentSession().merge(promotionStatus);
	}

	public void deletePromotionStatus(Long statusId) {
		PromotionStatus promotionStatus = findPromotionStatus(statusId);
		if (promotionStatus == null)
			throw new ItemNotFoundException("Promotion Status Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(promotionStatus);
	}

	public PromotionStatus findPromotionStatus(Long statusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PromotionStatus.class)
				.add(Restrictions.eq("id", statusId));
		if (criteria.uniqueResult() == null)
			return null;
		return (PromotionStatus) criteria.uniqueResult();
	}

	public PromotionStatus findPromotionStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PromotionStatus.class)
				.add(Restrictions.eq("status", status));
		if (criteria.uniqueResult() == null)
			return null;
		return (PromotionStatus) criteria.uniqueResult();
	}

	/**
	 * method to create PromotionStatusVo object
	 * 
	 * @param promotionStatus
	 * @return promotionStatusVo
	 */
	private PromotionStatusVo createPromotionStatusVo(PromotionStatus promotionStatus) {
		PromotionStatusVo promotionStatusVo = new PromotionStatusVo();
		promotionStatusVo.setPromotionStatus(promotionStatus.getStatus());
		promotionStatusVo.setPromotionStatusId(promotionStatus.getId());
		return promotionStatusVo;
	}

	public PromotionStatusVo findPromotionStatusById(Long statusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PromotionStatus.class)
				.add(Restrictions.eq("id", statusId));
		if (criteria.uniqueResult() == null)
			return null;
		return createPromotionStatusVo((PromotionStatus) criteria.uniqueResult());
	}

	public PromotionStatusVo findPromotionStatusByStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PromotionStatus.class)
				.add(Restrictions.eq("status", status));
		if (criteria.uniqueResult() == null)
			return null;
		return createPromotionStatusVo((PromotionStatus) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<PromotionStatusVo> findAllPromotionStatus() {
		List<PromotionStatusVo> promotionStatusVos = new ArrayList<PromotionStatusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PromotionStatus.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<PromotionStatus> promotionStatus = criteria.list();
		for (PromotionStatus status : promotionStatus)
			promotionStatusVos.add(createPromotionStatusVo(status));
		return promotionStatusVos;
	}

}
