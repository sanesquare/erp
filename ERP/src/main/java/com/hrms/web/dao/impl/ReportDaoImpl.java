package com.hrms.web.dao.impl;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ReportDao;
import com.hrms.web.entities.Attendance;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Holiday;
import com.hrms.web.entities.Leaves;
import com.hrms.web.entities.Project;
import com.hrms.web.entities.SalaryDailyWage;
import com.hrms.web.entities.SalaryHourlyWage;
import com.hrms.web.entities.UserProfile;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AttendanceVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.HolidayVo;
import com.hrms.web.vo.LeavesVo;
import com.hrms.web.vo.SalaryDailyWagesVo;
import com.hrms.web.vo.SalaryHourlyWageVo;

/**
 * 
 * @author Vinutha
 * @since 17-April-2015
 *
 */
@Repository
public class ReportDaoImpl extends AbstractDao implements ReportDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;
	/**
	 * Method to get number of departments
	 */
	public Long deparmentCount() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Department.class);
		criteria.setProjection(Projections.rowCount());
		return (Long) (criteria.uniqueResult());
		
	}
	/**
	 * Method to get number of projects
	 */
	public Long projectCount() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Project.class);
		criteria.setProjection(Projections.rowCount());
		return (Long) (criteria.uniqueResult());
	}
	/**
	 * Method to list all holidays for a branch within given date range
	 * @param branch
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<HolidayVo> findHolidaysForBranchWithinDateRange(Branch branch,Date startDate , Date endDate){
		
		List<HolidayVo> holidayVos = new ArrayList<HolidayVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Holiday.class)
				.add(Restrictions.eq("branch", branch))
				.add(Restrictions.ge("startDate", startDate)).add(Restrictions.le("startDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("startDate"));
		List<Holiday> holidays = criteria.list();
		for (Holiday holiday : holidays)
			holidayVos.add(createRepHolidayVo(holiday));
		return holidayVos;
	}
	/**
	 * Method to get holidays within date range
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<HolidayVo> findHolidayBetweenDate(Date startDate, Date endDate) {
		List<HolidayVo> holidayVos = new ArrayList<HolidayVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Holiday.class)
				.add(Restrictions.ge("startDate", startDate)).add(Restrictions.le("startDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("startDate"));
		List<Holiday> holidays = criteria.list();
		for (Holiday holiday : holidays)
			holidayVos.add(createRepHolidayVo(holiday));
		return holidayVos;
	}
	
	
	/**
	 * private method to create holidays vo
	 * @param holiday
	 * @return
	 */
	private HolidayVo createRepHolidayVo(Holiday holiday) {
		HolidayVo holidayVo = new HolidayVo();
		holidayVo.setBranch(holiday.getBranch().getBranchName());
		holidayVo.setHolidayId(holiday.getId());
		holidayVo.setTitle(holiday.getTitle());
		holidayVo.setStartDate(DateFormatter.convertDateToString(holiday.getStartDate()));
		holidayVo.setEndDate(DateFormatter.convertDateToString(holiday.getEndDate()));
		holidayVo.setDescription(holiday.getDescription());
		holidayVo.setNote(holiday.getNote());
		holidayVo.setRecordedBy(holiday.getCreatedBy().getUserName());
		holidayVo.setRecordedOn(DateFormatter.convertDateToDetailedString(holiday.getCreatedOn()));
		return holidayVo;
	}
	/**
	 * Method to get employeeVo by code
	 * @param code
	 * @return EmployeeVo
	 */
	public EmployeeVo findEmployeeByCode(String code) {
		return createNameDesignationEmpVo((Employee) this.sessionFactory.getCurrentSession()
				.createCriteria(Employee.class).add(Restrictions.eq("employeeCode", code)).uniqueResult());

	}
	/**
	 * Method to create employeeVo for project-employee report
	 * @param employee
	 * @return
	 */
	private EmployeeVo createNameDesignationEmpVo(Employee employee)
	{
		EmployeeVo employeeVo = new EmployeeVo();
	
		UserProfile profile=employee.getUserProfile();
		if(profile!=null){
			employeeVo.setFirstName(profile.getFirstName());
			employeeVo.setName(profile.getFirstName()+" "+profile.getLastname());
			employeeVo.setLastName(profile.getLastname());
		}
		if(employee.getEmployeeDesignation()!=null){
			employeeVo.setDesignation(employee.getEmployeeDesignation().getDesignation());
			employeeVo.setDesignationId(employee.getEmployeeDesignation().getId());
		}
		return employeeVo;
	}
	/**
	 * Method to get distinct days on which employee was present
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Timestamp>  listAttendanceObjectByEmployeeAndDate(Long employeeId, String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("employee.id", employeeId));
		criteria.add(Restrictions.between("date", startdate, enddate));
		
		criteria.setProjection(Projections.groupProperty("date"));
		List<Timestamp> attendances = criteria.list();
		
		return attendances;
	}
	
	private AttendanceVo createAttendanceVo(Attendance attendance){
		AttendanceVo vo = new AttendanceVo();
		vo.setEmployeeCode(attendance.getEmployee().getEmployeeCode());
		vo.setEmployeeId(attendance.getEmployee().getId());
		if(attendance.getEmployee().getUserProfile()!=null)
			vo.setEmployee(attendance.getEmployee().getUserProfile().getFirstName()+" "+attendance.getEmployee().getUserProfile().getLastname());
		vo.setAttendanceId(attendance.getId());
		if(attendance.getDate()!=null)
			vo.setDate(DateFormatter.convertDateToString(attendance.getDate()));
		if(attendance.getSignIn()!=null)
			vo.setSignInTime(DateFormatter.convertSqlTimeToString(attendance.getSignIn()));
		if(attendance.getSignOut()!=null)
			vo.setSignOutTime(DateFormatter.convertSqlTimeToString(attendance.getSignOut()));
		vo.setNotes(attendance.getNotes());
		vo.setCreatedBy(attendance.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(attendance.getCreatedOn()));
		return vo;
	}

	/**
	 * Method to get hourly wage for a day
	 * @param employeeCode
	 * @param date
	 * @return SalaryHourlyWageVo
	 */
	@SuppressWarnings("unchecked")
	public SalaryHourlyWageVo getHourlyWageForDay(String employeeCode ,Date date) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<SalaryHourlyWage> hourlyWages = this.sessionFactory.getCurrentSession().createCriteria(SalaryHourlyWage.class)
				.add(Restrictions.eq("employee", employee))
				.add(Restrictions.le("withEffectFrom",date)).list();
		int size = hourlyWages.size();
		if(size>0)
		{
			SalaryHourlyWage hourlyWage = hourlyWages.get(size-1);
			return createHourlyWageSalaryVo(hourlyWage);
		}
		else
		{
			return null;
		}
		
	}
	/**
	 * Method to createHourlyWages
	 * @param hourlyWage
	 * @return
	 */
	private SalaryHourlyWageVo createHourlyWageSalaryVo(SalaryHourlyWage hourlyWage){
		SalaryHourlyWageVo vo = new SalaryHourlyWageVo();
		if(hourlyWage.getEmployee().getUserProfile()!=null)
			vo.setEmployee(hourlyWage.getEmployee().getUserProfile().getFirstName()+" "+hourlyWage.getEmployee().getUserProfile().getLastname());
		vo.setEmployeeCode(hourlyWage.getEmployee().getEmployeeCode());

		vo.setWithEffectFrom(DateFormatter.convertDateToString(hourlyWage.getWithEffectFrom()));

		vo.setRegularHourSalary(hourlyWage.getRegularHourSalary().toString());
		vo.setOverTimerHourSalary(hourlyWage.getOverTimeHourSalary().toString());
		vo.setTax(hourlyWage.getTax().toString());

		return vo;
	}
	
	/**
	 * Method to get daily wage for a day
	 * @param employeeCode
	 * @param date
	 * @return SalaryHourlyWageVo
	 */
	@SuppressWarnings("unchecked")
	public SalaryDailyWagesVo getDailyWageForDay(String employeeCode ,Date date) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<SalaryDailyWage> dailyWages = this.sessionFactory.getCurrentSession().createCriteria(SalaryDailyWage.class)
				.add(Restrictions.eq("employee", employee))
				.add(Restrictions.le("withEffectFrom",date)).list();
		int size = dailyWages.size();
		if(size>0)
		{
			SalaryDailyWage dailyWage = dailyWages.get(size-1);
			return createDailyWageSlaryVo(dailyWage);
		}
		else
		{
			return null;
		}
		
	}
	/**
	 * Method to create dailywage vo
	 * @param wage
	 * @return
	 */
	private SalaryDailyWagesVo createDailyWageSlaryVo(SalaryDailyWage wage){
		SalaryDailyWagesVo dailyWagesVo = new SalaryDailyWagesVo();

		if(wage.getEmployee().getUserProfile()!=null)
			dailyWagesVo.setEmployee(wage.getEmployee().getUserProfile().getFirstName()+" "+wage.getEmployee().getUserProfile().getLastname());
		dailyWagesVo.setEmployeeCode(wage.getEmployee().getEmployeeCode());

		dailyWagesVo.setWithEffectFrom(DateFormatter.convertDateToString(wage.getWithEffectFrom()));

		dailyWagesVo.setSalary(wage.getSalary().toString());
		dailyWagesVo.setTax(wage.getTax().toString());
		return dailyWagesVo;
	}

	/**
	 * Method to get all leaves taken by employee
	 * @param employeeId
	 * @return
	 */
	public List<LeavesVo> getAllLeavesTakenByEmployee(Long employeeId , String start , String end)
	{
		Date startDate = DateFormatter.convertStringToDate(start);
		Date endDate = DateFormatter.convertStringToDate(end);
		List<LeavesVo> leavesVos = new ArrayList<LeavesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Leaves.class);
		criteria.add(Restrictions.eq("employee.id", employeeId));
		criteria.add(Restrictions.ge("fromDate", startDate));
		criteria.add(Restrictions.le("fromDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Leaves> leaves = criteria.list();
		for(Leaves leave : leaves){
			leavesVos.add(createSimpleLeavesVo(leave));
		}
		return leavesVos;
	}
	
	private LeavesVo createSimpleLeavesVo(Leaves leaves)
	{
		LeavesVo vo = new LeavesVo();
		vo.setReason(leaves.getReason());
		vo.setEmployeeId(leaves.getEmployee().getId());
		vo.setEmployeeCode(leaves.getEmployee().getEmployeeCode());
		vo.setNotes(leaves.getNotes());
		vo.setLeaveBalance(leaves.getLeavesBalance());
		vo.setTotalLeaves(leaves.getTotalLeaves());
		UserProfile profile = leaves.getEmployee().getUserProfile();
		if(profile!=null)
			vo.setEmployee(profile.getFirstName()+" "+profile.getLastname());
		vo.setLeaveId(leaves.getId());
		vo.setDescription(leaves.getDescription());
		if(leaves.getFromDate()!=null)
			vo.setFromDate(DateFormatter.convertDateToString(leaves.getFromDate()));
		if(leaves.getToDate()!=null)
			vo.setToDate(DateFormatter.convertDateToString(leaves.getToDate()));

		vo.setStatus(leaves.getLeaveStatus().getName());
		vo.setDuration(leaves.getDuration());
		vo.setLeaveType(leaves.getLeaveType().getType());
		return vo;

	}
}
