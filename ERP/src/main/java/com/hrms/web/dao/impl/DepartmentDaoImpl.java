package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.DepartmentDao;
import com.hrms.web.dao.ProjectStatusDao;
import com.hrms.web.entities.BranchAddress;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.DepartmentDocuments;
import com.hrms.web.entities.DepartmentHead;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Project;
import com.hrms.web.entities.ProjectDocuments;
import com.hrms.web.entities.Termination;
import com.hrms.web.entities.TerminationDocuments;
import com.hrms.web.entities.UserActivities;
import com.hrms.web.logger.Log;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.DepartmentDocumentsVo;
import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.ProjectDocumentsVo;

/**
 * 
 * @author Sangeeth and Bibin
 *
 */

@Repository
public class DepartmentDaoImpl extends AbstractDao implements DepartmentDao {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private EmployeeService employeeService;

	/**
	 * Method to save new Department
	 * 
	 * @param departmenttVo
	 * @return void
	 */
	public void saveDepartment(DepartmentVo departmentVo) {

		try {
			logger.info("inside>> DepartmentDao... saveDepartment");

			Department department = new Department();
			department.setName(departmentVo.getName());
			department.setSortingOrder(departmentVo.getSortingOrder());
			department.setBranch(branchService.findAndReturnBranchById(departmentVo.getBranchId()));
			department.setDepartmentHead(setDepartmentHead(departmentVo.getDepartmentHeadId(), department));

			department.setParentDepartment(findParentDepartmentById(departmentVo.getParentDepartmentId()));
			department.setAdditionalInformation(departmentVo.getAdditionalInformation());
			this.sessionFactory.getCurrentSession().saveOrUpdate(department);
			userActivitiesService.saveUserActivity("Save Department "+department.getName());
			departmentVo.setId(department.getId());
			departmentVo.setName(department.getName());
			departmentVo.setDepartmentDocuments(department.getDepartmentDocuments());


		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}

	}

	/**
	 * method to set department head
	 * 
	 * @param departmentHeadId
	 * @param department
	 * @return departmentHead
	 */
	private DepartmentHead setDepartmentHead(Long departmentHeadId, Department department) {
		DepartmentHead departmentHead = null;
		if (departmentHeadId != null) {
			departmentHead = department.getDepartmentHead();
			Employee employee = employeeService.findAndReturnEmployeeById(departmentHeadId);
			if (departmentHead == null)
				departmentHead = new DepartmentHead();
			departmentHead.setEmployee(employee);
			departmentHead.setDepartment(department);
		}
		return departmentHead;
	}

	public List<DepartmentVo> findAllDepartments() {
		logger.info("Inside findAlldepartments");
		List<DepartmentVo> departmentVo = new ArrayList<DepartmentVo>();

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Department.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Department> departments = criteria.list();

		for (Department department : departments) {
			departmentVo.add(createNameIdDepartmentVo(department));
		}
		return departmentVo;
	}

	/**
	 * method to create departmentVo from department object to return name and
	 * id only
	 * 
	 * @param department
	 * @return
	 */
	private DepartmentVo createNameIdDepartmentVo(Department department) {
		logger.info("inside>> Department... createDepartmentVo");
		DepartmentVo departmentVo = new DepartmentVo();
		departmentVo.setId(department.getId());
		departmentVo.setName(department.getName());
		return departmentVo;
	}

	/**
	 * method to create departmentVo from department object
	 * 
	 * @param department
	 * @return
	 */
	private DepartmentVo createDepartmentVo(Department department) {
		logger.info("inside>> Deparrment... createDepartmentVo");
		DepartmentVo departmentVo = new DepartmentVo();
		departmentVo.setId(department.getId());
		departmentVo.setName(department.getName());
		departmentVo.setCreatedBy(department.getCreatedBy().getUserName());
		if(department.getCreatedOn()!=null)
			departmentVo.setCreatedOn((department.getCreatedOn()));
		if (department.getParentDepartment() != null) {
			departmentVo.setParentDepartment(department.getParentDepartment());
			departmentVo.setParentDepartmentId(department.getParentDepartment().getId());
			departmentVo.setParentDepartmentName(department.getParentDepartment().getName());
		}
		if (department.getBranch() != null) {
			departmentVo.setBranch(department.getBranch());
			departmentVo.setBranchId(department.getBranch().getId());
			departmentVo.setBranchName(department.getBranch().getBranchName());
		}
		if (department.getEmployee() != null) {
			departmentVo.setEmployeeName(setEmployeesName(department.getEmployee()));
			departmentVo.setEmployeeId(setEmployeeIds(department.getEmployee()));
		}
		if (department.getDepartmentHead() != null) {
			departmentVo.setDepartmentHeadId(department.getDepartmentHead().getEmployee().getId());
			departmentVo.setDepartmentHeadName(department.getDepartmentHead().getEmployee().getEmployeeCode());
		}
		departmentVo.setAdditionalInformation(department.getAdditionalInformation());
		departmentVo.setSortingOrder(department.getSortingOrder());
		departmentVo.setDepartmentDocuments(department.getDepartmentDocuments());
		// departmentVo.setEmployeeName(setEmployeesName(department.getEmployee()));

		return departmentVo;
	}

	public List<DepartmentVo> findAllDepartmentDetails() {
		logger.info("Inside findAlldepartments");
		List<DepartmentVo> departmentVo = new ArrayList<DepartmentVo>();
		List<Department> departments = this.sessionFactory.getCurrentSession().createCriteria(Department.class).list();
		for (Department department : departments) {
			departmentVo.add(createDepartmentVo(department));
		}
		return departmentVo;
	}

	public DepartmentVo findDepartmentById(Long departmentId) {
		logger.info("inside>> DepartmentDao... findDepartmentById");
		Department department = (Department) this.sessionFactory.getCurrentSession().createCriteria(Department.class)
				.add(Restrictions.eq("id", departmentId)).uniqueResult();
		return createDepartmentVo(department);
	}

	public Department findParentDepartmentById(Long departmentId) {
		logger.info("inside>> DepartmentDao... findDepartmentById");
		Department department = (Department) this.sessionFactory.getCurrentSession().createCriteria(Department.class)
				.add(Restrictions.eq("id", departmentId)).uniqueResult();
		return department;
	}

	public void deleteDepartment(Long id) {
		logger.info("inside>> DepartmentDao... deleteDepartment");
		Department department = findAndReturnDepartmentObjectById(id);
		department.setParentDepartment(null);
		String departmentName=department.getName();
		this.sessionFactory.getCurrentSession().delete(findAndReturnDepartmentObjectById(id));
		userActivitiesService.saveUserActivity("Deleted Department "+departmentName);

	}

	/**
	 * Method to find and return Department Object by using departmentName
	 * 
	 * @param departmentName
	 * @return Department
	 */
	public Department findAndReturnDepartmentObjectByName(String departmentName) {
		logger.info("inside>> DepartmentDao... findAndReturnDepartmentObjectByName");
		return (Department) this.sessionFactory.getCurrentSession().createCriteria(Department.class)
				.add(Restrictions.eq("name", departmentName)).uniqueResult();
	}

	/**
	 * Method to find and return Department Object by using department Id
	 * 
	 * @param departmentId
	 * @return Department
	 */
	public Department findAndReturnDepartmentObjectById(Long departmentId) {
		logger.info("inside>> DepartmentDao... findAndReturnDepartmentObjectById");
		return (Department) this.sessionFactory.getCurrentSession().createCriteria(Department.class)
				.add(Restrictions.eq("id", departmentId)).uniqueResult();
	}

	public void updateDepartment(DepartmentVo departmentVo) {
		logger.info("inside>> DepartmentDao... updateDepartment");
		try {
			Department department = findAndReturnDepartmentObjectByName(departmentVo.getName());
			department.setName(departmentVo.getName());
			department.setSortingOrder(departmentVo.getSortingOrder());
			department.setBranch(branchService.findAndReturnBranchById(departmentVo.getBranchId()));
			department.setDepartmentHead(setDepartmentHead(departmentVo.getDepartmentHeadId(), department));

			department.setParentDepartment(findParentDepartmentById(departmentVo.getParentDepartmentId()));
			department.setAdditionalInformation(departmentVo.getAdditionalInformation());

			this.sessionFactory.getCurrentSession().merge(department);

			userActivitiesService.saveUserActivity("Updated Department "+department.getName());
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}

	}

	/**
	 * method to set employees names
	 * 
	 * @return
	 */
	private List<String> setEmployeesName(Set<Employee> employees) {
		List<String> employeeName = new ArrayList<String>();
		for (Employee employee : employees) {
			employeeName.add(employee.getEmployeeCode());

		}
		return employeeName;
	}

	/**
	 * method to set employee ids
	 * 
	 * @param employees
	 * @return
	 */
	private List<Long> setEmployeeIds(Set<Employee> employees) {
		List<Long> ids = new ArrayList<Long>();
		for (Employee employee : employees) {
			ids.add(employee.getId());
		}
		return ids;
	}

	/**
	 * method to set employees
	 * 
	 * @param employeeId
	 * @return
	 */
	@SuppressWarnings({})
	private Set<Employee> setEmployeeList(List<Long> employeeIds) {
		logger.info("inside>> ProjectDao... setEmployeeList");
		
		Set<Employee> employeesSet = new HashSet<Employee>();
		for (Long employeeId : employeeIds) {
			Employee employeeObj = new Employee();
			employeeObj.setId(employeeId);
			employeesSet.add(employeeObj);
		}
		return employeesSet;
	}

	/**
	 * method to delete project document
	 * 
	 * @param path
	 */
	public void deleteDepartmentDocument(Long docId) {

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(DepartmentDocuments.class)
				.add(Restrictions.eq("id", docId));
		DepartmentDocuments document = (DepartmentDocuments) criteria.uniqueResult();
		Department department = findDepartment(document.getDepartment().getId());
		department.getDepartmentDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(department);
	}

	public Department findDepartment(Long departmentId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Department.class)
				.add(Restrictions.eq("id", departmentId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Department) criteria.uniqueResult();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<DepartmentVo> findDepartmentByBranch(Branch branch) {
		List<DepartmentVo> departmentVos = new ArrayList<DepartmentVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Department.class)
				.add(Restrictions.eq("branch", branch));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Department> departments = criteria.list();
		for (Department department : departments)
			departmentVos.add(createDepartmentVo(department));
		return departmentVos;
	}

	public DepartmentVo updateDepartmentDocument(DepartmentVo departmentVo) {
		Department department=this.findAndReturnDepartmentObjectById(departmentVo.getId());
		department.setDepartmentDocuments(setDocuments(departmentVo.getDepartmentDocument(), department));
		this.sessionFactory.getCurrentSession().merge(department);
		return createDepartmentVo(department);
	}
	
	/**
	 * method to update documents
	 * @param documentsVos
	 * @param project
	 * @return
	 */
	private Set<DepartmentDocuments> setDocuments(List<DepartmentDocumentsVo> documentsVos , Department department){
		Set<DepartmentDocuments> documents = new HashSet<DepartmentDocuments>();
		for(DepartmentDocumentsVo vo : documentsVos){
			DepartmentDocuments document = new DepartmentDocuments();
			document.setDepartment(department);
			document.setDocumentName(vo.getFileName());
			document.setUrl(vo.getUrl());
			documents.add(document);
		}
		return documents;
	}

}
