package com.hrms.web.dao.impl;

import java.util.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.EmployeeDesignationDao;
import com.hrms.web.dao.EmployeeJoiningDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.DailyWages;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.EmployeeAdditionalInformation;
import com.hrms.web.entities.EmployeeDesignation;
import com.hrms.web.entities.EmployeeDocuments;
import com.hrms.web.entities.EmployeeJoining;
import com.hrms.web.entities.EmployeeJoiningDocuments;
import com.hrms.web.entities.EmployeeLeave;
import com.hrms.web.entities.EmployeeType;
import com.hrms.web.entities.HourlyWages;
import com.hrms.web.entities.Project;
import com.hrms.web.entities.User;
import com.hrms.web.entities.UserProfile;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeDetailsVo;
import com.hrms.web.vo.EmployeeDocumentsVo;
import com.hrms.web.vo.EmployeeJoiningVo;
import com.hrms.web.vo.EmployeeLeaveVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.JoiningJsonVo;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.SubordinateVo;
import com.hrms.web.vo.SuperiorSubordinateVo;
import com.hrms.web.vo.SuperiorVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author shamsheer
 * @since 23-March-2015
 */
@Repository
public class EmployeeJoiningDaoImpl extends AbstractDao implements EmployeeJoiningDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private TempNotificationDao notificationDao;
	
	@Autowired
	private BranchDao branchDao;
	
	@Autowired
	private EmployeeDesignationDao designationDao;

	/**
	 * method to save new employee joining
	 * 
	 * @return
	 */
	public Long saveEmployeeJoining(JoiningJsonVo joiningVo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(joiningVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");

		Department department = new Department();
		department.setId(joiningVo.getDepartmentId());

		Branch branch = branchDao.findAndReturnBranchObjectById(joiningVo.getBranchId());

		EmployeeType employeeType = new EmployeeType();
		employeeType.setId(joiningVo.getEmployeeTypeId());

		EmployeeDesignation designation = designationDao.findDesignationById(joiningVo.getDesignationId());

		employee.setBranch(branch);
		employee.setEmployeeType(employeeType);
		employee.setDepartmentEmployees(department);
		employee.setEmployeeDesignation(designation);

		EmployeeAdditionalInformation additionalInformation = null;
		if (employee.getAdditionalInformation() != null) {
			additionalInformation = employee.getAdditionalInformation();
			additionalInformation.setJoiningDate(DateFormatter.convertStringToDate(joiningVo.getJoiningDate()));
		}

		this.sessionFactory.getCurrentSession().merge(employee);

		EmployeeJoining employeeJoining = new EmployeeJoining();
		employeeJoining.setNotes(joiningVo.getNotes());
		employeeJoining.setJoiningDate(DateFormatter.convertStringToDate(joiningVo.getJoiningDate()));
		employeeJoining.setDepartment(department);
		employeeJoining.setBranch(branch);
		employeeJoining.setEmployee(employee);
		employeeJoining.setEmployeeType(employeeType);
		employeeJoining.setDesignation(designation);

		Long joiningId = (Long) this.sessionFactory.getCurrentSession().save(employeeJoining);
		userActivitiesService.saveUserActivity("Added new Employee Joining " + joiningVo.getEmployeeCode());

		
		if (joiningVo.getSuperiorIds()!=null && joiningVo.getSuperiorIds().size() > 0) {
			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setEmployeeIds(joiningVo.getSuperiorIds());
			EmployeeVo employeeVo = employeeDao.findDetailedEmployee(employee.getId());
			notificationVo.setSubject(employeeVo.getEmployeeName() + " had joined " + branch.getBranchName()
					+ " as a " + designation.getDesignation()+".");
			notificationDao.saveNotification(notificationVo);
		}
		return joiningId;
	}

	/**
	 * method to delete joining
	 */
	public void deleteEmployeeJoining(Long id) {
		EmployeeJoining employeeJoining = findJoiningById(id);
		if (employeeJoining.getEmployeeJoiningsDocuments() != null) {
			SFTPOperation operation = new SFTPOperation();
			for (EmployeeJoiningDocuments document : employeeJoining.getEmployeeJoiningsDocuments()) {
				operation.removeFileFromSFTP(document.getFileUrl());
			}
		}
		this.sessionFactory.getCurrentSession().delete(employeeJoining);
		userActivitiesService.saveUserActivity("Deleted Employee Joining");
	}

	public void updateJoining(JoiningJsonVo joiningVo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(joiningVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");

		Department department = new Department();
		department.setId(joiningVo.getDepartmentId());

		Branch branch = branchDao.findAndReturnBranchObjectById(joiningVo.getBranchId());

		EmployeeType employeeType = new EmployeeType();
		employeeType.setId(joiningVo.getEmployeeTypeId());

		EmployeeDesignation designation = designationDao.findDesignationById(joiningVo.getDesignationId());

		EmployeeAdditionalInformation additionalInformation = null;
		if (employee.getAdditionalInformation() != null) {
			additionalInformation = employee.getAdditionalInformation();
			additionalInformation.setJoiningDate(DateFormatter.convertStringToDate(joiningVo.getJoiningDate()));
		}

		employee.setBranch(branch);
		employee.setEmployeeType(employeeType);
		employee.setDepartmentEmployees(department);
		employee.setEmployeeDesignation(designation);

		this.sessionFactory.getCurrentSession().merge(employee);

		EmployeeJoining employeeJoining = findJoiningById(joiningVo.getJoiningId());
		employeeJoining.setNotes(joiningVo.getNotes());
		employeeJoining.setJoiningDate(DateFormatter.convertStringToDate(joiningVo.getJoiningDate()));
		employeeJoining.setDepartment(department);
		employeeJoining.setBranch(branch);
		employeeJoining.setEmployee(employee);
		employeeJoining.setEmployeeType(employeeType);
		employeeJoining.setDesignation(designation);
		if (joiningVo.getSuperiorIds().size() > 0) {
			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setEmployeeIds(joiningVo.getSuperiorIds());
			EmployeeVo employeeVo = employeeDao.findDetailedEmployee(employee.getId());
			notificationVo.setSubject(employeeVo.getEmployeeName() + " had joined " + branch.getBranchName()
					+ " as a " + designation.getDesignation()+".");
			notificationDao.saveNotification(notificationVo);
		}
		this.sessionFactory.getCurrentSession().merge(employeeJoining);
		userActivitiesService.saveUserActivity("Updated Joining " + joiningVo.getEmployeeCode());
	}

	public EmployeeJoining findJoiningById(Long id) {
		return (EmployeeJoining) this.sessionFactory.getCurrentSession().createCriteria(EmployeeJoining.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public EmployeeJoining findJoiningByEmployee(Long empId) {
		return (EmployeeJoining) this.sessionFactory.getCurrentSession().createCriteria(EmployeeJoining.class)
				.add(Restrictions.eq("employee.id", empId)).uniqueResult();
	}

	/**
	 * method to list all joinings
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeJoiningVo> listAllJoinings() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeJoining.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeJoining> employeeJoinings = criteria.list();
		List<EmployeeJoiningVo> employeeJoiningVos = new ArrayList<EmployeeJoiningVo>();
		for (EmployeeJoining employeeJoining : employeeJoinings) {
			employeeJoiningVos.add(createJoiningVo(employeeJoining));
		}
		return employeeJoiningVos;
	}

	/**
	 * method to create joining vo
	 * 
	 * @param joining
	 * @return
	 */
	private EmployeeJoiningVo createJoiningVo(EmployeeJoining joining) {
		EmployeeJoiningVo vo = new EmployeeJoiningVo();
		Employee employee = joining.getEmployee();
		vo.setJoiningId(joining.getId());
		if (employee.getUserProfile() != null)
			vo.setEmployee(employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname());
		vo.setEmployeeCode(joining.getEmployee().getEmployeeCode());
		vo.setJoiningDate(DateFormatter.convertDateToString(joining.getJoiningDate()));
		vo.setBranchName(joining.getBranch().getBranchName());
		vo.setDepartmentName(joining.getDepartment().getName());

		vo.setCurrentDepartment(employee.getDepartmentEmployees().getName());
		vo.setCurrentBranch(employee.getBranch().getBranchName());
		return vo;
	}

	/**
	 * method to upload documents
	 */
	public void updateDocuments(EmployeeJoiningVo employeeJoiningVo) {
		EmployeeJoining employeeJoining = findJoiningById(employeeJoiningVo.getJoiningId());
		employeeJoining.setEmployeeJoiningsDocuments(setDocuments(employeeJoiningVo.getDocuments(), employeeJoining));
		this.sessionFactory.getCurrentSession().merge(employeeJoining);
	}

	/**
	 * method to set employee documents
	 * 
	 * @param documentsVos
	 * @param employee
	 * @return
	 */
	private Set<EmployeeJoiningDocuments> setDocuments(List<EmployeeDocumentsVo> documentsVos, EmployeeJoining employee) {
		Set<EmployeeJoiningDocuments> employeeDocuments = new HashSet<EmployeeJoiningDocuments>();
		for (EmployeeDocumentsVo documentsVo : documentsVos) {
			EmployeeJoiningDocuments documents = new EmployeeJoiningDocuments();
			documents.setFileName(documentsVo.getFileName());
			documents.setFileUrl(documentsVo.getDocumnetUrl());
			documents.setEmployeeJoining(employee);
			employeeDocuments.add(documents);
		}
		return employeeDocuments;
	}

	public EmployeeJoiningVo findDetailedJoining(Long id) {
		return createDetailedVo(findJoiningById(id));
	}

	/**
	 * method to create detailed vo
	 * 
	 * @param joining
	 * @return
	 */
	private EmployeeJoiningVo createDetailedVo(EmployeeJoining joining) {
		EmployeeJoiningVo vo = new EmployeeJoiningVo();
		vo.setCreatedBy(joining.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToDetailedString(joining.getCreatedOn()));
		vo.setJoiningId(joining.getId());
		vo.setJoiningDate(DateFormatter.convertDateToString(joining.getJoiningDate()));
		vo.setEmployeeVo(employeeDao.findDetailedEmployee(joining.getEmployee().getId()));
		vo.setEmployeeId(joining.getEmployee().getId());
		vo.setEmployeeCode(joining.getEmployee().getEmployeeCode());
		vo.setEmployee(joining.getEmployee().getUserProfile().getFirstName() + " "
				+ joining.getEmployee().getUserProfile().getLastname());
		vo.setDepartmentId(joining.getDepartment().getId());
		vo.setDepartmentName(joining.getDepartment().getName());
		vo.setBranchId(joining.getBranch().getId());
		vo.setBranchName(joining.getBranch().getBranchName());
		vo.setEmployeeTypeId(joining.getEmployeeType().getId());
		vo.setEmployeeType(joining.getEmployeeType().getType());
		vo.setDesignationId(joining.getDesignation().getId());
		vo.setDesignation(joining.getDesignation().getDesignation());
		vo.setNotes(joining.getNotes());
		if (joining.getEmployeeJoiningsDocuments() != null) {
			List<EmployeeDocumentsVo> documentsVos = new ArrayList<EmployeeDocumentsVo>();
			for (EmployeeJoiningDocuments documents : joining.getEmployeeJoiningsDocuments()) {
				EmployeeDocumentsVo documentsVo = new EmployeeDocumentsVo();
				documentsVo.setDocumnetUrl(documents.getFileUrl());
				documentsVo.setFileName(documents.getFileName());
				documentsVos.add(documentsVo);
			}
			vo.setDocuments(documentsVos);
		}
		return vo;
	}

	public void deleteDocument(String url) {
		EmployeeJoiningDocuments documents = (EmployeeJoiningDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeJoiningDocuments.class).add(Restrictions.eq("fileUrl", url)).uniqueResult();

		EmployeeJoining employeeJoining = findJoiningById(documents.getEmployeeJoining().getId());
		employeeJoining.getEmployeeJoiningsDocuments().remove(documents);

		this.sessionFactory.getCurrentSession().merge(employeeJoining);

		this.sessionFactory.getCurrentSession().delete(documents);
	}

	/**
	 * method to get employee details by code
	 */
	public EmployeeDetailsVo getEmployeeDetails(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode.trim());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		return createEmployeeDetailsVo(employee);
	}

	/**
	 * method to return employee details vo
	 * 
	 * @param employee
	 * @return
	 */
	private EmployeeDetailsVo createEmployeeDetailsVo(Employee employee) {
		EmployeeDetailsVo detailVo = new EmployeeDetailsVo();
		// setting superiors
		SuperiorSubordinateVo vo = new SuperiorSubordinateVo();
		if (employee.getSuperiors() != null) {
			List<SuperiorVo> superiorVos = new ArrayList<SuperiorVo>();
			List<Long> ids = new ArrayList<Long>();
			for (Employee superior : employee.getSuperiors()) {
				superiorVos.add(createSuperiorVo(superior));
				ids.add(superior.getId());
			}
			vo.setSuperiors(superiorVos);
		}
		detailVo.setSuperiorSubordinateVo(vo);

		// setting designation & department
		if (employee.getEmployeeDesignation() != null) {
			detailVo.setDesignation(employee.getEmployeeDesignation().getDesignation());
			detailVo.setDesignationId(employee.getEmployeeDesignation().getId());
		}

		if (employee.getDepartmentEmployees() != null) {
			detailVo.setEmployeeDepartment(employee.getDepartmentEmployees().getName());
			detailVo.setEmployeeDepartmentId(employee.getDepartmentEmployees().getId());
		}

		// setting employee type & grade
		if (employee.getEmployeeType() != null) {
			detailVo.setEmployeeType(employee.getEmployeeType().getType());
			detailVo.setEmployeeTypeId(employee.getEmployeeType().getId());
		}

		if (employee.getBranch() != null) {
			detailVo.setEmployeeBranch(employee.getBranch().getBranchName());
			detailVo.setEmployeeBranchId(employee.getBranch().getId());
		}

		if (employee.getEmployeegrade() != null) {
			detailVo.setEmployeeGrade(employee.getEmployeegrade().getGrade());
			detailVo.setEmployeeGradeId(employee.getEmployeegrade().getId());
		}

		// setting employee category
		if (employee.getEmployeeCategory() != null) {
			detailVo.setEmployeeCategory(employee.getEmployeeCategory().getCategory());
			detailVo.setEmployeeCategoryId(employee.getEmployeeCategory().getId());
		}

		// setting user details
		if (employee.getUser() != null) {
			detailVo.setEmail(employee.getUser().getEmail());
		}

		// seeting user profile details
		if (employee.getUserProfile() != null) {
			detailVo.setEmployeeName(employee.getUserProfile().getFirstName() + " "
					+ employee.getUserProfile().getLastname());
		}

		// setting hourly wages
		HourlyWages hourlyWages = employee.getHourlyWages();
		if (hourlyWages != null) {
			detailVo.setHourlyWagesDescription(hourlyWages.getDescription());
			detailVo.setHourlyWagesId(hourlyWages.getId());
			detailVo.setHourlyWagesNotes(hourlyWages.getNotes());
			if (hourlyWages.getRegularHourAmount() != null)
				detailVo.setRegularHoursAmount(hourlyWages.getRegularHourAmount().toString());
			if (hourlyWages.getOverTimeHourAmount() != null)
				detailVo.setOverTimeHoursAmount(hourlyWages.getOverTimeHourAmount().toString());
		}

		// setting daily wages
		DailyWages dailyWages = employee.getDailyWages();
		if (dailyWages != null) {
			detailVo.setDailyWage(dailyWages.getWage().toString());
			detailVo.setDailyWageDescription(dailyWages.getDescription());
			detailVo.setDailyWageId(dailyWages.getId());
			detailVo.setDailyWageNotes(dailyWages.getNotes());
			detailVo.setDailyWageTitle(dailyWages.getTitle());
		}

		// setting joining date
		if (employee.getAdditionalInformation() != null) {
			if (employee.getAdditionalInformation().getJoiningDate() != null)
				detailVo.setJoiningDate(DateFormatter.convertDateToString(employee.getAdditionalInformation()
						.getJoiningDate()));
		}
		// setting salary type
		if (employee.getSalaryType() != null) {
			detailVo.setSalaryType(employee.getSalaryType().getType());
			detailVo.setSalaryTypeId(employee.getSalaryType().getId());
		}

		// setting leave type
		if (employee.getLeaves() != null) {
			List<EmployeeLeaveVo> leaveVos = new ArrayList<EmployeeLeaveVo>();
			for (EmployeeLeave leave : employee.getLeaves()) {
				EmployeeLeaveVo leaveVo = new EmployeeLeaveVo();
				leaveVo.setCarryOver(leave.getCarryOver());
				leaveVo.setLeaveId(leave.getId());
				leaveVo.setLeaveTitleId(leave.getTitle().getId());
				leaveVo.setLeaveTitle(leave.getTitle().getType());
				leaveVo.setLeaveType(leave.getType());
				leaveVos.add(leaveVo);
			}
			detailVo.setLeaveVos(leaveVos);
		}

		// setting project for assignment
		if (employee.getProject() != null) {
			List<ProjectVo> projectVos = new ArrayList<ProjectVo>();
			for (Project project : employee.getProject()) {
				ProjectVo projectVo = new ProjectVo();
				projectVo.setProjectId(project.getId());
				projectVo.setProjectTitle(project.getTitle());
				projectVos.add(projectVo);
			}
			detailVo.setProjectVo(projectVos);
		}
		return detailVo;
	}

	/**
	 * method to create superiorVo
	 * 
	 * @param superior
	 * @return
	 */
	private SuperiorVo createSuperiorVo(Employee superior) {
		SuperiorVo superiorVo = new SuperiorVo();
		superiorVo.setSuperiorId(superior.getId());
		superiorVo.setSuperiorCode(superior.getEmployeeCode());
		if (superior.getUserProfile() != null)
			superiorVo.setSuperiorName(superior.getUserProfile().getFirstName() + " "
					+ superior.getUserProfile().getLastname());
		return superiorVo;
	}

	/**
	 * method to get employee joining by start date & end date
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeJoiningVo> listJoiningByStartDateEndDate(String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeJoining.class);
		criteria.add(Restrictions.ge("joiningDate", startdate)).add(Restrictions.le("joiningDate", enddate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeJoining> employeeJoinings = criteria.list();
		List<EmployeeJoiningVo> employeeJoiningVos = new ArrayList<EmployeeJoiningVo>();
		for (EmployeeJoining employeeJoining : employeeJoinings) {
			employeeJoiningVos.add(createJoiningVo(employeeJoining));
		}
		return employeeJoiningVos;
	}

	/**
	 * method to get employee joining by branch , start date & end date
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeJoiningVo> listJoiningByBranchStartDateEndDate(Long branchId, String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeJoining.class)
				.add(Restrictions.eq("branch.id", branchId));
		criteria.add(Restrictions.ge("joiningDate", startdate)).add(Restrictions.le("date", enddate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeJoining> employeeJoinings = criteria.list();
		List<EmployeeJoiningVo> employeeJoiningVos = new ArrayList<EmployeeJoiningVo>();
		for (EmployeeJoining employeeJoining : employeeJoinings) {
			employeeJoiningVos.add(createJoiningVo(employeeJoining));
		}
		return employeeJoiningVos;
	}

	/**
	 * method to get employee joining by branch , department , start date & end
	 * date
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeJoiningVo> listJoiningByBranchDepartmentStartDateEndDate(Long branchId, Long departmentId,
			String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeJoining.class)
				.add(Restrictions.eq("branch.id", branchId)).add(Restrictions.eq("department.id", departmentId));
		criteria.add(Restrictions.ge("joiningDate", startdate)).add(Restrictions.le("date", enddate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeJoining> employeeJoinings = criteria.list();
		List<EmployeeJoiningVo> employeeJoiningVos = new ArrayList<EmployeeJoiningVo>();
		for (EmployeeJoining employeeJoining : employeeJoinings) {
			employeeJoiningVos.add(createJoiningVo(employeeJoining));
		}
		return employeeJoiningVos;
	}

	/**
	 * method to get employee joining by department , startDate & end Date
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeJoiningVo> listJoiningByDepartmentStartDateEndDate(Long departmentId, String startDate,
			String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeJoining.class)
				.add(Restrictions.eq("department.id", departmentId));
		criteria.add(Restrictions.ge("joiningDate", startdate)).add(Restrictions.le("date", enddate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeJoining> employeeJoinings = criteria.list();
		List<EmployeeJoiningVo> employeeJoiningVos = new ArrayList<EmployeeJoiningVo>();
		for (EmployeeJoining employeeJoining : employeeJoinings) {
			employeeJoiningVos.add(createJoiningVo(employeeJoining));
		}
		return employeeJoiningVos;
	}
}
