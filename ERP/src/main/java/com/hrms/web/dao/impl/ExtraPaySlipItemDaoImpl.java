package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ExtraPaySlipItemDao;
import com.hrms.web.entities.ExtraPaySlipItem;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.ExtraPaySlipItemVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Repository
public class ExtraPaySlipItemDaoImpl extends AbstractDao implements ExtraPaySlipItemDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveOrUpdateExtraPaySlipItem(ExtraPaySlipItemVo extraPaySlipItemVo) {
		ExtraPaySlipItemVo slipItemVo = findExtraPaySlipItemByTitle(extraPaySlipItemVo.getTitle());
		ExtraPaySlipItem paySlipItem = findExtraPaySlipItem(extraPaySlipItemVo.getExtraPaySlipItemId());
		if (paySlipItem == null) {
			paySlipItem = new ExtraPaySlipItem();
			if (slipItemVo != null)
				throw new DuplicateItemException("Item with this title already available");
		} else {
			if (slipItemVo != null && slipItemVo.getExtraPaySlipItemId() != extraPaySlipItemVo.getExtraPaySlipItemId())
				throw new DuplicateItemException("Item with this title already available");
		}
		paySlipItem.setTitle(extraPaySlipItemVo.getTitle());
		paySlipItem.setType(extraPaySlipItemVo.getType());
		paySlipItem.setTaxAllowance(Boolean.valueOf(extraPaySlipItemVo.getTaxAllowance()));
		paySlipItem.setCalculation(extraPaySlipItemVo.getCalculation());
		paySlipItem.setEffectFrom(DateFormatter.convertStringToDate(extraPaySlipItemVo.getEffectFrom()));
		this.sessionFactory.getCurrentSession().saveOrUpdate(paySlipItem);
	}

	public void deleteExtraPaySlipItem(Long extraPaySlipItemId) {
		ExtraPaySlipItem paySlipItem = findExtraPaySlipItem(extraPaySlipItemId);
		if (paySlipItem == null)
			throw new ItemNotFoundException("PaySlip Item Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(paySlipItem);
	}

	public ExtraPaySlipItem findExtraPaySlipItem(Long extraPaySlipItemId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ExtraPaySlipItem.class)
				.add(Restrictions.eq("id", extraPaySlipItemId));
		if (criteria.uniqueResult() == null)
			return null;
		return (ExtraPaySlipItem) criteria.uniqueResult();
	}

	/**
	 * method to create ExtraPaySlipItemVo object
	 * 
	 * @param paySlipItem
	 * @return paySlipItemVo
	 */
	private ExtraPaySlipItemVo createExtraPaySlipItemVo(ExtraPaySlipItem paySlipItem) {
		ExtraPaySlipItemVo paySlipItemVo = new ExtraPaySlipItemVo();
		paySlipItemVo.setExtraPaySlipItemId(paySlipItem.getId());
		paySlipItemVo.setTitle(paySlipItem.getTitle());
		paySlipItemVo.setType(paySlipItem.getType());
		paySlipItemVo.setTaxAllowance(String.valueOf(paySlipItem.getTaxAllowance()));
		paySlipItemVo.setCalculation(paySlipItem.getCalculation());
		paySlipItemVo.setEffectFrom(DateFormatter.convertDateToString(paySlipItem.getEffectFrom()));
		return paySlipItemVo;
	}

	public ExtraPaySlipItemVo findExtraPaySlipItemById(Long extraPaySlipItemId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ExtraPaySlipItem.class)
				.add(Restrictions.eq("id", extraPaySlipItemId));
		if (criteria.uniqueResult() == null)
			return null;
		return createExtraPaySlipItemVo((ExtraPaySlipItem) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ExtraPaySlipItemVo> findAllExtraPaySlipItem() {
		List<ExtraPaySlipItemVo> paySlipItemVos = new ArrayList<ExtraPaySlipItemVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ExtraPaySlipItem.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<ExtraPaySlipItem> items = criteria.list();
		for (ExtraPaySlipItem item : items)
			paySlipItemVos.add(createExtraPaySlipItemVo(item));
		return paySlipItemVos;
	}

	public ExtraPaySlipItemVo findExtraPaySlipItemByTitle(String title) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ExtraPaySlipItem.class)
				.add(Restrictions.eq("title", title));
		if (criteria.uniqueResult() == null)
			return null;
		return createExtraPaySlipItemVo((ExtraPaySlipItem) criteria.uniqueResult());
	}

	public ExtraPaySlipItem findExtraPaySlipItem(String title) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ExtraPaySlipItem.class)
				.add(Restrictions.eq("title", title));
		if (criteria.uniqueResult() == null)
			return null;
		return (ExtraPaySlipItem) criteria.uniqueResult();
	}

}
