package com.hrms.web.dao.impl;

import java.util.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.DepartmentDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ForwardApplicationStatusDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.dao.TransferDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.entities.ResignationDocuments;
import com.hrms.web.entities.Transfer;
import com.hrms.web.entities.TransferDocuments;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.TempNotificationVo;
import com.hrms.web.vo.TransferDocumentsVo;
import com.hrms.web.vo.TransferVo;

/**
 * 
 * @author Shamsheer
 * @since 24-April-2015
 */
@Repository
public class TransferDaoImpl extends AbstractDao implements TransferDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private BranchDao branchDao;

	@Autowired
	private DepartmentDao departmentDao;

	@Autowired
	private ForwardApplicationStatusDao statusDao;

	@Autowired
	private TempNotificationDao notificationDao;

	/**
	 * method to save transfer
	 */
	public Long saveTransfer(TransferVo transferVo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(transferVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		Branch branch = branchDao.findAndReturnBranchObjectById(transferVo.getBranchId());
		if (branch == null)
			throw new ItemNotFoundException("Branch Not Exist.");
		Department department = departmentDao.findDepartment(transferVo.getDepartmentId());
		if (department == null)
			throw new ItemNotFoundException("Department Not Exist.");
		Transfer transfer = new Transfer();
		transfer.setEmployee(employee);
		transfer.setTransferFromBranch(employee.getBranch());
		transfer.setTransferFromDepartment(employee.getDepartmentEmployees());
		transfer.setTransferToBranch(branch);
		transfer.setTransferToDepartment(department);
		if (transferVo.getDate() != "")
			transfer.setTransferDate(DateFormatter.convertStringToDate(transferVo.getDate()));
		if (transferVo.getSuperiorCode() != ""&&!transferVo.getSuperiorCode().contains("auto")) {
			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject("A Transfer Request Has Been Received From "+employee.getUserProfile().getFirstName()
					+" "+employee.getUserProfile().getLastname()+".");
			notificationVo.getEmployeeCodes().add(transferVo.getSuperiorCode());
			notificationDao.saveNotification(notificationVo);
			transfer.setForwardApplicationTo(employeeDao.findAndReturnEmployeeByCode(transferVo.getSuperiorCode()));
			ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.REQUESTED);
			transfer.setStatus(status);
		}else
			transfer.setStatus(statusDao.getStatus(StatusConstants.APPROVED));
		transfer.setDescription(transferVo.getDescription());
		transfer.setNotes(transferVo.getNotes());
		return (Long) this.sessionFactory.getCurrentSession().save(transfer);
	}

	/**
	 * method to update transfer
	 */
	public void updateTransfer(TransferVo transferVo) {
		Transfer transfer = getTransfer(transferVo.getTransferId());
		if (transfer == null)
			throw new ItemNotFoundException("Transfer Not Exist.");
		Employee employee = employeeDao.findAndReturnEmployeeByCode(transferVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		Branch branch = branchDao.findAndReturnBranchObjectById(transferVo.getBranchId());
		if (branch == null)
			throw new ItemNotFoundException("Branch Not Exist.");
		Department department = departmentDao.findDepartment(transferVo.getDepartmentId());
		if (department == null)
			throw new ItemNotFoundException("Department Not Exist.");
		transfer.setEmployee(employee);
		transfer.setTransferFromBranch(employee.getBranch());
		transfer.setTransferFromDepartment(employee.getDepartmentEmployees());
		transfer.setTransferToBranch(branch);
		transfer.setTransferToDepartment(department);
		if (transferVo.getDate() != "")
			transfer.setTransferDate(DateFormatter.convertStringToDate(transferVo.getDate()));
		if (transferVo.getSuperiorCode() != "") {
			transfer.setForwardApplicationTo(employeeDao.findAndReturnEmployeeByCode(transferVo.getEmployeeCode()));
			ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.REQUESTED);
			transfer.setStatus(status);
		}
		transfer.setDescription(transferVo.getDescription());
		transfer.setNotes(transferVo.getNotes());
		this.sessionFactory.getCurrentSession().merge(transfer);
	}

	/**
	 * method to delete transfer
	 */
	public void deleteTransfer(Long transferId) {
		Transfer transfer = getTransfer(transferId);
		if (transfer == null)
			throw new ItemNotFoundException("Transfer Not Exist.");
		if (transfer.getDocuments() != null) {
			SFTPOperation operation = new SFTPOperation();
			for (TransferDocuments documents : transfer.getDocuments()) {
				operation.removeFileFromSFTP(documents.getUrl());
			}
		}
		this.sessionFactory.getCurrentSession().delete(transfer);
	}

	/**
	 * method to get Transfer object
	 */
	public Transfer getTransfer(Long transferId) {
		return (Transfer) this.sessionFactory.getCurrentSession().createCriteria(Transfer.class)
				.add(Restrictions.eq("id", transferId)).uniqueResult();
	}

	/**
	 * method to list all transfers
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<TransferVo> listAllTransfer() {
		List<TransferVo> vos = new ArrayList<TransferVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transfer.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Transfer> transfers = criteria.list();
		for (Transfer transfer : transfers) {
			vos.add(createTransferVo(transfer));
		}
		return vos;
	}

	/**
	 * method to list transfers by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TransferVo> listTransferByEmployee(Long employeeId) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist");
		List<TransferVo> vos = new ArrayList<TransferVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transfer.class);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Transfer> transfers = criteria.list();
		for (Transfer transfer : transfers) {
			vos.add(createTransferVo(transfer));
		}
		return vos;
	}

	/**
	 * method to list transfers by employee code
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TransferVo> listTransferByEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist");
		List<TransferVo> vos = new ArrayList<TransferVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transfer.class);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Transfer> transfers = criteria.list();
		for (Transfer transfer : transfers) {
			vos.add(createTransferVo(transfer));
		}
		return vos;
	}

	/**
	 * method to update document
	 */
	public void updateDocument(TransferVo transferVo) {
		Transfer transfer = getTransfer(transferVo.getTransferId());
		if (transfer == null)
			throw new ItemNotFoundException("Transfer Not Exist.");
		transfer.setDocuments(setDocuments(transferVo.getDocumentsVos(), transfer));
		this.sessionFactory.getCurrentSession().merge(transfer);
	}

	/**
	 * method to delete document
	 */
	public void deleteDocument(String url) {
		TransferDocuments documents = (TransferDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(TransferDocuments.class).add(Restrictions.eq("url", url)).uniqueResult();
		Transfer transfer = documents.getTransfer();
		transfer.getDocuments().remove(documents);
		this.sessionFactory.getCurrentSession().merge(transfer);
		this.sessionFactory.getCurrentSession().delete(documents);
	}

	/**
	 * method to update transfer
	 */
	public void updateStatus(TransferVo transferVo) {
		Transfer transfer = getTransfer(transferVo.getTransferId());
		if (transfer == null)
			throw new ItemNotFoundException("Transfer Not Exist.");
		transfer.setStatusDescription(transferVo.getStatusDescription());
		ForwardApplicationStatus status = statusDao.getStatus(transferVo.getStatus());
		transfer.setStatus(status);
		if (transferVo.getSuperiorCode() != "")
			transfer.setForwardApplicationTo(employeeDao.findAndReturnEmployeeByCode(transferVo.getSuperiorCode()));
		else
			transfer.setForwardApplicationTo(null);
		
		TempNotificationVo notificationVo  = new TempNotificationVo();
		notificationVo.getEmployeeIds().add(transfer.getEmployee().getId());
		notificationVo.setSubject(transfer.getEmployee().getEmployeeCode()+"'s Transfer Request "+transferVo.getStatus());
		notificationDao.saveNotification(notificationVo);
		
		this.sessionFactory.getCurrentSession().merge(transfer);
	}

	/**
	 * method to list tranfers between two dates
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TransferVo> listTransferBetweenDates(String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		List<TransferVo> vos = new ArrayList<TransferVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transfer.class);
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.ge("transferDate", startdate)).add(Restrictions.le("transferDate", enddate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Transfer> transfers = criteria.list();
		for (Transfer transfer : transfers) {
			vos.add(createTransferVo(transfer));
		}
		return vos;
	}

	/**
	 * method to list transfers in a branch between two dates
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<TransferVo> listTransferBetweenDatesAndBranch(String startDate, String endDate, Long branchId) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Branch branch = branchDao.findAndReturnBranchObjectById(branchId);
		if (branch == null)
			throw new ItemNotFoundException("Branch Not Exist.");
		List<TransferVo> vos = new ArrayList<TransferVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transfer.class);
		criteria.add(Restrictions.ge("transferDate", startdate)).add(Restrictions.le("transferDate", enddate));
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.eq("transferFromBranch", branch));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Transfer> transfers = criteria.list();
		for (Transfer transfer : transfers) {
			vos.add(createTransferVo(transfer));
		}
		return vos;
	}

	/**
	 * method to list transfers in a department between dates
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TransferVo> listTransferBetweenDatesAndDepartment(String startDate, String endDate, Long departmentId) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Department department = departmentDao.findDepartment(departmentId);
		if (department == null)
			throw new ItemNotFoundException("Department Not Exist.");
		List<TransferVo> vos = new ArrayList<TransferVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transfer.class);
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.ge("transferDate", startdate)).add(Restrictions.le("transferDate", enddate));
		criteria.add(Restrictions.eq("transferFromDepartment", department));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Transfer> transfers = criteria.list();
		for (Transfer transfer : transfers) {
			vos.add(createTransferVo(transfer));
		}
		return vos;
	}

	/**
	 * method to list transfers in a branch & department between dates
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<TransferVo> listTransferByBranchDepartmetnAndBetweenDates(Long branchId, Long departmentId,
			String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Branch branch = branchDao.findAndReturnBranchObjectById(branchId);
		if (branch == null)
			throw new ItemNotFoundException("Branch Not Exist.");
		Department department = departmentDao.findDepartment(departmentId);
		if (department == null)
			throw new ItemNotFoundException("Department Not Exist.");
		List<TransferVo> vos = new ArrayList<TransferVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transfer.class);
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.ge("transferDate", startdate)).add(Restrictions.le("transferDate", enddate));
		criteria.add(Restrictions.eq("transferFromDepartment", department));
		criteria.add(Restrictions.eq("transferFromBranch", branch));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Transfer> transfers = criteria.list();
		for (Transfer transfer : transfers) {
			vos.add(createTransferVo(transfer));
		}
		return vos;
	}

	/**
	 * method to create transfer vo
	 * 
	 * @param transfer
	 * @return
	 */
	private TransferVo createTransferVo(Transfer transfer) {
		TransferVo vo = new TransferVo();
		vo.setTransferId(transfer.getId());
		vo.setEmployeeCode(transfer.getEmployee().getEmployeeCode());
		vo.setFromBranch(transfer.getTransferFromBranch().getBranchName());
		vo.setFromDepartment(transfer.getTransferFromDepartment().getName());
		vo.setEmployeeId(transfer.getEmployee().getId());
		if (transfer.getEmployee().getUserProfile() != null)
			vo.setEmployee(transfer.getEmployee().getUserProfile().getFirstName() + " "
					+ transfer.getEmployee().getUserProfile().getLastname());
		if (transfer.getForwardApplicationTo() != null) {
			Employee superior = transfer.getForwardApplicationTo();
			vo.setSuperiorId(superior.getId());
			vo.setSuperiorCode(superior.getEmployeeCode());
			vo.setSuperiorCodeAjax(superior.getEmployeeCode());
			if (superior.getUserProfile() != null)
				vo.setSuperior(superior.getUserProfile().getFirstName() + " " + superior.getUserProfile().getLastname());
		}
		if (transfer.getTransferDate() != null)
			vo.setDate(DateFormatter.convertDateToString(transfer.getTransferDate()));

		if (transfer.getTransferToBranch() != null) {
			vo.setBranch(transfer.getTransferToBranch().getBranchName());
			vo.setBranchId(transfer.getTransferToBranch().getId());
		}
		if (transfer.getTransferToDepartment() != null) {
			vo.setDepartment(transfer.getTransferToDepartment().getName());
			vo.setDepartmentId(transfer.getTransferToDepartment().getId());
		}

		vo.setDescription(transfer.getDescription());
		vo.setNotes(transfer.getNotes());

		if (transfer.getStatus() != null) {
			vo.setStatus(transfer.getStatus().getName());
			vo.setStatusId(transfer.getStatus().getId());
		}

		vo.setStatusDescription(transfer.getStatusDescription());
		vo.setCreatedBy(transfer.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(transfer.getCreatedOn()));

		if (transfer.getDocuments() != null)
			vo.setDocumentsVos(createDocumentVo(transfer.getDocuments()));
		return vo;
	}

	/**
	 * method to set documents
	 * 
	 * @param files
	 * @param transfer
	 * @return
	 */
	private Set<TransferDocuments> setDocuments(List<TransferDocumentsVo> files, Transfer transfer) {
		Set<TransferDocuments> documents = new HashSet<TransferDocuments>();
		for (TransferDocumentsVo file : files) {
			TransferDocuments document = new TransferDocuments();
			document.setTransfer(transfer);
			document.setUrl(file.getUrl());
			document.setName(file.getFileName());
			documents.add(document);
		}
		return documents;
	}

	/**
	 * method to get transfer by id
	 */
	public TransferVo getTransferById(Long transferId) {
		Transfer transfer = getTransfer(transferId);
		if (transfer == null)
			throw new ItemNotFoundException("Transfer Not Exist.");
		return createTransferVo(transfer);
	}

	/**
	 * method to create transfer document vos
	 * 
	 * @param documents
	 * @return
	 */
	private List<TransferDocumentsVo> createDocumentVo(Set<TransferDocuments> documents) {
		List<TransferDocumentsVo> vos = new ArrayList<TransferDocumentsVo>();
		for (TransferDocuments document : documents) {
			TransferDocumentsVo vo = new TransferDocumentsVo();
			vo.setUrl(document.getUrl());
			vo.setFileName(document.getName());
			vos.add(vo);
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TransferVo> findAllTransferByEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist");
		List<TransferVo> vos = new ArrayList<TransferVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transfer.class);
		criteria.createAlias("forwardApplicationTo", "employee");
		Criterion sender = Restrictions.eq("employee", employee);
		Criterion receiver = Restrictions.eq("employee.employeeCode", employeeCode);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Transfer> transfers = criteria.list();
		for (Transfer transfer : transfers) {
			vos.add(createTransferVo(transfer));
		}
		return vos;
	}
}
