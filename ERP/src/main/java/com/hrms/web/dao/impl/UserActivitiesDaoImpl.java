package com.hrms.web.dao.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.UserActivitiesDao;
import com.hrms.web.entities.Project;
import com.hrms.web.entities.UserActivities;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.IPAddress;
import com.hrms.web.vo.UserActivityVo;
/**
 * 
 * @author sangeeth
 * 13/3/2015
 *
 */
@Repository
public class UserActivitiesDaoImpl extends AbstractDao implements UserActivitiesDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/**
	 * Log
	 */
	@Log
	private static Logger logger;
	
	/**
	 * mathod to save UserActivity
	 * @param activity
	 */
	public void saveUserActivity(UserActivityVo userActivityVo) {
		logger.info("inside>> UserActivitiesDaoImpl... saveSystemLog");
		
		UserActivities userActivities=new UserActivities();
		userActivities.setActivity(userActivityVo.getActivity());
		userActivities.setPublicIP(userActivityVo.getPublicIP());
		userActivities.setPrivateIP(userActivityVo.getPrivateIP());
		userActivities.setCreatedOn(new java.sql.Timestamp(new java.util.Date().getTime()));
		
		this.sessionFactory.getCurrentSession().saveOrUpdate(userActivities);
	}
	
	

	public static void main(String args[]) throws Exception
	   {
		try {

			/*ClassPathXmlApplicationContext ctx = 
			        new ClassPathXmlApplicationContext("D:\\projectworkspace\\hrms\\src\\main\\webapp\\WEB-INF\\applicationContext.xml");*/
			//ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
			Date dNow = new Date( );
		      SimpleDateFormat ft = new SimpleDateFormat (CommonConstants.USER_ACTIVITY_LIST_FORMAT);
		      System.out.println("Current Date: " + ft.format(dNow));
			
			} catch (Exception e) {
			e.printStackTrace();
			}
			
	    	
	   }



	public List<Object[]> listDistinctMonthAndYear() {
		logger.info("inside>> UserActivitiesDaoImpl... listDistinctDatesByMonth");
		String hql = "select distinct month(u.createdOn), year(u.createdOn) from UserActivities u";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		List<Object[]> results = query.list();
		return results;
	}
	
	public List<UserActivityVo> listAllUserActivities(int month,int year) {
		logger.info("inside>> UserActivitiesDaoImpl... listAllUserActivities");
		List<UserActivityVo> userActivitiesList=new ArrayList<UserActivityVo>(0);
		//List<UserActivities> userActivities=this.sessionFactory.getCurrentSession().createQuery("from UserActivities u where month(u.createdOn)=:month and year(u.createdOn)=:year").set.list(); 
		
		
		Query query = this.sessionFactory.getCurrentSession().createQuery("from UserActivities u where month(u.createdOn)=:month and year(u.createdOn)=:year");
		query.setInteger("month", month);
		query.setInteger("year", year);
		List<UserActivities> userActivities = query.list();
		
		for(UserActivities activities:userActivities){
			userActivitiesList.add(createUserActivitiesVo(activities));
		}
		return userActivitiesList;
	}

	private UserActivityVo createUserActivitiesVo(UserActivities userActivities){
		logger.info("inside>> UserActivitiesDaoImpl... createUserActivitiesVo");
		UserActivityVo userActivityVo=new UserActivityVo();
		userActivityVo.setActivity(userActivities.getActivity());
		userActivityVo.setCreatedOn(userActivities.getCreatedOn());
		userActivityVo.setCreatedBy(userActivities.getCreatedBy().getUserName());
		userActivityVo.setId(userActivities.getId());
		userActivityVo.setPrivateIP(userActivities.getPrivateIP());
		userActivityVo.setPublicIP(userActivities.getPublicIP());
		Date dNow = new Date(userActivityVo.getCreatedOn().getTime());
	    SimpleDateFormat ft = new SimpleDateFormat (CommonConstants.USER_ACTIVITY_LIST_FORMAT);
		userActivityVo.setDateAndTime(ft.format(dNow)+"");
		return userActivityVo;
	}
}
