package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.HolidayDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Holiday;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.HolidayVo;

/**
 * 
 * @author Jithin Mohan
 * @since 29-March-2015
 *
 */
@Repository
public class HolidayDaoImpl extends AbstractDao implements HolidayDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private BranchDao branchDao;

	public void saveHoliday(HolidayVo holidayVo) {
		Holiday holiday = findHoliday(holidayVo.getHolidayId());
		if (holiday == null)
			holiday = new Holiday();

		holiday.setBranch(branchDao.findAndReturnBranchObjectByName(holidayVo.getBranch()));
		holiday.setTitle(holidayVo.getTitle());
		holiday.setStartDate(DateFormatter.convertStringToDate(holidayVo.getStartDate()));
		holiday.setEndDate(DateFormatter.convertStringToDate(holidayVo.getEndDate()));
		holiday.setDescription(holidayVo.getDescription());
		holiday.setNote(holidayVo.getNote());
		this.sessionFactory.getCurrentSession().saveOrUpdate(holiday);
		holidayVo.setHolidayId(holiday.getId());
		holidayVo.setRecordedBy(holiday.getCreatedBy().getUserName());
		holidayVo.setRecordedOn(DateFormatter.convertDateToDetailedString(holiday.getCreatedOn()));
	}

	public void updateHoliday(HolidayVo holidayVo) {
		// TODO Auto-generated method stub

	}

	public void deleteHoliday(Long holidayId) {
		Holiday holiday = findHoliday(holidayId);
		if (holiday == null)
			throw new ItemNotFoundException("Holiday Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(holiday);
	}

	public Holiday findHoliday(Long holidayId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Holiday.class)
				.add(Restrictions.eq("id", holidayId));
		if (criteria == null)
			return null;
		return (Holiday) criteria.uniqueResult();
	}

	/**
	 * method to create HolidayVo object
	 * 
	 * @param holiday
	 * @return holidayVo
	 */
	private HolidayVo createHolidayVo(Holiday holiday) {
		HolidayVo holidayVo = new HolidayVo();
		holidayVo.setBranch(holiday.getBranch().getBranchName());
		holidayVo.setHolidayId(holiday.getId());
		holidayVo.setTitle(holiday.getTitle());
		holidayVo.setStartDate(DateFormatter.convertDateToString(holiday.getStartDate()));
		holidayVo.setEndDate(DateFormatter.convertDateToString(holiday.getEndDate()));
		holidayVo.setDescription(holiday.getDescription());
		holidayVo.setNote(holiday.getNote());
		holidayVo.setRecordedBy(holiday.getCreatedBy().getUserName());
		holidayVo.setRecordedOn(DateFormatter.convertDateToDetailedString(holiday.getCreatedOn()));
		return holidayVo;
	}

	public HolidayVo findHolidayById(Long holidayId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Holiday.class)
				.add(Restrictions.eq("id", holidayId));
		if (criteria == null)
			return null;
		return createHolidayVo((Holiday) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<HolidayVo> findHolidayByBranch(Branch branch) {
		List<HolidayVo> holidayVos = new ArrayList<HolidayVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Holiday.class)
				.add(Restrictions.eq("branch", branch));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Holiday> holidays = criteria.list();
		for (Holiday holiday : holidays)
			holidayVos.add(createHolidayVo(holiday));
		return holidayVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<HolidayVo> findHolidayByStartDate(Date startDate) {
		List<HolidayVo> holidayVos = new ArrayList<HolidayVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Holiday.class)
				.add(Restrictions.eq("startDate", startDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Holiday> holidays = criteria.list();
		for (Holiday holiday : holidays)
			holidayVos.add(createHolidayVo(holiday));
		return holidayVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<HolidayVo> findHolidayByEndDate(Date endDate) {
		List<HolidayVo> holidayVos = new ArrayList<HolidayVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Holiday.class)
				.add(Restrictions.eq("endDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Holiday> holidays = criteria.list();
		for (Holiday holiday : holidays)
			holidayVos.add(createHolidayVo(holiday));
		return holidayVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<HolidayVo> findHolidayBetweenDate(Date startDate, Date endDate) {
		List<HolidayVo> holidayVos = new ArrayList<HolidayVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Holiday.class)
				.add(Restrictions.ge("startDate", startDate)).add(Restrictions.le("startDate", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Holiday> holidays = criteria.list();
		for (Holiday holiday : holidays)
			holidayVos.add(createHolidayVo(holiday));
		return holidayVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<HolidayVo> findAllHoliday() {
		List<HolidayVo> holidayVos = new ArrayList<HolidayVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Holiday.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Holiday> holidays = criteria.list();
		for (Holiday holiday : holidays)
			holidayVos.add(createHolidayVo(holiday));
		return holidayVos;
	}

}
