package com.hrms.web.dao.impl;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AttendanceDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.entities.Attendance;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AttendanceVo;
import com.hrms.web.vo.BiometricAttendanceVo;
import com.hrms.web.vo.RestBiometricAttendanceVo;

/**
 * 
 * @author shamsheer
 * @since 8-April-2015
 */
@Repository
public class AttendanceDaoImpl extends AbstractDao implements AttendanceDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private EmployeeDao employeeDao;

	/**
	 * method to save attendance
	 */
	public Long saveAttendance(AttendanceVo vo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		Attendance attendance = (Attendance) this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("date", DateFormatter.convertStringToDate(vo.getDate()))).uniqueResult();
		if (attendance == null)
			attendance = new Attendance();
		attendance.setEmployee(employee);
		if (vo.getDate() != "")
			attendance.setDate(DateFormatter.convertStringToDate(vo.getDate()));
		if (vo.getSignInTime() != "")
			attendance.setSignIn(DateFormatter.convertStringTimeToSqlTime(vo.getSignInTime()));
		if (vo.getSignOutTime() != "")
			attendance.setSignOut(DateFormatter.convertStringTimeToSqlTime(vo.getSignOutTime()));
		attendance.setNotes(vo.getNotes());
		attendance.setStatus(vo.getStatus());
		userActivitiesService.saveUserActivity("Added new Attendance of " + employee.getUserProfile().getFirstName()
				+ " " + employee.getUserProfile().getLastname());
		return (Long) this.sessionFactory.getCurrentSession().save(attendance);
	}

	/**
	 * method to update attendance
	 */
	public void updateAttendance(AttendanceVo vo) {
		Attendance attendance = getAttendanceById(vo.getAttendanceId());
		Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		attendance.setEmployee(employee);

		if (vo.getDate() != "")
			attendance.setDate(DateFormatter.convertStringToDate(vo.getDate()));
		if (vo.getSignInTime() != "")
			attendance.setSignIn(DateFormatter.convertStringTimeToSqlTime(vo.getSignInTime()));
		if (vo.getSignOutTime() != "")
			attendance.setSignOut(DateFormatter.convertStringTimeToSqlTime(vo.getSignOutTime()));
		attendance.setNotes(vo.getNotes());
		this.sessionFactory.getCurrentSession().update(attendance);
		userActivitiesService.saveUserActivity("Updated Attendance " + vo.getEmployeeCode());
	}

	/**
	 * method to get attendance by id
	 */
	public Attendance getAttendanceById(Long attendanceId) {
		return (Attendance) this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.add(Restrictions.eq("id", attendanceId)).uniqueResult();
	}

	/**
	 * method to delete attendance
	 */
	public void deleteAttendance(Long attendanceId) {
		this.sessionFactory.getCurrentSession().delete(getAttendanceById(attendanceId));
		userActivitiesService.saveUserActivity("Deleted Attendance");
	}

	/**
	 * method to list all attendance
	 */
	@SuppressWarnings("unchecked")
	public List<AttendanceVo> listAllAttendance() {
		List<AttendanceVo> vos = new ArrayList<AttendanceVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		// Query criteria
		// =this.sessionFactory.getCurrentSession().createQuery(" FROM Attendance a  WHERE a.date LIKE '2015-04%' GROUP BY a.date");
		List<Attendance> attendances = criteria.list();
		for (Attendance attendance : attendances) {
			vos.add(createAttendanceVo(attendance));
		}
		return vos;
	}

	/**
	 * method to create attendance vo
	 * 
	 * @param attendance
	 * @return
	 */
	private AttendanceVo createAttendanceVo(Attendance attendance) {
		AttendanceVo vo = new AttendanceVo();
		vo.setEmployeeCode(attendance.getEmployee().getEmployeeCode());
		vo.setEmployeeId(attendance.getEmployee().getId());
		if (attendance.getEmployee().getUserProfile() != null)
			vo.setEmployee(attendance.getEmployee().getUserProfile().getFirstName() + " "
					+ attendance.getEmployee().getUserProfile().getLastname());
		vo.setAttendanceId(attendance.getId());
		if (attendance.getDate() != null)
			vo.setDate(DateFormatter.convertDateToString(attendance.getDate()));
		if (attendance.getSignIn() != null)
			vo.setSignInTime(DateFormatter.convertSqlTimeToString(attendance.getSignIn()));
		if (attendance.getSignOut() != null)
			vo.setSignOutTime(DateFormatter.convertSqlTimeToString(attendance.getSignOut()));
		vo.setNotes(attendance.getNotes());
		vo.setCreatedBy(attendance.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(attendance.getCreatedOn()));
		return vo;
	}

	/**
	 * method to list attendance by employee
	 */
	@SuppressWarnings("unchecked")
	public List<AttendanceVo> listAttendanceByEmployee(Long employeeId) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<AttendanceVo> vos = new ArrayList<AttendanceVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		Criterion byEmployee = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression logicalExpression = Restrictions.or(byEmployee, createdBy);
		criteria.add(logicalExpression);
		List<Attendance> attendances = criteria.list();
		for (Attendance attendance : attendances) {
			vos.add(createAttendanceVo(attendance));
		}
		return vos;
	}

	/**
	 * method to get attendance by id
	 */
	public AttendanceVo getAttendanceByAttendanceId(Long attendanceId) {
		return createAttendanceVo(getAttendanceById(attendanceId));
	}

	/**
	 * method to list attendance by employee and date
	 */
	@SuppressWarnings("unchecked")
	public List<AttendanceVo> listAttendanceByEmployeeAndDate(Long employeeId, String date) {
		Date attendanceDate = DateFormatter.convertStringToDate(date);
		List<AttendanceVo> vos = new ArrayList<AttendanceVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("employee.id", employeeId));
		criteria.add(Restrictions.eq("date", attendanceDate));
		List<Attendance> attendances = criteria.list();
		for (Attendance attendance : attendances) {
			vos.add(createAttendanceVo(attendance));
		}
		return vos;
	}

	/**
	 * method to list attendance objects by employee and date
	 */
	@SuppressWarnings("unchecked")
	public List<Attendance> listAttendanceObjectByEmployeeAndDate(Long employeeId, String date) {
		Date attendanceDate = DateFormatter.convertStringToDate(date);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("employee.id", employeeId));
		criteria.add(Restrictions.eq("date", attendanceDate));
		List<Attendance> attendances = criteria.list();
		return attendances;
	}

	/**
	 * get total days attended by employee
	 */
	@SuppressWarnings("unchecked")
	public List<AttendanceVo> getTotalDaysAttendedByEmployee(Long employeeId, String date) {
		List<AttendanceVo> vos = new ArrayList<AttendanceVo>();
		Query criteria = this.sessionFactory.getCurrentSession().createQuery(
				" FROM Attendance a  WHERE a.employee = " + employeeId + "" + "AND  a.date LIKE '" + date
						+ "%' GROUP BY a.date");
		List<Attendance> attendances = criteria.list();
		for (Attendance attendance : attendances) {
			vos.add(createAttendanceVo(attendance));
		}
		return vos;
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Integer listAttendanceObjectByEmployeeAndDate(Long employeeId, String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("employee.id", employeeId));
		criteria.add(Restrictions.ge("date", startdate)).add(Restrictions.le("date", enddate));
		criteria.setProjection(Projections.groupProperty("date"));
		List<Attendance> attendances = criteria.list();
		if (attendances != null)
			return attendances.size();
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	public List<AttendanceVo> listAttendanceByEmployee(String employeeCode, String date) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		List<AttendanceVo> vos = new ArrayList<AttendanceVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.eq("date", DateFormatter.convertStringToDate(date)));
		List<Attendance> attendances = criteria.list();
		for (Attendance attendance : attendances) {
			vos.add(createAttendanceVo(attendance));
		}
		return vos;
	}

	@SuppressWarnings("unchecked")
	public List<Attendance> listAttendanceObjectByEmployeeAndDate(String employeeCode, String date) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Date attendanceDate = DateFormatter.convertStringToDate(date);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.eq("date", attendanceDate));
		List<Attendance> attendances = criteria.list();
		return attendances;
	}

	/**
	 * method to save attendance from biometric device
	 */
	public void saveAttendanceFromBiometric(Set<BiometricAttendanceVo> attendanceVos) {
		for (BiometricAttendanceVo vo : attendanceVos) {
			Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
			Attendance attendance = new Attendance();
			attendance.setEmployee(employee);
			attendance.setDate(DateFormatter.convertStringMmDdYyyyToDate(vo.getDate()));
			for (int i = 0; i < vo.getTime().size(); i++) {
				if (attendance.getSignIn() != null && attendance.getSignOut() != null) {
					if (checkExisting(attendance.getEmployee(), attendance.getSignIn(), attendance.getSignOut(),
							attendance.getDate()))
						this.sessionFactory.getCurrentSession().save(attendance);
					attendance = new Attendance();
					attendance.setEmployee(employee);
					attendance.setDate(DateFormatter.convertStringMmDdYyyyToDate(vo.getDate()));
				}
				if (i % 2 == 0) {
					attendance.setSignIn(DateFormatter.convertString24HrTimeToSqlTime(vo.getTime().get(i)));
				} else {
					attendance.setSignOut(DateFormatter.convertString24HrTimeToSqlTime(vo.getTime().get(i)));
				}
			}
			if (checkExisting(attendance.getEmployee(), attendance.getSignIn(), attendance.getSignOut(),
					attendance.getDate()))
				this.sessionFactory.getCurrentSession().save(attendance);
		}
	}

	/**
	 * method to save attendance from rest service controller
	 * @param attendanceMap
	 */
	public void saveAttendanceFromRestService(Map<String, RestBiometricAttendanceVo> attendanceMap){
		for(Map.Entry<String, RestBiometricAttendanceVo> entry : attendanceMap.entrySet()){
			Employee employee = employeeDao.findAndReturnEmployeeByCode(entry.getKey());
			Attendance attendance = new Attendance();
			attendance.setEmployee(employee);
			RestBiometricAttendanceVo vo = entry.getValue();
			String[] dateStrings =  vo.getTimes().get(0).split(" ");
			Date date = DateFormatter.convertStringFromRestToDate(dateStrings[0]);
			attendance.setDate(date);
			for (int i = 0; i < vo.getTimes().size(); i++) {
				if (attendance.getSignIn() != null && attendance.getSignOut() != null) {
					if (checkExisting(attendance.getEmployee(), attendance.getSignIn(), attendance.getSignOut(),
							attendance.getDate()))
						this.sessionFactory.getCurrentSession().save(attendance);
					attendance = new Attendance();
					attendance.setEmployee(employee);
					attendance.setDate(date);
				}
				if (i % 2 == 0) {
					attendance.setSignIn(DateFormatter.convertStringQualifiedTimeToSqlTime(vo.getTimes().get(i)));
				} else {
					attendance.setSignOut(DateFormatter.convertStringQualifiedTimeToSqlTime(vo.getTimes().get(i)));
				}
			}
			if (checkExisting(attendance.getEmployee(), attendance.getSignIn(), attendance.getSignOut(),
					attendance.getDate()))
				this.sessionFactory.getCurrentSession().save(attendance);
		
		}
	}
	
	/**
	 * method to find last saved attendance of an employee
	 * 
	 * @param employee
	 * @return
	 */
	@SuppressWarnings("unused")
	private Attendance findLastAttendanceOfEmployee(Employee employee) {
		Attendance attendance = null;
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Attendance.class);
		DetachedCriteria maxId = DetachedCriteria.forClass(Attendance.class);
		maxId.add(Restrictions.eq("employee", employee));
		maxId.setProjection(Projections.max("id"));
		criteria.add((Property.forName("id").eq(maxId)));
		attendance = (Attendance) criteria.uniqueResult();
		return attendance;
	}

	/**
	 * method to check attendance is existing or not..
	 * 
	 * @param employee
	 * @param signIn
	 * @param signOut
	 * @param date
	 * @return
	 */
	private Boolean checkExisting(Employee employee, Time signIn, Time signOut, Date date) {
		Attendance attendance = (Attendance) this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.add(Restrictions.eq("employee", employee))
				.add(signIn == null ? Restrictions.isNull("signIn") : Restrictions.eq("signIn", signIn))
				.add(signOut == null ? Restrictions.isNull("signOut") : Restrictions.eq("signOut", signOut))
				.add(Restrictions.eq("date", date)).uniqueResult();
		if (attendance != null)
			return false;
		else
			return true;
	}
	
	@SuppressWarnings("unchecked")
	private List<Attendance> getAbsentAttendances(Employee employee, Date startDate, Date endDate, String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Attendance.class)
				.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.le("date", endDate)).add(Restrictions.ge("date", startDate));
		criteria.add(Restrictions.eq("status", status));
		List<Attendance> attendances = criteria.list();
		return attendances;
	}

	public Double getLopCount(Employee employee, Date startDate, Date endDate) {
		Double absent = (double) getAbsentAttendances(employee, startDate, endDate, CommonConstants.ABSENT).size();
		Double halfDay = (double) getAbsentAttendances(employee, startDate, endDate, CommonConstants.HALF_DAY).size();
		if (halfDay != 0)
			halfDay = halfDay / 2;
		absent = absent + halfDay;
		return absent;
	}
}
