package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.HourlyWagesDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.HourlyWages;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.HourlyWagesVo;

/**
 * 
 * @author Shamsheer
 * @since 10-April-2015
 */
@Repository
public class HourlyWagesDaoImpl extends AbstractDao implements HourlyWagesDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	/**
	 * method to save hourly wages
	 */
	public Long saveHourlyWages(HourlyWagesVo wagesVo) {
		HourlyWages hourlyWages = new HourlyWages();

		Employee employee = employeeDao.findAndReturnEmployeeByCode(wagesVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		hourlyWages.setEmployee(employee);
		if (!wagesVo.getRegularHoursAmount().isEmpty())
			hourlyWages.setRegularHourAmount(new BigDecimal(wagesVo.getRegularHoursAmount()));
		if (!wagesVo.getOverTimeHoursAmount().isEmpty())
			hourlyWages.setOverTimeHourAmount(new BigDecimal(wagesVo.getOverTimeHoursAmount()));
		hourlyWages.setNotes(wagesVo.getNotes());
		hourlyWages.setDescription(wagesVo.getDescription());

		return (Long) this.sessionFactory.getCurrentSession().save(hourlyWages);
	}

	/**
	 * method to update hourly wages
	 */
	public void updateHourlyWage(HourlyWagesVo wagesVo) {
		HourlyWages hourlyWages = getHourlyWagesObjectById(wagesVo.getHourlyWagesId());

		Employee employee = employeeDao.findAndReturnEmployeeByCode(wagesVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		hourlyWages.setEmployee(employee);

		hourlyWages.setRegularHourAmount(new BigDecimal(wagesVo.getRegularHoursAmount()));
		hourlyWages.setOverTimeHourAmount(new BigDecimal(wagesVo.getOverTimeHoursAmount()));
		hourlyWages.setNotes(wagesVo.getNotes());
		hourlyWages.setDescription(wagesVo.getDescription());

		this.sessionFactory.getCurrentSession().merge(hourlyWages);
	}

	/**
	 * method to delete hourly wages
	 */
	public void deleteHourlyWage(Long hourlyWageId) {
		this.sessionFactory.getCurrentSession().delete(getHourlyWagesObjectById(hourlyWageId));
	}

	/**
	 * method to list all hourlyWages
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<HourlyWagesVo> listAllHourlyWages() {
		List<HourlyWagesVo> vos = new ArrayList<HourlyWagesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(HourlyWages.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<HourlyWages> wages = criteria.list();
		for (HourlyWages wage : wages) {
			vos.add(createHourlyWagesVo(wage));
		}
		return vos;
	}

	/**
	 * method to get hourly wages object by id
	 */
	public HourlyWages getHourlyWagesObjectById(Long hourlyWagesId) {
		return (HourlyWages) this.sessionFactory.getCurrentSession().createCriteria(HourlyWages.class)
				.add(Restrictions.eq("id", hourlyWagesId)).uniqueResult();
	}

	/**
	 * method to get hourly wages object by employee id
	 */
	public HourlyWages getHourlyWagesObjectByEmployee(Employee employee) {
		return (HourlyWages) this.sessionFactory.getCurrentSession().createCriteria(HourlyWages.class)
				.add(Restrictions.eq("employee", employee)).uniqueResult();
	}

	/**
	 * method to get hourly wages by id
	 */
	public HourlyWagesVo getHourlyWagesById(Long hourlyWagesId) {
		return createHourlyWagesVo(getHourlyWagesObjectById(hourlyWagesId));
	}

	/**
	 * method to get hourly wages by employee id
	 */
	public HourlyWagesVo getHourlyWagesByEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		HourlyWages hourlyWages = getHourlyWagesObjectByEmployee(employee);
		if (hourlyWages != null)
			return createHourlyWagesVo(hourlyWages);
		else
			return null;
	}

	/**
	 * method to create hourly wages
	 * 
	 * @param wages
	 * @return
	 */
	private HourlyWagesVo createHourlyWagesVo(HourlyWages wages) {
		HourlyWagesVo vo = new HourlyWagesVo();
		vo.setDescription(wages.getDescription());
		vo.setNotes(wages.getNotes());
		vo.setEmployeeId(wages.getEmployee().getId());
		if (wages.getEmployee().getUserProfile() != null)
			vo.setEmployee(wages.getEmployee().getUserProfile().getFirstName() + " "
					+ wages.getEmployee().getUserProfile().getLastname());
		vo.setHourlyWagesId(wages.getId());
		vo.setEmployeeCode(wages.getEmployee().getEmployeeCode());
		if (wages.getRegularHourAmount() != null)
			vo.setRegularHoursAmount(wages.getRegularHourAmount().toString());
		if (wages.getOverTimeHourAmount() != null)
			vo.setOverTimeHoursAmount(wages.getOverTimeHourAmount().toString());
		vo.setCreatedBy(wages.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(wages.getCreatedOn()));
		return vo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<HourlyWagesVo> findAllHourlyWagesByEmployee(String employeeCode) {
		List<HourlyWagesVo> vos = new ArrayList<HourlyWagesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(HourlyWages.class)
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeByCode(employeeCode)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<HourlyWages> wages = criteria.list();
		for (HourlyWages wage : wages) {
			vos.add(createHourlyWagesVo(wage));
		}
		return vos;
	}
}
