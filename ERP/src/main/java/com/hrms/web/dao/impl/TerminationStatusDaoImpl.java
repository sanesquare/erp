package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.TerminationStatusDao;
import com.hrms.web.entities.TerminationStatus;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.TerminationStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
@Repository
public class TerminationStatusDaoImpl extends AbstractDao implements TerminationStatusDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveTerminationStatus(TerminationStatusVo terminationStatusVo) {
		for (String status : StringSplitter.splitWithComma(terminationStatusVo.getStatus())) {
			if (findTerminationStatus(status.trim()) != null)
				throw new DuplicateItemException("Duplicate Termination Status : " + status.trim());
			TerminationStatus resignationStatus = new TerminationStatus();
			resignationStatus.setStatus(status.trim());
			this.sessionFactory.getCurrentSession().save(resignationStatus);
		}
	}

	public void updateTerminationStatus(TerminationStatusVo terminationStatusVo) {
		TerminationStatus terminationStatus = findTerminationStatus(terminationStatusVo.getTerminationStatusId());
		if (terminationStatus == null)
			throw new ItemNotFoundException("Termination Status Not Found.");
		terminationStatus.setStatus(terminationStatusVo.getStatus());
		this.sessionFactory.getCurrentSession().merge(terminationStatus);
	}

	public void deleteTerminationStatus(Long terminationStatusId) {
		TerminationStatus terminationStatus = findTerminationStatus(terminationStatusId);
		if (terminationStatus == null)
			throw new ItemNotFoundException("Termination Status Not Found.");
		this.sessionFactory.getCurrentSession().delete(terminationStatus);
	}

	public TerminationStatus findTerminationStatus(Long terminationStatusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TerminationStatus.class)
				.add(Restrictions.eq("id", terminationStatusId));
		if (criteria.uniqueResult() == null)
			return null;
		return (TerminationStatus) criteria.uniqueResult();
	}

	public TerminationStatus findTerminationStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TerminationStatus.class)
				.add(Restrictions.eq("status", status));
		if (criteria.uniqueResult() == null)
			return null;
		return (TerminationStatus) criteria.uniqueResult();
	}

	/**
	 * method to create TerminationStatusVo object
	 * 
	 * @param terminationStatus
	 * @return terminationStatusVo
	 */
	private TerminationStatusVo createTerminationStatusVo(TerminationStatus terminationStatus) {
		TerminationStatusVo terminationStatusVo = new TerminationStatusVo();
		terminationStatusVo.setTerminationStatusId(terminationStatus.getId());
		terminationStatusVo.setStatus(terminationStatus.getStatus());
		return terminationStatusVo;
	}

	public TerminationStatusVo findTerminationStatusById(Long terminationStatusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TerminationStatus.class)
				.add(Restrictions.eq("id", terminationStatusId));
		if (criteria.uniqueResult() == null)
			return null;
		return createTerminationStatusVo((TerminationStatus) criteria.uniqueResult());
	}

	public TerminationStatusVo findTerminationStatusByStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TerminationStatus.class)
				.add(Restrictions.eq("status", status));
		if (criteria.uniqueResult() == null)
			return null;
		return createTerminationStatusVo((TerminationStatus) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<TerminationStatusVo> findAllTerminationStatus() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TerminationStatus.class);
		List<TerminationStatusVo> terminationStatusVos = new ArrayList<TerminationStatusVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY); 
		List<TerminationStatus> terminationStatus = criteria.list();
		for (TerminationStatus status : terminationStatus)
			terminationStatusVos.add(createTerminationStatusVo(status));
		return terminationStatusVos;
	}

}
