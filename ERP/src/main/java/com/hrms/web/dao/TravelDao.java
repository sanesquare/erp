package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.Travel;
import com.hrms.web.vo.TravelVo;

/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
public interface TravelDao {

	/**
	 * method to save travel
	 * 
	 * @param travelVo
	 * @return
	 */
	public Long saveTravel(TravelVo travelVo);

	/**
	 * method to update travel
	 * 
	 * @param travelVo
	 */
	public void updateTravel(TravelVo travelVo);

	/**
	 * method to delete travel
	 * 
	 * @param travelId
	 */
	public void deleteTravel(Long travelId);

	/**
	 * method to get travel object by id
	 * 
	 * @param travelId
	 * @return
	 */
	public Travel getTravel(Long travelId);

	/**
	 * method to get travel by id
	 * 
	 * @param travelId
	 * @return
	 */
	public TravelVo getTravelById(Long travelId);

	/**
	 * method to list all travels
	 * 
	 * @return
	 */
	public List<TravelVo> listAllTravels();

	/**
	 * method to list travel by employee
	 * 
	 * @param employeeId
	 * @return
	 */
	public List<TravelVo> listAllTravelsByEmployee(Long employeeId);

	/**
	 * method to update documents
	 * 
	 * @param travelVo
	 */
	public void updateDocuments(TravelVo travelVo);

	/**
	 * method to delete document
	 * 
	 * @param url
	 */
	public void deleteDocument(String url);

	/**
	 * method to update status
	 * 
	 * @param travelVo
	 */
	public void updateStatus(TravelVo travelVo);

	/**
	 * method to find travels by employee between given dates
	 * 
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<TravelVo> findTravelsByEmployeeBetweenDates(Long employeeId, String startDate, String endDate);

	/**
	 * method to find all travels by employeeCode
	 * 
	 * @param employeeCode
	 * @return
	 */
	public List<TravelVo> findAllTravelsByEmployee(String employeeCode);
}
