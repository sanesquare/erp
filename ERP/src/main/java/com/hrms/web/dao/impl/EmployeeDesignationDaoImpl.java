package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDesignationDao;
import com.hrms.web.entities.EmployeeDesignation;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.DesignationRankVo;
import com.hrms.web.vo.EmployeeVo;

/**
 * 
 * @author shamsheer
 * @since 18-March-2015
 */
@Repository
public class EmployeeDesignationDaoImpl extends AbstractDao implements EmployeeDesignationDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveDesignation(DesignationRankVo designationRankVo) {
		/*
		 * for (String designation :
		 * StringSplitter.splitWithComma(employeeVo.getDesignation())) { if
		 * (findDesignationByDesignation(designation.trim()) != null) throw new
		 * DuplicateItemException("Duplicate Designation : " +
		 * designation.trim()); EmployeeDesignation employeeDesignation = new
		 * EmployeeDesignation();
		 * employeeDesignation.setDesignation(designation.trim());
		 * this.sessionFactory.getCurrentSession().save(employeeDesignation); }
		 */
		if (designationRankVo.getRank() > 0 && designationRankVo.getDesignation() != "") {
			EmployeeDesignation designation =null;
			designation = findDesignationByDesignation(designationRankVo.getTempDes());
			if(designation==null)
				designation = new EmployeeDesignation();
			designation.setRank(designationRankVo.getRank());
			designation.setDesignation(designationRankVo.getDesignation());
			this.sessionFactory.getCurrentSession().saveOrUpdate(designation);
			userActivitiesService.saveUserActivity("Added new Designation " + designationRankVo.getDesignation());
		} else
			throw new DuplicateItemException("Cannot insert null value.");
	}

	public void updateDesignation(EmployeeVo employeeVo) {
		EmployeeDesignation designation = findDesignationById(employeeVo.getDesignationId());
		if (designation == null)
			throw new ItemNotFoundException("Employee Designation Details Not Found.");
		designation.setDesignation(employeeVo.getDesignation());
		this.sessionFactory.getCurrentSession().merge(designation);
		userActivitiesService.saveUserActivity("Updated Designation as " + employeeVo.getDesignation());
	}

	public void deleteDesignation(Long id) {
		EmployeeDesignation designation = findDesignationById(id);
		if (designation == null)
			throw new ItemNotFoundException("Employee Designation Details Not Found.");
		String name = designation.getDesignation();
		this.sessionFactory.getCurrentSession().delete(designation);
		userActivitiesService.saveUserActivity("Deleted Employee Designation " + name);
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeDesignation> listAllDesignations() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeDesignation.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	public EmployeeDesignation findDesignationById(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeDesignation.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeDesignation) criteria.uniqueResult();
	}

	public EmployeeDesignation findDesignationByDesignation(String designation) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeDesignation.class)
				.add(Restrictions.eq("designation", designation));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeDesignation) criteria.uniqueResult();
	}

	public List<DesignationRankVo> findAllDesignations() {
		List<DesignationRankVo> designationRankVos = new ArrayList<DesignationRankVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeDesignation.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeDesignation> designations = criteria.list();
		for (EmployeeDesignation designation : designations) {
			DesignationRankVo designationRankVo = new DesignationRankVo();
			designationRankVo.setDesignation(designation.getDesignation());
			designationRankVo.setRank(designation.getRank());
			designationRankVo.setDesignation_rank_id(designation.getId());
			designationRankVos.add(designationRankVo);
		}
		return designationRankVos;
	}

	public EmployeeDesignation findEmployeeDesignation(String designation, Long rank) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeDesignation.class)
				.add(Restrictions.eq("designation", designation)).add(Restrictions.eq("rank", rank));
		if (criteria.uniqueResult() == null)
			return null;
		return (EmployeeDesignation) criteria.uniqueResult();
	}

}
