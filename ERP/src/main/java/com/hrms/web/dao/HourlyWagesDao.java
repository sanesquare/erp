package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.HourlyWages;
import com.hrms.web.vo.HourlyWagesVo;

/**
 * 
 * @author Shamsheer
 * @since 10-April-2015
 */
public interface HourlyWagesDao {

	/**
	 * method to save new hourly wage
	 * 
	 * @param wagesVo
	 * @return
	 */
	public Long saveHourlyWages(HourlyWagesVo wagesVo);

	/**
	 * method to update hourly wage
	 * 
	 * @param wagesVo
	 */
	public void updateHourlyWage(HourlyWagesVo wagesVo);

	/**
	 * method to delete hourly wage
	 * 
	 * @param hourlyWageId
	 */
	public void deleteHourlyWage(Long hourlyWageId);

	/**
	 * methods to list all hourly wages
	 * 
	 * @return
	 */
	public List<HourlyWagesVo> listAllHourlyWages();

	/**
	 * method to get hourly wages object by id
	 * 
	 * @param hourlyWagesId
	 * @return
	 */
	public HourlyWages getHourlyWagesObjectById(Long hourlyWagesId);

	/**
	 * method to get hourly wages object by employee id
	 * 
	 * @param hourlyWagesId
	 * @return
	 */
	public HourlyWages getHourlyWagesObjectByEmployee(Employee employee);

	/**
	 * method to get hourly wages by id
	 * 
	 * @param hourlyWagesId
	 * @return
	 */
	public HourlyWagesVo getHourlyWagesById(Long hourlyWagesId);

	/**
	 * method to get hourly wages by employee id
	 * 
	 * @param hourlyWagesId
	 * @return
	 */
	public HourlyWagesVo getHourlyWagesByEmployee(String employeeCode);

	/**
	 * methods to find all hourly wages by employee
	 * 
	 * @return
	 */
	public List<HourlyWagesVo> findAllHourlyWagesByEmployee(String employeeCode);
}
