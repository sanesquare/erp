package com.hrms.web.dao;

import java.sql.Date;
import java.util.List;

import com.hrms.web.entities.Branch;
import com.hrms.web.entities.BranchType;
import com.hrms.web.vo.BranchVo;

/**
 * 
 * @author Vinutha
 * @since 10-03-2015
 */

public interface BranchDao {
	/**
	 * method to Save new branch
	 * @param branchVo
	 */
	public Long saveBranch(BranchVo branchVo);
	
	/**
	 * method to Delete branch
	 * @param branchVo
	 */
	public void deleteBranch(BranchVo branchVo);
	
	/**
	 * method to Update branch
	 * @param branchVo
	 */
	public void updateBranch(BranchVo branchVo);
	
	/**
	 * method to find branches by id
	 * @param ids
	 * @return
	 */
	public List<Branch> findBranchesById(List<Long> ids);
	
	/**
	 * method to findAllBranches
	 * @return
	 */
	public List<BranchVo> findAllBranches();
	/**
	 * Method to find all branches other than the one specified
	 * @param id
	 * @return
	 */
	public List<BranchVo> findRemainingBranches(Long id);
	
	/**
	 * method to find branch by id
	 * @param id
	 * @return BranchVo
	 */
	public BranchVo findBranchById(Long id);
	
	/**
	 * find branch by name
	 * @param branchName
	 * @return branchVo
	 */
	public BranchVo findBranchByName(String branchName);
	
	/**
	 * method to find all branches  by branch type
	 * @param branchType
	 * @return
	 */
	public List<BranchVo> findBranchesByBranchType(BranchType branchType);
	
	/**
	 * method to find all branches by parent station
	 * @param parentBranchVo
	 * @return
	 */
	public List<BranchVo> findBranchesByParentStation(String parentBranch);	
	
	/**
	 * method to find branches based on creation date
	 * @param date
	 * @return
	 */
	public List<BranchVo> findBranchesBasedOnCreationDate(String start , String end);
	
	/**
	 * method to return branch 
	 * @param Long id
	 * @return branch
	 */
	 public Branch findAndReturnBranchObjectById(Long branchId);
	/**
	 * method to return branch object based on name
	 * @param branchName
	 * @return
	 */
	 public Branch findAndReturnBranchObjectByName(String branchName);
	 /**
	  * method to delete branch based on id
	  * @param branchId
	  */
	 public void deleteBranchById(Long branchId);

	 /**
	  * method to update branch documents
	  * @param branchVo
	  * @return branchVo
	  */
	 public BranchVo updateBranchDocument(BranchVo branchVo);
	 /**
	  * method to delete branchDocument
	  * @param path
	  */
	 public void deleteBranchDocument(String path);
	 /**
	  * Method to return branch count
	  * @return
	  */
	 public Long getbranchCount();
	 
	 public List<BranchVo> findBranchesByIds(List<Long> ids);
}
