package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ProfidentFundDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ProfidentFund;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.ProfidentFundVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-April-2015
 *
 */
@Repository
public class ProfidentFundDaoImpl extends AbstractDao implements ProfidentFundDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	public void saveOrUpdateProfidentFund(ProfidentFundVo profidentFundVo) {
		ProfidentFund profidentFund = findProfidentFund(profidentFundVo.getProfidentFundId());
		if (profidentFund == null)
			profidentFund = new ProfidentFund();
		profidentFund.setEmployee(employeeDao.findAndReturnEmployeeByCode(profidentFundVo.getEmployeeCode()));
		profidentFund.setEmployeeShare(profidentFundVo.getEmployeeShare());
		profidentFund.setOrganizationShare(profidentFundVo.getOrganizationShare());
		profidentFund.setDescription(profidentFundVo.getDescription());
		profidentFund.setNotes(profidentFundVo.getNotes());
		this.sessionFactory.getCurrentSession().saveOrUpdate(profidentFund);
		profidentFundVo.setProfidentFundId(profidentFund.getId());
		profidentFundVo.setRecordedBy(profidentFund.getCreatedBy().getUserName());
		profidentFundVo.setRecordedOn(DateFormatter.convertDateToDetailedString(profidentFund.getCreatedOn()));
	}

	public void deleteProfidentFund(Long profidentFundId) {
		ProfidentFund profidentFund = findProfidentFund(profidentFundId);
		if (profidentFund == null)
			throw new ItemNotFoundException("ProfidentFund Information not found.");
		this.sessionFactory.getCurrentSession().delete(profidentFund);
	}

	public ProfidentFund findProfidentFund(Long profidentFundId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfidentFund.class)
				.add(Restrictions.eq("id", profidentFundId));
		if (criteria.uniqueResult() == null)
			return null;
		return (ProfidentFund) criteria.uniqueResult();
	}

	/**
	 * method to create ProfidentFundVo object
	 * 
	 * @param profidentFund
	 * @return profidentFundVo
	 */
	private ProfidentFundVo createProfidentFundVo(ProfidentFund profidentFund) {
		ProfidentFundVo profidentFundVo = new ProfidentFundVo();
		profidentFundVo.setProfidentFundId(profidentFund.getId());
		profidentFundVo.setEmployee(profidentFund.getEmployee().getUserProfile().getFirstName() + " "
				+ profidentFund.getEmployee().getUserProfile().getLastname());
		profidentFundVo.setEmployeeCode(profidentFund.getEmployee().getEmployeeCode());
		profidentFundVo.setEmployeeShare(profidentFund.getEmployeeShare());
		profidentFundVo.setOrganizationShare(profidentFund.getOrganizationShare());
		profidentFundVo.setDescription(profidentFund.getDescription());
		profidentFundVo.setNotes(profidentFund.getNotes());
		profidentFundVo.setRecordedBy(profidentFund.getCreatedBy().getUserName());
		profidentFundVo.setRecordedOn(DateFormatter.convertDateToDetailedString(profidentFund.getCreatedOn()));
		return profidentFundVo;
	}

	public ProfidentFundVo findProfidentFundById(Long profidentFundId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfidentFund.class)
				.add(Restrictions.eq("id", profidentFundId));
		if (criteria.uniqueResult() == null)
			return null;
		return createProfidentFundVo((ProfidentFund) criteria.uniqueResult());
	}

	public ProfidentFundVo findProfidentFundByEmployee(Employee employee) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfidentFund.class)
				.add(Restrictions.eq("employee", employee));
		if (criteria.uniqueResult() == null)
			return null;
		return createProfidentFundVo((ProfidentFund) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ProfidentFundVo> findAllProfidentFund() {
		List<ProfidentFundVo> profidentFundVos = new ArrayList<ProfidentFundVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfidentFund.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<ProfidentFund> profidentFunds = criteria.list();
		for (ProfidentFund profidentFund : profidentFunds)
			profidentFundVos.add(createProfidentFundVo(profidentFund));
		return profidentFundVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ProfidentFundVo> findProfidentFundBetweenDate(Long employeeId, Date startDate, Date endDate) {
		List<ProfidentFundVo> profidentFundVos = new ArrayList<ProfidentFundVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfidentFund.class)
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeById(employeeId)))
				.add(Restrictions.ge("createdOn", startDate)).add(Restrictions.le("createdOn", endDate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<ProfidentFund> profidentFunds = criteria.list();
		for (ProfidentFund profidentFund : profidentFunds)
			profidentFundVos.add(createProfidentFundVo(profidentFund));
		return profidentFundVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ProfidentFundVo> findAllProfidentFundByEmployee(String employeeCode) {
		List<ProfidentFundVo> profidentFundVos = new ArrayList<ProfidentFundVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProfidentFund.class);
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("createdBy", employee.getUser());
		Criterion receiver = Restrictions.eq("employee", employee);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<ProfidentFund> profidentFunds = criteria.list();
		for (ProfidentFund profidentFund : profidentFunds)
			profidentFundVos.add(createProfidentFundVo(profidentFund));
		return profidentFundVos;
	}

}
