package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CandidateStatusDao;
import com.hrms.web.dao.CountryDao;
import com.hrms.web.dao.JobCandidatesDao;
import com.hrms.web.dao.JobFieldDao;
import com.hrms.web.dao.JobInterviewDao;
import com.hrms.web.dao.JobPostDao;
import com.hrms.web.dao.LanguageDao;
import com.hrms.web.dao.QualificationDegreeDao;
import com.hrms.web.dao.SkillDao;
import com.hrms.web.entities.CandidateDocuments;
import com.hrms.web.entities.CandidateResume;
import com.hrms.web.entities.CandidateSkills;
import com.hrms.web.entities.CandidateStatus;
import com.hrms.web.entities.CandidateWorkExperience;
import com.hrms.web.entities.CandidatesAddress;
import com.hrms.web.entities.CandidatesLanguage;
import com.hrms.web.entities.CandidatesQualification;
import com.hrms.web.entities.CandidatesReferences;
import com.hrms.web.entities.Country;
import com.hrms.web.entities.EmployeeDesignation;
import com.hrms.web.entities.JobCandidates;
import com.hrms.web.entities.JobFields;
import com.hrms.web.entities.JobInterview;
import com.hrms.web.entities.JobPost;
import com.hrms.web.entities.Language;
import com.hrms.web.entities.QualificationDegree;
import com.hrms.web.entities.Skills;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.EmployeeDesignationService;
import com.hrms.web.service.JobPostService;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.CandidateDocumentsVo;
import com.hrms.web.vo.CandidateResumeVo;
import com.hrms.web.vo.CandidatesExperienceVo;
import com.hrms.web.vo.CandidatesLanguageVo;
import com.hrms.web.vo.CandidatesQualificationsVo;
import com.hrms.web.vo.CandidatesReferencesVo;
import com.hrms.web.vo.CandidatesSkillsVo;
import com.hrms.web.vo.JobCandidatesVo;
import com.hrms.web.vo.JobInterviewVo;

/**
 * 
 * @author Vinutha
 * @since 22-March-2015
 *
 */
@Repository
public class JobCandidatesDaoImpl extends AbstractDao implements JobCandidatesDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private JobFieldDao jobFieldsDao;

	@Autowired
	private CountryDao countryDao;

	@Autowired
	private QualificationDegreeDao degreeDao;

	@Autowired
	private LanguageDao languageDao;

	@Autowired
	private SkillDao skillDao;

	@Autowired
	private CandidateStatusDao statusDao;

	@Autowired
	private EmployeeDesignationService designationService;

	@Autowired
	private JobPostDao jobPostDao;

	public void saveJobCandidates(JobCandidatesVo candidatesVo) {
		// TODO Auto-generated method stub

	}

	public void updateJobCandidates(JobCandidatesVo candidatesVo) {
		// TODO Auto-generated method stub

	}

	/**
	 * method to delete job candidate
	 * 
	 * @param candidatesVo
	 */
	public void deleteJobCandidates(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		this.sessionFactory.getCurrentSession().delete(candidates);

	}

	/**
	 * method to delete candidate object based on id
	 * 
	 * @param id
	 * 
	 */
	public void deleteJobCandidateById(Long id) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(id);
		if (candidates.getJobInterview() == null) {
			if (candidates.getCandidateResume() != null) {
				SFTPOperation operation = new SFTPOperation();
				operation.removeFileFromSFTP(candidates.getCandidateResume().getDocumentPath());
				this.sessionFactory.getCurrentSession().delete(candidates);
			} else
				this.sessionFactory.getCurrentSession().delete(candidates);
		}else{
			throw new ItemNotFoundException("Candidate is Assigned for Interview");
		}

	}

	/***
	 * Method to find job candidate by id
	 * 
	 * @param id
	 * @return JobCandidatesVo
	 */
	public JobCandidatesVo findJobCandidatesById(Long id) {

		JobCandidates candidate = (JobCandidates) this.sessionFactory.getCurrentSession()
				.createCriteria(JobCandidates.class).add(Restrictions.eq("id", id)).uniqueResult();
		return createJobCandidateVo(candidate);
	}

	/**
	 * Method to find list of candidates by first name
	 * 
	 * @param firstName
	 * @return List<JobCandidatesVo>
	 */
	public List<JobCandidatesVo> findAllCandidateswithFirstName(String firstName) {
		List<JobCandidatesVo> candidates = new ArrayList<JobCandidatesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobCandidates.class)
				.add(Restrictions.eq("firstName", firstName));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobCandidates> candidatesObject = criteria.list();
		for (JobCandidates cand : candidatesObject) {
			candidates.add(createJobCandidateVo(cand));
		}
		return candidates;
	}

	/**
	 * Method to find list of candidates by last name
	 * 
	 * @param lastName
	 * @return List<JobCandidatesVo>
	 */
	public List<JobCandidatesVo> findAllCandidateswithLastName(String lastName) {
		List<JobCandidatesVo> candidates = new ArrayList<JobCandidatesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobCandidates.class)
				.add(Restrictions.eq("lastName", lastName));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobCandidates> candidatesObject = criteria.list();
		for (JobCandidates cand : candidatesObject) {
			candidates.add(createJobCandidateVo(cand));
		}
		return candidates;
	}

	/**
	 * method to fin and return candidates object based on id
	 * 
	 * @param id
	 * @return JobCandidates
	 */
	public JobCandidates findAndReturnJobCandidatesObjectWithId(Long id) {

		return (JobCandidates) this.sessionFactory.getCurrentSession().createCriteria(JobCandidates.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<JobCandidates> listCandidatesBasedOnStatusAndNumber(JobInterviewVo interviewVo) {
		JobPost jobPost = jobPostDao.findAndReturnJobPostObjectById(interviewVo.getJobPostId());
		String title = jobPost.getJobTitle();
		EmployeeDesignation des = designationService.findDesignationByDesignation(title);
		List<Long> statusNew = interviewVo.getStatusId();
		Set<String> statusName = new HashSet<String>();
		if (statusNew != null) {

			for (Long sts : statusNew) {
				CandidateStatus st = statusDao.findAndReturnStatusObjectById(sts);
				statusName.add(st.getStatus());

			}
		}
		List<JobCandidates> candidatesResult = new ArrayList<JobCandidates>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobCandidates.class)
				.add(Restrictions.isNull("jobInterview")).add(Restrictions.eq("designation", des))
				.add(Restrictions.in("status", statusName));
		criteria.setMaxResults(Integer.parseInt(interviewVo.getSelectionNumber()));
		criteria.addOrder(Order.asc("id"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		candidatesResult.addAll(criteria.list());
		return candidatesResult;

	}

	/**
	 * method to save basic info
	 * 
	 * @param candidatesVo
	 * @return Long
	 */
	public Long saveCandidateBasicInfo(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = new JobCandidates();
		// candidates.setJobFields(jobFieldsDao.findJobFiled(candidatesVo.getJobFieldId()));
		candidates.setDesignation(designationService.findDesignationByDesignation(candidatesVo.getJobField()));
		candidates.setFirstName(candidatesVo.getFirstName());
		candidates.setLastName(candidatesVo.getLastName());
		candidates.setBirthDate(DateFormatter.convertStringToDate(candidatesVo.getBirthDate()));
		candidates.setGender(candidatesVo.getGender());
		candidates.setCountry(countryDao.findCountry(candidatesVo.getNationality()));
		Long result = (Long) this.sessionFactory.getCurrentSession().save(candidates);
		return result;
	}

	/**
	 * method to update candidate
	 * 
	 * @param candidatesVo
	 * @return
	 */
	public void updateCandidateBasicInfo(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		// candidates.setJobFields(jobFieldsDao.findJobFiled(candidatesVo.getJobFieldId()));
		candidates.setDesignation(designationService.findDesignationByDesignation(candidatesVo.getJobField()));
		candidates.setFirstName(candidatesVo.getFirstName());
		candidates.setLastName(candidatesVo.getLastName());
		candidates.setBirthDate(DateFormatter.convertStringToDate(candidatesVo.getBirthDate()));
		candidates.setGender(candidatesVo.getGender());
		candidates.setCountry(countryDao.findCountry(candidatesVo.getNationality()));
		this.sessionFactory.getCurrentSession().merge(candidates);

	}

	/**
	 * method to list all candidates
	 * 
	 * @return List<JobCandidates>
	 */
	@SuppressWarnings("unchecked")
	public List<JobCandidatesVo> listAllCandidates() {

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobCandidates.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobCandidates> candidates = criteria.list();
		List<JobCandidatesVo> candidatesVos = new ArrayList<JobCandidatesVo>();
		for (JobCandidates candidate : candidates) {
			candidatesVos.add(createSimpleCandidateVo(candidate));

		}
		return candidatesVos;
	}

	/**
	 * method to save achievements
	 * 
	 * @param candidatesVo
	 */
	public void saveAchievement(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		candidates.setAchievements(candidatesVo.getAchievements());
		this.sessionFactory.getCurrentSession().merge(candidates);

	}

	/**
	 * method to save interests
	 * 
	 * @param candidatesVo
	 */
	public void saveInterests(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		candidates.setInterests(candidatesVo.getInterests());
		this.sessionFactory.getCurrentSession().merge(candidates);

	}

	/**
	 * method to save additional Information
	 * 
	 * @param candidatesVo
	 */
	public void saveAdditionalInfo(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		candidates.setAdditionalInfo(candidatesVo.getAdditionalInfo());
		this.sessionFactory.getCurrentSession().merge(candidates);

	}

	/**
	 * method to save contact details
	 * 
	 * @param candidatesVo
	 */
	public void saveContactDetails(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		CandidatesAddress addressDetails = candidates.getAddress();
		if (addressDetails == null) {
			addressDetails = new CandidatesAddress();
		}
		addressDetails.setAddress(candidatesVo.getAddress());
		addressDetails.setCity(candidatesVo.getCity());
		addressDetails.setState(candidatesVo.getState());
		addressDetails.setZipCode(candidatesVo.getZipCode());
		if (candidatesVo.getCountryId() != null) {
			Country country = new Country();
			country.setId(candidatesVo.getCountryId());
			addressDetails.setCountry(country);
		}

		addressDetails.seteMail(candidatesVo.geteMail());
		addressDetails.setPhoneNumber(candidatesVo.getPhoneNumber());
		addressDetails.setMobileNumber(candidatesVo.getMobileNumber());
		candidates.setAddress(addressDetails);
		addressDetails.setJobCandidates(candidates);
		this.sessionFactory.getCurrentSession().merge(candidates);

	}

	/**
	 * method to save qualifications
	 * 
	 * @param candidatesVo
	 */
	public void saveCandidateQualifications(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		QualificationDegree degree = new QualificationDegree();
		degree.setId(candidatesVo.getQualificationId());
		CandidatesQualification qualification = (CandidatesQualification) this.sessionFactory.getCurrentSession()
				.createCriteria(CandidatesQualification.class).add(Restrictions.eq("jobCandidates", candidates))
				.add(Restrictions.eq("degree", degree)).uniqueResult();
		if (qualification == null) {
			qualification = new CandidatesQualification();
			qualification.setDegree(degree);
		}
		qualification.setInstitute(candidatesVo.getInstitute());
		qualification.setSubject(candidatesVo.getSubject());
		qualification.setInstitute(candidatesVo.getInstitute());
		qualification.setGrade(candidatesVo.getGrade());
		qualification.setYear(candidatesVo.getYear());

		candidates.getCandidateQualification().add(qualification);
		qualification.setJobCandidates(candidates);

		this.sessionFactory.getCurrentSession().merge(candidates);

	}

	/**
	 * method to save language
	 * 
	 * @param candidatesVo
	 */
	public void saveCandidateLanguages(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		Language languageObj = new Language();
		languageObj.setId(candidatesVo.getLanguageId());

		CandidatesLanguage language = (CandidatesLanguage) this.sessionFactory.getCurrentSession()
				.createCriteria(CandidatesLanguage.class).add(Restrictions.eq("jobCandidates", candidates))
				.add(Restrictions.eq("language", languageObj)).uniqueResult();

		if (language == null) {

			language = new CandidatesLanguage();
			language.setLanguage(languageObj);
		}
		language.setSpeakingLevel(candidatesVo.getSpeaking());
		language.setWritingLevel(candidatesVo.getWriting());
		language.setReadingLevel(candidatesVo.getReading());
		candidates.getCandidatesLanguage().add(language);
		language.setJobCandidates(candidates);
		this.sessionFactory.getCurrentSession().merge(candidates);
	}

	/**
	 * method to save skills
	 * 
	 * @param candidatesVo
	 */
	public void saveCandidatesSkills(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		Skills skillsObj = new Skills();
		skillsObj.setId(candidatesVo.getSkillsId());

		CandidateSkills skills = (CandidateSkills) this.sessionFactory.getCurrentSession()
				.createCriteria(CandidateSkills.class).add(Restrictions.eq("jobCandidates", candidates))
				.add(Restrictions.eq("skills", skillsObj)).uniqueResult();

		if (skills == null) {
			skills = new CandidateSkills();
			skills.setSkills(skillsObj);
		}
		skills.setSkillLevel(candidatesVo.getSkillLevel());
		candidates.getCandidateSkills().add(skills);
		skills.setJobCandidates(candidates);
		this.sessionFactory.getCurrentSession().merge(candidates);
	}

	/**
	 * method to save experience
	 * 
	 * @param candidatesVo
	 */
	public void saveCandidateExperiences(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		CandidateWorkExperience experience = new CandidateWorkExperience();
		experience.setOrganisationName(candidatesVo.getOrganisationName());
		experience.setDesignation(candidatesVo.getDesignation());

		JobFields jobFieldObj = new JobFields();
		jobFieldObj.setId(candidatesVo.getFieldId());

		experience.setJobFields(jobFieldObj);
		experience.setDescription(candidatesVo.getDescription());
		experience.setStartDate(DateFormatter.convertStringToDate(candidatesVo.getStartDate()));
		experience.setEndDate(DateFormatter.convertStringToDate(candidatesVo.getEndDate()));
		experience.setStartingSalary(Double.parseDouble(candidatesVo.getStartSalary()));
		experience.setEndingSalary(Double.parseDouble(candidatesVo.getEndSalary()));
		candidates.getCandidateWorkExperience().add(experience);
		experience.setJobCandidates(candidates);
		this.sessionFactory.getCurrentSession().merge(candidates);

	}

	/**
	 * method to save candidate status
	 * 
	 * @param candidatesVo
	 */
	public void saveCandidateStatus(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		candidates.setStatus(candidatesVo.getStatus());
		this.sessionFactory.getCurrentSession().merge(candidates);

	}

	/**
	 * method to save candidate references
	 * 
	 * @param candidatesVo
	 */
	public void saveCandidateReferences(JobCandidatesVo candidatesVo) {
		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(candidatesVo.getJobCandidateId());
		CandidatesReferences references = new CandidatesReferences();
		references.setName(candidatesVo.getRefName());
		references.setOrganisationName(candidatesVo.getRefOrganisation());
		references.setPhoneNumber(candidatesVo.getRefPhoneNumber());
		references.seteMail(candidatesVo.getRefEMail());
		candidates.getReferences().add(references);
		references.setJobCandidates(candidates);
		this.sessionFactory.getCurrentSession().merge(candidates);
	}

	/**
	 * method to save candidate resume
	 * 
	 * @param candidatesVo
	 */
	public void saveCandidateResume(JobCandidatesVo candidatesVo) {

	}

	/**
	 * method to create simple candidate vo
	 * 
	 * @param candidates
	 * @return
	 */
	private JobCandidatesVo createSimpleCandidateVo(JobCandidates candidates) {
		JobCandidatesVo candidatesVo = new JobCandidatesVo();
		candidatesVo.setJobCandidateId(candidates.getId());
		/*
		 * if(candidates.getJobFields()!=null)
		 * candidatesVo.setJobField(candidates.getJobFields().getField());
		 */
		if (candidates.getDesignation() != null) {
			candidatesVo.setJobField(candidates.getDesignation().getDesignation());
		}
		candidatesVo.setFirstName(candidates.getFirstName());
		candidatesVo.setLastName(candidates.getLastName());
		if (candidates.getBirthDate() != null)
			candidatesVo.setBirthDate(DateFormatter.convertDateToString(candidates.getBirthDate()));
		candidatesVo.setGender(candidates.getGender());
		if (candidates.getCountry() != null)
			candidatesVo.setNationality(candidates.getCountry().getName());
		candidatesVo.setInterests(candidates.getInterests());
		candidatesVo.setAdditionalInfo(candidates.getAdditionalInfo());
		candidatesVo.setAchievements(candidates.getAchievements());
		candidatesVo.setCreatedBy(candidates.getCreatedBy().getUserName());
		if (candidates.getCreatedOn() != null)
			candidatesVo.setCreatedOn(DateFormatter.convertDateToString(candidates.getCreatedOn()));
		candidatesVo.setStatus(candidates.getStatus());

		if (candidates.getAddress() != null) {
			candidatesVo.seteMail(candidates.getAddress().geteMail());
		}
		return candidatesVo;
	}

	/**
	 * method to create detailed jobCandidateVo
	 * 
	 * @param candidates
	 * @return
	 */
	private JobCandidatesVo createJobCandidateVo(JobCandidates candidates) {
		JobCandidatesVo candidatesVo = new JobCandidatesVo();
		candidatesVo.setJobCandidateId(candidates.getId());
		// candidatesVo.setJobField(candidates.getJobFields().getField());
		candidatesVo.setJobField(candidates.getDesignation().getDesignation());
		candidatesVo.setFirstName(candidates.getFirstName());
		candidatesVo.setLastName(candidates.getLastName());
		candidatesVo.setBirthDate(DateFormatter.convertDateToString(candidates.getBirthDate()));
		candidatesVo.setGender(candidates.getGender());
		candidatesVo.setNationality(candidates.getCountry().getName());
		candidatesVo.setInterests(candidates.getInterests());
		candidatesVo.setAdditionalInfo(candidates.getAdditionalInfo());
		candidatesVo.setAchievements(candidates.getAchievements());
		candidatesVo.setCreatedBy(candidates.getCreatedBy().getUserName());
		candidatesVo.setCreatedOn(DateFormatter.convertDateToString(candidates.getCreatedOn()));
		candidatesVo.setStatus(candidates.getStatus());

		// Contact
		if (candidates.getAddress() != null) {
			CandidatesAddress address = candidates.getAddress();
			candidatesVo.setAddress(address.getAddress());
			candidatesVo.setCity(address.getCity());
			candidatesVo.setState(address.getState());
			candidatesVo.setZipCode(address.getZipCode());
			if (address.getCountry() != null) {
				candidatesVo.setCountryId(address.getCountry().getId());
				candidatesVo.setCountry(address.getCountry().getName());
			}
			candidatesVo.seteMail(address.geteMail());
			candidatesVo.setPhoneNumber(address.getPhoneNumber());
			candidatesVo.setMobileNumber(address.getMobileNumber());

		}
		// Qualification
		if (candidates.getCandidateQualification() != null) {
			candidatesVo.setCandidateQualifications(createQualificationVo(candidates));
		}
		// Experience
		if (candidates.getCandidateWorkExperience() != null) {
			candidatesVo.setCandidateWorkExperiences(createExperienceVo(candidates));
		}
		// Skills
		if (candidates.getCandidateSkills() != null) {
			candidatesVo.setCandidateSkills(createSkillsVo(candidates));
		}
		// Language
		if (candidates.getCandidatesLanguage() != null) {
			candidatesVo.setCandidatesLanguages(createLanguageVo(candidates));
		}
		// References
		if (candidates.getReferences() != null) {
			candidatesVo.setCandidatesReferences(createReferenceVo(candidates));
		}
		// Resume
		if (candidates.getCandidateResume() != null) {
			CandidateResume resume = candidates.getCandidateResume();
			CandidateResumeVo resumeVo = new CandidateResumeVo();
			resumeVo.setResumeFileName(resume.getResumeFileName());
			resumeVo.setResumeUrl(resume.getDocumentPath());
			resumeVo.setResumeId(resume.getId());

			candidatesVo.setResumeVo(resumeVo);
		}
		// Documents
		if (candidates.getCandidateDocuments() != null) {
			Set<CandidateDocuments> documents = candidates.getCandidateDocuments();
			Set<CandidateDocumentsVo> documentVos = new HashSet<CandidateDocumentsVo>();
			for (CandidateDocuments document : documents) {
				documentVos.add(createDocumentsVo(document));
			}
			candidatesVo.setCandidateDocumentsVos(documentVos);
		}

		return candidatesVo;
	}

	/**
	 * method to create qualification vo
	 * 
	 * @param candidates
	 * @return
	 */
	private Set<CandidatesQualificationsVo> createQualificationVo(JobCandidates candidates) {
		Set<CandidatesQualificationsVo> qualificationsVos = new HashSet<CandidatesQualificationsVo>();
		Set<CandidatesQualification> qualifications = candidates.getCandidateQualification();
		for (CandidatesQualification qualification : qualifications) {
			CandidatesQualificationsVo qualificationsVo = new CandidatesQualificationsVo();
			qualificationsVo.setDegree(qualification.getDegree().getDegree());
			qualificationsVo.setSubject(qualification.getSubject());
			qualificationsVo.setInstitute(qualification.getInstitute());
			qualificationsVo.setGrade(qualification.getGrade());
			qualificationsVo.setGradYear(qualification.getYear());
			qualificationsVos.add(qualificationsVo);

		}
		return qualificationsVos;
	}

	/**
	 * method to create skills vo
	 * 
	 * @param candidates
	 * @return
	 */
	private Set<CandidatesSkillsVo> createSkillsVo(JobCandidates candidates) {
		Set<CandidatesSkillsVo> skillVos = new HashSet<CandidatesSkillsVo>();
		Set<CandidateSkills> skills = candidates.getCandidateSkills();
		for (CandidateSkills skills2 : skills) {
			CandidatesSkillsVo skillsVo = new CandidatesSkillsVo();
			skillsVo.setSkill(skills2.getSkills().getSkill());
			skillsVo.setSkillLevel(skills2.getSkillLevel());
			skillVos.add(skillsVo);
		}
		return skillVos;
	}

	/**
	 * method to create experience vo
	 * 
	 * @param candidates
	 * @return
	 */
	private Set<CandidatesExperienceVo> createExperienceVo(JobCandidates candidates) {
		Set<CandidatesExperienceVo> experienceVos = new HashSet<CandidatesExperienceVo>();
		Set<CandidateWorkExperience> experiences = candidates.getCandidateWorkExperience();
		for (CandidateWorkExperience experience : experiences) {
			CandidatesExperienceVo experienceVo = new CandidatesExperienceVo();
			experienceVo.setOrgName(experience.getOrganisationName());
			experienceVo.setDesignation(experience.getDesignation());
			if (experience.getJobFields() != null)
				experienceVo.setJobField(experience.getJobFields().getField());
			experienceVos.add(experienceVo);
		}
		return experienceVos;
	}

	/**
	 * method to create referce vo
	 * 
	 * @param candidates
	 * @return
	 */
	private Set<CandidatesReferencesVo> createReferenceVo(JobCandidates candidates) {
		Set<CandidatesReferencesVo> referencesVos = new HashSet<CandidatesReferencesVo>();
		Set<CandidatesReferences> references = candidates.getReferences();
		for (CandidatesReferences reference : references) {
			CandidatesReferencesVo referencesVo = new CandidatesReferencesVo();
			referencesVo.setName(reference.getName());
			referencesVo.seteMail(reference.geteMail());
			referencesVo.setOrgName(reference.getOrganisationName());
			referencesVo.setPhoneNumber(reference.getPhoneNumber());
			referencesVos.add(referencesVo);

		}
		return referencesVos;
	}

	/**
	 * method to create language vo
	 * 
	 * @param candidates
	 * @return
	 */
	private Set<CandidatesLanguageVo> createLanguageVo(JobCandidates candidates) {
		Set<CandidatesLanguageVo> languageVos = new HashSet<CandidatesLanguageVo>();
		Set<CandidatesLanguage> languages = candidates.getCandidatesLanguage();
		for (CandidatesLanguage language : languages) {
			CandidatesLanguageVo languageVo = new CandidatesLanguageVo();
			languageVo.setLanguage(language.getLanguage().getName());
			languageVo.setSpeakLevel(language.getSpeakingLevel());
			languageVo.setReadLevel(language.getReadingLevel());
			languageVo.setWriteLevel(language.getWritingLevel());
			languageVos.add(languageVo);
		}
		return languageVos;
	}

	/***
	 * method to update documents
	 * 
	 * @param candidatesVo
	 */
	public void updateDocuments(JobCandidatesVo candidatesVo) {
		JobCandidates candidate = (JobCandidates) this.sessionFactory.getCurrentSession()
				.createCriteria(JobCandidates.class).add(Restrictions.eq("id", candidatesVo.getJobCandidateId()))
				.uniqueResult();
		candidate.setCandidateDocuments(setDocuments(candidatesVo.getCandidateDocumentsVos(), candidate));
		this.sessionFactory.getCurrentSession().merge(candidate);

	}

	/**
	 * method to convert documentvos to document objects
	 * 
	 * @param documentVos
	 * @param candidates
	 * @return
	 */
	private Set<CandidateDocuments> setDocuments(Set<CandidateDocumentsVo> documentVos, JobCandidates candidates) {
		Set<CandidateDocuments> candidateDocuments = new HashSet<CandidateDocuments>();
		for (CandidateDocumentsVo documentsVo : documentVos) {
			CandidateDocuments documents = new CandidateDocuments();
			documents.setFileName(documentsVo.getFileName());
			documents.setDocumentPath(documentsVo.getDocumentUrl());
			documents.setJobCandidates(candidates);
			candidateDocuments.add(documents);
		}
		return candidateDocuments;

	}

	/**
	 * method to delete document
	 * 
	 * @param candidatesVo
	 */
	public void deleteDocuments(String url) {
		CandidateDocuments document = (CandidateDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(CandidateDocuments.class).add(Restrictions.eq("documentPath", url)).uniqueResult();

		JobCandidates candidates = findAndReturnJobCandidatesObjectWithId(document.getJobCandidates().getId());

		candidates.getCandidateDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(candidates);

	}

	/**
	 * method to update resume
	 * 
	 * @param candidatesVo
	 */
	public void updateResume(JobCandidatesVo candidatesVo) {
		JobCandidates candidate = (JobCandidates) this.sessionFactory.getCurrentSession()
				.createCriteria(JobCandidates.class).add(Restrictions.eq("id", candidatesVo.getJobCandidateId()))
				.uniqueResult();
		if (candidate.getCandidateResume() != null) {
			deleteResume(candidate);
		}
		CandidateResumeVo resumeVo = candidatesVo.getResumeVo();
		CandidateResume resume = new CandidateResume();
		resume.setResumeFileName(resumeVo.getResumeFileName());
		resume.setDocumentPath(resumeVo.getResumeUrl());
		resume.setJobCandidates(candidate);
		candidate.setCandidateResume(resume);
		this.sessionFactory.getCurrentSession().merge(candidate);

	}

	/**
	 * method to delete resume
	 * 
	 * @param candidatesVo
	 */
	public void deleteResume(String url) {

		CandidateResume resume = (CandidateResume) this.sessionFactory.getCurrentSession()
				.createCriteria(CandidateResume.class).add(Restrictions.eq("documentPath", url)).uniqueResult();
		JobCandidates candidate = resume.getJobCandidates();
		candidate.setCandidateResume(null);
		this.sessionFactory.getCurrentSession().delete(resume);
		this.sessionFactory.getCurrentSession().merge(candidate);
	}

	/**
	 * method to create candidate document vo
	 * 
	 * @param documents
	 * @return
	 */
	private CandidateDocumentsVo createDocumentsVo(CandidateDocuments documents) {
		CandidateDocumentsVo vo = new CandidateDocumentsVo();
		vo.setDocumentId(documents.getId());
		vo.setDocumentUrl(documents.getDocumentPath());
		vo.setFileName(documents.getFileName());
		return vo;
	}

	/**
	 * method to delete resume by passing JobCandidates Object
	 * 
	 * @param candidates
	 * @return
	 */
	private boolean deleteResume(JobCandidates candidates) {
		CandidateResume resume = candidates.getCandidateResume();
		candidates.setCandidateResume(null);
		this.sessionFactory.getCurrentSession().delete(resume);
		this.sessionFactory.getCurrentSession().merge(candidates);

		return true;
	}

	/**
	 * method to get candidate's qualification details
	 * 
	 * @param candidate
	 * @param degree
	 * @return candidate qualificaton vo
	 */
	public CandidatesQualificationsVo getCandQualification(Long candidate, Long degree) {
		CandidatesQualification qualification = (CandidatesQualification) this.sessionFactory.getCurrentSession()
				.createCriteria(CandidatesQualification.class).add(Restrictions.eq("jobCandidates.id", candidate))
				.add(Restrictions.eq("degree.id", degree)).uniqueResult();
		return (createCandQualificationVo(qualification));
	}

	/**
	 * method to create candidate qualification vo
	 * 
	 * @param qualification
	 * @return
	 */
	private CandidatesQualificationsVo createCandQualificationVo(CandidatesQualification qualification) {
		CandidatesQualificationsVo candQualVo = new CandidatesQualificationsVo();
		if (qualification != null) {
			candQualVo.setDegree(qualification.getDegree().getDegree());
			candQualVo.setInstitute(qualification.getInstitute());
			candQualVo.setSubject(qualification.getSubject());
			candQualVo.setGrade(qualification.getGrade());
			candQualVo.setGradYear(qualification.getYear());
		}
		return candQualVo;
	}

	/**
	 * method to get candidate skills details
	 * 
	 * @param candidate
	 * @param skill
	 * @return CandidatesSkillsVo
	 */
	public CandidatesSkillsVo getCandSkills(Long candidate, Long skill) {
		CandidateSkills candidateSkill = (CandidateSkills) this.sessionFactory.getCurrentSession()
				.createCriteria(CandidateSkills.class).add(Restrictions.eq("jobCandidates.id", candidate))
				.add(Restrictions.eq("skills.id", skill)).uniqueResult();

		return (createSkillsVo(candidateSkill));
	}

	/**
	 * method to createCandidatesSkillsVo
	 * 
	 * @param skillsObj
	 * @return CandidatesSkillsVo
	 */
	private CandidatesSkillsVo createSkillsVo(CandidateSkills skillsObj) {
		CandidatesSkillsVo skillsVo = new CandidatesSkillsVo();
		if (skillsObj != null) {
			skillsVo.setSkill(skillsObj.getSkills().getSkill());
			skillsVo.setSkillLevel(skillsObj.getSkillLevel());
		}
		return skillsVo;
	}

	/**
	 * method to get candidate's language
	 * 
	 * @param candidate
	 * @param language
	 * @return CandidatesLanguageVo
	 */
	public CandidatesLanguageVo getCandLanguage(Long candidate, Long language) {
		CandidatesLanguage candidatesLanguage = (CandidatesLanguage) this.sessionFactory.getCurrentSession()
				.createCriteria(CandidatesLanguage.class).add(Restrictions.eq("jobCandidates.id", candidate))
				.add(Restrictions.eq("language.id", language)).uniqueResult();
		return (createLanguageVo(candidatesLanguage));
	}

	/**
	 * method to create CandidatesLanguageVo
	 * 
	 * @param candidatesLanguage
	 * @return
	 */
	private CandidatesLanguageVo createLanguageVo(CandidatesLanguage candidatesLanguage) {
		CandidatesLanguageVo languageVo = new CandidatesLanguageVo();
		if (candidatesLanguage != null) {
			languageVo.setLanguage(candidatesLanguage.getLanguage().getName());
			languageVo.setReadLevel(candidatesLanguage.getReadingLevel());
			languageVo.setWriteLevel(candidatesLanguage.getWritingLevel());
			languageVo.setSpeakLevel(candidatesLanguage.getSpeakingLevel());
		}
		return languageVo;
	}

	/**
	 * Method to list candidates based on field and gender
	 * 
	 * @param fieldId
	 * @param gender
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<JobCandidatesVo> findCandidtesByPostAndGender(String designation, String gender) {
		EmployeeDesignation desObject = designationService.findDesignationByDesignation(designation);
		List<JobCandidatesVo> resultVo = new ArrayList<JobCandidatesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobCandidates.class)
				.add(Restrictions.eq("designation", desObject)).add(Restrictions.eq("gender", gender));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobCandidates> candidates = criteria.list();
		for (JobCandidates cand : candidates) {
			resultVo.add(createJobCandidateVo(cand));
		}
		return resultVo;
	}

	/**
	 * Method to list candidates based on field
	 * 
	 * @param fieldId
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<JobCandidatesVo> findCandidtesByPost(String designation) {
		EmployeeDesignation desObject = designationService.findDesignationByDesignation(designation);
		List<JobCandidatesVo> resultVo = new ArrayList<JobCandidatesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobCandidates.class)
				.add(Restrictions.eq("designation", desObject));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobCandidates> candidates = criteria.list();
		for (JobCandidates cand : candidates) {
			resultVo.add(createJobCandidateVo(cand));
		}
		return resultVo;
	}

	/**
	 * Method to list candidates based on gender
	 * 
	 * @param gender
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<JobCandidatesVo> findCandidtesByGender(String gender) {
		List<JobCandidatesVo> resultVo = new ArrayList<JobCandidatesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobCandidates.class)
				.add(Restrictions.eq("gender", gender));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobCandidates> candidates = criteria.list();
		for (JobCandidates cand : candidates) {
			resultVo.add(createJobCandidateVo(cand));
		}
		return resultVo;
	}

	/**
	 * Method to list candidates for report
	 * 
	 * @param gender
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<JobCandidatesVo> findAllCandidatesForReport() {
		List<JobCandidatesVo> resultVo = new ArrayList<JobCandidatesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobCandidates.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobCandidates> candidates = criteria.list();
		for (JobCandidates cand : candidates) {
			resultVo.add(createJobCandidateVo(cand));
		}
		return resultVo;
	}

	/**
	 * Method to get candidates by interview
	 * 
	 * @param interviewId
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<JobCandidatesVo> findCandidatesByInterview(Long interviewId) {

		List<JobCandidatesVo> resultVo = new ArrayList<JobCandidatesVo>();
		// JobInterview interview
		// =interviewDao.findAndReturnInterviewObjectById(interviewId);
		// JobInterview interview = new JobInterview();
		// interview.setId(interviewId);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobCandidates.class);
		criteria.add(Restrictions.eq("jobInterview.id", interviewId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<JobCandidates> candidates = criteria.list();
		for (JobCandidates cand : candidates) {
			resultVo.add(createJobCandidateVo(cand));
		}
		return resultVo;
	}
}
