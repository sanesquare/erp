package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.web.entities.ErPCurrency;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BaseCurrencyDao;
import com.hrms.web.dao.CurrencyDao;
import com.hrms.web.entities.BaseCurrency;
import com.hrms.web.entities.Currency;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.CurrencyVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Repository
public class CurrencyDaoImpl extends AbstractDao implements CurrencyDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private BaseCurrencyDao baseCurrencyDao;

	/**
	 * method to save new Currency
	 * 
	 * @param currencyVo
	 */
	public void saveCurrency(CurrencyVo currencyVo) {
		Currency currency = new Currency();
		currency.setDecimaplPlaces(currencyVo.getDecimalPlaces());
		currency.setBaseCurrency(getBaseCurrency(currencyVo.getBaseCurrency()));
		this.sessionFactory.getCurrentSession().save(currency);
	}

	/**
	 * method to get basecurrency object
	 * 
	 * @param baseCurrencyName
	 * @return BaseCurrency
	 */
	private BaseCurrency getBaseCurrency(String baseCurrencyName) {
		return baseCurrencyDao.findBaseCurrency(baseCurrencyName);
	}

	/**
	 * method to update Currency
	 * 
	 * @param currencyVo
	 */
	public void updateCurrency(CurrencyVo currencyVo) {
		Currency currency = findCurrency(currencyVo.getCurrencyId());
		if (currency == null)
			throw new ItemNotFoundException("Currency Not Found");
		currency.setBaseCurrency(getBaseCurrency(currencyVo.getBaseCurrency()));
		currency.setDecimaplPlaces(currencyVo.getDecimalPlaces());
		this.sessionFactory.getCurrentSession().merge(currency);
	}

	/**
	 * method to delete Currency
	 * 
	 * @param currencyId
	 */
	public void deleteCurrency(Long currencyId) {
		Currency currency = findCurrency(currencyId);
		if (currency == null)
			throw new ItemNotFoundException("Currency Not Found");
		this.sessionFactory.getCurrentSession().delete(currency);
	}

	/**
	 * method to find Currency by id
	 * 
	 * @param currencyId
	 * @return Currency
	 */
	public Currency findCurrency(Long currencyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Currency.class)
				.add(Restrictions.eq("id", currencyId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Currency) criteria.uniqueResult();
	}

	/**
	 * method to create currencyvo
	 * 
	 * @param currency
	 * @return currencyvo
	 */
	private CurrencyVo createCurrencyVo(Currency currency) {
		CurrencyVo currencyVo = new CurrencyVo();
		currencyVo.setBaseCurrency(currency.getBaseCurrency().getBaseCurrency());
		currencyVo.setBaseCurrencyId(currency.getBaseCurrency().getId());
		currencyVo.setCurrencyId(currency.getId());
		currencyVo.setCurrencySign(currency.getBaseCurrency().getSign());
		currencyVo.setDecimalPlaces(currency.getDecimaplPlaces());
		return currencyVo;
	}

	/**
	 * method to find Currency by id
	 * 
	 * @param currencyId
	 * @return CurrencyVo
	 */
	public CurrencyVo findCurrencyById(Long currencyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Currency.class)
				.add(Restrictions.eq("id", currencyId));
		if (criteria.uniqueResult() == null)
			return null;
		return createCurrencyVo((Currency) criteria.uniqueResult());
	}

	/**
	 * method to find all Currency
	 * 
	 * @return List<CurrencyVo>
	 */
	@SuppressWarnings("unchecked")
	public List<CurrencyVo> findAllCurrency() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Currency.class);
		List<CurrencyVo> currencyVos = new ArrayList<CurrencyVo>();
		List<Currency> currencies = criteria.list();
		for (Currency currency : currencies)
			currencyVos.add(createCurrencyVo(currency));
		return currencyVos;
	}


}
