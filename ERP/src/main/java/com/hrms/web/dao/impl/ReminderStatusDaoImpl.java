package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ReminderStatusDao;
import com.hrms.web.entities.ReminderStatus;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.ReminderStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Repository
public class ReminderStatusDaoImpl extends AbstractDao implements ReminderStatusDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveReminderStatus(ReminderStatusVo reminderStatusVo) {
		for (String reminderStatus : StringSplitter.splitWithComma(reminderStatusVo.getStatus())) {
			if (findReminderStatus(reminderStatus.trim()) != null)
				throw new DuplicateItemException("Duplicate Reminder Status : " + reminderStatus.trim());
			ReminderStatus status = findReminderStatus(reminderStatusVo.getTempStatus());
			if (status == null)
				status = new ReminderStatus();
			status.setStatus(reminderStatus.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(status);
		}
	}

	public void updateReminderStatus(ReminderStatusVo reminderStatusVo) {
		ReminderStatus reminderStatus = findReminderStatus(reminderStatusVo.getReminderStatusId());
		if (reminderStatus == null)
			throw new ItemNotFoundException("Reminder Status Not Exist");
		reminderStatus.setStatus(reminderStatusVo.getStatus());
		this.sessionFactory.getCurrentSession().merge(reminderStatus);
	}

	public void deleteReminderStatus(Long reminderStatusId) {
		ReminderStatus reminderStatus = findReminderStatus(reminderStatusId);
		if (reminderStatus == null)
			throw new ItemNotFoundException("Reminder Status Not Exist");
		this.sessionFactory.getCurrentSession().delete(reminderStatus);
	}

	public ReminderStatus findReminderStatus(Long reminderStatusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReminderStatus.class)
				.add(Restrictions.eq("id", reminderStatusId));
		if (criteria.uniqueResult() == null)
			return null;
		return (ReminderStatus) criteria.uniqueResult();
	}

	public ReminderStatus findReminderStatus(String reminderStatus) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReminderStatus.class)
				.add(Restrictions.eq("status", reminderStatus));
		if (criteria.uniqueResult() == null)
			return null;
		return (ReminderStatus) criteria.uniqueResult();
	}

	/**
	 * method to create reminderStatusVo object
	 * 
	 * @param reminderStatus
	 * @return reminderStatusVo
	 */
	private ReminderStatusVo createReminderStatusVo(ReminderStatus reminderStatus) {
		ReminderStatusVo reminderStatusVo = new ReminderStatusVo();
		reminderStatusVo.setStatus(reminderStatus.getStatus());
		reminderStatusVo.setReminderStatusId(reminderStatus.getId());
		return reminderStatusVo;
	}

	public ReminderStatusVo findReminderStatusById(Long reminderStatusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReminderStatus.class)
				.add(Restrictions.eq("id", reminderStatusId));
		if (criteria.uniqueResult() == null)
			return null;
		return createReminderStatusVo((ReminderStatus) criteria.uniqueResult());
	}

	public ReminderStatusVo findReminderStatusByStatus(String reminderStatus) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReminderStatus.class)
				.add(Restrictions.eq("status", reminderStatus));
		if (criteria.uniqueResult() == null)
			return null;
		return createReminderStatusVo((ReminderStatus) criteria.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<ReminderStatusVo> findAllReminderStatus() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ReminderStatus.class);
		List<ReminderStatusVo> reminderStatusVos = new ArrayList<ReminderStatusVo>();
		List<ReminderStatus> reminderStatuses = criteria.list();
		for (ReminderStatus reminderStatus : reminderStatuses)
			reminderStatusVos.add(createReminderStatusVo(reminderStatus));
		return reminderStatusVos;
	}

}
