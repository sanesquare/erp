package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.SecondLevelForwaderDao;
import com.hrms.web.entities.SecondLevelForwader;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.SecondLevelForwardingVo;

/**
 * 
 * @author Jithin Mohan
 * @since 27-April-2015
 *
 */
@Repository
public class SecondLevelForwaderDaoImpl extends AbstractDao implements SecondLevelForwaderDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	public void saveSecondLevelForwader(SecondLevelForwardingVo secondLevelForwardingVo) {
		deleteNonAvailableForwarders(secondLevelForwardingVo);
		for (String employeeCode : secondLevelForwardingVo.getForwaders()) {
			SecondLevelForwader secondLevelForwader = new SecondLevelForwader();
			if (findSecondLevelForwader(employeeCode) == null) {
				secondLevelForwader.setForwaders(employeeDao.findAndReturnEmployeeByCode(employeeCode));
				this.sessionFactory.getCurrentSession().saveOrUpdate(secondLevelForwader);
			}
		}
	}

	/**
	 * method to delete forwarder who is already available but not in the new
	 * list
	 * 
	 * @param secondLevelForwarders
	 * @return
	 */
	private void deleteNonAvailableForwarders(SecondLevelForwardingVo secondLevelForwardingVo) {
		List<String> availableList = findAllSecondLevelForwader().getForwaders();
		List<String> newList = secondLevelForwardingVo.getForwaders();
		List<String> tempList = new ArrayList<String>();

		if (availableList != null)
			tempList.addAll(availableList);

		tempList.removeAll(newList);

		for (String employeeCode : tempList) {
			deleteSecondLevelForwader(findSecondLevelForwader(employeeCode).getId());
		}
	}

	public void updateSecondLevelForwader(SecondLevelForwardingVo secondLevelForwardingVo) {
		// TODO Auto-generated method stub

	}

	public void deleteSecondLevelForwader(Long secondLevelForwaderId) {
		SecondLevelForwader forwader = findSecondLevelForwader(secondLevelForwaderId);
		if (forwader == null)
			throw new ItemNotFoundException("Second Level Forwarder Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(forwader);
	}

	public SecondLevelForwader findSecondLevelForwader(Long secondLevelForwaderId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SecondLevelForwader.class)
				.add(Restrictions.eq("id", secondLevelForwaderId));
		if (criteria.uniqueResult() == null)
			return null;
		return (SecondLevelForwader) criteria.uniqueResult();
	}

	public SecondLevelForwader findSecondLevelForwader(String secondLevelForwaderCode) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SecondLevelForwader.class)
				.add(Restrictions.eq("forwaders", employeeDao.findAndReturnEmployeeByCode(secondLevelForwaderCode)));
		if (criteria.uniqueResult() == null)
			return null;
		return (SecondLevelForwader) criteria.uniqueResult();
	}

	/**
	 * method to create SecondLevelForwardingVo object
	 * 
	 * @param forwader
	 * @return forwardingVo
	 */
	private SecondLevelForwardingVo createSecondLevelForwardingVo(SecondLevelForwader forwader) {
		SecondLevelForwardingVo forwardingVo = new SecondLevelForwardingVo();
		forwardingVo.setSec_level_fwdr_id(forwader.getId());
		forwardingVo.setForwaderCode(forwader.getForwaders().getEmployeeCode());
		forwardingVo.setForwaderName(forwader.getForwaders().getUserProfile().getFirstName() + " "
				+ forwader.getForwaders().getUserProfile().getLastname());
		return forwardingVo;
	}

	/**
	 * method to create SecondLevelForwardingVo object with list of forwarders
	 * 
	 * @param forwader
	 * @return forwardingVo
	 */
	private SecondLevelForwardingVo createSecondLevelForwardersList(List<SecondLevelForwader> forwaders) {
		SecondLevelForwardingVo forwardingVo = new SecondLevelForwardingVo();
		List<String> forwarders = new ArrayList<String>();
		List<String> forwardersName = new ArrayList<String>();
		for (SecondLevelForwader forwader : forwaders) {
			forwarders.add(forwader.getForwaders().getEmployeeCode());
			forwardersName.add(forwader.getForwaders().getUserProfile().getFirstName() + " "
					+ forwader.getForwaders().getUserProfile().getLastname());
		}
		forwardingVo.setForwaders(forwarders);
		forwardingVo.setForwadersName(forwardersName);
		return forwardingVo;
	}

	public SecondLevelForwardingVo findSecondLevelForwaderById(Long secondLevelForwaderId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SecondLevelForwader.class)
				.add(Restrictions.eq("id", secondLevelForwaderId));
		if (criteria.uniqueResult() == null)
			return null;
		return createSecondLevelForwardingVo((SecondLevelForwader) criteria.uniqueResult());
	}

	public SecondLevelForwardingVo findSecondLevelForwaderByForwader(String secondLevelForwaderCode) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SecondLevelForwader.class)
				.add(Restrictions.eq("forwaders", secondLevelForwaderCode));
		if (criteria.uniqueResult() == null)
			return null;
		return createSecondLevelForwardingVo((SecondLevelForwader) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public SecondLevelForwardingVo findAllSecondLevelForwader() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SecondLevelForwader.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return createSecondLevelForwardersList(criteria.list());
	}

	/**
	 * 
	 * @param forwaders
	 * @return
	 */
	private SecondLevelForwardingVo createEmailList(List<SecondLevelForwader> forwaders) {
		SecondLevelForwardingVo forwardingVo = new SecondLevelForwardingVo();
		List<String> emails = new ArrayList<String>();
		for (SecondLevelForwader forwader : forwaders) {
			String email = forwader.getForwaders().getUser().getEmail();
			if (email != null)
				emails.add(email);
		}
		forwardingVo.setEmail(emails);
		return forwardingVo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public SecondLevelForwardingVo findAllSecondLevelForwaderEmails() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SecondLevelForwader.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return createEmailList(criteria.list());
	}

}
