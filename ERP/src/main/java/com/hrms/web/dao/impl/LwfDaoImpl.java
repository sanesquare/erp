package com.hrms.web.dao.impl;

import java.math.BigDecimal;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.LwfDao;
import com.hrms.web.entities.Lwf;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.LwfVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-May-2015
 *
 */
@Repository
public class LwfDaoImpl extends AbstractDao implements LwfDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveLwf(LwfVo lwfVo) {
		Lwf lwf = findLwf(lwfVo.getLwfId());
		if (lwf == null)
			lwf = new Lwf();
		lwf.setAmount(new BigDecimal(lwfVo.getAmount()));
		this.sessionFactory.getCurrentSession().saveOrUpdate(lwf);
		lwfVo.setLwfId(lwf.getId());
	}

	public Lwf findLwf(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Lwf.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return (Lwf) criteria.uniqueResult();
	}

	/**
	 * method to create LwfVo object
	 * 
	 * @param lwf
	 * @return lwfVo
	 */
	private LwfVo createLwfVo(Lwf lwf) {
		LwfVo lwfVo = new LwfVo();
		lwfVo.setLwfId(lwf.getId());
		lwfVo.setAmount(lwf.getAmount().doubleValue());
		return lwfVo;
	}

	public LwfVo findLwfById(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Lwf.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return createLwfVo((Lwf) criteria.uniqueResult());
	}

	public LwfVo findLwf() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Lwf.class);
		if (criteria.uniqueResult() == null)
			return null;
		return createLwfVo((Lwf) criteria.uniqueResult());
	}

}
