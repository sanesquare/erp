package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.JournalDao;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.PaymentDao;
import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.Payment;
import com.accounts.web.entities.PaymentEntries;
import com.accounts.web.entities.Transactions;
import com.accounts.web.vo.JournalItemsVo;
import com.accounts.web.vo.JournalVo;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.vo.ErpCurrencyVo;
import com.hrms.web.constants.PaySlipConstants;
import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AdvanceSalaryDao;
import com.hrms.web.dao.AdvanceSalaryStatusDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.DepartmentDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ExtraPaySlipItemDao;
import com.hrms.web.dao.LoanDao;
import com.hrms.web.dao.LoanStatusDao;
import com.hrms.web.dao.PaySalaryDao;
import com.hrms.web.dao.PaySlipItemDao;
import com.hrms.web.dao.SalaryApprovalStatusDao;
import com.hrms.web.entities.AdvanceSalary;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Loan;
import com.hrms.web.entities.PaySalary;
import com.hrms.web.entities.PaySalaryDailyWages;
import com.hrms.web.entities.PaySalaryEmployeeItems;
import com.hrms.web.entities.PaySalaryEmployees;
import com.hrms.web.entities.PaySalaryHourlyWage;
import com.hrms.web.entities.PaySalaryOtherAllowanceItems;
import com.hrms.web.entities.PaySlipItem;
import com.hrms.web.entities.PaysalaryEmployeePayslipItem;
import com.hrms.web.entities.SalaryApprovalStatus;
import com.hrms.web.entities.SalaryType;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.ExtraPaySlipItemVo;
import com.hrms.web.vo.PayPaysalaryVo;
import com.hrms.web.vo.PaySalaryDailySalaryVo;
import com.hrms.web.vo.PaySalaryEmployeeItemsVo;
import com.hrms.web.vo.PaySalaryHourlySalaryVo;
import com.hrms.web.vo.PaySalaryVo;
import com.hrms.web.vo.PaySlipItemVo;

/**
 * 
 * @author Shamsheer
 * @since 08-June-2015
 *
 */
@Repository
public class PaySalaryDaoImpl extends AbstractDao implements PaySalaryDao {

	@Autowired
	private ErpCurrencyDao erpCurrencyDao;

	@Autowired
	private SalaryTypeDao salaryTypeDao;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private BranchDao branchDao;

	@Autowired
	private PaySlipItemDao paySlipItemDao;

	@Autowired
	private DepartmentDao departmentDao;

	@Autowired
	private ExtraPaySlipItemDao extraPayslipItemDao;

	@Autowired
	private SalaryApprovalStatusDao approvalStatusDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private LoanDao loanDao;

	@Autowired
	private AdvanceSalaryDao advanceSalaryDao;

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private PaymentDao paymentDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private AdvanceSalaryStatusDao advanceSalaryStatusDao;

	@Autowired
	private LoanStatusDao loanStatusDao;

	@Autowired
	private JournalDao journalDao;

	/**
	 * method to save hourly pay salary
	 */
	public Long savePaysalaryHourly(PaySalaryVo paySalaryVo) {
		PaySalary paySalary = new PaySalary();
		SalaryType salaryType = salaryTypeDao.findSalaryTypeObject(PaySlipConstants.SALARY_TYPE_HOURLY);
		paySalary.setSalaryType(salaryType);
		SalaryApprovalStatus approvalStatus = approvalStatusDao.findStatus("Requested");
		paySalary.setStatus(approvalStatus);
		Branch branch = null;
		Boolean foundBranch = false;
		BigDecimal amount = BigDecimal.ZERO;
		for (PaySalaryHourlySalaryVo hourlySalaryVo : paySalaryVo.getHourlyWagesDetailsVos()) {
			Employee employee = employeeDao.findAndReturnEmployeeByCode(hourlySalaryVo.getEmployeeCode());
			if (!foundBranch) {
				branch = employee.getBranch();
				foundBranch = true;
			}
			amount = amount.add(hourlySalaryVo.getAmount());
			paySalary.getBranches().add(branch);
			paySalary.getDepartments().add(employee.getDepartmentEmployees());
			paySalary.getPaySalaryHourlyWages().add(setHourlyWages(hourlySalaryVo, employee, paySalary));
		}
		paySalary.setAmount(amount);
		Company company = companyDao.findActiveCompany(branch.getId());
		if (company == null)
			throw new ItemNotFoundException("No active company found.");
		paySalary.setCompany(company);
		if (paySalaryVo.getDate() != "")
			paySalary.setDate(DateFormatter.convertStringToDate(paySalaryVo.getDate()));
		paySalary = this.setJournalToPaySalary(paySalary, paySalaryVo);
		Long id = (Long) this.sessionFactory.getCurrentSession().save(paySalary);
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.JOURNAL, company);
		return id;
	}

	/**
	 * method to set hourly wages object
	 * 
	 * @param vo
	 * @param employee
	 * @param paySalary
	 * @return
	 */
	private PaySalaryHourlyWage setHourlyWages(PaySalaryHourlySalaryVo vo, Employee employee, PaySalary paySalary) {
		PaySalaryHourlyWage wage = new PaySalaryHourlyWage();
		wage.setEmployee(employee);
		employee.getPaySalaryHourlyWages().add(wage);
		wage.setAmount(vo.getAmount());
		wage.setHourlyRate(vo.getHourlyRate());
		wage.setTotalHours(vo.getTotalHours());
		wage.setPaySalary(paySalary);
		wage.setDate(DateFormatter.convertStringToDate(vo.getHourlyWageDate()));
		return wage;
	}

	/**
	 * method to save pay salary daily
	 */
	public Long savePaysalaryDaily(PaySalaryVo paySalaryVo) {
		PaySalary paySalary = new PaySalary();

		SalaryType salaryType = salaryTypeDao.findSalaryTypeObject(PaySlipConstants.SALARY_TYPE_DAILY);
		paySalary.setSalaryType(salaryType);
		SalaryApprovalStatus approvalStatus = approvalStatusDao.findStatus("Requested");
		paySalary.setStatus(approvalStatus);
		Branch branch = null;
		Boolean foundBranch = false;
		BigDecimal amount = BigDecimal.ZERO;
		for (PaySalaryDailySalaryVo vo : paySalaryVo.getDailyWageDetailsVos()) {
			Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
			if (!foundBranch) {
				branch = employee.getBranch();
				foundBranch = true;
			}
			amount = amount.add(vo.getAmount());
			paySalary.getBranches().add(branch);
			paySalary.getDepartments().add(employee.getDepartmentEmployees());
			paySalary.getPaySalaryDailyWages().add(setDailyWages(vo, employee, paySalary));
		}
		Company company = companyDao.findActiveCompany(branch.getId());
		if (company == null)
			throw new ItemNotFoundException("No active company found.");
		paySalary.setCompany(company);
		paySalary.setAmount(amount);
		if (paySalaryVo.getDate() != "")
			paySalary.setDate(DateFormatter.convertStringToDate(paySalaryVo.getDate()));
		paySalary = this.setJournalToPaySalary(paySalary, paySalaryVo);
		Long id = (Long) this.sessionFactory.getCurrentSession().save(paySalary);
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.JOURNAL, company);
		return id;
	}

	/**
	 * method to set daily wages object
	 * 
	 * @param vo
	 * @param employee
	 * @param paySalary
	 * @return
	 */
	private PaySalaryDailyWages setDailyWages(PaySalaryDailySalaryVo vo, Employee employee, PaySalary paySalary) {
		PaySalaryDailyWages dailyWages = new PaySalaryDailyWages();
		dailyWages.setEmployee(employee);
		dailyWages.setPaySalary(paySalary);
		employee.getPaySalaryDailyWages().add(dailyWages);
		dailyWages.setAmount(vo.getAmount());
		dailyWages.setDailyRate(vo.getDailyRate());
		dailyWages.setDate(DateFormatter.convertStringToDate(vo.getDailyWageDate()));
		return dailyWages;
	}

	/**
	 * method to save monthly pay salary
	 */
	public Long savePaysalaryMonthly(PaySalaryVo paySalaryVo) {
		PaySalary paySalary = new PaySalary();
		SalaryType salaryType = salaryTypeDao.findSalaryTypeObject(PaySlipConstants.SALARY_TYPE_MONTHLY);
		paySalary.setSalaryType(salaryType);
		SalaryApprovalStatus approvalStatus = approvalStatusDao.findStatus("Requested");
		paySalary.setStatus(approvalStatus);
		Branch branch = null;
		Boolean foundBranch = false;
		BigDecimal amount = BigDecimal.ZERO;
		if (paySalaryVo.getDate() != "")
			paySalary.setDate(DateFormatter.convertStringToDate(paySalaryVo.getDate()));
		for (PaySalaryEmployeeItemsVo employeeItemsVo : paySalaryVo.getEmployeeItemsVo()) {
			Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeItemsVo.getEmployeeCode());
			if (!foundBranch) {
				branch = employee.getBranch();
				foundBranch = true;
			}
			amount = amount.add(employeeItemsVo.getNetPay());
			paySalary.getBranches().add(branch);
			paySalary.getDepartments().add(employee.getDepartmentEmployees());
			paySalary.getPaySalaryMonthly().add(
					setMonthlyPaysalary(employeeItemsVo, employee, paySalary, paySalaryVo.getDate()));
		}
		Company company = companyDao.findActiveCompany(branch.getId());
		if (company == null)
			throw new ItemNotFoundException("No active company found.");
		paySalary.setCompany(company);
		paySalary.setAmount(amount);
		paySalary = this.setJournalToPaySalary(paySalary, paySalaryVo);
		Long id = (Long) this.sessionFactory.getCurrentSession().save(paySalary);
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.JOURNAL, company);
		return id;
	}

	/**
	 * method to set monthly pay salary
	 * 
	 * @param vo
	 * @param employee
	 * @param paySalary
	 * @return
	 */
	private PaySalaryEmployees setMonthlyPaysalary(PaySalaryEmployeeItemsVo vo, Employee employee, PaySalary paySalary,
			String date) {
		PaySalaryEmployees employeeItems = null;
		Boolean isEditLoan = false;
		Boolean isEditLAdvanceSalary = false;
		BigDecimal loan = BigDecimal.ZERO;
		BigDecimal advanceSalary = BigDecimal.ZERO;
		BigDecimal previouAsdvanceSalary = BigDecimal.ZERO;
		BigDecimal previousLoan = BigDecimal.ZERO;
		if (vo.getMonthlyId() != null) {
			employeeItems = findPaysSalaryMonthlyItems(vo.getMonthlyId());
		}
		if (employeeItems == null) {
			employeeItems = new PaySalaryEmployees();
			employeeItems.setEmployee(employee);
			employeeItems.setPaySalary(paySalary);
		} else {
			if (employeeItems.getLoan().compareTo(BigDecimal.ZERO) != 0) {
				isEditLoan = true;
				previousLoan = employeeItems.getLoan();
			}
			if (employeeItems.getAdvance().compareTo(BigDecimal.ZERO) != 0) {
				isEditLAdvanceSalary = true;
				previouAsdvanceSalary = employeeItems.getAdvance();
			}
			employeeItems.getOtherItems().clear();
			employeeItems.getPayslipItems().clear();
		}
		advanceSalary = vo.getAdvanceSalary();
		loan = vo.getLoan();
		if (vo.getPfApplicability().equalsIgnoreCase("yes"))
			employeeItems.setPfAplicability(true);
		else
			employeeItems.setPfAplicability(false);
		if (vo.getEsiApplicability().equalsIgnoreCase("no"))
			employeeItems.setEsiAplicability(false);
		else
			employeeItems.setEsiAplicability(true);
		employeeItems.setDate(DateFormatter.convertStringToDate(date));

		/*employeeItems.setBasic(vo.getBasic());
		employeeItems.setDa(vo.getDa());
		employeeItems.setHra(vo.getHra());
		employeeItems.setCca(vo.getCca());
		employeeItems.setConveyance(vo.getConveyance());*/

		employeeItems.setTotalWorkingDays(vo.getTotlaWorkingDays());
		employeeItems.setPerDayCalculation(vo.getPerDayCalculation());
		employeeItems.setGrossSalary(vo.getGrossSalary());
		employeeItems.setLopCount(vo.getLopCount());
		employeeItems.setLopDeduction(vo.getLopDeduction());
		employeeItems.setIncentive(vo.getIncentive());
		employeeItems.setEncashment(vo.getEncashment());
		employeeItems.setBonus(vo.getBonus());
		employeeItems.setIncrement(vo.getIncrement());
		employeeItems.setTotalEarnings(vo.getTotalEarnings());
		employeeItems.setPf(vo.getPf());
		employeeItems.setEsi(vo.getEsi());
		employeeItems.setLwf(vo.getLwf());
		employeeItems.setProfessionalTax(vo.getProfessionalTaxs());
		employeeItems.setTds(vo.getTax());
		employeeItems.setOtherDeduction(vo.getDeductions());
		employeeItems.setAdvance(vo.getAdvanceSalary());

		updateLoanAndAdvanceSalary(loan, previousLoan, advanceSalary, previouAsdvanceSalary, employee,
				paySalary.getDate(), isEditLoan, isEditLAdvanceSalary);

		employeeItems.setLoan(vo.getLoan());
		employeeItems.setTotalDeductions(vo.getTotalDeduction());
		employeeItems.setNetPay(vo.getNetPay());
		employeeItems.setPfEmployer(vo.getPfEmployer());
		employeeItems.setEsiEmployer(vo.getEsiEmployer());
		employeeItems.setCtc(vo.getCtc());

		for (String item : vo.getOtherSalaryItemsList()) {
			employeeItems.getOtherItems().add(setOtherAllowances(item, employeeItems));
		}
		for (String item : vo.getSalaryItemsList()) {
			employeeItems.getPayslipItems().add(setPayslipIem(item, employeeItems));
		}
		return employeeItems;
	}

	private PaysalaryEmployeePayslipItem setPayslipIem(String item, PaySalaryEmployees employeeItems) {
		PaysalaryEmployeePayslipItem payslipItem = new PaysalaryEmployeePayslipItem();
		Map<String, BigDecimal> splittedResult = StringSplitter.splitWithSpecialCharacters(item);
		PaySlipItem paySlip = null;
		for (Map.Entry<String, BigDecimal> entry : splittedResult.entrySet()) {
			paySlip = paySlipItemDao.findPaySlipItem(entry.getKey());
			payslipItem.setItem(paySlip);
			payslipItem.setAmount(entry.getValue());
		}
		payslipItem.setEmployeeItem(employeeItems);
		return payslipItem;
	}

	private void updateLoanAndAdvanceSalary(BigDecimal loanAmount, BigDecimal previousLoanAmount,
			BigDecimal advanceSalaryAmount, BigDecimal previousAdvanceSalaryAmount, Employee employee, Date date,
			Boolean isEditLoan, Boolean isEditLAdvanceSalary) {

		date = DateFormatter.findLastDayOfPreviousMonth(date);
		if (loanAmount.compareTo(BigDecimal.ZERO) != 0 || previousLoanAmount.compareTo(BigDecimal.ZERO) != 0) {
			List<Loan> loans = loanDao.findApprovedLoanForSalary(DateFormatter.convertDateToString(date), employee);
			if (loans.size() > 0) {
				if (loanAmount.compareTo(BigDecimal.ZERO) != 0)
					loanAmount = loanAmount.divide(new BigDecimal(loans.size()), 2, RoundingMode.HALF_EVEN);
				if (previousLoanAmount.compareTo(BigDecimal.ZERO) != 0)
					previousLoanAmount = previousLoanAmount.divide(new BigDecimal(loans.size()), 2,
							RoundingMode.HALF_EVEN);
				for (Loan loan : loans) {
					loan.setBalanceAmount(loan.getBalanceAmount().add(previousLoanAmount));
					loan.setBalanceAmount(loan.getBalanceAmount().subtract(loanAmount));
					if (loan.getPendingInstallments() != 0) {
						loan.setRepaymentAmount(loan.getBalanceAmount().divide(
								new BigDecimal(loan.getPendingInstallments()), 2, RoundingMode.HALF_EVEN));
						if (!isEditLoan)
							loan.setPendingInstallments(loan.getPendingInstallments() - 1);
					} else {
						if (!isEditLoan)
							loan.setNoOfInstallments(loan.getNoOfInstallments() + 1);
						loan.setRepaymentAmount(loan.getBalanceAmount());
					}
					if (loan.getBalanceAmount().compareTo(BigDecimal.ZERO) <= 0) {
						loan.setStatus(loanStatusDao.findLoanStatus(StatusConstants.CLOSED));
					}
					loanDao.updateLoanObject(loan);
				}
			}
		}

		if (advanceSalaryAmount.compareTo(BigDecimal.ZERO) != 0
				|| previousAdvanceSalaryAmount.compareTo(BigDecimal.ZERO) != 0) {
			List<AdvanceSalary> advanceSalaries = advanceSalaryDao.findApprovedAdvanceSalaryForSalary(
					DateFormatter.convertDateToString(date), employee);
			if (advanceSalaries.size() > 0) {
				if (advanceSalaryAmount.compareTo(BigDecimal.ZERO) != 0)
					advanceSalaryAmount = advanceSalaryAmount.divide(new BigDecimal(advanceSalaries.size()), 2,
							RoundingMode.HALF_EVEN);
				if (previousAdvanceSalaryAmount.compareTo(BigDecimal.ZERO) != 0)
					previousAdvanceSalaryAmount = previousAdvanceSalaryAmount.divide(
							new BigDecimal(advanceSalaries.size()), 2, RoundingMode.HALF_EVEN);
				for (AdvanceSalary advanceSalary : advanceSalaries) {
					advanceSalary.setBalanceAmount(advanceSalary.getBalanceAmount().add(previousAdvanceSalaryAmount));
					advanceSalary.setBalanceAmount(advanceSalary.getBalanceAmount().subtract(advanceSalaryAmount));
					if (advanceSalary.getPendingInstallments() != 0) {
						advanceSalary.setRepaymentAmount(advanceSalary.getBalanceAmount().divide(
								new BigDecimal(advanceSalary.getPendingInstallments()), 2, RoundingMode.HALF_EVEN));
						if (!isEditLAdvanceSalary)
							advanceSalary.setPendingInstallments(advanceSalary.getPendingInstallments() - 1);
					} else {
						if (!isEditLAdvanceSalary)
							advanceSalary.setNoOfInstallments(advanceSalary.getNoOfInstallments() + 1);
						advanceSalary.setRepaymentAmount(advanceSalary.getBalanceAmount());
					}
					if (advanceSalary.getBalanceAmount().compareTo(BigDecimal.ZERO) <= 0) {
						advanceSalary.setStatus(advanceSalaryStatusDao.findAdvanceSalaryStatus(StatusConstants.CLOSED));
					}
					advanceSalaryDao.updateAdvanceSalaryObject(advanceSalary);
				}
			}
		}
	}

	private PaySalaryEmployees findPaysSalaryMonthlyItems(Long id) {
		return (PaySalaryEmployees) this.sessionFactory.getCurrentSession().createCriteria(PaySalaryEmployees.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * method to set pay salary employee items
	 * 
	 * @param list
	 * @param employeeItems
	 * @return
	 */
	private PaySalaryEmployeeItems setOtherAllowances(String item, PaySalaryEmployees employeeItems) {
		PaySalaryEmployeeItems allowanceItems = new PaySalaryEmployeeItems();
		Map<String, BigDecimal> splittedResult = StringSplitter.splitWithSpecialCharacters(item);
		PaySalaryOtherAllowanceItems otherAllowanceItems = null;
		for (Map.Entry<String, BigDecimal> entry : splittedResult.entrySet()) {
			otherAllowanceItems = findOtherAlowance(entry.getKey());
			if (otherAllowanceItems == null)
				saveNewAllowance(entry.getKey());
			otherAllowanceItems = findOtherAlowance(entry.getKey());
			allowanceItems.setItem(otherAllowanceItems);
			allowanceItems.setAmount(entry.getValue());
		}
		allowanceItems.setPaySalaryEmployees(employeeItems);
		return allowanceItems;
	}

	/**
	 * method to save new allowance item
	 * 
	 * @param name
	 */
	private void saveNewAllowance(String name) {
		PaySalaryOtherAllowanceItems allowanceItems = new PaySalaryOtherAllowanceItems();
		allowanceItems.setName(name);
		this.sessionFactory.getCurrentSession().save(allowanceItems);
	}

	/**
	 * method to find other allowances by name
	 * 
	 * @param name
	 * @return
	 */
	private PaySalaryOtherAllowanceItems findOtherAlowance(String name) {
		return (PaySalaryOtherAllowanceItems) this.sessionFactory.getCurrentSession()
				.createCriteria(PaySalaryOtherAllowanceItems.class).add(Restrictions.eq("name", name)).uniqueResult();
	}

	/**
	 * method to find other allowances by id
	 * 
	 * @param id
	 * @return
	 */
	private PaySalaryOtherAllowanceItems findOtherAlowance(Long id) {
		return (PaySalaryOtherAllowanceItems) this.sessionFactory.getCurrentSession()
				.createCriteria(PaySalaryOtherAllowanceItems.class).add(Restrictions.eq("id", id)).uniqueResult();
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PaySalaryVo> listPaySalaries(String employeeCode) {
		List<PaySalaryVo> salaryVos = new ArrayList<PaySalaryVo>();
		// Employee employee =
		// employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySalary.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		/*
		 * Criterion creatdBy = Restrictions.eq("createdBy",
		 * employee.getUser().getUserName()); Criterion byEmployee =
		 * Restrictions.eq("employee", employee); LogicalExpression expression =
		 * Restrictions.or(creatdBy, byEmployee); criteria.add(expression);
		 */
		List<PaySalary> paySalaries = criteria.list();
		for (PaySalary paySalary : paySalaries) {
			salaryVos.add(createPaySalaryVo(paySalary));
		}
		return salaryVos;
	}

	private PaySalaryVo createPaySalaryVo(PaySalary paySalary) {
		PaySalaryVo vo = new PaySalaryVo();
		vo.setId(paySalary.getId());
		vo.setCompanyId(paySalary.getCompany().getId());
		SalaryApprovalStatus approvalStatus = paySalary.getStatus();
		vo.setStatus(approvalStatus.getStatus());
		vo.setStatusId(approvalStatus.getId());
		for (Department department : paySalary.getDepartments()) {
			vo.getDepartments().add(department.getName());
			vo.getDepartmentIds().add(department.getId());
		}
		for (Branch branch : paySalary.getBranches()) {
			vo.getBranches().add(branch.getBranchName());
			vo.getBranchIds().add(branch.getId());
		}
		vo.setSalaryType(paySalary.getSalaryType().getId());
		vo.setSalaryTypeType(paySalary.getSalaryType().getType());
		if (paySalary.getDate() != null)
			vo.setDate(DateFormatter.convertDateToString(paySalary.getDate()));
		return vo;
	}

	/**
	 * method to find daily pay salary by id
	 */
	public PaySalaryVo findDailyPaySalary(Long id) {
		PaySalary paySalary = findPaySalary(id);
		if (paySalary == null)
			throw new ItemNotFoundException("Paysalary Not Exists.");
		PaySalaryVo vo = new PaySalaryVo();
		for (PaySalaryDailyWages dailyWages : paySalary.getPaySalaryDailyWages()) {
			vo.getDailyWageDetailsVos().add(createDailySalaryVo(dailyWages, paySalary));
		}
		vo.setCompanyId(paySalary.getCompany().getId());
		vo.setSalaryTypeType(paySalary.getSalaryType().getType());
		vo.setEdit(true);
		vo.setDaily(true);
		vo.setId(paySalary.getId());
		return vo;
	}

	/**
	 * method to create pay salary daily wages vo
	 * 
	 * @param dailyWages
	 * @return
	 */
	private PaySalaryDailySalaryVo createDailySalaryVo(PaySalaryDailyWages dailyWages, PaySalary paySalary) {
		PaySalaryDailySalaryVo vo = new PaySalaryDailySalaryVo();
		Payment payment = paySalary.getPayment();
		if (payment != null) {
			vo.setCheckBoxValue(dailyWages.getId() + "!!!" + payment.getId());
			vo.setIsPaid(true);
		} else
			vo.setCheckBoxValue(dailyWages.getId() + "!!!0");
		Employee employee = dailyWages.getEmployee();
		vo.setDailySalaryId(dailyWages.getId());
		vo.setAmount(dailyWages.getAmount());
		vo.setDailyRate(dailyWages.getDailyRate());
		vo.setDailyWageDate(DateFormatter.convertDateToString(paySalary.getDate()));
		vo.setEmployeeCode(dailyWages.getEmployee().getEmployeeCode());
		vo.setEmpName(employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname());
		return vo;
	}

	/**
	 * method to find hourly pay salary by id
	 */
	public PaySalaryVo findHourlyPaySalary(Long id) {
		PaySalary paySalary = findPaySalary(id);
		if (paySalary == null)
			throw new ItemNotFoundException("Paysalary Not Exists.");
		PaySalaryVo vo = new PaySalaryVo();
		for (PaySalaryHourlyWage hourlyWage : paySalary.getPaySalaryHourlyWages()) {
			vo.getHourlyWagesDetailsVos().add(createHourlySalaryVo(hourlyWage, paySalary));
		}
		vo.setCompanyId(paySalary.getCompany().getId());
		vo.setEdit(true);
		vo.setHourly(true);
		vo.setSalaryTypeType(paySalary.getSalaryType().getType());
		vo.setId(paySalary.getId());
		return vo;
	}

	/**
	 * method to create pay salary hourly vo
	 * 
	 * @param hourlyWage
	 * @param paySalary
	 * @return
	 */
	private PaySalaryHourlySalaryVo createHourlySalaryVo(PaySalaryHourlyWage hourlyWage, PaySalary paySalary) {
		PaySalaryHourlySalaryVo vo = new PaySalaryHourlySalaryVo();
		Payment payment = paySalary.getPayment();
		if (payment != null) {
			vo.setCheckBoxValue(hourlyWage.getId() + "!!!" + payment.getId());
			vo.setIsPaid(true);
		} else
			vo.setCheckBoxValue(hourlyWage.getId() + "!!!0");
		vo.setHourlySalaryId(hourlyWage.getId());
		Employee employee = hourlyWage.getEmployee();
		vo.setAmount(hourlyWage.getAmount());
		vo.setEmployeeCode(employee.getEmployeeCode());
		vo.setEmployeeName(employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname());
		vo.setHourlyWageDate(DateFormatter.convertDateToString(hourlyWage.getDate()));
		vo.setHourlyRate(hourlyWage.getHourlyRate());
		vo.setTotalHours(hourlyWage.getTotalHours());
		return vo;
	}

	/**
	 * method to find monthly pay salary by id
	 */
	public PaySalaryVo findMonthlyPaySalary(Long id) {
		PaySalary paySalary = findPaySalary(id);
		if (paySalary == null)
			throw new ItemNotFoundException("Paysalary Not Exists.");
		PaySalaryVo vo = new PaySalaryVo();
		vo.setDate(DateFormatter.convertDateToString(paySalary.getDate()));
		for (PaySalaryEmployees monthlyWage : paySalary.getPaySalaryMonthly()) {
			vo.getEmployeeItemsVo().add(createMonthlyPaySalaryVo(monthlyWage));
			vo.getBranches().add(monthlyWage.getEmployee().getBranch().getBranchName());
			vo.getDepartments().add(monthlyWage.getEmployee().getDepartmentEmployees().getName());
		}

		List<ExtraPaySlipItemVo> extraPaySlipItemVos = extraPayslipItemDao.findAllExtraPaySlipItem();
		if (extraPaySlipItemVos != null && extraPaySlipItemVos.size() > 0) {
			for (ExtraPaySlipItemVo extraPaySlipItemVo : extraPaySlipItemVos) {
				vo.getExtraPayslipItems().add(extraPaySlipItemVo.getTitle());
			}
		}
		
		List<PaySlipItemVo> paysSlips = new ArrayList<PaySlipItemVo>();
		paysSlips = paySlipItemDao.findAllPaySlipItem();
		for (PaySlipItemVo itemVo : paysSlips) {
			vo.getPaySlipItems().add(itemVo.getTitle());
		}

		/*for (PaySalaryEmployeeItemsVo itemsVo : vo.getEmployeeItemsVo()) {
			for (Map.Entry<String, BigDecimal> entry : itemsVo.getOtherSalaryItems().entrySet()) {
			}
		}*/
		vo.setCompanyId(paySalary.getCompany().getId());
		vo.setId(paySalary.getId());
		vo.setMonthly(true);
		vo.setSalaryTypeType(paySalary.getSalaryType().getType());
		vo.setEdit(true);
		return vo;
	}

	/**
	 * method to create monthly vo
	 * 
	 * @param paySalary
	 * @return
	 */
	private PaySalaryEmployeeItemsVo createMonthlyPaySalaryVo(PaySalaryEmployees paySalary) {
		PaySalaryEmployeeItemsVo vo = new PaySalaryEmployeeItemsVo();
		Payment payment = paySalary.getPayment();
		if (payment != null) {
			vo.setCheckBoxValue(paySalary.getId() + "!!!" + payment.getId());
			vo.setIsPaid(true);
		} else
			vo.setCheckBoxValue(paySalary.getId() + "!!!0");
		Employee employee = paySalary.getEmployee();
		vo.setEmployeeCode(employee.getEmployeeCode());
		vo.setName(employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname());
		vo.setDesignation(employee.getEmployeeDesignation().getDesignation());
		vo.setStatus(employee.getEmployeeStatus().getName());
		vo.setMonthlyId(paySalary.getId());
		vo.setBranch(employee.getBranch().getBranchName());
		vo.setDepartment(employee.getDepartmentEmployees().getName());
		if (paySalary.getPfAplicability() == true)
			vo.setPfApplicability("Yes");
		else
			vo.setPfApplicability("No");
		if (paySalary.getEsiAplicability() == true)
			vo.setEsiApplicability("Yes");
		else
			vo.setEsiApplicability("No");
		vo.setBasic(paySalary.getBasic());
		vo.setDa(paySalary.getDa());
		vo.setHra(paySalary.getHra());
		vo.setCca(paySalary.getCca());
		vo.setPerDayCalculation(paySalary.getPerDayCalculation());
		vo.setTotlaWorkingDays(paySalary.getTotalWorkingDays());
		vo.setGrossSalary(paySalary.getGrossSalary());
		vo.setConveyance(paySalary.getConveyance());
		vo.setLopCount(paySalary.getLopCount());
		vo.setLopDeduction(paySalary.getLopDeduction());
		vo.setIncentive(paySalary.getIncentive());
		vo.setEncashment(paySalary.getEncashment());
		vo.setBonus(paySalary.getBonus());
		vo.setIncrement(paySalary.getIncrement());
		vo.setTotalEarnings(paySalary.getTotalEarnings());
		vo.setPf(paySalary.getPf());
		vo.setEsi(paySalary.getEsi());
		vo.setLwf(paySalary.getLwf());
		vo.setTax(paySalary.getTds());
		vo.setDeductions(paySalary.getOtherDeduction());
		vo.setLoan(paySalary.getLoan());
		vo.setAdvanceSalary(paySalary.getAdvance());
		vo.setTotalDeduction(paySalary.getTotalDeductions());
		vo.setNetPay(paySalary.getNetPay());
		vo.setPfEmployer(paySalary.getPfEmployer());
		vo.setEsiEmployer(paySalary.getEsiEmployer());
		vo.setCtc(paySalary.getCtc());
		
		
		Map<String, BigDecimal> otherSalaryItems = new HashMap<String, BigDecimal>();
		List<String> otherSalaryItemNames = new ArrayList<String>();
		String title=null;
		for (PaySalaryEmployeeItems items : paySalary.getOtherItems()) {
			 title = items.getItem().getName();
			otherSalaryItemNames.add(title);
			otherSalaryItems.put(title, items.getAmount());
		}
		vo.setOtherSalaryItemsList(otherSalaryItemNames);
		Map<String, BigDecimal> salaryItems = new HashMap<String, BigDecimal>();
		List<String> salaryItemNames = new ArrayList<String>();
		for (PaysalaryEmployeePayslipItem items : paySalary.getPayslipItems()) {
			 title = items.getItem().getTitle();
			salaryItemNames.add(title);
			salaryItems.put(title, items.getAmount());
		}
		System.out.println(salaryItems);
		vo.setSalaryItemsList(salaryItemNames);
		vo.setItems(salaryItems);
		vo.setOtherSalaryItems(otherSalaryItems);
		return vo;
	}

	/**
	 * method to find pay salary by id
	 */
	public PaySalary findPaySalary(Long id) {
		return (PaySalary) this.sessionFactory.getCurrentSession().createCriteria(PaySalary.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * method to update pay salary daily
	 */
	public void updateDailyPaySalary(PaySalaryVo paySalaryVo) {
		PaySalary paySalary = findPaySalary(paySalaryVo.getId());
		if (paySalary == null)
			throw new ItemNotFoundException("Paysalary Not Exist.");
		BigDecimal amount = BigDecimal.ZERO;
		for (PaySalaryDailyWages dailyWages : paySalary.getPaySalaryDailyWages()) {
			for (PaySalaryDailySalaryVo vo : paySalaryVo.getDailyWageDetailsVos()) {
				if (dailyWages.getId() == vo.getDailySalaryId()) {
					dailyWages.setAmount(vo.getAmount());
					dailyWages.setDailyRate(vo.getDailyRate());
				}
				amount = amount.add(vo.getAmount());
			}
		}
		paySalary.setAmount(amount);
		paySalary = this.setJournalToPaySalary(paySalary, paySalaryVo);
		this.sessionFactory.getCurrentSession().merge(paySalary);
	}

	/**
	 * method to update hourly salary
	 */
	public void updateHourlyPaySalary(PaySalaryVo paySalaryVo) {
		PaySalary paySalary = findPaySalary(paySalaryVo.getId());
		if (paySalary == null)
			throw new ItemNotFoundException("Paysalary Not Exist.");
		BigDecimal amount = BigDecimal.ZERO;
		for (PaySalaryHourlyWage hourlyWage : paySalary.getPaySalaryHourlyWages()) {
			for (PaySalaryHourlySalaryVo vo : paySalaryVo.getHourlyWagesDetailsVos()) {
				if (hourlyWage.getId() == vo.getHourlySalaryId()) {
					hourlyWage.setAmount(vo.getAmount());
					hourlyWage.setHourlyRate(vo.getHourlyRate());
				}
				amount = amount.add(vo.getAmount());
			}
		}
		paySalary.setAmount(amount);
		paySalary = this.setJournalToPaySalary(paySalary, paySalaryVo);
		this.sessionFactory.getCurrentSession().merge(paySalary);
	}

	/**
	 * method to delete pay salary
	 */
	public void deletePaysalary(Long id) {
		PaySalary paySalary = findPaySalary(id);
		if (paySalary == null)
			throw new ItemNotFoundException("Paysalary Not Exist.");
		this.sessionFactory.getCurrentSession().delete(paySalary);
	}

	/**
	 * method to update monthly pay salary
	 */
	public void updateMonthlyPaySalary(PaySalaryVo paySalaryVo) {
		Employee employee = new Employee();
		PaySalary paySalary = findPaySalary(paySalaryVo.getId());
		BigDecimal amount = BigDecimal.ZERO;
		if (paySalary == null)
			throw new ItemNotFoundException("Paysalary Not Exist.");
		for (PaySalaryEmployeeItemsVo vo : paySalaryVo.getEmployeeItemsVo()) {
			paySalary.getPaySalaryMonthly().add(setMonthlyPaysalary(vo, employee, paySalary, paySalaryVo.getDate()));
			amount = amount.add(vo.getNetPay());
		}
		paySalary.setAmount(amount);
		paySalary = this.setJournalToPaySalary(paySalary, paySalaryVo);
		this.sessionFactory.getCurrentSession().merge(paySalary);
	}

	@SuppressWarnings("unchecked")
	public List<PaySalaryHourlySalaryVo> getEmployeeHourlyWage(String employeeCode, String startDate, String endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exists.");
		// SalaryType salaryType =
		// salaryTypeDao.findSalaryTypeObject(PaySlipConstants.SALARY_TYPE_HOURLY);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySalaryHourlyWage.class);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("date", DateFormatter.convertStringToDate(startDate))).add(
				Restrictions.le("date", DateFormatter.convertStringToDate(endDate)));
		List<PaySalaryHourlyWage> hourlyWages = criteria.list();
		List<PaySalaryHourlySalaryVo> vos = new ArrayList<PaySalaryHourlySalaryVo>();
		for (PaySalaryHourlyWage hourlyWage : hourlyWages) {
			vos.add(createHourlySalaryVo(hourlyWage, hourlyWage.getPaySalary()));
		}
		return vos;
	}

	@SuppressWarnings("unchecked")
	public List<PaySalaryDailySalaryVo> getEmployeeDailyWage(String employeeCode, String startDate, String endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exists.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySalaryDailyWages.class);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("date", DateFormatter.convertStringToDate(startDate))).add(
				Restrictions.le("date", DateFormatter.convertStringToDate(endDate)));
		List<PaySalaryDailyWages> dailyWages = criteria.list();
		List<PaySalaryDailySalaryVo> vos = new ArrayList<PaySalaryDailySalaryVo>();
		for (PaySalaryDailyWages dailyWage : dailyWages) {
			vos.add(createDailySalaryVo(dailyWage, dailyWage.getPaySalary()));
		}
		return vos;
	}

	@SuppressWarnings("unchecked")
	public List<PaySalaryEmployeeItemsVo> getEmployeeMonthlyWage(String employeeCode, String startDate, String endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exists.");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySalaryEmployees.class);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.ge("date", DateFormatter.convertStringToDate(startDate))).add(
				Restrictions.le("date", DateFormatter.convertStringToDate(endDate)));
		List<PaySalaryEmployees> salaries = criteria.list();
		List<PaySalaryEmployeeItemsVo> vos = new ArrayList<PaySalaryEmployeeItemsVo>();
		for (PaySalaryEmployees salary : salaries) {
			vos.add(createMonthlyPaySalaryVo(salary));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PaySalaryVo> listPaySalariesByCompany(Long companyId) {
		List<PaySalaryVo> salaryVos = new ArrayList<PaySalaryVo>();
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company not found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySalary.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("company", company));
		List<PaySalary> paySalaries = criteria.list();
		for (PaySalary paySalary : paySalaries) {
			salaryVos.add(createPaySalaryVo(paySalary));
		}
		return salaryVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PaySalaryVo> findSalariesByStatus(String stats, Long companyId) {
		List<PaySalaryVo> salaryVos = new ArrayList<PaySalaryVo>();
		SalaryApprovalStatus status = approvalStatusDao.findStatus(stats);
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company not found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySalary.class);
		criteria.add(Restrictions.eq("status", status));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("company", company));
		List<PaySalary> paySalaries = criteria.list();
		for (PaySalary paySalary : paySalaries) {
			salaryVos.add(createPaySalaryVo(paySalary));
		}
		return salaryVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<PaySalaryVo> findUnpaidSalaries(Long companyId) {
		List<PaySalaryVo> salaryVos = new ArrayList<PaySalaryVo>();
		SalaryApprovalStatus status = approvalStatusDao.findStatus(AccountsConstants.PAID_STATUS);
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company not found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PaySalary.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.not(Restrictions.eq("status", status)));
		criteria.add(Restrictions.eq("company", company));
		List<PaySalary> paySalaries = criteria.list();
		for (PaySalary paySalary : paySalaries) {
			salaryVos.add(createPaySalaryVo(paySalary));
		}
		return salaryVos;
	}

	public void updateSalaryStatus(Long salaryId, Long statusId) {
		PaySalary salary = this.findPaySalary(salaryId);
		if (salary == null)
			throw new ItemNotFoundException("Salary Not Found.");
		SalaryApprovalStatus approvalStatus = approvalStatusDao.findStatus(statusId);
		if (approvalStatus == null)
			throw new ItemNotFoundException("Status Not Found");
		salary.setStatus(approvalStatus);
		this.sessionFactory.getCurrentSession().merge(salary);
	}

	public void updateSalary(Long ledgerId, Long salaryId) {
		Ledger account = ledgerDao.findLedgerObjectById(ledgerId);
		PaySalary paySalary = this.findPaySalary(salaryId);
		Set<Branch> branches = paySalary.getBranches();
		Branch branch = null;
		for (Branch branch2 : branches)
			branch = branch2;
		Company company = companyDao.findActiveCompany(branch.getId());
		String voucherNumber = paymentDao.getPaymentNumberForCompany(company.getId());
		Payment payment = new Payment();
		payment.setCompany(company);
		payment.setPaymentNumber(voucherNumber);
		payment.setPaymentDate(paySalary.getDate());
		payment.setNarration("Salary for " + branch.getBranchName());
		ErpCurrencyVo currencyVo = null;
		currencyVo = erpCurrencyDao.findCurrencyForDate(payment.getPaymentDate(), payment.getCompany().getCurrency()
				.getBaseCurrency());
		BigDecimal zero = new BigDecimal("0.00");
		BigDecimal total = zero;
		if (!paySalary.getPaySalaryMonthly().isEmpty()) {
			for (PaySalaryEmployees salary : paySalary.getPaySalaryMonthly()) {
				Ledger ledger = salary.getEmployee().getLedger();
				PaymentEntries entries = this.setAttributesToPaymentEntry(ledger, salary.getNetPay(), payment,
						currencyVo);
				total = total.add(salary.getNetPay());
				payment.getEntries().add(entries);
			}
		} else if (!paySalary.getPaySalaryHourlyWages().isEmpty()) {
			for (PaySalaryHourlyWage salary : paySalary.getPaySalaryHourlyWages()) {
				Ledger ledger = salary.getEmployee().getLedger();
				payment.getEntries().add(
						this.setAttributesToPaymentEntry(ledger, salary.getAmount(), payment, currencyVo));
				total = total.add(salary.getAmount());
			}
		} else if (!paySalary.getPaySalaryDailyWages().isEmpty()) {
			for (PaySalaryDailyWages salary : paySalary.getPaySalaryDailyWages()) {
				Ledger ledger = salary.getEmployee().getLedger();
				payment.getEntries().add(
						this.setAttributesToPaymentEntry(ledger, salary.getAmount(), payment, currencyVo));
				total = total.add(salary.getAmount());
			}
		}
		BigDecimal plusAmount = zero;
		BigDecimal minusAmount = zero;
		PaymentEntries byEntry = new PaymentEntries();
		Transactions byTransaction = new Transactions();
		byTransaction.setPaymentEntries(byEntry);
		byTransaction.setCompany(payment.getCompany());
		byTransaction.setLedger(account);
		byEntry.setTransactions(byTransaction);
		byEntry.setLedger(account);
		if (currencyVo != null) {
			byEntry.setDebit(zero);
			byEntry.setCredit(total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
			plusAmount = zero;
			minusAmount = total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
		} else {
			byEntry.setCredit(total);
			byEntry.setDebit(zero);
			plusAmount = total;
			minusAmount = zero;
		}
		byEntry.getTransactions().setDate(payment.getPaymentDate());
		byEntry.getTransactions().setMinusAmount(minusAmount);
		byEntry.getTransactions().setPlusAmount(plusAmount);
		byEntry.setPayment(payment);
		payment.getEntries().add(byEntry);
		if (currencyVo != null) {
			payment.setAmount(total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
		} else {
			payment.setAmount(total);
		}
		payment.setPaySalary(paySalary);
		paySalary.setPayment(payment);
		paySalary.setStatus(approvalStatusDao.findStatus(AccountsConstants.PAID_STATUS));
		this.sessionFactory.getCurrentSession().merge(paySalary);
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PAYMENT, company);
	}

	private PaymentEntries setAttributesToPaymentEntry(Ledger ledger, BigDecimal amount, Payment payment,
			ErpCurrencyVo currencyVo) {
		PaymentEntries entries = new PaymentEntries();
		entries.setLedger(ledger);
		BigDecimal zero = new BigDecimal("0.00");
		String accountType = ledger.getLedgerGroup().getAccountType().getType();
		BigDecimal plusAmount = zero;
		BigDecimal minusAmount = zero;

		Transactions byTransaction = new Transactions();
		byTransaction.setPaymentEntries(entries);
		byTransaction.setCompany(payment.getCompany());
		byTransaction.setLedger(ledger);
		entries.setTransactions(byTransaction);
		entries.setLedger(ledger);
		if (currencyVo != null) {
			entries.setDebit(amount.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
			entries.setCredit(zero);

			if (accountType.contains("Expense")) {
				plusAmount = amount.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				minusAmount = zero;
			}

		} else {
			entries.setDebit(amount);
			entries.setCredit(zero);

			if (accountType.contains("Expense")) {
				plusAmount = amount;
				minusAmount = zero;
			}
		}
		entries.getTransactions().setDate(payment.getPaymentDate());
		entries.getTransactions().setMinusAmount(minusAmount);
		entries.getTransactions().setPlusAmount(plusAmount);
		entries.setPayment(payment);
		return entries;
	}

	private PaySalary setJournalToPaySalary(PaySalary paySalary, PaySalaryVo paySalaryVo) {
		Company company = paySalary.getCompany();
		Ledger byLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.SALARY_LEDGER, company.getId());
		Ledger toLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.SALARY_PAYABLE_LEDGER, company.getId());
		ErpCurrencyVo currencyVo = erpCurrencyDao.findCurrencyForDate(paySalary.getDate(), company.getCurrency()
				.getBaseCurrency());
		BigDecimal grandTotal = paySalary.getAmount();
		if (currencyVo != null) {
			grandTotal = grandTotal.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
		}
		Journal journal = paySalary.getJournal();
		if (journal == null) {
			journal = new Journal();
			journal.setJournalNumber(journalDao.getJournalNumberForCompany(paySalary.getCompany().getId()));
			journal.setCompany(paySalary.getCompany());
			journal.setIsDependant(true);
		} else {
			journal.getEntries().clear();
		}

		JournalVo journalVo = new JournalVo();
		journalVo.setDate(DateFormatter.convertDateToString(paySalary.getDate()));
		journalVo.setAmount(grandTotal);

		JournalItemsVo byItem = new JournalItemsVo();
		byItem.setAccTypeId(byLedger.getId());
		byItem.setDebitAmnt(grandTotal);
		journalVo.getEntries().add(byItem);

		JournalItemsVo toItem = new JournalItemsVo();
		toItem.setAccTypeId(toLedger.getId());
		toItem.setCreditAmnt(grandTotal);
		journalVo.getEntries().add(toItem);

		journal = journalDao.setAttributesForJournal(journal, journalVo, false);
		journal.setNarration("Salary processed for " + company.getParent().getBranchName());
		journal.setPaySalary(paySalary);
		paySalary.setJournal(journal);
		return paySalary;
	}

	public void updatePaySalaryPay(PayPaysalaryVo vo) {
		PaySalary paySalary = this.findPaySalary(vo.getPaysalaryId());
		Branch branch = null;
		Set<Branch> branchs = paySalary.getBranches();
		for (Branch bran : branchs)
			branch = bran;
		Map<Long, Payment> map = new HashMap<Long, Payment>();
		Company company = companyDao.findActiveCompany(branch.getId());
		Ledger byLedger = ledgerDao.findLedgerObjectByName(AccountsConstants.SALARY_PAYABLE_LEDGER, company.getId());
		Ledger toLedger = ledgerDao.findLedgerObjectById(vo.getLedgerId());
		String voucherNumber = paymentDao.getPaymentNumberForCompany(company.getId());
		Payment newPayment = new Payment();
		newPayment.setCompany(company);
		newPayment.setPaymentNumber(voucherNumber);
		newPayment.setPaymentDate(DateFormatter.convertStringToDate(vo.getDate()));
		newPayment.setNarration("Salary for " + branch.getBranchName());
		ErpCurrencyVo currencyVo = null;
		currencyVo = erpCurrencyDao.findCurrencyForDate(newPayment.getPaymentDate(), newPayment.getCompany()
				.getCurrency().getBaseCurrency());
		BigDecimal zero = new BigDecimal("0.00");
		BigDecimal total = zero;
		String type = paySalary.getSalaryType().getType();
		if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_DAILY)) {
			for (String value : vo.getCheckedValues()) {
				String[] split = this.splitString(value);
				Long id = Long.parseLong(split[0]);
				Long paymentId = Long.parseLong(split[1]);
				if (paymentId == 0) {
					PaySalaryDailyWages salary = (PaySalaryDailyWages) this.sessionFactory.getCurrentSession().get(
							PaySalaryDailyWages.class, id);
					total = total.add(salary.getAmount());
					salary.setPayment(newPayment);
					newPayment.getPaySalaryDailyWages().add(salary);
				}
			}
			for (String value : vo.getUnCheckedValues()) {
				String[] split = this.splitString(value);
				Long id = Long.parseLong(split[0]);
				Long paymentId = Long.parseLong(split[1]);
				if (paymentId != 0) {
					// remove from payment
					Payment payment = map.get(paymentId);
					PaySalaryDailyWages salary = (PaySalaryDailyWages) this.sessionFactory.getCurrentSession().get(
							PaySalaryDailyWages.class, id);
					if (payment == null) {
						payment = paymentDao.findPaymentObjectById(paymentId);
					}
					salary.setPayment(null);
					paySalary.getPaySalaryDailyWages().add(salary);
					payment.setAmount(payment.getAmount().subtract(salary.getAmount()));
					payment.getPaySalaryDailyWages().remove(salary);
					map.put(paymentId, payment);
				}
			}
		} else if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_HOURLY)) {
			for (String value : vo.getCheckedValues()) {
				String[] split = this.splitString(value);
				Long id = Long.parseLong(split[0]);
				Long paymentId = Long.parseLong(split[1]);
				if (paymentId == 0) {
					PaySalaryHourlyWage salary = (PaySalaryHourlyWage) this.sessionFactory.getCurrentSession().get(
							PaySalaryHourlyWage.class, id);
					total = total.add(salary.getAmount());
					salary.setPayment(newPayment);
					newPayment.getPaySalaryHourlyWages().add(salary);
				}
			}
			for (String value : vo.getUnCheckedValues()) {
				String[] split = this.splitString(value);
				Long id = Long.parseLong(split[0]);
				Long paymentId = Long.parseLong(split[1]);
				if (paymentId != 0) {
					// remove from payment
					Payment payment = map.get(paymentId);
					PaySalaryHourlyWage salary = (PaySalaryHourlyWage) this.sessionFactory.getCurrentSession().get(
							PaySalaryHourlyWage.class, id);
					if (payment == null) {
						payment = paymentDao.findPaymentObjectById(paymentId);
					}
					salary.setPayment(null);
					paySalary.getPaySalaryHourlyWages().add(salary);
					payment.setAmount(payment.getAmount().subtract(salary.getAmount()));
					payment.getPaySalaryHourlyWages().remove(salary);
					map.put(paymentId, payment);
				}
			}
		} else if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_MONTHLY)) {
			for (String value : vo.getCheckedValues()) {
				String[] split = this.splitString(value);
				Long id = Long.parseLong(split[0]);
				Long paymentId = Long.parseLong(split[1]);
				if (paymentId == 0) {
					PaySalaryEmployees salary = (PaySalaryEmployees) this.sessionFactory.getCurrentSession().get(
							PaySalaryEmployees.class, id);
					total = total.add(salary.getNetPay());
					salary.setPayment(newPayment);
					newPayment.getPaysalaryMonthlyWages().add(salary);
				}
			}
			for (String value : vo.getUnCheckedValues()) {
				String[] split = this.splitString(value);
				Long id = Long.parseLong(split[0]);
				Long paymentId = Long.parseLong(split[1]);
				if (paymentId != 0) {
					// remove from payment
					Payment payment = map.get(paymentId);
					PaySalaryEmployees salary = (PaySalaryEmployees) this.sessionFactory.getCurrentSession().get(
							PaySalaryEmployees.class, id);
					if (payment == null) {
						payment = paymentDao.findPaymentObjectById(paymentId);
					}
					salary.setPayment(null);
					paySalary.getPaySalaryMonthly().add(salary);
					payment.setAmount(payment.getAmount().subtract(salary.getNetPay()));
					payment.getPaysalaryMonthlyWages().remove(salary);
					map.put(paymentId, payment);
				}
			}
		}

		if (total.compareTo(zero) != 0) {
			BigDecimal byPlusAmount = zero;
			BigDecimal byMinusAmount = zero;
			PaymentEntries byEntry = new PaymentEntries();
			Transactions byTransaction = new Transactions();
			byTransaction.setPaymentEntries(byEntry);
			byTransaction.setCompany(newPayment.getCompany());
			byTransaction.setLedger(byLedger);
			byEntry.setTransactions(byTransaction);
			byEntry.setLedger(byLedger);
			if (currencyVo != null) {
				byEntry.setDebit(zero);
				byEntry.setCredit(total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
				byPlusAmount = zero;
				byMinusAmount = total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
			} else {
				byEntry.setCredit(total);
				byEntry.setDebit(zero);
				byPlusAmount = total;
				byMinusAmount = zero;
			}
			byEntry.getTransactions().setDate(newPayment.getPaymentDate());
			byEntry.getTransactions().setMinusAmount(byMinusAmount);
			byEntry.getTransactions().setPlusAmount(byPlusAmount);
			byEntry.setPayment(newPayment);
			newPayment.getEntries().add(byEntry);

			BigDecimal toPlusAmount = zero;
			BigDecimal toMinusAmount = zero;
			PaymentEntries toEntry = new PaymentEntries();
			Transactions toTransaction = new Transactions();
			toTransaction.setPaymentEntries(toEntry);
			toTransaction.setCompany(newPayment.getCompany());
			toTransaction.setLedger(toLedger);
			toEntry.setTransactions(toTransaction);
			toEntry.setLedger(toLedger);
			if (currencyVo != null) {
				toEntry.setDebit(total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
				toEntry.setCredit(zero);
				toPlusAmount = total.divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				toMinusAmount = zero;
			} else {
				toEntry.setCredit(zero);
				toEntry.setDebit(total);
				toPlusAmount = zero;
				toMinusAmount = total;
			}
			toEntry.getTransactions().setDate(newPayment.getPaymentDate());
			toEntry.getTransactions().setMinusAmount(toMinusAmount);
			toEntry.getTransactions().setPlusAmount(toPlusAmount);
			toEntry.setPayment(newPayment);
			newPayment.setAmount(total);
			newPayment.getEntries().add(toEntry);
			newPayment.setIsDependant(true);
			this.sessionFactory.getCurrentSession().save(newPayment);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PAYMENT, company);
		}

		for (Map.Entry<Long, Payment> entry : map.entrySet()) {
			Payment payment = entry.getValue();
			if (payment.getAmount().compareTo(zero) == 0)
				this.sessionFactory.getCurrentSession().delete(payment);
			else
				this.sessionFactory.getCurrentSession().saveOrUpdate(payment);
		}
		this.sessionFactory.getCurrentSession().merge(paySalary);
	}

	private String[] splitString(String string) {
		String res = string.trim();
		return res.split("!!!");
	}
}
