package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BranchTypeDao;
import com.hrms.web.entities.BranchType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.BranchTypeVo;
import com.hrms.web.vo.BranchVo;

/**
 * 
 * @author Vinutha
 * @since 12-March-2015
 *
 */
@Repository
public class BranchTypeDaoImpl extends AbstractDao implements BranchTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveBranchType(BranchTypeVo branchTypeVo) {
		for (String branchType : StringSplitter.splitWithComma(branchTypeVo.getBranchTypeName())) {
			if (findAndReturnBranchTypeByName(branchType.trim()) != null)
				throw new DuplicateItemException("Duplicate Branch Type : " + branchType.trim());
			BranchType type = findAndReturnBranchTypeByName(branchTypeVo.getTempBranchType());
			if (type == null)
				type = new BranchType();
			type.setType(branchType.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(type);
			userActivitiesService.saveUserActivity("Added new Branch Type " + branchType.trim());
		}
	}

	/*
	 * public BranchType findAndReturnBranchTypeByName(String branchTypeName) {
	 * Criteria criteria =
	 * this.sessionFactory.getCurrentSession().createCriteria(BranchType.class)
	 * .add(Restrictions.eq("type", branchTypeName)); if
	 * (criteria.uniqueResult() == null) return null; return (BranchType)
	 * criteria.uniqueResult(); }
	 */

	public BranchType findAndReturnBranchTypeById(Long branchTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BranchType.class)
				.add(Restrictions.eq("id", branchTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (BranchType) criteria.uniqueResult();
	}

	public void updateBranchType(BranchTypeVo branchTypeVo) {
		BranchType branchType = findAndReturnBranchTypeById(branchTypeVo.getBranchTypeId());
		if (branchType == null)
			throw new ItemNotFoundException("Branch Type Not Exist");
		branchType.setType(branchTypeVo.getBranchTypeName());
		this.sessionFactory.getCurrentSession().merge(branchType);
		userActivitiesService.saveUserActivity("Updated Branch Type " + branchTypeVo.getBranchTypeName());
	}

	public void deleteBranchType(Long branchTypeId) {
		BranchType branchType = findAndReturnBranchTypeById(branchTypeId);
		if (branchType == null)
			throw new ItemNotFoundException("Branch Type Not Exist");
		String name = branchType.getType();
		this.sessionFactory.getCurrentSession().delete(branchType);
		userActivitiesService.saveUserActivity("Deleted Branch Type " + name);
	}

	/**
	 * method to create branchTypeVo Object
	 * 
	 * @param branchType
	 * @return branchTypeVo
	 */
	private BranchTypeVo createBranchTypeVo(BranchType branchType) {
		BranchTypeVo branchTypeVo = new BranchTypeVo();
		branchTypeVo.setBranchTypeId(branchType.getId());
		branchTypeVo.setBranchTypeName(branchType.getType());
		return branchTypeVo;
	}

	/**
	 * method to find branch type object from branch type name
	 * 
	 * @param branchTypeName
	 * @return BranchType
	 */
	public BranchType findAndReturnBranchTypeByName(String branchTypeName) {
		return (BranchType) this.sessionFactory.getCurrentSession().createCriteria(BranchType.class)
				.add(Restrictions.eq("type", branchTypeName)).uniqueResult();
	}

	public BranchTypeVo findBranchType(Long branchTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BranchType.class)
				.add(Restrictions.eq("id", branchTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createBranchTypeVo((BranchType) criteria.uniqueResult());
	}

	public BranchTypeVo findBranchType(String branchType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BranchType.class)
				.add(Restrictions.eq("type", branchType));
		if (criteria.uniqueResult() == null)
			return null;
		return createBranchTypeVo((BranchType) criteria.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<BranchTypeVo> findAllBranchType() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BranchType.class);
		List<BranchTypeVo> branchTypeVos = new ArrayList<BranchTypeVo>();
		List<BranchType> branchTypes = criteria.list();
		for (BranchType branchType : branchTypes)
			branchTypeVos.add(createBranchTypeVo(branchType));
		return branchTypeVos;
	}

	public BranchTypeVo createBranchTypeVoFromBranchVo(BranchVo branchVo) {
		BranchTypeVo branchTypeVo = new BranchTypeVo();
		branchTypeVo.setBranchTypeName(branchVo.getNewBranchTypeName());
		;
		return branchTypeVo;
	}

}
