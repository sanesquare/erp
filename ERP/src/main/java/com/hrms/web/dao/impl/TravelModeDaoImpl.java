package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.TravelModeDao;
import com.hrms.web.entities.TravelMode;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.TravelModeVo;

/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
@Repository
public class TravelModeDaoImpl extends AbstractDao implements TravelModeDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveTravelMode(TravelModeVo vo) {
		// TODO Auto-generated method stub
		
	}

	public void updateTravelMode(TravelModeVo vo) {
		// TODO Auto-generated method stub
		
	}

	public void deleteTravelMode(Long id) {
		// TODO Auto-generated method stub
		
	}

	public TravelMode getTravelMode(Long id) {
		return (TravelMode) this.sessionFactory.getCurrentSession().createCriteria(TravelMode.class)
				.add(Restrictions.eq("id",id)).uniqueResult();
	}

	public TravelModeVo getTravelModeById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<TravelModeVo> listAllTravelModes() {
		List<TravelModeVo> vos = new ArrayList<TravelModeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TravelMode.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<TravelMode> modes = criteria.list();
		for(TravelMode mode : modes){
			vos.add(createModeVo(mode));
		}
		return vos;
	}

	private TravelModeVo createModeVo(TravelMode mode){
		TravelModeVo vo = new TravelModeVo();
		vo.setId(mode.getId());
		vo.setTravelMode(mode.getName());
		return vo;
	}
}
