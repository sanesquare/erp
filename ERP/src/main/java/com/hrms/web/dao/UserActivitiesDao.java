package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.vo.UserActivityVo;

public interface UserActivitiesDao {

	/**
	 * mathod to save SystemLog
	 * @param activity
	 */
	public void saveUserActivity(UserActivityVo userActivityVo);
	
	/**
	 * 
	 * @return 
	 */
	public List<Object[]> listDistinctMonthAndYear();
	
	public List<UserActivityVo> listAllUserActivities(int month,int year);
	
}
