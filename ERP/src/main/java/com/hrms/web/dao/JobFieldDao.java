package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.JobFields;
import com.hrms.web.vo.JobFieldVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public interface JobFieldDao {

	/**
	 * method to save new job field
	 * 
	 * @param jobFieldVo
	 */
	public void saveJobField(JobFieldVo jobFieldVo);

	/**
	 * method to update job field
	 * 
	 * @param jobFieldVo
	 */
	public void updateJobField(JobFieldVo jobFieldVo);

	/**
	 * method to delete jobfield
	 * 
	 * @param fieldId
	 */
	public void deleteJobField(Long fieldId);

	/**
	 * method to find job field
	 * 
	 * @param fieldId
	 * @return jobfield
	 */
	public JobFields findJobFiled(Long fieldId);

	/**
	 * method to find jobfiled
	 * 
	 * @param fieldId
	 * @return jobfieldvo
	 */
	public JobFieldVo findJobFieldById(Long fieldId);

	/**
	 * method to find job field by name
	 * 
	 * @param fieldName
	 * @return job field
	 */
	public JobFields findJobField(String fieldName);

	/**
	 * method to find job field by name
	 * 
	 * @param fieldName
	 * @return jobfieldvo
	 */
	public JobFieldVo findJobFieldByName(String fieldName);

	/**
	 * method to find all job field
	 * 
	 * @return
	 */
	public List<JobFieldVo> findAllJobField();
}
