package com.hrms.web.dao;

import java.math.BigDecimal;
import java.util.List;

import com.hrms.web.entities.TaxRule;
import com.hrms.web.vo.TaxRuleVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public interface TaxRuleDao {

	/**
	 * method to save or update TaxRule
	 * 
	 * @param taxRuleVo
	 */
	public void saveOrUpdateTaxRule(TaxRuleVo taxRuleVo);

	/**
	 * method to delete TaxRule
	 * 
	 * @param taxRuleId
	 */
	public void deleteTaxRule(Long taxRuleId);

	/**
	 * method to find TaxRule by id
	 * 
	 * @param taxRuleId
	 * @return TaxRule
	 */
	public TaxRule findTaxRule(Long taxRuleId);

	/**
	 * method to find TaxRule by id
	 * 
	 * @param taxRuleId
	 * @return TaxRuleVo
	 */
	public TaxRuleVo findTaxRuleById(Long taxRuleId);

	/**
	 * method to find all TaxRule
	 * 
	 * @return List<TaxRuleVo>
	 */
	public List<TaxRuleVo> findAllTaxRule();

	/**
	 * method to find TaxRule by salaryTo
	 * 
	 * @param salaryTo
	 * @return TaxRule
	 */
	public TaxRule findTaxRuleBySalaryTo(BigDecimal salaryTo);

	/**
	 * method to find TaxRule by salaryTo
	 * 
	 * @param salaryTo
	 * @return TaxRule
	 */
	public TaxRuleVo findTaxRuleVoBySalaryTo(BigDecimal salaryTo);

	/**
	 * method to find TaxRule by salaryFrom
	 * 
	 * @param salaryFrom
	 * @return TaxRule
	 */
	public TaxRule findTaxRuleBySalaryFrom(BigDecimal salaryFrom);

	/**
	 * method to find TaxRule by salaryFrom
	 * 
	 * @param salaryFrom
	 * @return TaxRule
	 */
	public TaxRuleVo findTaxRuleVoBySalaryFrom(BigDecimal salaryFrom);

	/**
	 * method to find all TaxRule by individualCategory
	 * 
	 * @param individualCategory
	 * @return List<TaxRuleVo>
	 */
	public List<TaxRuleVo> findAllTaxRuleByIndividual(String individualCategory);
}
