package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Loan;
import com.hrms.web.entities.LoanStatus;
import com.hrms.web.vo.LoanVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public interface LoanDao {

	/**
	 * method to save Loan
	 * 
	 * @param loanVo
	 */
	public void saveOrUpdateLoan(LoanVo loanVo);

	/**
	 * method to update Loan Status
	 * 
	 * @param loanVo
	 */
	public void updateLoanStatus(LoanVo loanVo);

	/**
	 * method to delete Loan
	 * 
	 * @param loanId
	 */
	public void deleteLoan(Long loanId);

	/**
	 * method to save Loan Document
	 * 
	 * @param documentVo
	 */
	public void saveLoanDocument(LoanVo loanVo);

	/**
	 * method to delete Loan Document
	 * 
	 * @param documentId
	 */
	public void deleteLoanDocument(Long documentId);

	/**
	 * method to find Loan by id
	 * 
	 * @param loanId
	 * @return Loan
	 */
	public Loan findLoan(Long loanId);

	/**
	 * method to find Loan by id
	 * 
	 * @param loanId
	 * @return LoanVo
	 */
	public LoanVo findLoanById(Long loanId);

	/**
	 * method to find Loan by employee
	 * 
	 * @param employee
	 * @return List<LoanVo>
	 */
	public List<LoanVo> findLoanByEmployee(Employee employee);

	/**
	 * method to find Loan by loan date
	 * 
	 * @param loanDate
	 * @return List<LoanVo>
	 */
	public List<LoanVo> findLoanByDate(Date loanDate);

	/**
	 * method to find Loan by repayment date
	 * 
	 * @param repaymentDate
	 * @return List<LoanVo>
	 */
	public List<LoanVo> findLoanByRepaymentDate(Date repaymentDate);

	/**
	 * method to find Loan by status
	 * 
	 * @param loanStatus
	 * @return List<LoanVo>
	 */
	public List<LoanVo> findLoanByStatus(LoanStatus loanStatus);

	/**
	 * method to find all Loan
	 * 
	 * @return List<LoanVo>
	 */
	public List<LoanVo> findAllLoan();

	/**
	 * method to find all Loan between two dates
	 * 
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return List<LoanVo>
	 */
	public List<LoanVo> findLoanByBetweenDates(Long employeeId, Date startDate, Date endDate);

	/**
	 * method to find all Loan by employee
	 * 
	 * @return List<LoanVo>
	 */
	public List<LoanVo> findAllLoanByEmployee(String employeeCode);

	/**
	 * 
	 * @param status
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<LoanVo> findLoanByStatusAndDates(String employeeCode, String status, Date startDate, Date endDate);
	
	/**
	 * Method to find approved loans for salary
	 * @param endDate
	 * @param employee
	 * @return
	 */
	public List<Loan> findApprovedLoanForSalary(String endDate, Employee employee);
	
	public void updateLoanObject(Loan loan);
}
