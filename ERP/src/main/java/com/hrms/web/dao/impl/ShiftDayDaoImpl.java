package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ShiftDayDao;
import com.hrms.web.entities.ShiftDay;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.ShiftDayVo;

/**
 * 
 * @author Jithin Mohan
 * @since 31-March-2015
 *
 */
@Repository
public class ShiftDayDaoImpl extends AbstractDao implements ShiftDayDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveShiftDay(ShiftDayVo shiftDayVo) {
		for (String day : StringSplitter.splitWithComma(shiftDayVo.getShiftDay())) {
			if (findShiftDay(day.trim()) != null)
				throw new DuplicateItemException("Duplicate Shift Day : " + day.trim());
			ShiftDay shiftDay = new ShiftDay();
			shiftDay.setDay(day.trim());
			this.sessionFactory.getCurrentSession().save(shiftDay);
		}
	}

	public void updateShiftDay(ShiftDayVo shiftDayVo) {
		ShiftDay shiftDay = findShiftDay(shiftDayVo.getShiftDayId());
		if (shiftDay == null)
			throw new ItemNotFoundException("Shift Day Details Not Found");
		shiftDay.setDay(shiftDayVo.getShiftDay());
		this.sessionFactory.getCurrentSession().merge(shiftDay);
	}

	public void deleteShiftDay(Long shiftDayId) {
		ShiftDay shiftDay = findShiftDay(shiftDayId);
		if (shiftDay == null)
			throw new ItemNotFoundException("Shift Day Details Not Found");
		this.sessionFactory.getCurrentSession().delete(shiftDay);
	}

	public ShiftDay findShiftDay(Long shiftDayId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ShiftDay.class)
				.add(Restrictions.eq("id", shiftDayId));
		if (criteria.uniqueResult() == null)
			return null;
		return (ShiftDay) criteria.uniqueResult();
	}

	public ShiftDay findShiftDay(String shiftDay) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ShiftDay.class)
				.add(Restrictions.eq("day", shiftDay));
		if (criteria.uniqueResult() == null)
			return null;
		return (ShiftDay) criteria.uniqueResult();
	}

	/**
	 * method to create ShiftDayVo object
	 * 
	 * @param shiftDay
	 * @return shiftDayVo
	 */
	private ShiftDayVo createShiftDayVo(ShiftDay shiftDay) {
		ShiftDayVo shiftDayVo = new ShiftDayVo();
		shiftDayVo.setShiftDay(shiftDay.getDay());
		shiftDayVo.setShiftDayId(shiftDay.getId());
		return shiftDayVo;
	}

	public ShiftDayVo findShiftDayById(Long shiftDayId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ShiftDay.class)
				.add(Restrictions.eq("id", shiftDayId));
		if (criteria.uniqueResult() == null)
			return null;
		return createShiftDayVo((ShiftDay) criteria.uniqueResult());
	}

	public ShiftDayVo findShiftDayByDay(String shiftDay) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ShiftDay.class)
				.add(Restrictions.eq("day", shiftDay));
		if (criteria.uniqueResult() == null)
			return null;
		return createShiftDayVo((ShiftDay) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<ShiftDayVo> findAllShiftDay() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ShiftDay.class);
		List<ShiftDayVo> shiftDayVos = new ArrayList<ShiftDayVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<ShiftDay> shiftDays = criteria.list();
		for (ShiftDay shiftDay : shiftDays)
			shiftDayVos.add(createShiftDayVo(shiftDay));
		return shiftDayVos;
	}

}
