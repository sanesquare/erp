package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.Transfer;
import com.hrms.web.vo.TransferVo;

/**
 * 
 * @author Shamsheer
 * @since 24-April-2015
 */
public interface TransferDao {

	/**
	 * method to save transfer
	 * 
	 * @param transferVo
	 * @return
	 */
	public Long saveTransfer(TransferVo transferVo);

	/**
	 * method to update transfer
	 * 
	 * @param transferVo
	 */
	public void updateTransfer(TransferVo transferVo);

	/**
	 * method to delete transfer
	 * 
	 * @param transferId
	 */
	public void deleteTransfer(Long transferId);

	/**
	 * method to get Transfer object
	 * 
	 * @param transferId
	 * @return
	 */
	public Transfer getTransfer(Long transferId);

	/**
	 * method to list all transfer
	 * 
	 * @return
	 */
	public List<TransferVo> listAllTransfer();

	/**
	 * method to get transfer by id
	 * 
	 * @param transferId
	 * @return
	 */
	public TransferVo getTransferById(Long transferId);

	/**
	 * method to list transfer by Employee
	 * 
	 * @return
	 */
	public List<TransferVo> listTransferByEmployee(Long employeeId);

	/**
	 * method to list transfer by Employee code
	 * 
	 * @return
	 */
	public List<TransferVo> listTransferByEmployee(String employeeCode);

	/**
	 * method to update document
	 * 
	 * @param transferVo
	 */
	public void updateDocument(TransferVo transferVo);

	/**
	 * method to delete document
	 * 
	 * @param url
	 */
	public void deleteDocument(String url);

	/**
	 * method to update transfer status
	 * 
	 * @param transferVo
	 */
	public void updateStatus(TransferVo transferVo);

	/**
	 * method to list transfers between two dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<TransferVo> listTransferBetweenDates(String startDate, String endDate);

	/**
	 * method to list transfers between dates and in a branch
	 * 
	 * @param startDate
	 * @param endDate
	 * @param branchId
	 * @return
	 */
	public List<TransferVo> listTransferBetweenDatesAndBranch(String startDate, String endDate, Long branchId);

	/**
	 * method to list transfers between dates and in a department
	 * 
	 * @param startDate
	 * @param endDate
	 * @param departmentId
	 * @return
	 */
	public List<TransferVo> listTransferBetweenDatesAndDepartment(String startDate, String endDate, Long departmentId);

	/**
	 * method to list transfers in a branch and department by given dates
	 * 
	 * @param branchId
	 * @param departmentId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<TransferVo> listTransferByBranchDepartmetnAndBetweenDates(Long branchId, Long departmentId,
			String startDate, String endDate);

	/**
	 * method to find all transfer by Employee code
	 * 
	 * @param employeeCode
	 * @return
	 */
	public List<TransferVo> findAllTransferByEmployee(String employeeCode);
}
