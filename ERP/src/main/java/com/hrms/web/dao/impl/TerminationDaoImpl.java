package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.EmployeeStatusDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.dao.TerminationDao;
import com.hrms.web.dao.TerminationStatusDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Termination;
import com.hrms.web.entities.TerminationDocuments;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SmsServiceUtil;
import com.hrms.web.vo.TempNotificationVo;
import com.hrms.web.vo.TerminationDocumentsVo;
import com.hrms.web.vo.TerminationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
@Repository
public class TerminationDaoImpl extends AbstractDao implements TerminationDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private TerminationStatusDao resignationStatusDao;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private EmployeeStatusDao employeeStatusDao;

	@Autowired
	private TempNotificationDao notificationDao;

	public void saveTermination(TerminationVo terminationVo) {
		Termination termination = findTermination(terminationVo.getTerminationId());
		if (termination == null)
			termination = new Termination();
		Employee employee = employeeDao.findAndReturnEmployeeByCode(terminationVo.getEmployee());
		if (employee == null)
			throw new ItemNotFoundException("Employee Details Not Found");
		termination.setTerminated(employee);
		termination.setTerminator(setTerminationForwadList(terminationVo.getForwaders(), termination));
		if (terminationVo.getForwaders() != null && terminationVo.getForwaders().size() > 0) {
			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject("A Termination Request Has Been Received. ");
			notificationVo.getEmployeeCodes().addAll(terminationVo.getForwaders());
			notificationDao.saveNotification(notificationVo);
		}
		termination.setTerminatedOn(DateFormatter.convertStringToDate(terminationVo.getTermiantedDate()));
		termination.setDescription(terminationVo.getDescription());
		termination.setStatus(resignationStatusDao.findTerminationStatus(terminationVo.getTerminationStatus()));
		if (terminationVo.getTerminationStatus().equalsIgnoreCase(StatusConstants.APPROVED))
			sendSms(termination,terminationVo);
		termination.setStatusDescription(terminationVo.getTerminationStatusDescription());
		termination.setNotes(terminationVo.getNotes());
		this.sessionFactory.getCurrentSession().saveOrUpdate(termination);
		employee.setEmployeeStatus(employeeStatusDao.findStatus("Inactive"));
		if (employee.getUser() != null)
			employee.getUser().setAllowLogin(false);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to create termination forward list
	 * 
	 * @param employee
	 * @param termination
	 * @return forwardTo
	 */
	private Set<Employee> setTerminationForwadList(List<String> employee, Termination termination) {
		Set<Employee> forwardTo = termination.getTerminator();

		Set<Employee> forwardToTemp = new HashSet<Employee>();
		if (forwardTo != null)
			forwardToTemp.addAll(forwardTo);

		Set<Employee> newForwardTo = new HashSet<Employee>();
		if (forwardTo == null || forwardTo.isEmpty())
			forwardTo = new HashSet<Employee>();
		for (String employeeCode : employee) {
			Employee forwader = employeeDao.findAndReturnEmployeeByCode(employeeCode);
			newForwardTo.add(forwader);
			if (!forwardTo.contains(forwader)) {
				// forwader.setAdvanceSalary(advanceSalary);
				forwardTo.add(forwader);
			}
		}

		forwardToTemp.removeAll(newForwardTo);
		Iterator<Employee> iterator = forwardToTemp.iterator();
		while (iterator.hasNext()) {
			Employee employee2 = (Employee) iterator.next();
			forwardTo.remove(employee2);
		}
		return forwardTo;
	}

	public void saveTerminationDocuments(TerminationVo terminationVo) {
		Termination termination = findTermination(terminationVo.getTerminationId());
		if (termination == null)
			throw new ItemNotFoundException("Termination Details Not Found.");
		termination.setDocuments(setTerminationDocuments(terminationVo.getTerminationDocumentsVos(), termination));
		this.sessionFactory.getCurrentSession().merge(termination);
	}

	/**
	 * method to set documents to termination object
	 * 
	 * @param documentsVos
	 * @param termination
	 * @return
	 */
	private Set<TerminationDocuments> setTerminationDocuments(List<TerminationDocumentsVo> documentsVos,
			Termination termination) {
		Set<TerminationDocuments> terminationDocuments = new HashSet<TerminationDocuments>();
		for (TerminationDocumentsVo documentsVo : documentsVos) {
			TerminationDocuments documents = new TerminationDocuments();
			documents.setName(documentsVo.getDocumentName());
			documents.setUrl(documentsVo.getDocumentUrl());
			documents.setTermination(termination);
			terminationDocuments.add(documents);
		}
		return terminationDocuments;
	}

	public void updateTermination(TerminationVo terminationVo) {
		// TODO Auto-generated method stub

	}

	public void deleteTermination(Long terminationId) {
		Termination termination = findTermination(terminationId);
		if (termination == null)
			throw new ItemNotFoundException("Resignation Details Not Found.");
		termination.getTerminator().clear();
		this.sessionFactory.getCurrentSession().delete(termination);
	}

	public void deleteTerminationDocument(Long terminationDocId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TerminationDocuments.class)
				.add(Restrictions.eq("id", terminationDocId));
		TerminationDocuments document = (TerminationDocuments) criteria.uniqueResult();
		Termination termination = findTermination(document.getTermination().getId());
		termination.getDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);

		this.sessionFactory.getCurrentSession().merge(termination);
	}

	public Termination findTermination(Long terminationId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Termination.class)
				.add(Restrictions.eq("id", terminationId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Termination) criteria.uniqueResult();
	}

	public List<Termination> findTermination(String terminator) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Termination> findTermination(Date terminatedOn) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Termination.class)
				.add(Restrictions.eq("terminatedOn", terminatedOn));
		if (criteria.list().isEmpty())
			return null;
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Termination> findTermination(Date startDate, Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Termination.class)
				.add(Restrictions.ge("terminatedOn", startDate)).add(Restrictions.le("terminatedOn", endDate));
		if (criteria.list().isEmpty())
			return null;
		return criteria.list();
	}

	/**
	 * method to create TerminationVo object
	 * 
	 * @param termination
	 * @return terminationVo
	 */
	private TerminationVo createTerminationVo(Termination termination) {
		TerminationVo terminationVo = new TerminationVo();
		terminationVo.setEmployee(termination.getTerminated().getUserProfile().getFirstName() + " "
				+ termination.getTerminated().getUserProfile().getLastname());
		terminationVo.setEmployeeCode(termination.getTerminated().getEmployeeCode());
		terminationVo.setForwaders(getTerminationForwadersList(termination.getTerminator()));
		terminationVo.setForwadersName(getTerminationForwadersNameList(termination.getTerminator()));
		terminationVo.setTermiantedDate(DateFormatter.convertDateToString(termination.getTerminatedOn()));
		terminationVo.setDescription(termination.getDescription());
		terminationVo.setTerminationStatus(termination.getStatus().getStatus());
		terminationVo.setTerminationStatusDescription(termination.getStatusDescription());
		terminationVo.setNotes(termination.getNotes());
		terminationVo.setTerminationId(termination.getId());
		terminationVo.setRecordedBy(termination.getCreatedBy().getUserName());
		terminationVo.setRecordedOn(DateFormatter.convertDateToDetailedString(termination.getCreatedOn()));
		terminationVo.setTerminationDocumentsVos(getTerminationDocumentsList(termination));
		return terminationVo;
	}

	/**
	 * method to get termination forwaders list
	 * 
	 * @param forwaders
	 * @return terminationFrwdrs
	 */
	private List<String> getTerminationForwadersList(Set<Employee> forwaders) {
		List<String> terminationFrwdrs = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			terminationFrwdrs.add(employee.getEmployeeCode());
		}
		return terminationFrwdrs;
	}

	/**
	 * method to get termination forwaders name list
	 * 
	 * @param forwaders
	 * @return terminationFrwdrs
	 */
	private List<String> getTerminationForwadersNameList(Set<Employee> forwaders) {
		List<String> terminationFrwdrs = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			terminationFrwdrs.add(employee.getUserProfile().getFirstName() + " "
					+ employee.getUserProfile().getLastname());
		}
		return terminationFrwdrs;
	}

	/**
	 * method to list all termination documents
	 * 
	 * @param termination
	 * @return documentsVos
	 */
	private List<TerminationDocumentsVo> getTerminationDocumentsList(Termination termination) {
		List<TerminationDocumentsVo> documentsVos = new ArrayList<TerminationDocumentsVo>();
		Iterator<TerminationDocuments> iterator = termination.getDocuments().iterator();
		while (iterator.hasNext()) {
			TerminationDocumentsVo documentsVo = new TerminationDocumentsVo();
			TerminationDocuments terminationDocument = (TerminationDocuments) iterator.next();
			documentsVo.setDocumentName(terminationDocument.getName());
			documentsVo.setDocumentUrl(terminationDocument.getUrl());
			documentsVo.setTer_doc_id(terminationDocument.getId());
			documentsVos.add(documentsVo);
		}
		return documentsVos;
	}

	public TerminationVo findTerminationById(Long terminationId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Termination.class)
				.add(Restrictions.eq("id", terminationId));
		if (criteria.uniqueResult() == null)
			return null;
		return createTerminationVo((Termination) criteria.uniqueResult());
	}

	public List<TerminationVo> findTerminationByTerminator(String terminator) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<TerminationVo> findTerminationByDate(Date terminatedOn) {
		List<TerminationVo> terminationVos = new ArrayList<TerminationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Termination.class)
				.add(Restrictions.eq("terminatedOn", terminatedOn));
		if (criteria.list().isEmpty())
			return null;
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Termination> terminations = criteria.list();
		for (Termination termination : terminations)
			terminationVos.add(createTerminationVo(termination));
		return terminationVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<TerminationVo> findTerminationBetweenDates(Date startDate, Date endDate) {
		List<TerminationVo> terminationVos = new ArrayList<TerminationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Termination.class)
				.add(Restrictions.ge("terminatedOn", startDate)).add(Restrictions.le("terminatedOn", endDate));
		if (criteria.list().isEmpty())
			return null;
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Termination> terminations = criteria.list();
		for (Termination termination : terminations)
			terminationVos.add(createTerminationVo(termination));
		return terminationVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<TerminationVo> findAllTermination() {
		List<TerminationVo> terminationVos = new ArrayList<TerminationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Termination.class);
		if (criteria.list().isEmpty())
			return null;
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Termination> terminations = criteria.list();
		for (Termination termination : terminations)
			terminationVos.add(createTerminationVo(termination));
		return terminationVos;
	}

	public TerminationVo findTerminationByEmployee(String employeeCode) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Termination.class)
				.add(Restrictions.eq("terminated", employeeDao.findAndReturnEmployeeByCode(employeeCode)));
		if (criteria.uniqueResult() == null)
			return null;
		return createTerminationVo((Termination) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TerminationVo> findAllTerminationByEmployee(Long employeeId) {
		List<TerminationVo> terminationVos = new ArrayList<TerminationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Termination.class);
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		Criterion sender = Restrictions.eq("createdBy", employee.getUser());
		Criterion receiver = Restrictions.eq("terminated", employee);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		if (criteria.list().isEmpty())
			return null;
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Termination> terminations = criteria.list();
		for (Termination termination : terminations)
			terminationVos.add(createTerminationVo(termination));
		return terminationVos;
	}

	/**
	 * method to send sms
	 * 
	 * @param termination
	 */
	private void sendSms(Termination termination , TerminationVo terminationVo) {
		Employee employee = termination.getTerminated();
		if (employee.getUserProfile() != null) {
			List<String> toNumbers = new ArrayList<String>();
			for(Long id : terminationVo.getSmsEmployees()){
				Employee smsEmployee = employeeDao.findAndReturnEmployeeById(id);
				if(smsEmployee.getUserProfile()!=null && smsEmployee.getUserProfile().getMobile()!=null)
					toNumbers.add(smsEmployee.getUserProfile().getMobile());
			}
			try {
				String msg = null;
				
				if (employee.getUserProfile() != null)
					msg = employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname()
							+ " of " + employee.getBranch().getBranchName() + " is terminated.";
				else
					msg = employee.getEmployeeCode() + " of " + employee.getBranch().getBranchName() + " is terminated.";
				SmsServiceUtil.sendSMS(toNumbers, msg);
			} catch (Exception e) {
			}
		}
	}
}
