package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.RelegionDao;
import com.hrms.web.entities.Relegion;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.RelegionVo;

/**
 * 
 * @author shamsheer, Jithin Mohan
 * @since 21-March-2015
 */
@Repository
public class RelegianDaoImpl extends AbstractDao implements RelegionDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveRelegion(RelegionVo relegionVo) {
		for (String religion : StringSplitter.splitWithComma(relegionVo.getRelegion())) {
			if (findRelegian(religion.trim()) != null)
				throw new DuplicateItemException("Duplicate Religion : " + religion.trim());
			Relegion relegion = new Relegion();
			relegion.setRelegion(religion.trim());
			this.sessionFactory.getCurrentSession().save(relegion);
		}
	}

	public void updateRelegion(RelegionVo relegionVo) {
		Relegion relegion = findRelegian(relegionVo.getRelegionId());
		if (relegion == null)
			throw new ItemNotFoundException("Religion Details Not Found.");
		relegion.setRelegion(relegionVo.getRelegion());
		this.sessionFactory.getCurrentSession().merge(relegion);
	}

	public void deleteReligion(Long id) {
		Relegion relegion = findRelegian(id);
		if (relegion == null)
			throw new ItemNotFoundException("Religion Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(relegion);
	}

	public RelegionVo findRelegianById(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Relegion.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return createRelegianVo((Relegion) criteria.uniqueResult());
	}

	public RelegionVo findReligianByName(String relegian) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Relegion.class)
				.add(Restrictions.eq("relegion", relegian));
		if (criteria.uniqueResult() == null)
			return null;
		return createRelegianVo((Relegion) criteria.uniqueResult());
	}

	public Relegion findRelegian(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Relegion.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return (Relegion) criteria.uniqueResult();
	}

	public Relegion findRelegian(String relegian) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Relegion.class)
				.add(Restrictions.eq("relegion", relegian));
		if (criteria.uniqueResult() == null)
			return null;
		return (Relegion) criteria.uniqueResult();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<RelegionVo> listAllRelegians() {
		List<RelegionVo> relegionVos = new ArrayList<RelegionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Relegion.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Relegion> relegions = criteria.list();
		for (Relegion relegion : relegions) {
			relegionVos.add(createRelegianVo(relegion));
		}
		return relegionVos;
	}

	/**
	 * method to create RelegionVo object
	 * 
	 * @param relegion
	 * @return
	 */
	private RelegionVo createRelegianVo(Relegion relegion) {
		RelegionVo relegionVo = new RelegionVo();
		relegionVo.setRelegion(relegion.getRelegion());
		relegionVo.setRelegionId(relegion.getId());
		return relegionVo;
	}
}
