package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.PromotionStatus;
import com.hrms.web.vo.PromotionStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 25-March-2015
 *
 */
public interface PromotionStatusDao {

	/**
	 * method to save PromotionStatus
	 * 
	 * @param promotionStatusVo
	 */
	public void savePromotionStatus(PromotionStatusVo promotionStatusVo);

	/**
	 * method to update PromotionStatus
	 * 
	 * @param promotionStatusVo
	 */
	public void updatePromotionStatus(PromotionStatusVo promotionStatusVo);

	/**
	 * method to delete PromotionStatus
	 * 
	 * @param statusId
	 */
	public void deletePromotionStatus(Long statusId);

	/**
	 * method to find PromotionStatus by id
	 * 
	 * @param statusId
	 * @return PromotionStatus
	 */
	public PromotionStatus findPromotionStatus(Long statusId);

	/**
	 * method to find PromotionStatus by status
	 * 
	 * @param status
	 * @return PromotionStatus
	 */
	public PromotionStatus findPromotionStatus(String status);

	/**
	 * method to find PromotionStatus by id
	 * 
	 * @param statusId
	 * @return PromotionStatusVo
	 */
	public PromotionStatusVo findPromotionStatusById(Long statusId);

	/**
	 * method to find PromotionStatus by status
	 * 
	 * @param status
	 * @return PromotionStatusVo
	 */
	public PromotionStatusVo findPromotionStatusByStatus(String status);

	/**
	 * method to find all PromotionStatus
	 * 
	 * @return List<PromotionStatusVo>
	 */
	public List<PromotionStatusVo> findAllPromotionStatus();
}
