package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Memo;
import com.hrms.web.vo.MemoVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
public interface MemoDao {

	/**
	 * method to save or update Memo
	 * 
	 * @param memoVo
	 */
	public void saveOrUpdateMemo(MemoVo memoVo);

	/**
	 * method to save Memo Documents
	 * 
	 * @param memoVo
	 */
	public void saveMemoDocuments(MemoVo memoVo);

	/**
	 * method to delete Memo
	 * 
	 * @param memoId
	 */
	public void deleteMemo(Long memoId);

	/**
	 * method to delete Memo Document
	 * 
	 * @param memoDocumentId
	 */
	public void deleteMemoDocument(Long memoDocumentId);

	/**
	 * method to find Memo by id
	 * 
	 * @param memoId
	 * @return Memo
	 */
	public Memo findMemo(Long memoId);

	/**
	 * method to find Memo by id
	 * 
	 * @param memoId
	 * @return MemoVo
	 */
	public MemoVo findMemoById(Long memoId);

	/**
	 * method to find Memo by date
	 * 
	 * @param memoDate
	 * @return List<MemoVo>
	 */
	public List<MemoVo> findMemoByDate(Date memoDate);

	/**
	 * method to find Memo between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return List<MemoVo>
	 */
	public List<MemoVo> findMemoBetweenDates(Date startDate, Date endDate);

	/**
	 * method to find all Memo
	 * 
	 * @return List<MemoVo>
	 */
	public List<MemoVo> findAllMemo();
}
