package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ComplaintsDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.entities.ComplaintDocuments;
import com.hrms.web.entities.ComplaintStatus;
import com.hrms.web.entities.Complaints;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.DepartmentDocuments;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Promotion;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.ComplaintStatusVo;
import com.hrms.web.vo.ComplaintsVo;
import com.hrms.web.vo.PromotionVo;

@Repository
public class ComplaintsDaoImpl extends AbstractDao implements ComplaintsDao {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;
	
	
	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private EmployeeService employeeService;

	public void saveComplaint(ComplaintsVo complaintVo) {
		logger.info("Inside >>complaintsDao save complaints");
		try {
			Complaints complaints = new Complaints();
			complaints.setComplaintsDescription(complaintVo.getComplaintDescription());
			complaints.setComplaintsFrom(employeeService.findAndReturnEmployeeById(complaintVo.getComplaintFromEmpId()));
			complaints.setComplaintsAganist(setComplaintAgainst(complaintVo.getComplaintAganistEmp(), complaints));
			complaints.setForwardTo(setComplaintForwardTo(employeeService.findAndReturnEmployeeById(complaintVo.getForwardAppToEmpId()), complaints));

			complaints.getForwardTo().add(employeeService.findAndReturnEmployeeById(complaintVo.getForwardAppToEmpId()));
			complaints.setComplaintDate(DateFormatter.convertStringToDate(complaintVo.getComplaintDate()));
			complaints.setNote(complaintVo.getNote());
			ComplaintStatus complaintStatus=new ComplaintStatus();
			complaintStatus.setId(complaintVo.getComplaintStatusId());
			complaints.setComplaintStatus(complaintStatus);
			complaints.setStatusDescription(complaintVo.getStatusDescription());
			this.sessionFactory.getCurrentSession().saveOrUpdate(complaints);
			complaintVo.setId(complaints.getId());

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}

	}

	private Set<Employee> setComplaintForwardTo(Employee employee, Complaints complaint) {
		Set<Employee> againstr = new HashSet<Employee>();
		Set<Complaints> complaints=new HashSet<Complaints>();
		//complaints.add(complaint);
		//employee.setComplaints2(complaints); 
		againstr.add(employee);
		return againstr;
	}
	
	private Set<Employee> setComplaintAgainst(List<String> employee, Complaints complaint) {
		/*Set<Employee> againstr = new HashSet<Employee>();
		Set<Complaints> complaints=new HashSet<Complaints>();
		//complaints.add(complaint);
		//employee.setComplaints2(complaints); 
		againstr.add(employee);
		return againstr;*/
		
		
		Set<Employee> complaintOn = complaint.getComplaintsAganist();

		Set<Employee> complaintOnTemp = new HashSet<Employee>();
		if (complaintOn != null)
			complaintOnTemp.addAll(complaintOn);

		Set<Employee> newForwardTo = new HashSet<Employee>();
		if (complaintOn == null || complaintOn.isEmpty())
			complaintOn = new HashSet<Employee>();
		for (String employeeCode : employee) {
			Employee forwader = employeeDao.findAndReturnEmployeeByCode(employeeCode);
			newForwardTo.add(forwader);
			if (!complaintOn.contains(forwader)) {
				//forwader.setC(complaint);
				complaintOn.add(forwader);
			}
		}

		complaintOnTemp.removeAll(newForwardTo);
		Iterator<Employee> iterator = complaintOnTemp.iterator();
		while (iterator.hasNext()) {
			Employee employee2 = (Employee) iterator.next();
			complaintOn.remove(employee2);
		}
		return complaintOn;
	}

	public List<ComplaintsVo> findAllComplaintDetails() {
		logger.info("Inside findAllcomplaints");
		List<ComplaintsVo> complaintsVo = new ArrayList<ComplaintsVo>();
		List<Complaints> complaints = this.sessionFactory.getCurrentSession().createCriteria(Complaints.class).list();
		for (Complaints complaint : complaints) {
			complaintsVo.add(createComplaintVo(complaint));
		}
		return complaintsVo;
	}
	
	
	public List<ComplaintStatusVo> findAllComplaintStatusDetails() {
		logger.info("Inside findAllcomplaintsStatus");
		List<ComplaintStatusVo> complaintStatusVo = new ArrayList<ComplaintStatusVo>();
		List<ComplaintStatus> complaintStatus = this.sessionFactory.getCurrentSession().createCriteria(ComplaintStatus.class).list();
		for (ComplaintStatus complaintStat : complaintStatus) {
			complaintStatusVo.add(createComplaintStatusVo(complaintStat));
		}
		return complaintStatusVo;
	}

	private ComplaintsVo createComplaintVo(Complaints complaints) {
		logger.info("inside>> Deparrment... createPromotionVo");
		ComplaintsVo complaintsVo = new ComplaintsVo();
		complaintsVo.setId(complaints.getId());
		complaintsVo.setComplaintDescription(complaints.getComplaintsDescription());
		complaintsVo.setComplaintDate(DateFormatter.convertDateToString(complaints.getComplaintDate()));
		complaintsVo.setNote(complaints.getNote());
		complaintsVo.setStatusDescription(complaints.getStatusDescription());
		complaintsVo.setComplaintFromEmp(complaints.getComplaintsFrom());
		complaintsVo.setComplaintFromEmpCode(complaints.getComplaintsFrom().getEmployeeCode());
		complaintsVo.setComplaintFromEmpId(complaints.getComplaintsFrom().getId());
		return complaintsVo;
	}
	
	private ComplaintStatusVo createComplaintStatusVo(ComplaintStatus complaintStatus) {
		logger.info("inside>> Deparrment... createPromotionVo");
		ComplaintStatusVo complaintStatusVo = new ComplaintStatusVo();
		complaintStatusVo.setId(complaintStatus.getId());
		complaintStatusVo.setStatusDescription(complaintStatus.getStatus());
		

		return complaintStatusVo;
	}

	public void deleteComplaint(Long id) {
		logger.info("inside>> ComplaintsDao... deleteComplaint");
		Complaints complaints=findAndReturnComplaintsObjectById(id);
		complaints.setComplaintsAganist(null);
		complaints.setComplaintsFrom(null);
		this.sessionFactory.getCurrentSession().delete(complaints);

	}

	public ComplaintsVo findComplaintById(Long complaintId) {
		logger.info("inside>> complaintsDao... findComplaintsById");
		Complaints complaints = (Complaints) this.sessionFactory.getCurrentSession().createCriteria(Complaints.class)
				.add(Restrictions.eq("id", complaintId)).uniqueResult();
		
		return createComplaintVo(complaints);
	}

	public void updateComplaint(ComplaintsVo complaintVo) {
		logger.info("inside>> ComplaintsDao... updateComplaints");
		try {
			Complaints complaints = findAndReturnComplaintsObjectById(complaintVo.getId());
			complaints.setComplaintsDescription(complaintVo.getComplaintDescription());
			complaints
					.setComplaintsFrom(employeeService.findAndReturnEmployeeById(complaintVo.getComplaintFromEmpId()));
			System.out.println("llllllllllllllllllllll" + complaintVo.getComplaintDate());
			complaints.setComplaintDate(DateFormatter.convertStringToDate(complaintVo.getComplaintDate()));
			complaints.setNote(complaintVo.getNote());
			complaints.setStatusDescription(complaintVo.getStatusDescription());
			this.sessionFactory.getCurrentSession().saveOrUpdate(complaints);

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}

	}

	public Complaints findAndReturnComplaintsObjectById(Long complaintsId) {
		logger.info("inside>> ComplaintsDao... findAndReturnComplaintsObjectById");
		return (Complaints) this.sessionFactory.getCurrentSession().createCriteria(Complaints.class)
				.add(Restrictions.eq("id", complaintsId)).uniqueResult();
	}

	public void deleteComplaintsDocument(Long docId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ComplaintDocuments.class)
				.add(Restrictions.eq("id", docId));
		ComplaintDocuments document = (ComplaintDocuments) criteria.uniqueResult();
		Complaints complaints = findComplaints(document.getComplaints().getId());
		complaints.getComplaintDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(complaints);
		
	}
	
	public Complaints findComplaints(Long complaintId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Complaints.class)
				.add(Restrictions.eq("id", complaintId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Complaints) criteria.uniqueResult();
	}

}
