package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ForwardApplicationStatusDao;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.ForwardApplicationStatusVo;
import com.hrms.web.vo.LeavesVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
@Repository
public class ForwardApplicationStatusDaoImpl extends AbstractDao implements ForwardApplicationStatusDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveStatus(ForwardApplicationStatusVo statusVo) {
		ForwardApplicationStatus status = new ForwardApplicationStatus();
		status.setName(statusVo.getStatus());
		this.sessionFactory.getCurrentSession().save(status);
	}

	public void updateStatus(ForwardApplicationStatusVo statusVo) {
		ForwardApplicationStatus status = getStatus(statusVo.getId());
		status.setName(statusVo.getStatus());
		this.sessionFactory.getCurrentSession().merge(status);
	}

	public void deleteStatus(Long id) {
		ForwardApplicationStatus status = getStatus(id);
		if(status!=null)
		this.sessionFactory.getCurrentSession().delete(status);
	}

	@SuppressWarnings("unchecked")
	public List<ForwardApplicationStatusVo> listAllStatus() {
		List<ForwardApplicationStatusVo> vos = new ArrayList<ForwardApplicationStatusVo>();
		List<ForwardApplicationStatus> leaveStatus = this.sessionFactory.getCurrentSession().createCriteria(ForwardApplicationStatus.class)
				.add(Restrictions.not(Restrictions.eq("name", StatusConstants.REQUESTED))).list();
		for(ForwardApplicationStatus status : leaveStatus){
			vos.add(createStatusVo(status));
		}
		return vos;
	}

	public ForwardApplicationStatus getStatus(Long statusId) {
		return (ForwardApplicationStatus) this.sessionFactory.getCurrentSession().createCriteria(ForwardApplicationStatus.class)
				.add(Restrictions.eq("id", statusId)).uniqueResult();
	}

	private ForwardApplicationStatusVo createStatusVo(ForwardApplicationStatus status){
		ForwardApplicationStatusVo vo = new ForwardApplicationStatusVo();
		vo.setId(status.getId());
		vo.setStatus(status.getName());
		return vo;
	}

	public ForwardApplicationStatus getStatus(String status) {
		return (ForwardApplicationStatus) this.sessionFactory.getCurrentSession().createCriteria(ForwardApplicationStatus.class)
				.add(Restrictions.eq("name", status)).uniqueResult();
	}
}
