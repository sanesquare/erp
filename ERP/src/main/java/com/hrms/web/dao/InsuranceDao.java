package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Insurance;
import com.hrms.web.entities.InsuranceType;
import com.hrms.web.vo.InsuranceVo;

/**
 * 
 * @author Jithin Mohan
 * @since 10-April-2015
 *
 */
public interface InsuranceDao {

	/**
	 * method to create or update Insurance
	 * 
	 * @param insuranceVo
	 */
	public void saveOrUpdateInsurance(InsuranceVo insuranceVo);

	/**
	 * method to delete Insurance
	 * 
	 * @param insuranceId
	 */
	public void deleteInsurance(Long insuranceId);

	/**
	 * method to find Insurance by id
	 * 
	 * @param insuranceId
	 * @return Insurance
	 */
	public Insurance findInsurance(Long insuranceId);

	/**
	 * method to find Insurance by id
	 * 
	 * @param insuranceId
	 * @return InsuranceVo
	 */
	public InsuranceVo findInsuranceById(Long insuranceId);

	/**
	 * method to find Insurance by employee
	 * 
	 * @param employee
	 * @return InsuranceVo
	 */
	public InsuranceVo findInsuranceByEmployee(Employee employee);

	/**
	 * method to find Insurance by insurance type
	 * 
	 * @param insuranceType
	 * @return List<InsuranceVo>
	 */
	public List<InsuranceVo> findInsuranceByType(InsuranceType insuranceType);

	/**
	 * method to find all Insurance
	 * 
	 * @return List<InsuranceVo>
	 */
	public List<InsuranceVo> findAllInsurance();

	/**
	 * method to find all insurance of an employee by between dates
	 * 
	 * @param employee
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<InsuranceVo> findAllInsuranceByEmployee(Employee employee, Date startDate, Date endDate);
}
