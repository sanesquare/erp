package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.ProjectStatus;
import com.hrms.web.vo.ProjectVo;

/**
 * 
 * @author Shamsheer
 * @since 11-March-2015
 *
 */
public interface ProjectStatusDao {

	/**
	 * method to save projectStatus
	 * @param projectVo
	 */
	public void saveProjectStatus(ProjectVo projectVo);
	
	/**
	 * method to update projectStatus
	 * @param projectVo
	 */
	public void updateProjectStatus(ProjectVo projectVo);
	
	/**
	 * method to delete projectStatus
	 * @param projectVo
	 */
	public void deleteProjectStatus(Long id);
	
	/**
	 * method to list all projectStatus
	 * @return
	 */
	public List<ProjectVo> listAllProjectStatus();
	
	/**
	 * method to find and return ProjectStatus object
	 * @param id
	 * @return
	 */
	public ProjectStatus findAndReturnProjectStatusObjById(Long id);
	
	/**
	 * method to find and return ProjectStatus object by status
	 * @param status
	 * @return
	 */
	public ProjectStatus findAndReturnProjectStatusObjectByStatus(String status);
	
}
