package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.CandidateStatus;
import com.hrms.web.vo.CandidatesStatusVo;

/**
 * 
 * @author Vinutha
 * @since 24-March-2015
 *
 */
public interface CandidateStatusDao {

	/**
	 * method to save new candidate status
	 * @param candidatesStatusVo
	 */
	public void  saveCandidatesStatus(CandidatesStatusVo candidatesStatusVo);
	/**
	 * method to return candidate status object based on id
	 * @param id
	 * @return
	 */
	public CandidateStatus findAndReturnStatusObjectById(Long id);
	/**
	 * method to delete candidates status
	 * @param candidatesStatusVo
	 */
	public void deleteCandidateStatus(CandidatesStatusVo candidatesStatusVo);
	/**
	 * method to list all status
	 * @return
	 */
	public List<CandidatesStatusVo> listAllStatus();
	/**
	 * method to find status by id
	 * @param id
	 * @return
	 */
	public CandidatesStatusVo findAndReturnStatusById(Long id);	
	/**
	 * Method to find and return status object by name
	 * @param status
	 * @return
	 */
	public CandidateStatus findAndReturnStatusByName(String status);

}
