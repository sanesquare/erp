package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Termination;
import com.hrms.web.vo.TerminationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
public interface TerminationDao {

	/**
	 * method to save new Termination
	 * 
	 * @param resignationVo
	 */
	public void saveTermination(TerminationVo terminationVo);

	/**
	 * method to save Termination Document
	 * 
	 * @param terminationVo
	 */
	public void saveTerminationDocuments(TerminationVo terminationVo);

	/**
	 * method to update Termination
	 * 
	 * @param terminationVo
	 */
	public void updateTermination(TerminationVo terminationVo);

	/**
	 * method to delete Termination
	 * 
	 * @param terminationId
	 */
	public void deleteTermination(Long terminationId);

	/**
	 * method to delete Termination Document
	 * 
	 * @param terminationDocId
	 */
	public void deleteTerminationDocument(Long terminationDocId);

	/**
	 * method to find Termination by id
	 * 
	 * @param terminationId
	 * @return Termination
	 */
	public Termination findTermination(Long terminationId);

	/**
	 * method to find Termination by terminator
	 * 
	 * @param terminator
	 * @return Termination
	 */
	public List<Termination> findTermination(String terminator);

	/**
	 * method to find Termination by date
	 * 
	 * @param terminatedOn
	 * @return Termination
	 */
	public List<Termination> findTermination(Date terminatedOn);

	/**
	 * method to find Termination by between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return Termination
	 */
	public List<Termination> findTermination(Date startDate, Date endDate);

	/**
	 * method to find Termination by id
	 * 
	 * @param resignationId
	 * @return TerminationVo
	 */
	public TerminationVo findTerminationById(Long terminationId);

	/**
	 * method to find Termination by terminator
	 * 
	 * @param terminator
	 * @return TerminationVo
	 */
	public List<TerminationVo> findTerminationByTerminator(String terminator);

	/**
	 * method to find Termination by date
	 * 
	 * @param terminatedOn
	 * @return TerminationVo
	 */
	public List<TerminationVo> findTerminationByDate(Date terminatedOn);

	/**
	 * method to find Termination by between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return TerminationVo
	 */
	public List<TerminationVo> findTerminationBetweenDates(Date startDate, Date endDate);

	/**
	 * method to find all Termination
	 * 
	 * @return List<TerminationVo>
	 */
	public List<TerminationVo> findAllTermination();

	/**
	 * method to find Termination by employeeCode
	 * 
	 * @param employeeCode
	 * @return TerminationVo
	 */
	public TerminationVo findTerminationByEmployee(String employeeCode);

	/**
	 * method to find all Termination by employee
	 * 
	 * @return List<TerminationVo>
	 */
	public List<TerminationVo> findAllTerminationByEmployee(Long employeeId);
}
