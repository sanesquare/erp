package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.Skills;
import com.hrms.web.vo.SkillVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public interface SkillDao {

	/**
	 * method to save new skill
	 * 
	 * @param skillVo
	 */
	public void saveSkill(SkillVo skillVo);

	/**
	 * method to update skill information
	 * 
	 * @param skillVo
	 */
	public void updateSkill(SkillVo skillVo);

	/**
	 * method to delete skill information
	 * 
	 * @param skillId
	 */
	public void deleteSkill(Long skillId);

	/**
	 * method to find skill by id and return skill object
	 * 
	 * @param skillId
	 * @return skills
	 */
	public Skills findSkill(Long skillId);

	/**
	 * method to find skill by id and return skillvo object
	 * 
	 * @param skillId
	 * @return skillvo
	 */
	public SkillVo findSkillById(Long skillId);

	/**
	 * method to find skill by name and return skillvo object
	 * 
	 * @param skillName
	 * @return skillvo
	 */
	public SkillVo findSkillByName(String skillName);

	/**
	 * method to find skill by name and return skill object
	 * 
	 * @param skillName
	 * @return skills
	 */
	public Skills findSkill(String skillName);

	/**
	 * method to find all skills
	 * 
	 * @return List<SkillVo>
	 */
	public List<SkillVo> findAllSkills();
}
