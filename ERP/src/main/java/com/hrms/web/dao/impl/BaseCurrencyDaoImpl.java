package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BaseCurrencyDao;
import com.hrms.web.entities.BaseCurrency;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.BaseCurrencyVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Repository
public class BaseCurrencyDaoImpl extends AbstractDao implements BaseCurrencyDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/**
	 * method to save new BaseCurrency
	 * 
	 * @param baseCurrencyVo
	 */
	public void saveBaseCurrency(BaseCurrencyVo baseCurrencyVo) {
		if (findBaseCurrency(baseCurrencyVo.getBaseCurrency()) != null)
			throw new DuplicateItemException("Duplicate Base Currency : " + baseCurrencyVo.getBaseCurrency());
		BaseCurrency baseCurrency = new BaseCurrency();
		baseCurrency.setBaseCurrency(baseCurrencyVo.getBaseCurrency());
		//baseCurrency.setSign(baseCurrencyVo.getSign());
		this.sessionFactory.getCurrentSession().save(baseCurrency);
		userActivitiesService.saveUserActivity("Added new Base Currency "+baseCurrencyVo.getBaseCurrency());
	}

	/**
	 * method to update BaseCurrency
	 * 
	 * @param baseCurrencyVo
	 */
	public void updateBaseCurrency(BaseCurrencyVo baseCurrencyVo) {
		BaseCurrency baseCurrency = findBaseCurrency(baseCurrencyVo.getBaseCurrencyId());
		if (baseCurrency == null)
			throw new ItemNotFoundException("Base Currency Not Exists.");
		baseCurrency.setBaseCurrency(baseCurrencyVo.getBaseCurrency());
		baseCurrency.setSign(baseCurrencyVo.getSign());
		this.sessionFactory.getCurrentSession().merge(baseCurrency);
		userActivitiesService.saveUserActivity("Updated Base Currency as "+baseCurrencyVo.getBaseCurrency());
	}

	/**
	 * method to delete BaseCurrency
	 * 
	 * @param baseCurrencyId
	 */
	public void deleteBaseCurrency(Long baseCurrencyId) {
		BaseCurrency baseCurrency = findBaseCurrency(baseCurrencyId);
		if (baseCurrency == null)
			throw new ItemNotFoundException("Base Currency Not Exists.");
		this.sessionFactory.getCurrentSession().delete(baseCurrency);
		userActivitiesService.saveUserActivity("Deleted Base Currency");
	}

	/**
	 * method to find BaseCurrency by id
	 * 
	 * @param baseCurrencyId
	 * @return BaseCurrency
	 */
	public BaseCurrency findBaseCurrency(Long baseCurrencyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BaseCurrency.class)
				.add(Restrictions.eq("id", baseCurrencyId));
		if (criteria.uniqueResult() == null)
			return null;
		return (BaseCurrency) criteria.uniqueResult();
	}

	/**
	 * method to find BaseCurrency by id
	 * 
	 * @param baseCurrency
	 * @return BaseCurrency
	 */
	public BaseCurrency findBaseCurrency(String baseCurrency) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BaseCurrency.class)
				.add(Restrictions.eq("baseCurrency", baseCurrency));
		if (criteria.uniqueResult() == null)
			return null;
		return (BaseCurrency) criteria.uniqueResult();
	}

	/**
	 * method to create base currencyvo
	 * 
	 * @param baseCurrency
	 * @return
	 */
	private BaseCurrencyVo createBaseCurrencyVo(BaseCurrency baseCurrency) {
		BaseCurrencyVo baseCurrencyVo = new BaseCurrencyVo();
		baseCurrencyVo.setBaseCurrency(baseCurrency.getBaseCurrency());
		baseCurrencyVo.setBaseCurrencyId(baseCurrency.getId());
		baseCurrencyVo.setSign(baseCurrency.getSign());
		return baseCurrencyVo;
	}

	/**
	 * method to find BaseCurrency by currency
	 * 
	 * @param baseCurrencyId
	 * @return BaseCurrencyVo
	 */
	public BaseCurrencyVo findBaseCurrencyById(Long baseCurrencyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BaseCurrency.class)
				.add(Restrictions.eq("id", baseCurrencyId));
		if (criteria.uniqueResult() == null)
			return null;
		return createBaseCurrencyVo((BaseCurrency) criteria.uniqueResult());
	}

	/**
	 * method to find BaseCurrency by currency
	 * 
	 * @param baseCurrency
	 * @return BaseCurrencyVo
	 */
	public BaseCurrencyVo findBaseCurrencyByCurrency(String baseCurrency) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BaseCurrency.class)
				.add(Restrictions.eq("baseCurrency", baseCurrency));
		if (criteria.uniqueResult() == null)
			return null;
		return createBaseCurrencyVo((BaseCurrency) criteria.uniqueResult());
	}

	/**
	 * method to find all BaseCurrency
	 * 
	 * @return List<BaseCurrencyVo>
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCurrencyVo> findAllBaseCurrency() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(BaseCurrency.class);
		List<BaseCurrencyVo> baseCurrencyVos = new ArrayList<BaseCurrencyVo>();
		List<BaseCurrency> baseCurrencies = criteria.list();
		for (BaseCurrency baseCurrency : baseCurrencies)
			baseCurrencyVos.add(createBaseCurrencyVo(baseCurrency));
		return baseCurrencyVos;
	}

}
