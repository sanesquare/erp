package com.hrms.web.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EsiSyntaxDao;
import com.hrms.web.entities.EsiSyntax;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.EsiSyntaxVo;

/**
 * 
 * @author Jithin Mohan
 * @since 5-May-2015
 *
 */
@Repository
public class EsiSyntaxDaoImpl extends AbstractDao implements EsiSyntaxDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveOrUpdateEsiSyntax(EsiSyntaxVo esiSyntaxVo) {
		EsiSyntax esiSyntax = findEsiSyntax(esiSyntaxVo.getEsiSyntaxId());
		if (esiSyntax == null)
			esiSyntax = new EsiSyntax();
		esiSyntax.setEmployeeSyntax(esiSyntaxVo.getEmployeeSyntax());
		esiSyntax.setEmployerSyntax(esiSyntaxVo.getEmployerSyntax());
		esiSyntax.setEsiRange(esiSyntaxVo.getRange());
		esiSyntax.setAmount(esiSyntaxVo.getAmount());
		this.sessionFactory.getCurrentSession().saveOrUpdate(esiSyntax);
	}

	public void deleteEsiSyntax(Long esiSyntaxId) {
		EsiSyntax esiSyntax = findEsiSyntax(esiSyntaxId);
		if (esiSyntax == null)
			throw new ItemNotFoundException("ESI Syntax Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(esiSyntax);
	}

	public EsiSyntax findEsiSyntax(Long esiSyntaxId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EsiSyntax.class)
				.add(Restrictions.eq("id", esiSyntaxId));
		if (criteria.uniqueResult() == null)
			return null;
		return (EsiSyntax) criteria.uniqueResult();
	}

	/**
	 * method to create EsiSyntaxVo object
	 * 
	 * @param esiSyntax
	 * @return esiSyntaxVo
	 */
	private EsiSyntaxVo createEsiSyntaxVo(EsiSyntax esiSyntax) {
		EsiSyntaxVo esiSyntaxVo = new EsiSyntaxVo();
		esiSyntaxVo.setEsiSyntaxId(esiSyntax.getId());
		esiSyntaxVo.setEmployeeSyntax(esiSyntax.getEmployeeSyntax());
		esiSyntaxVo.setEmployerSyntax(esiSyntax.getEmployerSyntax());
		esiSyntaxVo.setRange(esiSyntax.getEsiRange());
		esiSyntaxVo.setAmount(esiSyntax.getAmount());
		return esiSyntaxVo;
	}

	public EsiSyntaxVo findEsiSyntaxId(Long esiSyntaxId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EsiSyntax.class)
				.add(Restrictions.eq("id", esiSyntaxId));
		if (criteria.uniqueResult() == null)
			return null;
		return createEsiSyntaxVo((EsiSyntax) criteria.uniqueResult());
	}

	public EsiSyntaxVo findEsiSyntax() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EsiSyntax.class);
		if (criteria.uniqueResult() == null)
			return null;
		return createEsiSyntaxVo(((EsiSyntax) criteria.uniqueResult()));
	}

}
