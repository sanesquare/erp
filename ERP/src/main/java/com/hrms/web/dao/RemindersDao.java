package com.hrms.web.dao;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Reminders;
import com.hrms.web.vo.RemindersVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public interface RemindersDao {

	/**
	 * method to save Reminder
	 * 
	 * @param remindersVo
	 */
	public void saveReminder(RemindersVo remindersVo);

	/**
	 * method to update Reminder
	 * 
	 * @param remindersVo
	 */
	public void updateReminder(RemindersVo remindersVo);

	/**
	 * method to delete Reminder
	 * 
	 * @param reminderId
	 */
	public void deleteReminder(Long reminderId);

	/**
	 * method to find Reminder by id
	 * 
	 * @param reminderId
	 * @return Reminder
	 */
	public Reminders findReminder(Long reminderId);

	/**
	 * method to find Reminder by date
	 * 
	 * @param reminderDate
	 * @return Reminder
	 */
	public List<Reminders> findReminder(Date reminderDate);

	/**
	 * method to find Reminder by id
	 * 
	 * @param reminderId
	 * @return ReminderVo
	 */
	public RemindersVo findReminderById(Long reminderId);

	/**
	 * method to find Reminder by date
	 * 
	 * @param reminderDate
	 * @return ReminderVo
	 */
	public List<RemindersVo> findReminderByDate(Date reminderDate);

	/**
	 * method to find all Reminder
	 * 
	 * @return List<ReminderVo>
	 */
	public List<RemindersVo> findAllReminder();
}
