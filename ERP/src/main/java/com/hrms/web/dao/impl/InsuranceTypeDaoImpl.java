package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.InsuranceTypeDao;
import com.hrms.web.entities.InsuranceType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.InsuranceTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Repository
public class InsuranceTypeDaoImpl extends AbstractDao implements InsuranceTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveInsuranceType(InsuranceTypeVo insuranceTypeVo) {
		for (String insuranceType : StringSplitter.splitWithComma(insuranceTypeVo.getInsuranceType())) {
			if (findInsuranceType(insuranceType.trim()) != null)
				throw new DuplicateItemException("Duplicate Insurance Type : " + insuranceType.trim());
			InsuranceType type = new InsuranceType();
			type.setType(insuranceType.trim());
			this.sessionFactory.getCurrentSession().save(type);
		}
	}

	public void updateInsuranceType(InsuranceTypeVo insuranceTypeVo) {
		InsuranceType insuranceType = findInsuranceType(insuranceTypeVo.getInsuranceTypeId());
		if (insuranceType == null)
			throw new ItemNotFoundException("Insurance Type Not Exist");
		insuranceType.setType(insuranceTypeVo.getInsuranceType());
		this.sessionFactory.getCurrentSession().merge(insuranceType);
	}

	public void deleteInsuranceType(Long insuranceTypeId) {
		InsuranceType insuranceType = findInsuranceType(insuranceTypeId);
		if (insuranceType == null)
			throw new ItemNotFoundException("Insurance Type Not Exist");
		this.sessionFactory.getCurrentSession().delete(insuranceType);
	}

	public InsuranceType findInsuranceType(Long insuranceTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InsuranceType.class)
				.add(Restrictions.eq("id", insuranceTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (InsuranceType) criteria.uniqueResult();
	}

	public InsuranceType findInsuranceType(String insuranceType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InsuranceType.class)
				.add(Restrictions.eq("type", insuranceType));
		if (criteria.uniqueResult() == null)
			return null;
		return (InsuranceType) criteria.uniqueResult();
	}

	/**
	 * method to create insuranceTypeVo object
	 * 
	 * @param insuranceType
	 * @return insuranceTypeVo
	 */
	private InsuranceTypeVo createInsuranceTypeVo(InsuranceType insuranceType) {
		InsuranceTypeVo insuranceTypeVo = new InsuranceTypeVo();
		insuranceTypeVo.setInsuranceType(insuranceType.getType());
		insuranceTypeVo.setInsuranceTypeId(insuranceType.getId());
		return insuranceTypeVo;
	}

	public InsuranceTypeVo findInsuranceTypeById(Long insuranceTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InsuranceType.class)
				.add(Restrictions.eq("id", insuranceTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createInsuranceTypeVo((InsuranceType) criteria.uniqueResult());
	}

	public InsuranceTypeVo findInsuranceTypeByType(String insuranceType) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InsuranceType.class)
				.add(Restrictions.eq("type", insuranceType));
		if (criteria.uniqueResult() == null)
			return null;
		return createInsuranceTypeVo((InsuranceType) criteria.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<InsuranceTypeVo> findAllInsuranceType() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InsuranceType.class);
		List<InsuranceTypeVo> insuranceTypeVos = new ArrayList<InsuranceTypeVo>();
		List<InsuranceType> insuranceTypes = criteria.list();
		for (InsuranceType insuranceType : insuranceTypes)
			insuranceTypeVos.add(createInsuranceTypeVo(insuranceType));
		return insuranceTypeVos;
	}

}
