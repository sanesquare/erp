package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.OrganisationAssetsDao;
import com.hrms.web.entities.OrganisationAssets;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.OrganisationAssetsVo;

/**
 * 
 * @author shamsheer, Jithin Mohan
 * @since 21-March-2015
 */
@Repository
public class OrganisationAssetsDaoImpl extends AbstractDao implements OrganisationAssetsDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveAssets(OrganisationAssetsVo assetsVo) {
		OrganisationAssets assets = findAssetObjByCode(assetsVo.getTempAssetCode());
		if (assets == null) {
			assets = new OrganisationAssets();
			if (findAssetObjByCode(assetsVo.getAssetCode()) != null)
				throw new DuplicateItemException("Duplicate Asset Code : " + assetsVo.getAssetCode());
		}
		assets.setAssetCode(assetsVo.getAssetCode());
		assets.setAssetName(assetsVo.getAssetName());
		this.sessionFactory.getCurrentSession().saveOrUpdate(assets);
	}

	public void updateAssets(OrganisationAssetsVo assetsVo) {
		OrganisationAssets assets = findAssetObjById(assetsVo.getAssetId());
		if (assets == null)
			throw new ItemNotFoundException("Assets Details Not Found.");
		assets.setAssetCode(assetsVo.getAssetCode());
		assets.setAssetName(assetsVo.getAssetName());
		this.sessionFactory.getCurrentSession().merge(assets);
	}

	public void deleteAsset(Long id) {
		OrganisationAssets assets = findAssetObjById(id);
		if (assets == null)
			throw new ItemNotFoundException("Assets Details Not Found.");
		this.sessionFactory.getCurrentSession().delete(assets);
	}

	public OrganisationAssetsVo findAssetById(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganisationAssets.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return createAssetVo((OrganisationAssets) criteria.uniqueResult());
	}

	public OrganisationAssetsVo findAssetByName(String name) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganisationAssets.class)
				.add(Restrictions.eq("assetName", name));
		if (criteria.uniqueResult() == null)
			return null;
		return createAssetVo((OrganisationAssets) criteria.uniqueResult());
	}

	public OrganisationAssetsVo findAssetByCode(String code) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganisationAssets.class)
				.add(Restrictions.eq("assetCode", code));
		if (criteria.uniqueResult() == null)
			return null;
		return createAssetVo((OrganisationAssets) criteria.uniqueResult());
	}

	public OrganisationAssets findAssetObjById(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganisationAssets.class)
				.add(Restrictions.eq("id", id));
		if (criteria.uniqueResult() == null)
			return null;
		return (OrganisationAssets) criteria.uniqueResult();
	}

	public OrganisationAssets findAssetObjByName(String name) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganisationAssets.class)
				.add(Restrictions.eq("assetName", name));
		if (criteria.uniqueResult() == null)
			return null;
		return (OrganisationAssets) criteria.uniqueResult();
	}

	public OrganisationAssets findAssetObjByCode(String code) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganisationAssets.class)
				.add(Restrictions.eq("assetCode", code));
		if (criteria.uniqueResult() == null)
			return null;
		return (OrganisationAssets) criteria.uniqueResult();
	}

	/**
	 * method to list all assets
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<OrganisationAssetsVo> listAllAssets() {
		List<OrganisationAssetsVo> organisationAssetsVos = new ArrayList<OrganisationAssetsVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganisationAssets.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<OrganisationAssets> organisationAssets = criteria.list();
		for (OrganisationAssets assets : organisationAssets) {
			organisationAssetsVos.add(createAssetVo(assets));
		}
		return organisationAssetsVos;
	}

	/**
	 * method to create assetsVo
	 * 
	 * @param assets
	 * @return
	 */
	private OrganisationAssetsVo createAssetVo(OrganisationAssets assets) {
		OrganisationAssetsVo assetsVo = new OrganisationAssetsVo();
		assetsVo.setAssetCode(assets.getAssetCode());
		assetsVo.setAssetId(assets.getId());
		assetsVo.setAssetName(assets.getAssetName());
		return assetsVo;
	}
}
