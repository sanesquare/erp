package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ProjectStatusDao;
import com.hrms.web.entities.ProjectStatus;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.ProjectVo;

/**
 * 
 * @author Shamsheer
 * @since 11-March-2015
 *
 */
@Repository
public class ProjectStatusDaoImpl extends AbstractDao implements ProjectStatusDao{
	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/**
	 * method to save projectStatus
	 * @param projectVo
	 */
	public void saveProjectStatus(ProjectVo projectVo) {
		logger.info("inside>> project status dao..");
		ProjectStatus projectStatus=new  ProjectStatus();
		projectStatus.setName(projectVo.getProjectStatus());
		this.sessionFactory.getCurrentSession().saveOrUpdate(projectStatus);
	}

	/**
	 * method to update projectStatus
	 * @param projectVo
	 */
	public void updateProjectStatus(ProjectVo projectVo) {
		logger.info("inside>> project status dao..");
		ProjectStatus projectStatus=findAndReturnProjectStatusObjById(projectVo.getProjectStatusId());
		projectStatus.setName(projectVo.getProjectStatus());
		this.sessionFactory.getCurrentSession().merge(projectStatus);
	}

	/**
	 * method to delete projectStatus
	 * @param projectVo
	 */
	public void deleteProjectStatus(Long id) {
		logger.info("inside>> project status dao..");
		this.sessionFactory.getCurrentSession().delete(findAndReturnProjectStatusObjById(id));
	}

	/**
	 * method to list all projectStatus
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ProjectVo> listAllProjectStatus() {
		logger.info("inside>> project status dao..");
		List<ProjectVo> projectVos=new ArrayList<ProjectVo>();
		List<ProjectStatus> projectStatus=this.sessionFactory.getCurrentSession()
				.createCriteria(ProjectStatus.class).list();
		for(ProjectStatus projectStatusObj:projectStatus){
			projectVos.add(createProjectVo(projectStatusObj));
		}
		return projectVos;
	}

	/**
	 * method to create projectVo
	 * @param projectStatus
	 * @return
	 */
	private ProjectVo createProjectVo(ProjectStatus projectStatus){
		logger.info("inside>> project status dao..");
		ProjectVo projectVo=new ProjectVo();
		projectVo.setProjectStatus(projectStatus.getName());
		projectVo.setProjectStatusId(projectStatus.getId());
		return projectVo;
	}


	/**
	 * method to find and return ProjectStatus object
	 * @param id
	 * @return
	 */
	public ProjectStatus findAndReturnProjectStatusObjById(Long id) {
		logger.info("inside>> project status dao..findAndReturnProjectStatusObjById");
		return (ProjectStatus) this.sessionFactory.getCurrentSession().createCriteria(ProjectStatus.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	/**
	 * method to find and return ProjectStatus object by status
	 * @param status
	 * @return
	 */
	public ProjectStatus findAndReturnProjectStatusObjectByStatus(String status) {
		return (ProjectStatus) this.sessionFactory.getCurrentSession().createCriteria(ProjectStatus.class)
				.add(Restrictions.eq("name", status)).uniqueResult();
	}
}
