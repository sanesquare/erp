package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ForwardApplicationStatusDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.dao.TravelArrangementTypeDao;
import com.hrms.web.dao.TravelDao;
import com.hrms.web.dao.TravelModeDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.entities.TransferDocuments;
import com.hrms.web.entities.Travel;
import com.hrms.web.entities.TravelArrangementType;
import com.hrms.web.entities.TravelDestinations;
import com.hrms.web.entities.TravelDocuments;
import com.hrms.web.entities.TravelMode;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.TempNotificationVo;
import com.hrms.web.vo.TravelDestinationVo;
import com.hrms.web.vo.TravelDocumentVo;
import com.hrms.web.vo.TravelVo;

/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
@Repository
public class TravelDaoImpl extends AbstractDao implements TravelDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private TravelModeDao travelModeDao;

	@Autowired
	private TravelArrangementTypeDao arrangementTypeDao;

	@Autowired
	private ForwardApplicationStatusDao statusDao;
	
	@Autowired
	private TempNotificationDao notificationDao;
	

	/**
	 * method to save travel
	 */
	public Long saveTravel(TravelVo travelVo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(travelVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		Travel travel = new Travel();
		travel.setEmployee(employee);

		if (travelVo.getSuperiorCode() != "") {
			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject("A Travel Request Has Been Received From "+employee.getUserProfile().getFirstName()
					+" "+employee.getUserProfile().getLastname()+".");
			notificationVo.getEmployeeCodes().add(travelVo.getSuperiorCode());
			notificationDao.saveNotification(notificationVo);
			travel.setForwardApplicationTo(employeeDao.findAndReturnEmployeeByCode(travelVo.getSuperiorCode()));
			ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.REQUESTED);
			travel.setStatus(status);
			travel.setStatusDescription(travelVo.getStatusDescription());
		}

		travel.setPurposeOfVisit(travelVo.getPurposeOfVisit());
		travel.setTravelDescription(travelVo.getDescription());
		if (travelVo.getStartDate() != "")
			travel.setTravelStartDate(DateFormatter.convertStringToDate(travelVo.getStartDate()));
		if (travelVo.getEndDate() != "")
			travel.setTravelEndDate(DateFormatter.convertStringToDate(travelVo.getEndDate()));
		if (travelVo.getExpectedBudget() != "")
			travel.setExpectedTravelBudget(new BigDecimal(travelVo.getExpectedBudget()));
		if (travelVo.getActualBudget() != "")
			travel.setActualTravelBudget(new BigDecimal(travelVo.getActualBudget()));
		travel.setNotes(travelVo.getNotes());
		if (!travelVo.getDestinationVos().isEmpty()) {
			for (TravelDestinationVo vo : travelVo.getDestinationVos()) {
				TravelDestinations destinations = setDestinations(vo, travel);
				if (destinations != null)
					travel.getDestinations().add(destinations);
			}
		}
		return (Long) this.sessionFactory.getCurrentSession().save(travel);
	}

	/**
	 * method to set destinations
	 * 
	 * @return
	 */
	private TravelDestinations setDestinations(TravelDestinationVo vo, Travel travel) {
		if (vo.getArrangementTypeId() != null && vo.getTravelModeId() != null) {
			TravelDestinations destination = new TravelDestinations();
			destination.setTravel(travel);
			TravelMode travelMode = travelModeDao.getTravelMode(vo.getTravelModeId());
			if (travelMode == null)
				throw new ItemNotFoundException("Travel Mode Not Exist.");
			destination.setTravelMode(travelMode);
			TravelArrangementType arrangementType = arrangementTypeDao.getArrangementType(vo.getArrangementTypeId());
			if (arrangementType == null)
				throw new ItemNotFoundException("Travel Arrangement Type Not Exist.");
			destination.setTravelArrangementType(arrangementType);
			destination.setPlaceOfVisit(vo.getPlace());
			return destination;
		}
		return null;
	}

	/**
	 * method to update travel
	 */
	public void updateTravel(TravelVo travelVo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(travelVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		Travel travel = getTravel(travelVo.getTravelId());
		if (travel == null)
			throw new ItemNotFoundException("Travel Not Exist.");
		travel.getDestinations().clear();
		travel.setEmployee(employee);

		if (travelVo.getSuperiorCode() != "") {
			travel.setForwardApplicationTo(employeeDao.findAndReturnEmployeeByCode(travelVo.getSuperiorCode()));
			ForwardApplicationStatus status = statusDao.getStatus(StatusConstants.REQUESTED);
			travel.setStatus(status);
			travel.setStatusDescription(travelVo.getStatusDescription());
		}

		travel.setPurposeOfVisit(travelVo.getPurposeOfVisit());
		travel.setTravelDescription(travelVo.getDescription());
		if (travelVo.getStartDate() != "")
			travel.setTravelStartDate(DateFormatter.convertStringToDate(travelVo.getStartDate()));
		if (travelVo.getEndDate() != "")
			travel.setTravelEndDate(DateFormatter.convertStringToDate(travelVo.getEndDate()));
		if (travelVo.getExpectedBudget() != "")
			travel.setExpectedTravelBudget(new BigDecimal(travelVo.getExpectedBudget()));
		if (travelVo.getActualBudget() != "")
			travel.setActualTravelBudget(new BigDecimal(travelVo.getActualBudget()));
		travel.setNotes(travelVo.getNotes());
		if (!travelVo.getDestinationVos().isEmpty()) {
			for (TravelDestinationVo vo : travelVo.getDestinationVos()) {
				TravelDestinations destinations = setDestinations(vo, travel);
				if (destinations != null)
					travel.getDestinations().add(destinations);
			}
		}
		this.sessionFactory.getCurrentSession().merge(travel);
	}

	/**
	 * method to delete travel
	 */
	public void deleteTravel(Long travelId) {
		Travel travel = getTravel(travelId);
		if (travel == null)
			throw new ItemNotFoundException("Travel Not Exist.");
		if (travel.getDocuments() != null) {
			SFTPOperation operation = new SFTPOperation();
			for (TravelDocuments documents : travel.getDocuments()) {
				operation.removeFileFromSFTP(documents.getUrl());
			}
		}
		this.sessionFactory.getCurrentSession().delete(travel);
	}

	/**
	 * method to get travel object by id
	 */
	public Travel getTravel(Long travelId) {
		return (Travel) this.sessionFactory.getCurrentSession().createCriteria(Travel.class)
				.add(Restrictions.eq("id", travelId)).uniqueResult();
	}

	/**
	 * method to get travel by id
	 */
	public TravelVo getTravelById(Long travelId) {
		Travel travel = getTravel(travelId);
		if (travel == null)
			throw new ItemNotFoundException("Travel Not Exist.");
		return createTravelVo(travel);
	}

	/**
	 * method to list all travels
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TravelVo> listAllTravels() {
		List<TravelVo> vos = new ArrayList<TravelVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Travel.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Travel> travels = criteria.list();
		for (Travel travel : travels) {
			vos.add(createTravelVo(travel));
		}
		return vos;
	}

	/**
	 * method to list travels by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TravelVo> listAllTravelsByEmployee(Long employeeId) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<TravelVo> vos = new ArrayList<TravelVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Travel.class);
		criteria.createAlias("forwardApplicationTo", "employee");
		Criterion sender = Restrictions.eq("employee", employee);
		Criterion receiver = Restrictions.eq("employee.employeeCode", employee.getEmployeeCode());
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Travel> travels = criteria.list();
		for (Travel travel : travels) {
			vos.add(createTravelVo(travel));
		}
		return vos;
	}

	/**
	 * method to updaqte documents
	 */
	public void updateDocuments(TravelVo travelVo) {
		Travel travel = getTravel(travelVo.getTravelId());
		if (travel == null)
			throw new ItemNotFoundException("Travel Not Exist.");
		travel.setDocuments(setDocuments(travelVo.getDocumentVos(), travel));
		this.sessionFactory.getCurrentSession().merge(travel);
	}

	/**
	 * method to set documents
	 * 
	 * @param documentVos
	 * @param travel
	 * @return
	 */
	private Set<TravelDocuments> setDocuments(List<TravelDocumentVo> documentVos, Travel travel) {
		Set<TravelDocuments> documents = new HashSet<TravelDocuments>();
		for (TravelDocumentVo vo : documentVos) {
			TravelDocuments document = new TravelDocuments();
			document.setTravel(travel);
			document.setUrl(vo.getUrl());
			document.setName(vo.getFileName());
			documents.add(document);
		}
		return documents;
	}

	/**
	 * method to delte document
	 */
	public void deleteDocument(String url) {
		TravelDocuments documents = (TravelDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(TravelDocuments.class).add(Restrictions.eq("url", url)).uniqueResult();
		Travel travel = documents.getTravel();
		travel.getDocuments().remove(documents);
		this.sessionFactory.getCurrentSession().merge(travel);
		this.sessionFactory.getCurrentSession().delete(documents);
	}

	/**
	 * method to update status
	 */
	public void updateStatus(TravelVo travelVo) {
		Travel travel = getTravel(travelVo.getTravelId());
		if (travel == null)
			throw new ItemNotFoundException("Travel Not Exist.");
		travel.setStatusDescription(travelVo.getStatusDescription());
		ForwardApplicationStatus status = statusDao.getStatus(travelVo.getStatus());
		travel.setStatus(status);
		if (travelVo.getSuperiorCode() != "") {
			travel.setForwardApplicationTo(employeeDao.findAndReturnEmployeeByCode(travelVo.getSuperiorCode()));
		} else {
			travel.setForwardApplicationTo(null);
		}
		
		TempNotificationVo notificationVo  = new TempNotificationVo();
		notificationVo.getEmployeeIds().add(travel.getEmployee().getId());
		notificationVo.setSubject(travel.getEmployee().getEmployeeCode()+"'s Travel Request "+travelVo.getStatus());
		notificationDao.saveNotification(notificationVo);
		
		this.sessionFactory.getCurrentSession().merge(travel);
	}

	/**
	 * method to create travel vo
	 * 
	 * @param travel
	 * @return
	 */
	private TravelVo createTravelVo(Travel travel) {
		TravelVo vo = new TravelVo();
		vo.setTravelId(travel.getId());
		if (travel.getEmployee().getUserProfile() != null)
			vo.setEmployee(travel.getEmployee().getUserProfile().getFirstName() + " "
					+ travel.getEmployee().getUserProfile().getLastname());
		vo.setEmployeeCode(travel.getEmployee().getEmployeeCode());

		if (travel.getForwardApplicationTo() != null) {
			if (travel.getForwardApplicationTo().getUserProfile() != null)
				vo.setSuperior(travel.getForwardApplicationTo().getUserProfile().getFirstName() + " "
						+ travel.getForwardApplicationTo().getUserProfile().getLastname());

			vo.setSuperiorCode(travel.getForwardApplicationTo().getEmployeeCode());
			vo.setSuperiorCodeAjax(travel.getForwardApplicationTo().getEmployeeCode());
		}
		vo.setPurposeOfVisit(travel.getPurposeOfVisit());
		vo.setStatus(travel.getStatus().getName());
		vo.setStatusId(travel.getStatus().getId());
		vo.setStatusDescription(travel.getStatusDescription());
		vo.setCreatedBy(travel.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(travel.getCreatedOn()));
		vo.setNotes(travel.getNotes());

		if (travel.getTravelStartDate() != null)
			vo.setStartDate(DateFormatter.convertDateToString(travel.getTravelStartDate()));
		if (travel.getTravelEndDate() != null)
			vo.setEndDate(DateFormatter.convertDateToString(travel.getTravelEndDate()));

		if (travel.getExpectedTravelBudget() != null)
			vo.setExpectedBudget(travel.getExpectedTravelBudget().toString());
		if (travel.getActualTravelBudget() != null)
			vo.setActualBudget(travel.getActualTravelBudget().toString());

		if (travel.getDestinations() != null)
			vo.setDestinationVos(createDestinationVos(travel.getDestinations()));
		vo.setDescription(travel.getTravelDescription());

		if (travel.getDocuments() != null)
			vo.setDocumentVos(createDocumentVo(travel.getDocuments()));
		return vo;
	}

	/**
	 * method to create destination vos
	 * 
	 * @param destinations
	 * @return
	 */
	private List<TravelDestinationVo> createDestinationVos(Set<TravelDestinations> destinations) {
		List<TravelDestinationVo> destinationVos = new ArrayList<TravelDestinationVo>();
		for (TravelDestinations destination : destinations) {
			TravelDestinationVo destinationVo = new TravelDestinationVo();
			destinationVo.setArrangementType(destination.getTravelArrangementType().getName());
			destinationVo.setArrangementTypeId(destination.getTravelArrangementType().getId());
			destinationVo.setTravelId(destination.getTravel().getId());
			destinationVo.setTravelMode(destination.getTravelMode().getName());
			destinationVo.setTravelModeId(destination.getTravelMode().getId());
			destinationVo.setPlace(destination.getPlaceOfVisit());
			destinationVos.add(destinationVo);
		}
		return destinationVos;
	}

	/**
	 * method to create document vos
	 * 
	 * @param documents
	 * @return
	 */
	private List<TravelDocumentVo> createDocumentVo(Set<TravelDocuments> documents) {
		List<TravelDocumentVo> documentVos = new ArrayList<TravelDocumentVo>();
		for (TravelDocuments document : documents) {
			TravelDocumentVo vo = new TravelDocumentVo();
			vo.setUrl(document.getUrl());
			vo.setFileName(document.getName());
			documentVos.add(vo);
		}
		return documentVos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TravelVo> findTravelsByEmployeeBetweenDates(Long employeeId, String startDate, String endDate) {
		Date startdate = DateFormatter.convertStringToDate(startDate);
		Date enddate = DateFormatter.convertStringToDate(endDate);
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<TravelVo> vos = new ArrayList<TravelVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Travel.class);
		criteria.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.ge("travelStartDate", startdate)).add(Restrictions.le("travelStartDate", enddate));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Travel> travels = criteria.list();
		for (Travel travel : travels) {
			vos.add(createTravelVo(travel));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TravelVo> findAllTravelsByEmployee(String employeeCode) {
		List<TravelVo> vos = new ArrayList<TravelVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Travel.class);
		criteria.createAlias("forwardApplicationTo", "employee");
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("employee", employee);
		Criterion receiver = Restrictions.eq("employee.employeeCode", employeeCode);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Travel> travels = criteria.list();
		for (Travel travel : travels) {
			vos.add(createTravelVo(travel));
		}
		return vos;
	}
}
