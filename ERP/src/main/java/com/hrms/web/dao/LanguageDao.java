package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.Language;
import com.hrms.web.vo.LanguageVo;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
public interface LanguageDao {

	/**
	 * method to save new language
	 * 
	 * @param languageVo
	 */
	public void saveLanguage(LanguageVo languageVo);

	/**
	 * method to update language information
	 * 
	 * @param languageVo
	 */
	public void updateLanguage(LanguageVo languageVo);

	/**
	 * method to delete language information
	 * 
	 * @param languageId
	 */
	public void deleteLanguage(Long languageId);

	/**
	 * method to find language by id and return language object
	 * 
	 * @param languageId
	 * @return language
	 */
	public Language findLanguage(Long languageId);

	/**
	 * method to find language by id and return languagevo
	 * 
	 * @param languageId
	 * @return languageVo
	 */
	public LanguageVo findLanguageById(Long languageId);

	/**
	 * method to find language by name and return languageVO object
	 * 
	 * @param languageName
	 * @return languageVo
	 */
	public LanguageVo findLanguageByName(String languageName);

	/**
	 * method to find all language
	 * 
	 * @return List<LanguageVo>
	 */
	public List<LanguageVo> findAllLanguage();

	/**
	 * method to find language by name and return language object
	 * 
	 * @param languageName
	 * @return language
	 */
	public Language findLanguage(String languageName);
}
