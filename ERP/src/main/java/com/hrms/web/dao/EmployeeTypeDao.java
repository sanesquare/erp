package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.EmployeeType;
import com.hrms.web.vo.EmployeeTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public interface EmployeeTypeDao {

	/**
	 * method to save new EmployeeType
	 * 
	 * @param employeeTypeVo
	 */
	public void saveEmployeeType(EmployeeTypeVo employeeTypeVo);

	/**
	 * method to update EmployeeType
	 * 
	 * @param employeeTypeVo
	 */
	public void updateEmployeeType(EmployeeTypeVo employeeTypeVo);

	/**
	 * method to delete EmployeeType
	 * 
	 * @param employeeTypeId
	 */
	public void deleteEmployeeType(Long employeeTypeId);

	/**
	 * method to find EmployeeType by id
	 * 
	 * @param employeeTypeId
	 * @return EmployeeType
	 */
	public EmployeeType findEmployeeType(Long employeeTypeId);

	/**
	 * method to find EmployeeType by type
	 * 
	 * @param employeeTypeId
	 * @return EmployeeType
	 */
	public EmployeeType findEmployeeType(String employeeType);

	/**
	 * method to find EmployeeType by id
	 * 
	 * @param employeeTypeId
	 * @return EmployeeTypeVo
	 */
	public EmployeeTypeVo findEmployeeTypeById(Long employeeTypeId);

	/**
	 * method to find EmployeeType by type
	 * 
	 * @param employeeType
	 * @return EmployeeTypeVo
	 */
	public EmployeeTypeVo findEmployeeTypeByType(String employeeType);

	/**
	 * method to find all EmployeeType
	 * 
	 * @return List<EmployeeTypeVo>
	 */
	public List<EmployeeTypeVo> findAllEmployeeType();
}
