package com.hrms.web.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */

public class AbstractDao {

	@Autowired
	protected SessionFactory sessionFactory;
}
