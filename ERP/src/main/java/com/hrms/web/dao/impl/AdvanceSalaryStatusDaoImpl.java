package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AdvanceSalaryStatusDao;
import com.hrms.web.entities.AdvanceSalaryStatus;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.AdvanceSalaryStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Repository
public class AdvanceSalaryStatusDaoImpl extends AbstractDao implements AdvanceSalaryStatusDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void saveAdvanceSalaryStatus(AdvanceSalaryStatusVo advanceSalaryStatusVo) {
		for (String status : StringSplitter.splitWithComma(advanceSalaryStatusVo.getStatus())) {
			if (findAdvanceSalaryStatus(status.trim()) != null)
				throw new DuplicateItemException("Duplicate Advance Salary Status : " + status.trim());
			AdvanceSalaryStatus advanceSalaryStatus = findAdvanceSalaryStatus(advanceSalaryStatusVo.getTempStatus());
			if (advanceSalaryStatus == null)
				advanceSalaryStatus = new AdvanceSalaryStatus();
			advanceSalaryStatus.setStatus(status.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(advanceSalaryStatus);
			userActivitiesService.saveUserActivity("Added new Advance Salary Status " + status.trim());
		}
	}

	public void updateAdvanceSalaryStatus(AdvanceSalaryStatusVo advanceSalaryStatusVo) {
		// TODO Auto-generated method stub

	}

	public void deleteAdvanceSalaryStatus(Long advanceSalaryStatusId) {
		AdvanceSalaryStatus advanceSalaryStatus = findAdvanceSalaryStatus(advanceSalaryStatusId);
		if (advanceSalaryStatus == null)
			throw new ItemNotFoundException("Advance Salary Status Details Not Found.");
		String title = advanceSalaryStatus.getStatus();
		this.sessionFactory.getCurrentSession().delete(advanceSalaryStatus);
		userActivitiesService.saveUserActivity("Deleted Advance Salary Status " + title);
	}

	public AdvanceSalaryStatus findAdvanceSalaryStatus(Long advanceSalaryStatusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalaryStatus.class)
				.add(Restrictions.eq("id", advanceSalaryStatusId));
		if (criteria.uniqueResult() == null)
			return null;
		return (AdvanceSalaryStatus) criteria.uniqueResult();
	}

	public AdvanceSalaryStatus findAdvanceSalaryStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalaryStatus.class)
				.add(Restrictions.eq("status", status));
		if (criteria.uniqueResult() == null)
			return null;
		return (AdvanceSalaryStatus) criteria.uniqueResult();
	}

	/**
	 * method to create AdvanceSalaryStatusVo object
	 * 
	 * @param status
	 * @return advanceSalaryStatusVo
	 */
	private AdvanceSalaryStatusVo createAdvanceSalaryStatusVo(AdvanceSalaryStatus status) {
		AdvanceSalaryStatusVo advanceSalaryStatusVo = new AdvanceSalaryStatusVo();
		advanceSalaryStatusVo.setAdvanceSalaryStatusId(status.getId());
		advanceSalaryStatusVo.setStatus(status.getStatus());
		return advanceSalaryStatusVo;
	}

	public AdvanceSalaryStatusVo findAdvanceSalaryStatusById(Long advanceSalaryStatusId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalaryStatus.class)
				.add(Restrictions.eq("id", advanceSalaryStatusId));
		if (criteria.uniqueResult() == null)
			return null;
		return createAdvanceSalaryStatusVo((AdvanceSalaryStatus) criteria.uniqueResult());
	}

	public AdvanceSalaryStatusVo findAdvanceSalaryStatusByStatus(String status) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalaryStatus.class)
				.add(Restrictions.eq("status", status));
		if (criteria.uniqueResult() == null)
			return null;
		return createAdvanceSalaryStatusVo((AdvanceSalaryStatus) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<AdvanceSalaryStatusVo> findAllAdvanceSalaryStatus() {
		List<AdvanceSalaryStatusVo> advanceSalaryStatusVos = new ArrayList<AdvanceSalaryStatusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdvanceSalaryStatus.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<AdvanceSalaryStatus> advanceSalaryStatus = criteria.list();
		for (AdvanceSalaryStatus salaryStatus : advanceSalaryStatus)
			advanceSalaryStatusVos.add(createAdvanceSalaryStatusVo(salaryStatus));
		return advanceSalaryStatusVos;
	}

}
