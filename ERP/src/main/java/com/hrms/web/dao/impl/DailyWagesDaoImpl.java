package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.DailyWagesDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.entities.DailyWages;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.DailyWagesVo;

/**
 * 
 * @author Shamsheer
 * @since 16-April-2015
 */
@Repository
public class DailyWagesDaoImpl extends AbstractDao implements DailyWagesDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	/**
	 * method to save daily wage
	 */
	public Long saveDailyWage(DailyWagesVo vo) {
		DailyWages dailyWages = new DailyWages();

		Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		dailyWages.setEmployee(employee);

		dailyWages.setTitle(vo.getTitle());
		dailyWages.setWage(new BigDecimal(vo.getWage()));
		dailyWages.setDescription(vo.getDescription());
		dailyWages.setNotes(vo.getNotes());

		return (Long) this.sessionFactory.getCurrentSession().save(dailyWages);
	}

	/**
	 * method to update daily wages
	 */
	public void updateDailyWage(DailyWagesVo vo) {
		DailyWages dailyWages = getDailyWage(vo.getId());
		if (dailyWages == null)
			throw new ItemNotFoundException("Daily Wage Not Exist.");

		Employee employee = employeeDao.findAndReturnEmployeeByCode(vo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		dailyWages.setEmployee(employee);

		dailyWages.setTitle(vo.getTitle());
		dailyWages.setWage(new BigDecimal(vo.getWage()));
		dailyWages.setDescription(vo.getDescription());
		dailyWages.setNotes(vo.getNotes());

		this.sessionFactory.getCurrentSession().merge(dailyWages);
	}

	/**
	 * method to delete daily wages
	 */
	public void deleteDailyWage(Long wageId) {
		DailyWages dailyWages = getDailyWage(wageId);
		if (dailyWages == null)
			throw new ItemNotFoundException("Daily Wage Not Exist.");
		this.sessionFactory.getCurrentSession().delete(dailyWages);
	}

	/**
	 * method to get daily wages object
	 */
	public DailyWages getDailyWage(Long wageId) {
		return (DailyWages) this.sessionFactory.getCurrentSession().createCriteria(DailyWages.class)
				.add(Restrictions.eq("id", wageId)).uniqueResult();
	}

	/**
	 * method to create daily wages vo
	 * 
	 * @param dailyWages
	 * @return
	 */
	private DailyWagesVo createDailyWagesVo(DailyWages dailyWages) {
		DailyWagesVo vo = new DailyWagesVo();
		vo.setId(dailyWages.getId());
		vo.setEmployeeId(dailyWages.getEmployee().getId());
		vo.setEmployeeCode(dailyWages.getEmployee().getEmployeeCode());
		if (dailyWages.getEmployee().getUserProfile() != null)
			vo.setEmployee(dailyWages.getEmployee().getUserProfile().getFirstName() + " "
					+ dailyWages.getEmployee().getUserProfile().getLastname());
		vo.setCreatedBy(dailyWages.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(dailyWages.getCreatedOn()));
		vo.setTitle(dailyWages.getTitle());
		vo.setDescription(dailyWages.getDescription());
		vo.setNotes(dailyWages.getNotes());
		vo.setWage(dailyWages.getWage().toString());
		return vo;
	}

	/**
	 * method to list all daily wages
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<DailyWagesVo> listAllDailyWages() {
		List<DailyWagesVo> vos = new ArrayList<DailyWagesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(DailyWages.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<DailyWages> wages = criteria.list();
		for (DailyWages wage : wages) {
			vos.add(createDailyWagesVo(wage));
		}
		return vos;
	}

	/**
	 * method to get dialy wage by id
	 */
	public DailyWagesVo getDailyWagesById(Long dailyWagesId) {
		DailyWages dailyWages = getDailyWage(dailyWagesId);
		if (dailyWages == null)
			throw new ItemNotFoundException("Daily Wage Not Exist.");
		return createDailyWagesVo(dailyWages);
	}

	/**
	 * method to get daily wage by employee
	 */
	public DailyWagesVo getDailyWagesByEmployeeId(String employeeCode) {
		DailyWages dailyWages = getDailyWageByEmployee(employeeCode);
		if (dailyWages == null)
			throw new ItemNotFoundException("Daily Wage Not Exist.");
		return createDailyWagesVo(dailyWages);
	}

	/**
	 * method to get dailyWageObject by employee
	 */
	public DailyWages getDailyWageByEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		return (DailyWages) this.sessionFactory.getCurrentSession().createCriteria(DailyWages.class)
				.add(Restrictions.eq("employee", employee)).uniqueResult();
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<DailyWagesVo> findAllDailyWagesByEmployee(String employeeCode) {
		List<DailyWagesVo> vos = new ArrayList<DailyWagesVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(DailyWages.class)
				.add(Restrictions.eq("employee", employeeDao.findAndReturnEmployeeByCode(employeeCode)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<DailyWages> wages = criteria.list();
		for (DailyWages wage : wages) {
			vos.add(createDailyWagesVo(wage));
		}
		return vos;
	}
}
