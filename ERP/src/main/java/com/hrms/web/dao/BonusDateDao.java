package com.hrms.web.dao;

import com.hrms.web.entities.BonusDate;
import com.hrms.web.vo.BonusDateVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public interface BonusDateDao {

	/**
	 * method to save or update BonusDate
	 * 
	 * @param bonusDateVo
	 */
	public void saveOrUpdateBonusDate(BonusDateVo bonusDateVo);

	/**
	 * method to delete BonusDate
	 * 
	 * @param bonusDateId
	 */
	public void deleteBonusDate(Long bonusDateId);

	/**
	 * method to find BonusDate by id
	 * 
	 * @param bonusDateId
	 * @return BonusDate
	 */
	public BonusDate findBonusDate(Long bonusDateId);

	/**
	 * method to find BonusDate by id
	 * 
	 * @param bonusDateId
	 * @return BonusDateVo
	 */
	public BonusDateVo findBonusDateById(Long bonusDateId);

	/**
	 * method to find BonusDate
	 * 
	 * @return BonusDateVo
	 */
	public BonusDateVo findBonusDate();

}
