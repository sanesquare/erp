package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.AdjustmentTitleDao;
import com.hrms.web.entities.AdjustmentTitle;
import com.hrms.web.entities.CommissionTitles;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.AdjustmentTitleVo;
import com.hrms.web.vo.CommissionTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 14-April-2015
 */
@Repository
public class AdjustmentTitleDaoImpl extends AbstractDao implements AdjustmentTitleDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveAdjustmentTitle(AdjustmentTitleVo titleVo) {
		for(String title : StringSplitter.splitWithComma(titleVo.getTitle().trim())){
			if(findAdjustmentTitle(title.trim())!=null)
				throw new DuplicateItemException("Duplicate Entry : "+title.trim());
			AdjustmentTitle adjustmentTitle = new AdjustmentTitle();
			adjustmentTitle.setTitle(title.trim());
			this.sessionFactory.getCurrentSession().save(adjustmentTitle);
			userActivitiesService.saveUserActivity("Added new Adjustment Title "+adjustmentTitle.getTitle());
		}
		
	}

	public void updateAdjustmentTitle(AdjustmentTitleVo titleVo) {
		// TODO Auto-generated method stub
		
	}

	public void deleteAdjustmentTitle(Long adjustmentTitleId) {
		AdjustmentTitle adjustmentTitle = findAdjustmentTitle(adjustmentTitleId);
		if(adjustmentTitle==null)
			throw new ItemNotFoundException("Adjustment Title Not Found.  ");
		String title=adjustmentTitle.getTitle();
		this.sessionFactory.getCurrentSession().delete(adjustmentTitle);
		userActivitiesService.saveUserActivity("Deleted adjustment Title "+title);
		
	}

	public void deleteAdjustmentTitle(String adjustmentTitle) {
		AdjustmentTitle adjustmentTitles = findAdjustmentTitle(adjustmentTitle);
		if(adjustmentTitles==null)
			throw new ItemNotFoundException("Adjustment Title Not Found.  ");
		this.sessionFactory.getCurrentSession().delete(adjustmentTitles);
		
	}

	public AdjustmentTitle findAdjustmentTitle(Long adjustmentTitleId) {
		return (AdjustmentTitle) this.sessionFactory.getCurrentSession().createCriteria(AdjustmentTitle.class)
				.add(Restrictions.eq("id", adjustmentTitleId)).uniqueResult();
	}

	public AdjustmentTitle findAdjustmentTitle(String adjustmentTitle) {
		return (AdjustmentTitle) this.sessionFactory.getCurrentSession().createCriteria(AdjustmentTitle.class)
				.add(Restrictions.eq("title", adjustmentTitle)).uniqueResult();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<AdjustmentTitleVo> listAllAdjustmentTitles() {
		List<AdjustmentTitleVo> vos = new ArrayList<AdjustmentTitleVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(AdjustmentTitle.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<AdjustmentTitle> titles = criteria.list();
		for(AdjustmentTitle title : titles){
			vos.add(createAdjustmentTitleVo(title));
		}
		return vos;
	}

	private AdjustmentTitleVo createAdjustmentTitleVo(AdjustmentTitle titles){
		AdjustmentTitleVo vo = new AdjustmentTitleVo();
		vo.setId(titles.getId());
		vo.setTitle(titles.getTitle());
		return vo;
	}
}
