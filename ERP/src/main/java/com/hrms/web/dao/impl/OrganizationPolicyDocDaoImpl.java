package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.OrganizationPolicyDocDao;
import com.hrms.web.entities.OrganizationPolicyDoc;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.vo.OrganizationPolicyDocVo;

/**
 * 
 * @author Jithin Mohan
 * @since 5-June-2015
 *
 */
@Repository
public class OrganizationPolicyDocDaoImpl extends AbstractDao implements OrganizationPolicyDocDao {

	public void saveOrUpdateOrganizationPolicyDoc(OrganizationPolicyDocVo docVo) {
		OrganizationPolicyDoc doc = new OrganizationPolicyDoc();
		doc.setDocumentName(docVo.getDocumentName());
		doc.setDocumentUrl(docVo.getDocumentUrl());
		this.sessionFactory.getCurrentSession().saveOrUpdate(doc);
	}

	public void deleteOrganizationPolicyDoc(Long documentId) {
		OrganizationPolicyDoc doc = findOrganizationPolicyDoc(documentId);
		if (doc == null)
			throw new ItemNotFoundException("Organization policy document not found.");
		this.sessionFactory.getCurrentSession().delete(doc);
	}

	public OrganizationPolicyDoc findOrganizationPolicyDoc(Long documentId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicyDoc.class)
				.add(Restrictions.eq("id", documentId));
		if (criteria.uniqueResult() == null)
			return null;
		return (OrganizationPolicyDoc) criteria.uniqueResult();
	}

	/**
	 * Method to create OrganizationPolicyDocVo object
	 * 
	 * @param doc
	 * @return
	 */
	private OrganizationPolicyDocVo createOrganizationPolicyDocVo(OrganizationPolicyDoc doc) {
		OrganizationPolicyDocVo docVo = new OrganizationPolicyDocVo();
		docVo.setDocumentId(doc.getId());
		docVo.setDocumentName(doc.getDocumentName());
		docVo.setDocumentUrl(doc.getDocumentUrl());
		return docVo;
	}

	public OrganizationPolicyDocVo findOrganizationPolicyDocById(Long documentId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicyDoc.class)
				.add(Restrictions.eq("id", documentId));
		if (criteria.uniqueResult() == null)
			return null;
		return createOrganizationPolicyDocVo((OrganizationPolicyDoc) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<OrganizationPolicyDocVo> findAllOrganizationPolicyDocs() {
		List<OrganizationPolicyDocVo> docVos = new ArrayList<OrganizationPolicyDocVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationPolicyDoc.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<OrganizationPolicyDoc> docs = criteria.list();
		for (OrganizationPolicyDoc doc : docs)
			docVos.add(createOrganizationPolicyDocVo(doc));
		return docVos;
	}

}
