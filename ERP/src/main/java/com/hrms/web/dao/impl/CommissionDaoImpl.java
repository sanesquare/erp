package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.CommissionDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ForwardApplicationStatusDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.CommissionTitles;
import com.hrms.web.entities.Commissions;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.CommissionVo;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
@Repository
public class CommissionDaoImpl extends AbstractDao implements CommissionDao {

	@Log
	private Logger logger;

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private ForwardApplicationStatusDao statusDao;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private TempNotificationDao notificationDao;

	/**
	 * method to save commission
	 */
	public Long saveCommission(CommissionVo commissionVo) {
		Commissions commission = new Commissions();

		Employee employee = employeeDao.findAndReturnEmployeeByCode(commissionVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		commission.setEmployee(employee);

		ForwardApplicationStatus status = null;
		if (commissionVo.getSuperiorIds() != null && !commissionVo.getSuperiorIds().contains(0L)) {
			status = statusDao.getStatus(StatusConstants.REQUESTED);// Requested
			commission.setForwardApplicationTo(setSuperiors(commissionVo.getSuperiorIds()));

			TempNotificationVo notificationVo = new TempNotificationVo();
			notificationVo.setSubject("A Request For Commission Has Been Received From "
					+ employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname() + ".");
			notificationVo.getEmployeeIds().addAll(commissionVo.getSuperiorIds());
			notificationDao.saveNotification(notificationVo);
		} else {
			status = statusDao.getStatus(StatusConstants.APPROVED);// Approved
		}
		commission.setStatus(status);

		CommissionTitles title = new CommissionTitles();
		title.setId(commissionVo.getTitleId());
		commission.setTitles(title);

		commission.setNotes(commissionVo.getNotes());
		commission.setDescription(commissionVo.getDescription());

		commission.setAmount(new BigDecimal(commissionVo.getAmount()));

		if (!commissionVo.getDate().isEmpty())
			commission.setDate(DateFormatter.convertStringToDate(commissionVo.getDate()));
		userActivitiesService.saveUserActivity("Added new Commission " + title);
		return (Long) this.sessionFactory.getCurrentSession().save(commission);
	}

	/**
	 * method to set superiors
	 * 
	 * @param vo
	 * @param commissions
	 * @return
	 */
	private Set<Employee> setSuperiors(List<Long> superiorIds) {
		Set<Employee> superiors = new HashSet<Employee>();
		for (Long id : superiorIds) {
			if (id != null) {
				Employee superior = new Employee();
				superior.setId(id);
				superiors.add(superior);
			}
		}
		return superiors;
	}

	/**
	 * method to update commission
	 */
	public void updateCommission(CommissionVo commissionVo) {
		Commissions commission = getCommission(commissionVo.getCommissionId());
		if (commission == null)
			throw new ItemNotFoundException("Commission not exist.");

		ForwardApplicationStatus status = null;
		if (commissionVo.getSuperiorIds() != null) {
			status = statusDao.getStatus(StatusConstants.REQUESTED);// Requested
			commission.setForwardApplicationTo(setSuperiors(commissionVo.getSuperiorIds()));
		} else if (commissionVo.getSuperiorIds() == null) {

			commission.getForwardApplicationTo().clear();
			this.sessionFactory.getCurrentSession().merge(commission);
			commission = getCommission(commissionVo.getCommissionId());

			if (commissionVo.getStatusId() != null) {
				status = statusDao.getStatus(commissionVo.getStatus());// As per
																		// user
																		// selection
			} else {
				status = statusDao.getStatus(StatusConstants.APPROVED);// Approved
			}
		}
		commission.setStatus(status);

		Employee employee = employeeDao.findAndReturnEmployeeByCode(commissionVo.getEmployeeCode());
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		commission.setEmployee(employee);

		CommissionTitles title = new CommissionTitles();
		title.setId(commissionVo.getTitleId());
		commission.setTitles(title);

		commission.setNotes(commissionVo.getNotes());
		commission.setDescription(commissionVo.getDescription());

		commission.setAmount(new BigDecimal(commissionVo.getAmount()));

		if (!commissionVo.getDate().isEmpty())
			commission.setDate(DateFormatter.convertStringToDate(commissionVo.getDate()));

		this.sessionFactory.getCurrentSession().merge(commission);
		userActivitiesService.saveUserActivity("Updated Commission " + commissionVo.getEmployeeCode());
	}

	/**
	 * method to delete commission
	 */
	public void deleteCommission(Long commissionId) {
		Commissions commission = getCommission(commissionId);
		if (commission == null)
			throw new ItemNotFoundException("Commission not exist.");
		this.sessionFactory.getCurrentSession().delete(commission);
		userActivitiesService.saveUserActivity("Deleted Commission");
	}

	/**
	 * method to get commission by id
	 */
	public Commissions getCommission(Long commissionId) {
		Commissions commissions = (Commissions) this.sessionFactory.getCurrentSession()
				.createCriteria(Commissions.class).add(Restrictions.eq("id", commissionId)).uniqueResult();
		if (commissions == null)
			throw new ItemNotFoundException("Commission not Exist.");
		return commissions;
	}

	/**
	 * method to list all commissions
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<CommissionVo> listAllCommissiones() {
		List<CommissionVo> vos = new ArrayList<CommissionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Commissions.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Commissions> commissions = criteria.list();
		for (Commissions commission : commissions) {
			vos.add(createCommissionVo(commission));
		}
		return vos;
	}

	/**
	 * method to list commissions by employee
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<CommissionVo> listCommissionesByEmployee(Long employeeId) {
		List<CommissionVo> vos = new ArrayList<CommissionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Commissions.class)
				.add(Restrictions.eq("employee.id", employeeId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Commissions> commissions = criteria.list();
		for (Commissions commission : commissions) {
			vos.add(createCommissionVo(commission));
		}
		return vos;
	}

	/**
	 * method to list commissions by title
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<CommissionVo> listCommissionesByTitle(Long commissionTitleId) {
		List<CommissionVo> vos = new ArrayList<CommissionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Commissions.class)
				.add(Restrictions.eq("titles.id", commissionTitleId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Commissions> commissions = criteria.list();
		for (Commissions commission : commissions) {
			vos.add(createCommissionVo(commission));
		}
		return vos;
	}

	/**
	 * method to list commissions by status
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<CommissionVo> listCommissionesByStatus(Long statusId) {
		List<CommissionVo> vos = new ArrayList<CommissionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Commissions.class)
				.add(Restrictions.eq("status.id", statusId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Commissions> commissions = criteria.list();
		for (Commissions commission : commissions) {
			vos.add(createCommissionVo(commission));
		}
		return vos;
	}

	/**
	 * method to get commissin by id
	 */
	public CommissionVo getCommissionById(Long commissionId) {
		return createCommissionVo(getCommission(commissionId));
	}

	/**
	 * method to get commission by employee and title
	 */
	public CommissionVo getCommissionByEmployeeAndTitle(String employeeCode, Long commissionTitleId) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);

		return createCommissionVo((Commissions) this.sessionFactory.getCurrentSession()
				.createCriteria(Commissions.class).add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("titles.id", commissionTitleId)).uniqueResult());
	}

	/**
	 * method to create commissionvo
	 * 
	 * @param commission
	 * @return
	 */
	private CommissionVo createCommissionVo(Commissions commission) {
		CommissionVo vo = new CommissionVo();
		vo.setCommissionId(commission.getId());
		vo.setEmployeeId(commission.getEmployee().getId());
		vo.setEmployeeCode(commission.getEmployee().getEmployeeCode());
		vo.setEmployee(commission.getEmployee().getUserProfile().getFirstName() + " "
				+ commission.getEmployee().getUserProfile().getLastname());
		vo.setStatus(commission.getStatus().getName());
		vo.setStatusId(commission.getStatus().getId());
		vo.setStatusDescription(commission.getStatusDescription());
		vo.setTitle(commission.getTitles().getTitle());
		vo.setTitleId(commission.getTitles().getId());
		vo.setAmount(commission.getAmount().toString());
		vo.setNotes(commission.getNotes());
		vo.setDescription(commission.getDescription());
		vo.setCreatedBy(commission.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(commission.getCreatedOn()));

		if (commission.getDate() != null)
			vo.setDate(DateFormatter.convertDateToString(commission.getDate()));
		if (commission.getForwardApplicationTo() != null) {
			List<Long> ids = new ArrayList<Long>();
			List<Long> ajaxids = new ArrayList<Long>();
			List<String> codes = new ArrayList<String>();
			List<String> names = new ArrayList<String>();
			for (Employee superior : commission.getForwardApplicationTo()) {
				ids.add(superior.getId());
				ajaxids.add(superior.getId());
				codes.add(superior.getEmployeeCode());
				if (superior.getUserProfile() != null)
					names.add(superior.getUserProfile().getFirstName() + " " + superior.getUserProfile().getLastname());
			}
			vo.setSuperiorAjaxIds(ajaxids);
			vo.setSuperiorIds(ids);
			vo.setSuperiorCodes(names);
			vo.setSuperiors(codes);
		}
		return vo;
	}

	/**
	 * method to update commission status
	 */
	public void updateCommissinStatus(CommissionVo commissionVo) {
		Commissions commission = getCommission(commissionVo.getCommissionId());
		if (commissionVo.getSuperiorIds() != null)
			commission.setForwardApplicationTo(setSuperiors(commissionVo.getSuperiorIds()));
		else {
			commission.getForwardApplicationTo().clear();
			this.sessionFactory.getCurrentSession().merge(commission);
			commission = getCommission(commissionVo.getCommissionId());
		}
		commission.setStatusDescription(commissionVo.getStatusDescription());

		ForwardApplicationStatus status = statusDao.getStatus(commissionVo.getStatus());
		commission.setStatus(status);

		if (commissionVo.getSuperiorCodes() != null)
			commission.setForwardApplicationTo(setSuperiorsByCodes(commissionVo.getSuperiorCodes()));

		TempNotificationVo notificationVo = new TempNotificationVo();
		notificationVo.getEmployeeIds().add(commission.getEmployee().getId());
		notificationVo.setSubject(commission.getEmployee().getEmployeeCode() + "'s Commission Request "
				+ commissionVo.getStatus());
		notificationDao.saveNotification(notificationVo);

		this.sessionFactory.getCurrentSession().merge(commission);
	}

	/**
	 * method to list commission by employee , start date & end date
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<CommissionVo> listCommissionesByEmployeeStartDateEndDate(Long employeeId, Date startDate, Date endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if (employee == null)
			throw new ItemNotFoundException("Employee Not Exist");
		List<CommissionVo> vos = new ArrayList<CommissionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Commissions.class);
		criteria.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Commissions> commissions = criteria.list();
		for (Commissions commission : commissions) {
			vos.add(createCommissionVo(commission));
		}
		return vos;
	}

	/**
	 * method to list commission by start date & end date
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<CommissionVo> listCommissionesByStartDateEndDate(Date startDate, Date endDate) {
		List<CommissionVo> vos = new ArrayList<CommissionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Commissions.class);
		criteria.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Commissions> commissions = criteria.list();
		for (Commissions commission : commissions) {
			vos.add(createCommissionVo(commission));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<CommissionVo> listCommissionesByEmployee(Employee employee) {
		List<CommissionVo> vos = new ArrayList<CommissionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Commissions.class)
				.add(Restrictions.eq("employee", employee));
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Commissions> commissions = criteria.list();
		for (Commissions commission : commissions) {
			vos.add(createCommissionVo(commission));
		}
		return vos;
	}

	private Set<Employee> setSuperiorsByCodes(List<String> superiorCodes) {
		Set<Employee> superiors = new HashSet<Employee>();
		for (String coded : superiorCodes) {
			Employee superior = employeeDao.findAndReturnEmployeeByCode(coded);
			if (superior != null)
				superiors.add(superior);
		}
		return superiors;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<CommissionVo> listCommissionesByEmployeeStartDateEndDate(Employee employee, Date startDate, Date endDate) {
		List<CommissionVo> vos = new ArrayList<CommissionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Commissions.class);
		criteria.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("status", statusDao.getStatus(StatusConstants.APPROVED)));
		criteria.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Commissions> commissions = criteria.list();
		for (Commissions commission : commissions) {
			vos.add(createCommissionVo(commission));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<CommissionVo> findAllCommissionesByEmployee(String employeeCode) {
		List<CommissionVo> vos = new ArrayList<CommissionVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Commissions.class);
		criteria.createAlias("forwardApplicationTo", "employee");
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		Criterion sender = Restrictions.eq("employee", employee);
		Criterion receiver = Restrictions.eq("employee.employeeCode", employeeCode);
		LogicalExpression orExp = Restrictions.or(sender, receiver);
		criteria.add(orExp);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Commissions> commissions = criteria.list();
		for (Commissions commission : commissions) {
			vos.add(createCommissionVo(commission));
		}
		return vos;
	}
}
