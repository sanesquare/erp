package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.MemoDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Memo;
import com.hrms.web.entities.MemoDocument;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.MemoDocumentVo;
import com.hrms.web.vo.MemoVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Repository
public class MemoDaoImpl extends AbstractDao implements MemoDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	public void saveOrUpdateMemo(MemoVo memoVo) {
		Memo memo = findMemo(memoVo.getMemoId());
		if (memo == null)
			memo = new Memo();
		memo.setMemoFrom(employeeDao.findAndReturnEmployeeByCode(memoVo.getEmployeeCode()));
		memo.setForwader(setMemoToForwadList(memoVo.getForwaders(), memo));
		memo.setSubject(memoVo.getMemoSubject());
		memo.setMemoOn(DateFormatter.convertStringToDate(memoVo.getMemoOn()));
		memo.setDescription(memoVo.getDescription());
		memo.setNotes(memoVo.getNotes());
		this.sessionFactory.getCurrentSession().saveOrUpdate(memo);
		memoVo.setMemoId(memo.getId());
		memoVo.setRecordedOn(DateFormatter.convertDateToDetailedString(memo.getCreatedOn()));
		memoVo.setRecordedBy(memo.getCreatedBy().getUserName());
	}

	/**
	 * method to create memo list
	 * 
	 * @param employee
	 * @param memo
	 * @return forwardTo
	 */
	private Set<Employee> setMemoToForwadList(List<String> employee, Memo memo) {
		Set<Employee> forwardTo = memo.getForwader();

		Set<Employee> forwardToTemp = new HashSet<Employee>();
		if (forwardTo != null)
			forwardToTemp.addAll(forwardTo);

		Set<Employee> newForwardTo = new HashSet<Employee>();
		if (forwardTo == null || forwardTo.isEmpty())
			forwardTo = new HashSet<Employee>();
		for (String employeeCode : employee) {
			Employee forwader = employeeDao.findAndReturnEmployeeByCode(employeeCode);
			newForwardTo.add(forwader);
			if (!forwardTo.contains(forwader)) {
				forwardTo.add(forwader);
			}
		}

		forwardToTemp.removeAll(newForwardTo);
		Iterator<Employee> iterator = forwardToTemp.iterator();
		while (iterator.hasNext()) {
			Employee employee2 = (Employee) iterator.next();
			forwardTo.remove(employee2);
		}
		return forwardTo;
	}

	public void saveMemoDocuments(MemoVo memoVo) {
		Memo memo = findMemo(memoVo.getMemoId());
		if (memo == null)
			throw new ItemNotFoundException("Memo Details Not Found.");
		memo.setDocuments(setMemoDocuments(memoVo.getDocuments(), memo));
		this.sessionFactory.getCurrentSession().merge(memo);
	}

	/**
	 * method to set document details
	 * 
	 * @param documentVos
	 * @param memo
	 * @return documents
	 */
	private Set<MemoDocument> setMemoDocuments(List<MemoDocumentVo> documentVos, Memo memo) {
		Set<MemoDocument> documents = new HashSet<MemoDocument>();
		for (MemoDocumentVo documentVo : documentVos) {
			MemoDocument document = new MemoDocument();
			document.setName(documentVo.getDocumentName());
			document.setUrl(documentVo.getDocumentUrl());
			document.setMemo(memo);
			documents.add(document);
		}
		return documents;
	}

	public void deleteMemo(Long memoId) {
		Memo memo = findMemo(memoId);
		if (memo == null)
			throw new ItemNotFoundException("Memo Details Not Found.");
		memo.getForwader().clear();
		this.sessionFactory.getCurrentSession().delete(memo);
	}

	public void deleteMemoDocument(Long memoDocumentId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MemoDocument.class)
				.add(Restrictions.eq("id", memoDocumentId));
		MemoDocument document = (MemoDocument) criteria.uniqueResult();
		Memo memo = document.getMemo();
		memo.getDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(memo);
	}

	public Memo findMemo(Long memoId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Memo.class)
				.add(Restrictions.eq("id", memoId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Memo) criteria.uniqueResult();
	}

	/**
	 * method to create memoVO object
	 * 
	 * @param memo
	 * @return memoVo
	 */
	private MemoVo createMemoVo(Memo memo) {
		MemoVo memoVo = new MemoVo();
		memoVo.setEmployee(memo.getMemoFrom().getUserProfile().getFirstName() + " "
				+ memo.getMemoFrom().getUserProfile().getLastname());
		memoVo.setEmployeeCode(memo.getMemoFrom().getEmployeeCode());
		memoVo.setForwaders(getMemoToList(memo.getForwader()));
		memoVo.setForwadersName(getMemoToNameList(memo.getForwader()));
		memoVo.setMemoId(memo.getId());
		memoVo.setMemoSubject(memo.getSubject());
		memoVo.setMemoOn(DateFormatter.convertDateToString(memo.getMemoOn()));
		memoVo.setDescription(memo.getDescription());
		memoVo.setNotes(memo.getNotes());
		memoVo.setRecordedBy(memo.getCreatedBy().getUserName());
		memoVo.setRecordedOn(DateFormatter.convertDateToDetailedString(memo.getCreatedOn()));
		memoVo.setDocuments(getMemoDocuments(memo));
		return memoVo;
	}

	/**
	 * method to get memo_to list
	 * 
	 * @param forwaders
	 * @return memoTo
	 */
	private List<String> getMemoToList(Set<Employee> forwaders) {
		List<String> memoTo = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			memoTo.add(employee.getEmployeeCode());
		}
		return memoTo;
	}

	/**
	 * method to get memo_to name list
	 * 
	 * @param forwaders
	 * @return memoTo
	 */
	private List<String> getMemoToNameList(Set<Employee> forwaders) {
		List<String> memoTo = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			memoTo.add(employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname());
		}
		return memoTo;
	}

	/**
	 * method to get all the documents
	 * 
	 * @param memo
	 * @return documentVos
	 */
	private List<MemoDocumentVo> getMemoDocuments(Memo memo) {
		List<MemoDocumentVo> documentVos = new ArrayList<MemoDocumentVo>();
		Iterator<MemoDocument> iterator = memo.getDocuments().iterator();
		while (iterator.hasNext()) {
			MemoDocumentVo documentVo = new MemoDocumentVo();
			MemoDocument memoDocument = (MemoDocument) iterator.next();
			documentVo.setDocumentName(memoDocument.getName());
			documentVo.setDocumentUrl(memoDocument.getUrl());
			documentVo.setMemoDocId(memoDocument.getId());
			documentVos.add(documentVo);
		}
		return documentVos;
	}

	public MemoVo findMemoById(Long memoId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Memo.class)
				.add(Restrictions.eq("id", memoId));
		if (criteria.uniqueResult() == null)
			return null;
		return createMemoVo((Memo) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<MemoVo> findMemoByDate(Date memoDate) {
		List<MemoVo> memoVos = new ArrayList<MemoVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Memo.class)
				.add(Restrictions.eq("memoOn", memoDate));
		if (criteria.list().isEmpty())
			return null;
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Memo> memos = criteria.list();
		for (Memo memo : memos)
			memoVos.add(createMemoVo(memo));
		return memoVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<MemoVo> findMemoBetweenDates(Date startDate, Date endDate) {
		List<MemoVo> memoVos = new ArrayList<MemoVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Memo.class)
				.add(Restrictions.ge("memoOn", startDate)).add(Restrictions.le("memoOn", endDate));
		if (criteria.list().isEmpty())
			return null;
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Memo> memos = criteria.list();
		for (Memo memo : memos)
			memoVos.add(createMemoVo(memo));
		return memoVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<MemoVo> findAllMemo() {
		List<MemoVo> memoVos = new ArrayList<MemoVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Memo.class);
		if (criteria.list().isEmpty())
			return null;
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Memo> memos = criteria.list();
		for (Memo memo : memos)
			memoVos.add(createMemoVo(memo));
		return memoVos;
	}

}
