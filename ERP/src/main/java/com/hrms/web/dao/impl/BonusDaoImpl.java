package com.hrms.web.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BonusDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.entities.BonusTitles;
import com.hrms.web.entities.Bonuses;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.BonusVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
@Repository
public class BonusDaoImpl extends AbstractDao implements BonusDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;
	
	/**
	 * method to save bonus
	 */
	public Long saveBonus(BonusVo bonusVo) {
		Bonuses bonuse = new Bonuses();
		BonusTitles title = new BonusTitles();
		
		Employee employee = employeeDao.findAndReturnEmployeeByCode(bonusVo.getEmployeeCode());
		if(employee==null)
			throw new ItemNotFoundException("Employee Not Exist.");
		bonuse.setEmployee(employee);
		
		title.setId(bonusVo.getBonusTitleId());
		bonuse.setBonusTitle(title);
		
		bonuse.setAmount(new BigDecimal(bonusVo.getAmount()));
		bonuse.setNotes(bonusVo.getNotes());
		bonuse.setDescription(bonusVo.getDescription());
		if(bonusVo.getDate()!="")
			bonuse.setDate(DateFormatter.convertStringToDate(bonusVo.getDate()));
		userActivitiesService.saveUserActivity("Added new Bonus "+bonusVo.getBonusTitleId());
		return (Long) this.sessionFactory.getCurrentSession().save(bonuse);
	}

	/**
	 * method to update bonus
	 */
	public void updateBonus(BonusVo bonusVo) {
		Bonuses bonuse = getBonus(bonusVo.getBonusId());
		
		if(bonuse==null)
			throw new ItemNotFoundException("Bonus Not Exist.");
		
		BonusTitles title = new BonusTitles();
		
		Employee employee = employeeDao.findAndReturnEmployeeByCode(bonusVo.getEmployeeCode());
		if(employee==null)
			throw new ItemNotFoundException("Employee Not Exist.");
		bonuse.setEmployee(employee);
		
		title.setId(bonusVo.getBonusTitleId());
		bonuse.setBonusTitle(title);
		
		bonuse.setAmount(new BigDecimal(bonusVo.getAmount()));
		bonuse.setNotes(bonusVo.getNotes());
		bonuse.setDescription(bonusVo.getDescription());
		if(bonusVo.getDate()!="")
			bonuse.setDate(DateFormatter.convertStringToDate(bonusVo.getDate()));
		
		this.sessionFactory.getCurrentSession().merge(bonuse);
		userActivitiesService.saveUserActivity("Updated Bonus "+bonusVo.getEmployeeCode());
	}

	/**
	 * method to delete bonus
	 */
	public void deleteBonus(Long bonusId) {
		Bonuses bonuses = getBonus(bonusId);
		if(bonuses==null)
			throw new ItemNotFoundException("Bonus Not Exist.");
		this.sessionFactory.getCurrentSession().delete(bonuses);
		userActivitiesService.saveUserActivity("Deleted Bonus");
	}

	/**
	 * method to get bonus Object
	 */
	public Bonuses getBonus(Long bonusId) {
		return (Bonuses) this.sessionFactory.getCurrentSession().createCriteria(Bonuses.class)
				.add(Restrictions.eq("id", bonusId)).uniqueResult();
	}

	/**
	 * method to list all bonuses
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<BonusVo> listAllBonuses() {
		List<BonusVo> vos = new ArrayList<BonusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Bonuses.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Bonuses > bonuses = criteria.list();
		for(Bonuses bonuse : bonuses){
			vos.add(createBonusVo(bonuse));
		}
		return vos;
	}

	/**
	 * method to list bonus by employee
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<BonusVo> listBonusesByEmployee(Long employeeId) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<BonusVo> vos = new ArrayList<BonusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Bonuses.class);
		Criterion byEmployee = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression logicalExpression = Restrictions.or(byEmployee, createdBy);
		criteria.add(logicalExpression);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Bonuses > bonuses = criteria.list();
		for(Bonuses bonuse : bonuses){
			vos.add(createBonusVo(bonuse));
		}
		return vos;
	}

	/**
	 * method to list bonus by title
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<BonusVo> listBonusesByTitle(Long bonusTitleId) {
		List<BonusVo> vos = new ArrayList<BonusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Bonuses.class)
				.add(Restrictions.eq("bonusTitle.id", bonusTitleId));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Bonuses > bonuses = criteria.list();
		for(Bonuses bonuse : bonuses){
			vos.add(createBonusVo(bonuse));
		}
		return vos;
	}

	/**
	 * method to get bonus details
	 */
	public BonusVo getBonusById(Long bonusId) {
		Bonuses bonuse = getBonus(bonusId);
		if(bonuse==null)
			throw new ItemNotFoundException("Bonus Not Found.");
		return createBonusVo(bonuse);
	}

	/**
	 * method to create bonus vo
	 * @param bonuse
	 * @return
	 */
	private BonusVo createBonusVo(Bonuses bonuse){
		BonusVo vo = new BonusVo();
		vo.setBonusId(bonuse.getId());
		vo.setId(bonuse.getId());
		vo.setEmployeeId(bonuse.getEmployee().getId());
		vo.setEmployeeCode(bonuse.getEmployee().getEmployeeCode());
		vo.setEmployee(bonuse.getEmployee().getUserProfile().getFirstName()+" "+bonuse.getEmployee().getUserProfile().getLastname());
		vo.setTitle(bonuse.getBonusTitle().getTitle());
		vo.setBonusTitleId(bonuse.getBonusTitle().getId());
		vo.setCreatdBy(bonuse.getCreatedBy().getUserName());
		if(bonuse.getCreatedOn()!=null)
			vo.setCreatedOn(DateFormatter.convertDateToString(bonuse.getCreatedOn()));
		if(bonuse.getDate()!=null)
			vo.setDate(DateFormatter.convertDateToString(bonuse.getDate()));
		vo.setAmount(bonuse.getAmount().toString());
		vo.setDescription(bonuse.getDescription());
		vo.setNotes(bonuse.getNotes());
		return vo;
	}

	/**
	 * method to get bonus by employee and title
	 */
	public BonusVo getBonusByEmployeeAndTitle(String employeeCode, Long BonusTitleId) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		if(employee==null)
			throw new ItemNotFoundException("Employee Not Exist.");
		
		Bonuses bonuses = (Bonuses) this.sessionFactory.getCurrentSession().createCriteria(Bonuses.class)
				.add(Restrictions.eq("bonusTitle.id", BonusTitleId)).add(Restrictions.eq("employee", employee))
				.uniqueResult();
		if(bonuses==null)
			throw new ItemNotFoundException("Bonus Not Exist.");
		return createBonusVo(bonuses);
	}

	/**
	 * method to list bonus by employee , start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<BonusVo> listBonusesByEmployeeStartDateEndDate(Long employeeId, Date startDate, Date endDate) {
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		if(employee == null)
			throw new ItemNotFoundException("Employee Not Exist.");
		List<BonusVo> vos = new ArrayList<BonusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Bonuses.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("employee", employee));
		List<Bonuses > bonuses = criteria.list();
		for(Bonuses bonuse : bonuses){
			vos.add(createBonusVo(bonuse));
		}
		return vos;
	}

	/**
	 * method to list bonus bystart date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<BonusVo> listBonusesByStartDateEndDate(Date startDate, Date endDate) {
		List<BonusVo> vos = new ArrayList<BonusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Bonuses.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate));
		List<Bonuses > bonuses = criteria.list();
		for(Bonuses bonuse : bonuses){
			vos.add(createBonusVo(bonuse));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<BonusVo> listBonusesByEmployee(Employee employee) {
		List<BonusVo> vos = new ArrayList<BonusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Bonuses.class)
				.add(Restrictions.eq("employee", employee));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Bonuses > bonuses = criteria.list();
		for(Bonuses bonuse : bonuses){
			vos.add(createBonusVo(bonuse));
		}
		return vos;
	}

	/**
	 * method to list bonus by employee , start date & end date
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<BonusVo> listBonusesByEmployeeStartDateEndDate(Employee employee, Date startDate, Date endDate) {
		List<BonusVo> vos = new ArrayList<BonusVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Bonuses.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.ge("date", startDate)).add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("employee", employee));
		List<Bonuses > bonuses = criteria.list();
		for(Bonuses bonuse : bonuses){
			vos.add(createBonusVo(bonuse));
		}
		return vos;
	}
}
