package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.WarningDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Warning;
import com.hrms.web.entities.WarningDocument;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.WarningDocumentsVo;
import com.hrms.web.vo.WarningVo;

/**
 * 
 * @author Jithin Mohan
 * @since 20-March-2015
 *
 */
@Repository
public class WarningDaoImpl extends AbstractDao implements WarningDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	public void saveWarning(WarningVo warningVo) {
		Warning warning = findWarning(warningVo.getWarningId());
		if (warning == null)
			warning = new Warning();
		warning.setWarningTo(employeeDao.findAndReturnEmployeeByCode(warningVo.getEmployeeCode()));
		warning.setForwader(setWarningGivenByList(warningVo.getForwaders(), warning));
		warning.setWarningOn(DateFormatter.convertStringToDate(warningVo.getWarningOn()));
		warning.setSubject(warningVo.getSubject());
		warning.setDescription(warningVo.getDescription());
		warning.setNotes(warningVo.getNotes());
		this.sessionFactory.getCurrentSession().saveOrUpdate(warning);
		warningVo.setWarningId(warning.getId());
		warningVo.setRecordedBy(warning.getCreatedBy().getUserName());
		warningVo.setRecordedOn(DateFormatter.convertDateToDetailedString(warning.getCreatedOn()));
	}

	/**
	 * method to create warning given by list
	 * 
	 * @param employee
	 * @param warning
	 * @return forwardTo
	 */
	private Set<Employee> setWarningGivenByList(List<String> employee, Warning warning) {
		Set<Employee> forwardTo = warning.getForwader();
		Set<Employee> forwardToTemp = new HashSet<Employee>();
		if (forwardTo != null)
			forwardToTemp.addAll(forwardTo);

		Set<Employee> newForwardTo = new HashSet<Employee>();
		if (forwardTo == null || forwardTo.isEmpty())
			forwardTo = new HashSet<Employee>();
		for (String employeeCode : employee) {
			Employee forwader = employeeDao.findAndReturnEmployeeByCode(employeeCode);
			newForwardTo.add(forwader);
			if (!forwardTo.contains(forwader)) {
				forwardTo.add(forwader);
			}
		}

		forwardToTemp.removeAll(newForwardTo);
		Iterator<Employee> iterator = forwardToTemp.iterator();
		while (iterator.hasNext()) {
			Employee employee2 = (Employee) iterator.next();
			forwardTo.remove(employee2);
		}
		return forwardTo;
	}

	public void saveWarningDocuments(WarningVo warningVo) {
		Warning warning = findWarning(warningVo.getWarningId());
		if (warning == null)
			throw new ItemNotFoundException("Warning Details Not Found.");
		warning.setWarningDocuments(setWarningDocuments(warningVo.getDocuments(), warning));
		this.sessionFactory.getCurrentSession().merge(warning);
	}

	/**
	 * method to set document details to warning document object
	 * 
	 * @param documentsVos
	 * @param warning
	 * @return documents
	 */
	private Set<WarningDocument> setWarningDocuments(List<WarningDocumentsVo> documentsVos, Warning warning) {
		Set<WarningDocument> documents = new HashSet<WarningDocument>();
		for (WarningDocumentsVo documentsVo : documentsVos) {
			WarningDocument document = new WarningDocument();
			document.setName(documentsVo.getDocumentName());
			document.setUrl(documentsVo.getDocumentUrl());
			document.setWarning(warning);
			documents.add(document);
		}
		return documents;
	}

	public void updateWarning(WarningVo warningVo) {
		// TODO Auto-generated method stub

	}

	public void deleteWarning(Long warningId) {
		Warning warning = findWarning(warningId);
		if (warning == null)
			throw new ItemNotFoundException("Warning Details Not Found.");
		warning.getForwader().clear();
		this.sessionFactory.getCurrentSession().delete(warning);
	}

	public void deleteWarningDocument(Long documentId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(WarningDocument.class)
				.add(Restrictions.eq("id", documentId));
		WarningDocument document = (WarningDocument) criteria.uniqueResult();
		Warning warning = document.getWarning();
		warning.getWarningDocuments().remove(document);
		this.sessionFactory.getCurrentSession().delete(document);
		this.sessionFactory.getCurrentSession().merge(warning);
	}

	public Warning findWarning(Long warningId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Warning.class)
				.add(Restrictions.eq("id", warningId));
		if (criteria.uniqueResult() == null)
			return null;
		return (Warning) criteria.uniqueResult();
	}

	/**
	 * method to create warningvo object
	 * 
	 * @param warning
	 * @return warningVo
	 */
	private WarningVo createWarningVo(Warning warning) {
		WarningVo warningVo = new WarningVo();
		warningVo.setWarningId(warning.getId());
		warningVo.setEmployee(warning.getWarningTo().getUserProfile().getFirstName() + " "
				+ warning.getWarningTo().getUserProfile().getLastname());
		warningVo.setEmployeeCode(warning.getWarningTo().getEmployeeCode());
		warningVo.setForwaders(getWarningGivenByList(warning.getForwader()));
		warningVo.setForwadersName(getWarningGivenByNameList(warning.getForwader()));
		warningVo.setWarningOn(DateFormatter.convertDateToString(warning.getWarningOn()));
		warningVo.setSubject(warning.getSubject());
		warningVo.setDescription(warning.getDescription());
		warningVo.setNotes(warning.getNotes());
		warningVo.setRecordedBy(warning.getCreatedBy().getUserName());
		warningVo.setRecordedOn(DateFormatter.convertDateToDetailedString(warning.getCreatedOn()));
		warningVo.setDocuments(getDocuments(warning.getWarningDocuments()));
		return warningVo;
	}

	/**
	 * method to get warning given by list
	 * 
	 * @param forwaders
	 * @return warningBy
	 */
	private List<String> getWarningGivenByList(Set<Employee> forwaders) {
		List<String> warningBy = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			warningBy.add(employee.getEmployeeCode());
		}
		return warningBy;
	}

	/**
	 * method to get warning given by name list
	 * 
	 * @param forwaders
	 * @return warningBy
	 */
	private List<String> getWarningGivenByNameList(Set<Employee> forwaders) {
		List<String> warningBy = new ArrayList<String>();
		Iterator<Employee> iterator = forwaders.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			warningBy.add(employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname());
		}
		return warningBy;
	}

	/**
	 * method to get all documents
	 * 
	 * @param documents
	 * @return documentsVos
	 */
	private List<WarningDocumentsVo> getDocuments(Set<WarningDocument> documents) {
		List<WarningDocumentsVo> documentsVos = new ArrayList<WarningDocumentsVo>();
		Iterator<WarningDocument> iterator = documents.iterator();
		while (iterator.hasNext()) {
			WarningDocumentsVo documentsVo = new WarningDocumentsVo();
			WarningDocument warningDocument = (WarningDocument) iterator.next();
			documentsVo.setDocumentName(warningDocument.getName());
			documentsVo.setDocumentUrl(warningDocument.getUrl());
			documentsVo.setWarning_doc_id(warningDocument.getId());
			documentsVos.add(documentsVo);
		}
		return documentsVos;
	}

	public WarningVo findWarningById(Long warningId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Warning.class)
				.add(Restrictions.eq("id", warningId));
		if (criteria.uniqueResult() == null)
			return null;
		return createWarningVo((Warning) criteria.uniqueResult());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<WarningVo> findWarningByDate(Date warningOn) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Warning.class)
				.add(Restrictions.eq("warningOn", warningOn));
		List<WarningVo> warningVos = new ArrayList<WarningVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Warning> warnings = criteria.list();
		for (Warning warning : warnings)
			warningVos.add(createWarningVo(warning));
		return warningVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<WarningVo> findWarningBetweenDates(Date startDate, Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Warning.class)
				.add(Restrictions.ge("warningOn", startDate)).add(Restrictions.le("warningOn", endDate));
		List<WarningVo> warningVos = new ArrayList<WarningVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Warning> warnings = criteria.list();
		for (Warning warning : warnings)
			warningVos.add(createWarningVo(warning));
		return warningVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<WarningVo> findWarningByCreatedDate(Date createdOn) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Warning.class)
				.add(Restrictions.eq("createdOn", createdOn));
		List<WarningVo> warningVos = new ArrayList<WarningVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Warning> warnings = criteria.list();
		for (Warning warning : warnings)
			warningVos.add(createWarningVo(warning));
		return warningVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<WarningVo> findWarningCreatedBetweenDates(Date startDate, Date endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Warning.class)
				.add(Restrictions.ge("createdOn", startDate)).add(Restrictions.le("createdOn", endDate));
		List<WarningVo> warningVos = new ArrayList<WarningVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Warning> warnings = criteria.list();
		for (Warning warning : warnings)
			warningVos.add(createWarningVo(warning));
		return warningVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<WarningVo> findWarningByCreator(Employee warningBy) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Warning.class)
				.add(Restrictions.eq("warningBy", warningBy));
		List<WarningVo> warningVos = new ArrayList<WarningVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Warning> warnings = criteria.list();
		for (Warning warning : warnings)
			warningVos.add(createWarningVo(warning));
		return warningVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<WarningVo> findAllWarning() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Warning.class);
		List<WarningVo> warningVos = new ArrayList<WarningVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Warning> warnings = criteria.list();
		for (Warning warning : warnings)
			warningVos.add(createWarningVo(warning));
		return warningVos;
	}

}
