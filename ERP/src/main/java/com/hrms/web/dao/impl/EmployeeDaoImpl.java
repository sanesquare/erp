package com.hrms.web.dao.impl;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.input.BOMInputStream;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;
import org.springframework.util.DigestUtils;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.LedgerGroupDao;
import com.accounts.web.entities.Ledger;
import com.accounts.web.vo.LedgerVo;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.vo.ErpCurrencyVo;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.constants.CommonConstants;
import com.hrms.web.constants.RolesCategoryConstant;
import com.hrms.web.controller.EmployeeController;
import com.hrms.web.controller.HrmsController;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.BranchDao;
import com.hrms.web.dao.DepartmentDao;
import com.hrms.web.dao.EmployeeCategoryDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.EmployeeStatusDao;
import com.hrms.web.dao.EmployeeTypeDao;
import com.hrms.web.dao.LanguageDao;
import com.hrms.web.dao.OrganisationAssetsDao;
import com.hrms.web.dao.QualificationDegreeDao;
import com.hrms.web.dao.SkillDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.dao.UserDao;
import com.hrms.web.dao.UserProfileDao;
import com.hrms.web.entities.AccountType;
import com.hrms.web.entities.BloodGroup;
import com.hrms.web.entities.Bonuses;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Country;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.EmployeNextOfKin;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.EmployeeAdditionalInformation;
import com.hrms.web.entities.EmployeeBankAccount;
import com.hrms.web.entities.EmployeeBenefit;
import com.hrms.web.entities.EmployeeCategory;
import com.hrms.web.entities.EmployeeDependants;
import com.hrms.web.entities.EmployeeDesignation;
import com.hrms.web.entities.EmployeeDocuments;
import com.hrms.web.entities.EmployeeEmergencyContact;
import com.hrms.web.entities.EmployeeExperience;
import com.hrms.web.entities.EmployeeGrade;
import com.hrms.web.entities.EmployeeLanguage;
import com.hrms.web.entities.EmployeeLeave;
import com.hrms.web.entities.EmployeeOrganizationAssets;
import com.hrms.web.entities.EmployeeQualifications;
import com.hrms.web.entities.EmployeeReference;
import com.hrms.web.entities.EmployeeRoles;
import com.hrms.web.entities.EmployeeSkills;
import com.hrms.web.entities.EmployeeStatus;
import com.hrms.web.entities.EmployeeType;
import com.hrms.web.entities.EmployeeUniform;
import com.hrms.web.entities.EmployeeWorkShift;
import com.hrms.web.entities.JobFields;
import com.hrms.web.entities.Language;
import com.hrms.web.entities.LeaveType;
import com.hrms.web.entities.OrganisationAssets;
import com.hrms.web.entities.Project;
import com.hrms.web.entities.QualificationDegree;
import com.hrms.web.entities.Relegion;
import com.hrms.web.entities.RoleType;
import com.hrms.web.entities.RoleTypeTitles;
import com.hrms.web.entities.Roles;
import com.hrms.web.entities.RolesCategory;
import com.hrms.web.entities.SalaryType;
import com.hrms.web.entities.Skills;
import com.hrms.web.entities.User;
import com.hrms.web.entities.UserAddress;
import com.hrms.web.entities.UserProfile;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.AddressJsonVo;
import com.hrms.web.vo.AttendanceEmployeeVo;
import com.hrms.web.vo.EmployeeAdditionalInformationVo;
import com.hrms.web.vo.EmployeeBankAccountVo;
import com.hrms.web.vo.EmployeeBenefitVo;
import com.hrms.web.vo.EmployeeDependantsVo;
import com.hrms.web.vo.EmployeeDocumentsVo;
import com.hrms.web.vo.EmployeeEmergencyJsonVo;
import com.hrms.web.vo.EmployeeExtraInfoJsonVo;
import com.hrms.web.vo.EmployeeLanguageVo;
import com.hrms.web.vo.EmployeeLeaveVo;
import com.hrms.web.vo.EmployeeOrganisationAssetsVo;
import com.hrms.web.vo.EmployeePersonalDetailsJsonVo;
import com.hrms.web.vo.EmployeePhoneNumbersVo;
import com.hrms.web.vo.EmployeeQualificationVo;
import com.hrms.web.vo.EmployeeReferenceVo;
import com.hrms.web.vo.EmployeeSkillsVo;
import com.hrms.web.vo.EmployeeSuperiorsSubordinatesVo;
import com.hrms.web.vo.EmployeeUniformVo;
import com.hrms.web.vo.EmployeeUserJsonVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.EmployeeWorkExperiencVo;
import com.hrms.web.vo.EmployeeextOfKinVo;
import com.hrms.web.vo.PaysSlipVo;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.RoleERPVo;
import com.hrms.web.vo.RoleEmployeesVo;
import com.hrms.web.vo.RoleHomeVo;
import com.hrms.web.vo.RoleMiscellaneousVo;
import com.hrms.web.vo.RoleOrganisationVo;
import com.hrms.web.vo.RolePayrollVo;
import com.hrms.web.vo.RoleRecruitmentVo;
import com.hrms.web.vo.RoleReportsVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.RoleTimeSheetVo;
import com.hrms.web.vo.RoleTypesVo;
import com.hrms.web.vo.RolesVo;
import com.hrms.web.vo.SalaryPayslipItemsVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.SubordinateVo;
import com.hrms.web.vo.SuperiorSubordinateVo;
import com.hrms.web.vo.SuperiorVo;
import com.hrms.web.vo.TempNotificationVo;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

/**
 * 
 * @author Sangeeth and Bibin
 * @modified Shamsheer
 */

@Repository
public class EmployeeDaoImpl extends AbstractDao implements EmployeeDao {

	@Log
	private static Logger logger;

	@Autowired
	private UserActivitiesService userActivitiesService;

	@Autowired
	private UserProfileDao userProfileDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private QualificationDegreeDao degreeDao;

	@Autowired
	private SkillDao skillDao;

	@Autowired
	private BranchDao branchDao;

	@Autowired
	private LanguageDao languageDao;

	@Autowired
	private EmployeeStatusDao statusDao;

	@Autowired
	private OrganisationAssetsDao assetsDao;

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private LedgerGroupDao groupDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	/**
	 * method to list all employees
	 * 
	 * @return List<Employee>
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeVo> listAllEmployee() {
		logger.info("inside>> EmployeeDao... listAllEmployee");
		List<EmployeeVo> employeeVo = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVo.add(createIdCodeVo(employee));
		}
		return employeeVo;
	}

	/**
	 * method to save a new employee
	 */
	public Long saveEmployee(EmployeeVo employeeVo) {
		Employee employee = new Employee();

		employee.setWithPayroll(employeeVo.getWithPayroll());

		employee.setIsAdmin(false);

		EmployeeType employeeType = new EmployeeType();
		employeeType.setId(employeeVo.getEmployeeTypeId());
		employee.setEmployeeType(employeeType);

		EmployeeCategory employeeCategory = new EmployeeCategory();
		employeeCategory.setId(employeeVo.getEmployeeCategoryId());
		employee.setEmployeeCategory(employeeCategory);

		EmployeeDesignation employeeDesignation = new EmployeeDesignation();
		employeeDesignation.setId(employeeVo.getDesignationId());
		employee.setEmployeeDesignation(employeeDesignation);

		employee.setEmployeeStatus(statusDao.findStatus("Active"));

		Branch branch = branchDao.findAndReturnBranchObjectById(employeeVo.getEmployeeBranchId());
		employee.setBranch(branch);

		Department department = new Department();
		department.setId(employeeVo.getEmployeeDepartmentId());
		employee.setDepartmentEmployees(department);

		if (employeeVo.getSalaryTypeId() != null) {
			SalaryType salaryType = new SalaryType();
			salaryType.setId(employeeVo.getSalaryTypeId());
			employee.setSalaryType(salaryType);
		}

		EmployeeGrade employeeGrade = new EmployeeGrade();
		employeeGrade.setId(employeeVo.getEmployeeGradeId());
		employee.setEmployeegrade(employeeGrade);

		EmployeeWorkShift employeeWorkShift = new EmployeeWorkShift();
		employeeWorkShift.setId(employeeVo.getWorkShiftId());
		employee.setWorkShift(employeeWorkShift);

		employee.setEmployeeCode(this.generateEmployeeCode());
		if (employeeVo.getProfilePic() != null)
			employee.setProfilePic(employeeVo.getProfilePic());
		/*
		 * Set<Company> companies = branch.getCompanies(); Company company =
		 * null; if (companies.isEmpty()) throw new ItemNotFoundException(
		 * "No active company found."); else { for (Company comp : companies) {
		 * if (comp.getStatus().getStatus().equalsIgnoreCase("Active")) {
		 * company = comp; } } if (company == null) throw new
		 * ItemNotFoundException("No active company found."); }
		 */

		/*
		 * Ledger ledger = new Ledger(); ledger.setCompany(company);
		 * ledger.setIsEditable(false); ledger.setStartDate(new Date());
		 * ledger.setLedgerName(employee.getEmployeeCode() + " 's Salary");
		 * ledger.setOpeningBalance(new BigDecimal("0.00"));
		 * ledger.setBalance(new BigDecimal("0.00"));
		 * ledger.setLedgerGroup(groupDao.findLedgerGroup(AccountsConstants.
		 * SALARY, company.getId())); ledger.setEmployee(employee);
		 * employee.setLedger(ledger);
		 */

		Long id = (Long) this.sessionFactory.getCurrentSession().save(employee);
		numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.EMPLOYEE);
		userActivitiesService.saveUserActivity("Added new Employee " + employeeVo.getEmployeeCode());
		return id;
	}

	/**
	 * method to update employee
	 */
	public void updateEmployee(EmployeeVo employeeVo) {

		Employee employee = findAndReturnEmployeeById(employeeVo.getEmployeeId());

		employee.setWithPayroll(employeeVo.getWithPayroll());

		EmployeeType employeeType = new EmployeeType();
		employeeType.setId(employeeVo.getEmployeeTypeId());
		employee.setEmployeeType(employeeType);

		EmployeeCategory employeeCategory = new EmployeeCategory();
		employeeCategory.setId(employeeVo.getEmployeeCategoryId());
		employee.setEmployeeCategory(employeeCategory);

		if (employeeVo.getSalaryTypeId() != null) {
			SalaryType salaryType = new SalaryType();
			salaryType.setId(employeeVo.getSalaryTypeId());
			employee.setSalaryType(salaryType);
		}

		EmployeeDesignation employeeDesignation = new EmployeeDesignation();
		employeeDesignation.setId(employeeVo.getDesignationId());
		employee.setEmployeeDesignation(employeeDesignation);

		Branch branch = branchDao.findAndReturnBranchObjectById(employeeVo.getEmployeeBranchId());
		employee.setBranch(branch);

		/*
		 * Set<Company> companies = branch.getCompanies(); Company company =
		 * null; if (companies.isEmpty()) throw new ItemNotFoundException(
		 * "No active company found."); else { for (Company comp : companies) {
		 * if (comp.getStatus().getStatus().equalsIgnoreCase("Active")) {
		 * company = comp; } } if (company == null) throw new
		 * ItemNotFoundException("No active company found."); }
		 */

		/*
		 * Ledger ledger = employee.getLedger(); if (ledger == null ||
		 * !branch.equals(employee.getBranch())) { if (ledger == null) ledger =
		 * new Ledger(); ledger.setIsEditable(false); ledger.setStartDate(new
		 * Date()); String ledgerName = employee.getEmployeeCode(); UserProfile
		 * profile = employee.getUserProfile(); if (profile != null) ledgerName
		 * = profile.getFirstName() + " ( " + ledgerName + " )";
		 * ledger.setLedgerName(ledgerName + " 's Salary");
		 * ledger.setOpeningBalance(new BigDecimal("0.00"));
		 * ledger.setBalance(new BigDecimal("0.00"));
		 * ledger.setLedgerGroup(groupDao.findLedgerGroup(AccountsConstants.
		 * SALARY, company.getId())); ledger.setEmployee(employee); }
		 * employee.setBranch(branch); ledger.setCompany(company);
		 * employee.setLedger(ledger);
		 */

		Department department = new Department();
		department.setId(employeeVo.getEmployeeDepartmentId());
		employee.setDepartmentEmployees(department);

		EmployeeGrade employeeGrade = new EmployeeGrade();
		employeeGrade.setId(employeeVo.getEmployeeGradeId());
		employee.setEmployeegrade(employeeGrade);

		EmployeeWorkShift employeeWorkShift = new EmployeeWorkShift();
		employeeWorkShift.setId(employeeVo.getWorkShiftId());
		employee.setWorkShift(employeeWorkShift);

		employee.setEmployeeCode(employeeVo.getEmployeeCode());
		if (employeeVo.getProfilePic() != null)
			employee.setProfilePic(employeeVo.getProfilePic());

		this.sessionFactory.getCurrentSession().merge(employee);
		userActivitiesService.saveUserActivity("Updated Employee " + employeeVo.getEmployeeCode());
	}

	/**
	 * method to delete employee
	 */
	public void deleteEmployee(Long id) {
		Employee employee = findAndReturnEmployeeById(id);
		/*
		 * Set<EmployeeDocuments> documents = employee.getDocuments(); if
		 * (documents != null) { SFTPOperation sftpOperation = new
		 * SFTPOperation(); for (EmployeeDocuments document : documents) {
		 * sftpOperation.removeFileFromSFTP(document.getFilePath()); } }
		 */
		String name = employee.getEmployeeCode();
		this.sessionFactory.getCurrentSession().delete(employee);
		userActivitiesService.saveUserActivity("Deleted Employee " + name);

	}

	/**
	 * method to find employee by id
	 */
	public Employee findAndReturnEmployeeById(Long id) {
		return (Employee) this.sessionFactory.getCurrentSession().createCriteria(Employee.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public Employee findAndReturnEmployeeByCode(String code) {
		if (code != null)
			return (Employee) this.sessionFactory.getCurrentSession().createCriteria(Employee.class)
					.add(Restrictions.eq("employeeCode", code.trim())).uniqueResult();
		else
			return null;
	}

	public EmployeeVo findEmployeeById(Long id) {

		return createIdCodeVo((Employee) this.sessionFactory.getCurrentSession().createCriteria(Employee.class)
				.add(Restrictions.eq("id", id)).uniqueResult());
	}

	public EmployeeVo findEmployeeByCode(String code) {
		return createIdCodeVo((Employee) this.sessionFactory.getCurrentSession().createCriteria(Employee.class)
				.add(Restrictions.eq("employeeCode", code.trim())).uniqueResult());
	}

	/**
	 * method to upload files
	 */
	public void updateDocuments(EmployeeVo employeeVo) {
		Employee employee = findAndReturnEmployeeById(employeeVo.getEmployeeId());
		// employee.setDocuments(setDocuments(employeeVo.getDocuments(),
		// employee));
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to set employee documents
	 * 
	 * @param documentsVos
	 * @param employee
	 * @return
	 */
	private Set<EmployeeDocuments> setDocuments(List<EmployeeDocumentsVo> documentsVos, Employee employee) {
		Set<EmployeeDocuments> employeeDocuments = new HashSet<EmployeeDocuments>();
		for (EmployeeDocumentsVo documentsVo : documentsVos) {
			EmployeeDocuments documents = new EmployeeDocuments();
			documents.setFileName(documentsVo.getFileName());
			documents.setFilePath(documentsVo.getDocumnetUrl());
			documents.setEmployee(employee);
			employeeDocuments.add(documents);
		}
		return employeeDocuments;
	}

	/**
	 * method to create id,code vo
	 * 
	 * @param employee
	 * @return
	 */
	private EmployeeVo createIdCodeVo(Employee employee) {
		EmployeeVo employeeVo = new EmployeeVo();
		employeeVo.setEmployeeId(employee.getId());
		employeeVo.setEmployeeCode(employee.getEmployeeCode());
		employeeVo.setIsAdmin(employee.getIsAdmin());
		UserProfile profile = employee.getUserProfile();
		if (profile != null && profile.getFirstName() != null && profile.getLastname() != null) {
			employeeVo
					.setName(employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname());
			employeeVo.setGender(profile.getGender());
		}
		if (employee.getDepartmentEmployees() != null)
			employeeVo.setEmployeeDepartment(employee.getDepartmentEmployees().getName());
		if (employee.getProfilePic() != null)
			employeeVo.setProfilePic(employee.getProfilePic());
		return employeeVo;
	}

	/**
	 * method to save employee user info
	 */
	public void saveEmployeeUserInfo(EmployeeUserJsonVo employeeVo) {
		User user = userDao.findUserByEmployeeId(employeeVo.getEmployeeId());
		if (user == null) {
			user = new User();
		}
		user.setUserName(employeeVo.getUserName());
		user.setPassWord(DigestUtils.md5DigestAsHex(employeeVo.getPassword().getBytes()));
		user.setAllowLogin(employeeVo.getAllowLogin());
		user.setEmail(employeeVo.getEmail());
		Employee employee = findAndReturnEmployeeById(employeeVo.getEmployeeId());

		employee.setUser(user);
		user.setEmployee(employee);

		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee personal info
	 */
	public void saveEmployeeUserProfileInfo(EmployeePersonalDetailsJsonVo employeeVo) {
		employeeVo.setFirstName(employeeVo.getFirstName().trim());
		Employee employee = findAndReturnEmployeeById(employeeVo.getEmployeeId());
		/*
		 * Ledger ledger = employee.getLedger(); if (ledger == null) { throw new
		 * ItemNotFoundException("No Active Company Found."); }
		 * ledger.setLedgerName(employeeVo.getFirstName() + " ( " +
		 * employee.getEmployeeCode() + " ) 's Salary");
		 * employee.setLedger(ledger);
		 */
		UserProfile userProfile = userProfileDao.findUserProfilByEmployeeId(employeeVo.getEmployeeId());
		if (userProfile == null) {
			userProfile = new UserProfile();
		}
		userProfile.setSalutation(employeeVo.getSalutation());
		userProfile.setFirstName(employeeVo.getFirstName());
		userProfile.setLastname(employeeVo.getLastName());
		if (employeeVo.getDateOfBirth() != null)
			userProfile.setDateOfBirth(DateFormatter.convertStringToDate(employeeVo.getDateOfBirth()));
		userProfile.setGender(employeeVo.getGender());

		Relegion relegion = new Relegion();
		relegion.setId(employeeVo.getRelegionId());
		userProfile.setRelegioin(relegion);

		Country nationality = new Country();
		nationality.setId(employeeVo.getNationalityId());
		userProfile.setNationality(nationality);

		BloodGroup bloodGroup = new BloodGroup();
		bloodGroup.setId(employeeVo.getBloodGroupId());
		userProfile.setBloodGroup(bloodGroup);

		employee.setUserProfile(userProfile);
		userProfile.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);

	}

	/**
	 * method to save employee's additional info
	 */
	public void saveEmployeeAdditionalInfo(EmployeeExtraInfoJsonVo employeeVo) {
		Employee employee = findAndReturnEmployeeById(employeeVo.getEmployeeId());
		EmployeeAdditionalInformation additionalInformation = (EmployeeAdditionalInformation) this.sessionFactory
				.getCurrentSession().createCriteria(EmployeeAdditionalInformation.class)
				.add(Restrictions.eq("employee", employee)).uniqueResult();
		if (additionalInformation == null) {
			additionalInformation = new EmployeeAdditionalInformation();
		}
		additionalInformation.setPassport(employeeVo.getPassportNumber());
		if (employeeVo.getPassportExpiration() != "")
			additionalInformation
					.setPassportExpiration(DateFormatter.convertStringToDate(employeeVo.getPassportExpiration()));
		if (employeeVo.getLicenceExpiration() != "")
			additionalInformation
					.setLicenceExpiration(DateFormatter.convertStringToDate(employeeVo.getLicenceExpiration()));
		additionalInformation.setDrivingLicence(employeeVo.getLicenceNumber());
		additionalInformation.setPfId(employeeVo.getPfId());
		additionalInformation.setEsiId(employeeVo.getEsiId());
		additionalInformation.setGovernmentId(employeeVo.getGoovernmentId());
		additionalInformation.setTaxNumber(employeeVo.getEmployeeTaxNumber());
		if (employeeVo.getJoiningDate() != null)
			additionalInformation.setJoiningDate(DateFormatter.convertStringToDate(employeeVo.getJoiningDate()));

		employee.setAdditionalInformation(additionalInformation);
		additionalInformation.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee's address
	 */
	public void saveAddress(AddressJsonVo employeeVo) {
		Employee employee = findAndReturnEmployeeById(employeeVo.getEmployeeId());
		UserAddress address = (UserAddress) this.sessionFactory.getCurrentSession().createCriteria(UserAddress.class)
				.add(Restrictions.eq("employee", employee)).uniqueResult();
		if (address == null) {
			address = new UserAddress();
		}
		address.setAddress(employeeVo.getAddress());
		address.setCity(employeeVo.getCity());
		address.setState(employeeVo.getState());
		address.setPincode(employeeVo.getZipCode());

		Country country = new Country();
		country.setId(employeeVo.getCountryId());
		address.setCountry(country);

		employee.setUserAddress(address);
		address.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save phone number
	 */
	public void savePhone(EmployeePhoneNumbersVo employeeVo) {
		Employee employee = findAndReturnEmployeeById(employeeVo.getEmployeeId());
		UserProfile profile = (UserProfile) this.sessionFactory.getCurrentSession().createCriteria(UserProfile.class)
				.add(Restrictions.eq("employee", employee)).uniqueResult();
		if (profile == null) {
			profile = new UserProfile();
		}
		profile.setHomePhone(employeeVo.getHomePhoneNumber());
		profile.setOfficePhone(employeeVo.getOfficePhoneNumber());
		profile.setMobile(employeeVo.getMobileNumber());

		employee.setUserProfile(profile);
		profile.setEmployee(employee);

		this.sessionFactory.getCurrentSession().merge(employee);

	}

	/**
	 * method to save notes
	 */
	public void saveNotes(EmployeeExtraInfoJsonVo employeeVo) {
		Employee employee = findAndReturnEmployeeById(employeeVo.getEmployeeId());
		EmployeeAdditionalInformation additionalInformation = (EmployeeAdditionalInformation) this.sessionFactory
				.getCurrentSession().createCriteria(EmployeeAdditionalInformation.class)
				.add(Restrictions.eq("employee", employee)).uniqueResult();
		if (additionalInformation == null) {
			additionalInformation = new EmployeeAdditionalInformation();
		}
		additionalInformation.setNotes(employeeVo.getNotes());
		employee.setAdditionalInformation(additionalInformation);
		additionalInformation.setEmployee(employee);

		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employees emergency contact info
	 */
	public void saveEmergencyContact(EmployeeEmergencyJsonVo employeeVo) {
		Employee employee = findAndReturnEmployeeById(employeeVo.getEmployeeId());
		EmployeeEmergencyContact emergencyContact = (EmployeeEmergencyContact) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeEmergencyContact.class).add(Restrictions.eq("employee", employee))
				.uniqueResult();
		if (emergencyContact == null) {
			emergencyContact = new EmployeeEmergencyContact();
		}
		emergencyContact.setName(employeeVo.getEmergencyPerson());
		emergencyContact.setRelation(employeeVo.getEmergencyRelation());
		emergencyContact.setPhone(employeeVo.getEmergencyMobile());

		employee.setEmergencyContact(emergencyContact);
		emergencyContact.setEmployee(employee);

		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee qualification
	 */
	public void saveEmployeeQualification(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());

		QualificationDegree degree = new QualificationDegree();
		degree.setId(additionalInformationVo.getDegreeId());

		EmployeeQualifications qualification = (EmployeeQualifications) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeQualifications.class).add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("degree", degree)).uniqueResult();
		if (qualification == null) {
			qualification = new EmployeeQualifications();
			qualification.setDegree(degree);
		}
		qualification.setGrade(additionalInformationVo.getGpa());
		qualification.setGraduationYear(additionalInformationVo.getGraduationYear());
		qualification.setSubject(additionalInformationVo.getSubject());
		qualification.setInstitute(additionalInformationVo.getInstitute());

		employee.getQualification().add(qualification);
		qualification.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee language
	 */
	public void saveEmployeeLanguage(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());

		Language languageObj = new Language();
		languageObj.setId(additionalInformationVo.getLanguageId());

		EmployeeLanguage language = (EmployeeLanguage) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeLanguage.class).add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("language", languageObj)).uniqueResult();
		if (language == null) {
			language = new EmployeeLanguage();
			language.setLanguage(languageObj);
		}
		language.setSpeakingLevel(additionalInformationVo.getSpeakingLevel());
		language.setReadingLevel(additionalInformationVo.getReadingLevel());
		language.setWritingLevel(additionalInformationVo.getWritingLevel());

		employee.getLanguage().add(language);
		language.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee experience info
	 */
	public void saveEmployeeExperience(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());

		EmployeeExperience experience = new EmployeeExperience();
		experience.setOrganisation(additionalInformationVo.getOrganisationName());

		EmployeeDesignation designation = new EmployeeDesignation();
		designation.setId(additionalInformationVo.getDesignationId());
		experience.setDesignation(designation);

		JobFields jobField = new JobFields();
		jobField.setId(additionalInformationVo.getJobFieldId());
		experience.setJobField(jobField);

		if (additionalInformationVo.getJobStartDate() != null)
			experience.setStartDate(DateFormatter.convertStringToDate(additionalInformationVo.getJobStartDate()));
		if (additionalInformationVo.getJobEndDate() != null)
			experience.setEndDate(DateFormatter.convertStringToDate(additionalInformationVo.getJobEndDate()));
		experience.setStartingSalary(new BigDecimal(additionalInformationVo.getStartingSalary()));
		experience.setEndingSalary(new BigDecimal(additionalInformationVo.getEndingSalary()));
		experience.setDescription(additionalInformationVo.getJobDescription());

		// employee.getExperience().add(experience);
		experience.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employees bank details
	 */
	public void saveEmployeeBankAccount(EmployeeBankAccountVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());

		AccountType accountType = new AccountType();
		accountType.setId(additionalInformationVo.getTypeId());

		EmployeeBankAccount account = (EmployeeBankAccount) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeBankAccount.class).add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("accountType", accountType)).uniqueResult();

		if (account == null) {
			account = new EmployeeBankAccount();
			account.setAccountType(accountType);
		}

		account.setName(additionalInformationVo.getBankName());
		account.setBranch(additionalInformationVo.getBranchName());
		account.setBranchCode(additionalInformationVo.getBranchCode());
		account.setAccountTitle(additionalInformationVo.getAccountTitle());

		account.setAccountNumber(additionalInformationVo.getAccountNumber());

		// employee.getBankAccount().add(account);
		account.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee benefit details
	 */
	public void saveEmployeeBenefitsDetails(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());
		EmployeeBenefit benefit = new EmployeeBenefit();
		benefit.setTitle(additionalInformationVo.getBenefitTitle());
		benefit.setDetails(additionalInformationVo.getBenefitDetails());

		// employee.getEmployeeBenefit().add(benefit);
		benefit.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee uniform details
	 */
	public void saveEmployeeUniformDetails(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());
		EmployeeUniform employeeUniform = new EmployeeUniform();
		if (additionalInformationVo.getUniformCheckOutDate() != "")
			employeeUniform.setCheckoutDate(
					DateFormatter.convertStringToDate(additionalInformationVo.getUniformCheckOutDate()));
		employeeUniform.setAdvanceAmount(new BigDecimal(additionalInformationVo.getUniformAdvanceAmount()));
		employeeUniform.setDuration(additionalInformationVo.getUniformDuration());
		if (additionalInformationVo.getUniformReimbursementDate() != "")
			employeeUniform.setReimbursementDate(
					DateFormatter.convertStringToDate(additionalInformationVo.getUniformReimbursementDate()));

		employee.getEmployeeUniform().add(employeeUniform);
		employeeUniform.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employees organization assets
	 */
	public void saveEmployeeOrganizationAssets(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());

		OrganisationAssets assets = assetsDao.findAssetObjById(additionalInformationVo.getAssetId());

		EmployeeOrganizationAssets employeeOrganizationAssets = (EmployeeOrganizationAssets) this.sessionFactory
				.getCurrentSession().createCriteria(EmployeeOrganizationAssets.class)
				.add(Restrictions.eq("employee", employee)).add(Restrictions.eq("organisationAssets", assets))
				.uniqueResult();
		if (employeeOrganizationAssets == null) {
			employeeOrganizationAssets = new EmployeeOrganizationAssets();
			employeeOrganizationAssets.setOrganisationAssets(assets);
		}
		if (additionalInformationVo.getAssetCheckin() != "") {
			employeeOrganizationAssets
					.setCheckinDate(DateFormatter.convertStringToDate(additionalInformationVo.getAssetCheckin()));
		} else {
			employeeOrganizationAssets.setCheckinDate(null);
		}
		if (additionalInformationVo.getAssetCheckOut() != "") {
			employeeOrganizationAssets
					.setCheckoutDate(DateFormatter.convertStringToDate(additionalInformationVo.getAssetCheckOut()));
		}
		employeeOrganizationAssets.setDescription(additionalInformationVo.getAssetDescription());

		// employee.getOrganizationAssets().add(employeeOrganizationAssets);
		employeeOrganizationAssets.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee skills information
	 */
	public void saveEmployeeSkillsInfo(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());
		Skills skills = new Skills();
		skills.setId(additionalInformationVo.getSkillId());

		EmployeeSkills employeeSkills = (EmployeeSkills) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeSkills.class).add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("skills", skills)).uniqueResult();
		if (employeeSkills == null) {
			employeeSkills = new EmployeeSkills();
			employeeSkills.setSkills(skills);
		}
		employeeSkills.setSkillLevel(additionalInformationVo.getSkillLevel());

		employee.getSkills().add(employeeSkills);
		employeeSkills.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee reference details
	 */
	public void saveEmployeeReferenceDetails(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());

		EmployeeReference reference = employee.getReference();
		if (reference == null) {
			reference = new EmployeeReference();
		}

		reference.setName(additionalInformationVo.getReferenceName());
		reference.setEmail(additionalInformationVo.getReferenceEmail());
		reference.setOrganizationName(additionalInformationVo.getReferenceOrganisationName());
		reference.setPhone(additionalInformationVo.getReferencePhoneNUmber());

		employee.setReference(reference);
		reference.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee dependents details
	 */
	public void saveEmployeeDependants(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());
		EmployeeDependants dependants = new EmployeeDependants();
		dependants.setName(additionalInformationVo.getDependantName());
		if (additionalInformationVo.getDependantDateOfBirth() != null)
			dependants.setDateOfBirth(
					DateFormatter.convertStringToDate(additionalInformationVo.getDependantDateOfBirth()));
		dependants.setAddress(additionalInformationVo.getAddress());
		dependants.setRelation(additionalInformationVo.getDependantRelation());
		dependants.setNominee(additionalInformationVo.getDependantNominee());

		// employee.getEmployeeDependants().add(dependants);
		dependants.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee next of kin
	 */
	public void saveEmployeeNextOfKinDetails(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());
		EmployeNextOfKin nextOfKin = new EmployeNextOfKin();
		nextOfKin.setName(additionalInformationVo.getNextOfKinName());
		nextOfKin.setAddress(additionalInformationVo.getNextOfKinAddress());
		if (additionalInformationVo.getNextOfKinDOB() != null)
			nextOfKin.setDateOfBirth(DateFormatter.convertStringToDate(additionalInformationVo.getNextOfKinDOB()));
		nextOfKin.setRelation(additionalInformationVo.getNextOFKinRelation());

		// employee.getNextOfKin().add(nextOfKin);
		nextOfKin.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to set employee status
	 */
	public void saveEmployeeStatus(EmployeeAdditionalInformationVo additionalInformationVo) {
		Employee employee = findAndReturnEmployeeById(additionalInformationVo.getEmployeeId());
		EmployeeStatus employeeStatus = new EmployeeStatus();
		employeeStatus.setId(additionalInformationVo.getStatusId());
		employee.setEmployeeStatus(employeeStatus);
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to list employee list details
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeVo> listEmployeesDeetails() {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("id"));
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createDetailedEmployeeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to create detailed employeeVo
	 * 
	 * @param employee
	 * @return
	 */
	private EmployeeVo createDetailedEmployeeVo(Employee employee) {
		EmployeeVo employeeVo = new EmployeeVo();
		employeeVo.setEmployeeId(employee.getId());
		employeeVo.setEmployeeCode(employee.getEmployeeCode());
		employeeVo.setIsAdmin(employee.getIsAdmin());
		if (employee.getProfilePic() != null)
			employeeVo.setProfilePic(employee.getProfilePic());
		UserProfile profile = employee.getUserProfile();
		if (profile != null && profile.getFirstName() != null && profile.getLastname() != null) {
			employeeVo.setFirstName(profile.getFirstName());
			employeeVo.setName(profile.getFirstName() + " " + profile.getLastname());
			employeeVo.setLastName(profile.getLastname());
			employeeVo.setEmployeeName(profile.getFirstName() + " " + profile.getLastname());
			employeeVo.setGender(profile.getGender());
		}
		EmployeeDesignation designation = employee.getEmployeeDesignation();
		if (designation != null)
			employeeVo.setDesignation(designation.getDesignation());
		Department department = employee.getDepartmentEmployees();
		if (department != null)
			employeeVo.setEmployeeDepartment(department.getName());
		Branch branch = employee.getBranch();
		if (branch != null)
			employeeVo.setEmployeeBranch(branch.getBranchName());
		User user = employee.getUser();
		if (user != null)
			employeeVo.setUserName(user.getUserName());
		EmployeeStatus status = employee.getEmployeeStatus();
		if (status != null)
			employeeVo.setStatus(status.getName());
		SalaryType salaryType = employee.getSalaryType();
		if (salaryType != null) {
			employeeVo.setSalaryType(salaryType.getType());
			employeeVo.setSalaryTypeId(salaryType.getId());
		}
		return employeeVo;
	}

	/**
	 * to get detailed employee
	 * 
	 * @param id
	 * @return
	 */
	public EmployeeVo getEmployeesDetails(Long id) {
		return createViewEmployeeVo(findAndReturnEmployeeById(id));
	}

	/**
	 * method to get employee by branch
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> getEmployeesByBranch(Long branchId) {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("branch.id", branchId));
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get employees by department
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> getEmployeesByDepartment(Long departmentId) {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("departmentEmployees.id", departmentId));
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get employees by branch and department
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeVo> getEmployeesByBranchAndDepartment(Long branchId, Long departmentId) {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class)
				.add(Restrictions.eq("branch.id", branchId))
				.add(Restrictions.eq("departmentEmployees.id", departmentId));
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * update notify by email
	 */
	public void updateNotifyByEmail(EmployeeExtraInfoJsonVo employeeVo) {
		Employee employee = findAndReturnEmployeeById(employeeVo.getEmployeeId());
		employee.setNotifyByEmail(employeeVo.getNotifyByEmail());
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to save employee subordinates & superiors
	 */
	public void saveEmployeeSubordinateSuperior(EmployeeSuperiorsSubordinatesVo employeeSuperiorsSubordinatesVo) {
		Employee employee = findAndReturnEmployeeById(employeeSuperiorsSubordinatesVo.getEmployeeId());

		employee.getSubordinates().clear();
		employee.getSuperiors().clear();
		this.sessionFactory.getCurrentSession().merge(employee);
		TempNotificationVo notificationVo = new TempNotificationVo();
		notificationVo.setSubject("A New Employee ( " + employee.getEmployeeCode() + " ) Has Joined Our Company. ");
		if (employeeSuperiorsSubordinatesVo.getSubordinatesIds() != null) {
			for (Long subordinates : employeeSuperiorsSubordinatesVo.getSubordinatesIds()) {
				notificationVo.getEmployeeIds().add(subordinates);
				Employee subordinate = findAndReturnEmployeeById(subordinates);
				// subordinate.setId(subordinates);
				employee.getSubordinates().add(subordinate);
			}
		}
		if (employeeSuperiorsSubordinatesVo.getSuperiorsIds() != null) {
			for (Long superiors : employeeSuperiorsSubordinatesVo.getSuperiorsIds()) {
				notificationVo.getEmployeeIds().add(superiors);
				Employee superior = findAndReturnEmployeeById(superiors);
				// superior.setId(superiors);
				employee.getSuperiors().add(superior);
			}
		}
		/*
		 * if(notificationVo.getEmployeeIds().size()>0)
		 * notificationDao.saveNotification(notificationVo);
		 */
		this.sessionFactory.getCurrentSession().update(employee);
	}

	public EmployeeVo findDetailedEmployee(Long id) {
		return createViewEmployeeVo((Employee) this.sessionFactory.getCurrentSession().createCriteria(Employee.class)
				.add(Restrictions.eq("id", id)).uniqueResult());
	}

	/**
	 * method to create detailed employeeVo
	 * 
	 * @param employee
	 * @return
	 */
	private EmployeeVo createViewEmployeeVo(Employee employee) {
		EmployeeVo employeeVo = new EmployeeVo();
		employeeVo.setIsAdmin(employee.getIsAdmin());
		employeeVo.setEmployeeId(employee.getId());
		employeeVo.setEmployeeCode(employee.getEmployeeCode());
		if (employee.getCreatedBy() != null)
			employeeVo.setCreatedBy(employee.getCreatedBy().getUserName());
		employeeVo.setWithPayroll(employee.getWithPayroll());

		if (employee.getProfilePic() != null) {
			employeeVo.setProfilePic(employee.getProfilePic());
			employeeVo.setProfilePicHidden(employee.getProfilePic());
		}

		SuperiorSubordinateVo vo = new SuperiorSubordinateVo();
		Set<Employee> superiors = employee.getSuperiors();
		if (superiors != null) {
			List<SuperiorVo> superiorVos = new ArrayList<SuperiorVo>();
			List<Long> ids = new ArrayList<Long>();
			for (Employee superior : superiors) {
				superiorVos.add(createSuperiorVo(superior));
				ids.add(superior.getId());
			}
			vo.setSuperiors(superiorVos);
			employeeVo.setSuperiorsIds(ids);
		}

		Set<Employee> subordinates = employee.getSubordinates();
		if (subordinates != null) {
			List<SubordinateVo> subordinateVos = new ArrayList<SubordinateVo>();
			List<Long> ids = new ArrayList<Long>();
			for (Employee subordinate : subordinates) {
				subordinateVos.add(createSubordinateVo(subordinate));
			}
			vo.setSubordinates(subordinateVos);
			employeeVo.setSubordinatesIds(ids);
		}
		employeeVo.setSuperiorSubordinateVo(vo);

		if (employee.getCreatedOn() != null)
			employeeVo.setCreatedOn(DateFormatter.convertDateToString(employee.getCreatedOn()));

		EmployeeDesignation designation = employee.getEmployeeDesignation();
		if (designation != null) {
			employeeVo.setDesignation(designation.getDesignation());
			employeeVo.setDesignationId(designation.getId());
		}

		Department department = employee.getDepartmentEmployees();
		if (department != null) {
			employeeVo.setEmployeeDepartment(department.getName());
			employeeVo.setEmployeeDepartmentId(department.getId());
		}

		EmployeeStatus status = employee.getEmployeeStatus();
		if (status != null) {
			employeeVo.setStatusId(status.getId());
			employeeVo.setStatus(status.getName());
		}
		EmployeeType employeeType = employee.getEmployeeType();
		if (employeeType != null) {
			employeeVo.setEmployeeType(employeeType.getType());
			employeeVo.setEmployeeTypeId(employeeType.getId());
		}

		Branch branch = employee.getBranch();
		if (branch != null) {
			employeeVo.setEmployeeBranch(branch.getBranchName());
			employeeVo.setEmployeeBranchId(branch.getId());
		}

		EmployeeGrade employeeGrade = employee.getEmployeegrade();
		if (employeeGrade != null) {
			employeeVo.setEmployeeGrade(employeeGrade.getGrade());
			employeeVo.setEmployeeGradeId(employeeGrade.getId());
		}
		EmployeeWorkShift employeeWorkShift = employee.getWorkShifts();
		if (employeeWorkShift != null) {
			employeeVo.setWorkShift(employeeWorkShift.getWorkShift());
			employeeVo.setWorkShiftId(employeeWorkShift.getId());
		}
		EmployeeCategory category = employee.getEmployeeCategory();
		if (category != null) {
			employeeVo.setEmployeeCategory(category.getCategory());
			employeeVo.setEmployeeCategoryId(category.getId());
		}
		SalaryType salaryType = employee.getSalaryType();
		if (salaryType != null) {
			employeeVo.setSalaryType(salaryType.getType());
			employeeVo.setSalaryTypeId(salaryType.getId());
		}

		User user = employee.getUser();
		if (user != null) {
			employeeVo.setAllowLogin(user.getAllowLogin());
			employeeVo.setUserName(user.getUserName());
			employeeVo.setPassword(user.getPassWord());
			employeeVo.setEmail(user.getEmail());
		}

		employeeVo.setNotifyByEmail(employee.getNotifyByEmail());

		UserProfile profile = employee.getUserProfile();
		if (profile != null) {
			if (profile.getFirstName() != null && profile.getLastname() != null) {
				employeeVo.setSalutation(profile.getSalutation());
				employeeVo.setFirstName(profile.getFirstName());
				employeeVo.setName(profile.getFirstName() + " " + profile.getLastname());
				employeeVo.setLastName(profile.getLastname());
				employeeVo.setEmployeeName(
						profile.getSalutation() + " " + profile.getFirstName() + " " + profile.getLastname());
			}
			if (profile.getDateOfBirth() != null)
				employeeVo.setDateOfBirth(DateFormatter.convertDateToString(profile.getDateOfBirth()));
			employeeVo.setGender(profile.getGender());

			BloodGroup bloodGroup = profile.getBloodGroup();
			if (bloodGroup != null) {
				employeeVo.setBloodGroup(bloodGroup.getGroup());
				employeeVo.setBloodGroupId(bloodGroup.getId());
			}
			Relegion relegion = profile.getRelegioin();
			if (relegion != null) {
				employeeVo.setRelegion(relegion.getRelegion());
				employeeVo.setRelegionId(relegion.getId());
			}
			Country country = profile.getNationality();
			if (country != null) {
				employeeVo.setNationality(country.getName());
				employeeVo.setNationalityId(country.getId());
			}
			employeeVo.setHomePhoneNumber(profile.getHomePhone());
			employeeVo.setOfficePhoneNumber(profile.getOfficePhone());
			employeeVo.setMobileNumber(profile.getMobile());
		}

		EmployeeAdditionalInformation additionalInformation = employee.getAdditionalInformation();
		if (additionalInformation != null) {
			if (additionalInformation.getJoiningDate() != null)
				employeeVo.setJoiningDate(DateFormatter.convertDateToString(additionalInformation.getJoiningDate()));
			if (additionalInformation.getPassportExpiration() != null)
				employeeVo.setPassportExpiration(
						DateFormatter.convertDateToString(additionalInformation.getPassportExpiration()));
			employeeVo.setPassportNumber(additionalInformation.getPassport());
			if (additionalInformation.getLicenceExpiration() != null)
				employeeVo.setLicenceExpiration(
						DateFormatter.convertDateToString(additionalInformation.getLicenceExpiration()));
			employeeVo.setLicenceNumber(additionalInformation.getDrivingLicence());
			employeeVo.setGoovernmentId(additionalInformation.getGovernmentId());
			employeeVo.setEmployeeTaxNumber(additionalInformation.getTaxNumber());
			employeeVo.setPfId(additionalInformation.getPfId());
			employeeVo.setEsiId(additionalInformation.getEsiId());
			employeeVo.setNotes(additionalInformation.getNotes());
		}

		UserAddress address = employee.getUserAddress();
		if (address != null) {
			employeeVo.setAddress(address.getAddress());
			employeeVo.setCity(address.getCity());
			employeeVo.setState(address.getState());
			employeeVo.setZipCode(address.getPincode());
			Country country = address.getCountry();
			if (country != null) {
				employeeVo.setCountry(country.getName());
				employeeVo.setCountryId(country.getId());
			}
		}

		EmployeeEmergencyContact emergencyContact = employee.getEmergencyContact();
		if (emergencyContact != null) {
			employeeVo.setEmergencyPerson(emergencyContact.getName());
			employeeVo.setEmergencyMobile(emergencyContact.getPhone());
			employeeVo.setEmergencyRelation(emergencyContact.getRelation());
		}

		Set<EmployeeQualifications> employeeQualifications = employee.getQualification();
		if (employeeQualifications != null) {
			List<EmployeeQualificationVo> employeeQualificationVos = new ArrayList<EmployeeQualificationVo>();
			for (EmployeeQualifications qualifications : employeeQualifications) {
				employeeQualificationVos.add(creatQualificatioVo(qualifications));
			}
			employeeVo.setQualifications(employeeQualificationVos);
		}

		Set<EmployeeOrganizationAssets> assets = employee.getOrganizationAssets();
		if (assets != null) {
			List<EmployeeOrganisationAssetsVo> organisationAssetsVos = new ArrayList<EmployeeOrganisationAssetsVo>();
			for (EmployeeOrganizationAssets organizationAssets : assets) {
				organisationAssetsVos.add(createAssetsVo(organizationAssets));
			}
			employeeVo.setOrganizationAssets(organisationAssetsVos);
		}

		Set<EmployeeLanguage> employeeLanguages = employee.getLanguage();
		if (employeeLanguages != null) {
			List<EmployeeLanguageVo> languageVos = new ArrayList<EmployeeLanguageVo>();
			for (EmployeeLanguage language : employeeLanguages) {
				languageVos.add(createLanguageVo(language));
			}
			employeeVo.setEmployeeLanguages(languageVos);
		}

		Set<EmployeeUniform> employeeUniforms = employee.getEmployeeUniform();
		if (employeeUniforms != null) {
			List<EmployeeUniformVo> uniformVos = new ArrayList<EmployeeUniformVo>();
			for (EmployeeUniform uniform : employeeUniforms) {
				uniformVos.add(createUniformVo(uniform));
			}
			employeeVo.setUniforms(uniformVos);
		}

		Set<EmployeeBankAccount> accounts = employee.getBankAccount();
		if (accounts != null) {
			List<EmployeeBankAccountVo> accountVos = new ArrayList<EmployeeBankAccountVo>();
			for (EmployeeBankAccount bankAccount : accounts) {
				accountVos.add(createBankAccountVo(bankAccount));
			}
			employeeVo.setBankAccountVo(accountVos);
		}

		EmployeeReference employeeReference = employee.getReference();
		if (employeeReference != null)
			employeeVo.setReferenceVo(createEmployeeReference(employeeReference));

		EmployeeStatus employeeStatus = employee.getEmployeeStatus();
		if (employeeStatus != null)
			employeeVo.setStatus(employeeStatus.getName());

		Set<EmployeeDocuments> employeeDocuments = employee.getDocuments();
		if (employeeDocuments != null) {
			List<EmployeeDocumentsVo> documentsVos = new ArrayList<EmployeeDocumentsVo>();
			for (EmployeeDocuments documents : employeeDocuments) {
				documentsVos.add(createDocumentsVo(documents));
			}
			employeeVo.setDocumentsVos(documentsVos);
		}

		/*
		 * Set<Employee> subordinates = employee.getSubordinates(); if
		 * (subordinates != null) { List<Long> ids = new ArrayList<Long>(); for
		 * (Employee subordinate : subordinates) { ids.add(subordinate.getId());
		 * } employeeVo.setSubordinatesIds(ids); }
		 * 
		 * Set<Employee> superiors = employee.getSubordinates(); if (superiors
		 * != null) { List<Long> ids = new ArrayList<Long>(); for (Employee
		 * superior : superiors) { ids.add(superior.getId()); }
		 * employeeVo.setSubordinatesIds(ids); }
		 */
		Set<EmployeeBenefit> benefits = employee.getEmployeeBenefit();
		if (benefits != null) {
			List<EmployeeBenefitVo> vos = new ArrayList<EmployeeBenefitVo>();
			for (EmployeeBenefit benefit : benefits) {
				vos.add(createBenefitVo(benefit));
			}
			employeeVo.setBenefits(vos);
		}

		Set<EmployeeSkills> employeeSkills = employee.getSkills();
		if (employeeSkills != null) {
			List<EmployeeSkillsVo> skillsVos = new ArrayList<EmployeeSkillsVo>();
			for (EmployeeSkills skills : employeeSkills) {
				skillsVos.add(createSkill(skills));
			}
			employeeVo.setSkills(skillsVos);
		}

		Set<EmployeeExperience> employeeExperiences = employee.getExperience();
		if (employeeExperiences != null) {
			List<EmployeeWorkExperiencVo> experiencVos = new ArrayList<EmployeeWorkExperiencVo>();
			for (EmployeeExperience experience : employeeExperiences) {
				experiencVos.add(creteExperienceVo(experience));
			}
			employeeVo.setExperiences(experiencVos);
		}

		Set<EmployeeDependants> employeeDependants = employee.getEmployeeDependants();
		if (employeeDependants != null) {
			List<EmployeeDependantsVo> dependantsVos = new ArrayList<EmployeeDependantsVo>();
			for (EmployeeDependants dependants : employeeDependants) {
				dependantsVos.add(createDependantsVo(dependants));
			}
			employeeVo.setDependants(dependantsVos);
		}

		Set<EmployeNextOfKin> employeNextOfKins = employee.getNextOfKin();
		if (employeNextOfKins != null) {
			List<EmployeeextOfKinVo> kinVos = new ArrayList<EmployeeextOfKinVo>();
			for (EmployeNextOfKin kin : employeNextOfKins) {
				kinVos.add(createKinVo(kin));
			}
			employeeVo.setNextOfKins(kinVos);
		}

		Set<Project> projects = employee.getProject();
		if (projects != null) {
			List<ProjectVo> projectVos = new ArrayList<ProjectVo>();
			for (Project project : projects) {
				ProjectVo projectVo = new ProjectVo();
				projectVo.setProjectId(project.getId());
				projectVo.setProjectTitle(project.getTitle());
				projectVos.add(projectVo);
			}
			employeeVo.setProjectVo(projectVos);
		}

		Set<EmployeeLeave> employeeLeaves = employee.getLeaves();
		if (employeeLeaves != null) {
			List<EmployeeLeaveVo> leaveVos = new ArrayList<EmployeeLeaveVo>();
			for (EmployeeLeave leave : employeeLeaves) {
				EmployeeLeaveVo leaveVo = new EmployeeLeaveVo();
				leaveVo.setCarryOver(leave.getCarryOver());
				leaveVo.setLeaves(leave.getLeaves());
				leaveVo.setLeaveId(leave.getId());
				leaveVo.setLeaveStatus(leave.getStatus());
				LeaveType leaveType = leave.getTitle();
				leaveVo.setLeaveTitleId(leaveType.getId());
				leaveVo.setLeaveTitle(leaveType.getType());
				leaveVo.setLeaveType(leave.getType());
				if (leave.getLeavesTaken() != null)
					leaveVo.setLeavesTaken(leave.getLeavesTaken());
				else
					leaveVo.setLeavesTaken(0L);
				if (leave.getLeavesBalance() != null)
					leaveVo.setLeavesBalance(leave.getLeavesBalance());
				else
					leaveVo.setLeavesBalance(0L);
				leaveVos.add(leaveVo);
			}
			employeeVo.setLeaveVos(leaveVos);
		}
		return employeeVo;
	}

	/**
	 * method to create nextOfKinVo
	 * 
	 * @param kin
	 * @return
	 */
	private EmployeeextOfKinVo createKinVo(EmployeNextOfKin kin) {
		EmployeeextOfKinVo vo = new EmployeeextOfKinVo();
		vo.setAddress(kin.getAddress());
		vo.setName(kin.getName());
		vo.setRelation(kin.getRelation());
		vo.setDateOfBirth(DateFormatter.convertDateToDetailedString(kin.getDateOfBirth()));
		return vo;
	}

	/**
	 * method to create dependants vo
	 * 
	 * @param dependants
	 * @return
	 */
	private EmployeeDependantsVo createDependantsVo(EmployeeDependants dependants) {
		EmployeeDependantsVo vo = new EmployeeDependantsVo();
		vo.setAddress(dependants.getAddress());
		vo.setDatoeOfBirth(DateFormatter.convertDateToDetailedString(dependants.getDateOfBirth()));
		vo.setDependantName(dependants.getName());
		vo.setRelation(dependants.getRelation());
		vo.setNominee(dependants.getNominee());
		return vo;
	}

	/**
	 * method to create experience vo
	 * 
	 * @param experience
	 * @return
	 */
	private EmployeeWorkExperiencVo creteExperienceVo(EmployeeExperience experience) {
		EmployeeWorkExperiencVo vo = new EmployeeWorkExperiencVo();
		vo.setDesignation(experience.getDesignation().getDesignation());
		vo.setJobField(experience.getJobField().getField());
		vo.setOrganisation(experience.getOrganisation());
		vo.setEndDate(DateFormatter.convertDateToDetailedString(experience.getEndDate()));
		vo.setStartDate(DateFormatter.convertDateToDetailedString(experience.getStartDate()));
		vo.setStartingSalary(experience.getStartingSalary().toString());
		vo.setEndingSalary(experience.getEndingSalary().toString());
		vo.setDescription(experience.getDescription());
		return vo;
	}

	/**
	 * method to create skill vo
	 * 
	 * @param skills
	 * @return
	 */
	private EmployeeSkillsVo createSkill(EmployeeSkills skills) {
		EmployeeSkillsVo vo = new EmployeeSkillsVo();
		if (skills != null) {
			if (skills.getSkills() != null)
				vo.setSkill(skills.getSkills().getSkill());
			vo.setSkillLevel(skills.getSkillLevel());
		}
		return vo;
	}

	/**
	 * method to create organisationAssetsVo
	 * 
	 * @param assets
	 * @return
	 */
	private EmployeeOrganisationAssetsVo createAssetsVo(EmployeeOrganizationAssets assets) {
		EmployeeOrganisationAssetsVo assetsVo = new EmployeeOrganisationAssetsVo();
		if (assets != null) {
			if (assets.getCheckinDate() != null)
				assetsVo.setAssetCheckin(DateFormatter.convertDateToString(assets.getCheckinDate()));
			if (assets.getCheckoutDate() != null)
				assetsVo.setAssetCheckout(DateFormatter.convertDateToString(assets.getCheckoutDate()));
			assetsVo.setAssetCode(assets.getOrganisationAssets().getAssetCode());
			assetsVo.setAssetName(assets.getOrganisationAssets().getAssetName());
			assetsVo.setDescription(assets.getDescription());
		}
		return assetsVo;
	}

	/**
	 * method to create qualification vo
	 * 
	 * @param qualifications
	 * @return
	 */
	private EmployeeQualificationVo creatQualificatioVo(EmployeeQualifications qualifications) {
		EmployeeQualificationVo vo = new EmployeeQualificationVo();
		if (qualifications != null) {
			vo.setDegree(qualifications.getDegree().getDegree());
			vo.setGpa(qualifications.getGrade());
			vo.setGraduationYear(qualifications.getGraduationYear());
			vo.setSubject(qualifications.getSubject());
			vo.setInstitute(qualifications.getInstitute());
		}
		return vo;
	}

	/**
	 * method to create languageVo
	 * 
	 * @param language
	 * @return
	 */
	private EmployeeLanguageVo createLanguageVo(EmployeeLanguage language) {
		EmployeeLanguageVo vo = new EmployeeLanguageVo();
		if (language != null) {
			if (language.getLanguage() != null)
				vo.setLanguage(language.getLanguage().getName());
			vo.setReadingLevel(language.getReadingLevel());
			vo.setWritingLevel(language.getWritingLevel());
			vo.setSpeakingLevel(language.getSpeakingLevel());
		}
		return vo;
	}

	/**
	 * method to set uniformVo
	 * 
	 * @param uniform
	 * @return
	 */
	private EmployeeUniformVo createUniformVo(EmployeeUniform uniform) {
		EmployeeUniformVo vo = new EmployeeUniformVo();
		vo.setAmount(uniform.getAdvanceAmount().toString());
		if (uniform.getCheckoutDate() != null)
			vo.setCheckout(DateFormatter.convertDateToString(uniform.getCheckoutDate()));
		if (uniform.getReimbursementDate() != null)
			vo.setReimbursemnt(DateFormatter.convertDateToString(uniform.getReimbursementDate()));
		vo.setDuration(uniform.getDuration());
		return vo;
	}

	/**
	 * method to set bankvo
	 * 
	 * @param uniform
	 * @return
	 */
	private EmployeeBankAccountVo createBankAccountVo(EmployeeBankAccount bankAccount) {
		EmployeeBankAccountVo vo = new EmployeeBankAccountVo();
		if (bankAccount != null) {
			vo.setAccountNumber(bankAccount.getAccountNumber());
			vo.setAccountTitle(bankAccount.getAccountTitle());

			if (bankAccount.getAccountType() != null)
				vo.setAccountType(bankAccount.getAccountType().getType());

			vo.setBankName(bankAccount.getName());
			vo.setBranchCode(bankAccount.getBranchCode());
			vo.setBranchName(bankAccount.getBranch());
		}
		return vo;
	}

	/**
	 * method to set referenceVo
	 * 
	 * @param reference
	 * @return
	 */
	private EmployeeReferenceVo createEmployeeReference(EmployeeReference reference) {
		EmployeeReferenceVo vo = new EmployeeReferenceVo();
		vo.setEmail(reference.getEmail());
		vo.setOrganisationName(reference.getOrganizationName());
		vo.setPhone(reference.getPhone());
		vo.setReferenceName(reference.getName());
		return vo;
	}

	/**
	 * method to create benefitVo
	 * 
	 * @param benefit
	 * @return
	 */
	private EmployeeBenefitVo createBenefitVo(EmployeeBenefit benefit) {
		EmployeeBenefitVo vo = new EmployeeBenefitVo();
		vo.setBenefit(benefit.getTitle());
		return vo;
	}

	/**
	 * method to create documents vo
	 * 
	 * @param documents
	 * @return
	 */
	private EmployeeDocumentsVo createDocumentsVo(EmployeeDocuments documents) {
		EmployeeDocumentsVo vo = new EmployeeDocumentsVo();
		vo.setDocumentId(documents.getId());
		vo.setDocumnetUrl(documents.getFilePath());
		vo.setFileName(documents.getFileName());
		return vo;
	}

	/**
	 * method to delete document
	 */
	public void deleteDocument(String url) {
		EmployeeDocuments document = (EmployeeDocuments) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeDocuments.class).add(Restrictions.eq("filePath", url)).uniqueResult();
		Employee employee = findAndReturnEmployeeById(document.getEmployee().getId());

		// employee.getDocuments().remove(document);
		this.sessionFactory.getCurrentSession().merge(employee);
		this.sessionFactory.getCurrentSession().delete(document);
	}

	/**
	 * method to save employee leave
	 */
	public Long saveEmployeeLeave(EmployeeLeaveVo leaveVo) {
		Employee employee = findAndReturnEmployeeById(leaveVo.getEmployeeId());
		LeaveType leaveType = new LeaveType();
		leaveType.setId(leaveVo.getLeaveTitleId());

		EmployeeLeave leave = (EmployeeLeave) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeLeave.class).add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("title", leaveType)).uniqueResult();

		if (leave == null) {
			leave = new EmployeeLeave();
		}

		leave.setCarryOver(leaveVo.getCarryOver());
		leave.setLeaves(leaveVo.getLeaves());
		leave.setType(leaveVo.getLeaveType());
		leave.setStatus(leaveVo.getLeaveStatus());
		leave.setLeavesBalance(0L);
		leave.setLeavesTaken(0L);
		leave.setTitle(leaveType);
		employee.getLeaves().add(leave);

		leave.setEmployee(employee);
		this.sessionFactory.getCurrentSession().merge(employee);
		return null;
	}

	/**
	 * method to return leave by employee and type
	 */
	public EmployeeLeaveVo getLeaveByEmployeeAndTitle(Long employeeId, Long typeId) {
		Employee employee = findAndReturnEmployeeById(employeeId);
		LeaveType leaveType = new LeaveType();
		leaveType.setId(typeId);

		EmployeeLeave leave = (EmployeeLeave) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeLeave.class).add(Restrictions.eq("employee", employee))
				.add(Restrictions.eq("title", leaveType)).uniqueResult();
		EmployeeLeaveVo vo = new EmployeeLeaveVo();
		if (leave != null) {
			vo.setCarryOver(leave.getCarryOver());
			vo.setLeaves(leave.getLeaves());
			vo.setLeaveType(leave.getType());
			vo.setLeaveStatus(leave.getStatus());
		}
		return vo;
	}

	/**
	 * method to get employee qualification
	 */
	public EmployeeQualificationVo getQualificationByEmployeeAndDegree(Long employeeId, Long degree) {
		EmployeeQualifications qualifications = (EmployeeQualifications) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeQualifications.class).add(Restrictions.eq("employee.id", employeeId))
				.add(Restrictions.eq("degree.id", degree)).uniqueResult();
		return creatQualificatioVo(qualifications);
	}

	/**
	 * method to get language
	 */
	public EmployeeLanguageVo getLanguagesByEmployeeAndLanguage(Long employeeId, Long languageId) {
		EmployeeLanguage language = (EmployeeLanguage) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeLanguage.class).add(Restrictions.eq("employee.id", employeeId))
				.add(Restrictions.eq("language.id", languageId)).uniqueResult();
		return createLanguageVo(language);
	}

	/**
	 * method to get assets
	 */
	public EmployeeOrganisationAssetsVo getAssetsByEmployeeAndAsset(Long employeeId, Long assetId) {
		EmployeeOrganizationAssets assets = (EmployeeOrganizationAssets) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeOrganizationAssets.class).add(Restrictions.eq("employee.id", employeeId))
				.add(Restrictions.eq("organisationAssets.id", assetId)).uniqueResult();
		return createAssetsVo(assets);
	}

	/**
	 * method to get skills
	 */
	public EmployeeSkillsVo getSkillByEmployeeAndSkill(Long employeeId, Long skillId) {
		EmployeeSkills skills = (EmployeeSkills) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeSkills.class).add(Restrictions.eq("employee.id", employeeId))
				.add(Restrictions.eq("skills.id", skillId)).uniqueResult();
		return createSkill(skills);
	}

	/**
	 * method to list superiors and subordinates
	 */
	public SuperiorSubordinateVo listSuperiorSubordinates(Long employeeId) {
		return createSuperiorsSubordinateVo(findAndReturnEmployeeById(employeeId));
	}

	/**
	 * method to create superiors subordinates
	 * 
	 * @param employee
	 * @return
	 */
	private SuperiorSubordinateVo createSuperiorsSubordinateVo(Employee employee) {
		SuperiorSubordinateVo vo = new SuperiorSubordinateVo();
		if (employee.getSuperiors() != null) {
			List<SuperiorVo> superiorVos = new ArrayList<SuperiorVo>();
			for (Employee superior : employee.getSuperiors()) {
				superiorVos.add(createSuperiorVo(superior));
			}
			vo.setSuperiors(superiorVos);
		}
		if (employee.getSubordinates() != null) {
			List<SubordinateVo> subordinateVos = new ArrayList<SubordinateVo>();
			for (Employee subordinate : employee.getSubordinates()) {
				subordinateVos.add(createSubordinateVo(subordinate));
			}
			vo.setSubordinates(subordinateVos);
		}
		return vo;
	}

	/**
	 * method to create subordinate vo
	 * 
	 * @param employee
	 * @return
	 */
	private SubordinateVo createSubordinateVo(Employee subordinate) {
		SubordinateVo subordinateVo = new SubordinateVo();
		subordinateVo.setSubordinateId(subordinate.getId());
		subordinateVo.setSubordinateCode(subordinate.getEmployeeCode());
		UserProfile profile = subordinate.getUserProfile();
		if (profile != null)
			subordinateVo.setSubordinateName(profile.getFirstName() + " " + profile.getLastname());
		return subordinateVo;
	}

	/**
	 * method to create superiorVo
	 * 
	 * @param superior
	 * @return
	 */
	private SuperiorVo createSuperiorVo(Employee superior) {
		SuperiorVo superiorVo = new SuperiorVo();
		superiorVo.setSuperiorId(superior.getId());
		superiorVo.setSuperiorCode(superior.getEmployeeCode());
		UserProfile profile = superior.getUserProfile();
		if (profile != null)
			superiorVo.setSuperiorName(profile.getFirstName() + " " + profile.getLastname());
		return superiorVo;
	}

	/**
	 * method to get superiors&subordinates by designation
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public SuperiorSubordinateVo getSuperiorSubordinateByDesignation(Long designationId) {
		SuperiorSubordinateVo superiorSubordinateVo = new SuperiorSubordinateVo();
		List<SubordinateVo> subordinateVos = new ArrayList<SubordinateVo>();
		List<SuperiorVo> vos = new ArrayList<SuperiorVo>();

		EmployeeDesignation designation = (EmployeeDesignation) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeDesignation.class).add(Restrictions.eq("id", designationId)).uniqueResult();
		Long rank = designation.getRank();

		Criteria criteriaSub = this.sessionFactory.getCurrentSession().createCriteria(EmployeeDesignation.class)
				.add(Restrictions.gt("rank", rank));
		criteriaSub.setResultTransformer(criteriaSub.DISTINCT_ROOT_ENTITY);
		List<EmployeeDesignation> subordinateDesignations = criteriaSub.list();
		if (!subordinateDesignations.isEmpty()) {
			Criteria criteriaSub2 = this.sessionFactory.getCurrentSession().createCriteria(Employee.class)
					.add(Restrictions.in("employeeDesignation", subordinateDesignations));
			criteriaSub2.setResultTransformer(criteriaSub2.DISTINCT_ROOT_ENTITY);
			List<Employee> subordinates = criteriaSub2.list();

			if (subordinates != null) {
				for (Employee subordinate : subordinates) {
					subordinateVos.add(createSubordinateVo(subordinate));
				}
			}
		}
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmployeeDesignation.class)
				.add(Restrictions.le("rank", rank));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<EmployeeDesignation> superiorDesignations = criteria.list();
		if (!superiorDesignations.isEmpty()) {
			Criteria criteria2 = this.sessionFactory.getCurrentSession().createCriteria(Employee.class)
					.add(Restrictions.in("employeeDesignation", superiorDesignations));
			criteria2.setResultTransformer(criteria2.DISTINCT_ROOT_ENTITY);
			List<Employee> superiors = criteria2.list();

			if (superiors != null) {
				for (Employee superior : superiors) {
					vos.add(createSuperiorVo(superior));
				}
			}
		}
		superiorSubordinateVo.setSuperiors(vos);
		superiorSubordinateVo.setSubordinates(subordinateVos);
		return superiorSubordinateVo;
	}

	/**
	 * method to get role types
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public RolesVo getRoleTypes() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(RolesCategory.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<RolesCategory> roles = criteria.list();
		RolesVo rolesVo = new RolesVo();

		List<RoleHomeVo> homeVos = new ArrayList<RoleHomeVo>();
		List<RoleOrganisationVo> organisationVos = new ArrayList<RoleOrganisationVo>();
		List<RoleRecruitmentVo> recruitmentVos = new ArrayList<RoleRecruitmentVo>();
		List<RoleEmployeesVo> employeesVos = new ArrayList<RoleEmployeesVo>();
		List<RolePayrollVo> payrollVos = new ArrayList<RolePayrollVo>();
		List<RoleReportsVo> reportsVos = new ArrayList<RoleReportsVo>();
		List<RoleTimeSheetVo> timeSheetVos = new ArrayList<RoleTimeSheetVo>();
		List<RoleMiscellaneousVo> miscellaneousVos = new ArrayList<RoleMiscellaneousVo>();
		List<RoleERPVo> erpVos = new ArrayList<RoleERPVo>();

		for (RolesCategory category : roles) {
			if (category.getCategory().equalsIgnoreCase(RolesCategoryConstant.HOME)) {
				for (RoleTypeTitles titles : category.getTitles()) {
					homeVos.add(setHomeVo(titles));
				}
			}
			if (category.getCategory().equalsIgnoreCase(RolesCategoryConstant.ORGANIZATION)) {
				for (RoleTypeTitles titles : category.getTitles()) {
					organisationVos.add(setOrganisationVo(titles));
				}
			}
			if (category.getCategory().equalsIgnoreCase(RolesCategoryConstant.RECRUITMENT)) {
				for (RoleTypeTitles titles : category.getTitles()) {
					recruitmentVos.add(setRecruitmentVo(titles));
				}
			}
			if (category.getCategory().equalsIgnoreCase(RolesCategoryConstant.EMPLOYEES)) {
				for (RoleTypeTitles titles : category.getTitles()) {
					employeesVos.add(setEmployeeVo(titles));
				}
			}
			if (category.getCategory().equalsIgnoreCase(RolesCategoryConstant.TIME_SHEET)) {
				for (RoleTypeTitles titles : category.getTitles()) {
					timeSheetVos.add(setTimeSheetVo(titles));
				}
			}
			if (category.getCategory().equalsIgnoreCase(RolesCategoryConstant.PAYROLL)) {
				for (RoleTypeTitles titles : category.getTitles()) {
					payrollVos.add(setPayRollVo(titles));
				}
			}
			if (category.getCategory().equalsIgnoreCase(RolesCategoryConstant.REPORTS)) {
				for (RoleTypeTitles titles : category.getTitles()) {
					reportsVos.add(setReportsVo(titles));
				}
			}
			if (category.getCategory().equalsIgnoreCase(RolesCategoryConstant.MISCELLANEOUS)) {
				for (RoleTypeTitles titles : category.getTitles()) {
					miscellaneousVos.add(setMiscellaneousVo(titles));
				}
			}
			if (category.getCategory().equalsIgnoreCase(RolesCategoryConstant.ERP)) {
				for (RoleTypeTitles titles : category.getTitles()) {
					erpVos.add(setErpVo(titles));
				}
			}
		}
		rolesVo.setTimeSheetVos(timeSheetVos);
		rolesVo.setErpVos(erpVos);
		rolesVo.setHomeVos(homeVos);
		rolesVo.setOrganisationVos(organisationVos);
		rolesVo.setRecruitmentVos(recruitmentVos);
		rolesVo.setEmployeesVos(employeesVos);
		rolesVo.setPayrollVos(payrollVos);
		rolesVo.setReportsVos(reportsVos);
		rolesVo.setMiscellaneousVos(miscellaneousVos);

		return rolesVo;
	}

	/**
	 * method to set home role vo
	 * 
	 * @param roleTypeTitle
	 * @return
	 */
	private RoleHomeVo setHomeVo(RoleTypeTitles roleTypeTitle) {
		RoleHomeVo homeVo = new RoleHomeVo();
		homeVo.setTitle(roleTypeTitle.getTitle());
		List<RoleTypesVo> list = new ArrayList<RoleTypesVo>();
		for (RoleType roleType : roleTypeTitle.getRoleTypes()) {
			list.add(setRolesVo(roleType));
		}
		homeVo.setList(list);
		return homeVo;
	}

	private RoleERPVo setErpVo(RoleTypeTitles roleTypeTitle) {
		RoleERPVo erpVo = new RoleERPVo();
		erpVo.setTitle(roleTypeTitle.getTitle());
		List<RoleTypesVo> list = new ArrayList<RoleTypesVo>();
		for (RoleType roleType : roleTypeTitle.getRoleTypes()) {
			list.add(setRolesVo(roleType));
		}
		erpVo.setList(list);
		return erpVo;
	}

	private RoleTimeSheetVo setTimeSheetVo(RoleTypeTitles roleTypeTitle) {
		RoleTimeSheetVo homeVo = new RoleTimeSheetVo();
		homeVo.setTitle(roleTypeTitle.getTitle());
		List<RoleTypesVo> list = new ArrayList<RoleTypesVo>();
		for (RoleType roleType : roleTypeTitle.getRoleTypes()) {
			list.add(setRolesVo(roleType));
		}
		homeVo.setList(list);
		return homeVo;
	}

	/**
	 * method to set home role vo
	 * 
	 * @param roleTypeTitle
	 * @return
	 */
	private RoleOrganisationVo setOrganisationVo(RoleTypeTitles roleTypeTitle) {
		RoleOrganisationVo vo = new RoleOrganisationVo();
		vo.setTitle(roleTypeTitle.getTitle());
		List<RoleTypesVo> list = new ArrayList<RoleTypesVo>();
		for (RoleType roleType : roleTypeTitle.getRoleTypes()) {
			list.add(setRolesVo(roleType));
		}
		vo.setList(list);
		return vo;
	}

	/**
	 * method to set recruitment role vo
	 * 
	 * @param roleTypeTitle
	 * @return
	 */
	private RoleRecruitmentVo setRecruitmentVo(RoleTypeTitles roleTypeTitle) {
		RoleRecruitmentVo vo = new RoleRecruitmentVo();
		vo.setTitle(roleTypeTitle.getTitle());
		List<RoleTypesVo> list = new ArrayList<RoleTypesVo>();
		for (RoleType roleType : roleTypeTitle.getRoleTypes()) {
			list.add(setRolesVo(roleType));
		}
		vo.setList(list);
		return vo;
	}

	/**
	 * method to set employee role vo
	 * 
	 * @param roleTypeTitle
	 * @return
	 */
	private RoleEmployeesVo setEmployeeVo(RoleTypeTitles roleTypeTitle) {
		RoleEmployeesVo vo = new RoleEmployeesVo();
		vo.setTitle(roleTypeTitle.getTitle());
		List<RoleTypesVo> list = new ArrayList<RoleTypesVo>();
		for (RoleType roleType : roleTypeTitle.getRoleTypes()) {
			list.add(setRolesVo(roleType));
		}
		vo.setList(list);
		return vo;
	}

	/**
	 * method to set payRoll role vo
	 * 
	 * @param roleTypeTitle
	 * @return
	 */
	private RolePayrollVo setPayRollVo(RoleTypeTitles roleTypeTitle) {
		RolePayrollVo vo = new RolePayrollVo();
		vo.setTitle(roleTypeTitle.getTitle());
		List<RoleTypesVo> list = new ArrayList<RoleTypesVo>();
		for (RoleType roleType : roleTypeTitle.getRoleTypes()) {
			list.add(setRolesVo(roleType));
		}
		vo.setList(list);
		return vo;
	}

	/**
	 * method to set reports role vo
	 * 
	 * @param roleTypeTitle
	 * @return
	 */
	private RoleReportsVo setReportsVo(RoleTypeTitles roleTypeTitle) {
		RoleReportsVo homeVo = new RoleReportsVo();
		homeVo.setTitle(roleTypeTitle.getTitle());
		List<RoleTypesVo> list = new ArrayList<RoleTypesVo>();
		for (RoleType roleType : roleTypeTitle.getRoleTypes()) {
			list.add(setRolesVo(roleType));
		}
		homeVo.setList(list);
		return homeVo;
	}

	/**
	 * method to set miscellaneous role vo
	 * 
	 * @param roleTypeTitle
	 * @return
	 */
	private RoleMiscellaneousVo setMiscellaneousVo(RoleTypeTitles roleTypeTitle) {
		RoleMiscellaneousVo homeVo = new RoleMiscellaneousVo();
		homeVo.setTitle(roleTypeTitle.getTitle());
		List<RoleTypesVo> list = new ArrayList<RoleTypesVo>();
		for (RoleType roleType : roleTypeTitle.getRoleTypes()) {
			list.add(setRolesVo(roleType));
		}
		homeVo.setList(list);
		return homeVo;
	}

	/**
	 * method to set home vo
	 * 
	 * @param roleType
	 * @return
	 */
	private RoleTypesVo setRolesVo(RoleType roleType) {
		RoleTypesVo homeVo = new RoleTypesVo();
		homeVo.setRoleType(roleType.getType());
		homeVo.setRoleTypeId(roleType.getId());
		return homeVo;
	}

	/**
	 * method to save roles
	 */
	public void saveRoles(RolesVo rolesVo) {
		Employee employee = findAndReturnEmployeeById(rolesVo.getEmployeeId());
		Query query = this.sessionFactory.getCurrentSession()
				.createQuery("DELETE FROM EmployeeRoles WHERE roleTypeId.employee.id=?");
		query.setLong(0, employee.getId());

		query.executeUpdate();

		if (rolesVo.getViewIds() != null) {
			for (Long id : rolesVo.getViewIds()) {

				this.sessionFactory.getCurrentSession().update(setViewRole(employee, id));
			}
		}
		if (rolesVo.getAddIds() != null) {
			for (Long id : rolesVo.getAddIds()) {
				this.sessionFactory.getCurrentSession().update(setAddRole(employee, id));
			}
		}
		if (rolesVo.getUpdateIds() != null) {
			for (Long id : rolesVo.getUpdateIds()) {
				this.sessionFactory.getCurrentSession().update(setUpdateRole(employee, id));
			}
		}
		if (rolesVo.getDeleteIds() != null) {
			for (Long id : rolesVo.getDeleteIds()) {
				this.sessionFactory.getCurrentSession().update(setDeleteRole(employee, id));
			}
		}
	}

	/**
	 * set view roles
	 * 
	 * @param employee
	 * @param id
	 * @return
	 */
	private Employee setViewRole(Employee employee, Long id) {
		EmployeeRoles employeeRoles = new EmployeeRoles();
		RoleType roleType = new RoleType();
		roleType.setId(id);

		Roles roles = new Roles();
		roles.setId(1L);

		employeeRoles.setEmployee(employee);
		employeeRoles.setRoles(roles);
		employeeRoles.setRoleType(roleType);

		employee.getEmployeeRoles().add(employeeRoles);

		return employee;
	}

	/**
	 * set add roles
	 * 
	 * @param employee
	 * @param id
	 * @return
	 */
	private Employee setAddRole(Employee employee, Long id) {
		EmployeeRoles employeeRoles = new EmployeeRoles();
		RoleType roleType = new RoleType();
		roleType.setId(id);

		Roles roles = new Roles();
		roles.setId(2L);

		employeeRoles.setEmployee(employee);
		employeeRoles.setRoles(roles);
		employeeRoles.setRoleType(roleType);

		employee.getEmployeeRoles().add(employeeRoles);
		return employee;
	}

	/**
	 * set update roles
	 * 
	 * @param employee
	 * @param id
	 * @return
	 */
	private Employee setUpdateRole(Employee employee, Long id) {
		EmployeeRoles employeeRoles = new EmployeeRoles();
		RoleType roleType = new RoleType();
		roleType.setId(id);

		Roles roles = new Roles();
		roles.setId(3L);

		employeeRoles.setEmployee(employee);
		employeeRoles.setRoles(roles);
		employeeRoles.setRoleType(roleType);

		employee.getEmployeeRoles().add(employeeRoles);
		return employee;
	}

	/**
	 * set delete roles
	 * 
	 * @param employee
	 * @param id
	 * @return
	 */
	private Employee setDeleteRole(Employee employee, Long id) {
		EmployeeRoles employeeRoles = new EmployeeRoles();
		RoleType roleType = new RoleType();
		roleType.setId(id);

		Roles roles = new Roles();
		roles.setId(4L);

		employeeRoles.setEmployee(employee);
		employeeRoles.setRoles(roles);
		employeeRoles.setRoleType(roleType);

		employee.getEmployeeRoles().add(employeeRoles);
		return employee;
	}

	/**
	 * method to get employee by code
	 */
	public EmployeeVo getEmployeeByCode(String code) {
		return createViewEmployeeVo(findAndReturnEmployeeByCode(code));
	}

	/**
	 * method to find employee roles
	 */
	public RolesVo findRolesByEmployee(Long id) {
		return createRolesVo(findAndReturnEmployeeById(id));
	}

	/**
	 * method to create roles vo
	 * 
	 * @param employee
	 * @return
	 */
	private RolesVo createRolesVo(Employee employee) {
		RolesVo rolesVo = new RolesVo();
		rolesVo.setEmployeeId(employee.getId());
		if (employee.getEmployeeRoles() != null) {

			List<Long> viewIds = new ArrayList<Long>();
			List<Long> addIds = new ArrayList<Long>();
			List<Long> updateIds = new ArrayList<Long>();
			List<Long> deleteIds = new ArrayList<Long>();

			for (EmployeeRoles roles : employee.getEmployeeRoles()) {
				if (roles.getRoles().getPrivilege().equalsIgnoreCase("VIEW")) {
					viewIds.add(roles.getRoleType().getId());
				} else if (roles.getRoles().getPrivilege().equalsIgnoreCase("ADD")) {
					addIds.add(roles.getRoleType().getId());
				} else if (roles.getRoles().getPrivilege().equalsIgnoreCase("UPDATE")) {
					updateIds.add(roles.getRoleType().getId());
				} else if (roles.getRoles().getPrivilege().equalsIgnoreCase("DELETE")) {
					deleteIds.add(roles.getRoleType().getId());
				}
			}
			rolesVo.setAddIds(addIds);
			rolesVo.setViewIds(viewIds);
			rolesVo.setUpdateIds(updateIds);
			rolesVo.setDeleteIds(deleteIds);
		}
		return rolesVo;
	}

	/**
	 * method to get employees by departments
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> getEmployeesByDepartments(List<Long> departmentIds) {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.in("departmentEmployees.id", departmentIds));
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get employees by branches
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeVo> getEmployeesByBranches(List<Long> branchIds) {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.in("branch.id", branchIds));
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get employees by branches and departments
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> getEmployeesByBranchesAndDepartments(List<Long> branchIds, List<Long> departmentIds) {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.in("branch.id", branchIds));
		criteria.add(Restrictions.in("departmentEmployees.id", departmentIds));
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to update employee as admin
	 */
	public void updateEmployeeAsAdmin(Long employeeId, Boolean isAdmin, List<Long> branchIds) {
		Employee employee = findAndReturnEmployeeById(employeeId);
		employee.setIsAdmin(isAdmin);
		for (Branch branch : employee.getBranches())
			branch.getUsers().remove(employee);
		employee.getBranches().clear();
		if (branchIds != null) {
			for (Long id : branchIds) {
				Branch branch = branchDao.findAndReturnBranchObjectById(id);
				employee.getBranches().add(branch);
				branch.getUsers().add(employee);
			}
		}
		this.sessionFactory.getCurrentSession().merge(employee);
	}

	/**
	 * method to list all admins
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> listAllAdmins() {
		List<EmployeeVo> vos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("isAdmin", true));
		List<Employee> admins = criteria.list();
		for (Employee admin : admins) {
			vos.add(createDetailedEmployeeVo(admin));
		}
		return vos;
	}

	/**
	 * method to list employee full details
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> listEmployeeCompleteDetails() {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createViewEmployeeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get bank account details by employee and account type
	 */
	public EmployeeBankAccountVo getBankAccountByEmployeeAndType(Long employeeId, Long typeId) {
		EmployeeBankAccount account = (EmployeeBankAccount) this.sessionFactory.getCurrentSession()
				.createCriteria(EmployeeBankAccount.class).add(Restrictions.eq("accountType.id", typeId))
				.add(Restrictions.eq("employee.id", employeeId)).uniqueResult();
		return createBankAccountVo(account);
	}

	/**
	 * method to list employees with payroll
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> listEmployeesWithPayroll() {
		List<EmployeeVo> vos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("withPayroll", true));
		List<Employee> admins = criteria.list();
		for (Employee admin : admins) {
			vos.add(createDetailedEmployeeVo(admin));
		}
		return vos;
	}

	/**
	 * method to list employees with payroll by branch and departments
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> listEmployeesWithPayroll(Long branchId, Long departmentId) {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class)
				.add(Restrictions.eq("branch.id", branchId))
				.add(Restrictions.eq("departmentEmployees.id", departmentId)).add(Restrictions.eq("withPayroll", true));
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get employees by type
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> listEmployeesByEmployeeType(Long employeeTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeType.id", employeeTypeId));
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get employees by employee category
	 * 
	 * @param employeeCategoryId
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> listEmployeesByEmployeeCategory(Long employeeCategoryId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeCategory.id", employeeCategoryId));
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get employees by category and type
	 * 
	 * @param employeeCategoryId
	 * @param employeeTypeId
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> listEmployeesByEmployeeCategoryAndType(Long employeeCategoryId, Long employeeTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeCategory.id", employeeCategoryId));
		criteria.add(Restrictions.eq("employeeType.id", employeeTypeId));
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get employees by branch , category and type
	 * 
	 * @param branchId
	 * @param employeeCategoryId
	 * @param employeeTypeId
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> listEmployeesByBranchEmployeeCategoryAndType(Long branchId, Long employeeCategoryId,
			Long employeeTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeCategory.id", employeeCategoryId));
		criteria.add(Restrictions.eq("employeeType.id", employeeTypeId));
		criteria.add(Restrictions.eq("branch.id", branchId));
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get employees by department , category and type
	 * 
	 * @param departmentId
	 * @param employeeCategoryId
	 * @param employeeTypeId
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> listEmployeesByDepartmentEmployeeCategoryAndType(Long departmentId, Long employeeCategoryId,
			Long employeeTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeCategory.id", employeeCategoryId));
		criteria.add(Restrictions.eq("employeeType.id", employeeTypeId));
		criteria.add(Restrictions.eq("departmentEmployees.id", departmentId));
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	/**
	 * method to get employees by branch , department , category and type
	 * 
	 * @param branchId
	 * @param departmentId
	 * @param employeeCategoryId
	 * @param employeeTypeId
	 * @return
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> listEmployeesByBranchDepartmentEmployeeCategoryAndType(Long branchId, Long departmentId,
			Long employeeCategoryId, Long employeeTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeCategory.id", employeeTypeId));
		criteria.add(Restrictions.eq("employeeType.id", employeeCategoryId));
		criteria.add(Restrictions.eq("departmentEmployees.id", departmentId));
		criteria.add(Restrictions.eq("branch.id", branchId));
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	public EmployeeLeave findEmployeeLeaveByEMployeeAndTyep(Employee employee, LeaveType leaveType) {
		return (EmployeeLeave) this.sessionFactory.getCurrentSession().createCriteria(EmployeeLeave.class)
				.add(Restrictions.eq("employee", employee)).add(Restrictions.eq("title", leaveType)).uniqueResult();
	}

	/**
	 * method to get employee privilages
	 */
	public SessionRolesVo getEmployeeRoles(Long employeeId) {
		SessionRolesVo sessionRolesVo = new SessionRolesVo();
		Employee employee = findAndReturnEmployeeById(employeeId);
		Map<String, RoleSessionVo> privilages = new HashMap<String, RoleSessionVo>();
		Set<EmployeeRoles> employeeRoles = employee.getEmployeeRoles();
		if (employeeRoles != null) {
			String roleType = null;
			RoleSessionVo roleSessionVo = new RoleSessionVo();
			Boolean view = false;
			Boolean add = false;
			Boolean update = false;
			Boolean delete = false;

			for (EmployeeRoles roles : employeeRoles) {
				String type = roles.getRoleType().getType();
				if (roleType == null)
					roleType = type;
				else {
					if (!roleType.equals(type)) {
						privilages.put(roleType, roleSessionVo);
						roleType = type;
						roleSessionVo = new RoleSessionVo();
						view = false;
						add = false;
						update = false;
						delete = false;
					}
				}
				String privilage = roles.getRoles().getPrivilege();
				if (privilage.equalsIgnoreCase("VIEW")) {
					view = true;
				} else if (privilage.equalsIgnoreCase("ADD")) {
					add = true;
				} else if (privilage.equalsIgnoreCase("UPDATE")) {
					update = true;
				} else if (privilage.equalsIgnoreCase("DELETE")) {
					delete = true;
				}
				roleSessionVo.setAdd(add);
				roleSessionVo.setDelete(delete);
				roleSessionVo.setUpdate(update);
				roleSessionVo.setView(view);
			}
			privilages.put(roleType, roleSessionVo);
		}
		sessionRolesVo.setRoles(privilages);
		return sessionRolesVo;
	}

	/**
	 * method to carry over leave
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public void leaveCarryOver() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			if (employee.getLeaves() != null) {
				employee.setLeaves(resetLeaves(employee.getLeaves(), employee));
				this.sessionFactory.getCurrentSession().update(employee);
			}
		}
	}

	private Set<EmployeeLeave> resetLeaves(Set<EmployeeLeave> leaves, Employee employee) {
		Set<EmployeeLeave> newLeaves = new HashSet<EmployeeLeave>();
		for (EmployeeLeave leave : leaves) {
			if (leave.getLeavesBalance() != null) {
				Long balance = leave.getLeavesBalance();
				if (balance > 0) {
					if (leave.getCarryOver().equalsIgnoreCase("yes")) {
						leave.setLeaves(leave.getLeaves() + balance);
					}
				}
			}
			leave.setLeavesBalance(0L);
			leave.setLeavesTaken(0L);
			newLeaves.add(leave);
		}
		return newLeaves;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> findAllEmployeesDeetailsByCode(String employeeCode) {
		List<EmployeeVo> vos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeCode", employeeCode));
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			vos.add(createDetailedEmployeeVo(employee));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<EmployeeVo> findInactiveEmployees() {
		List<EmployeeVo> employeeVo = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Inactive")));
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVo.add(createIdCodeVo(employee));
		}
		return employeeVo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<Employee> findEmployeesByDesignation(Long designationId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.add(Restrictions.eq("employeeDesignation.id", designationId));
		return criteria.list();
	}

	public EmployeeVo getEmployeesDetails(String employeeCode) {
		Employee employee = findAndReturnEmployeeByCode(employeeCode);
		if (employee != null)
			return createViewEmployeeVo(employee);
		else
			return null;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeVo> findPayrollOptionEmployees() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.isNotNull("payRollOption"));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Employee> employees = criteria.list();
		List<EmployeeVo> employeeVo = new ArrayList<EmployeeVo>();
		for (Employee employee : employees) {
			employeeVo.add(createIdCodeVo(employee));
		}
		return employeeVo;
	}

	public String generateEmployeeCode() {
		NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(NumberPropertyConstants.EMPLOYEE);
		String employeeCode = numberPropertyVo.getPrefix() + String.valueOf(numberPropertyVo.getNumber());
		return employeeCode;
	}

	public void updateEmployeeStatus(Long id) {
		Employee employee = this.findAndReturnEmployeeById(id);
		employee.setEmployeeStatus(statusDao.findStatus("Inactive"));
		this.sessionFactory.getCurrentSession().merge(employee);
		userActivitiesService.saveUserActivity("Inactivated Employee " + employee.getEmployeeCode());
	}

	public List<EmployeeVo> getEmployeesByBranchAndDepartments(Long branchId, List<Long> departmentIds) {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("branch.id", branchId));
		criteria.add(Restrictions.in("departmentEmployees.id", departmentIds));
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createIdCodeVo(employee));
		}
		return employeeVos;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<EmployeeVo> getDetailedEmployeesByBranchAndChildBranches(Long branchId) {
		List<EmployeeVo> employeeVos = new ArrayList<EmployeeVo>();
		Branch branch = branchDao.findAndReturnBranchObjectById(branchId);
		Set<Branch> branchs = branch.getBranches();
		branchs.add(branch);

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.in("branch", branchs));
		List<Employee> employees = criteria.list();
		for (Employee employee : employees) {
			employeeVos.add(createDetailedEmployeeVo(employee));
		}
		return employeeVos;
	}
	
	private List<Employee> findEmployeeByBranchAndDepartments(Long branchId, List<Long> departmentIds) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class)
				.add(Restrictions.eq("branch.id", branchId))
				.add(Restrictions.in("departmentEmployees.id", departmentIds));
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}
	
	public List<AttendanceEmployeeVo> findEmployeesForAttendanceStatus(Long branchId, List<Long> departmentIds) {
		List<AttendanceEmployeeVo> vos = new ArrayList<AttendanceEmployeeVo>();
		List<Employee > employees = this.findEmployeeByBranchAndDepartments(branchId, departmentIds);
		for(Employee employee : employees){
			AttendanceEmployeeVo vo = new AttendanceEmployeeVo();
			vo.setEmployeeCode(employee.getEmployeeCode());
			vo.setEmployeeId(employee.getId());
			UserProfile profile = employee.getUserProfile();
			if(profile!=null){
				vo.setName(profile.getFirstName()+" "+profile.getLastname());
			}
			for(EmployeeLeave leave : employee.getLeaves()){
				EmployeeLeaveVo leaveVo = new EmployeeLeaveVo();
				leaveVo.setLeaveId(leave.getId());
				leaveVo.setLeaveTitle(leave.getTitle().getType());
				vo.getLeaveVos().add(leaveVo);
			}
			vos.add(vo);
		}
		return vos;
	}

	public List<EmployeeVo> listAllAutoApproveEmployees() {
		List<EmployeeVo> vos = new ArrayList<EmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Employee.class);
		criteria.add(Restrictions.eq("employeeStatus", statusDao.findStatus("Active")));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("isAutoApprove", true));
		List<Employee> admins = criteria.list();
		for (Employee admin : admins) {
			vos.add(createDetailedEmployeeVo(admin));
		}
		return vos;
	}

	public void updateEmployeeWithAutoApprove(Long employeeId, Boolean isAutoApprove) {
		Employee employee = findAndReturnEmployeeById(employeeId);
		employee.setIsAutoApprove(isAutoApprove);
		this.sessionFactory.getCurrentSession().merge(employee);

	}

}
