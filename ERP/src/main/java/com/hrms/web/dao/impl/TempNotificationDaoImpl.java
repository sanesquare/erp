package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.TempNotifications;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Shamsheer
 * @since 19-May-2015
 */
@Repository
public class TempNotificationDaoImpl extends AbstractDao implements TempNotificationDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	public void saveNotification(TempNotificationVo notificationVo) {
		TempNotifications notifications = new TempNotifications();
		notifications.setSubject(notificationVo.getSubject());
		if (notificationVo.getEmployeeIds().size() > 0) {
			for (Long id : notificationVo.getEmployeeIds()) {
				Employee employee = employeeDao.findAndReturnEmployeeById(id);
				notifications.getEmployees().add(employee);
			}
		} else {
			for (String code : notificationVo.getEmployeeCodes()) {
				Employee employee = employeeDao.findAndReturnEmployeeByCode(code);
				notifications.getEmployees().add(employee);
			}
		}
		if (!notifications.getEmployees().isEmpty())
			this.sessionFactory.getCurrentSession().saveOrUpdate(notifications);
	}

	public void updateNotification(TempNotificationVo notificationVo) {
		// TODO Auto-generated method stub

	}

	public void deleteNotification(Long id, String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		TempNotifications notifications = (TempNotifications) this.sessionFactory.getCurrentSession().createCriteria(TempNotifications.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
		notifications.getEmployees().remove(employee);
		if(notifications.getEmployees().size()==0)
			this.sessionFactory.getCurrentSession().delete(notifications);
		else
			this.sessionFactory.getCurrentSession().merge(notifications);
	}

	public List<TempNotificationVo> listNotificationForEmployee(Long employeeId) {
		List<TempNotificationVo> notificationVos = new ArrayList<TempNotificationVo>();
		// TODO Auto-generated method stub
		return notificationVos;
	}

	@SuppressWarnings("unchecked")
	public List<TempNotificationVo> listNotificationForEmployee(String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		List<TempNotificationVo> notificationVos = new ArrayList<TempNotificationVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TempNotifications.class);
		List<TempNotifications> notifications = criteria.list();
		List<TempNotifications> tempNotifications = new ArrayList<TempNotifications>();
		for(TempNotifications nn : notifications){
			if(nn.getEmployees().contains(employee))
				tempNotifications.add(nn);
		}
		for(TempNotifications notification : tempNotifications){
			TempNotificationVo notificationVo = new  TempNotificationVo();
			notificationVo.setSubject(notification.getSubject());
			notificationVo.setId(notification.getId());
			notificationVos.add(notificationVo);
		}
		return notificationVos;		
	}
}
