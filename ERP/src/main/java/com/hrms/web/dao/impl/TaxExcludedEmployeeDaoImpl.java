package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.TaxExcludedEmployeeDao;
import com.hrms.web.entities.TaxExcludeEmployee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.TaxExcludedEmployeeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 28-April-2015
 *
 */
@Repository
public class TaxExcludedEmployeeDaoImpl extends AbstractDao implements TaxExcludedEmployeeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	public void saveTaxExcludedEmployee(TaxExcludedEmployeeVo taxExcludedEmployeeVo) {
		deleteNonAvailableEmployees(taxExcludedEmployeeVo);
		for (String employeeCode : taxExcludedEmployeeVo.getExcludedEmployeeCodes()) {
			TaxExcludeEmployee taxExcludeEmployee = new TaxExcludeEmployee();
			if (findTaxExcludedEmployee(employeeCode) == null) {
				taxExcludeEmployee.setTaxExluder(employeeDao.findAndReturnEmployeeByCode(employeeCode));
				this.sessionFactory.getCurrentSession().saveOrUpdate(taxExcludeEmployee);
			}
		}
	}

	/**
	 * method to delete employee who is already available but not in the new
	 * list
	 * 
	 * @param taxExcludedEmployeeVo
	 * @return
	 */
	private void deleteNonAvailableEmployees(TaxExcludedEmployeeVo taxExcludedEmployeeVo) {
		List<String> availableList = findTaxExcludedEmployeeList().getExcludedEmployeeCodes();
		List<String> newList = taxExcludedEmployeeVo.getExcludedEmployeeCodes();
		List<String> tempList = new ArrayList<String>();

		if (availableList != null)
			tempList.addAll(availableList);

		tempList.removeAll(newList);

		for (String employeeCode : tempList) {
			deleteTaxExcludedEmployee(findTaxExcludedEmployee(employeeCode).getId());
		}
	}

	public void updateTaxExcludedEmployee(TaxExcludedEmployeeVo taxExcludedEmployeeVo) {
		// TODO Auto-generated method stub

	}

	public void deleteTaxExcludedEmployee(Long taxExcludedId) {
		TaxExcludeEmployee taxExcludeEmployee = findTaxExcludedEmployee(taxExcludedId);
		if (taxExcludeEmployee == null)
			throw new ItemNotFoundException("Tax Excluded Employee Details Not Found");
		this.sessionFactory.getCurrentSession().delete(taxExcludeEmployee);
	}

	public TaxExcludeEmployee findTaxExcludedEmployee(Long taxExludedId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxExcludeEmployee.class)
				.add(Restrictions.eq("id", taxExludedId));
		if (criteria.uniqueResult() == null)
			return null;
		return (TaxExcludeEmployee) criteria.uniqueResult();
	}

	public TaxExcludeEmployee findTaxExcludedEmployee(String employeeCode) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxExcludeEmployee.class)
				.add(Restrictions.eq("taxExluder", employeeDao.findAndReturnEmployeeByCode(employeeCode)));
		if (criteria.uniqueResult() == null)
			return null;
		return (TaxExcludeEmployee) criteria.uniqueResult();
	}

	/**
	 * method to create TaxExcludedEmployeeVo object
	 * 
	 * @param taxExcludeEmployee
	 * @return taxExcludedEmployeeVo
	 */
	private TaxExcludedEmployeeVo createTaxExcludedEmployeeVo(TaxExcludeEmployee taxExcludeEmployee) {
		TaxExcludedEmployeeVo taxExcludedEmployeeVo = new TaxExcludedEmployeeVo();
		taxExcludedEmployeeVo.setTax_excluded_emp_id(taxExcludeEmployee.getId());
		taxExcludedEmployeeVo.setExcludedEmployeeCode(taxExcludeEmployee.getTaxExluder().getEmployeeCode());
		taxExcludedEmployeeVo.setExcludedEmployeeName(taxExcludeEmployee.getTaxExluder().getUserProfile()
				.getFirstName()
				+ " " + taxExcludeEmployee.getTaxExluder().getUserProfile().getLastname());
		return taxExcludedEmployeeVo;
	}

	/**
	 * method to create TaxExcludedEmployeeVo object with list of excluders
	 * 
	 * @param taxExcludeEmployees
	 * @return taxExcludedEmployeeVo
	 */
	private TaxExcludedEmployeeVo createTaxExcludedEmployeesList(List<TaxExcludeEmployee> taxExcludeEmployees) {
		TaxExcludedEmployeeVo taxExcludedEmployeeVo = new TaxExcludedEmployeeVo();
		List<String> employeesCode = new ArrayList<String>();
		List<String> employeesName = new ArrayList<String>();
		for (TaxExcludeEmployee employee : taxExcludeEmployees) {
			employeesCode.add(employee.getTaxExluder().getEmployeeCode());
			employeesName.add(employee.getTaxExluder().getUserProfile().getFirstName() + " "
					+ employee.getTaxExluder().getUserProfile().getLastname());
		}
		taxExcludedEmployeeVo.setExcludedEmployeeCodes(employeesCode);
		taxExcludedEmployeeVo.setExcludedEmployeeNames(employeesName);
		return taxExcludedEmployeeVo;
	}

	public TaxExcludedEmployeeVo findTaxExcludedEmployeeById(Long taxExcludedId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxExcludeEmployee.class)
				.add(Restrictions.eq("id", taxExcludedId));
		if (criteria.uniqueResult() == null)
			return null;
		return createTaxExcludedEmployeeVo((TaxExcludeEmployee) criteria.uniqueResult());
	}

	public TaxExcludedEmployeeVo findTaxExcludedEmployeeByEmployee(String employeeCode) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxExcludeEmployee.class)
				.add(Restrictions.eq("taxExluder", employeeDao.findAndReturnEmployeeByCode(employeeCode)));
		if (criteria.uniqueResult() == null)
			return null;
		return createTaxExcludedEmployeeVo((TaxExcludeEmployee) criteria.uniqueResult());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public TaxExcludedEmployeeVo findTaxExcludedEmployeeList() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxExcludeEmployee.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		return createTaxExcludedEmployeesList(criteria.list());
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<TaxExcludedEmployeeVo> findAllTaxExcludedEmployee() {
		List<TaxExcludedEmployeeVo> taxExcludedEmployeeVos = new ArrayList<TaxExcludedEmployeeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TaxExcludeEmployee.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<TaxExcludeEmployee> taxExcludeEmployees = criteria.list();
		for (TaxExcludeEmployee taxExcludeEmployee : taxExcludeEmployees)
			taxExcludedEmployeeVos.add(createTaxExcludedEmployeeVo(taxExcludeEmployee));
		return taxExcludedEmployeeVos;
	}

}
