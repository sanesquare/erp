package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.MstReference;

/**
 * This contain methods  for the 
 * data  which is used among all services 
 * @author Vips
 *
 */
public interface CommonDao {
	
	/**
	 * 
	 * @param refernceType
	 * @return
	 */
	public List<MstReference>  fectMstreferneceByType(String refernceType);

}
