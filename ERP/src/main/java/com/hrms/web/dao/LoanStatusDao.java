package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.LoanStatus;
import com.hrms.web.vo.LoanStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public interface LoanStatusDao {

	/**
	 * method to save LoanStatus
	 * 
	 * @param loanStatusVo
	 */
	public void saveLoanStatus(LoanStatusVo loanStatusVo);

	/**
	 * method to update LoanStatus
	 * 
	 * @param loanStatusVo
	 */
	public void updateLoanStatus(LoanStatusVo loanStatusVo);

	/**
	 * method to delete LoanStatus
	 * 
	 * @param statusId
	 */
	public void deleteLoanStatus(Long statusId);

	/**
	 * method to find LoanStatus by id
	 * 
	 * @param statusId
	 * @return LoanStatus
	 */
	public LoanStatus findLoanStatus(Long statusId);

	/**
	 * method to find LoanStatus by status
	 * 
	 * @param status
	 * @return LoanStatus
	 */
	public LoanStatus findLoanStatus(String status);

	/**
	 * method to find LoanStatus by id
	 * 
	 * @param statusId
	 * @return LoanStatusVo
	 */
	public LoanStatusVo findLoanStatusById(Long statusId);

	/**
	 * method to find LoanStatus by status
	 * 
	 * @param status
	 * @return LoanStatusVo
	 */
	public LoanStatusVo findLoanStatusByStatus(String status);

	/**
	 * method to find all LoanStatus
	 * 
	 * @return List<LoanStatusVo>
	 */
	public List<LoanStatusVo> findAllLoanStatus();
}
