package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.hql.internal.ast.tree.RestrictableStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ContractDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.entities.Contract;
import com.hrms.web.entities.ContractDocuments;
import com.hrms.web.entities.ContractType;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.ContractDocumentVo;
import com.hrms.web.vo.ContractJsonVo;
import com.hrms.web.vo.ContractVo;
import com.hrms.web.vo.EmployeeVo;

/**
 * 
 * @author Shamsheer
 * @since 25-March-2015
 *
 */
@Repository
public class ContractDaoImpl extends AbstractDao implements ContractDao{
	
	@Autowired
	private UserActivitiesService userActivitiesService;
	
	@Autowired
	private EmployeeDao employeeDao;

	/**
	 * method to save contract
	 */
	public Long saveContract(ContractJsonVo contractVo) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(contractVo.getEmployeeCode());
		if(employee==null)
			throw new ItemNotFoundException("Employee Not Exist.");
		
		ContractType contractType = new ContractType();
		contractType.setId(contractVo.getContractTypeId());
		
		Contract contract = new Contract();
		contract.setDescription(contractVo.getDescription());
		contract.setNotes(contractVo.getNotes());
		contract.setStartDate(DateFormatter.convertStringToDate(contractVo.getStartingDate()));
		contract.setEndDate(DateFormatter.convertStringToDate(contractVo.getEndDate()));
		contract.setTitle(contractVo.getTitle());
		
		contract.setEmployee(employee);
		contract.setContractType(contractType);
		
		return (Long) this.sessionFactory.getCurrentSession().save(contract);
	}

	/**
	 * method to update contract
	 */
	public void updateContract(ContractJsonVo contractVo) {
		Contract contract = findContract(contractVo.getContractId());
		
		contract.setTitle(contractVo.getTitle());
		
		ContractType contractType= new ContractType();
		contractType.setId(contractVo.getContractTypeId());
		contract.setContractType(contractType);
		
		contract.setNotes(contractVo.getNotes());
		contract.setDescription(contractVo.getDescription());
		contract.setStartDate(DateFormatter.convertStringToDate
				(contractVo.getStartingDate()));
		contract.setEndDate(DateFormatter.convertStringToDate
				(contractVo.getEndDate()));
		this.sessionFactory.getCurrentSession().merge(contract);
	}

	/**
	 * method to delete Contract
	 */
	public void deleteContract(Long id) {
		Contract contract = findContract(id);
		if(contract.getContractDocuments()!=null){
			SFTPOperation operation = new SFTPOperation();
			for(ContractDocuments documents : contract.getContractDocuments()){
				operation.removeFileFromSFTP(documents.getUrl());
			}
		}
		this.sessionFactory.getCurrentSession().delete(contract);
	}

	/**
	 * method to find contract by id
	 */
	public ContractVo findContractById(Long id) {
		return createContractVo((Contract) this.sessionFactory.getCurrentSession()
				.createCriteria(Contract.class).add(Restrictions.eq("id", id)).uniqueResult());
	}

	/**
	 * method to list contract by title
	 */
	public List<ContractVo> findContractByTitle(String title) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * method to list all contracts
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public List<ContractVo> listAllContracts() {
		List<ContractVo> contractVos =new ArrayList<ContractVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Contract.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Contract> contracts = criteria.list();
		for(Contract contract : contracts){
			contractVos.add(createContractVo(contract));
		}
		return contractVos;
	}

	/**
	 * method to create contract vo
	 * @param contract
	 * @return
	 */
	private ContractVo createContractVo(Contract contract){
		ContractVo vo = new ContractVo();
		vo.setEmployeeVo(employeeDao.findDetailedEmployee(contract.getEmployee().getId()));
		vo.setContractId(contract.getId());
		vo.setTitle(contract.getTitle());
		vo.setNotes(contract.getNotes());
		vo.setCreatedBy(contract.getCreatedBy().getUserName());
		vo.setCreatedOn(DateFormatter.convertDateToString(contract.getCreatedOn()));
		vo.setDescription(contract.getDescription());
		vo.setEmployeeCode(contract.getEmployee().getEmployeeCode());
		if(contract.getEmployee().getUserProfile()!=null)
			vo.setEmployee(contract.getEmployee().getUserProfile().getFirstName()+" "+contract.getEmployee().getUserProfile().getLastname());
		vo.setContractType(contract.getContractType().getType());
		vo.setContractTypeId(contract.getContractType().getId());
		vo.setStartingDate(DateFormatter.convertDateToString(contract.getStartDate()));
		vo.setEndDate(DateFormatter.convertDateToString(contract.getEndDate()));
		if(contract.getContractDocuments()!=null){
			List<ContractDocumentVo > contractDocumentVos = new ArrayList<ContractDocumentVo>();
			for(ContractDocuments documents:contract.getContractDocuments()){
				contractDocumentVos.add(setDocumnetVo(documents));
			}
			vo.setDocumentVos(contractDocumentVos);
		}
		return vo;
	}
	
	/**
	 * method to create documents vo
	 * @param documents
	 * @return
	 */
	private ContractDocumentVo setDocumnetVo(ContractDocuments documents){
		ContractDocumentVo vo = new ContractDocumentVo();
		vo.setFileName(documents.getFileName());
		vo.setUrl(documents.getUrl());
		return vo;
	}
	
	
	/**
	 * method to list contract by employee
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public List<ContractVo> listContractByEmployee(Long  employeeId) {
		List<ContractVo>  contractVos = new ArrayList<ContractVo>();
		Employee employee = employeeDao.findAndReturnEmployeeById(employeeId);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Contract.class);
		Criterion byEmployee = Restrictions.eq("employee", employee);
		Criterion createdBy = Restrictions.eq("createdBy", employee.getUser());
		LogicalExpression logicalExpression = Restrictions.or(byEmployee, createdBy);
		criteria.add(logicalExpression);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<Contract> contracts = criteria.list();
		for(Contract contract : contracts){
			contractVos.add(createContractVo(contract));
		}
		return contractVos;
	}

	/**
	 * method to find contract by id
	 */
	public Contract findContract(Long id) {
		return (Contract) this.sessionFactory.getCurrentSession().createCriteria(Contract.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}


	/**
	 * method to delete document
	 */
	public void deleteDocument(String url) {
		ContractDocuments documents = (ContractDocuments) this.sessionFactory.getCurrentSession().createCriteria(ContractDocuments.class)
				.add(Restrictions.eq("url", url)).uniqueResult();
		Contract contract = documents.getContract();
		contract.getContractDocuments().remove(documents);
		this.sessionFactory.getCurrentSession().merge(contract);
		this.sessionFactory.getCurrentSession().delete(documents);
	}

	/**
	 * method to update document
	 */
	public void updateDocument(ContractVo contractVo) {
		Contract contract = findContract(contractVo.getContractId());
		contract.setContractDocuments(setDocuments(contractVo.getDocumentVos(), contract));;
		this.sessionFactory.getCurrentSession().merge(contract);
	}

	/**
	 * method to set documents
	 * @param vos
	 * @param contract
	 * @return
	 */
	private Set<ContractDocuments> setDocuments(List<ContractDocumentVo> vos,  Contract contract){
		Set<ContractDocuments> contractDocuments = new HashSet<ContractDocuments>();
		for(ContractDocumentVo vo : vos){
			ContractDocuments documents = new ContractDocuments();
			documents.setFileName(vo.getFileName());
			documents.setUrl(vo.getUrl());
			documents.setContract(contract);
			contractDocuments.add(documents);
		}
		return contractDocuments;
	}
}
