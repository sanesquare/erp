package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.TravelArrangementTypeDao;
import com.hrms.web.entities.TravelArrangementType;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.TravelArrangementTypeVo;

/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
@Repository
public class TravelArrangementTypeDaoImpl extends AbstractDao implements TravelArrangementTypeDao{

	@Autowired
	private UserActivitiesService userActivitiesService;
	
	public void saveArrangementType(TravelArrangementTypeVo vo) {
		// TODO Auto-generated method stub
		
	}

	public void updateArrangementType(TravelArrangementTypeVo vo) {
		// TODO Auto-generated method stub
		
	}

	public void deleteArrangementType(Long id) {
		// TODO Auto-generated method stub
		
	}

	public TravelArrangementType getArrangementType(Long id) {
		return (TravelArrangementType) this.sessionFactory.getCurrentSession().createCriteria(TravelArrangementType.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public TravelArrangementTypeVo getArrangementTypeById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public List<TravelArrangementTypeVo> listAllArrangementType() {
		List<TravelArrangementTypeVo> vos = new ArrayList<TravelArrangementTypeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TravelArrangementType.class);
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		List<TravelArrangementType> arrangementTypes = criteria.list();
		for(TravelArrangementType arrangementType : arrangementTypes){
			vos.add(createArrangementTypeVo(arrangementType));
		}
		return vos;
	}
	
	private TravelArrangementTypeVo createArrangementTypeVo(TravelArrangementType type){
		TravelArrangementTypeVo vo = new TravelArrangementTypeVo();
		vo.setId(type.getId());
		vo.setArrangementType(type.getName());
		return vo;
	}

}
