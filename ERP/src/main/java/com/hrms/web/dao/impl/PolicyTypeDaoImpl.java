package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.PolicyTypeDao;
import com.hrms.web.entities.PolicyType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.PolicyTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Repository
public class PolicyTypeDaoImpl extends AbstractDao implements PolicyTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	public void savePolicyType(PolicyTypeVo policyTypeVo) {
		for (String policyType : StringSplitter.splitWithComma(policyTypeVo.getPolicyType())) {
			if (findPolicyType(policyType.trim()) != null)
				throw new DuplicateItemException("Duplicate Policy Type : " + policyType.trim());
			PolicyType type = findPolicyType(policyTypeVo.getTempPolicyType());
			if (type == null)
				type = new PolicyType();
			type.setType(policyType.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(type);
		}
	}

	public void updatePolicyType(PolicyTypeVo policyTypeVo) {
		PolicyType policyType = findPolicyType(policyTypeVo.getPolicyId());
		if (policyType == null)
			throw new ItemNotFoundException("Policy Type Not exist.");
		policyType.setType(policyTypeVo.getPolicyType());
		this.sessionFactory.getCurrentSession().merge(policyType);
	}

	public void deletePolicyType(Long policyTypeId) {
		PolicyType policyType = findPolicyType(policyTypeId);
		if (policyType == null)
			throw new ItemNotFoundException("Policy Type Not exist.");
		this.sessionFactory.getCurrentSession().delete(policyType);
	}

	public PolicyType findPolicyType(Long policyTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PolicyType.class)
				.add(Restrictions.eq("id", policyTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (PolicyType) criteria.uniqueResult();
	}

	public PolicyType findPolicyType(String policyTypeName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PolicyType.class)
				.add(Restrictions.eq("type", policyTypeName));
		if (criteria.uniqueResult() == null)
			return null;
		return (PolicyType) criteria.uniqueResult();
	}

	/**
	 * method to create policy typevo object
	 * 
	 * @param policyType
	 * @return policytypevo
	 */
	private PolicyTypeVo createPolicyTypeVo(PolicyType policyType) {
		PolicyTypeVo policyTypeVo = new PolicyTypeVo();
		policyTypeVo.setPolicyId(policyType.getId());
		policyTypeVo.setPolicyType(policyType.getType());
		return policyTypeVo;
	}

	public PolicyTypeVo findPolicyTypeById(Long policyTypeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PolicyType.class)
				.add(Restrictions.eq("id", policyTypeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createPolicyTypeVo((PolicyType) criteria.uniqueResult());
	}

	public PolicyTypeVo findPolicyTypeByType(String policyTypeName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PolicyType.class)
				.add(Restrictions.eq("type", policyTypeName));
		if (criteria.uniqueResult() == null)
			return null;
		return createPolicyTypeVo((PolicyType) criteria.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<PolicyTypeVo> findAllPolicyType() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PolicyType.class);
		List<PolicyTypeVo> policyTypeVos = new ArrayList<PolicyTypeVo>();
		List<PolicyType> policyTypes = criteria.list();
		for (PolicyType policyType : policyTypes)
			policyTypeVos.add(createPolicyTypeVo(policyType));
		return policyTypeVos;
	}

}
