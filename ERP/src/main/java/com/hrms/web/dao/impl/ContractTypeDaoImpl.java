package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ContractTypeDao;
import com.hrms.web.entities.ContractType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.ContractTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-march-2015
 *
 */
@Repository
public class ContractTypeDaoImpl extends AbstractDao implements ContractTypeDao {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private UserActivitiesService userActivitiesService;

	/**
	 * method to save new contractType
	 * 
	 * @param contractTypeVo
	 */
	public void saveContractType(ContractTypeVo contractTypeVo) {
		logger.info("Inside ContractType Dao >>> saveContractType....");
		for (String contractType : StringSplitter.splitWithComma(contractTypeVo.getContractType())) {
			if (findContractType(contractType.trim()) != null)
				throw new DuplicateItemException("Duplicate Contract Type : " + contractType.trim());
			ContractType type = findContractType(contractTypeVo.getTempType());
			if (type == null)
				type = new ContractType();
			type.setType(contractType.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(type);
		}

	}

	/**
	 * method to update contracttype information
	 * 
	 * @param contractTypeVo
	 */
	public void updateContractType(ContractTypeVo contractTypeVo) {
		logger.info("Inside ContractType Dao >>> updateContractType....");
		ContractType contractType = findContractType(contractTypeVo.getContractType());
		if (contractType == null)
			throw new ItemNotFoundException("Contract Type Not Exists.");
		contractType.setType(contractTypeVo.getContractType());
		this.sessionFactory.getCurrentSession().merge(contractType);
	}

	/**
	 * method to delete contracttype information
	 * 
	 * @param typeId
	 */
	public void deleteContractType(Long typeId) {
		logger.info("Inside ContractType Dao >>> deleteContractType....");
		ContractType contractType = findContractType(typeId);
		if (contractType == null)
			throw new ItemNotFoundException("Contract Type Not Exists.");
		this.sessionFactory.getCurrentSession().delete(contractType);
	}

	/**
	 * method to find contractType by id
	 * 
	 * @param typeId
	 * @return contractType
	 */
	public ContractType findContractType(Long typeId) {
		logger.info("Inside ContractType Dao >>> findContractType....");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ContractType.class)
				.add(Restrictions.eq("id", typeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (ContractType) criteria.uniqueResult();
	}

	/**
	 * method to create contractTypeVo
	 * 
	 * @param contractType
	 * @return contractTypeVo
	 */
	private ContractTypeVo createContractTypeVo(ContractType contractType) {
		logger.info("Inside ContractType Dao >>> createContractTypeVo....");
		ContractTypeVo contractTypeVo = new ContractTypeVo();
		contractTypeVo.setContractTypeId(contractType.getId());
		contractTypeVo.setContractType(contractType.getType());
		return contractTypeVo;
	}

	/**
	 * method to find contractType by id
	 * 
	 * @param typeId
	 * @return contractTypevo
	 */
	public ContractTypeVo findContractTypeById(Long typeId) {
		logger.info("Inside ContractType Dao >>> findContractTypeById....");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ContractType.class)
				.add(Restrictions.eq("id", typeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createContractTypeVo((ContractType) criteria.uniqueResult());
	}

	/**
	 * method to find contract type by type
	 * 
	 * @param typeName
	 * @return contractType
	 */
	public ContractType findContractType(String typeName) {
		logger.info("Inside ContractType Dao >>> findContractType....");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ContractType.class)
				.add(Restrictions.eq("type", typeName));
		if (criteria.uniqueResult() == null)
			return null;
		return (ContractType) criteria.uniqueResult();
	}

	/**
	 * method to find contractType by type
	 * 
	 * @param typeName
	 * @return contractType
	 */
	public ContractTypeVo findContractTypeByType(String typeName) {
		logger.info("Inside ContractType Dao >>> findContractTypeByType....");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ContractType.class)
				.add(Restrictions.eq("type", typeName));
		if (criteria.uniqueResult() == null)
			return null;
		return createContractTypeVo((ContractType) criteria.uniqueResult());
	}

	/**
	 * method to find all contractType
	 * 
	 * @return List<ContractTypeVo>
	 */
	@SuppressWarnings("unchecked")
	public List<ContractTypeVo> findAllContractType() {
		logger.info("Inside ContractType Dao >>> findAllContractType....");
		List<ContractTypeVo> contractTypeVos = new ArrayList<ContractTypeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ContractType.class);

		List<ContractType> contractTypes = criteria.list();
		for (ContractType contractType : contractTypes)
			contractTypeVos.add(createContractTypeVo(contractType));
		return contractTypeVos;
	}

}
