package com.hrms.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.JobTypeDao;
import com.hrms.web.entities.JobType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.StringSplitter;
import com.hrms.web.vo.JobTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Repository
public class JobTypeDaoImpl extends AbstractDao implements JobTypeDao {

	@Autowired
	private UserActivitiesService userActivitiesService;

	/**
	 * method to save new job type
	 * 
	 * @param jobTypeVo
	 */
	public void saveJobType(JobTypeVo jobTypeVo) {
		for (String typeName : StringSplitter.splitWithComma(jobTypeVo.getJobType())) {
			if (findJobType(typeName.trim()) != null)
				throw new DuplicateItemException("Duplicate Job Type : " + typeName.trim());
			JobType jobType = findJobType(jobTypeVo.getTempJobType());
			if (jobType == null)
				jobType = new JobType();
			jobType.setType(typeName.trim());
			this.sessionFactory.getCurrentSession().saveOrUpdate(jobType);
		}
	}

	/**
	 * method to update job type
	 * 
	 * @param jobTypeVo
	 */
	public void updateJobType(JobTypeVo jobTypeVo) {
		JobType jobType = findJobType(jobTypeVo.getJobTypeId());
		if (jobType == null)
			throw new ItemNotFoundException("Job Type Not Exists");
		jobType.setType(jobTypeVo.getJobType());
		this.sessionFactory.getCurrentSession().merge(jobType);

	}

	/**
	 * method to delete job type
	 * 
	 * @param typeId
	 */
	public void deleteJobType(Long typeId) {
		JobType jobType = findJobType(typeId);
		if (jobType == null)
			throw new ItemNotFoundException("Job Type Not Exists");
		this.sessionFactory.getCurrentSession().delete(jobType);
	}

	/**
	 * method to find job type by id
	 * 
	 * @param typeId
	 * @return jobType
	 */
	public JobType findJobType(Long typeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobType.class)
				.add(Restrictions.eq("id", typeId));
		if (criteria.uniqueResult() == null)
			return null;
		return (JobType) criteria.uniqueResult();
	}

	/**
	 * method to create job typevo
	 * 
	 * @param jobType
	 * @return jobtypevo
	 */
	private JobTypeVo createJobTypeVo(JobType jobType) {
		JobTypeVo jobTypeVo = new JobTypeVo();
		jobTypeVo.setJobType(jobType.getType());
		jobTypeVo.setJobTypeId(jobType.getId());
		return jobTypeVo;
	}

	/**
	 * method to find job type by id
	 * 
	 * @param typeId
	 * @return jobTypeVo
	 */
	public JobTypeVo findJobTypeById(Long typeId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobType.class)
				.add(Restrictions.eq("id", typeId));
		if (criteria.uniqueResult() == null)
			return null;
		return createJobTypeVo((JobType) criteria.uniqueResult());
	}

	/**
	 * method to find job type by name
	 * 
	 * @param typeName
	 * @return jobType
	 */
	public JobType findJobType(String typeName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobType.class)
				.add(Restrictions.eq("type", typeName));
		if (criteria.uniqueResult() == null)
			return null;
		return (JobType) criteria.uniqueResult();
	}

	/**
	 * method to find job type by name
	 * 
	 * @param typeName
	 * @return jobType
	 */
	public JobTypeVo findJobTypeByType(String typeName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobType.class)
				.add(Restrictions.eq("type", typeName));
		if (criteria.uniqueResult() == null)
			return null;
		return createJobTypeVo((JobType) criteria.uniqueResult());
	}

	/**
	 * method to find all job types
	 * 
	 * @return List<JobTypeVo>
	 */
	@SuppressWarnings("unchecked")
	public List<JobTypeVo> findAllJobType() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(JobType.class);
		List<JobTypeVo> jobTypeVos = new ArrayList<JobTypeVo>();
		List<JobType> jobTypes = criteria.list();
		for (JobType jobType : jobTypes)
			jobTypeVos.add(createJobTypeVo(jobType));
		return jobTypeVos;
	}

}
