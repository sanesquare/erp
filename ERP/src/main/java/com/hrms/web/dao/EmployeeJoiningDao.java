package com.hrms.web.dao;

import java.util.List;

import com.hrms.web.entities.EmployeeJoining;
import com.hrms.web.vo.EmployeeDetailsVo;
import com.hrms.web.vo.EmployeeJoiningVo;
import com.hrms.web.vo.JoiningJsonVo;

/**
 * 
 * @author shamsheer
 * @since 23-March-2015
 */
public interface EmployeeJoiningDao {

	/**
	 * method to save employee joining
	 * @param joiningVo
	 * @return 
	 */
	public Long saveEmployeeJoining(JoiningJsonVo joiningVo);
	
	/**
	 * method to delete employeeJoining
	 * @param id
	 */
	public void deleteEmployeeJoining(Long id);
	
	/**
	 * method to update joining
	 * @param employeeJoiningVo
	 */
	public void updateJoining(JoiningJsonVo employeeJoiningVo);
	
	/**
	 * method to find joining by id
	 * @param id
	 * @return
	 */
	public EmployeeJoining findJoiningById(Long id);
	
	/**
	 * method to find joining by employee
	 * @param empId
	 * @return
	 */
	public EmployeeJoining findJoiningByEmployee(Long empId);
	
	/**
	 * method to list all joinings
	 * @return
	 */
	public List<EmployeeJoiningVo> listAllJoinings();
	
	/**
	 * method to get employee joiningby startdate and enddate
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<EmployeeJoiningVo> listJoiningByStartDateEndDate(String startDate , String endDate);
	
	/**
	 * method to get employee joining by branch , start date & end date
	 * @param branchId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<EmployeeJoiningVo> listJoiningByBranchStartDateEndDate(Long branchId , String startDate , String endDate);
	
	/**
	 * method to get employee joining by department , start date & end date
	 * @param branchId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<EmployeeJoiningVo> listJoiningByDepartmentStartDateEndDate(Long departmentId , String startDate , String endDate);
	
	/**
	 * method to get employee joining by branch , department , start date & end date
	 * @param branchId
	 * @param departmentId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<EmployeeJoiningVo> listJoiningByBranchDepartmentStartDateEndDate(Long branchId , Long departmentId , String startDate , String endDate);
	
	/**
	 * method to save documents
	 * @param employeeJoiningVo
	 */
	public void updateDocuments(EmployeeJoiningVo employeeJoiningVo);
	
	/**
	 * metho to find detailed joining
	 * @param id
	 * @return
	 */
	public EmployeeJoiningVo findDetailedJoining(Long id);
	
	/**
	 * method to delete document
	 * @param url
	 */
	public void deleteDocument(String url);
	
	/**
	 * method to get employee details
	 * @param employeeCode
	 * @return
	 */
	public EmployeeDetailsVo getEmployeeDetails(String employeeCode);
}
