package com.hrms.web.dto;

/**
 * 
 * @author Jithin Mohan
 * @since 1-May-2015
 *
 */
public class SelectedEmployee {

	private String name;

	private String code;

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

}
