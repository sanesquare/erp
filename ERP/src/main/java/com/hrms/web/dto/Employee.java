package com.hrms.web.dto;

import java.util.ArrayList;
import java.util.List;

import com.hrms.web.vo.EmployeeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 27-April-2015
 *
 */
public class Employee {

	/**
	 * method to get employee list
	 * 
	 * @param employeeVos
	 * @return
	 */
	public static List<SelectListDto> getEmployees(List<EmployeeVo> employeeVos) {
		List<SelectListDto> selectLists = new ArrayList<SelectListDto>();
		for (EmployeeVo employee : employeeVos) {
			SelectListDto selectList = new SelectListDto();
			selectList.setLabel(employee.getEmployeeName());
			selectList.setValue(employee.getEmployeeCode());
			selectLists.add(selectList);
		}
		return selectLists;
	}
}
