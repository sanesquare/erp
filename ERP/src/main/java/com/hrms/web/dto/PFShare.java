package com.hrms.web.dto;

import java.math.BigDecimal;

/**
 * 
 * @author Jithin Mohan
 * @since 2-May-2015
 *
 */
public class PFShare {

	private BigDecimal employeeShare;

	private BigDecimal employerShare;

	/**
	 * 
	 * @return
	 */
	public BigDecimal getEmployeeShare() {
		return employeeShare;
	}

	/**
	 * 
	 * @param employeeShare
	 */
	public void setEmployeeShare(BigDecimal employeeShare) {
		this.employeeShare = employeeShare;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getEmployerShare() {
		return employerShare;
	}

	/**
	 * 
	 * @param employerShare
	 */
	public void setEmployerShare(BigDecimal employerShare) {
		this.employerShare = employerShare;
	}

}
