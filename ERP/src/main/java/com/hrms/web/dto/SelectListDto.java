package com.hrms.web.dto;

/**
 * 
 * @author Jithin Mohan
 * @since 20-April-2015
 *
 */
public class SelectListDto {

	private String value;

	private String label;

	/**
	 * 
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * 
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * 
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

}
