package com.hrms.web.dto;

import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 28-April-2015
 *
 */
public class StringList {

	private List<String> item;

	/**
	 * 
	 * @return
	 */
	public List<String> getItem() {
		return item;
	}

	/**
	 * 
	 * @param item
	 */
	public void setItem(List<String> item) {
		this.item = item;
	}

}
