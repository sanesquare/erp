package com.hrms.web.dto;

import java.util.ArrayList;
import java.util.List;

import com.hrms.web.vo.SubordinateVo;

/**
 * 
 * @author Jithin Mohan
 * @since 27-April-2015
 *
 */
public class Subordinates {

	/**
	 * method to get subordinate list
	 * 
	 * @param subordinateVos
	 * @return
	 */
	public static List<SelectListDto> getSubordinates(List<SubordinateVo> subordinateVos) {
		List<SelectListDto> selectLists = new ArrayList<SelectListDto>();
		for (SubordinateVo subordinate : subordinateVos) {
			SelectListDto selectList = new SelectListDto();
			selectList.setLabel(subordinate.getSubordinateName());
			selectList.setValue(subordinate.getSubordinateCode());
			selectLists.add(selectList);
		}
		return selectLists;
	}
}
