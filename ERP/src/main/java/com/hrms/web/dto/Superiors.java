package com.hrms.web.dto;

import java.util.ArrayList;
import java.util.List;

import com.hrms.web.vo.SuperiorVo;

/**
 * 
 * @author Jithin Mohan
 * @since 20-April-2015
 *
 */
public class Superiors {

	/**
	 * method to get superior list
	 * 
	 * @param superiorVos
	 * @return
	 */
	public static List<SelectListDto> getSuperiors(List<SuperiorVo> superiorVos) {
		List<SelectListDto> selectLists = new ArrayList<SelectListDto>();
		for (SuperiorVo superior : superiorVos) {
			SelectListDto selectList = new SelectListDto();
			selectList.setLabel(superior.getSuperiorName());
			selectList.setValue(superior.getSuperiorCode());
			selectLists.add(selectList);
		}
		return selectLists;
	}
}
