package com.hrms.web.responses;


/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class JsonResponse {

	private String status;

	private Object result;

	/**
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 */
	public Object getResult() {
		return result;
	}

	/**
	 * 
	 * @param result
	 */
	public void setResult(Object result) {
		this.result = result;
	}

}
