package com.hrms.web.interceptor;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Component
@Aspect
public class LoggingInterceptor {

	@Before("execution(* com.hrms.web.*.*.*(..))")
	public void beforeExecution(JoinPoint jp) {
		System.out.println("Entry Method >>> " + jp.getSignature().getName() + " Class <<< "
				+ jp.getTarget().getClass().getSimpleName());
	}

	@After("execution(* com.hrms.web.*.*.*(..))")
	public void afterExecution(JoinPoint jp) {
		System.out.println("Exit Method >>> " + jp.getSignature().getName() + " Class <<< "
				+ jp.getTarget().getClass().getSimpleName());
	}
}
