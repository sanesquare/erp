package com.hrms.web.interceptor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.controller.service.rest.RestServiceController;
import com.hrms.web.dao.UserDao;
import com.hrms.web.interfaces.AuditEntity;
import com.hrms.web.security.MyUser;
import com.hrms.web.service.UserService;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
public class AuditInterceptor extends EmptyInterceptor {


	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6500370549098342410L;

	/**
	 * This method sets the value on save
	 * 
	 * @param entity
	 *            Object
	 * @param id
	 *            Serializable
	 * @param state
	 *            Object[]
	 * @param propertyNames
	 *            String[]
	 * @param types
	 *            Type[]
	 * @return changed
	 */
	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		boolean changed = false;
		User user = null;
		MyUser myUser=null;
		try {
			myUser = (MyUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			// erpUser=userDao.findUserByUsername(user.getUsername());
		} catch (Exception e) {
			if (RestServiceController.authenticatedRequest) {
				List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				user = new User("biometric", "biometric", authorities);
			}
		}
		if (entity instanceof AuditEntity) {
			setValue(state, propertyNames, "createdBy", myUser.getUser());
			setValue(state, propertyNames, "createdOn", new Date());
			changed = true;
		}
		return changed;
	}

	/**
	 * This method sets the value on update
	 * 
	 * @param entity
	 *            Object
	 * @param id
	 *            Serializable
	 * @param currentState
	 *            Object[]
	 * @param propertyNames
	 *            String[]
	 * @param previousState
	 *            Object[]
	 * @param types
	 *            Type[]
	 * @return changed
	 * @return
	 */
	@Override
	public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
			String[] propertyNames, Type[] types) {
		boolean changed = false;
		User user = null;
		MyUser myUser=null;
		try {
			myUser = (MyUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			// erpUser=userDao.findUserByUsername(user.getUsername());
		} catch (Exception e) {
			if (RestServiceController.authenticatedRequest) {
				List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				user = new User("biometric", "biometric", authorities);
			}
		}
		if (entity instanceof AuditEntity) {
			setValue(currentState, propertyNames, "updatedBy",  myUser.getUser());
			setValue(currentState, propertyNames, "updatedOn", new Date());
			changed = true;
		}
		return changed;
	}

	/**
	 * This method sets the value.
	 * 
	 * @param currentState
	 * @param propertyNames
	 * @param propertyToSet
	 * @param value
	 */
	private void setValue(Object[] currentState, String[] propertyNames, String propertyToSet, Object value) {
		int index = Arrays.asList(propertyNames).indexOf(propertyToSet);
		if (index >= 0) {
			currentState[index] = value;
		}
	}
}
