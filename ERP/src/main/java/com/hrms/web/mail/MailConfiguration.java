package com.hrms.web.mail;

import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

/**
 * 
 * @author Jithin Mohan
 * @since 7-May-2015
 *
 */
public class MailConfiguration {

	public static final String SENDER_ID="service@snogol.net";
	public Session mailSender() {
		final String user_name = "service@snogol.net";
		final String password = "snogol123";
		String host = "mail.snogol.net";
		Properties javaMailProperties = new Properties();
		javaMailProperties.put("mail.smtp.auth", "true");
		javaMailProperties.put("mail.smtp.socketFactory.port", "465");
		javaMailProperties.put("mail.smtp.startssl.enable", "true");
		javaMailProperties.put("mail.smtp.host", host);
		return Session.getInstance(javaMailProperties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user_name, password);
			}
		});
	}
}
