package com.hrms.web.mail;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.activation.*;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Configurable;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.OfferLetterVo;
import com.hrms.web.vo.SalaryCalculatorBasicsVo;
import com.hrms.web.vo.SalaryCalculatorEsiVo;
import com.hrms.web.vo.SalaryCalculatorLWFVo;
import com.hrms.web.vo.SalaryCalculatorPfShareVo;
import com.hrms.web.vo.SalaryCalculatorVo;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * 
 * @author Jithin Mohan
 * @since 7-May-2015
 *
 */
@Configurable
public class ApplicationMailSender implements Runnable {

	private MailConfiguration mailConfiguration = new MailConfiguration();

	private String to = null;
	private String category = null;
	private String sender = null;
	private EmployeeVo withWhome = null;
	private Date date = null;
	private String time = null;
	private String venue = null;
	private String agenda = null;
	private String subject = null;
	private Map<String, String> superiors = null;
	private String candidates = null;
	private List<OfferLetterVo> offerLetterVos = null;

	/**
	 * to send offerm letter
	 * 
	 * @param offerLetterVos
	 */
	public ApplicationMailSender(List<OfferLetterVo> offerLetterVos, final String category) {
		this.offerLetterVos = offerLetterVos;
		this.category = category;
	}

	/**
	 * 
	 * @param to
	 */
	public ApplicationMailSender(final List<String> emails, final String category, final String sender,
			final EmployeeVo withWhome, Date date) {
		this.to = StringUtils.join(emails, ',');
		this.category = category;
		this.sender = sender;
		this.withWhome = withWhome;
		this.date = date;
	}

	public ApplicationMailSender(Map<String, String> superiors, final String category, final String sender, Date date,
			String time, String venue, String agenda, String subject) {
		this.category = category;
		this.sender = sender;
		this.date = date;
		this.time = time;
		this.venue = venue;
		this.agenda = agenda;
		this.subject = subject;
		this.superiors = superiors;
	}

	public ApplicationMailSender(Map<String, String> superiors, final String category, final String sender, Date date,
			String time, String venue) {
		this.category = category;
		this.sender = sender;
		this.date = date;
		this.time = time;
		this.venue = venue;
		this.superiors = superiors;
	}

	public ApplicationMailSender(Map<String, String> superiors, final String category, final String sender, Date date,
			String time, String venue, String candidates) {
		this.category = category;
		this.sender = sender;
		this.date = date;
		this.time = time;
		this.venue = venue;
		this.superiors = superiors;
		this.candidates = candidates;
	}

	/**
	 * 
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	public boolean sendMail() throws MessagingException, IOException {
		try {
			Message message = new MimeMessage(mailConfiguration.mailSender());
			message.setFrom(new InternetAddress(mailConfiguration.SENDER_ID));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			MimeMultipart multipart = new MimeMultipart("related");
			MimeBodyPart messageBodyPart = new MimeBodyPart();

			if (category.equalsIgnoreCase(CommonConstants.TERMINATION_MAIL)) {
				messageBodyPart.setContent(createTerminationMail(), "text/html");
				message.setSubject(CommonConstants.TERMINATION_MAIL);
			} /*
				 * else if
				 * (category.equalsIgnoreCase(CommonConstants.MEETING_MAIL)) {
				 * boolean result = false; for(Entry<String, String> entry :
				 * superiors.entrySet()){ } return result; }
				 */

			// For attachments
			// String filename = "file.txt";
			// DataSource source = new
			// FileDataSource("http://www.snogol.com/resources/images/logo.png");
			// messageBodyPart.setDataHandler(new DataHandler(source));
			// messageBodyPart.setFileName(filename);
			// multipart.addBodyPart(messageBodyPart);

			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * method to send meeting mail
	 * 
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	private boolean sendMeetingMail() throws MessagingException, IOException {

		try {
			for (Entry<String, String> entry : superiors.entrySet()) {
				Message message = new MimeMessage(mailConfiguration.mailSender());
				message.setFrom(new InternetAddress(mailConfiguration.SENDER_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(entry.getKey()));
				MimeMultipart multipart = new MimeMultipart("related");
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(createMeetingMail(entry.getValue()), "text/html");
				message.setSubject(CommonConstants.MEETING_MAIL);
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
				Transport.send(message);
			}
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Method to create interview invitation mail
	 * 
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	private boolean sendInterviewInvitationMail() throws MessagingException, IOException {

		try {
			for (Entry<String, String> entry : superiors.entrySet()) {
				Message message = new MimeMessage(mailConfiguration.mailSender());
				message.setFrom(new InternetAddress(mailConfiguration.SENDER_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(entry.getKey()));
				MimeMultipart multipart = new MimeMultipart("related");
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(createInterviewInvitationMail(entry.getValue()), "text/html");
				message.setSubject(CommonConstants.INTERVIEW_INVITAION_MAIL);
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
				Transport.send(message);
			}
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean sendInterviewNotificationMail() throws MessagingException, IOException {

		try {
			for (Entry<String, String> entry : superiors.entrySet()) {
				Message message = new MimeMessage(mailConfiguration.mailSender());
				message.setFrom(new InternetAddress(mailConfiguration.SENDER_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(entry.getKey()));
				MimeMultipart multipart = new MimeMultipart("related");
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(createInterviewNotificationMail(entry.getValue()), "text/html");
				message.setSubject(CommonConstants.INTERVIEW_NOTIFICATION_MAIL);
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
				Transport.send(message);
			}
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * method to configure termination mail body
	 * 
	 * @return
	 */
	private String createTerminationMail() {
		return "<html> <head> <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'> </head> <body style='background: #FFFFFF; font-family: 'Open Sans', sans-serif;'> <div style='margin: auto; width: 80%; height: auto; margin-top: 20px; margin-bottom: 20px; text-align: left; background: #fff; padding: 20px; border: #999 solid 1px; min-height: 660px;'> <div style='width: 100%; color: #000000; font-size: 12px; font-weight: bold; text-transform: uppercase; margin-bottom: 25px; margin-top: 22px;'> <img src='http://3gmobileworld.in/wp-content/uploads/2015/04/3g-logo-header-2.png' style='width: 200px; margin-left: -29px;'><br> Ref : 3G/AO/SEP-2014 </div> <div style='width: 100%; color: #333; text-align: left; line-height: 24px; font-size: 14px;'> <div style='font-size: 23px; font-weight: bold; color: #24668B; width: 100%; margin-bottom: 10px;'> EMPLOYEE TERMINATION</div> <br> <br> Hi, <br> <br> This is to inform that our employee, <strong>"
				+ withWhome.getSalutation() + " " + withWhome.getName() + " ( " + withWhome.getEmployeeCode()
				+ " )</strong> has been terminated from the position of " + withWhome.getDesignation()
				+ ", <strong>in 3G Mobile World</strong>., dated <strong>"
				+ DateFormatter.convertDateToDetailedString(date)
				+ "</strong>. No employee of our company shall not share any information with him that is regarded confidential to the company. <br> <br> <br><br>Thanks,<br> Best regards,<br> <strong>"
				+ sender + "</strong><br></div></div></body></html>";
	}

	/**
	 * method to configure meeting mail body
	 * 
	 * @return
	 */
	private String createMeetingMail(String name) {
		return "<html> <head> <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'> </head> <body style='background: #FFFFFF; font-family: 'Open Sans', sans-serif;'> <div style='margin: auto; width: 80%; height: auto; margin-top: 20px; margin-bottom: 20px; text-align: left; background: #fff; padding: 20px; border: #999 solid 1px; min-height: 660px;'> <div style='width: 100%; color: #000000; font-size: 12px; font-weight: bold; text-transform: uppercase; margin-bottom: 25px; margin-top: 22px;'> <img src='http://3gmobileworld.in/wp-content/uploads/2015/04/3g-logo-header-2.png' style='width: 200px; margin-left: -29px;'><br> Ref : 3G/AO/SEP-2014 </div> <div style='width: 100%; color: #333; text-align: left; line-height: 24px; font-size: 14px;'> <div style='font-size: 23px; font-weight: bold; color: #24668B; width: 100%; margin-bottom: 10px;'> MEETING </div> <br> <br> Hi, <strong>"
				+ name + " </strong><br> You are requested to attend a meeting on at <strong>"
				+ DateFormatter.convertDateToDetailedString(date) + " " + time + " ( " + venue
				+ " )</strong>  to deliberate on <strong>" + subject + "</strong>.<br> "
				+ "The agenda for the meeting is as under <br><br><strong>" + agenda
				+ "</strong><br><br> In case you are unable to attend the meeting you may intimate the same to   "
				+ "<strong>" + sender + "</strong>" + "<br> <br> <br><br>Thanks,<br> Best regards,<br> <strong>"
				+ sender + "</strong><br></div></div></body></html>";
	}

	private String createInterviewInvitationMail(String name) {
		return "<html><head><link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'> </head> <body style='background: #FFFFFF;font-family: 'Open Sans', sans-serif;'> <div style='margin:auto;width:80%;height:auto;margin-top:20px;margin-bottom:20px;text-align:left;background:#fff;padding:20px; border: #999 solid 1px;min-height: 660px;'> <div style='width: 100%;color: #000000;font-size: 12px;font-weight: bold;text-transform: uppercase;margin-bottom: 25px;margin-top: 22px;'><img src='http://3gmobileworld.in/wp-content/uploads/2015/04/3g-logo-header-2.png' style='width: 200px;margin-left: -29px;'></div> <div style='width:100%;color:#333;text-align:left;line-height: 24px;font-size: 14px;'><div style='font-size: 23px;font-weight:bold;color: #24668B;width: 100%;margin-bottom: 10px;'> JOB INTERVIEW</div><br><br>Hi<strong>  "
				+ name
				+ "</strong>. <br><br>Thanks for your application to <strong>3G Mobile World</strong>. We were impressed by your background and would like to invite you to interview at <strong>"
				+ venue + "</strong> on <strong>" + DateFormatter.convertDateToString(date) + "</strong> at <strong>"
				+ time
				+ "</strong> to tell you a little more about the position and get to know you better.<br><br><br> Looking forward to meeting you,<br><strong>"
				+ sender + "</strong> <br></div></body></html>";
	}

	private String createInterviewNotificationMail(String name) {
		return "<html><head><link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'> </head> <body style='background: #FFFFFF;font-family: 'Open Sans', sans-serif;'> <div style='margin:auto;width:80%;height:auto;margin-top:20px;margin-bottom:20px;text-align:left;background:#fff;padding:20px; border: #999 solid 1px;min-height: 660px;'> <div style='width: 100%;color: #000000;font-size: 12px;font-weight: bold;text-transform: uppercase;margin-bottom: 25px;margin-top: 22px;'><img src='http://3gmobileworld.in/wp-content/uploads/2015/04/3g-logo-header-2.png' style='width: 200px;margin-left: -29px;'> </div> <div style='width:100%;color:#333;text-align:left;line-height: 24px;font-size: 14px;'><div style='font-size: 23px;font-weight:bold;color: #24668B;width: 100%;margin-bottom: 10px;'> JOB INTERVIEW</div><br><br>Hi<strong>  "
				+ name
				+ "</strong>. <br><br>This is to inform you that you are included in the interviewers panel for interview at <strong>"
				+ venue + "</strong> on <strong>" + DateFormatter.convertDateToString(date) + "</strong> at <strong>"
				+ time + "</strong> .Interviewees names are given below .<br><br><br> Thanking you,<br><strong>"
				+ sender + "</strong> <br><br>" + candidates + "</div></body></html>";
	}

	/**
	 * thread run
	 */
	public void run() {
		try {
			if (category.equalsIgnoreCase(CommonConstants.MEETING_MAIL)) {
				sendMeetingMail();
			} else if (category.equalsIgnoreCase(CommonConstants.INTERVIEW_INVITAION_MAIL)) {
				sendInterviewInvitationMail();
			} else if (category.equalsIgnoreCase(CommonConstants.INTERVIEW_NOTIFICATION_MAIL)) {
				sendInterviewNotificationMail();
			} else if (category.equalsIgnoreCase(CommonConstants.OFFER_LETTER)) {
				sendOfferLetter();
			} else
				sendMail();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean sendOfferLetter() throws MessagingException, IOException {
		String filename = null;
		File file = null;
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {
			for (OfferLetterVo letterVo : offerLetterVos) {
				if(letterVo.getEmail()!=null){
				Document document = new Document();
				filename = DateFormatter.getUniqueTimestamp() + "_offer_lette.pdf";
				OutputStream outputStream = new FileOutputStream(filename);
				PdfWriter writer = PdfWriter.getInstance(document, outputStream);
				document.open();
				fileWriter = new FileWriter("offer.html");
				bufferedWriter = new BufferedWriter(fileWriter);
				bufferedWriter.write(createOfferLetter(letterVo));
				bufferedWriter.close();
				/*HTMLWorker htmlWorker = new HTMLWorker(document);
				htmlWorker.parse(new StringReader(createOfferLetter(letterVo)));*/
				document.close();
				outputStream.close();
				writer.close();
				file = new File(filename);

				Message message = new MimeMessage(mailConfiguration.mailSender());
				message.setFrom(new InternetAddress(mailConfiguration.SENDER_ID));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(letterVo.getEmail()));
				MimeMultipart multipart = new MimeMultipart("related");
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(createOfferLetterBody(letterVo), "text/html");
				message.setSubject(CommonConstants.OFFER_LETTER);
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(file);
				attachPart.setFileName("OfferLetter.pdf");
				multipart.addBodyPart(messageBodyPart);
				multipart.addBodyPart(attachPart);
				message.setContent(multipart);
				Transport.send(message);
				}
			}
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		} catch (DocumentException e) {
			e.printStackTrace();
		} finally {
			if (file != null)
				file.delete();
		}
		return true;
	}

	/**
	 * method to create offer letter body
	 * 
	 * @return
	 */
	private String createOfferLetterBody(OfferLetterVo letterVo) {
		String body = "<html><head><link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'></head><body style='background: #FFFFFF;font-family: 'Open Sans', sans-serif;'><div style='margin:auto;width:80%;height:auto;margin-top:20px;margin-bottom:20px;text-align:left;background:#fff;padding:20px;border: #999 solid 1px;min-height: 660px;'>"
				+ "<div style='width: 100%;color: #000000;font-size: 12px;font-weight: bold;text-transform: uppercase;margin-bottom: 25px;margin-top: 22px;'><img src='http://3gmobileworld.in/wp-content/uploads/2015/04/3g-logo-header-2.png' style='width: 200px;margin-left: -29px;'><br>Ref : 3G/AO/SEP-2014</div>"
				+ "<div style='width:100%;color:#333;text-align:left;line-height: 24px;font-size: 14px;'><div style='font-size: 23px;font-weight:bold;color: #24668B;width: 100%;margin-bottom: 10px;'> OFFER LETTER</div><br><br>"
				+ "Dear  <strong>"+letterVo.getName()+"</strong>.  <br><br><br>With reference to your application and subsequent discussions, we are pleased to offer you the posting of  <strong>"+letterVo.getDesignation()+"</strong> ,  under  Grade <strong>"+letterVo.getGrade()+"</strong> at  our  <strong>"+letterVo.getBranch()+"</strong>   <br> <br>Please sign and return the duplicate copy of the attached letter of terms and conditions to the undersigned as a token of your acceptance. <br>Thanking You<br>Yours faithfully <br><br><br>"
				+ "<div style=' float: left;width: 50%;'>For 3G Mobile World.<br><br>"
				+ "<strong>"+letterVo.getSender()+"</strong><br/><strong>"+letterVo.getSenderDesignation()+"</strong> </div></div></body></html>";
		return body;
	}

	/**
	 * create offer letter attachment
	 * 
	 * @return
	 */
	private String createOfferLetter(OfferLetterVo letterVo) {
		SalaryCalculatorVo salaryCalculatorVo = letterVo.getSalaryCalculatorVo();
		SalaryCalculatorBasicsVo basicsVo = salaryCalculatorVo.getBasicsVo();
		SalaryCalculatorPfShareVo pfShareVo = salaryCalculatorVo.getPfShare();
		SalaryCalculatorEsiVo esiVo = salaryCalculatorVo.getEsiVo();
		SalaryCalculatorLWFVo lwfVo = salaryCalculatorVo.getLwfVo();
		String offerLetterBody = "<html><head><link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'></link> </head>"
				+ "<body style='background: #FFFFFF; font-family: ' open='' sans',='' sans-serif;'=''>"
				+ "<div style='margin: auto; width: 80%; height: auto; margin-top: 20px; margin-bottom: 20px; text-align: left; background: #fff; padding: 20px; border: #999 solid 1px; min-height: 660px;'>"
				+ "<div style='width: 150px; color: #000000; font-size: 12px; font-weight: bold; text-transform: uppercase; margin-bottom: 25px; margin-top: 22px;' align='center' > <img src='http://www.adscalicut.com/temp/55804769_188176_155245027870782_3140791_n.jpg'/></div>"
				+ "<div style='width: 100%; color: #333; text-align: center; line-height: 24px; font-size: 14px;margin-top: 95px;'>"
				+ "<div style='font-size: 23px; font-weight: bold; color: #24668B; width: 100%; margin-bottom: 10px;text-align: center;'>"
				+ "<p><br/><br/>Letter of Offer</p>" + "</div></div>"
				+ "<p style='width: 100%;margin-top: 15px; float: left;'>Dear  "+letterVo.getName()+",<br/><br/><br/></p>"
				+ "<p style='width: 100%; margin-top:15px;float: left;  line-height: 22px;'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  With reference to your application and subsequent discussions, we are pleased to offer you the posting of<strong>"+letterVo.getDesignation()+"</strong> ,  under  Grade  "+letterVo.getGrade()+"  at  our  <strong>"+letterVo.getBranch()+"</strong> on  the  following  terms  and Conditions.<br/><br/></p>"
				+ "<ul style='float: left; width: 100%; line-height: 26px; margin-bottom: 15px;'>"
				+ "<li style='list-style: disc; width: 80%; clear: both; margin-bottom: 15px;'><u>Date of Joining:</u> Your date of joining with<strong> 3G Mobile World </strong> will be on  <strong>"+letterVo.getDateOfJoining()+"</strong><br/><br/></li>"
				+ "<li style='list-style: disc; width: 80%; clear: both;margin-bottom: 15px;'>"
				+ "<u>Duties :</u> Responsibilities: You will be responsible to undertake the duties and responsibilities assigned / communicated to you by the management, from time to time.</li><br/><br/>"
				+ "<li style='list-style: disc; width: 80%; clear: both;  margin-bottom: 15px;'> <br/><br/>"
				+ "<u>Salary : </u>"
				+ "You will be eligible to receive a salary of<strong> Rs. "+salaryCalculatorVo.getNetMonthly()+"(Take Home) </strong>/- per month .All other benefits as applicable to you are detailed in Annexure-1<br/><br/></li>"
				+ "<li style='list-style: disc;  width: 80%; clear: both; margin-bottom: 15px;'>"
				+ "<u>Probation: </u>Your appointment with the organization will be confirmed after the successful completion of the probation period for six months.<br/><br/></li>"
				+ "<li style='list-style: disc; width: 80%; clear: both;margin-bottom: 15px;'><u>Transfer:</u> During the course of the service, you shall be liable to be posted / transferred anywhere to serve any of the Projects of <strong>3G Mobil World </strong>, at the sole discretion of the Management.<br/><br/></li>"
				+ "<li style='list-style: disc; width: 80%;clear: both;margin-bottom: 15px;'><u>Leave: </u>You will be eligible to the benefits of the Company’s Leave Rules on your confirmation in the service with the organization. Absence for a continuous period of 3 days without prior approval of your superior officer, would result in your losing your lien on the service and the same shall automatically come to an end without any notice or intimation<br/><br/></li>"
				+ "<li style=' list-style: disc;width: 80%; clear:both;margin-bottom: 15px;'><u>Termination / Resignation:</u> Your appointment with 3G Mobil World. shall be terminated on one month’s notice in written, on either side.<br/><br/></li>"
				+ "</ul>"
				+ "<p style=' width: 100%;    margin-top: 15px;    float: left;  line-height: 22px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You will be required to maintain utmost secrecy in respect of the documents, Proposals &amp; Estimates, Technology, Company’s polices, Company’s Trade Mark and Human assets profile. You will be required to comply with all such rules and regulations as the Company may frame from time to time.<br/><br/></p>"
				+ "<p style=' width: 100%;    margin-top: 15px;    float: left;  line-height: 22px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If at any time, in our opinion, which is final in this matter you are found non- performer or guilty of fraud, dishonest, theft, disobedience, disorderly behaviour, negligence, indiscipline, absence from duty without permission or any other conduct considered by us deterrent to our interest or of violation of one or more terms of this letter, your services may be terminated without notice and on account of reason of any of the acts or omission the company shall be entitled to recover the damages from you.<br/><br/></p>"
				+ "<p style=' width: 100%;    margin-top: 15px;    float: left;  line-height: 22px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This letter of offer is being issued to you on the basis of the information and particulars furnished by you at the time of the interview and subsequent discussions. If it transpires that that you have made a false statement (or have not disclosed a material fact) resulting in your being offered this appointment, the management may take  such action as it deems fit in its sole discretion, including termination of your employment.<br/><br/></p>"
				+ "<p style=' width: 100%;    margin-top: 15px;    float: left;  line-height: 22px;'>Please sign and return the duplicate copy of this letter to the undersigned as a token of your acceptance.<br/><br/></p>"
				+ "<p style=' width: 100%;    margin-top: 15px;    float: left;  line-height: 22px;'>For 3G Mobile World.<br/></p>"
				+ "<p style='width: 100%;    margin-top: 15px;    float: left;  line-height: 22px;'>"+letterVo.getSender()+" <br/>"+letterVo.getSenderDesignation()+"<br/><br/></p>"
				+ "<u>Declaration </u>: I have read carefully the terms and conditions written above and declare that I do accept the"
				+ "offer for the above job .<br/><br/>"
				+ "<p style='width:100%;text-align:right; clear:both;'>Name       :&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;<br/> Signature  :&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</p><br/><br/>"
				+ "<table border='1' cellspacing='2' style='font-size:12px;'>" + "<tbody><tr>"
				+ "<td width='70%'><strong class='amt_high'> INPUT GROSS SALARY</strong></td>"
				+ "<td width='30%'><strong>"+salaryCalculatorVo.getGrossSalary()+"</strong></td></tr>" + "</tbody></table>"
				+ "<table border='1' cellspacing='2' style='font-size:12px;'>" + "<thead>" + "<tr>"
				+ "<th width='20%'>&nbsp;</th>" + "<th width='3%'>S.No</th>" + "<th width='48%'>Description</th>"
				+ "<th width='20%' colspan='2'>In Rupees</th>" + "<th width='10%'>Remarks</th>" + "</tr>" + "</thead>"
				+ "<tbody>" + "<tr>" + "<td><strong class='amt_high'>SPLIT </strong></td>" + "<td>&nbsp;</td>"
				+ "<td>&nbsp;</td>" + "<td><strong class='amt_high'>salary</strong></td>"
				+ "<td><strong class='amt_high'>Annual</strong></td>" + "<td>&nbsp;</td>" + "</tr>" + "<tr>"
				+ "<td>BA</td>" + "<td>1</td>" + "<td>Basic Salary</td>"
				+ "<td id='calculator_basic_salary_monthly'>"+basicsVo.getBasicSalaryMonthly()+"</td>" + "<td id='calculator_basic_salary_annually'>"+basicsVo.getBasicSalaryAnnual()+"</td>"
				+ "<td id='calculator_basic_salary_remarks'></td>" + "</tr>" + "<tr>" + "<td>DA</td>" + "<td>2</td>"
				+ "<td>Dearness Allownace</td>" + "<td id='calculator_da_monthly'>"+basicsVo.getDearnessAllowanceMonthly()+"</td>"
				+ "<td id='calculator_da_annually'>"+basicsVo.getDearnessAllowanceAnnual()+"</td>" + "<td id='calculator_da_remarks'></td>" + "</tr>" + "<tr>"
				+ "<td>HRA</td>" + "<td>3</td>" + "<td>HRA</td>" + "<td id='calculator_hra_monthly'>"+basicsVo.getHraMonthly()+"</td>"
				+ "<td id='calculator_hra_annual'>"+basicsVo.getHraAnnual()+"</td>" + "<td id='calculator_hra_remarks'></td>" + "</tr>" + "<tr>"
				+ "<td>CCA</td>" + "<td>4</td>" + "<td>CCA</td>" + "<td id='calculator_cca_monthly'>"+basicsVo.getCcaMonthly()+"</td>"
				+ "<td id='calculator_cca_annual'>"+basicsVo.getCcaAnnual()+"</td>" + "<td id='calculator_cca_remarks'></td>" + "</tr>" + "<tr>"
				+ "<td>conveyance</td>" + "<td>5</td>" + "<td>Conveyance</td>"
				+ "<td id='calculator_conveyance_monthly'>"+basicsVo.getConveyanceMonthly()+"</td>" + "<td id='calculator_conveyance_annual'>"+basicsVo.getConveyanceAnnual()+"</td>"
				+ "<td id='calculator_conveyance_remarks'></td>" + "</tr>" + "<tr>" + "<td>Other Allowance</td>"
				+ "<td>6</td>" + "<td>Other Allowance(Balance)</td>" + "<td id='calculator_other_monthly'>"+basicsVo.getOtherAllowancesMonthly()+"</td>"
				+ "<td id='calculator_other_annual'>"+basicsVo.getOtherAllowancesAnnual()+"</td>" + "<td id='calculator_other_remarks'></td>" + "</tr>"
				+ "<tr>" + "<td></td>" + "<td></td>" + "<td><strong class='amt_high'> GROSS AMOUNT</strong></td>"
				+ "<td><strong class='amt_high' id='calculator_gross_monthly'>"+basicsVo.getGrossMonthly()+"</strong></td>"
				+ "<td><strong class='amt_high' id='calculator_gross_annual'>"+basicsVo.getGrossAnnual()+"</strong></td>"
				+ "<td id='calculator_gross_remarks'></td>" + "</tr>" + "<tr>"
				+ "<td rowspan='3'>DEDUCTIONS emloyee</td>" + "<td></td>" + "<td>P.F. (Employee Contribution)</td>"
				+ "<td id='calculator_pf_monthly'>"+pfShareVo.getPfEmployeeMonthly()+"</td>" + "<td id='calculator_pf_annual'>"+pfShareVo.getPfEmployeeAnnual()+"</td>"
				+ "<td id='calculator_pf_remarks'></td>" + "</tr>" + "<tr>" + "<td></td>"
				+ "<td>E.S.I (Employee Contribution)</td>" + "<td id='calculator_esi_monthly'>"+esiVo.getEsiEmployeeMonthly()+"</td>"
				+ "<td id='calculator_esi_annual'>"+esiVo.getEsiEmployeeAnnual()+"</td>" + "<td id='calculator_esi_remarks'></td>" + "</tr>" + "<tr>"
				+ "<td></td>" + "<td>LWF</td>" + "<td id='calculator_lwf_monthly'>"+lwfVo.getLwfEmployeeMonthly()+"</td>"
				+ "<td id='calculator_lwf_annual'>"+lwfVo.getLwfEmployeeAnnual()+"</td>" + "<td id='calculator_lwf_remarks'></td>" + "</tr>" + "<tr>"
				+ "<td></td>" + "<td></td>" + "<td><strong class='amt_high'> NET TAKE HOME</strong></td>"
				+ "<td><strong class='amt_high' id='calculator_net_monthly'>"+salaryCalculatorVo.getNetMonthly()+"</strong></td>"
				+ "<td><strong class='amt_high' id='calculator_net_annual'>"+salaryCalculatorVo.getNetAnnual()+"</strong></td>"
				+ "<td id='calculator_net_remarks'></td>" + "</tr>" + "<tr>"
				+ "<td rowspan='3'>EMPLOYER CONTRIBUTION</td>" + "<td></td>" + "<td>P.F. (Employer Contribution)</td>"
				+ "<td id='calculator_pf_monthly_employer'>"+pfShareVo.getPfEmployerMonthly()+"</td>" + "<td id='calculator_pf_annual_employer'>"+pfShareVo.getPfEmployerAnnual()+"</td>"
				+ "<td id='calculator_pf_remarks_employer'></td>" + "</tr>" + "<tr>" + "<td></td>"
				+ "<td>E.S.I (Employer Contribution)</td>" + "<td id='calculator_esi_monthly_employer'>"+esiVo.getEsiEmployerMonthly()+"</td>"
				+ "<td id='calculator_esi_annual_employer'>"+esiVo.getEsiEmployerAnnual()+"</td>" + "<td id='calculator_esi_remarks_employer'></td>"
				+ "</tr>" + "<tr>" + "<td></td>" + "<td>LWF</td>" + "<td id='calculator_lwf_monthly_employer'>"+lwfVo.getLwfEmployerMonthly()+"</td>"
				+ "<td id='calculator_lwf_annual_employer'>"+lwfVo.getLwfEmployerAnnual()+"</td>" + "<td id='calculator_lwf_remarks_employer'></td>"
				+ "</tr>" + "<tr>" + "<td></td>" + "<td></td>" + "<td><strong class='amt_high'>CTC</strong></td>"
				+ "<td><strong class='amt_high' id='calculator_ctc_monthly'>"+salaryCalculatorVo.getCtcMonthly()+"</strong></td>"
				+ "<td><strong class='amt_high' id='calculator_ctc_annual'>"+salaryCalculatorVo.getCtcAnnual()+"</strong></td>"
				+ "<td id='calculator_ctc_remarks'></td>" + "</tr>" + "</tbody></table>";
		return offerLetterBody;
	}
}
