package com.hrms.web.notifications;

public class Message {

	private NotificationContent content;

	/**
	 * 
	 * @param content
	 * @param recipients
	 */
	public Message(NotificationContent content) {
		this.content = content;
	}

	/**
	 * 
	 * @return
	 */
	public NotificationContent getContent() {
		return content;
	}

}
