package com.hrms.web.notifications;

import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 05-May-2015
 *
 */
public class NotificationContent {

	private String content;
	private List<String> recipients;
	private String url;
	private String picture;

	/**
	 * 
	 * @return
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 
	 * @return
	 */
	public String getPicture() {
		return picture;
	}

	/**
	 * 
	 * @param picture
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}

	/**
	 * 
	 * @return
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getRecipients() {
		return recipients;
	}

	/**
	 * 
	 * @param recipients
	 */
	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

}
