package com.hrms.web.notifications;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class PushNotification {
	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	@Scheduled(fixedDelay = 100)
	public void sendNotification(NotificationContent message) {
		System.out.println("<-- inside afterTradeExecuted-->> ");
		this.messagingTemplate.convertAndSend("/HRMS/topic/message", new Message(message));
	}
}
