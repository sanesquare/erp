package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 13-July-2015
 *
 */
public class AppointmentLetterVo {

	private String date;
	private Long designationId;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Long getDesignationId() {
		return designationId;
	}
	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}
}
