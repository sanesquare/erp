package com.hrms.web.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 7-May-2015
 *
 */
public class EmpSummaryPromotionDetailVo {

	private int slno;
	
	@DateTimeFormat
	private Date promoDate;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the promoDate
	 */
	public Date getPromoDate() {
		return promoDate;
	}

	/**
	 * @param promoDate the promoDate to set
	 */
	public void setPromoDate(Date promoDate) {
		this.promoDate = promoDate;
	}
	
	
}
