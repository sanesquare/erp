package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Jithin Mohan
 * @since 25-April-2015
 *
 */
public class AdditionalPayRollResultVo {

	private String extraPaySlipItem;

	private BigDecimal amount;

	/**
	 * 
	 * @return
	 */
	public String getExtraPaySlipItem() {
		return extraPaySlipItem;
	}

	/**
	 * 
	 * @param extraPaySlipItem
	 */
	public void setExtraPaySlipItem(String extraPaySlipItem) {
		this.extraPaySlipItem = extraPaySlipItem;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
