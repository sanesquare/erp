package com.hrms.web.vo;
/**
 * 
 * @author shamsheer
 * @since 19-March-2015
 */
public class EmployeeAdditionalInformationVo {
	
	private Long designationId;
	
	private Long jobFieldId;
	
	private Long accountTypeId;
	
	private Long employeeId;
	
	private String degree;
	
	private Long degreeId;
	
	private String subject;
	
	private String institute;
	
	private String gpa;
	
	private String graduationYear;
	
	private String language;
	
	private Long languageId;
	
	private String speakingLevel;
	
	private String readingLevel;
	
	private String writingLevel;
	
	private String organisationName;
	
	private String jobDesignation;
	
	private String jobField;
	
	private String jobDescription;
	
	private String jobStartDate;
	
	private String jobEndDate;
	
	private String startingSalary;
	
	private String endingSalary;
	
	private String bankName;
	
	private String branchName;
	
	private String branchCode;
	
	private String accountTitle;
	
	private String accountNumber;
	
	private String accountType;
	
	private String benefitTitle;
	
	private String benefitDetails;
	
	private String assetName;
	
	private Long assetId;
	
	private String assetCode;
	
	private String assetCheckOut;
	
	private String assetCheckin;
	
	private String assetDescription;
	
	private String uniformCheckOutDate;
	
	private String uniformAdvanceAmount;
	
	private String uniformDuration;
	
	private String uniformReimbursementDate;
	
	private String skill;
	
	private Long skillId;
	
	private String skillLevel;
	
	private String referenceName;
	
	private String referenceOrganisationName;
	
	private String referencePhoneNUmber;
	
	private String referenceEmail;
	
	private String dependantName;
	
	private String dependantDateOfBirth;
	
	private String dependantRelation;
	
	private Boolean dependantNominee;
	
	private String address;
	
	private String nextOfKinName;
	
	private String nextOfKinDOB;
	
	private String nextOFKinRelation;
	
	private String nextOfKinAddress;
	
	private String status;
	
	private Long statusId;

	/**
	 * @return the degree
	 */
	public String getDegree() {
		return degree;
	}

	/**
	 * @param degree the degree to set
	 */
	public void setDegree(String degree) {
		this.degree = degree;
	}

	/**
	 * @return the degreeId
	 */
	public Long getDegreeId() {
		return degreeId;
	}

	/**
	 * @param degreeId the degreeId to set
	 */
	public void setDegreeId(Long degreeId) {
		this.degreeId = degreeId;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the institute
	 */
	public String getInstitute() {
		return institute;
	}

	/**
	 * @param institute the institute to set
	 */
	public void setInstitute(String institute) {
		this.institute = institute;
	}

	/**
	 * @return the gpa
	 */
	public String getGpa() {
		return gpa;
	}

	/**
	 * @param gpa the gpa to set
	 */
	public void setGpa(String gpa) {
		this.gpa = gpa;
	}

	/**
	 * @return the graduationYear
	 */
	public String getGraduationYear() {
		return graduationYear;
	}

	/**
	 * @param graduationYear the graduationYear to set
	 */
	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the languageId
	 */
	public Long getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the speakingLevel
	 */
	public String getSpeakingLevel() {
		return speakingLevel;
	}

	/**
	 * @param speakingLevel the speakingLevel to set
	 */
	public void setSpeakingLevel(String speakingLevel) {
		this.speakingLevel = speakingLevel;
	}

	/**
	 * @return the readingLevel
	 */
	public String getReadingLevel() {
		return readingLevel;
	}

	/**
	 * @param readingLevel the readingLevel to set
	 */
	public void setReadingLevel(String readingLevel) {
		this.readingLevel = readingLevel;
	}

	/**
	 * @return the writingLevel
	 */
	public String getWritingLevel() {
		return writingLevel;
	}

	/**
	 * @param writingLevel the writingLevel to set
	 */
	public void setWritingLevel(String writingLevel) {
		this.writingLevel = writingLevel;
	}

	/**
	 * @return the organisationName
	 */
	public String getOrganisationName() {
		return organisationName;
	}

	/**
	 * @param organisationName the organisationName to set
	 */
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	/**
	 * @return the jobDesignation
	 */
	public String getJobDesignation() {
		return jobDesignation;
	}

	/**
	 * @param jobDesignation the jobDesignation to set
	 */
	public void setJobDesignation(String jobDesignation) {
		this.jobDesignation = jobDesignation;
	}

	/**
	 * @return the jobField
	 */
	public String getJobField() {
		return jobField;
	}

	/**
	 * @param jobField the jobField to set
	 */
	public void setJobField(String jobField) {
		this.jobField = jobField;
	}

	/**
	 * @return the jobDescription
	 */
	public String getJobDescription() {
		return jobDescription;
	}

	/**
	 * @param jobDescription the jobDescription to set
	 */
	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	/**
	 * @return the jobStartDate
	 */
	public String getJobStartDate() {
		return jobStartDate;
	}

	/**
	 * @param jobStartDate the jobStartDate to set
	 */
	public void setJobStartDate(String jobStartDate) {
		this.jobStartDate = jobStartDate;
	}

	/**
	 * @return the jobEndDate
	 */
	public String getJobEndDate() {
		return jobEndDate;
	}

	/**
	 * @param jobEndDate the jobEndDate to set
	 */
	public void setJobEndDate(String jobEndDate) {
		this.jobEndDate = jobEndDate;
	}

	/**
	 * @return the startingSalary
	 */
	public String getStartingSalary() {
		return startingSalary;
	}

	/**
	 * @param startingSalary the startingSalary to set
	 */
	public void setStartingSalary(String startingSalary) {
		this.startingSalary = startingSalary;
	}

	/**
	 * @return the endingSalary
	 */
	public String getEndingSalary() {
		return endingSalary;
	}

	/**
	 * @param endingSalary the endingSalary to set
	 */
	public void setEndingSalary(String endingSalary) {
		this.endingSalary = endingSalary;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * @param branchName the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * @return the branchCode
	 */
	public String getBranchCode() {
		return branchCode;
	}

	/**
	 * @param branchCode the branchCode to set
	 */
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	/**
	 * @return the accountTitle
	 */
	public String getAccountTitle() {
		return accountTitle;
	}

	/**
	 * @param accountTitle the accountTitle to set
	 */
	public void setAccountTitle(String accountTitle) {
		this.accountTitle = accountTitle;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the accountType
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/**
	 * @return the benefitTitle
	 */
	public String getBenefitTitle() {
		return benefitTitle;
	}

	/**
	 * @param benefitTitle the benefitTitle to set
	 */
	public void setBenefitTitle(String benefitTitle) {
		this.benefitTitle = benefitTitle;
	}

	/**
	 * @return the benefitDetails
	 */
	public String getBenefitDetails() {
		return benefitDetails;
	}

	/**
	 * @param benefitDetails the benefitDetails to set
	 */
	public void setBenefitDetails(String benefitDetails) {
		this.benefitDetails = benefitDetails;
	}

	/**
	 * @return the assetName
	 */
	public String getAssetName() {
		return assetName;
	}

	/**
	 * @param assetName the assetName to set
	 */
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	/**
	 * @return the assetId
	 */
	public Long getAssetId() {
		return assetId;
	}

	/**
	 * @param assetId the assetId to set
	 */
	public void setAssetId(Long assetId) {
		this.assetId = assetId;
	}

	/**
	 * @return the assetCode
	 */
	public String getAssetCode() {
		return assetCode;
	}

	/**
	 * @param assetCode the assetCode to set
	 */
	public void setAssetCode(String assetCode) {
		this.assetCode = assetCode;
	}

	/**
	 * @return the assetCheckOut
	 */
	public String getAssetCheckOut() {
		return assetCheckOut;
	}

	/**
	 * @param assetCheckOut the assetCheckOut to set
	 */
	public void setAssetCheckOut(String assetCheckOut) {
		this.assetCheckOut = assetCheckOut;
	}

	/**
	 * @return the assetCheckin
	 */
	public String getAssetCheckin() {
		return assetCheckin;
	}

	/**
	 * @param assetCheckin the assetCheckin to set
	 */
	public void setAssetCheckin(String assetCheckin) {
		this.assetCheckin = assetCheckin;
	}

	/**
	 * @return the assetDescription
	 */
	public String getAssetDescription() {
		return assetDescription;
	}

	/**
	 * @param assetDescription the assetDescription to set
	 */
	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}

	/**
	 * @return the uniformCheckOutDate
	 */
	public String getUniformCheckOutDate() {
		return uniformCheckOutDate;
	}

	/**
	 * @param uniformCheckOutDate the uniformCheckOutDate to set
	 */
	public void setUniformCheckOutDate(String uniformCheckOutDate) {
		this.uniformCheckOutDate = uniformCheckOutDate;
	}

	/**
	 * @return the uniformAdvanceAmount
	 */
	public String getUniformAdvanceAmount() {
		return uniformAdvanceAmount;
	}

	/**
	 * @param uniformAdvanceAmount the uniformAdvanceAmount to set
	 */
	public void setUniformAdvanceAmount(String uniformAdvanceAmount) {
		this.uniformAdvanceAmount = uniformAdvanceAmount;
	}

	/**
	 * @return the uniformDuration
	 */
	public String getUniformDuration() {
		return uniformDuration;
	}

	/**
	 * @param uniformDuration the uniformDuration to set
	 */
	public void setUniformDuration(String uniformDuration) {
		this.uniformDuration = uniformDuration;
	}

	/**
	 * @return the uniformReimbursementDate
	 */
	public String getUniformReimbursementDate() {
		return uniformReimbursementDate;
	}

	/**
	 * @param uniformReimbursementDate the uniformReimbursementDate to set
	 */
	public void setUniformReimbursementDate(String uniformReimbursementDate) {
		this.uniformReimbursementDate = uniformReimbursementDate;
	}

	/**
	 * @return the skill
	 */
	public String getSkill() {
		return skill;
	}

	/**
	 * @param skill the skill to set
	 */
	public void setSkill(String skill) {
		this.skill = skill;
	}

	/**
	 * @return the skillId
	 */
	public Long getSkillId() {
		return skillId;
	}

	/**
	 * @param skillId the skillId to set
	 */
	public void setSkillId(Long skillId) {
		this.skillId = skillId;
	}

	/**
	 * @return the skillLevel
	 */
	public String getSkillLevel() {
		return skillLevel;
	}

	/**
	 * @param skillLevel the skillLevel to set
	 */
	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}

	/**
	 * @return the referenceName
	 */
	public String getReferenceName() {
		return referenceName;
	}

	/**
	 * @param referenceName the referenceName to set
	 */
	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}

	/**
	 * @return the referenceOrganisationName
	 */
	public String getReferenceOrganisationName() {
		return referenceOrganisationName;
	}

	/**
	 * @param referenceOrganisationName the referenceOrganisationName to set
	 */
	public void setReferenceOrganisationName(String referenceOrganisationName) {
		this.referenceOrganisationName = referenceOrganisationName;
	}

	/**
	 * @return the referencePhoneNUmber
	 */
	public String getReferencePhoneNUmber() {
		return referencePhoneNUmber;
	}

	/**
	 * @param referencePhoneNUmber the referencePhoneNUmber to set
	 */
	public void setReferencePhoneNUmber(String referencePhoneNUmber) {
		this.referencePhoneNUmber = referencePhoneNUmber;
	}

	/**
	 * @return the referenceEmail
	 */
	public String getReferenceEmail() {
		return referenceEmail;
	}

	/**
	 * @param referenceEmail the referenceEmail to set
	 */
	public void setReferenceEmail(String referenceEmail) {
		this.referenceEmail = referenceEmail;
	}

	/**
	 * @return the dependantName
	 */
	public String getDependantName() {
		return dependantName;
	}

	/**
	 * @param dependantName the dependantName to set
	 */
	public void setDependantName(String dependantName) {
		this.dependantName = dependantName;
	}

	/**
	 * @return the dependantDateOfBirth
	 */
	public String getDependantDateOfBirth() {
		return dependantDateOfBirth;
	}

	/**
	 * @param dependantDateOfBirth the dependantDateOfBirth to set
	 */
	public void setDependantDateOfBirth(String dependantDateOfBirth) {
		this.dependantDateOfBirth = dependantDateOfBirth;
	}

	/**
	 * @return the dependantRelation
	 */
	public String getDependantRelation() {
		return dependantRelation;
	}

	/**
	 * @param dependantRelation the dependantRelation to set
	 */
	public void setDependantRelation(String dependantRelation) {
		this.dependantRelation = dependantRelation;
	}

	/**
	 * @return the dependantNominee
	 */
	public Boolean getDependantNominee() {
		return dependantNominee;
	}

	/**
	 * @param dependantNominee the dependantNominee to set
	 */
	public void setDependantNominee(Boolean dependantNominee) {
		this.dependantNominee = dependantNominee;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the nextOfKinName
	 */
	public String getNextOfKinName() {
		return nextOfKinName;
	}

	/**
	 * @param nextOfKinName the nextOfKinName to set
	 */
	public void setNextOfKinName(String nextOfKinName) {
		this.nextOfKinName = nextOfKinName;
	}

	/**
	 * @return the nextOfKinDOB
	 */
	public String getNextOfKinDOB() {
		return nextOfKinDOB;
	}

	/**
	 * @param nextOfKinDOB the nextOfKinDOB to set
	 */
	public void setNextOfKinDOB(String nextOfKinDOB) {
		this.nextOfKinDOB = nextOfKinDOB;
	}

	/**
	 * @return the nextOFKinRelation
	 */
	public String getNextOFKinRelation() {
		return nextOFKinRelation;
	}

	/**
	 * @param nextOFKinRelation the nextOFKinRelation to set
	 */
	public void setNextOFKinRelation(String nextOFKinRelation) {
		this.nextOFKinRelation = nextOFKinRelation;
	}

	/**
	 * @return the nextOfKinAddress
	 */
	public String getNextOfKinAddress() {
		return nextOfKinAddress;
	}

	/**
	 * @param nextOfKinAddress the nextOfKinAddress to set
	 */
	public void setNextOfKinAddress(String nextOfKinAddress) {
		this.nextOfKinAddress = nextOfKinAddress;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the employeeId
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the accountTypeId
	 */
	public Long getAccountTypeId() {
		return accountTypeId;
	}

	/**
	 * @param accountTypeId the accountTypeId to set
	 */
	public void setAccountTypeId(Long accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	/**
	 * @return the jobFieldId
	 */
	public Long getJobFieldId() {
		return jobFieldId;
	}

	/**
	 * @param jobFieldId the jobFieldId to set
	 */
	public void setJobFieldId(Long jobFieldId) {
		this.jobFieldId = jobFieldId;
	}

	/**
	 * @return the designationId
	 */
	public Long getDesignationId() {
		return designationId;
	}

	/**
	 * @param designationId the designationId to set
	 */
	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}
	
	
}
