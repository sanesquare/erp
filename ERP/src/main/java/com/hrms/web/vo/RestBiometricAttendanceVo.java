package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 4-July-2015
 */
public class RestBiometricAttendanceVo {

	private String employeeCode;
	
	private String date;
	
	private String time;

	private List<String> times = new ArrayList<String>();
	
	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<String> getTimes() {
		return times;
	}

	public void setTimes(List<String> times) {
		this.times = times;
	}
}
