package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 10-April-2015
 */
public class HourlyWagesVo {

	private String employee;
	
	private Long hourlyWagesId;
	
	private Long employeeId;
	
	private String employeeCode;
	
	private String regularHoursAmount;
	
	private String overTimeHoursAmount;
	
	private String notes;
	
	private String description;
	
	private String createdBy;
	
	private String createdOn;

	public Long getHourlyWagesId() {
		return hourlyWagesId;
	}

	public void setHourlyWagesId(Long hourlyWagesId) {
		this.hourlyWagesId = hourlyWagesId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getRegularHoursAmount() {
		return regularHoursAmount;
	}

	public void setRegularHoursAmount(String regularHoursAmount) {
		this.regularHoursAmount = regularHoursAmount;
	}

	public String getOverTimeHoursAmount() {
		return overTimeHoursAmount;
	}

	public void setOverTimeHoursAmount(String overTimeHoursAmount) {
		this.overTimeHoursAmount = overTimeHoursAmount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}
}
