package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author shamsheer
 * @since 23-March-2015
 */
public class EmployeeJoiningVo {
	
	private String currentBranch;
	
	private String currentDepartment;
	
	private String employee;
	
	private String createdBy;
	
	private String createdOn;
	
	private String employeeName;
	
	private String employeeCode;
	
	private Long joiningId;

	private Long bId;
	
	private Long dId;
	
	private Long employeeId;
	
	private String joiningDate;
	
	private String notes;
	
	private String employeeType;
	
	private Long employeeTypeId;
	
	private String designation;
	
	private Long designationId;
	
	private String branchName;
	
	private Long branchId;
	
	private String departmentName;
	
	private Long departmentId;
	
	private List<MultipartFile> emp_join_files_upld;
	
	public List<EmployeeDocumentsVo> documents;
	
	private EmployeeVo employeeVo;

	/**
	 * @return the employeeId
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the joiningDate
	 */
	public String getJoiningDate() {
		return joiningDate;
	}

	/**
	 * @param joiningDate the joiningDate to set
	 */
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the employeeType
	 */
	public String getEmployeeType() {
		return employeeType;
	}

	/**
	 * @param employeeType the employeeType to set
	 */
	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	/**
	 * @return the employeeTypeId
	 */
	public Long getEmployeeTypeId() {
		return employeeTypeId;
	}

	/**
	 * @param employeeTypeId the employeeTypeId to set
	 */
	public void setEmployeeTypeId(Long employeeTypeId) {
		this.employeeTypeId = employeeTypeId;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the designationId
	 */
	public Long getDesignationId() {
		return designationId;
	}

	/**
	 * @param designationId the designationId to set
	 */
	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * @param branchName the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * @return the branchId
	 */
	public Long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the departmentName
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * @param departmentName the departmentName to set
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * @return the departmentId
	 */
	public Long getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId the departmentId to set
	 */
	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the bId
	 */
	public Long getbId() {
		return bId;
	}

	/**
	 * @param bId the bId to set
	 */
	public void setbId(Long bId) {
		this.bId = bId;
	}

	/**
	 * @return the dId
	 */
	public Long getdId() {
		return dId;
	}

	/**
	 * @param dId the dId to set
	 */
	public void setdId(Long dId) {
		this.dId = dId;
	}

	/**
	 * @return the joiningId
	 */
	public Long getJoiningId() {
		return joiningId;
	}

	/**
	 * @param joiningId the joiningId to set
	 */
	public void setJoiningId(Long joiningId) {
		this.joiningId = joiningId;
	}

	/**
	 * @return the emp_join_files_upld
	 */
	public List<MultipartFile> getEmp_join_files_upld() {
		return emp_join_files_upld;
	}

	/**
	 * @param emp_join_files_upld the emp_join_files_upld to set
	 */
	public void setEmp_join_files_upld(List<MultipartFile> emp_join_files_upld) {
		this.emp_join_files_upld = emp_join_files_upld;
	}

	/**
	 * @return the documents
	 */
	public List<EmployeeDocumentsVo> getDocuments() {
		return documents;
	}

	/**
	 * @param documents the documents to set
	 */
	public void setDocuments(List<EmployeeDocumentsVo> documents) {
		this.documents = documents;
	}

	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * @return the employeeCode
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * @param employeeCode the employeeCode to set
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * @return the employeeVo
	 */
	public EmployeeVo getEmployeeVo() {
		return employeeVo;
	}

	/**
	 * @param employeeVo the employeeVo to set
	 */
	public void setEmployeeVo(EmployeeVo employeeVo) {
		this.employeeVo = employeeVo;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getCurrentBranch() {
		return currentBranch;
	}

	public void setCurrentBranch(String currentBranch) {
		this.currentBranch = currentBranch;
	}

	public String getCurrentDepartment() {
		return currentDepartment;
	}

	public void setCurrentDepartment(String currentDepartment) {
		this.currentDepartment = currentDepartment;
	}
	
}
