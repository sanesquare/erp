package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 23-March-2015
 *
 */
public class CandidatesQualificationsVo {

	private Long candQualId;
	
	private String degree;
	
	private String subject;
	
	private String institute;
	
	private String grade;
	
	private String gradYear;

	
	/**
	 * @return the degree
	 */
	public String getDegree() {
		return degree;
	}

	/**
	 * @param degree the degree to set
	 */
	public void setDegree(String degree) {
		this.degree = degree;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the institute
	 */
	public String getInstitute() {
		return institute;
	}

	/**
	 * @param institute the institute to set
	 */
	public void setInstitute(String institute) {
		this.institute = institute;
	}

	/**
	 * @return the grade
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * @return the gradYear
	 */
	public String getGradYear() {
		return gradYear;
	}

	/**
	 * @param gradYear the gradYear to set
	 */
	public void setGradYear(String gradYear) {
		this.gradYear = gradYear;
	}

	/**
	 * @return the candQualId
	 */
	public Long getCandQualId() {
		return candQualId;
	}

	/**
	 * @param candQualId the candQualId to set
	 */
	public void setCandQualId(Long candQualId) {
		this.candQualId = candQualId;
	}
	
	
}
