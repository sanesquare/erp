package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 31-March-2015
 *
 */
public class ShiftDayVo {

	public Long shiftDayId;

	public String shiftDay;

	/**
	 * 
	 * @return
	 */
	public Long getShiftDayId() {
		return shiftDayId;
	}

	/**
	 * 
	 * @param shiftDayId
	 */
	public void setShiftDayId(Long shiftDayId) {
		this.shiftDayId = shiftDayId;
	}

	/**
	 * 
	 * @return
	 */
	public String getShiftDay() {
		return shiftDay;
	}

	/**
	 * 
	 * @param shiftDay
	 */
	public void setShiftDay(String shiftDay) {
		this.shiftDay = shiftDay;
	}

}
