package com.hrms.web.vo;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.JobCandidates;
import com.hrms.web.entities.JobInterviewDocuments;


/**
 * 
 * @author Vinutha
 * @since 19-March-2015
 *
 */
public class JobInterviewVo {

	private Long jobInterviewId;
	
	private Long jobPostId;
	
	private String jobPost;
	
	private String interviewDate;
	
	private String interviewTime;
	
	private Set<String> interviewers;
	
	private List<String> interviewees;
	
	private Set<String> statusses;
	//private Set<EmployeePersonalDetailsJsonVo> interviewers;
	
	//private Set<Employee> interviewers;
	
	private Set<Long> interViewerIds;
	
	//private Set<JobCandidates> interviewees;
	
	private Set<Long> intervieweeIds;
	
	private List<Long> statusId;
	
	private Set<CandidatesStatusVo> status;
	
	private String selectionNumber;
	
	private String place;
	
	private String description;
	
	private String additionalInfo;
	
	private String createdOn;
	
	private String createdBy;

	private List<MultipartFile> intUploadDocs;
	
	private List<InterviewDocumentsVo> documentVos;
	
	private String documentsPath;
	
	private String documentAccessUrl;
	
	private String documentName;
	
	//private Set<JobInterviewDocuments> interviewDocuments;

	/**
	 * @return the jobInterviewId
	 */
	public Long getJobInterviewId() {
		return jobInterviewId;
	}

	/**
	 * @param jobInterviewId the jobInterviewId to set
	 */
	public void setJobInterviewId(Long jobInterviewId) {
		this.jobInterviewId = jobInterviewId;
	}

	/**
	 * @return the jobPostId
	 */
	public Long getJobPostId() {
		return jobPostId;
	}

	/**
	 * @param jobPostId the jobPostId to set
	 */
	public void setJobPostId(Long jobPostId) {
		this.jobPostId = jobPostId;
	}

	/**
	 * @return the jobPost
	 */
	public String getJobPost() {
		return jobPost;
	}

	/**
	 * @param jobPost the jobPost to set
	 */
	public void setJobPost(String jobPost) {
		this.jobPost = jobPost;
	}

	/**
	 * @return the interviewDate
	 */
	public String getInterviewDate() {
		return interviewDate;
	}

	/**
	 * @param interviewDate the interviewDate to set
	 */
	public void setInterviewDate(String interviewDate) {
		this.interviewDate = interviewDate;
	}

	/**
	 * @return the interviewTime
	 */
	public String getInterviewTime() {
		return interviewTime;
	}

	/**
	 * @param interviewTime the interviewTime to set
	 */
	public void setInterviewTime(String interviewTime) {
		this.interviewTime = interviewTime;
	}
	/**
	 * @return the interViewerIds
	 */
	public Set<Long> getInterViewerIds() {
		return interViewerIds;
	}

	/**
	 * @param interViewerIds the interViewerIds to set
	 */
	public void setInterViewerIds(Set<Long> interViewerIds) {
		this.interViewerIds = interViewerIds;
	}
	/**
	 * @return the intervieweeIds
	 */
	public Set<Long> getIntervieweeIds() {
		return intervieweeIds;
	}

	/**
	 * @param intervieweeIds the intervieweeIds to set
	 */
	public void setIntervieweeIds(Set<Long> intervieweeIds) {
		this.intervieweeIds = intervieweeIds;
	}

	/**
	 * @return the statusId
	 */
	public List<Long> getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(List<Long> statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the status
	 */
	public Set<CandidatesStatusVo> getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Set<CandidatesStatusVo> status) {
		this.status = status;
	}

	/**
	 * @return the selectionNumber
	 */
	public String getSelectionNumber() {
		return selectionNumber;
	}

	/**
	 * @param selectionNumber the selectionNumber to set
	 */
	public void setSelectionNumber(String selectionNumber) {
		this.selectionNumber = selectionNumber;
	}

	/**
	 * @return the place
	 */
	public String getPlace() {
		return place;
	}

	/**
	 * @param place the place to set
	 */
	public void setPlace(String place) {
		this.place = place;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the additionalInfo
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * @param additionalInfo the additionalInfo to set
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the intUploadDocs
	 */
	public List<MultipartFile> getIntUploadDocs() {
		return intUploadDocs;
	}

	/**
	 * @param intUploadDocs the intUploadDocs to set
	 */
	public void setIntUploadDocs(List<MultipartFile> intUploadDocs) {
		this.intUploadDocs = intUploadDocs;
	}

	/**
	 * @return the documentVos
	 */
	public List<InterviewDocumentsVo> getDocumentVos() {
		return documentVos;
	}

	/**
	 * @param documentVos the documentVos to set
	 */
	public void setDocumentVos(List<InterviewDocumentsVo> documentVos) {
		this.documentVos = documentVos;
	}

	/**
	 * @return the documentsPath
	 */
	public String getDocumentsPath() {
		return documentsPath;
	}

	/**
	 * @param documentsPath the documentsPath to set
	 */
	public void setDocumentsPath(String documentsPath) {
		this.documentsPath = documentsPath;
	}

	/**
	 * @return the documentAccessUrl
	 */
	public String getDocumentAccessUrl() {
		return documentAccessUrl;
	}

	/**
	 * @param documentAccessUrl the documentAccessUrl to set
	 */
	public void setDocumentAccessUrl(String documentAccessUrl) {
		this.documentAccessUrl = documentAccessUrl;
	}

	/**
	 * @return the documentName
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * @param documentName the documentName to set
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * @return the interviewers
	 */
	public Set<String> getInterviewers() {
		return interviewers;
	}

	/**
	 * @param interviewers the interviewers to set
	 */
	public void setInterviewers(Set<String> interviewers) {
		this.interviewers = interviewers;
	}

	/**
	 * @return the statusses
	 */
	public Set<String> getStatusses() {
		return statusses;
	}

	/**
	 * @param statusses the statusses to set
	 */
	public void setStatusses(Set<String> statusses) {
		this.statusses = statusses;
	}

	/**
	 * @return the interviewees
	 */
	public List<String> getInterviewees() {
		return interviewees;
	}

	/**
	 * @param interviewees the interviewees to set
	 */
	public void setInterviewees(List<String> interviewees) {
		this.interviewees = interviewees;
	}

}
