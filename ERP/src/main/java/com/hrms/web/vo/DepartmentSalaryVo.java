package com.hrms.web.vo;

import java.math.BigDecimal;

/***
 * 
 * @author Vinutha
 * @since 17-April-2015
 *
 */
public class DepartmentSalaryVo {

	private String department;
	
	private BigDecimal depAmnt;

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the depAmnt
	 */
	public BigDecimal getDepAmnt() {
		return depAmnt;
	}

	/**
	 * @param depAmnt the depAmnt to set
	 */
	public void setDepAmnt(BigDecimal depAmnt) {
		this.depAmnt = depAmnt;
	}
	
	
}
