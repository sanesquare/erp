package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 06-May-2015
 */
public class RoleSessionVo {

	private Boolean view;
	private Boolean add;
	private Boolean update;
	private Boolean delete;
	public Boolean getView() {
		return view;
	}
	public void setView(Boolean view) {
		this.view = view;
	}
	public Boolean getAdd() {
		return add;
	}
	public void setAdd(Boolean add) {
		this.add = add;
	}
	public Boolean getUpdate() {
		return update;
	}
	public void setUpdate(Boolean update) {
		this.update = update;
	}
	public Boolean getDelete() {
		return delete;
	}
	public void setDelete(Boolean delete) {
		this.delete = delete;
	}
}
