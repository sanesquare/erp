package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

public class PayslipReportItemsVo {

	private List<PaysSlipVo> paysSlipVos = new ArrayList<PaysSlipVo>();

	public List<PaysSlipVo> getPaysSlipVos() {
		return paysSlipVos;
	}

	public void setPaysSlipVos(List<PaysSlipVo> paysSlipVos) {
		this.paysSlipVos = paysSlipVos;
	}
}
