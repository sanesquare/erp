package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Vinutha
 * @since 17-April-2015
 *
 */
public class BranchSalaryVo {

	private String branch;
	
	private BigDecimal branchAmnt;

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the branchAmnt
	 */
	public BigDecimal getBranchAmnt() {
		return branchAmnt;
	}

	/**
	 * @param branchAmnt the branchAmnt to set
	 */
	public void setBranchAmnt(BigDecimal branchAmnt) {
		this.branchAmnt = branchAmnt;
	}
	
	
}
