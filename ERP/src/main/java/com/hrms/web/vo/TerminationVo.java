package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
public class TerminationVo {

	private Long terminationId;

	private String employee;

	private String employeeCode;

	private Long terminatedEmployeeId;

	private List<String> forwaders;

	private List<String> forwadersName;
	
	private List<Long> smsEmployees;

	private Long terminatorEmployeeId;

	private String termiantedDate;

	private String description;

	private String terminationStatus;

	private String terminationStatusDescription;

	private String notes;

	private String recordedBy;

	private String recordedOn;

	private List<TerminationDocumentsVo> terminationDocumentsVos;

	private List<MultipartFile> terminationDocs;

	/**
	 * 
	 * @return
	 */
	public List<String> getForwadersName() {
		return forwadersName;
	}

	/**
	 * 
	 * @param forwadersName
	 */
	public void setForwadersName(List<String> forwadersName) {
		this.forwadersName = forwadersName;
	}

	/**
	 * 
	 * @return
	 */
	public List<TerminationDocumentsVo> getTerminationDocumentsVos() {
		return terminationDocumentsVos;
	}

	/**
	 * 
	 * @param terminationDocumentsVos
	 */
	public void setTerminationDocumentsVos(List<TerminationDocumentsVo> terminationDocumentsVos) {
		this.terminationDocumentsVos = terminationDocumentsVos;
	}

	/**
	 * 
	 * @return
	 */
	public List<MultipartFile> getTerminationDocs() {
		return terminationDocs;
	}

	/**
	 * 
	 * @param terminationDocs
	 */
	public void setTerminationDocs(List<MultipartFile> terminationDocs) {
		this.terminationDocs = terminationDocs;
	}

	/**
	 * 
	 * @return
	 */
	public Long getTerminationId() {
		return terminationId;
	}

	/**
	 * 
	 * @param terminationId
	 */
	public void setTerminationId(Long terminationId) {
		this.terminationId = terminationId;
	}

	/**
	 * 
	 * @return
	 */
	public String getTerminationStatus() {
		return terminationStatus;
	}

	/**
	 * 
	 * @param terminationStatus
	 */
	public void setTerminationStatus(String terminationStatus) {
		this.terminationStatus = terminationStatus;
	}

	/**
	 * 
	 * @return
	 */
	public String getTerminationStatusDescription() {
		return terminationStatusDescription;
	}

	/**
	 * 
	 * @param terminationStatusDescription
	 */
	public void setTerminationStatusDescription(String terminationStatusDescription) {
		this.terminationStatusDescription = terminationStatusDescription;
	}

	/**
	 * 
	 * @return
	 */
	public Long getTerminatedEmployeeId() {
		return terminatedEmployeeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * 
	 * @param employeeCode
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * 
	 * @param terminatedEmployeeId
	 */
	public void setTerminatedEmployeeId(Long terminatedEmployeeId) {
		this.terminatedEmployeeId = terminatedEmployeeId;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getForwaders() {
		return forwaders;
	}

	/**
	 * 
	 * @param forwaders
	 */
	public void setForwaders(List<String> forwaders) {
		this.forwaders = forwaders;
	}

	/**
	 * 
	 * @return
	 */
	public Long getTerminatorEmployeeId() {
		return terminatorEmployeeId;
	}

	/**
	 * 
	 * @param terminatorEmployeeId
	 */
	public void setTerminatorEmployeeId(Long terminatorEmployeeId) {
		this.terminatorEmployeeId = terminatorEmployeeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getTermiantedDate() {
		return termiantedDate;
	}

	/**
	 * 
	 * @param termiantedDate
	 */
	public void setTermiantedDate(String termiantedDate) {
		this.termiantedDate = termiantedDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	public List<Long> getSmsEmployees() {
		return smsEmployees;
	}

	public void setSmsEmployees(List<Long> smsEmployees) {
		this.smsEmployees = smsEmployees;
	}

}
