package com.hrms.web.vo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 23-April-2015
 *
 */
public class PayRollResultVo {

	private String paySlipItem;

	private BigDecimal amount = new BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);

	/**
	 * 
	 * @return
	 */
	public String getPaySlipItem() {
		return paySlipItem;
	}

	/**
	 * 
	 * @param paySlipItem
	 */
	public void setPaySlipItem(String paySlipItem) {
		this.paySlipItem = paySlipItem;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
