package com.hrms.web.vo;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.entities.CandidateDocuments;
import com.hrms.web.entities.CandidateResume;
import com.hrms.web.entities.CandidateSkills;
import com.hrms.web.entities.CandidateWorkExperience;
import com.hrms.web.entities.CandidatesLanguage;
import com.hrms.web.entities.CandidatesQualification;
import com.hrms.web.entities.CandidatesReferences;

/**
 * 
 * @author Vinutha
 * @since 19-March-2015
 *
 */
public class JobCandidatesVo {

	private Long jobCandidateId;
	
	private String jobField;
	
	private Long jobFieldId;
	
	//private String designation
	
	private String firstName;
	
	private String lastName;
	
	private String birthDate;
	
	private String Gender;
	
	private String nationality;
	
	private Long countryId;
	
	private String address;
	
	private String city;
	
	private String state;
	
	private String zipCode;
	
	private String country;
	
	private String phoneNumber;
	
	private String mobileNumber;
	
	private String eMail;
	
	private String interests;
	
	private String achievements;
	
	private String additionalInfo;
	
	private String createdBy;
	
	private String createdOn;
	
	
	//Qualifications
	private Set<CandidatesQualificationsVo> candidateQualifications;
	
	private String qualification;
	
	private Long qualificationId;
	
	private String subject ;
	
	private String institute;
	
	private String grade;
	
	private String year;
	
	//language
	private Set<CandidatesLanguageVo> candidatesLanguages;
	
	private String language;
	
	private Long languageId;
	
	private String speaking;
	
	private String reading;
	
	private String writing;
	
	//Skills
	private Set<CandidatesSkillsVo> candidateSkills;
	
	private Long skillsId;
	
	private String skills;
	
	private String skillLevel;
	
	//Experience
	private Set<CandidatesExperienceVo> candidateWorkExperiences;
	
	private String organisationName;
	
	private String designation;
		
	private String field;
	
	private Long fieldId;
	
	private String description;
	
	private String startDate;
	
	private String endDate;
	
	private String startSalary;
	
	private String endSalary;
	
	//Reference
	private Set<CandidatesReferencesVo> candidatesReferences;
	
	private String refName;
	
	private String refOrganisation;
	
	private String refPhoneNumber;
	
	private String refEMail;
	
	//status
	
	private String status;
	
	//private Long statusId;
	
	//resume
	
	private String resumeAccessUrl;
	
	private String resumePath;
	
	private String resumeFileName;
	
	private CandidateResumeVo resumeVo;
	
	private MultipartFile uploadCandResume;
	
	//douments
	
	private Set<CandidateDocumentsVo> candidateDocumentsVos;
	
	private String documentAccessUrl;
	
	private String documentName;

	private String documentsPath;
	
	private List<MultipartFile> uploadCandDocs;

	/**
	 * @return the jobCandidateId
	 */
	public Long getJobCandidateId() {
		return jobCandidateId;
	}

	/**
	 * @param jobCandidateId the jobCandidateId to set
	 */
	public void setJobCandidateId(Long jobCandidateId) {
		this.jobCandidateId = jobCandidateId;
	}

	/**
	 * @return the jobField
	 */
	public String getJobField() {
		return jobField;
	}

	/**
	 * @param jobField the jobField to set
	 */
	public void setJobField(String jobField) {
		this.jobField = jobField;
	}

	/**
	 * @return the jobFieldId
	 */
	public Long getJobFieldId() {
		return jobFieldId;
	}

	/**
	 * @param jobFieldId the jobFieldId to set
	 */
	public void setJobFieldId(Long jobFieldId) {
		this.jobFieldId = jobFieldId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the birthDate
	 */
	public String getBirthDate() {
		return birthDate;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return Gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		Gender = gender;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the countryId
	 */
	public Long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the eMail
	 */
	public String geteMail() {
		return eMail;
	}

	/**
	 * @param eMail the eMail to set
	 */
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	/**
	 * @return the interests
	 */
	public String getInterests() {
		return interests;
	}

	/**
	 * @param interests the interests to set
	 */
	public void setInterests(String interests) {
		this.interests = interests;
	}

	/**
	 * @return the achievements
	 */
	public String getAchievements() {
		return achievements;
	}

	/**
	 * @param achievements the achievements to set
	 */
	public void setAchievements(String achievements) {
		this.achievements = achievements;
	}

	/**
	 * @return the additionalInfo
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * @param additionalInfo the additionalInfo to set
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the candidateQualifications
	 */
	public Set<CandidatesQualificationsVo> getCandidateQualifications() {
		return candidateQualifications;
	}

	/**
	 * @param candidateQualifications the candidateQualifications to set
	 */
	public void setCandidateQualifications(Set<CandidatesQualificationsVo> candidateQualifications) {
		this.candidateQualifications = candidateQualifications;
	}

	/**
	 * @return the qualification
	 */
	public String getQualification() {
		return qualification;
	}

	/**
	 * @param qualification the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	/**
	 * @return the qualificationId
	 */
	public Long getQualificationId() {
		return qualificationId;
	}

	/**
	 * @param qualificationId the qualificationId to set
	 */
	public void setQualificationId(Long qualificationId) {
		this.qualificationId = qualificationId;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the institute
	 */
	public String getInstitute() {
		return institute;
	}

	/**
	 * @param institute the institute to set
	 */
	public void setInstitute(String institute) {
		this.institute = institute;
	}

	/**
	 * @return the grade
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the candidatesLanguages
	 */
	public Set<CandidatesLanguageVo> getCandidatesLanguages() {
		return candidatesLanguages;
	}

	/**
	 * @param candidatesLanguages the candidatesLanguages to set
	 */
	public void setCandidatesLanguages(Set<CandidatesLanguageVo> candidatesLanguages) {
		this.candidatesLanguages = candidatesLanguages;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the languageId
	 */
	public Long getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the speaking
	 */
	public String getSpeaking() {
		return speaking;
	}

	/**
	 * @param speaking the speaking to set
	 */
	public void setSpeaking(String speaking) {
		this.speaking = speaking;
	}

	/**
	 * @return the reading
	 */
	public String getReading() {
		return reading;
	}

	/**
	 * @param reading the reading to set
	 */
	public void setReading(String reading) {
		this.reading = reading;
	}

	/**
	 * @return the writing
	 */
	public String getWriting() {
		return writing;
	}

	/**
	 * @param writing the writing to set
	 */
	public void setWriting(String writing) {
		this.writing = writing;
	}

	/**
	 * @return the candidateSkills
	 */
	public Set<CandidatesSkillsVo> getCandidateSkills() {
		return candidateSkills;
	}

	/**
	 * @param candidateSkills the candidateSkills to set
	 */
	public void setCandidateSkills(Set<CandidatesSkillsVo> candidateSkills) {
		this.candidateSkills = candidateSkills;
	}

	/**
	 * @return the skillsId
	 */
	public Long getSkillsId() {
		return skillsId;
	}

	/**
	 * @param skillsId the skillsId to set
	 */
	public void setSkillsId(Long skillsId) {
		this.skillsId = skillsId;
	}

	/**
	 * @return the skills
	 */
	public String getSkills() {
		return skills;
	}

	/**
	 * @param skills the skills to set
	 */
	public void setSkills(String skills) {
		this.skills = skills;
	}

	/**
	 * @return the skillLevel
	 */
	public String getSkillLevel() {
		return skillLevel;
	}

	/**
	 * @param skillLevel the skillLevel to set
	 */
	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}

	/**
	 * @return the candidateWorkExperiences
	 */
	public Set<CandidatesExperienceVo> getCandidateWorkExperiences() {
		return candidateWorkExperiences;
	}

	/**
	 * @param candidateWorkExperiences the candidateWorkExperiences to set
	 */
	public void setCandidateWorkExperiences(Set<CandidatesExperienceVo> candidateWorkExperiences) {
		this.candidateWorkExperiences = candidateWorkExperiences;
	}

	/**
	 * @return the organisationName
	 */
	public String getOrganisationName() {
		return organisationName;
	}

	/**
	 * @param organisationName the organisationName to set
	 */
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**
	 * @return the fieldId
	 */
	public Long getFieldId() {
		return fieldId;
	}

	/**
	 * @param fieldId the fieldId to set
	 */
	public void setFieldId(Long fieldId) {
		this.fieldId = fieldId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the startSalary
	 */
	public String getStartSalary() {
		return startSalary;
	}

	/**
	 * @param startSalary the startSalary to set
	 */
	public void setStartSalary(String startSalary) {
		this.startSalary = startSalary;
	}

	/**
	 * @return the endSalary
	 */
	public String getEndSalary() {
		return endSalary;
	}

	/**
	 * @param endSalary the endSalary to set
	 */
	public void setEndSalary(String endSalary) {
		this.endSalary = endSalary;
	}

	/**
	 * @return the candidatesReferences
	 */
	public Set<CandidatesReferencesVo> getCandidatesReferences() {
		return candidatesReferences;
	}

	/**
	 * @param candidatesReferences the candidatesReferences to set
	 */
	public void setCandidatesReferences(Set<CandidatesReferencesVo> candidatesReferences) {
		this.candidatesReferences = candidatesReferences;
	}

	/**
	 * @return the refName
	 */
	public String getRefName() {
		return refName;
	}

	/**
	 * @param refName the refName to set
	 */
	public void setRefName(String refName) {
		this.refName = refName;
	}

	/**
	 * @return the refOrganisation
	 */
	public String getRefOrganisation() {
		return refOrganisation;
	}

	/**
	 * @param refOrganisation the refOrganisation to set
	 */
	public void setRefOrganisation(String refOrganisation) {
		this.refOrganisation = refOrganisation;
	}

	/**
	 * @return the refPhoneNumber
	 */
	public String getRefPhoneNumber() {
		return refPhoneNumber;
	}

	/**
	 * @param refPhoneNumber the refPhoneNumber to set
	 */
	public void setRefPhoneNumber(String refPhoneNumber) {
		this.refPhoneNumber = refPhoneNumber;
	}

	/**
	 * @return the refEMail
	 */
	public String getRefEMail() {
		return refEMail;
	}

	/**
	 * @param refEMail the refEMail to set
	 */
	public void setRefEMail(String refEMail) {
		this.refEMail = refEMail;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the resumeAccessUrl
	 */
	public String getResumeAccessUrl() {
		return resumeAccessUrl;
	}

	/**
	 * @param resumeAccessUrl the resumeAccessUrl to set
	 */
	public void setResumeAccessUrl(String resumeAccessUrl) {
		this.resumeAccessUrl = resumeAccessUrl;
	}

	/**
	 * @return the resumeFileName
	 */
	public String getResumeFileName() {
		return resumeFileName;
	}

	/**
	 * @param resumeFileName the resumeFileName to set
	 */
	public void setResumeFileName(String resumeFileName) {
		this.resumeFileName = resumeFileName;
	}

	
	/**
	 * @return the resumeVo
	 */
	public CandidateResumeVo getResumeVo() {
		return resumeVo;
	}

	/**
	 * @param resumeVo the resumeVo to set
	 */
	public void setResumeVo(CandidateResumeVo resumeVo) {
		this.resumeVo = resumeVo;
	}

	/**
	 * @return the documentAccessUrl
	 */
	public String getDocumentAccessUrl() {
		return documentAccessUrl;
	}

	/**
	 * @param documentAccessUrl the documentAccessUrl to set
	 */
	public void setDocumentAccessUrl(String documentAccessUrl) {
		this.documentAccessUrl = documentAccessUrl;
	}

	/**
	 * @return the documentName
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * @param documentName the documentName to set
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * @return the documentsPath
	 */
	public String getDocumentsPath() {
		return documentsPath;
	}

	/**
	 * @param documentsPath the documentsPath to set
	 */
	public void setDocumentsPath(String documentsPath) {
		this.documentsPath = documentsPath;
	}

	/**
	 * @return the candidateDocumentsVos
	 */
	public Set<CandidateDocumentsVo> getCandidateDocumentsVos() {
		return candidateDocumentsVos;
	}

	/**
	 * @param candidateDocumentsVos the candidateDocumentsVos to set
	 */
	public void setCandidateDocumentsVos(Set<CandidateDocumentsVo> candidateDocumentsVos) {
		this.candidateDocumentsVos = candidateDocumentsVos;
	}

	/**
	 * @return the resumePath
	 */
	public String getResumePath() {
		return resumePath;
	}

	/**
	 * @param resumePath the resumePath to set
	 */
	public void setResumePath(String resumePath) {
		this.resumePath = resumePath;
	}


	/**
	 * @return the uploadCandResume
	 */
	public MultipartFile getUploadCandResume() {
		return uploadCandResume;
	}

	/**
	 * @param uploadCandResume the uploadCandResume to set
	 */
	public void setUploadCandResume(MultipartFile uploadCandResume) {
		this.uploadCandResume = uploadCandResume;
	}

	/**
	 * @return the uploadCandDocs
	 */
	public List<MultipartFile> getUploadCandDocs() {
		return uploadCandDocs;
	}

	/**
	 * @param uploadCandDocs the uploadCandDocs to set
	 */
	public void setUploadCandDocs(List<MultipartFile> uploadCandDocs) {
		this.uploadCandDocs = uploadCandDocs;
	}
	
	
}
