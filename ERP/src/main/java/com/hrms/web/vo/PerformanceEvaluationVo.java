package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;


/**
 * 
 * @author Jithin Mohan
 * @since 24-March-2015
 *
 */
public class PerformanceEvaluationVo {

	private Long evaluationId;

	private String evaluationTitle;

	private String startDate;

	private String endDate;

	private String description;

	private String notes;

	private String recordedOn;

	private String recordedBy;

	private List<Long> branches;

	private List<PerformanceEvaluationDocumentVo> evaluationDocuments;

	private List<PerformanceEvaluationQuestionVo> evaluationQuestionVos;

	private List<MultipartFile> documents;

	/**
	 * 
	 * @return
	 */
	public List<Long> getBranches() {
		return branches;
	}

	/**
	 * 
	 * @param branches
	 */
	public void setBranches(List<Long> branches) {
		this.branches = branches;
	}

	/**
	 * 
	 * @return
	 */
	public List<PerformanceEvaluationQuestionVo> getEvaluationQuestionVos() {
		return evaluationQuestionVos;
	}

	/**
	 * 
	 * @param evaluationQuestionVos
	 */
	public void setEvaluationQuestionVos(List<PerformanceEvaluationQuestionVo> evaluationQuestionVos) {
		this.evaluationQuestionVos = evaluationQuestionVos;
	}

	/**
	 * 
	 * @return
	 */
	public String getEvaluationTitle() {
		return evaluationTitle;
	}

	/**
	 * 
	 * @param evaluationTitle
	 */
	public void setEvaluationTitle(String evaluationTitle) {
		this.evaluationTitle = evaluationTitle;
	}

	/**
	 * 
	 * @return
	 */
	public Long getEvaluationId() {
		return evaluationId;
	}

	/**
	 * 
	 * @param evaluationId
	 */
	public void setEvaluationId(Long evaluationId) {
		this.evaluationId = evaluationId;
	}

	/**
	 * 
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * 
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * 
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

	/**
	 * 
	 * @return
	 */
	public List<PerformanceEvaluationDocumentVo> getEvaluationDocuments() {
		return evaluationDocuments;
	}

	/**
	 * 
	 * @param evaluationDocuments
	 */
	public void setEvaluationDocuments(List<PerformanceEvaluationDocumentVo> evaluationDocuments) {
		this.evaluationDocuments = evaluationDocuments;
	}

	/**
	 * 
	 * @return
	 */
	public List<MultipartFile> getDocuments() {
		return documents;
	}

	/**
	 * 
	 * @param documents
	 */
	public void setDocuments(List<MultipartFile> documents) {
		this.documents = documents;
	}

}
