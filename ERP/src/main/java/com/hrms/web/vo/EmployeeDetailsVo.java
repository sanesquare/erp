package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 20-April-2015
 */
public class EmployeeDetailsVo {

	private String employeeGrade;
	private String designation;
	private String employeeType;
	private String employeeCategory;
	private Long employeeCategoryId;
	private String joiningDate;
	private Long employeeTypeId;
	private Long designationId;
	private String employeeBranch;
	private Long employeeBranchId;
	private String employeeDepartment;
	private Long employeeDepartmentId;
	private Long employeeGradeId;
	private String email;
	private String employeeName;
	private SuperiorSubordinateVo superiorSubordinateVo;
	private String regularHoursAmount;
	private String overTimeHoursAmount;
	private Long hourlyWagesId;
	private String hourlyWagesDescription;
	private String hourlyWagesNotes;
	private String dailyWageTitle;
	private String dailyWage;
	private String dailyWageDescription;
	private String dailyWageNotes;
	private Long dailyWageId;
	private List<ProjectVo> projectVo;
	private String salaryType;
	private Long salaryTypeId;
	private List<EmployeeLeaveVo> leaveVos;
	
	public String getEmployeeGrade() {
		return employeeGrade;
	}
	public void setEmployeeGrade(String employeeGrade) {
		this.employeeGrade = employeeGrade;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}
	public String getEmployeeCategory() {
		return employeeCategory;
	}
	public void setEmployeeCategory(String employeeCategory) {
		this.employeeCategory = employeeCategory;
	}
	public String getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}
	public Long getEmployeeTypeId() {
		return employeeTypeId;
	}
	public void setEmployeeTypeId(Long employeeTypeId) {
		this.employeeTypeId = employeeTypeId;
	}
	public Long getDesignationId() {
		return designationId;
	}
	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}
	public String getEmployeeBranch() {
		return employeeBranch;
	}
	public void setEmployeeBranch(String employeeBranch) {
		this.employeeBranch = employeeBranch;
	}
	public Long getEmployeeBranchId() {
		return employeeBranchId;
	}
	public void setEmployeeBranchId(Long employeeBranchId) {
		this.employeeBranchId = employeeBranchId;
	}
	public String getEmployeeDepartment() {
		return employeeDepartment;
	}
	public void setEmployeeDepartment(String employeeDepartment) {
		this.employeeDepartment = employeeDepartment;
	}
	public Long getEmployeeDepartmentId() {
		return employeeDepartmentId;
	}
	public void setEmployeeDepartmentId(Long employeeDepartmentId) {
		this.employeeDepartmentId = employeeDepartmentId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public SuperiorSubordinateVo getSuperiorSubordinateVo() {
		return superiorSubordinateVo;
	}
	public void setSuperiorSubordinateVo(SuperiorSubordinateVo superiorSubordinateVo) {
		this.superiorSubordinateVo = superiorSubordinateVo;
	}
	public String getRegularHoursAmount() {
		return regularHoursAmount;
	}
	public void setRegularHoursAmount(String regularHoursAmount) {
		this.regularHoursAmount = regularHoursAmount;
	}
	public String getOverTimeHoursAmount() {
		return overTimeHoursAmount;
	}
	public void setOverTimeHoursAmount(String overTimeHoursAmount) {
		this.overTimeHoursAmount = overTimeHoursAmount;
	}
	public Long getHourlyWagesId() {
		return hourlyWagesId;
	}
	public void setHourlyWagesId(Long hourlyWagesId) {
		this.hourlyWagesId = hourlyWagesId;
	}
	public String getHourlyWagesDescription() {
		return hourlyWagesDescription;
	}
	public void setHourlyWagesDescription(String hourlyWagesDescription) {
		this.hourlyWagesDescription = hourlyWagesDescription;
	}
	public String getHourlyWagesNotes() {
		return hourlyWagesNotes;
	}
	public void setHourlyWagesNotes(String hourlyWagesNotes) {
		this.hourlyWagesNotes = hourlyWagesNotes;
	}
	public String getDailyWageTitle() {
		return dailyWageTitle;
	}
	public void setDailyWageTitle(String dailyWageTitle) {
		this.dailyWageTitle = dailyWageTitle;
	}
	public String getDailyWage() {
		return dailyWage;
	}
	public void setDailyWage(String dailyWage) {
		this.dailyWage = dailyWage;
	}
	public String getDailyWageDescription() {
		return dailyWageDescription;
	}
	public void setDailyWageDescription(String dailyWageDescription) {
		this.dailyWageDescription = dailyWageDescription;
	}
	public String getDailyWageNotes() {
		return dailyWageNotes;
	}
	public void setDailyWageNotes(String dailyWageNotes) {
		this.dailyWageNotes = dailyWageNotes;
	}
	public Long getDailyWageId() {
		return dailyWageId;
	}
	public void setDailyWageId(Long dailyWageId) {
		this.dailyWageId = dailyWageId;
	}
	public Long getEmployeeGradeId() {
		return employeeGradeId;
	}
	public void setEmployeeGradeId(Long employeeGradeId) {
		this.employeeGradeId = employeeGradeId;
	}
	public Long getEmployeeCategoryId() {
		return employeeCategoryId;
	}
	public void setEmployeeCategoryId(Long employeeCategoryId) {
		this.employeeCategoryId = employeeCategoryId;
	}
	public List<ProjectVo> getProjectVo() {
		return projectVo;
	}
	public void setProjectVo(List<ProjectVo> projectVo) {
		this.projectVo = projectVo;
	}
	public String getSalaryType() {
		return salaryType;
	}
	public void setSalaryType(String salaryType) {
		this.salaryType = salaryType;
	}
	public Long getSalaryTypeId() {
		return salaryTypeId;
	}
	public void setSalaryTypeId(Long salaryTypeId) {
		this.salaryTypeId = salaryTypeId;
	}
	public List<EmployeeLeaveVo> getLeaveVos() {
		return leaveVos;
	}
	public void setLeaveVos(List<EmployeeLeaveVo> leaveVos) {
		this.leaveVos = leaveVos;
	}
	
	
}
