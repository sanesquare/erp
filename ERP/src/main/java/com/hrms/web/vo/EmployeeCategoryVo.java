package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class EmployeeCategoryVo {

	private Long employeeCategoryId;

	private String employeeCategory;

	private String tempEmployeeCategory;

	/**
	 * 
	 * @return
	 */
	public String getTempEmployeeCategory() {
		return tempEmployeeCategory;
	}

	/**
	 * 
	 * @param tempEmployeeCategory
	 */
	public void setTempEmployeeCategory(String tempEmployeeCategory) {
		this.tempEmployeeCategory = tempEmployeeCategory;
	}

	/**
	 * 
	 * @return
	 */
	public Long getEmployeeCategoryId() {
		return employeeCategoryId;
	}

	/**
	 * 
	 * @param employeeTypeId
	 */
	public void setEmployeeCategoryId(Long employeeCategoryId) {
		this.employeeCategoryId = employeeCategoryId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeType() {
		return employeeCategory;
	}

	/**
	 * 
	 * @param employeeType
	 */
	public void setEmployeeCategory(String employeeCategory) {
		this.employeeCategory = employeeCategory;
	}

	/**
	 * @return the employeeCategory
	 */
	public String getEmployeeCategory() {
		return employeeCategory;
	}

}
