package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public class LoanDocumentVo {

	private Long loanDocId;

	private String documentName;

	private String documentUrl;

	public Long getLoanDocId() {
		return loanDocId;
	}

	/**
	 * 
	 * @param loanDocId
	 */
	public void setLoanDocId(Long loanDocId) {
		this.loanDocId = loanDocId;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * 
	 * @param documentName
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentUrl() {
		return documentUrl;
	}

	/**
	 * 
	 * @param documentUrl
	 */
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

}
