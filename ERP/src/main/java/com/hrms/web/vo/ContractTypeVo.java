package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public class ContractTypeVo {

	private Long contractTypeId;

	private String contractType;

	private String tempType;

	/**
	 * 
	 * @return
	 */
	public String getTempType() {
		return tempType;
	}

	/**
	 * 
	 * @param tempType
	 */
	public void setTempType(String tempType) {
		this.tempType = tempType;
	}

	/**
	 * 
	 * @return
	 */
	public Long getContractTypeId() {
		return contractTypeId;
	}

	/**
	 * 
	 * @param contractTypeId
	 */
	public void setContractTypeId(Long contractTypeId) {
		this.contractTypeId = contractTypeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getContractType() {
		return contractType;
	}

	/**
	 * 
	 * @param contractType
	 */
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

}
