package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public class ManagerReportingNotificationVo {

	private Long reportingNotificationId;

	private boolean isReport;

	private String managerReportingNotification;

	/**
	 * 
	 * @return
	 */
	public String getManagerReportingNotification() {
		return managerReportingNotification;
	}

	/**
	 * 
	 * @param managerReportingNotification
	 */
	public void setManagerReportingNotification(String managerReportingNotification) {
		this.managerReportingNotification = managerReportingNotification;
	}

	/**
	 * 
	 * @return
	 */
	public Long getReportingNotificationId() {
		return reportingNotificationId;
	}

	/**
	 * 
	 * @param reportingNotificationId
	 */
	public void setReportingNotificationId(Long reportingNotificationId) {
		this.reportingNotificationId = reportingNotificationId;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isReport() {
		return isReport;
	}

	/**
	 * 
	 * @param isReport
	 */
	public void setReport(boolean isReport) {
		this.isReport = isReport;
	}

}
