package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 08-09-2015
 *
 */
public class PopUpVo {

	private String test;

	private List<Long> branchIdList = new ArrayList<Long>();
	
	private List<Long> departmentIdList = new ArrayList<Long>();

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public List<Long> getBranchIdList() {
		return branchIdList;
	}

	public void setBranchIdList(List<Long> branchIdList) {
		this.branchIdList = branchIdList;
	}

	public List<Long> getDepartmentIdList() {
		return departmentIdList;
	}

	public void setDepartmentIdList(List<Long> departmentIdList) {
		this.departmentIdList = departmentIdList;
	}
}
