package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
public class TravelModeVo {

	private Long id;
	
	private String travelMode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTravelMode() {
		return travelMode;
	}

	public void setTravelMode(String travelMode) {
		this.travelMode = travelMode;
	}
}
