package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 30-March-2015
 */
public class SuperiorVo {

	private Long superiorId;
	
	private String superiorCode;
	
	private String superiorName;

	/**
	 * @return the superiorId
	 */
	public Long getSuperiorId() {
		return superiorId;
	}

	/**
	 * @param superiorId the superiorId to set
	 */
	public void setSuperiorId(Long superiorId) {
		this.superiorId = superiorId;
	}

	/**
	 * @return the superiorCode
	 */
	public String getSuperiorCode() {
		return superiorCode;
	}

	/**
	 * @param superiorCode the superiorCode to set
	 */
	public void setSuperiorCode(String superiorCode) {
		this.superiorCode = superiorCode;
	}

	/**
	 * @return the superiorName
	 */
	public String getSuperiorName() {
		return superiorName;
	}

	/**
	 * @param superiorName the superiorName to set
	 */
	public void setSuperiorName(String superiorName) {
		this.superiorName = superiorName;
	}
	
	
}
