package com.hrms.web.vo;



import java.util.ArrayList;
import java.util.List;

import com.hrms.web.entities.MstReference;
import com.hrms.web.entities.NotificationType;


public class NotificationVo {

	private Long id;
	
	private NotificationType notificationType;
	
	private MstReference mstReference;
	
	private List<Long> employeeIds = new ArrayList<Long>(0);
	
	private String isStoS;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the notificationType
	 */
	public NotificationType getNotificationType() {
		return notificationType;
	}

	/**
	 * @param notificationType the notificationType to set
	 */
	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * @return the mstReference
	 */
	public MstReference getMstReference() {
		return mstReference;
	}

	/**
	 * @param mstReference the mstReference to set
	 */
	public void setMstReference(MstReference mstReference) {
		this.mstReference = mstReference;
	}

	
	/**
	 * @return the isStoS
	 */
	public String getIsStoS() {
		return isStoS;
	}

	/**
	 * @param isStoS the isStoS to set
	 */
	public void setIsStoS(String isStoS) {
		this.isStoS = isStoS;
	}

	/**
	 * @return the employeeIds
	 */
	public List<Long> getEmployeeIds() {
		return employeeIds;
	}

	/**
	 * @param employeeIds the employeeIds to set
	 */
	public void setEmployeeIds(List<Long> employeeIds) {
		this.employeeIds = employeeIds;
	}


	
}
