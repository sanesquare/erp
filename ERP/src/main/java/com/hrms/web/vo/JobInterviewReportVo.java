package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 20-April-2015
 *
 */
public class JobInterviewReportVo {

	private Long branchID;
	
	private Long postId;
	
	private String startDate;
	
	private String endDate;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	/**
	 * @return the branchID
	 */
	public Long getBranchID() {
		return branchID;
	}

	/**
	 * @param branchID the branchID to set
	 */
	public void setBranchID(Long branchID) {
		this.branchID = branchID;
	}
	
	
}
