package com.hrms.web.vo;
/**
 * 
 * @author shamsheer
 * @since 21-March-2015
 */
public class EmployeeDocumentsVo {

	private String documnetUrl;
	
	private Long documentId;
	
	private String fileName;

	/**
	 * @return the documnetUrl
	 */
	public String getDocumnetUrl() {
		return documnetUrl;
	}

	/**
	 * @param documnetUrl the documnetUrl to set
	 */
	public void setDocumnetUrl(String documnetUrl) {
		this.documnetUrl = documnetUrl;
	}

	/**
	 * @return the documentId
	 */
	public Long getDocumentId() {
		return documentId;
	}

	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
