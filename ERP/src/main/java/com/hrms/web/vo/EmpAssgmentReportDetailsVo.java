package com.hrms.web.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpAssgmentReportDetailsVo {

	private int slno;
	
	private String project;
	
	private String assigmnt;
	
	private String assignedTo;
	
	private String assignedBy;
	
	@DateTimeFormat
	private Date startDate;
	
	@DateTimeFormat
	private Date endDate;

	private String startDateString;
	
	private String endDateString;
	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(String project) {
		this.project = project;
	}

	/**
	 * @return the assigmnt
	 */
	public String getAssigmnt() {
		return assigmnt;
	}

	/**
	 * @param assigmnt the assigmnt to set
	 */
	public void setAssigmnt(String assigmnt) {
		this.assigmnt = assigmnt;
	}

	/**
	 * @return the assignedTo
	 */
	public String getAssignedTo() {
		return assignedTo;
	}

	/**
	 * @param assignedTo the assignedTo to set
	 */
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	/**
	 * @return the assignedBy
	 */
	public String getAssignedBy() {
		return assignedBy;
	}

	/**
	 * @param assignedBy the assignedBy to set
	 */
	public void setAssignedBy(String assignedBy) {
		this.assignedBy = assignedBy;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the startDateString
	 */
	public String getStartDateString() {
		return startDateString;
	}

	/**
	 * @param startDateString the startDateString to set
	 */
	public void setStartDateString(String startDateString) {
		this.startDateString = startDateString;
	}

	/**
	 * @return the endDateString
	 */
	public String getEndDateString() {
		return endDateString;
	}

	/**
	 * @param endDateString the endDateString to set
	 */
	public void setEndDateString(String endDateString) {
		this.endDateString = endDateString;
	}

	
	
	
	
}
