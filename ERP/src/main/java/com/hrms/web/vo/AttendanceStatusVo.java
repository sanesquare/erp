package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 27-May-2015
 *
 *
 */
public class AttendanceStatusVo {

	private List<Long> departmentIds = new ArrayList<Long>();

	private Long attendanceStatusId;

	private Long branchId;

	private Long departmentId;

	private String employee;

	private String employeeCode;

	private String status;

	private Long statusId;

	private List<AttendanceEmployeeVo> attendanceEmployeeVos = new ArrayList<AttendanceEmployeeVo>();

	private String date;

	private String notes;

	private String createdBy;

	private String createdOn;

	private List<Long> superiors;

	/**
	 * @return the attendanceStatusId
	 */
	public Long getAttendanceStatusId() {
		return attendanceStatusId;
	}

	/**
	 * @param attendanceStatusId
	 *            the attendanceStatusId to set
	 */
	public void setAttendanceStatusId(Long attendanceStatusId) {
		this.attendanceStatusId = attendanceStatusId;
	}

	/**
	 * @return the branchId
	 */
	public Long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId
	 *            the branchId to set
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the departmentId
	 */
	public Long getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            the departmentId to set
	 */
	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the employee
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * @param employee
	 *            the employee to set
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * @return the employeeCode
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * @param employeeCode
	 *            the employeeCode to set
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId
	 *            the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the superiors
	 */
	public List<Long> getSuperiors() {
		return superiors;
	}

	/**
	 * @param superiors
	 *            the superiors to set
	 */
	public void setSuperiors(List<Long> superiors) {
		this.superiors = superiors;
	}

	public List<Long> getDepartmentIds() {
		return departmentIds;
	}

	public void setDepartmentIds(List<Long> departmentIds) {
		this.departmentIds = departmentIds;
	}

	public List<AttendanceEmployeeVo> getAttendanceEmployeeVos() {
		return attendanceEmployeeVos;
	}

	public void setAttendanceEmployeeVos(List<AttendanceEmployeeVo> attendanceEmployeeVos) {
		this.attendanceEmployeeVos = attendanceEmployeeVos;
	}

}
