package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 1-April-2015
 *
 */
public class WorkShiftDetailsVo {

	private Long workShiftId;

	private String shiftDay;

	private String regularWorkFrom;

	private String regularWorkTo;

	private String refreshmentBreakFrom;

	private String refreshmentBreakTo;

	private String additionalBreakFrom;

	private String additionalBreakTo;

	private String employee;

	private Boolean available;

	/**
	 * 
	 * @return
	 */
	public Boolean getAvailable() {
		return available;
	}

	/**
	 * 
	 * @param available
	 */
	public void setAvailable(Boolean available) {
		this.available = available;
	}

	/**
	 * 
	 * @return
	 */
	public Long getWorkShiftId() {
		return workShiftId;
	}

	/**
	 * 
	 * @param workShiftId
	 */
	public void setWorkShiftId(Long workShiftId) {
		this.workShiftId = workShiftId;
	}

	/**
	 * 
	 * @return
	 */
	public String getShiftDay() {
		return shiftDay;
	}

	/**
	 * 
	 * @param shiftDay
	 */
	public void setShiftDay(String shiftDay) {
		this.shiftDay = shiftDay;
	}

	/**
	 * 
	 * @return
	 */
	public String getRegularWorkFrom() {
		return regularWorkFrom;
	}

	/**
	 * 
	 * @param regularWorkFrom
	 */
	public void setRegularWorkFrom(String regularWorkFrom) {
		this.regularWorkFrom = regularWorkFrom;
	}

	/**
	 * 
	 * @return
	 */
	public String getRegularWorkTo() {
		return regularWorkTo;
	}

	/**
	 * 
	 * @param regularWorkTo
	 */
	public void setRegularWorkTo(String regularWorkTo) {
		this.regularWorkTo = regularWorkTo;
	}

	/**
	 * 
	 * @return
	 */
	public String getRefreshmentBreakFrom() {
		return refreshmentBreakFrom;
	}

	/**
	 * 
	 * @param refreshmentBreakFrom
	 */
	public void setRefreshmentBreakFrom(String refreshmentBreakFrom) {
		this.refreshmentBreakFrom = refreshmentBreakFrom;
	}

	/**
	 * 
	 * @return
	 */
	public String getRefreshmentBreakTo() {
		return refreshmentBreakTo;
	}

	/**
	 * 
	 * @param refreshmentBreakTo
	 */
	public void setRefreshmentBreakTo(String refreshmentBreakTo) {
		this.refreshmentBreakTo = refreshmentBreakTo;
	}

	/**
	 * 
	 * @return
	 */
	public String getAdditionalBreakFrom() {
		return additionalBreakFrom;
	}

	/**
	 * 
	 * @param additionalBreakFrom
	 */
	public void setAdditionalBreakFrom(String additionalBreakFrom) {
		this.additionalBreakFrom = additionalBreakFrom;
	}

	/**
	 * 
	 * @return
	 */
	public String getAdditionalBreakTo() {
		return additionalBreakTo;
	}

	/**
	 * 
	 * @param additionalBreakTo
	 */
	public void setAdditionalBreakTo(String additionalBreakTo) {
		this.additionalBreakTo = additionalBreakTo;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

}
