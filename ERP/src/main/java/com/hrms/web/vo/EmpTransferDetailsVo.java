package com.hrms.web.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpTransferDetailsVo {

	private int slno;
	
	private String empName;
	
	private String transferTo;
	
	@DateTimeFormat
	private Date transferDate;
	
	private String dateString;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the transferTo
	 */
	public String getTransferTo() {
		return transferTo;
	}

	/**
	 * @param transferTo the transferTo to set
	 */
	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	/**
	 * @return the transferDate
	 */
	public Date getTransferDate() {
		return transferDate;
	}

	/**
	 * @param transferDate the transferDate to set
	 */
	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	/**
	 * @return the dateString
	 */
	public String getDateString() {
		return dateString;
	}

	/**
	 * @param dateString the dateString to set
	 */
	public void setDateString(String dateString) {
		this.dateString = dateString;
	}
	
}
