package com.hrms.web.vo;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public class RemindersVo {

	private Long reminderId;

	private String reminderTitle;

	private String reminderMonth;

	private String reminderDay;

	private String reminderTime;

	private String timeMeridian;

	private List<String> reminderTo;

	private String reminderStatus;

	private String reminderMessage;

	private String reminderDate;

	/**
	 * @return
	 */
	public String getReminderDate() {
		return reminderDate;
	}

	/**
	 * 
	 * @param reminderDate
	 */
	public void setReminderDate(String reminderDate) {
		this.reminderDate = reminderDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getReminderTime() {
		return reminderTime;
	}

	/**
	 * 
	 * @param reminderTime
	 */
	public void setReminderTime(String reminderTime) {
		this.reminderTime = reminderTime;
	}

	/**
	 * 
	 * @return
	 */
	public String getTimeMeridian() {
		return timeMeridian;
	}

	/**
	 * 
	 * @param timeMeridian
	 */
	public void setTimeMeridian(String timeMeridian) {
		this.timeMeridian = timeMeridian;
	}

	/**
	 * 
	 * @return
	 */
	public Long getReminderId() {
		return reminderId;
	}

	/**
	 * 
	 * @param reminderId
	 */
	public void setReminderId(Long reminderId) {
		this.reminderId = reminderId;
	}

	/**
	 * 
	 * @return
	 */
	public String getReminderTitle() {
		return reminderTitle;
	}

	/**
	 * 
	 * @param reminderTitle
	 */
	public void setReminderTitle(String reminderTitle) {
		this.reminderTitle = reminderTitle;
	}

	/**
	 * 
	 * @return
	 */
	public String getReminderMonth() {
		return reminderMonth;
	}

	/**
	 * 
	 * @param reminderMonth
	 */
	public void setReminderMonth(String reminderMonth) {
		this.reminderMonth = reminderMonth;
	}

	/**
	 * 
	 * @return
	 */
	public String getReminderDay() {
		return reminderDay;
	}

	/**
	 * 
	 * @param reminderDay
	 */
	public void setReminderDay(String reminderDay) {
		this.reminderDay = reminderDay;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getReminderTo() {
		return reminderTo;
	}

	/**
	 * 
	 * @param reminderTo
	 */
	public void setReminderTo(List<String> reminderTo) {
		this.reminderTo = reminderTo;
	}

	/**
	 * 
	 * @return
	 */
	public String getReminderStatus() {
		return reminderStatus;
	}

	/**
	 * 
	 * @param reminderStatus
	 */
	public void setReminderStatus(String reminderStatus) {
		this.reminderStatus = reminderStatus;
	}

	/**
	 * 
	 * @return
	 */
	public String getReminderMessage() {
		return reminderMessage;
	}

	/**
	 * 
	 * @param reminderMessage
	 */
	public void setReminderMessage(String reminderMessage) {
		this.reminderMessage = reminderMessage;
	}

}
