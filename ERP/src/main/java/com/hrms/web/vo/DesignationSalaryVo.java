package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Vinutha
 * @since 17-April-2015
 *
 */
public class DesignationSalaryVo {
	
	private String designation;
	
	private BigDecimal desSalary;

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the desSalary
	 */
	public BigDecimal getDesSalary() {
		return desSalary;
	}

	/**
	 * @param desSalary the desSalary to set
	 */
	public void setDesSalary(BigDecimal desSalary) {
		this.desSalary = desSalary;
	}
	
	

}
