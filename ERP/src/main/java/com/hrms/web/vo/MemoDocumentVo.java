package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-March-2015
 *
 */
public class MemoDocumentVo {

	private Long memoDocId;

	private String documentName;

	private String documentUrl;

	/**
	 * 
	 * @return
	 */
	public Long getMemoDocId() {
		return memoDocId;
	}

	/**
	 * 
	 * @param memoDocId
	 */
	public void setMemoDocId(Long memoDocId) {
		this.memoDocId = memoDocId;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * 
	 * @param documentName
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentUrl() {
		return documentUrl;
	}

	/**
	 * 
	 * @param documentUrl
	 */
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

}
