package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public class AdvanceSalaryVo {

	private Long advanceSalaryId;

	private Boolean isView = false;

	private Integer pendingInstallments;

	private Integer noOfInstallments;

	private String employee;

	private String employeeCode;

	private List<String> forwaders;

	private List<String> forwadersName;

	private String advanceSalaryTitle;

	private BigDecimal advanceAmount;

	private String requestedDate;

	private String description;

	private String notes;

	private String status;

	private String statusDescription;

	private String recordedOn;

	private String recordedBy;

	private BigDecimal repaymentAmount;

	private String repaymentStartDate;

	private BigDecimal balance;

	private BigDecimal balanceAmount;

	/**
	 * 
	 * @return
	 */
	public List<String> getForwadersName() {
		return forwadersName;
	}

	/**
	 * 
	 * @param forwadersName
	 */
	public void setForwadersName(List<String> forwadersName) {
		this.forwadersName = forwadersName;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * 
	 * @param employeeCode
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * 
	 * @return
	 */
	public Long getAdvanceSalaryId() {
		return advanceSalaryId;
	}

	/**
	 * 
	 * @param advanceSalaryId
	 */
	public void setAdvanceSalaryId(Long advanceSalaryId) {
		this.advanceSalaryId = advanceSalaryId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getForwaders() {
		return forwaders;
	}

	/**
	 * 
	 * @param forwaders
	 */
	public void setForwaders(List<String> forwaders) {
		this.forwaders = forwaders;
	}

	/**
	 * 
	 * @return
	 */
	public String getAdvanceSalaryTitle() {
		return advanceSalaryTitle;
	}

	/**
	 * 
	 * @param advanceSalaryTitle
	 */
	public void setAdvanceSalaryTitle(String advanceSalaryTitle) {
		this.advanceSalaryTitle = advanceSalaryTitle;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getAdvanceAmount() {
		return advanceAmount;
	}

	/**
	 * 
	 * @param advanceAmount
	 */
	public void setAdvanceAmount(BigDecimal advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	/**
	 * 
	 * @return
	 */
	public String getRequestedDate() {
		return requestedDate;
	}

	/**
	 * 
	 * @param requestedDate
	 */
	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * 
	 * @param statusDescription
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

	public BigDecimal getRepaymentAmount() {
		return repaymentAmount;
	}

	public void setRepaymentAmount(BigDecimal repaymentAmount) {
		this.repaymentAmount = repaymentAmount;
	}

	public String getRepaymentStartDate() {
		return repaymentStartDate;
	}

	public void setRepaymentStartDate(String repaymentStartDate) {
		this.repaymentStartDate = repaymentStartDate;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Boolean getIsView() {
		return isView;
	}

	public void setIsView(Boolean isView) {
		this.isView = isView;
	}

	public Integer getPendingInstallments() {
		return pendingInstallments;
	}

	public void setPendingInstallments(Integer pendingInstallments) {
		this.pendingInstallments = pendingInstallments;
	}

	public Integer getNoOfInstallments() {
		return noOfInstallments;
	}

	public void setNoOfInstallments(Integer noOfInstallments) {
		this.noOfInstallments = noOfInstallments;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

}
