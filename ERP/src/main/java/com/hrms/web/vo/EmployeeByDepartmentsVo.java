package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 16-april-2015
 * 
 *
 */
public class EmployeeByDepartmentsVo {

	private String department;
	
	private int departmentEmp;

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the departmentEmp
	 */
	public int getDepartmentEmp() {
		return departmentEmp;
	}

	/**
	 * @param departmentEmp the departmentEmp to set
	 */
	public void setDepartmentEmp(int departmentEmp) {
		this.departmentEmp = departmentEmp;
	}
	
	
}
