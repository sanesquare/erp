package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 25-March-2015
 *
 */
public class PromotionStatusVo {

	private Long promotionStatusId;

	private String promotionStatus;

	/**
	 * 
	 * @return
	 */
	public Long getPromotionStatusId() {
		return promotionStatusId;
	}

	/**
	 * 
	 * @param promotionStatusId
	 */
	public void setPromotionStatusId(Long promotionStatusId) {
		this.promotionStatusId = promotionStatusId;
	}

	/**
	 * 
	 * @return
	 */
	public String getPromotionStatus() {
		return promotionStatus;
	}

	/**
	 * 
	 * @param promotionStatus
	 */
	public void setPromotionStatus(String promotionStatus) {
		this.promotionStatus = promotionStatus;
	}

}
