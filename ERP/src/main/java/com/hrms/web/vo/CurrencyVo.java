package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class CurrencyVo {

	private Long currencyId;

	private Long baseCurrencyId;

	private String baseCurrency;

	private String currencySign;

	private Integer decimalPlaces;

	/**
	 * 
	 * @return
	 */
	public Long getCurrencyId() {
		return currencyId;
	}

	/**
	 * 
	 * @param currencyId
	 */
	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * 
	 * @return
	 */
	public Long getBaseCurrencyId() {
		return baseCurrencyId;
	}

	/**
	 * 
	 * @param baseCurrencyId
	 */
	public void setBaseCurrencyId(Long baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}

	/**
	 * 
	 * @return
	 */
	public String getBaseCurrency() {
		return baseCurrency;
	}

	/**
	 * 
	 * @param baseCurrency
	 */
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	/**
	 * 
	 * @return
	 */
	public String getCurrencySign() {
		return currencySign;
	}

	/**
	 * 
	 * @param currencySign
	 */
	public void setCurrencySign(String currencySign) {
		this.currencySign = currencySign;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getDecimalPlaces() {
		return decimalPlaces;
	}

	/**
	 * 
	 * @param decimalPlaces
	 */
	public void setDecimalPlaces(Integer decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}

}
