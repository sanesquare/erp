package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 08-June-2015
 *
 */
public class PaySalaryMonthlyVo {

	private List<PaySalaryMonthlyTitleVo> titleVos;

	private List<String> branches;

	private List<Long> branchIds;

	private List<String> departments;

	private List<Long> departmentIds;
	
	private String date;

	public List<PaySalaryMonthlyTitleVo> getTitleVos() {
		return titleVos;
	}

	public void setTitleVos(List<PaySalaryMonthlyTitleVo> titleVos) {
		this.titleVos = titleVos;
	}

	public List<String> getBranches() {
		return branches;
	}

	public void setBranches(List<String> branches) {
		this.branches = branches;
	}

	public List<Long> getBranchIds() {
		return branchIds;
	}

	public void setBranchIds(List<Long> branchIds) {
		this.branchIds = branchIds;
	}

	public List<String> getDepartments() {
		return departments;
	}

	public void setDepartments(List<String> departments) {
		this.departments = departments;
	}

	public List<Long> getDepartmentIds() {
		return departmentIds;
	}

	public void setDepartmentIds(List<Long> departmentIds) {
		this.departmentIds = departmentIds;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
