package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 2-May-2015
 *
 */
public class PaySalaryDetailsVo {

	private int slno;
	
	private String empName;
	
	private BigDecimal basicSalary;
	
	private double taxAmount;
	
	//private List<BigDecimal> allowances;
	private HashMap<String, BigDecimal> allowances;
	
	private BigDecimal totalSalary;
	
	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the taxAmount
	 */
	public double getTaxAmount() {
		return taxAmount;
	}

	/**
	 * @param taxAmount the taxAmount to set
	 */
	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * @return the basicSalary
	 */
	public BigDecimal getBasicSalary() {
		return basicSalary;
	}

	/**
	 * @param basicSalary the basicSalary to set
	 */
	public void setBasicSalary(BigDecimal basicSalary) {
		this.basicSalary = basicSalary;
	}

	/**
	 * @return the totalSalary
	 */
	public BigDecimal getTotalSalary() {
		return totalSalary;
	}

	/**
	 * @param totalSalary the totalSalary to set
	 */
	public void setTotalSalary(BigDecimal totalSalary) {
		this.totalSalary = totalSalary;
	}

	/**
	 * @return the allowances
	 */
	public HashMap<String, BigDecimal> getAllowances() {
		return allowances;
	}

	/**
	 * @param allowances the allowances to set
	 */
	public void setAllowances(HashMap<String, BigDecimal> allowances) {
		this.allowances = allowances;
	}

	
}
