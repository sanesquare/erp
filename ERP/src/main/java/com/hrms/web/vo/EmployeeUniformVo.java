package com.hrms.web.vo;
/**
 * 
 * @author shamsheer
 * @since 22-March-2015
 */
public class EmployeeUniformVo {

	private String checkout;
	
	private String amount;
	
	private String duration;
	
	private String reimbursemnt;

	/**
	 * @return the checkout
	 */
	public String getCheckout() {
		return checkout;
	}

	/**
	 * @param checkout the checkout to set
	 */
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the duration
	 */
	public String getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}

	/**
	 * @return the reimbursemnt
	 */
	public String getReimbursemnt() {
		return reimbursemnt;
	}

	/**
	 * @param reimbursemnt the reimbursemnt to set
	 */
	public void setReimbursemnt(String reimbursemnt) {
		this.reimbursemnt = reimbursemnt;
	}
	
	
}
