package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Shamsheer
 * @since 26-March-2015
 */
public class AssignmentVo {
	
	private String edit;

	private Long assignmentId;
	
	private String employee;
	
	private Long projectId;
	
	private Long employeeId;
	
	private String projectTitle;
	
	private String employeeName;
	
	private String employeeCode;
	
	private String assignmentName;
	
	private String description;
	
	private String notes;
	
	private String startDate;
	
	private String priority;
	
	private String endDate;
	
	private List<AssignmentDocumentsVo> documentsVos;
	
	private String createdOn;
	
	private String createdBy;
	
    private Long branchID;
    
    private Long departmetnId;
    
    private List<MultipartFile> upload_assignmnt_file;
    

	/**
	 * @return the assignmentId
	 */
	public Long getAssignmentId() {
		return assignmentId;
	}

	/**
	 * @param assignmentId the assignmentId to set
	 */
	public void setAssignmentId(Long assignmentId) {
		this.assignmentId = assignmentId;
	}

	/**
	 * @return the projectId
	 */
	public Long getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the employeeId
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the projectTitle
	 */
	public String getProjectTitle() {
		return projectTitle;
	}

	/**
	 * @param projectTitle the projectTitle to set
	 */
	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * @return the employeeCode
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * @param employeeCode the employeeCode to set
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * @return the assignmentName
	 */
	public String getAssignmentName() {
		return assignmentName;
	}

	/**
	 * @param assignmentName the assignmentName to set
	 */
	public void setAssignmentName(String assignmentName) {
		this.assignmentName = assignmentName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the documentsVos
	 */
	public List<AssignmentDocumentsVo> getDocumentsVos() {
		return documentsVos;
	}

	/**
	 * @param documentsVos the documentsVos to set
	 */
	public void setDocumentsVos(List<AssignmentDocumentsVo> documentsVos) {
		this.documentsVos = documentsVos;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * @return the branchID
	 */
	public Long getBranchID() {
		return branchID;
	}

	/**
	 * @param branchID the branchID to set
	 */
	public void setBranchID(Long branchID) {
		this.branchID = branchID;
	}

	/**
	 * @return the departmetnId
	 */
	public Long getDepartmetnId() {
		return departmetnId;
	}

	/**
	 * @param departmetnId the departmetnId to set
	 */
	public void setDepartmetnId(Long departmetnId) {
		this.departmetnId = departmetnId;
	}

	/**
	 * @return the upload_assignmnt_file
	 */
	public List<MultipartFile> getUpload_assignmnt_file() {
		return upload_assignmnt_file;
	}

	/**
	 * @param upload_assignmnt_file the upload_assignmnt_file to set
	 */
	public void setUpload_assignmnt_file(List<MultipartFile> upload_assignmnt_file) {
		this.upload_assignmnt_file = upload_assignmnt_file;
	}

	/**
	 * @return the edit
	 */
	public String getEdit() {
		return edit;
	}

	/**
	 * @param edit the edit to set
	 */
	public void setEdit(String edit) {
		this.edit = edit;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}
}
