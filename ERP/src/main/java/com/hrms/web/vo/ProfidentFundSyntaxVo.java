package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public class ProfidentFundSyntaxVo {

	private Long pfSyntaxId;

	private String employeeSyntax;

	private String employerSyntax;

	/**
	 * 
	 * @return
	 */
	public Long getPfSyntaxId() {
		return pfSyntaxId;
	}

	/**
	 * 
	 * @param pfSyntaxId
	 */
	public void setPfSyntaxId(Long pfSyntaxId) {
		this.pfSyntaxId = pfSyntaxId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeSyntax() {
		return employeeSyntax;
	}

	/**
	 * 
	 * @param employeeSyntax
	 */
	public void setEmployeeSyntax(String employeeSyntax) {
		this.employeeSyntax = employeeSyntax;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployerSyntax() {
		return employerSyntax;
	}

	/**
	 * 
	 * @param employerSyntax
	 */
	public void setEmployerSyntax(String employerSyntax) {
		this.employerSyntax = employerSyntax;
	}

}
