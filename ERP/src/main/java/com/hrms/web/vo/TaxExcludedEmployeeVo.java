package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 28-April-2015
 *
 */
public class TaxExcludedEmployeeVo {

	private Long tax_excluded_emp_id;

	private String excludedEmployeeCode;

	private String excludedEmployeeName;

	private List<String> excludedEmployeeCodes;

	private List<String> excludedEmployeeNames;

	/**
	 * 
	 * @return
	 */
	public Long getTax_excluded_emp_id() {
		return tax_excluded_emp_id;
	}

	/**
	 * 
	 * @param tax_excluded_emp_id
	 */
	public void setTax_excluded_emp_id(Long tax_excluded_emp_id) {
		this.tax_excluded_emp_id = tax_excluded_emp_id;
	}

	/**
	 * 
	 * @return
	 */
	public String getExcludedEmployeeCode() {
		return excludedEmployeeCode;
	}

	/**
	 * 
	 * @param excludedEmployeeCode
	 */
	public void setExcludedEmployeeCode(String excludedEmployeeCode) {
		this.excludedEmployeeCode = excludedEmployeeCode;
	}

	/**
	 * 
	 * @return
	 */
	public String getExcludedEmployeeName() {
		return excludedEmployeeName;
	}

	/**
	 * 
	 * @param excludedEmployeeName
	 */
	public void setExcludedEmployeeName(String excludedEmployeeName) {
		this.excludedEmployeeName = excludedEmployeeName;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getExcludedEmployeeCodes() {
		return excludedEmployeeCodes;
	}

	/**
	 * 
	 * @param excludedEmployeeCodes
	 */
	public void setExcludedEmployeeCodes(List<String> excludedEmployeeCodes) {
		this.excludedEmployeeCodes = excludedEmployeeCodes;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getExcludedEmployeeNames() {
		return excludedEmployeeNames;
	}

	/**
	 * 
	 * @param excludedEmployeeNames
	 */
	public void setExcludedEmployeeNames(List<String> excludedEmployeeNames) {
		this.excludedEmployeeNames = excludedEmployeeNames;
	}

}
