package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 20-April-2015
 *
 */
public class JobCandidateReportVo {

	private Long fieldId;
	
	private String designation;
	
	private String gender;

	public Long getFieldId() {
		return fieldId;
	}

	public void setFieldId(Long fieldId) {
		this.fieldId = fieldId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	
}
