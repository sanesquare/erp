package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 25-March-2015
 *
 */
public class ContractVo {
	
	private String createdBy;
	
	private String employee;
	
	private String createdOn; 
	
	private Long contractId;
	
	private String title;
	
	private String startingDate;
	
	private String endDate;
	
	private String notes;
	
	private String description;
	
	private Long employeeId;
	
	private String employeeCode;
	
	private Long contractTypeId;
	
	private String contractType;
	
	private Long branchID;
	
	private Long departmetnId;
	
	private EmployeeVo employeeVo;
	
	private List<ContractDocumentVo> documentVos;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(String startingDate) {
		this.startingDate = startingDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public Long getContractTypeId() {
		return contractTypeId;
	}

	public void setContractTypeId(Long contractTypeId) {
		this.contractTypeId = contractTypeId;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public Long getBranchID() {
		return branchID;
	}

	public void setBranchID(Long branchID) {
		this.branchID = branchID;
	}

	public Long getDepartmetnId() {
		return departmetnId;
	}

	public void setDepartmetnId(Long departmetnId) {
		this.departmetnId = departmetnId;
	}

	public EmployeeVo getEmployeeVo() {
		return employeeVo;
	}

	public void setEmployeeVo(EmployeeVo employeeVo) {
		this.employeeVo = employeeVo;
	}

	public List<ContractDocumentVo> getDocumentVos() {
		return documentVos;
	}

	public void setDocumentVos(List<ContractDocumentVo> documentVos) {
		this.documentVos = documentVos;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}
	
}
