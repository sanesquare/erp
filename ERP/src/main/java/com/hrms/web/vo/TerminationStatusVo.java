package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
public class TerminationStatusVo {

	private Long terminationStatusId;

	private String status;

	/**
	 * 
	 * @return
	 */
	public Long getTerminationStatusId() {
		return terminationStatusId;
	}

	/**
	 * 
	 * @param terminationStatusId
	 */
	public void setTerminationStatusId(Long terminationStatusId) {
		this.terminationStatusId = terminationStatusId;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
