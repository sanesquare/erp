package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
public class BonusVo {

	private String employee;
	
	private Long id;
	
	private Long bonusId;
	
	private Long bonusTitleId;
	
	private Long employeeId;
	
	private String amount;
	
	private String date;
	
	private String description;
	
	private String notes;
	
	private String employeeCode;
	
	private String title;
	
	private String creatdBy;
	
	private String createdOn;

	public Long getBonusId() {
		return bonusId;
	}

	public void setBonusId(Long bonusId) {
		this.bonusId = bonusId;
	}

	public Long getBonusTitleId() {
		return bonusTitleId;
	}

	public void setBonusTitleId(Long bonusTitleId) {
		this.bonusTitleId = bonusTitleId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCreatdBy() {
		return creatdBy;
	}

	public void setCreatdBy(String creatdBy) {
		this.creatdBy = creatdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
