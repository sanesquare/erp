/**
 * 
 */
package com.hrms.web.vo;

/**
 * @author LogOn
 *
 */
public class AchievementsVo {

	private String achievementsTitle;

	/**
	 * @return the achievementsTitle
	 */
	public String getAchievementsTitle() {
		return achievementsTitle;
	}

	/**
	 * @param achievementsTitle the achievementsTitle to set
	 */
	public void setAchievementsTitle(String achievementsTitle) {
		this.achievementsTitle = achievementsTitle;
	}
}
