package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public class ReimbursementCategoryVo {

	private Long reimbursementCategoryId;

	private String reimbursementCategory;

	private String tempReimbursementCategory;

	/**
	 * 
	 * @return
	 */
	public String getTempReimbursementCategory() {
		return tempReimbursementCategory;
	}

	/**
	 * 
	 * @param tempReimbursementCategory
	 */
	public void setTempReimbursementCategory(String tempReimbursementCategory) {
		this.tempReimbursementCategory = tempReimbursementCategory;
	}

	/**
	 * 
	 * @return
	 */
	public Long getReimbursementCategoryId() {
		return reimbursementCategoryId;
	}

	/**
	 * 
	 * @param reimbursementCategoryId
	 */
	public void setReimbursementCategoryId(Long reimbursementCategoryId) {
		this.reimbursementCategoryId = reimbursementCategoryId;
	}

	/**
	 * 
	 * @return
	 */
	public String getReimbursementCategory() {
		return reimbursementCategory;
	}

	/**
	 * 
	 * @param reimbursementCategory
	 */
	public void setReimbursementCategory(String reimbursementCategory) {
		this.reimbursementCategory = reimbursementCategory;
	}

}
