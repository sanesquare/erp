package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 23-May-2015
 *
 */
public class IndividualEvaluationVo {

	private Long individualId;
	
	private String employee;
	
	private String employeeCode;
	
	private String branch;
	
	private String designation;
	
	private String maxMark;
	
	private String score;
	
	private Long performanceId;

	/**
	 * @return the individualId
	 */
	public Long getIndividualId() {
		return individualId;
	}

	/**
	 * @param individualId the individualId to set
	 */
	public void setIndividualId(Long individualId) {
		this.individualId = individualId;
	}

	/**
	 * @return the employee
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * @return the employeeCode
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * @param employeeCode the employeeCode to set
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the maxMark
	 */
	public String getMaxMark() {
		return maxMark;
	}

	/**
	 * @param maxMark the maxMark to set
	 */
	public void setMaxMark(String maxMark) {
		this.maxMark = maxMark;
	}

	/**
	 * @return the score
	 */
	public String getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(String score) {
		this.score = score;
	}

	/**
	 * @return the performanceId
	 */
	public Long getPerformanceId() {
		return performanceId;
	}

	/**
	 * @param performanceId the performanceId to set
	 */
	public void setPerformanceId(Long performanceId) {
		this.performanceId = performanceId;
	}
	
	
}
