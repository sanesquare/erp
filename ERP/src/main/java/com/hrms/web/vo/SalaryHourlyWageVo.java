package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 22-April-2015
 */
public class SalaryHourlyWageVo {

	private String employeeCode;

	private String employee;

	private String withEffectFrom;

	private String regularHourSalary;

	private String overTimerHourSalary;

	private Double finalRegularHourSalary;

	private Double finalOverTimerHourSalary;

	private String tax;

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getWithEffectFrom() {
		return withEffectFrom;
	}

	public void setWithEffectFrom(String withEffectFrom) {
		this.withEffectFrom = withEffectFrom;
	}

	public String getRegularHourSalary() {
		return regularHourSalary;
	}

	public void setRegularHourSalary(String regularHourSalary) {
		this.regularHourSalary = regularHourSalary;
	}

	public String getOverTimerHourSalary() {
		return overTimerHourSalary;
	}

	public void setOverTimerHourSalary(String overTimerHourSalary) {
		this.overTimerHourSalary = overTimerHourSalary;
	}

	public Double getFinalRegularHourSalary() {
		return finalRegularHourSalary;
	}

	public void setFinalRegularHourSalary(Double finalRegularHourSalary) {
		this.finalRegularHourSalary = finalRegularHourSalary;
	}

	public Double getFinalOverTimerHourSalary() {
		return finalOverTimerHourSalary;
	}

	public void setFinalOverTimerHourSalary(Double finalOverTimerHourSalary) {
		this.finalOverTimerHourSalary = finalOverTimerHourSalary;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}
}
