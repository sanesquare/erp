package com.hrms.web.vo;

import java.math.BigDecimal;
/**
 * 
 * @author Vinutha
 * @since 10-September-2015
 *
 */
public class BankStatementDetails {

	private Integer srNo;
	
	private String employeeCode;
	
	private String employee;
	
	private String account;
	
	private BigDecimal amount = new BigDecimal(0.00);
	
	private String branch;

	public Integer getSrNo() {
		return srNo;
	}

	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	
}
