package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public class TrainingTypeVo {

	private Long trainingTypeId;

	private String trainingType;

	/**
	 * 
	 * @return
	 */
	public Long getTrainingTypeId() {
		return trainingTypeId;
	}

	/**
	 * 
	 * @param trainingTypeId
	 */
	public void setTrainingTypeId(Long trainingTypeId) {
		this.trainingTypeId = trainingTypeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getTrainingType() {
		return trainingType;
	}

	/**
	 * 
	 * @param trainingType
	 */
	public void setTrainingType(String trainingType) {
		this.trainingType = trainingType;
	}

}
