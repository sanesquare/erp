package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-May-2015
 *
 */
public class ProfesionalTaxVo {

	private Long ptId;
	private Double amount;

	/**
	 * 
	 * @return
	 */
	public Long getPtId() {
		return ptId;
	}

	/**
	 * 
	 * @param ptId
	 */
	public void setPtId(Long ptId) {
		this.ptId = ptId;
	}

	/**
	 * 
	 * @return
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
