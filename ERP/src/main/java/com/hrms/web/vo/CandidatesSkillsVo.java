package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 23-March-2015
 *
 */
public class CandidatesSkillsVo {

	private Long candSkillId;
	
	private String skill;
	
	private String skillLevel;

	/**
	 * @return the candSkillId
	 */
	public Long getCandSkillId() {
		return candSkillId;
	}

	/**
	 * @param candSkillId the candSkillId to set
	 */
	public void setCandSkillId(Long candSkillId) {
		this.candSkillId = candSkillId;
	}

	/**
	 * @return the skill
	 */
	public String getSkill() {
		return skill;
	}

	/**
	 * @param skill the skill to set
	 */
	public void setSkill(String skill) {
		this.skill = skill;
	}

	/**
	 * @return the skillLevel
	 */
	public String getSkillLevel() {
		return skillLevel;
	}

	/**
	 * @param skillLevel the skillLevel to set
	 */
	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}
	
	
}
