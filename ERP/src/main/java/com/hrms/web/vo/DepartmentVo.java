package com.hrms.web.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.DepartmentDocuments;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ProjectDocuments;

/**
 * 
 * @author Sangeeth and Bibin
 *
 */
public class DepartmentVo {

	private Long id;

	private String name;

	private Branch branch;

	private Long branchId;

	private String branchName;

	private Employee employee;

	private List<Long> employeeId;
	
	private String departmentHeadName;
	
	private Long departmentHeadId;

	private List<String> employeeName;

	private Department parentDepartment;

	private Long parentDepartmentId;

	private String parentDepartmentName;	

	private Set<Department> subDepartments = new HashSet<Department>();

	private String documentAccessUrl;

	private String documentName;

	private Integer sortingOrder;

	private String additionalInformation;

	private Date createdOn;

	private String createdBy;

	private Date updatedOn;

	private String updatedBy;

	private Set<DepartmentDocuments> departmentDocuments = new HashSet<DepartmentDocuments>();

	private List<MultipartFile> department_file_upload;
	private List<DepartmentDocumentsVo> departmentDocument;
	

	public List<MultipartFile> getDepartment_file_upload() {
		return department_file_upload;
	}
	
	public List<DepartmentDocumentsVo> getDepartmentDocument() {
		return departmentDocument;
	}



	public void setDepartmentDocument(List<DepartmentDocumentsVo> departmentDocument) {
		this.departmentDocument = departmentDocument;
	}



	public void setDepartment_file_upload(List<MultipartFile> department_file_upload) {
		this.department_file_upload = department_file_upload;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	public String getDepartmentHeadName() {
		return departmentHeadName;
	}

	public void setDepartmentHeadName(String departmentHeadName) {
		this.departmentHeadName = departmentHeadName;
	}

	public Long getDepartmentHeadId() {
		return departmentHeadId;
	}

	public void setDepartmentHeadId(Long departmentHeadId) {
		this.departmentHeadId = departmentHeadId;
	}

	public String getName() {
		return name;
	}

	public Long getParentDepartmentId() {
		return parentDepartmentId;
	}

	public void setParentDepartmentId(Long parentDepartmentId) {
		this.parentDepartmentId = parentDepartmentId;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	

	public String getParentDepartmentName() {
		return parentDepartmentName;
	}

	public void setParentDepartmentName(String parentDepartmentName) {
		this.parentDepartmentName = parentDepartmentName;
	}

	public List<Long> getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(List<Long> employeeId) {
		this.employeeId = employeeId;
	}

	public List<String> getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(List<String> employeeName) {
		this.employeeName = employeeName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Department getParentDepartment() {
		return parentDepartment;
	}

	public void setParentDepartment(Department parentDepartment) {
		this.parentDepartment = parentDepartment;
	}

	public Set<Department> getSubDepartments() {
		return subDepartments;
	}

	public void setSubDepartments(Set<Department> subDepartments) {
		this.subDepartments = subDepartments;
	}

	public Integer getSortingOrder() {
		return sortingOrder;
	}

	public void setSortingOrder(Integer sortingOrder) {
		this.sortingOrder = sortingOrder;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public String getDocumentAccessUrl() {
		return documentAccessUrl;
	}

	public void setDocumentAccessUrl(String documentAccessUrl) {
		this.documentAccessUrl = documentAccessUrl;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Set<DepartmentDocuments> getDepartmentDocuments() {
		return departmentDocuments;
	}

	public void setDepartmentDocuments(Set<DepartmentDocuments> departmentDocuments) {
		this.departmentDocuments = departmentDocuments;
	}
	

}
