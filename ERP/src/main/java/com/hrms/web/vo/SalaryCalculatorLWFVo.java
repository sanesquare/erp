package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer
 * @since 05-May-2015
 */
public class SalaryCalculatorLWFVo {

	private BigDecimal lwfEmployeeMonthly;

	private BigDecimal lwfEmployeeAnnual;

	private BigDecimal lwfEmployerMonthly;

	private BigDecimal lwfEmployerAnnual;

	public BigDecimal getLwfEmployeeMonthly() {
		return lwfEmployeeMonthly;
	}

	public void setLwfEmployeeMonthly(BigDecimal lwfEmployeeMonthly) {
		this.lwfEmployeeMonthly = lwfEmployeeMonthly;
	}

	public BigDecimal getLwfEmployeeAnnual() {
		return lwfEmployeeAnnual;
	}

	public void setLwfEmployeeAnnual(BigDecimal lwfEmployeeAnnual) {
		this.lwfEmployeeAnnual = lwfEmployeeAnnual;
	}

	public BigDecimal getLwfEmployerMonthly() {
		return lwfEmployerMonthly;
	}

	public void setLwfEmployerMonthly(BigDecimal lwfEmployerMonthly) {
		this.lwfEmployerMonthly = lwfEmployerMonthly;
	}

	public BigDecimal getLwfEmployerAnnual() {
		return lwfEmployerAnnual;
	}

	public void setLwfEmployerAnnual(BigDecimal lwfEmployerAnnual) {
		this.lwfEmployerAnnual = lwfEmployerAnnual;
	}
}
