package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author shamsheer
 * @since 21-March-2015
 */
public class EmployeeSuperiorsSubordinatesVo {

	private List<Long> subordinatesIds;
	
	private List<Long> superiorsIds;
	 
	private Long superiorId;
	
	private Long subordinateId;
	
	private Long employeeId;

	/**
	 * @return the subordinatesIds
	 */
	public List<Long> getSubordinatesIds() {
		return subordinatesIds;
	}

	/**
	 * @param subordinatesIds the subordinatesIds to set
	 */
	public void setSubordinatesIds(List<Long> subordinatesIds) {
		this.subordinatesIds = subordinatesIds;
	}

	/**
	 * @return the superiorsIds
	 */
	public List<Long> getSuperiorsIds() {
		return superiorsIds;
	}

	/**
	 * @param superiorsIds the superiorsIds to set
	 */
	public void setSuperiorsIds(List<Long> superiorsIds) {
		this.superiorsIds = superiorsIds;
	}

	/**
	 * @return the employeeId
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the superiorId
	 */
	public Long getSuperiorId() {
		return superiorId;
	}

	/**
	 * @param superiorId the superiorId to set
	 */
	public void setSuperiorId(Long superiorId) {
		this.superiorId = superiorId;
	}

	/**
	 * @return the subordinateId
	 */
	public Long getSubordinateId() {
		return subordinateId;
	}

	/**
	 * @param subordinateId the subordinateId to set
	 */
	public void setSubordinateId(Long subordinateId) {
		this.subordinateId = subordinateId;
	}
	
	
}
