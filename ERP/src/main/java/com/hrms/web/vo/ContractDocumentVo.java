package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Shamsheer
 * @since 25-March-2015
 */
public class ContractDocumentVo {

	private String fileName;
	
	private String url;
	
	private Long id;
	
	private Long contractId;
	
	private List<MultipartFile> cntrctFileUpload;

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the contractId
	 */
	public Long getContractId() {
		return contractId;
	}

	/**
	 * @param contractId the contractId to set
	 */
	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	/**
	 * @return the cntrctFileUpload
	 */
	public List<MultipartFile> getCntrctFileUpload() {
		return cntrctFileUpload;
	}

	/**
	 * @param cntrctFileUpload the cntrctFileUpload to set
	 */
	public void setCntrctFileUpload(List<MultipartFile> cntrctFileUpload) {
		this.cntrctFileUpload = cntrctFileUpload;
	}
	
}
