package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 18-April-2015
 *  
 *
 */
public class HrLeavesVo {

	private int year;
	
	private Long branchId;
	
	private Long depId;
	
	private Long empId;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public Long getDepId() {
		return depId;
	}

	public void setDepId(Long depId) {
		this.depId = depId;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	
	
}
