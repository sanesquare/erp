package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public class SkillVo {

	private Long skillId;

	private String skillName;

	private String tempSkill;

	/**
	 * 
	 * @return
	 */
	public String getTempSkill() {
		return tempSkill;
	}

	/**
	 * 
	 * @param tempSkill
	 */
	public void setTempSkill(String tempSkill) {
		this.tempSkill = tempSkill;
	}

	/**
	 * 
	 * @return
	 */
	public Long getSkillId() {
		return skillId;
	}

	/**
	 * 
	 * @param skillId
	 */
	public void setSkillId(Long skillId) {
		this.skillId = skillId;
	}

	/**
	 * 
	 * @return
	 */
	public String getSkillName() {
		return skillName;
	}

	/**
	 * 
	 * @param skillName
	 */
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

}
