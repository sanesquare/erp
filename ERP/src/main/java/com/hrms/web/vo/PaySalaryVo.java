package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 08-June-2015
 *
 */
public class PaySalaryVo {

	private Long companyId;
	
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	private Long branchId;
	
	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	private Long id;
	
	private String status;
	
	private Long statusId;
	
	private List<PaySalaryEmployeeItemsVo> employeeItemsVo = new ArrayList<PaySalaryEmployeeItemsVo>();
	
	private List<PaySalaryDailySalaryVo> dailyWageDetailsVos = new ArrayList<PaySalaryDailySalaryVo>();
	
	private List<PaySalaryHourlySalaryVo> hourlyWagesDetailsVos = new ArrayList<PaySalaryHourlySalaryVo>();
	
	private Boolean monthly = false;
	
	private Boolean daily = false;
	
	private Boolean hourly = false;
	
	private Boolean edit = false;
	
	private List<Long> branchIds = new ArrayList<Long>();
	
	private List<Long> departmentIds = new ArrayList<Long>();
	
	private List<Long> employeeIds;
	
	private List<String> branches = new ArrayList<String>();
	
	private List<String> departments = new ArrayList<String>();
	
	private String salaryTypeType;
	
	private Long salaryType;
	
	private String date;
	
	private String dailyWageDate;
	
	private String hourlyWageDate;

	private List<String> extraPayslipItems = new ArrayList<String>();
	
	private List<String> paySlipItems = new ArrayList<String>();
	
	public Boolean getMonthly() {
		return monthly;
	}

	public void setMonthly(Boolean monthly) {
		this.monthly = monthly;
	}

	public Boolean getDaily() {
		return daily;
	}

	public void setDaily(Boolean daily) {
		this.daily = daily;
	}

	public Boolean getHourly() {
		return hourly;
	}

	public void setHourly(Boolean hourly) {
		this.hourly = hourly;
	}

	public List<Long> getBranchIds() {
		return branchIds;
	}

	public void setBranchIds(List<Long> branchIds) {
		this.branchIds = branchIds;
	}

	public List<Long> getDepartmentIds() {
		return departmentIds;
	}

	public void setDepartmentIds(List<Long> departmentIds) {
		this.departmentIds = departmentIds;
	}

	public List<Long> getEmployeeIds() {
		return employeeIds;
	}

	public void setEmployeeIds(List<Long> employeeIds) {
		this.employeeIds = employeeIds;
	}

	public Long getSalaryType() {
		return salaryType;
	}

	public void setSalaryType(Long salaryType) {
		this.salaryType = salaryType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<PaySalaryEmployeeItemsVo> getEmployeeItemsVo() {
		return employeeItemsVo;
	}

	public void setEmployeeItemsVo(List<PaySalaryEmployeeItemsVo> employeeItemsVo) {
		this.employeeItemsVo = employeeItemsVo;
	}

	public List<String> getExtraPayslipItems() {
		return extraPayslipItems;
	}

	public void setExtraPayslipItems(List<String> extraPayslipItems) {
		this.extraPayslipItems = extraPayslipItems;
	}

	public List<PaySalaryDailySalaryVo> getDailyWageDetailsVos() {
		return dailyWageDetailsVos;
	}

	public void setDailyWageDetailsVos(List<PaySalaryDailySalaryVo> dailyWageDetailsVos) {
		this.dailyWageDetailsVos = dailyWageDetailsVos;
	}

	public String getDailyWageDate() {
		return dailyWageDate;
	}

	public void setDailyWageDate(String dailyWageDate) {
		this.dailyWageDate = dailyWageDate;
	}

	public List<PaySalaryHourlySalaryVo> getHourlyWagesDetailsVos() {
		return hourlyWagesDetailsVos;
	}

	public void setHourlyWagesDetailsVos(List<PaySalaryHourlySalaryVo> hourlyWagesDetailsVos) {
		this.hourlyWagesDetailsVos = hourlyWagesDetailsVos;
	}

	public String getHourlyWageDate() {
		return hourlyWageDate;
	}

	public void setHourlyWageDate(String hourlyWageDate) {
		this.hourlyWageDate = hourlyWageDate;
	}

	public List<String> getBranches() {
		return branches;
	}

	public void setBranches(List<String> branches) {
		this.branches = branches;
	}

	public List<String> getDepartments() {
		return departments;
	}

	public void setDepartments(List<String> departments) {
		this.departments = departments;
	}

	public String getSalaryTypeType() {
		return salaryTypeType;
	}

	public void setSalaryTypeType(String salaryTypeType) {
		this.salaryTypeType = salaryTypeType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getEdit() {
		return edit;
	}

	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public List<String> getPaySlipItems() {
		return paySlipItems;
	}

	public void setPaySlipItems(List<String> paySlipItems) {
		this.paySlipItems = paySlipItems;
	}

}
