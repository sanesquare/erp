package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer
 * @since 05-May-2015
 *
 */
public class SalaryCalculatorEsiVo {

	private BigDecimal esiEmployeeMonthly = new BigDecimal(0);

	private BigDecimal esiEmployeeAnnual = new BigDecimal(0);;

	private BigDecimal esiEmployerMonthly = new BigDecimal(0);;

	private BigDecimal esiEmployerAnnual = new BigDecimal(0);;

	public BigDecimal getEsiEmployeeMonthly() {
		return esiEmployeeMonthly;
	}

	public void setEsiEmployeeMonthly(BigDecimal esiEmployeeMonthly) {
		this.esiEmployeeMonthly = esiEmployeeMonthly;
	}

	public BigDecimal getEsiEmployeeAnnual() {
		return esiEmployeeAnnual;
	}

	public void setEsiEmployeeAnnual(BigDecimal esiEmployeeAnnual) {
		this.esiEmployeeAnnual = esiEmployeeAnnual;
	}

	public BigDecimal getEsiEmployerMonthly() {
		return esiEmployerMonthly;
	}

	public void setEsiEmployerMonthly(BigDecimal esiEmployerMonthly) {
		this.esiEmployerMonthly = esiEmployerMonthly;
	}

	public BigDecimal getEsiEmployerAnnual() {
		return esiEmployerAnnual;
	}

	public void setEsiEmployerAnnual(BigDecimal esiEmployerAnnual) {
		this.esiEmployerAnnual = esiEmployerAnnual;
	}
}
