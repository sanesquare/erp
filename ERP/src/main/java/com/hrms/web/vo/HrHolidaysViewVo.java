package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 18-April-2015
 *
 */
public class HrHolidaysViewVo {

	private List<HrHolidaysDetailsVo> detailsVo;
	
	private String type;

	/**
	 * @return the detailsVo
	 */
	public List<HrHolidaysDetailsVo> getDetailsVo() {
		return detailsVo;
	}

	/**
	 * @param detailsVo the detailsVo to set
	 */
	public void setDetailsVo(List<HrHolidaysDetailsVo> detailsVo) {
		this.detailsVo = detailsVo;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
