package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 28-April-2015
 */
public class PayslipItemAjaxVo {

	private Long id;
	
	private String name;
	
	private String query;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
}
