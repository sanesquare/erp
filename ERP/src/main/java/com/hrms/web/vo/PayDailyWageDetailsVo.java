package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayDailyWageDetailsVo {

	private int slno;
	
	private Date date;
	
	private String empName;
	
	private int days;
	
	private BigDecimal dailyRate;
	
	private BigDecimal amount;
	
	private String dateString;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the days
	 */
	public int getDays() {
		return days;
	}

	/**
	 * @param days the days to set
	 */
	public void setDays(int days) {
		this.days = days;
	}

	/**
	 * @return the dailyRate
	 */
	public BigDecimal getDailyRate() {
		return dailyRate;
	}

	/**
	 * @param dailyRate the dailyRate to set
	 */
	public void setDailyRate(BigDecimal dailyRate) {
		this.dailyRate = dailyRate;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the dateString
	 */
	public String getDateString() {
		return dateString;
	}

	/**
	 * @param dateString the dateString to set
	 */
	public void setDateString(String dateString) {
		this.dateString = dateString;
	}
	
	
}
