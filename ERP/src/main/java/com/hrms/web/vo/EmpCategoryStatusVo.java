package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 16-April-2015
 */
public class EmpCategoryStatusVo {

	private String category;
	
	private int number;

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}
	
}
