package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
public class TravelDestinationVo {

	private Long travelId;
	
	private Long travelModeId;
	
	private Long arrangementTypeId;
	
	private String place;
	
	private String travelMode;
	
	private String arrangementType;

	public Long getTravelId() {
		return travelId;
	}

	public void setTravelId(Long travelId) {
		this.travelId = travelId;
	}

	public Long getTravelModeId() {
		return travelModeId;
	}

	public void setTravelModeId(Long travelModeId) {
		this.travelModeId = travelModeId;
	}

	public Long getArrangementTypeId() {
		return arrangementTypeId;
	}

	public void setArrangementTypeId(Long arrangementTypeId) {
		this.arrangementTypeId = arrangementTypeId;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getTravelMode() {
		return travelMode;
	}

	public void setTravelMode(String travelMode) {
		this.travelMode = travelMode;
	}

	public String getArrangementType() {
		return arrangementType;
	}

	public void setArrangementType(String arrangementType) {
		this.arrangementType = arrangementType;
	}
}
