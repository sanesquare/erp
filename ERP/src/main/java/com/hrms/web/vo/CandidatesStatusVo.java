package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 24-March-2015
 *
 */
public class CandidatesStatusVo {

	private Long statusId;
	
	private String status;

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
