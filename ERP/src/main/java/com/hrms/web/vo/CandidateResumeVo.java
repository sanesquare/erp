package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * 
 * @since 28-March-2015
 */
public class CandidateResumeVo {

	private String resumeUrl;
	
	private Long resumeId;
	
	private String resumeFileName;

	/**
	 * @return the resumeUrl
	 */
	public String getResumeUrl() {
		return resumeUrl;
	}

	/**
	 * @param resumeUrl the resumeUrl to set
	 */
	public void setResumeUrl(String resumeUrl) {
		this.resumeUrl = resumeUrl;
	}

	/**
	 * @return the resumeId
	 */
	public Long getResumeId() {
		return resumeId;
	}

	/**
	 * @param resumeId the resumeId to set
	 */
	public void setResumeId(Long resumeId) {
		this.resumeId = resumeId;
	}

	/**
	 * @return the resumeFileName
	 */
	public String getResumeFileName() {
		return resumeFileName;
	}

	/**
	 * @param resumeFileName the resumeFileName to set
	 */
	public void setResumeFileName(String resumeFileName) {
		this.resumeFileName = resumeFileName;
	}
	
	
}
