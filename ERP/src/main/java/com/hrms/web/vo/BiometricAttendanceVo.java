package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.entities.Attendance;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer
 * @since 19-June-2015
 */
public class BiometricAttendanceVo {

	private List<MultipartFile> attendanceReports;
	
	private String employeeCode;
	
	private String department;
	
	private String date;
	
	private List<String> time;

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<String> getTime() {
		return time;
	}

	public void setTime(List<String> time) {
		this.time = time;
	}

	public List<MultipartFile> getAttendanceReports() {
		return attendanceReports;
	}

	public void setAttendanceReports(List<MultipartFile> attendanceReports) {
		this.attendanceReports = attendanceReports;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Attendance){
			BiometricAttendanceVo attendance = (BiometricAttendanceVo) obj;
			return (attendance.employeeCode.equals(this.employeeCode)&&attendance.time.equals(this.time)&&attendance.date.equals(this.date)&&attendance.department.equals(this.department));
		}else
		return false;
	}
	
}
