package com.hrms.web.vo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.entities.DepartmentDocuments;
import com.hrms.web.entities.PromotionDocuments;


public class PromotionVo {
	
	private Long id;
	private Long employeeId;
	private Long designationId;
	private String employeeCode;
	private String designationName;
	private Long superiorId;
	private String superiorCode;
	private String promotionDescription;
	private String promotionNote;
	private String promotionDate;
	
	private String documentAccessUrl;
	private String documentName;
	private Set<PromotionDocuments> promotionDocuments = new HashSet<PromotionDocuments>();
	
	private List<MultipartFile> promotion_file_upload;
	private List<PromotionDocumentVo> promotionDocument;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDesignationId() {
		return designationId;
	}
	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	
	public String getSuperiorCode() {
		return superiorCode;
	}
	public void setSuperiorCode(String superiorCode) {
		this.superiorCode = superiorCode;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public Long getSuperiorId() {
		return superiorId;
	}
	public void setSuperiorId(Long superiorId) {
		this.superiorId = superiorId;
	}
	public String getPromotionDescription() {
		return promotionDescription;
	}
	public void setPromotionDescription(String promotionDescription) {
		this.promotionDescription = promotionDescription;
	}
	public String getPromotionNote() {
		return promotionNote;
	}
	public void setPromotionNote(String promotionNote) {
		this.promotionNote = promotionNote;
	}
	public String getPromotionDate() {
		return promotionDate;
	}
	public void setPromotionDate(String promotionDate) {
		this.promotionDate = promotionDate;
	}
	public Set<PromotionDocuments> getPromotionDocuments() {
		return promotionDocuments;
	}
	public void setPromotionDocuments(Set<PromotionDocuments> promotionDocuments) {
		this.promotionDocuments = promotionDocuments;
	}
	
	public String getDocumentAccessUrl() {
		return documentAccessUrl;
	}
	public void setDocumentAccessUrl(String documentAccessUrl) {
		this.documentAccessUrl = documentAccessUrl;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public List<MultipartFile> getPromotion_file_upload() {
		return promotion_file_upload;
	}
	public void setPromotion_file_upload(List<MultipartFile> promotion_file_upload) {
		this.promotion_file_upload = promotion_file_upload;
	}
	public List<PromotionDocumentVo> getPromotionDocument() {
		return promotionDocument;
	}
	public void setPromotionDocument(List<PromotionDocumentVo> promotionDocument) {
		this.promotionDocument = promotionDocument;
	}
	

}
