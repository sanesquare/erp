package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 23-March-2015
 *
 */
public class CandidatesExperienceVo {
	private Long candExpId;
	
	private String orgName;
	
	private String designation;
	
	private String jobField;

	/**
	 * @return the candExpId
	 */
	public Long getCandExpId() {
		return candExpId;
	}

	/**
	 * @param candExpId the candExpId to set
	 */
	public void setCandExpId(Long candExpId) {
		this.candExpId = candExpId;
	}

	/**
	 * @return the orgName
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * @param orgName the orgName to set
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the jobField
	 */
	public String getJobField() {
		return jobField;
	}

	/**
	 * @param jobField the jobField to set
	 */
	public void setJobField(String jobField) {
		this.jobField = jobField;
	}
	
	
}
