package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 23-March-2015
 *
 */
public class CandidatesLanguageVo {
	
	private Long candLangId;
	
	private String language;
	
	private String speakLevel;
	
	private String writeLevel;
	
	private String readLevel;

	/**
	 * @return the candLangId
	 */
	public Long getCandLangId() {
		return candLangId;
	}

	/**
	 * @param candLangId the candLangId to set
	 */
	public void setCandLangId(Long candLangId) {
		this.candLangId = candLangId;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the speakLevel
	 */
	public String getSpeakLevel() {
		return speakLevel;
	}

	/**
	 * @param speakLevel the speakLevel to set
	 */
	public void setSpeakLevel(String speakLevel) {
		this.speakLevel = speakLevel;
	}

	/**
	 * @return the writeLevel
	 */
	public String getWriteLevel() {
		return writeLevel;
	}

	/**
	 * @param writeLevel the writeLevel to set
	 */
	public void setWriteLevel(String writeLevel) {
		this.writeLevel = writeLevel;
	}

	/**
	 * @return the readLevel
	 */
	public String getReadLevel() {
		return readLevel;
	}

	/**
	 * @param readLevel the readLevel to set
	 */
	public void setReadLevel(String readLevel) {
		this.readLevel = readLevel;
	}
	
	
}
