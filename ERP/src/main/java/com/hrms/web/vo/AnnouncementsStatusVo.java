package com.hrms.web.vo;

/**
 * 
 * @author Shimil Babu
 * @since 10-March-2015
 */
public class AnnouncementsStatusVo {

	private String announcementsStatus;
	
	private Long announcementsStatusId;
	
	private String announcementsStatusNotes;
	
	private String createdOn;
	
	private String updatedOn;
		
	private String createdBy;
		
	private String updatedBy;

	/**
	 * @return the announcementsStatus
	 */
	public String getAnnouncementsStatus() {
		return announcementsStatus;
	}

	/**
	 * @param announcementsStatus the announcementsStatus to set
	 */
	public void setAnnouncementsStatus(String announcementsStatus) {
		this.announcementsStatus = announcementsStatus;
	}

	/**
	 * @return the announcementsStatusId
	 */
	public Long getAnnouncementsStatusId() {
		return announcementsStatusId;
	}

	/**
	 * @param announcementsStatusId the announcementsStatusId to set
	 */
	public void setAnnouncementsStatusId(Long announcementsStatusId) {
		this.announcementsStatusId = announcementsStatusId;
	}

	/**
	 * @return the announcementsStatusNotes
	 */
	public String getAnnouncementsStatusNotes() {
		return announcementsStatusNotes;
	}

	/**
	 * @param announcementsStatusNotes the announcementsStatusNotes to set
	 */
	public void setAnnouncementsStatusNotes(String announcementsStatusNotes) {
		this.announcementsStatusNotes = announcementsStatusNotes;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public String getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
}
