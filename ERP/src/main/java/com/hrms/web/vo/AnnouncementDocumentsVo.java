package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
public class AnnouncementDocumentsVo {

	private Long annr_doc_id;

	private String documentName;

	private String documentUrl;

	/**
	 * 
	 * @return
	 */
	public Long getAnnr_doc_id() {
		return annr_doc_id;
	}

	/**
	 * 
	 * @param annr_doc_id
	 */
	public void setAnnr_doc_id(Long annr_doc_id) {
		this.annr_doc_id = annr_doc_id;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * 
	 * @param documentName
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentUrl() {
		return documentUrl;
	}

	/**
	 * 
	 * @param documentUrl
	 */
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

}
