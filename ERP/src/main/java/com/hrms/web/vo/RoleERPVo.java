package com.hrms.web.vo;

import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 19, 2015
 */
public class RoleERPVo {

	private String title;

	private List<RoleTypesVo> list;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the list
	 */
	public List<RoleTypesVo> getList() {
		return list;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(List<RoleTypesVo> list) {
		this.list = list;
	}
}
