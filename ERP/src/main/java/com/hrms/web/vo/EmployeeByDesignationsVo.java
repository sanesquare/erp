package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 16-April-2015
 *
 */
public class EmployeeByDesignationsVo {

	private String designation;
	
	private int designationEmp;

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the designationEmp
	 */
	public int getDesignationEmp() {
		return designationEmp;
	}

	/**
	 * @param designationEmp the designationEmp to set
	 */
	public void setDesignationEmp(int designationEmp) {
		this.designationEmp = designationEmp;
	}
	
	
}
