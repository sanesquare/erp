package com.hrms.web.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpJoiningReportDetailsVo {

	private int slno;
	
	private String empName;
	
	@DateTimeFormat
	private Date joiningDate;
	
	private String joinDepartment;
	
	private String joinBranch;
	
	private String currentDepartment;
	
	private String currentBranch;

	private String joiningDateString;
	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the joiningDate
	 */
	public Date getJoiningDate() {
		return joiningDate;
	}

	/**
	 * @param joiningDate the joiningDate to set
	 */
	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}

	/**
	 * @return the joinDepartment
	 */
	public String getJoinDepartment() {
		return joinDepartment;
	}

	/**
	 * @param joinDepartment the joinDepartment to set
	 */
	public void setJoinDepartment(String joinDepartment) {
		this.joinDepartment = joinDepartment;
	}

	/**
	 * @return the joinBranch
	 */
	public String getJoinBranch() {
		return joinBranch;
	}

	/**
	 * @param joinBranch the joinBranch to set
	 */
	public void setJoinBranch(String joinBranch) {
		this.joinBranch = joinBranch;
	}

	/**
	 * @return the currentDepartment
	 */
	public String getCurrentDepartment() {
		return currentDepartment;
	}

	/**
	 * @param currentDepartment the currentDepartment to set
	 */
	public void setCurrentDepartment(String currentDepartment) {
		this.currentDepartment = currentDepartment;
	}

	/**
	 * @return the currentBranch
	 */
	public String getCurrentBranch() {
		return currentBranch;
	}

	/**
	 * @param currentBranch the currentBranch to set
	 */
	public void setCurrentBranch(String currentBranch) {
		this.currentBranch = currentBranch;
	}

	/**
	 * @return the joiningDateString
	 */
	public String getJoiningDateString() {
		return joiningDateString;
	}

	/**
	 * @param joiningDateString the joiningDateString to set
	 */
	public void setJoiningDateString(String joiningDateString) {
		this.joiningDateString = joiningDateString;
	}
	
	
}
