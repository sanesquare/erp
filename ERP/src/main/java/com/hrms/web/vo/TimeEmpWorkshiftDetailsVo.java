package com.hrms.web.vo;

import java.sql.Time;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 8-May-2015
 *
 */
public class TimeEmpWorkshiftDetailsVo {

	private String day;
	
	private Time regIn;
	
	private Time regOut;
	
	private Time refrIn;
	
	private Time refrOut;
	
	private Time addIn;
	
	private Time addOut;

	private String regInString;
	
	private String regOutString;
	
	private String refrInString;
	
	private String refrOutString;
	
	private String addInString;
	
	private String addOuString;
	/**
	 * @return the day
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return the regIn
	 */
	public Time getRegIn() {
		return regIn;
	}

	/**
	 * @param regIn the regIn to set
	 */
	public void setRegIn(Time regIn) {
		this.regIn = regIn;
	}

	/**
	 * @return the regOut
	 */
	public Time getRegOut() {
		return regOut;
	}

	/**
	 * @param regOut the regOut to set
	 */
	public void setRegOut(Time regOut) {
		this.regOut = regOut;
	}

	/**
	 * @return the refrIn
	 */
	public Time getRefrIn() {
		return refrIn;
	}

	/**
	 * @param refrIn the refrIn to set
	 */
	public void setRefrIn(Time refrIn) {
		this.refrIn = refrIn;
	}

	/**
	 * @return the refrOut
	 */
	public Time getRefrOut() {
		return refrOut;
	}

	/**
	 * @param refrOut the refrOut to set
	 */
	public void setRefrOut(Time refrOut) {
		this.refrOut = refrOut;
	}

	/**
	 * @return the addIn
	 */
	public Time getAddIn() {
		return addIn;
	}

	/**
	 * @param addIn the addIn to set
	 */
	public void setAddIn(Time addIn) {
		this.addIn = addIn;
	}

	/**
	 * @return the addOut
	 */
	public Time getAddOut() {
		return addOut;
	}

	/**
	 * @param addOut the addOut to set
	 */
	public void setAddOut(Time addOut) {
		this.addOut = addOut;
	}

	/**
	 * @return the regInString
	 */
	public String getRegInString() {
		return regInString;
	}

	/**
	 * @param regInString the regInString to set
	 */
	public void setRegInString(String regInString) {
		this.regInString = regInString;
	}

	/**
	 * @return the regOutString
	 */
	public String getRegOutString() {
		return regOutString;
	}

	/**
	 * @param regOutString the regOutString to set
	 */
	public void setRegOutString(String regOutString) {
		this.regOutString = regOutString;
	}

	/**
	 * @return the refrInString
	 */
	public String getRefrInString() {
		return refrInString;
	}

	/**
	 * @param refrInString the refrInString to set
	 */
	public void setRefrInString(String refrInString) {
		this.refrInString = refrInString;
	}

	/**
	 * @return the refrOutString
	 */
	public String getRefrOutString() {
		return refrOutString;
	}

	/**
	 * @param refrOutString the refrOutString to set
	 */
	public void setRefrOutString(String refrOutString) {
		this.refrOutString = refrOutString;
	}

	/**
	 * @return the addInString
	 */
	public String getAddInString() {
		return addInString;
	}

	/**
	 * @param addInString the addInString to set
	 */
	public void setAddInString(String addInString) {
		this.addInString = addInString;
	}

	/**
	 * @return the addOuString
	 */
	public String getAddOuString() {
		return addOuString;
	}

	/**
	 * @param addOuString the addOuString to set
	 */
	public void setAddOuString(String addOuString) {
		this.addOuString = addOuString;
	}
	
}
