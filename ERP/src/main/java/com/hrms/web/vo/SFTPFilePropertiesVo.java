package com.hrms.web.vo;


import org.springframework.web.multipart.MultipartFile;

public class SFTPFilePropertiesVo {

	private MultipartFile file;
	
	private String fileName;
	
	private String moduleName;
	
	private String userName;

	private String sftpRootPath;
	private String sftpUser;
	private String sftpIP;
	private int sftpPort;
	private String sftpKey;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getSftpRootPath() {
		return sftpRootPath;
	}

	public void setSftpRootPath(String sftpRootPath) {
		this.sftpRootPath = sftpRootPath;
	}

	public String getSftpUser() {
		return sftpUser;
	}

	public void setSftpUser(String sftpUser) {
		this.sftpUser = sftpUser;
	}

	public String getSftpIP() {
		return sftpIP;
	}

	public void setSftpIP(String sftpIP) {
		this.sftpIP = sftpIP;
	}

	public int getSftpPort() {
		return sftpPort;
	}

	public void setSftpPort(int sftpPort) {
		this.sftpPort = sftpPort;
	}

	public String getSftpKey() {
		return sftpKey;
	}

	public void setSftpKey(String sftpKey) {
		this.sftpKey = sftpKey;
	}

}
