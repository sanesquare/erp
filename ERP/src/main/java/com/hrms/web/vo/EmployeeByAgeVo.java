package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 16-April-2015
 *
 */
public class EmployeeByAgeVo {

	private String ageGroup;
	
	private int ageEmp;

	/**
	 * @return the ageGroup
	 */
	public String getAgeGroup() {
		return ageGroup;
	}

	/**
	 * @param ageGroup the ageGroup to set
	 */
	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}

	/**
	 * @return the ageEmp
	 */
	public int getAgeEmp() {
		return ageEmp;
	}

	/**
	 * @param ageEmp the ageEmp to set
	 */
	public void setAgeEmp(int ageEmp) {
		this.ageEmp = ageEmp;
	}
	
	
}
