package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 24-April-2015
 */
public class ResignationDocumentsVo {

	private String url;
	
	private String fileName;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
