package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Oct 28, 2015
 */
public class PayPaysalaryVo {

	private Long paysalaryId;
	
	private Long ledgerId;
	
	private String date;
	
	private List<String> checkedValues=new ArrayList<String>();
	
	private List<String> unCheckedValues=new ArrayList<String>();

	public List<String> getCheckedValues() {
		return checkedValues;
	}

	public void setCheckedValues(List<String> checkedValues) {
		this.checkedValues = checkedValues;
	}

	public List<String> getUnCheckedValues() {
		return unCheckedValues;
	}

	public void setUnCheckedValues(List<String> unCheckedValues) {
		this.unCheckedValues = unCheckedValues;
	}

	public Long getPaysalaryId() {
		return paysalaryId;
	}

	public void setPaysalaryId(Long paysalaryId) {
		this.paysalaryId = paysalaryId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(Long ledgerId) {
		this.ledgerId = ledgerId;
	}
	
	
}
