package com.hrms.web.vo;

import java.util.Map;

/**
 * 
 * @author Shamsheer
 * @since 07-May-2015
 */
public class SessionRolesVo {

	private Map<String, RoleSessionVo> roles ;

	public Map<String, RoleSessionVo> getRoles() {
		return roles;
	}

	public void setRoles(Map<String, RoleSessionVo> roles) {
		this.roles = roles;
	}
}
