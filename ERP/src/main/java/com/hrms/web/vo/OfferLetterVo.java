package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 13-July-2015
 */
public class OfferLetterVo {

	private Long designationId;
	private SalaryCalculatorVo salaryCalculatorVo;
	private String email;
	private String name;
	private String designation;
	private String grade;
	private String branch;
	private String sender;
	private String senderDesignation;
	private String dateOfJoining;
	private List<Long>candId ;
	private String date;
	private BigDecimal gross;
	private Long gradeId;
	
	
	public String getDateOfJoining() {
		return dateOfJoining;
	}
	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getSenderDesignation() {
		return senderDesignation;
	}
	public void setSenderDesignation(String senderDesignation) {
		this.senderDesignation = senderDesignation;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public SalaryCalculatorVo getSalaryCalculatorVo() {
		return salaryCalculatorVo;
	}
	public void setSalaryCalculatorVo(SalaryCalculatorVo salaryCalculatorVo) {
		this.salaryCalculatorVo = salaryCalculatorVo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public List<Long> getCandId() {
		return candId;
	}
	public void setCandId(List<Long> candId) {
		this.candId = candId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public BigDecimal getGross() {
		return gross;
	}
	public void setGross(BigDecimal gross) {
		this.gross = gross;
	}
	public Long getGradeId() {
		return gradeId;
	}
	public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}
	public Long getDesignationId() {
		return designationId;
	}
	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}
	
	
}
