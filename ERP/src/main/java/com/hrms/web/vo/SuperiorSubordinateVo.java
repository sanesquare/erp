package com.hrms.web.vo;

import java.util.List;


/**
 * 
 * @author Shamsheer
 * @since 30-March-2015
 */
public class SuperiorSubordinateVo {

	private List<SuperiorVo> superiors;
	
	private List<SubordinateVo> subordinates;

	/**
	 * @return the superiors
	 */
	public List<SuperiorVo> getSuperiors() {
		return superiors;
	}

	/**
	 * @param superiors the superiors to set
	 */
	public void setSuperiors(List<SuperiorVo> superiors) {
		this.superiors = superiors;
	}

	/**
	 * @return the subordinates
	 */
	public List<SubordinateVo> getSubordinates() {
		return subordinates;
	}

	/**
	 * @param subordinates the subordinates to set
	 */
	public void setSubordinates(List<SubordinateVo> subordinates) {
		this.subordinates = subordinates;
	}
}
