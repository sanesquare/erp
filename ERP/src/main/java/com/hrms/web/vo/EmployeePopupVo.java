package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 16-April-2015
 *
 */
public class EmployeePopupVo {

	private String name;

	private String email;

	private String designation;

	private String department;

	private String branch;

	private String joinDate;

	private String picture;

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 * @return
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * 
	 * @param designation
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * 
	 * @return
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * 
	 * @param department
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * 
	 * @return
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * 
	 * @param branch
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * 
	 * @return
	 */
	public String getJoinDate() {
		return joinDate;
	}

	/**
	 * 
	 * @param joinDate
	 */
	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getPicture() {
		return picture;
	}

	/**
	 * 
	 * @param picture
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}

}
