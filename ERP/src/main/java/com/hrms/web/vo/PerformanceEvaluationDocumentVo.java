package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 24-March-2015
 *
 */
public class PerformanceEvaluationDocumentVo {

	private Long documentId;

	private String documentName;

	private String documentUrl;

	private String evaluationFor;

	private String evaluationBy;

	/**
	 * 
	 * @return
	 */
	public Long getDocumentId() {
		return documentId;
	}

	/**
	 * 
	 * @param documentId
	 */
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * 
	 * @param documentName
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentUrl() {
		return documentUrl;
	}

	/**
	 * 
	 * @param documentUrl
	 */
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

	/**
	 * 
	 * @return
	 */
	public String getEvaluationFor() {
		return evaluationFor;
	}

	/**
	 * 
	 * @param evaluationFor
	 */
	public void setEvaluationFor(String evaluationFor) {
		this.evaluationFor = evaluationFor;
	}

	/**
	 * 
	 * @return
	 */
	public String getEvaluationBy() {
		return evaluationBy;
	}

	/**
	 * 
	 * @param evaluationBy
	 */
	public void setEvaluationBy(String evaluationBy) {
		this.evaluationBy = evaluationBy;
	}

}
