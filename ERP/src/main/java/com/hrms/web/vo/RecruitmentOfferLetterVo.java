package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 12-May-2015
 *
 */
public class RecruitmentOfferLetterVo {

	private String date;
	
	private Long postId;

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the postId
	 */
	public Long getPostId() {
		return postId;
	}

	/**
	 * @param postId the postId to set
	 */
	public void setPostId(Long postId) {
		this.postId = postId;
	}
	
}
