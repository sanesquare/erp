package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 2-April-2015
 */
public class JoiningJsonVo {

	private String employeeCode;
	
	private Long joiningId;
	
	private Long employeeId;
	
	private Long departmentId;
	
	private Long branchId;
	
	private Long employeeTypeId;
	
	private Long designationId;
	
	private String joiningDate;
	
	private List<Long> superiorIds = new ArrayList<Long>();
	
	private String notes;

	public Long getJoiningId() {
		return joiningId;
	}

	public void setJoiningId(Long joiningId) {
		this.joiningId = joiningId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public Long getEmployeeTypeId() {
		return employeeTypeId;
	}

	public void setEmployeeTypeId(Long employeeTypeId) {
		this.employeeTypeId = employeeTypeId;
	}

	public Long getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public String getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public List<Long> getSuperiorIds() {
		return superiorIds;
	}

	public void setSuperiorIds(List<Long> superiorIds) {
		this.superiorIds = superiorIds;
	}
}
