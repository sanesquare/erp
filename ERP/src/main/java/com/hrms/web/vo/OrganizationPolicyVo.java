package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Jithin Mohan
 * @since 15-May-2015
 *
 */
public class OrganizationPolicyVo {

	private Long org_policy_id;

	private Long policy_type_id;

	private String policyType;

	private String title;

	private List<Long> branch_id;

	private List<String> branches;

	private List<Long> department_id;

	private List<String> departments;

	private String description;

	private String information;

	private String recordedBy;

	private String recordedOn;

	private List<MultipartFile> organizationPolicyDocuments;

	private List<OrganizationPolicyDocumentsVo> documents;

	/**
	 * 
	 * @return
	 */
	public String getPolicyType() {
		return policyType;
	}

	/**
	 * 
	 * @param policyType
	 */
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getBranches() {
		return branches;
	}

	/**
	 * 
	 * @param branches
	 */
	public void setBranches(List<String> branches) {
		this.branches = branches;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getDepartments() {
		return departments;
	}

	/**
	 * 
	 * @param departments
	 */
	public void setDepartments(List<String> departments) {
		this.departments = departments;
	}

	/**
	 * 
	 * @return
	 */
	public List<MultipartFile> getOrganizationPolicyDocuments() {
		return organizationPolicyDocuments;
	}

	/**
	 * 
	 * @param organizationPolicyDocuments
	 */
	public void setOrganizationPolicyDocuments(List<MultipartFile> organizationPolicyDocuments) {
		this.organizationPolicyDocuments = organizationPolicyDocuments;
	}

	/**
	 * 
	 * @return
	 */
	public List<OrganizationPolicyDocumentsVo> getDocuments() {
		return documents;
	}

	/**
	 * 
	 * @param documents
	 */
	public void setDocuments(List<OrganizationPolicyDocumentsVo> documents) {
		this.documents = documents;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	/**
	 * 
	 * @return
	 */
	public Long getOrg_policy_id() {
		return org_policy_id;
	}

	/**
	 * 
	 * @param org_policy_id
	 */
	public void setOrg_policy_id(Long org_policy_id) {
		this.org_policy_id = org_policy_id;
	}

	/**
	 * 
	 * @return
	 */
	public Long getPolicy_type_id() {
		return policy_type_id;
	}

	/**
	 * 
	 * @param policy_type_id
	 */
	public void setPolicy_type_id(Long policy_type_id) {
		this.policy_type_id = policy_type_id;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 */
	public List<Long> getBranch_id() {
		return branch_id;
	}

	/**
	 * 
	 * @param branch_id
	 */
	public void setBranch_id(List<Long> branch_id) {
		this.branch_id = branch_id;
	}

	/**
	 * 
	 * @return
	 */
	public List<Long> getDepartment_id() {
		return department_id;
	}

	/**
	 * 
	 * @param department_id
	 */
	public void setDepartment_id(List<Long> department_id) {
		this.department_id = department_id;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getInformation() {
		return information;
	}

	/**
	 * 
	 * @param information
	 */
	public void setInformation(String information) {
		this.information = information;
	}

}
