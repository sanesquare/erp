package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 30-March-2015
 */
public class SubordinateVo {

	private Long subordinateId;
	
	private String subordinateCode;
	
	private String subordinateName;

	/**
	 * @return the subordinateId
	 */
	public Long getSubordinateId() {
		return subordinateId;
	}

	/**
	 * @param subordinateId the subordinateId to set
	 */
	public void setSubordinateId(Long subordinateId) {
		this.subordinateId = subordinateId;
	}

	/**
	 * @return the subordinateCode
	 */
	public String getSubordinateCode() {
		return subordinateCode;
	}

	/**
	 * @param subordinateCode the subordinateCode to set
	 */
	public void setSubordinateCode(String subordinateCode) {
		this.subordinateCode = subordinateCode;
	}

	/**
	 * @return the subordinateName
	 */
	public String getSubordinateName() {
		return subordinateName;
	}

	/**
	 * @param subordinateName the subordinateName to set
	 */
	public void setSubordinateName(String subordinateName) {
		this.subordinateName = subordinateName;
	}
	
}
