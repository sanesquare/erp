package com.hrms.web.vo;
/**
 * 
 * @author shamsheer
 * @since 22-March-2015
 */
public class EmployeeLanguageVo {

	private String language;
	
	private String speakingLevel;
	
	private String readingLevel;
	
	private String writingLevel;

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the speakingLevel
	 */
	public String getSpeakingLevel() {
		return speakingLevel;
	}

	/**
	 * @param speakingLevel the speakingLevel to set
	 */
	public void setSpeakingLevel(String speakingLevel) {
		this.speakingLevel = speakingLevel;
	}

	/**
	 * @return the readingLevel
	 */
	public String getReadingLevel() {
		return readingLevel;
	}

	/**
	 * @param readingLevel the readingLevel to set
	 */
	public void setReadingLevel(String readingLevel) {
		this.readingLevel = readingLevel;
	}

	/**
	 * @return the writingLevel
	 */
	public String getWritingLevel() {
		return writingLevel;
	}

	/**
	 * @param writingLevel the writingLevel to set
	 */
	public void setWritingLevel(String writingLevel) {
		this.writingLevel = writingLevel;
	}
	
	
}
