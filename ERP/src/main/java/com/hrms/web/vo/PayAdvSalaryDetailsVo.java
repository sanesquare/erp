package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayAdvSalaryDetailsVo {

	private int slno;
	
	private String empName;
	
	private String title;
	
	private BigDecimal amount;
	
	@DateTimeFormat
	private Date salaryDate;
	
	private String salaryDateString;
	
	@DateTimeFormat
	private Date repayDate;
	
	private String repayDateString;
	
	private BigDecimal repaidAmount;
	
	private BigDecimal remainingAmount;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	
	/**
	 * @return the salaryDate
	 */
	public Date getSalaryDate() {
		return salaryDate;
	}

	/**
	 * @param salaryDate the salaryDate to set
	 */
	public void setSalaryDate(Date salaryDate) {
		this.salaryDate = salaryDate;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the salaryDateString
	 */
	public String getSalaryDateString() {
		return salaryDateString;
	}

	/**
	 * @param salaryDateString the salaryDateString to set
	 */
	public void setSalaryDateString(String salaryDateString) {
		this.salaryDateString = salaryDateString;
	}

	public Date getRepayDate() {
		return repayDate;
	}

	public void setRepayDate(Date repayDate) {
		this.repayDate = repayDate;
	}

	public String getRepayDateString() {
		return repayDateString;
	}

	public void setRepayDateString(String repayDateString) {
		this.repayDateString = repayDateString;
	}

	public BigDecimal getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(BigDecimal remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public BigDecimal getRepaidAmount() {
		return repaidAmount;
	}

	public void setRepaidAmount(BigDecimal repaidAmount) {
		this.repaidAmount = repaidAmount;
	}
	
	
}
