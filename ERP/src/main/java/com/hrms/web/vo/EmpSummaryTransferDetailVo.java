package com.hrms.web.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 7-May-2015
 *
 */
public class EmpSummaryTransferDetailVo {

	private int slno;
	
	@DateTimeFormat
	private Date transferDate;
	
	private String fromBranch;
	
	private String fromDep;
	
	private String toBranch;
	
	private String toDep;
	
	private String transferDateString;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the transferDate
	 */
	public Date getTransferDate() {
		return transferDate;
	}

	/**
	 * @param transferDate the transferDate to set
	 */
	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	/**
	 * @return the fromBranch
	 */
	public String getFromBranch() {
		return fromBranch;
	}

	/**
	 * @param fromBranch the fromBranch to set
	 */
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}

	/**
	 * @return the fromDep
	 */
	public String getFromDep() {
		return fromDep;
	}

	/**
	 * @param fromDep the fromDep to set
	 */
	public void setFromDep(String fromDep) {
		this.fromDep = fromDep;
	}

	/**
	 * @return the toBranch
	 */
	public String getToBranch() {
		return toBranch;
	}

	/**
	 * @param toBranch the toBranch to set
	 */
	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}

	/**
	 * @return the toDep
	 */
	public String getToDep() {
		return toDep;
	}

	/**
	 * @param toDep the toDep to set
	 */
	public void setToDep(String toDep) {
		this.toDep = toDep;
	}

	/**
	 * @return the transferDateString
	 */
	public String getTransferDateString() {
		return transferDateString;
	}

	/**
	 * @param transferDateString the transferDateString to set
	 */
	public void setTransferDateString(String transferDateString) {
		this.transferDateString = transferDateString;
	}
	
	
}
