package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 20-April-2015
 */
public class SalaryVo {

	private String employee;

	private String employeeCode;

	private Long employeeId;

	private String salaryType;

	private Long salaryTypeId;

	private SalaryDailyWagesVo dailyWagesVo;

	private SalaryHourlyWageVo hourlyWageVo;

	private String withEffectFrom;

	private String monthlyTaxDeduction;

	private String annualTaxDeduction;

	private String monthlyEstimatedSalary;

	private String annualEstimatedSalary;

	private String monthlyGrossSalary;

	private String annualGrossSalary;

	private List<SalaryPayslipItemsVo> itemsVos;

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getSalaryType() {
		return salaryType;
	}

	public void setSalaryType(String salaryType) {
		this.salaryType = salaryType;
	}

	public Long getSalaryTypeId() {
		return salaryTypeId;
	}

	public void setSalaryTypeId(Long salaryTypeId) {
		this.salaryTypeId = salaryTypeId;
	}

	public SalaryDailyWagesVo getDailyWagesVo() {
		return dailyWagesVo;
	}

	public void setDailyWagesVo(SalaryDailyWagesVo dailyWagesVo) {
		this.dailyWagesVo = dailyWagesVo;
	}

	public String getWithEffectFrom() {
		return withEffectFrom;
	}

	public void setWithEffectFrom(String withEffectFrom) {
		this.withEffectFrom = withEffectFrom;
	}

	public SalaryHourlyWageVo getHourlyWageVo() {
		return hourlyWageVo;
	}

	public void setHourlyWageVo(SalaryHourlyWageVo hourlyWageVo) {
		this.hourlyWageVo = hourlyWageVo;
	}

	public List<SalaryPayslipItemsVo> getItemsVos() {
		return itemsVos;
	}

	public void setItemsVos(List<SalaryPayslipItemsVo> itemsVos) {
		this.itemsVos = itemsVos;
	}

	public String getMonthlyGrossSalary() {
		return monthlyGrossSalary;
	}

	public void setMonthlyGrossSalary(String monthlyGrossSalary) {
		this.monthlyGrossSalary = monthlyGrossSalary;
	}

	public String getAnnualGrossSalary() {
		return annualGrossSalary;
	}

	public void setAnnualGrossSalary(String annualGrossSalary) {
		this.annualGrossSalary = annualGrossSalary;
	}

	public String getMonthlyTaxDeduction() {
		return monthlyTaxDeduction;
	}

	public void setMonthlyTaxDeduction(String monthlyTaxDeduction) {
		this.monthlyTaxDeduction = monthlyTaxDeduction;
	}

	public String getAnnualTaxDeduction() {
		return annualTaxDeduction;
	}

	public void setAnnualTaxDeduction(String annualTaxDeduction) {
		this.annualTaxDeduction = annualTaxDeduction;
	}

	public String getMonthlyEstimatedSalary() {
		return monthlyEstimatedSalary;
	}

	public void setMonthlyEstimatedSalary(String monthlyEstimatedSalary) {
		this.monthlyEstimatedSalary = monthlyEstimatedSalary;
	}

	public String getAnnualEstimatedSalary() {
		return annualEstimatedSalary;
	}

	public void setAnnualEstimatedSalary(String annualEstimatedSalary) {
		this.annualEstimatedSalary = annualEstimatedSalary;
	}
}
