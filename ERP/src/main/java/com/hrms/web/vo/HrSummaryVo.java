package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 16-April-2015
 *
 */
public class HrSummaryVo {

	private Long branchNo;
	
	private Long depNo;
	
	private Long projectNo;
	
	private int totalEmp;
	
	private int activeEmp;
	
	private int inactiveEmp;
	
	private int maleEmp;
	
	private int femaleEmp;
	
	private List<EmpTypeStatusVo> empType;
	
	private List<EmpCategoryStatusVo> empCategory;

	

	/**
	 * @return the branchNo
	 */
	public Long getBranchNo() {
		return branchNo;
	}

	/**
	 * @param branchNo the branchNo to set
	 */
	public void setBranchNo(Long branchNo) {
		this.branchNo = branchNo;
	}

	/**
	 * @return the depNo
	 */
	

	/**
	 * @return the totalEmp
	 */
	public int getTotalEmp() {
		return totalEmp;
	}

	/**
	 * @return the depNo
	 */
	public Long getDepNo() {
		return depNo;
	}

	/**
	 * @param depNo the depNo to set
	 */
	public void setDepNo(Long depNo) {
		this.depNo = depNo;
	}

	/**
	 * @return the projectNo
	 */
	public Long getProjectNo() {
		return projectNo;
	}

	/**
	 * @param projectNo the projectNo to set
	 */
	public void setProjectNo(Long projectNo) {
		this.projectNo = projectNo;
	}

	/**
	 * @param totalEmp the totalEmp to set
	 */
	public void setTotalEmp(int totalEmp) {
		this.totalEmp = totalEmp;
	}

	/**
	 * @return the activeEmp
	 */
	public int getActiveEmp() {
		return activeEmp;
	}

	/**
	 * @param activeEmp the activeEmp to set
	 */
	public void setActiveEmp(int activeEmp) {
		this.activeEmp = activeEmp;
	}

	/**
	 * @return the inactiveEmp
	 */
	public int getInactiveEmp() {
		return inactiveEmp;
	}

	/**
	 * @param inactiveEmp the inactiveEmp to set
	 */
	public void setInactiveEmp(int inactiveEmp) {
		this.inactiveEmp = inactiveEmp;
	}

	/**
	 * @return the maleEmp
	 */
	public int getMaleEmp() {
		return maleEmp;
	}

	/**
	 * @param maleEmp the maleEmp to set
	 */
	public void setMaleEmp(int maleEmp) {
		this.maleEmp = maleEmp;
	}

	/**
	 * @return the femaleEmp
	 */
	public int getFemaleEmp() {
		return femaleEmp;
	}

	/**
	 * @param femaleEmp the femaleEmp to set
	 */
	public void setFemaleEmp(int femaleEmp) {
		this.femaleEmp = femaleEmp;
	}

	/**
	 * @return the empType
	 */
	public List<EmpTypeStatusVo> getEmpType() {
		return empType;
	}

	/**
	 * @param empType the empType to set
	 */
	public void setEmpType(List<EmpTypeStatusVo> empType) {
		this.empType = empType;
	}

	/**
	 * @return the empCategory
	 */
	public List<EmpCategoryStatusVo> getEmpCategory() {
		return empCategory;
	}

	/**
	 * @param empCategory the empCategory to set
	 */
	public void setEmpCategory(List<EmpCategoryStatusVo> empCategory) {
		this.empCategory = empCategory;
	}
	
	
	
}
