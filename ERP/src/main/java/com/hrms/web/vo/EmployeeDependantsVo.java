package com.hrms.web.vo;
/**
 * 
 * @author shamsheer
 * @since 22-March-2015
 */
public class EmployeeDependantsVo {

	private String dependantName;
	
	private String relation;
	
	private String datoeOfBirth;
	
	private String address;
	
	private Boolean nominee;

	/**
	 * @return the dependantName
	 */
	public String getDependantName() {
		return dependantName;
	}

	/**
	 * @param dependantName the dependantName to set
	 */
	public void setDependantName(String dependantName) {
		this.dependantName = dependantName;
	}

	/**
	 * @return the relation
	 */
	public String getRelation() {
		return relation;
	}

	/**
	 * @param relation the relation to set
	 */
	public void setRelation(String relation) {
		this.relation = relation;
	}

	/**
	 * @return the datoeOfBirth
	 */
	public String getDatoeOfBirth() {
		return datoeOfBirth;
	}

	/**
	 * @param datoeOfBirth the datoeOfBirth to set
	 */
	public void setDatoeOfBirth(String datoeOfBirth) {
		this.datoeOfBirth = datoeOfBirth;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the nominee
	 */
	public Boolean getNominee() {
		return nominee;
	}

	/**
	 * @param nominee the nominee to set
	 */
	public void setNominee(Boolean nominee) {
		this.nominee = nominee;
	}
	
	
}
