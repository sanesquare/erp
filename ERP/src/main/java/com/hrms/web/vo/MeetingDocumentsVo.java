package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 4-April-2015
 */
public class MeetingDocumentsVo {

	private Long meetingId;
	
	private String filename;
	
	private String url;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(Long meetingId) {
		this.meetingId = meetingId;
	}
	
}
