package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public class ReminderTabVo {

	private List<RemindersVo> reminders;

	private List<ReminderStatusVo> reminderStatus;

	/**
	 * 
	 * @return
	 */
	public List<RemindersVo> getReminders() {
		return reminders;
	}

	/**
	 * 
	 * @param reminders
	 */
	public void setReminders(List<RemindersVo> reminders) {
		this.reminders = reminders;
	}

	/**
	 * 
	 * @return
	 */
	public List<ReminderStatusVo> getReminderStatus() {
		return reminderStatus;
	}

	/**
	 * 
	 * @param reminderStatus
	 */
	public void setReminderStatus(List<ReminderStatusVo> reminderStatus) {
		this.reminderStatus = reminderStatus;
	}

}
