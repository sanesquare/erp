package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 06-May-2015
 */
public class RoleTimeSheetVo {

	private String title;

	private List<RoleTypesVo> list;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<RoleTypesVo> getList() {
		return list;
	}

	public void setList(List<RoleTypesVo> list) {
		this.list = list;
	}
}
