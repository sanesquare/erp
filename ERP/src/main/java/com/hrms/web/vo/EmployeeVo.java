package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Sangeeth and Bibin
 * 
 * @modified_by Shamsheer
 * @modified_on 18-March-2015
 */

public class EmployeeVo {
	
	private String panCadNumber;

	private String profilePicHidden;
	
	private Long salaryTypeId;
	
	private String salaryType;
	
	private Boolean isAdmin;
	
	private Long designationRank;

	private SuperiorSubordinateVo superiorSubordinateVo;

	private Long leaveId;
	
	private String name;

	private Long leaveTitleId;

	private String leaveType;
	
	private String pfId;
	
	private String esiId;
	
	private Long leaves;

	private String leaveStatus;

	private String carryOver;
 
	private List<ProjectVo> projectVo;

	private Long countryId;

	private Long bloodGroupId;

	private Long nationalityId;

	private String createdBy;

	private String createdOn;

	private Boolean notifyByEmail;

	private Boolean allowLogin;

	private Boolean withPayroll;

	private String salutation;

	private String profilePic;

	private Long employeeId;

	private String employeeCode;

	private String employeeName;

	private String firstName;

	private String lastName;

	private String dateOfBirth;

	private String gender;

	private String bloodGroup;

	private String relegion;

	private Long relegionId;

	private String nationality;

	private String designation;

	private Long designationId;

	private String employeeType;

	private Long employeeTypeId;

	private String employeeCategory;

	private Long employeeCategoryId;

	private String employeeBranch;

	private Long employeeBranchId;

	private String employeeDepartment;

	private Long employeeDepartmentId;

	private String employeeGrade;

	private Long employeeGradeId;

	private String userName;

	private String password;

	private String email;

	private String status;

	private Long statusId;

	private String workShift;

	private long workShiftId;

	private String address;

	private String city;

	private String state;

	private String zipCode;

	private String country;

	private String homePhoneNumber;

	private String officePhoneNumber;

	private String mobileNumber;

	private String emergencyPerson;

	private String emergencyRelation;

	private String emergencyMobile;

	private String notes;

	private String joiningDate;

	private String passportExpiration;

	private String passportNumber;

	private String licenceExpiration;

	private String licenceNumber;

	private String goovernmentId;

	private String employeeTaxNumber;

	private List<MultipartFile> emp_upld_docs;

	private List<EmployeeDocumentsVo> documents;

	private List<EmployeeQualificationVo> qualifications;

	private List<EmployeeOrganisationAssetsVo> organizationAssets;

	private List<EmployeeLanguageVo> employeeLanguages;

	private List<EmployeeUniformVo> uniforms;

	private List<EmployeeWorkExperiencVo> experiences;

	private List<EmployeeSkillsVo> skills;

	private List<EmployeeDependantsVo> dependants;

	private List<EmployeeextOfKinVo> nextOfKins;

	private List<EmployeeBenefitVo> benefits;

	private List<EmployeeBankAccountVo> bankAccountVo;

	private EmployeeReferenceVo referenceVo;

	private List<EmployeeDocumentsVo> documentsVos;

	private List<Long> subordinatesIds;

	private List<Long> superiorsIds;
	
	private List<EmployeeLeaveVo> leaveVos;
	
	private List<Long> branchIds=new ArrayList<Long>();

	/**
	 * @return the designationRank
	 */
	public Long getDesignationRank() {
		return designationRank;
	}

	/**
	 * @param designationRank the designationRank to set
	 */
	public void setDesignationRank(Long designationRank) {
		this.designationRank = designationRank;
	}

	/**
	 * @return the superiorSubordinateVo
	 */
	public SuperiorSubordinateVo getSuperiorSubordinateVo() {
		return superiorSubordinateVo;
	}

	/**
	 * @param superiorSubordinateVo the superiorSubordinateVo to set
	 */
	public void setSuperiorSubordinateVo(SuperiorSubordinateVo superiorSubordinateVo) {
		this.superiorSubordinateVo = superiorSubordinateVo;
	}

	/**
	 * @return the leaveId
	 */
	public Long getLeaveId() {
		return leaveId;
	}

	/**
	 * @param leaveId the leaveId to set
	 */
	public void setLeaveId(Long leaveId) {
		this.leaveId = leaveId;
	}

	/**
	 * @return the leaveTitleId
	 */
	public Long getLeaveTitleId() {
		return leaveTitleId;
	}

	/**
	 * @param leaveTitleId the leaveTitleId to set
	 */
	public void setLeaveTitleId(Long leaveTitleId) {
		this.leaveTitleId = leaveTitleId;
	}

	/**
	 * @return the leaveType
	 */
	public String getLeaveType() {
		return leaveType;
	}

	/**
	 * @param leaveType the leaveType to set
	 */
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	/**
	 * @return the leaves
	 */
	public Long getLeaves() {
		return leaves;
	}

	/**
	 * @param leaves the leaves to set
	 */
	public void setLeaves(Long leaves) {
		this.leaves = leaves;
	}

	/**
	 * @return the leaveStatus
	 */
	public String getLeaveStatus() {
		return leaveStatus;
	}

	/**
	 * @param leaveStatus the leaveStatus to set
	 */
	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	/**
	 * @return the carryOver
	 */
	public String getCarryOver() {
		return carryOver;
	}

	/**
	 * @param carryOver the carryOver to set
	 */
	public void setCarryOver(String carryOver) {
		this.carryOver = carryOver;
	}

	/**
	 * @return the projectVo
	 */
	public List<ProjectVo> getProjectVo() {
		return projectVo;
	}

	/**
	 * @param projectVo the projectVo to set
	 */
	public void setProjectVo(List<ProjectVo> projectVo) {
		this.projectVo = projectVo;
	}

	/**
	 * @return the countryId
	 */
	public Long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the bloodGroupId
	 */
	public Long getBloodGroupId() {
		return bloodGroupId;
	}

	/**
	 * @param bloodGroupId the bloodGroupId to set
	 */
	public void setBloodGroupId(Long bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}

	/**
	 * @return the nationalityId
	 */
	public Long getNationalityId() {
		return nationalityId;
	}

	/**
	 * @param nationalityId the nationalityId to set
	 */
	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the notifyByEmail
	 */
	public Boolean getNotifyByEmail() {
		return notifyByEmail;
	}

	/**
	 * @param notifyByEmail the notifyByEmail to set
	 */
	public void setNotifyByEmail(Boolean notifyByEmail) {
		this.notifyByEmail = notifyByEmail;
	}

	/**
	 * @return the allowLogin
	 */
	public Boolean getAllowLogin() {
		return allowLogin;
	}

	/**
	 * @param allowLogin the allowLogin to set
	 */
	public void setAllowLogin(Boolean allowLogin) {
		this.allowLogin = allowLogin;
	}

	/**
	 * @return the withPayroll
	 */
	public Boolean getWithPayroll() {
		return withPayroll;
	}

	/**
	 * @param withPayroll the withPayroll to set
	 */
	public void setWithPayroll(Boolean withPayroll) {
		this.withPayroll = withPayroll;
	}

	/**
	 * @return the salutation
	 */
	public String getSalutation() {
		return salutation;
	}

	/**
	 * @param salutation the salutation to set
	 */
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	/**
	 * @return the profilePic
	 */
	public String getProfilePic() {
		return profilePic;
	}

	/**
	 * @param profilePic the profilePic to set
	 */
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	/**
	 * @return the employeeId
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the employeeCode
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * @param employeeCode the employeeCode to set
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the bloodGroup
	 */
	public String getBloodGroup() {
		return bloodGroup;
	}

	/**
	 * @param bloodGroup the bloodGroup to set
	 */
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	/**
	 * @return the relegion
	 */
	public String getRelegion() {
		return relegion;
	}

	/**
	 * @param relegion the relegion to set
	 */
	public void setRelegion(String relegion) {
		this.relegion = relegion;
	}

	/**
	 * @return the relegionId
	 */
	public Long getRelegionId() {
		return relegionId;
	}

	/**
	 * @param relegionId the relegionId to set
	 */
	public void setRelegionId(Long relegionId) {
		this.relegionId = relegionId;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the designationId
	 */
	public Long getDesignationId() {
		return designationId;
	}

	/**
	 * @param designationId the designationId to set
	 */
	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	/**
	 * @return the employeeType
	 */
	public String getEmployeeType() {
		return employeeType;
	}

	/**
	 * @param employeeType the employeeType to set
	 */
	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	/**
	 * @return the employeeTypeId
	 */
	public Long getEmployeeTypeId() {
		return employeeTypeId;
	}

	/**
	 * @param employeeTypeId the employeeTypeId to set
	 */
	public void setEmployeeTypeId(Long employeeTypeId) {
		this.employeeTypeId = employeeTypeId;
	}

	/**
	 * @return the employeeCategory
	 */
	public String getEmployeeCategory() {
		return employeeCategory;
	}

	/**
	 * @param employeeCategory the employeeCategory to set
	 */
	public void setEmployeeCategory(String employeeCategory) {
		this.employeeCategory = employeeCategory;
	}

	/**
	 * @return the employeeCategoryId
	 */
	public Long getEmployeeCategoryId() {
		return employeeCategoryId;
	}

	/**
	 * @param employeeCategoryId the employeeCategoryId to set
	 */
	public void setEmployeeCategoryId(Long employeeCategoryId) {
		this.employeeCategoryId = employeeCategoryId;
	}

	/**
	 * @return the employeeBranch
	 */
	public String getEmployeeBranch() {
		return employeeBranch;
	}

	/**
	 * @param employeeBranch the employeeBranch to set
	 */
	public void setEmployeeBranch(String employeeBranch) {
		this.employeeBranch = employeeBranch;
	}

	/**
	 * @return the employeeBranchId
	 */
	public Long getEmployeeBranchId() {
		return employeeBranchId;
	}

	/**
	 * @param employeeBranchId the employeeBranchId to set
	 */
	public void setEmployeeBranchId(Long employeeBranchId) {
		this.employeeBranchId = employeeBranchId;
	}

	/**
	 * @return the employeeDepartment
	 */
	public String getEmployeeDepartment() {
		return employeeDepartment;
	}

	/**
	 * @param employeeDepartment the employeeDepartment to set
	 */
	public void setEmployeeDepartment(String employeeDepartment) {
		this.employeeDepartment = employeeDepartment;
	}

	/**
	 * @return the employeeDepartmentId
	 */
	public Long getEmployeeDepartmentId() {
		return employeeDepartmentId;
	}

	/**
	 * @param employeeDepartmentId the employeeDepartmentId to set
	 */
	public void setEmployeeDepartmentId(Long employeeDepartmentId) {
		this.employeeDepartmentId = employeeDepartmentId;
	}

	/**
	 * @return the employeeGrade
	 */
	public String getEmployeeGrade() {
		return employeeGrade;
	}

	/**
	 * @param employeeGrade the employeeGrade to set
	 */
	public void setEmployeeGrade(String employeeGrade) {
		this.employeeGrade = employeeGrade;
	}

	/**
	 * @return the employeeGradeId
	 */
	public Long getEmployeeGradeId() {
		return employeeGradeId;
	}

	/**
	 * @param employeeGradeId the employeeGradeId to set
	 */
	public void setEmployeeGradeId(Long employeeGradeId) {
		this.employeeGradeId = employeeGradeId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the workShift
	 */
	public String getWorkShift() {
		return workShift;
	}

	/**
	 * @param workShift the workShift to set
	 */
	public void setWorkShift(String workShift) {
		this.workShift = workShift;
	}

	/**
	 * @return the workShiftId
	 */
	public long getWorkShiftId() {
		return workShiftId;
	}

	/**
	 * @param workShiftId the workShiftId to set
	 */
	public void setWorkShiftId(long workShiftId) {
		this.workShiftId = workShiftId;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the homePhoneNumber
	 */
	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	/**
	 * @param homePhoneNumber the homePhoneNumber to set
	 */
	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	/**
	 * @return the officePhoneNumber
	 */
	public String getOfficePhoneNumber() {
		return officePhoneNumber;
	}

	/**
	 * @param officePhoneNumber the officePhoneNumber to set
	 */
	public void setOfficePhoneNumber(String officePhoneNumber) {
		this.officePhoneNumber = officePhoneNumber;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the emergencyPerson
	 */
	public String getEmergencyPerson() {
		return emergencyPerson;
	}

	/**
	 * @param emergencyPerson the emergencyPerson to set
	 */
	public void setEmergencyPerson(String emergencyPerson) {
		this.emergencyPerson = emergencyPerson;
	}

	/**
	 * @return the emergencyRelation
	 */
	public String getEmergencyRelation() {
		return emergencyRelation;
	}

	/**
	 * @param emergencyRelation the emergencyRelation to set
	 */
	public void setEmergencyRelation(String emergencyRelation) {
		this.emergencyRelation = emergencyRelation;
	}

	/**
	 * @return the emergencyMobile
	 */
	public String getEmergencyMobile() {
		return emergencyMobile;
	}

	/**
	 * @param emergencyMobile the emergencyMobile to set
	 */
	public void setEmergencyMobile(String emergencyMobile) {
		this.emergencyMobile = emergencyMobile;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the joiningDate
	 */
	public String getJoiningDate() {
		return joiningDate;
	}

	/**
	 * @param joiningDate the joiningDate to set
	 */
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}

	/**
	 * @return the passportExpiration
	 */
	public String getPassportExpiration() {
		return passportExpiration;
	}

	/**
	 * @param passportExpiration the passportExpiration to set
	 */
	public void setPassportExpiration(String passportExpiration) {
		this.passportExpiration = passportExpiration;
	}

	/**
	 * @return the passportNumber
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * @param passportNumber the passportNumber to set
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	/**
	 * @return the licenceExpiration
	 */
	public String getLicenceExpiration() {
		return licenceExpiration;
	}

	/**
	 * @param licenceExpiration the licenceExpiration to set
	 */
	public void setLicenceExpiration(String licenceExpiration) {
		this.licenceExpiration = licenceExpiration;
	}

	/**
	 * @return the licenceNumber
	 */
	public String getLicenceNumber() {
		return licenceNumber;
	}

	/**
	 * @param licenceNumber the licenceNumber to set
	 */
	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}

	/**
	 * @return the goovernmentId
	 */
	public String getGoovernmentId() {
		return goovernmentId;
	}

	/**
	 * @param goovernmentId the goovernmentId to set
	 */
	public void setGoovernmentId(String goovernmentId) {
		this.goovernmentId = goovernmentId;
	}

	/**
	 * @return the employeeTaxNumber
	 */
	public String getEmployeeTaxNumber() {
		return employeeTaxNumber;
	}

	/**
	 * @param employeeTaxNumber the employeeTaxNumber to set
	 */
	public void setEmployeeTaxNumber(String employeeTaxNumber) {
		this.employeeTaxNumber = employeeTaxNumber;
	}

	/**
	 * @return the emp_upld_docs
	 */
	public List<MultipartFile> getEmp_upld_docs() {
		return emp_upld_docs;
	}

	/**
	 * @param emp_upld_docs the emp_upld_docs to set
	 */
	public void setEmp_upld_docs(List<MultipartFile> emp_upld_docs) {
		this.emp_upld_docs = emp_upld_docs;
	}

	/**
	 * @return the documents
	 */
	public List<EmployeeDocumentsVo> getDocuments() {
		return documents;
	}

	/**
	 * @param documents the documents to set
	 */
	public void setDocuments(List<EmployeeDocumentsVo> documents) {
		this.documents = documents;
	}

	/**
	 * @return the qualifications
	 */
	public List<EmployeeQualificationVo> getQualifications() {
		return qualifications;
	}

	/**
	 * @param qualifications the qualifications to set
	 */
	public void setQualifications(List<EmployeeQualificationVo> qualifications) {
		this.qualifications = qualifications;
	}

	/**
	 * @return the organizationAssets
	 */
	public List<EmployeeOrganisationAssetsVo> getOrganizationAssets() {
		return organizationAssets;
	}

	/**
	 * @param organizationAssets the organizationAssets to set
	 */
	public void setOrganizationAssets(List<EmployeeOrganisationAssetsVo> organizationAssets) {
		this.organizationAssets = organizationAssets;
	}

	/**
	 * @return the employeeLanguages
	 */
	public List<EmployeeLanguageVo> getEmployeeLanguages() {
		return employeeLanguages;
	}

	/**
	 * @param employeeLanguages the employeeLanguages to set
	 */
	public void setEmployeeLanguages(List<EmployeeLanguageVo> employeeLanguages) {
		this.employeeLanguages = employeeLanguages;
	}

	/**
	 * @return the uniforms
	 */
	public List<EmployeeUniformVo> getUniforms() {
		return uniforms;
	}

	/**
	 * @param uniforms the uniforms to set
	 */
	public void setUniforms(List<EmployeeUniformVo> uniforms) {
		this.uniforms = uniforms;
	}

	/**
	 * @return the experiences
	 */
	public List<EmployeeWorkExperiencVo> getExperiences() {
		return experiences;
	}

	/**
	 * @param experiences the experiences to set
	 */
	public void setExperiences(List<EmployeeWorkExperiencVo> experiences) {
		this.experiences = experiences;
	}

	/**
	 * @return the skills
	 */
	public List<EmployeeSkillsVo> getSkills() {
		return skills;
	}

	/**
	 * @param skills the skills to set
	 */
	public void setSkills(List<EmployeeSkillsVo> skills) {
		this.skills = skills;
	}

	/**
	 * @return the dependants
	 */
	public List<EmployeeDependantsVo> getDependants() {
		return dependants;
	}

	/**
	 * @param dependants the dependants to set
	 */
	public void setDependants(List<EmployeeDependantsVo> dependants) {
		this.dependants = dependants;
	}

	/**
	 * @return the nextOfKins
	 */
	public List<EmployeeextOfKinVo> getNextOfKins() {
		return nextOfKins;
	}

	/**
	 * @param nextOfKins the nextOfKins to set
	 */
	public void setNextOfKins(List<EmployeeextOfKinVo> nextOfKins) {
		this.nextOfKins = nextOfKins;
	}

	/**
	 * @return the benefits
	 */
	public List<EmployeeBenefitVo> getBenefits() {
		return benefits;
	}

	/**
	 * @param benefits the benefits to set
	 */
	public void setBenefits(List<EmployeeBenefitVo> benefits) {
		this.benefits = benefits;
	}

	/**
	 * @return the bankAccountVo
	 */
	public List<EmployeeBankAccountVo> getBankAccountVo() {
		return bankAccountVo;
	}

	/**
	 * @param bankAccountVo the bankAccountVo to set
	 */
	public void setBankAccountVo(List<EmployeeBankAccountVo> bankAccountVo) {
		this.bankAccountVo = bankAccountVo;
	}

	/**
	 * @return the referenceVo
	 */
	public EmployeeReferenceVo getReferenceVo() {
		return referenceVo;
	}

	/**
	 * @param referenceVo the referenceVo to set
	 */
	public void setReferenceVo(EmployeeReferenceVo referenceVo) {
		this.referenceVo = referenceVo;
	}

	/**
	 * @return the documentsVos
	 */
	public List<EmployeeDocumentsVo> getDocumentsVos() {
		return documentsVos;
	}

	/**
	 * @param documentsVos the documentsVos to set
	 */
	public void setDocumentsVos(List<EmployeeDocumentsVo> documentsVos) {
		this.documentsVos = documentsVos;
	}

	/**
	 * @return the subordinatesIds
	 */
	public List<Long> getSubordinatesIds() {
		return subordinatesIds;
	}

	/**
	 * @param subordinatesIds the subordinatesIds to set
	 */
	public void setSubordinatesIds(List<Long> subordinatesIds) {
		this.subordinatesIds = subordinatesIds;
	}

	/**
	 * @return the superiorsIds
	 */
	public List<Long> getSuperiorsIds() {
		return superiorsIds;
	}

	/**
	 * @param superiorsIds the superiorsIds to set
	 */
	public void setSuperiorsIds(List<Long> superiorsIds) {
		this.superiorsIds = superiorsIds;
	}

	public List<EmployeeLeaveVo> getLeaveVos() {
		return leaveVos;
	}

	public void setLeaveVos(List<EmployeeLeaveVo> leaveVos) {
		this.leaveVos = leaveVos;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getSalaryTypeId() {
		return salaryTypeId;
	}

	public void setSalaryTypeId(Long salaryTypeId) {
		this.salaryTypeId = salaryTypeId;
	}

	public String getSalaryType() {
		return salaryType;
	}

	public void setSalaryType(String salaryType) {
		this.salaryType = salaryType;
	}

	public String getPfId() {
		return pfId;
	}

	public void setPfId(String pfId) {
		this.pfId = pfId;
	}

	public String getEsiId() {
		return esiId;
	}

	public void setEsiId(String esiId) {
		this.esiId = esiId;
	}

	public String getProfilePicHidden() {
		return profilePicHidden;
	}

	public void setProfilePicHidden(String profilePicHidden) {
		this.profilePicHidden = profilePicHidden;
	}

	public List<Long> getBranchIds() {
		return branchIds;
	}

	public void setBranchIds(List<Long> branchIds) {
		this.branchIds = branchIds;
	}

	public String getPanCadNumber() {
		return panCadNumber;
	}

	public void setPanCadNumber(String panCadNumber) {
		this.panCadNumber = panCadNumber;
	}

}
