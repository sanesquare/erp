package com.hrms.web.vo;

import java.util.List;

import com.hrms.web.entities.OrganisationContactPerson;

/**
 * 
 * @author Jithin Mohan
 * @since 14-March-2015
 *
 */
public class OrganisationVo {

	private Long organisationId;

	private String organisationCode;

	private String organisationUrt;

	private String organisationName;

	private String organisationYear;

	private String fiscalMonth;

	private String fiscalDay;

	private String contactFirstName;

	private String contactLastName;

	private String contactEmail;

	private String contactCountry;

	private String contactNumber;

	private List<OrganisationContactPerson> contactPersons;

	/**
	 * 
	 * @return
	 */
	public List<OrganisationContactPerson> getContactPersons() {
		return contactPersons;
	}

	/**
	 * 
	 * @param contactPersons
	 */
	public void setContactPersons(List<OrganisationContactPerson> contactPersons) {
		this.contactPersons = contactPersons;
	}

	/**
	 * 
	 * @return
	 */
	public Long getOrganisationId() {
		return organisationId;
	}

	/**
	 * 
	 * @param organisationId
	 */
	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	/**
	 * 
	 * @return
	 */
	public String getOrganisationCode() {
		return organisationCode;
	}

	/**
	 * 
	 * @param organisationCode
	 */
	public void setOrganisationCode(String organisationCode) {
		this.organisationCode = organisationCode;
	}

	/**
	 * 
	 * @return
	 */
	public String getOrganisationUrt() {
		return organisationUrt;
	}

	/**
	 * 
	 * @param organisationUrt
	 */
	public void setOrganisationUrt(String organisationUrt) {
		this.organisationUrt = organisationUrt;
	}

	/**
	 * 
	 * @return
	 */
	public String getOrganisationName() {
		return organisationName;
	}

	/**
	 * 
	 * @param organisationName
	 */
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	/**
	 * 
	 * @return
	 */
	public String getOrganisationYear() {
		return organisationYear;
	}

	/**
	 * 
	 * @param organisationYear
	 */
	public void setOrganisationYear(String organisationYear) {
		this.organisationYear = organisationYear;
	}

	/**
	 * 
	 * @return
	 */
	public String getFiscalMonth() {
		return fiscalMonth;
	}

	/**
	 * 
	 * @param fiscalMonth
	 */
	public void setFiscalMonth(String fiscalMonth) {
		this.fiscalMonth = fiscalMonth;
	}

	/**
	 * 
	 * @return
	 */
	public String getFiscalDay() {
		return fiscalDay;
	}

	/**
	 * 
	 * @param fiscalDay
	 */
	public void setFiscalDay(String fiscalDay) {
		this.fiscalDay = fiscalDay;
	}

	/**
	 * 
	 * @return
	 */
	public String getContactFirstName() {
		return contactFirstName;
	}

	/**
	 * 
	 * @param contactFirstName
	 */
	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	/**
	 * 
	 * @return
	 */
	public String getContactLastName() {
		return contactLastName;
	}

	/**
	 * 
	 * @param contactLastName
	 */
	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	/**
	 * 
	 * @return
	 */
	public String getContactEmail() {
		return contactEmail;
	}

	/**
	 * 
	 * @param contactEmail
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	/**
	 * 
	 * @return
	 */
	public String getContactCountry() {
		return contactCountry;
	}

	/**
	 * 
	 * @param contactCountry
	 */
	public void setContactCountry(String contactCountry) {
		this.contactCountry = contactCountry;
	}

	/**
	 * 
	 * @return
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * 
	 * @param contactNumber
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

}
