package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpPrimaryReportVo {

	private Long branchId;
	
	private Long depId;
	
	private String startDate;
	
	private String endDate;
	
	private String employee;
	
	private String employeeCode;
	
	private String empUserName;
	
	private Long projectId;
	
	private String compByEmp;
	
	private String compAgnstEmp;
	
	

	/**
	 * @return the branchId
	 */
	public Long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the depId
	 */
	public Long getDepId() {
		return depId;
	}

	/**
	 * @param depId the depId to set
	 */
	public void setDepId(Long depId) {
		this.depId = depId;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * @return the empUserName
	 */
	public String getEmpUserName() {
		return empUserName;
	}

	/**
	 * @param empUserName the empUserName to set
	 */
	public void setEmpUserName(String empUserName) {
		this.empUserName = empUserName;
	}

	/**
	 * @return the projectId
	 */
	public Long getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the compByEmp
	 */
	public String getCompByEmp() {
		return compByEmp;
	}

	/**
	 * @param compByEmp the compByEmp to set
	 */
	public void setCompByEmp(String compByEmp) {
		this.compByEmp = compByEmp;
	}

	/**
	 * @return the compAgnstEmp
	 */
	public String getCompAgnstEmp() {
		return compAgnstEmp;
	}

	/**
	 * @param compAgnstEmp the compAgnstEmp to set
	 */
	public void setCompAgnstEmp(String compAgnstEmp) {
		this.compAgnstEmp = compAgnstEmp;
	}

	/**
	 * @return the employeeCode
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * @param employeeCode the employeeCode to set
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	
	
	
}
