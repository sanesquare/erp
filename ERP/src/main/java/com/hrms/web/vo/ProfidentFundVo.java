package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Jithin Mohan
 * @since 14-April-2015
 *
 */
public class ProfidentFundVo {

	private Long profidentFundId;

	private String employee;

	private String employeeCode;

	private BigDecimal employeeShare;

	private BigDecimal organizationShare;

	private String description;

	private String notes;

	private String recordedOn;

	private String recordedBy;

	/**
	 * 
	 * @return
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * 
	 * @param employeeCode
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * 
	 * @return
	 */
	public Long getProfidentFundId() {
		return profidentFundId;
	}

	/**
	 * 
	 * @param profidentFundId
	 */
	public void setProfidentFundId(Long profidentFundId) {
		this.profidentFundId = profidentFundId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getEmployeeShare() {
		return employeeShare;
	}

	/**
	 * 
	 * @param employeeShare
	 */
	public void setEmployeeShare(BigDecimal employeeShare) {
		this.employeeShare = employeeShare;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getOrganizationShare() {
		return organizationShare;
	}

	/**
	 * 
	 * @param organizationShare
	 */
	public void setOrganizationShare(BigDecimal organizationShare) {
		this.organizationShare = organizationShare;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

}
