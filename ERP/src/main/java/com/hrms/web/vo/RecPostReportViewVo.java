package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 20-April-2015
 *
 */
public class RecPostReportViewVo {

	private List<RecPostReportDetailsVo> detailsVo;
	
	private String type;

	public List<RecPostReportDetailsVo> getDetailsVo() {
		return detailsVo;
	}

	public void setDetailsVo(List<RecPostReportDetailsVo> detailsVo) {
		this.detailsVo = detailsVo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
