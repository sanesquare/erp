package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
public class TravelArrangementTypeVo {

	private Long id;
	
	private String arrangementType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getArrangementType() {
		return arrangementType;
	}

	public void setArrangementType(String arrangementType) {
		this.arrangementType = arrangementType;
	}
}
