package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer
 * @since 29-April-2015
 */
public class PayslipEmployeeItemsVo {

	private String title;
	
	private BigDecimal amount;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
