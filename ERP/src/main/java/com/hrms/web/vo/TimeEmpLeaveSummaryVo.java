package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 29-April-2015
 *
 */
public class TimeEmpLeaveSummaryVo {

	private String leaveType;
	
	private String payType;
	
	private Long quota;
	
	private int leavesTaken;
	
	private int remainingLeaves;

	/**
	 * @return the leaveType
	 */
	public String getLeaveType() {
		return leaveType;
	}

	/**
	 * @param leaveType the leaveType to set
	 */
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	/**
	 * @return the payType
	 */
	public String getPayType() {
		return payType;
	}

	/**
	 * @param payType the payType to set
	 */
	public void setPayType(String payType) {
		this.payType = payType;
	}

	
	/**
	 * @return the leavesTaken
	 */
	public int getLeavesTaken() {
		return leavesTaken;
	}

	/**
	 * @param leavesTaken the leavesTaken to set
	 */
	public void setLeavesTaken(int leavesTaken) {
		this.leavesTaken = leavesTaken;
	}

	/**
	 * @return the remainingLeaves
	 */
	public int getRemainingLeaves() {
		return remainingLeaves;
	}

	/**
	 * @param remainingLeaves the remainingLeaves to set
	 */
	public void setRemainingLeaves(int remainingLeaves) {
		this.remainingLeaves = remainingLeaves;
	}

	/**
	 * @return the quota
	 */
	public Long getQuota() {
		return quota;
	}

	/**
	 * @param quota the quota to set
	 */
	public void setQuota(Long quota) {
		this.quota = quota;
	}
	
	
}
