package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public class OvertimeVo {

	private Long overtimeId;

	private String employee;

	private String employeeCode;

	private List<String> forwaders;

	private List<String> forwadersName;

	private String overtimeTitle;

	private BigDecimal overtimeAmount;

	private String requestedDate;

	private String description;

	private String notes;

	private String status;

	private String statusDescription;

	private String timeIn;

	private String timeOut;

	private String recordedOn;

	private String recordedBy;

	/**
	 * 
	 * @return
	 */
	public List<String> getForwadersName() {
		return forwadersName;
	}

	/**
	 * 
	 * @param forwadersName
	 */
	public void setForwadersName(List<String> forwadersName) {
		this.forwadersName = forwadersName;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * 
	 * @param employeeCode
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * 
	 * @return
	 */
	public Long getOvertimeId() {
		return overtimeId;
	}

	/**
	 * 
	 * @param overtimeId
	 */
	public void setOvertimeId(Long overtimeId) {
		this.overtimeId = overtimeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getForwaders() {
		return forwaders;
	}

	/**
	 * 
	 * @param forwaders
	 */
	public void setForwaders(List<String> forwaders) {
		this.forwaders = forwaders;
	}

	/**
	 * 
	 * @return
	 */
	public String getOvertimeTitle() {
		return overtimeTitle;
	}

	/**
	 * 
	 * @param overtimeTitle
	 */
	public void setOvertimeTitle(String overtimeTitle) {
		this.overtimeTitle = overtimeTitle;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getOvertimeAmount() {
		return overtimeAmount;
	}

	/**
	 * 
	 * @param overtimeAmount
	 */
	public void setOvertimeAmount(BigDecimal overtimeAmount) {
		this.overtimeAmount = overtimeAmount;
	}

	/**
	 * 
	 * @return
	 */
	public String getRequestedDate() {
		return requestedDate;
	}

	/**
	 * 
	 * @param requestedDate
	 */
	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * 
	 * @param statusDescription
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/**
	 * 
	 * @return
	 */
	public String getTimeIn() {
		return timeIn;
	}

	/**
	 * 
	 * @param timeIn
	 */
	public void setTimeIn(String timeIn) {
		this.timeIn = timeIn;
	}

	/**
	 * 
	 * @return
	 */
	public String getTimeOut() {
		return timeOut;
	}

	/**
	 * 
	 * @param timeOut
	 */
	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

}
