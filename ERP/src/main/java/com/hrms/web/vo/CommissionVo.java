package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
public class CommissionVo {

	private Boolean viewStatus;
	
	private String employee;
	
	private Long id;
	
	private Long commissionId;
	
	private Long titleId;
	
	private Long employeeId;
	
	private Long statusId;
	
	private List<Long> superiorIds;
	
	private List<Long> superiorAjaxIds;
	
	private String title;
	
	private String employeeCode;
	
	private String status;
	
	private String statusDescription;
	
	private List<String> superiorCodes;
	
	private List<String> superiors;
	
	private String date;
	
	private String amount;
	
	private String createdBy;
	
	private String createdOn;
	
	private String notes;
	
	private String description;

	public Long getCommissionId() {
		return commissionId;
	}

	public void setCommissionId(Long commissionId) {
		this.commissionId = commissionId;
	}

	public Long getTitleId() {
		return titleId;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public List<Long> getSuperiorIds() {
		return superiorIds;
	}

	public void setSuperiorIds(List<Long> superiorIds) {
		this.superiorIds = superiorIds;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getSuperiorCodes() {
		return superiorCodes;
	}

	public void setSuperiorCodes(List<String> superiorCodes) {
		this.superiorCodes = superiorCodes;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public List<Long> getSuperiorAjaxIds() {
		return superiorAjaxIds;
	}

	public void setSuperiorAjaxIds(List<Long> superiorAjaxIds) {
		this.superiorAjaxIds = superiorAjaxIds;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public Boolean getViewStatus() {
		return viewStatus;
	}

	public void setViewStatus(Boolean viewStatus) {
		this.viewStatus = viewStatus;
	}

	public List<String> getSuperiors() {
		return superiors;
	}

	public void setSuperiors(List<String> superiors) {
		this.superiors = superiors;
	}
	
}
