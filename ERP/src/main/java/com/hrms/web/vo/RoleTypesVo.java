package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 1-April-2015
 */
public class RoleTypesVo {

	private Long roleTypeId;

	private String roleType;

	/**
	 * @return the roleTypeId
	 */
	public Long getRoleTypeId() {
		return roleTypeId;
	}

	/**
	 * @param roleTypeId the roleTypeId to set
	 */
	public void setRoleTypeId(Long roleTypeId) {
		this.roleTypeId = roleTypeId;
	}

	/**
	 * @return the roleType
	 */
	public String getRoleType() {
		return roleType;
	}

	/**
	 * @param roleType the roleType to set
	 */
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
}
