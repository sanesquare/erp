package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 29-March-2015
 *
 */
public class HolidayVo {

	private Long holidayId;

	private String branch;

	private String title;

	private String startDate;

	private String endDate;

	private String description;

	private String note;

	private String recordedOn;

	private String recordedBy;

	/**
	 * 
	 * @return
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * 
	 * @param branch
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * 
	 * @return
	 */
	public Long getHolidayId() {
		return holidayId;
	}

	/**
	 * 
	 * @param holidayId
	 */
	public void setHolidayId(Long holidayId) {
		this.holidayId = holidayId;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * 
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * 
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNote() {
		return note;
	}

	/**
	 * 
	 * @param note
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

}
