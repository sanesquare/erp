package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public class ReminderStatusVo {

	private Long reminderStatusId;

	private String status;

	private String tempStatus;

	/**
	 * 
	 * @return
	 */
	public String getTempStatus() {
		return tempStatus;
	}

	/**
	 * 
	 * @param tempStatus
	 */
	public void setTempStatus(String tempStatus) {
		this.tempStatus = tempStatus;
	}

	/**
	 * 
	 * @return
	 */
	public Long getReminderStatusId() {
		return reminderStatusId;
	}

	/**
	 * 
	 * @param reminderStatusId
	 */
	public void setReminderStatusId(Long reminderStatusId) {
		this.reminderStatusId = reminderStatusId;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
