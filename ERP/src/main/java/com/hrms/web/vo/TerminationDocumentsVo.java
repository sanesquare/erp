package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
public class TerminationDocumentsVo {

	private Long ter_doc_id;

	private String documentName;

	private String documentUrl;

	/**
	 * 
	 * @return
	 */
	public Long getTer_doc_id() {
		return ter_doc_id;
	}

	/**
	 * 
	 * @param ter_doc_id
	 */
	public void setTer_doc_id(Long ter_doc_id) {
		this.ter_doc_id = ter_doc_id;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * 
	 * @param documentName
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentUrl() {
		return documentUrl;
	}

	/**
	 * 
	 * @param documentUrl
	 */
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

}
