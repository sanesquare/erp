package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 24-March-2015
 *
 */
public class PerformanceEvaluationQuestionVo {

	private Long questionId;

	private String documentName;

	private String documentUrl;

	/**
	 * 
	 * @return
	 */
	public Long getQuestionId() {
		return questionId;
	}

	/**
	 * 
	 * @param questionId
	 */
	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * 
	 * @param documentName
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentUrl() {
		return documentUrl;
	}

	/**
	 * 
	 * @param documentUrl
	 */
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

}
