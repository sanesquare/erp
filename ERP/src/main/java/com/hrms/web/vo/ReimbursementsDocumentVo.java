package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 16-April-2015
 */
public class ReimbursementsDocumentVo {

	private Long reimbursementId;
	
	private Long documentId;
	
	private String fileName;
	
	private String url;

	public Long getReimbursementId() {
		return reimbursementId;
	}

	public void setReimbursementId(Long reimbursementId) {
		this.reimbursementId = reimbursementId;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
