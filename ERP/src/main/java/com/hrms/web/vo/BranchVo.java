package com.hrms.web.vo;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.entities.BranchDocuments;

/**
 * 
 * @author Vinutha
 * @since 12-March-2015
 *
 */
public class BranchVo {

	private Long branchId;

	private String branchName;

	private Long parentBranchId;

	private String parentBranch;
	
	private String startDate;

	private String branchType;

	private String timeZoneComplete;

	private String timeZoneOffset;

	private String TimeZoneLocation;

	private String currency;

	private String address;

	private String city;

	private String state;

	private String zipCode;

	private String country;

	private String phoneNumber;

	private String faxNumber;

	private String eMail;

	private String webSite;

	private String latitude;

	private String longitude;

	private String notes;

	private String createdOn;

	private String createdBy;

	private String documentsPath;

	private String newBranchTypeName;

	private String documentAccessUrl;

	private String documentName;

	private Set<BranchDocumentVo> branchDocuments;

	private List<MultipartFile> branchUploadFiles;

	/**
	 * @return the branchId
	 */
	public Long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId
	 *            the branchId to set
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * @param branchName
	 *            the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * @return the parentBranchId
	 */
	public Long getParentBranchId() {
		return parentBranchId;
	}

	/**
	 * @param parentBranchId
	 *            the parentBranchId to set
	 */
	public void setParentBranchId(Long parentBranchId) {
		this.parentBranchId = parentBranchId;
	}

	/**
	 * @return the parentBranch
	 */
	public String getParentBranch() {
		return parentBranch;
	}

	/**
	 * @param parentBranch
	 *            the parentBranch to set
	 */
	public void setParentBranch(String parentBranch) {
		this.parentBranch = parentBranch;
	}

	/**
	 * @return the branchType
	 */
	public String getBranchType() {
		return branchType;
	}

	/**
	 * @param branchType
	 *            the branchType to set
	 */
	public void setBranchType(String branchType) {
		this.branchType = branchType;
	}

	/**
	 * @return the timeZoneComplete
	 */
	public String getTimeZoneComplete() {
		return timeZoneComplete;
	}

	/**
	 * @param timeZoneComplete
	 *            the timeZoneComplete to set
	 */
	public void setTimeZoneComplete(String timeZoneComplete) {
		this.timeZoneComplete = timeZoneComplete;
	}

	/**
	 * @return the timeZoneOffset
	 */
	public String getTimeZoneOffset() {
		return timeZoneOffset;
	}

	/**
	 * @param timeZoneOffset
	 *            the timeZoneOffset to set
	 */
	public void setTimeZoneOffset(String timeZoneOffset) {
		this.timeZoneOffset = timeZoneOffset;
	}

	/**
	 * @return the timeZoneLocation
	 */
	public String getTimeZoneLocation() {
		return TimeZoneLocation;
	}

	/**
	 * @param timeZoneLocation
	 *            the timeZoneLocation to set
	 */
	public void setTimeZoneLocation(String timeZoneLocation) {
		TimeZoneLocation = timeZoneLocation;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the faxNumber
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * @param faxNumber
	 *            the faxNumber to set
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * @return the eMail
	 */
	public String geteMail() {
		return eMail;
	}

	/**
	 * @param eMail
	 *            the eMail to set
	 */
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	/**
	 * @return the webSite
	 */
	public String getWebSite() {
		return webSite;
	}

	/**
	 * @param webSite
	 *            the webSite to set
	 */
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the documentsPath
	 */
	public String getDocumentsPath() {
		return documentsPath;
	}

	/**
	 * @param documentsPath
	 *            the documentsPath to set
	 */
	public void setDocumentsPath(String documentsPath) {
		this.documentsPath = documentsPath;
	}

	/**
	 * @return the newBranchTypeName
	 */
	public String getNewBranchTypeName() {
		return newBranchTypeName;
	}

	/**
	 * @param newBranchTypeName
	 *            the newBranchTypeName to set
	 */
	public void setNewBranchTypeName(String newBranchTypeName) {
		this.newBranchTypeName = newBranchTypeName;
	}

	/**
	 * @return the documentAccessUrl
	 */
	public String getDocumentAccessUrl() {
		return documentAccessUrl;
	}

	/**
	 * @param documentAccessUrl
	 *            the documentAccessUrl to set
	 */
	public void setDocumentAccessUrl(String documentAccessUrl) {
		this.documentAccessUrl = documentAccessUrl;
	}

	/**
	 * @return the documentName
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * @param documentName
	 *            the documentName to set
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * @return the branchUploadFiles
	 */
	public List<MultipartFile> getBranchUploadFiles() {
		return branchUploadFiles;
	}

	/**
	 * @param branchUploadFiles
	 *            the branchUploadFiles to set
	 */
	public void setBranchUploadFiles(List<MultipartFile> branchUploadFiles) {
		this.branchUploadFiles = branchUploadFiles;
	}

	/**
	 * @return the branchDocuments
	 */
	public Set<BranchDocumentVo> getBranchDocuments() {
		return branchDocuments;
	}

	/**
	 * @param branchDocuments
	 *            the branchDocuments to set
	 */
	public void setBranchDocuments(Set<BranchDocumentVo> branchDocuments) {
		this.branchDocuments = branchDocuments;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

}
