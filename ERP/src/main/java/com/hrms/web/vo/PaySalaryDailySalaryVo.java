package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author Shamsheer
 * @since 10-June-2015
 */
public class PaySalaryDailySalaryVo {

	private Boolean isPaid=false;
	
	private String checkBoxValue;
	
	private Long dailySalaryId;
	
	private int slno;

	private Date date;

	private String empName;

	private int days;

	private BigDecimal dailyRate;

	private BigDecimal amount;

	private String dateString;

	private String employeeCode;

	private String dailyWageDate;

	public int getSlno() {
		return slno;
	}

	public void setSlno(int slno) {
		this.slno = slno;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public BigDecimal getDailyRate() {
		return dailyRate;
	}

	public void setDailyRate(BigDecimal dailyRate) {
		this.dailyRate = dailyRate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getDailyWageDate() {
		return dailyWageDate;
	}

	public void setDailyWageDate(String dailyWageDate) {
		this.dailyWageDate = dailyWageDate;
	}

	public Long getDailySalaryId() {
		return dailySalaryId;
	}

	public void setDailySalaryId(Long dailySalaryId) {
		this.dailySalaryId = dailySalaryId;
	}

	public String getCheckBoxValue() {
		return checkBoxValue;
	}

	public void setCheckBoxValue(String checkBoxValue) {
		this.checkBoxValue = checkBoxValue;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}
}
