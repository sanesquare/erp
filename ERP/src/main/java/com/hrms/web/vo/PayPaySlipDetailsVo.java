package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 5-May-2015
 *
 */
public class PayPaySlipDetailsVo {

	private String adjustment;

	private String month;

	private BigDecimal adjAmount;

	private BigDecimal totalEarnings = new BigDecimal("0.00");

	private BigDecimal totalDeductions = new BigDecimal("0.00");

	private BigDecimal netPay = new BigDecimal("0.00");

	private BigDecimal ctc = new BigDecimal("0.00");

	private Double lop;

	private Long totalDays;

	private Long actualDays;

	private LinkedHashMap<String, BigDecimal> detail = new LinkedHashMap<String, BigDecimal>();

	private LinkedHashMap<String, BigDecimal> deductionDetail = new LinkedHashMap<String, BigDecimal>();

	public String getAdjustment() {
		return adjustment;
	}

	public void setAdjustment(String adjustment) {
		this.adjustment = adjustment;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public BigDecimal getAdjAmount() {
		return adjAmount;
	}

	public void setAdjAmount(BigDecimal adjAmount) {
		this.adjAmount = adjAmount;
	}

	public BigDecimal getTotalEarnings() {
		return totalEarnings;
	}

	public void setTotalEarnings(BigDecimal totalEarnings) {
		this.totalEarnings = totalEarnings;
	}

	public BigDecimal getTotalDeductions() {
		return totalDeductions;
	}

	public void setTotalDeductions(BigDecimal totalDeductions) {
		this.totalDeductions = totalDeductions;
	}

	public BigDecimal getNetPay() {
		return netPay;
	}

	public void setNetPay(BigDecimal netPay) {
		this.netPay = netPay;
	}

	public BigDecimal getCtc() {
		return ctc;
	}

	public void setCtc(BigDecimal ctc) {
		this.ctc = ctc;
	}

	public Long getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(Long totalDays) {
		this.totalDays = totalDays;
	}

	public Long getActualDays() {
		return actualDays;
	}

	public void setActualDays(Long actualDays) {
		this.actualDays = actualDays;
	}

	public LinkedHashMap<String, BigDecimal> getDetail() {
		return detail;
	}

	public void setDetail(LinkedHashMap<String, BigDecimal> detail) {
		this.detail = detail;
	}

	public LinkedHashMap<String, BigDecimal> getDeductionDetail() {
		return deductionDetail;
	}

	public void setDeductionDetail(LinkedHashMap<String, BigDecimal> deductionDetail) {
		this.deductionDetail = deductionDetail;
	}

	
	public Double getLop() {
		return lop;
	}

	public void setLop(Double lop) {
		this.lop = lop;
	}

	

}
