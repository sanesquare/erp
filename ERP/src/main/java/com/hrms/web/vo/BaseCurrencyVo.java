package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class BaseCurrencyVo {

	private Long baseCurrencyId;

	private String baseCurrency;

	private String sign;

	/**
	 * 
	 * @return
	 */
	public Long getBaseCurrencyId() {
		return baseCurrencyId;
	}

	/**
	 * 
	 * @param baseCurrencyId
	 */
	public void setBaseCurrencyId(Long baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}

	/**
	 * 
	 * @return
	 */
	public String getBaseCurrency() {
		return baseCurrency;
	}

	/**
	 * 
	 * @param baseCurrency
	 */
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	/**
	 * 
	 * @return
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * 
	 * @param sign
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}

}
