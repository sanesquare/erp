package com.hrms.web.vo;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.entities.Project;
import com.hrms.web.entities.ProjectDocuments;

/**
 * 
 * @author Shamsheer
 * @since 10-March-2015
 *
 */
public class ProjectVo {

	private Long branchId;
	
	private Long departmentID;
	
	private Long projectId;
	
	private List<String> employees;
	
	private List<String> projectHeads;
	
	private List<Long> headIds;
	
	private String projectTitle;
	
	private String projectName;
	
	private String projectStartDate;
	
	private Project project;
	
	private String projectEndDate;
	
	private String projectDescription;
	
	private String projectStatus;
	
	private String documentAccessUrl;
	
	private String documentName;
	
	private List<ProjectDocumentsVo> projectDocuments;
	
	private String projectAdditionalInformation;
	
	private List<Long> employeesIds;
	
	private List<String> employeesCodes;
	
	private List<String> headNames;
	
	private List<String> headCodes;
	
	private Long projectStatusId;
	
	private String createdOn;
	
	private String updatedOn;
	
	private String createdBy;
	
	private String updatedBy;
	
	private List<MultipartFile> project_file_upload;
	
	

	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}


	/**
	 * @return the employeesIds
	 */
	public List<Long> getEmployeesIds() {
		return employeesIds;
	}

	/**
	 * @param employeesIds the employeesIds to set
	 */
	public void setEmployeesIds(List<Long> employeesIds) {
		this.employeesIds = employeesIds;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public String getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the projectId
	 */
	public Long getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the projectTitle
	 */
	public String getProjectTitle() {
		return projectTitle;
	}

	/**
	 * @param projectTitle the projectTitle to set
	 */
	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the projectStartDate
	 */
	public String getProjectStartDate() {
		return projectStartDate;
	}

	/**
	 * @param projectStartDate the projectStartDate to set
	 */
	public void setProjectStartDate(String projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	/**
	 * @return the projectEndDate
	 */
	public String getProjectEndDate() {
		return projectEndDate;
	}

	/**
	 * @param projectEndDate the projectEndDate to set
	 */
	public void setProjectEndDate(String projectEndDate) {
		this.projectEndDate = projectEndDate;
	}

	/**
	 * @return the projectDescription
	 */
	public String getProjectDescription() {
		return projectDescription;
	}

	/**
	 * @param projectDescription the projectDescription to set
	 */
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	/**
	 * @return the projectStatus
	 */
	public String getProjectStatus() {
		return projectStatus;
	}

	/**
	 * @param projectStatus the projectStatus to set
	 */
	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	/**
	 * @return the projectAdditionalInformation
	 */
	public String getProjectAdditionalInformation() {
		return projectAdditionalInformation;
	}

	/**
	 * @param projectAdditionalInformation the projectAdditionalInformation to set
	 */
	public void setProjectAdditionalInformation(String projectAdditionalInformation) {
		this.projectAdditionalInformation = projectAdditionalInformation;
	}


	/**
	 * @return the employeesCodes
	 */
	public List<String> getEmployeesCodes() {
		return employeesCodes;
	}

	/**
	 * @param employeesCodes the employeesCodes to set
	 */
	public void setEmployeesCodes(List<String> employeesCodes) {
		this.employeesCodes = employeesCodes;
	}

	/**
	 * @return the projectStatusId
	 */
	public Long getProjectStatusId() {
		return projectStatusId;
	}

	/**
	 * @param projectStatusId the projectStatusId to set
	 */
	public void setProjectStatusId(Long projectStatusId) {
		this.projectStatusId = projectStatusId;
	}



	/**
	 * @return the projectDocuments
	 */
	public List<ProjectDocumentsVo> getProjectDocuments() {
		return projectDocuments;
	}

	/**
	 * @param projectDocuments the projectDocuments to set
	 */
	public void setProjectDocuments(List<ProjectDocumentsVo> projectDocuments) {
		this.projectDocuments = projectDocuments;
	}

	/**
	 * @return the documentAccessUrl
	 */
	public String getDocumentAccessUrl() {
		return documentAccessUrl;
	}

	/**
	 * @param documentAccessUrl the documentAccessUrl to set
	 */
	public void setDocumentAccessUrl(String documentAccessUrl) {
		this.documentAccessUrl = documentAccessUrl;
	}

	/**
	 * @return the documentName
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * @param documentName the documentName to set
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * @return the branchId
	 */
	public Long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the departmentID
	 */
	public Long getDepartmentID() {
		return departmentID;
	}

	/**
	 * @param departmentID the departmentID to set
	 */
	public void setDepartmentID(Long departmentID) {
		this.departmentID = departmentID;
	}

	public List<MultipartFile> getProject_file_upload() {
		return project_file_upload;
	}

	public void setProject_file_upload(List<MultipartFile> project_file_upload) {
		this.project_file_upload = project_file_upload;
	}

	public List<String> getEmployees() {
		return employees;
	}

	public void setEmployees(List<String> employees) {
		this.employees = employees;
	}

	public List<String> getProjectHeads() {
		return projectHeads;
	}

	public void setProjectHeads(List<String> projectHeads) {
		this.projectHeads = projectHeads;
	}

	public List<Long> getHeadIds() {
		return headIds;
	}

	public void setHeadIds(List<Long> headIds) {
		this.headIds = headIds;
	}

	public List<String> getHeadNames() {
		return headNames;
	}

	public void setHeadNames(List<String> headNames) {
		this.headNames = headNames;
	}

	public List<String> getHeadCodes() {
		return headCodes;
	}

	public void setHeadCodes(List<String> headCodes) {
		this.headCodes = headCodes;
	}

}
