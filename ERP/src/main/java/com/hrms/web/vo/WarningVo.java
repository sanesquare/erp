package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Jithin Mohan
 * @since 20-March-2015
 *
 */
public class WarningVo {

	private Long warningId;

	private String employee;

	private String employeeCode;

	private List<String> forwaders;

	private List<String> forwadersName;

	private String warningOn;

	private String subject;

	private String description;

	private String notes;

	private String recordedOn;

	private String recordedBy;

	private List<MultipartFile> warningDocuments;

	private List<WarningDocumentsVo> documents;

	/**
	 * 
	 * @return
	 */
	public List<String> getForwadersName() {
		return forwadersName;
	}

	/**
	 * 
	 * @param forwadersName
	 */
	public void setForwadersName(List<String> forwadersName) {
		this.forwadersName = forwadersName;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * 
	 * @param employeeCode
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * 
	 * @return
	 */
	public Long getWarningId() {
		return warningId;
	}

	/**
	 * 
	 * @param warningId
	 */
	public void setWarningId(Long warningId) {
		this.warningId = warningId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getForwaders() {
		return forwaders;
	}

	/**
	 * 
	 * @param forwaders
	 */
	public void setForwaders(List<String> forwaders) {
		this.forwaders = forwaders;
	}

	/**
	 * 
	 * @return
	 */
	public String getWarningOn() {
		return warningOn;
	}

	/**
	 * 
	 * @param warningOn
	 */
	public void setWarningOn(String warningOn) {
		this.warningOn = warningOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * 
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

	/**
	 * 
	 * @return
	 */
	public List<MultipartFile> getWarningDocuments() {
		return warningDocuments;
	}

	/**
	 * 
	 * @param warningDocuments
	 */
	public void setWarningDocuments(List<MultipartFile> warningDocuments) {
		this.warningDocuments = warningDocuments;
	}

	/**
	 * 
	 * @return
	 */
	public List<WarningDocumentsVo> getDocuments() {
		return documents;
	}

	/**
	 * 
	 * @param documents
	 */
	public void setDocuments(List<WarningDocumentsVo> documents) {
		this.documents = documents;
	}

}
