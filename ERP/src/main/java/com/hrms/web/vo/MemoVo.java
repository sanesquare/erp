package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Jithin Mohan
 * @since 22-March-2015
 *
 */
public class MemoVo {

	private Long memoId;

	private String employee;

	private String employeeCode;

	private List<String> forwaders;

	private List<String> forwadersName;

	private String memoOn;

	private String memoSubject;

	private String description;

	private String notes;

	private String recordedOn;

	private String recordedBy;

	private List<MemoDocumentVo> documents;

	private List<MultipartFile> memoDocuments;

	/**
	 * 
	 * @return
	 */
	public List<MemoDocumentVo> getDocuments() {
		return documents;
	}

	/**
	 * 
	 * @param documents
	 */
	public void setDocuments(List<MemoDocumentVo> documents) {
		this.documents = documents;
	}

	/**
	 * 
	 * @return
	 */
	public List<MultipartFile> getMemoDocuments() {
		return memoDocuments;
	}

	/**
	 * 
	 * @param memoDocuments
	 */
	public void setMemoDocuments(List<MultipartFile> memoDocuments) {
		this.memoDocuments = memoDocuments;
	}

	/**
	 * 
	 * @return
	 */
	public Long getMemoId() {
		return memoId;
	}

	/**
	 * 
	 * @param memoId
	 */
	public void setMemoId(Long memoId) {
		this.memoId = memoId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * 
	 * @param employeeCode
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getForwaders() {
		return forwaders;
	}

	/**
	 * 
	 * @param forwaders
	 */
	public void setForwaders(List<String> forwaders) {
		this.forwaders = forwaders;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getForwadersName() {
		return forwadersName;
	}

	/**
	 * 
	 * @param forwadersName
	 */
	public void setForwadersName(List<String> forwadersName) {
		this.forwadersName = forwadersName;
	}

	/**
	 * 
	 * @return
	 */
	public String getMemoOn() {
		return memoOn;
	}

	/**
	 * 
	 * @param memoOn
	 */
	public void setMemoOn(String memoOn) {
		this.memoOn = memoOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getMemoSubject() {
		return memoSubject;
	}

	/**
	 * 
	 * @param memoSubject
	 */
	public void setMemoSubject(String memoSubject) {
		this.memoSubject = memoSubject;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

}
