package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class PolicyTypeVo {

	private Long policyId;

	private String policyType;

	private String tempPolicyType;

	/**
	 * 
	 * @return
	 */
	public String getTempPolicyType() {
		return tempPolicyType;
	}

	/**
	 * 
	 * @param tempPolicyType
	 */
	public void setTempPolicyType(String tempPolicyType) {
		this.tempPolicyType = tempPolicyType;
	}

	/**
	 * 
	 * @return
	 */
	public Long getPolicyId() {
		return policyId;
	}

	/**
	 * 
	 * @param policyId
	 */
	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	/**
	 * 
	 * @return
	 */
	public String getPolicyType() {
		return policyType;
	}

	/**
	 * 
	 * @param policyType
	 */
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

}
