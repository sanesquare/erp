package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
public class TravelVo {

	private Boolean viewStatus;
	
	private String superiorCodeAjax;
	
	private Long travelId;
	
	private Long employeeId;
	
	private Long superiorId;
	
	private String superiorCode;
	
	private String purposeOfVisit;
	
	private String employeeCode;
	
	private String employee;
	
	private String description;
	
	private String startDate;
	
	private String endDate;
	
	private String expectedBudget;
	
	private String actualBudget;
	
	private String notes;
	
	private Long statusId;
	
	private String status;
	
	private String superior;
	
	private String statusDescription;
	
	private String createdBy;
	
	private String createdOn;
	
	private List<TravelDestinationVo> destinationVos;
	
	private List<TravelDocumentVo> documentVos;
	
	private List<MultipartFile> travelDocuments;

	public Long getTravelId() {
		return travelId;
	}

	public void setTravelId(Long travelId) {
		this.travelId = travelId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getSuperiorId() {
		return superiorId;
	}

	public void setSuperiorId(Long superiorId) {
		this.superiorId = superiorId;
	}

	public String getPurposeOfVisit() {
		return purposeOfVisit;
	}

	public void setPurposeOfVisit(String purposeOfVisit) {
		this.purposeOfVisit = purposeOfVisit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getExpectedBudget() {
		return expectedBudget;
	}

	public void setExpectedBudget(String expectedBudget) {
		this.expectedBudget = expectedBudget;
	}

	public String getActualBudget() {
		return actualBudget;
	}

	public void setActualBudget(String actualBudget) {
		this.actualBudget = actualBudget;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public List<TravelDestinationVo> getDestinationVos() {
		return destinationVos;
	}

	public void setDestinationVos(List<TravelDestinationVo> destinationVos) {
		this.destinationVos = destinationVos;
	}

	public List<TravelDocumentVo> getDocumentVos() {
		return documentVos;
	}

	public void setDocumentVos(List<TravelDocumentVo> documentVos) {
		this.documentVos = documentVos;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getSuperiorCode() {
		return superiorCode;
	}

	public void setSuperiorCode(String superiorCode) {
		this.superiorCode = superiorCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSuperior() {
		return superior;
	}

	public void setSuperior(String superior) {
		this.superior = superior;
	}

	public List<MultipartFile> getTravelDocuments() {
		return travelDocuments;
	}

	public void setTravelDocuments(List<MultipartFile> travelDocuments) {
		this.travelDocuments = travelDocuments;
	}

	public String getSuperiorCodeAjax() {
		return superiorCodeAjax;
	}

	public void setSuperiorCodeAjax(String superiorCodeAjax) {
		this.superiorCodeAjax = superiorCodeAjax;
	}

	public Boolean getViewStatus() {
		return viewStatus;
	}

	public void setViewStatus(Boolean viewStatus) {
		this.viewStatus = viewStatus;
	}
}
