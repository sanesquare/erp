package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public class TaxRuleVo {

	private Long taxRuleId;

	private BigDecimal salaryFrom;

	private BigDecimal salaryTo;

	private BigDecimal taxPercentage;

	private BigDecimal exemptedAmount;

	private BigDecimal additionalAmount;

	private BigDecimal rowTax;

	private String individual;

	/**
	 * 
	 * @return
	 */
	public BigDecimal getRowTax() {
		return rowTax;
	}

	/**
	 * 
	 * @param rowTax
	 */
	public void setRowTax(BigDecimal rowTax) {
		this.rowTax = rowTax;
	}

	/**
	 * 
	 * @return
	 */
	public Long getTaxRuleId() {
		return taxRuleId;
	}

	/**
	 * 
	 * @param taxRuleId
	 */
	public void setTaxRuleId(Long taxRuleId) {
		this.taxRuleId = taxRuleId;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getSalaryFrom() {
		return salaryFrom;
	}

	/**
	 * 
	 * @param salaryFrom
	 */
	public void setSalaryFrom(BigDecimal salaryFrom) {
		this.salaryFrom = salaryFrom;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getSalaryTo() {
		return salaryTo;
	}

	/**
	 * 
	 * @param salaryTo
	 */
	public void setSalaryTo(BigDecimal salaryTo) {
		this.salaryTo = salaryTo;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getTaxPercentage() {
		return taxPercentage;
	}

	/**
	 * 
	 * @param taxPercentage
	 */
	public void setTaxPercentage(BigDecimal taxPercentage) {
		this.taxPercentage = taxPercentage;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getExemptedAmount() {
		return exemptedAmount;
	}

	/**
	 * 
	 * @param exemptedAmount
	 */
	public void setExemptedAmount(BigDecimal exemptedAmount) {
		this.exemptedAmount = exemptedAmount;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getAdditionalAmount() {
		return additionalAmount;
	}

	/**
	 * 
	 * @param additionalAmount
	 */
	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}

	/**
	 * 
	 * @return
	 */
	public String getIndividual() {
		return individual;
	}

	/**
	 * 
	 * @param individual
	 */
	public void setIndividual(String individual) {
		this.individual = individual;
	}

}
