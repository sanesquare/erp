package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public class LoanVo {

	private Long loanId;

	private Boolean isView = false;

	private String employee;

	private String employeeCode;

	private List<String> forwardTo;

	private List<String> forwardToNames;

	private String loanTitle;

	private BigDecimal loanAmount;

	private Integer pendingInstallments;

	private Integer noOfInstallments;

	private String loanDate;

	private String salaryType;

	private BigDecimal repaymentAmount;

	private String repaymentStartDate;

	private String description;

	private String notes;

	private String status;

	private String statusDescription;

	private String recordedBy;

	private String recordedOn;

	private List<MultipartFile> documents;

	private List<LoanDocumentVo> loanDocuments;

	private BigDecimal balanceAmount;

	/**
	 * 
	 * @return
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * 
	 * @param employeeCode
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getForwardToNames() {
		return forwardToNames;
	}

	/**
	 * 
	 * @param forwardToNames
	 */
	public void setForwardToNames(List<String> forwardToNames) {
		this.forwardToNames = forwardToNames;
	}

	/**
	 * 
	 * @return
	 */
	public Long getLoanId() {
		return loanId;
	}

	/**
	 * 
	 * @param loanId
	 */
	public void setLoanId(Long loanId) {
		this.loanId = loanId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getForwardTo() {
		return forwardTo;
	}

	/**
	 * 
	 * @param forwardTo
	 */
	public void setForwardTo(List<String> forwardTo) {
		this.forwardTo = forwardTo;
	}

	/**
	 * 
	 * @return
	 */
	public String getLoanTitle() {
		return loanTitle;
	}

	/**
	 * 
	 * @param loanTitle
	 */
	public void setLoanTitle(String loanTitle) {
		this.loanTitle = loanTitle;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getLoanAmount() {
		return loanAmount;
	}

	/**
	 * 
	 * @param loanAmount
	 */
	public void setLoanAmount(BigDecimal loanAmount) {
		this.loanAmount = loanAmount;
	}

	/**
	 * 
	 * @return
	 */
	public String getLoanDate() {
		return loanDate;
	}

	/**
	 * 
	 * @param loanDate
	 */
	public void setLoanDate(String loanDate) {
		this.loanDate = loanDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getSalaryType() {
		return salaryType;
	}

	/**
	 * 
	 * @param salaryType
	 */
	public void setSalaryType(String salaryType) {
		this.salaryType = salaryType;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getRepaymentAmount() {
		return repaymentAmount;
	}

	/**
	 * 
	 * @param repaymentAmount
	 */
	public void setRepaymentAmount(BigDecimal repaymentAmount) {
		this.repaymentAmount = repaymentAmount;
	}

	/**
	 * 
	 * @return
	 */
	public String getRepaymentStartDate() {
		return repaymentStartDate;
	}

	/**
	 * 
	 * @param repaymentStartDate
	 */
	public void setRepaymentStartDate(String repaymentStartDate) {
		this.repaymentStartDate = repaymentStartDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * 
	 * @param statusDescription
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	/**
	 * 
	 * @return
	 */
	public List<MultipartFile> getDocuments() {
		return documents;
	}

	/**
	 * 
	 * @param documents
	 */
	public void setDocuments(List<MultipartFile> documents) {
		this.documents = documents;
	}

	/**
	 * 
	 * @return
	 */
	public List<LoanDocumentVo> getLoanDocuments() {
		return loanDocuments;
	}

	/**
	 * 
	 * @param loanDocuments
	 */
	public void setLoanDocuments(List<LoanDocumentVo> loanDocuments) {
		this.loanDocuments = loanDocuments;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public Integer getNoOfInstallments() {
		return noOfInstallments;
	}

	public void setNoOfInstallments(Integer noOfInstallments) {
		this.noOfInstallments = noOfInstallments;
	}

	public Integer getPendingInstallments() {
		return pendingInstallments;
	}

	public void setPendingInstallments(Integer pendingInstallments) {
		this.pendingInstallments = pendingInstallments;
	}

	public Boolean getIsView() {
		return isView;
	}

	public void setIsView(Boolean isView) {
		this.isView = isView;
	}

}
