package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 20-April-2015
 *
 */
public class RecInterviewReportViewVo {

	private List<RecInterviewReportDetailsVo> detailsVo;
	
	private String type;

	public List<RecInterviewReportDetailsVo> getDetailsVo() {
		return detailsVo;
	}

	public void setDetailsVo(List<RecInterviewReportDetailsVo> detailsVo) {
		this.detailsVo = detailsVo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
