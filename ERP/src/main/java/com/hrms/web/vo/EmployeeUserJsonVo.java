package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 2-April-2015
 */
public class EmployeeUserJsonVo {

	private Boolean allowLogin;
	
	private Long employeeId;
	
	private String userName;

	private String password;

	private String email;

	public Boolean getAllowLogin() {
		return allowLogin;
	}

	public void setAllowLogin(Boolean allowLogin) {
		this.allowLogin = allowLogin;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
