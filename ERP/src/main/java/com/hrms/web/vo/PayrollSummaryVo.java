package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Vinutha
 * @since 17-April-2015
 *
 */
public class PayrollSummaryVo {

	private BigDecimal totalSalary;
	
	private BigDecimal yearSalary;
	
	private BigDecimal avgSalary;
	
	private String topBranch;
	
	private BigDecimal topBranchAmnt;
	
	private String topDepartment;
	
	private BigDecimal topDepAmnt;
	
	private String topDesignation;
	
	private BigDecimal topDesAmnt;

	/**
	 * @return the totalSalary
	 */
	public BigDecimal getTotalSalary() {
		return totalSalary;
	}

	/**
	 * @param totalSalary the totalSalary to set
	 */
	public void setTotalSalary(BigDecimal totalSalary) {
		this.totalSalary = totalSalary;
	}

	/**
	 * @return the yearSalary
	 */
	public BigDecimal getYearSalary() {
		return yearSalary;
	}

	/**
	 * @param yearSalary the yearSalary to set
	 */
	public void setYearSalary(BigDecimal yearSalary) {
		this.yearSalary = yearSalary;
	}

	/**
	 * @return the avgSalary
	 */
	public BigDecimal getAvgSalary() {
		return avgSalary;
	}

	/**
	 * @param avgSalary the avgSalary to set
	 */
	public void setAvgSalary(BigDecimal avgSalary) {
		this.avgSalary = avgSalary;
	}

	/**
	 * @return the topBranch
	 */
	public String getTopBranch() {
		return topBranch;
	}

	/**
	 * @param topBranch the topBranch to set
	 */
	public void setTopBranch(String topBranch) {
		this.topBranch = topBranch;
	}

	/**
	 * @return the topBranchAmnt
	 */
	public BigDecimal getTopBranchAmnt() {
		return topBranchAmnt;
	}

	/**
	 * @param topBranchAmnt the topBranchAmnt to set
	 */
	public void setTopBranchAmnt(BigDecimal topBranchAmnt) {
		this.topBranchAmnt = topBranchAmnt;
	}

	/**
	 * @return the topDepartment
	 */
	public String getTopDepartment() {
		return topDepartment;
	}

	/**
	 * @param topDepartment the topDepartment to set
	 */
	public void setTopDepartment(String topDepartment) {
		this.topDepartment = topDepartment;
	}

	/**
	 * @return the topDepAmnt
	 */
	public BigDecimal getTopDepAmnt() {
		return topDepAmnt;
	}

	/**
	 * @param topDepAmnt the topDepAmnt to set
	 */
	public void setTopDepAmnt(BigDecimal topDepAmnt) {
		this.topDepAmnt = topDepAmnt;
	}

	/**
	 * @return the topDesignation
	 */
	public String getTopDesignation() {
		return topDesignation;
	}

	/**
	 * @param topDesignation the topDesignation to set
	 */
	public void setTopDesignation(String topDesignation) {
		this.topDesignation = topDesignation;
	}

	/**
	 * @return the topDesAmnt
	 */
	public BigDecimal getTopDesAmnt() {
		return topDesAmnt;
	}

	/**
	 * @param topDesAmnt the topDesAmnt to set
	 */
	public void setTopDesAmnt(BigDecimal topDesAmnt) {
		this.topDesAmnt = topDesAmnt;
	}
	
	
}
