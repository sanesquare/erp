package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Jithin Mohan
 * @since 10-April-2015
 *
 */
public class InsuranceVo {

	private Long insuranceId;

	private String employeeCode;

	private String employee;

	private String insuranceType;

	private String insuranceTitle;

	private BigDecimal employeeShare;

	private BigDecimal organizationShare;

	private String description;

	private String notes;

	private String recordedOn;

	private String recordedBy;

	private EmployeePopupVo employeePopup;

	/**
	 * 
	 * @return
	 */
	public EmployeePopupVo getEmployeePopup() {
		return employeePopup;
	}

	/**
	 * 
	 * @param employeePopup
	 */
	public void setEmployeePopup(EmployeePopupVo employeePopup) {
		this.employeePopup = employeePopup;
	}

	/**
	 * 
	 * @return
	 */
	public Long getInsuranceId() {
		return insuranceId;
	}

	/**
	 * 
	 * @param insuranceId
	 */
	public void setInsuranceId(Long insuranceId) {
		this.insuranceId = insuranceId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * 
	 * @param employeeName
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * 
	 * @return
	 */
	public String getInsuranceType() {
		return insuranceType;
	}

	/**
	 * 
	 * @param insuranceType
	 */
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	/**
	 * 
	 * @return
	 */
	public String getInsuranceTitle() {
		return insuranceTitle;
	}

	/**
	 * 
	 * @param insuranceTitle
	 */
	public void setInsuranceTitle(String insuranceTitle) {
		this.insuranceTitle = insuranceTitle;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getEmployeeShare() {
		return employeeShare;
	}

	/**
	 * 
	 * @param employeeShare
	 */
	public void setEmployeeShare(BigDecimal employeeShare) {
		this.employeeShare = employeeShare;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getOrganizationShare() {
		return organizationShare;
	}

	/**
	 * 
	 * @param organizationShare
	 */
	public void setOrganizationShare(BigDecimal organizationShare) {
		this.organizationShare = organizationShare;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

}
