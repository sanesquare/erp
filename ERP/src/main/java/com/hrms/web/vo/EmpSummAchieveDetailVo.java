package com.hrms.web.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 7-May-2015
 *
 */
public class EmpSummAchieveDetailVo {

	private int slno;
	
	private String title;
	
	@DateTimeFormat
	private Date achieveDate;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the achieveDate
	 */
	public Date getAchieveDate() {
		return achieveDate;
	}

	/**
	 * @param achieveDate the achieveDate to set
	 */
	public void setAchieveDate(Date achieveDate) {
		this.achieveDate = achieveDate;
	}
	
	
}
