package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public class BonusDateVo {

	private Long bonusDateId;

	private Integer day;

	private Integer month;

	private Integer year;

	/**
	 * 
	 * @return
	 */
	public Long getBonusDateId() {
		return bonusDateId;
	}

	/**
	 * 
	 * @param bonusDateId
	 */
	public void setBonusDateId(Long bonusDateId) {
		this.bonusDateId = bonusDateId;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getDay() {
		return day;
	}

	/**
	 * 
	 * @param day
	 */
	public void setDay(Integer day) {
		this.day = day;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * 
	 * @param month
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * 
	 * @param year
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

}
