package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 2-April-2015
 */
public class ProjectJsonVo {

	private Long projectId;
	
	private String projectTitle;
	
	private String projectStartDate;
	
	private String projectEndDate;
	
	private String projectDescription;
	
	private String projectAdditionalInformation;
	
	private List<Long> employeesIds;
	
	private List<Long> headIds;
	
	private Long projectStatusId;

	public String getProjectTitle() {
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}


	public String getProjectStartDate() {
		return projectStartDate;
	}

	public void setProjectStartDate(String projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public String getProjectEndDate() {
		return projectEndDate;
	}

	public void setProjectEndDate(String projectEndDate) {
		this.projectEndDate = projectEndDate;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public String getProjectAdditionalInformation() {
		return projectAdditionalInformation;
	}

	public void setProjectAdditionalInformation(String projectAdditionalInformation) {
		this.projectAdditionalInformation = projectAdditionalInformation;
	}

	public List<Long> getEmployeesIds() {
		return employeesIds;
	}

	public void setEmployeesIds(List<Long> employeesIds) {
		this.employeesIds = employeesIds;
	}

	public Long getProjectStatusId() {
		return projectStatusId;
	}

	public void setProjectStatusId(Long projectStatusId) {
		this.projectStatusId = projectStatusId;
	}

	/**
	 * @return the projectId
	 */
	public Long getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public List<Long> getHeadIds() {
		return headIds;
	}

	public void setHeadIds(List<Long> headIds) {
		this.headIds = headIds;
	}
	
}
