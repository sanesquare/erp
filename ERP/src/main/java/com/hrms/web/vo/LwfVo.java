package com.hrms.web.vo;


/**
 * 
 * @author Jithin Mohan
 *
 */
public class LwfVo {

	private Long lwfId;
	private Double amount;

	/**
	 * 
	 * @return
	 */
	public Long getLwfId() {
		return lwfId;
	}

	/**
	 * 
	 * @param lwfId
	 */
	public void setLwfId(Long lwfId) {
		this.lwfId = lwfId;
	}

	/**
	 * 
	 * @return
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
