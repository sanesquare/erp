package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 2-April-2015
 */
public class EmployeeExtraInfoJsonVo {

	private Long employeeId;

	private String passportExpiration;

	private String passportNumber;

	private String licenceExpiration;

	private String pfId;

	private String esiId;

	private String licenceNumber;

	private String goovernmentId;

	private String employeeTaxNumber;

	private String notes;

	private String joiningDate;

	private Boolean notifyByEmail;

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getPassportExpiration() {
		return passportExpiration;
	}

	public void setPassportExpiration(String passportExpiration) {
		this.passportExpiration = passportExpiration;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getLicenceExpiration() {
		return licenceExpiration;
	}

	public void setLicenceExpiration(String licenceExpiration) {
		this.licenceExpiration = licenceExpiration;
	}

	public String getLicenceNumber() {
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}

	public String getGoovernmentId() {
		return goovernmentId;
	}

	public void setGoovernmentId(String goovernmentId) {
		this.goovernmentId = goovernmentId;
	}

	public String getEmployeeTaxNumber() {
		return employeeTaxNumber;
	}

	public void setEmployeeTaxNumber(String employeeTaxNumber) {
		this.employeeTaxNumber = employeeTaxNumber;
	}

	public String getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getNotifyByEmail() {
		return notifyByEmail;
	}

	public void setNotifyByEmail(Boolean notifyByEmail) {
		this.notifyByEmail = notifyByEmail;
	}

	public String getPfId() {
		return pfId;
	}

	public void setPfId(String pfId) {
		this.pfId = pfId;
	}

	public String getEsiId() {
		return esiId;
	}

	public void setEsiId(String esiId) {
		this.esiId = esiId;
	}
}
