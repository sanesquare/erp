package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer
 * @since 25-April-2015
 */
public class SalaryPayslipItemsVo {
	
	private String item;
	
	private BigDecimal amount;
	
	private String stringAmount;

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getStringAmount() {
		return stringAmount;
	}

	public void setStringAmount(String stringAmount) {
		this.stringAmount = stringAmount;
	}

}
