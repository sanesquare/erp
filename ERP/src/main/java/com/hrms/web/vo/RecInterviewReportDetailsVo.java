package com.hrms.web.vo;

import java.sql.Time;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 20-April-2015
 *
 */
public class RecInterviewReportDetailsVo {

	private int slno;
	
	private String jobPost;
	
	private String place;
	//interviewees
	private String interviewers;
	//candidates
	private String statuses;
	
	private int candCount;
	
	private Time intTime;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date intDate;
	
	private String intTimeString;
	
	private String intDateString;

	public int getSlno() {
		return slno;
	}

	public void setSlno(int slno) {
		this.slno = slno;
	}

	public String getJobPost() {
		return jobPost;
	}

	public void setJobPost(String jobPost) {
		this.jobPost = jobPost;
	}

	public Time getIntTime() {
		return intTime;
	}

	public void setIntTime(Time intTime) {
		this.intTime = intTime;
	}

	public Date getIntDate() {
		return intDate;
	}

	public void setIntDate(Date intDate) {
		this.intDate = intDate;
	}

	/**
	 * @return the place
	 */
	public String getPlace() {
		return place;
	}

	/**
	 * @param place the place to set
	 */
	public void setPlace(String place) {
		this.place = place;
	}

	/**
	 * @return the interviewers
	 */
	public String getInterviewers() {
		return interviewers;
	}

	/**
	 * @param interviewers the interviewers to set
	 */
	public void setInterviewers(String interviewers) {
		this.interviewers = interviewers;
	}

	/**
	 * @return the statuses
	 */
	public String getStatuses() {
		return statuses;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(String statuses) {
		this.statuses = statuses;
	}

	/**
	 * @return the candCount
	 */
	public int getCandCount() {
		return candCount;
	}

	/**
	 * @param candCount the candCount to set
	 */
	public void setCandCount(int candCount) {
		this.candCount = candCount;
	}

	/**
	 * @return the intTimeString
	 */
	public String getIntTimeString() {
		return intTimeString;
	}

	/**
	 * @param intTimeString the intTimeString to set
	 */
	public void setIntTimeString(String intTimeString) {
		this.intTimeString = intTimeString;
	}

	/**
	 * @return the intDateString
	 */
	public String getIntDateString() {
		return intDateString;
	}

	/**
	 * @param intDateString the intDateString to set
	 */
	public void setIntDateString(String intDateString) {
		this.intDateString = intDateString;
	}
	
	
	
}
