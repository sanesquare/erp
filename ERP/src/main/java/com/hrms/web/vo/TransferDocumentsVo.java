package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 24-April-2015
 */
public class TransferDocumentsVo {

	private String url;
	
	private String fileName;
	
	private Long id;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
