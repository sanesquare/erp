package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 18-March-2015
 *
 */
public class EmployeeExitDocumentsVo {

	private Long emp_ext_doc_id;

	private String documentName;

	private String documentUrl;

	/**
	 * 
	 * @return
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * 
	 * @param documentName
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * 
	 * @return
	 */
	public Long getEmp_ext_doc_id() {
		return emp_ext_doc_id;
	}

	/**
	 * 
	 * @param emp_ext_doc_id
	 */
	public void setEmp_ext_doc_id(Long emp_ext_doc_id) {
		this.emp_ext_doc_id = emp_ext_doc_id;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentUrl() {
		return documentUrl;
	}

	/**
	 * 
	 * @param documentUrl
	 */
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

}
