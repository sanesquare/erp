package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public class LanguageVo {

	private Long languageId;

	private String language;

	private String tempLanguage;

	/**
	 * 
	 * @return
	 */
	public String getTempLanguage() {
		return tempLanguage;
	}

	/**
	 * 
	 * @param tempLanguage
	 */
	public void setTempLanguage(String tempLanguage) {
		this.tempLanguage = tempLanguage;
	}

	/**
	 * 
	 * @return
	 */
	public Long getLanguageId() {
		return languageId;
	}

	/**
	 * 
	 * @param languageId
	 */
	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	/**
	 * 
	 * @return
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * 
	 * @param language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

}
