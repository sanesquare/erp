package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public class OvertimeStatusVo {

	private Long overtimeStatusId;

	private String status;

	/**
	 * 
	 * @return
	 */
	public Long getOvertimeStatusId() {
		return overtimeStatusId;
	}

	/**
	 * 
	 * @param overtimeStatusId
	 */
	public void setOvertimeStatusId(Long overtimeStatusId) {
		this.overtimeStatusId = overtimeStatusId;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
