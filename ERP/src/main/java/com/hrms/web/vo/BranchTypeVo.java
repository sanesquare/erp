package com.hrms.web.vo;

/**
 * 
 * @author Vinutha
 * @since 12-March-2015
 *
 */
public class BranchTypeVo {

	private Long branchTypeId;

	private String branchTypeName;

	private String tempBranchType;

	/**
	 * 
	 * @return
	 */
	public String getTempBranchType() {
		return tempBranchType;
	}

	/**
	 * 
	 * @param tempBranchType
	 */
	public void setTempBranchType(String tempBranchType) {
		this.tempBranchType = tempBranchType;
	}

	/**
	 * 
	 * @return
	 */
	public Long getBranchTypeId() {
		return branchTypeId;
	}

	/**
	 * 
	 * @param branchTypeId
	 */
	public void setBranchTypeId(Long branchTypeId) {
		this.branchTypeId = branchTypeId;
	}

	/**
	 * @return the branchTypeName
	 */
	public String getBranchTypeName() {
		return branchTypeName;
	}

	/**
	 * @param branchTypeName
	 *            the branchTypeName to set
	 */
	public void setBranchTypeName(String branchTypeName) {
		this.branchTypeName = branchTypeName;
	}

}
