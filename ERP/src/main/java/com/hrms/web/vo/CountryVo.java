package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public class CountryVo {

	private Long countryId;

	private String name;

	private String tempName;

	/**
	 * 
	 * @return
	 */
	public String getTempName() {
		return tempName;
	}

	/**
	 * 
	 * @param tempName
	 */
	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

	/**
	 * 
	 * @return
	 */
	public Long getCountryId() {
		return countryId;
	}

	/**
	 * 
	 * @param countryId
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

}
