package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-May-2015
 *
 */
public class DesignationRankVo {

	private Long designation_rank_id;
	private String designation;
	private Long rank;
	private String tempDes;

	/**
	 * 
	 * @return
	 */
	public Long getDesignation_rank_id() {
		return designation_rank_id;
	}

	/**
	 * 
	 * @param designation_rank_id
	 */
	public void setDesignation_rank_id(Long designation_rank_id) {
		this.designation_rank_id = designation_rank_id;
	}

	/**
	 * 
	 * @return
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * 
	 * @param designation
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * 
	 * @return
	 */
	public Long getRank() {
		return rank;
	}

	/**
	 * 
	 * @param rank
	 */
	public void setRank(Long rank) {
		this.rank = rank;
	}

	public String getTempDes() {
		return tempDes;
	}

	public void setTempDes(String tempDes) {
		this.tempDes = tempDes;
	}

}
