package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 26-March-2015
 *
 */
public class EmployeeExitTypeVo {

	private Long exitTypeId;

	private String exitType;

	public Long getExitTypeId() {
		return exitTypeId;
	}

	public void setExitTypeId(Long exitTypeId) {
		this.exitTypeId = exitTypeId;
	}

	public String getExitType() {
		return exitType;
	}

	public void setExitType(String exitType) {
		this.exitType = exitType;
	}

}
