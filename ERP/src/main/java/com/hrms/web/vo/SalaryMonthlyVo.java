package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 28-April-2015
 */
public class SalaryMonthlyVo {

	private String employee;
	
	private String employeeCode;
	
	private String monthlyTaxDeduction;

	private String annualTaxDeduction;
	
	private String basicSalary;
	
	private List<SalaryPayslipItemsVo> itemsVos;
	
	private String monthlyEstimatedSalary;

	private String annualEstimatedSalary;

	private String monthlyGrossSalary;

	private String annualGrossSalary;
	
	private String withEffectFrom;
	
	List<PayRollResultVo> payRollResultVos;
	

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getMonthlyTaxDeduction() {
		return monthlyTaxDeduction;
	}

	public void setMonthlyTaxDeduction(String monthlyTaxDeduction) {
		this.monthlyTaxDeduction = monthlyTaxDeduction;
	}

	public String getAnnualTaxDeduction() {
		return annualTaxDeduction;
	}

	public void setAnnualTaxDeduction(String annualTaxDeduction) {
		this.annualTaxDeduction = annualTaxDeduction;
	}

	public List<SalaryPayslipItemsVo> getItemsVos() {
		return itemsVos;
	}

	public void setItemsVos(List<SalaryPayslipItemsVo> itemsVos) {
		this.itemsVos = itemsVos;
	}

	public String getMonthlyEstimatedSalary() {
		return monthlyEstimatedSalary;
	}

	public void setMonthlyEstimatedSalary(String monthlyEstimatedSalary) {
		this.monthlyEstimatedSalary = monthlyEstimatedSalary;
	}

	public String getAnnualEstimatedSalary() {
		return annualEstimatedSalary;
	}

	public void setAnnualEstimatedSalary(String annualEstimatedSalary) {
		this.annualEstimatedSalary = annualEstimatedSalary;
	}

	public String getMonthlyGrossSalary() {
		return monthlyGrossSalary;
	}

	public void setMonthlyGrossSalary(String monthlyGrossSalary) {
		this.monthlyGrossSalary = monthlyGrossSalary;
	}

	public String getAnnualGrossSalary() {
		return annualGrossSalary;
	}

	public void setAnnualGrossSalary(String annualGrossSalary) {
		this.annualGrossSalary = annualGrossSalary;
	}

	public String getWithEffectFrom() {
		return withEffectFrom;
	}

	public void setWithEffectFrom(String withEffectFrom) {
		this.withEffectFrom = withEffectFrom;
	}

	public String getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(String basicSalary) {
		this.basicSalary = basicSalary;
	}

	public List<PayRollResultVo> getPayRollResultVos() {
		return payRollResultVos;
	}

	public void setPayRollResultVos(List<PayRollResultVo> payRollResultVos) {
		this.payRollResultVos = payRollResultVos;
	}
}
