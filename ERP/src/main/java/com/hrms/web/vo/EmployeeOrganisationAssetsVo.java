package com.hrms.web.vo;
/**
 * 
 * @author shamsheer
 * @since 22-March-2015
 */
public class EmployeeOrganisationAssetsVo {

	private String assetName;
	
	private String assetCode;
	
	private String assetCheckout;
	
	private String assetCheckin;
	
	private String description;

	/**
	 * @return the assetName
	 */
	public String getAssetName() {
		return assetName;
	}

	/**
	 * @param assetName the assetName to set
	 */
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	/**
	 * @return the assetCode
	 */
	public String getAssetCode() {
		return assetCode;
	}

	/**
	 * @param assetCode the assetCode to set
	 */
	public void setAssetCode(String assetCode) {
		this.assetCode = assetCode;
	}

	/**
	 * @return the assetCheckout
	 */
	public String getAssetCheckout() {
		return assetCheckout;
	}

	/**
	 * @param assetCheckout the assetCheckout to set
	 */
	public void setAssetCheckout(String assetCheckout) {
		this.assetCheckout = assetCheckout;
	}

	/**
	 * @return the assetCheckin
	 */
	public String getAssetCheckin() {
		return assetCheckin;
	}

	/**
	 * @param assetCheckin the assetCheckin to set
	 */
	public void setAssetCheckin(String assetCheckin) {
		this.assetCheckin = assetCheckin;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
