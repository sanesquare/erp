package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 19-May-2015
 */
public class TempNotificationVo {

	private Long id;
	
	private String subject;
	
	private List<Long> employeeIds = new ArrayList<Long>();
	
	private List<String> employeeCodes = new ArrayList<String>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public List<Long> getEmployeeIds() {
		return employeeIds;
	}

	public void setEmployeeIds(List<Long> employeeIds) {
		this.employeeIds = employeeIds;
	}

	public List<String> getEmployeeCodes() {
		return employeeCodes;
	}

	public void setEmployeeCodes(List<String> employeeCodes) {
		this.employeeCodes = employeeCodes;
	}
}
