package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer
 * @since 08-June-2015
 *
 */
public class PaySalaryMonthlyAmountVo {

	private BigDecimal amount;
	
	private String employeeCode;
	
	private String employeeId;
	
	private String employeeName;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
}
