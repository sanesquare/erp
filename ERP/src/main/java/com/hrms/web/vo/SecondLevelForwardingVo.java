package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 27-April-2015
 *
 */
public class SecondLevelForwardingVo {

	private Long sec_level_fwdr_id;

	private String forwaderName;

	private String forwaderCode;

	private List<String> email;

	private List<String> forwaders;

	private List<String> forwadersName;

	/**
	 * 
	 * @return
	 */
	public List<String> getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 */
	public void setEmail(List<String> email) {
		this.email = email;
	}

	/**
	 * 
	 * @return
	 */
	public String getForwaderName() {
		return forwaderName;
	}

	/**
	 * 
	 * @param forwaderName
	 */
	public void setForwaderName(String forwaderName) {
		this.forwaderName = forwaderName;
	}

	/**
	 * 
	 * @return
	 */
	public String getForwaderCode() {
		return forwaderCode;
	}

	/**
	 * 
	 * @param forwaderCode
	 */
	public void setForwaderCode(String forwaderCode) {
		this.forwaderCode = forwaderCode;
	}

	/**
	 * 
	 * @return
	 */
	public Long getSec_level_fwdr_id() {
		return sec_level_fwdr_id;
	}

	/**
	 * 
	 * @param sec_level_fwdr_id
	 */
	public void setSec_level_fwdr_id(Long sec_level_fwdr_id) {
		this.sec_level_fwdr_id = sec_level_fwdr_id;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getForwaders() {
		return forwaders;
	}

	/**
	 * 
	 * @param forwaders
	 */
	public void setForwaders(List<String> forwaders) {
		this.forwaders = forwaders;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getForwadersName() {
		return forwadersName;
	}

	/**
	 * 
	 * @param forwadersName
	 */
	public void setForwadersName(List<String> forwadersName) {
		this.forwadersName = forwadersName;
	}

}
