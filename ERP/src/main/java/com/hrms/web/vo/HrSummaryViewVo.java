package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 16-April-2015
 *
 */
public class HrSummaryViewVo {

	private HrSummaryVo summaryDetails;
	
	private List<EmployeeByBranchVo> branchDetails;
	
	private List<EmployeeByDepartmentsVo> departmentDetails;
	
	private List<EmployeeByDesignationsVo> designationDetails;
	
	private List<EmployeeByAgeVo> ageGroupDetails;
	
	private int totalBranchEmp;
	
	private int totalDepEmp;
	
	private int totalDesEmp;
	
	
	private String type;

	/**
	 * @return the summaryDetails
	 */
	public HrSummaryVo getSummaryDetails() {
		return summaryDetails;
	}

	/**
	 * @param summaryDetails the summaryDetails to set
	 */
	public void setSummaryDetails(HrSummaryVo summaryDetails) {
		this.summaryDetails = summaryDetails;
	}

	/**
	 * @return the branchDetails
	 */
	public List<EmployeeByBranchVo> getBranchDetails() {
		return branchDetails;
	}

	/**
	 * @param branchDetails the branchDetails to set
	 */
	public void setBranchDetails(List<EmployeeByBranchVo> branchDetails) {
		this.branchDetails = branchDetails;
	}

	/**
	 * @return the departmentDetails
	 */
	public List<EmployeeByDepartmentsVo> getDepartmentDetails() {
		return departmentDetails;
	}

	/**
	 * @param departmentDetails the departmentDetails to set
	 */
	public void setDepartmentDetails(List<EmployeeByDepartmentsVo> departmentDetails) {
		this.departmentDetails = departmentDetails;
	}

	/**
	 * @return the designationDetails
	 */
	public List<EmployeeByDesignationsVo> getDesignationDetails() {
		return designationDetails;
	}

	/**
	 * @param designationDetails the designationDetails to set
	 */
	public void setDesignationDetails(List<EmployeeByDesignationsVo> designationDetails) {
		this.designationDetails = designationDetails;
	}

	/**
	 * @return the ageGroupDetails
	 */
	public List<EmployeeByAgeVo> getAgeGroupDetails() {
		return ageGroupDetails;
	}

	/**
	 * @param ageGroupDetails the ageGroupDetails to set
	 */
	public void setAgeGroupDetails(List<EmployeeByAgeVo> ageGroupDetails) {
		this.ageGroupDetails = ageGroupDetails;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	
	
}
