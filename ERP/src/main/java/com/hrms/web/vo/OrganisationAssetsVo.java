package com.hrms.web.vo;

/**
 * 
 * @author shamsheer
 * @since 21-March-2015
 */
public class OrganisationAssetsVo {

	private String assetName;

	private String assetCode;

	private Long assetId;

	private String tempAssetCode;

	/**
	 * 
	 * @return
	 */
	public String getTempAssetCode() {
		return tempAssetCode;
	}

	/**
	 * 
	 * @param tempAssetCode
	 */
	public void setTempAssetCode(String tempAssetCode) {
		this.tempAssetCode = tempAssetCode;
	}

	/**
	 * @return the assetName
	 */
	public String getAssetName() {
		return assetName;
	}

	/**
	 * @param assetName
	 *            the assetName to set
	 */
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	/**
	 * @return the assetCode
	 */
	public String getAssetCode() {
		return assetCode;
	}

	/**
	 * @param assetCode
	 *            the assetCode to set
	 */
	public void setAssetCode(String assetCode) {
		this.assetCode = assetCode;
	}

	/**
	 * @return the assetId
	 */
	public Long getAssetId() {
		return assetId;
	}

	/**
	 * @param assetId
	 *            the assetId to set
	 */
	public void setAssetId(Long assetId) {
		this.assetId = assetId;
	}

}
