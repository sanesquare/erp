package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
public class ForwardApplicationStatusVo {

	private Long id;
	
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
