package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 2-April-2015
 */
public class WorksheetTasksVo {

	private Long workSheetTasksId;
	
	private String project;
	
	private String task;
	
	private String startTime;
	
	private String endTime; 
	
	private Long projectId;

	/**
	 * @return the workSheetTasksId
	 */
	public Long getWorkSheetTasksId() {
		return workSheetTasksId;
	}

	/**
	 * @param workSheetTasksId the workSheetTasksId to set
	 */
	public void setWorkSheetTasksId(Long workSheetTasksId) {
		this.workSheetTasksId = workSheetTasksId;
	}

	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(String project) {
		this.project = project;
	}

	/**
	 * @return the task
	 */
	public String getTask() {
		return task;
	}

	/**
	 * @param task the task to set
	 */
	public void setTask(String task) {
		this.task = task;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the projectId
	 */
	public Long getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	
	
}
