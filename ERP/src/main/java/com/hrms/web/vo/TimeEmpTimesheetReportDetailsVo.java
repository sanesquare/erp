package com.hrms.web.vo;

import java.sql.Time;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 28-April-2015
 *
 */
public class TimeEmpTimesheetReportDetailsVo {

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date date;
	
	private Time signIn;
	
	private Time signOut;
	
	private double breakHours;
	
	private double totalHours;

	private String dateString;
	
	private String signInString;
	
	private String signOutString;
	
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the signIn
	 */
	public Time getSignIn() {
		return signIn;
	}

	/**
	 * @param signIn the signIn to set
	 */
	public void setSignIn(Time signIn) {
		this.signIn = signIn;
	}

	/**
	 * @return the signOut
	 */
	public Time getSignOut() {
		return signOut;
	}

	/**
	 * @param signOut the signOut to set
	 */
	public void setSignOut(Time signOut) {
		this.signOut = signOut;
	}

	/**
	 * @return the dateString
	 */
	public String getDateString() {
		return dateString;
	}

	/**
	 * @param dateString the dateString to set
	 */
	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	/**
	 * @return the signInString
	 */
	public String getSignInString() {
		return signInString;
	}

	/**
	 * @param signInString the signInString to set
	 */
	public void setSignInString(String signInString) {
		this.signInString = signInString;
	}

	/**
	 * @return the signOutString
	 */
	public String getSignOutString() {
		return signOutString;
	}

	/**
	 * @param signOutString the signOutString to set
	 */
	public void setSignOutString(String signOutString) {
		this.signOutString = signOutString;
	}

	/**
	 * @return the breakHours
	 */
	public double getBreakHours() {
		return breakHours;
	}

	/**
	 * @param breakHours the breakHours to set
	 */
	public void setBreakHours(double breakHours) {
		this.breakHours = breakHours;
	}

	/**
	 * @return the totalHours
	 */
	public double getTotalHours() {
		return totalHours;
	}

	/**
	 * @param totalHours the totalHours to set
	 */
	public void setTotalHours(double totalHours) {
		this.totalHours = totalHours;
	}

	
}
