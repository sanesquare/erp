package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 31-March-2015
 */
public class RolesVo {

	private Long authEmployeeId;
	
	private Long employeeId;
	
	private String userInformation;
	
	private Boolean userInformationView;
	
	private List<RoleHomeVo> homeVos;
	
	private List<RoleOrganisationVo> organisationVos;
	
	private List<RoleRecruitmentVo> recruitmentVos;
	
	private List<RoleEmployeesVo> employeesVos;
	
	private List<RoleERPVo> erpVos;
	
	private List<RolePayrollVo> payrollVos;
	
	private List<RoleReportsVo> reportsVos;
	
	private List<RoleTimeSheetVo> timeSheetVos;
	
	private List<RoleMiscellaneousVo> miscellaneousVos;
	
	private List<Long> viewIds;
	
	private List<Long> addIds;
	
	private List<Long> updateIds;
	
	private List<Long> deleteIds;

	/**
	 * @return the employeeId
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the userInformation
	 */
	public String getUserInformation() {
		return userInformation;
	}

	/**
	 * @param userInformation the userInformation to set
	 */
	public void setUserInformation(String userInformation) {
		this.userInformation = userInformation;
	}

	/**
	 * @return the userInformationView
	 */
	public Boolean getUserInformationView() {
		return userInformationView;
	}

	/**
	 * @param userInformationView the userInformationView to set
	 */
	public void setUserInformationView(Boolean userInformationView) {
		this.userInformationView = userInformationView;
	}


	/**
	 * @return the addIds
	 */
	public List<Long> getAddIds() {
		return addIds;
	}

	/**
	 * @param addIds the addIds to set
	 */
	public void setAddIds(List<Long> addIds) {
		this.addIds = addIds;
	}

	/**
	 * @return the updateIds
	 */
	public List<Long> getUpdateIds() {
		return updateIds;
	}

	/**
	 * @param updateIds the updateIds to set
	 */
	public void setUpdateIds(List<Long> updateIds) {
		this.updateIds = updateIds;
	}

	/**
	 * @return the deleteIds
	 */
	public List<Long> getDeleteIds() {
		return deleteIds;
	}

	/**
	 * @param deleteIds the deleteIds to set
	 */
	public void setDeleteIds(List<Long> deleteIds) {
		this.deleteIds = deleteIds;
	}

	/**
	 * @param viewIds the viewIds to set
	 */
	public void setViewIds(List<Long> viewIds) {
		this.viewIds = viewIds;
	}

	/**
	 * @return the viewIds
	 */
	public List<Long> getViewIds() {
		return viewIds;
	}

	/**
	 * @return the homeVos
	 */
	public List<RoleHomeVo> getHomeVos() {
		return homeVos;
	}

	/**
	 * @param homeVos the homeVos to set
	 */
	public void setHomeVos(List<RoleHomeVo> homeVos) {
		this.homeVos = homeVos;
	}

	/**
	 * @return the organisationVos
	 */
	public List<RoleOrganisationVo> getOrganisationVos() {
		return organisationVos;
	}

	/**
	 * @param organisationVos the organisationVos to set
	 */
	public void setOrganisationVos(List<RoleOrganisationVo> organisationVos) {
		this.organisationVos = organisationVos;
	}

	/**
	 * @return the recruitmentVos
	 */
	public List<RoleRecruitmentVo> getRecruitmentVos() {
		return recruitmentVos;
	}

	/**
	 * @param recruitmentVos the recruitmentVos to set
	 */
	public void setRecruitmentVos(List<RoleRecruitmentVo> recruitmentVos) {
		this.recruitmentVos = recruitmentVos;
	}

	/**
	 * @return the employeesVos
	 */
	public List<RoleEmployeesVo> getEmployeesVos() {
		return employeesVos;
	}

	/**
	 * @param employeesVos the employeesVos to set
	 */
	public void setEmployeesVos(List<RoleEmployeesVo> employeesVos) {
		this.employeesVos = employeesVos;
	}

	/**
	 * @return the payrollVos
	 */
	public List<RolePayrollVo> getPayrollVos() {
		return payrollVos;
	}

	/**
	 * @param payrollVos the payrollVos to set
	 */
	public void setPayrollVos(List<RolePayrollVo> payrollVos) {
		this.payrollVos = payrollVos;
	}

	/**
	 * @return the reportsVos
	 */
	public List<RoleReportsVo> getReportsVos() {
		return reportsVos;
	}

	/**
	 * @param reportsVos the reportsVos to set
	 */
	public void setReportsVos(List<RoleReportsVo> reportsVos) {
		this.reportsVos = reportsVos;
	}

	/**
	 * @return the miscellaneousVos
	 */
	public List<RoleMiscellaneousVo> getMiscellaneousVos() {
		return miscellaneousVos;
	}

	/**
	 * @param miscellaneousVos the miscellaneousVos to set
	 */
	public void setMiscellaneousVos(List<RoleMiscellaneousVo> miscellaneousVos) {
		this.miscellaneousVos = miscellaneousVos;
	}

	public List<RoleTimeSheetVo> getTimeSheetVos() {
		return timeSheetVos;
	}

	public void setTimeSheetVos(List<RoleTimeSheetVo> timeSheetVos) {
		this.timeSheetVos = timeSheetVos;
	}

	public Long getAuthEmployeeId() {
		return authEmployeeId;
	}

	public void setAuthEmployeeId(Long authEmployeeId) {
		this.authEmployeeId = authEmployeeId;
	}

	public List<RoleERPVo> getErpVos() {
		return erpVos;
	}

	public void setErpVos(List<RoleERPVo> erpVos) {
		this.erpVos = erpVos;
	}
}
