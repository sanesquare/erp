package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class InsuranceTypeVo {

	private Long insuranceTypeId;

	private String insuranceType;

	/**
	 * 
	 * @return
	 */
	public Long getInsuranceTypeId() {
		return insuranceTypeId;
	}

	/**
	 * 
	 * @param insuranceTypeId
	 */
	public void setInsuranceTypeId(Long insuranceTypeId) {
		this.insuranceTypeId = insuranceTypeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getInsuranceType() {
		return insuranceType;
	}

	/**
	 * 
	 * @param insuranceType
	 */
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

}
