package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public class TrainingEventTypeVo {

	private Long trainingEventTypeId;

	private String trainingEventType;

	/**
	 * 
	 * @return
	 */
	public Long getTrainingEventTypeId() {
		return trainingEventTypeId;
	}

	/**
	 * 
	 * @param trainingEventTypeId
	 */
	public void setTrainingEventTypeId(Long trainingEventTypeId) {
		this.trainingEventTypeId = trainingEventTypeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getTrainingEventType() {
		return trainingEventType;
	}

	/**
	 * 
	 * @param trainingEventType
	 */
	public void setTrainingEventType(String trainingEventType) {
		this.trainingEventType = trainingEventType;
	}

}
