package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 13-April-2015
 *
 */
public class HrEmpReportVo {
	
	private Long bId;

	private Long depId;
	
	private Long typeId;
	
	private Long statusId;
	
	private Long cateId;
	/**
	 * @return the bId
	 */
	public Long getbId() {
		return bId;
	}

	/**
	 * @param bId the bId to set
	 */
	public void setbId(Long bId) {
		this.bId = bId;
	}

	/**
	 * @return the depId
	 */
	public Long getDepId() {
		return depId;
	}

	/**
	 * @param depId the depId to set
	 */
	public void setDepId(Long depId) {
		this.depId = depId;
	}

	/**
	 * @return the typeId
	 */
	public Long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the cateId
	 */
	public Long getCateId() {
		return cateId;
	}

	/**
	 * @param cateId the cateId to set
	 */
	public void setCateId(Long cateId) {
		this.cateId = cateId;
	}

	
	
	

}
