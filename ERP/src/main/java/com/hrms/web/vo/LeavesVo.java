package com.hrms.web.vo;

import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer
 * @since 6-April-2015
 *
 */
public class LeavesVo implements Comparable<LeavesVo> {

	private String designation;

	private String department;

	private String branch;

	private Boolean viewStatus;

	private String employeeName;

	private String employeeCode;

	private Long leaveId;

	private Long leaveTypeId;

	private Long leaveTypeAjaxId;

	private List<Long> superiorAjaxIds;

	private Long statusId;

	private Long employeeId;

	private Long superiorId;

	private List<Long> superiorIds;

	private List<String> superiorNames;

	private List<String> superiorCode;

	private String reason;

	private String fromDate;

	private String toDate;

	private String notes;

	private String description;

	private Long leaveBalance;

	private Long totalLeaves;

	private String createdBy;

	private String createdOn;

	private String alternateContactPerson;

	private String alternateContactNumber;

	private String leaveType;

	private String status;

	private String statusDescription;

	private String employee;

	private String superior;

	private List<LeaveDocumentsVo> documentsVos;

	private List<MultipartFile> leaveDocs;

	private int duration;

	public Long getLeaveId() {
		return leaveId;
	}

	public void setLeaveId(Long leaveId) {
		this.leaveId = leaveId;
	}

	public Long getLeaveTypeId() {
		return leaveTypeId;
	}

	public void setLeaveTypeId(Long leaveTypeId) {
		this.leaveTypeId = leaveTypeId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getSuperiorId() {
		return superiorId;
	}

	public void setSuperiorId(Long superiorId) {
		this.superiorId = superiorId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAlternateContactPerson() {
		return alternateContactPerson;
	}

	public void setAlternateContactPerson(String alternateContactPerson) {
		this.alternateContactPerson = alternateContactPerson;
	}

	public String getAlternateContactNumber() {
		return alternateContactNumber;
	}

	public void setAlternateContactNumber(String alternateContactNumber) {
		this.alternateContactNumber = alternateContactNumber;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getSuperior() {
		return superior;
	}

	public void setSuperior(String superior) {
		this.superior = superior;
	}

	public List<MultipartFile> getLeaveDocs() {
		return leaveDocs;
	}

	public void setLeaveDocs(List<MultipartFile> leaveDocs) {
		this.leaveDocs = leaveDocs;
	}

	public List<LeaveDocumentsVo> getDocumentsVos() {
		return documentsVos;
	}

	public void setDocumentsVos(List<LeaveDocumentsVo> documentsVos) {
		this.documentsVos = documentsVos;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public List<Long> getSuperiorIds() {
		return superiorIds;
	}

	public void setSuperiorIds(List<Long> superiorIds) {
		this.superiorIds = superiorIds;
	}

	public List<String> getSuperiorNames() {
		return superiorNames;
	}

	public void setSuperiorNames(List<String> superiorNames) {
		this.superiorNames = superiorNames;
	}

	public Long getLeaveTypeAjaxId() {
		return leaveTypeAjaxId;
	}

	public void setLeaveTypeAjaxId(Long leaveTypeAjaxId) {
		this.leaveTypeAjaxId = leaveTypeAjaxId;
	}

	public List<Long> getSuperiorAjaxIds() {
		return superiorAjaxIds;
	}

	public void setSuperiorAjaxIds(List<Long> superiorAjaxIds) {
		this.superiorAjaxIds = superiorAjaxIds;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public List<String> getSuperiorCode() {
		return superiorCode;
	}

	public void setSuperiorCode(List<String> superiorCode) {
		this.superiorCode = superiorCode;
	}

	public Long getLeaveBalance() {
		return leaveBalance;
	}

	public void setLeaveBalance(Long leaveBalance) {
		this.leaveBalance = leaveBalance;
	}

	public Long getTotalLeaves() {
		return totalLeaves;
	}

	public void setTotalLeaves(Long totalLeaves) {
		this.totalLeaves = totalLeaves;
	}

	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Boolean getViewStatus() {
		return viewStatus;
	}

	public void setViewStatus(Boolean viewStatus) {
		this.viewStatus = viewStatus;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public int compareTo(LeavesVo o) {
		Date fromDateOfLeave1 = DateFormatter.convertStringToDate(this.getFromDate());
		Date fromDateOfLeave2 = DateFormatter.convertStringToDate(o.getFromDate());
		if(fromDateOfLeave1.before(fromDateOfLeave2))
			return -1;
		else if(fromDateOfLeave1.after(fromDateOfLeave2))
			return 1;
		return 0;
	}

}
