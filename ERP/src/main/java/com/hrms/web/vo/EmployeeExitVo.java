package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Jithin Mohan
 * @since 18-March-2015
 *
 */
public class EmployeeExitVo {

	private Long employeeExitId;

	private String employee;

	private Long employeeId;

	private String employeeCode;

	private String exitDate;

	private String exitStringDate;

	private String exitType;

	private String exitInterview;

	private String exitReason;

	private String exitNotes;

	private String recordedBy;

	private String recordedOn;

	private List<EmployeeExitDocumentsVo> employeeExitDocuments;

	private List<MultipartFile> emp_ext_docs;

	private MultipartFile docs;

	/**
	 * 
	 * @return
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * 
	 * @param employeeCode
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * 
	 * @return
	 */
	public MultipartFile getDocs() {
		return docs;
	}

	/**
	 * 
	 * @param docs
	 */
	public void setDocs(MultipartFile docs) {
		this.docs = docs;
	}

	/**
	 * 
	 * @return
	 */
	public String getExitStringDate() {
		return exitStringDate;
	}

	/**
	 * 
	 * @param exitStringDate
	 */
	public void setExitStringDate(String exitStringDate) {
		this.exitStringDate = exitStringDate;
	}

	/**
	 * 
	 * @return
	 */
	public List<MultipartFile> getEmp_ext_docs() {
		return emp_ext_docs;
	}

	/**
	 * 
	 * @param emp_ext_docs
	 */
	public void setEmp_ext_docs(List<MultipartFile> emp_ext_docs) {
		this.emp_ext_docs = emp_ext_docs;
	}

	/**
	 * 
	 * @return
	 */
	public List<EmployeeExitDocumentsVo> getEmployeeExitDocuments() {
		return employeeExitDocuments;
	}

	/**
	 * 
	 * @param employeeExitDocuments
	 */
	public void setEmployeeExitDocuments(List<EmployeeExitDocumentsVo> employeeExitDocuments) {
		this.employeeExitDocuments = employeeExitDocuments;
	}

	/**
	 * 
	 * @return
	 */
	public Long getEmployeeExitId() {
		return employeeExitId;
	}

	/**
	 * 
	 * @param employeeExitId
	 */
	public void setEmployeeExitId(Long employeeExitId) {
		this.employeeExitId = employeeExitId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * 
	 * @param employeeId
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getExitDate() {
		return exitDate;
	}

	/**
	 * 
	 * @param exitDate
	 */
	public void setExitDate(String exitDate) {
		this.exitDate = exitDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getExitType() {
		return exitType;
	}

	/**
	 * 
	 * @param exitType
	 */
	public void setExitType(String exitType) {
		this.exitType = exitType;
	}

	/**
	 * 
	 * @return
	 */
	public String getExitInterview() {
		return exitInterview;
	}

	/**
	 * 
	 * @param exitInterview
	 */
	public void setExitInterview(String exitInterview) {
		this.exitInterview = exitInterview;
	}

	/**
	 * 
	 * @return
	 */
	public String getExitReason() {
		return exitReason;
	}

	/**
	 * 
	 * @param exitReason
	 */
	public void setExitReason(String exitReason) {
		this.exitReason = exitReason;
	}

	/**
	 * 
	 * @return
	 */
	public String getExitNotes() {
		return exitNotes;
	}

	/**
	 * 
	 * @param exitNotes
	 */
	public void setExitNotes(String exitNotes) {
		this.exitNotes = exitNotes;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedBy() {
		return recordedBy;
	}

	/**
	 * 
	 * @param recordedBy
	 */
	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}

	/**
	 * 
	 * @return
	 */
	public String getRecordedOn() {
		return recordedOn;
	}

	/**
	 * 
	 * @param recordedOn
	 */
	public void setRecordedOn(String recordedOn) {
		this.recordedOn = recordedOn;
	}

}
