package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
public class BloodGroupVo {

	private Long bloodGroupId;

	private String groupName;

	/**
	 * 
	 * @return
	 */
	public Long getBloodGroupId() {
		return bloodGroupId;
	}

	/**
	 * 
	 * @param bloodGroupId
	 */
	public void setBloodGroupId(Long bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}

	/**
	 * 
	 * @return
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * 
	 * @param groupName
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
