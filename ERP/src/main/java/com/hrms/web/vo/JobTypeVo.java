package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class JobTypeVo {

	private Long jobTypeId;

	private String jobType;

	private String tempJobType;

	/**
	 * 
	 * @return
	 */
	public String getTempJobType() {
		return tempJobType;
	}

	/**
	 * 
	 * @param tempJobType
	 */
	public void setTempJobType(String tempJobType) {
		this.tempJobType = tempJobType;
	}

	/**
	 * 
	 * @return
	 */
	public Long getJobTypeId() {
		return jobTypeId;
	}

	/**
	 * 
	 * @param jobTypeId
	 */
	public void setJobTypeId(Long jobTypeId) {
		this.jobTypeId = jobTypeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getJobType() {
		return jobType;
	}

	/**
	 * 
	 * @param jobType
	 */
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

}
