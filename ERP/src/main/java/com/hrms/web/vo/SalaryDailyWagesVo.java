package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer
 * @since 21-April-2015
 */
public class SalaryDailyWagesVo {

	private String employeeCode;
	
	private String employee;
	
	private String withEffectFrom;
	
	private String salary;
	
	private String tax;
	
	private BigDecimal finalSalary;

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getWithEffectFrom() {
		return withEffectFrom;
	}

	public void setWithEffectFrom(String withEffectFrom) {
		this.withEffectFrom = withEffectFrom;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public BigDecimal getFinalSalary() {
		return finalSalary;
	}

	public void setFinalSalary(BigDecimal finalSalary) {
		this.finalSalary = finalSalary;
	}
}
