package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 20-March-2015
 *
 */
public class WarningDocumentsVo {

	private Long warning_doc_id;

	private String documentName;

	private String documentUrl;

	/**
	 * 
	 * @return
	 */
	public Long getWarning_doc_id() {
		return warning_doc_id;
	}

	/**
	 * 
	 * @param warning_doc_id
	 */
	public void setWarning_doc_id(Long warning_doc_id) {
		this.warning_doc_id = warning_doc_id;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * 
	 * @param documentName
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * 
	 * @return
	 */
	public String getDocumentUrl() {
		return documentUrl;
	}

	/**
	 * 
	 * @param documentUrl
	 */
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

}
