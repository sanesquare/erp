package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.Multipart;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Vinutha
 * @since 1-April-2015
 *
 */
public class WorkSheetVo {

	private Long workSheetId;
	
	private Long taskId;
	
	private Long employeeId;
	
	private String employee;
	
	private String employeeCode;
	
	private Long branchID;
	
	private Long departmetnId;	
	
	private String date;
	
	private String startTime;
	
	private String endTime;
	
	private String description;
	
	private String addInfo;
	
	private String createdBy;
	
	private String createdOn;
	
	private Set<WorkSheetDocumentVo> documentsVo= new HashSet<WorkSheetDocumentVo>();
	
	List<MultipartFile> uploadSheetDocs =new ArrayList<MultipartFile>();
	
	//tasks
	
	private Set<WorksheetTasksVo> tasksVo = new HashSet<WorksheetTasksVo>();
	
	//private Long projectId;
	
	//private String project;
	
	private Set<String> reportingTo;
	
	private Set<String> reportingCode;
	
	private String task;

	/**
	 * @return the workSheetId
	 */
	public Long getWorkSheetId() {
		return workSheetId;
	}

	/**
	 * @param workSheetId the workSheetId to set
	 */
	public void setWorkSheetId(Long workSheetId) {
		this.workSheetId = workSheetId;
	}

	/**
	 * @return the taskId
	 */
	public Long getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	/**
	 * @return the employeeId
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the employee
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * @return the employeeCode
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * @param employeeCode the employeeCode to set
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * @return the branchID
	 */
	public Long getBranchID() {
		return branchID;
	}

	/**
	 * @param branchID the branchID to set
	 */
	public void setBranchID(Long branchID) {
		this.branchID = branchID;
	}

	/**
	 * @return the departmetnId
	 */
	public Long getDepartmetnId() {
		return departmetnId;
	}

	/**
	 * @param departmetnId the departmetnId to set
	 */
	public void setDepartmetnId(Long departmetnId) {
		this.departmetnId = departmetnId;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the addInfo
	 */
	public String getAddInfo() {
		return addInfo;
	}

	/**
	 * @param addInfo the addInfo to set
	 */
	public void setAddInfo(String addInfo) {
		this.addInfo = addInfo;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the documentsVo
	 */
	public Set<WorkSheetDocumentVo> getDocumentsVo() {
		return documentsVo;
	}

	/**
	 * @param documentsVo the documentsVo to set
	 */
	public void setDocumentsVo(Set<WorkSheetDocumentVo> documentsVo) {
		this.documentsVo = documentsVo;
	}

	/**
	 * @return the uploadSheetDocs
	 */
	public List<MultipartFile> getUploadSheetDocs() {
		return uploadSheetDocs;
	}

	/**
	 * @param uploadSheetDocs the uploadSheetDocs to set
	 */
	public void setUploadSheetDocs(List<MultipartFile> uploadSheetDocs) {
		this.uploadSheetDocs = uploadSheetDocs;
	}

	/**
	 * @return the tasksVo
	 */
	public Set<WorksheetTasksVo> getTasksVo() {
		return tasksVo;
	}

	/**
	 * @param tasksVo the tasksVo to set
	 */
	public void setTasksVo(Set<WorksheetTasksVo> tasksVo) {
		this.tasksVo = tasksVo;
	}

	/**
	 * @return the reportingTo
	 */
	public Set<String> getReportingTo() {
		return reportingTo;
	}

	/**
	 * @param reportingTo the reportingTo to set
	 */
	public void setReportingTo(Set<String> reportingTo) {
		this.reportingTo = reportingTo;
	}

	/**
	 * @return the reportingCode
	 */
	public Set<String> getReportingCode() {
		return reportingCode;
	}

	/**
	 * @param reportingCode the reportingCode to set
	 */
	public void setReportingCode(Set<String> reportingCode) {
		this.reportingCode = reportingCode;
	}

	/**
	 * @return the task
	 */
	public String getTask() {
		return task;
	}

	/**
	 * @param task the task to set
	 */
	public void setTask(String task) {
		this.task = task;
	}

	
	
}
