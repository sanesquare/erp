package com.hrms.web.vo;

import java.math.BigDecimal;


/**
 * 
 * @author Shamsheer
 * @since 05-May-2015
 */
public class SalaryCalculatorVo {

	private BigDecimal grossSalary;
	
	private SalaryCalculatorPfShareVo pfShare;

	private SalaryCalculatorBasicsVo basicsVo;

	private SalaryCalculatorEsiVo esiVo;

	private SalaryCalculatorLWFVo lwfVo;

	private BigDecimal netAnnual;

	private BigDecimal netMonthly;

	private BigDecimal ctcAnnual;

	private BigDecimal ctcMonthly;

	public SalaryCalculatorPfShareVo getPfShare() {
		return pfShare;
	}

	public void setPfShare(SalaryCalculatorPfShareVo pfShare) {
		this.pfShare = pfShare;
	}

	public SalaryCalculatorBasicsVo getBasicsVo() {
		return basicsVo;
	}

	public void setBasicsVo(SalaryCalculatorBasicsVo basicsVo) {
		this.basicsVo = basicsVo;
	}

	public BigDecimal getNetAnnual() {
		return netAnnual;
	}

	public void setNetAnnual(BigDecimal netAnnual) {
		this.netAnnual = netAnnual;
	}

	public BigDecimal getNetMonthly() {
		return netMonthly;
	}

	public void setNetMonthly(BigDecimal netMonthly) {
		this.netMonthly = netMonthly;
	}

	public SalaryCalculatorEsiVo getEsiVo() {
		return esiVo;
	}

	public void setEsiVo(SalaryCalculatorEsiVo esiVo) {
		this.esiVo = esiVo;
	}

	public SalaryCalculatorLWFVo getLwfVo() {
		return lwfVo;
	}

	public void setLwfVo(SalaryCalculatorLWFVo lwfVo) {
		this.lwfVo = lwfVo;
	}

	public BigDecimal getCtcAnnual() {
		return ctcAnnual;
	}

	public void setCtcAnnual(BigDecimal ctcAnnual) {
		this.ctcAnnual = ctcAnnual;
	}

	public BigDecimal getCtcMonthly() {
		return ctcMonthly;
	}

	public void setCtcMonthly(BigDecimal ctcMonthly) {
		this.ctcMonthly = ctcMonthly;
	}

	public BigDecimal getGrossSalary() {
		return grossSalary;
	}

	public void setGrossSalary(BigDecimal grossSalary) {
		this.grossSalary = grossSalary;
	}

}
