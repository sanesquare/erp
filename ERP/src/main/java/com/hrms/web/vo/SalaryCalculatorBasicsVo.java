package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 05-May-2015
 */
public class SalaryCalculatorBasicsVo {
	
	private List<PayRollResultVo> resultVo =new ArrayList<PayRollResultVo>();
	
	private BigDecimal basicSalaryMonthly = new BigDecimal(0);

	private BigDecimal basicSalaryAnnual = new BigDecimal(0);

	private BigDecimal dearnessAllowanceMonthly = new BigDecimal(0);

	private BigDecimal dearnessAllowanceAnnual = new BigDecimal(0);

	private BigDecimal hraMonthly = new BigDecimal(0);

	private BigDecimal hraAnnual = new BigDecimal(0);

	private BigDecimal ccaMonthly = new BigDecimal(0);

	private BigDecimal ccaAnnual = new BigDecimal(0);

	private BigDecimal conveyanceMonthly = new BigDecimal(0);

	private BigDecimal conveyanceAnnual = new BigDecimal(0);
	
	private BigDecimal grossMonthly = new BigDecimal(0);

	private BigDecimal grossAnnual = new BigDecimal(0);
	
	private BigDecimal otherAllowancesMonthly = new BigDecimal(0);

	private BigDecimal otherAllowancesAnnual = new BigDecimal(0);

	public BigDecimal getBasicSalaryMonthly() {
		return basicSalaryMonthly;
	}

	public void setBasicSalaryMonthly(BigDecimal basicSalaryMonthly) {
		this.basicSalaryMonthly = basicSalaryMonthly;
	}

	public BigDecimal getBasicSalaryAnnual() {
		return basicSalaryAnnual;
	}

	public void setBasicSalaryAnnual(BigDecimal basicSalaryAnnual) {
		this.basicSalaryAnnual = basicSalaryAnnual;
	}

	public BigDecimal getDearnessAllowanceMonthly() {
		return dearnessAllowanceMonthly;
	}

	public void setDearnessAllowanceMonthly(BigDecimal dearnessAllowanceMonthly) {
		this.dearnessAllowanceMonthly = dearnessAllowanceMonthly;
	}

	public BigDecimal getDearnessAllowanceAnnual() {
		return dearnessAllowanceAnnual;
	}

	public void setDearnessAllowanceAnnual(BigDecimal dearnessAllowanceAnnual) {
		this.dearnessAllowanceAnnual = dearnessAllowanceAnnual;
	}

	public BigDecimal getHraMonthly() {
		return hraMonthly;
	}

	public void setHraMonthly(BigDecimal hraMonthly) {
		this.hraMonthly = hraMonthly;
	}

	public BigDecimal getHraAnnual() {
		return hraAnnual;
	}

	public void setHraAnnual(BigDecimal hraAnnual) {
		this.hraAnnual = hraAnnual;
	}

	public BigDecimal getCcaMonthly() {
		return ccaMonthly;
	}

	public void setCcaMonthly(BigDecimal ccaMonthly) {
		this.ccaMonthly = ccaMonthly;
	}

	public BigDecimal getCcaAnnual() {
		return ccaAnnual;
	}

	public void setCcaAnnual(BigDecimal ccaAnnual) {
		this.ccaAnnual = ccaAnnual;
	}

	public BigDecimal getConveyanceMonthly() {
		return conveyanceMonthly;
	}

	public void setConveyanceMonthly(BigDecimal conveyanceMonthly) {
		this.conveyanceMonthly = conveyanceMonthly;
	}

	public BigDecimal getConveyanceAnnual() {
		return conveyanceAnnual;
	}

	public void setConveyanceAnnual(BigDecimal conveyanceAnnual) {
		this.conveyanceAnnual = conveyanceAnnual;
	}

	public BigDecimal getGrossMonthly() {
		return grossMonthly;
	}

	public void setGrossMonthly(BigDecimal grossMonthly) {
		this.grossMonthly = grossMonthly;
	}

	public BigDecimal getGrossAnnual() {
		return grossAnnual;
	}

	public void setGrossAnnual(BigDecimal grossAnnual) {
		this.grossAnnual = grossAnnual;
	}

	public BigDecimal getOtherAllowancesMonthly() {
		return otherAllowancesMonthly;
	}

	public void setOtherAllowancesMonthly(BigDecimal otherAllowancesMonthly) {
		this.otherAllowancesMonthly = otherAllowancesMonthly;
	}

	public BigDecimal getOtherAllowancesAnnual() {
		return otherAllowancesAnnual;
	}

	public void setOtherAllowancesAnnual(BigDecimal otherAllowancesAnnual) {
		this.otherAllowancesAnnual = otherAllowancesAnnual;
	}

	public List<PayRollResultVo> getResultVo() {
		return resultVo;
	}

	public void setResultVo(List<PayRollResultVo> resultVo) {
		this.resultVo = resultVo;
	}
	
	
}
