package com.hrms.web.vo;

import java.util.Date;

/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpComplaintsreportDetailsVo {

	private int slno;
	
	private String compBy;
	
	private String compAgainst;
	
	private String title;
	
	private Date comDate;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the compBy
	 */
	public String getCompBy() {
		return compBy;
	}

	/**
	 * @param compBy the compBy to set
	 */
	public void setCompBy(String compBy) {
		this.compBy = compBy;
	}

	/**
	 * @return the compAgainst
	 */
	public String getCompAgainst() {
		return compAgainst;
	}

	/**
	 * @param compAgainst the compAgainst to set
	 */
	public void setCompAgainst(String compAgainst) {
		this.compAgainst = compAgainst;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the comDate
	 */
	public Date getComDate() {
		return comDate;
	}

	/**
	 * @param comDate the comDate to set
	 */
	public void setComDate(Date comDate) {
		this.comDate = comDate;
	}
	
	
	
}
