package com.hrms.web.vo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.entities.ComplaintDocuments;
import com.hrms.web.entities.DepartmentDocuments;
import com.hrms.web.entities.Employee;

public class ComplaintsVo {
	
	private Long id;
	private Employee complaintFromEmp;
	private Long complaintFromEmpId;
	private String complaintFromEmpCode;
	private Employee forwardAppToEmp;
	private Long forwardAppToEmpId;
	private String forwardAppToEmpCode;
	private String complaintDate;
	//private Employee complaintAganistEmp;
	private List<String> complaintAganistEmp;
	private Long complaintAganistEmpId;
	private String complaintAganistEmpCode;
	private String complaintDescription;
	private Long complaintStatusId;
	private String statusDescription;
	private String status;
	private String note;
	private List<Long> employeeId;
	private List<String> employeeCode;
	private String documentAccessUrl;
	private String documentName;
	private Set<ComplaintDocuments> complaintDocuments = new HashSet<ComplaintDocuments>();
	private MultipartFile complaint_file_upload;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Employee getComplaintFromEmp() {
		return complaintFromEmp;
	}
	public void setComplaintFromEmp(Employee complaintFromEmp) {
		this.complaintFromEmp = complaintFromEmp;
	}
	
	public Long getComplaintStatusId() {
		return complaintStatusId;
	}
	public void setComplaintStatusId(Long complaintStatusId) {
		this.complaintStatusId = complaintStatusId;
	}
	public String getComplaintFromEmpCode() {
		return complaintFromEmpCode;
	}
	public void setComplaintFromEmpCode(String complaintFromEmpCode) {
		this.complaintFromEmpCode = complaintFromEmpCode;
	}
	public Employee getForwardAppToEmp() {
		return forwardAppToEmp;
	}
	public void setForwardAppToEmp(Employee forwardAppToEmp) {
		this.forwardAppToEmp = forwardAppToEmp;
	}
	
	public String getForwardAppToEmpCode() {
		return forwardAppToEmpCode;
	}
	public void setForwardAppToEmpCode(String forwardAppToEmpCode) {
		this.forwardAppToEmpCode = forwardAppToEmpCode;
	}
	public String getComplaintDate() {
		return complaintDate;
	}
	public void setComplaintDate(String complaintDate) {
		this.complaintDate = complaintDate;
	}
	public List<String> getComplaintAganistEmp() {
		return complaintAganistEmp;
	}
	public void setComplaintAganistEmp(List<String> complaintAganistEmp) {
		this.complaintAganistEmp = complaintAganistEmp;
	}
	
	public Long getComplaintFromEmpId() {
		return complaintFromEmpId;
	}
	public void setComplaintFromEmpId(Long complaintFromEmpId) {
		this.complaintFromEmpId = complaintFromEmpId;
	}
	public Long getForwardAppToEmpId() {
		return forwardAppToEmpId;
	}
	public void setForwardAppToEmpId(Long forwardAppToEmpId) {
		this.forwardAppToEmpId = forwardAppToEmpId;
	}
	public Long getComplaintAganistEmpId() {
		return complaintAganistEmpId;
	}
	public void setComplaintAganistEmpId(Long complaintAganistEmpId) {
		this.complaintAganistEmpId = complaintAganistEmpId;
	}
	public String getComplaintAganistEmpCode() {
		return complaintAganistEmpCode;
	}
	public void setComplaintAganistEmpCode(String complaintAganistEmpCode) {
		this.complaintAganistEmpCode = complaintAganistEmpCode;
	}
	public String getComplaintDescription() {
		return complaintDescription;
	}
	public void setComplaintDescription(String complaintDescription) {
		this.complaintDescription = complaintDescription;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public List<Long> getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(List<Long> employeeId) {
		this.employeeId = employeeId;
	}
	public List<String> getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(List<String> employeeCode) {
		this.employeeCode = employeeCode;
	}
	public Set<ComplaintDocuments> getComplaintDocuments() {
		return complaintDocuments;
	}
	public void setComplaintDocuments(Set<ComplaintDocuments> complaintDocuments) {
		this.complaintDocuments = complaintDocuments;
	}
	public MultipartFile getComplaint_file_upload() {
		return complaint_file_upload;
	}
	public void setComplaint_file_upload(MultipartFile complaint_file_upload) {
		this.complaint_file_upload = complaint_file_upload;
	}
	public String getDocumentAccessUrl() {
		return documentAccessUrl;
	}
	public void setDocumentAccessUrl(String documentAccessUrl) {
		this.documentAccessUrl = documentAccessUrl;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	
	
}