package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
public class TravelDocumentVo {

	private Long travelId;
	
	private String url;
	
	private String fileName;

	public Long getTravelId() {
		return travelId;
	}

	public void setTravelId(Long travelId) {
		this.travelId = travelId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
