/**
 * 
 */
package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * @author logon 5
 *
 */
public class SalaryViewVo {

	private String employeeCode;
	
	private String employeeName;
	
	private Long employeeId;
	
	private Long salaryId;
	
	private String salaryType;
	
	private String withEffectFrom;
	
	private BigDecimal amount;

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getSalaryId() {
		return salaryId;
	}

	public void setSalaryId(Long salaryId) {
		this.salaryId = salaryId;
	}

	public String getSalaryType() {
		return salaryType;
	}

	public void setSalaryType(String salaryType) {
		this.salaryType = salaryType;
	}

	public String getWithEffectFrom() {
		return withEffectFrom;
	}

	public void setWithEffectFrom(String withEffectFrom) {
		this.withEffectFrom = withEffectFrom;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
