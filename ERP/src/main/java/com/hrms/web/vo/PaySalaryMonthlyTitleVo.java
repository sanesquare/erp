package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 08-June-2015
 *
 */
public class PaySalaryMonthlyTitleVo {

	private String title;
	
	private List<PaySalaryMonthlyAmountVo> amountVos = new ArrayList<PaySalaryMonthlyAmountVo>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<PaySalaryMonthlyAmountVo> getAmountVos() {
		return amountVos;
	}

	public void setAmountVos(List<PaySalaryMonthlyAmountVo> amountVos) {
		this.amountVos = amountVos;
	}
}
