package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer
 * @since 05-May-2015
 */
public class SalaryCalculatorPfShareVo {

	private BigDecimal pfEmployeeMonthly = new BigDecimal(0);

	private BigDecimal pfEmployeeAnnual = new BigDecimal(0);;

	private BigDecimal pfEmployerMonthly = new BigDecimal(0);;

	private BigDecimal pfEmployerAnnual = new BigDecimal(0);;

	public BigDecimal getPfEmployeeMonthly() {
		return pfEmployeeMonthly;
	}

	public void setPfEmployeeMonthly(BigDecimal pfEmployeeMonthly) {
		this.pfEmployeeMonthly = pfEmployeeMonthly;
	}

	public BigDecimal getPfEmployeeAnnual() {
		return pfEmployeeAnnual;
	}

	public void setPfEmployeeAnnual(BigDecimal pfEmployeeAnnual) {
		this.pfEmployeeAnnual = pfEmployeeAnnual;
	}

	public BigDecimal getPfEmployerMonthly() {
		return pfEmployerMonthly;
	}

	public void setPfEmployerMonthly(BigDecimal pfEmployerMonthly) {
		this.pfEmployerMonthly = pfEmployerMonthly;
	}

	public BigDecimal getPfEmployerAnnual() {
		return pfEmployerAnnual;
	}

	public void setPfEmployerAnnual(BigDecimal pfEmployerAnnual) {
		this.pfEmployerAnnual = pfEmployerAnnual;
	}
}
