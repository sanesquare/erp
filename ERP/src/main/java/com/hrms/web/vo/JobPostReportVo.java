package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 20-April-2015
 *
 */
public class JobPostReportVo {

	private Long depId;

	public Long getDepId() {
		return depId;
	}

	public void setDepId(Long depId) {
		this.depId = depId;
	}
	
	
}
