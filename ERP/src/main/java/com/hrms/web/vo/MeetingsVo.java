package com.hrms.web.vo;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Shamsheer
 * @since 12-March-2015
 *
 */
public class MeetingsVo {

	private Long meetingId;
	
	private String meetingsTitle;
	
	private boolean sendEmail;
	
	private boolean sendNotification;
	
	private String meetingStartDate;
	
	private String meetingEndDate;
	
	private String time;
	
	private String venue;
	
	private String agenda;
	
	private String additionalInformation;
	
	private String createdBy;
	
	private String createdOn;
	
	private String updatedBy;
	
	private String updatedOn;
	
	private String status;
	
	private String statusDescription;
	
	private Long statusId;
	
	private Set<Long> branchIds;
	
	private Set<Long> departmentIds;
	
	private Set<Long> employeeIds;

	private List<MultipartFile> meetingDocs;
	
	private List<MeetingDocumentsVo> documentsVos;
	
	private List<String> employees;
	
	/**
	 * @return the meetingsTitle
	 */
	public String getMeetingsTitle() {
		return meetingsTitle;
	}

	/**
	 * @param meetingsTitle the meetingsTitle to set
	 */
	public void setMeetingsTitle(String meetingsTitle) {
		this.meetingsTitle = meetingsTitle;
	}

	/**
	 * @return the sendEmail
	 */
	public boolean isSendEmail() {
		return sendEmail;
	}

	/**
	 * @param sendEmail the sendEmail to set
	 */
	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	/**
	 * @return the meetingStartDate
	 */
	public String getMeetingStartDate() {
		return meetingStartDate;
	}

	/**
	 * @param meetingStartDate the meetingStartDate to set
	 */
	public void setMeetingStartDate(String meetingStartDate) {
		this.meetingStartDate = meetingStartDate;
	}

	/**
	 * @return the meetingEndDate
	 */
	public String getMeetingEndDate() {
		return meetingEndDate;
	}

	/**
	 * @param meetingEndDate the meetingEndDate to set
	 */
	public void setMeetingEndDate(String meetingEndDate) {
		this.meetingEndDate = meetingEndDate;
	}


	/**
	 * @return the venue
	 */
	public String getVenue() {
		return venue;
	}

	/**
	 * @param venue the venue to set
	 */
	public void setVenue(String venue) {
		this.venue = venue;
	}

	/**
	 * @return the agenda
	 */
	public String getAgenda() {
		return agenda;
	}

	/**
	 * @param agenda the agenda to set
	 */
	public void setAgenda(String agenda) {
		this.agenda = agenda;
	}

	/**
	 * @return the additionalInformation
	 */
	public String getAdditionalInformation() {
		return additionalInformation;
	}

	/**
	 * @param additionalInformation the additionalInformation to set
	 */
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}


	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedOn
	 */
	public String getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the statusDescription
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * @param statusDescription the statusDescription to set
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the branchIds
	 */
	public Set<Long> getBranchIds() {
		return branchIds;
	}

	/**
	 * @param branchIds the branchIds to set
	 */
	public void setBranchIds(Set<Long> branchIds) {
		this.branchIds = branchIds;
	}

	/**
	 * @return the departmentIds
	 */
	public Set<Long> getDepartmentIds() {
		return departmentIds;
	}

	/**
	 * @param departmentIds the departmentIds to set
	 */
	public void setDepartmentIds(Set<Long> departmentIds) {
		this.departmentIds = departmentIds;
	}

	/**
	 * @return the employeeIds
	 */
	public Set<Long> getEmployeeIds() {
		return employeeIds;
	}

	/**
	 * @param employeeIds the employeeIds to set
	 */
	public void setEmployeeIds(Set<Long> employeeIds) {
		this.employeeIds = employeeIds;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Long getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(Long meetingId) {
		this.meetingId = meetingId;
	}

	public List<MultipartFile> getMeetingDocs() {
		return meetingDocs;
	}

	public void setMeetingDocs(List<MultipartFile> meetingDocs) {
		this.meetingDocs = meetingDocs;
	}

	public List<MeetingDocumentsVo> getDocumentsVos() {
		return documentsVos;
	}

	public void setDocumentsVos(List<MeetingDocumentsVo> documentsVos) {
		this.documentsVos = documentsVos;
	}

	public List<String> getEmployees() {
		return employees;
	}

	public void setEmployees(List<String> employees) {
		this.employees = employees;
	}

	public boolean isSendNotification() {
		return sendNotification;
	}

	public void setSendNotification(boolean sendNotification) {
		this.sendNotification = sendNotification;
	}
}
