package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * 
 * @author Vinutha
 * @since 4-May-2015
 *
 */
public class PaySalarySplitDetailsVo {

	private int slno;

	private String empCode;

	private String empName;

	private String department;

	private String designation;

	private String pFApp;

	private String esiApp;

	private String status;

	private HashMap<String, BigDecimal> offeredSalary = new HashMap<String, BigDecimal>();

	private BigDecimal totalSalary= new BigDecimal("0.00");

	private BigDecimal lopCount= new BigDecimal("0.00");

	private BigDecimal lopDeduction= new BigDecimal("0.00");

	private BigDecimal commission= new BigDecimal("0.00");

	private BigDecimal bonuses= new BigDecimal("0.00");

	private BigDecimal increment= new BigDecimal("0.00");

	private BigDecimal encashment= new BigDecimal("0.00");

	private BigDecimal totalEarnings= new BigDecimal("0.00");

	private BigDecimal pf= new BigDecimal("0.00");
	
	private BigDecimal esi= new BigDecimal("0.00");

	private BigDecimal lwf= new BigDecimal("0.00");
	
	private BigDecimal proffTax= new BigDecimal("0.00");
	
	private BigDecimal tax= new BigDecimal("0.00");
	
	private BigDecimal decrement= new BigDecimal("0.00");
	
	private BigDecimal deduction= new BigDecimal("0.00");
	
	private BigDecimal loan= new BigDecimal("0.00");
	
	private BigDecimal advance= new BigDecimal("0.00");
	
	private BigDecimal totalDeduction= new BigDecimal("0.00");
	
	private HashMap<String, BigDecimal> deductions;

	private BigDecimal netPay= new BigDecimal("0.00");

	private BigDecimal pfEmployerContribution= new BigDecimal("0.00");

	private BigDecimal esiEmployerContribution= new BigDecimal("0.00");

	private BigDecimal ctc= new BigDecimal("0.00");

	private int allowanceSpan;

	public int getSlno() {
		return slno;
	}

	public String getEmpCode() {
		return empCode;
	}

	public String getEmpName() {
		return empName;
	}

	public String getDepartment() {
		return department;
	}

	public String getDesignation() {
		return designation;
	}

	public String getpFApp() {
		return pFApp;
	}

	public String getEsiApp() {
		return esiApp;
	}

	public String getStatus() {
		return status;
	}

	public HashMap<String, BigDecimal> getOfferedSalary() {
		return offeredSalary;
	}

	public BigDecimal getTotalSalary() {
		return totalSalary;
	}

	public BigDecimal getLopCount() {
		return lopCount;
	}

	public BigDecimal getLopDeduction() {
		return lopDeduction;
	}

	public BigDecimal getCommission() {
		return commission;
	}

	public BigDecimal getBonuses() {
		return bonuses;
	}

	public BigDecimal getIncrement() {
		return increment;
	}

	public BigDecimal getEncashment() {
		return encashment;
	}

	public BigDecimal getTotalEarnings() {
		return totalEarnings;
	}

	public BigDecimal getPf() {
		return pf;
	}

	public BigDecimal getEsi() {
		return esi;
	}

	public BigDecimal getLwf() {
		return lwf;
	}

	public BigDecimal getProffTax() {
		return proffTax;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public BigDecimal getDecrement() {
		return decrement;
	}

	public BigDecimal getDeduction() {
		return deduction;
	}

	public BigDecimal getLoan() {
		return loan;
	}

	public BigDecimal getAdvance() {
		return advance;
	}

	public BigDecimal getTotalDeduction() {
		return totalDeduction;
	}

	public HashMap<String, BigDecimal> getDeductions() {
		return deductions;
	}

	public BigDecimal getNetPay() {
		return netPay;
	}

	public BigDecimal getPfEmployerContribution() {
		return pfEmployerContribution;
	}

	public BigDecimal getEsiEmployerContribution() {
		return esiEmployerContribution;
	}

	public BigDecimal getCtc() {
		return ctc;
	}

	public int getAllowanceSpan() {
		return allowanceSpan;
	}

	public void setSlno(int slno) {
		this.slno = slno;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void setpFApp(String pFApp) {
		this.pFApp = pFApp;
	}

	public void setEsiApp(String esiApp) {
		this.esiApp = esiApp;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setOfferedSalary(HashMap<String, BigDecimal> offeredSalary) {
		this.offeredSalary = offeredSalary;
	}

	public void setTotalSalary(BigDecimal totalSalary) {
		this.totalSalary = totalSalary;
	}

	public void setLopCount(BigDecimal lopCount) {
		this.lopCount = lopCount;
	}

	public void setLopDeduction(BigDecimal lopDeduction) {
		this.lopDeduction = lopDeduction;
	}

	public void setCommission(BigDecimal commission) {
		this.commission = commission;
	}

	public void setBonuses(BigDecimal bonuses) {
		this.bonuses = bonuses;
	}

	public void setIncrement(BigDecimal increment) {
		this.increment = increment;
	}

	public void setEncashment(BigDecimal encashment) {
		this.encashment = encashment;
	}

	public void setTotalEarnings(BigDecimal totalEarnings) {
		this.totalEarnings = totalEarnings;
	}

	public void setPf(BigDecimal pf) {
		this.pf = pf;
	}

	public void setEsi(BigDecimal esi) {
		this.esi = esi;
	}

	public void setLwf(BigDecimal lwf) {
		this.lwf = lwf;
	}

	public void setProffTax(BigDecimal proffTax) {
		this.proffTax = proffTax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public void setDecrement(BigDecimal decrement) {
		this.decrement = decrement;
	}

	public void setDeduction(BigDecimal deduction) {
		this.deduction = deduction;
	}

	public void setLoan(BigDecimal loan) {
		this.loan = loan;
	}

	public void setAdvance(BigDecimal advance) {
		this.advance = advance;
	}

	public void setTotalDeduction(BigDecimal totalDeduction) {
		this.totalDeduction = totalDeduction;
	}

	public void setDeductions(HashMap<String, BigDecimal> deductions) {
		this.deductions = deductions;
	}

	public void setNetPay(BigDecimal netPay) {
		this.netPay = netPay;
	}

	public void setPfEmployerContribution(BigDecimal pfEmployerContribution) {
		this.pfEmployerContribution = pfEmployerContribution;
	}

	public void setEsiEmployerContribution(BigDecimal esiEmployerContribution) {
		this.esiEmployerContribution = esiEmployerContribution;
	}

	public void setCtc(BigDecimal ctc) {
		this.ctc = ctc;
	}

	public void setAllowanceSpan(int allowanceSpan) {
		this.allowanceSpan = allowanceSpan;
	}

	
}
