package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer
 * @since 01-Dec-2015
 */
public class AttendanceEmployeeVo {

	private String name;
	
	private String employeeCode;
	
	private Long employeeId;
	
	private List<EmployeeLeaveVo> leaveVos = new ArrayList<EmployeeLeaveVo>();

	private String status;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public List<EmployeeLeaveVo> getLeaveVos() {
		return leaveVos;
	}

	public void setLeaveVos(List<EmployeeLeaveVo> leaveVos) {
		this.leaveVos = leaveVos;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
