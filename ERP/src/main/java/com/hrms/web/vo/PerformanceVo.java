package com.hrms.web.vo;

import java.util.Set;

/**
 * 
 * @author Vinutha
 * @since 23-May-2015
 *
 */
public class PerformanceVo {

	private Long evaluationId;

	private String employee;

	private String employeeCode;

	private String title;

	private String date;

	private String notes;

	private String createdBy;

	private String createdOn;

	private Set<IndividualEvaluationVo> individualVos;

	private Long individualId;
	
	private String maxMark;

	private String score;

	/**
	 * @return the evaluationId
	 */
	public Long getEvaluationId() {
		return evaluationId;
	}

	/**
	 * @param evaluationId
	 *            the evaluationId to set
	 */
	public void setEvaluationId(Long evaluationId) {
		this.evaluationId = evaluationId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the individualVos
	 */
	public Set<IndividualEvaluationVo> getIndividualVos() {
		return individualVos;
	}

	/**
	 * @param individualVos
	 *            the individualVos to set
	 */
	public void setIndividualVos(Set<IndividualEvaluationVo> individualVos) {
		this.individualVos = individualVos;
	}

	/**
	 * @return the employee
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * @param employee
	 *            the employee to set
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/**
	 * @return the employeeCode
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * @param employeeCode
	 *            the employeeCode to set
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * @return the maxMark
	 */
	public String getMaxMark() {
		return maxMark;
	}

	/**
	 * @param maxMark the maxMark to set
	 */
	public void setMaxMark(String maxMark) {
		this.maxMark = maxMark;
	}

	/**
	 * @return the score
	 */
	public String getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(String score) {
		this.score = score;
	}

	/**
	 * @return the individualId
	 */
	public Long getIndividualId() {
		return individualId;
	}

	/**
	 * @param individualId the individualId to set
	 */
	public void setIndividualId(Long individualId) {
		this.individualId = individualId;
	}

}
