package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Vinutha
 * @since 17-April-2015
 *
 */
public class YearlySalaryVo {

	private String year;
	
	private BigDecimal yearAmnt;

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the yearAmnt
	 */
	public BigDecimal getYearAmnt() {
		return yearAmnt;
	}

	/**
	 * @param yearAmnt the yearAmnt to set
	 */
	public void setYearAmnt(BigDecimal yearAmnt) {
		this.yearAmnt = yearAmnt;
	}
	
	
}
