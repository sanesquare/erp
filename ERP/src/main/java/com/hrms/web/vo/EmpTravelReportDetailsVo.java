package com.hrms.web.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpTravelReportDetailsVo {

	private int slno;
	
	private String empName;
	
	@DateTimeFormat
	private Date travelStartDate;
	
	@DateTimeFormat
	private Date travelEndDate;
	
	private double expBudget;
	
	private double actBudget;
	
	private String purpose;
	
	private String startDateString;
	
	private String endDateString;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the travelStartDate
	 */
	public Date getTravelStartDate() {
		return travelStartDate;
	}

	/**
	 * @param travelStartDate the travelStartDate to set
	 */
	public void setTravelStartDate(Date travelStartDate) {
		this.travelStartDate = travelStartDate;
	}

	/**
	 * @return the travelEndDate
	 */
	public Date getTravelEndDate() {
		return travelEndDate;
	}

	/**
	 * @param travelEndDate the travelEndDate to set
	 */
	public void setTravelEndDate(Date travelEndDate) {
		this.travelEndDate = travelEndDate;
	}

	/**
	 * @return the expBudget
	 */
	public double getExpBudget() {
		return expBudget;
	}

	/**
	 * @param expBudget the expBudget to set
	 */
	public void setExpBudget(double expBudget) {
		this.expBudget = expBudget;
	}

	/**
	 * @return the actBudget
	 */
	public double getActBudget() {
		return actBudget;
	}

	/**
	 * @param actBudget the actBudget to set
	 */
	public void setActBudget(double actBudget) {
		this.actBudget = actBudget;
	}

	/**
	 * @return the purpose
	 */
	public String getPurpose() {
		return purpose;
	}

	/**
	 * @param purpose the purpose to set
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	/**
	 * @return the startDateString
	 */
	public String getStartDateString() {
		return startDateString;
	}

	/**
	 * @param startDateString the startDateString to set
	 */
	public void setStartDateString(String startDateString) {
		this.startDateString = startDateString;
	}

	/**
	 * @return the endDateString
	 */
	public String getEndDateString() {
		return endDateString;
	}

	/**
	 * @param endDateString the endDateString to set
	 */
	public void setEndDateString(String endDateString) {
		this.endDateString = endDateString;
	}

}
