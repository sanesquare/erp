package com.hrms.web.vo;

import java.math.BigDecimal;
import java.sql.Time;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayOtDetailsVo {

	private int slno;
	
	private String empName;
	
	private Time inTime;
	
	private Time outTime;
	
	private double hours;
	
	private BigDecimal amount;
	
	private String status;

	private String otIN;
	
	private String otOut;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the inTime
	 */
	public Time getInTime() {
		return inTime;
	}

	/**
	 * @param inTime the inTime to set
	 */
	public void setInTime(Time inTime) {
		this.inTime = inTime;
	}

	/**
	 * @return the outTime
	 */
	public Time getOutTime() {
		return outTime;
	}

	/**
	 * @param outTime the outTime to set
	 */
	public void setOutTime(Time outTime) {
		this.outTime = outTime;
	}

	/**
	 * @return the hours
	 */
	public double getHours() {
		return hours;
	}

	/**
	 * @param hours the hours to set
	 */
	public void setHours(double hours) {
		this.hours = hours;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the otIN
	 */
	public String getOtIN() {
		return otIN;
	}

	/**
	 * @param otIN the otIN to set
	 */
	public void setOtIN(String otIN) {
		this.otIN = otIN;
	}

	/**
	 * @return the otOut
	 */
	public String getOtOut() {
		return otOut;
	}

	/**
	 * @param otOut the otOut to set
	 */
	public void setOtOut(String otOut) {
		this.otOut = otOut;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
