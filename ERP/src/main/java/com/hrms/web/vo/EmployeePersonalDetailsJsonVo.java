package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 2-April-2015
 */
public class EmployeePersonalDetailsJsonVo {
	
	private Long employeeId;
	
	private String firstName;

	private String lastName;

	private String dateOfBirth;
	
	private String salutation;
	
	private String gender;
	
	private Long relegionId;
	
	private Long bloodGroupId;

	private Long nationalityId;

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getRelegionId() {
		return relegionId;
	}

	public void setRelegionId(Long relegionId) {
		this.relegionId = relegionId;
	}

	public Long getBloodGroupId() {
		return bloodGroupId;
	}

	public void setBloodGroupId(Long bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}

	public Long getNationalityId() {
		return nationalityId;
	}

	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}


}
