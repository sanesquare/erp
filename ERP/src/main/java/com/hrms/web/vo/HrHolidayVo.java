package com.hrms.web.vo;

import java.util.Date;

/***
 * 
 * @author Vinutha
 * @sincce 18-April-2015
 *
 */
public class HrHolidayVo {

	private Long branchId;
	
	private String startDate;
	
	private String endDate;

	/**
	 * @return the branchId
	 */
	public Long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
}
