package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayPFDetails {

	private int slno;
	
	private String acNo;
	
	private String empName;
	
	private String department;
	
	private String pfId;
	
	private BigDecimal basicWages = new BigDecimal(0.00);
	
	private BigDecimal empShare = new BigDecimal(0.00);
	
	private BigDecimal orgShare = new BigDecimal(0.00);
	
	private BigDecimal total = new BigDecimal(0.00);
	
	private String branch;
	
	private String employeeCode;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the acNo
	 */
	public String getAcNo() {
		return acNo;
	}

	/**
	 * @param acNo the acNo to set
	 */
	public void setAcNo(String acNo) {
		this.acNo = acNo;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the pfId
	 */
	public String getPfId() {
		return pfId;
	}

	/**
	 * @param pfId the pfId to set
	 */
	public void setPfId(String pfId) {
		this.pfId = pfId;
	}

	/**
	 * @return the basicWages
	 */
	public BigDecimal getBasicWages() {
		return basicWages;
	}

	/**
	 * @param basicWages the basicWages to set
	 */
	public void setBasicWages(BigDecimal basicWages) {
		this.basicWages = basicWages;
	}

	/**
	 * @return the empShare
	 */
	public BigDecimal getEmpShare() {
		return empShare;
	}

	/**
	 * @param empShare the empShare to set
	 */
	public void setEmpShare(BigDecimal empShare) {
		this.empShare = empShare;
	}

	/**
	 * @return the orgShare
	 */
	public BigDecimal getOrgShare() {
		return orgShare;
	}

	/**
	 * @param orgShare the orgShare to set
	 */
	public void setOrgShare(BigDecimal orgShare) {
		this.orgShare = orgShare;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	
}
