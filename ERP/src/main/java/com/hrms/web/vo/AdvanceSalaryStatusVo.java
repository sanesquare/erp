package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public class AdvanceSalaryStatusVo {

	private Long advanceSalaryStatusId;

	private String status;

	private String tempStatus;

	/**
	 * 
	 * @return
	 */
	public String getTempStatus() {
		return tempStatus;
	}

	/**
	 * 
	 * @param tempStatus
	 */
	public void setTempStatus(String tempStatus) {
		this.tempStatus = tempStatus;
	}

	/**
	 * 
	 * @return
	 */
	public Long getAdvanceSalaryStatusId() {
		return advanceSalaryStatusId;
	}

	/**
	 * 
	 * @param advanceSalaryStatusId
	 */
	public void setAdvanceSalaryStatusId(Long advanceSalaryStatusId) {
		this.advanceSalaryStatusId = advanceSalaryStatusId;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
