package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 16-April-2015
 *
 */
public class EmpTypeStatusVo {

	private String type ;
	
	private int number;

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}
	
	
	
	
}
