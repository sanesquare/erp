package com.hrms.web.vo;
/**
 * 
 * @author shamsheer
 * @since 21-March-2015
 */
public class RelegionVo {

	private String relegion;
	
	private Long relegionId;

	/**
	 * @return the relegion
	 */
	public String getRelegion() {
		return relegion;
	}

	/**
	 * @param relegion the relegion to set
	 */
	public void setRelegion(String relegion) {
		this.relegion = relegion;
	}

	/**
	 * @return the relegionId
	 */
	public Long getRelegionId() {
		return relegionId;
	}

	/**
	 * @param relegionId the relegionId to set
	 */
	public void setRelegionId(Long relegionId) {
		this.relegionId = relegionId;
	}
	
	
}
