package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class EmployeeTypeVo {

	private Long employeeTypeId;

	private String employeeType;

	private String tempEmployeeType;

	/**
	 * 
	 * @return
	 */
	public String getTempEmployeeType() {
		return tempEmployeeType;
	}

	/**
	 * 
	 * @param tempEmployeeType
	 */
	public void setTempEmployeeType(String tempEmployeeType) {
		this.tempEmployeeType = tempEmployeeType;
	}

	/**
	 * 
	 * @return
	 */
	public Long getEmployeeTypeId() {
		return employeeTypeId;
	}

	/**
	 * 
	 * @param employeeTypeId
	 */
	public void setEmployeeTypeId(Long employeeTypeId) {
		this.employeeTypeId = employeeTypeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeType() {
		return employeeType;
	}

	/**
	 * 
	 * @param employeeType
	 */
	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

}
