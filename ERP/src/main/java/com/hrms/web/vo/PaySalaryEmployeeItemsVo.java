package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Shamsheer
 * @since 09-June-2015
 */
public class PaySalaryEmployeeItemsVo {

	private Boolean isPaid;
	
	private String checkBoxValue;
	
	private Map<String, BigDecimal> items = new HashMap<String, BigDecimal>();
	
	private Long monthlyId;
	
	private Map<String, BigDecimal> otherSalaryItems = new HashMap<String, BigDecimal>();
	
	private List<String> otherSalaryItemsList = new ArrayList<String>();
	
	private List<String> salaryItemsList = new ArrayList<String>();
	
	private String employeeCode;

	private String name;
	
	private String date;

	private String branch;

	private String department;

	private String designation;
	
	private String pfApplicability = "No";
	
	private String esiApplicability = "No";
	
	private String status;

	private BigDecimal pf = new BigDecimal(0.00);
	
	private Long totlaWorkingDays;
	
	private BigDecimal perDayCalculation = new BigDecimal(0.00);

	private BigDecimal totalEarnings = new BigDecimal(0.00);
	
	private BigDecimal bonus = new BigDecimal(0.00);
	
	private BigDecimal increment = new BigDecimal(0.00);
	
	private BigDecimal lopDeduction = new BigDecimal(0.00);
	
	private BigDecimal totalDeduction = new BigDecimal(0.00);
	
	private BigDecimal netPay = new BigDecimal(0.00);
	
	private BigDecimal ctc = new BigDecimal(0.00);
	
	private Double lopCount;
	
	private BigDecimal advanceSalary = new BigDecimal(0.00);
	
	private BigDecimal conveyance = new BigDecimal(0.00);
	
	private BigDecimal esi = new BigDecimal(0.00);

	private BigDecimal basic = new BigDecimal(0.00);

	private BigDecimal da = new BigDecimal(0.00);

	private BigDecimal hra = new BigDecimal(0.00);

	private BigDecimal cca = new BigDecimal(0.00);

	private BigDecimal grossSalary = new BigDecimal(0.00);

	private BigDecimal pfEmployer = new BigDecimal(0.00);

	private BigDecimal esiEmployer = new BigDecimal(0.00);
	
	private BigDecimal lwf = new BigDecimal(0.00);
	
	private BigDecimal professionalTaxs = new BigDecimal(0.00);
	
	private BigDecimal deductions = new BigDecimal(0.00);
	
	private BigDecimal encashment = new BigDecimal(0.00);
	
	private BigDecimal incentive = new BigDecimal(0.00);
	
	private BigDecimal loan = new BigDecimal(0.00);
	
	private BigDecimal tax = new BigDecimal(0.00);
	
	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}


	public BigDecimal getBasic() {
		return basic;
	}

	public void setBasic(BigDecimal basic) {
		this.basic = basic;
	}

	public BigDecimal getDa() {
		return da;
	}

	public void setDa(BigDecimal da) {
		this.da = da;
	}

	public BigDecimal getHra() {
		return hra;
	}

	public void setHra(BigDecimal hra) {
		this.hra = hra;
	}

	public BigDecimal getCca() {
		return cca;
	}

	public void setCca(BigDecimal cca) {
		this.cca = cca;
	}

	public BigDecimal getGrossSalary() {
		return grossSalary;
	}

	public void setGrossSalary(BigDecimal grossSalary) {
		this.grossSalary = grossSalary;
	}


	public BigDecimal getPfEmployer() {
		return pfEmployer;
	}

	public void setPfEmployer(BigDecimal pfEmployer) {
		this.pfEmployer = pfEmployer;
	}

	public BigDecimal getEsiEmployer() {
		return esiEmployer;
	}

	public void setEsiEmployer(BigDecimal esiEmployer) {
		this.esiEmployer = esiEmployer;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public BigDecimal getPf() {
		return pf;
	}

	public void setPf(BigDecimal pf) {
		this.pf = pf;
	}

	public BigDecimal getEsi() {
		return esi;
	}

	public void setEsi(BigDecimal esi) {
		this.esi = esi;
	}

	public BigDecimal getLwf() {
		return lwf;
	}

	public void setLwf(BigDecimal lwf) {
		this.lwf = lwf;
	}

	public BigDecimal getProfessionalTaxs() {
		return professionalTaxs;
	}

	public void setProfessionalTaxs(BigDecimal professionalTaxs) {
		this.professionalTaxs = professionalTaxs;
	}

	public BigDecimal getDeductions() {
		return deductions;
	}

	public void setDeductions(BigDecimal deductions) {
		this.deductions = deductions;
	}

	public BigDecimal getEncashment() {
		return encashment;
	}

	public void setEncashment(BigDecimal encashment) {
		this.encashment = encashment;
	}

	public BigDecimal getIncentive() {
		return incentive;
	}

	public void setIncentive(BigDecimal incentive) {
		this.incentive = incentive;
	}

	public BigDecimal getLoan() {
		return loan;
	}

	public void setLoan(BigDecimal loan) {
		this.loan = loan;
	}

	public String getPfApplicability() {
		return pfApplicability;
	}

	public void setPfApplicability(String pfApplicability) {
		this.pfApplicability = pfApplicability;
	}

	public String getEsiApplicability() {
		return esiApplicability;
	}

	public void setEsiApplicability(String esiApplicability) {
		this.esiApplicability = esiApplicability;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getTotalEarnings() {
		return totalEarnings;
	}

	public void setTotalEarnings(BigDecimal totalEarnings) {
		this.totalEarnings = totalEarnings;
	}

	public BigDecimal getAdvanceSalary() {
		return advanceSalary;
	}

	public void setAdvanceSalary(BigDecimal advanceSalary) {
		this.advanceSalary = advanceSalary;
	}

	public BigDecimal getConveyance() {
		return conveyance;
	}

	public void setConveyance(BigDecimal conveyance) {
		this.conveyance = conveyance;
	}

	public Map<String, BigDecimal> getOtherSalaryItems() {
		return otherSalaryItems;
	}

	public void setOtherSalaryItems(Map<String, BigDecimal> otherSalaryItems) {
		this.otherSalaryItems = otherSalaryItems;
	}

	public BigDecimal getBonus() {
		return bonus;
	}

	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus;
	}

	public BigDecimal getIncrement() {
		return increment;
	}

	public void setIncrement(BigDecimal increment) {
		this.increment = increment;
	}

	public BigDecimal getLopDeduction() {
		return lopDeduction;
	}

	public void setLopDeduction(BigDecimal lopDeduction) {
		this.lopDeduction = lopDeduction;
	}

	public Double getLopCount() {
		return lopCount;
	}

	public void setLopCount(Double lopCount) {
		this.lopCount = lopCount;
	}

	public BigDecimal getTotalDeduction() {
		return totalDeduction;
	}

	public void setTotalDeduction(BigDecimal totalDeduction) {
		this.totalDeduction = totalDeduction;
	}

	public BigDecimal getNetPay() {
		return netPay;
	}

	public void setNetPay(BigDecimal netPay) {
		this.netPay = netPay;
	}

	public BigDecimal getCtc() {
		return ctc;
	}

	public void setCtc(BigDecimal ctc) {
		this.ctc = ctc;
	}

	public List<String> getOtherSalaryItemsList() {
		return otherSalaryItemsList;
	}

	public void setOtherSalaryItemsList(List<String> otherSalaryItemsList) {
		this.otherSalaryItemsList = otherSalaryItemsList;
	}

	public Long getMonthlyId() {
		return monthlyId;
	}

	public void setMonthlyId(Long monthlyId) {
		this.monthlyId = monthlyId;
	}

	public Map<String, BigDecimal> getItems() {
		return items;
	}

	public void setItems(Map<String, BigDecimal> items) {
		this.items = items;
	}

	public List<String> getSalaryItemsList() {
		return salaryItemsList;
	}

	public void setSalaryItemsList(List<String> salaryItemsList) {
		this.salaryItemsList = salaryItemsList;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public BigDecimal getPerDayCalculation() {
		return perDayCalculation;
	}

	public void setPerDayCalculation(BigDecimal perDayCalculation) {
		this.perDayCalculation = perDayCalculation;
	}

	public Long getTotlaWorkingDays() {
		return totlaWorkingDays;
	}

	public void setTotlaWorkingDays(Long totlaWorkingDays) {
		this.totlaWorkingDays = totlaWorkingDays;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

	public String getCheckBoxValue() {
		return checkBoxValue;
	}

	public void setCheckBoxValue(String checkBoxValue) {
		this.checkBoxValue = checkBoxValue;
	}
}
