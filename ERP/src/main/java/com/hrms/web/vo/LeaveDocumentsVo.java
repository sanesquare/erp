package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 6-April-2015
 */
public class LeaveDocumentsVo {

	private Long leaveId;
	
	private String fileName;
	
	private String url;

	public Long getLeaveId() {
		return leaveId;
	}

	public void setLeaveId(Long leaveId) {
		this.leaveId = leaveId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
