package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 28-April-2015
 */
public class PaysSlipVo {
	
	private BigDecimal perDayCalculation = BigDecimal.ZERO;
	
	private Long totalWorkingDays;
	
	private Boolean authEmployee;

	private Long id;
	
	private String fromMonth;
	
	private String fromYear;
	
	private String toMonth;
	
	private String toYear;
	
	private String employee;
	
	private String designation;
	
	private String department;
	
	private String branch;
	
	private String employeeCode;
	
	private String status;
	
	private String statusId;
	
	private String statusdescription;
	
	private BigDecimal total;
	
	private String salaryDate;
	
	private String startDate;
	
	private String endDate;
	
	private String notes;
	
	private String createdBy;
	
	private String createdOn;
	
	private String fromDate;
	
	private String toDate;
	
	private List<PayslipEmployeeItemsVo> items = new ArrayList<PayslipEmployeeItemsVo>();

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public List<PayslipEmployeeItemsVo> getItems() {
		return items;
	}

	public void setItems(List<PayslipEmployeeItemsVo> items) {
		this.items = items;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getSalaryDate() {
		return salaryDate;
	}

	public void setSalaryDate(String salaryDate) {
		this.salaryDate = salaryDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(String fromMonth) {
		this.fromMonth = fromMonth;
	}

	public String getFromYear() {
		return fromYear;
	}

	public void setFromYear(String fromYear) {
		this.fromYear = fromYear;
	}

	public String getToMonth() {
		return toMonth;
	}

	public void setToMonth(String toMonth) {
		this.toMonth = toMonth;
	}

	public String getToYear() {
		return toYear;
	}

	public void setToYear(String toYear) {
		this.toYear = toYear;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getStatusdescription() {
		return statusdescription;
	}

	public void setStatusdescription(String statusdescription) {
		this.statusdescription = statusdescription;
	}

	public Boolean getAuthEmployee() {
		return authEmployee;
	}

	public void setAuthEmployee(Boolean authEmployee) {
		this.authEmployee = authEmployee;
	}

	public BigDecimal getPerDayCalculation() {
		return perDayCalculation;
	}

	public void setPerDayCalculation(BigDecimal perDayCalculation) {
		this.perDayCalculation = perDayCalculation;
	}

	public Long getTotalWorkingDays() {
		return totalWorkingDays;
	}

	public void setTotalWorkingDays(Long totalWorkingDays) {
		this.totalWorkingDays = totalWorkingDays;
	}

}
