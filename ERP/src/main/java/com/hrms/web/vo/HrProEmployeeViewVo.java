package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 19-April-2015
 *
 */
public class HrProEmployeeViewVo {

	private List<HrProEmployeeDetailsVo> detailsVo;
	
	private String type;

	/**
	 * @return the detailsVo
	 */
	public List<HrProEmployeeDetailsVo> getDetailsVo() {
		return detailsVo;
	}

	/**
	 * @param detailsVo the detailsVo to set
	 */
	public void setDetailsVo(List<HrProEmployeeDetailsVo> detailsVo) {
		this.detailsVo = detailsVo;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
