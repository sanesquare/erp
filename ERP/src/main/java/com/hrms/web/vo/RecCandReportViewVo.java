package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Vinutha
 * @since 20-April-2015
 *
 */
public class RecCandReportViewVo {

	private List<RecCandidatesReportDetailsVo> detailsVo;
	
	private String type;

	public List<RecCandidatesReportDetailsVo> getDetailsVo() {
		return detailsVo;
	}

	public void setDetailsVo(List<RecCandidatesReportDetailsVo> detailsVo) {
		this.detailsVo = detailsVo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
