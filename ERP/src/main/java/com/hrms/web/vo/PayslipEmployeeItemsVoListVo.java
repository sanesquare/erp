package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer
 * @since 11-May-2015
 */
public class PayslipEmployeeItemsVoListVo {

	private BigDecimal total = new BigDecimal(0);
	
	private List<PayslipEmployeeItemsVo> itemsVos = new ArrayList<PayslipEmployeeItemsVo>();

	public List<PayslipEmployeeItemsVo> getItemsVos() {
		return itemsVos;
	}

	public void setItemsVos(List<PayslipEmployeeItemsVo> itemsVos) {
		this.itemsVos = itemsVos;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
}
