package com.hrms.web.vo;

import java.util.ArrayList;
import java.util.List;

public class RestServiceVo {

	private String key;
	private String secret;
	
	private List<RestBiometricAttendanceVo> attendanceVos = new ArrayList<RestBiometricAttendanceVo>();
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}
	public List<RestBiometricAttendanceVo> getAttendanceVos() {
		return attendanceVos;
	}
	public void setAttendanceVos(List<RestBiometricAttendanceVo> attendanceVos) {
		this.attendanceVos = attendanceVos;
	}
}
