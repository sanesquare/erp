package com.hrms.web.vo;
/**
 * 
 * @author shamsheer
 * @since 22-March-2015
 */
public class EmployeeWorkExperiencVo {

	private String organisation;
	
	private String designation;
	
	private String jobField;
	
	private String description;
	
	private String startingSalary;
	
	private String endingSalary;
	
	private String startDate;
	
	private String endDate;
	
	

	/**
	 * @return the organisation
	 */
	public String getOrganisation() {
		return organisation;
	}

	/**
	 * @param organisation the organisation to set
	 */
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the jobField
	 */
	public String getJobField() {
		return jobField;
	}

	/**
	 * @param jobField the jobField to set
	 */
	public void setJobField(String jobField) {
		this.jobField = jobField;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the startingSalary
	 */
	public String getStartingSalary() {
		return startingSalary;
	}

	/**
	 * @param startingSalary the startingSalary to set
	 */
	public void setStartingSalary(String startingSalary) {
		this.startingSalary = startingSalary;
	}

	/**
	 * @return the endingSalary
	 */
	public String getEndingSalary() {
		return endingSalary;
	}

	/**
	 * @param endingSalary the endingSalary to set
	 */
	public void setEndingSalary(String endingSalary) {
		this.endingSalary = endingSalary;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
}
