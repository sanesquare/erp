package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Vinutha 
 * @since 22-April-2015
 *
 */
public class PayEsiDetailsVo {

	private int slno;
	
	private String esciId;
	
	private String acNo;
	
	private String empName;
	
	private String department;
	
	private BigDecimal grossSalary  = new BigDecimal(0.00);
	
	private int daysPresent;
	
	private BigDecimal empContribution = new BigDecimal(0.00);
	
	private BigDecimal orgContribution = new BigDecimal(0.00);
	
	private String branch;
	
	private BigDecimal total = new BigDecimal(0.00);

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the esciId
	 */
	public String getEsciId() {
		return esciId;
	}

	/**
	 * @param esciId the esciId to set
	 */
	public void setEsciId(String esciId) {
		this.esciId = esciId;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the grossSalary
	 */
	public BigDecimal getGrossSalary() {
		return grossSalary;
	}

	/**
	 * @param grossSalary the grossSalary to set
	 */
	public void setGrossSalary(BigDecimal grossSalary) {
		this.grossSalary = grossSalary;
	}

	/**
	 * @return the daysPresent
	 */
	public int getDaysPresent() {
		return daysPresent;
	}

	/**
	 * @param daysPresent the daysPresent to set
	 */
	public void setDaysPresent(int daysPresent) {
		this.daysPresent = daysPresent;
	}

	/**
	 * @return the empContribution
	 */
	public BigDecimal getEmpContribution() {
		return empContribution;
	}

	/**
	 * @param empContribution the empContribution to set
	 */
	public void setEmpContribution(BigDecimal empContribution) {
		this.empContribution = empContribution;
	}

	/**
	 * @return the orgContribution
	 */
	public BigDecimal getOrgContribution() {
		return orgContribution;
	}

	/**
	 * @param orgContribution the orgContribution to set
	 */
	public void setOrgContribution(BigDecimal orgContribution) {
		this.orgContribution = orgContribution;
	}

	/**
	 * @return the acNo
	 */
	public String getAcNo() {
		return acNo;
	}

	/**
	 * @param acNo the acNo to set
	 */
	public void setAcNo(String acNo) {
		this.acNo = acNo;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
}
