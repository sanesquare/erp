package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 27-March-2015
 */
public class EmployeeLeaveVo {

	private Long leaveId;
	
	private Long employeeId;
	
	private Long leaveTitleId;
	
	private String leaveTitle;
	
	private String leaveType;
	
	private Long leaves;
	
	private String leaveStatus;
	
	private Long leavesTaken;
	
	private Long leavesBalance;
	
	private String carryOver;

	/**
	 * @return the leaveId
	 */
	public Long getLeaveId() {
		return leaveId;
	}

	/**
	 * @param leaveId the leaveId to set
	 */
	public void setLeaveId(Long leaveId) {
		this.leaveId = leaveId;
	}

	/**
	 * @return the employeeId
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the leaveTitleId
	 */
	public Long getLeaveTitleId() {
		return leaveTitleId;
	}

	/**
	 * @param leaveTitleId the leaveTitleId to set
	 */
	public void setLeaveTitleId(Long leaveTitleId) {
		this.leaveTitleId = leaveTitleId;
	}

	/**
	 * @return the leaveType
	 */
	public String getLeaveType() {
		return leaveType;
	}

	/**
	 * @param leaveType the leaveType to set
	 */
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}


	/**
	 * @return the carryOver
	 */
	public String getCarryOver() {
		return carryOver;
	}

	/**
	 * @param carryOver the carryOver to set
	 */
	public void setCarryOver(String carryOver) {
		this.carryOver = carryOver;
	}

	/**
	 * @return the leaveStatus
	 */
	public String getLeaveStatus() {
		return leaveStatus;
	}

	/**
	 * @param leaveStatus the leaveStatus to set
	 */
	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	/**
	 * @return the leaves
	 */
	public Long getLeaves() {
		return leaves;
	}

	/**
	 * @param leaves the leaves to set
	 */
	public void setLeaves(Long leaves) {
		this.leaves = leaves;
	}

	public String getLeaveTitle() {
		return leaveTitle;
	}

	public void setLeaveTitle(String leaveTitle) {
		this.leaveTitle = leaveTitle;
	}

	public Long getLeavesTaken() {
		return leavesTaken;
	}

	public void setLeavesTaken(Long leavesTaken) {
		this.leavesTaken = leavesTaken;
	}

	public Long getLeavesBalance() {
		return leavesBalance;
	}

	public void setLeavesBalance(Long leavesBalance) {
		this.leavesBalance = leavesBalance;
	}
	
	

}
