package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class TimeZoneVo {

	private Long timeZoneId;

	private String offSet;

	private String location;

	public Long getTimeZoneId() {
		return timeZoneId;
	}

	/**
	 * 
	 * @param timeZoneId
	 */
	public void setTimeZoneId(Long timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	/**
	 * 
	 * @return
	 */
	public String getOffSet() {
		return offSet;
	}

	/**
	 * 
	 * @param offSet
	 */
	public void setOffSet(String offSet) {
		this.offSet = offSet;
	}

	/**
	 * 
	 * @return
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * 
	 * @param location
	 */
	public void setLocation(String location) {
		this.location = location;
	}

}
