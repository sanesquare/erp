package com.hrms.web.vo;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.entities.JobPostDocuments;

/**
 * 
 * 
 * @author Vinutha
 * @since 19-March-2015
 *
 */
public class JobPostVo {

	private Long jobPostId;
	
	private String department;
	
	private Long departmentId;
	
	private String jobTitle;
	
	private String jobType;
	
	private Long jobTypeId;
	
	private String positions;
	
	private String ageStart;
	
	private String ageEnd;
	
	private Long branchId;
	
	private List<Long> branchIds;
	
	private String location;
	
	private List<String> locations;
	
	private String startSalary;
	
	private String endSalary;
	
	private String jobPostEndDate;
	
	private String qualification;
	
	private int experienceYears;
	
	private int experienceMonths;
	
	private String postDescription;
	
	private String postAdditionalInfo;
	
	private String createdBy;
	
	private String createdOn;

	private String documentsPath;
	
	private String documentAccessUrl;
	
	private String documentName;
	
	private Set<JobPostDocumentsVo> jobPostDocumentsVos;
	
	private List<MultipartFile> uploadPostDocs;
	
	/**
	 * @return the jobPostId
	 */
	public Long getJobPostId() {
		return jobPostId;
	}

	/**
	 * @param jobPostId the jobPostId to set
	 */
	public void setJobPostId(Long jobPostId) {
		this.jobPostId = jobPostId;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the departmentId
	 */
	public Long getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId the departmentId to set
	 */
	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the jobTitle
	 */
	public String getJobTitle() {
		return jobTitle;
	}

	/**
	 * @param jobTitle the jobTitle to set
	 */
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	/**
	 * @return the jobType
	 */
	public String getJobType() {
		return jobType;
	}

	/**
	 * @param jobType the jobType to set
	 */
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	/**
	 * @return the jobTypeId
	 */
	public Long getJobTypeId() {
		return jobTypeId;
	}

	/**
	 * @param jobTypeId the jobTypeId to set
	 */
	public void setJobTypeId(Long jobTypeId) {
		this.jobTypeId = jobTypeId;
	}

	/**
	 * @return the positions
	 */
	public String getPositions() {
		return positions;
	}

	/**
	 * @param positions the positions to set
	 */
	public void setPositions(String positions) {
		this.positions = positions;
	}

	/**
	 * @return the ageStart
	 */
	public String getAgeStart() {
		return ageStart;
	}

	/**
	 * @param ageStart the ageStart to set
	 */
	public void setAgeStart(String ageStart) {
		this.ageStart = ageStart;
	}

	/**
	 * @return the ageEnd
	 */
	public String getAgeEnd() {
		return ageEnd;
	}

	/**
	 * @param ageEnd the ageEnd to set
	 */
	public void setAgeEnd(String ageEnd) {
		this.ageEnd = ageEnd;
	}

	/**
	 * @return the branchId
	 */
	public Long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the startSalary
	 */
	public String getStartSalary() {
		return startSalary;
	}

	/**
	 * @param startSalary the startSalary to set
	 */
	public void setStartSalary(String startSalary) {
		this.startSalary = startSalary;
	}

	/**
	 * @return the endSalary
	 */
	public String getEndSalary() {
		return endSalary;
	}

	/**
	 * @param endSalary the endSalary to set
	 */
	public void setEndSalary(String endSalary) {
		this.endSalary = endSalary;
	}

	/**
	 * @return the jobPostEndDate
	 */
	public String getJobPostEndDate() {
		return jobPostEndDate;
	}

	/**
	 * @param jobPostEndDate the jobPostEndDate to set
	 */
	public void setJobPostEndDate(String jobPostEndDate) {
		this.jobPostEndDate = jobPostEndDate;
	}

	/**
	 * @return the qualification
	 */
	public String getQualification() {
		return qualification;
	}

	/**
	 * @param qualification the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	
	/**
	 * @return the postDescription
	 */
	public String getPostDescription() {
		return postDescription;
	}

	/**
	 * @param postDescription the postDescription to set
	 */
	public void setPostDescription(String postDescription) {
		this.postDescription = postDescription;
	}

	/**
	 * @return the postAdditionalInfo
	 */
	public String getPostAdditionalInfo() {
		return postAdditionalInfo;
	}

	/**
	 * @param postAdditionalInfo the postAdditionalInfo to set
	 */
	public void setPostAdditionalInfo(String postAdditionalInfo) {
		this.postAdditionalInfo = postAdditionalInfo;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the documentAccessUrl
	 */
	public String getDocumentAccessUrl() {
		return documentAccessUrl;
	}

	/**
	 * @param documentAccessUrl the documentAccessUrl to set
	 */
	public void setDocumentAccessUrl(String documentAccessUrl) {
		this.documentAccessUrl = documentAccessUrl;
	}

	/**
	 * @return the documentName
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * @param documentName the documentName to set
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}



	/**
	 * @return the documentsPath
	 */
	public String getDocumentsPath() {
		return documentsPath;
	}

	/**
	 * @param documentsPath the documentsPath to set
	 */
	public void setDocumentsPath(String documentsPath) {
		this.documentsPath = documentsPath;
	}

	/**
	 * @return the jobPostDocumentsVos
	 */
	public Set<JobPostDocumentsVo> getJobPostDocumentsVos() {
		return jobPostDocumentsVos;
	}

	/**
	 * @param jobPostDocumentsVos the jobPostDocumentsVos to set
	 */
	public void setJobPostDocumentsVos(Set<JobPostDocumentsVo> jobPostDocumentsVos) {
		this.jobPostDocumentsVos = jobPostDocumentsVos;
	}

	/**
	 * @return the uploadPostDocs
	 */
	public List<MultipartFile> getUploadPostDocs() {
		return uploadPostDocs;
	}

	/**
	 * @param uploadPostDocs the uploadPostDocs to set
	 */
	public void setUploadPostDocs(List<MultipartFile> uploadPostDocs) {
		this.uploadPostDocs = uploadPostDocs;
	}

	/**
	 * @return the branchIds
	 */
	public List<Long> getBranchIds() {
		return branchIds;
	}

	/**
	 * @param branchIds the branchIds to set
	 */
	public void setBranchIds(List<Long> branchIds) {
		this.branchIds = branchIds;
	}

	/**
	 * @return the locations
	 */
	public List<String> getLocations() {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations(List<String> locations) {
		this.locations = locations;
	}

	/**
	 * @return the experienceYears
	 */
	public int getExperienceYears() {
		return experienceYears;
	}

	/**
	 * @param experienceYears the experienceYears to set
	 */
	public void setExperienceYears(int experienceYears) {
		this.experienceYears = experienceYears;
	}

	/**
	 * @return the experienceMonths
	 */
	public int getExperienceMonths() {
		return experienceMonths;
	}

	/**
	 * @param experienceMonths the experienceMonths to set
	 */
	public void setExperienceMonths(int experienceMonths) {
		this.experienceMonths = experienceMonths;
	}

	
	
}
