package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer
 * @since 11-June-2015
 */
public class PaySalaryHourlySalaryVo {

	private Boolean isPaid=false;
	
	private String checkBoxValue;
	
	private Long hourlySalaryId;
	
	private String dateMonthYear;
	
	private String hourlyWageDate;
	
	private String dateUpTo;
	
	private String employeeCode;
	
	private String employeeName;
	
	private Double totalHours;
	
	private BigDecimal hourlyRate;
	
	private BigDecimal amount;

	public String getDateMonthYear() {
		return dateMonthYear;
	}

	public void setDateMonthYear(String dateMonthYear) {
		this.dateMonthYear = dateMonthYear;
	}

	public String getDateUpTo() {
		return dateUpTo;
	}

	public void setDateUpTo(String dateUpTo) {
		this.dateUpTo = dateUpTo;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Double getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Double totalHours) {
		this.totalHours = totalHours;
	}

	public BigDecimal getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(BigDecimal hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getHourlyWageDate() {
		return hourlyWageDate;
	}

	public void setHourlyWageDate(String hourlyWageDate) {
		this.hourlyWageDate = hourlyWageDate;
	}

	public Long getHourlySalaryId() {
		return hourlySalaryId;
	}

	public void setHourlySalaryId(Long hourlySalaryId) {
		this.hourlySalaryId = hourlySalaryId;
	}

	public String getCheckBoxValue() {
		return checkBoxValue;
	}

	public void setCheckBoxValue(String checkBoxValue) {
		this.checkBoxValue = checkBoxValue;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}
}
