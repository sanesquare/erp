package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Shamsheer
 * @since 16-April-2015
 */
public class ReimbursementsVo {
	
	private Boolean viewStatus;
	
	private String employee;
	
	private Long reimbursementId;
	
	private Long statusId;
	
	private Long employeeId;
	
	private List<Long> superiorIds;
	
	private List<Long> superiorAjaxIds;
	
	private String employeeCode;
	
	private String employeeName;
	
	private List<String> superiorName;
	
	private List<String> superiorCodes;
	
	private String title;
	
	private String amount;
	
	private String date;
	
	private String description;
	
	private String statusDescription;
	
	private String notes;
	
	private String status;
	
	private String createdBy;
	
	private String createdOn;

	private List<MultipartFile> reimbursementDocuments;
	
	private List<ReimbursementsDocumentVo>  documentVos;
	
	private List<ReimbursementsItemsVo> itemsVos;
	
	public List<ReimbursementsItemsVo> getItemsVos() {
		return itemsVos;
	}

	public void setItemsVos(List<ReimbursementsItemsVo> itemsVos) {
		this.itemsVos = itemsVos;
	}

	public Long getReimbursementId() {
		return reimbursementId;
	}

	public void setReimbursementId(Long reimbursementId) {
		this.reimbursementId = reimbursementId;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public List<Long> getSuperiorIds() {
		return superiorIds;
	}

	public void setSuperiorIds(List<Long> superiorIds) {
		this.superiorIds = superiorIds;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public List<String> getSuperiorName() {
		return superiorName;
	}

	public void setSuperiorName(List<String> superiorName) {
		this.superiorName = superiorName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public List<MultipartFile> getReimbursementDocuments() {
		return reimbursementDocuments;
	}

	public void setReimbursementDocuments(List<MultipartFile> reimbursementDocuments) {
		this.reimbursementDocuments = reimbursementDocuments;
	}

	public List<ReimbursementsDocumentVo> getDocumentVos() {
		return documentVos;
	}

	public void setDocumentVos(List<ReimbursementsDocumentVo> documentVos) {
		this.documentVos = documentVos;
	}

	public List<Long> getSuperiorAjaxIds() {
		return superiorAjaxIds;
	}

	public void setSuperiorAjaxIds(List<Long> superiorAjaxIds) {
		this.superiorAjaxIds = superiorAjaxIds;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public List<String> getSuperiorCodes() {
		return superiorCodes;
	}

	public void setSuperiorCodes(List<String> superiorCodes) {
		this.superiorCodes = superiorCodes;
	}

	public Boolean getViewStatus() {
		return viewStatus;
	}

	public void setViewStatus(Boolean viewStatus) {
		this.viewStatus = viewStatus;
	}
}
