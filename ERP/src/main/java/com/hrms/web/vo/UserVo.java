package com.hrms.web.vo;
/**
 * 
 * @author shamsheer
 * @since 19-March-2015
 */
public class UserVo {

	private String employeeCode;
	
	private String userName;
	
	private String employee;

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}
}
