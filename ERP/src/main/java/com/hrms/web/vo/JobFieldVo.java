package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class JobFieldVo {

	private Long jobFieldId;

	private String jobField;

	private String tempJobField;

	/**
	 * 
	 * @return
	 */
	public String getTempJobField() {
		return tempJobField;
	}

	/**
	 * 
	 * @param tempJobField
	 */
	public void setTempJobField(String tempJobField) {
		this.tempJobField = tempJobField;
	}

	/**
	 * 
	 * @return
	 */
	public Long getJobFieldId() {
		return jobFieldId;
	}

	/**
	 * 
	 * @param jobFieldId
	 */
	public void setJobFieldId(Long jobFieldId) {
		this.jobFieldId = jobFieldId;
	}

	/**
	 * 
	 * @return
	 */
	public String getJobField() {
		return jobField;
	}

	/**
	 * 
	 * @param jobField
	 */
	public void setJobField(String jobField) {
		this.jobField = jobField;
	}

}
