package com.hrms.web.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 20-April-2015
 *
 */
public class RecPostReportDetailsVo {

	private int slno;
	
	private String jobTitle;
	
	private String jobType;
	
	private int positions;
	
	private String ageRange;
	
	private String qualification;
	
	private String experience;
	
	private String salaryRange;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date closingDate;
	
	private String closingDateString;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the jobTitle
	 */
	public String getJobTitle() {
		return jobTitle;
	}

	/**
	 * @param jobTitle the jobTitle to set
	 */
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	/**
	 * @return the jobType
	 */
	public String getJobType() {
		return jobType;
	}

	/**
	 * @param jobType the jobType to set
	 */
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	/**
	 * @return the positions
	 */
	public int getPositions() {
		return positions;
	}

	/**
	 * @param positions the positions to set
	 */
	public void setPositions(int positions) {
		this.positions = positions;
	}

	/**
	 * @return the ageRange
	 */
	public String getAgeRange() {
		return ageRange;
	}

	/**
	 * @param ageRange the ageRange to set
	 */
	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}

	/**
	 * @return the qualification
	 */
	public String getQualification() {
		return qualification;
	}

	/**
	 * @param qualification the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	/**
	 * @return the experience
	 */
	public String getExperience() {
		return experience;
	}

	/**
	 * @param experience the experience to set
	 */
	public void setExperience(String experience) {
		this.experience = experience;
	}

	/**
	 * @return the salaryRange
	 */
	public String getSalaryRange() {
		return salaryRange;
	}

	/**
	 * @param salaryRange the salaryRange to set
	 */
	public void setSalaryRange(String salaryRange) {
		this.salaryRange = salaryRange;
	}

	/**
	 * @return the closingDate
	 */
	public Date getClosingDate() {
		return closingDate;
	}

	/**
	 * @param closingDate the closingDate to set
	 */
	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	/**
	 * @return the closingDateString
	 */
	public String getClosingDateString() {
		return closingDateString;
	}

	/**
	 * @param closingDateString the closingDateString to set
	 */
	public void setClosingDateString(String closingDateString) {
		this.closingDateString = closingDateString;
	}


	
}
