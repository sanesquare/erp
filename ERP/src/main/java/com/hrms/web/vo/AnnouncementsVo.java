package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Shimil Babu
 * @since 10-March-2015
 */
public class AnnouncementsVo {

	private String announcementsTitle;

	private String startDate;

	private String endDate;

	private Long announcementsId;

	private String message;

	private String notes;

	private String status;

	private Long stausId;

	private String createdOn;

	private String updatedOn;

	private String createdBy;

	private String updatedBy;

	private boolean sendMail;

	private String statusNotes;

	private String announcementsStatus;

	private Long announcementsStatusId;

	private String announcementsStatusNotes;

	private List<AnnouncementDocumentsVo> announcementDocumentsVos;

	private List<MultipartFile> announcementDocs;

	/*
	 * private String createdOn;
	 * 
	 * private String updatedOn;
	 * 
	 * private String createdBy;
	 * 
	 * private String updatedBy;
	 */

	/**
	 * @return the announcementsTitle
	 */
	public String getAnnouncementsTitle() {
		return announcementsTitle;
	}

	/**
	 * 
	 * @return
	 */
	public List<AnnouncementDocumentsVo> getAnnouncementDocumentsVos() {
		return announcementDocumentsVos;
	}

	/**
	 * 
	 * @param announcementDocumentsVos
	 */
	public void setAnnouncementDocumentsVos(List<AnnouncementDocumentsVo> announcementDocumentsVos) {
		this.announcementDocumentsVos = announcementDocumentsVos;
	}

	/**
	 * 
	 * @return
	 */
	public List<MultipartFile> getAnnouncementDocs() {
		return announcementDocs;
	}

	/**
	 * 
	 * @param announcementDocs
	 */
	public void setAnnouncementDocs(List<MultipartFile> announcementDocs) {
		this.announcementDocs = announcementDocs;
	}

	/**
	 * @param announcementsTitle
	 *            the announcementsTitle to set
	 */
	public void setAnnouncementsTitle(String announcementsTitle) {
		this.announcementsTitle = announcementsTitle;
	}

	/**
	 * @return the announcementsId
	 */
	public Long getAnnouncementsId() {
		return announcementsId;
	}

	/**
	 * @param announcementsId
	 *            the announcementsId to set
	 */
	public void setAnnouncementsId(Long announcementsId) {
		this.announcementsId = announcementsId;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the stausId
	 */
	public Long getStausId() {
		return stausId;
	}

	/**
	 * @param stausId
	 *            the stausId to set
	 */
	public void setStausId(Long stausId) {
		this.stausId = stausId;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public String getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn
	 *            the updatedOn to set
	 */
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 *            the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the sendMail
	 */
	public boolean isSendMail() {
		return sendMail;
	}

	/**
	 * @param sendMail
	 *            the sendMail to set
	 */
	public void setSendMail(boolean sendMail) {
		this.sendMail = sendMail;
	}

	/**
	 * @return the statusNotes
	 */
	public String getStatusNotes() {
		return statusNotes;
	}

	/**
	 * @param statusNotes
	 *            the statusNotes to set
	 */
	public void setStatusNotes(String statusNotes) {
		this.statusNotes = statusNotes;
	}

	/**
	 * @return the announcementsStatus
	 */
	public String getAnnouncementsStatus() {
		return announcementsStatus;
	}

	/**
	 * @param announcementsStatus
	 *            the announcementsStatus to set
	 */
	public void setAnnouncementsStatus(String announcementsStatus) {
		this.announcementsStatus = announcementsStatus;
	}

	/**
	 * @return the announcementsStatusId
	 */
	public Long getAnnouncementsStatusId() {
		return announcementsStatusId;
	}

	/**
	 * @param announcementsStatusId
	 *            the announcementsStatusId to set
	 */
	public void setAnnouncementsStatusId(Long announcementsStatusId) {
		this.announcementsStatusId = announcementsStatusId;
	}

	/**
	 * @return the announcementsStatusNotes
	 */
	public String getAnnouncementsStatusNotes() {
		return announcementsStatusNotes;
	}

	/**
	 * @param announcementsStatusNotes
	 *            the announcementsStatusNotes to set
	 */
	public void setAnnouncementsStatusNotes(String announcementsStatusNotes) {
		this.announcementsStatusNotes = announcementsStatusNotes;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
