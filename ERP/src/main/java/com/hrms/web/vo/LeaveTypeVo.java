package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 26-March-2015
 *
 */
public class LeaveTypeVo {

	private Long leaveTypeId;

	private String leaveType;

	private String tempLeaveType;

	/**
	 * 
	 * @return
	 */
	public String getTempLeaveType() {
		return tempLeaveType;
	}

	/**
	 * 
	 * @param tempLeaveType
	 */
	public void setTempLeaveType(String tempLeaveType) {
		this.tempLeaveType = tempLeaveType;
	}

	/**
	 * 
	 * @return
	 */
	public Long getLeaveTypeId() {
		return leaveTypeId;
	}

	/**
	 * 
	 * @param leaveTypeId
	 */
	public void setLeaveTypeId(Long leaveTypeId) {
		this.leaveTypeId = leaveTypeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getLeaveType() {
		return leaveType;
	}

	/**
	 * 
	 * @param leaveType
	 */
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

}
