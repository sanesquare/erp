package com.hrms.web.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpResignationDetailsVo {

	private int slno;
	
	private String empName;
	
	@DateTimeFormat
	private Date noticeDate;
	
	@DateTimeFormat
	private Date resignDate;
	
	private String noticeDateString;
	
	private String resignDateString;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the noticeDate
	 */
	public Date getNoticeDate() {
		return noticeDate;
	}

	/**
	 * @param noticeDate the noticeDate to set
	 */
	public void setNoticeDate(Date noticeDate) {
		this.noticeDate = noticeDate;
	}

	/**
	 * @return the resignDate
	 */
	public Date getResignDate() {
		return resignDate;
	}

	/**
	 * @param resignDate the resignDate to set
	 */
	public void setResignDate(Date resignDate) {
		this.resignDate = resignDate;
	}

	/**
	 * @return the noticeDateString
	 */
	public String getNoticeDateString() {
		return noticeDateString;
	}

	/**
	 * @param noticeDateString the noticeDateString to set
	 */
	public void setNoticeDateString(String noticeDateString) {
		this.noticeDateString = noticeDateString;
	}

	/**
	 * @return the resignDateString
	 */
	public String getResignDateString() {
		return resignDateString;
	}

	/**
	 * @param resignDateString the resignDateString to set
	 */
	public void setResignDateString(String resignDateString) {
		this.resignDateString = resignDateString;
	}
	
	
}
