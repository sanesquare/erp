package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
public class WorkShiftVo {

	private Long workShiftId;

	private String workShift;

	private String tempWorkShift;

	/**
	 * 
	 * @return
	 */
	public String getTempWorkShift() {
		return tempWorkShift;
	}

	/**
	 * 
	 * @param tempWorkShift
	 */
	public void setTempWorkShift(String tempWorkShift) {
		this.tempWorkShift = tempWorkShift;
	}

	/**
	 * 
	 * @return
	 */
	public Long getWorkShiftId() {
		return workShiftId;
	}

	/**
	 * 
	 * @param workShiftId
	 */
	public void setWorkShiftId(Long workShiftId) {
		this.workShiftId = workShiftId;
	}

	/**
	 * 
	 * @return
	 */
	public String getWorkShift() {
		return workShift;
	}

	/**
	 * 
	 * @param workShift
	 */
	public void setWorkShift(String workShift) {
		this.workShift = workShift;
	}

}
