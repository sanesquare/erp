package com.hrms.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Jithin Mohan
 * @since 5-May-2015
 *
 */
public class EsiSyntaxVo {

	private Long esiSyntaxId;

	private String employeeSyntax;

	private String employerSyntax;

	private String range;

	private BigDecimal amount;

	/**
	 * 
	 * @return
	 */
	public String getRange() {
		return range;
	}

	/**
	 * 
	 * @param range
	 */
	public void setRange(String range) {
		this.range = range;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * 
	 * @return
	 */
	public Long getEsiSyntaxId() {
		return esiSyntaxId;
	}

	/**
	 * 
	 * @param esiSyntaxId
	 */
	public void setEsiSyntaxId(Long esiSyntaxId) {
		this.esiSyntaxId = esiSyntaxId;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeSyntax() {
		return employeeSyntax;
	}

	/**
	 * 
	 * @param employeeSyntax
	 */
	public void setEmployeeSyntax(String employeeSyntax) {
		this.employeeSyntax = employeeSyntax;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployerSyntax() {
		return employerSyntax;
	}

	/**
	 * 
	 * @param employerSyntax
	 */
	public void setEmployerSyntax(String employerSyntax) {
		this.employerSyntax = employerSyntax;
	}
}
