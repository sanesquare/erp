package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public class QualificationDegreeVo {

	private Long degreeId;

	private String degreeName;

	private String tempQualification;

	/**
	 * 
	 * @return
	 */
	public String getTempQualification() {
		return tempQualification;
	}

	/**
	 * 
	 * @param tempQualification
	 */
	public void setTempQualification(String tempQualification) {
		this.tempQualification = tempQualification;
	}

	/**
	 * 
	 * @return
	 */
	public Long getDegreeId() {
		return degreeId;
	}

	/**
	 * 
	 * @param degreeId
	 */
	public void setDegreeId(Long degreeId) {
		this.degreeId = degreeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getDegreeName() {
		return degreeName;
	}

	/**
	 * 
	 * @param degreeName
	 */
	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}

}
