package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public class ExtraPaySlipItemVo {

	private Long extraPaySlipItemId;

	private String title;

	private String type;

	private String taxAllowance;

	private String calculation;

	private String effectFrom;

	/**
	 * 
	 * @return
	 */
	public Long getExtraPaySlipItemId() {
		return extraPaySlipItemId;
	}

	/**
	 * 
	 * @param extraPaySlipItemId
	 */
	public void setExtraPaySlipItemId(Long extraPaySlipItemId) {
		this.extraPaySlipItemId = extraPaySlipItemId;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 
	 * @return
	 */
	public String getTaxAllowance() {
		return taxAllowance;
	}

	/**
	 * 
	 * @param taxAllowance
	 */
	public void setTaxAllowance(String taxAllowance) {
		this.taxAllowance = taxAllowance;
	}

	/**
	 * 
	 * @return
	 */
	public String getCalculation() {
		return calculation;
	}

	/**
	 * 
	 * @param calculation
	 */
	public void setCalculation(String calculation) {
		this.calculation = calculation;
	}

	/**
	 * 
	 * @return
	 */
	public String getEffectFrom() {
		return effectFrom;
	}

	/**
	 * 
	 * @param effectFrom
	 */
	public void setEffectFrom(String effectFrom) {
		this.effectFrom = effectFrom;
	}
}
