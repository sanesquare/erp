package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
public class AccountTypeVo {

	private Long accountTypeId;

	private String accountType;

	private String tempAccountType;

	/**
	 * 
	 * @return
	 */
	public String getTempAccountType() {
		return tempAccountType;
	}

	/**
	 * 
	 * @param tempAccountType
	 */
	public void setTempAccountType(String tempAccountType) {
		this.tempAccountType = tempAccountType;
	}

	/**
	 * 
	 * @return
	 */
	public Long getAccountTypeId() {
		return accountTypeId;
	}

	/**
	 * 
	 * @param accountTypeId
	 */
	public void setAccountTypeId(Long accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * 
	 * @param accountType
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

}
