package com.hrms.web.vo;

import java.util.Date;

/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpPerformanceEvaluationDetailsVo {

	private String evaluation;
	
	private int slno;
	
	private String empName;
	
	private String branch;
	
	private String designation;
	
	private double score;
	
	private double maxMark;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the score
	 */
	public double getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(double score) {
		this.score = score;
	}

	/**
	 * @return the maxMark
	 */
	public double getMaxMark() {
		return maxMark;
	}

	/**
	 * @param maxMark the maxMark to set
	 */
	public void setMaxMark(double maxMark) {
		this.maxMark = maxMark;
	}

	/**
	 * @return the evaluation
	 */
	public String getEvaluation() {
		return evaluation;
	}

	/**
	 * @param evaluation the evaluation to set
	 */
	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}
	

}
