package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 28-March-2015
 *
 */
public class EmployeeGradeVo {

	private Long gradeId;

	private String grade;

	private String tempGrade;

	/**
	 * 
	 * @return
	 */
	public String getTempGrade() {
		return tempGrade;
	}

	/**
	 * 
	 * @param tempGrade
	 */
	public void setTempGrade(String tempGrade) {
		this.tempGrade = tempGrade;
	}

	/**
	 * 
	 * @return
	 */
	public Long getGradeId() {
		return gradeId;
	}

	/**
	 * 
	 * @param gradeId
	 */
	public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}

	/**
	 * 
	 * @return
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * 
	 * @param grade
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

}
