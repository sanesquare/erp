package com.hrms.web.vo;

import java.sql.Time;

/**
 * 
 * @author Shamsheer
 * @since 7-April-2015
 */
public class AttendanceVo {

	private String totalWorkedHours;
	
	private Long attendanceId;
	
	private Long employeeId;
	
	private String employeeCode;
	
	private String date;
	
	private String signInTime;
	
	private String signOutTime;
	
	private String notes;
	
	private String createdBy;
	
	private String status;
	
	private String createdOn;
	
	private String employee;
	
	private Time signIn;
	
	private Time signOut;
	
	private String breakHours;
	
	private Long workedHoursMilli;
	
	private Long breakHoursMilli;

	public Long getAttendanceId() {
		return attendanceId;
	}

	public void setAttendanceId(Long attendanceId) {
		this.attendanceId = attendanceId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSignInTime() {
		return signInTime;
	}

	public void setSignInTime(String signInTime) {
		this.signInTime = signInTime;
	}

	public String getSignOutTime() {
		return signOutTime;
	}

	public void setSignOutTime(String signOutTime) {
		this.signOutTime = signOutTime;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getTotalWorkedHours() {
		return totalWorkedHours;
	}

	public void setTotalWorkedHours(String totalWorkedHours) {
		this.totalWorkedHours = totalWorkedHours;
	}

	public Time getSignIn() {
		return signIn;
	}

	public void setSignIn(Time signIn) {
		this.signIn = signIn;
	}

	public Time getSignOut() {
		return signOut;
	}

	public void setSignOut(Time signOut) {
		this.signOut = signOut;
	}

	public String getBreakHours() {
		return breakHours;
	}

	public void setBreakHours(String breakHours) {
		this.breakHours = breakHours;
	}

	public Long getWorkedHoursMilli() {
		return workedHoursMilli;
	}

	public void setWorkedHoursMilli(Long workedHoursMilli) {
		this.workedHoursMilli = workedHoursMilli;
	}

	public Long getBreakHoursMilli() {
		return breakHoursMilli;
	}

	public void setBreakHoursMilli(Long breakHoursMilli) {
		this.breakHoursMilli = breakHoursMilli;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
