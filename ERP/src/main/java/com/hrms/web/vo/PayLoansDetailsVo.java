package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayLoansDetailsVo {

	private int slno;
	
	private String empName;
	
	private String title;
	
	@DateTimeFormat
	private Date loanDate;
	
	@DateTimeFormat
	private Date repayStartDate;
	
	private BigDecimal loanAmount;
	
	private BigDecimal repaidAmount;
	
	private BigDecimal remainingAmount;

	private String loanDateString;
	
	private String repayStartDateString;
	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the loanDate
	 */
	public Date getLoanDate() {
		return loanDate;
	}

	/**
	 * @param loanDate the loanDate to set
	 */
	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}

	/**
	 * @return the repayStartDate
	 */
	public Date getRepayStartDate() {
		return repayStartDate;
	}

	/**
	 * @param repayStartDate the repayStartDate to set
	 */
	public void setRepayStartDate(Date repayStartDate) {
		this.repayStartDate = repayStartDate;
	}

	

	/**
	 * @return the loanAmount
	 */
	public BigDecimal getLoanAmount() {
		return loanAmount;
	}

	/**
	 * @param loanAmount the loanAmount to set
	 */
	public void setLoanAmount(BigDecimal loanAmount) {
		this.loanAmount = loanAmount;
	}

	/**
	 * @return the repaidAmount
	 */
	public BigDecimal getRepaidAmount() {
		return repaidAmount;
	}

	/**
	 * @param repaidAmount the repaidAmount to set
	 */
	public void setRepaidAmount(BigDecimal repaidAmount) {
		this.repaidAmount = repaidAmount;
	}

	/**
	 * @return the remainingAmount
	 */
	public BigDecimal getRemainingAmount() {
		return remainingAmount;
	}

	/**
	 * @param remainingAmount the remainingAmount to set
	 */
	public void setRemainingAmount(BigDecimal remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	/**
	 * @return the loanDateString
	 */
	public String getLoanDateString() {
		return loanDateString;
	}

	/**
	 * @param loanDateString the loanDateString to set
	 */
	public void setLoanDateString(String loanDateString) {
		this.loanDateString = loanDateString;
	}

	/**
	 * @return the repayStartDateString
	 */
	public String getRepayStartDateString() {
		return repayStartDateString;
	}

	/**
	 * @param repayStartDateString the repayStartDateString to set
	 */
	public void setRepayStartDateString(String repayStartDateString) {
		this.repayStartDateString = repayStartDateString;
	}

	
	
	
}
