package com.hrms.web.vo;
/**
 * 
 * @author Vinutha
 * @since 16-April-2015
 *
 */
public class EmployeeByBranchVo {

	private String branch;
	
	private int branchEmp;

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the branchEmp
	 */
	public int getBranchEmp() {
		return branchEmp;
	}

	/**
	 * @param branchEmp the branchEmp to set
	 */
	public void setBranchEmp(int branchEmp) {
		this.branchEmp = branchEmp;
	}
	
	
}
