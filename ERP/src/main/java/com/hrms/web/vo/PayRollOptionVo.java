package com.hrms.web.vo;

import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public class PayRollOptionVo {

	private Long payRollOptionId;

	private String autoAprovePayRoll;

	private String autoEmailPaySlip;

	private String perDaySalaryCalculation;

	private String perDaySalaryCalculationType;

	private Integer perDaySalaryCalculationDays;

	private List<String> approvers;

	/**
	 * 
	 * @return
	 */
	public List<String> getApprovers() {
		return approvers;
	}

	/**
	 * 
	 * @param approvers
	 */
	public void setApprovers(List<String> approvers) {
		this.approvers = approvers;
	}

	/**
	 * 
	 * @return
	 */
	public Long getPayRollOptionId() {
		return payRollOptionId;
	}

	/**
	 * 
	 * @param payRollOptionId
	 */
	public void setPayRollOptionId(Long payRollOptionId) {
		this.payRollOptionId = payRollOptionId;
	}

	/**
	 * 
	 * @return
	 */
	public String getAutoAprovePayRoll() {
		return autoAprovePayRoll;
	}

	/**
	 * 
	 * @param autoAprovePayRoll
	 */
	public void setAutoAprovePayRoll(String autoAprovePayRoll) {
		this.autoAprovePayRoll = autoAprovePayRoll;
	}

	/**
	 * 
	 * @return
	 */
	public String getAutoEmailPaySlip() {
		return autoEmailPaySlip;
	}

	/**
	 * 
	 * @param autoEmailPaySlip
	 */
	public void setAutoEmailPaySlip(String autoEmailPaySlip) {
		this.autoEmailPaySlip = autoEmailPaySlip;
	}

	/**
	 * 
	 * @return
	 */
	public String getPerDaySalaryCalculation() {
		return perDaySalaryCalculation;
	}

	/**
	 * 
	 * @param perDaySalaryCalculation
	 */
	public void setPerDaySalaryCalculation(String perDaySalaryCalculation) {
		this.perDaySalaryCalculation = perDaySalaryCalculation;
	}

	/**
	 * 
	 * @return
	 */
	public String getPerDaySalaryCalculationType() {
		return perDaySalaryCalculationType;
	}

	/**
	 * 
	 * @param perDaySalaryCalculationType
	 */
	public void setPerDaySalaryCalculationType(String perDaySalaryCalculationType) {
		this.perDaySalaryCalculationType = perDaySalaryCalculationType;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getPerDaySalaryCalculationDays() {
		return perDaySalaryCalculationDays;
	}

	/**
	 * 
	 * @param perDaySalaryCalculationDays
	 */
	public void setPerDaySalaryCalculationDays(Integer perDaySalaryCalculationDays) {
		this.perDaySalaryCalculationDays = perDaySalaryCalculationDays;
	}

}
