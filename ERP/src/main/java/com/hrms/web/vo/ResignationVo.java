package com.hrms.web.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Shamsheer
 * @since 24-April-2015
 */
public class ResignationVo {

	private Boolean viewStatus;
	
	private Long resignationId;
	
	private Long statusId;
	
	private Long employeeId;
	
	private Long superiorId;
	
	private String superiorCodeAjax;
	
	private String status;
	
	private String superior;
	
	private String superiorCode;
	
	private String employee;
	
	private String employeeCode;
	
	private String noticeDate;
	
	private String resignationDate;
	
	private String description;
	
	private String notes;
	
	private String statusDescription;
	
	private String createdBy;
	
	private String createdOn;
	
	private List<MultipartFile> files;
	
	private List<ResignationDocumentsVo> documentsVos;

	public Long getResignationId() {
		return resignationId;
	}

	public void setResignationId(Long resignationId) {
		this.resignationId = resignationId;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getSuperiorId() {
		return superiorId;
	}

	public void setSuperiorId(Long superiorId) {
		this.superiorId = superiorId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSuperior() {
		return superior;
	}

	public void setSuperior(String superior) {
		this.superior = superior;
	}

	public String getSuperiorCode() {
		return superiorCode;
	}

	public void setSuperiorCode(String superiorCode) {
		this.superiorCode = superiorCode;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(String noticeDate) {
		this.noticeDate = noticeDate;
	}

	public String getResignationDate() {
		return resignationDate;
	}

	public void setResignationDate(String resignationDate) {
		this.resignationDate = resignationDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public List<ResignationDocumentsVo> getDocumentsVos() {
		return documentsVos;
	}

	public void setDocumentsVos(List<ResignationDocumentsVo> documentsVos) {
		this.documentsVos = documentsVos;
	}

	public String getSuperiorCodeAjax() {
		return superiorCodeAjax;
	}

	public void setSuperiorCodeAjax(String superiorCodeAjax) {
		this.superiorCodeAjax = superiorCodeAjax;
	}

	public Boolean getViewStatus() {
		return viewStatus;
	}

	public void setViewStatus(Boolean viewStatus) {
		this.viewStatus = viewStatus;
	}
	
}
