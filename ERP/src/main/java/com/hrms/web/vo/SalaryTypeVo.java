package com.hrms.web.vo;
/**
 * 
 * @author Shamsheer
 * @since 18-April-2015
 */
public class SalaryTypeVo {

	private Long id;
	
	private String type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
