package com.hrms.web.vo;
/**
 * 
 * @author shamsheer
 * @since 22-March-2015
 */
public class EmployeeBenefitVo {

	private String benefit;

	/**
	 * @return the benefit
	 */
	public String getBenefit() {
		return benefit;
	}

	/**
	 * @param benefit the benefit to set
	 */
	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}
	
	
}
