package com.hrms.web.vo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayCommisionsDetailsVo {

	private int slno;
	
	private String empName;
	
	private String title;
	
	@DateTimeFormat(pattern ="dd/MM/yyyy")
	private Date date;
	
	private BigDecimal amount;
	
	private String dateString;

	/**
	 * @return the slno
	 */
	public int getSlno() {
		return slno;
	}

	/**
	 * @param slno the slno to set
	 */
	public void setSlno(int slno) {
		this.slno = slno;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the dateString
	 */
	public String getDateString() {
		return dateString;
	}

	/**
	 * @param dateString the dateString to set
	 */
	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	
}
