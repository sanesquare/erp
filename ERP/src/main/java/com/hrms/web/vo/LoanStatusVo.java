package com.hrms.web.vo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public class LoanStatusVo {

	private Long loanStatusId;

	private String loanStatus;

	/**
	 * 
	 * @return
	 */
	public Long getLoanStatusId() {
		return loanStatusId;
	}

	/**
	 * 
	 * @param loanStatusId
	 */
	public void setLoanStatusId(Long loanStatusId) {
		this.loanStatusId = loanStatusId;
	}

	/**
	 * 
	 * @return
	 */
	public String getLoanStatus() {
		return loanStatus;
	}

	/**
	 * 
	 * @param loanStatus
	 */
	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

}
