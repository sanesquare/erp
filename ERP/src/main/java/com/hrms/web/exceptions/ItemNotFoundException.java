package com.hrms.web.exceptions;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public class ItemNotFoundException extends RuntimeException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6665049863168927376L;

	private String message = null;

	/**
	 * 
	 * @param message
	 */
	public ItemNotFoundException(String message) {
		super(message);
		this.message = message;
	}

	/**
	 * 
	 * @param cause
	 */
	public ItemNotFoundException(Throwable cause) {
		super(cause);
	}

	@Override
	public String toString() {
		return message;
	}

	@Override
	public String getMessage() {
		return message;
	}

}
