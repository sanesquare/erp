package com.hrms.web.exceptions;


/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public class DuplicateItemException extends RuntimeException  {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7765526401639441640L;
	
	private String message = null;
	
	/**
	 * 
	 * @param message
	 */
	public DuplicateItemException(String message) {
		super(message);
		this.message = message;
	}

	/**
	 * 
	 * @param cause
	 */
	public DuplicateItemException(Throwable cause) {
		super(cause);
	}

	@Override
	public String toString() {
		return message;
	}

	@Override
	public String getMessage() {
		return message;
	}

}
