package com.hrms.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.erp.web.constants.ErpRequestConstants;
import com.hrms.web.constants.RequestConstants;

/**
 * 
 * @author Jithin Mohan
 * @since 29-March-2015
 *
 */
public class HrmsSuccessAuthenticationHandler implements AuthenticationSuccessHandler {

	/**
	 * 
	 */
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		// Set<String> roles =
		// AuthorityUtils.authorityListToSet(authentication.getAuthorities());
		/*
		 * if (roles.contains("ADMINUSER")) {
		 * response.sendRedirect("admin/adminProductAdd"); return; } else if
		 * (roles.contains("MANAGERUSER")) {
		 * response.sendRedirect("manager/Home"); return; }
		 */
		response.sendRedirect(ErpRequestConstants.HOME+"?jsession=FA");
	}

}
