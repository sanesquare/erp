package com.hrms.web.security;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.persistence.NonUniqueResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.hrms.web.dao.UserDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.service.UserService;



public class UserDetailServiceImpl implements UserDetailsService{

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserDao userDao;
	
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		String password = null;
		MyUser myUser=null;
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		try {
			com.hrms.web.entities.User user = userService.findUserByUsername(username);
			if (user != null) {
				if (user.getEmployee().getIsAdmin())
					authorities.add(new GrantedAuthorityImpl("ADMINUSER"));
				else
					authorities.add(new GrantedAuthorityImpl("NORMALUSER"));
				myUser=new MyUser(username, user.getPassWord(), true, true, true, true,authorities, user);
			} else
				throw new BadCredentialsException("Invalid Username");
			if (!user.getAllowLogin())
				throw new BadCredentialsException("Login access failed, contact administrator");

			

		} catch (EntityNotFoundException e) {
			throw new BadCredentialsException("Invalid user");
		} catch (NonUniqueResultException e) {
			throw new BadCredentialsException("Non-unique user, contact administrator");
		}
		return myUser;

	}

}
