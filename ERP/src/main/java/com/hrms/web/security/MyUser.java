package com.hrms.web.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
/**
 * 
 * @author Sangeeth
 * @since 28-Sep-2015
 */
public class MyUser extends User{

	com.hrms.web.entities.User user;
	
	public MyUser(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities,com.hrms.web.entities.User user) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		 this.user=user;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public com.hrms.web.entities.User getUser() {
		return user;
	}

	public void setUser(com.hrms.web.entities.User user) {
		this.user = user;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
