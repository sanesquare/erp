package com.hrms.web.security;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.persistence.NonUniqueResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.hrms.web.service.UserService;

/**
 * 
 * @author Jithin Mohan
 * @since 29-March-2015
 *
 */
@EnableTransactionManagement
public class HrmsSecureAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	@Autowired
	private UserService userService;

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		return;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {

		String password = null;
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		password = (String) authentication.getCredentials();
		if (!StringUtils.hasText(password)) {
			throw new BadCredentialsException("Please enter password");
		}
		String encryptedPassword = DigestUtils.md5DigestAsHex(password.getBytes());
		String expectedPassword = null;
		try {
			com.hrms.web.entities.User user = userService.findUserByUsername(username);
			if (user != null) {
				expectedPassword = user.getPassWord();
				if (!StringUtils.hasText(expectedPassword)) {
					throw new BadCredentialsException("No password for " + username
							+ " set in database, contact administrator");
				}
			} else
				throw new BadCredentialsException("Invalid Username");
			if (!encryptedPassword.equals(expectedPassword))
				throw new BadCredentialsException("Invalid Password");
			if (!user.getAllowLogin())
				throw new BadCredentialsException("Login access failed, contact administrator");

			if (user.getEmployee().getIsAdmin())
				authorities.add(new GrantedAuthorityImpl("ADMINUSER"));
			else
				authorities.add(new GrantedAuthorityImpl("NORMALUSER"));

		} catch (EntityNotFoundException e) {
			throw new BadCredentialsException("Invalid user");
		} catch (NonUniqueResultException e) {
			throw new BadCredentialsException("Non-unique user, contact administrator");
		}
		return new User(username, password, true, // enabled
				true, // account not expired
				true, // credentials not expired
				true, // account not locked
				authorities);

	}
}
