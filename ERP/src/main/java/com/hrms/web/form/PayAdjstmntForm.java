package com.hrms.web.form;

import java.math.BigDecimal;
import java.util.List;

import com.hrms.web.vo.PayAdjstmntsDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayAdjstmntForm {

	private List<PayAdjstmntsDetailsVo> details ;
	
	private BigDecimal totalAmnt;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<PayAdjstmntsDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<PayAdjstmntsDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the totalAmnt
	 */
	public BigDecimal getTotalAmnt() {
		return totalAmnt;
	}

	/**
	 * @param totalAmnt the totalAmnt to set
	 */
	public void setTotalAmnt(BigDecimal totalAmnt) {
		this.totalAmnt = totalAmnt;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	
}
