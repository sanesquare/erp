package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.EmpAssgmentReportDetailsVo;

/**
 * 
 * @author Vinutha
 * @since
 *
 */
public class EmpAssigmentsReportForm {

	private List<EmpAssgmentReportDetailsVo> details;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<EmpAssgmentReportDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<EmpAssgmentReportDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
