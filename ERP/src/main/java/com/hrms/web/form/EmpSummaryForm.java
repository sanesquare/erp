package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.EmpSummWarnDetailsVo;
import com.hrms.web.vo.EmpSummaryDetailsVo;
import com.hrms.web.vo.EmpSummaryLeaveDetailVo;
import com.hrms.web.vo.EmpSummaryPromotionDetailVo;
import com.hrms.web.vo.EmpSummaryTransferDetailVo;

/**
 * 
 * @author Vinutha
 * @since 6-May-2015
 *
 */
public class EmpSummaryForm {

	private EmpSummaryDetailsVo detail;
	
	private List<EmpSummaryTransferDetailVo> transferDetails;
	
	private List<EmpSummaryLeaveDetailVo> leaveDetails;
	
	private List<EmpSummaryPromotionDetailVo> promoDetails;
	
	private List<EmpSummWarnDetailsVo> warnDetails;
	
	private String type;

	/**
	 * @return the detail
	 */
	public EmpSummaryDetailsVo getDetail() {
		return detail;
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(EmpSummaryDetailsVo detail) {
		this.detail = detail;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the transferDetails
	 */
	public List<EmpSummaryTransferDetailVo> getTransferDetails() {
		return transferDetails;
	}

	/**
	 * @param transferDetails the transferDetails to set
	 */
	public void setTransferDetails(List<EmpSummaryTransferDetailVo> transferDetails) {
		this.transferDetails = transferDetails;
	}

	/**
	 * @return the leaveDetails
	 */
	public List<EmpSummaryLeaveDetailVo> getLeaveDetails() {
		return leaveDetails;
	}

	/**
	 * @param leaveDetails the leaveDetails to set
	 */
	public void setLeaveDetails(List<EmpSummaryLeaveDetailVo> leaveDetails) {
		this.leaveDetails = leaveDetails;
	}

	/**
	 * @return the promoDetails
	 */
	public List<EmpSummaryPromotionDetailVo> getPromoDetails() {
		return promoDetails;
	}

	/**
	 * @param promoDetails the promoDetails to set
	 */
	public void setPromoDetails(List<EmpSummaryPromotionDetailVo> promoDetails) {
		this.promoDetails = promoDetails;
	}

	/**
	 * @return the warnDetails
	 */
	public List<EmpSummWarnDetailsVo> getWarnDetails() {
		return warnDetails;
	}

	/**
	 * @param warnDetails the warnDetails to set
	 */
	public void setWarnDetails(List<EmpSummWarnDetailsVo> warnDetails) {
		this.warnDetails = warnDetails;
	}

	
	
}
