package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.EmpSummWarnDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpWarnReportForm {

	private List<EmpSummWarnDetailsVo> details;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<EmpSummWarnDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<EmpSummWarnDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
