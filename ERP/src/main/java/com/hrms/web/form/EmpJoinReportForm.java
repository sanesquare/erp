package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.EmpJoiningReportDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpJoinReportForm {

	private List<EmpJoiningReportDetailsVo> details;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<EmpJoiningReportDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<EmpJoiningReportDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	
	
}
