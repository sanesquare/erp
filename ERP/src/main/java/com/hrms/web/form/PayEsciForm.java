package com.hrms.web.form;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.hrms.web.vo.PayEsiDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayEsciForm {

	private List<PayEsiDetailsVo> details = new ArrayList<PayEsiDetailsVo>();
	
	private BigDecimal totalEmpContribution = new BigDecimal(0.00);
	
	private BigDecimal totalOrgContribution = new BigDecimal(0.00);
	
	private String type;
	
	private BigDecimal totalTotal = new BigDecimal(0.00);

	/**
	 * @return the details
	 */
	public List<PayEsiDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<PayEsiDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the totalEmpContribution
	 */
	public BigDecimal getTotalEmpContribution() {
		return totalEmpContribution;
	}

	/**
	 * @param totalEmpContribution the totalEmpContribution to set
	 */
	public void setTotalEmpContribution(BigDecimal totalEmpContribution) {
		this.totalEmpContribution = totalEmpContribution;
	}

	/**
	 * @return the totalOrgContribution
	 */
	public BigDecimal getTotalOrgContribution() {
		return totalOrgContribution;
	}

	/**
	 * @param totalOrgContribution the totalOrgContribution to set
	 */
	public void setTotalOrgContribution(BigDecimal totalOrgContribution) {
		this.totalOrgContribution = totalOrgContribution;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getTotalTotal() {
		return totalTotal;
	}

	public void setTotalTotal(BigDecimal totalTotal) {
		this.totalTotal = totalTotal;
	}

	
}
