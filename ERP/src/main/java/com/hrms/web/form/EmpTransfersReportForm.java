package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.EmpTransferDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 21-April-2015
 *
 */
public class EmpTransfersReportForm {

	private List<EmpTransferDetailsVo> details;
	
	private String type;

	
	/**
	 * @return the details
	 */
	public List<EmpTransferDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<EmpTransferDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
