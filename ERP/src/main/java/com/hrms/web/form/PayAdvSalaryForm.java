package com.hrms.web.form;

import java.util.HashMap;
import java.util.List;

import com.hrms.web.vo.PayAdvSalaryDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayAdvSalaryForm {

	private List<PayAdvSalaryDetailsVo> details;
	
	private String type;
	

	/**
	 * @return the details
	 */
	public List<PayAdvSalaryDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<PayAdvSalaryDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}


	
}
