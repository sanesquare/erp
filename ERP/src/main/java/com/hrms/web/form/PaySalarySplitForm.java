package com.hrms.web.form;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.hrms.web.vo.PaySalarySplitDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 5-May-2015
 *
 */
public class PaySalarySplitForm {

	private List<PaySalarySplitDetailsVo> detail = new ArrayList<PaySalarySplitDetailsVo>();
	
	private String type;
	
	private int allowancesSpan;
	
	private List<String>allowanceColumns = new ArrayList<String>();

	/**
	 * @return the detail
	 */
	public List<PaySalarySplitDetailsVo> getDetail() {
		return detail;
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(List<PaySalarySplitDetailsVo> detail) {
		this.detail = detail;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the allowancesSpan
	 */
	public int getAllowancesSpan() {
		return allowancesSpan;
	}

	/**
	 * @param allowancesSpan the allowancesSpan to set
	 */
	public void setAllowancesSpan(int allowancesSpan) {
		this.allowancesSpan = allowancesSpan;
	}

	public List<String> getAllowanceColumns() {
		return allowanceColumns;
	}

	public void setAllowanceColumns(List<String> allowanceColumns) {
		this.allowanceColumns = allowanceColumns;
	}

	

	
	
	
}
