package com.hrms.web.form;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.hrms.web.vo.BranchSalaryVo;
import com.hrms.web.vo.DepartmentSalaryVo;
import com.hrms.web.vo.DesignationSalaryVo;
import com.hrms.web.vo.PayrollSummaryVo;
import com.hrms.web.vo.YearlySalaryVo;

/**
 * 
 * @author Vinutha
 * @since 17-April-2015
 *
 */
public class PayrollSummaryForm {

	private PayrollSummaryVo summary;
	
	private List<YearlySalaryVo> yearlySalary = new ArrayList<YearlySalaryVo>();
	
	private BigDecimal totalYearly;
	
	private List<BranchSalaryVo> branchSalary = new ArrayList<BranchSalaryVo>();
	
	private BigDecimal totalBranch;
	
	private List<DepartmentSalaryVo> depSalary = new ArrayList<DepartmentSalaryVo>();
	
	private BigDecimal totalDep;
	
	private List<DesignationSalaryVo> desSalary = new ArrayList<DesignationSalaryVo>();
	
	private BigDecimal totalDes;
	
	private String type;

	/**
	 * @return the summary
	 */
	public PayrollSummaryVo getSummary() {
		return summary;
	}

	/**
	 * @param summary the summary to set
	 */
	public void setSummary(PayrollSummaryVo summary) {
		this.summary = summary;
	}

	/**
	 * @return the yearlySalary
	 */
	public List<YearlySalaryVo> getYearlySalary() {
		return yearlySalary;
	}

	/**
	 * @param yearlySalary the yearlySalary to set
	 */
	public void setYearlySalary(List<YearlySalaryVo> yearlySalary) {
		this.yearlySalary = yearlySalary;
	}

	/**
	 * @return the totalYearly
	 */
	public BigDecimal getTotalYearly() {
		return totalYearly;
	}

	/**
	 * @param totalYearly the totalYearly to set
	 */
	public void setTotalYearly(BigDecimal totalYearly) {
		this.totalYearly = totalYearly;
	}

	/**
	 * @return the branchSalary
	 */
	public List<BranchSalaryVo> getBranchSalary() {
		return branchSalary;
	}

	/**
	 * @param branchSalary the branchSalary to set
	 */
	public void setBranchSalary(List<BranchSalaryVo> branchSalary) {
		this.branchSalary = branchSalary;
	}

	/**
	 * @return the totalBranch
	 */
	public BigDecimal getTotalBranch() {
		return totalBranch;
	}

	/**
	 * @param totalBranch the totalBranch to set
	 */
	public void setTotalBranch(BigDecimal totalBranch) {
		this.totalBranch = totalBranch;
	}

	/**
	 * @return the depSalary
	 */
	public List<DepartmentSalaryVo> getDepSalary() {
		return depSalary;
	}

	/**
	 * @param depSalary the depSalary to set
	 */
	public void setDepSalary(List<DepartmentSalaryVo> depSalary) {
		this.depSalary = depSalary;
	}

	/**
	 * @return the totalDep
	 */
	public BigDecimal getTotalDep() {
		return totalDep;
	}

	/**
	 * @param totalDep the totalDep to set
	 */
	public void setTotalDep(BigDecimal totalDep) {
		this.totalDep = totalDep;
	}

	/**
	 * @return the desSalary
	 */
	public List<DesignationSalaryVo> getDesSalary() {
		return desSalary;
	}

	/**
	 * @param desSalary the desSalary to set
	 */
	public void setDesSalary(List<DesignationSalaryVo> desSalary) {
		this.desSalary = desSalary;
	}

	/**
	 * @return the totalDes
	 */
	public BigDecimal getTotalDes() {
		return totalDes;
	}

	/**
	 * @param totalDes the totalDes to set
	 */
	public void setTotalDes(BigDecimal totalDes) {
		this.totalDes = totalDes;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
