package com.hrms.web.form;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.hrms.web.vo.PaySalaryDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 2-May-2015
 *
 */
public class PaySalaryReportForm {

	private HashMap<String, Integer> colHeads;
	
	private List<PaySalaryDetailsVo> details;
	
	private BigDecimal totalBasicsSalary;
	
	private double totalTaxAmount;
	
	private List<BigDecimal> totalAllowances = new ArrayList<BigDecimal>();
	
	private BigDecimal totalHra ;
	
	private BigDecimal totalDa;
	
	private BigDecimal grandSalary;
	
	HashMap<String, BigDecimal> totalAmountMap;
	
	private String type;

	/**
	 * @return the colHeads
	 */
	public HashMap<String, Integer> getColHeads() {
		return colHeads;
	}

	/**
	 * @param colHeads the colHeads to set
	 */
	public void setColHeads(HashMap<String, Integer> colHeads) {
		this.colHeads = colHeads;
	}

	/**
	 * @return the details
	 */
	public List<PaySalaryDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<PaySalaryDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the totalBasicsSalary
	 */
	public BigDecimal getTotalBasicsSalary() {
		return totalBasicsSalary;
	}

	/**
	 * @param totalBasicsSalary the totalBasicsSalary to set
	 */
	public void setTotalBasicsSalary(BigDecimal totalBasicsSalary) {
		this.totalBasicsSalary = totalBasicsSalary;
	}

	/**
	 * @return the totalTaxAmount
	 */
	public double getTotalTaxAmount() {
		return totalTaxAmount;
	}

	/**
	 * @param totalTaxAmount the totalTaxAmount to set
	 */
	public void setTotalTaxAmount(double totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

	/**
	 * @return the totalAllowances
	 */
	public List<BigDecimal> getTotalAllowances() {
		return totalAllowances;
	}

	/**
	 * @param totalAllowances the totalAllowances to set
	 */
	public void setTotalAllowances(List<BigDecimal> totalAllowances) {
		this.totalAllowances = totalAllowances;
	}

	/**
	 * @return the totalHra
	 */
	public BigDecimal getTotalHra() {
		return totalHra;
	}

	/**
	 * @param totalHra the totalHra to set
	 */
	public void setTotalHra(BigDecimal totalHra) {
		this.totalHra = totalHra;
	}

	/**
	 * @return the totalDa
	 */
	public BigDecimal getTotalDa() {
		return totalDa;
	}

	/**
	 * @param totalDa the totalDa to set
	 */
	public void setTotalDa(BigDecimal totalDa) {
		this.totalDa = totalDa;
	}

	/**
	 * @return the grandSalary
	 */
	public BigDecimal getGrandSalary() {
		return grandSalary;
	}

	/**
	 * @param grandSalary the grandSalary to set
	 */
	public void setGrandSalary(BigDecimal grandSalary) {
		this.grandSalary = grandSalary;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public HashMap<String, BigDecimal> getTotalAmountMap() {
		return totalAmountMap;
	}

	public void setTotalAmountMap(HashMap<String, BigDecimal> totalAmountMap) {
		this.totalAmountMap = totalAmountMap;
	}

	
}
