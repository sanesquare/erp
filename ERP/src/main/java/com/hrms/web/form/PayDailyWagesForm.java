package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.PayDailyWageDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayDailyWagesForm {

	private List<PayDailyWageDetailsVo> details;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<PayDailyWageDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<PayDailyWageDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
