package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.DetailedEmployeeReportVo;
import com.hrms.web.vo.HrEmpReportViewVo;
import com.hrms.web.vo.HrEmpReportVo;

public class HREmpReportForm {
	
	 private List<HrEmpReportViewVo> empReportVos;
	 
	 public List<DetailedEmployeeReportVo> detailedVos;

	 private String type;
	 
	/**
	 * @return the empReportVos
	 */
	public List<HrEmpReportViewVo> getEmpReportVos() {
		return empReportVos;
	}

	/**
	 * @param empReportVos the empReportVos to set
	 */
	public void setEmpReportVos(List<HrEmpReportViewVo> empReportVos) {
		this.empReportVos = empReportVos;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public List<DetailedEmployeeReportVo> getDetailedVos() {
		return detailedVos;
	}

	public void setDetailedVos(List<DetailedEmployeeReportVo> detailedVos) {
		this.detailedVos = detailedVos;
	}


	
	 
}
