package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.TimeEmpLeaveDetailsVo;
import com.hrms.web.vo.TimeEmpLeaveSummaryVo;

/**
 * 
 * @author Vinutha
 * @since 29-April-2015
 *
 */
public class TimeEmpLeaveSummaryForm {

	private List<TimeEmpLeaveDetailsVo> details;
	
	private List<TimeEmpLeaveSummaryVo> summary;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<TimeEmpLeaveDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<TimeEmpLeaveDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the summary
	 */
	public List<TimeEmpLeaveSummaryVo> getSummary() {
		return summary;
	}

	/**
	 * @param summary the summary to set
	 */
	public void setSummary(List<TimeEmpLeaveSummaryVo> summary) {
		this.summary = summary;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
