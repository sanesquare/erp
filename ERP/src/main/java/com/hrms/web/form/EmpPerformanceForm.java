package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.EmpPerformanceEvaluationDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 26-May-2015
 *
 */
public class EmpPerformanceForm {

	private List<EmpPerformanceEvaluationDetailsVo> details;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<EmpPerformanceEvaluationDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<EmpPerformanceEvaluationDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
