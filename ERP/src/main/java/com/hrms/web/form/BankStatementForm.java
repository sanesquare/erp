package com.hrms.web.form;

import java.util.ArrayList;
import java.util.List;
import com.hrms.web.vo.BankStatementDetails;
/**
 * 
 * @author Vinutha
 * @since 10-September-2015
 *
 */
public class BankStatementForm {

	private List<BankStatementDetails> details = new ArrayList<BankStatementDetails>();
	
	private String type;

	public List<BankStatementDetails> getDetails() {
		return details;
	}

	public void setDetails(List<BankStatementDetails> details) {
		this.details = details;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
