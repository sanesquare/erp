package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.PayHrlyWagesDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayHourlyWagesForm {

	private List<PayHrlyWagesDetailsVo> details;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<PayHrlyWagesDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<PayHrlyWagesDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
