package com.hrms.web.form;

import java.math.BigDecimal;
import java.util.List;

import com.hrms.web.vo.PayOtDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayOTForm {

	private List<PayOtDetailsVo> details ;
	
	private BigDecimal totalAmount;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<PayOtDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<PayOtDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	
}
