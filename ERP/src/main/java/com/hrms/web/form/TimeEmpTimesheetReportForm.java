package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.TimeEmpTimesheetReportDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 28-April-2015
 *
 */
public class TimeEmpTimesheetReportForm {

	private List<TimeEmpTimesheetReportDetailsVo> details;

	private String empName;

	private String designation;

	private String department;

	private String userName;
	
	private double hoursTotal;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<TimeEmpTimesheetReportDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details
	 *            the details to set
	 */
	public void setDetails(List<TimeEmpTimesheetReportDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the hoursTotal
	 */
	public double getHoursTotal() {
		return hoursTotal;
	}

	/**
	 * @param hoursTotal the hoursTotal to set
	 */
	public void setHoursTotal(double hoursTotal) {
		this.hoursTotal = hoursTotal;
	}

	
}
