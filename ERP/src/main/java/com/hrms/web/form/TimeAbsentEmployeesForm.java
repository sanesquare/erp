package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.TimeAbsentEmployeesDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 29-April-2015
 *
 */
public class TimeAbsentEmployeesForm {

	private List<TimeAbsentEmployeesDetailsVo> details ;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<TimeAbsentEmployeesDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<TimeAbsentEmployeesDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
