package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.PayCommisionsDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayCommisionForm {

	private List<PayCommisionsDetailsVo> details;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<PayCommisionsDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<PayCommisionsDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
