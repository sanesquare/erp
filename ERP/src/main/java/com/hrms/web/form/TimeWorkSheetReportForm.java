package com.hrms.web.form;

import java.util.List;

import com.hrms.web.vo.TimeWorkSheetDetailsVo;

/**
 * 
 * @author Vinutha
 * @since 28-April-2015
 *
 */
public class TimeWorkSheetReportForm {

	private List<TimeWorkSheetDetailsVo> details ;
	
	private String type;

	/**
	 * @return the details
	 */
	public List<TimeWorkSheetDetailsVo> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<TimeWorkSheetDetailsVo> details) {
		this.details = details;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
}
