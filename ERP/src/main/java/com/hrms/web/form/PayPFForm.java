package com.hrms.web.form;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.hrms.web.vo.PayPFDetails;

/**
 * 
 * @author Vinutha
 * @since 22-April-2015
 *
 */
public class PayPFForm {

	private List<PayPFDetails> details = new ArrayList<PayPFDetails>();
	
	private BigDecimal totalEmpShare = new BigDecimal(0.00);;
	
	private BigDecimal totalOrgShare = new BigDecimal(0.00);;
	
	private String type;
	
	private BigDecimal totalTotal = new BigDecimal(0.00);

	/**
	 * @return the details
	 */
	public List<PayPFDetails> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<PayPFDetails> details) {
		this.details = details;
	}

	/**
	 * @return the totalEmpShare
	 */
	public BigDecimal getTotalEmpShare() {
		return totalEmpShare;
	}

	/**
	 * @param totalEmpShare the totalEmpShare to set
	 */
	public void setTotalEmpShare(BigDecimal totalEmpShare) {
		this.totalEmpShare = totalEmpShare;
	}

	/**
	 * @return the totalOrgShare
	 */
	public BigDecimal getTotalOrgShare() {
		return totalOrgShare;
	}

	/**
	 * @param totalOrgShare the totalOrgShare to set
	 */
	public void setTotalOrgShare(BigDecimal totalOrgShare) {
		this.totalOrgShare = totalOrgShare;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getTotalTotal() {
		return totalTotal;
	}

	public void setTotalTotal(BigDecimal totalTotal) {
		this.totalTotal = totalTotal;
	}

	
	
}
