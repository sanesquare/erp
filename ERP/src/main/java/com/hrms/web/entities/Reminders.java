package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Entity
@Table(name = "reminders")
public class Reminders implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2231439522798441664L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "title")
	private String title;

	@Column(name = "reminder_on")
	private Date reminderOn;

	@Column(name = "reminder_time", length = 35)
	private String reminderTime;

	@Column(name = "time_meridian")
	private String timeMeridian;

	@Column(name = "message", length = 1000)
	private String message;

	@ManyToOne(fetch = FetchType.LAZY)
	private ReminderStatus reminderStatus;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "employee_reminders", joinColumns = { @JoinColumn(name = "reminder_id") }, inverseJoinColumns = { @JoinColumn(name = "employee_id") })
	private Set<Employee> employee;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 */
	public Date getReminderOn() {
		return reminderOn;
	}

	/**
	 * 
	 * @param reminderOn
	 */
	public void setReminderOn(Date reminderOn) {
		this.reminderOn = reminderOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * 
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return
	 */
	public ReminderStatus getReminderStatus() {
		return reminderStatus;
	}

	/**
	 * 
	 * @param reminderStatus
	 */
	public void setReminderStatus(ReminderStatus reminderStatus) {
		this.reminderStatus = reminderStatus;
	}

	/**
	 * 
	 * @return
	 */
	public String getReminderTime() {
		return reminderTime;
	}

	/**
	 * 
	 * @param reminderTime
	 */
	public void setReminderTime(String reminderTime) {
		this.reminderTime = reminderTime;
	}

	/**
	 * 
	 * @return
	 */
	public String getTimeMeridian() {
		return timeMeridian;
	}

	/**
	 * 
	 * @param timeMeridian
	 */
	public void setTimeMeridian(String timeMeridian) {
		this.timeMeridian = timeMeridian;
	}

	public Set<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(Set<Employee> employee) {
		this.employee = employee;
	}


}
