package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Entity
@Table(name = "tax_rules")
public class TaxRule implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7220443283427535602L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "salary_from")
	private BigDecimal salaryFrom;

	@Column(name = "salary_to")
	private BigDecimal salaryTo;

	@Column(name = "tax_percentage")
	private BigDecimal taxPercentage;

	@Column(name = "exempted_amount")
	private BigDecimal exemptedAmount;

	@Column(name = "additional_amount")
	private BigDecimal additionalAmount;

	@Column(name = "row_tax")
	private BigDecimal rowTax;

	@Column(name = "individual")
	private String individual;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * 
	 * @return
	 */
	public BigDecimal getRowTax() {
		return rowTax;
	}

	/**
	 * 
	 * @param rowTax
	 */
	public void setRowTax(BigDecimal rowTax) {
		this.rowTax = rowTax;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getSalaryFrom() {
		return salaryFrom;
	}

	/**
	 * 
	 * @param salaryFrom
	 */
	public void setSalaryFrom(BigDecimal salaryFrom) {
		this.salaryFrom = salaryFrom;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getSalaryTo() {
		return salaryTo;
	}

	/**
	 * 
	 * @param salaryTo
	 */
	public void setSalaryTo(BigDecimal salaryTo) {
		this.salaryTo = salaryTo;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getTaxPercentage() {
		return taxPercentage;
	}

	/**
	 * 
	 * @param taxPercentage
	 */
	public void setTaxPercentage(BigDecimal taxPercentage) {
		this.taxPercentage = taxPercentage;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getExemptedAmount() {
		return exemptedAmount;
	}

	/**
	 * 
	 * @param exemptedAmount
	 */
	public void setExemptedAmount(BigDecimal exemptedAmount) {
		this.exemptedAmount = exemptedAmount;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getAdditionalAmount() {
		return additionalAmount;
	}

	/**
	 * 
	 * @param additionalAmount
	 */
	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}

	/**
	 * 
	 * @return
	 */
	public String getIndividual() {
		return individual;
	}

	/**
	 * 
	 * @param individual
	 */
	public void setIndividual(String individual) {
		this.individual = individual;
	}

}
