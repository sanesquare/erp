package com.hrms.web.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Entity
@Table(name = "currency")
public class Currency implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7455082759812146892L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private BaseCurrency baseCurrency;

	@Column(name = "decimal_places")
	private Integer decimaplPlaces;

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public BaseCurrency getBaseCurrency() {
		return baseCurrency;
	}

	/**
	 * 
	 * @param baseCurrency
	 */
	public void setBaseCurrency(BaseCurrency baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getDecimaplPlaces() {
		return decimaplPlaces;
	}

	/**
	 * 
	 * @param decimaplPlaces
	 */
	public void setDecimaplPlaces(Integer decimaplPlaces) {
		this.decimaplPlaces = decimaplPlaces;
	}

}
