package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;


@Entity
@Table(name="promotion")
public class Promotion implements AuditEntity, Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	Employee employee;
	
	@ManyToOne
	EmployeeDesignation promotiontoTo;
	
	@Column(name="promotion_date")
	Date promotionDate;
	
	@Column(name="promotion_description")
	String promotionDescription;
	
	@Column(name="description")
	String description;
	
	@Column(name="note")
	String note;
	
	@OneToOne
	PromotionStatus promotionStatus;
	
	@ManyToMany
	Set<Employee> forwardTo;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "promotion", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<PromotionDocuments> promotionDocuments = new HashSet<PromotionDocuments>();

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	

	/**
	 * @return the promotionDate
	 */
	public Date getPromotionDate() {
		return promotionDate;
	}

	/**
	 * @param promotionDate the promotionDate to set
	 */
	public void setPromotionDate(Date promotionDate) {
		this.promotionDate = promotionDate;
	}

	/**
	 * @return the promotionDescription
	 */
	public String getPromotionDescription() {
		return promotionDescription;
	}

	/**
	 * @param promotionDescription the promotionDescription to set
	 */
	public void setPromotionDescription(String promotionDescription) {
		this.promotionDescription = promotionDescription;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the promotionStatus
	 */
	public PromotionStatus getPromotionStatus() {
		return promotionStatus;
	}

	/**
	 * @param promotionStatus the promotionStatus to set
	 */
	public void setPromotionStatus(PromotionStatus promotionStatus) {
		this.promotionStatus = promotionStatus;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public EmployeeDesignation getPromotiontoTo() {
		return promotiontoTo;
	}

	public void setPromotiontoTo(EmployeeDesignation promotiontoTo) {
		this.promotiontoTo = promotiontoTo;
	}

	public Set<Employee> getForwardTo() {
		return forwardTo;
	}

	public void setForwardTo(Set<Employee> forwardTo) {
		this.forwardTo = forwardTo;
	}

	public Set<PromotionDocuments> getPromotionDocuments() {
		return promotionDocuments;
	}

	public void setPromotionDocuments(Set<PromotionDocuments> promotionDocuments) {
		this.promotionDocuments = promotionDocuments;
	}

	
	

}
