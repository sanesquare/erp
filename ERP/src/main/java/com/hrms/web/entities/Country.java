package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.crm.web.entities.Lead;
import com.hrms.web.interfaces.AuditEntity;
import com.purchase.web.entities.Vendor;
import com.sales.web.entities.Customer;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
@Entity
@Table(name = "country")
public class Country implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4894826419551754112L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	@OneToMany(mappedBy = "country")
	private Set<JobCandidates> jobCandidates=new HashSet<JobCandidates>();

	@OneToOne(mappedBy = "country")
	private OrganisationContactPerson contactPerson;

	@OneToMany(mappedBy = "country")
	private Set<BranchAddress> address=new HashSet<BranchAddress>();
	
	@OneToMany(mappedBy = "country")
	private Set<CandidatesAddress> candidateAddress= new HashSet<CandidatesAddress>();
	
	@OneToMany(mappedBy="country")
	private Set<Vendor> vendors;
	
	@OneToMany(mappedBy="country")
	private Set<Customer> customers;
	
	@OneToMany(mappedBy="country")
	private Set<Lead> leads;
	
	public OrganisationContactPerson getContactPerson() {
		return contactPerson;
	}

	/**
	 * 
	 * @param contactPerson
	 */
	public void setContactPerson(OrganisationContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the jobCandidates
	 */
	public Set<JobCandidates> getJobCandidates() {
		return jobCandidates;
	}

	/**
	 * @param jobCandidates the jobCandidates to set
	 */
	public void setJobCandidates(Set<JobCandidates> jobCandidates) {
		this.jobCandidates = jobCandidates;
	}

	/**
	 * @return the address
	 */
	public Set<BranchAddress> getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Set<BranchAddress> address) {
		this.address = address;
	}

	/**
	 * @return the candidateAddress
	 */
	public Set<CandidatesAddress> getCandidateAddress() {
		return candidateAddress;
	}

	/**
	 * @param candidateAddress the candidateAddress to set
	 */
	public void setCandidateAddress(Set<CandidatesAddress> candidateAddress) {
		this.candidateAddress = candidateAddress;
	}

	public Set<Vendor> getVendors() {
		return vendors;
	}

	public void setVendors(Set<Vendor> vendors) {
		this.vendors = vendors;
	}

	public Set<Customer> getCustomers() {
		return customers;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<Lead> getLeads() {
		return leads;
	}

	public void setLeads(Set<Lead> leads) {
		this.leads = leads;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
