package com.hrms.web.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author Shamsheer
 * @since 12-March-2015
 *
 */
@Entity
@Table(name="meeting_employee")
@AssociationOverrides({
	@AssociationOverride(name="meetingEmployeeId.meetings",joinColumns=@JoinColumn(name="meeting_id")),
	@AssociationOverride(name="meetingEmployeeId.employee",joinColumns=@JoinColumn(name="employee_id")),
	@AssociationOverride(name="meetingEmployeeId.department",joinColumns=@JoinColumn(name="department_id")),
	@AssociationOverride(name="meetingEmployeeId.branch",joinColumns=@JoinColumn(name="branch_id"))
})
public class MeetingEmployee implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MeetingEmployeeId meetingEmployeeId=new MeetingEmployeeId();
	
	@Transient
	public Meetings getMeetings(){
		return this.meetingEmployeeId.getMeetings();
	}

	public void setMeetings(Meetings meetings){
		 this.meetingEmployeeId.setMeetings(meetings);
	}
	
	@Transient
	public Department getDeaprtment(){
		return  this.meetingEmployeeId.getDepartment();
	}
	
	public void setDepartment(Department department){
		 this.meetingEmployeeId.setDepartment(department);
	}
	
	@Transient
	public Branch getBranch(){
		return  this.meetingEmployeeId.getBranch();
	}
	
	public void setBranch(Branch branch){
		 this.meetingEmployeeId.setBranch(branch);
	}
	
	@Transient
	public Employee getEmployee(){
		return  this.meetingEmployeeId.getEmployee();
	}
	
	public void setEmployee(Employee employee){
		 this.meetingEmployeeId.setEmployee(employee);
	}
	/**
	 * @return the meetingEmployeeId
	 */
	public MeetingEmployeeId getMeetingEmployeeId() {
		return meetingEmployeeId;
	}

	/**
	 * @param meetingEmployeeId the meetingEmployeeId to set
	 */
	public void setMeetingEmployeeId(MeetingEmployeeId meetingEmployeeId) {
		this.meetingEmployeeId = meetingEmployeeId;
	}
	

}
