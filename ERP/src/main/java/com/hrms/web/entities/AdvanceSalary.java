package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Entity
@Table(name = "advance_salary")
public class AdvanceSalary implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1587150915492287956L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	private Employee employee;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "advance_salary_forwards", joinColumns = { @JoinColumn(name = "advance_salary_id") }, inverseJoinColumns = { @JoinColumn(name = "employee_id") })
	private Set<Employee> forwader;

	@Column(name = "title")
	private String title;
	
	@Column(name="no_of_installments")
	private Integer noOfInstallments;
	
	@Column(name="pending_installments")
	private Integer pendingInstallments;

	@Column(name = "advance_amount")
	private BigDecimal advanceAmount;

	@Column(name = "amount_requested_on")
	private Date amountRequestedOn;

	@Column(name = "description")
	private String description;

	@Column(name = "notes")
	private String notes;

	@ManyToOne(fetch = FetchType.LAZY)
	private AdvanceSalaryStatus status;

	@Column(name = "status_description")
	private String statusDescription;
	
	@Column(name = "repayment_amount")
	private BigDecimal repaymentAmount;
	
	@Column(name = "balance_amount")
	private BigDecimal balanceAmount;

	@Column(name = "repayment_start_date")
	private Date repaymentStartDate;


	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public Set<Employee> getForwader() {
		return forwader;
	}

	/**
	 * 
	 * @param advanceSalaryForwader
	 */
	public void setForwader(Set<Employee> forwader) {
		this.forwader = forwader;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getAdvanceAmount() {
		return advanceAmount;
	}

	/**
	 * 
	 * @param advanceAmount
	 */
	public void setAdvanceAmount(BigDecimal advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	/**
	 * 
	 * @return
	 */
	public Date getAmountRequestedOn() {
		return amountRequestedOn;
	}

	/**
	 * 
	 * @param amountRequestedOn
	 */
	public void setAmountRequestedOn(Date amountRequestedOn) {
		this.amountRequestedOn = amountRequestedOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public AdvanceSalaryStatus getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(AdvanceSalaryStatus status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * 
	 * @param statusDescription
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getNoOfInstallments() {
		return noOfInstallments;
	}

	public void setNoOfInstallments(Integer noOfInstallments) {
		this.noOfInstallments = noOfInstallments;
	}

	public Integer getPendingInstallments() {
		return pendingInstallments;
	}

	public void setPendingInstallments(Integer pendingInstallments) {
		this.pendingInstallments = pendingInstallments;
	}

	public BigDecimal getRepaymentAmount() {
		return repaymentAmount;
	}

	public void setRepaymentAmount(BigDecimal repaymentAmount) {
		this.repaymentAmount = repaymentAmount;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public Date getRepaymentStartDate() {
		return repaymentStartDate;
	}

	public void setRepaymentStartDate(Date repaymentStartDate) {
		this.repaymentStartDate = repaymentStartDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
