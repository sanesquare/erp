package com.hrms.web.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * 
 * @author Shamsheer
 * @since 12-March-2015
 *
 */
@Embeddable
public class MeetingEmployeeId implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Meetings meetings;
	
	@ManyToOne
	private Employee employee;
	
	@ManyToOne
	private Department department;
	
	@ManyToOne
	private Branch branch;

	/**
	 * @return the meetings
	 */
	public Meetings getMeetings() {
		return meetings;
	}

	/**
	 * @param meetings the meetings to set
	 */
	public void setMeetings(Meetings meetings) {
		this.meetings = meetings;
	}

	/**
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
}
