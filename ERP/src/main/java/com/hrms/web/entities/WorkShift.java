package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 31-March-2015
 *
 */
@Entity
@Table(name = "work_shifts")
public class WorkShift implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -667338822029022400L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "regular_work_from")
	private String regularWorkFrom;

	@Column(name = "regular_work_to")
	private String regularWorkTo;

	@Column(name = "refreshment_break_from")
	private String refreshmentBreakFrom;

	@Column(name = "refreshment_break_to")
	private String refreshmentBreakTo;

	@Column(name = "additional_break_from")
	private String additionalBreakFrom;

	@Column(name = "additional_break_to")
	private String additionalBreakTo;

	@OneToOne(fetch = FetchType.LAZY)
	private ShiftDay shiftDay;

	@ManyToOne(fetch = FetchType.LAZY)
	private Employee employee;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * 
	 * @return
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public ShiftDay getShiftDay() {
		return shiftDay;
	}

	/**
	 * 
	 * @param shiftDay
	 */
	public void setShiftDay(ShiftDay shiftDay) {
		this.shiftDay = shiftDay;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getRegularWorkFrom() {
		return regularWorkFrom;
	}

	/**
	 * 
	 * @param regularWorkFrom
	 */
	public void setRegularWorkFrom(String regularWorkFrom) {
		this.regularWorkFrom = regularWorkFrom;
	}

	/**
	 * 
	 * @return
	 */
	public String getRegularWorkTo() {
		return regularWorkTo;
	}

	/**
	 * 
	 * @param regularWorkTo
	 */
	public void setRegularWorkTo(String regularWorkTo) {
		this.regularWorkTo = regularWorkTo;
	}

	/**
	 * 
	 * @return
	 */
	public String getRefreshmentBreakFrom() {
		return refreshmentBreakFrom;
	}

	/**
	 * 
	 * @param refreshmentBreakFrom
	 */
	public void setRefreshmentBreakFrom(String refreshmentBreakFrom) {
		this.refreshmentBreakFrom = refreshmentBreakFrom;
	}

	/**
	 * 
	 * @return
	 */
	public String getRefreshmentBreakTo() {
		return refreshmentBreakTo;
	}

	/**
	 * 
	 * @param refreshmentBreakTo
	 */
	public void setRefreshmentBreakTo(String refreshmentBreakTo) {
		this.refreshmentBreakTo = refreshmentBreakTo;
	}

	/**
	 * 
	 * @return
	 */
	public String getAdditionalBreakFrom() {
		return additionalBreakFrom;
	}

	/**
	 * 
	 * @param additionalBreakFrom
	 */
	public void setAdditionalBreakFrom(String additionalBreakFrom) {
		this.additionalBreakFrom = additionalBreakFrom;
	}

	/**
	 * 
	 * @return
	 */
	public String getAdditionalBreakTo() {
		return additionalBreakTo;
	}

	/**
	 * 
	 * @param additionalBreakTo
	 */
	public void setAdditionalBreakTo(String additionalBreakTo) {
		this.additionalBreakTo = additionalBreakTo;
	}

}
