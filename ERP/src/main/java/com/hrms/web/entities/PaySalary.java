package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.accounts.web.entities.Journal;
import com.accounts.web.entities.Payment;
import com.erp.web.entities.Company;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 08-June-2015
 */
@Entity
@Table(name = "pay_salary")
public class PaySalary implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7597582611563466435L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@OneToMany(cascade = CascadeType.ALL, mappedBy = "paySalary", orphanRemoval = true , fetch = FetchType.EAGER)
	private Set<PaySalaryEmployees> paySalaryMonthly = new HashSet<PaySalaryEmployees>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "paySalary", orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<PaySalaryHourlyWage> paySalaryHourlyWages = new HashSet<PaySalaryHourlyWage>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "paySalary", orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<PaySalaryDailyWages> paySalaryDailyWages = new HashSet<PaySalaryDailyWages>();

	@Column(name = "salary_date")
	private Date date;

	@ManyToMany
	private Set<Branch> branches = new HashSet<Branch>();

	@ManyToMany
	private Set<Department> departments = new HashSet<Department>();

	@ManyToOne
	private SalaryType salaryType;
	
	@ManyToOne
	private SalaryApprovalStatus status;

	@ManyToOne
	private Company company;
	
	@Column(name="amount")
	private BigDecimal amount;
	
	@OneToOne(mappedBy="paySalary",cascade=CascadeType.ALL,orphanRemoval=true)
	private Payment payment;
	
	@OneToOne(mappedBy="paySalary",cascade=CascadeType.ALL,orphanRemoval=true)
	private Journal  journal;
	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Set<Branch> getBranches() {
		return branches;
	}

	public void setBranches(Set<Branch> branches) {
		this.branches = branches;
	}

	public Set<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(Set<Department> departments) {
		this.departments = departments;
	}

	public SalaryType getSalaryType() {
		return salaryType;
	}

	public void setSalaryType(SalaryType salaryType) {
		this.salaryType = salaryType;
	}

	public Set<PaySalaryEmployees> getPaySalaryMonthly() {
		return paySalaryMonthly;
	}

	public void setPaySalaryMonthly(Set<PaySalaryEmployees> paySalaryMonthly) {
		this.paySalaryMonthly = paySalaryMonthly;
	}

	public Set<PaySalaryHourlyWage> getPaySalaryHourlyWages() {
		return paySalaryHourlyWages;
	}

	public void setPaySalaryHourlyWages(Set<PaySalaryHourlyWage> paySalaryHourlyWages) {
		this.paySalaryHourlyWages = paySalaryHourlyWages;
	}

	public Set<PaySalaryDailyWages> getPaySalaryDailyWages() {
		return paySalaryDailyWages;
	}

	public void setPaySalaryDailyWages(Set<PaySalaryDailyWages> paySalaryDailyWages) {
		this.paySalaryDailyWages = paySalaryDailyWages;
	}

	public SalaryApprovalStatus getStatus() {
		return status;
	}

	public void setStatus(SalaryApprovalStatus status) {
		this.status = status;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
