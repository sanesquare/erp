package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Entity
@Table(name = "overtimes")
public class Overtime implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -8818293298607591068L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	private Employee employee;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "overtime_forwards", joinColumns = { @JoinColumn(name = "overtime_id") }, inverseJoinColumns = { @JoinColumn(name = "forwader_id") })
	private Set<Employee> forwader;

	@Column(name = "title")
	private String title;

	@Column(name = "overtime_amount")
	private BigDecimal overtimeAmount;

	@Column(name = "amount_requested_on")
	private Date amountRequestedOn;

	@Column(name = "time_in")
	private Date timeIn;

	@Column(name = "time_out")
	private Date timeOut;

	@Column(name = "description")
	private String description;

	@Column(name = "notes")
	private String notes;

	@ManyToOne(fetch = FetchType.LAZY)
	private OvertimeStatus status;

	@Column(name = "status_description")
	private String statusDescription;


	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public Set<Employee> getForwader() {
		return forwader;
	}

	/**
	 * 
	 * @param forwader
	 */
	public void setForwader(Set<Employee> forwader) {
		this.forwader = forwader;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getOvertimeAmount() {
		return overtimeAmount;
	}

	/**
	 * 
	 * @param overtimeAmount
	 */
	public void setOvertimeAmount(BigDecimal overtimeAmount) {
		this.overtimeAmount = overtimeAmount;
	}

	/**
	 * 
	 * @return
	 */
	public Date getAmountRequestedOn() {
		return amountRequestedOn;
	}

	/**
	 * 
	 * @param amountRequestedOn
	 */
	public void setAmountRequestedOn(Date amountRequestedOn) {
		this.amountRequestedOn = amountRequestedOn;
	}

	/**
	 * 
	 * @return
	 */
	public Date getTimeIn() {
		return timeIn;
	}

	/**
	 * 
	 * @param timeIn
	 */
	public void setTimeIn(Date timeIn) {
		this.timeIn = timeIn;
	}

	/**
	 * 
	 * @return
	 */
	public Date getTimeOut() {
		return timeOut;
	}

	/**
	 * 
	 * @param timeOut
	 */
	public void setTimeOut(Date timeOut) {
		this.timeOut = timeOut;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public OvertimeStatus getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(OvertimeStatus status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * 
	 * @param statusDescription
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
