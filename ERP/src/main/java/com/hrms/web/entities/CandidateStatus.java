package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Vinutha
 * @since 18-March-2015
 *
 */
@Entity
@Table(name = "candidate_status")
public class CandidateStatus implements AuditEntity , Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2026082481740982226L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "status")
	private String status;
	
	@OneToMany(mappedBy = "status" , fetch = FetchType.EAGER)
	private Set<JobCandidates> jobCandidates = new HashSet<JobCandidates>();
	
	@ManyToMany(mappedBy="status")
	private Set<JobInterview> interviews = new HashSet<JobInterview>();
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the jobCandidates
	 */
	public Set<JobCandidates> getJobCandidates() {
		return jobCandidates;
	}

	/**
	 * @param jobCandidates the jobCandidates to set
	 */
	public void setJobCandidates(Set<JobCandidates> jobCandidates) {
		this.jobCandidates = jobCandidates;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the interviews
	 */
	public Set<JobInterview> getInterviews() {
		return interviews;
	}

	/**
	 * @param interviews the interviews to set
	 */
	public void setInterviews(Set<JobInterview> interviews) {
		this.interviews = interviews;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
}
