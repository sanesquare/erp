package com.hrms.web.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * 
 * @author Shamsheer
 * @since 28-April-2015
 */
@Embeddable
public class SalaryExtraPaySlipItemsId implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6663882384706205375L;

	@ManyToOne 
	private SalaryMonthly salaryMonthly;
	
	@ManyToOne
	private ExtraPaySlipItem extraPaySlipItem;

	public SalaryMonthly getSalaryMonthly() {
		return salaryMonthly;
	}

	public void setSalaryMonthly(SalaryMonthly salaryMonthly) {
		this.salaryMonthly = salaryMonthly;
	}

	public ExtraPaySlipItem getExtraPaySlipItem() {
		return extraPaySlipItem;
	}

	public void setExtraPaySlipItem(ExtraPaySlipItem extraPaySlipItem) {
		this.extraPaySlipItem = extraPaySlipItem;
	}
}
