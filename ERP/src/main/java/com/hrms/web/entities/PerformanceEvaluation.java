package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 24-March-2015
 *
 */
@Entity
@Table(name = "performance_evaluations")
public class PerformanceEvaluation implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 282256089916775523L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "title")
	private String title;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "performance_evaluation_branches", joinColumns = { @JoinColumn(name = "performance_evaluation_id") }, inverseJoinColumns = { @JoinColumn(name = "branch_id") })
	private Set<Branch> branch;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "notes", length = 1000)
	private String notes;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "evaluation")
	private Set<PerformanceEvaluationDocument> documents;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "evaluation")
	private Set<PerformanceEvaluationQuestion> questions;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * 
	 * @return
	 */
	public Set<Branch> getBranch() {
		return branch;
	}

	/**
	 * 
	 * @param branch
	 */
	public void setBranch(Set<Branch> branch) {
		this.branch = branch;
	}

	/**
	 * 
	 * @return
	 */
	public Set<PerformanceEvaluationQuestion> getQuestions() {
		return questions;
	}

	/**
	 * 
	 * @param questions
	 */
	public void setQuestions(Set<PerformanceEvaluationQuestion> questions) {
		this.questions = questions;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 */
	public Set<PerformanceEvaluationDocument> getDocuments() {
		return documents;
	}

	/**
	 * 
	 * @param documents
	 */
	public void setDocuments(Set<PerformanceEvaluationDocument> documents) {
		this.documents = documents;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * 
	 * @param startDate
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * 
	 * @return
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * 
	 * @param endDate
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

}
