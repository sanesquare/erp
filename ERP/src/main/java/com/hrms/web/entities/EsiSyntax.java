package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 5-May-2015
 *
 */
@Entity
@Table(name = "esi_syntax")
public class EsiSyntax implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4926862082613753667L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "employee_syntax")
	private String employeeSyntax;

	@Column(name = "employer_syntax")
	private String employerSyntax;

	@Column(name = "esi_range")
	private String esiRange;

	@Column(name = "amount")
	private BigDecimal amount;


	/**
	 * 
	 * @return
	 */
	public String getEsiRange() {
		return esiRange;
	}

	/**
	 * 
	 * @param esiRange
	 */
	public void setEsiRange(String esiRange) {
		this.esiRange = esiRange;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeSyntax() {
		return employeeSyntax;
	}

	/**
	 * 
	 * @param employeeSyntax
	 */
	public void setEmployeeSyntax(String employeeSyntax) {
		this.employeeSyntax = employeeSyntax;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployerSyntax() {
		return employerSyntax;
	}

	/**
	 * 
	 * @param employerSyntax
	 */
	public void setEmployerSyntax(String employerSyntax) {
		this.employerSyntax = employerSyntax;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
