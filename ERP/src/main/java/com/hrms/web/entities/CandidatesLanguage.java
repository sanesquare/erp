package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Vinutha
 * @since 18-March-2015
 *
 */
@Entity
@Table(name = "Candidates_language")
public class CandidatesLanguage implements AuditEntity , Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5296409725926331910L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	private Language language;
	
	@Column(name = "speakingLevel")
	private String speakingLevel;
	
	@Column(name = "readingLevel")
	private String readingLevel;
	
	@Column(name = "writingLevel")
	private String writingLevel;
	
	@ManyToOne
	private JobCandidates jobCandidates;
	

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the language
	 */
	public Language getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(Language language) {
		this.language = language;
	}

	/**
	 * @return the speakingLevel
	 */
	public String getSpeakingLevel() {
		return speakingLevel;
	}

	/**
	 * @param speakingLevel the speakingLevel to set
	 */
	public void setSpeakingLevel(String speakingLevel) {
		this.speakingLevel = speakingLevel;
	}

	/**
	 * @return the readingLevel
	 */
	public String getReadingLevel() {
		return readingLevel;
	}

	/**
	 * @param readingLevel the readingLevel to set
	 */
	public void setReadingLevel(String readingLevel) {
		this.readingLevel = readingLevel;
	}

	/**
	 * @return the writingLevel
	 */
	public String getWritingLevel() {
		return writingLevel;
	}

	/**
	 * @param writingLevel the writingLevel to set
	 */
	public void setWritingLevel(String writingLevel) {
		this.writingLevel = writingLevel;
	}

	/**
	 * @return the jobCandidates
	 */
	public JobCandidates getJobCandidates() {
		return jobCandidates;
	}

	/**
	 * @param jobCandidates the jobCandidates to set
	 */
	public void setJobCandidates(JobCandidates jobCandidates) {
		this.jobCandidates = jobCandidates;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
