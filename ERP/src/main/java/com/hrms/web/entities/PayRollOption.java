package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Entity
@Table(name = "payrolloptions")
public class PayRollOption implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3233546509069907770L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "auto_aprove_payroll")
	private Boolean autoAprovePayroll;

	@Column(name = "auto_email_payslip")
	private Boolean autoEmailPayslip;

	@Column(name = "per_day_calculation")
	private Boolean perDayCalculation;

	@Column(name = "per_day_calculation_type")
	private String perDayCalculationType;

	@Column(name = "per_day_calculation_days")
	private Integer perDayCalculationDays;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "payRollOption", cascade = CascadeType.ALL)
	private Set<Employee> approvers;


	/**
	 * 
	 * @return
	 */
	public Set<Employee> getApprovers() {
		return approvers;
	}

	/**
	 * 
	 * @param approvers
	 */
	public void setApprovers(Set<Employee> approvers) {
		this.approvers = approvers;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public Boolean getAutoAprovePayroll() {
		return autoAprovePayroll;
	}

	/**
	 * 
	 * @param autoAprovePayroll
	 */
	public void setAutoAprovePayroll(Boolean autoAprovePayroll) {
		this.autoAprovePayroll = autoAprovePayroll;
	}

	/**
	 * 
	 * @return
	 */
	public Boolean getAutoEmailPayslip() {
		return autoEmailPayslip;
	}

	/**
	 * 
	 * @param autoEmailPayslip
	 */
	public void setAutoEmailPayslip(Boolean autoEmailPayslip) {
		this.autoEmailPayslip = autoEmailPayslip;
	}

	/**
	 * 
	 * @return
	 */
	public Boolean getPerDayCalculation() {
		return perDayCalculation;
	}

	/**
	 * 
	 * @param perDayCalculation
	 */
	public void setPerDayCalculation(Boolean perDayCalculation) {
		this.perDayCalculation = perDayCalculation;
	}

	/**
	 * 
	 * @return
	 */
	public String getPerDayCalculationType() {
		return perDayCalculationType;
	}

	/**
	 * 
	 * @param perDayCalculationType
	 */
	public void setPerDayCalculationType(String perDayCalculationType) {
		this.perDayCalculationType = perDayCalculationType;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getPerDayCalculationDays() {
		return perDayCalculationDays;
	}

	/**
	 * 
	 * @param perDayCalculationDays
	 */
	public void setPerDayCalculationDays(Integer perDayCalculationDays) {
		this.perDayCalculationDays = perDayCalculationDays;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
