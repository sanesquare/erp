package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
@Entity
@Table(name = "terminations")
public class Termination implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7889320110144227800L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	private Employee terminated;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "termination_forwards", joinColumns = { @JoinColumn(name = "termination_id") }, inverseJoinColumns = { @JoinColumn(name = "terminator_id") })
	private Set<Employee> terminator;

	@Column(name = "terminated_on")
	private Date terminatedOn;

	@Column(name = "description", length = 1000)
	private String description;

	@OneToOne(fetch = FetchType.EAGER)
	private TerminationStatus status;

	@Column(name = "status_description", length = 1000)
	private String statusDescription;

	@Column(name = "notes")
	private String notes;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "termination", cascade = CascadeType.ALL)
	private Set<TerminationDocuments> documents;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * 
	 * @return
	 */
	public Set<TerminationDocuments> getDocuments() {
		return documents;
	}

	/**
	 * 
	 * @param documents
	 */
	public void setDocuments(Set<TerminationDocuments> documents) {
		this.documents = documents;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public Employee getTerminated() {
		return terminated;
	}

	/**
	 * 
	 * @param terminated
	 */
	public void setTerminated(Employee terminated) {
		this.terminated = terminated;
	}

	/**
	 * 
	 * @return
	 */
	public Set<Employee> getTerminator() {
		return terminator;
	}

	/**
	 * 
	 * @param terminator
	 */
	public void setTerminator(Set<Employee> terminator) {
		this.terminator = terminator;
	}

	/**
	 * 
	 * @return
	 */
	public Date getTerminatedOn() {
		return terminatedOn;
	}

	/**
	 * 
	 * @param terminatedOn
	 */
	public void setTerminatedOn(Date terminatedOn) {
		this.terminatedOn = terminatedOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public TerminationStatus getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(TerminationStatus status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * 
	 * @param statusDescription
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

}
