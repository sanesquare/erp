package com.hrms.web.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * 
 * @author Shamsheer
 * @since 25-April-2015
 */
@Embeddable
public class SalaryPayslipItemsId implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1045763451343613792L;

	@ManyToOne
	private SalaryMonthly salaryMonthly;
	
	@ManyToOne
	private PaySlipItem paySlipItem;

	public SalaryMonthly getSalaryMonthly() {
		return salaryMonthly;
	}

	public void setSalaryMonthly(SalaryMonthly salaryMonthly) {
		this.salaryMonthly = salaryMonthly;
	}

	public PaySlipItem getPaySlipItem() {
		return paySlipItem;
	}

	public void setPaySlipItem(PaySlipItem paySlipItem) {
		this.paySlipItem = paySlipItem;
	}
}
