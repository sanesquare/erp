package com.hrms.web.entities;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Vips
 *
 */

@Entity
@Table(name = "MST_REFERENCE", uniqueConstraints = @UniqueConstraint(columnNames = "reference_cd"))
public class MstReference implements AuditEntity,java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "reference_type")
	private String referenceType;
	
	@Column(name = "reference_cd")
	private String referenceCd;
	
	@Column(name = "reference_val")
	private String referenceVal;
	

	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReferenceType() {
		return this.referenceType;
	}

	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}

	
	public String getReferenceCd() {
		return this.referenceCd;
	}

	public void setReferenceCd(String referenceCd) {
		this.referenceCd = referenceCd;
	}


	public String getReferenceVal() {
		return this.referenceVal;
	}

	public void setReferenceVal(String referenceVal) {
		this.referenceVal = referenceVal;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
