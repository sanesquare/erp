package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 25-April-2015
 */
@Entity
@Table(name = "salary_monthly")
public class SalaryMonthly implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3687713906204671566L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@ManyToOne
	private Employee employee;
	
	@Column(name = "with_effect_from")
	private Date withEffectFrom;
	
	@Column(name = "gross_salary_monthly")
	private BigDecimal grossSalaryMonthly;
	
	@Column(name = "gross_salary_annually")
	private BigDecimal grossSalaryAnnually;
	
	@Column(name = "tax_deduction_monthly")
	private BigDecimal taxDeductionMonthly;
	
	@Column(name = "tax_deduction_annually")
	private BigDecimal taxDeductionAnnually;
	
	@Column(name = "estimated_monthly_salary")
	private BigDecimal estimatedMonthlySalary;
	
	@Column(name = "estimated_annual_salary")
	private BigDecimal estimatedAnnualSalary;
	
	@OneToMany(mappedBy = "salaryPayslipItemsId.salaryMonthly" , fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	private Set<SalaryPayslipItems> payslipItems = new HashSet<SalaryPayslipItems>();

	@OneToMany(mappedBy = "salaryExtraPaySlipItemsId.salaryMonthly" , fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	private Set<SalaryExtraPayslipItem> extraPayslipItems = new HashSet<SalaryExtraPayslipItem>();
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Date getWithEffectFrom() {
		return withEffectFrom;
	}

	public void setWithEffectFrom(Date withEffectFrom) {
		this.withEffectFrom = withEffectFrom;
	}

	public BigDecimal getGrossSalaryMonthly() {
		return grossSalaryMonthly;
	}

	public void setGrossSalaryMonthly(BigDecimal grossSalaryMonthly) {
		this.grossSalaryMonthly = grossSalaryMonthly;
	}

	public BigDecimal getGrossSalaryAnnually() {
		return grossSalaryAnnually;
	}

	public void setGrossSalaryAnnually(BigDecimal grossSalaryAnnually) {
		this.grossSalaryAnnually = grossSalaryAnnually;
	}

	public BigDecimal getTaxDeductionMonthly() {
		return taxDeductionMonthly;
	}

	public void setTaxDeductionMonthly(BigDecimal taxDeductionMonthly) {
		this.taxDeductionMonthly = taxDeductionMonthly;
	}

	public BigDecimal getTaxDeductionAnnually() {
		return taxDeductionAnnually;
	}

	public void setTaxDeductionAnnually(BigDecimal taxDeductionAnnually) {
		this.taxDeductionAnnually = taxDeductionAnnually;
	}

	public BigDecimal getEstimatedMonthlySalary() {
		return estimatedMonthlySalary;
	}

	public void setEstimatedMonthlySalary(BigDecimal estimatedMonthlySalary) {
		this.estimatedMonthlySalary = estimatedMonthlySalary;
	}

	public BigDecimal getEstimatedAnnualSalary() {
		return estimatedAnnualSalary;
	}

	public void setEstimatedAnnualSalary(BigDecimal estimatedAnnualSalary) {
		this.estimatedAnnualSalary = estimatedAnnualSalary;
	}

	public Set<SalaryPayslipItems> getPayslipItems() {
		return payslipItems;
	}

	public void setPayslipItems(Set<SalaryPayslipItems> payslipItems) {
		this.payslipItems = payslipItems;
	}

	public Set<SalaryExtraPayslipItem> getExtraPayslipItems() {
		return extraPayslipItems;
	}

	public void setExtraPayslipItems(Set<SalaryExtraPayslipItem> extraPayslipItems) {
		this.extraPayslipItems = extraPayslipItems;
	}
}
