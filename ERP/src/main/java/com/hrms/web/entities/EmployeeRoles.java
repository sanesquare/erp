package com.hrms.web.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 
 * @author Shamsheer
 * @since 31-March-2015
 */
@Entity
@Table(name="employee_roles")
@AssociationOverrides({
	@AssociationOverride(name = "roleTypeId.employee", joinColumns=@JoinColumn(name="employee_id")),
	@AssociationOverride(name = "roleTypeId.roleType", joinColumns=@JoinColumn(name="role_type_id")),
	@AssociationOverride(name="roleTypeId.roles",joinColumns=@JoinColumn(name="role_id"))
})
public class EmployeeRoles implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2626087815204443681L;

	@EmbeddedId
	private RoleTypeId roleTypeId = new RoleTypeId();
	
	
	@Transient
	public Employee getEmployee(){
		return this.roleTypeId.getEmployee();
	}
	
	public void setEmployee(Employee employee){
		this.roleTypeId.setEmployee(employee);
	}
	
	@Transient
	public RoleType getRoleType(){
		return this.roleTypeId.getRoleType();
	}
	
	public void setRoles(Roles roles){
		this.roleTypeId.setRoles(roles);
	}
	
	@Transient
	public Roles getRoles(){
		return this.roleTypeId.getRoles();
	}
	
	public void setRoleType(RoleType roleType){
		this.roleTypeId.setRoleType(roleType);
	}

	/**
	 * @return the roleTypeId
	 */
	public RoleTypeId getRoleTypeId() {
		return roleTypeId;
	}

	/**
	 * @param roleTypeId the roleTypeId to set
	 */
	public void setRoleTypeId(RoleTypeId roleTypeId) {
		this.roleTypeId = roleTypeId;
	}


}
