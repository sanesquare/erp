package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Entity
@Table(name = "payslip_items")
public class PaySlipItem implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3140084400058601148L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "title")
	private String title;

	@Column(name = "type")
	private String type;

	@Column(name = "tax_allowance")
	private Boolean taxAllowance;

	@Column(name = "calculation")
	private String calculation;

	@Column(name = "effect_from")
	private Date effectFrom;

	@OneToMany(mappedBy= "salaryPayslipItemsId.paySlipItem" , fetch = FetchType.LAZY)
	private Set<SalaryPayslipItems> salaryPayslipItems = new HashSet<SalaryPayslipItems>();
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 
	 * @return
	 */
	public Boolean getTaxAllowance() {
		return taxAllowance;
	}

	/**
	 * 
	 * @param taxAllowance
	 */
	public void setTaxAllowance(Boolean taxAllowance) {
		this.taxAllowance = taxAllowance;
	}

	/**
	 * 
	 * @return
	 */
	public String getCalculation() {
		return calculation;
	}

	/**
	 * 
	 * @param calculation
	 */
	public void setCalculation(String calculation) {
		this.calculation = calculation;
	}

	/**
	 * 
	 * @return
	 */
	public Date getEffectFrom() {
		return effectFrom;
	}

	/**
	 * 
	 * @param effectFrom
	 */
	public void setEffectFrom(Date effectFrom) {
		this.effectFrom = effectFrom;
	}

	public Set<SalaryPayslipItems> getSalaryPayslipItems() {
		return salaryPayslipItems;
	}

	public void setSalaryPayslipItems(Set<SalaryPayslipItems> salaryPayslipItems) {
		this.salaryPayslipItems = salaryPayslipItems;
	}

}
