package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.erp.web.entities.ErPCurrency;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Entity
@Table(name = "base_currency")
public class BaseCurrency implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -930371509452374937L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "base_currency")
	private String baseCurrency;

	@Column(name = "sign")
	private String sign;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "baseCurrency")
	private Set<Currency> currency;
	
	@OneToMany(mappedBy = "currency")
	private Set<Branch> branches = new HashSet<Branch>();

	@OneToMany(mappedBy="baseCurrency")
	private Set<ErPCurrency> erPCurrencies;
	
	/**
	 * 
	 * @return
	 */
	public Set<Currency> getCurrency() {
		return currency;
	}

	/**
	 * 
	 * @param currency
	 */
	public void setCurrency(Set<Currency> currency) {
		this.currency = currency;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getBaseCurrency() {
		return baseCurrency;
	}

	/**
	 * 
	 * @param baseCurrency
	 */
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	/**
	 * 
	 * @return
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * 
	 * @param sign
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}


	public Set<Branch> getBranches() {
		return branches;
	}

	public void setBranches(Set<Branch> branches) {
		this.branches = branches;
	}

	public Set<ErPCurrency> getErPCurrencies() {
		return erPCurrencies;
	}

	public void setErPCurrencies(Set<ErPCurrency> erPCurrencies) {
		this.erPCurrencies = erPCurrencies;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
