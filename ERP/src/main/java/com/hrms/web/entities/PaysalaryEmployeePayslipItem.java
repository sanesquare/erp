package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

@Entity
@Table(name="pay_salary_employee_payslip_item")
public class PaysalaryEmployeePayslipItem implements AuditEntity,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1515192767382747704L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	private User createdBy;
	
	@ManyToOne
	private User updatedBy;
	
	@Column(name = "created_on")
	private Date createdOn;
	
	@Column(name = "updated_on")
	private Date updatedOn;
	
	@OneToOne
	private PaySlipItem item;
	
	@Column(name = "item_amount")
	private BigDecimal amount;
	
	@ManyToOne
	private PaySalaryEmployees employeeItem;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public PaySlipItem getItem() {
		return item;
	}

	public void setItem(PaySlipItem item) {
		this.item = item;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public PaySalaryEmployees getEmployeeItem() {
		return employeeItem;
	}

	public void setEmployeeItem(PaySalaryEmployees employeeItem) {
		this.employeeItem = employeeItem;
	}
}
