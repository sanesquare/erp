package com.hrms.web.entities;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;
import com.hrms.web.vo.InterviewDocumentsVo;

/**
 * 
 * @author Vinutha
 * @since 18-March-2015
 *
 */
@Entity
@Table(name = "job_interview")
public class JobInterview implements AuditEntity , Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -127697539802363631L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Employee> interviewers = new HashSet<Employee>();
	
	@OneToMany(mappedBy = "jobInterview" , fetch = FetchType.LAZY)
	private Set<JobCandidates> interviewee ;
	
	@OneToMany(mappedBy = "jobInterview" , cascade = CascadeType.ALL , fetch = FetchType.EAGER)
	private Set<JobInterviewDocuments> InterviewDocuments = new HashSet<JobInterviewDocuments>();
	
	@ManyToMany
	private Set<CandidateStatus> status = new HashSet<CandidateStatus>();
	
	@ManyToOne
	private JobPost jobPost;
	
	@Column(name = "interviewDate")
	private Date interviewDate;
	
	@Column(name="interviewTime")
	private Time interviewTime;
	
	@Column(name = "placeOfInterview")
	private String placeOfInterview;
	
	@Column(name ="selection_number")
	private int selectionNumber;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "additionalInfo")
	private String additionalInfo;
	 	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the interviewee
	 */
	public Set<JobCandidates> getInterviewee() {
		return interviewee;
	}

	/**
	 * @param interviewee the interviewee to set
	 */
	public void setInterviewee(Set<JobCandidates> interviewee) {
		this.interviewee = interviewee;
	}

	/**
	 * @return the jobPost
	 */
	public JobPost getJobPost() {
		return jobPost;
	}

	/**
	 * @param jobPost the jobPost to set
	 */
	public void setJobPost(JobPost jobPost) {
		this.jobPost = jobPost;
	}

	/**
	 * @return the interviewDate
	 */
	public Date getInterviewDate() {
		return interviewDate;
	}

	/**
	 * @param interviewDate the interviewDate to set
	 */
	public void setInterviewDate(Date interviewDate) {
		this.interviewDate = interviewDate;
	}

	/**
	 * @return the placeOfInterview
	 */
	public String getPlaceOfInterview() {
		return placeOfInterview;
	}

	/**
	 * @param placeOfInterview the placeOfInterview to set
	 */
	public void setPlaceOfInterview(String placeOfInterview) {
		this.placeOfInterview = placeOfInterview;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the additionalInfo
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * @param additionalInfo the additionalInfo to set
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	

	/**
	 * @return the interviewers
	 */
	public Set<Employee> getInterviewers() {
		return interviewers;
	}

	/**
	 * @param interviewers the interviewers to set
	 */
	public void setInterviewers(Set<Employee> interviewers) {
		this.interviewers = interviewers;
	}

	/**
	 * @return the status
	 */
	public Set<CandidateStatus> getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Set<CandidateStatus> status) {
		this.status = status;
	}

	/**
	 * @return the selectionNumber
	 */
	public int getSelectionNumber() {
		return selectionNumber;
	}

	/**
	 * @param selectionNumber the selectionNumber to set
	 */
	public void setSelectionNumber(int selectionNumber) {
		this.selectionNumber = selectionNumber;
	}

	

	/**
	 * @return the interviewDocuments
	 */
	public Set<JobInterviewDocuments> getInterviewDocuments() {
		return InterviewDocuments;
	}

	/**
	 * @param interviewDocuments the interviewDocuments to set
	 */
	public void setInterviewDocuments(Set<JobInterviewDocuments> interviewDocuments) {
		InterviewDocuments = interviewDocuments;
	}

	/**
	 * @return the interviewTime
	 */
	public Time getInterviewTime() {
		return interviewTime;
	}

	/**
	 * @param interviewTime the interviewTime to set
	 */
	public void setInterviewTime(Time interviewTime) {
		this.interviewTime = interviewTime;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
