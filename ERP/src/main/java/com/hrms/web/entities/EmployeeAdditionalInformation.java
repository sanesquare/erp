package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author shamsheer
 * @since 18-March-2015
 */
@Entity
@Table(name="employee_additional_information")
public class EmployeeAdditionalInformation implements AuditEntity,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	
	@Column(name="notes" , length = 1000)
	private String notes;
	
	@Column(name="driving_licence")
	private String drivingLicence;
	
	@Column(name="joining_date")
	private Date joiningDate;
	
	@Column(name="driving_licence_expiration")
	private Date licenceExpiration;
	
	@Column(name="passport")
	private String passport;
	
	@Column(name="passport_expiration")
	private Date passportExpiration;
	
	@Column(name="govt_id")
	private String governmentId;
	
	@Column(name = "pf_id")
	private String pfId;
	
	@Column(name = "esi_id")
	private String esiId;
	
	@Column(name="tax_number")
	private String taxNumber;
	
	@OneToOne
	private Employee employee;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the drivingLicence
	 */
	public String getDrivingLicence() {
		return drivingLicence;
	}

	/**
	 * @param drivingLicence the drivingLicence to set
	 */
	public void setDrivingLicence(String drivingLicence) {
		this.drivingLicence = drivingLicence;
	}

	/**
	 * @return the licenceExpiration
	 */
	public Date getLicenceExpiration() {
		return licenceExpiration;
	}

	/**
	 * @param licenceExpiration the licenceExpiration to set
	 */
	public void setLicenceExpiration(Date licenceExpiration) {
		this.licenceExpiration = licenceExpiration;
	}

	/**
	 * @return the passport
	 */
	public String getPassport() {
		return passport;
	}

	/**
	 * @param passport the passport to set
	 */
	public void setPassport(String passport) {
		this.passport = passport;
	}

	/**
	 * @return the passportExpiration
	 */
	public Date getPassportExpiration() {
		return passportExpiration;
	}

	/**
	 * @param passportExpiration the passportExpiration to set
	 */
	public void setPassportExpiration(Date passportExpiration) {
		this.passportExpiration = passportExpiration;
	}

	/**
	 * @return the governmentId
	 */
	public String getGovernmentId() {
		return governmentId;
	}

	/**
	 * @param governmentId the governmentId to set
	 */
	public void setGovernmentId(String governmentId) {
		this.governmentId = governmentId;
	}

	/**
	 * @return the taxNumber
	 */
	public String getTaxNumber() {
		return taxNumber;
	}

	/**
	 * @param taxNumber the taxNumber to set
	 */
	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	/**
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * @return the joiningDate
	 */
	public Date getJoiningDate() {
		return joiningDate;
	}

	/**
	 * @param joiningDate the joiningDate to set
	 */
	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPfId() {
		return pfId;
	}

	public void setPfId(String pfId) {
		this.pfId = pfId;
	}

	public String getEsiId() {
		return esiId;
	}

	public void setEsiId(String esiId) {
		this.esiId = esiId;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
