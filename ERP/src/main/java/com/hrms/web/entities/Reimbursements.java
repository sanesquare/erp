package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 16-April-2015
 */
@Entity
@Table(name = "reimbursements")
public class Reimbursements implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3671391281197145667L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@OneToMany(mappedBy = "reimbursements" , cascade = CascadeType.ALL , orphanRemoval = true)
	private Set<ReimbursementsItems> items = new HashSet<ReimbursementsItems>();
	
	@ManyToOne
	private Employee employee;
	
	@ManyToMany
	private Set<Employee> forwardApplicationTo;
	
	@ManyToOne
	private ForwardApplicationStatus status;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "amount")
	private BigDecimal amount;
	
	@Column(name = "reimbursement_date")
	private Date date;
	
	@Column(name = "description" , length = 1000)
	private String description;
	
	@Column(name = "status_description" , length = 1000)
	private String statusDescription;
	
	@Column(name = "notes" , length = 1000)
	private String notes;

	@OneToMany(mappedBy = "reimbursements" , cascade = CascadeType.ALL)
	private Set<ReimbursementsDocuments> documents;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<ReimbursementsItems> getItems() {
		return items;
	}


	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Set<Employee> getForwardApplicationTo() {
		return forwardApplicationTo;
	}

	public void setForwardApplicationTo(Set<Employee> forwardApplicationTo) {
		this.forwardApplicationTo = forwardApplicationTo;
	}

	public ForwardApplicationStatus getStatus() {
		return status;
	}

	public void setStatus(ForwardApplicationStatus status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Set<ReimbursementsDocuments> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<ReimbursementsDocuments> documents) {
		this.documents = documents;
	}
}
