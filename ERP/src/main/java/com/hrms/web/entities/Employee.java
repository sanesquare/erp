package com.hrms.web.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.accounts.web.entities.Ledger;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 10-March-2015
 *
 */
@Entity
@Table(name = "employee")
public class Employee implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "employee_code")
	private String employeeCode;

	@Column(name = "created_on")
	private Date createdOn;

	@ManyToOne
	private User createdBy;

	@ManyToMany(mappedBy = "forwardTo")
	Set<Promotion> promotions;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@Column(name = "payroll")
	private Boolean withPayroll;

	@Column(name = "is_admin")
	private Boolean isAdmin;

	@Column(name = "is_auto_approve")
	private Boolean isAutoApprove;

	@ManyToOne
	private SalaryType salaryType;

	@Column(name = "notify_by_email")
	private Boolean notifyByEmail;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeeSkills> skills;

	@ManyToOne
	private EmployeeType employeeType;

	@OneToOne(mappedBy = "employee", fetch = FetchType.LAZY)
	private EmployeeJoining employeeJoining;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeeUniform> employeeUniform;

	@ManyToOne
	private EmployeeStatus employeeStatus;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<WorkSheet> worksheets = new HashSet<WorkSheet>();

	@ManyToOne
	private Branch branch;

	@ManyToOne
	private EmployeeCategory employeeCategory;

	@ManyToMany(mappedBy = "users", fetch = FetchType.EAGER)
	private Set<Branch> branches = new HashSet<Branch>();

	@ManyToOne
	private EmployeeDesignation employeeDesignation;

	@OneToOne(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EmployeeReference reference;

	@OneToOne(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private UserProfile userProfile;

	@OneToOne(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EmployeeAdditionalInformation additionalInformation;

	@OneToOne(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private UserAddress userAddress;

	@ManyToOne
	private EmployeeGrade employeegrade;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeeExperience> experience;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeeOrganizationAssets> organizationAssets;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeeBankAccount> bankAccount;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeeDependants> employeeDependants;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeNextOfKin> nextOfKin;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeeBenefit> employeeBenefit;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "employee", cascade = CascadeType.ALL)
	private Set<EmployeeDocuments> documents;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "employee", fetch = FetchType.LAZY)
	private EmployeeEmergencyContact emergencyContact;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeeLanguage> language;

	@ManyToOne(fetch = FetchType.LAZY)
	private EmployeeWorkShift workShifts;

	@ManyToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Project> project;

	@ManyToMany(mappedBy = "projectHeads", fetch = FetchType.LAZY)
	private Set<Project> projectHead;

	@ManyToMany(mappedBy = "employee")
	private Set<Reminders> reminders;

	@ManyToMany(mappedBy = "interviewers", fetch = FetchType.LAZY)
	private Set<JobInterview> interviews;

	@OneToOne(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private User user;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "meetingEmployeeId.employee")
	private Set<MeetingEmployee> meetingEmployees = new HashSet<MeetingEmployee>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "roleTypeId.employee", cascade = CascadeType.ALL)
	@OrderBy("id")
	private Set<EmployeeRoles> employeeRoles = new LinkedHashSet<EmployeeRoles>();

	@OneToOne(mappedBy = "employee", fetch = FetchType.LAZY)
	private EmployeeExit employeeExit;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "employee_superiors", joinColumns = { @JoinColumn(name = "employee_id") }, inverseJoinColumns = { @JoinColumn(name = "superior_id") })
	private Set<Employee> superiors = new HashSet<Employee>();

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "employee_subordinates", joinColumns = { @JoinColumn(name = "employee_id") }, inverseJoinColumns = { @JoinColumn(name = "subordinate_id") })
	private Set<Employee> subordinates = new HashSet<Employee>();

	@OneToOne(mappedBy = "terminated", fetch = FetchType.LAZY)
	private Termination terminatedEmployee;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<EmployeeQualifications> qualification;

	@ManyToMany(mappedBy = "terminator", fetch = FetchType.LAZY)
	private Set<Termination> terminatorEmployee;

	@OneToOne(mappedBy = "warningTo")
	private Warning warningTo;

	@ManyToMany(mappedBy = "forwader")
	private Set<Warning> warningBy;

	@ManyToOne(fetch = FetchType.LAZY)
	private Department departmentEmployees;

	private Department departmentHead;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employees")
	private Set<Notification> notifications;

	@OneToOne(mappedBy = "memoFrom", fetch = FetchType.LAZY)
	private Memo memoFrom;

	@ManyToMany(mappedBy = "forwader")
	private Set<Memo> memoTo;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "employee")
	private PerformanceEvaluationDocument performanceEvaluationDocument;

	@OneToOne(mappedBy = "employee", fetch = FetchType.LAZY)
	private Resignation resignation;

	@OneToMany(mappedBy = "forwardApplicationTo", fetch = FetchType.LAZY)
	private Set<Resignation> resignationForwardApplicationList;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Transfer> employeeToTransferList;

	@OneToMany(mappedBy = "forwardApplicationTo", fetch = FetchType.LAZY)
	private Set<Transfer> transferForwardApplicationList;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Travel> travelList;

	@OneToMany(mappedBy = "forwardApplicationTo", fetch = FetchType.LAZY)
	private Set<Travel> travelForwardApplicationList;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Achievement> achievements;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private Set<Contract> contract;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private Set<Assignments> assignments;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee", cascade = CascadeType.ALL)
	private Set<EmployeeLeave> leaves;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private Set<WorkShift> workShift;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Leaves> leavesTaken;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "forwardApplicationTo")
	private Set<Leaves> leaveApplications;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private Set<Attendance> attendances;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "employee")
	private HourlyWages hourlyWages;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "employee")
	private Insurance insurance;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "forwaders")
	private SecondLevelForwader secondLevelForwader;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "taxExluder")
	private TaxExcludeEmployee taxExludedEmployee;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Bonuses> bonuses;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Deductions> deductions;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "employee")
	private Loan loan;

	@ManyToMany(mappedBy = "forwardTo", fetch = FetchType.LAZY)
	private Set<Loan> loanForwader;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "employee")
	private AdvanceSalary advanceSalary;

	@ManyToMany(mappedBy = "forwader", fetch = FetchType.LAZY)
	private Set<AdvanceSalary> advanceSalaryForwader;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "employee")
	private Overtime overtime;

	@ManyToMany(mappedBy = "forwader", fetch = FetchType.LAZY)
	private Set<Overtime> overtimeForwader;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Commissions> commissions;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "forwardApplicationTo")
	private Set<Commissions> commissionApplications;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "employee")
	private Esi esi;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "employee")
	private ProfidentFund profidentFund;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Adjustments> adjustments;

	@OneToOne(mappedBy = "employee", fetch = FetchType.LAZY)
	private DailyWages dailyWages;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Reimbursements> reimbursements;

	@ManyToMany(mappedBy = "forwardApplicationTo", fetch = FetchType.LAZY)
	private Set<Reimbursements> reimbursementsApplications;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<SalaryDailyWage> salaryDailyWages;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<SalaryHourlyWage> salaryHourlyWages;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<SalaryMonthly> salaries;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<Payslip> payslips;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pay_roll_option")
	private PayRollOption payRollOption;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private Set<IndividualEvaluation> individualEvaluation;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "reportTo")
	private Set<WorkSheet> reportedWorksheets;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private Set<PaySalaryEmployees> paySalaryEmployees;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private Set<PaySalaryHourlyWage> paySalaryHourlyWages;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<PaySalaryDailyWages> paySalaryDailyWages;

	@OneToOne(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = false, fetch = FetchType.LAZY)
	private Ledger ledger;

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	/**
	 * 
	 * @return
	 */
	public PayRollOption getPayRollOption() {
		return payRollOption;
	}

	/**
	 * 
	 * @param payRollOption
	 */
	public void setPayRollOption(PayRollOption payRollOption) {
		this.payRollOption = payRollOption;
	}

	/**
	 * 
	 * @return
	 */
	public SecondLevelForwader getSecondLevelForwader() {
		return secondLevelForwader;
	}

	/**
	 * 
	 * @param secondLevelForwader
	 */
	public void setSecondLevelForwader(SecondLevelForwader secondLevelForwader) {
		this.secondLevelForwader = secondLevelForwader;
	}

	/**
	 * 
	 * @return
	 */
	public TaxExcludeEmployee getTaxExludedEmployee() {
		return taxExludedEmployee;
	}

	/**
	 * 
	 * @param taxExludedEmployee
	 */
	public void setTaxExludedEmployee(TaxExcludeEmployee taxExludedEmployee) {
		this.taxExludedEmployee = taxExludedEmployee;
	}

	public Set<EmployeeLeave> getLeaves() {
		return leaves;
	}

	public void setLeaves(Set<EmployeeLeave> leaves) {
		this.leaves = leaves;
	}

	/**
	 * 
	 * @return
	 */
	public Set<Termination> getTerminatorEmployee() {
		return terminatorEmployee;
	}

	/**
	 * 
	 * @param terminatorEmployee
	 */
	public void setTerminatorEmployee(Set<Termination> terminatorEmployee) {
		this.terminatorEmployee = terminatorEmployee;
	}

	/**
	 * 
	 * @return
	 */
	public Termination getTerminatedEmployee() {
		return terminatedEmployee;
	}

	/**
	 * 
	 * @param terminatedEmployee
	 */
	public void setTerminatedEmployee(Termination terminatedEmployee) {
		this.terminatedEmployee = terminatedEmployee;
	}

	@Column(name = "profile_pic")
	private String profilePic;

	/**
	 * 
	 * @return
	 */
	public EmployeeExit getEmployeeExit() {
		return employeeExit;
	}

	/**
	 * 
	 * @param employeeExit
	 */
	public void setEmployeeExit(EmployeeExit employeeExit) {
		this.employeeExit = employeeExit;
	}

	/*
	 * @ManyToMany(mappedBy="employee") private Set<Project> project;
	 */

	/*
	 * @OneToMany(fetch=FetchType.LAZY,mappedBy="meetingEmployeeId.employee")
	 * private Set<MeetingEmployee> meetingEmployees=new
	 * HashSet<MeetingEmployee>(0);
	 */
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the createdBy
	 */
	public User getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn
	 *            the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the updatedBy
	 */
	public User getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 *            the updatedBy to set
	 */
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the project
	 */
	public Set<Project> getProject() {
		return project;
	}

	/**
	 * @param project
	 *            the project to set
	 */
	public void setProject(Set<Project> project) {
		this.project = project;
	}

	/**
	 * @return the meetingEmployees
	 */
	public Set<MeetingEmployee> getMeetingEmployees() {
		return meetingEmployees;
	}

	/**
	 * @param meetingEmployees
	 *            the meetingEmployees to set
	 */
	public void setMeetingEmployees(Set<MeetingEmployee> meetingEmployees) {
		this.meetingEmployees = meetingEmployees;
	}

	/**
	 * @return the employeeType
	 */
	public EmployeeType getEmployeeType() {
		return employeeType;
	}

	/**
	 * @param employeeType
	 *            the employeeType to set
	 */
	public void setEmployeeType(EmployeeType employeeType) {
		this.employeeType = employeeType;
	}

	/**
	 * @return the employeeCategory
	 */
	public EmployeeCategory getEmployeeCategory() {
		return employeeCategory;
	}

	/**
	 * @param employeeCategory
	 *            the employeeCategory to set
	 */
	public void setEmployeeCategory(EmployeeCategory employeeCategory) {
		this.employeeCategory = employeeCategory;
	}

	/**
	 * @return the employeeDesignation
	 */
	public EmployeeDesignation getEmployeeDesignation() {
		return employeeDesignation;
	}

	/**
	 * @param employeeDesignation
	 *            the employeeDesignation to set
	 */
	public void setEmployeeDesignation(EmployeeDesignation employeeDesignation) {
		this.employeeDesignation = employeeDesignation;
	}

	/**
	 * @return the employeeStatus
	 */
	public EmployeeStatus getEmployeeStatus() {
		return employeeStatus;
	}

	/**
	 * @param employeeStatus
	 *            the employeeStatus to set
	 */
	public void setEmployeeStatus(EmployeeStatus employeeStatus) {
		this.employeeStatus = employeeStatus;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the employeeCode
	 */
	public String getEmployeeCode() {
		return employeeCode;
	}

	/**
	 * @param employeeCode
	 *            the employeeCode to set
	 */
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	/**
	 * @return the userProfile
	 */
	public UserProfile getUserProfile() {
		return userProfile;
	}

	/**
	 * @param userProfile
	 *            the userProfile to set
	 */
	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}

	/**
	 * @return the employeegrade
	 */
	public EmployeeGrade getEmployeegrade() {
		return employeegrade;
	}

	/**
	 * @param employeegrade
	 *            the employeegrade to set
	 */
	public void setEmployeegrade(EmployeeGrade employeegrade) {
		this.employeegrade = employeegrade;
	}

	/**
	 * @return the emergencyContact
	 */
	public EmployeeEmergencyContact getEmergencyContact() {
		return emergencyContact;
	}

	/**
	 * @param emergencyContact
	 *            the emergencyContact to set
	 */
	public void setEmergencyContact(EmployeeEmergencyContact emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	/**
	 * @return the workShift
	 */
	public EmployeeWorkShift getWorkShifts() {
		return workShifts;
	}

	/**
	 * @param workShift
	 *            the workShift to set
	 */
	public void setWorkShift(EmployeeWorkShift workShifts) {
		this.workShifts = workShifts;
	}

	/**
	 * @return the skills
	 */
	public Set<EmployeeSkills> getSkills() {
		return skills;
	}

	/**
	 * @param skills
	 *            the skills to set
	 */
	public void setSkills(Set<EmployeeSkills> skills) {
		this.skills = skills;
	}

	/**
	 * @return the additionalInformation
	 */
	public EmployeeAdditionalInformation getAdditionalInformation() {
		return additionalInformation;
	}

	/**
	 * @param employee
	 *            the employee to set
	 * @param additionalInformation
	 *            the additionalInformation to set
	 */
	public void setAdditionalInformation(EmployeeAdditionalInformation additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	/**
	 * @return the profilePic
	 */
	public String getProfilePic() {
		return profilePic;
	}

	/**
	 * @param superiors
	 *            the superiors to set
	 * @param profilePic
	 *            the profilePic to set
	 */
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	/**
	 * @return the branch
	 */
	public Branch getBranch() {
		return branch;
	}

	/**
	 * 
	 * @param branch
	 */
	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	/**
	 * @return the interviews
	 */
	public Set<JobInterview> getInterviews() {
		return interviews;
	}

	/**
	 * @param interviews
	 *            the interviews to set
	 */
	public void setInterviews(Set<JobInterview> interviews) {
		this.interviews = interviews;
	}

	/**
	 * @return the userAddress
	 */
	public UserAddress getUserAddress() {
		return userAddress;
	}

	/**
	 * @param userAddress
	 *            the userAddress to set
	 */
	public void setUserAddress(UserAddress userAddress) {
		this.userAddress = userAddress;
	}

	/**
	 * @return the language
	 */
	public Set<EmployeeLanguage> getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(Set<EmployeeLanguage> language) {
		this.language = language;
	}

	/**
	 * @return the qualification
	 */
	public Set<EmployeeQualifications> getQualification() {
		return qualification;
	}

	/**
	 * @param qualification
	 *            the qualification to set
	 */
	public void setQualification(Set<EmployeeQualifications> qualification) {
		this.qualification = qualification;
	}

	/**
	 * @return the employeeUniform
	 */
	public Set<EmployeeUniform> getEmployeeUniform() {
		return employeeUniform;
	}

	/**
	 * @param employeeUniform
	 *            the employeeUniform to set
	 */
	public void setEmployeeUniform(Set<EmployeeUniform> employeeUniform) {
		this.employeeUniform = employeeUniform;
	}

	/**
	 * @return the reference
	 */
	public EmployeeReference getReference() {
		return reference;
	}

	/**
	 * @param reference
	 *            the reference to set
	 */
	public void setReference(EmployeeReference reference) {
		this.reference = reference;
	}

	/**
	 * @return the departmentEmployees
	 */
	public Department getDepartmentEmployees() {
		return departmentEmployees;
	}

	/**
	 * @param departmentEmployees
	 *            the departmentEmployees to set
	 */
	public void setDepartmentEmployees(Department departmentEmployees) {
		this.departmentEmployees = departmentEmployees;
	}

	/**
	 * @return the withPayroll
	 */
	public Boolean getWithPayroll() {
		return withPayroll;
	}

	/**
	 * @param withPayroll
	 *            the withPayroll to set
	 */
	public void setWithPayroll(Boolean withPayroll) {
		this.withPayroll = withPayroll;
	}

	/**
	 * @return the notifyByEmail
	 */
	public Boolean getNotifyByEmail() {
		return notifyByEmail;
	}

	/**
	 * @param notifyByEmail
	 *            the notifyByEmail to set
	 */
	public void setNotifyByEmail(Boolean notifyByEmail) {
		this.notifyByEmail = notifyByEmail;
	}

	/**
	 * @return the subordinates
	 */
	public Set<Employee> getSubordinates() {
		return subordinates;
	}

	/**
	 * @param subordinates
	 *            the subordinates to set
	 */
	public void setSubordinates(Set<Employee> subordinates) {
		this.subordinates = subordinates;
	}

	/**
	 * @return the superiors
	 */
	public Set<Employee> getSuperiors() {
		return superiors;
	}

	/**
	 * @param superiors
	 *            the superiors to set
	 */
	public void setSuperiors(Set<Employee> superiors) {
		this.superiors = superiors;
	}

	/**
	 * @return the employeeJoining
	 */
	public EmployeeJoining getEmployeeJoining() {
		return employeeJoining;
	}

	/**
	 * @param employeeJoining
	 *            the employeeJoining to set
	 */
	public void setEmployeeJoining(EmployeeJoining employeeJoining) {
		this.employeeJoining = employeeJoining;
	}

	/**
	 * @return the departmentHead
	 */
	public Department getDepartmentHead() {
		return departmentHead;
	}

	/**
	 * @param departmentHead
	 *            the departmentHead to set
	 */
	public void setDepartmentHead(Department departmentHead) {
		this.departmentHead = departmentHead;
	}

	/**
	 * @return the performanceEvaluationDocument
	 */
	public PerformanceEvaluationDocument getPerformanceEvaluationDocument() {
		return performanceEvaluationDocument;
	}

	/**
	 * @param performanceEvaluationDocument
	 *            the performanceEvaluationDocument to set
	 */
	public void setPerformanceEvaluationDocument(PerformanceEvaluationDocument performanceEvaluationDocument) {
		this.performanceEvaluationDocument = performanceEvaluationDocument;
	}

	public Resignation getResignation() {
		return resignation;
	}

	public void setResignation(Resignation resignation) {
		this.resignation = resignation;
	}

	public Set<Resignation> getResignationForwardApplicationList() {
		return resignationForwardApplicationList;
	}

	public void setResignationForwardApplicationList(Set<Resignation> resignationForwardApplicationList) {
		this.resignationForwardApplicationList = resignationForwardApplicationList;
	}

	public Set<Transfer> getEmployeeToTransferList() {
		return employeeToTransferList;
	}

	public void setEmployeeToTransferList(Set<Transfer> employeeToTransferList) {
		this.employeeToTransferList = employeeToTransferList;
	}

	public Set<Transfer> getTransferForwardApplicationList() {
		return transferForwardApplicationList;
	}

	public void setTransferForwardApplicationList(Set<Transfer> transferForwardApplicationList) {
		this.transferForwardApplicationList = transferForwardApplicationList;
	}

	public Set<Travel> getTravelList() {
		return travelList;
	}

	public void setTravelList(Set<Travel> travelList) {
		this.travelList = travelList;
	}

	public Set<Travel> getTravelForwardApplicationList() {
		return travelForwardApplicationList;
	}

	public void setTravelForwardApplicationList(Set<Travel> travelForwardApplicationList) {
		this.travelForwardApplicationList = travelForwardApplicationList;
	}

	public Set<Achievement> getAchievements() {
		return achievements;
	}

	public void setAchievements(Set<Achievement> achievements) {
		this.achievements = achievements;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the contract
	 */
	public Set<Contract> getContract() {
		return contract;
	}

	/**
	 * @param contract
	 *            the contract to set
	 */
	public void setContract(Set<Contract> contract) {
		this.contract = contract;
	}

	/**
	 * @return the assignments
	 */
	public Set<Assignments> getAssignments() {
		return assignments;
	}

	/**
	 * @param assignments
	 *            the assignments to set
	 */
	public void setAssignments(Set<Assignments> assignments) {
		this.assignments = assignments;
	}

	public Set<Promotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(Set<Promotion> promotions) {
		this.promotions = promotions;
	}

	/**
	 * @param workShifts
	 *            the workShifts to set
	 */
	public void setWorkShifts(EmployeeWorkShift workShifts) {
		this.workShifts = workShifts;
	}

	/**
	 * @return the workShift
	 */
	public Set<WorkShift> getWorkShift() {
		return workShift;
	}

	/**
	 * @param workShift
	 *            the workShift to set
	 */
	public void setWorkShift(Set<WorkShift> workShift) {
		this.workShift = workShift;
	}


	public Set<Leaves> getLeavesTaken() {
		return leavesTaken;
	}

	public void setLeavesTaken(Set<Leaves> leavesTaken) {
		this.leavesTaken = leavesTaken;
	}

	public Set<Leaves> getLeaveApplications() {
		return leaveApplications;
	}

	public void setLeaveApplications(Set<Leaves> leaveApplications) {
		this.leaveApplications = leaveApplications;
	}

	public Set<Attendance> getAttendances() {
		return attendances;
	}

	public void setAttendances(Set<Attendance> attendances) {
		this.attendances = attendances;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public HourlyWages getHourlyWages() {
		return hourlyWages;
	}

	public void setHourlyWages(HourlyWages hourlyWages) {
		this.hourlyWages = hourlyWages;
	}

	public Insurance getInsurance() {
		return insurance;
	}

	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}

	public Set<Bonuses> getBonuses() {
		return bonuses;
	}

	public void setBonuses(Set<Bonuses> bonuses) {
		this.bonuses = bonuses;
	}

	public Set<Deductions> getDeductions() {
		return deductions;
	}

	public void setDeductions(Set<Deductions> deductions) {
		this.deductions = deductions;
	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public Set<Loan> getLoanForwader() {
		return loanForwader;
	}

	public void setLoanForwader(Set<Loan> loanForwader) {
		this.loanForwader = loanForwader;
	}

	public AdvanceSalary getAdvanceSalary() {
		return advanceSalary;
	}

	public void setAdvanceSalary(AdvanceSalary advanceSalary) {
		this.advanceSalary = advanceSalary;
	}

	public Set<AdvanceSalary> getAdvanceSalaryForwader() {
		return advanceSalaryForwader;
	}

	public void setAdvanceSalaryForwader(Set<AdvanceSalary> advanceSalaryForwader) {
		this.advanceSalaryForwader = advanceSalaryForwader;
	}

	public Set<WorkSheet> getWorksheets() {
		return worksheets;
	}

	public void setWorksheets(Set<WorkSheet> worksheets) {
		this.worksheets = worksheets;
	}

	public Overtime getOvertime() {
		return overtime;
	}

	public void setOvertime(Overtime overtime) {
		this.overtime = overtime;
	}

	public Set<Overtime> getOvertimeForwader() {
		return overtimeForwader;
	}

	public void setOvertimeForwader(Set<Overtime> overtimeForwader) {
		this.overtimeForwader = overtimeForwader;
	}

	public Set<Commissions> getCommissions() {
		return commissions;
	}

	public void setCommissions(Set<Commissions> commissions) {
		this.commissions = commissions;
	}

	public Esi getEsi() {
		return esi;
	}

	public void setEsi(Esi esi) {
		this.esi = esi;
	}

	public ProfidentFund getProfidentFund() {
		return profidentFund;
	}

	public void setProfidentFund(ProfidentFund profidentFund) {
		this.profidentFund = profidentFund;
	}

	public Set<Commissions> getCommissionApplications() {
		return commissionApplications;
	}

	public void setCommissionApplications(Set<Commissions> commissionApplications) {
		this.commissionApplications = commissionApplications;
	}

	public Set<Adjustments> getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(Set<Adjustments> adjustments) {
		this.adjustments = adjustments;
	}

	public DailyWages getDailyWages() {
		return dailyWages;
	}

	public void setDailyWages(DailyWages dailyWages) {
		this.dailyWages = dailyWages;
	}

	public Set<Reimbursements> getReimbursements() {
		return reimbursements;
	}

	public void setReimbursements(Set<Reimbursements> reimbursements) {
		this.reimbursements = reimbursements;
	}

	public Set<Reimbursements> getReimbursementsApplications() {
		return reimbursementsApplications;
	}

	public void setReimbursementsApplications(Set<Reimbursements> reimbursementsApplications) {
		this.reimbursementsApplications = reimbursementsApplications;
	}

	public SalaryType getSalaryType() {
		return salaryType;
	}

	public void setSalaryType(SalaryType salaryType) {
		this.salaryType = salaryType;
	}

	public Set<SalaryDailyWage> getSalaryDailyWages() {
		return salaryDailyWages;
	}

	public void setSalaryDailyWages(Set<SalaryDailyWage> salaryDailyWages) {
		this.salaryDailyWages = salaryDailyWages;
	}

	public Set<SalaryHourlyWage> getSalaryHourlyWages() {
		return salaryHourlyWages;
	}

	public void setSalaryHourlyWages(Set<SalaryHourlyWage> salaryHourlyWages) {
		this.salaryHourlyWages = salaryHourlyWages;
	}

	public Set<SalaryMonthly> getSalaries() {
		return salaries;
	}

	public void setSalaries(Set<SalaryMonthly> salaries) {
		this.salaries = salaries;
	}

	public Set<Payslip> getPayslips() {
		return payslips;
	}

	public void setPayslips(Set<Payslip> payslips) {
		this.payslips = payslips;
	}

	public Set<Project> getProjectHead() {
		return projectHead;
	}

	public void setProjectHead(Set<Project> projectHead) {
		this.projectHead = projectHead;
	}

	/**
	 * @return the individualEvaluation
	 */
	public Set<IndividualEvaluation> getIndividualEvaluation() {
		return individualEvaluation;
	}

	/**
	 * @param individualEvaluation
	 *            the individualEvaluation to set
	 */
	public void setIndividualEvaluation(Set<IndividualEvaluation> individualEvaluation) {
		this.individualEvaluation = individualEvaluation;
	}

	/**
	 * @return the reportedWorksheets
	 */
	public Set<WorkSheet> getReportedWorksheets() {
		return reportedWorksheets;
	}

	/**
	 * @param reportedWorksheets
	 *            the reportedWorksheets to set
	 */
	public void setReportedWorksheets(Set<WorkSheet> reportedWorksheets) {
		this.reportedWorksheets = reportedWorksheets;
	}

	public Set<PaySalaryEmployees> getPaySalaryEmployees() {
		return paySalaryEmployees;
	}

	public void setPaySalaryEmployees(Set<PaySalaryEmployees> paySalaryEmployees) {
		this.paySalaryEmployees = paySalaryEmployees;
	}

	public Set<PaySalaryHourlyWage> getPaySalaryHourlyWages() {
		return paySalaryHourlyWages;
	}

	public void setPaySalaryHourlyWages(Set<PaySalaryHourlyWage> paySalaryHourlyWages) {
		this.paySalaryHourlyWages = paySalaryHourlyWages;
	}

	public Set<PaySalaryDailyWages> getPaySalaryDailyWages() {
		return paySalaryDailyWages;
	}

	public void setPaySalaryDailyWages(Set<PaySalaryDailyWages> paySalaryDailyWages) {
		this.paySalaryDailyWages = paySalaryDailyWages;
	}

	public Set<EmployeeExperience> getExperience() {
		return experience;
	}

	public void setExperience(Set<EmployeeExperience> experience) {
		this.experience = experience;
	}

	public Set<EmployeeOrganizationAssets> getOrganizationAssets() {
		return organizationAssets;
	}

	public void setOrganizationAssets(Set<EmployeeOrganizationAssets> organizationAssets) {
		this.organizationAssets = organizationAssets;
	}

	public Set<EmployeeBankAccount> getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(Set<EmployeeBankAccount> bankAccount) {
		this.bankAccount = bankAccount;
	}

	public Set<EmployeeDependants> getEmployeeDependants() {
		return employeeDependants;
	}

	public void setEmployeeDependants(Set<EmployeeDependants> employeeDependants) {
		this.employeeDependants = employeeDependants;
	}

	public Set<EmployeNextOfKin> getNextOfKin() {
		return nextOfKin;
	}

	public void setNextOfKin(Set<EmployeNextOfKin> nextOfKin) {
		this.nextOfKin = nextOfKin;
	}

	public Set<EmployeeBenefit> getEmployeeBenefit() {
		return employeeBenefit;
	}

	public void setEmployeeBenefit(Set<EmployeeBenefit> employeeBenefit) {
		this.employeeBenefit = employeeBenefit;
	}

	public Set<EmployeeDocuments> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<EmployeeDocuments> documents) {
		this.documents = documents;
	}

	public Set<Reminders> getReminders() {
		return reminders;
	}

	public void setReminders(Set<Reminders> reminders) {
		this.reminders = reminders;
	}

	public Warning getWarningTo() {
		return warningTo;
	}

	public void setWarningTo(Warning warningTo) {
		this.warningTo = warningTo;
	}

	public Set<Warning> getWarningBy() {
		return warningBy;
	}

	public void setWarningBy(Set<Warning> warningBy) {
		this.warningBy = warningBy;
	}

	public Set<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}

	public Memo getMemoFrom() {
		return memoFrom;
	}

	public void setMemoFrom(Memo memoFrom) {
		this.memoFrom = memoFrom;
	}

	public Set<Memo> getMemoTo() {
		return memoTo;
	}

	public void setMemoTo(Set<Memo> memoTo) {
		this.memoTo = memoTo;
	}

	public Set<Branch> getBranches() {
		return branches;
	}

	public void setBranches(Set<Branch> branches) {
		this.branches = branches;
	}

	public Boolean getIsAutoApprove() {
		return isAutoApprove;
	}

	public void setIsAutoApprove(Boolean isAutoApprove) {
		this.isAutoApprove = isAutoApprove;
	}

	public Set<EmployeeRoles> getEmployeeRoles() {
		return employeeRoles;
	}

	public void setEmployeeRoles(Set<EmployeeRoles> employeeRoles) {
		this.employeeRoles = employeeRoles;
	}

}
