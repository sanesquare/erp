package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 31-March-2015
 */
@Entity
@Table(name="role_types")
public class RoleType implements AuditEntity,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6914580848942016987L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name="type")
	private String type;
	
	@ManyToOne
	@JoinColumn(name = "title")
	private RoleTypeTitles titles;
	
	@OneToMany(fetch = FetchType.LAZY , mappedBy="roleTypeId.roleType")
	private Set<EmployeeRoles> employeeRoles = new HashSet<EmployeeRoles>(0);
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the employeeRoles
	 */
	public Set<EmployeeRoles> getEmployeeRoles() {
		return employeeRoles;
	}

	/**
	 * @param employeeRoles the employeeRoles to set
	 */
	public void setEmployeeRoles(Set<EmployeeRoles> employeeRoles) {
		this.employeeRoles = employeeRoles;
	}

	/**
	 * @return the titles
	 */
	public RoleTypeTitles getTitles() {
		return titles;
	}

	/**
	 * @param titles the titles to set
	 */
	public void setTitles(RoleTypeTitles titles) {
		this.titles = titles;
	}


}
