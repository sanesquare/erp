package com.hrms.web.entities;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Vinutha
 * @since 31-March-2015
 *
 */
@Entity
@Table(name="work_sheet")
public class WorkSheet implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4138091363912582726L;
	
	@Id
	@GeneratedValue(strategy  = GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
		
	@Column(name="date")
	private Date date;
	
	@ManyToOne
	private Employee employee; 
	
	@OneToMany(mappedBy = "workSheet",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	private Set<WorkSheetTasks> workSheetTasks = new HashSet<WorkSheetTasks>();
	
		
	@OneToMany(mappedBy ="workSheet",cascade = CascadeType.ALL)
	private Set<WorkSheetDocuments> workSheetDocuments = new HashSet<WorkSheetDocuments>();
	
	@Column(name="description")
	private String description;
	
	@Column(name="additional_info")
	private String additionalInfo;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Employee> reportTo;
	
	/*@Column(name="start_time")
	private Time startTime;
	
	@Column(name="end_time")
	private Time endTime;*/
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * @return the workSheetTasks
	 */
	public Set<WorkSheetTasks> getWorkSheetTasks() {
		return workSheetTasks;
	}

	/**
	 * @param workSheetTasks the workSheetTasks to set
	 */
	public void setWorkSheetTasks(Set<WorkSheetTasks> workSheetTasks) {
		this.workSheetTasks = workSheetTasks;
	}

	/**
	 * @return the workSheetDocuments
	 */
	public Set<WorkSheetDocuments> getWorkSheetDocuments() {
		return workSheetDocuments;
	}

	/**
	 * @param workSheetDocuments the workSheetDocuments to set
	 */
	public void setWorkSheetDocuments(Set<WorkSheetDocuments> workSheetDocuments) {
		this.workSheetDocuments = workSheetDocuments;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the additionalInfo
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * @param additionalInfo the additionalInfo to set
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	/**
	 * @return the reportTo
	 */
	public Set<Employee> getReportTo() {
		return reportTo;
	}

	/**
	 * @param reportTo the reportTo to set
	 */
	public void setReportTo(Set<Employee> reportTo) {
		this.reportTo = reportTo;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
}
