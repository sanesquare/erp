package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Sangeeth
 * @since 25-March-2015
 *
 */
@Entity
@Table(name = "travel")
public class Travel implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7889320110144227800L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	private Employee employee;

	@ManyToOne(fetch = FetchType.EAGER)
	private Employee forwardApplicationTo; 
	
	@Column(name = "purpose_of_visit")
	private String purposeOfVisit;
	
	@Column(name = "travel_start_date")
	private Date travelStartDate;

	@Column(name = "travel_end_date")
	private Date travelEndDate;
	
	@Column(name = "expected_travel_budget")
	private BigDecimal expectedTravelBudget;

	@Column(name = "actual_travel_budget")
	private BigDecimal actualTravelBudget;
	
	@Column(name = "travel_description", length = 1000)
	private String travelDescription;
	
	@ManyToOne
	private ForwardApplicationStatus status;

	@Column(name = "status_description" , length = 1000)
	private String statusDescription;
	
	@Column(name = "notes" , length = 1000)
	private String notes;

	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "travel", cascade = CascadeType.ALL)
	private Set<TravelDocuments> documents;
	
	@OneToMany(mappedBy = "travel" , cascade  = CascadeType.ALL , fetch = FetchType.EAGER , orphanRemoval = true)
	private Set<TravelDestinations> destinations = new HashSet<TravelDestinations>();

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Employee getForwardApplicationTo() {
		return forwardApplicationTo;
	}

	public void setForwardApplicationTo(Employee forwardApplicationTo) {
		this.forwardApplicationTo = forwardApplicationTo;
	}

	public String getPurposeOfVisit() {
		return purposeOfVisit;
	}

	public void setPurposeOfVisit(String purposeOfVisit) {
		this.purposeOfVisit = purposeOfVisit;
	}

	public Date getTravelStartDate() {
		return travelStartDate;
	}

	public void setTravelStartDate(Date travelStartDate) {
		this.travelStartDate = travelStartDate;
	}

	public Date getTravelEndDate() {
		return travelEndDate;
	}

	public void setTravelEndDate(Date travelEndDate) {
		this.travelEndDate = travelEndDate;
	}

	public BigDecimal getExpectedTravelBudget() {
		return expectedTravelBudget;
	}

	public void setExpectedTravelBudget(BigDecimal expectedTravelBudget) {
		this.expectedTravelBudget = expectedTravelBudget;
	}

	public BigDecimal getActualTravelBudget() {
		return actualTravelBudget;
	}

	public void setActualTravelBudget(BigDecimal actualTravelBudget) {
		this.actualTravelBudget = actualTravelBudget;
	}

	public String getTravelDescription() {
		return travelDescription;
	}

	public void setTravelDescription(String travelDescription) {
		this.travelDescription = travelDescription;
	}


	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<TravelDocuments> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<TravelDocuments> documents) {
		this.documents = documents;
	}

	public void setStatus(ForwardApplicationStatus status) {
		this.status = status;
	}

	public Set<TravelDestinations> getDestinations() {
		return destinations;
	}

	public ForwardApplicationStatus getStatus() {
		return status;
	}

	
}
