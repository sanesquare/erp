package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
@Entity
@Table(name = "language")
public class Language implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7858864157727187358L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@OneToMany(mappedBy = "language")
	private Set<CandidatesLanguage> candidatesLanguage;
	
	
	@OneToMany(mappedBy="language")
	private Set<EmployeeLanguage> employeeLanguages;

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the candidatesLanguage
	 */
	public Set<CandidatesLanguage> getCandidatesLanguage() {
		return candidatesLanguage;
	}

	/**
	 * @param candidatesLanguage the candidatesLanguage to set
	 */
	public void setCandidatesLanguage(Set<CandidatesLanguage> candidatesLanguage) {
		this.candidatesLanguage = candidatesLanguage;
	}

	/**
	 * @return the employeeLanguages
	 */
	public Set<EmployeeLanguage> getEmployeeLanguages() {
		return employeeLanguages;
	}

	/**
	 * @param employeeLanguages the employeeLanguages to set
	 */
	public void setEmployeeLanguages(Set<EmployeeLanguage> employeeLanguages) {
		this.employeeLanguages = employeeLanguages;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
