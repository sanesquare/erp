package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Vips
 *
 */
@Entity
@Table(name="notification")
public class Notification implements AuditEntity , Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 7567128263307157732L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "employee_notification", joinColumns = { @JoinColumn(name = "notification_id") }, inverseJoinColumns = { @JoinColumn(name = "employee_id") })
	private Set<Employee> employees = new HashSet<Employee>(1);
	
	@ManyToOne
	private NotificationType notificationType;
	
	@ManyToOne
	private MstReference mstReference;
	
	@Column(name = "s_to_s")
	private String sendToSelf;
	
	
	/**
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	

	/**
	 * @return the notificationType
	 */
	public NotificationType getNotificationType() {
		return notificationType;
	}

	/**
	 * @param notificationType the notificationType to set
	 */
	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * @return the mstReference
	 */
	public MstReference getMstReference() {
		return mstReference;
	}

	/**
	 * @param mstReference the mstReference to set
	 */
	public void setMstReference(MstReference mstReference) {
		this.mstReference = mstReference;
	}

	/**
	 * @return the sendToSelf
	 */
	public String getSendToSelf() {
		return sendToSelf;
	}

	/**
	 * @param sendToSelf the sendToSelf to set
	 */
	public void setSendToSelf(String sendToSelf) {
		this.sendToSelf = sendToSelf;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Set<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}	
}
