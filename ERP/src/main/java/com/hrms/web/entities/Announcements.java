package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shimil Babu
 * @since 10-March-2015
 *
 */
@Entity
@Table(name = "announcements")
public class Announcements implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7658850369370361429L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "announcement_title")
	private String title;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "message")
	private String message;

	@Column(name = "notes")
	private String notes;

	@Column(name = "status_notes")
	private String statusNotes;

	@Column(name = "send_email")
	private boolean sendEmail;

	@ManyToOne
	AnnouncementsStatus announcementsStatus;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "announcements")
	private Set<AnnouncementsDocuments> announcementsDocuments;

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * 
	 * @param startDate
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * 
	 * @return
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * 
	 * @param endDate
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * 
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * 
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * 
	 * @return
	 */
	public AnnouncementsStatus getAnnouncementsStatus() {
		return announcementsStatus;
	}

	/**
	 * 
	 * @param announcementsStatus
	 */
	public void setAnnouncementsStatus(AnnouncementsStatus announcementsStatus) {
		this.announcementsStatus = announcementsStatus;
	}


	/**
	 * @return the sendEmail
	 */
	public boolean isSendEmail() {
		return sendEmail;
	}

	/**
	 * @param sendEmail
	 *            the sendEmail to set
	 */
	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	/**
	 * @return the announcementsDocuments
	 */
	public Set<AnnouncementsDocuments> getAnnouncementsDocuments() {
		return announcementsDocuments;
	}

	/**
	 * @param announcementsDocuments
	 *            the announcementsDocuments to set
	 */
	public void setAnnouncementsDocuments(Set<AnnouncementsDocuments> announcementsDocuments) {
		this.announcementsDocuments = announcementsDocuments;
	}

	/**
	 * @return the statusNotes
	 */
	public String getStatusNotes() {
		return statusNotes;
	}

	/**
	 * @param statusNotes
	 *            the statusNotes to set
	 */
	public void setStatusNotes(String statusNotes) {
		this.statusNotes = statusNotes;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
