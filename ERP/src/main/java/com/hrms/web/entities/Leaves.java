package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 6-April-2015
 */
@Entity
@Table(name = "leaves")
public class Leaves implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7741766631150040273L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "reason")
	private String reason;
	
	@Column(name = "from_date")
	private Date fromDate;
	
	@Column(name = "to_date")
	private Date toDate;
	
	@Column(name = "notes" , length = 1000)
	private String notes;
	
	@Column(name = "description" , length = 1000)
	private String description;
	
	@Column(name = "alternate_contact_person")
	private String alternateContactPerson;
	
	@Column(name ="alternate_contact_number")
	private String alternateContactNumber;
	
	@ManyToOne
	@JoinColumn(name="type")
	private LeaveType leaveType;
	
	@Column(name = "status_description" , length = 1000)
	private String statusDescription;
	
	@ManyToOne 
	private Employee employee;
	
	@ManyToMany
	@JoinColumn(name = "forward_to")
	private Set<Employee> forwardApplicationTo;
	
	@OneToMany(mappedBy = "leaves" , cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	private Set<LeaveDocuments> documents;
	
	@ManyToOne
	private ForwardApplicationStatus leaveStatus;
	
	@Column(name = "duration")
	private Integer duration;
	
	@Column(name = "leaves_balance")
	private Long leavesBalance;
	
	@Column(name="leaves")
	private Long totalLeaves;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAlternateContactPerson() {
		return alternateContactPerson;
	}

	public void setAlternateContactPerson(String alternateContactPerson) {
		this.alternateContactPerson = alternateContactPerson;
	}

	public String getAlternateContactNumber() {
		return alternateContactNumber;
	}

	public void setAlternateContactNumber(String alternateContactNumber) {
		this.alternateContactNumber = alternateContactNumber;
	}

	public LeaveType getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	public Set<LeaveDocuments> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<LeaveDocuments> documents) {
		this.documents = documents;
	}

	public ForwardApplicationStatus getLeaveStatus() {
		return leaveStatus;
	}

	public void setLeaveStatus(ForwardApplicationStatus leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public Set<Employee> getForwardApplicationTo() {
		return forwardApplicationTo;
	}

	public void setForwardApplicationTo(Set<Employee> forwardApplicationTo) {
		this.forwardApplicationTo = forwardApplicationTo;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Long getLeavesBalance() {
		return leavesBalance;
	}

	public void setLeavesBalance(Long leavesBalance) {
		this.leavesBalance = leavesBalance;
	}

	public Long getTotalLeaves() {
		return totalLeaves;
	}

	public void setTotalLeaves(Long totalLeaves) {
		this.totalLeaves = totalLeaves;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
}
