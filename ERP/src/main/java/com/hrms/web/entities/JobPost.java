package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Vinutha
 * @since 18-March-2015
 *
 */
@Entity
@Table(name="job_post")
public class JobPost implements AuditEntity,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2799713435931030491L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name="positions")
	private int positions;
	
	@Column(name="ageStart")
	private int ageStart; 
	
	@Column(name="ageEnd")
	private int ageEnd;
	
	@Column(name="closingDate")
	private Date closingDate ;
	
	@Column(name="qualificationRequired")
	private String qualificationRequired;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "additionalInformation")
	private String additionalInformation;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Department department;
	
	@ManyToOne
	private JobType jobType;
	
	@Column(name="jobTitle")
	private String jobTitle;
	/*@ManyToOne
	private JobFields jobFields;*/
	
	@ManyToMany
	private Set<Branch> branch = new HashSet<Branch>();
	
	@OneToOne(mappedBy = "jobPost",cascade = CascadeType.ALL)
	private ExperienceRequired experienceRequired;
	
	@OneToMany(mappedBy = "jobPost",cascade = CascadeType.ALL)
	private Set<JobPostDocuments> jobPostDocuments;
	
	@Column(name="salaryStartRange")
	private double salaryStartRange;
	
	@Column(name="salaryEndRange")
	private double salaryEndRange;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the positions
	 */
	public int getPositions() {
		return positions;
	}

	/**
	 * @param positions the positions to set
	 */
	public void setPositions(int positions) {
		this.positions = positions;
	}

	/**
	 * @return the ageStart
	 */
	public int getAgeStart() {
		return ageStart;
	}

	/**
	 * @param ageStart the ageStart to set
	 */
	public void setAgeStart(int ageStart) {
		this.ageStart = ageStart;
	}

	/**
	 * @return the ageEnd
	 */
	public int getAgeEnd() {
		return ageEnd;
	}

	/**
	 * @param ageEnd the ageEnd to set
	 */
	public void setAgeEnd(int ageEnd) {
		this.ageEnd = ageEnd;
	}

	/**
	 * @return the closingDate
	 */
	public Date getClosingDate() {
		return closingDate;
	}

	/**
	 * @param closingDate the closingDate to set
	 */
	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	/**
	 * @return the qualificationRequired
	 */
	public String getQualificationRequired() {
		return qualificationRequired;
	}

	/**
	 * @param qualificationRequired the qualificationRequired to set
	 */
	public void setQualificationRequired(String qualificationRequired) {
		this.qualificationRequired = qualificationRequired;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the additionalInformation
	 */
	public String getAdditionalInformation() {
		return additionalInformation;
	}

	/**
	 * @param additionalInformation the additionalInformation to set
	 */
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	/**
	 * @return the department
	 */
	public Department getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(Department department) {
		this.department = department;
	}

	/**
	 * @return the jobType
	 */
	public JobType getJobType() {
		return jobType;
	}

	/**
	 * @param jobType the jobType to set
	 */
	public void setJobType(JobType jobType) {
		this.jobType = jobType;
	}

	/**
	 * @return the jobFields
	 *//*
	public JobFields getJobFields() {
		return jobFields;
	}

	*//**
	 * @param jobFields the jobFields to set
	 *//*
	public void setJobFields(JobFields jobFields) {
		this.jobFields = jobFields;
	}*/

	/**
	 * @return the experienceRequired
	 */
	public ExperienceRequired getExperienceRequired() {
		return experienceRequired;
	}

	/**
	 * @param experienceRequired the experienceRequired to set
	 */
	public void setExperienceRequired(ExperienceRequired experienceRequired) {
		this.experienceRequired = experienceRequired;
	}

	/**
	 * @return the jobPostDocuments
	 */
	public Set<JobPostDocuments> getJobPostDocuments() {
		return jobPostDocuments;
	}

	/**
	 * @param jobPostDocuments the jobPostDocuments to set
	 */
	public void setJobPostDocuments(Set<JobPostDocuments> jobPostDocuments) {
		this.jobPostDocuments = jobPostDocuments;
	}

	/**
	 * @return the salaryStartRange
	 */
	public double getSalaryStartRange() {
		return salaryStartRange;
	}

	/**
	 * @param salaryStartRange the salaryStartRange to set
	 */
	public void setSalaryStartRange(double salaryStartRange) {
		this.salaryStartRange = salaryStartRange;
	}

	/**
	 * @return the salaryEndRange
	 */
	public double getSalaryEndRange() {
		return salaryEndRange;
	}

	/**
	 * @param salaryEndRange the salaryEndRange to set
	 */
	public void setSalaryEndRange(double salaryEndRange) {
		this.salaryEndRange = salaryEndRange;
	}

	/**
	 * @return the jobTitle
	 */
	public String getJobTitle() {
		return jobTitle;
	}

	/**
	 * @param jobTitle the jobTitle to set
	 */
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	/**
	 * @return the branch
	 */
	public Set<Branch> getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(Set<Branch> branch) {
		this.branch = branch;
	}
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
}
