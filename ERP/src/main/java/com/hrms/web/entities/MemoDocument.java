package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Entity
@Table(name = "memo_documents")
public class MemoDocument implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1598479059342552717L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "docuemnt_name")
	private String name;

	@Column(name = "document_url")
	private String url;


	@ManyToOne(fetch = FetchType.LAZY)
	private Memo memo;

	/**
	 * 
	 * @return
	 */
	public Memo getMemo() {
		return memo;
	}

	/**
	 * 
	 * @param memo
	 */
	public void setMemo(Memo memo) {
		this.memo = memo;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
