package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Vinutha
 * @since 18-March-2015
 *
 */
@Entity
@Table(name="job_candidates")
public class JobCandidates implements AuditEntity,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4470594470860890258L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name="firstName")
	private String firstName;
	
	@Column(name="lastName")
	private String lastName;
	
	@Column(name="birthDate")
	private Date birthDate;
	
	@Column(name="gender")
	private String gender;
	
	@ManyToOne
	private Country country;
	
	@OneToOne(mappedBy = "jobCandidates",cascade=CascadeType.ALL)
	private CandidatesAddress address;
	
	/*@ManyToOne
	private JobFields jobFields;*/
	
	@ManyToOne
	private EmployeeDesignation designation;
	
	@ManyToOne
	private JobInterview jobInterview;
	
	@OneToMany(mappedBy = "jobCandidates" ,fetch= FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<CandidateDocuments> candidateDocuments;
	
	
	private String status ;
	
	@OneToOne(mappedBy = "jobCandidates",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private CandidateResume candidateResume;
	
	@OneToMany(mappedBy = "jobCandidates" ,fetch= FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<CandidatesReferences> references;
	
	@OneToMany(mappedBy = "jobCandidates" ,fetch= FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<CandidateWorkExperience> candidateWorkExperience;
	
	@OneToMany(mappedBy = "jobCandidates" ,fetch= FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<CandidateSkills> candidateSkills; 
	
	@OneToMany(mappedBy = "jobCandidates" ,fetch= FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<CandidatesQualification> candidateQualification;
	
	@OneToMany(mappedBy = "jobCandidates" ,fetch= FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<CandidatesLanguage> candidatesLanguage;
	
	@Column(name="interests")
	private String interests;

	@Column(name="achievements")
	private String achievements;
	
	@Column(name="additionalInfo")
	private String additionalInfo;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the birthDate
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the jobInterview
	 */
	public JobInterview getJobInterview() {
		return jobInterview;
	}

	/**
	 * @param jobInterview the jobInterview to set
	 */
	public void setJobInterview(JobInterview jobInterview) {
		this.jobInterview = jobInterview;
	}

	/**
	 * @return the candidateDocuments
	 */
	public Set<CandidateDocuments> getCandidateDocuments() {
		return candidateDocuments;
	}

	/**
	 * @param candidateDocuments the candidateDocuments to set
	 */
	public void setCandidateDocuments(Set<CandidateDocuments> candidateDocuments) {
		this.candidateDocuments = candidateDocuments;
	}

	/**
	 * @return the candidateResume
	 *//*
	public CandidateResume getCandidateResume() {
		return candidateResume;
	}

	*//**
	 * @param candidateResume the candidateResume to set
	 *//*
	public void setCandidateResume(CandidateResume candidateResume) {
		this.candidateResume = candidateResume;
	}*/

	/**
	 * @return the references
	 */
	public Set<CandidatesReferences> getReferences() {
		return references;
	}

	/**
	 * @param references the references to set
	 */
	public void setReferences(Set<CandidatesReferences> references) {
		this.references = references;
	}

	/**
	 * @return the candidateWorkExperience
	 */
	public Set<CandidateWorkExperience> getCandidateWorkExperience() {
		return candidateWorkExperience;
	}

	/**
	 * @param candidateWorkExperience the candidateWorkExperience to set
	 */
	public void setCandidateWorkExperience(Set<CandidateWorkExperience> candidateWorkExperience) {
		this.candidateWorkExperience = candidateWorkExperience;
	}

	/**
	 * @return the candidateSkills
	 */
	public Set<CandidateSkills> getCandidateSkills() {
		return candidateSkills;
	}

	/**
	 * @param candidateSkills the candidateSkills to set
	 */
	public void setCandidateSkills(Set<CandidateSkills> candidateSkills) {
		this.candidateSkills = candidateSkills;
	}

	/**
	 * @return the candidateQualification
	 */
	public Set<CandidatesQualification> getCandidateQualification() {
		return candidateQualification;
	}

	/**
	 * @param candidateQualification the candidateQualification to set
	 */
	public void setCandidateQualification(Set<CandidatesQualification> candidateQualification) {
		this.candidateQualification = candidateQualification;
	}

	/**
	 * @return the interests
	 */
	public String getInterests() {
		return interests;
	}

	/**
	 * @param interests the interests to set
	 */
	public void setInterests(String interests) {
		this.interests = interests;
	}

	/**
	 * @return the achievements
	 */
	public String getAchievements() {
		return achievements;
	}

	/**
	 * @param achievements the achievements to set
	 */
	public void setAchievements(String achievements) {
		this.achievements = achievements;
	}

	/**
	 * @return the address
	 */
	public CandidatesAddress getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(CandidatesAddress address) {
		this.address = address;
	}

	/**
	 * @return the jobFields
	 *//*
	public JobFields getJobFields() {
		return jobFields;
	}

	*//**
	 * @param jobFields the jobFields to set
	 *//*
	public void setJobFields(JobFields jobFields) {
		this.jobFields = jobFields;
	}*/

	/**
	 * @return the candidateResume
	 */
	public CandidateResume getCandidateResume() {
		return candidateResume;
	}

	/**
	 * @param candidateResume the candidateResume to set
	 */
	public void setCandidateResume(CandidateResume candidateResume) {
		this.candidateResume = candidateResume;
	}

	/**
	 * @return the candidatesLanguage
	 */
	public Set<CandidatesLanguage> getCandidatesLanguage() {
		return candidatesLanguage;
	}

	/**
	 * @param candidatesLanguage the candidatesLanguage to set
	 */
	public void setCandidatesLanguage(Set<CandidatesLanguage> candidatesLanguage) {
		this.candidatesLanguage = candidatesLanguage;
	}

	/**
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * @return the additionalInfo
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * @param additionalInfo the additionalInfo to set
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the designation
	 */
	public EmployeeDesignation getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(EmployeeDesignation designation) {
		this.designation = designation;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
