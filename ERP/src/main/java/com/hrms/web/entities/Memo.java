package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 22-March-2015
 *
 */
@Entity
@Table(name = "memos")
public class Memo implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4682741768252767350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	private Employee memoFrom;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "memo_forwards_to", joinColumns = { @JoinColumn(name = "memo_id") }, inverseJoinColumns = { @JoinColumn(name = "forwardTo_id") })
	private Set<Employee> forwader;

	@Column(name = "subject")
	private String subject;

	@Column(name = "memo_on")
	private Date memoOn;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "notes", length = 1000)
	private String notes;


	@OneToMany(fetch = FetchType.LAZY, mappedBy = "memo", cascade = CascadeType.ALL)
	private Set<MemoDocument> documents;

	/**
	 * 
	 * @return
	 */
	public Set<MemoDocument> getDocuments() {
		return documents;
	}

	/**
	 * 
	 * @param documents
	 */
	public void setDocuments(Set<MemoDocument> documents) {
		this.documents = documents;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * 
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * 
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * 
	 * @return
	 */
	public Date getMemoOn() {
		return memoOn;
	}

	/**
	 * 
	 * @param memoOn
	 */
	public void setMemoOn(Date memoOn) {
		this.memoOn = memoOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Employee getMemoFrom() {
		return memoFrom;
	}

	public void setMemoFrom(Employee memoFrom) {
		this.memoFrom = memoFrom;
	}

	public Set<Employee> getForwader() {
		return forwader;
	}

	public void setForwader(Set<Employee> forwader) {
		this.forwader = forwader;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
