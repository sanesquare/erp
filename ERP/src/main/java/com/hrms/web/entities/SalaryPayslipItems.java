package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author Shamsheer
 * @since 25-April-2015
 */
@Entity
@Table(name = "salary_payslip_items")
@AssociationOverrides({
	@AssociationOverride(name = "salaryPayslipItemsId.salaryMonthly " , joinColumns = @JoinColumn(name = "salary_id")),
	@AssociationOverride(name = "salaryPayslipItemsId.paySlipItem" , joinColumns = @JoinColumn(name = "payslip_item_id"))
})
public class SalaryPayslipItems implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5709445310188565748L;

	@EmbeddedId
	private SalaryPayslipItemsId salaryPayslipItemsId = new SalaryPayslipItemsId();
	
	@Column(name = "amount")
	private BigDecimal amount;
	
	@Transient
	public SalaryMonthly getSalaryMonthly(){
		return this.salaryPayslipItemsId.getSalaryMonthly();
	}
	public void setSalaryMonthly(SalaryMonthly salaryMonthly){
		this.salaryPayslipItemsId.setSalaryMonthly(salaryMonthly);
	}
	
	@Transient
	public PaySlipItem getPaySlipItem(){
		return this.salaryPayslipItemsId.getPaySlipItem();
	}
	public void setPayslipItem(PaySlipItem paySlipItem){
		this.salaryPayslipItemsId.setPaySlipItem(paySlipItem);
	}

	public SalaryPayslipItemsId getSalaryPayslipItemsId() {
		return salaryPayslipItemsId;
	}

	public void setSalaryPayslipItemsId(SalaryPayslipItemsId salaryPayslipItemsId) {
		this.salaryPayslipItemsId = salaryPayslipItemsId;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
