package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 10-April-2015
 */
@Entity
@Table( name = "hourly_wages")
public class HourlyWages implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6748060238820012861L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@Column(name = "description" , length = 1000)
	private String description;
	
	@Column(name = "notes" , length = 1000)
	private String notes;
	
	@Column(name = "regular_hour_amount")
	private BigDecimal regularHourAmount;
	
	@Column(name = "overtime_hour_amount")
	private BigDecimal overTimeHourAmount;
	
	@OneToOne
	private Employee employee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public BigDecimal getRegularHourAmount() {
		return regularHourAmount;
	}

	public void setRegularHourAmount(BigDecimal regularHourAmount) {
		this.regularHourAmount = regularHourAmount;
	}

	public BigDecimal getOverTimeHourAmount() {
		return overTimeHourAmount;
	}

	public void setOverTimeHourAmount(BigDecimal overTimeHourAmount) {
		this.overTimeHourAmount = overTimeHourAmount;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
