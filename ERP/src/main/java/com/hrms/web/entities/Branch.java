package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Vinutha
 * @since 10-03-2015
 *
 */
@Entity
@Table(name = "branch")
public class Branch implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 208363343147083115L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "branchName")
	private String branchName;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "branch")
	private Set<Holiday> holiday;

	@Column(name = "additionalInformation")
	private String additionalInformation;

	@Column(name = "start_date")
	private Date startDate;

	@ManyToMany(mappedBy = "branch")
	private Set<OrganizationPolicy> organizationPolicy;

	@ManyToOne
	private Branch parentBranch;

	@OneToMany(mappedBy = "parentBranch")
	private Set<Branch> branches = new HashSet<Branch>();

	@ManyToMany(mappedBy = "branch")
	private Set<PerformanceEvaluation> performanceEvaluation;

	@ManyToOne
	private BranchType branchType;

	@ManyToOne
	private TimeZone timeZone;

	@ManyToOne
	private BaseCurrency currency;

	@OneToOne(mappedBy = "branch", cascade = CascadeType.ALL)
	private BranchAddress addressField;

	@OneToOne(mappedBy = "branch", cascade = CascadeType.ALL)
	private GeographicalLocation geographicalLocation;

	@OneToMany(mappedBy = "branch", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<BranchDocuments> branchDocuments = new HashSet<BranchDocuments>();

	@ManyToMany
	private Set<JobPost> jobPost = new HashSet<JobPost>();

	@OneToMany(mappedBy = "transferToBranch", fetch = FetchType.EAGER)
	private Set<Transfer> transferToBranchList;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "meetingEmployeeId.branch")
	private Set<MeetingEmployee> meetingEmployees = new HashSet<MeetingEmployee>(0);

	@OneToMany(mappedBy = "branch", fetch = FetchType.LAZY)
	private Set<Employee> employees;

	@OneToMany(mappedBy = "transferFromBranch", fetch = FetchType.LAZY)
	private Set<Transfer> transferFromBranchList;

	@ManyToMany(mappedBy = "branches")
	private Set<PaySalary> paySalaries;

	@OneToMany(mappedBy = "parent", orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<Company> companies = new HashSet<Company>();

	@ManyToMany
	private Set<Employee> users = new HashSet<Employee>(); 
	
	/**
	 * 
	 * @return
	 */
	public Set<OrganizationPolicy> getOrganizationPolicy() {
		return organizationPolicy;
	}

	/**
	 * 
	 * @param organizationPolicy
	 */
	public void setOrganizationPolicy(Set<OrganizationPolicy> organizationPolicy) {
		this.organizationPolicy = organizationPolicy;
	}

	/**
	 * 
	 * @return
	 */
	public Set<PerformanceEvaluation> getPerformanceEvaluation() {
		return performanceEvaluation;
	}

	/**
	 * 
	 * @param performanceEvaluation
	 */
	public void setPerformanceEvaluation(Set<PerformanceEvaluation> performanceEvaluation) {
		this.performanceEvaluation = performanceEvaluation;
	}

	/**
	 * 
	 * @return
	 */
	public Set<Holiday> getHoliday() {
		return holiday;
	}

	/**
	 * 
	 * @param holiday
	 */
	public void setHoliday(Set<Holiday> holiday) {
		this.holiday = holiday;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * @param branchName
	 *            the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * @return the additionalInformation
	 */
	public String getAdditionalInformation() {
		return additionalInformation;
	}

	/**
	 * @param additionalInformation
	 *            the additionalInformation to set
	 */
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	/**
	 * @return the parentBranch
	 */
	public Branch getParentBranch() {
		return parentBranch;
	}

	/**
	 * @param parentBranch
	 *            the parentBranch to set
	 */
	public void setParentBranch(Branch parentBranch) {
		this.parentBranch = parentBranch;
	}

	/**
	 * @return the branches
	 */
	public Set<Branch> getBranches() {
		return branches;
	}

	/**
	 * @param branches
	 *            the branches to set
	 */
	public void setBranches(Set<Branch> branches) {
		this.branches = branches;
	}

	/**
	 * @return the branchType
	 */
	public BranchType getBranchType() {
		return branchType;
	}

	/**
	 * @param branchType
	 *            the branchType to set
	 */
	public void setBranchType(BranchType branchType) {
		this.branchType = branchType;
	}

	/**
	 * @return the geographicalLocation
	 */
	public GeographicalLocation getGeographicalLocation() {
		return geographicalLocation;
	}

	/**
	 * @param geographicalLocation
	 *            the geographicalLocation to set
	 */
	public void setGeographicalLocation(GeographicalLocation geographicalLocation) {
		this.geographicalLocation = geographicalLocation;
	}

	/**
	 * @return the branchDocuments
	 */
	public Set<BranchDocuments> getBranchDocuments() {
		return branchDocuments;
	}

	/**
	 * @param branchDocuments
	 *            the branchDocuments to set
	 */
	public void setBranchDocuments(Set<BranchDocuments> branchDocuments) {
		this.branchDocuments = branchDocuments;
	}

	/**
	 * @return the currency
	 */
	public BaseCurrency getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(BaseCurrency currency) {
		this.currency = currency;
	}

	/**
	 * @return the timeZone
	 */
	public TimeZone getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone
	 *            the timeZone to set
	 */
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return the addressField
	 */
	public BranchAddress getAddressField() {
		return addressField;
	}

	/**
	 * @param addressField
	 *            the addressField to set
	 */
	public void setAddressField(BranchAddress addressField) {
		this.addressField = addressField;
	}

	public Set<MeetingEmployee> getMeetingEmployees() {
		return meetingEmployees;
	}

	public void setMeetingEmployees(Set<MeetingEmployee> meetingEmployees) {
		this.meetingEmployees = meetingEmployees;
	}

	public Set<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}

	public Set<Transfer> getTransferToBranchList() {
		return transferToBranchList;
	}

	public void setTransferToBranchList(Set<Transfer> transferToBranchList) {
		this.transferToBranchList = transferToBranchList;
	}

	public Set<Transfer> getTransferFromBranchList() {
		return transferFromBranchList;
	}

	public void setTransferFromBranchList(Set<Transfer> transferFromBranchList) {
		this.transferFromBranchList = transferFromBranchList;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the jobPost
	 */
	public Set<JobPost> getJobPost() {
		return jobPost;
	}

	/**
	 * @param jobPost
	 *            the jobPost to set
	 */
	public void setJobPost(Set<JobPost> jobPost) {
		this.jobPost = jobPost;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<PaySalary> getPaySalaries() {
		return paySalaries;
	}

	public void setPaySalaries(Set<PaySalary> paySalaries) {
		this.paySalaries = paySalaries;
	}

	public Set<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(Set<Company> companies) {
		this.companies = companies;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Set<Employee> getUsers() {
		return users;
	}

	public void setUsers(Set<Employee> users) {
		this.users = users;
	}

}
