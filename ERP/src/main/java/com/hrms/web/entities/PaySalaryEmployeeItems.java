package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 05-June-2015
 */
@Entity
@Table(name = "pay_salary_employee_items")
public class PaySalaryEmployeeItems implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2267113608060912171L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@OneToOne
	private PaySalaryOtherAllowanceItems item;
	
	@Column(name = "item_amount")
	private BigDecimal amount;
	
	@ManyToOne
	private PaySalaryEmployees paySalaryEmployees;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public PaySalaryEmployees getPaySalaryEmployees() {
		return paySalaryEmployees;
	}

	public void setPaySalaryEmployees(PaySalaryEmployees paySalaryEmployees) {
		this.paySalaryEmployees = paySalaryEmployees;
	}

	public PaySalaryOtherAllowanceItems getItem() {
		return item;
	}

	public void setItem(PaySalaryOtherAllowanceItems item) {
		this.item = item;
	}
}
