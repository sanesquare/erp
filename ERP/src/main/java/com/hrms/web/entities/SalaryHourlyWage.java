package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 22-April-2015
 */
@Entity
@Table(name = "salary_hourly_wage")
public class SalaryHourlyWage implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2801001879590698224L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name = "regular_hour_salary")
	private BigDecimal regularHourSalary;
	
	@Column(name = "overtime_hour_salary")
	private BigDecimal overTimeHourSalary;
	
	@Column(name = "with_effect_from")
	private Date withEffectFrom;
	
	@Column(name = "tax")
	private Double tax;
	
	@ManyToOne
	private Employee employee;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getRegularHourSalary() {
		return regularHourSalary;
	}

	public void setRegularHourSalary(BigDecimal regularHourSalary) {
		this.regularHourSalary = regularHourSalary;
	}

	public BigDecimal getOverTimeHourSalary() {
		return overTimeHourSalary;
	}

	public void setOverTimeHourSalary(BigDecimal overTimeHourSalary) {
		this.overTimeHourSalary = overTimeHourSalary;
	}

	public Date getWithEffectFrom() {
		return withEffectFrom;
	}

	public void setWithEffectFrom(Date withEffectFrom) {
		this.withEffectFrom = withEffectFrom;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
}
