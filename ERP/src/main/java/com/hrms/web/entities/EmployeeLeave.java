package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 27-March-2015
 */
@Entity
@Table(name="employee_leave")
public class EmployeeLeave implements AuditEntity,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2724617284074781883L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name="type")
	private String type;
	
	@Column(name="leaves")
	private Long leaves;
	
	@Column(name="carry_over")
	private String carryOver;
	
	@Column(name="status")
	private String status;
	
	@ManyToOne
	@JoinColumn(name="title")
	private LeaveType title;
	
	@ManyToOne
	private Employee employee;
	
	@Column(name = "leaves_taken")
	private Long leavesTaken;
	
	@Column(name = "leaves_balance")
	private Long leavesBalance;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getLeaves() {
		return leaves;
	}

	public void setLeaves(Long leaves) {
		this.leaves = leaves;
	}

	public String getCarryOver() {
		return carryOver;
	}

	public void setCarryOver(String carryOver) {
		this.carryOver = carryOver;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LeaveType getTitle() {
		return title;
	}

	public void setTitle(LeaveType title) {
		this.title = title;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Long getLeavesTaken() {
		return leavesTaken;
	}

	public void setLeavesTaken(Long leavesTaken) {
		this.leavesTaken = leavesTaken;
	}

	public Long getLeavesBalance() {
		return leavesBalance;
	}

	public void setLeavesBalance(Long leavesBalance) {
		this.leavesBalance = leavesBalance;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
