package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;


@Entity
@Table(name="complaints")
public class Complaints implements AuditEntity, Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Employee complaintsFrom;
	
		
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "complaint_forward_to", joinColumns = { @JoinColumn(name = "complaint_id") }, inverseJoinColumns = { @JoinColumn(name = "employee_id") })
	private Set<Employee> forwardTo;
	
	@ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinTable(name="complaint_aganist",joinColumns = { @JoinColumn(name = "complaint_id")},inverseJoinColumns = { @JoinColumn(name = "employee_id") })
	private Set<Employee> complaintsAganist;
	
	@Column(name="complaints_description")
	private String complaintsDescription;
	
	@Column(name="status_description")
	private String statusDescription;
	
	@Column(name="note")
	private String note;
	
	@Column(name="complaints_date")
	Date complaintDate;
	
	@OneToOne
	ComplaintStatus complaintStatus;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "complaints", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<ComplaintDocuments> complaintDocuments = new HashSet<ComplaintDocuments>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Employee getComplaintsFrom() {
		return complaintsFrom;
	}

	public void setComplaintsFrom(Employee complaintsFrom) {
		this.complaintsFrom = complaintsFrom;
	}

	public Set<Employee> getForwardTo() {
		return forwardTo;
	}

	public void setForwardTo(Set<Employee> forwardTo) {
		this.forwardTo = forwardTo;
	}

	public Set<Employee> getComplaintsAganist() {
		return complaintsAganist;
	}

	public void setComplaintsAganist(Set<Employee> complaintsAganist) {
		this.complaintsAganist = complaintsAganist;
	}

	public String getComplaintsDescription() {
		return complaintsDescription;
	}

	public void setComplaintsDescription(String complaintsDescription) {
		this.complaintsDescription = complaintsDescription;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getComplaintDate() {
		return complaintDate;
	}

	public void setComplaintDate(Date promotionDate) {
		this.complaintDate = promotionDate;
	}

	public ComplaintStatus getComplaintStatus() {
		return complaintStatus;
	}

	public void setComplaintStatus(ComplaintStatus complaintStatus) {
		this.complaintStatus = complaintStatus;
	}

	public Set<ComplaintDocuments> getComplaintDocuments() {
		return complaintDocuments;
	}

	public void setComplaintDocuments(Set<ComplaintDocuments> complaintDocuments) {
		this.complaintDocuments = complaintDocuments;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
}
