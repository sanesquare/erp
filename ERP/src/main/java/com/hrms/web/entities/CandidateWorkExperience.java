package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Vinutha
 * @since 18-March-2015
 *
 */
@Entity
@Table(name="candidate_workExperience")
public class CandidateWorkExperience implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8679304371842533051L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name = "organisationName")
	private String organisationName;
	
	@Column(name = "designation")
	private String designation;
	
	@ManyToOne
	private JobFields jobFields;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "startDate")
	private Date startDate;
	
	@Column(name = "endDate")
	private Date endDate;
	
	@Column(name = "startingSalary")
	private double startingSalary;
	
	@Column(name = "endingSalary")
	private double endingSalary;
	
	@ManyToOne
	private JobCandidates jobCandidates;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the organisationName
	 */
	public String getOrganisationName() {
		return organisationName;
	}

	/**
	 * @param organisationName the organisationName to set
	 */
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the jobFields
	 */
	public JobFields getJobFields() {
		return jobFields;
	}

	/**
	 * @param jobFields the jobFields to set
	 */
	public void setJobFields(JobFields jobFields) {
		this.jobFields = jobFields;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the startingSalary
	 */
	public double getStartingSalary() {
		return startingSalary;
	}

	/**
	 * @param startingSalary the startingSalary to set
	 */
	public void setStartingSalary(double startingSalary) {
		this.startingSalary = startingSalary;
	}

	/**
	 * @return the endingSalary
	 */
	public double getEndingSalary() {
		return endingSalary;
	}

	/**
	 * @param endingSalary the endingSalary to set
	 */
	public void setEndingSalary(double endingSalary) {
		this.endingSalary = endingSalary;
	}

	/**
	 * @return the jobCandidates
	 */
	public JobCandidates getJobCandidates() {
		return jobCandidates;
	}

	/**
	 * @param jobCandidates the jobCandidates to set
	 */
	public void setJobCandidates(JobCandidates jobCandidates) {
		this.jobCandidates = jobCandidates;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
