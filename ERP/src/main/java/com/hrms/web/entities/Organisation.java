package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
@Entity
@Table(name = "organisation")
public class Organisation implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6523200784690211765L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "code")
	private String code;

	@Column(name = "urt")
	private String urt;

	@Column(name = "name")
	private String name;

	@Column(name = "year")
	private String year;

	@Column(name = "fiscal_month")
	private String fiscalMonth;

	@Column(name = "fiscal_date")
	private String fiscalDate;


	@OneToOne(fetch = FetchType.LAZY, mappedBy = "organisation", cascade = CascadeType.ALL)
	private OrganisationContactPerson contactPerson;

	/**
	 * 
	 * @return
	 */
	public OrganisationContactPerson getContactPerson() {
		return contactPerson;
	}

	/**
	 * 
	 * @param contactPerson
	 */
	public void setContactPerson(OrganisationContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 
	 * @return
	 */
	public String getUrt() {
		return urt;
	}

	/**
	 * 
	 * @param urt
	 */
	public void setUrt(String urt) {
		this.urt = urt;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 */
	public String getYear() {
		return year;
	}

	/**
	 * 
	 * @param year
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * 
	 * @return
	 */
	public String getFiscalMonth() {
		return fiscalMonth;
	}

	/**
	 * 
	 * @param fiscalMonth
	 */
	public void setFiscalMonth(String fiscalMonth) {
		this.fiscalMonth = fiscalMonth;
	}

	/**
	 * 
	 * @return
	 */
	public String getFiscalDate() {
		return fiscalDate;
	}

	/**
	 * 
	 * @param fiscalDate
	 */
	public void setFiscalDate(String fiscalDate) {
		this.fiscalDate = fiscalDate;
	}

}
