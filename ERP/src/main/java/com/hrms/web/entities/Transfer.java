package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Sangeeth
 * @since 25-March-2015
 *
 */
@Entity
@Table(name = "transfer")
public class Transfer implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7889320110144227800L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Employee employee;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Employee forwardApplicationTo;
	
	@Column(name = "transfer_date")
	private Date transferDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Department transferToDepartment;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Branch transferToBranch;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Department transferFromDepartment;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Branch transferFromBranch;
	
	@Column(name = "description", length = 1000)
	private String description;
	
	@OneToOne(fetch = FetchType.EAGER)
	private ForwardApplicationStatus status;
	
	@Column(name = "status_description", length = 1000)
	private String statusDescription;
	
	@Column(name = "notes" , length = 1000)
	private String notes;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "transfer", cascade = CascadeType.ALL)
	private Set<TransferDocuments> documents;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Employee getForwardApplicationTo() {
		return forwardApplicationTo;
	}

	public void setForwardApplicationTo(Employee forwardApplicationTo) {
		this.forwardApplicationTo = forwardApplicationTo;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public Department getTransferToDepartment() {
		return transferToDepartment;
	}

	public void setTransferToDepartment(Department transferToDepartment) {
		this.transferToDepartment = transferToDepartment;
	}

	public Branch getTransferToBranch() {
		return transferToBranch;
	}

	public void setTransferToBranch(Branch transferToBranch) {
		this.transferToBranch = transferToBranch;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ForwardApplicationStatus getStatus() {
		return status;
	}

	public void setStatus(ForwardApplicationStatus status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Set<TransferDocuments> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<TransferDocuments> documents) {
		this.documents = documents;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Department getTransferFromDepartment() {
		return transferFromDepartment;
	}

	public void setTransferFromDepartment(Department transferFromDepartment) {
		this.transferFromDepartment = transferFromDepartment;
	}

	public Branch getTransferFromBranch() {
		return transferFromBranch;
	}

	public void setTransferFromBranch(Branch transferFromBranch) {
		this.transferFromBranch = transferFromBranch;
	}

	

}
