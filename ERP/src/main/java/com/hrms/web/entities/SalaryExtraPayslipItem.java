package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 
 * @author Shamsheer
 *
 */
@Entity
@Table(name = "salary_extra_payslip_items")
@AssociationOverrides({
	@AssociationOverride(name = "salaryExtraPaySlipItemsId.salaryMonthly" , joinColumns = @JoinColumn(name = "salary_id")),
	@AssociationOverride(name = "salaryExtraPaySlipItemsId.extraPaySlipItem" , joinColumns = @JoinColumn(name = "extra_payslip_id"))
})
public class SalaryExtraPayslipItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8867358618871327865L;

	@EmbeddedId
	private SalaryExtraPaySlipItemsId salaryExtraPaySlipItemsId = new SalaryExtraPaySlipItemsId();
	
	@Column(name = "amount")
	private BigDecimal amount;

	public SalaryExtraPaySlipItemsId getSalaryExtraPaySlipItemsId() {
		return salaryExtraPaySlipItemsId;
	}

	public void setSalaryExtraPaySlipItemsId(SalaryExtraPaySlipItemsId salaryExtraPaySlipItemsId) {
		this.salaryExtraPaySlipItemsId = salaryExtraPaySlipItemsId;
	}
	
	@Transient
	public SalaryMonthly getSalaryMonthly(){
		return this.salaryExtraPaySlipItemsId.getSalaryMonthly();
	}
	
	public void setSalaryMonthly(SalaryMonthly salaryMonthly){
		this.salaryExtraPaySlipItemsId.setSalaryMonthly(salaryMonthly);
	}
	
	@Transient
	public ExtraPaySlipItem getExtraPaySlipItem(){
		return this.salaryExtraPaySlipItemsId.getExtraPaySlipItem();
	}
	
	public void setExtraPayslipItem(ExtraPaySlipItem extraPaySlipItem){
		this.salaryExtraPaySlipItemsId.setExtraPaySlipItem(extraPaySlipItem);
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
