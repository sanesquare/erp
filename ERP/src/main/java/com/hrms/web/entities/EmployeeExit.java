package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 18-March-2015
 *
 */
@Entity
@Table(name = "employee_exits")
public class EmployeeExit implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	private Employee employee;

	@Column(name = "exit_date")
	private Date exitDate;

	@ManyToOne(fetch = FetchType.LAZY)
	private EmployeeExitType exitType;

	@Column(name = "exit_interview")
	private String exitInterview;

	@Column(name = "exit_reason", length = 1000)
	private String reason;

	@Column(name = "additioal_information", length = 1000)
	private String additionalInformation;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "employeeExit", cascade = CascadeType.ALL)
	private Set<EmployeeExitDocuments> employeeExitDocuments;

	/**
	 * 
	 * @return
	 */
	public Set<EmployeeExitDocuments> getEmployeeExitDocuments() {
		return employeeExitDocuments;
	}

	/**
	 * 
	 * @param employeeExitDocuments
	 */
	public void setEmployeeExitDocuments(Set<EmployeeExitDocuments> employeeExitDocuments) {
		this.employeeExitDocuments = employeeExitDocuments;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * 
	 * @param employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * 
	 * @return
	 */
	public Date getExitDate() {
		return exitDate;
	}

	/**
	 * 
	 * @param exitDate
	 */
	public void setExitDate(Date exitDate) {
		this.exitDate = exitDate;
	}

	/**
	 * 
	 * @return
	 */
	public EmployeeExitType getExitType() {
		return exitType;
	}

	/**
	 * 
	 * @param exitType
	 */
	public void setExitType(EmployeeExitType exitType) {
		this.exitType = exitType;
	}

	/**
	 * 
	 * @return
	 */
	public String getExitInterview() {
		return exitInterview;
	}

	/**
	 * 
	 * @param exitInterview
	 */
	public void setExitInterview(String exitInterview) {
		this.exitInterview = exitInterview;
	}

	/**
	 * 
	 * @return
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * 
	 * @param reason
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 
	 * @return
	 */
	public String getAdditionalInformation() {
		return additionalInformation;
	}

	/**
	 * 
	 * @param additionalInformation
	 */
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
