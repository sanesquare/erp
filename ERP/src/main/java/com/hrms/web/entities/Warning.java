package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 20-March-2015
 *
 */
@Entity
@Table(name = "warnings")
public class Warning implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8220580608181893833L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	private Employee warningTo;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "warning_givenby", joinColumns = { @JoinColumn(name = "warning_id") }, inverseJoinColumns = { @JoinColumn(name = "warningby_id") })
	private Set<Employee> forwader;

	@Column(name = "warning_on")
	private Date warningOn;

	@Column(name = "subject")
	private String subject;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "notes", length = 1000)
	private String notes;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "warning", cascade = CascadeType.ALL)
	private Set<WarningDocument> warningDocuments;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * 
	 * @return
	 */
	public Set<WarningDocument> getWarningDocuments() {
		return warningDocuments;
	}

	/**
	 * 
	 * @param warningDocuments
	 */
	public void setWarningDocuments(Set<WarningDocument> warningDocuments) {
		this.warningDocuments = warningDocuments;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * 
	 * @return
	 */
	public Date getWarningOn() {
		return warningOn;
	}

	/**
	 * 
	 * @param warningOn
	 */
	public void setWarningOn(Date warningOn) {
		this.warningOn = warningOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * 
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Employee getWarningTo() {
		return warningTo;
	}

	public void setWarningTo(Employee warningTo) {
		this.warningTo = warningTo;
	}

	public Set<Employee> getForwader() {
		return forwader;
	}

	public void setForwader(Set<Employee> forwader) {
		this.forwader = forwader;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
