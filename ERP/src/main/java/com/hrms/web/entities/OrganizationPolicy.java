package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Jithin Mohan
 * @since 15-May-2015
 *
 */
@Entity
@Table(name = "organization_policy")
public class OrganizationPolicy implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "policy_title")
	private String title;

	@ManyToOne(fetch = FetchType.LAZY)
	private PolicyType policyType;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "organization_policy_branch", joinColumns = { @JoinColumn(name = "organization_policy_branch") }, inverseJoinColumns = { @JoinColumn(name = "branch_id") })
	private Set<Branch> branch;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "organization_policy_department", joinColumns = { @JoinColumn(name = "organization_policy_id") }, inverseJoinColumns = { @JoinColumn(name = "department_id") })
	private Set<Department> department;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "organizationPolicy", cascade = CascadeType.ALL)
	private Set<OrganizationPolicyDocument> documents;

	@Column(name = "description")
	private String description;

	@Column(name = "information")
	private String information;


	/**
	 * 
	 * @return
	 */
	public Set<OrganizationPolicyDocument> getDocuments() {
		return documents;
	}

	/**
	 * 
	 * @param documents
	 */
	public void setDocuments(Set<OrganizationPolicyDocument> documents) {
		this.documents = documents;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return
	 */
	public PolicyType getPolicyType() {
		return policyType;
	}

	/**
	 * 
	 * @param policyType
	 */
	public void setPolicyType(PolicyType policyType) {
		this.policyType = policyType;
	}

	/**
	 * 
	 * @return
	 */
	public Set<Branch> getBranch() {
		return branch;
	}

	/**
	 * 
	 * @param branch
	 */
	public void setBranch(Set<Branch> branch) {
		this.branch = branch;
	}

	/**
	 * 
	 * @return
	 */
	public Set<Department> getDepartment() {
		return department;
	}

	/**
	 * 
	 * @param department
	 */
	public void setDepartment(Set<Department> department) {
		this.department = department;
	}

	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getInformation() {
		return information;
	}

	/**
	 * 
	 * @param information
	 */
	public void setInformation(String information) {
		this.information = information;
	}

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
