package com.hrms.web.entities;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 7-April-2015
 */
@Entity
@Table(name = "attendance")
public class Attendance implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9129018947966383994L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	private Employee employee;
	
	@Column(name = "attendance_date")
	private Date date ;
	
	@Column(name = "sign_in_time")
	private Time signIn;
	
	@Column(name = "sign_out_time")
	private Time signOut;
	
	@Column(name = "notes" , length = 1000)
	private String notes;
	
	@Column(name="status")
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the signIn
	 */
	public Time getSignIn() {
		return signIn;
	}

	/**
	 * @param signIn the signIn to set
	 */
	public void setSignIn(Time signIn) {
		this.signIn = signIn;
	}

	/**
	 * @return the signOut
	 */
	public Time getSignOut() {
		return signOut;
	}

	/**
	 * @param signOut the signOut to set
	 */
	public void setSignOut(Time signOut) {
		this.signOut = signOut;
	}
	
	@Override
	public int hashCode() {
		int hashCode = 0;
		hashCode = (int)signIn.getTime();
		hashCode += signOut.getTime();
		hashCode+=employee.hashCode();
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Attendance){
			Attendance attendance = (Attendance) obj;
			return (attendance.employee.equals(this.employee)&&attendance.signIn.equals(this.signIn)&&attendance.signOut.equals(this.signOut));
		}else
		return false;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
