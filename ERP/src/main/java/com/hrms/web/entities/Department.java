package com.hrms.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hrms.web.interfaces.AuditEntity;

//import com.sun.org.glassfish.gmbal.ManagedAttribute;
/**
 * 
 * @author Sangeeth and Bibin
 *
 */
@Entity
@Table(name = "department")
public class Department implements AuditEntity, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Branch branch;

	@Column(name = "name")
	private String name;

	@OneToMany(mappedBy = "departmentEmployees", orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Employee> employee;

	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_department_id")
	private Department parentDepartment;

	@OneToMany(mappedBy = "parentDepartment", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Department> subDepartments = new HashSet<Department>();

	@Column(name = "sorting_order")
	private Integer sortingOrder;

	@Column(name = "notes")
	private String additionalInformation;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "department", cascade = CascadeType.ALL, orphanRemoval = true)
	DepartmentHead departmentHead;

	@ManyToMany(mappedBy = "department")
	private Set<OrganizationPolicy> organizationPolicy;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "department", cascade = CascadeType.ALL)
	private Set<DepartmentDocuments> departmentDocuments = new HashSet<DepartmentDocuments>();

	@OneToMany(mappedBy = "transferToDepartment", fetch = FetchType.EAGER)
	private Set<Transfer> transferToDepartmentList;

	@OneToMany(mappedBy = "department")
	private Set<JobPost> jobPosts = new HashSet<JobPost>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "meetingEmployeeId.department")
	private Set<MeetingEmployee> meetingEmployees;

	@OneToMany(mappedBy = "transferFromDepartment", fetch = FetchType.LAZY)
	private Set<Transfer> transferFromDepartmentList;

	@ManyToMany(mappedBy = "departments")
	private Set<PaySalary> paySalaries;
	
	/**
	 * 
	 * @return
	 */
	public Set<OrganizationPolicy> getOrganizationPolicy() {
		return organizationPolicy;
	}

	/**
	 * 
	 * @param organizationPolicy
	 */
	public void setOrganizationPolicy(Set<OrganizationPolicy> organizationPolicy) {
		this.organizationPolicy = organizationPolicy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(Set<Employee> employee) {
		this.employee = employee;
	}

	public Department getParentDepartment() {
		return parentDepartment;
	}

	public void setParentDepartment(Department parentDepartment) {
		this.parentDepartment = parentDepartment;
	}

	public Set<Department> getSubDepartments() {
		return subDepartments;
	}

	public void setSubDepartments(Set<Department> subDepartments) {
		this.subDepartments = subDepartments;
	}

	public Integer getSortingOrder() {
		return sortingOrder;
	}

	public void setSortingOrder(Integer sortingOrder) {
		this.sortingOrder = sortingOrder;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public Set<DepartmentDocuments> getDepartmentDocuments() {
		return departmentDocuments;
	}

	public void setDepartmentDocuments(Set<DepartmentDocuments> departmentDocuments) {
		this.departmentDocuments = departmentDocuments;
	}

	public DepartmentHead getDepartmentHead() {
		return departmentHead;
	}

	public void setDepartmentHead(DepartmentHead departmentHead) {
		this.departmentHead = departmentHead;
	}

	public Set<Transfer> getTransferToDepartmentList() {
		return transferToDepartmentList;
	}

	public void setTransferToDepartmentList(Set<Transfer> transferToDepartmentList) {
		this.transferToDepartmentList = transferToDepartmentList;
	}

	/**
	 * @return the jobPosts
	 */
	public Set<JobPost> getJobPosts() {
		return jobPosts;
	}

	/**
	 * @param jobPosts
	 *            the jobPosts to set
	 */
	public void setJobPosts(Set<JobPost> jobPosts) {
		this.jobPosts = jobPosts;
	}

	public Set<MeetingEmployee> getMeetingEmployees() {
		return meetingEmployees;
	}

	public void setMeetingEmployees(Set<MeetingEmployee> meetingEmployees) {
		this.meetingEmployees = meetingEmployees;
	}

	public Set<Transfer> getTransferFromDepartmentList() {
		return transferFromDepartmentList;
	}

	public void setTransferFromDepartmentList(Set<Transfer> transferFromDepartmentList) {
		this.transferFromDepartmentList = transferFromDepartmentList;
	}

	public Set<PaySalary> getPaySalaries() {
		return paySalaries;
	}

	public void setPaySalaries(Set<PaySalary> paySalaries) {
		this.paySalaries = paySalaries;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
