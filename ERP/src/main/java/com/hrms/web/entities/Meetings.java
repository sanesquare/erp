package com.hrms.web.entities;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;






import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 12-March-2015
 *
 */
@Entity
@Table(name="meetings")
public class Meetings implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name="title")
	private String title;
	
	@Column(name="sendEmail")
	private boolean sendEmail;
	
	@Column(name="send_notification")
	private boolean sendNotification;
	
	@Column(name="startdate")
	private Date meetingStartDate;
	
	@Column(name="enddate")
	private Date meetingEndDate;
	
	@Column(name="time")
	private Time time;

	@Column(name="venue")
	private String venue;
	
	@Column(name="agenda" , length = 1000)
	private String agenda;
	
	@Column(name="notes" , length = 1000)
	private String additionalInformation;
	
	@ManyToOne
	private MeetingStatus meetingStatus;
	
	@Column(name="statusnotes" , length = 1000)
	private String statusNotes;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy = "meetingEmployeeId.meetings",cascade=CascadeType.ALL )
	private Set<MeetingEmployee> meetingEmployees=new HashSet<MeetingEmployee>(0);
	
	@OneToMany( fetch = FetchType.EAGER,mappedBy="meetings",cascade = CascadeType.ALL)
	private Set<MeetingDocuments> meetingDocuments;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the sendEmail
	 */
	public boolean isSendEmail() {
		return sendEmail;
	}

	/**
	 * @param sendEmail the sendEmail to set
	 */
	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	/**
	 * @return the meetingStartDate
	 */
	public Date getMeetingStartDate() {
		return meetingStartDate;
	}

	/**
	 * @param meetingStartDate the meetingStartDate to set
	 */
	public void setMeetingStartDate(Date meetingStartDate) {
		this.meetingStartDate = meetingStartDate;
	}

	/**
	 * @return the meetingEndDate
	 */
	public Date getMeetingEndDate() {
		return meetingEndDate;
	}

	/**
	 * @param meetingEndDate the meetingEndDate to set
	 */
	public void setMeetingEndDate(Date meetingEndDate) {
		this.meetingEndDate = meetingEndDate;
	}

	/**
	 * @return the venue
	 */
	public String getVenue() {
		return venue;
	}

	/**
	 * @param venue the venue to set
	 */
	public void setVenue(String venue) {
		this.venue = venue;
	}

	/**
	 * @return the agenda
	 */
	public String getAgenda() {
		return agenda;
	}

	/**
	 * @param agenda the agenda to set
	 */
	public void setAgenda(String agenda) {
		this.agenda = agenda;
	}

	/**
	 * @return the additionalInformation
	 */
	public String getAdditionalInformation() {
		return additionalInformation;
	}

	/**
	 * @param additionalInformation the additionalInformation to set
	 */
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	/**
	 * @return the meetingStatus
	 */
	public MeetingStatus getMeetingStatus() {
		return meetingStatus;
	}

	/**
	 * @param meetingStatus the meetingStatus to set
	 */
	public void setMeetingStatus(MeetingStatus meetingStatus) {
		this.meetingStatus = meetingStatus;
	}

	/**
	 * @return the meetingEmployees
	 */
	public Set<MeetingEmployee> getMeetingEmployees() {
		return meetingEmployees;
	}

	/**
	 * @param meetingEmployees the meetingEmployees to set
	 */
	public void setMeetingEmployees(Set<MeetingEmployee> meetingEmployees) {
		this.meetingEmployees = meetingEmployees;
	}

	/**
	 * @return the statusNotes
	 */
	public String getStatusNotes() {
		return statusNotes;
	}

	/**
	 * @param statusNotes the statusNotes to set
	 */
	public void setStatusNotes(String statusNotes) {
		this.statusNotes = statusNotes;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Set<MeetingDocuments> getMeetingDocuments() {
		return meetingDocuments;
	}

	public void setMeetingDocuments(Set<MeetingDocuments> meetingDocuments) {
		this.meetingDocuments = meetingDocuments;
	}

	public boolean isSendNotification() {
		return sendNotification;
	}

	public void setSendNotification(boolean sendNotification) {
		this.sendNotification = sendNotification;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
