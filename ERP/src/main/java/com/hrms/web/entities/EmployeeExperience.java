package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;







import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author shamsheer
 * @since 18-March-2015
 */
@Entity
@Table(name="employee_experience")
public class EmployeeExperience implements AuditEntity,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;

	@Column(name="organisation")
	private String organisation;
	
	
	@ManyToOne
	@JoinColumn(name="designation")
	private EmployeeDesignation designation;
	
	@ManyToOne
	@JoinColumn(name="field")
	private JobFields jobField;
 	
	@Column(name="description" , length = 1000)
	private String description;
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate;
	
	@Column(name="starting_salary")
	private BigDecimal startingSalary;
	
	@Column(name="ending_salary")
	private BigDecimal endingSalary;

	@ManyToOne
	private Employee employee;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the organisation
	 */
	public String getOrganisation() {
		return organisation;
	}

	/**
	 * @param organisation the organisation to set
	 */
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	/**


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the startingSalary
	 */
	public BigDecimal getStartingSalary() {
		return startingSalary;
	}

	/**
	 * @param startingSalary the startingSalary to set
	 */
	public void setStartingSalary(BigDecimal startingSalary) {
		this.startingSalary = startingSalary;
	}

	/**
	 * @return the endingSalary
	 */
	public BigDecimal getEndingSalary() {
		return endingSalary;
	}

	/**
	 * @param endingSalary the endingSalary to set
	 */
	public void setEndingSalary(BigDecimal endingSalary) {
		this.endingSalary = endingSalary;
	}

	/**
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * @return the jobField
	 */
	public JobFields getJobField() {
		return jobField;
	}

	/**
	 * @param jobField the jobField to set
	 */
	public void setJobField(JobFields jobField) {
		this.jobField = jobField;
	}

	/**
	 * @return the designation
	 */
	public EmployeeDesignation getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(EmployeeDesignation designation) {
		this.designation = designation;
	}
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
