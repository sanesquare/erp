package com.hrms.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.accounts.web.entities.Payment;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer
 * @since 08-June-2015
 */
@Entity
@Table(name = "pay_salary_employees")
public class PaySalaryEmployees implements AuditEntity , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 770309171370262296L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "wage_date")
	private Date date;
	
	@ManyToOne
	private Employee employee;
	
	@OneToMany(cascade = CascadeType.ALL , mappedBy = "paySalaryEmployees" , orphanRemoval = true)
	private Set<PaySalaryEmployeeItems> otherItems = new HashSet<PaySalaryEmployeeItems>();
	
	@OneToMany(cascade = CascadeType.ALL , mappedBy = "employeeItem" , orphanRemoval = true)
	private Set<PaysalaryEmployeePayslipItem> payslipItems = new HashSet<PaysalaryEmployeePayslipItem>();
	
	@ManyToOne
	private PaySalary paySalary;
	
	@Column(name = "pf_applicability")
	private Boolean pfAplicability;
	
	@Column(name = "esi_applicability")
	private Boolean esiAplicability;
	
	@Column(name = "basic_salary")
	private BigDecimal basic;
	
	@Column(name = "da")
	private BigDecimal da;
	
	@Column(name = "hra")
	private BigDecimal hra;
	
	@Column(name="per_day_calculation")
	private BigDecimal perDayCalculation;
	
	@Column(name="total_working_days")
	private Long totalWorkingDays;
	
	@Column(name = "lop_count")
	private Double lopCount;
	
	@Column(name = "cca")
	private BigDecimal cca;
	
	@Column(name = "conveyance")
	private BigDecimal conveyance;
	
	@Column(name = "gross_salary")
	private BigDecimal grossSalary;
	
	@Column(name = "lop_deduction")
	private BigDecimal lopDeduction;
	
	@Column(name = "incentive")
	private BigDecimal incentive;
	
	@Column(name = "encashment")
	private BigDecimal encashment;
	
	@Column(name = "bonus")
	private BigDecimal bonus;
	
	@Column(name = "increment")
	private BigDecimal increment;
	
	@Column(name = "total_earnings")
	private BigDecimal totalEarnings;
	
	
	@Column(name = "pf")
	private BigDecimal pf;
	
	@Column(name = "esi")
	private BigDecimal esi;
	
	@Column(name = "lwf")
	private BigDecimal lwf;
	
	@Column(name = "professional_tax")
	private BigDecimal professionalTax;
	
	@Column(name = "tds")
	private BigDecimal tds;
	
	@Column(name = "other_deduction")
	private BigDecimal otherDeduction;
	
	@Column(name = "loan")
	private BigDecimal loan;
	
	@Column(name = "advance")
	private BigDecimal advance;
	
	@Column(name = "total_deduction")
	private BigDecimal totalDeductions;	
	
	@Column(name = "net_pay")
	private BigDecimal netPay;
	
	@Column(name = "pf_employer")
	private BigDecimal pfEmployer;
	
	@Column(name = "esi_employer")
	private BigDecimal esiEmployer;
	
	@Column(name = "ctc")
	private BigDecimal ctc;	
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;
	
	@ManyToOne
	private Payment payment;

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public PaySalary getPaySalary() {
		return paySalary;
	}

	public void setPaySalary(PaySalary paySalary) {
		this.paySalary = paySalary;
	}

	public Set<PaySalaryEmployeeItems> getOtherItems() {
		return otherItems;
	}

	public void setOtherItems(Set<PaySalaryEmployeeItems> otherItems) {
		this.otherItems = otherItems;
	}

	public Boolean getPfAplicability() {
		return pfAplicability;
	}

	public void setPfAplicability(Boolean pfAplicability) {
		this.pfAplicability = pfAplicability;
	}

	public Boolean getEsiAplicability() {
		return esiAplicability;
	}

	public void setEsiAplicability(Boolean esiAplicability) {
		this.esiAplicability = esiAplicability;
	}

	public BigDecimal getBasic() {
		return basic;
	}

	public void setBasic(BigDecimal basic) {
		this.basic = basic;
	}

	public BigDecimal getDa() {
		return da;
	}

	public void setDa(BigDecimal da) {
		this.da = da;
	}

	public BigDecimal getHra() {
		return hra;
	}

	public void setHra(BigDecimal hra) {
		this.hra = hra;
	}

	public BigDecimal getCca() {
		return cca;
	}

	public void setCca(BigDecimal cca) {
		this.cca = cca;
	}

	public BigDecimal getConveyance() {
		return conveyance;
	}

	public void setConveyance(BigDecimal conveyance) {
		this.conveyance = conveyance;
	}

	public BigDecimal getGrossSalary() {
		return grossSalary;
	}

	public void setGrossSalary(BigDecimal grossSalary) {
		this.grossSalary = grossSalary;
	}

	public BigDecimal getLopDeduction() {
		return lopDeduction;
	}

	public void setLopDeduction(BigDecimal lopDeduction) {
		this.lopDeduction = lopDeduction;
	}

	public BigDecimal getIncentive() {
		return incentive;
	}

	public void setIncentive(BigDecimal incentive) {
		this.incentive = incentive;
	}

	public BigDecimal getEncashment() {
		return encashment;
	}

	public void setEncashment(BigDecimal encashment) {
		this.encashment = encashment;
	}

	public BigDecimal getBonus() {
		return bonus;
	}

	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus;
	}

	public BigDecimal getIncrement() {
		return increment;
	}

	public void setIncrement(BigDecimal increment) {
		this.increment = increment;
	}

	public BigDecimal getTotalEarnings() {
		return totalEarnings;
	}

	public void setTotalEarnings(BigDecimal totalEarnings) {
		this.totalEarnings = totalEarnings;
	}

	public BigDecimal getPf() {
		return pf;
	}

	public void setPf(BigDecimal pf) {
		this.pf = pf;
	}

	public BigDecimal getEsi() {
		return esi;
	}

	public void setEsi(BigDecimal esi) {
		this.esi = esi;
	}

	public BigDecimal getLwf() {
		return lwf;
	}

	public void setLwf(BigDecimal lwf) {
		this.lwf = lwf;
	}

	public BigDecimal getProfessionalTax() {
		return professionalTax;
	}

	public void setProfessionalTax(BigDecimal professionalTax) {
		this.professionalTax = professionalTax;
	}

	public BigDecimal getTds() {
		return tds;
	}

	public void setTds(BigDecimal tds) {
		this.tds = tds;
	}

	public BigDecimal getOtherDeduction() {
		return otherDeduction;
	}

	public void setOtherDeduction(BigDecimal otherDeduction) {
		this.otherDeduction = otherDeduction;
	}

	public BigDecimal getLoan() {
		return loan;
	}

	public void setLoan(BigDecimal loan) {
		this.loan = loan;
	}

	public BigDecimal getAdvance() {
		return advance;
	}

	public void setAdvance(BigDecimal advance) {
		this.advance = advance;
	}

	public BigDecimal getTotalDeductions() {
		return totalDeductions;
	}

	public void setTotalDeductions(BigDecimal totalDeductions) {
		this.totalDeductions = totalDeductions;
	}

	public BigDecimal getNetPay() {
		return netPay;
	}

	public void setNetPay(BigDecimal netPay) {
		this.netPay = netPay;
	}

	public BigDecimal getPfEmployer() {
		return pfEmployer;
	}

	public void setPfEmployer(BigDecimal pfEmployer) {
		this.pfEmployer = pfEmployer;
	}

	public BigDecimal getEsiEmployer() {
		return esiEmployer;
	}

	public void setEsiEmployer(BigDecimal esiEmployer) {
		this.esiEmployer = esiEmployer;
	}

	public BigDecimal getCtc() {
		return ctc;
	}

	public void setCtc(BigDecimal ctc) {
		this.ctc = ctc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getPerDayCalculation() {
		return perDayCalculation;
	}

	public void setPerDayCalculation(BigDecimal perDayCalculation) {
		this.perDayCalculation = perDayCalculation;
	}

	public Long getTotalWorkingDays() {
		return totalWorkingDays;
	}

	public void setTotalWorkingDays(Long totalWorkingDays) {
		this.totalWorkingDays = totalWorkingDays;
	}

	public Double getLopCount() {
		return lopCount;
	}

	public void setLopCount(Double lopCount) {
		this.lopCount = lopCount;
	}

	public Set<PaysalaryEmployeePayslipItem> getPayslipItems() {
		return payslipItems;
	}

	public void setPayslipItems(Set<PaysalaryEmployeePayslipItem> payslipItems) {
		this.payslipItems = payslipItems;
	}
}
