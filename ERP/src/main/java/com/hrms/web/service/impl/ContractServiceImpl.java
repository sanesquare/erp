package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ContractDao;
import com.hrms.web.entities.ContractDocuments;
import com.hrms.web.service.ContractService;
import com.hrms.web.vo.ContractJsonVo;
import com.hrms.web.vo.ContractVo;

/**
 * 
 * @author Shamsheer
 * @since 25-March-2015
 */
@Service
public class ContractServiceImpl implements ContractService{

	@Autowired
	private ContractDao contractDao;
	
	public Long saveContract(ContractJsonVo contractVo) {
		return contractDao.saveContract(contractVo);
	}

	public void updateContract(ContractJsonVo contractVo) {
		contractDao.updateContract(contractVo);
	}

	public void deleteContract(Long id) {
		contractDao.deleteContract(id);
	}

	public ContractVo findContractById(Long id) {
		return contractDao.findContractById(id);
	}

	public List<ContractVo> findContractByTitle(String title) {
		return contractDao.findContractByTitle(title);
	}

	public List<ContractVo> listAllContracts() {
		return contractDao.listAllContracts();
	}

	public List<ContractVo> listContractByEmployee(Long employeeId) {
		return contractDao.listContractByEmployee(employeeId);
	}

	public void deleteDocument(String url) {
		contractDao.deleteDocument(url);
	}

	public void updateDocument(ContractVo contractVo) {
		contractDao.updateDocument(contractVo);
	}

}
