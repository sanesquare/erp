package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.BaseCurrency;
import com.hrms.web.vo.BaseCurrencyVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public interface BaseCurrencyService {

	/**
	 * method to save new BaseCurrency
	 * 
	 * @param baseCurrencyVo
	 */
	public void saveBaseCurrency(BaseCurrencyVo baseCurrencyVo);

	/**
	 * method to update BaseCurrency
	 * 
	 * @param baseCurrencyVo
	 */
	public void updateBaseCurrency(BaseCurrencyVo baseCurrencyVo);

	/**
	 * method to delete BaseCurrency
	 * 
	 * @param baseCurrencyId
	 */
	public void deleteBaseCurrency(Long baseCurrencyId);

	/**
	 * method to find BaseCurrency by id
	 * 
	 * @param baseCurrencyId
	 * @return BaseCurrency
	 */
	public BaseCurrency findBaseCurrency(Long baseCurrencyId);

	/**
	 * method to find BaseCurrency by id
	 * 
	 * @param baseCurrency
	 * @return BaseCurrency
	 */
	public BaseCurrency findBaseCurrency(String baseCurrency);

	/**
	 * method to find BaseCurrency by currency
	 * 
	 * @param baseCurrencyId
	 * @return BaseCurrencyVo
	 */
	public BaseCurrencyVo findBaseCurrencyById(Long baseCurrencyId);

	/**
	 * method to find BaseCurrency by currency
	 * 
	 * @param baseCurrency
	 * @return BaseCurrencyVo
	 */
	public BaseCurrencyVo findBaseCurrencyByCurrency(String baseCurrency);

	/**
	 * method to find all BaseCurrency
	 * 
	 * @return List<BaseCurrencyVo>
	 */
	public List<BaseCurrencyVo> findAllBaseCurrency();
}
