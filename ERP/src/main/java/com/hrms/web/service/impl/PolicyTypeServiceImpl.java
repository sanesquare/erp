package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.PolicyTypeDao;
import com.hrms.web.entities.PolicyType;
import com.hrms.web.service.PolicyTypeService;
import com.hrms.web.vo.PolicyTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Service
public class PolicyTypeServiceImpl implements PolicyTypeService {

	@Autowired
	private PolicyTypeDao policyTypeDao;

	public void savePolicyType(PolicyTypeVo policyTypeVo) {
		policyTypeDao.savePolicyType(policyTypeVo);
	}

	public void updatePolicyType(PolicyTypeVo policyTypeVo) {
		policyTypeDao.updatePolicyType(policyTypeVo);
	}

	public void deletePolicyType(Long policyTypeId) {
		policyTypeDao.deletePolicyType(policyTypeId);
	}

	public PolicyType findPolicyType(Long policyTypeId) {
		return policyTypeDao.findPolicyType(policyTypeId);
	}

	public PolicyType findPolicyType(String policyTypeName) {
		return policyTypeDao.findPolicyType(policyTypeName);
	}

	public PolicyTypeVo findPolicyTypeById(Long policyTypeId) {
		return policyTypeDao.findPolicyTypeById(policyTypeId);
	}

	public PolicyTypeVo findPolicyTypeByType(String policyTypeName) {
		return policyTypeDao.findPolicyTypeByType(policyTypeName);
	}

	public List<PolicyTypeVo> findAllPolicyType() {
		return policyTypeDao.findAllPolicyType();
	}

}
