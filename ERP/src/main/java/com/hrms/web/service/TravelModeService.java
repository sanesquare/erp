package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.TravelModeVo;

/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
public interface TravelModeService {

public void saveTravelMode(TravelModeVo vo);
	
	public void updateTravelMode(TravelModeVo vo);
	
	public void deleteTravelMode(Long id);
	
	public TravelModeVo getTravelModeById(Long id);
	
	public List<TravelModeVo> listAllTravelModes();
}
