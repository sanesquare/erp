package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.SalaryTypeVo;

/**
 * 
 * @author Shamsheer
 * @since 18-April-2015
 */
public interface SalaryTypeService {

	/**
	 * method to save salary type
	 * @param salaryTypeVo
	 */
	public void saveSalaryType(SalaryTypeVo salaryTypeVo);
	
	/**
	 * method to update salary type
	 * @param salaryTypeVo
	 */
	public void updateSalaryType(SalaryTypeVo salaryTypeVo);
	
	/**
	 * method to delete salary type
	 * @param id
	 */
	public void deleteSalaryType(Long id);
	
	/**
	 * method to list all salary types
	 * @return
	 */
	public List<SalaryTypeVo> listAllSalaryTypes();
	/**
	 * method to find salary type by id
	 * @param id
	 * @return
	 */
	public SalaryTypeVo findSalaryType(Long id);
	
	/**
	 * method to find salary type by type
	 * @param type
	 * @return
	 */
	public SalaryTypeVo findSalaryType(String type);
}
