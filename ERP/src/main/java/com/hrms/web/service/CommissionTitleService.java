package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.CommissionTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
public interface CommissionTitleService {

	/**
	 * method to save Commission title
	 * @param titleVo
	 */
	public void saveCommissionTitle(CommissionTitleVo titleVo); 
	
	/**
	 * method to update Commission title
	 * @param titleVo
	 */
	public void updateCommissionTitle(CommissionTitleVo titleVo);
	
	/**
	 * method to delete Commission title
	 * @param CommissionTitleId
	 */
	public void deleteCommissionTitle(Long commissionTitleId);
	
	/**
	 * method to delete Commission title
	 * @param CommissionTitle
	 */
	public void deleteCommissionTitle(String commissionTitle);
	
	/**
	 * method to list all Commission titles
	 * @return
	 */
	public List<CommissionTitleVo> listAllCommissionTitles();
}
