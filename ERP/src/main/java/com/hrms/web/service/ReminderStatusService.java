package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.ReminderStatus;
import com.hrms.web.vo.ReminderStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public interface ReminderStatusService {

	/**
	 * method to save ReminderStatus
	 * 
	 * @param reminderStatusVo
	 */
	public void saveReminderStatus(ReminderStatusVo reminderStatusVo);

	/**
	 * method to update ReminderStatus
	 * 
	 * @param reminderStatusVo
	 */
	public void updateReminderStatus(ReminderStatusVo reminderStatusVo);

	/**
	 * method to delete ReminderStatus
	 * 
	 * @param reminderStatusId
	 */
	public void deleteReminderStatus(Long reminderStatusId);

	/**
	 * method to find ReminderStatus by id
	 * 
	 * @param reminderStatusId
	 * @return ReminderStatus
	 */
	public ReminderStatus findReminderStatus(Long reminderStatusId);

	/**
	 * method to find ReminderStatus by status
	 * 
	 * @param reminderStatus
	 * @return ReminderStatus
	 */
	public ReminderStatus findReminderStatus(String reminderStatus);

	/**
	 * method to find ReminderStatus by id
	 * 
	 * @param reminderStatusId
	 * @return ReminderStatusVo
	 */
	public ReminderStatusVo findReminderStatusById(Long reminderStatusId);

	/**
	 * method to find ReminderStatus by status
	 * 
	 * @param reminderStatus
	 * @return ReminderStatusVo
	 */
	public ReminderStatusVo findReminderStatusByStatus(String reminderStatus);

	/**
	 * method to find all ReminderStatus
	 * 
	 * @return List<ReminderStatusVo>
	 */
	public List<ReminderStatusVo> findAllReminderStatus();
}
