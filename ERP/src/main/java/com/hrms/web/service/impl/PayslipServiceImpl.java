package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.PayslipDao;
import com.hrms.web.service.PaySalaryService;
import com.hrms.web.service.PayslipService;
import com.hrms.web.vo.PaySalaryEmployeeItemsVo;
import com.hrms.web.vo.PaysSlipVo;

/**
 * 
 * @author Shamsheer
 * @since 29-April-2015
 */
@Service
public class PayslipServiceImpl implements PayslipService {

	@Autowired
	private PayslipDao dao;
	
	@Autowired
	private PaySalaryService paySalaryService;

	public List<PaySalaryEmployeeItemsVo> getPayslipDetailsByEmployee(String employeeCode , String dateFrom , String dateTo) {
		//return dao.getPayslipDetailsByEmployee(employeeCode,dateFrom,dateTo);
		return paySalaryService.getEmployeeMonthlyWage(employeeCode, dateFrom, dateTo);
	}

	public Long savePayslip(PaysSlipVo paysSlipVo) {
		return dao.savePayslip(paysSlipVo);
	}

	public void updatePayslip(PaysSlipVo paysSlipVo) {
		dao.updatePayslip(paysSlipVo);
	}

	public List<PaysSlipVo> listAllPayslips() {
		return dao.listAllPayslips();
	}

	public void deletPayslip(Long id) {
		dao.deletePayslip(id);
	}

	public PaysSlipVo getPayslipById(Long id) {
		return dao.getPayslipById(id);
	}

	public List<PaysSlipVo> findPayslipOfEmployeeBetweenDates(Long employeeId, Date startDate, Date endDate) {
		return dao.findPayslipOfEmployeeBetweenDates(employeeId, startDate, endDate);
	}

	public List<PaysSlipVo> findAllPayslipsByEmployee(String employeeCode) {
		return dao.findAllPayslipsByEmployee(employeeCode);
	}

	public void updatePayslipStatus(PaysSlipVo paysSlipVo) {
		dao.updatePayslipStatus(paysSlipVo);
	}
}
