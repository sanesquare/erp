package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.PolicyType;
import com.hrms.web.vo.PolicyTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public interface PolicyTypeService {

	/**
	 * method to save PolicyType
	 * 
	 * @param policyTypeVo
	 */
	public void savePolicyType(PolicyTypeVo policyTypeVo);

	/**
	 * method to update PolicyType
	 * 
	 * @param policyTypeVo
	 */
	public void updatePolicyType(PolicyTypeVo policyTypeVo);

	/**
	 * method to delete PolicyType
	 * 
	 * @param policyTypeVo
	 */
	public void deletePolicyType(Long policyTypeId);

	/**
	 * method to find PolicyType by id
	 * 
	 * @param policyTypeId
	 * @return PolicyType
	 */
	public PolicyType findPolicyType(Long policyTypeId);

	/**
	 * method to find PolicyType by type
	 * 
	 * @param policyTypeName
	 * @return PolicyType
	 */
	public PolicyType findPolicyType(String policyTypeName);

	/**
	 * method to find PolicyType by id
	 * 
	 * @param policyTypeId
	 * @return PolicyTypeVo
	 */
	public PolicyTypeVo findPolicyTypeById(Long policyTypeId);

	/**
	 * method to find PolicyType by type
	 * 
	 * @param policyTypeName
	 * @return PolicyTypeVo
	 */
	public PolicyTypeVo findPolicyTypeByType(String policyTypeName);

	/**
	 * method to find all PolicyType
	 * 
	 * @return List<PolicyTypeVo>
	 */
	public List<PolicyTypeVo> findAllPolicyType();
}
