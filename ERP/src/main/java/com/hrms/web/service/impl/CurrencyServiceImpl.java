package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.CurrencyDao;
import com.hrms.web.entities.Currency;
import com.hrms.web.service.CurrencyService;
import com.hrms.web.vo.CurrencyVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Service
public class CurrencyServiceImpl implements CurrencyService {

	@Autowired
	private CurrencyDao currencyDao;

	/**
	 * method to save new Currency
	 * 
	 * @param currencyVo
	 */
	public void saveCurrency(CurrencyVo currencyVo) {
		currencyDao.saveCurrency(currencyVo);
	}

	/**
	 * method to update Currency
	 * 
	 * @param currencyVo
	 */
	public void updateCurrency(CurrencyVo currencyVo) {
		currencyDao.updateCurrency(currencyVo);
	}

	/**
	 * method to delete Currency
	 * 
	 * @param currencyId
	 */
	public void deleteCurrency(Long currencyId) {
		currencyDao.deleteCurrency(currencyId);
	}

	/**
	 * method to find Currency by id
	 * 
	 * @param currencyId
	 * @return Currency
	 */
	public Currency findCurrency(Long currencyId) {
		return currencyDao.findCurrency(currencyId);
	}

	/**
	 * method to find Currency by id
	 * 
	 * @param currencyId
	 * @return CurrencyVo
	 */
	public CurrencyVo findCurrencyById(Long currencyId) {
		return currencyDao.findCurrencyById(currencyId);
	}

	/**
	 * method to find all Currency
	 * 
	 * @return List<CurrencyVo>
	 */
	public List<CurrencyVo> findAllCurrency() {
		return currencyDao.findAllCurrency();
	}

}
