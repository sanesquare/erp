package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.PaySlipItemDao;
import com.hrms.web.entities.PaySlipItem;
import com.hrms.web.service.PaySlipItemService;
import com.hrms.web.vo.PaySlipItemVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Service
public class PaySlipItemServiceImpl implements PaySlipItemService {

	@Autowired
	private PaySlipItemDao paySlipItemDao;

	public void saveOrUpdatePaySlipItem(PaySlipItemVo paySlipItemVo) {
		paySlipItemDao.saveOrUpdatePaySlipItem(paySlipItemVo);
	}

	public void deletePaySlipItem(Long paySlipItemId) {
		paySlipItemDao.deletePaySlipItem(paySlipItemId);
	}

	public PaySlipItem findPaySlipItem(Long paySlipItemId) {
		return paySlipItemDao.findPaySlipItem(paySlipItemId);
	}

	public PaySlipItemVo findPaySlipItemById(Long paySlipItemId) {
		return paySlipItemDao.findPaySlipItemById(paySlipItemId);
	}

	public List<PaySlipItemVo> findAllPaySlipItem() {
		return paySlipItemDao.findAllPaySlipItem();
	}

	public PaySlipItemVo findPaySlipItemByTitle(String title) {
		return paySlipItemDao.findPaySlipItemByTitle(title);
	}

	public PaySlipItem findPaySlipItem(String title) {
		return paySlipItemDao.findPaySlipItem(title);
	}

}
