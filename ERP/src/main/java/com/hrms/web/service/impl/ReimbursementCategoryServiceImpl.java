package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ReimbursementCategoryDao;
import com.hrms.web.entities.ReimbursementCategory;
import com.hrms.web.service.ReimbursementCategoryService;
import com.hrms.web.vo.ReimbursementCategoryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Service
public class ReimbursementCategoryServiceImpl implements ReimbursementCategoryService {

	@Autowired
	private ReimbursementCategoryDao reimbursementCategoryDao;

	public void saveReimbursementCategory(ReimbursementCategoryVo reimbursementCategoryVo) {
		reimbursementCategoryDao.saveReimbursementCategory(reimbursementCategoryVo);
	}

	public void updateReimbursementCategory(ReimbursementCategoryVo reimbursementCategoryVo) {
		reimbursementCategoryDao.updateReimbursementCategory(reimbursementCategoryVo);
	}

	public void deleteReimbursementCategory(Long reimbursementCategoryId) {
		reimbursementCategoryDao.deleteReimbursementCategory(reimbursementCategoryId);
	}

	public ReimbursementCategory findReimbursementCategory(Long reimbursementCategoryId) {
		return reimbursementCategoryDao.findReimbursementCategory(reimbursementCategoryId);
	}

	public ReimbursementCategory findReimbursementCategory(String reimbursementCategory) {
		return reimbursementCategoryDao.findReimbursementCategory(reimbursementCategory);
	}

	public ReimbursementCategoryVo findReimbursementCategoryById(Long reimbursementCategoryId) {
		return reimbursementCategoryDao.findReimbursementCategoryById(reimbursementCategoryId);
	}

	public ReimbursementCategoryVo findReimbursementCategoryByCategory(String reimbursementCategory) {
		return reimbursementCategoryDao.findReimbursementCategoryByCategory(reimbursementCategory);
	}

	public List<ReimbursementCategoryVo> findAllReimbursementCategory() {
		return reimbursementCategoryDao.findAllReimbursementCategory();
	}

}
