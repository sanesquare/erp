package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TrainingEventTypeDao;
import com.hrms.web.entities.TrainingEventType;
import com.hrms.web.service.TrainingEventTypeService;
import com.hrms.web.vo.TrainingEventTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Service
public class TrainingEventTypeServiceImpl implements TrainingEventTypeService {

	@Autowired
	private TrainingEventTypeDao trainingEventTypeDao;

	public void saveTrainingEventType(TrainingEventTypeVo trainingEventTypeVo) {
		trainingEventTypeDao.saveTrainingEventType(trainingEventTypeVo);
	}

	public void updateTrainingEventType(TrainingEventTypeVo trainingEventTypeVo) {
		trainingEventTypeDao.updateTrainingEventType(trainingEventTypeVo);
	}

	public void deleteTrainingEventType(Long trainingEventTypeId) {
		trainingEventTypeDao.deleteTrainingEventType(trainingEventTypeId);
	}

	public TrainingEventType findTrainingEventType(Long trainingEventTypeId) {
		return trainingEventTypeDao.findTrainingEventType(trainingEventTypeId);
	}

	public TrainingEventType findTrainingEventType(String trainingEventType) {
		return trainingEventTypeDao.findTrainingEventType(trainingEventType);
	}

	public TrainingEventTypeVo findTrainingEventTypeById(Long trainingEventTypeId) {
		return trainingEventTypeDao.findTrainingEventTypeById(trainingEventTypeId);
	}

	public TrainingEventTypeVo findTrainingEventTypeBytype(String trainingEventType) {
		return trainingEventTypeDao.findTrainingEventTypeBytype(trainingEventType);
	}

	public List<TrainingEventTypeVo> findAllTrainingEventType() {
		return trainingEventTypeDao.findAllTrainingEventType();
	}

}
