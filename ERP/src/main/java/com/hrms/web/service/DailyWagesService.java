package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.DailyWagesVo;

/**
 * 
 * @author Shamsheer
 * @since 16-April-2015
 */
public interface DailyWagesService {

	/**
	 * method to save new wage
	 * 
	 * @param vo
	 * @return
	 */
	public Long saveDailyWage(DailyWagesVo vo);

	/**
	 * method to update wage
	 * 
	 * @param vo
	 */
	public void updateDailyWage(DailyWagesVo vo);

	/**
	 * method to delete wage
	 * 
	 * @param wageId
	 */
	public void deleteDailyWage(Long wageId);

	/**
	 * 
	 * method to list all daily wages
	 * 
	 * @return
	 */
	public List<DailyWagesVo> listAllDailyWages();

	/**
	 * method to get daily wages by id
	 * 
	 * @param dailyWagesId
	 * @return
	 */
	public DailyWagesVo getDailyWagesById(Long dailyWagesId);

	/**
	 * method to get daily wages by employee id
	 * 
	 * @param dailyWagesId
	 * @return
	 */
	public DailyWagesVo getDailyWagesByEmployeeId(String employeeCode);

	/**
	 * 
	 * method to find all daily wages by employee
	 * 
	 * @return
	 */
	public List<DailyWagesVo> findAllDailyWagesByEmployee(String employeeCode);
}
