package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.OrganizationPolicyDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.OrganizationPolicy;
import com.hrms.web.service.OrganizationPolicyService;
import com.hrms.web.vo.OrganizationPolicyVo;

/**
 * 
 * @author Jithin Mohan
 * @since 15-May-2015
 *
 */
@Service
public class OrganizationPolicyServiceImpl implements OrganizationPolicyService {

	@Autowired
	private OrganizationPolicyDao organizationPolicyDao;

	public void saveOrUpdateOrganizationPolicy(OrganizationPolicyVo organizationPolicyVo) {
		organizationPolicyDao.saveOrUpdateOrganizationPolicy(organizationPolicyVo);
	}

	public void saveOrganizationPolicyDocument(OrganizationPolicyVo organizationPolicyVo) {
		organizationPolicyDao.saveOrganizationPolicyDocument(organizationPolicyVo);
	}

	public void deleteOrganizationPolicy(Long organizationPolicyId) {
		organizationPolicyDao.deleteOrganizationPolicy(organizationPolicyId);
	}

	public OrganizationPolicy findOrganizationPolicy(Long organizationPolicyId) {
		return organizationPolicyDao.findOrganizationPolicy(organizationPolicyId);
	}

	public List<OrganizationPolicy> findOrganizationPolicy(Branch branch) {
		return organizationPolicyDao.findOrganizationPolicy(branch);
	}

	public List<OrganizationPolicy> findOrganizationPolicy(Branch branch, Department department) {
		return organizationPolicyDao.findOrganizationPolicy(branch, department);
	}

	public OrganizationPolicyVo findOrganizationPolicyById(Long organizationPolicyId) {
		return organizationPolicyDao.findOrganizationPolicyById(organizationPolicyId);
	}

	public List<OrganizationPolicyVo> findOrganizationPolicyByBranch(Long branchId) {
		return organizationPolicyDao.findOrganizationPolicyByBranch(branchId);
	}

	public List<OrganizationPolicyVo> findOrganizationPolicyByBranchAndDepartment(Long branchId, Long departmentId) {
		return organizationPolicyDao.findOrganizationPolicyByBranchAndDepartment(branchId, departmentId);
	}

	public List<OrganizationPolicyVo> findOrganizationPolicyByType(String policyType) {
		return organizationPolicyDao.findOrganizationPolicyByType(policyType);
	}

	public List<OrganizationPolicyVo> findAllOrganizationPolicy() {
		return organizationPolicyDao.findAllOrganizationPolicy();
	}

	public void deleteOrganizationPolicyDocument(Long orgPolicyDocId) {
		organizationPolicyDao.deleteOrganizationPolicyDocument(orgPolicyDocId);
	}
}
