package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.PaySlipItem;
import com.hrms.web.vo.PaySlipItemVo;
import com.hrms.web.vo.PaysSlipVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public interface PaySlipItemService {

	/**
	 * method to save or edit PaySlipItem
	 * 
	 * @param paySlipItemVo
	 */
	public void saveOrUpdatePaySlipItem(PaySlipItemVo paySlipItemVo);

	/**
	 * method to delete PaySlipItem
	 * 
	 * @param paySlipItemId
	 */
	public void deletePaySlipItem(Long paySlipItemId);

	/**
	 * method to find PaySlipItem by id
	 * 
	 * @param paySlipItemId
	 * @return PaySlipItem
	 */
	public PaySlipItem findPaySlipItem(Long paySlipItemId);

	/**
	 * method to find PaySlipItem by id
	 * 
	 * @param paySlipItemId
	 * @return PaySlipItemVo
	 */
	public PaySlipItemVo findPaySlipItemById(Long paySlipItemId);

	/**
	 * method to find all PaySlipItem
	 * 
	 * @return List<PaySlipItemVo>
	 */
	public List<PaySlipItemVo> findAllPaySlipItem();
	
	/**
	 * method to find PaySlipItem by title
	 * 
	 * @param title
	 * @return PaySlipItemVo
	 */
	public PaySlipItemVo findPaySlipItemByTitle(String title);
	
	/**
	 * method to find PaySlipItem by title
	 * 
	 * @param title
	 * @return PaySlipItem
	 */
	public PaySlipItem findPaySlipItem(String title);
	
}
