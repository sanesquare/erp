package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.BranchType;
import com.hrms.web.vo.BranchTypeVo;
import com.hrms.web.vo.BranchVo;

/**
 * 
 * @author Vinutha
 * @since 12-March-2015
 *
 */
public interface BranchTypeService {
	
	/**
	 * method to save new BranchType
	 */
	public void saveBranchType(BranchTypeVo branchTypeVo);

	/**
	 * method to return BranchType object found by name
	 * 
	 * @param branchTypeName
	 * @return
	 */
	public BranchType findAndReturnBranchTypeByName(String branchTypeName);

	/**
	 * method to return branchType object
	 * 
	 * @param branchTypeVo
	 * @return
	 */
	public BranchType findAndReturnBranchTypeById(Long branchTypeId);

	/**
	 * 
	 * @param branchTypeVo
	 */
	public void updateBranchType(BranchTypeVo branchTypeVo);

	/**
	 * 
	 * @param branchTypeId
	 */
	public void deleteBranchType(Long branchTypeId);

	/**
	 * 
	 * @param branchTypeId
	 * @return
	 */
	public BranchTypeVo findBranchType(Long branchTypeId);

	/**
	 * 
	 * @param branchType
	 * @return
	 */
	public BranchTypeVo findBranchType(String branchType);

	/**
	 * 
	 * @return
	 */
	List<BranchTypeVo> findAllBranchType();
	/**
	 * method to create branch type Vo from branchVo
	 * @param branchVo
	 */
	public BranchTypeVo createBranchTypeVoFromBranchVo(BranchVo branchVo);

}
