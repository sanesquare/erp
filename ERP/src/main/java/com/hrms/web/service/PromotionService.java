package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.PromotionVo;

public interface PromotionService {
	
	/**
	 * Method to save new Department
	 * @param departmentVo
	 * @return void
	 */
	public void savePromotion(PromotionVo promotionVo);
	
	/**
	 * method to list complete promotion details
	 * @return
	 */
	public List<PromotionVo> listAllPromotionDetails();
	
	/**
	 * method to delete existing promotion
	 * @param promotionVo
	 * @return void
	 */
	public void deletePromotion(Long id);
	
	/**
	 * method to update promotion document
	 * @param promotionVo
	 */
	public PromotionVo updatePromotionDocument(PromotionVo promotionVo);
	
	/**
	 * method to find promotion by using promotion Id
	 * @param id
	 * @return PromotionVo
	 */
	public PromotionVo findPromotionById(Long promotionId);
	
	/**
	 * method to update existing promotion
	 * @param promotionVo
	 * @return void
	 */
	public void updatePromotion(PromotionVo promotionVo);
	
	/**
	 * method to delete promotion document
	 * 
	 * @param path
	 */
	public void deletePromotionDocument(Long id);

}
