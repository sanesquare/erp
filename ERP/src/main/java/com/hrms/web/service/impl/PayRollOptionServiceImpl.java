package com.hrms.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.PayRollOptionDao;
import com.hrms.web.entities.PayRollOption;
import com.hrms.web.service.PayRollOptionService;
import com.hrms.web.vo.PayRollOptionVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Service
public class PayRollOptionServiceImpl implements PayRollOptionService {

	@Autowired
	private PayRollOptionDao payRollOptionDao;

	public void saveOrUpdatePayRollOption(PayRollOptionVo payRollOptionVo) {
		payRollOptionDao.saveOrUpdatePayRollOption(payRollOptionVo);
	}

	public void deletePayRollOption(Long payRollOptionId) {
		payRollOptionDao.deletePayRollOption(payRollOptionId);
	}

	public PayRollOption findPayRollOption(Long payRollOptionId) {
		return payRollOptionDao.findPayRollOption(payRollOptionId);
	}

	public PayRollOptionVo findPayRollOptionById(Long payRollOptionId) {
		return payRollOptionDao.findPayRollOptionById(payRollOptionId);
	}

	public PayRollOptionVo findPayRollOption() {
		return payRollOptionDao.findPayRollOption();
	}

}
