package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.AdvanceSalaryDao;
import com.hrms.web.entities.AdvanceSalary;
import com.hrms.web.entities.AdvanceSalaryStatus;
import com.hrms.web.entities.Employee;
import com.hrms.web.service.AdvanceSalaryService;
import com.hrms.web.vo.AdvanceSalaryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Service
public class AdvanceSalaryServiceImpl implements AdvanceSalaryService {

	@Autowired
	private AdvanceSalaryDao advanceSalaryDao;

	public void saveOrUpdateAdvanceSalary(AdvanceSalaryVo advanceSalaryVo) {
		advanceSalaryDao.saveOrUpdateAdvanceSalary(advanceSalaryVo);
	}

	public void updateAdvanceSalaryStatus(AdvanceSalaryVo advanceSalaryVo) {
		advanceSalaryDao.updateAdvanceSalaryStatus(advanceSalaryVo);
	}

	public void deleteAdvanceSalary(Long advanceSalaryId) {
		advanceSalaryDao.deleteAdvanceSalary(advanceSalaryId);
	}

	public AdvanceSalary findAdvanceSalary(Long advanceSalaryId) {
		return advanceSalaryDao.findAdvanceSalary(advanceSalaryId);
	}

	public AdvanceSalaryVo findAdvanceSalaryById(Long advanceSalaryId) {
		return advanceSalaryDao.findAdvanceSalaryById(advanceSalaryId);
	}

	public AdvanceSalaryVo findAdvanceSalaryByEmployee(Employee employee) {
		return advanceSalaryDao.findAdvanceSalaryByEmployee(employee);
	}

	public List<AdvanceSalaryVo> findAdvanceSalaryByDate(Date requestedDate) {
		return advanceSalaryDao.findAdvanceSalaryByDate(requestedDate);
	}

	public List<AdvanceSalaryVo> findAdvanceSalaryBetweenDates(Date startDate, Date endDate) {
		return advanceSalaryDao.findAdvanceSalaryBetweenDates(startDate, endDate);
	}

	public List<AdvanceSalaryVo> findAdvanceSalaryByStatus(AdvanceSalaryStatus status) {
		return advanceSalaryDao.findAdvanceSalaryByStatus(status);
	}

	public List<AdvanceSalaryVo> findAllAdvanceSalary() {
		return advanceSalaryDao.findAllAdvanceSalary();
	}

	public List<AdvanceSalaryVo> findAdvanceSalaryBetweenDate(Long employeeId, Date startDate, Date endDate) {
		return advanceSalaryDao.findAdvanceSalaryBetweenDate(employeeId, startDate, endDate);
	}

	public List<AdvanceSalaryVo> findAllAdvanceSalaryByEmployee(String employeeCode) {
		return advanceSalaryDao.findAllAdvanceSalaryByEmployee(employeeCode);
	}

	public List<AdvanceSalaryVo> findAdvanceSalaryByStatusAndDates(String employeeCode, String status, Date startDate,
			Date endDate) {
		return advanceSalaryDao.findAdvanceSalaryByStatusAndDates(employeeCode, status, startDate, endDate);
	}

}
