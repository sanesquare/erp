package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.EmployeeStatus;
import com.hrms.web.vo.EmployeeStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
public interface EmployeeStatusService {

	/**
	 * method to save new status
	 * 
	 * @param statusVo
	 */
	public void saveStatus(EmployeeStatusVo statusVo);

	/**
	 * method to update status
	 * 
	 * @param statusVo
	 */
	public void updateStatus(EmployeeStatusVo statusVo);

	/**
	 * method to delete status
	 * 
	 * @param id
	 */
	public void deleteStatus(Long id);

	/**
	 * method to list all status
	 * 
	 * @return
	 */
	public List<EmployeeStatusVo> listAllStatus();

	/**
	 * method to find status by id
	 * 
	 * @param id
	 * @return
	 */
	public EmployeeStatusVo findStatusById(Long id);

	/**
	 * method to find status by name
	 * 
	 * @param name
	 * @return
	 */
	public EmployeeStatusVo findStatusByName(String name);

	/**
	 * method to find status
	 * 
	 * @param id
	 * @return
	 */
	public EmployeeStatus findStatus(Long id);

	/**
	 * method to find status
	 * 
	 * @param status
	 * @return
	 */
	public EmployeeStatus findStatus(String status);
	
	/**
	 * 
	 * @return
	 */
	public List<EmployeeStatusVo> findAllEmployeeStatus();
}
