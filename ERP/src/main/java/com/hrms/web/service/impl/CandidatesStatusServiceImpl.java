package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.CandidateStatusDao;
import com.hrms.web.entities.CandidateStatus;
import com.hrms.web.service.CandidatesStatusService;
import com.hrms.web.vo.CandidatesStatusVo;

/**
 * 
 * @author Vinutha
 * @since 24-March-2015
 *
 */
@Service
public class CandidatesStatusServiceImpl implements CandidatesStatusService{

	@Autowired
	private CandidateStatusDao candidateStatusDao;
	/**
	 * method to save status
	 * @param candidatesStatusVo
	 */
	public void saveCandidatesStatus(CandidatesStatusVo candidatesStatusVo) {
		candidateStatusDao.saveCandidatesStatus(candidatesStatusVo);
		
	}
	/**
	 * method to delete status
	 *  @param candidatesStatusVo
	 */
	public void deleteCandidatesStatus(CandidatesStatusVo candidatesStatusVo) {
		candidateStatusDao.deleteCandidateStatus(candidatesStatusVo);
		
	}
	/**
	 * method to list status
	 * @return List<CandidatesStatusVo>
	 */
	public List<CandidatesStatusVo> listAllStatus() {
		
		return candidateStatusDao.listAllStatus();
	}
	/**
	 * method to find status object by id
	 * @param Long id
	 * @return CandidateStatus
	 */
	public CandidateStatus findAndReturnStatusObjectById(Long id) {
		
		return candidateStatusDao.findAndReturnStatusObjectById(id);
	}
	/**
	 * method to find status by id
	 * @param Long id
	 * @return CandidateStatus
	 */
	public CandidatesStatusVo findAndReturnStatusById(Long id) {
		
		return candidateStatusDao.findAndReturnStatusById(id);
	}
	
	

	

}
