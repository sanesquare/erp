package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Department;
import com.hrms.web.entities.OrganizationPolicy;
import com.hrms.web.vo.OrganizationPolicyVo;

/**
 * 
 * @author Jithin Mohan
 * @since 15-May-2015
 *
 */
public interface OrganizationPolicyService {

	/**
	 * 
	 * @param organizationPolicyVo
	 */
	public void saveOrUpdateOrganizationPolicy(OrganizationPolicyVo organizationPolicyVo);

	/**
	 * 
	 * @param organizationPolicyVo
	 */
	public void saveOrganizationPolicyDocument(OrganizationPolicyVo organizationPolicyVo);

	/**
	 * 
	 * @param organizationPolicyId
	 */
	public void deleteOrganizationPolicy(Long organizationPolicyId);

	/**
	 * 
	 * @param organizationPolicyId
	 * @return
	 */
	public OrganizationPolicy findOrganizationPolicy(Long organizationPolicyId);

	/**
	 * 
	 * @param branch
	 * @return
	 */
	public List<OrganizationPolicy> findOrganizationPolicy(Branch branch);

	/**
	 * 
	 * @param branch
	 * @param department
	 * @return
	 */
	public List<OrganizationPolicy> findOrganizationPolicy(Branch branch, Department department);

	/**
	 * 
	 * @param organizationPolicyId
	 * @return
	 */
	public OrganizationPolicyVo findOrganizationPolicyById(Long organizationPolicyId);

	/**
	 * 
	 * @param branchId
	 * @return
	 */
	public List<OrganizationPolicyVo> findOrganizationPolicyByBranch(Long branchId);

	/**
	 * 
	 * @param branchId
	 * @param departmentId
	 * @return
	 */
	public List<OrganizationPolicyVo> findOrganizationPolicyByBranchAndDepartment(Long branchId, Long departmentId);

	/**
	 * 
	 * @param policyType
	 * @return
	 */
	public List<OrganizationPolicyVo> findOrganizationPolicyByType(String policyType);

	/**
	 * 
	 * @return
	 */
	public List<OrganizationPolicyVo> findAllOrganizationPolicy();

	/**
	 * 
	 * @param terminationDocId
	 */
	public void deleteOrganizationPolicyDocument(Long orgPolicyDocId);
}
