package com.hrms.web.service;

import com.hrms.web.entities.ProfidentFundSyntax;
import com.hrms.web.vo.ProfidentFundSyntaxVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public interface ProfidentFundSyntaxService {

	/**
	 * method to save or update ProfidentFundSyntax
	 * 
	 * @param profidentFundSyntaxVo
	 */
	public void saveOrUpdateProfidentFundSyntax(ProfidentFundSyntaxVo profidentFundSyntaxVo);

	/**
	 * method to delete ProfidentFundSyntax
	 * 
	 * @param pfSyntaxId
	 */
	public void deleteProfidentFundSyntax(Long pfSyntaxId);

	/**
	 * method to find ProfidentFundSyntax by id
	 * 
	 * @param pfSyntaxId
	 * @return ProfidentFundSyntax
	 */
	public ProfidentFundSyntax findProfidentFundSyntax(Long pfSyntaxId);

	/**
	 * method to find ProfidentFundSyntax by id
	 * 
	 * @param pfSyntaxId
	 * @return ProfidentFundSyntaxVo
	 */
	public ProfidentFundSyntaxVo findProfidentFundSyntaxById(Long pfSyntaxId);

	/**
	 * method to find ProfidentFundSyntax
	 * 
	 * @return ProfidentFundSyntaxVo
	 */
	public ProfidentFundSyntaxVo findProfidentFundSyntax();
}
