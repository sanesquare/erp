package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.EmployeeGrade;
import com.hrms.web.entities.EmployeeWorkShift;
import com.hrms.web.vo.EmployeeGradeVo;
import com.hrms.web.vo.EmployeeVo;

/**
 * 
 * @author shamsheer
 * @since 18-March-2015
 */
public interface EmployeeGradeService {

	/**
	 * method to save employee grade
	 * 
	 * @param employeeVo
	 */
	public void saveGrade(EmployeeGradeVo gradeVo);

	/**
	 * method to list all grades
	 * 
	 * @return
	 */
	public List<EmployeeGrade> listAllGrades();

	/**
	 * method to list work shifts
	 * 
	 * @return
	 */
	public List<EmployeeWorkShift> listWorkShift();

	/**
	 * method to update EmployeeGrade
	 * 
	 * @param gradeVo
	 */
	public void updateGrade(EmployeeGradeVo gradeVo);

	/**
	 * method to delete EmployeeGrade
	 * 
	 * @param gradeId
	 */
	public void deleteGrade(Long gradeId);

	/**
	 * method to find EmployeeGrade by id
	 * 
	 * @param gradeId
	 * @return EmployeeGrade
	 */
	public EmployeeGrade findGrade(Long gradeId);

	/**
	 * method to find EmployeeGrade by grade
	 * 
	 * @param grade
	 * @return EmployeeGrade
	 */
	public EmployeeGrade findGrade(String grade);

	/**
	 * method to find EmployeeGrade by id
	 * 
	 * @param gradeId
	 * @return EmployeeGradeVo
	 */
	public EmployeeGradeVo findGradeById(Long gradeId);

	/**
	 * method to find EmployeeGrade by grade
	 * 
	 * @param gradeType
	 * @return EmployeeGradeVo
	 */
	public EmployeeGradeVo findGradeByType(String gradeType);

	/**
	 * method to find all EmployeeGrade
	 * 
	 * @return List<EmployeeGradeVo>
	 */
	public List<EmployeeGradeVo> findAllGrade();
}
