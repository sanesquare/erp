package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.PromotionStatusDao;
import com.hrms.web.entities.PromotionStatus;
import com.hrms.web.service.PromotionStatusService;
import com.hrms.web.vo.PromotionStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 25-March-2015
 *
 */
@Service
public class PromotionStatusServiceImpl implements PromotionStatusService {

	@Autowired
	private PromotionStatusDao promotionStatusDao;

	public void savePromotionStatus(PromotionStatusVo promotionStatusVo) {
		promotionStatusDao.savePromotionStatus(promotionStatusVo);
	}

	public void updatePromotionStatus(PromotionStatusVo promotionStatusVo) {
		promotionStatusDao.updatePromotionStatus(promotionStatusVo);
	}

	public void deletePromotionStatus(Long statusId) {
		promotionStatusDao.deletePromotionStatus(statusId);
	}

	public PromotionStatus findPromotionStatus(Long statusId) {
		return promotionStatusDao.findPromotionStatus(statusId);
	}

	public PromotionStatus findPromotionStatus(String status) {
		return promotionStatusDao.findPromotionStatus(status);
	}

	public PromotionStatusVo findPromotionStatusById(Long statusId) {
		return promotionStatusDao.findPromotionStatusById(statusId);
	}

	public PromotionStatusVo findPromotionStatusByStatus(String status) {
		return promotionStatusDao.findPromotionStatusByStatus(status);
	}

	public List<PromotionStatusVo> findAllPromotionStatus() {
		return promotionStatusDao.findAllPromotionStatus();
	}

}
