package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.AdjustmentTitleVo;
/**
 * @author Shamsheer
 * @since 14-April-2015
 */
public interface AdjustmentTitleService {

	/**
	 * method to save Adjustment title
	 * @param titleVo
	 */
	public void saveAdjustmentTitle(AdjustmentTitleVo titleVo); 
	
	/**
	 * method to update Adjustment title
	 * @param titleVo
	 */
	public void updateAdjustmentTitle(AdjustmentTitleVo titleVo);
	
	/**
	 * method to delete Adjustment title
	 * @param AdjustmentTitleId
	 */
	public void deleteAdjustmentTitle(Long adjustmentTitleId);
	
	/**
	 * method to delete Adjustment title
	 * @param AdjustmentTitle
	 */
	public void deleteAdjustmentTitle(String adjustmentTitle);
	
	/**
	 * method to list all Adjustment titles
	 * @return
	 */
	public List<AdjustmentTitleVo> listAllAdjustmentTitles();
}
