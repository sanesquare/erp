package com.hrms.web.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TaxRuleDao;
import com.hrms.web.entities.TaxRule;
import com.hrms.web.service.TaxRuleService;
import com.hrms.web.vo.TaxRuleVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Service
public class TaxRuleServiceImpl implements TaxRuleService {

	@Autowired
	private TaxRuleDao taxRuleDao;

	public void saveOrUpdateTaxRule(TaxRuleVo taxRuleVo) {
		taxRuleDao.saveOrUpdateTaxRule(taxRuleVo);
	}

	public void deleteTaxRule(Long taxRuleId) {
		taxRuleDao.deleteTaxRule(taxRuleId);
	}

	public TaxRule findTaxRule(Long taxRuleId) {
		return taxRuleDao.findTaxRule(taxRuleId);
	}

	public TaxRuleVo findTaxRuleById(Long taxRuleId) {
		return taxRuleDao.findTaxRuleById(taxRuleId);
	}

	public List<TaxRuleVo> findAllTaxRule() {
		return taxRuleDao.findAllTaxRule();
	}

	public TaxRule findTaxRuleBySalaryTo(BigDecimal salaryTo) {
		return taxRuleDao.findTaxRuleBySalaryTo(salaryTo);
	}

	public TaxRuleVo findTaxRuleVoBySalaryTo(BigDecimal salaryTo) {
		return taxRuleDao.findTaxRuleVoBySalaryTo(salaryTo);
	}

	public TaxRule findTaxRuleBySalaryFrom(BigDecimal salaryFrom) {
		return taxRuleDao.findTaxRuleBySalaryFrom(salaryFrom);
	}

	public TaxRuleVo findTaxRuleVoBySalaryFrom(BigDecimal salaryFrom) {
		return taxRuleDao.findTaxRuleVoBySalaryFrom(salaryFrom);
	}

	public List<TaxRuleVo> findAllTaxRuleByIndividual(String individualCategory) {
		return taxRuleDao.findAllTaxRuleByIndividual(individualCategory);
	}

}
