package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.InsuranceType;
import com.hrms.web.vo.InsuranceTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public interface InsuranceTypeService {

	/**
	 * method to save InsuranceType
	 * 
	 * @param insuranceTypeVo
	 */
	public void saveInsuranceType(InsuranceTypeVo insuranceTypeVo);

	/**
	 * method to update InsuranceType
	 * 
	 * @param insuranceTypeVo
	 */
	public void updateInsuranceType(InsuranceTypeVo insuranceTypeVo);

	/**
	 * method to delete InsuranceType
	 * 
	 * @param insuranceTypeId
	 */
	public void deleteInsuranceType(Long insuranceTypeId);

	/**
	 * method to find InsuranceType by id
	 * 
	 * @param insuranceTypeId
	 * @return InsuranceType
	 */
	public InsuranceType findInsuranceType(Long insuranceTypeId);

	/**
	 * method to find InsuranceType by type
	 * 
	 * @param insuranceType
	 * @return InsuranceType
	 */
	public InsuranceType findInsuranceType(String insuranceType);

	/**
	 * method to find InsuranceType by id
	 * 
	 * @param insuranceTypeId
	 * @return InsuranceTypeVo
	 */
	public InsuranceTypeVo findInsuranceTypeById(Long insuranceTypeId);

	/**
	 * method to find InsuranceType by type
	 * 
	 * @param insuranceType
	 * @return InsuranceTypeVo
	 */
	public InsuranceTypeVo findInsuranceTypeByType(String insuranceType);

	/**
	 * method to find all InsuranceType
	 * 
	 * @return List<InsuranceTypeVo>
	 */
	public List<InsuranceTypeVo> findAllInsuranceType();
}
