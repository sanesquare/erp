package com.hrms.web.service;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Warning;
import com.hrms.web.vo.WarningVo;

/**
 * 
 * @author Jithin Mohan
 * @since 20-March-2015
 *
 */
public interface WarningService {

	/**
	 * method to save Warning
	 * 
	 * @param warningVo
	 */
	public void saveWarning(WarningVo warningVo);

	/**
	 * method to save Warning Document
	 * 
	 * @param warningVo
	 */
	public void saveWarningDocuments(WarningVo warningVo);

	/**
	 * method to update Warning
	 * 
	 * @param warningVo
	 */
	public void updateWarning(WarningVo warningVo);

	/**
	 * method to delete Warning
	 * 
	 * @param warningId
	 */
	public void deleteWarning(Long warningId);

	/**
	 * method to delete Warning Document
	 * 
	 * @param documentId
	 */
	public void deleteWarningDocument(Long documentId);

	/**
	 * method to find Warning by id
	 * 
	 * @param warningId
	 * @return Warning
	 */
	public Warning findWarning(Long warningId);

	/**
	 * method to find Warning by id
	 * 
	 * @param warningId
	 * @return WarningVo
	 */
	public WarningVo findWarningById(Long warningId);

	/**
	 * method to find Warning by date
	 * 
	 * @param warningOn
	 * @return List<WarningVo>
	 */
	public List<WarningVo> findWarningByDate(Date warningOn);

	/**
	 * method to find Warning between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return List<WarningVo>
	 */
	public List<WarningVo> findWarningBetweenDates(Date startDate, Date endDate);

	/**
	 * method to find Warning by date
	 * 
	 * @param createdOn
	 * @return List<WarningVo>
	 */
	public List<WarningVo> findWarningByCreatedDate(Date createdOn);

	/**
	 * method to find Warning between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return List<WarningVo>
	 */
	public List<WarningVo> findWarningCreatedBetweenDates(Date startDate, Date endDate);

	/**
	 * method to find Warning by creater
	 * 
	 * @param warningBy
	 * @return List<WarningVo>
	 */
	public List<WarningVo> findWarningByCreator(Employee warningBy);

	/**
	 * method to find all Warning
	 * 
	 * @return List<WarningVo>
	 */
	public List<WarningVo> findAllWarning();
}
