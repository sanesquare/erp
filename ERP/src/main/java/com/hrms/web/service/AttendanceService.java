package com.hrms.web.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hrms.web.vo.AttendanceVo;
import com.hrms.web.vo.BiometricAttendanceVo;
import com.hrms.web.vo.RestBiometricAttendanceVo;
/**
 * 
 * @author shamsheer
 *
 */
public interface AttendanceService {

	/**
	 * method to save attendance
	 * @param vo
	 * @return
	 */
	public Long saveAttendance(AttendanceVo vo);
	
	/**
	 * method to update attendance
	 * @param vo
	 */
	public void updateAttendance(AttendanceVo vo);
	
	/**
	 * method to get attendance by id
	 * @param attendanceId
	 * @return
	 */
	public AttendanceVo getAttendanceByAttendanceId(Long attendanceId);
	
	/**
	 * method to delete attendance
	 * @param attendanceId
	 */
	public void deleteAttendance(Long attendanceId);
	
	/**
	 * method to list all attendances
	 * @return
	 */
	public List<AttendanceVo> listAllAttendance();
	
	/**
	 * method to list attendance by employee
	 * @param employeeId
	 * @return
	 */
	public List<AttendanceVo> listAttendanceByEmployee(Long employeeId);
	
	/**
	 * method to list attendance by employee and date
	 * @param employeeId
	 * @param date
	 * @return
	 */
	public List<AttendanceVo> listAttendanceByEmployeeAndDate(Long employeeId , String date);
	
	/**
	 * method to calculate total hours worked by an employee on given day
	 * @param employeeId
	 * @param date
	 * @return
	 */
	public AttendanceVo calculateHoursWorkedByEmployeeOnDate(Long employeeId , String date);
	
	/**
	 * method to calculate total hours worked by an employee on given day
	 * @param employeeId
	 * @param date
	 * @return
	 */
	public AttendanceVo calculateHoursWorkedByEmployeeOnDate(String employeeCode , String date);
	
	/**
	 * method to get total days attended by employee
	 * @param employeeId
	 * @param date
	 * @return
	 */
	public List<AttendanceVo> getTotalDaysAttendedByEmployee(Long employeeId , String date);
	
	/**
	 * method to get attendance of employee between dates
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Integer listAttendanceObjectByEmployeeAndDate(Long employeeId , String startDate , String endDate);
	
	/**
	 * method to list attendance by employee
	 * @param employeeId
	 * @return
	 */
	public List<AttendanceVo> listAttendanceByEmployee(String employeeCode , String date);
	
	/**
	 * method to save attendance from biometric device
	 * @param attendanceVos
	 */
	public void saveAttendanceFromBiometric(Set<BiometricAttendanceVo> attendanceVos);
	
	/**
	 * method to save attendance from rest
	 * @param attendanceMap
	 */
	public void saveAttendanceFromRestService(Map<String, RestBiometricAttendanceVo> attendanceMap);
}
