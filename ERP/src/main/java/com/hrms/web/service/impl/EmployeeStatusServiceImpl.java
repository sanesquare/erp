package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EmployeeStatusDao;
import com.hrms.web.entities.EmployeeStatus;
import com.hrms.web.service.EmployeeStatusService;
import com.hrms.web.vo.EmployeeStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Service
public class EmployeeStatusServiceImpl implements EmployeeStatusService {

	@Autowired
	private EmployeeStatusDao employeeStatusDao;
	
	public void saveStatus(EmployeeStatusVo statusVo) {
		employeeStatusDao.saveStatus(statusVo);
	}

	public void updateStatus(EmployeeStatusVo statusVo) {
		employeeStatusDao.updateStatus(statusVo);
	}

	public void deleteStatus(Long id) {
		employeeStatusDao.deleteStatus(id);
	}

	public List<EmployeeStatusVo> listAllStatus() {
		return employeeStatusDao.listAllStatus();
	}

	public EmployeeStatusVo findStatusById(Long id) {
		return employeeStatusDao.findStatusById(id);
	}

	public EmployeeStatusVo findStatusByName(String name) {
		return employeeStatusDao.findStatusByName(name);
	}

	public EmployeeStatus findStatus(Long id) {
		return employeeStatusDao.findStatus(id);
	}

	public EmployeeStatus findStatus(String status) {
		return employeeStatusDao.findStatus(status);
	}

	public List<EmployeeStatusVo> findAllEmployeeStatus() {
		return employeeStatusDao.findAllEmployeeStatus();
	}

}
