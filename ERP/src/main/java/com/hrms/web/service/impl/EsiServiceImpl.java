package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EsiDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Esi;
import com.hrms.web.service.EsiService;
import com.hrms.web.vo.EsiVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-April-2015
 *
 */
@Service
public class EsiServiceImpl implements EsiService {

	@Autowired
	private EsiDao esiDao;

	public void saveOrUpdateEsi(EsiVo esiVo) {
		esiDao.saveOrUpdateEsi(esiVo);
	}

	public void deleteEsi(Long esiId) {
		esiDao.deleteEsi(esiId);
	}

	public Esi findEsi(Long esiId) {
		return esiDao.findEsi(esiId);
	}

	public EsiVo findEsiById(Long esiId) {
		return esiDao.findEsiById(esiId);
	}

	public EsiVo findEsiByEmployee(Employee employee) {
		return esiDao.findEsiByEmployee(employee);
	}

	public List<EsiVo> findAllEsi() {
		return esiDao.findAllEsi();
	}

	public List<EsiVo> findAllEsiByEmployee(String employeeCode) {
		return esiDao.findAllEsiByEmployee(employeeCode);
	}

}
