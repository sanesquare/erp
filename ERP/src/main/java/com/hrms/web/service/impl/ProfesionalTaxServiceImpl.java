package com.hrms.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ProfesionalTaxDao;
import com.hrms.web.entities.ProfesionalTax;
import com.hrms.web.service.ProfesionalTaxService;
import com.hrms.web.vo.ProfesionalTaxVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-May-2015
 *
 */
@Service
public class ProfesionalTaxServiceImpl implements ProfesionalTaxService {

	@Autowired
	private ProfesionalTaxDao profesionalTaxDao;

	public void saveOrUpdate(ProfesionalTaxVo profesionalTaxVo) {
		profesionalTaxDao.saveOrUpdate(profesionalTaxVo);
	}

	public ProfesionalTax findProfesionalTax(Long id) {
		return profesionalTaxDao.findProfesionalTax(id);
	}

	public ProfesionalTaxVo findProfesionalTaxById(Long id) {
		return profesionalTaxDao.findProfesionalTaxById(id);
	}

	public ProfesionalTaxVo findProfesionalTax() {
		return profesionalTaxDao.findProfesionalTax();
	}

}
