package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.DepartmentDao;
import com.hrms.web.dao.ProjectDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.ProjectVo;
/**
 * 
 * @author Sangeeth and Bibin
 *
 */
@Service
public class DepartmentServiceImpl implements DepartmentService{
	
	@Log
	private static Logger logger;
	
	@Autowired
	private DepartmentDao departmentDao;

	/**
	 * Method to save new Department
	 * @param departmentVo
	 * @return void
	 */
	public void saveDepartment(DepartmentVo departmentVo) {
		logger.info("inside>> DepartmentService.. saveDepartment");
		departmentDao.saveDepartment(departmentVo);
	}

	public List<DepartmentVo> listAllDepartments() {
		return departmentDao.findAllDepartments();		
	}

	public List<DepartmentVo> listAllDepartmentDetails() {
		return departmentDao.findAllDepartmentDetails();		
		
	}

	public DepartmentVo findDepartmetById(Long departmentId) {
		logger.info("inside>> DepartmentService.. findDepartmentById");
		return departmentDao.findDepartmentById(departmentId);
	}

	public void deleteDepartment(Long id) {
		logger.info("inside>> DepartmentService.. deleteDepartment");
		departmentDao.deleteDepartment(id);
		
	}

	public void updateDepartment(DepartmentVo departmentVo) {
		logger.info("inside>> DepartmentService.. updatedepartment");
		departmentDao.updateDepartment(departmentVo);
		
	}

	public void deleteDepartmentDocument(Long id) {
		departmentDao.deleteDepartmentDocument(id);
		
	}

	public List<DepartmentVo> findDepartmentByBranch(Branch branch) {
		return departmentDao.findDepartmentByBranch(branch);
	}

	public DepartmentVo updateDepartmentDocument(DepartmentVo departmentVo) {
		return departmentDao.updateDepartmentDocument(departmentVo);
	}

	
	
	/**
	 * method to list all employees
	 * @return List<Employee>
	 */
	


}
