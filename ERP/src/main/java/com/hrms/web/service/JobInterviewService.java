package com.hrms.web.service;

import java.util.Date;
import java.util.List;

import org.springframework.scheduling.config.IntervalTask;

import com.hrms.web.entities.JobInterview;
import com.hrms.web.vo.JobInterviewVo;

/**
 * 
 * @author Vinutha
 * @since 24-March-2015
 *
 */
public interface JobInterviewService {
	
	/**
	 * method to save interview information
	 * @param interviewVo
	 * @return
	 */
	public Long saveJobInterviewInfo(JobInterviewVo interviewVo);
	/**
	 * method to save interview description
	 * @param interviewVo
	 */
	public void saveInterviewDesc(JobInterviewVo interviewVo);
	/**
	 * method to save interview additional info
	 * @param interviewVo
	 */
	public void saveInterviewAddInfo(JobInterviewVo interviewVo);
	/**
	 * method to update interview info 
	 * @param interviewVo
	 */
	public void updateJobInterviewInfo(JobInterviewVo interviewVo);
	/**
	 * method to delete job interview
	 * @param interviewVo
	 */
	public void deleteJobInterview(JobInterviewVo interviewVo);
	/**
	 * method to delete job interview by id
	 * @param id
	 */
	public void deleteJobInterviewById(Long id);
	/**
	 * method to list all interviews
	 * @return
	 */
	public List<JobInterviewVo> listAllInterviews();
	/**
	 * method to find and return interview object
	 * @param id
	 * @return
	 */
	public JobInterview findAndReturnInterviewObjectById(Long id);
	/**
	 * method to list interviews based on place
	 * @param place
	 * @return
	 */
	public List<JobInterviewVo> listInterviewsBasedOnPlace(String place);
	/**
	 * method to return JobInterviewVo based on id
	 * @param id
	 * @return
	 */
	public JobInterviewVo findJobInterviewById(Long id);
	/**
	 * method to update documents
	 * @param interviewVo
	 */
	public void updateDocuments(JobInterviewVo interviewVo);
	/**
	 * method to delete document
	 * @param url
	 */
	public void deleteDocument(String url); 
	/**
	 * Method to list interviews based on date
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<JobInterviewVo> listInterviewsByDateRange(Date startDate , Date endDate);
	/**
	 * Method to list interviews based on date range and job post
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<JobInterviewVo> listInterviewsByJobPostDateRange(Long postId,Date startDate , Date endDate);
	/**
	 * Method to list job interviews by branch and date
	 * @param branchId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<JobInterviewVo> listInterviewByBranchAndDate(Long branchId , Date startDate , Date endDate);
	/**
	 * Method to Get interview by post and date
	 * @param postId
	 * @param date
	 * @return
	 */
	public JobInterviewVo listInterviewByPostAndDate(Long postId , String date);
	/**
	 * Method to getinterviews by date
	 * @param date
	 * @return
	 */
	public List<JobInterviewVo> listInterviewsByDate(String date);
}
