package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.ReimbursementCategory;
import com.hrms.web.vo.ReimbursementCategoryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public interface ReimbursementCategoryService {

	/**
	 * method to save ReimbursementCategory
	 * 
	 * @param reimbursementCategoryVo
	 */
	public void saveReimbursementCategory(ReimbursementCategoryVo reimbursementCategoryVo);

	/**
	 * method to update ReimbursementCategory
	 * 
	 * @param reimbursementCategoryVo
	 */
	public void updateReimbursementCategory(ReimbursementCategoryVo reimbursementCategoryVo);

	/**
	 * method to delete ReimbursementCategory
	 * 
	 * @param reimbursementCategoryId
	 */
	public void deleteReimbursementCategory(Long reimbursementCategoryId);

	/**
	 * method to find ReimbursementCategory by id
	 * 
	 * @param reimbursementCategoryId
	 * @return ReimbursementCategory
	 */
	public ReimbursementCategory findReimbursementCategory(Long reimbursementCategoryId);

	/**
	 * method to find ReimbursementCategory by category
	 * 
	 * @param reimbursementCategory
	 * @return ReimbursementCategory
	 */
	public ReimbursementCategory findReimbursementCategory(String reimbursementCategory);

	/**
	 * method to find ReimbursementCategory by id
	 * 
	 * @param reimbursementCategoryId
	 * @return ReimbursementCategoryVo
	 */
	public ReimbursementCategoryVo findReimbursementCategoryById(Long reimbursementCategoryId);

	/**
	 * method to find ReimbursementCategory by category
	 * 
	 * @param reimbursementCategory
	 * @return ReimbursementCategoryVo
	 */
	public ReimbursementCategoryVo findReimbursementCategoryByCategory(String reimbursementCategory);

	/**
	 * method to find all ReimbursementCategory
	 * 
	 * @return List<ReimbursementCategoryVo>
	 */
	public List<ReimbursementCategoryVo> findAllReimbursementCategory();
}
