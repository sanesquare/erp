package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.BranchTypeDao;
import com.hrms.web.entities.BranchType;
import com.hrms.web.service.BranchTypeService;
import com.hrms.web.vo.BranchTypeVo;
import com.hrms.web.vo.BranchVo;

/**
 * 
 * @author Vinutha
 * @since 12-March-2015
 *
 */
@Service
public class BranchTypeServiceImpl implements BranchTypeService{

	@Autowired
	private BranchTypeDao branchTypeDao;

	public void saveBranchType(BranchTypeVo branchTypeVo) {
		branchTypeDao.saveBranchType(branchTypeVo);
	}

	public BranchType findAndReturnBranchTypeByName(String branchTypeName) {
		return branchTypeDao.findAndReturnBranchTypeByName(branchTypeName);
	}

	public BranchType findAndReturnBranchTypeById(Long branchTypeId) {
		return branchTypeDao.findAndReturnBranchTypeById(branchTypeId);
	}

	public void updateBranchType(BranchTypeVo branchTypeVo) {
		branchTypeDao.updateBranchType(branchTypeVo);
	}

	public void deleteBranchType(Long branchTypeId) {
		branchTypeDao.deleteBranchType(branchTypeId);
	}

	public BranchTypeVo findBranchType(Long branchTypeId) {
		return branchTypeDao.findBranchType(branchTypeId);
	}

	public BranchTypeVo findBranchType(String branchType) {
		return branchTypeDao.findBranchType(branchType);
	}

	public List<BranchTypeVo> findAllBranchType() {
		return branchTypeDao.findAllBranchType();
	}

	public BranchTypeVo createBranchTypeVoFromBranchVo(BranchVo branchVo) {
		return branchTypeDao.createBranchTypeVoFromBranchVo(branchVo);
	}
	
	
}
