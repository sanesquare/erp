package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TransferDao;
import com.hrms.web.service.TransferService;
import com.hrms.web.vo.TransferVo;
/**
 * 
 * @author Shamsheer
 * @since 24-April-2015
 */
@Service
public class TransferServiceImpl implements TransferService{

	@Autowired
	private TransferDao dao;

	public Long saveTransfer(TransferVo transferVo) {
		return dao.saveTransfer(transferVo);
	}

	public void updateTransfer(TransferVo transferVo) {
		dao.updateTransfer(transferVo);
	}

	public void deleteTransfer(Long transferId) {
		dao.deleteTransfer(transferId);
	}

	public List<TransferVo> listAllTransfer() {
		return dao.listAllTransfer();
	}

	public TransferVo getTransferById(Long transferId) {
		return dao.getTransferById(transferId);
	}

	public List<TransferVo> listTransferByEmployee(Long employeeId) {
		return dao.listTransferByEmployee(employeeId);
	}

	public List<TransferVo> listTransferByEmployee(String employeeCode) {
		return dao.listTransferByEmployee(employeeCode);
	}

	public void updateDocument(TransferVo transferVo) {
		dao.updateDocument(transferVo);
	}

	public void deleteDocument(String url) {
		dao.deleteDocument(url);
	}

	public void updateStatus(TransferVo transferVo) {
		dao.updateStatus(transferVo);
	}

	public List<TransferVo> listTransferBetweenDates(String startDate, String endDate) {
		return dao.listTransferBetweenDates(startDate, endDate);
	}

	public List<TransferVo> listTransferBetweenDatesAndBranch(String startDate, String endDate, Long branchId) {
		return dao.listTransferBetweenDatesAndBranch(startDate, endDate, branchId);
	}

	public List<TransferVo> listTransferBetweenDatesAndDepartment(String startDate, String endDate, Long departmentId) {
		return dao.listTransferBetweenDatesAndDepartment(startDate, endDate, departmentId);
	}

	public List<TransferVo> listTransferByBranchDepartmetnAndBetweenDates(Long branchId, Long departmentId,
			String startDate, String endDate) {
		return dao.listTransferByBranchDepartmetnAndBetweenDates(branchId, departmentId, startDate, endDate);
	}

	public List<TransferVo> findAllTransferByEmployee(String employeeCode) {
		return dao.findAllTransferByEmployee(employeeCode);
	}
}
