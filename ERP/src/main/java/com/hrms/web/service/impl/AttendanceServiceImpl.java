package com.hrms.web.service.impl;

import java.sql.Time;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.AttendanceDao;
import com.hrms.web.entities.Attendance;
import com.hrms.web.service.AttendanceService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AttendanceVo;
import com.hrms.web.vo.BiometricAttendanceVo;
import com.hrms.web.vo.RestBiometricAttendanceVo;

/**
 * 
 * @author shamsheer
 * @since 8-April-2015
 */
@Service
public class AttendanceServiceImpl implements AttendanceService{

	@Autowired
	private AttendanceDao attendanceDao;

	public Long saveAttendance(AttendanceVo vo) {
		return attendanceDao.saveAttendance(vo);
	}

	public void updateAttendance(AttendanceVo vo) {
		attendanceDao.updateAttendance(vo);
	}

	public void deleteAttendance(Long attendanceId) {
		attendanceDao.deleteAttendance(attendanceId);
	}

	public List<AttendanceVo> listAllAttendance() {
		return attendanceDao.listAllAttendance();
	}

	public List<AttendanceVo> listAttendanceByEmployee(Long employeeId) {
		return attendanceDao.listAttendanceByEmployee(employeeId);
	}

	public AttendanceVo getAttendanceByAttendanceId(Long attendanceId) {
		return attendanceDao.getAttendanceByAttendanceId(attendanceId);
	}

	public List<AttendanceVo> listAttendanceByEmployeeAndDate(Long employeeId, String date) {
		return attendanceDao.listAttendanceByEmployeeAndDate(employeeId, date);
	}

	/**
	 * method to calculate employee's worked hours on given date
	 */
	public AttendanceVo calculateHoursWorkedByEmployeeOnDate(Long employeeId, String date) {
		List<Attendance> attendances = attendanceDao.listAttendanceObjectByEmployeeAndDate(employeeId, date);
		if(attendances!=null)
			return calculateHours(attendances);
		else
		return null;
	}

	private AttendanceVo calculateHours(List<Attendance> attendances){
		AttendanceVo attendanceVo = new AttendanceVo();
		String hoursWorked = null;
		Long millis = null;
		Time inTime = null;
		Time outTime = null;
		for(Attendance attendance : attendances){
			attendanceVo.setDate(DateFormatter.convertDateToString(attendance.getDate()));
			if(attendance.getEmployee().getUserProfile()!=null)
				attendanceVo.setEmployee(attendance.getEmployee().getUserProfile().getFirstName()+" "+attendance.getEmployee().getUserProfile().getLastname());
			attendanceVo.setEmployeeCode(attendance.getEmployee().getEmployeeCode());
			attendanceVo.setEmployeeId(attendance.getEmployee().getId());

			inTime = attendance.getSignIn();
			outTime = attendance.getSignOut();
			if(inTime != null && outTime != null){
				if(inTime.before(outTime)){
					if(millis!=null)
						millis = millis + (outTime.getTime() - inTime.getTime());
					else
						millis = (outTime.getTime() - inTime.getTime());
				}else{
					if(millis!=null)
						millis = millis - (inTime.getTime() - outTime.getTime());
					else
						millis = (inTime.getTime() - outTime.getTime());
				}
			}else{
				hoursWorked = "Oops..!! Failed to Calculate.";
			}
		}
		if(millis!=null){
			hoursWorked =  DateFormatter.convertMilliSecondsToHours(millis);
		}
		attendanceVo.setTotalWorkedHours(hoursWorked);
		return attendanceVo;
	}
	
	public List<AttendanceVo> getTotalDaysAttendedByEmployee(Long employeeId, String date) {
		return attendanceDao.getTotalDaysAttendedByEmployee(employeeId, date);
	}

	public Integer listAttendanceObjectByEmployeeAndDate(Long employeeId, String startDate, String endDate) {
		return attendanceDao.listAttendanceObjectByEmployeeAndDate(employeeId, startDate, endDate);
	}

	public List<AttendanceVo> listAttendanceByEmployee(String employeeCode, String date) {
		return attendanceDao.listAttendanceByEmployee(employeeCode, date);
	}

	public AttendanceVo calculateHoursWorkedByEmployeeOnDate(String employeeCode, String date) {
		List<Attendance> attendances = attendanceDao.listAttendanceObjectByEmployeeAndDate(employeeCode, date);
		if(attendances!=null)
			return calculateHours(attendances);
		else
		return null;
	}

	public void saveAttendanceFromBiometric(Set<BiometricAttendanceVo> attendanceVos) {
		attendanceDao.saveAttendanceFromBiometric(attendanceVos);
	}

	public void saveAttendanceFromRestService(Map<String, RestBiometricAttendanceVo> attendanceMap) {
		attendanceDao.saveAttendanceFromRestService(attendanceMap);
	}

}
