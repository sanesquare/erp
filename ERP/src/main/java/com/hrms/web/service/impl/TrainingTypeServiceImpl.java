package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TrainingTypeDao;
import com.hrms.web.entities.TrainingType;
import com.hrms.web.service.TrainingTypeService;
import com.hrms.web.vo.TrainingTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Service
public class TrainingTypeServiceImpl implements TrainingTypeService {

	@Autowired
	private TrainingTypeDao trainingTypeDao;

	public void saveTrainingType(TrainingTypeVo trainingTypeVo) {
		trainingTypeDao.saveTrainingType(trainingTypeVo);
	}

	public void updateTrainingType(TrainingTypeVo trainingTypeVo) {
		trainingTypeDao.updateTrainingType(trainingTypeVo);
	}

	public void deleteTrainingType(Long trainingTypeId) {
		trainingTypeDao.deleteTrainingType(trainingTypeId);
	}

	public TrainingType findTrainingType(Long trainingTypeId) {
		return trainingTypeDao.findTrainingType(trainingTypeId);
	}

	public TrainingType findTrainingType(String trainingType) {
		return trainingTypeDao.findTrainingType(trainingType);
	}

	public TrainingTypeVo findTrainingTypeById(Long trainingTypeId) {
		return trainingTypeDao.findTrainingTypeById(trainingTypeId);
	}

	public TrainingTypeVo findTrainingTypeByType(String trainingType) {
		return trainingTypeDao.findTrainingTypeByType(trainingType);
	}

	public List<TrainingTypeVo> findAllTrainingType() {
		return trainingTypeDao.findAllTrainingType();
	}

}
