package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.JobPostVo;

/**
 * 
 * @author Vinutha
 * @since 19-March-2015
 *
 */
public interface JobPostService {
	/**
	 * method to save job post
	 * @param jobPostVo
	 */
	public void saveJobPost(JobPostVo jobPostVo);
	/**
	 * method to delete job post
	 * @param jobPostVo
	 */
	public void deleteJobPost(JobPostVo jobPostVo);
	/**
	 * method to update job post
	 * @param jobPostVo
	 */
	public void updateJobPost(JobPostVo jobPostVo);
	/**
	 * method to list all job posts
	 * @return
	 */
	public List<JobPostVo> listAllJobPosts();
	/**
	 * method to find job post by id
	 * @param id
	 * @return jobPostVo
	 */
	public JobPostVo findJobPostById(Long id);
	/**
	 * method to save basic info
	 * @param jobPostVo
	 * @return
	 */
	public Long saveBasicInfo(JobPostVo jobPostVo);
	/**
	 * method to save qualification
	 * @param jobPostVo
	 */
	public void saveQualificationInfo(JobPostVo jobPostVo);
	/**
	 * method to save experience info
	 * @param jobPostVo
	 */
	public void saveExperienceInfo(JobPostVo jobPostVo);
	/**
	 * method to save additional info
	 * @param jobPostVo 
	 */
	public void  saveAdditionalInfo(JobPostVo jobPostVo);
	/**
	 * method to save job post description
	 * @param jobPostVo
	 */
	public void savePostDescription(JobPostVo jobPostVo);
	/**
	 * method to update Job Post documents
	 * @param jobPostVo
	 */
	public void updateDocuments(JobPostVo jobPostVo);
	/**
	 * method to delete document
	 * @param url
	 */
	public void deleteDocument(String url);
	/**
	 * method to find job post by department id
	 * @param departmentId
	 * @return List<JobPostVo>
	 */
	public List<JobPostVo> findListOfJobPostByDepartmentId(Long departmentId);
}
