package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.LeavesDao;
import com.hrms.web.service.LeaveService;
import com.hrms.web.vo.LeavesVo;

/**
 * 
 * @author Shamsheer
 * @since 6-April-2015
 */
@Service
public class LeaveServiceImpl implements LeaveService{

	@Autowired
	private LeavesDao leavesDao;
	
	public Long saveLeave(LeavesVo leavesVo) {
		return leavesDao.saveLeave(leavesVo);
	}

	public void updateLeave(LeavesVo leavesVo) {
		leavesDao.updateLeave(leavesVo);
	}

	public void deleteLeave(Long leaveId) {
		leavesDao.deleteLeave(leaveId);
	}

	public LeavesVo getleaveById(Long leaveId) {
		return leavesDao.getleaveById(leaveId);
	}

	public List<LeavesVo> getLeavesByEmployee(Long employeeId) {
		return leavesDao.getLeavesByEmployee(employeeId);
	}

	public List<LeavesVo> getLeavesByType(Long typeId) {
		return leavesDao.getLeavesByType(typeId);
	}

	public List<LeavesVo> listAllLeaves() {
		return leavesDao.listAllLeaves();
	}

	public void updateDocuments(LeavesVo leavesVo) {
		leavesDao.updateDocuments(leavesVo);
	}

	public void deleteDocument(String url) {
		leavesDao.deleteDocument(url);
	}

	public List<LeavesVo> listStatus() {
		return leavesDao.listStatus();
	}

	public List<LeavesVo> getLeavesTakenByEmployee(Long employeeId) {
		return leavesDao.getLeavesTakenByEmployee(employeeId);
	}

	public void updateStatus(LeavesVo vo) {
		leavesDao.updateStatus(vo);
	}

	public List<LeavesVo> findAllLeaveByEmployee(String employeeCode) {
		return leavesDao.findAllLeaveByEmployee(employeeCode);
	}

	public List<LeavesVo> findApprovedLeavesBetweenDates(Long employeeId, String startDate, String endDate) {
		return leavesDao.findApprovedLeavesBetweenDates(employeeId, startDate, endDate);
	}
}
