package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.AssignmentVo;

/**
 * 
 * @author Shamsheer
 * @since 26-March-2015
 */
public interface AssignmentService {

	/**
	 * method to save Assignment
	 * @param AssignmentVo
	 * @return
	 */
	public Long saveAssignment(AssignmentVo assignmentVo);
	
	/**
	 * method to update Assignment
	 * @param AssignmentVo
	 */
	public void updateAssignment(AssignmentVo assignmentVo);
	
	/**
	 * method to delete Assignment
	 * @param id
	 */
	public void deleteAssignment(Long id);
	
	/**
	 * method to find Assignment by Id
	 * @param id
	 * @return
	 */
	public AssignmentVo findAssignmentById(Long id);
	
	/**
	 * method to find Assignment by title
	 * @param title
	 * @return
	 */
	public List<AssignmentVo> findAssignmentByName(String title);
	
	/**
	 * method to list all Assignment
	 * @return
	 */
	public List<AssignmentVo> listAllAssignment();
	
	/**
	 * method to list Assignment by employee
	 * @param employee
	 * @return
	 */
	public List<AssignmentVo > listAssignmentByEmployee(Long  employeeId);
	
	/**
	 * method to delete document
	 * @param url
	 */
	public void deleteDocument(String url);
	
	/**
	 * method to update document
	 * @param AssignmentVo
	 */
	public void updateDocument(AssignmentVo assignmentVo);
}
