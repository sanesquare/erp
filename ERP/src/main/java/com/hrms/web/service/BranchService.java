package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.Branch;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.ProjectVo;

/**
 * 
 * @author Vinutha
 * @since 12-March-2015
 *
 */

public interface BranchService {
	/**
	 * method to save new branch
	 * @param branchVo
	 */
	public Long saveBranch(BranchVo branchVo);
	/**
	 * method to delete existing branch
	 * @param branchVo
	 */
	public void deleteBranch(BranchVo branchVo);
	/**
	 * method to delete branch based on id
	 * @param branchId
	 */
	public void deleteBranchById(Long branchId);
	/**
	 * method to updateBranch
	 * @param branchVo
	 */
	public void updateBranch(BranchVo branchVo);
	/**
	 * method to list branches
	 * @return
	 */
	public List<BranchVo> listAllBranches();
	/**
	 * Method to list all branches other than the one specified
	 * @param id
	 * @return
	 */
	public List<BranchVo> listRemainingBranches(Long id);
	 /**
	  * method to find branch by id
	  * @param branchId
	  * @return
	  */
	public BranchVo findBranchById(Long branchId);
	/**
	 * method to find branch by name
	 * @param branchName
	 * @return
	 */
	public BranchVo findBranchByName(String branchName);
	/**
	 * method to find and return branch object
	 * @param branchId
	 * @return
	 */
	public Branch findAndReturnBranchById(Long branchId);
	/**
	 * method to find and return branch by branch name
	 * @param branchName
	 * @return
	 */
	public Branch findAndReturnBranchByName(String branchName);
	/**
	 * method list branches by branch type
	 * @param branchType
	 * @return
	 */
	public List<BranchVo> listBranchesByBranchType(String branchType);
	/***
	 * method to update branch documents
	 * @param branchVo
	 * @return
	 */
	public BranchVo updateBranchDocument(BranchVo branchVo);
	/**
	 * method to delete uploaded document
	 * @param path
	 */
	public void deleteBranchDocument(String path);
	/**
	 * Method to return branch count
	 * @return int
	 */
	public Long getbranchCount();
	/**
	 * Method to list branches based on start date
	 * @param start
	 * @param end
	 * @return
	 */
	public List<BranchVo> findBranchWithinDateRange(String start , String end);
	
	/**
	 * method to find branches by id
	 * @param ids
	 * @return
	 */
	public List<BranchVo> findBranchesById(List<Long> ids);
}
