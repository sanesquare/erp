package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.DeductionDao;
import com.hrms.web.service.DeductionService;
import com.hrms.web.vo.DeductionVo;

/**
 * @author Shamsheer
 * @since 11-April-2015
 */ 
@Service
public class DeductionServiceImpl implements DeductionService{

	@Autowired
	private DeductionDao deductionDao;

	public Long saveDeduction(DeductionVo deductionVo) {
		return deductionDao.saveDeduction(deductionVo);
	}

	public void updateDeduction(DeductionVo deductionVo) {
		deductionDao.updateDeduction(deductionVo);
	}

	public void deleteDeduction(Long deduction) {
		deductionDao.deleteDeduction(deduction);		
	}

	public DeductionVo getDeductionById(Long deductionId) {
		return deductionDao.getDeductionById(deductionId);
	}

	public List<DeductionVo> listAllDeduction() {
		return deductionDao.listAllDeduction();
	}

	public List<DeductionVo> listDeductionByEmployee(String employeeCode) {
		return deductionDao.listDeductionByEmployee(employeeCode);
	}

	public List<DeductionVo> listDeductionByStatus(Long statusId) {
		return deductionDao.listDeductionByStatus(statusId);
	}

	public DeductionVo getDeductionByEmployeeAndTitle(Long titleId, String employeeCode) {
		return deductionDao.getDeductionByEmployeeAndTitle(titleId, employeeCode);
	}

	public List<DeductionVo> listDeductionByEmployeeStartDateEndDate(Long employeeId, Date startDate, Date endDate) {
		return deductionDao.listDeductionByEmployeeStartDateEndDate(employeeId, startDate, endDate);
	}

	public List<DeductionVo> listDeductionByStartDateEndDate(Date startDate, Date endDate) {
		return deductionDao.listDeductionByStartDateEndDate(startDate, endDate);
	}
}
