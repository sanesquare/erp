package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.CountryDao;
import com.hrms.web.entities.Country;
import com.hrms.web.logger.Log;
import com.hrms.web.service.CountryService;
import com.hrms.web.vo.CountryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
@Service
public class CountryServiceImpl implements CountryService {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private CountryDao countryDao;

	/**
	 * method to save new country
	 * 
	 * @param countryVo
	 */
	public void saveCountry(CountryVo countryVo) {
		logger.info("Inside Country Service >>>> saveCountry......");
		countryDao.saveCountry(countryVo);
	}

	/**
	 * method to update country information
	 * 
	 * @param countryVo
	 */
	public void updateCountry(CountryVo countryVo) {
		logger.info("Inside Country Service >>>> updateCountry......");
		countryDao.updateCountry(countryVo);
	}

	/**
	 * method to delete country
	 * 
	 * @param countryId
	 */
	public void deleteCountry(Long countryId) {
		logger.info("Inside Country Service >>>> deleteCountry......");
		countryDao.deleteCountry(countryId);
	}

	/**
	 * method to find country by id and return country object
	 * 
	 * @param countryId
	 * @return Country
	 */
	public Country findCountry(Long countryId) {
		logger.info("Inside Country Service >>>> findCountry......");
		return countryDao.findCountry(countryId);
	}

	/**
	 * method to find country by id and return countryvo object
	 * 
	 * @param countryId
	 * @return CountryVo
	 */
	public CountryVo findCountryById(Long countryId) {
		logger.info("Inside Country Service >>>> findCountryById......");
		return countryDao.findCountryById(countryId);
	}

	/**
	 * method to find country by name
	 * 
	 * @param countryName
	 * @return CountryVo
	 */
	public CountryVo findCountrybyName(String countryName) {
		logger.info("Inside Country Service >>>> findCountrybyName......");
		return countryDao.findCountrybyName(countryName);
	}

	/**
	 * method to find all country
	 * 
	 * @return List<CountryVo>
	 */
	public List<CountryVo> findAllCountry() {
		logger.info("Inside Country Service >>>> findAllCountry......");
		return countryDao.findAllCountry();
	}

}
