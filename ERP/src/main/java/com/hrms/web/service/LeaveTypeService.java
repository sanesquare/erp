package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.LeaveType;
import com.hrms.web.vo.LeaveTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 26-March-2015
 *
 */
public interface LeaveTypeService {

	/**
	 * method to save LeaveType
	 * 
	 * @param leaveTypeVo
	 */
	public void saveLeaveType(LeaveTypeVo leaveTypeVo);

	/**
	 * method to update LeaveType
	 * 
	 * @param leaveTypeVo
	 */
	public void updateLeaveType(LeaveTypeVo leaveTypeVo);

	/**
	 * method to delete LeaveType
	 * 
	 * @param leaveTypeId
	 */
	public void deleteLeaveType(Long leaveTypeId);

	/**
	 * method to find LeaveType by id
	 * 
	 * @param leaveTypeId
	 * @return LeaveType
	 */
	public LeaveType findLeaveType(Long leaveTypeId);

	/**
	 * method to find LeaveType by type
	 * 
	 * @param leaveType
	 * @return LeaveType
	 */
	public LeaveType findLeaveType(String leaveType);

	/**
	 * method to find LeaveType by id
	 * 
	 * @param leaveTypeId
	 * @return LeaveTypeVo
	 */
	public LeaveTypeVo findLeaveTypeById(Long leaveTypeId);

	/**
	 * method to find LeaveType by type
	 * 
	 * @param leaveType
	 * @return LeaveTypeVo
	 */
	public LeaveTypeVo findLeaveTypeByType(String leaveType);

	/**
	 * method to find all LeaveType
	 * 
	 * @return List<LeaveTypeVo>
	 */
	public List<LeaveTypeVo> findAllLeaveType();
}
