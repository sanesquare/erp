package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.BloodGroupDao;
import com.hrms.web.entities.BloodGroup;
import com.hrms.web.service.BloodGroupService;
import com.hrms.web.vo.BloodGroupVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Service
public class BloodGroupServiceImpl implements BloodGroupService {

	@Autowired
	private BloodGroupDao bloodGroupDao;

	public void saveBloodGroup(BloodGroupVo bloodGroupVo) {
		bloodGroupDao.saveBloodGroup(bloodGroupVo);
	}

	public void updateBloodGroup(BloodGroupVo bloodGroupVo) {
		bloodGroupDao.updateBloodGroup(bloodGroupVo);
	}

	public void deleteBloodGroup(Long groupId) {
		bloodGroupDao.deleteBloodGroup(groupId);
	}

	public BloodGroup findBloodGroup(Long groupId) {
		return bloodGroupDao.findBloodGroup(groupId);
	}

	public BloodGroup findBloodGroup(String groupName) {
		return bloodGroupDao.findBloodGroup(groupName);
	}

	public BloodGroupVo findBloodGroupById(Long groupId) {
		return bloodGroupDao.findBloodGroupById(groupId);
	}

	public BloodGroupVo findBloodGroupByGroup(String groupName) {
		return bloodGroupDao.findBloodGroupByGroup(groupName);
	}

	public List<BloodGroupVo> findAllBloodGroup() {
		return bloodGroupDao.findAllBloodGroup();
	}

}
