package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ContractTypeDao;
import com.hrms.web.entities.ContractType;
import com.hrms.web.logger.Log;
import com.hrms.web.service.ContractTypeService;
import com.hrms.web.vo.ContractTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
@Service
public class ContractTypeServiceImpl implements ContractTypeService {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private ContractTypeDao contractTypeDao;

	/**
	 * method to save new contractType
	 * 
	 * @param contractTypeVo
	 */
	public void saveContractType(ContractTypeVo contractTypeVo) {
		logger.info("Inside ContractType Service >>>> saveContractType........");
		contractTypeDao.saveContractType(contractTypeVo);
	}

	/**
	 * method to update contracttype information
	 * 
	 * @param contractTypeVo
	 */
	public void updateContractType(ContractTypeVo contractTypeVo) {
		logger.info("Inside ContractType Service >>>> updateContractType........");
		contractTypeDao.updateContractType(contractTypeVo);
	}

	/**
	 * method to delete contracttype information
	 * 
	 * @param typeId
	 */
	public void deleteContractType(Long typeId) {
		logger.info("Inside ContractType Service >>>> deleteContractType........");
		contractTypeDao.deleteContractType(typeId);
	}

	/**
	 * method to find contractType by id
	 * 
	 * @param typeId
	 * @return contractType
	 */
	public ContractType findContractType(Long typeId) {
		logger.info("Inside ContractType Service >>>> findContractType........");
		return contractTypeDao.findContractType(typeId);
	}

	/**
	 * method to find contractType by id
	 * 
	 * @param typeId
	 * @return contractTypevo
	 */
	public ContractTypeVo findContractTypeById(Long typeId) {
		logger.info("Inside ContractType Service >>>> findContractTypeById........");
		return contractTypeDao.findContractTypeById(typeId);
	}

	/**
	 * method to find contract type by type
	 * 
	 * @param typeName
	 * @return contractType
	 */
	public ContractType findContractType(String typeName) {
		logger.info("Inside ContractType Service >>>> findContractType........");
		return contractTypeDao.findContractType(typeName);
	}

	/**
	 * method to find contractType by type
	 * 
	 * @param typeName
	 * @return contractType
	 */
	public ContractTypeVo findContractTypeByType(String typeName) {
		logger.info("Inside ContractType Service >>>> findContractTypeByType........");
		return contractTypeDao.findContractTypeByType(typeName);
	}

	/**
	 * method to find all contractType
	 * 
	 * @return List<ContractTypeVo>
	 */
	public List<ContractTypeVo> findAllContractType() {
		logger.info("Inside ContractType Service >>>> findAllContractType........");
		return contractTypeDao.findAllContractType();
	}

}
