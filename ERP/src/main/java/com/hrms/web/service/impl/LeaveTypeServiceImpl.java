package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.LeaveTypeDao;
import com.hrms.web.entities.LeaveType;
import com.hrms.web.service.LeaveTypeService;
import com.hrms.web.vo.LeaveTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 26-March-2015
 *
 */
@Service
public class LeaveTypeServiceImpl implements LeaveTypeService {

	@Autowired
	private LeaveTypeDao leaveTypeDao;

	public void saveLeaveType(LeaveTypeVo leaveTypeVo) {
		leaveTypeDao.saveLeaveType(leaveTypeVo);
	}

	public void updateLeaveType(LeaveTypeVo leaveTypeVo) {
		leaveTypeDao.updateLeaveType(leaveTypeVo);
	}

	public void deleteLeaveType(Long leaveTypeId) {
		leaveTypeDao.deleteLeaveType(leaveTypeId);
	}

	public LeaveType findLeaveType(Long leaveTypeId) {
		return leaveTypeDao.findLeaveType(leaveTypeId);
	}

	public LeaveType findLeaveType(String leaveType) {
		return leaveTypeDao.findLeaveType(leaveType);
	}

	public LeaveTypeVo findLeaveTypeById(Long leaveTypeId) {
		return leaveTypeDao.findLeaveTypeById(leaveTypeId);
	}

	public LeaveTypeVo findLeaveTypeByType(String leaveType) {
		return leaveTypeDao.findLeaveTypeByType(leaveType);
	}

	public List<LeaveTypeVo> findAllLeaveType() {
		return leaveTypeDao.findAllLeaveType();
	}

}
