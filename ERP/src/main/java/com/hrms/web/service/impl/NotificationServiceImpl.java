package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.NotificationDao;
import com.hrms.web.entities.Notification;
import com.hrms.web.entities.NotificationType;
import com.hrms.web.service.NotificationService;
import com.hrms.web.vo.NotificationVo;

@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	public NotificationDao notificationDao;
	
	/**
	 * 
	 */
	public NotificationVo saveNotification(NotificationVo notificationVo) {
		notificationDao.saveNotification(notificationVo);
		return notificationVo;
	}

	public List<NotificationType> fetchAllNotificationType() {
		return notificationDao.fetchAllNotificationType();
	}

	public List<Notification> fetchAllNotifications() {
		return notificationDao.fetchAllNotifications();
	}

	/**
	 * 
	 */
	public int deleteNotificationById(Long notificationId) {
		return notificationDao.deleteNotificationById(notificationId);
	}

	/**
	 * 
	 */
	public NotificationVo fetchNotificationById(Long notificationId) {
		NotificationVo notificationVo = notificationDao.fetchNotificationById(notificationId);
		if (notificationVo == null) {
			notificationVo = new NotificationVo();
		}
		return notificationVo;
	}

}
