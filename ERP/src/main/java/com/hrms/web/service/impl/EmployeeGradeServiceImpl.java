package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EmployeeGradeDao;
import com.hrms.web.entities.EmployeeGrade;
import com.hrms.web.entities.EmployeeWorkShift;
import com.hrms.web.service.EmployeeGradeService;
import com.hrms.web.vo.EmployeeGradeVo;

/**
 * 
 * @author shamsheer
 * @since 18-March-2015
 */
@Service
public class EmployeeGradeServiceImpl implements EmployeeGradeService {

	@Autowired
	private EmployeeGradeDao employeeGradeDao;

	public void saveGrade(EmployeeGradeVo gradeVo) {
		employeeGradeDao.saveGrade(gradeVo);
	}

	public List<EmployeeGrade> listAllGrades() {
		return employeeGradeDao.listAllGrades();
	}

	public List<EmployeeWorkShift> listWorkShift() {
		return employeeGradeDao.listWorkShift();
	}

	public void updateGrade(EmployeeGradeVo gradeVo) {
		employeeGradeDao.updateGrade(gradeVo);
	}

	public void deleteGrade(Long gradeId) {
		employeeGradeDao.deleteGrade(gradeId);
	}

	public EmployeeGrade findGrade(Long gradeId) {
		return employeeGradeDao.findGrade(gradeId);
	}

	public EmployeeGrade findGrade(String grade) {
		return employeeGradeDao.findGrade(grade);
	}

	public EmployeeGradeVo findGradeById(Long gradeId) {
		return employeeGradeDao.findGradeById(gradeId);
	}

	public EmployeeGradeVo findGradeByType(String gradeType) {
		return employeeGradeDao.findGradeByType(gradeType);
	}

	public List<EmployeeGradeVo> findAllGrade() {
		return employeeGradeDao.findAllGrade();
	}

}
