package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.CommissionDao;
import com.hrms.web.entities.Commissions;
import com.hrms.web.service.CommissionService;
import com.hrms.web.vo.CommissionVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
@Service
public class CommissionServiceImpl implements CommissionService{

	@Autowired
	private CommissionDao dao;

	public Long saveCommission(CommissionVo commissionVo) {
		return dao.saveCommission(commissionVo);
	}

	public void updateCommission(CommissionVo commissionVo) {
		dao.updateCommission(commissionVo);
	}

	public void deleteCommission(Long commissionId) {
		dao.deleteCommission(commissionId);
	}

	public List<CommissionVo> listAllCommissiones() {
		return dao.listAllCommissiones();
	}

	public List<CommissionVo> listCommissionesByEmployee(Long employeeId) {
		return dao.listCommissionesByEmployee(employeeId);
	}

	public List<CommissionVo> listCommissionesByTitle(Long commissionTitleId) {
		return dao.listCommissionesByTitle(commissionTitleId);
	}

	public List<CommissionVo> listCommissionesByStatus(Long statusId) {
		return dao.listCommissionesByStatus(statusId);
	}

	public CommissionVo getCommissionById(Long commissionId) {
		return dao.getCommissionById(commissionId);
	}

	public CommissionVo getCommissionByEmployeeAndTitle(String employeeCode, Long commissionTitleId) {
		return dao.getCommissionByEmployeeAndTitle(employeeCode, commissionTitleId);
	}

	public void updateCommissinStatus(CommissionVo commissionVo) {
		dao.updateCommissinStatus(commissionVo);
	}

	public List<CommissionVo> listCommissionesByEmployeeStartDateEndDate(Long employeeId, Date startDate, Date endDate) {
		return dao.listCommissionesByEmployeeStartDateEndDate(employeeId, startDate, endDate);
	}

	public List<CommissionVo> listCommissionesByStartDateEndDate(Date startDate, Date endDate) {
		return dao.listCommissionesByStartDateEndDate(startDate, endDate);
	}

	public List<CommissionVo> findAllCommissionesByEmployee(String employeeCode) {
		return dao.findAllCommissionesByEmployee(employeeCode);
	}
}
