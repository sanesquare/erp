package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.HourlyWagesDao;
import com.hrms.web.service.HourlyWagesService;
import com.hrms.web.vo.HourlyWagesVo;

/**
 * 
 * @author Shamsheer
 * @since 10-April-2015
 */
@Service
public class HourlyWagesServiceImpl implements HourlyWagesService{

	@Autowired
	private HourlyWagesDao wagesDao;
	
	public Long saveHourlyWages(HourlyWagesVo wagesVo) {
		return wagesDao.saveHourlyWages(wagesVo);
	}

	public void updateHourlyWage(HourlyWagesVo wagesVo) {
		wagesDao.updateHourlyWage(wagesVo);
	}

	public void deleteHourlyWage(Long hourlyWageId) {
		wagesDao.deleteHourlyWage(hourlyWageId);
	}

	public List<HourlyWagesVo> listAllHourlyWages() {
		return wagesDao.listAllHourlyWages();
	}

	public HourlyWagesVo getHourlyWagesById(Long hourlyWagesId) {
		return wagesDao.getHourlyWagesById(hourlyWagesId);
	}

	public HourlyWagesVo getHourlyWagesByEmployee(String employeeCode) {
		return wagesDao.getHourlyWagesByEmployee(employeeCode);
	}

	public List<HourlyWagesVo> findAllHourlyWagesByEmployee(String employeeCode) {
		return wagesDao.findAllHourlyWagesByEmployee(employeeCode);
	}

}
