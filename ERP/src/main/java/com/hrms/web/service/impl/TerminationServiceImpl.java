package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TerminationDao;
import com.hrms.web.entities.Termination;
import com.hrms.web.service.TerminationService;
import com.hrms.web.vo.TerminationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
@Service
public class TerminationServiceImpl implements TerminationService {

	@Autowired
	private TerminationDao terminationDao;

	public void saveTermination(TerminationVo terminationVo) {
		terminationDao.saveTermination(terminationVo);
	}

	public void saveTerminationDocuments(TerminationVo terminationVo) {
		terminationDao.saveTerminationDocuments(terminationVo);
	}

	public void updateTermination(TerminationVo terminationVo) {
		terminationDao.updateTermination(terminationVo);
	}

	public void deleteTermination(Long terminationId) {
		terminationDao.deleteTermination(terminationId);
	}

	public void deleteTerminationDocument(Long terminationDocId) {
		terminationDao.deleteTerminationDocument(terminationDocId);
	}

	public Termination findTermination(Long terminationId) {
		return terminationDao.findTermination(terminationId);
	}

	public List<Termination> findTermination(String terminator) {
		return terminationDao.findTermination(terminator);
	}

	public List<Termination> findTermination(Date terminatedOn) {
		return terminationDao.findTermination(terminatedOn);
	}

	public List<Termination> findTermination(Date startDate, Date endDate) {
		return terminationDao.findTermination(startDate, endDate);
	}

	public TerminationVo findTerminationById(Long terminationId) {
		return terminationDao.findTerminationById(terminationId);
	}

	public List<TerminationVo> findTerminationByTerminator(String terminator) {
		return terminationDao.findTerminationByTerminator(terminator);
	}

	public List<TerminationVo> findTerminationByDate(Date terminatedOn) {
		return terminationDao.findTerminationByDate(terminatedOn);
	}

	public List<TerminationVo> findTerminationBetweenDates(Date startDate, Date endDate) {
		return terminationDao.findTerminationBetweenDates(startDate, endDate);
	}

	public List<TerminationVo> findAllTermination() {
		return terminationDao.findAllTermination();
	}

	public TerminationVo findTerminationByEmployee(String employeeCode) {
		return terminationDao.findTerminationByEmployee(employeeCode);
	}

	public List<TerminationVo> findAllTerminationByEmployee(Long employeeId) {
		return terminationDao.findAllTerminationByEmployee(employeeId);
	}

}
