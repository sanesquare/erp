package com.hrms.web.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.hrms.web.vo.PayRollResultVo;
import com.hrms.web.vo.SalaryDailyWagesVo;
import com.hrms.web.vo.SalaryHourlyWageVo;
import com.hrms.web.vo.SalaryMonthlyVo;
import com.hrms.web.vo.SalaryViewVo;
import com.hrms.web.vo.SalaryVo;

/**
 * 
 * @author Shamsheer
 * @since 21-April-2015
 */
public interface SalaryService {

	/**
	 * method to save salary daily wage
	 * @param salaryVo
	 */
	public void saveSalaryDailyWage(SalaryVo salaryVo);
	
	/**
	 * method to save salary hourly wage
	 * @param salaryVo
	 */
	public void saveSalaryHourlyWage(SalaryVo salaryVo);
	
	/**
	 * method to save monthly salary
	 * @param salaryVo
	 */
	public void saveSalaryMonthly(SalaryVo salaryVo);
	
	/**
	 * method to get employee's daily wage salary
	 * @param employeeId
	 * @return
	 */
	public SalaryDailyWagesVo getSalaryDailyWage(String employeeCode);
	
	/**
	 * method to get employee's hourly wage salary
	 * @param employeeId
	 * @return
	 */
	public SalaryHourlyWageVo getSalaryHourlyWage(String employeeCode);
	
	/**
	 * method to get employee's daily wage salary list
	 * @param employeeCode
	 * @return
	 */
	public List<SalaryDailyWagesVo> getSalaryDailyWageListByEmployee(String employeeCode);
	
	/**
	 * method to get list daily wages of employee by start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<SalaryDailyWagesVo> getSalaryDailyWageListByEmployeeStartDateEndDate(Long employeeId , Date startDate , Date endDate);
	
	/**
	 * method to get employee's hourly wages salary list
	 * @param employeeCode
	 * @return
	 */
	public List<SalaryHourlyWageVo> getSalaryHourlyWageListByEmployee(String employeeCode);
	
	/**
	 * method to get list daily wages of employee by start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<SalaryHourlyWageVo> getSalaryHourlyWageListByEmployeeStartDateEndDate(Long employeeId , Date startDate , Date endDate);
	
	/**
	 * method to calculate payroll items
	 * @param grossSalary
	 * @return
	 */
	public SalaryMonthlyVo calculatePayrollItems(BigDecimal grossSalary , String employeeCode);
	
	/**
	 * method to calculate additional payroll items
	 * @param grossSalary
	 * @return
	 */
	public String calculateAdditionalPayrollItems(BigDecimal grossSalary , String title);
	
	/**
	 * method to get salary by employe
	 * @param employeeCode
	 * @return
	 */
	public SalaryMonthlyVo getMonthlySalaryByEmployee(String employeeCode);
	
	/**
	 * method to get list salaries by employee
	 * @param employeeId
	 * @return
	 */
	public List<SalaryMonthlyVo> getSalariesByEmployee(Long employeeId);
	
	/**
	 * method to get salary by employee , start date & end Date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<SalaryMonthlyVo> getMonthlySalariesByEmployeeStartDateEndDate(Long employeeId , Date startDate , Date endDate);
	
	/**
	 * Method to get hourly wage for a day
	 * @param employeeCode
	 * @param date
	 * @return
	 */
	public SalaryHourlyWageVo getHourlyWageForDay(String employeeCode ,Date date);

	/**
	 * Method to get daily wage for a day
	 * @param employeeCode
	 * @param date
	 * @return
	 */
	public SalaryDailyWagesVo getDailyWageForDay(String employeeCode ,Date date);
	
	/**
	 * method to find salary applicable for given date
	 * @param date
	 * @return
	 */
	public SalaryMonthlyVo findSalaryForDate(String date , String employeeCode);
	
	/**
	 * method to list all salaries
	 * @return
	 */
	public List<SalaryViewVo> listAllSalaries();
	
	/**
	 * method to delete salary
	 * @param id
	 * @param type
	 */
	public void deleteSalary(Long id,String type);
}
