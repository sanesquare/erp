package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.CommonDao;
import com.hrms.web.entities.MstReference;
import com.hrms.web.service.CommonService;

@Service
public class CommonServiceImpl implements CommonService{

	@Autowired
	CommonDao commonDao;
	public List<MstReference> fectMstreferneceByType(String refernceType) {
		return commonDao.fectMstreferneceByType(refernceType);
	}

}
