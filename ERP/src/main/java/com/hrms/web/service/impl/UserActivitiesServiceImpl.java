package com.hrms.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.dao.UserActivitiesDao;
import com.hrms.web.logger.Log;
import com.hrms.web.service.SystemLogService;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.util.IPAddress;
import com.hrms.web.vo.UserActivityVo;

@Service
public class UserActivitiesServiceImpl implements UserActivitiesService {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;
	
	/**
	 * method to save UserActivity
	 */
	@Autowired
	UserActivitiesDao userActivitiesDao;
	
	public void saveUserActivity(String activity) {
		logger.info("inside>> UserActivitiesServiceImpl.. saveUserActivity");
		try{
			IPAddress ipAddress=new IPAddress();
			UserActivityVo userActivityVo=new UserActivityVo();
			userActivityVo.setActivity(activity);
			userActivityVo.setPrivateIP(ipAddress.getPrivateIPAddress());
			userActivityVo.setPublicIP(ipAddress.getPublicIPAddress());
			userActivitiesDao.saveUserActivity(userActivityVo);
		}catch(Exception e){
			logger.error("saveUserActivity",e.getStackTrace());
		}
	}

	
	public List<UserActivityVo> listDistinctDatesByMonth() {
		logger.info("inside>> UserActivitiesServiceImpl.. listDistinctDatesByMonth");
		List<UserActivityVo> listOfMonthYear=new ArrayList<UserActivityVo>();
		try{
			List<Object[]> results=userActivitiesDao.listDistinctMonthAndYear();
			for (Object object[] : results) {
				UserActivityVo userActivityVo=new UserActivityVo();
				userActivityVo.setMonthIndex(Integer.parseInt(String.valueOf(object[0])));
				userActivityVo.setMonthName(CommonConstants.MONTH_NAMES[userActivityVo.getMonthIndex()-1]);
				userActivityVo.setYear(Integer.parseInt(String.valueOf(object[1])));
				listOfMonthYear.add(userActivityVo);
			   }
		}catch(Exception e){
			logger.error("listDistinctDatesByMonth",e.getStackTrace());
		}
		return listOfMonthYear;
	}
	
	/**
	 * method to list all user activities of users
	 * @return List<UserActivityVo>
	 */
	public List<UserActivityVo> listAllUserActivities(int month,int year){
			return userActivitiesDao.listAllUserActivities(month,year);
	}

}
