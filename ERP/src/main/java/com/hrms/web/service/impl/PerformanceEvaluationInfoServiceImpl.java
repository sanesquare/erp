package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.PerformanceEvaluationInfoDao;
import com.hrms.web.entities.PerformanceEvaluationInfo;
import com.hrms.web.service.PerformanceEvaluationInfoService;
import com.hrms.web.vo.IndividualEvaluationVo;
import com.hrms.web.vo.PerformanceVo;

/**
 * 
 * @author Vinutha
 * @since 23-May-2015
 *
 */
@Service
public class PerformanceEvaluationInfoServiceImpl implements PerformanceEvaluationInfoService{

	@Autowired
	private PerformanceEvaluationInfoDao dao;
	/**
	 * Method to save performance evaluation
	 * @param performanceVo
	 * @return
	 */
	public Long savePerformanceEvaluation(PerformanceVo performanceVo){
		return dao.savePerformanceEvaluation(performanceVo);
	}
	/**
	 * Method to update performance evaluation
	 * @param performanceVo
	 */
	public void updatePerformanceEvaluation(PerformanceVo performanceVo){
		dao.updatePerformanceEvaluation(performanceVo);
	}
	/**
	 * Method to delete performance evaluation
	 * @param id
	 */
	public void deletePerformanceEvaluation(Long id){
		dao.deletePerformanceEvaluation(id);
	}
	/**
	 * Method to list performance evaluations
	 * @return
	 */
	public List<PerformanceVo> listPerformanceEvaluations(){
		return dao.listPerformanceEvaluations();
	}
	/**
	 * method to return performance evaluation info object by id
	 * @param id
	 * @return
	 */
	public PerformanceEvaluationInfo findAndReturnEvaluationById(Long id){
		return dao.findAndReturnEvaluationById(id);
	}
	/**
	 * method to return performance evaluation info Vo object by id
	 * @param id
	 * @return
	 */
	public PerformanceVo findEvaluationById(Long id){
		return dao.findEvaluationById(id);
	}
	/**
	 * Method to find employee evaluation details
	 * @param employeeCode
	 * @param id
	 * @return
	 */
	public IndividualEvaluationVo finddEmployeeEvaluationInfo(String employeeCode , Long id){
		return dao.finddEmployeeEvaluationInfo(employeeCode, id);
	}
	/**
	 * Method to delete individual evaluation record
	 * @param id
	 */
	public void deleteEmployeeEvaluation(Long id , Long individualId){
		dao.deleteEmployeeEvaluation(id, individualId);
	}
	/**
	 * Method to find performance evaluations within date range
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PerformanceVo> findEvaluationWithinDateRange(String startDate , String endDate){
		return dao.findEvaluationWithinDateRange(startDate, endDate);
	}
}
