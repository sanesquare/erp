package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.JobFieldDao;
import com.hrms.web.entities.JobFields;
import com.hrms.web.service.JobFieldService;
import com.hrms.web.vo.JobFieldVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Service
public class JobFieldServiceImpl implements JobFieldService {

	@Autowired
	private JobFieldDao jobFieldDao;

	/**
	 * method to save new job field
	 * 
	 * @param jobFieldVo
	 */
	public void saveJobField(JobFieldVo jobFieldVo) {
		jobFieldDao.saveJobField(jobFieldVo);
	}

	/**
	 * method to update job field
	 * 
	 * @param jobFieldVo
	 */
	public void updateJobField(JobFieldVo jobFieldVo) {
		jobFieldDao.updateJobField(jobFieldVo);
	}

	/**
	 * method to delete jobfield
	 * 
	 * @param fieldId
	 */
	public void deleteJobField(Long fieldId) {
		jobFieldDao.deleteJobField(fieldId);
	}

	/**
	 * method to find job field
	 * 
	 * @param fieldId
	 * @return jobfield
	 */
	public JobFields findJobFiled(Long fieldId) {
		return jobFieldDao.findJobFiled(fieldId);
	}

	/**
	 * method to find jobfiled
	 * 
	 * @param fieldId
	 * @return jobfieldvo
	 */
	public JobFieldVo findJobFieldById(Long fieldId) {
		return jobFieldDao.findJobFieldById(fieldId);
	}

	/**
	 * method to find job field by name
	 * 
	 * @param fieldName
	 * @return job field
	 */
	public JobFields findJobField(String fieldName) {
		return jobFieldDao.findJobField(fieldName);
	}

	/**
	 * method to find job field by name
	 * 
	 * @param fieldName
	 * @return jobfieldvo
	 */
	public JobFieldVo findJobFieldByName(String fieldName) {
		return jobFieldDao.findJobFieldByName(fieldName);
	}

	/**
	 * method to find all job field
	 * 
	 * @return
	 */
	public List<JobFieldVo> findAllJobField() {
		return jobFieldDao.findAllJobField();
	}

}
