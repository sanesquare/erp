package com.hrms.web.service;

import com.hrms.web.vo.WorksheetTasksVo;

/**
 * 
 * @author Vinutha
 * @since 19-May-2015
 *
 */
public interface WorkSheetTaskService {

	/**
	 * Method to find task by id
	 * @param id
	 * @return
	 */
	public WorksheetTasksVo findTaskById(Long id);
	/**
	 * Method to delete task
	 * @param id
	 */
	public void deleteTaskById(Long id);
}
