package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EmployeeDesignationDao;
import com.hrms.web.entities.EmployeeDesignation;
import com.hrms.web.service.EmployeeDesignationService;
import com.hrms.web.vo.DesignationRankVo;
import com.hrms.web.vo.EmployeeVo;

/**
 * 
 * @author shamsheer
 * @since 18-March-2015
 */
@Service
public class EmployeeDesignationServiceImpl implements EmployeeDesignationService{

	@Autowired
	private EmployeeDesignationDao designationDao;
	
	public void saveDesignation(DesignationRankVo designationRankVo) {
		designationDao.saveDesignation(designationRankVo);
	}

	public void updateDesignation(EmployeeVo employeeVo) {
		designationDao.updateDesignation(employeeVo);
	}

	public void deleteDesignation(Long id) {
		designationDao.deleteDesignation(id);
	}

	public List<EmployeeDesignation> listAllDesignations() {
		return designationDao.listAllDesignations();
	}

	public EmployeeDesignation findDesignationById(Long id) {
		return designationDao.findDesignationById(id);
	}

	public EmployeeDesignation findDesignationByDesignation(String designation) {
		return designationDao.findDesignationByDesignation(designation);
	}

	public List<DesignationRankVo> findAllDesignations() {
		return designationDao.findAllDesignations();
	}

	public EmployeeDesignation findEmployeeDesignation(String designation, Long rank) {
		return designationDao.findEmployeeDesignation(designation, rank);
	}

}
