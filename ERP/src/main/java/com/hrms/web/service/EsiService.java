package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Esi;
import com.hrms.web.vo.EsiVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-April-2015
 *
 */
public interface EsiService {

	/**
	 * 
	 * @param esiVo
	 */
	public void saveOrUpdateEsi(EsiVo esiVo);

	/**
	 * 
	 * @param esiId
	 */
	public void deleteEsi(Long esiId);

	/**
	 * 
	 * @param esiId
	 * @return
	 */
	public Esi findEsi(Long esiId);

	/**
	 * 
	 * @param esiId
	 * @return
	 */
	public EsiVo findEsiById(Long esiId);

	/**
	 * 
	 * @param employee
	 * @return
	 */
	public EsiVo findEsiByEmployee(Employee employee);

	/**
	 * 
	 * @return
	 */
	public List<EsiVo> findAllEsi();

	/**
	 * 
	 * @return
	 */
	public List<EsiVo> findAllEsiByEmployee(String employeeCode);
}
