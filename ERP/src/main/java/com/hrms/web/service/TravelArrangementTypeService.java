package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.TravelArrangementTypeVo;

/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
public interface TravelArrangementTypeService {

	public void saveArrangementType(TravelArrangementTypeVo vo);

	public void updateArrangementType(TravelArrangementTypeVo vo);

	public void deleteArrangementType(Long id);

	public TravelArrangementTypeVo getArrangementTypeById(Long id);
	
	public List<TravelArrangementTypeVo> listAllArrangementType();
}
