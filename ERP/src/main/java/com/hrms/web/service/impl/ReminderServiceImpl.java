package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.RemindersDao;
import com.hrms.web.entities.Reminders;
import com.hrms.web.service.ReminderService;
import com.hrms.web.vo.RemindersVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Service
public class ReminderServiceImpl implements ReminderService {

	@Autowired
	private RemindersDao remindersDao;

	public void saveReminder(RemindersVo remindersVo) {
		remindersDao.saveReminder(remindersVo);
	}

	public void updateReminder(RemindersVo remindersVo) {
		remindersDao.updateReminder(remindersVo);
	}

	public void deleteReminder(Long reminderId) {
		remindersDao.deleteReminder(reminderId);
	}

	public Reminders findReminder(Long reminderId) {
		return remindersDao.findReminder(reminderId);
	}

	public List<Reminders> findReminder(Date reminderDate) {
		return remindersDao.findReminder(reminderDate);
	}

	public RemindersVo findReminderById(Long reminderId) {
		return remindersDao.findReminderById(reminderId);
	}

	public List<RemindersVo> findReminderByDate(Date reminderDate) {
		return remindersDao.findReminderByDate(reminderDate);
	}

	public List<RemindersVo> findAllReminder() {
		return remindersDao.findAllReminder();
	}

}
