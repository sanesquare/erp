package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.InsuranceTypeDao;
import com.hrms.web.entities.InsuranceType;
import com.hrms.web.service.InsuranceTypeService;
import com.hrms.web.vo.InsuranceTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Service
public class InsuranceTypeServiceImpl implements InsuranceTypeService {

	@Autowired
	private InsuranceTypeDao insuranceTypeDao;

	public void saveInsuranceType(InsuranceTypeVo insuranceTypeVo) {
		insuranceTypeDao.saveInsuranceType(insuranceTypeVo);
	}

	public void updateInsuranceType(InsuranceTypeVo insuranceTypeVo) {
		insuranceTypeDao.updateInsuranceType(insuranceTypeVo);
	}

	public void deleteInsuranceType(Long insuranceTypeId) {
		insuranceTypeDao.deleteInsuranceType(insuranceTypeId);
	}

	public InsuranceType findInsuranceType(Long insuranceTypeId) {
		return insuranceTypeDao.findInsuranceType(insuranceTypeId);
	}

	public InsuranceType findInsuranceType(String insuranceType) {
		return insuranceTypeDao.findInsuranceType(insuranceType);
	}

	public InsuranceTypeVo findInsuranceTypeById(Long insuranceTypeId) {
		return insuranceTypeDao.findInsuranceTypeById(insuranceTypeId);
	}

	public InsuranceTypeVo findInsuranceTypeByType(String insuranceType) {
		return insuranceTypeDao.findInsuranceTypeByType(insuranceType);
	}

	public List<InsuranceTypeVo> findAllInsuranceType() {
		return insuranceTypeDao.findAllInsuranceType();
	}

}
