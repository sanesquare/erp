package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.PromotionDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.service.PromotionService;
import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.PromotionVo;

@Service
public class PromotionServiceImpl implements PromotionService{
	
	@Log
	private static Logger logger;
	
	@Autowired
	private PromotionDao promotionDao;

	public void savePromotion(PromotionVo promotionVo) {
		logger.info("inside >> promotionServiceImpl ....savePromotion");
		promotionDao.savePromotion(promotionVo);
		
	}

	public List<PromotionVo> listAllPromotionDetails() {
		return promotionDao.findAllPromotionDetails();
	}

	public void deletePromotion(Long id) {
		logger.info("inside>> PromotionService.. deletePromotion");
		promotionDao.deletePromotion(id);
		
	}

	public PromotionVo findPromotionById(Long promotionId) {
			logger.info("inside>> PromotionService.. findPromotionById");
			return promotionDao.findPromotionById(promotionId);
	}

	public void updatePromotion(PromotionVo promotionVo) {
		logger.info("inside>> PromotionService.. updatePromotion");
		promotionDao.updatePromotion(promotionVo);
		
	}

	public void deletePromotionDocument(Long id) {
		promotionDao.deletePromotionDocument(id);
		
	}

	public PromotionVo updatePromotionDocument(PromotionVo promotionVo) {
		return promotionDao.updatePromotionDocument(promotionVo);
	}
	


}
