package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.JobPostDao;
import com.hrms.web.service.JobPostService;
import com.hrms.web.vo.JobPostVo;

/**
 * 
 * @author Vinutha
 * @since 19-March-2015
 *
 */
@Service
public class JobPostServiceImpl implements JobPostService{

	@Autowired
	private JobPostDao jobPostDao;
	/**
	 * method to save job post
	 * @param jobPostVo
	 */
	public void saveJobPost(JobPostVo jobPostVo) {
		jobPostDao.saveJobPost(jobPostVo);
		
	}
	/**
	 * method to delete jobPost
	 * @param jobpostVo
	 */
	public void deleteJobPost(JobPostVo jobPostVo) {
		jobPostDao.deleteJobPost(jobPostVo);
		
	}
	/**
	 * method to update jobPost
	 * @param jobpostVo
	 */
	public void updateJobPost(JobPostVo jobPostVo) {
		jobPostDao.updateJobPost(jobPostVo);
		
	}
	/**
	 * method to list all job posts
	 */
	public List<JobPostVo> listAllJobPosts() {
		
		return jobPostDao.listAllJobPost();
	}
	/**
	 * method to find job post by id
	 * @param id
	 */
	public JobPostVo findJobPostById(Long id) {
		
		return jobPostDao.findJobPostById(id);
	}
	/**
	 * method to save basic info
	 * @param jobPostVo
	 */
	public Long saveBasicInfo(JobPostVo jobPostVo) {
		
		return jobPostDao.saveBasicInfo(jobPostVo);
	}
	/**
	 * method to save qualification
	 * @param JobPostVo
	 */
	public void saveQualificationInfo(JobPostVo jobPostVo) {
		jobPostDao.saveQualificationInfo(jobPostVo);
		
	}
	/**
	 * method to save experience info
	 * @param JobPostVo
	 */
	public void saveExperienceInfo(JobPostVo jobPostVo) {
		jobPostDao.saveExperienceRequired(jobPostVo);
		
	}
	/**
	 * method to save additional info
	 * @param JobPostVo
	 */
	public void saveAdditionalInfo(JobPostVo jobPostVo) {
		jobPostDao.saveAdditionalInfo(jobPostVo);
		
	}
	/**
	 * method to save post description
	 * @param JobPostVo
	 */
	public void savePostDescription(JobPostVo jobPostVo) {
		jobPostDao.savePostDescription(jobPostVo);
		
	}
	public void updateDocuments(JobPostVo jobPostVo) {
		jobPostDao.updateDocuments(jobPostVo);
		
	}
	public void deleteDocument(String url) {
		jobPostDao.deleteDocument(url);
		
	}
	public List<JobPostVo> findListOfJobPostByDepartmentId(Long departmentId) {
		
		return jobPostDao.findListOfJobPostByDepartmentId(departmentId);
	}

}
