package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.SalaryApprovalStatusVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-09-2015
 */
public interface SalaryApprovalStatusService {

	public List<SalaryApprovalStatusVo> findAllStatus();
}
