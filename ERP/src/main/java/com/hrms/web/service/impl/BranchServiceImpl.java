package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.BranchDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.logger.Log;
import com.hrms.web.service.BranchService;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.ProjectVo;

/**
 * 
 * @author Vinutha
 * @since 12-March-2015
 *
 */
@Service
public class BranchServiceImpl implements BranchService{

	@Autowired
	private BranchDao branchDao;
	
	/**
	 * method to save new branch
	 * @param branchVo
	 */
	public Long saveBranch(BranchVo branchVo) {
		 return branchDao.saveBranch(branchVo);		
	}
	/**
	 * method to delete existing branch
	 * @param branchVo
	 */
	public void deleteBranch(BranchVo branchVo) {
		branchDao.deleteBranch(branchVo);		
	}
	/**
	 * method to update existing branch
	 * @param branchVo
	 */
	public void updateBranch(BranchVo branchVo) {
		branchDao.updateBranch(branchVo);		
	}
	/**
	 * method to list all branches
	 * @return List<BranchVo>
	 */
	public List<BranchVo> listAllBranches() {
		return branchDao.findAllBranches();		
	}
	/**
	 * Method to list all branches other than the one specified
	 * @param id
	 * @return
	 */
	public List<BranchVo> listRemainingBranches(Long id){
		return branchDao.findRemainingBranches(id);
	}
	/**
	 *method to find branch by id
	 *@param Lon branchId
	 *@return BranchVo 
	 */
	public BranchVo findBranchById(Long branchId) {		
		return branchDao.findBranchById(branchId);
	}
	/**
	 * method to find  branch by name
	 * @param String branchName
	 * @return BranchVo
	 * 
	 */
	public BranchVo findBranchByName(String branchName) {
		return branchDao.findBranchByName(branchName);
	}
	/**
	 * method to find and return branch by id
	 * @param Long branchId
	 * @return Branch
	 */
	public Branch findAndReturnBranchById(Long branchId) {
		return branchDao.findAndReturnBranchObjectById(branchId);
	}
	/**
	 * method to find list of branches by branch type
	 * @param BranchType
	 * @return List<BranchVo>
	 */
	public List<BranchVo> listBranchesByBranchType(String branchType) {
		//return branchDao.findBranchesByBranchType(branchType);
		return null;
	}
	/**
	 * method to find and return branch object by name
	 * @param String branchName
	 * @return branch
	 */
	public Branch findAndReturnBranchByName(String branchName) {
		
		return branchDao.findAndReturnBranchObjectByName(branchName);
	}
	/**
	 * method to delete branch by id
	 * @param  Long branchId
	 * 
	 */
	public void deleteBranchById(Long branchId) {
		branchDao.deleteBranchById(branchId);
		
	}
	/**
	 * method to update branch documents
	 * @param branchVo
	 * @return 
	 */
	public BranchVo updateBranchDocument(BranchVo branchVo){
		return branchDao.updateBranchDocument(branchVo);
	}
	/**
	 * Method to delete branch documents
	 * @param path
	 */
	public void deleteBranchDocument(String path) {
		branchDao.deleteBranchDocument(path);
		
	}
	/**
	 * Method to get count of branches
	 * @return int
	 */
	public Long getbranchCount() {
		
		return branchDao.getbranchCount();
	}
	/**
	 * Method to list branches based on start date
	 * @param start
	 * @param end
	 * @return
	 */
	public List<BranchVo> findBranchWithinDateRange(String start , String end){
		return branchDao.findBranchesBasedOnCreationDate(start, end);
	}
	public List<BranchVo> findBranchesById(List<Long> ids) {
		return branchDao.findBranchesByIds(ids);
	}
	
	
}
