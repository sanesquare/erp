package com.hrms.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ReminderStatusDao;
import com.hrms.web.entities.ReminderStatus;
import com.hrms.web.vo.ReminderStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Service
public class ReminderStatusServiceImpl implements ReminderStatusService {

	@Autowired
	private ReminderStatusDao reminderStatusDao;

	public void saveReminderStatus(ReminderStatusVo reminderStatusVo) {
		reminderStatusDao.saveReminderStatus(reminderStatusVo);
	}

	public void updateReminderStatus(ReminderStatusVo reminderStatusVo) {
		reminderStatusDao.updateReminderStatus(reminderStatusVo);
	}

	public void deleteReminderStatus(Long reminderStatusId) {
		reminderStatusDao.deleteReminderStatus(reminderStatusId);
	}

	public ReminderStatus findReminderStatus(Long reminderStatusId) {
		return reminderStatusDao.findReminderStatus(reminderStatusId);
	}

	public ReminderStatus findReminderStatus(String reminderStatus) {
		return reminderStatusDao.findReminderStatus(reminderStatus);
	}

	public ReminderStatusVo findReminderStatusById(Long reminderStatusId) {
		return reminderStatusDao.findReminderStatusById(reminderStatusId);
	}

	public ReminderStatusVo findReminderStatusByStatus(String reminderStatus) {
		return reminderStatusDao.findReminderStatusByStatus(reminderStatus);
	}

	public List<ReminderStatusVo> findAllReminderStatus() {
		return reminderStatusDao.findAllReminderStatus();
	}

}
