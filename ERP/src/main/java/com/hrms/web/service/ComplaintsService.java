package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.ComplaintStatusVo;
import com.hrms.web.vo.ComplaintsVo;


public interface ComplaintsService {
	
	/**
	 * Method to save new Complaints
	 * @param ComplaintsVo
	 * @return void
	 */
	public void saveComplaints(ComplaintsVo omplaintsVo);
	
	/**
	 * method to list complete promotion details
	 * @return
	 */
	public List<ComplaintsVo> listAllComplaintDetails();
	
	/**
	 * method to list complete promotion details
	 * @return
	 */
	public List<ComplaintStatusVo> listAllComplaintStatusDetails();
	
	/**
	 * method to delete existing Complaints
	 * @param ComplaintsVo
	 * @return void
	 */
	public void deleteComplaint(Long id);
	
	/**
	 * method to find Complaints by using Complaints Id
	 * @param id
	 * @return ComplaintsVo
	 */
	public ComplaintsVo findComplaintById(Long complaintId);
	
	/**
	 * method to update existing Complaints
	 * @param ComplaintsVo
	 * @return void
	 */
	public void updateComplaint(ComplaintsVo ComplaintsVo);
	
	/**
	 * method to delete complaints document
	 * 
	 * @param path
	 */
	public void deleteComplaintsDocument(Long id);

}
