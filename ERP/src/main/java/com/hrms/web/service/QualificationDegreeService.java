package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.QualificationDegree;
import com.hrms.web.vo.QualificationDegreeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public interface QualificationDegreeService {

	/**
	 * method to save new qualification degree
	 * 
	 * @param degreeVo
	 */
	public void saveQualificationDegree(QualificationDegreeVo degreeVo);

	/**
	 * method to update qualification information
	 * 
	 * @param degreeVo
	 */
	public void updateQualificationDegree(QualificationDegreeVo degreeVo);

	/**
	 * method to delete qualification information
	 * 
	 * @param degreeId
	 */
	public void deleteQualificationDegree(Long degreeId);

	/**
	 * method to find qualification by id
	 * 
	 * @param degreeId
	 * @return qualificationdegree
	 */
	public QualificationDegree findQualification(Long degreeId);

	/**
	 * method to find qualification information
	 * 
	 * @param degreeId
	 * @return qualificationdegreevo
	 */
	public QualificationDegreeVo findQialificationDegreeById(Long degreeId);

	/**
	 * method to find qualification by name
	 * 
	 * @param degreeName
	 * @return qualificationdegree
	 */
	public QualificationDegree findQualification(String degreeName);

	/**
	 * method to find qualification by name
	 * 
	 * @param degreeName
	 * @return qualificationdegreevo
	 */
	public QualificationDegreeVo findQualificationDegreeByName(String degreeName);

	/**
	 * method to find all qualification
	 * 
	 * @return List<QualificationDegree>
	 */
	public List<QualificationDegreeVo> findAllQualificationDegree();
}
