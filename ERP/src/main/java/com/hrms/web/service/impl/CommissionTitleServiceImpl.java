package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.CommitionTitleDao;
import com.hrms.web.service.CommissionTitleService;
import com.hrms.web.vo.CommissionTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
@Service
public class CommissionTitleServiceImpl implements CommissionTitleService{

	@Autowired
	private CommitionTitleDao dao;

	public void saveCommissionTitle(CommissionTitleVo titleVo) {
		dao.saveCommissionTitle(titleVo);
	}

	public void updateCommissionTitle(CommissionTitleVo titleVo) {
		dao.updateCommissionTitle(titleVo);
	}

	public void deleteCommissionTitle(Long commissionTitleId) {
		dao.deleteCommissionTitle(commissionTitleId);
	}

	public void deleteCommissionTitle(String commissionTitle) {
		dao.deleteCommissionTitle(commissionTitle);
	}

	public List<CommissionTitleVo> listAllCommissionTitles() {
		return dao.listAllCommissionTitles();
	}
}
