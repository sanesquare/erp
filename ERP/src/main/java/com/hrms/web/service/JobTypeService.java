package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.JobType;
import com.hrms.web.vo.JobTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public interface JobTypeService {
	
	/**
	 * method to save new job type
	 * 
	 * @param jobTypeVo
	 */
	public void saveJobType(JobTypeVo jobTypeVo);

	/**
	 * method to update job type
	 * 
	 * @param jobTypeVo
	 */
	public void updateJobType(JobTypeVo jobTypeVo);

	/**
	 * method to delete job type
	 * 
	 * @param typeId
	 */
	public void deleteJobType(Long typeId);

	/**
	 * method to find job type by id
	 * 
	 * @param typeId
	 * @return jobType
	 */
	public JobType findJobType(Long typeId);

	/**
	 * method to find job type by id
	 * 
	 * @param typeId
	 * @return jobTypeVo
	 */
	public JobTypeVo findJobTypeById(Long typeId);

	/**
	 * method to find job type by name
	 * 
	 * @param typeName
	 * @return jobType
	 */
	public JobType findJobType(String typeName);

	/**
	 * method to find job type by name
	 * 
	 * @param typeName
	 * @return jobType
	 */
	public JobTypeVo findJobTypeByType(String typeName);

	/**
	 * method to find all job types
	 * 
	 * @return List<JobTypeVo>
	 */
	public List<JobTypeVo> findAllJobType();
}
