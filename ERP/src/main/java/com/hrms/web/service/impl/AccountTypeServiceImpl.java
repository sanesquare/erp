package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.AccountTypeDao;
import com.hrms.web.entities.AccountType;
import com.hrms.web.service.AccountTypeService;
import com.hrms.web.vo.AccountTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Service
public class AccountTypeServiceImpl implements AccountTypeService {

	@Autowired
	private AccountTypeDao accountTypeDao;

	public void saveAccountType(AccountTypeVo accountTypeVo) {
		accountTypeDao.saveAccountType(accountTypeVo);
	}

	public void updateAccountType(AccountTypeVo accountTypeVo) {
		accountTypeDao.updateAccountType(accountTypeVo);
	}

	public void deleteAccountType(Long accountTypeId) {
		accountTypeDao.deleteAccountType(accountTypeId);
	}

	public AccountType findAccountType(Long accountTypeId) {
		return accountTypeDao.findAccountType(accountTypeId);
	}

	public AccountType findAccountType(String typeName) {
		return accountTypeDao.findAccountType(typeName);
	}

	public AccountTypeVo findAccountTypeById(Long accountTypeId) {
		return accountTypeDao.findAccountTypeById(accountTypeId);
	}

	public AccountTypeVo findAccountTypeByType(String typeName) {
		return accountTypeDao.findAccountTypeByType(typeName);
	}

	public List<AccountTypeVo> findAllAccountType() {
		return accountTypeDao.findAllAccountType();
	}

}
