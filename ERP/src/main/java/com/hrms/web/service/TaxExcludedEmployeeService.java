package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.TaxExcludeEmployee;
import com.hrms.web.vo.TaxExcludedEmployeeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 28-April-2015
 *
 */
public interface TaxExcludedEmployeeService {

	/**
	 * 
	 * @param taxExcludedEmployeeVo
	 */
	public void saveTaxExcludedEmployee(TaxExcludedEmployeeVo taxExcludedEmployeeVo);

	/**
	 * 
	 * @param taxExcludedEmployeeVo
	 */
	public void updateTaxExcludedEmployee(TaxExcludedEmployeeVo taxExcludedEmployeeVo);

	/**
	 * 
	 * @param taxExcludedId
	 */
	public void deleteTaxExcludedEmployee(Long taxExcludedId);

	/**
	 * 
	 * @param taxExludedId
	 * @return
	 */
	public TaxExcludeEmployee findTaxExcludedEmployee(Long taxExludedId);

	/**
	 * 
	 * @param employeeCode
	 * @return
	 */
	public TaxExcludeEmployee findTaxExcludedEmployee(String employeeCode);

	/**
	 * 
	 * @param taxExcludedId
	 * @return
	 */
	public TaxExcludedEmployeeVo findTaxExcludedEmployeeById(Long taxExcludedId);

	/**
	 * 
	 * @param employeeCode
	 * @return
	 */
	public TaxExcludedEmployeeVo findTaxExcludedEmployeeByEmployee(String employeeCode);

	/**
	 * 
	 * @return
	 */
	public TaxExcludedEmployeeVo findTaxExcludedEmployeeList();

	/**
	 * 
	 * @return
	 */
	public List<TaxExcludedEmployeeVo> findAllTaxExcludedEmployee();
}
