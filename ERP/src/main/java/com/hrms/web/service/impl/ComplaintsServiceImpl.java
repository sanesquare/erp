package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ComplaintsDao;
import com.hrms.web.logger.Log;
import com.hrms.web.service.ComplaintsService;
import com.hrms.web.vo.ComplaintStatusVo;
import com.hrms.web.vo.ComplaintsVo;


@Service
public class ComplaintsServiceImpl implements ComplaintsService{

	
	@Log
	private static Logger logger;
	
	@Autowired
	private ComplaintsDao complaintsDao;

	public void deleteComplaint(Long id) {
		logger.info("inside>> ComplaintsService.. deleteComplaints");
		complaintsDao.deleteComplaint(id);
		
	}

	public void saveComplaints(ComplaintsVo complaintsVo) {
		logger.info("inside >> ComplaintsServiceImpl ....saveComplaints");
		complaintsDao.saveComplaint(complaintsVo);
		
	}

	public List<ComplaintsVo> listAllComplaintDetails() {
		return complaintsDao.findAllComplaintDetails();
	}

	
	public List<ComplaintStatusVo> listAllComplaintStatusDetails() {
		return complaintsDao.findAllComplaintStatusDetails();
	}

	public ComplaintsVo findComplaintById(Long complaintId) {
		logger.info("inside>> ComplaintsService.. findComplaintsById");
		return complaintsDao.findComplaintById(complaintId);
	}

	public void updateComplaint(ComplaintsVo ComplaintsVo) {
		logger.info("inside>> ComplaintsService.. updateComplaints");
		complaintsDao.updateComplaint(ComplaintsVo);
		
	}

	public void deleteComplaintsDocument(Long id) {
		complaintsDao.deleteComplaintsDocument(id);
		
	}
}
