package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ProfidentFundDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ProfidentFund;
import com.hrms.web.service.ProfidentFundService;
import com.hrms.web.vo.ProfidentFundVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-April-2015
 *
 */
@Service
public class ProfidentFundServiceImpl implements ProfidentFundService {

	@Autowired
	private ProfidentFundDao profidentFundDao;

	public void saveOrUpdateProfidentFund(ProfidentFundVo profidentFundVo) {
		profidentFundDao.saveOrUpdateProfidentFund(profidentFundVo);
	}

	public void deleteProfidentFund(Long profidentFundId) {
		profidentFundDao.deleteProfidentFund(profidentFundId);
	}

	public ProfidentFund findProfidentFund(Long profidentFundId) {
		return profidentFundDao.findProfidentFund(profidentFundId);
	}

	public ProfidentFundVo findProfidentFundById(Long profidentFundId) {
		return profidentFundDao.findProfidentFundById(profidentFundId);
	}

	public ProfidentFundVo findProfidentFundByEmployee(Employee employee) {
		return profidentFundDao.findProfidentFundByEmployee(employee);
	}

	public List<ProfidentFundVo> findAllProfidentFund() {
		return profidentFundDao.findAllProfidentFund();
	}

	public List<ProfidentFundVo> findProfidentFundBetweenDate(Long employeeId, Date startDate, Date endDate) {
		return profidentFundDao.findProfidentFundBetweenDate(employeeId, startDate, endDate);
	}

	public List<ProfidentFundVo> findAllProfidentFundByEmployee(String employeeCode) {
		return profidentFundDao.findAllProfidentFundByEmployee(employeeCode);
	}

}
