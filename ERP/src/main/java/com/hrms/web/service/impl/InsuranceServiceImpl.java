package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.InsuranceDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Insurance;
import com.hrms.web.entities.InsuranceType;
import com.hrms.web.service.InsuranceService;
import com.hrms.web.vo.InsuranceVo;

/**
 * 
 * @author Jithin Mohan
 * @since 10-April-2015
 *
 */
@Service
public class InsuranceServiceImpl implements InsuranceService {

	@Autowired
	private InsuranceDao insuranceDao;

	public void saveOrUpdateInsurance(InsuranceVo insuranceVo) {
		insuranceDao.saveOrUpdateInsurance(insuranceVo);
	}

	public void deleteInsurance(Long insuranceId) {
		insuranceDao.deleteInsurance(insuranceId);
	}

	public Insurance findInsurance(Long insuranceId) {
		return insuranceDao.findInsurance(insuranceId);
	}

	public InsuranceVo findInsuranceById(Long insuranceId) {
		return insuranceDao.findInsuranceById(insuranceId);
	}

	public InsuranceVo findInsuranceByEmployee(Employee employee) {
		return insuranceDao.findInsuranceByEmployee(employee);
	}

	public List<InsuranceVo> findInsuranceByType(InsuranceType insuranceType) {
		return insuranceDao.findInsuranceByType(insuranceType);
	}

	public List<InsuranceVo> findAllInsurance() {
		return insuranceDao.findAllInsurance();
	}

}
