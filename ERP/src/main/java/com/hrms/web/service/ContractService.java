package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.ContractDocuments;
import com.hrms.web.entities.Employee;
import com.hrms.web.vo.ContractJsonVo;
import com.hrms.web.vo.ContractVo;

/**
 * 
 * @author Shamsheer
 * @since 25-March-2015
 */
public interface ContractService {

	/**
	 * method to save contractVo
	 * @param contractVo
	 * @return
	 */
	public Long saveContract(ContractJsonVo contractVo);
	
	/**
	 * method to update Contract
	 * @param contractVo
	 */
	public void updateContract(ContractJsonVo contractVo);
	
	/**
	 * method to delete contract
	 * @param id
	 */
	public void deleteContract(Long id);
	
	/**
	 * method to find contract by Id
	 * @param id
	 * @return
	 */
	public ContractVo findContractById(Long id);
	
	/**
	 * method to find contract by title
	 * @param title
	 * @return
	 */
	public List<ContractVo> findContractByTitle(String title);
	
	/**
	 * method to list all contracts
	 * @return
	 */
	public List<ContractVo> listAllContracts();
	
	/**
	 * method to list contract by employee
	 * @param employee
	 * @return
	 */
	public List<ContractVo > listContractByEmployee(Long  employeeId);
	
	
	/**
	 * method to delete document
	 * @param url
	 */
	public void deleteDocument(String url);
	
	/**
	 * method to update document
	 * @param contractVo
	 */
	public void updateDocument(ContractVo contractVo);
}
