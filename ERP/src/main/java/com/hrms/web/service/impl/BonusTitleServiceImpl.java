package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.BonusTitleDao;
import com.hrms.web.service.BonusTitleService;
import com.hrms.web.vo.BonusTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
@Service
public class BonusTitleServiceImpl implements BonusTitleService{

	@Autowired
	private BonusTitleDao bonusTitleDao;

	public void saveBonusTitle(BonusTitleVo titleVo) {
		bonusTitleDao.saveBonusTitle(titleVo);
	}

	public void updateBonusTitle(BonusTitleVo titleVo) {
		bonusTitleDao.updateBonusTitle(titleVo);
	}

	public void deleteBonusTitle(Long bonusTitleId) {
		bonusTitleDao.deleteBonusTitle(bonusTitleId);
	}

	public void deleteBonusTitle(String bonusTitle) {
		bonusTitleDao.deleteBonusTitle(bonusTitle);
	}

	public List<BonusTitleVo> listAllBonusTitles() {
		return bonusTitleDao.listAllBonusTitles();
	}
}
