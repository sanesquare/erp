package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.TimeZone;
import com.hrms.web.vo.TimeZoneVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public interface TimeZoneService {

	/**
	 * method to save new TimeZone
	 * 
	 * @param timeZoneVo
	 */
	public void saveTimeZone(TimeZoneVo timeZoneVo);

	/**
	 * method to update TimeZone
	 * 
	 * @param timeZoneVo
	 */
	public void updateTimeZone(TimeZoneVo timeZoneVo);

	/**
	 * method to delete TimeZone
	 * 
	 * @param timeZoneId
	 */
	public void deleteTimeZone(Long timeZoneId);

	/**
	 * method to find TimeZone by id
	 * 
	 * @param timeZoneId
	 * @return TimeZone
	 */
	public TimeZone findTimeZone(Long timeZoneId);

	/**
	 * method to find TimeZone by location
	 * 
	 * @param location
	 * @return TimeZone
	 */
	public TimeZone findTimeZone(String location);

	/**
	 * method to find TimeZone by id
	 * 
	 * @param timeZoneId
	 * @return TimeZoneVo
	 */
	public TimeZoneVo findTimeZoneById(Long timeZoneId);

	/**
	 * method to find TimeZone by location
	 * 
	 * @param location
	 * @return TimeZoneVo
	 */
	public TimeZoneVo findTimeZoneByLocation(String location);

	/**
	 * method to find all TimeZone
	 * 
	 * @return List<TimeZoneVo>
	 */
	public List<TimeZoneVo> findAllTimeZone();
}
