package com.hrms.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.LwfDao;
import com.hrms.web.entities.Lwf;
import com.hrms.web.service.LwfService;
import com.hrms.web.vo.LwfVo;

/**
 * 
 * @author Jithin Mohan
 *
 */
@Service
public class LwfServiceImpl implements LwfService {

	@Autowired
	private LwfDao lwfDao;

	public void saveLwf(LwfVo lwfVo) {
		lwfDao.saveLwf(lwfVo);
	}

	public Lwf findLwf(Long id) {
		return lwfDao.findLwf(id);
	}

	public LwfVo findLwfById(Long id) {
		return lwfDao.findLwfById(id);
	}

	public LwfVo findLwf() {
		return lwfDao.findLwf();
	}

}
