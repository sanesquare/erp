package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Employee;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.ProjectVo;

/**
 * 
 * @author Sangeeth and Bibin
 *
 */

public interface DepartmentService {
	/**
	 * Method to save new Department
	 * 
	 * @param departmentVo
	 * @return void
	 */
	public void saveDepartment(DepartmentVo departmentVo);

	/**
	 * method to list departments
	 * 
	 * @return
	 */
	public List<DepartmentVo> listAllDepartments();
	
	
	/**
	 * method to update department document
	 * @param projectVo
	 */
	public DepartmentVo updateDepartmentDocument(DepartmentVo departmentVo);

	/**
	 * method to list complete department details
	 * 
	 * @return
	 */
	public List<DepartmentVo> listAllDepartmentDetails();

	/**
	 * method to find department by using department Id
	 * 
	 * @param id
	 * @return DepartmentVo
	 */
	public DepartmentVo findDepartmetById(Long departmentId);

	/**
	 * method to delete existing department
	 * 
	 * @param departmentVo
	 * @return void
	 */
	public void deleteDepartment(Long id);

	/**
	 * method to update existing department
	 * 
	 * @param departmentVo
	 * @return void
	 */
	public void updateDepartment(DepartmentVo departmentVo);

	/**
	 * method to delete department document
	 * 
	 * @param path
	 */
	public void deleteDepartmentDocument(Long id);

	/**
	 * 
	 * @param branch
	 * @return
	 */
	public List<DepartmentVo> findDepartmentByBranch(Branch branch);

}
