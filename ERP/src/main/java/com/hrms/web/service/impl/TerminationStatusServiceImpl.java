package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TerminationStatusDao;
import com.hrms.web.entities.TerminationStatus;
import com.hrms.web.service.TerminationStatusService;
import com.hrms.web.vo.TerminationStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
@Service
public class TerminationStatusServiceImpl implements TerminationStatusService {

	@Autowired
	private TerminationStatusDao terminationStatusDao;

	public void saveTerminationStatus(TerminationStatusVo terminationStatusVo) {
		terminationStatusDao.saveTerminationStatus(terminationStatusVo);
	}

	public void updateTerminationStatus(TerminationStatusVo terminationStatusVo) {
		terminationStatusDao.updateTerminationStatus(terminationStatusVo);
	}

	public void deleteTerminationStatus(Long terminationStatusId) {
		terminationStatusDao.deleteTerminationStatus(terminationStatusId);
	}

	public TerminationStatus findTerminationStatus(Long terminationStatusId) {
		return terminationStatusDao.findTerminationStatus(terminationStatusId);
	}

	public TerminationStatus findTerminationStatus(String status) {
		return terminationStatusDao.findTerminationStatus(status);
	}

	public TerminationStatusVo findTerminationStatusById(Long terminationStatusId) {
		return terminationStatusDao.findTerminationStatusById(terminationStatusId);
	}

	public TerminationStatusVo findTerminationStatusByStatus(String status) {
		return terminationStatusDao.findTerminationStatusByStatus(status);
	}

	public List<TerminationStatusVo> findAllTerminationStatus() {
		return terminationStatusDao.findAllTerminationStatus();
	}

}
