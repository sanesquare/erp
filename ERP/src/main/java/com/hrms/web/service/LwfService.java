package com.hrms.web.service;

import com.hrms.web.entities.Lwf;
import com.hrms.web.vo.LwfVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-May-2015
 *
 */
public interface LwfService {

	/**
	 * 
	 * @param lwfVo
	 */
	public void saveLwf(LwfVo lwfVo);

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Lwf findLwf(Long id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	public LwfVo findLwfById(Long id);

	/**
	 * 
	 * @return
	 */
	public LwfVo findLwf();
}
