package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ShiftDayDao;
import com.hrms.web.entities.ShiftDay;
import com.hrms.web.service.ShiftDayService;
import com.hrms.web.vo.ShiftDayVo;

/**
 * 
 * @author Jithin Mohan
 * @since 31-March-2015
 *
 */
@Service
public class ShiftDayServiceImpl implements ShiftDayService {

	@Autowired
	private ShiftDayDao shiftDayDao;

	public void saveShiftDay(ShiftDayVo shiftDayVo) {
		shiftDayDao.saveShiftDay(shiftDayVo);
	}

	public void updateShiftDay(ShiftDayVo shiftDayVo) {
		shiftDayDao.updateShiftDay(shiftDayVo);
	}

	public void deleteShiftDay(Long shiftDayId) {
		shiftDayDao.deleteShiftDay(shiftDayId);
	}

	public ShiftDay findShiftDay(Long shiftDayId) {
		return shiftDayDao.findShiftDay(shiftDayId);
	}

	public ShiftDay findShiftDay(String shiftDay) {
		return shiftDayDao.findShiftDay(shiftDay);
	}

	public ShiftDayVo findShiftDayById(Long shiftDayId) {
		return shiftDayDao.findShiftDayById(shiftDayId);
	}

	public ShiftDayVo findShiftDayByDay(String shiftDay) {
		return shiftDayDao.findShiftDayByDay(shiftDay);
	}

	public List<ShiftDayVo> findAllShiftDay() {
		return shiftDayDao.findAllShiftDay();
	}

}
