package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.Notification;
import com.hrms.web.entities.NotificationType;
import com.hrms.web.vo.NotificationVo;

/**
 * 
 * @author Vips
 *
 */
public interface NotificationService {

 /**
  * 
  * @param notificationVo
  * @return NotificationVo
  */
 public NotificationVo saveNotification(NotificationVo notificationVo);
 
 /**
  * 
  * @return
  */
 public List<NotificationType> fetchAllNotificationType();
 
 /**
  * 
  * @param notificationVo
  * @return NotificationVo
  */
 public List<Notification> fetchAllNotifications();
 
 /**
  * 
  * @param notificationId
  * @return
  */
 public int deleteNotificationById(Long notificationId);
 
 /**
  * 
  * @param notificationId
  * @return
  */
 public NotificationVo fetchNotificationById(Long notificationId);
 
}
