package com.hrms.web.service;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ProfidentFund;
import com.hrms.web.vo.ProfidentFundVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-April-2015
 *
 */
public interface ProfidentFundService {

	/**
	 * 
	 * @param profidentFundVo
	 */
	public void saveOrUpdateProfidentFund(ProfidentFundVo profidentFundVo);

	/**
	 * 
	 * @param profidentFundId
	 */
	public void deleteProfidentFund(Long profidentFundId);

	/**
	 * 
	 * @param profidentFundId
	 * @return
	 */
	public ProfidentFund findProfidentFund(Long profidentFundId);

	/**
	 * 
	 * @param profidentFundId
	 * @return
	 */
	public ProfidentFundVo findProfidentFundById(Long profidentFundId);

	/**
	 * 
	 * @param employee
	 * @return
	 */
	public ProfidentFundVo findProfidentFundByEmployee(Employee employee);

	/**
	 * 
	 * @return
	 */
	public List<ProfidentFundVo> findAllProfidentFund();

	/**
	 * 
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ProfidentFundVo> findProfidentFundBetweenDate(Long employeeId, Date startDate, Date endDate);

	/**
	 * 
	 * @return
	 */
	public List<ProfidentFundVo> findAllProfidentFundByEmployee(String employeeCode);
}
