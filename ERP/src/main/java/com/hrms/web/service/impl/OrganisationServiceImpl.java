package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.OrganisationDao;
import com.hrms.web.entities.Organisation;
import com.hrms.web.service.OrganisationService;
import com.hrms.web.vo.OrganisationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-March-2015
 *
 */
@Service
public class OrganisationServiceImpl implements OrganisationService {

	@Autowired
	private OrganisationDao organisationDao;

	public void saveOrganisation(OrganisationVo organisationVo) {
		organisationDao.saveOrganisation(organisationVo);
	}

	public void updateOrganisation(OrganisationVo organisationVo) {
		organisationDao.updateOrganisation(organisationVo);
	}

	public void deleteOrganisation(Long organisationId) {
		organisationDao.deleteOrganisation(organisationId);
	}

	public Organisation findOrganisation(Long organisationId) {
		return organisationDao.findOrganisation(organisationId);
	}

	public Organisation findOrganisation(String organisationCode) {
		return organisationDao.findOrganisation(organisationCode);
	}

	public Organisation findOrganisationByUrt(String organisationUrt) {
		return organisationDao.findOrganisationByUrt(organisationUrt);
	}

	public Organisation findOrganisationByName(String organisationName) {
		return organisationDao.findOrganisationByName(organisationName);
	}

	public OrganisationVo findOrganisationById(Long organisationId) {
		return organisationDao.findOrganisationById(organisationId);
	}

	public OrganisationVo findOrganisationByCode(String organisationCode) {
		return organisationDao.findOrganisationByCode(organisationCode);
	}

	public OrganisationVo findOrganisationByOrgUrt(String organisationUrt) {
		return organisationDao.findOrganisationByOrgUrt(organisationUrt);
	}

	public OrganisationVo findOrganisationByOrgName(String organisationName) {
		return organisationDao.findOrganisationByOrgName(organisationName);
	}

	public List<OrganisationVo> findAllOrganisation() {
		return organisationDao.findAllOrganisation();
	}

	public OrganisationVo findOrganisation() {
		return organisationDao.findOrganisation();
	}

}
