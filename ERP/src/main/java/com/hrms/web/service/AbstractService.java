package com.hrms.web.service;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public class AbstractService {

	@Autowired
	protected CountryService countryService;

	@Autowired
	protected LanguageService languageService;

	@Autowired
	protected SkillService skillService;

	@Autowired
	protected QualificationDegreeService qualificationService;

	@Autowired
	protected ContractTypeService contractTypeService;

	@Autowired
	protected JobTypeService jobTypeService;

	@Autowired
	protected JobFieldService jobFieldService;

	@Autowired
	protected BaseCurrencyService baseCurrencyService;

	@Autowired
	protected CurrencyService currencyService;

	@Autowired
	protected TimeZoneService timeZoneService;

	@Autowired
	protected PolicyTypeService policyTypeService;

	@Autowired
	protected EmployeeTypeService employeeTypeService;

	@Autowired
	protected EmployeeCategoryService employeeCategoryService;

	@Autowired
	protected InsuranceTypeService insuranceTypeService;

	@Autowired
	protected BranchTypeService branchTypeService;

	@Autowired
	protected TrainingTypeService trainingTypeService;

	@Autowired
	protected TrainingEventTypeService trainingEventTypeService;

	@Autowired
	protected ReimbursementCategoryService reimbursementCategoryService;

	@Autowired
	protected ManagerReportingNotificationService managerReportingNotificationService;

	@Autowired
	protected ReminderStatusService reminderStatusService;

	@Autowired
	protected ReminderService reminderService;

	@Autowired
	protected BloodGroupService bloodGroupService;

	@Autowired
	protected RelegionService relegionService;

	@Autowired
	protected AccountTypeService accountTypeService;

	@Autowired
	protected WorkShiftService workShiftService;

	@Autowired
	protected EmployeeStatusService employeeStatusService;

	@Autowired
	protected OrganisationAssetsService assetsService;

	@Autowired
	protected TerminationStatusService terminationStatusService;

	@Autowired
	protected PromotionStatusService promotionStatusService;

	@Autowired
	protected EmployeeExitTypeService employeeExitTypeService;

	@Autowired
	protected LeaveTypeService leaveTypeService;

	@Autowired
	protected EmployeeGradeService employeeGradeService;

	@Autowired
	protected EmployeeDesignationService employeeDesignationService;

	@Autowired
	protected ShiftDayService shiftDayService;

	@Autowired
	protected AdvanceSalaryStatusService advanceSalaryStatusService;

	@Autowired
	protected OvertimeStatusService overtimeStatusService;

	@Autowired
	protected LoanStatusService loanStatusService;

	@Autowired
	protected SecondLevelForwaderService secondLevelForwaderService;
	
	@Autowired
	protected OrganisationService organisationService;
}
