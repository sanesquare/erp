package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.OvertimeDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Overtime;
import com.hrms.web.entities.OvertimeStatus;
import com.hrms.web.service.OvertimeService;
import com.hrms.web.vo.OvertimeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Service
public class OvertimeServiceImpl implements OvertimeService {

	@Autowired
	private OvertimeDao overtimeDao;

	public void saveOrUpdateOvertime(OvertimeVo overtimeVo) {
		overtimeDao.saveOrUpdateOvertime(overtimeVo);
	}

	public void updateOvertimeStatus(OvertimeVo overtimeVo) {
		overtimeDao.updateOvertimeStatus(overtimeVo);
	}

	public void deleteOvertime(Long overtimeId) {
		overtimeDao.deleteOvertime(overtimeId);
	}

	public Overtime findOvertime(Long overtimeId) {
		return overtimeDao.findOvertime(overtimeId);
	}

	public OvertimeVo findOvertimeById(Long overtimeId) {
		return overtimeDao.findOvertimeById(overtimeId);
	}

	public OvertimeVo findOvertimeByEmployee(Employee employee) {
		return overtimeDao.findOvertimeByEmployee(employee);
	}

	public List<OvertimeVo> findOvertimeByDate(Date requestedDate) {
		return overtimeDao.findOvertimeByDate(requestedDate);
	}

	public List<OvertimeVo> findOvertimeBetweenDates(Date startDate, Date endDate) {
		return overtimeDao.findOvertimeBetweenDates(startDate, endDate);
	}

	public List<OvertimeVo> findOvertimeByStatus(OvertimeStatus status) {
		return overtimeDao.findOvertimeByStatus(status);
	}

	public List<OvertimeVo> findOvertimeByTimeIn(Date timeIn) {
		return overtimeDao.findOvertimeByTimeIn(timeIn);
	}

	public List<OvertimeVo> findOvertimeByTimeOut(Date timeOut) {
		return overtimeDao.findOvertimeByTimeOut(timeOut);
	}

	public List<OvertimeVo> findOvertimeBetweenInAndOut(Date timeIn, Date timeOut) {
		return overtimeDao.findOvertimeBetweenInAndOut(timeIn, timeOut);
	}

	public List<OvertimeVo> findAllOvertime() {
		return overtimeDao.findAllOvertime();
	}

	public List<OvertimeVo> findOvertimeBetweenDates(Long employeeId, Date startDate, Date endDate) {
		return overtimeDao.findOvertimeBetweenDates(employeeId, startDate, endDate);
	}

	public List<OvertimeVo> findAllOvertimeByEmployee(String employeeCode) {
		return overtimeDao.findAllOvertimeByEmployee(employeeCode);
	}

	public List<OvertimeVo> findAllOvertimeBetweenDatesAndStatus(String employeeCode, String status, Date startDate,
			Date endDate) {
		return overtimeDao.findAllOvertimeBetweenDatesAndStatus(employeeCode, status, startDate, endDate);
	}

}
