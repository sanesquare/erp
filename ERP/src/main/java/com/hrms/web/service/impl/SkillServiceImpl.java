package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.SkillDao;
import com.hrms.web.entities.Skills;
import com.hrms.web.logger.Log;
import com.hrms.web.service.SkillService;
import com.hrms.web.vo.SkillVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
@Service
public class SkillServiceImpl implements SkillService {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private SkillDao skillDao;

	/**
	 * method to save new skill
	 * 
	 * @param skillVo
	 */
	public void saveSkill(SkillVo skillVo) {
		logger.info("Inside Skill Service >>> saveSkill....");
		skillDao.saveSkill(skillVo);
	}

	/**
	 * method to update skill information
	 * 
	 * @param skillVo
	 */
	public void updateSkill(SkillVo skillVo) {
		logger.info("Inside Skill Service >>> updateSkill....");
		skillDao.updateSkill(skillVo);
	}

	/**
	 * method to delete skill information
	 * 
	 * @param skillId
	 */
	public void deleteSkill(Long skillId) {
		logger.info("Inside Skill Service >>> deleteSkill....");
		skillDao.deleteSkill(skillId);
	}

	/**
	 * method to find skill by id and return skill object
	 * 
	 * @param skillId
	 * @return skills
	 */
	public Skills findSkill(Long skillId) {
		logger.info("Inside Skill Service >>> findSkill....");
		return skillDao.findSkill(skillId);
	}

	/**
	 * method to find skill by id and return skillvo object
	 * 
	 * @param skillId
	 * @return skillvo
	 */
	public SkillVo findSkillById(Long skillId) {
		logger.info("Inside Skill Service >>> findSkillById....");
		return skillDao.findSkillById(skillId);
	}

	/**
	 * method to find skill by name and return skillvo object
	 * 
	 * @param skillName
	 * @return skillvo
	 */
	public SkillVo findSkillByName(String skillName) {
		logger.info("Inside Skill Service >>> findSkillByName....");
		return skillDao.findSkillByName(skillName);
	}

	/**
	 * method to find skill by name and return skill object
	 * 
	 * @param skillName
	 * @return skills
	 */
	public Skills findSkill(String skillName) {
		logger.info("Inside Skill Service >>> findSkill....");
		return skillDao.findSkill(skillName);
	}

	/**
	 * method to find all skills
	 * 
	 * @return List<SkillVo>
	 */
	public List<SkillVo> findAllSkills() {
		logger.info("Inside Skill Service >>> findAllSkills....");
		return skillDao.findAllSkills();
	}

}
