package com.hrms.web.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.constants.PayRollConstants;
import com.hrms.web.dao.LwfDao;
import com.hrms.web.dto.EsiShare;
import com.hrms.web.dto.PFShare;
import com.hrms.web.service.EsiSyntaxService;
import com.hrms.web.service.PaySlipItemService;
import com.hrms.web.service.ProfidentFundSyntaxService;
import com.hrms.web.service.SalaryCalculatorService;
import com.hrms.web.util.PayRollCalculator;
import com.hrms.web.vo.EsiSyntaxVo;
import com.hrms.web.vo.LwfVo;
import com.hrms.web.vo.PayRollResultVo;
import com.hrms.web.vo.PaySlipItemVo;
import com.hrms.web.vo.ProfidentFundSyntaxVo;
import com.hrms.web.vo.SalaryCalculatorBasicsVo;
import com.hrms.web.vo.SalaryCalculatorEsiVo;
import com.hrms.web.vo.SalaryCalculatorLWFVo;
import com.hrms.web.vo.SalaryCalculatorPfShareVo;
import com.hrms.web.vo.SalaryCalculatorVo;

/**
 * 
 * @author Shamsheer
 * @since 05-May-2015
 */
@Service
public class SalaryCalculatorServiceImpl implements SalaryCalculatorService{

	@Autowired
	private PaySlipItemService paySlipItemService;

	@Autowired
	private ProfidentFundSyntaxService profidentFundSyntaxService;

	@Autowired
	private EsiSyntaxService esiSyntaxService;

	@Autowired
	private LwfDao lwfdao;

	/**
	 * method to calculate salary
	 */
	@SuppressWarnings("static-access")
	public SalaryCalculatorVo calculateSalary(BigDecimal grossSalary) {
		SalaryCalculatorVo salaryCalculatorVo = new SalaryCalculatorVo();
		SalaryCalculatorPfShareVo pfShareVo = new SalaryCalculatorPfShareVo();
		SalaryCalculatorEsiVo esiVo = new SalaryCalculatorEsiVo();
		SalaryCalculatorBasicsVo basicsVo = new SalaryCalculatorBasicsVo();

		List<PaySlipItemVo> paySlipItemVos = paySlipItemService.findAllPaySlipItem();
		if (paySlipItemVos != null) {
			PayRollCalculator calculator = new PayRollCalculator();
			Map<String, String> components = calculator.getPayRollComponents(paySlipItemVos);
			List<PayRollResultVo> resultVo = calculator.getPayRollResult(grossSalary, components,paySlipItemVos);
			basicsVo = setBasicCalculations(resultVo);
			BigDecimal grossMonthly = new BigDecimal(0);
			for(PayRollResultVo vo : resultVo){
				grossMonthly = grossMonthly.add(vo.getAmount());
			}
			basicsVo.setGrossMonthly(grossMonthly);
			basicsVo.setGrossAnnual(grossMonthly.multiply(new BigDecimal(12)));
			basicsVo.setResultVo(resultVo);
			ProfidentFundSyntaxVo profidentFundSyntaxVo = profidentFundSyntaxService.findProfidentFundSyntax();
			if (profidentFundSyntaxVo != null) {
				PFShare pfShare = PayRollCalculator.getPFShare(grossSalary, profidentFundSyntaxVo.getEmployeeSyntax(),
						profidentFundSyntaxVo.getEmployerSyntax(), components);
				pfShareVo = setPfShare(pfShare);
			}
		}

		EsiSyntaxVo esiSyntaxVo = esiSyntaxService.findEsiSyntax();
		if (esiSyntaxVo != null) {
			EsiShare esiShare = PayRollCalculator.getESIShare(grossSalary, esiSyntaxVo.getEmployeeSyntax(),
					esiSyntaxVo.getEmployerSyntax(),esiSyntaxVo);
			esiVo = setEsiShare(esiShare);
		}

		SalaryCalculatorLWFVo lwfVo = setLwfVo();

		BigDecimal netMonthly = basicsVo.getGrossMonthly().subtract(pfShareVo.getPfEmployeeMonthly())
				.subtract(esiVo.getEsiEmployeeMonthly()).subtract(lwfVo.getLwfEmployeeMonthly());
		BigDecimal netAnnual = basicsVo.getGrossAnnual().subtract(pfShareVo.getPfEmployeeAnnual())
				.subtract(esiVo.getEsiEmployeeAnnual()).subtract(lwfVo.getLwfEmployeeAnnual());

		BigDecimal ctcAnnual = netAnnual.add(pfShareVo.getPfEmployerAnnual()).add(esiVo.getEsiEmployerAnnual())
				.add(lwfVo.getLwfEmployerAnnual());

		BigDecimal ctcMonthly = netMonthly.add(pfShareVo.getPfEmployerMonthly()).add(esiVo.getEsiEmployerMonthly())
				.add(lwfVo.getLwfEmployerMonthly());

		salaryCalculatorVo.setEsiVo(esiVo);
		salaryCalculatorVo.setLwfVo(lwfVo);
		salaryCalculatorVo.setBasicsVo(basicsVo);
		salaryCalculatorVo.setPfShare(pfShareVo);
		salaryCalculatorVo.setNetAnnual(netAnnual);
		salaryCalculatorVo.setNetMonthly(netMonthly);
		salaryCalculatorVo.setCtcAnnual(ctcAnnual);
		salaryCalculatorVo.setCtcMonthly(ctcMonthly);
		return salaryCalculatorVo;
	}

	/**
	 * method to set basic calculations into vo
	 * 
	 * @return
	 */
	private SalaryCalculatorBasicsVo setBasicCalculations(List<PayRollResultVo> resultVo) {
		SalaryCalculatorBasicsVo calculatorVo = new SalaryCalculatorBasicsVo();
		BigDecimal basicSalaryMonthly = new BigDecimal(0);
		BigDecimal basicSalaryAnnual = new BigDecimal(0);
		BigDecimal dearnessAllowanceMonthly = new BigDecimal(0);
		BigDecimal dearnessAllowanceAnnual = new BigDecimal(0);
		BigDecimal hraMonthly = new BigDecimal(0);
		BigDecimal hraAnnual = new BigDecimal(0);
		BigDecimal ccaMonthly = new BigDecimal(0);
		BigDecimal ccaAnnual = new BigDecimal(0);
		BigDecimal conveyanceMonthly = new BigDecimal(0);
		BigDecimal conveyanceAnnual = new BigDecimal(0);
		BigDecimal grossMonthly = new BigDecimal(0);
		BigDecimal grossAnnual = new BigDecimal(0);
		BigDecimal otherAllowancesMonthly = new BigDecimal(0);
		BigDecimal otherAllowancesAnnual = new BigDecimal(0);

		for (PayRollResultVo payRollResultVo : resultVo) {
			if (payRollResultVo.getPaySlipItem().equalsIgnoreCase(PayRollConstants.BASIC_SALARY)) {
				basicSalaryMonthly = payRollResultVo.getAmount();
				basicSalaryAnnual = basicSalaryMonthly.multiply(new BigDecimal(12));
				grossMonthly = grossMonthly.add(basicSalaryMonthly);
				grossAnnual = grossAnnual.add(basicSalaryAnnual);
			} else if (payRollResultVo.getPaySlipItem().equalsIgnoreCase(PayRollConstants.CCA)) {
				ccaMonthly = payRollResultVo.getAmount();
				ccaAnnual = ccaMonthly.multiply(new BigDecimal(12));
				grossMonthly = grossMonthly.add(ccaMonthly);
				grossAnnual = grossAnnual.add(ccaAnnual);
			} else if (payRollResultVo.getPaySlipItem().equalsIgnoreCase(PayRollConstants.CONVEYANCE)) {
				conveyanceMonthly = payRollResultVo.getAmount();
				conveyanceAnnual = conveyanceMonthly.multiply(new BigDecimal(12));
				grossMonthly = grossMonthly.add(conveyanceMonthly);
				grossAnnual = grossAnnual.add(conveyanceAnnual);
			} else if (payRollResultVo.getPaySlipItem().equalsIgnoreCase(PayRollConstants.DA)) {
				dearnessAllowanceMonthly = payRollResultVo.getAmount();
				dearnessAllowanceAnnual = dearnessAllowanceMonthly.multiply(new BigDecimal(12));
				grossMonthly = grossMonthly.add(dearnessAllowanceMonthly);
				grossAnnual = grossAnnual.add(dearnessAllowanceAnnual);
			} else if (payRollResultVo.getPaySlipItem().equalsIgnoreCase(PayRollConstants.HRA)) {
				hraMonthly = payRollResultVo.getAmount();
				hraAnnual = hraMonthly.multiply(new BigDecimal(12));
				grossMonthly = grossMonthly.add(hraMonthly);
				grossAnnual = grossAnnual.add(hraAnnual);
			}
		}
		calculatorVo.setBasicSalaryAnnual(basicSalaryAnnual);
		calculatorVo.setBasicSalaryMonthly(basicSalaryMonthly);
		calculatorVo.setDearnessAllowanceAnnual(dearnessAllowanceAnnual);
		calculatorVo.setDearnessAllowanceMonthly(dearnessAllowanceMonthly);
		calculatorVo.setHraAnnual(hraAnnual);
		calculatorVo.setHraMonthly(hraMonthly);
		calculatorVo.setCcaAnnual(ccaAnnual);
		calculatorVo.setCcaMonthly(ccaMonthly);
		calculatorVo.setConveyanceMonthly(conveyanceMonthly);
		calculatorVo.setConveyanceAnnual(conveyanceAnnual);
		calculatorVo.setGrossAnnual(grossAnnual);
		calculatorVo.setGrossMonthly(grossMonthly);
		calculatorVo.setOtherAllowancesAnnual(otherAllowancesAnnual);
		calculatorVo.setOtherAllowancesMonthly(otherAllowancesMonthly);
		return calculatorVo;
	}

	/**
	 * method to set pf share
	 * 
	 * @return
	 */
	private SalaryCalculatorPfShareVo setPfShare(PFShare pfShare) {
		SalaryCalculatorPfShareVo calculatorVo = new SalaryCalculatorPfShareVo();
		BigDecimal pfEmployeeMonthly = pfShare.getEmployeeShare();
		BigDecimal pfEmployeeAnnual = pfEmployeeMonthly.multiply(new BigDecimal(12));
		BigDecimal pfEmployerMonthly = pfShare.getEmployerShare();
		BigDecimal pfEmployerAnnual = pfEmployerMonthly.multiply(new BigDecimal(12));
		calculatorVo.setPfEmployeeAnnual(pfEmployeeAnnual);
		calculatorVo.setPfEmployeeMonthly(pfEmployeeMonthly);
		calculatorVo.setPfEmployerMonthly(pfEmployerMonthly);
		calculatorVo.setPfEmployerAnnual(pfEmployerAnnual);
		return calculatorVo;
	}

	/**
	 * method to set esi shares
	 * 
	 * @return
	 */
	private SalaryCalculatorEsiVo setEsiShare(EsiShare esiShare) {
		SalaryCalculatorEsiVo esiVo = new SalaryCalculatorEsiVo();
		BigDecimal esiEmployeeMonthly = esiShare.getEmployeeShare();
		BigDecimal esiEmployeeAnnual = esiEmployeeMonthly.multiply(new BigDecimal(12));
		BigDecimal esiEmployerMonthly = esiShare.getEmployerShare();
		BigDecimal esiEmployerAnnual = esiEmployerMonthly.multiply(new BigDecimal(12));

		esiVo.setEsiEmployeeAnnual(esiEmployeeAnnual);
		esiVo.setEsiEmployeeMonthly(esiEmployeeMonthly);
		esiVo.setEsiEmployerMonthly(esiEmployerMonthly);
		esiVo.setEsiEmployerAnnual(esiEmployerAnnual);
		return esiVo;
	}

	/**
	 * method to set lwf
	 * 
	 * @return
	 */
	private SalaryCalculatorLWFVo setLwfVo() {
		LwfVo lwfVoo = lwfdao.findLwf();
		BigDecimal amount = new BigDecimal(0);
		if (lwfVoo != null) {
			amount = new BigDecimal(lwfVoo.getAmount());
		}
		SalaryCalculatorLWFVo lwfVo = new SalaryCalculatorLWFVo();
		lwfVo.setLwfEmployeeMonthly(amount);
		lwfVo.setLwfEmployerMonthly(amount);
		lwfVo.setLwfEmployeeAnnual(amount.multiply(new BigDecimal(12)));
		lwfVo.setLwfEmployerAnnual(amount.multiply(new BigDecimal(12)));
		return lwfVo;
	}
}
