package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TravelArrangementTypeDao;
import com.hrms.web.service.TravelArrangementTypeService;
import com.hrms.web.vo.TravelArrangementTypeVo;
/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
@Service
public class TravelArrangementServiceImpl implements TravelArrangementTypeService{

	@Autowired
	private TravelArrangementTypeDao dao;

	public void saveArrangementType(TravelArrangementTypeVo vo) {
		dao.saveArrangementType(vo);
	}

	public void updateArrangementType(TravelArrangementTypeVo vo) {
		dao.updateArrangementType(vo);
	}

	public void deleteArrangementType(Long id) {
		dao.deleteArrangementType(id);
	}

	public TravelArrangementTypeVo getArrangementTypeById(Long id) {
		return dao.getArrangementTypeById(id);
	}

	public List<TravelArrangementTypeVo> listAllArrangementType() {
		return dao.listAllArrangementType();
	};
}
