package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.RelegionDao;
import com.hrms.web.entities.Relegion;
import com.hrms.web.service.RelegionService;
import com.hrms.web.vo.RelegionVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Service
public class RelegionServiceImpl implements RelegionService {

	@Autowired
	private RelegionDao relegionDao;

	public void saveRelegion(RelegionVo relegionVo) {
		relegionDao.saveRelegion(relegionVo);
	}

	public void updateRelegion(RelegionVo relegionVo) {
		relegionDao.updateRelegion(relegionVo);
	}

	public void deleteReligion(Long id) {
		relegionDao.deleteReligion(id);
	}

	public RelegionVo findRelegianById(Long id) {
		return relegionDao.findRelegianById(id);
	}

	public RelegionVo findReligianByName(String relegian) {
		return relegionDao.findReligianByName(relegian);
	}

	public Relegion findRelegian(Long id) {
		return relegionDao.findRelegian(id);
	}

	public Relegion findRelegian(String relegian) {
		return relegionDao.findRelegian(relegian);
	}

	public List<RelegionVo> listAllRelegians() {
		return relegionDao.listAllRelegians();
	}

}
