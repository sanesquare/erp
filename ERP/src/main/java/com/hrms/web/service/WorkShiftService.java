package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.EmployeeWorkShift;
import com.hrms.web.vo.WorkShiftVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
public interface WorkShiftService {

	/**
	 * method to save WorkShiftVo
	 * 
	 * @param workShiftVo
	 */
	public void saveWorkShift(WorkShiftVo workShiftVo);

	/**
	 * method to update WorkShiftVo
	 * 
	 * @param workShiftVo
	 */
	public void updateWorkShift(WorkShiftVo workShiftVo);

	/**
	 * method to delete WorkShiftVo
	 * 
	 * @param workShiftId
	 */
	public void deleteWorkShift(Long workShiftId);

	/**
	 * method to find WorkShiftVo by id
	 * 
	 * @param workShiftId
	 * @return EmployeeWorkShift
	 */
	public EmployeeWorkShift findWorkShift(Long workShiftId);

	/**
	 * method to find WorkShiftVo by type
	 * 
	 * @param shiftType
	 * @return EmployeeWorkShift
	 */
	public EmployeeWorkShift findWorkShift(String shiftType);

	/**
	 * method to find WorkShiftVo by id
	 * 
	 * @param workShiftId
	 * @return WorkShiftVo
	 */
	public WorkShiftVo findWorkShiftById(Long workShiftId);

	/**
	 * method to find WorkShiftVo by type
	 * 
	 * @param shiftType
	 * @return WorkShiftVo
	 */
	public WorkShiftVo findWorkShiftByType(String shiftType);

	/**
	 * method to find all WorkShiftVo
	 * 
	 * @return List<WorkShiftVo>
	 */
	public List<WorkShiftVo> findAllWorkShift();
}
