package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.AdvanceSalaryStatusDao;
import com.hrms.web.entities.AdvanceSalaryStatus;
import com.hrms.web.service.AdvanceSalaryStatusService;
import com.hrms.web.vo.AdvanceSalaryStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Service
public class AdvanceSalaryStatusServiceImpl implements AdvanceSalaryStatusService {

	@Autowired
	private AdvanceSalaryStatusDao advanceSalaryStatusDao;

	public void saveAdvanceSalaryStatus(AdvanceSalaryStatusVo advanceSalaryStatusVo) {
		advanceSalaryStatusDao.saveAdvanceSalaryStatus(advanceSalaryStatusVo);
	}

	public void updateAdvanceSalaryStatus(AdvanceSalaryStatusVo advanceSalaryStatusVo) {
		advanceSalaryStatusDao.updateAdvanceSalaryStatus(advanceSalaryStatusVo);
	}

	public void deleteAdvanceSalaryStatus(Long advanceSalaryStatusId) {
		advanceSalaryStatusDao.deleteAdvanceSalaryStatus(advanceSalaryStatusId);
	}

	public AdvanceSalaryStatus findAdvanceSalaryStatus(Long advanceSalaryStatusId) {
		return advanceSalaryStatusDao.findAdvanceSalaryStatus(advanceSalaryStatusId);
	}

	public AdvanceSalaryStatus findAdvanceSalaryStatus(String status) {
		return advanceSalaryStatusDao.findAdvanceSalaryStatus(status);
	}

	public AdvanceSalaryStatusVo findAdvanceSalaryStatusById(Long advanceSalaryStatusId) {
		return advanceSalaryStatusDao.findAdvanceSalaryStatusById(advanceSalaryStatusId);
	}

	public AdvanceSalaryStatusVo findAdvanceSalaryStatusByStatus(String status) {
		return advanceSalaryStatusDao.findAdvanceSalaryStatusByStatus(status);
	}

	public List<AdvanceSalaryStatusVo> findAllAdvanceSalaryStatus() {
		return advanceSalaryStatusDao.findAllAdvanceSalaryStatus();
	}

}
