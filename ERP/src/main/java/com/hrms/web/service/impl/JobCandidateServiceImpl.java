package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.JobCandidatesDao;
import com.hrms.web.entities.JobCandidates;
import com.hrms.web.service.JobCandidateService;
import com.hrms.web.vo.CandidatesLanguageVo;
import com.hrms.web.vo.CandidatesQualificationsVo;
import com.hrms.web.vo.CandidatesSkillsVo;
import com.hrms.web.vo.JobCandidatesVo;
import com.hrms.web.vo.JobInterviewVo;
import com.hrms.web.vo.JobPostVo;

/**
 * 
 * @author Vinutha
 * @since 24-March-2015
 *
 */
@Service
public class JobCandidateServiceImpl implements JobCandidateService{

	@Autowired
	private JobCandidatesDao candidatesDao;
	/**
	 * method save job candidates
	 * @param candidatesVo
	 */
	public void saveJobCandidates(JobCandidatesVo candidatesVo) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * method to update job candidates
	 * @param candidatesVo
	 */
	public void updateJobCandidates(JobCandidatesVo candidatesVo) {
		//
		
	}
	/**
	 * method to delete job candidates
	 * @param candidatesVo
	 */
	public void deleteJobCandidates(JobCandidatesVo candidatesVo) {
		candidatesDao.deleteJobCandidates(candidatesVo);
		
	}
	/**
	 * method to delete job candidate by id
	 * @param id
	 */
	public void deleteJobCandidateById(Long id) {
		candidatesDao.deleteJobCandidateById(id);
		
	}
	/**
	 * method to find candidate by id
	 * @param id
	 */
	public JobCandidatesVo findJobCandidatesById(Long id) {
		
		return candidatesDao.findJobCandidatesById(id);
	}
	/**
	 * method to find all candidates with first name
	 * @param firstName
	 */
	public List<JobCandidatesVo> findAllCandidateswithFirstName(String firstName) {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * method to find all candidates with last name
	 * @param lastName
	 */
	public List<JobCandidatesVo> findAllCandidateswithLastName(String lastName) {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * method to find and return job candidate object with id
	 * @param id
	 */
	public JobCandidates findAndReturnJobCandidatesObjectWithId(Long id) {
		
		return candidatesDao.findAndReturnJobCandidatesObjectWithId(id);
	}
	/**
	 * method to list candidates based on status and number
	 * @param interviewVo
	 */
	public List<JobCandidates> listCandidatesBasedOnStatusAndNumber(JobInterviewVo interviewVo) {
		
		//return candidatesDao.listCandidatesBasedOnStatusAndNumber(interviewVo);
		return null;
	}
	/**
	 * method to save basic info
	 * @param candidatesVo
	 * @return Long 
	 */
	public Long saveCandidateBasicInfo(JobCandidatesVo candidatesVo) {
		return candidatesDao.saveCandidateBasicInfo(candidatesVo);
	}
	/**
	 * method to list all candidates
	 * @return List<JobCandidatesVo>
	 */
	public List<JobCandidatesVo> listAllCandidates() {
		
		return candidatesDao.listAllCandidates();
	}
	/**
	 * method to save achievement
	 * @param candidatesVo
	 */
	public void saveAchievement(JobCandidatesVo candidatesVo) {
		 candidatesDao.saveAchievement(candidatesVo);
		
	}
	/**
	 * method to save interests
	 * @param candidatesVo
	 */
	public void saveInterests(JobCandidatesVo candidatesVo) {
		candidatesDao.saveInterests(candidatesVo);
		
	}
	/**
	 * method to save additional info
	 * @param candidatesVo
	 */
	public void saveAdditionalInfo(JobCandidatesVo candidatesVo) {
		candidatesDao.saveAdditionalInfo(candidatesVo);
		
	}
	/**
	 * method to save contact details
	 * @param candidatesVo
	 */
	public void saveContactDetails(JobCandidatesVo candidatesVo) {
		candidatesDao.saveContactDetails(candidatesVo);
		
	}
	/**
	 * method to save candidate qualifications
	 * @param candidatesVo
	 */
	public void saveCandidateQualifications(JobCandidatesVo candidatesVo) {
		candidatesDao.saveCandidateQualifications(candidatesVo);
		
	}
	/**
	 * method to save candidate languages
	 * @param  candidatesVo
	 */
	public void saveCandidateLanguages(JobCandidatesVo candidatesVo) {
		candidatesDao.saveCandidateLanguages(candidatesVo);
		
	}
	/**
	 * method to save candidate skills
	 * @param candidatesVo
	 */
	public void saveCandidatesSkills(JobCandidatesVo candidatesVo) {
		candidatesDao.saveCandidatesSkills(candidatesVo);
		
	}
	/**
	 * method to save candidate work experiences
	 * @param candidatesVo
	 */
	public void saveCandidateExperiences(JobCandidatesVo candidatesVo) {
		candidatesDao.saveCandidateExperiences(candidatesVo);
		
	}
	/**
	 * method to save candidate status
	 * @param candidatesVo
	 */
	public void saveCandidateStatus(JobCandidatesVo candidatesVo) {
		candidatesDao.saveCandidateStatus(candidatesVo);
		
	}
	/**
	 * method to save candidate references
	 * @param candidatesVo
	 */
	public void saveCandidateReferences(JobCandidatesVo candidatesVo) {
		candidatesDao.saveCandidateReferences(candidatesVo);
		
	}
	/**
	 * method to save candidate resume
	 * @param candidatesVo
	 * 
	 */
	public void updateCandidateResume(JobCandidatesVo candidatesVo) {
		candidatesDao.updateResume(candidatesVo);
		
	}
	/**
	 * method to update candidate basic info
	 * @param candidatesVo
	 */
	public void updateCandidateBasicInfo(JobCandidatesVo candidatesVo) {
		candidatesDao.updateCandidateBasicInfo(candidatesVo);
		
	}
	/**
	 * method to update candidate documents
	 * @param candidatesVo
	 */
	public void updateCandidateDocuments(JobCandidatesVo candidatesVo) {
		candidatesDao.updateDocuments(candidatesVo);
		
	}
	/**
	 * method to delete document
	 * @param url
	 */
	public void deleteDocument(String url) {
		candidatesDao.deleteDocuments(url);
		
	}
	/**
	 * method to delete candidate resume
	 * @param url
	 */
	public void deleteResume(String url) {
		candidatesDao.deleteResume(url);
		
	}
	/**
	 * method to get candidate qualification
	 * @param candidate
	 * @param degree
	 * @return CandidatesQualificationsVo
	 */
	public CandidatesQualificationsVo getCandQualification(Long candidate, Long degree) {

		return candidatesDao.getCandQualification(candidate, degree);
	}
	/***
	 * method to get candidate skills
	 * @param candidate
	 * @param skill
	 * @return CandidatesSkillsVo
	 */
	public CandidatesSkillsVo getCandSkills(Long candidate, Long skill) {
		
		return candidatesDao.getCandSkills(candidate, skill);
	}
	/**
	 * method to get candidate language
	 * @param candidate
	 * @param language
	 * @return CandidatesLanguageVo
	 */
	public CandidatesLanguageVo getCandLanguage(Long candidate, Long language) {
		
		return candidatesDao.getCandLanguage(candidate, language);
	}

	public List<JobCandidatesVo> findCandidtesByPostAndGender(String designation, String gender) {
		
		return candidatesDao.findCandidtesByPostAndGender(designation, gender);
	}
	
	public List<JobCandidatesVo> findCandidtesByPost(String designation) {
		return candidatesDao.findCandidtesByPost(designation);
	}
	
	public List<JobCandidatesVo> findCandidtesByGender(String gender) {
		return candidatesDao.findCandidtesByGender(gender);
	}
	
	public List<JobCandidatesVo> listAllCandidatesForReport() {
		return candidatesDao.findAllCandidatesForReport();
	}
	/**
	 * Method to get candidates by interview
	 * @param interviewId
	 * @return
	 */
	public List<JobCandidatesVo> findCandidatesByInterview(Long interviewId){
		return candidatesDao.findCandidatesByInterview(interviewId);
	}

}
