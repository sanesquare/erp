package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ProjectDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Project;
import com.hrms.web.logger.Log;
import com.hrms.web.service.ProjectService;
import com.hrms.web.vo.ProjectJsonVo;
import com.hrms.web.vo.ProjectVo;
/**
 * 
 * @author Shamsheer
 * @since 10-March-2015
 *
 */
@Service
public class ProjectServiceImpl implements ProjectService {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;
	
	@Autowired
	private ProjectDao projectDao;

	/**
	 * Method to save new Project
	 * @param projectVo
	 * @return void
	 */
	public Long saveProject(ProjectJsonVo projectVo) {
		logger.info("inside>> ProjectService.. saveProject");
		return projectDao.saveProject(projectVo);
	}

	/**
	 * method to update existing project
	 * @param projectVo
	 * @return void
	 */
	public void updateProject(ProjectJsonVo projectVo) {
		logger.info("inside>> ProjectService.. updateProject");
		projectDao.updateProject(projectVo);
	}

	/**
	 * method to delete existing project
	 * @param projectVo
	 * @return void
	 */
	public void deleteProject(Long id) {
		logger.info("inside>> ProjectService.. deleteProject");
		projectDao.deleteProject(id);
	}

	/**
	 * method to find project by using project Id
	 * @param id
	 * @return ProjectVo
	 */
	public ProjectVo findProjectById(Long projectId) {
		logger.info("inside>> ProjectService.. findProjectById");
		return projectDao.findProjectById(projectId);
	}

	/**
	 * method to find project by using project title
	 * @param title
	 * @return ProjectVo
	 */
	public ProjectVo findProjectByTitle(String projectTitle) {
		logger.info("inside>> ProjectService.. findProjectByTitle");
		return projectDao.findProjectByTitle(projectTitle);
	}

	/**
	 * method to list all projects
	 * @return List<ProjectVo>
	 */
	public List<ProjectVo> listAllProjects() {
		logger.info("inside>> ProjectService.. listAllProjects");
		return projectDao.listAllProjects();
	}


	/**
	 * method to find and return Project object by id
	 * @return
	 */
	public Project findAndReturnProjectObjByTitle(String title) {
		logger.info("inside>> ProjectService.. findAndReturnProjectObjByTitle");
		return projectDao.findAndReturnProjectObjByTitle(title);
	}

	/**
	 * method to list project by status
	 * @return
	 */
	public List<ProjectVo> listProjectByStatus(String status) {
		return projectDao.listProjectByStatus(status);
	}

	/**
	 * method to delete project document
	 * @param path
	 */
	public void deleteProjectDocument(String path) {
		projectDao.deleteProjectDocument(path);
	}

	/**
	 * method to updateProject documents
	 */
	public ProjectVo updateProjectDocument(ProjectVo projectVo) {
		return projectDao.updateProjectDocument(projectVo);
	}

	/**
	 * method to update projectstatus
	 */
	public void updateProjectStatus(ProjectJsonVo projectVo) {
		projectDao.updateProjectStatus(projectVo);
	}

}
