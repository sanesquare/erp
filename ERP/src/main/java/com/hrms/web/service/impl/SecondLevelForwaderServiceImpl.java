package com.hrms.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.SecondLevelForwaderDao;
import com.hrms.web.entities.SecondLevelForwader;
import com.hrms.web.service.SecondLevelForwaderService;
import com.hrms.web.vo.SecondLevelForwardingVo;

/**
 * 
 * @author Jithin Mohan
 * @since 27-April-2015
 *
 */
@Service
public class SecondLevelForwaderServiceImpl implements SecondLevelForwaderService {

	@Autowired
	private SecondLevelForwaderDao secondLevelForwaderDao;

	public void saveSecondLevelForwader(SecondLevelForwardingVo secondLevelForwardingVo) {
		secondLevelForwaderDao.saveSecondLevelForwader(secondLevelForwardingVo);
	}

	public void updateSecondLevelForwader(SecondLevelForwardingVo secondLevelForwardingVo) {
		secondLevelForwaderDao.updateSecondLevelForwader(secondLevelForwardingVo);
	}

	public void deleteSecondLevelForwader(Long secondLevelForwaderId) {
		secondLevelForwaderDao.deleteSecondLevelForwader(secondLevelForwaderId);
	}

	public SecondLevelForwader findSecondLevelForwader(Long secondLevelForwaderId) {
		return secondLevelForwaderDao.findSecondLevelForwader(secondLevelForwaderId);
	}

	public SecondLevelForwader findSecondLevelForwader(String secondLevelForwaderCode) {
		return secondLevelForwaderDao.findSecondLevelForwader(secondLevelForwaderCode);
	}

	public SecondLevelForwardingVo findSecondLevelForwaderById(Long secondLevelForwaderId) {
		return secondLevelForwaderDao.findSecondLevelForwaderById(secondLevelForwaderId);
	}

	public SecondLevelForwardingVo findSecondLevelForwaderByForwader(String secondLevelForwaderCode) {
		return secondLevelForwaderDao.findSecondLevelForwaderByForwader(secondLevelForwaderCode);
	}

	public SecondLevelForwardingVo findAllSecondLevelForwader() {
		return secondLevelForwaderDao.findAllSecondLevelForwader();
	}

	public SecondLevelForwardingVo findAllSecondLevelForwaderEmails() {
		return secondLevelForwaderDao.findAllSecondLevelForwaderEmails();
	}

}
