package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EmployeeExitDao;
import com.hrms.web.entities.EmployeeExit;
import com.hrms.web.service.EmployeeExitService;
import com.hrms.web.vo.EmployeeExitVo;

/**
 * 
 * @author Jithin Mohan
 * @since 18-March-2015
 *
 */
@Service
public class EmployeeExitServiceImpl implements EmployeeExitService {

	@Autowired
	private EmployeeExitDao employeeExitDao;

	public Long saveEmployeeExit(EmployeeExitVo employeeExitVo) {
		return employeeExitDao.saveEmployeeExit(employeeExitVo);
	}

	public void updateEmployeeExit(EmployeeExitVo employeeExitVo) {
		employeeExitDao.updateEmployeeExit(employeeExitVo);
	}

	public void deleteEmployeeExit(Long employeeExitId) {
		employeeExitDao.deleteEmployeeExit(employeeExitId);
	}

	public EmployeeExit findEmployeeExit(Long employeeExitId) {
		return employeeExitDao.findEmployeeExit(employeeExitId);
	}

	public EmployeeExit findEmployeeExit(Date exitDate) {
		return employeeExitDao.findEmployeeExit(exitDate);
	}

	public EmployeeExit findEmployeeExit(Date startDate, Date endDate) {
		return employeeExitDao.findEmployeeExit(startDate, endDate);
	}

	public EmployeeExitVo findEmployeeExitById(Long employeeExitId) {
		return employeeExitDao.findEmployeeExitById(employeeExitId);
	}

	public EmployeeExitVo findEmployeeExitByDate(Date exitDate) {
		return employeeExitDao.findEmployeeExitByDate(exitDate);
	}

	public EmployeeExitVo findEmployeeExitBetweenDates(Date startDate, Date endDate) {
		return employeeExitDao.findEmployeeExitBetweenDates(startDate, endDate);
	}

	public List<EmployeeExitVo> findAllEmployeeExit() {
		return employeeExitDao.findAllEmployeeExit();
	}

	public void saveEmployeeExitDocuments(EmployeeExitVo employeeExitVo) {
		employeeExitDao.saveEmployeeExitDocuments(employeeExitVo);
	}

	public void deleteEmployeeExitDocument(Long employeeExitDocumentId) {
		employeeExitDao.deleteEmployeeExitDocument(employeeExitDocumentId);
	}

	public EmployeeExitVo findEmployeeExitByEmployee(String employeeCode) {
		return employeeExitDao.findEmployeeExitByEmployee(employeeCode);
	}

	public List<EmployeeExitVo> findAllEmployeeExitByEmployee(String employeeCode) {
		return employeeExitDao.findAllEmployeeExitByEmployee(employeeCode);
	}

}
