package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.MeetingsDao;
import com.hrms.web.service.MeetingsService;
import com.hrms.web.vo.MeetingsVo;
/**
 * 
 * @author Shamsheer
 * @since 12-March-2015
 *
 */
@Service
public class MeetingsServiceImpl implements MeetingsService{
	
	@Autowired
	MeetingsDao meetingsDao;

	/**
	 * method to save new meetings
	 * @param meetingsVo
	 * @throws Exception 
	 */
	public Long saveMeetings(MeetingsVo meetingsVo) throws Exception {
		return meetingsDao.saveMeetings(meetingsVo);
	}

	/**
	 * method to update an existing meeting
	 * @param meetingsVo
	 */
	public void updateMeetings(MeetingsVo meetingsVo) {
		meetingsDao.updateMeetings(meetingsVo);
	}

	/**
	 * method to delete an existing meeting
	 * @param meetingsId
	 */
	public void deleteMeeting(Long meetingsId) {
		meetingsDao.deleteMeeting(meetingsId);
	}

	/**
	 * method to list all meetings
	 * @return
	 */
	public List<MeetingsVo> listAllMeetings() {
		return meetingsDao.listAllMeetings();
	}

	/**
	 * method to find meetings by id
	 * @param id
	 * @return
	 */
	public MeetingsVo findMeetingsById(Long id) {
		return meetingsDao.findMeetingsById(id);
	}

	/**
	 * method to find meetings by title
	 * @param title
	 * @return
	 */
	public MeetingsVo findMeetingsByTitle(String title) {
		return meetingsDao.findMeetingsByTitle(title);
	}

	public void updateDocument(MeetingsVo meetingsVo) {
		meetingsDao.updateDocument(meetingsVo);
	}

	public void deleteDocument(String url) {
		meetingsDao.deleteDocument(url);
	}
}
