package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EmployeeJoiningDao;
import com.hrms.web.entities.EmployeeJoining;
import com.hrms.web.service.EmployeeJoiningService;
import com.hrms.web.vo.EmployeeDetailsVo;
import com.hrms.web.vo.EmployeeJoiningVo;
import com.hrms.web.vo.JoiningJsonVo;

/**
 * 
 * @author shamsheer
 * @since 23-March-2015
 */
@Service
public class EmployeeJoiningServiceImpl implements EmployeeJoiningService{

	@Autowired
	private EmployeeJoiningDao employeeJoiningDao;
	
	public Long saveEmployeeJoining(JoiningJsonVo joiningVo) {
		return employeeJoiningDao.saveEmployeeJoining(joiningVo);
	}

	public void deleteEmployeeJoining(Long id) {
		employeeJoiningDao.deleteEmployeeJoining(id);
	}

	public void updateJoining(JoiningJsonVo employeeJoiningVo) {
		employeeJoiningDao.updateJoining(employeeJoiningVo);
	}

	public EmployeeJoining findJoiningById(Long id) {
		return null;
	}

	public EmployeeJoining findJoiningByEmployee(Long empId) {
		return employeeJoiningDao.findJoiningByEmployee(empId);
	}

	public List<EmployeeJoiningVo> listAllJoinings() {
		return employeeJoiningDao.listAllJoinings();
	}

	public void uploadDocuments(EmployeeJoiningVo employeeJoiningVo) {
		employeeJoiningDao.updateDocuments(employeeJoiningVo);
	}

	public EmployeeJoiningVo findDetailedJoining(Long id) {
		return employeeJoiningDao.findDetailedJoining(id);
	}

	public void deleteDocument(String url) {
		employeeJoiningDao.deleteDocument(url);
	}

	public EmployeeDetailsVo getEmployeeDetails(String employeeCode) {
		return employeeJoiningDao.getEmployeeDetails(employeeCode);
	}

	public List<EmployeeJoiningVo> listJoiningByStartDateEndDate(String startDate, String endDate) {
		return employeeJoiningDao.listJoiningByStartDateEndDate(startDate, endDate);
	}

	public List<EmployeeJoiningVo> listJoiningByBranchStartDateEndDate(Long branchId, String startDate, String endDate) {
		return employeeJoiningDao.listJoiningByBranchStartDateEndDate(branchId, startDate, endDate);
	}

	public List<EmployeeJoiningVo> listJoiningByBranchDepartmentStartDateEndDate(Long branchId, Long departmentId,
			String startDate, String endDate) {
		return employeeJoiningDao.listJoiningByBranchDepartmentStartDateEndDate(branchId, departmentId, startDate, endDate);
	}

	public List<EmployeeJoiningVo> listJoiningByDepartmentStartDateEndDate(Long departmentId, String startDate,
			String endDate) {
		return employeeJoiningDao.listJoiningByDepartmentStartDateEndDate(departmentId, startDate, endDate);
	}

}
