package com.hrms.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ManagerReportingNotificationDao;
import com.hrms.web.entities.ManagerReportingNotification;
import com.hrms.web.service.ManagerReportingNotificationService;
import com.hrms.web.vo.ManagerReportingNotificationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Service
public class ManagerReportingNotificationServiceImpl implements ManagerReportingNotificationService {

	@Autowired
	private ManagerReportingNotificationDao managerReportingNotificationDao;

	public void updateManagerReportingNotification(boolean isRepot) {
		managerReportingNotificationDao.updateManagerReportingNotification(isRepot);
	}

	public ManagerReportingNotificationVo findManagerReportingNotification(Long id) {
		return managerReportingNotificationDao.findManagerReportingNotification(id);
	}

	public ManagerReportingNotification findManagerReportingNotification() {
		return managerReportingNotificationDao.findManagerReportingNotification();
	}

}
