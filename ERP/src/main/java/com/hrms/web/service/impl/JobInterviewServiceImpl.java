package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.JobInterviewDao;
import com.hrms.web.entities.JobInterview;
import com.hrms.web.service.JobInterviewService;
import com.hrms.web.vo.JobInterviewVo;

/**
 * 
 * @author Vinutha
 * @since 24-March-2015
 *
 */
@Service
public class JobInterviewServiceImpl implements JobInterviewService{

	@Autowired
	private JobInterviewDao interviewDao;
	/**
	 * method to save interview info
	 * @param interviewVo
	 * @return Long
	 */
	public Long saveJobInterviewInfo(JobInterviewVo interviewVo) {
		return interviewDao.saveJobInterviewInfo(interviewVo);
		
	}
	/**
	 * method to save interview decription
	 * @param interviewVo
	 * 
	 */
	public void saveInterviewDesc(JobInterviewVo interviewVo) {
		interviewDao.saveInterviewDesc(interviewVo);
		
	}
	/**
	 * method to save interview additional info
	 * @param interviewVo
	 */
	public void saveInterviewAddInfo(JobInterviewVo interviewVo) {
		interviewDao.saveInterviewAddInfo(interviewVo);
		
	}
	/**
	 * method to update job interview info
	 * @param interviewVo
	 */
	public void updateJobInterviewInfo(JobInterviewVo interviewVo) {
		interviewDao.updateJobInterviewInfo(interviewVo);
		
	}
	/**
	 * method to delete job interview
	 * @param interviewVo
	 */
	public void deleteJobInterview(JobInterviewVo interviewVo) {
		interviewDao.deleteJobInterview(interviewVo);
		
	}
	/**
	 * method to delete interview by id
	 * @param Long id
	 */
	public void deleteJobInterviewById(Long id) {
		interviewDao.deleteJobInterviewById(id);
		
	}
	/**
	 * method to list all interviews
	 * @return List<JobInterviewVo>
	 */
	public List<JobInterviewVo> listAllInterviews() {
		
		return interviewDao.listAllInterviews();
	}
	/**
	 * method to find and return interview object by id
	 * @param Long id
	 * @return JobInterview
	 */
	public JobInterview findAndReturnInterviewObjectById(Long id) {
		
		return interviewDao.findAndReturnInterviewObjectById(id);
	}
	/**
	 * method to list interviews based on place
	 * @param String place
	 * @return List<JobInterviewVo>
	 */
	public List<JobInterviewVo> listInterviewsBasedOnPlace(String place) {
		
		return interviewDao.listInterviewsBasedOnPlace(place);
	}
	public JobInterviewVo findJobInterviewById(Long id) {
		
		return interviewDao.findJobInterviewById(id);
	}
	public void updateDocuments(JobInterviewVo interviewVo) {
		interviewDao.updateDocuments(interviewVo);
		
	}
	public void deleteDocument(String url) {
		interviewDao.deleteDocument(url);
		
	}
	public List<JobInterviewVo> listInterviewsByDateRange(Date startDate, Date endDate) {
		return interviewDao.listInterviewsByDateRange(startDate, endDate);
	}
	public List<JobInterviewVo> listInterviewsByJobPostDateRange(Long postId, Date startDate, Date endDate) {
		return interviewDao.listInterviewsByJobPostDateRange(postId, startDate, endDate);
	}
	/**
	 * Method to list job interviews by branch and date
	 * @param branchId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<JobInterviewVo> listInterviewByBranchAndDate(Long branchId , Date startDate , Date endDate){
		return interviewDao.listInterviewByBranchAndDate(branchId, startDate, endDate);
	}
	/**
	 * Method to Get interview by post and date
	 * @param postId
	 * @param date
	 * @return
	 */
	public JobInterviewVo listInterviewByPostAndDate(Long postId , String date){
		
		return interviewDao.listInterviewByPostAndDate(postId, date);
	}
	/**
	 * Method to getinterviews by date
	 * @param date
	 * @return
	 */
	public List<JobInterviewVo> listInterviewsByDate(String date){
		return interviewDao.listInterviewsByDate(date);
	}
}
