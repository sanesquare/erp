package com.hrms.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.WorkSheetTaskDao;
import com.hrms.web.service.WorkSheetTaskService;
import com.hrms.web.vo.WorksheetTasksVo;

/**
 * 
 * @author Vinutha
 * @since 19-May-2015
 *
 */
@Service
public class WorkSheetTaskServiceImpl implements WorkSheetTaskService {

	@Autowired
	private WorkSheetTaskDao taskDao;
	
	/**
	 * Method to find task by id
	 * @param id
	 * @return
	 */
	public WorksheetTasksVo findTaskById(Long id){
		return taskDao.findTaskById(id);
	}
	/**
	 * Method to delete task
	 * @param id
	 */
	public void deleteTaskById(Long id){
		 taskDao.deleteTaskById(id);
	}
}
