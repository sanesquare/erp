package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.WorkShiftDetailsDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ShiftDay;
import com.hrms.web.entities.WorkShift;
import com.hrms.web.service.WorkShiftDetailsService;
import com.hrms.web.vo.WorkShiftDetailsVo;

/**
 * 
 * @author Jithin Mohan
 * @since 1-April-2015
 *
 */
@Service
public class WorkShiftDetailsServiceImpl implements WorkShiftDetailsService {

	@Autowired
	private WorkShiftDetailsDao shiftDetailsDao;

	public void saveWorkShiftDetails(List<WorkShiftDetailsVo> shiftDetailsVo) {
		shiftDetailsDao.saveWorkShiftDetails(shiftDetailsVo);
	}

	public void updateWorkShiftDetails(WorkShiftDetailsVo shiftDetailsVo) {
		shiftDetailsDao.updateWorkShiftDetails(shiftDetailsVo);
	}

	public void deleteWorkShiftDetails(Long shiftDetailsId) {
		shiftDetailsDao.deleteWorkShiftDetails(shiftDetailsId);
	}

	public WorkShift findWorkShiftDetails(Long shiftDetailsId) {
		return shiftDetailsDao.findWorkShiftDetails(shiftDetailsId);
	}

	public List<WorkShift> findWorkShiftDetails(ShiftDay shiftDay) {
		return shiftDetailsDao.findWorkShiftDetails(shiftDay);
	}

	public WorkShiftDetailsVo findWorkShiftDetailsById(Long shiftDetailsId) {
		return shiftDetailsDao.findWorkShiftDetailsById(shiftDetailsId);
	}

	public List<WorkShiftDetailsVo> findWorkShiftDetailsByEmployee(Employee employee) {
		return shiftDetailsDao.findWorkShiftDetailsByEmployee(employee);
	}

	public List<WorkShiftDetailsVo> findAllWorkShiftDetails() {
		return shiftDetailsDao.findAllWorkShiftDetails();
	}

	public WorkShiftDetailsVo findWorkShiftByDay(String day, String employeeCode) {
		return shiftDetailsDao.findWorkShiftByDay(day, employeeCode);
	}

}
