package com.hrms.web.service;

import java.util.Date;
import java.util.List;

import com.hrms.web.vo.CommissionVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
public interface CommissionService {

	/**
	 * method to save Commission
	 * 
	 * @param CommissionVo
	 * @return
	 */
	public Long saveCommission(CommissionVo commissionVo);

	/**
	 * method to update Commission
	 * 
	 * @param CommissionVo
	 */
	public void updateCommission(CommissionVo commissionVo);

	/**
	 * method to delete Commission
	 * 
	 * @param CommissionId
	 */
	public void deleteCommission(Long commissionId);

	/**
	 * method to list all Commissions
	 * 
	 * @return
	 */
	public List<CommissionVo> listAllCommissiones();

	/**
	 * method to list Commissions by employee
	 * 
	 * @param employeeId
	 * @return
	 */
	public List<CommissionVo> listCommissionesByEmployee(Long employeeId);

	/**
	 * method to list Commissions by title
	 * 
	 * @param CommissionTitleId
	 * @return
	 */
	public List<CommissionVo> listCommissionesByTitle(Long commissionTitleId);

	/**
	 * method to list Commissions by title
	 * 
	 * @param CommissionTitleId
	 * @return
	 */
	public List<CommissionVo> listCommissionesByStatus(Long statusId);

	/**
	 * method to get Commission details
	 * 
	 * @param CommissionId
	 * @return
	 */
	public CommissionVo getCommissionById(Long commissionId);

	/**
	 * method to get Commission by employee and Commission title
	 * 
	 * @param employeeId
	 * @param CommissionTitleId
	 * @return
	 */
	public CommissionVo getCommissionByEmployeeAndTitle(String employeeCode, Long commissionTitleId);

	/**
	 * method to update commission status
	 * 
	 * @param commissionVo
	 */
	public void updateCommissinStatus(CommissionVo commissionVo);

	/**
	 * method to get commission by employee , start date & end date
	 * 
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<CommissionVo> listCommissionesByEmployeeStartDateEndDate(Long employeeId, Date startDate, Date endDate);

	/**
	 * method to get commission by start date & end date
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<CommissionVo> listCommissionesByStartDateEndDate(Date startDate, Date endDate);

	/**
	 * method to find all Commissions by employee
	 * 
	 * @param employeeCode
	 * @return
	 */
	public List<CommissionVo> findAllCommissionesByEmployee(String employeeCode);
}
