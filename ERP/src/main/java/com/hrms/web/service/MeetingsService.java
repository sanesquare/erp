package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.MeetingsVo;
/**
 * 
 * @author Shamsheer
 * @since 12-March-2015
 *
 */
public interface MeetingsService {

	/**
	 * method to save new meetings
	 * @param meetingsVo
	 * @return 
	 * @throws Exception 
	 */
	public Long saveMeetings(MeetingsVo meetingsVo) throws Exception;
	
	/**
	 * method to update an existing meeting
	 * @param meetingsVo
	 */
	public void updateMeetings(MeetingsVo meetingsVo);
	
	/**
	 * method to delete an existing meeting
	 * @param meetingsId
	 */
	public void deleteMeeting(Long meetingsId);
	
	/**
	 * method to list all meetings
	 * @return
	 */
	public List<MeetingsVo> listAllMeetings();
	
	/**
	 * method to find meetings by id
	 * @param id
	 * @return
	 */
	public MeetingsVo findMeetingsById(Long id);
	
	/**
	 * method to find meetings by title
	 * @param title
	 * @return
	 */
	public MeetingsVo findMeetingsByTitle(String title);
	/**
	 * method to upload document
	 * @param meetingsVo
	 */
	public void updateDocument(MeetingsVo meetingsVo);
	
	/**
	 * method to delete document
	 * @param url
	 */
	public void deleteDocument(String url);
}
