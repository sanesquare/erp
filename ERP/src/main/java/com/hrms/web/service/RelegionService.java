package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.Relegion;
import com.hrms.web.vo.RelegionVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
public interface RelegionService {

	/**
	 * method to save relegion
	 * 
	 * @param relegionVo
	 */
	public void saveRelegion(RelegionVo relegionVo);

	/**
	 * method to update religion
	 * 
	 * @param relegionVo
	 */
	public void updateRelegion(RelegionVo relegionVo);

	/**
	 * method to delete religion
	 * 
	 * @param id
	 */
	public void deleteReligion(Long id);

	/**
	 * method to find religion by id
	 * 
	 * @param id
	 * @return
	 */
	public RelegionVo findRelegianById(Long id);

	/**
	 * method to find relegian by name
	 * 
	 * @param relegian
	 * @return
	 */
	public RelegionVo findReligianByName(String relegian);

	/**
	 * method to find relegion
	 * 
	 * @param id
	 * @return
	 */
	public Relegion findRelegian(Long id);

	/**
	 * method to find relegian
	 * 
	 * @param relegian
	 * @return
	 */
	public Relegion findRelegian(String relegian);

	/**
	 * list all relegians
	 * 
	 * @return
	 */
	public List<RelegionVo> listAllRelegians();
}
