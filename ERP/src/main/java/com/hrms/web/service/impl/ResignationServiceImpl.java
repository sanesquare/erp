package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ResignationDao;
import com.hrms.web.service.ResignationService;
import com.hrms.web.vo.ResignationVo;

/**
 * 
 * @author Shamsheer
 * @since 24-April-2015
 */
@Service
public class ResignationServiceImpl implements ResignationService{

	@Autowired
	private ResignationDao dao;

	public Long saveResignation(ResignationVo resignationVo) {
		return dao.saveResignation(resignationVo);
	}

	public void updateResignation(ResignationVo resignationVo) {
		dao.updateResignation(resignationVo);
	}

	public void deleteResignation(Long resignationId) {
		dao.deleteResignation(resignationId);
	}

	public List<ResignationVo> listAllResignation() {
		return dao.listAllResignation();
	}

	public ResignationVo getResignationById(Long resignationId) {
		return dao.getResignationById(resignationId);
	}

	public List<ResignationVo> listResignationByEmployee(Long employeeId) {
		return dao.listResignationByEmployee(employeeId);
	}

	public List<ResignationVo> listResignationByEmployee(String employeeCode) {
		return dao.listResignationByEmployee(employeeCode);
	}

	public void updateDocument(ResignationVo resignationVo) {
		dao.updateDocument(resignationVo);
	}

	public void deleteDocument(String url) {
		dao.deleteDocument(url);
	}

	public void updateStatus(ResignationVo resignationVo) {
		dao.updateStatus(resignationVo);
	}

	public List<ResignationVo> listResignationBetweenDates(String startDate, String endDate) {
		return dao.listResignationBetweenDates(startDate, endDate);
	}

	public List<ResignationVo> listResignationsByEmployeeIds(List<Long> employeeIds) {
		return dao.listResignationsByEmployeeIds(employeeIds);
	}

	public List<ResignationVo> findResignationBetweenDates(String startDate, String endDate) {
		return dao.findResignationBetweenDates(startDate, endDate);
	}

	public List<ResignationVo> findResignationByBranchBetweenDates(Long branchId, String startDate, String endDate) {
		return dao.findResignationByBranchBetweenDates(branchId, startDate, endDate);
	}

	public List<ResignationVo> findResignationByDepartmentBetweenDates(Long departmentId, String startDate,
			String endDate) {
		return dao.findResignationByDepartmentBetweenDates(departmentId, startDate, endDate);
	}

	public List<ResignationVo> findResignationByBranchDepartmentBetweenDates(Long branchId, Long departmentId,
			String startDate, String endDate) {
		return dao.findResignationByBranchDepartmentBetweenDates(branchId, departmentId, startDate, endDate);
	}

	public List<ResignationVo> findAllResignationByEmployee(String employeeCode) {
		return dao.findAllResignationByEmployee(employeeCode);
	}
}
