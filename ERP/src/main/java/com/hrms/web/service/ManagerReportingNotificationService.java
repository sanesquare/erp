package com.hrms.web.service;

import com.hrms.web.entities.ManagerReportingNotification;
import com.hrms.web.vo.ManagerReportingNotificationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public interface ManagerReportingNotificationService {

	/**
	 * method to update ManagerReportingNotification
	 * 
	 * @param isRepot
	 */
	public void updateManagerReportingNotification(boolean isRepot);

	/**
	 * method to find ManagerReportingNotification by id
	 * 
	 * @param id
	 * @return ManagerReportingNotificationVo
	 */
	public ManagerReportingNotificationVo findManagerReportingNotification(Long id);

	/**
	 * method to find ManagerReportingNotification
	 * 
	 * @return ManagerReportingNotification
	 */
	public ManagerReportingNotification findManagerReportingNotification();
}
