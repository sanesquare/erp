package com.hrms.web.service;

import java.util.Date;
import java.util.List;

import com.hrms.web.vo.ReimbursementsVo;

/**
 * 
 * @author Shamsheer
 * @since 17-April-2015
 */
public interface ReimbursementsService {

	/**
	 * method to save reimbursement
	 * 
	 * @param vo
	 * @return
	 */
	public Long saveReimbursement(ReimbursementsVo vo);

	/**
	 * method to update reimbursement
	 * 
	 * @param vo
	 */
	public void updateReimbursement(ReimbursementsVo vo);

	/**
	 * method to delete reimbursements
	 * 
	 * @param reimbursementId
	 */
	public void deleteReimbursements(Long reimbursementId);

	/**
	 * method to update documents
	 * 
	 * @param vo
	 */
	public void updateDocuments(ReimbursementsVo vo);

	/**
	 * method to delete documents
	 * 
	 * @param url
	 */
	public void deleteDocuments(String url);

	/**
	 * method to list all reimbursements
	 * 
	 * @return
	 */
	public List<ReimbursementsVo> listAllReimbursements();

	/**
	 * method to list reimbursements by employee
	 * 
	 * @param employeeId
	 * @return
	 */
	public List<ReimbursementsVo> listReimbursementsByEmployee(Long employeeId);

	/**
	 * method to update status
	 * 
	 * @param vo
	 */
	public void updateStatus(ReimbursementsVo vo);

	/**
	 * method to get reimbursements by id
	 * 
	 * @param reimbursementsId
	 * @return
	 */
	public ReimbursementsVo getReimbursementsById(Long reimbursementsId);

	/**
	 * method to list reimbursements by employee , startDate * endDate
	 * 
	 * @param employeeId
	 * @return
	 */
	public List<ReimbursementsVo> listReimbursementsByEmployeeStartDateEndDate(Long employeeId, Date startDate,
			Date endDate);

	/**
	 * method to list reimbursements by startDate * endDate
	 * 
	 * @param employeeId
	 * @return
	 */
	public List<ReimbursementsVo> listReimbursementsByStartDateEndDate(Date startDate, Date endDate);

	/**
	 * method to find all reimbursements by employee
	 * 
	 * @param employeeCode
	 * @return
	 */
	public List<ReimbursementsVo> findAllReimbursementsByEmployee(String employeeCode);

}
