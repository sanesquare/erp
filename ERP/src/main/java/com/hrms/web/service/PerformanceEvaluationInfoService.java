package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.PerformanceEvaluationInfo;
import com.hrms.web.vo.IndividualEvaluationVo;
import com.hrms.web.vo.PerformanceVo;

/**
 * 
 * @author Vinutha
 * @since 23-May-2015
 *
 */
public interface PerformanceEvaluationInfoService {

	/**
	 * Method to save performance evaluation
	 * @param performanceVo
	 * @return
	 */
	public Long savePerformanceEvaluation(PerformanceVo performanceVo);
	/**
	 * Method to update performance evaluation
	 * @param performanceVo
	 */
	public void updatePerformanceEvaluation(PerformanceVo performanceVo);
	/**
	 * Method to delete performance evaluation
	 * @param id
	 */
	public void deletePerformanceEvaluation(Long id);
	/**
	 * Method to list performance evaluations
	 * @return
	 */
	public List<PerformanceVo> listPerformanceEvaluations();
	/**
	 * method to return performance evaluation info object by id
	 * @param id
	 * @return
	 */
	public PerformanceEvaluationInfo findAndReturnEvaluationById(Long id);
	/**
	 * method to return performance evaluation info Vo object by id
	 * @param id
	 * @return
	 */
	public PerformanceVo findEvaluationById(Long id);
	/**
	 * Method to find employee evaluation details
	 * @param employeeCode
	 * @param id
	 * @return
	 */
	public IndividualEvaluationVo finddEmployeeEvaluationInfo(String employeeCode , Long id);
	/**
	 * Method to delete individual evaluation record
	 * @param id
	 */
	public void deleteEmployeeEvaluation(Long id , Long individualId);
	/**
	 * Method to find performance evaluations within date range
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PerformanceVo> findEvaluationWithinDateRange(String startDate , String endDate);
}
