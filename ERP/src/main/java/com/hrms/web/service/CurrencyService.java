package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.Currency;
import com.hrms.web.vo.CurrencyVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
public interface CurrencyService {

	/**
	 * method to save new Currency
	 * 
	 * @param currencyVo
	 */
	public void saveCurrency(CurrencyVo currencyVo);

	/**
	 * method to update Currency
	 * 
	 * @param currencyVo
	 */
	public void updateCurrency(CurrencyVo currencyVo);

	/**
	 * method to delete Currency
	 * 
	 * @param currencyId
	 */
	public void deleteCurrency(Long currencyId);

	/**
	 * method to find Currency by id
	 * 
	 * @param currencyId
	 * @return Currency
	 */
	public Currency findCurrency(Long currencyId);

	/**
	 * method to find Currency by id
	 * 
	 * @param currencyId
	 * @return CurrencyVo
	 */
	public CurrencyVo findCurrencyById(Long currencyId);

	/**
	 * method to find all Currency
	 * 
	 * @return List<CurrencyVo>
	 */
	public List<CurrencyVo> findAllCurrency();
}
