package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.WarningDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Warning;
import com.hrms.web.service.WarningService;
import com.hrms.web.vo.WarningVo;

/**
 * 
 * @author Jithin Mohan
 * @since 20-March-2015
 *
 */
@Service
public class WarningServiceImpl implements WarningService {

	@Autowired
	private WarningDao warningDao;

	public void saveWarning(WarningVo warningVo) {
		warningDao.saveWarning(warningVo);
	}

	public void saveWarningDocuments(WarningVo warningVo) {
		warningDao.saveWarningDocuments(warningVo);
	}

	public void updateWarning(WarningVo warningVo) {
		warningDao.updateWarning(warningVo);
	}

	public void deleteWarning(Long warningId) {
		warningDao.deleteWarning(warningId);
	}

	public void deleteWarningDocument(Long documentId) {
		warningDao.deleteWarningDocument(documentId);
	}

	public Warning findWarning(Long warningId) {
		return warningDao.findWarning(warningId);
	}

	public WarningVo findWarningById(Long warningId) {
		return warningDao.findWarningById(warningId);
	}

	public List<WarningVo> findWarningByDate(Date warningOn) {
		return warningDao.findWarningByDate(warningOn);
	}

	public List<WarningVo> findWarningBetweenDates(Date startDate, Date endDate) {
		return warningDao.findWarningBetweenDates(startDate, endDate);
	}

	public List<WarningVo> findWarningByCreatedDate(Date createdOn) {
		return warningDao.findWarningByCreatedDate(createdOn);
	}

	public List<WarningVo> findWarningCreatedBetweenDates(Date startDate, Date endDate) {
		return warningDao.findWarningCreatedBetweenDates(startDate, endDate);
	}

	public List<WarningVo> findWarningByCreator(Employee warningBy) {
		return warningDao.findWarningByCreator(warningBy);
	}

	public List<WarningVo> findAllWarning() {
		return warningDao.findAllWarning();
	}

}
