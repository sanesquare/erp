package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.WorkShiftDao;
import com.hrms.web.entities.EmployeeWorkShift;
import com.hrms.web.service.WorkShiftService;
import com.hrms.web.vo.WorkShiftVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Service
public class WorkShiftServiceImpl implements WorkShiftService {

	@Autowired
	private WorkShiftDao workShiftDao;

	public void saveWorkShift(WorkShiftVo workShiftVo) {
		workShiftDao.saveWorkShift(workShiftVo);
	}

	public void updateWorkShift(WorkShiftVo workShiftVo) {
		workShiftDao.updateWorkShift(workShiftVo);
	}

	public void deleteWorkShift(Long workShiftId) {
		workShiftDao.deleteWorkShift(workShiftId);
	}

	public EmployeeWorkShift findWorkShift(Long workShiftId) {
		return workShiftDao.findWorkShift(workShiftId);
	}

	public EmployeeWorkShift findWorkShift(String shiftType) {
		return workShiftDao.findWorkShift(shiftType);
	}

	public WorkShiftVo findWorkShiftById(Long workShiftId) {
		return workShiftDao.findWorkShiftById(workShiftId);
	}

	public WorkShiftVo findWorkShiftByType(String shiftType) {
		return workShiftDao.findWorkShiftByType(shiftType);
	}

	public List<WorkShiftVo> findAllWorkShift() {
		return workShiftDao.findAllWorkShift();
	}

}
