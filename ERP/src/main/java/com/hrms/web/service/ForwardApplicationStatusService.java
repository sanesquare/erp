package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.vo.ForwardApplicationStatusVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
public interface ForwardApplicationStatusService {

	/**
	 * method to save status
	 * @param statusVo
	 */
	public void saveStatus(ForwardApplicationStatusVo statusVo);

	/**
	 * method to update status
	 * @param statusVo
	 */
	public void updateStatus(ForwardApplicationStatusVo statusVo);

	/**
	 * method to delete status
	 * @param id
	 */
	public void deleteStatus(Long id);

	/**
	 * method to list all status
	 * @return
	 */
	public List<ForwardApplicationStatusVo> listAllStatus();
	/**
	 * method to find and return status
	 * @param statusId
	 * @return
	 */
	public ForwardApplicationStatus getStatus(String status);
}
