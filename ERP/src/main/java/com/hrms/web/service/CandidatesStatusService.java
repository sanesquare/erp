package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.CandidateStatus;
import com.hrms.web.vo.CandidatesStatusVo;

/**
 * 
 * @author Vinutha
 * @since 24-March-2015
 *
 */
public interface CandidatesStatusService {
	
	/**
	 * method to save new candidates status
	 * @param candidatesStatusVo
	 */
	public void saveCandidatesStatus(CandidatesStatusVo candidatesStatusVo);
	/**
	 * method to list all status
	 * @param candidatesStatusVo
	 */
	public List<CandidatesStatusVo> listAllStatus();
	/**
	 * method to delete status
	 * @param candidatesStatusVo
	 */
	public void deleteCandidatesStatus(CandidatesStatusVo candidatesStatusVo);
	/**
	 * method to find and return candidate status object by id
	 * @param id
	 * @return
	 */
	public CandidateStatus findAndReturnStatusObjectById(Long id);
	/**
	 * method to find and return candidates status
	 * @param id
	 * @return
	 */
	public CandidatesStatusVo findAndReturnStatusById(Long id);

}
