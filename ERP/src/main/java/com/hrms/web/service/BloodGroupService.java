package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.BloodGroup;
import com.hrms.web.vo.BloodGroupVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
public interface BloodGroupService {

	/**
	 * method to save BloodGroup
	 * 
	 * @param bloodGroupVo
	 */
	public void saveBloodGroup(BloodGroupVo bloodGroupVo);

	/**
	 * method to update BloodGroup
	 * 
	 * @param bloodGroupVo
	 */
	public void updateBloodGroup(BloodGroupVo bloodGroupVo);

	/**
	 * method to delete BloodGroup
	 * 
	 * @param groupId
	 */
	public void deleteBloodGroup(Long groupId);

	/**
	 * method to find BloodGroup by id
	 * 
	 * @param groupId
	 * @return BloodGroup
	 */
	public BloodGroup findBloodGroup(Long groupId);

	/**
	 * method to find BloodGroup by group
	 * 
	 * @param groupName
	 * @return BloodGroup
	 */
	public BloodGroup findBloodGroup(String groupName);

	/**
	 * method to find BloodGroup by id
	 * 
	 * @param groupId
	 * @return BloodGroupVo
	 */
	public BloodGroupVo findBloodGroupById(Long groupId);

	/**
	 * method to find BloodGroup by group
	 * 
	 * @param groupName
	 * @return BloodGroupVo
	 */
	public BloodGroupVo findBloodGroupByGroup(String groupName);

	/**
	 * method to find all BloodGroup
	 * 
	 * @return List<BloodGroupVo>
	 */
	public List<BloodGroupVo> findAllBloodGroup();
}
