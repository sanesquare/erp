package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.AdvanceSalaryStatus;
import com.hrms.web.vo.AdvanceSalaryStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public interface AdvanceSalaryStatusService {

	/**
	 * 
	 * @param advanceSalaryStatusVo
	 */
	public void saveAdvanceSalaryStatus(AdvanceSalaryStatusVo advanceSalaryStatusVo);

	/**
	 * 
	 * @param advanceSalaryStatusVo
	 */
	public void updateAdvanceSalaryStatus(AdvanceSalaryStatusVo advanceSalaryStatusVo);

	/**
	 * 
	 * @param advanceSalaryStatusId
	 */
	public void deleteAdvanceSalaryStatus(Long advanceSalaryStatusId);

	/**
	 * 
	 * @param advanceSalaryStatusId
	 * @return
	 */
	public AdvanceSalaryStatus findAdvanceSalaryStatus(Long advanceSalaryStatusId);

	/**
	 * 
	 * @param status
	 * @return
	 */
	public AdvanceSalaryStatus findAdvanceSalaryStatus(String status);

	/**
	 * 
	 * @param advanceSalaryStatusId
	 * @return
	 */
	public AdvanceSalaryStatusVo findAdvanceSalaryStatusById(Long advanceSalaryStatusId);

	/**
	 * 
	 * @param status
	 * @return
	 */
	public AdvanceSalaryStatusVo findAdvanceSalaryStatusByStatus(String status);

	/**
	 * 
	 * @return
	 */
	public List<AdvanceSalaryStatusVo> findAllAdvanceSalaryStatus();
}
