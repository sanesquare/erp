package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.DeductionTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
public interface DeductionTitleService {

	/**
	 * method to save Deduction title
	 * @param titleVo
	 */
	public void saveDeductionTitle(DeductionTitleVo titleVo); 
	
	/**
	 * method to update Deduction title
	 * @param titleVo
	 */
	public void updateDeductionTitle(DeductionTitleVo titleVo);
	
	/**
	 * method to delete Deduction title
	 * @param DeductionTitleId
	 */
	public void deleteDeductionTitle(Long deductionTitleId);
	
	/**
	 * method to delete Deduction title
	 * @param DeductionTitle
	 */
	public void deleteDeductionTitle(String deductionTitle);

	/**
	 * method to list all Deduction titles
	 * @return
	 */
	public List<DeductionTitleVo> listAllDeductionTitles();
}
