package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.AdjustmentDao;
import com.hrms.web.service.AdjustmentService;
import com.hrms.web.vo.AdjustmentVo;
/**
 * 
 * @author Shamsheer
 * @since 14-April-2015
 */
@Service
public class AdjustmentServiceImpl implements AdjustmentService{

	@Autowired
	private AdjustmentDao dao;

	public Long saveAdjustment(AdjustmentVo vo) {
		return dao.saveAdjustment(vo);
	}

	public void updateAdjustment(AdjustmentVo vo) {
		dao.updateAdjustment(vo);
	}

	public void deleteAdjustment(Long adjustmentId) {
		dao.deleteAdjustment(adjustmentId);
	}

	public List<AdjustmentVo> listAllAdjustments() {
		return dao.listAllAdjustments();
	}

	public List<AdjustmentVo> listAdjustmentsByEmployee(Long employeeId) {
		return dao.listAdjustmentsByEmployee(employeeId);
	}

	public List<AdjustmentVo> listAdjustmentsByTitle(Long titleId) {
		return dao.listAdjustmentsByTitle(titleId);
	}

	public List<AdjustmentVo> listAdjustmentsByType(String type) {
		return dao.listAdjustmentsByType(type);
	}

	public AdjustmentVo findAdjustmentById(Long adjustmentId) {
		return dao.findAdjustmentById(adjustmentId);
	}

	public AdjustmentVo getAdjustmentByEmployeeAndTitle(String employeeCode, Long titleId) {
		return dao.getAdjustmentByEmployeeAndTitle(employeeCode, titleId);
	}

	public List<AdjustmentVo> listAdjustmentsByStartDateEndDate(Date startDate, Date endDate) {
		return dao.listAdjustmentsByStartDateEndDate(startDate, endDate);
	}

	public List<AdjustmentVo> listAdjustmentsByEmployeeStartDateEndDate(Long employeeId, Date startDate, Date endDate) {
		return dao.listAdjustmentsByEmployeeStartDateEndDate(employeeId, startDate, endDate);
	}
}
