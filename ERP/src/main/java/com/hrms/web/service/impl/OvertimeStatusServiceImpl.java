package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.OvertimeStatusDao;
import com.hrms.web.entities.OvertimeStatus;
import com.hrms.web.service.OvertimeStatusService;
import com.hrms.web.vo.OvertimeStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Service
public class OvertimeStatusServiceImpl implements OvertimeStatusService {

	@Autowired
	private OvertimeStatusDao overtimeStatusDao;

	public void saveOvertimeStatus(OvertimeStatusVo overtimeStatusVo) {
		overtimeStatusDao.saveOvertimeStatus(overtimeStatusVo);
	}

	public void updateOvertimeStatus(OvertimeStatusVo overtimeStatusVo) {
		overtimeStatusDao.updateOvertimeStatus(overtimeStatusVo);
	}

	public void deleteOvertimeStatus(Long overtimeStatusId) {
		overtimeStatusDao.deleteOvertimeStatus(overtimeStatusId);
	}

	public OvertimeStatus findOvertimeStatus(Long overtimeStatusId) {
		return overtimeStatusDao.findOvertimeStatus(overtimeStatusId);
	}

	public OvertimeStatus findOvertimeStatus(String status) {
		return overtimeStatusDao.findOvertimeStatus(status);
	}

	public OvertimeStatusVo findOvertimeStatusById(Long overtimeStatusId) {
		return overtimeStatusDao.findOvertimeStatusById(overtimeStatusId);
	}

	public OvertimeStatusVo findOvertimeStatusByStatus(String status) {
		return overtimeStatusDao.findOvertimeStatusByStatus(status);
	}

	public List<OvertimeStatusVo> findAllOvertimeStatus() {
		return overtimeStatusDao.findAllOvertimeStatus();
	}

}
