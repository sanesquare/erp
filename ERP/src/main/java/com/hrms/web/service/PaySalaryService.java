package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.PayPaysalaryVo;
import com.hrms.web.vo.PaySalaryDailySalaryVo;
import com.hrms.web.vo.PaySalaryEmployeeItemsVo;
import com.hrms.web.vo.PaySalaryHourlySalaryVo;
import com.hrms.web.vo.PaySalaryVo;

/**
 * 
 * @author Shamsheer
 * @since 08-June-2015
 *
 */
public interface PaySalaryService {

	/**
	 * method to calculate paysalary vo
	 * @param paySalaryVo
	 * @return
	 */
	public PaySalaryVo calculatePaySalary(PaySalaryVo paySalaryVo);
	
	/**
	 * method to calculate paysalary daily
	 * @param paySalaryVo
	 * @return
	 */
	public PaySalaryVo calculateDailyPaySalary(PaySalaryVo paySalaryVo);
	
	/**
	 * method to calculate hourly pay salary
	 * @param paySalaryVo
	 * @return
	 */
	public PaySalaryVo calculateHourlyPaySalary(PaySalaryVo paySalaryVo); 
	
	/**
	 * method to find pay salary by company
	 * @param companyId
	 * @return
	 */
	public List<PaySalaryVo> listPaySalariesByCompany(Long companyId);
	
	/**
	 * method to save paysalary hourly wages
	 * @param paySalaryVo
	 * @return
	 */
	public Long savePaysalaryHourly(PaySalaryVo paySalaryVo);
	
	/**
	 * method to save paysalary daily wages
	 * @param paySalaryVo
	 * @return
	 */
	public Long savePaysalaryDaily(PaySalaryVo paySalaryVo);
	
	/**
	 * method to save paysalary monthly
	 * @param paySalaryVo
	 * @return
	 */
	public Long savePaysalaryMonthly(PaySalaryVo paySalaryVo);
	
	/**
	 * metod to list pay salaries
	 * @param employeeCode
	 * @return
	 */
	public List<PaySalaryVo> listPaySalaries(String employeeCode);
	/**
	 * method to find daily pay salary
	 * @param id
	 * @return
	 */
	public PaySalaryVo findDailyPaySalary(Long id);
	
	/**
	 * method to find hourly pay salary
	 * @param id
	 * @return
	 */
	public PaySalaryVo findHourlyPaySalary(Long id);
	/**
	 * method to find hourly pay salary
	 * @param id
	 * @return
	 */
	public PaySalaryVo findMonthlyPaySalary(Long id);
	
	/**
	 * method to update daily pay salary
	 * @param paySalaryVo
	 */
	public void updateDailyPaySalary(PaySalaryVo paySalaryVo);
	
	/**
	 * method to update hourly pay salary
	 * @param paySalaryVo
	 */
	public void updateHourlyPaySalary(PaySalaryVo paySalaryVo);
	
	/**
	 * method to delete paysalary
	 * @param id
	 */
	public void deletePaysalary(Long id);
	
	/**
	 * method to update monthly pay salary
	 * @param paySalaryVo
	 */
	public void updateMonthlyPaySalary(PaySalaryVo paySalaryVo);
	
	/**
	 * method to send group sms
	 * @param id
	 * @return
	 */
	public Boolean sendGroupSms(Long id);
	
	/**
	 * method to send individual sms
	 * @param id
	 * @return
	 */
	public Boolean sendIndividualSms(String	empCode,Long id,String type);
	
	/**
	 * method to find employee's hourly wage between dates
	 * @param employeeCode
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PaySalaryHourlySalaryVo> getEmployeeHourlyWage(String employeeCode , String startDate , String endDate);
	
	/**
	 * method to find employee's daily wage between dates
	 * @param employeeCode
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PaySalaryDailySalaryVo> getEmployeeDailyWage(String employeeCode , String startDate , String endDate);
	
	/**
	 * method to find employee's monthly wage between dates
	 * @param employeeCode
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PaySalaryEmployeeItemsVo> getEmployeeMonthlyWage(String employeeCode , String startDate , String endDate);
	/**
	 * method to find salaries by status
	 * @param status
	 * @return
	 */
	public List<PaySalaryVo> findSalariesByStatus(String status,Long companyId);
	
	/**
	 * method to find unpaid salaries
	 * @return
	 */
	public List<PaySalaryVo> findUnpaidSalaries(Long companyId);
	
	/**
	 * method to update salary status
	 * @param salaryId
	 * @param statusId
	 */
	public void updateSalaryStatus(Long salaryId,Long statusId);
	
	/**
	 * method to pay salary
	 * @param ledgerId
	 * @param salaryId
	 */
	public void paySalary(Long ledgerId,Long salaryId);
	/**
	 * method to pay employee salaries
	 * @param vo
	 */
	public void updatePaySalaryPay(PayPaysalaryVo vo);
}
