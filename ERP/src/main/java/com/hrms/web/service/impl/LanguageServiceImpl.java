package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.LanguageDao;
import com.hrms.web.entities.Language;
import com.hrms.web.logger.Log;
import com.hrms.web.service.LanguageService;
import com.hrms.web.vo.LanguageVo;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
@Service
public class LanguageServiceImpl implements LanguageService {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private LanguageDao languageDao;

	/**
	 * method to save new language
	 * 
	 * @param languageVo
	 */
	public void saveLanguage(LanguageVo languageVo) {
		logger.info("Inside Language Service >>>> saveLanguage.........");
		languageDao.saveLanguage(languageVo);
	}

	/**
	 * method to update language information
	 * 
	 * @param languageVo
	 */
	public void updateLanguage(LanguageVo languageVo) {
		logger.info("Inside Language Service >>>> updateLanguage.........");
		languageDao.updateLanguage(languageVo);
	}

	/**
	 * method to delete language information
	 * 
	 * @param languageId
	 */
	public void deleteLanguage(Long languageId) {
		logger.info("Inside Language Service >>>> deleteLanguage.........");
		languageDao.deleteLanguage(languageId);
	}

	/**
	 * method to find language byid and return language object
	 * 
	 * @param languageId
	 * @return language
	 */
	public Language findLanguage(Long languageId) {
		logger.info("Inside Language Service >>>> findLanguage.........");
		return languageDao.findLanguage(languageId);
	}

	/**
	 * method to find language by id and return languagevo object
	 * 
	 * @param languageId
	 * @return languagevo
	 */
	public LanguageVo findLanguageById(Long languageId) {
		logger.info("Inside Language Service >>>> findLanguageById.........");
		return languageDao.findLanguageById(languageId);
	}

	/**
	 * method to find language by name and return languagevo object
	 * 
	 * @param languageName
	 * @return languagevo
	 */
	public LanguageVo findLanguageByName(String languageName) {
		logger.info("Inside Language Service >>>> findLanguageByName.........");
		return languageDao.findLanguageByName(languageName);
	}

	/**
	 * method to find all languages
	 * 
	 * @return List<LanguageVo>
	 */
	public List<LanguageVo> findAllLanguage() {
		logger.info("Inside Language Service >>>> findAllLanguage.........");
		return languageDao.findAllLanguage();
	}

	/**
	 * method to find language by name and return language object
	 * 
	 * @param languageName
	 * @return language
	 */
	public Language findLanguage(String languageName) {
		logger.info("Inside Language Service >>>> findLanguage.........");
		return languageDao.findLanguage(languageName);
	}

}
