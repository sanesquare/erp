package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.ExtraPaySlipItem;
import com.hrms.web.vo.ExtraPaySlipItemVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
public interface ExtraPaySlipItemService {

	/**
	 * method to save or edit ExtraPaySlipItem
	 * 
	 * @param extraPaySlipItemVo
	 */
	public void saveOrUpdateExtraPaySlipItem(ExtraPaySlipItemVo extraPaySlipItemVo);

	/**
	 * method to delete ExtraPaySlipItem
	 * 
	 * @param extraPaySlipItemId
	 */
	public void deleteExtraPaySlipItem(Long extraPaySlipItemId);

	/**
	 * method to find ExtraPaySlipItem by id
	 * 
	 * @param extraPaySlipItemId
	 * @return ExtraPaySlipItem
	 */
	public ExtraPaySlipItem findExtraPaySlipItem(Long extraPaySlipItemId);

	/**
	 * method to find ExtraPaySlipItem by id
	 * 
	 * @param extraPaySlipItemId
	 * @return ExtraPaySlipItemVo
	 */
	public ExtraPaySlipItemVo findExtraPaySlipItemById(Long extraPaySlipItemId);

	/**
	 * method to find all ExtraPaySlipItem
	 * 
	 * @return List<ExtraPaySlipItemVo>
	 */
	public List<ExtraPaySlipItemVo> findAllExtraPaySlipItem();
	
	/**
	 * method to find ExtraPaySlipItem by title
	 * 
	 * @param title
	 * @return ExtraPaySlipItemVo
	 */
	public ExtraPaySlipItemVo findExtraPaySlipItemByTitle(String title);
	
	/**
	 * method to find ExtraPaySlipItem by title
	 * 
	 * @param title
	 * @return ExtraPaySlipItem
	 */
	public ExtraPaySlipItem findExtraPaySlipItem(String title);
}
