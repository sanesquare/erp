package com.hrms.web.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ExtraPaySlipItemDao;
import com.hrms.web.dao.SalaryDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.service.PaySlipItemService;
import com.hrms.web.service.SalaryService;
import com.hrms.web.service.TaxExcludedEmployeeService;
import com.hrms.web.service.TaxRuleService;
import com.hrms.web.util.PayRollCalculator;
import com.hrms.web.util.TaxCalculator;
import com.hrms.web.vo.AdditionalPayRollResultVo;
import com.hrms.web.vo.ExtraPaySlipItemVo;
import com.hrms.web.vo.PayRollResultVo;
import com.hrms.web.vo.PaySlipItemVo;
import com.hrms.web.vo.SalaryDailyWagesVo;
import com.hrms.web.vo.SalaryHourlyWageVo;
import com.hrms.web.vo.SalaryMonthlyVo;
import com.hrms.web.vo.SalaryPayslipItemsVo;
import com.hrms.web.vo.SalaryViewVo;
import com.hrms.web.vo.SalaryVo;

/**
 * 
 * @author Shamsheer
 * @since 21-April-2015
 */
@Service
public class SalaryServiceImpl implements SalaryService {

	@Autowired
	private SalaryDao salaryDao;

	@Autowired
	private PaySlipItemService paySlipItemService;

	@Autowired
	private ExtraPaySlipItemDao extraPayslipItemDao;

	@Autowired
	private TaxRuleService taxRuleService;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private TaxExcludedEmployeeService taxExcludedEmployeeService;

	public void saveSalaryDailyWage(SalaryVo salaryVo) {
		salaryVo.setDailyWagesVo(calculateDailyWageSalary(salaryVo.getDailyWagesVo()));
		salaryDao.saveSalaryDailyWage(salaryVo);
	}

	public void saveSalaryHourlyWage(SalaryVo salaryVo) {
		salaryVo.setHourlyWageVo(calculateHourlyWageSalary(salaryVo.getHourlyWageVo()));
		salaryDao.saveSalaryHourlyWage(salaryVo);
	}

	public void saveSalaryMonthly(SalaryVo salaryVo) {
		salaryDao.saveSalaryMonthly(salaryVo);
	}

	/**
	 * method to calculate daily wage
	 * 
	 * @param vo
	 * @return
	 */
	private SalaryDailyWagesVo calculateDailyWageSalary(SalaryDailyWagesVo vo) {
		BigDecimal salary = new BigDecimal(vo.getSalary());
		if (vo.getTax() != "") {
			BigDecimal tax = new BigDecimal(vo.getTax());
			BigDecimal dec = new BigDecimal("100.0");
			BigDecimal finalSalary = salary.subtract(salary.multiply(tax.divide(dec)));
			vo.setFinalSalary(finalSalary);
		} else {
			vo.setFinalSalary(salary);
			vo.setTax("0");
		}
		return vo;
	}

	/**
	 * method to calculate hourly wage
	 * 
	 * @param vo
	 * @return
	 */
	private SalaryHourlyWageVo calculateHourlyWageSalary(SalaryHourlyWageVo vo) {

		BigDecimal regularHour = new BigDecimal(0);
		BigDecimal overTimeHour = new BigDecimal(0);
		if (vo.getOverTimerHourSalary() != "")
			overTimeHour = new BigDecimal(vo.getOverTimerHourSalary());
		if (vo.getRegularHourSalary() != "")
			regularHour = new BigDecimal(vo.getRegularHourSalary());
		if (vo.getTax() != "") {
			BigDecimal dec = new BigDecimal("100.0");
			BigDecimal tax = new BigDecimal(vo.getTax());
			BigDecimal regularHourSalary = regularHour.subtract(regularHour.multiply(tax.divide(dec)));
			BigDecimal overTimeHourSalary = overTimeHour.subtract(overTimeHour.multiply(tax.divide(dec)));
			vo.setFinalOverTimerHourSalary(overTimeHourSalary.doubleValue());
			vo.setFinalRegularHourSalary(regularHourSalary.doubleValue());
		} else {
			vo.setFinalOverTimerHourSalary(overTimeHour.doubleValue());
			vo.setFinalRegularHourSalary(regularHour.doubleValue());
			vo.setTax("0");
		}

		return vo;
	}

	/**
	 * method to get employee's daily wage salary
	 */
	public SalaryDailyWagesVo getSalaryDailyWage(String employeeCode) {
		return salaryDao.getSalaryDailyWage(employeeCode);
	}

	/**
	 * method to get employee's hourly wage salary
	 */
	public SalaryHourlyWageVo getSalaryHourlyWage(String employeeCode) {
		return salaryDao.getSalaryHourlyWage(employeeCode);
	}

	/**
	 * method to get salary daily wage list by employee
	 */
	public List<SalaryDailyWagesVo> getSalaryDailyWageListByEmployee(String employeeCode) {
		return salaryDao.getSalaryDailyWageListByEmployee(employeeCode);
	}

	/**
	 * method to get salary hourly wage list by employee
	 */
	public List<SalaryHourlyWageVo> getSalaryHourlyWageListByEmployee(String employeeCode) {
		return salaryDao.getSalaryHourlyWageListByEmployee(employeeCode);
	}

	/**
	 * method to get salary daily wage list by employee , start date & end date
	 */
	public List<SalaryDailyWagesVo> getSalaryDailyWageListByEmployeeStartDateEndDate(Long employeeId, Date startDate,
			Date endDate) {
		return salaryDao.getSalaryDailyWageListByEmployeeStartDateEndDate(employeeId, startDate, endDate);
	}

	/**
	 * method to get salary hourly wage list by employee , start date & end date
	 */
	public List<SalaryHourlyWageVo> getSalaryHourlyWageListByEmployeeStartDateEndDate(Long employeeId, Date startDate,
			Date endDate) {
		return salaryDao.getSalaryHourlyWageListByEmployeeStartDateEndDate(employeeId, startDate, endDate);
	}

	/**
	 * method to calculate payroll
	 */
	@SuppressWarnings("static-access")
	public SalaryMonthlyVo calculatePayrollItems(BigDecimal grossSalary, String employeeCode) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(employeeCode);
		SalaryMonthlyVo salaryMonthlyVo = new SalaryMonthlyVo();
		List<PaySlipItemVo> paySlipItemVos = paySlipItemService.findAllPaySlipItem();
		PayRollCalculator calculator = new PayRollCalculator();
		Map<String, String> components = calculator.getPayRollComponents(paySlipItemVos);
		List<PayRollResultVo> resultVo = calculator.getPayRollResult(grossSalary, components,paySlipItemVos);
		if (taxExcludedEmployeeService.findTaxExcludedEmployee(employeeCode) == null) {
			BigDecimal annualTax = TaxCalculator.getTax(
					taxRuleService.findAllTaxRuleByIndividual(employee.getUserProfile().getGender()), grossSalary);
			if (annualTax != null) {
				BigDecimal monthlyTax = annualTax.divide(new BigDecimal(12), 3, RoundingMode.CEILING);
				salaryMonthlyVo.setAnnualTaxDeduction(annualTax.toString());
				salaryMonthlyVo.setMonthlyTaxDeduction(monthlyTax.toString());
				salaryMonthlyVo.setMonthlyEstimatedSalary(grossSalary.subtract(monthlyTax).toString());
				salaryMonthlyVo.setAnnualEstimatedSalary((grossSalary.multiply(new BigDecimal(12))).subtract(annualTax)
						.toString());
			} else {
				salaryMonthlyVo.setAnnualTaxDeduction("0");
				salaryMonthlyVo.setMonthlyTaxDeduction("0");
				salaryMonthlyVo.setMonthlyEstimatedSalary(grossSalary.toString());
				salaryMonthlyVo.setAnnualEstimatedSalary(grossSalary.multiply(new BigDecimal(12)).toString());
			}
		} else {
			salaryMonthlyVo.setAnnualTaxDeduction("0");
			salaryMonthlyVo.setMonthlyTaxDeduction("0");
			salaryMonthlyVo.setMonthlyEstimatedSalary(grossSalary.toString());
			salaryMonthlyVo.setAnnualEstimatedSalary(grossSalary.multiply(new BigDecimal(12)).toString());
		}
		salaryMonthlyVo.setPayRollResultVos(resultVo);
		return salaryMonthlyVo;
	}

	/**
	 * method to calculate extra payslip items
	 */
	public String calculateAdditionalPayrollItems(BigDecimal grossSalary, String title) {
		ExtraPaySlipItemVo extraPaySlipItemVo = extraPayslipItemDao.findExtraPaySlipItemByTitle(title);
		PayRollCalculator calculator = new PayRollCalculator();
		List<ExtraPaySlipItemVo> extraPaySlipItemVos = new ArrayList<ExtraPaySlipItemVo>();
		extraPaySlipItemVos.add(extraPaySlipItemVo);
		Map<String, String> additionalSyntax = PayRollCalculator.getAdditionalPayRollComponents(extraPaySlipItemVos);
		List<PaySlipItemVo> paySlipItemVos = paySlipItemService.findAllPaySlipItem();
		Map<String, String> syntax = calculator.getPayRollComponents(paySlipItemVos);
		List<AdditionalPayRollResultVo> additionalPayRollResultVos = PayRollCalculator.getAdditionalPayRollResult(
				grossSalary, syntax, additionalSyntax);
		String amount = null;
		for (AdditionalPayRollResultVo additionalPayRollResultVo : additionalPayRollResultVos) {
			amount = additionalPayRollResultVo.getAmount().toString();
		}
		return amount;
	}

	public List<SalaryMonthlyVo> getSalariesByEmployee(Long employeeId) {
		return salaryDao.getSalariesByEmployee(employeeId);
	}

	public SalaryMonthlyVo getMonthlySalaryByEmployee(String employeeCode) {
		return salaryDao.getMonthlySalaryByEmployee(employeeCode);
	}

	public List<SalaryMonthlyVo> getMonthlySalariesByEmployeeStartDateEndDate(Long employeeId, Date startDate,
			Date endDate) {
		return salaryDao.getMonthlySalariesByEmployeeStartDateEndDate(employeeId, startDate, endDate);
	}

	public SalaryHourlyWageVo getHourlyWageForDay(String employeeCode, Date date) {
		return salaryDao.getHourlyWageForDay(employeeCode, date);
	}

	public SalaryDailyWagesVo getDailyWageForDay(String employeeCode, Date date) {
		return salaryDao.getDailyWageForDay(employeeCode, date);
	}

	public SalaryMonthlyVo findSalaryForDate(String date, String employeeCode) {
		return salaryDao.findSalaryForDate(date, employeeCode);
	}

	public List<SalaryViewVo> listAllSalaries() {
		return salaryDao.listAllSalaries();
	}

	public void deleteSalary(Long id, String type) {
		salaryDao.deleteSalary(id, type);
	}

}
