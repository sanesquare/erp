package com.hrms.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EsiSyntaxDao;
import com.hrms.web.entities.EsiSyntax;
import com.hrms.web.service.EsiSyntaxService;
import com.hrms.web.vo.EsiSyntaxVo;

/**
 * 
 * @author Jithin Mohan
 * @since 5-May-2015
 *
 */
@Service
public class EsiSyntaxServiceImpl implements EsiSyntaxService {

	@Autowired
	private EsiSyntaxDao esiSyntaxDao;

	public void saveOrUpdateEsiSyntax(EsiSyntaxVo esiSyntaxVo) {
		esiSyntaxDao.saveOrUpdateEsiSyntax(esiSyntaxVo);
	}

	public void deleteEsiSyntax(Long esiSyntaxId) {
		esiSyntaxDao.deleteEsiSyntax(esiSyntaxId);
	}

	public EsiSyntax findEsiSyntax(Long esiSyntaxId) {
		return esiSyntaxDao.findEsiSyntax(esiSyntaxId);
	}

	public EsiSyntaxVo findEsiSyntaxId(Long esiSyntaxId) {
		return esiSyntaxDao.findEsiSyntaxId(esiSyntaxId);
	}

	public EsiSyntaxVo findEsiSyntax() {
		return esiSyntaxDao.findEsiSyntax();
	}

}
