package com.hrms.web.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.util.BiometricServiceUtil;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AddressJsonVo;
import com.hrms.web.vo.AttendanceEmployeeVo;
import com.hrms.web.vo.EmployeeAdditionalInformationVo;
import com.hrms.web.vo.EmployeeBankAccountVo;
import com.hrms.web.vo.EmployeeEmergencyJsonVo;
import com.hrms.web.vo.EmployeeExtraInfoJsonVo;
import com.hrms.web.vo.EmployeeLanguageVo;
import com.hrms.web.vo.EmployeeLeaveVo;
import com.hrms.web.vo.EmployeeOrganisationAssetsVo;
import com.hrms.web.vo.EmployeePersonalDetailsJsonVo;
import com.hrms.web.vo.EmployeePhoneNumbersVo;
import com.hrms.web.vo.EmployeeQualificationVo;
import com.hrms.web.vo.EmployeeSkillsVo;
import com.hrms.web.vo.EmployeeSuperiorsSubordinatesVo;
import com.hrms.web.vo.EmployeeUserJsonVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.RolesVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.SuperiorSubordinateVo;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Log
	private static Logger logger;

	@Autowired
	private EmployeeDao employeeDao;

	public List<EmployeeVo> listAllEmployee() {
		logger.info("inside>> EmployeeService.. listAllEmployee");
		return employeeDao.listAllEmployee();
	}

	public Long saveEmployee(EmployeeVo employeeVo) {
		return employeeDao.saveEmployee(employeeVo);
	}

	public void updateEmployee(EmployeeVo employeeVo) {
		employeeDao.updateEmployee(employeeVo);
	}

	public void deleteEmployee(Long id) {
		employeeDao.deleteEmployee(id);
	}

	public Employee findAndReturnEmployeeById(Long id) {
		return employeeDao.findAndReturnEmployeeById(id);
	}

	public Employee findAndReturnEmployeeByCode(String code) {
		return employeeDao.findAndReturnEmployeeByCode(code);
	}

	public EmployeeVo findEmployeeById(Long id) {
		return employeeDao.findEmployeeById(id);
	}

	public EmployeeVo findEmployeeByCode(String code) {
		return employeeDao.findEmployeeByCode(code);
	}

	public void updateDocuments(EmployeeVo employeeVo) {
		employeeDao.updateDocuments(employeeVo);
	}

	public void saveEmployeeUserInfo(EmployeeUserJsonVo employeeVo) {
		employeeDao.saveEmployeeUserInfo(employeeVo);
	}

	public void saveEmployeeUserProfileInfo(EmployeePersonalDetailsJsonVo employeeVo) {
		employeeDao.saveEmployeeUserProfileInfo(employeeVo);
	}

	public void saveEmployeeAdditionalInfo(EmployeeExtraInfoJsonVo employeeVo) {
		employeeDao.saveEmployeeAdditionalInfo(employeeVo);
	}

	public void saveAddress(AddressJsonVo employeeVo) {
		employeeDao.saveAddress(employeeVo);
	}

	public void savePhone(EmployeePhoneNumbersVo employeeVo) {
		employeeDao.savePhone(employeeVo);
	}

	public void saveNotes(EmployeeExtraInfoJsonVo employeeVo) {
		employeeDao.saveNotes(employeeVo);
	}

	public void saveEmergencyContact(EmployeeEmergencyJsonVo employeeVo) {
		employeeDao.saveEmergencyContact(employeeVo);
	}

	public void saveEmployeeQualification(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeQualification(additionalInformationVo);
	}

	public void saveEmployeeLanguage(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeLanguage(additionalInformationVo);
	}

	public void saveEmployeeExperience(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeExperience(additionalInformationVo);
	}

	public void saveEmployeeBankAccount(EmployeeBankAccountVo additionalInformationVo) {
		employeeDao.saveEmployeeBankAccount(additionalInformationVo);
	}

	public void saveEmployeeBenefitsDetails(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeBenefitsDetails(additionalInformationVo);
	}

	public void saveEmployeeUniformDetails(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeUniformDetails(additionalInformationVo);
	}

	public void saveEmployeeOrganizationAssets(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeOrganizationAssets(additionalInformationVo);
	}

	public void saveEmployeeSkillsInfo(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeSkillsInfo(additionalInformationVo);
	}

	public void saveEmployeeReferenceDetails(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeReferenceDetails(additionalInformationVo);
	}

	public void saveEmployeeDependants(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeDependants(additionalInformationVo);
	}

	public void saveEmployeeNextOfKinDetails(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeNextOfKinDetails(additionalInformationVo);
	}

	public void saveEmployeeStatus(EmployeeAdditionalInformationVo additionalInformationVo) {
		employeeDao.saveEmployeeStatus(additionalInformationVo);
	}

	public List<EmployeeVo> listEmployeesDeetails() {
		return employeeDao.listEmployeesDeetails();
	}

	public EmployeeVo getEmployeesDetails(Long id) {
		return employeeDao.getEmployeesDetails(id);
	}

	public List<EmployeeVo> getEmployeesByBranch(Long branchId) {
		return employeeDao.getEmployeesByBranch(branchId);
	}

	public List<EmployeeVo> getEmployeesByDepartment(Long departmentId) {
		return employeeDao.getEmployeesByDepartment(departmentId);
	}

	public List<EmployeeVo> getEmployeesByBranchAndDepartment(Long branchId, Long departmentId) {
		return employeeDao.getEmployeesByBranchAndDepartment(branchId, departmentId);
	}

	public void updateNotifyByEmail(EmployeeExtraInfoJsonVo employeeVo) {
		employeeDao.updateNotifyByEmail(employeeVo);
	}

	public void saveEmployeeSubordinateSuperior(EmployeeSuperiorsSubordinatesVo employeeSuperiorsSubordinatesVo) {
		employeeDao.saveEmployeeSubordinateSuperior(employeeSuperiorsSubordinatesVo);
	}

	public EmployeeVo findDetailedEmployee(Long id) {
		return employeeDao.findDetailedEmployee(id);
	}

	public void deleteDocument(String url) {
		employeeDao.deleteDocument(url);
	}

	public Long saveEmployeeLeave(EmployeeLeaveVo leaveVo) {
		return employeeDao.saveEmployeeLeave(leaveVo);
	}

	public EmployeeLeaveVo getLeaveByEmployeeAndTitle(Long employeeId, Long typeId) {
		return employeeDao.getLeaveByEmployeeAndTitle(employeeId, typeId);
	}

	public EmployeeQualificationVo getQualificationByEmployeeAndDegree(Long employeeId, Long degree) {
		return employeeDao.getQualificationByEmployeeAndDegree(employeeId, degree);
	}

	public EmployeeLanguageVo getLanguagesByEmployeeAndLanguage(Long employeeId, Long languageId) {
		return employeeDao.getLanguagesByEmployeeAndLanguage(employeeId, languageId);
	}

	public EmployeeOrganisationAssetsVo getAssetsByEmployeeAndAsset(Long employeeId, Long assetId) {
		return employeeDao.getAssetsByEmployeeAndAsset(employeeId, assetId);
	}

	public EmployeeSkillsVo getSkillByEmployeeAndSkill(Long employeeId, Long skillId) {
		return employeeDao.getSkillByEmployeeAndSkill(employeeId, skillId);
	}

	public SuperiorSubordinateVo listSuperiorSubordinates(Long employeeId) {
		return employeeDao.listSuperiorSubordinates(employeeId);
	}

	public SuperiorSubordinateVo getSuperiorSubordinateByDesignation(Long designationId) {
		return employeeDao.getSuperiorSubordinateByDesignation(designationId);
	}

	public RolesVo getRoleTypes() {
		return employeeDao.getRoleTypes();
	}

	public void saveRoles(RolesVo rolesVo) {
		employeeDao.saveRoles(rolesVo);
	}

	public EmployeeVo getEmployeeByCode(String code) {
		return employeeDao.getEmployeeByCode(code);
	}

	public RolesVo findRolesByEmployee(Long id) {
		return employeeDao.findRolesByEmployee(id);
	}

	public List<EmployeeVo> getEmployeesByDepartments(List<Long> departmentIds) {
		return employeeDao.getEmployeesByDepartments(departmentIds);
	}

	public List<EmployeeVo> getEmployeesByBranches(List<Long> branchIds) {
		return employeeDao.getEmployeesByBranches(branchIds);
	}

	public List<EmployeeVo> getEmployeesByBranchesAndDepartments(List<Long> branchIds, List<Long> departmentIds) {
		return employeeDao.getEmployeesByBranchesAndDepartments(branchIds, departmentIds);
	}

	public void updateEmployeeAsAdmin(Long employeeId, Boolean isAdmin, List<Long> branchIds) {
		employeeDao.updateEmployeeAsAdmin(employeeId, isAdmin, branchIds);
	}

	public List<EmployeeVo> listAllAdmins() {
		return employeeDao.listAllAdmins();
	}

	public List<EmployeeVo> listEmployeeCompleteDetails() {
		return employeeDao.listEmployeeCompleteDetails();
	}

	public EmployeeBankAccountVo getBankAccountByEmployeeAndType(Long employeeId, Long typeId) {
		return employeeDao.getBankAccountByEmployeeAndType(employeeId, typeId);
	}

	public List<EmployeeVo> listEmployeesWithPayroll() {
		return employeeDao.listEmployeesWithPayroll();
	}

	public List<EmployeeVo> listEmployeesWithPayroll(Long branchId, Long departmentId) {
		return employeeDao.listEmployeesWithPayroll(branchId, departmentId);
	}

	public List<EmployeeVo> listEmployeesByEmployeeType(Long employeeTypeId) {
		return employeeDao.listEmployeesByEmployeeType(employeeTypeId);
	}

	public List<EmployeeVo> listEmployeesByEmployeeCategory(Long employeeCategoryId) {
		return employeeDao.listEmployeesByEmployeeCategory(employeeCategoryId);
	}

	public List<EmployeeVo> listEmployeesByEmployeeCategoryAndType(Long employeeCategoryId, Long employeeTypeId) {
		return employeeDao.listEmployeesByEmployeeCategoryAndType(employeeCategoryId, employeeTypeId);
	}

	public List<EmployeeVo> listEmployeesByBranchEmployeeCategoryAndType(Long branchId, Long employeeCategoryId,
			Long employeeTypeId) {
		return employeeDao.listEmployeesByBranchEmployeeCategoryAndType(branchId, employeeCategoryId, employeeTypeId);
	}

	public List<EmployeeVo> listEmployeesByDepartmentEmployeeCategoryAndType(Long departmentId, Long employeeCategoryId,
			Long employeeTypeId) {
		return employeeDao.listEmployeesByDepartmentEmployeeCategoryAndType(departmentId, employeeCategoryId,
				employeeTypeId);
	}

	public List<EmployeeVo> listEmployeesByBranchDepartmentEmployeeCategoryAndType(Long branchId, Long departmentId,
			Long employeeCategoryId, Long employeeTypeId) {
		return employeeDao.listEmployeesByBranchDepartmentEmployeeCategoryAndType(branchId, departmentId,
				employeeCategoryId, employeeTypeId);
	}

	public SessionRolesVo getEmployeeRoles(Long employeeId) {
		return employeeDao.getEmployeeRoles(employeeId);
	}

	public void leaveCarryOver() {
		employeeDao.leaveCarryOver();
	}

	public List<EmployeeVo> findAllEmployeesDeetailsByCode(String employeeCode) {
		return employeeDao.findAllEmployeesDeetailsByCode(employeeCode);
	}

	public List<EmployeeVo> findInactiveEmployees() {
		return employeeDao.findInactiveEmployees();
	}

	public EmployeeVo getEmployeesDetails(String employeeCode) {
		return employeeDao.getEmployeesDetails(employeeCode);
	}

	public List<EmployeeVo> findPayrollOptionEmployees() {
		return employeeDao.findPayrollOptionEmployees();
	}

	public String generateEmployeeCode() {
		return employeeDao.generateEmployeeCode();
	}

	public String writeEmployeesToExcel(HttpServletResponse response, Long branchId) {
		List<EmployeeVo> employeeVos = employeeDao.getEmployeesByBranch(branchId);
		if (employeeVos.size() <= 0)
			return null;
		else {
			BiometricServiceUtil.writeExcel("employee_" + DateFormatter.getUniqueTimestamp() + ".xls", employeeVos,
					response);
			return "success";
		}
	}

	public void updateEmployeeStatus(Long id) {
		employeeDao.updateEmployeeStatus(id);
	}

	public List<EmployeeVo> getEmployeesByBranchAndDepartments(Long branchId, List<Long> departmentIds) {
		return employeeDao.getEmployeesByBranchAndDepartments(branchId, departmentIds);
	}

	public List<EmployeeVo> getDetailedEmployeesByBranchAndChildBranches(Long branchId) {
		return employeeDao.getDetailedEmployeesByBranchAndChildBranches(branchId);
	}

	public List<EmployeeVo> listAllAutoApproveEmployees() {
		return employeeDao.listAllAutoApproveEmployees();
	}

	public void updateEmployeeWithAutoApprove(Long employeeId, Boolean isAutoApprove) {
		employeeDao.updateEmployeeWithAutoApprove(employeeId, isAutoApprove);
	}
	
	public List<AttendanceEmployeeVo> findEmployeesForAttendanceStatus(Long branchId,List<Long> departmentIds){
		return employeeDao.findEmployeesForAttendanceStatus(branchId, departmentIds);
	}
}
