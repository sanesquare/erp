package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.JobTypeDao;
import com.hrms.web.entities.JobType;
import com.hrms.web.service.JobTypeService;
import com.hrms.web.vo.JobTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Service
public class JobTypeServiceImpl implements JobTypeService {

	@Autowired
	private JobTypeDao jobTypeDao;

	/**
	 * method to save new job type
	 * 
	 * @param jobTypeVo
	 */
	public void saveJobType(JobTypeVo jobTypeVo) {
		jobTypeDao.saveJobType(jobTypeVo);
	}

	/**
	 * method to update job type
	 * 
	 * @param jobTypeVo
	 */
	public void updateJobType(JobTypeVo jobTypeVo) {
		jobTypeDao.updateJobType(jobTypeVo);
	}

	/**
	 * method to delete job type
	 * 
	 * @param typeId
	 */
	public void deleteJobType(Long typeId) {
		jobTypeDao.deleteJobType(typeId);
	}

	/**
	 * method to find job type by id
	 * 
	 * @param typeId
	 * @return jobType
	 */
	public JobType findJobType(Long typeId) {
		return jobTypeDao.findJobType(typeId);
	}

	/**
	 * method to find job type by id
	 * 
	 * @param typeId
	 * @return jobTypeVo
	 */
	public JobTypeVo findJobTypeById(Long typeId) {
		return jobTypeDao.findJobTypeById(typeId);
	}

	/**
	 * method to find job type by name
	 * 
	 * @param typeName
	 * @return jobType
	 */
	public JobType findJobType(String typeName) {
		return jobTypeDao.findJobType(typeName);
	}

	/**
	 * method to find job type by name
	 * 
	 * @param typeName
	 * @return jobType
	 */
	public JobTypeVo findJobTypeByType(String typeName) {
		return jobTypeDao.findJobTypeByType(typeName);
	}

	/**
	 * method to find all job types
	 * 
	 * @return List<JobTypeVo>
	 */
	public List<JobTypeVo> findAllJobType() {
		return jobTypeDao.findAllJobType();
	}

}
