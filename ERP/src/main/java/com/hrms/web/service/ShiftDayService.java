package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.ShiftDay;
import com.hrms.web.vo.ShiftDayVo;

/**
 * 
 * @author Jithin Mohan
 * @since 31-March-2015
 *
 */
public interface ShiftDayService {

	/**
	 * method to save ShiftDay
	 * 
	 * @param shiftDayVo
	 */
	public void saveShiftDay(ShiftDayVo shiftDayVo);

	/**
	 * method to update ShiftDay
	 * 
	 * @param shiftDayVo
	 */
	public void updateShiftDay(ShiftDayVo shiftDayVo);

	/**
	 * method to delete ShiftDay
	 * 
	 * @param shiftDayId
	 */
	public void deleteShiftDay(Long shiftDayId);

	/**
	 * method to find ShiftDay by id
	 * 
	 * @param shiftDayId
	 * @return ShiftDay
	 */
	public ShiftDay findShiftDay(Long shiftDayId);

	/**
	 * method to find ShiftDay by day
	 * 
	 * @param shiftDay
	 * @return ShiftDay
	 */
	public ShiftDay findShiftDay(String shiftDay);

	/**
	 * method to find ShiftDay by id
	 * 
	 * @param shiftDayId
	 * @return ShiftDayVo
	 */
	public ShiftDayVo findShiftDayById(Long shiftDayId);

	/**
	 * method to find ShiftDay by day
	 * 
	 * @param shiftDay
	 * @return ShiftDayVo
	 */
	public ShiftDayVo findShiftDayByDay(String shiftDay);

	/**
	 * method to find all ShiftDay
	 * 
	 * @return List<ShiftDayVo>
	 */
	public List<ShiftDayVo> findAllShiftDay();
}
