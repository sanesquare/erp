package com.hrms.web.service;

import java.math.BigDecimal;

import com.hrms.web.vo.SalaryCalculatorVo;

/**
 * 
 * @author Shamsheer
 * @since 05-May-2015
 */
public interface SalaryCalculatorService {

	/**
	 * method to calculate salary
	 * @param grossSalary
	 * @return
	 */
	public SalaryCalculatorVo calculateSalary(BigDecimal grossSalary);
}
