package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TravelDao;
import com.hrms.web.service.TravelService;
import com.hrms.web.vo.TravelVo;
/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
@Service
public class TravelServiceImpl implements TravelService{

	@Autowired
	private TravelDao dao;

	public Long saveTravel(TravelVo travelVo) {
		return dao.saveTravel(travelVo);
	}

	public void updateTravel(TravelVo travelVo) {
		dao.updateTravel(travelVo);
	}

	public void deleteTravel(Long travelId) {
		dao.deleteTravel(travelId);
	}

	public TravelVo getTravelById(Long travelId) {
		return dao.getTravelById(travelId);
	}

	public List<TravelVo> listAllTravels() {
		return dao.listAllTravels();
	}

	public List<TravelVo> listAllTravelsByEmployee(Long employeeId) {
		return dao.listAllTravelsByEmployee(employeeId);
	}

	public void updateDocuments(TravelVo travelVo) {
		dao.updateDocuments(travelVo);
	}

	public void deleteDocument(String url) {
		dao.deleteDocument(url);
	}

	public void updateStatus(TravelVo travelVo) {
		dao.updateStatus(travelVo);
	}

	public List<TravelVo> findTravelsByEmployeeBetweenDates(Long employeeId, String startDate, String endDate) {
		return dao.findTravelsByEmployeeBetweenDates(employeeId, startDate, endDate);
	}

	public List<TravelVo> findAllTravelsByEmployee(String employeeCode) {
		return dao.findAllTravelsByEmployee(employeeCode);
	}
}
