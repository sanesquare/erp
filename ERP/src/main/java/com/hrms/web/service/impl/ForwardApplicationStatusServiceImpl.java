package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ForwardApplicationStatusDao;
import com.hrms.web.entities.ForwardApplicationStatus;
import com.hrms.web.service.ForwardApplicationStatusService;
import com.hrms.web.vo.ForwardApplicationStatusVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
@Service
public class ForwardApplicationStatusServiceImpl implements ForwardApplicationStatusService{

	@Autowired
	private ForwardApplicationStatusDao dao;

	public void saveStatus(ForwardApplicationStatusVo statusVo) {
		dao.saveStatus(statusVo);
	}

	public void updateStatus(ForwardApplicationStatusVo statusVo) {
		dao.updateStatus(statusVo);
	}

	public void deleteStatus(Long id) {
		dao.deleteStatus(id);
	}

	public List<ForwardApplicationStatusVo> listAllStatus() {
		return dao.listAllStatus();
	}

	public ForwardApplicationStatus getStatus(String status) {
		return dao.getStatus(status);
	}
}
