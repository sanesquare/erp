package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.HolidayDao;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Holiday;
import com.hrms.web.service.HolidayService;
import com.hrms.web.vo.HolidayVo;

/**
 * 
 * @author Jithin Mohan
 * @since 29-March-2015
 *
 */
@Service
public class HolidayServiceImpl implements HolidayService {

	@Autowired
	private HolidayDao holidayDao;

	public void saveHoliday(HolidayVo holidayVo) {
		holidayDao.saveHoliday(holidayVo);
	}

	public void updateHoliday(HolidayVo holidayVo) {
		holidayDao.updateHoliday(holidayVo);
	}

	public void deleteHoliday(Long holidayId) {
		holidayDao.deleteHoliday(holidayId);
	}

	public Holiday findHoliday(Long holidayId) {
		return holidayDao.findHoliday(holidayId);
	}

	public HolidayVo findHolidayById(Long holidayId) {
		return holidayDao.findHolidayById(holidayId);
	}

	public List<HolidayVo> findHolidayByBranch(Branch branch) {
		return holidayDao.findHolidayByBranch(branch);
	}

	public List<HolidayVo> findHolidayByStartDate(Date startDate) {
		return holidayDao.findHolidayByStartDate(startDate);
	}

	public List<HolidayVo> findHolidayByEndDate(Date endDate) {
		return holidayDao.findHolidayByEndDate(endDate);
	}

	public List<HolidayVo> findHolidayBetweenDate(Date startDate, Date endDate) {
		return holidayDao.findHolidayBetweenDate(startDate, endDate);
	}

	public List<HolidayVo> findAllHoliday() {
		return holidayDao.findAllHoliday();
	}

}
