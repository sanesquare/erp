package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ExtraPaySlipItemDao;
import com.hrms.web.entities.ExtraPaySlipItem;
import com.hrms.web.service.ExtraPaySlipItemService;
import com.hrms.web.vo.ExtraPaySlipItemVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Service
public class ExtraPaySlipItemServiceImpl implements ExtraPaySlipItemService {

	@Autowired
	private ExtraPaySlipItemDao extraPaySlipItemDao;

	public void saveOrUpdateExtraPaySlipItem(ExtraPaySlipItemVo extraPaySlipItemVo) {
		extraPaySlipItemDao.saveOrUpdateExtraPaySlipItem(extraPaySlipItemVo);
	}

	public void deleteExtraPaySlipItem(Long extraPaySlipItemId) {
		extraPaySlipItemDao.deleteExtraPaySlipItem(extraPaySlipItemId);
	}

	public ExtraPaySlipItem findExtraPaySlipItem(Long extraPaySlipItemId) {
		return extraPaySlipItemDao.findExtraPaySlipItem(extraPaySlipItemId);
	}

	public ExtraPaySlipItemVo findExtraPaySlipItemById(Long extraPaySlipItemId) {
		return extraPaySlipItemDao.findExtraPaySlipItemById(extraPaySlipItemId);
	}

	public List<ExtraPaySlipItemVo> findAllExtraPaySlipItem() {
		return extraPaySlipItemDao.findAllExtraPaySlipItem();
	}

	public ExtraPaySlipItemVo findExtraPaySlipItemByTitle(String title) {
		return extraPaySlipItemDao.findExtraPaySlipItemByTitle(title);
	}

	public ExtraPaySlipItem findExtraPaySlipItem(String title) {
		return extraPaySlipItemDao.findExtraPaySlipItem(title);
	}

}
