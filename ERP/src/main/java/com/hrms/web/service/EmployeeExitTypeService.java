package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.EmployeeExitType;
import com.hrms.web.vo.EmployeeExitTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 26-March-2015
 *
 */
public interface EmployeeExitTypeService {

	/**
	 * method to save EmployeeExitType
	 * 
	 * @param employeeExitTypeVo
	 */
	public void saveEmployeeExitType(EmployeeExitTypeVo employeeExitTypeVo);

	/**
	 * method to update EmployeeExitType
	 * 
	 * @param employeeExitTypeVo
	 */
	public void updateEmployeeExitType(EmployeeExitTypeVo employeeExitTypeVo);

	/**
	 * method to delete EmployeeExitType
	 * 
	 * @param exitTypeId
	 */
	public void deleteEmployeeExitType(Long exitTypeId);

	/**
	 * method to find EmployeeExitType by id
	 * 
	 * @param exitTypeId
	 * @return EmployeeExitType
	 */
	public EmployeeExitType findEmployeeExitType(Long exitTypeId);

	/**
	 * method to find EmployeeExitType by type
	 * 
	 * @param exitType
	 * @return EmployeeExitType
	 */
	public EmployeeExitType findEmployeeExitType(String exitType);

	/**
	 * method to find EmployeeExitType by id
	 * 
	 * @param exitTypeId
	 * @return EmployeeExitTypeVo
	 */
	public EmployeeExitTypeVo findEmployeeExitTypeById(Long exitTypeId);

	/**
	 * method to find EmployeeExitType by type
	 * 
	 * @param exitType
	 * @return EmployeeExitTypeVo
	 */
	public EmployeeExitTypeVo findEmployeeExitTypeByType(String exitType);

	/**
	 * method to find all EmployeeExitType
	 * 
	 * @return List<EmployeeExitTypeVo>
	 */
	public List<EmployeeExitTypeVo> findAllEmployeeExitType();
}
