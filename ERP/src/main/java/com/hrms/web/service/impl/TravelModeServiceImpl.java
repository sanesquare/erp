package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TravelModeDao;
import com.hrms.web.service.TravelModeService;
import com.hrms.web.vo.TravelModeVo;

/**
 * 
 * @author Shamsheer
 * @since 23-April-2015
 */
@Service
public class TravelModeServiceImpl implements TravelModeService{

	@Autowired
	private TravelModeDao dao;

	public void saveTravelMode(TravelModeVo vo) {
		dao.saveTravelMode(vo);
	}

	public void updateTravelMode(TravelModeVo vo) {
		dao.updateTravelMode(vo);
	}

	public void deleteTravelMode(Long id) {
		dao.deleteTravelMode(id);
	}

	public TravelModeVo getTravelModeById(Long id) {
		return dao.getTravelModeById(id);
	}

	public List<TravelModeVo> listAllTravelModes() {
		return dao.listAllTravelModes();
	}
}
