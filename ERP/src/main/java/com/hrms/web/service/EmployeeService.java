package com.hrms.web.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.hrms.web.entities.Employee;
import com.hrms.web.vo.AddressJsonVo;
import com.hrms.web.vo.AttendanceEmployeeVo;
import com.hrms.web.vo.EmployeeAdditionalInformationVo;
import com.hrms.web.vo.EmployeeBankAccountVo;
import com.hrms.web.vo.EmployeeEmergencyJsonVo;
import com.hrms.web.vo.EmployeeExtraInfoJsonVo;
import com.hrms.web.vo.EmployeeLanguageVo;
import com.hrms.web.vo.EmployeeLeaveVo;
import com.hrms.web.vo.EmployeeOrganisationAssetsVo;
import com.hrms.web.vo.EmployeePersonalDetailsJsonVo;
import com.hrms.web.vo.EmployeePhoneNumbersVo;
import com.hrms.web.vo.EmployeeQualificationVo;
import com.hrms.web.vo.EmployeeSkillsVo;
import com.hrms.web.vo.EmployeeSuperiorsSubordinatesVo;
import com.hrms.web.vo.EmployeeUserJsonVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.RolesVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.SuperiorSubordinateVo;

/**
 * 
 * @author Sangeeth and Bibin
 * @modified Shamsheer
 */

public interface EmployeeService {

	/**
	 * method to list all employees
	 * 
	 * @return List<Employee>
	 */
	public List<EmployeeVo> listAllEmployee();

	public void updateEmployeeStatus(Long id);

	/**
	 * Method to save employee
	 * 
	 * @param employeeVo
	 * @return id
	 */
	public Long saveEmployee(EmployeeVo employeeVo);

	/**
	 * method to update employee as admin
	 * 
	 * @param employeeId
	 * @param isAdmin
	 */
	public void updateEmployeeAsAdmin(Long employeeId, Boolean isAdmin, List<Long> branchIds);

	/**
	 * method to get employee by code
	 * 
	 * @param code
	 * @return
	 */
	public EmployeeVo getEmployeeByCode(String code);

	/**
	 * method to save phone number
	 * 
	 * @param employeeVo
	 */
	public void savePhone(EmployeePhoneNumbersVo employeeVo);

	/**
	 * method to get superiors subordinate by designation
	 * 
	 * @param designationId
	 * @return
	 */
	public SuperiorSubordinateVo getSuperiorSubordinateByDesignation(Long designationId);

	/**
	 * method to delete document
	 * 
	 * @param url
	 */
	public void deleteDocument(String url);

	/**
	 * method to list superiors and subordinates
	 * 
	 * @param employeeId
	 * @return
	 */
	public SuperiorSubordinateVo listSuperiorSubordinates(Long employeeId);

	/**
	 * method to save emergency contact
	 * 
	 * @param employeeVo
	 */
	public void saveEmergencyContact(EmployeeEmergencyJsonVo employeeVo);

	/**
	 * method to save notify by email
	 * 
	 * @param employeeVo
	 */
	public void updateNotifyByEmail(EmployeeExtraInfoJsonVo employeeVo);

	/**
	 * method to save notes
	 * 
	 * @param employeeVo
	 */
	public void saveNotes(EmployeeExtraInfoJsonVo employeeVo);

	/**
	 * method to save employee personal info
	 * 
	 * @param employeeVo
	 */
	public void saveEmployeeUserProfileInfo(EmployeePersonalDetailsJsonVo employeeVo);

	/**
	 * method to save employee's address
	 * 
	 * @param employeeVo
	 */
	public void saveAddress(AddressJsonVo employeeVo);

	/**
	 * method to save employee's additional info
	 * 
	 * @param employeeVo
	 */
	public void saveEmployeeAdditionalInfo(EmployeeExtraInfoJsonVo employeeVo);

	/**
	 * method to save employee user info
	 * 
	 * @param employeeVo
	 */
	public void saveEmployeeUserInfo(EmployeeUserJsonVo employeeVo);

	/**
	 * method to update employee
	 * 
	 * @param employeeVo
	 */
	public void updateEmployee(EmployeeVo employeeVo);

	/**
	 * method to delete employee
	 * 
	 * @param id
	 */
	public void deleteEmployee(Long id);

	/**
	 * method to find and return employee by id
	 * 
	 * @param id
	 * @return
	 */
	public Employee findAndReturnEmployeeById(Long id);

	/**
	 * method to find and return employee by code
	 * 
	 * @param id
	 * @return
	 */
	public Employee findAndReturnEmployeeByCode(String code);

	/**
	 * method to find employee by id
	 * 
	 * @param id
	 * @return
	 */
	public EmployeeVo findEmployeeById(Long id);

	/**
	 * method to find employee by code
	 * 
	 * @param id
	 * @return
	 */
	public EmployeeVo findEmployeeByCode(String code);

	/**
	 * method to update documents
	 * 
	 * @param employeeVo
	 */
	public void updateDocuments(EmployeeVo employeeVo);

	/**
	 * method to save employee additional information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeQualification(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to save employee language information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeLanguage(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to save employee experience information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeExperience(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to save employee bank account information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeBankAccount(EmployeeBankAccountVo additionalInformationVo);

	/**
	 * method to save employee benefits information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeBenefitsDetails(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to save employee uniform information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeUniformDetails(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to save employee organization assets information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeOrganizationAssets(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to save employee skills information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeSkillsInfo(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to save employee reference information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeReferenceDetails(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to save employee dependants information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeDependants(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to save employee next of kin information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeNextOfKinDetails(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to save employee status information
	 * 
	 * @param additionalInformationVo
	 */
	public void saveEmployeeStatus(EmployeeAdditionalInformationVo additionalInformationVo);

	/**
	 * method to list employees details
	 * 
	 * @return
	 */
	public List<EmployeeVo> listEmployeesDeetails();

	/**
	 * method to list employees complete details
	 * 
	 * @return
	 */
	public List<EmployeeVo> listEmployeeCompleteDetails();

	/**
	 * method to get employees details
	 */
	public EmployeeVo getEmployeesDetails(Long id);

	/**
	 * list employees by branch
	 * 
	 * @param branchId
	 * @return
	 */
	public List<EmployeeVo> getEmployeesByBranch(Long branchId);

	/**
	 * list employees by department
	 * 
	 * @param branchId
	 * @return
	 */
	public List<EmployeeVo> getEmployeesByDepartment(Long departmentId);

	/**
	 * method to list employees by branch and department
	 * 
	 * @param branchId
	 * @param departmentId
	 * @return
	 */
	public List<EmployeeVo> getEmployeesByBranchAndDepartment(Long branchId, Long departmentId);

	/**
	 * method to return employees by departments
	 * 
	 * @param departmentIds
	 * @return
	 */
	public List<EmployeeVo> getEmployeesByDepartments(List<Long> departmentIds);

	/**
	 * method to return employees by branches
	 * 
	 * @param branchIds
	 * @return
	 */
	public List<EmployeeVo> getEmployeesByBranches(List<Long> branchIds);

	/**
	 * method to return employees by branches
	 * 
	 * @param branchIds
	 * @return
	 */
	public List<EmployeeVo> getDetailedEmployeesByBranchAndChildBranches(Long branchIds);

	/**
	 * method to return employees by branches and departments
	 * 
	 * @param branchIds
	 * @param departmentIds
	 * @return
	 */
	public List<EmployeeVo> getEmployeesByBranchesAndDepartments(List<Long> branchIds, List<Long> departmentIds);

	/**
	 * method to list all admins
	 * 
	 * @return
	 */
	public List<EmployeeVo> listAllAdmins();

	/**
	 * method to save employee's superiors and subordinates
	 * 
	 * @param employeeSuperiorsSubordinatesVo
	 */
	public void saveEmployeeSubordinateSuperior(EmployeeSuperiorsSubordinatesVo employeeSuperiorsSubordinatesVo);

	/**
	 * method to return detailed employee
	 * 
	 * @param id
	 * @return
	 */
	public EmployeeVo findDetailedEmployee(Long id);

	/**
	 * method to save employee leave
	 * 
	 * @param leaveVo
	 * @return
	 */
	public Long saveEmployeeLeave(EmployeeLeaveVo leaveVo);

	/**
	 * method to get leave
	 * 
	 * @param employeeId
	 * @param typeId
	 * @return
	 */
	public EmployeeLeaveVo getLeaveByEmployeeAndTitle(Long employeeId, Long typeId);

	/**
	 * method to get qualification
	 * 
	 * @param employeeId
	 * @param typeId
	 * @return
	 */
	public EmployeeQualificationVo getQualificationByEmployeeAndDegree(Long employeeId, Long degree);

	/**
	 * method to get language
	 * 
	 * @param employeeId
	 * @param lamguageId
	 * @return
	 */
	public EmployeeLanguageVo getLanguagesByEmployeeAndLanguage(Long employeeId, Long languageId);

	/**
	 * method to get assets
	 * 
	 * @param employeeId
	 * @param lamguageId
	 * @return
	 */
	public EmployeeOrganisationAssetsVo getAssetsByEmployeeAndAsset(Long employeeId, Long assetId);

	/**
	 * method to get skill
	 * 
	 * @param employeeId
	 * @param lamguageId
	 * @return
	 */
	public EmployeeSkillsVo getSkillByEmployeeAndSkill(Long employeeId, Long skillId);

	/**
	 * method to get bank account details by employee and account type
	 * 
	 * @param employeeId
	 * @param typeId
	 * @return
	 */
	public EmployeeBankAccountVo getBankAccountByEmployeeAndType(Long employeeId, Long typeId);

	/**
	 * method to get roles
	 * 
	 * @return
	 */
	public RolesVo getRoleTypes();

	/**
	 * method to save roles
	 * 
	 * @param rolesVo
	 */
	public void saveRoles(RolesVo rolesVo);

	/**
	 * method to find employee roles
	 * 
	 * @param id
	 * @return
	 */
	public RolesVo findRolesByEmployee(Long id);

	/**
	 * list employees with payroll
	 * 
	 * @return
	 */
	public List<EmployeeVo> listEmployeesWithPayroll();

	/**
	 * method to list employees with payroll by branch and department
	 * 
	 * @param branchId
	 * @param departmentId
	 * @return
	 */
	public List<EmployeeVo> listEmployeesWithPayroll(Long branchId, Long departmentId);

	/**
	 * method to get employees by employee type
	 * 
	 * @param employeeTypeId
	 * @return
	 */
	public List<EmployeeVo> listEmployeesByEmployeeType(Long employeeTypeId);

	/**
	 * method to get employees by employee category
	 * 
	 * @param employeeCategoryId
	 * @return
	 */
	public List<EmployeeVo> listEmployeesByEmployeeCategory(Long employeeCategoryId);

	/**
	 * method to get employees by category and type
	 * 
	 * @param employeeCategoryId
	 * @param employeeTypeId
	 * @return
	 */
	public List<EmployeeVo> listEmployeesByEmployeeCategoryAndType(Long employeeCategoryId, Long employeeTypeId);

	/**
	 * method to get employees by branch , category and type
	 * 
	 * @param branchId
	 * @param employeeCategoryId
	 * @param employeeTypeId
	 * @return
	 */
	public List<EmployeeVo> listEmployeesByBranchEmployeeCategoryAndType(Long branchId, Long employeeCategoryId,
			Long employeeTypeId);

	/**
	 * method to get employees by department , category and type
	 * 
	 * @param departmentId
	 * @param employeeCategoryId
	 * @param employeeTypeId
	 * @return
	 */
	public List<EmployeeVo> listEmployeesByDepartmentEmployeeCategoryAndType(Long departmentId, Long employeeCategoryId,
			Long employeeTypeId);

	/**
	 * method to get employees by branch , department , category and type
	 * 
	 * @param branchId
	 * @param departmentId
	 * @param employeeCategoryId
	 * @param employeeTypeId
	 * @return
	 */
	public List<EmployeeVo> listEmployeesByBranchDepartmentEmployeeCategoryAndType(Long branchId, Long departmentId,
			Long employeeCategoryId, Long employeeTypeId);

	/**
	 * method to find employee roles
	 * 
	 * @param employeeId
	 * @return
	 */
	public SessionRolesVo getEmployeeRoles(Long employeeId);

	/**
	 * method to carry over leaves
	 */
	public void leaveCarryOver();

	/**
	 * method to find all employees details by employee code
	 * 
	 * @return
	 */
	public List<EmployeeVo> findAllEmployeesDeetailsByCode(String employeeCode);

	/**
	 * method to find inActive employees
	 * 
	 * @return
	 */
	public List<EmployeeVo> findInactiveEmployees();

	public EmployeeVo getEmployeesDetails(String employeeCode);

	/**
	 * method to find employees who has pay roll option
	 * 
	 * @return
	 */
	public List<EmployeeVo> findPayrollOptionEmployees();

	/**
	 * method to generate employee Code
	 * 
	 * @return
	 */
	public String generateEmployeeCode();

	/**
	 * method to write employees into excel
	 * 
	 * @param response
	 * @param branchId
	 * @return
	 */
	public String writeEmployeesToExcel(HttpServletResponse response, Long branchId);

	public List<EmployeeVo> getEmployeesByBranchAndDepartments(Long branchId, List<Long> departmentIds);

	/**
	 * Method to list all auto approve employees
	 * 
	 * @return
	 */
	public List<EmployeeVo> listAllAutoApproveEmployees();

	/**
	 * Method to update auto approval status
	 * 
	 * @param employeeId
	 * @param isAutoApprove
	 */
	public void updateEmployeeWithAutoApprove(Long employeeId, Boolean isAutoApprove);
	
	/**
	 * method to find employees for attendance status
	 * @param branchId
	 * @param departmentIds
	 * @return
	 */
	public List<AttendanceEmployeeVo> findEmployeesForAttendanceStatus(Long branchId,List<Long> departmentIds);
}
