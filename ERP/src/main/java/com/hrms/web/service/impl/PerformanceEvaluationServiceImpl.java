package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.PerformanceEvaluationDao;
import com.hrms.web.entities.PerformanceEvaluation;
import com.hrms.web.service.PerformanceEvaluationService;
import com.hrms.web.vo.PerformanceEvaluationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 24-March-2015
 *
 */
@Service
public class PerformanceEvaluationServiceImpl implements PerformanceEvaluationService {

	@Autowired
	private PerformanceEvaluationDao evaluationDao;

	public void savePerformanceEvaluation(PerformanceEvaluationVo evaluationVo) {
		evaluationDao.savePerformanceEvaluation(evaluationVo);
	}

	public void savePerformanceEvaluationDocuments(PerformanceEvaluationVo evaluationVo) {
		evaluationDao.savePerformanceEvaluationDocuments(evaluationVo);
	}

	public void updatePerformanceEvaluation(PerformanceEvaluationVo evaluationVo) {
		evaluationDao.updatePerformanceEvaluation(evaluationVo);
	}

	public void deletePerformanceEvaluation(Long evaluationId) {
		evaluationDao.deletePerformanceEvaluation(evaluationId);
	}

	public void deletePerformanceEvaluationDocument(Long documentId) {
		evaluationDao.deletePerformanceEvaluationDocument(documentId);
	}

	public PerformanceEvaluation findPerformanceEvaluation(Long evaluationId) {
		return evaluationDao.findPerformanceEvaluation(evaluationId);
	}

	public List<PerformanceEvaluation> findPerformanceEvaluationByStartDate(Date startDate) {
		return evaluationDao.findPerformanceEvaluationByStartDate(startDate);
	}

	public List<PerformanceEvaluation> findPerformanceEvaluationByEndDate(Date endDate) {
		return evaluationDao.findPerformanceEvaluationByEndDate(endDate);
	}

	public List<PerformanceEvaluation> findPerformanceEvaluationBetweenDates(Date startDate, Date endDate) {
		return evaluationDao.findPerformanceEvaluationBetweenDates(startDate, endDate);
	}

	public PerformanceEvaluationVo findPerformanceEvaluationById(Long evaluationId) {
		return evaluationDao.findPerformanceEvaluationById(evaluationId);
	}

	public List<PerformanceEvaluationVo> findPerformanceEvaluationVoByStartDate(Date startDate) {
		return evaluationDao.findPerformanceEvaluationVoByStartDate(startDate);
	}

	public List<PerformanceEvaluationVo> findPerformanceEvaluationVoByEndDate(Date endDate) {
		return evaluationDao.findPerformanceEvaluationVoByEndDate(endDate);
	}

	public List<PerformanceEvaluationVo> findPerformanceEvaluationVoBetweenDates(Date startDate, Date endDate) {
		return evaluationDao.findPerformanceEvaluationVoBetweenDates(startDate, endDate);
	}

	public List<PerformanceEvaluationVo> findAllPerformanceEvaluation() {
		return evaluationDao.findAllPerformanceEvaluation();
	}

	public void savePerformanceEvaluationQuestions(PerformanceEvaluationVo evaluationVo) {
		evaluationDao.savePerformanceEvaluationQuestions(evaluationVo);
	}

	public void deletePerformanceEvaluationQuestion(Long documentId) {
		evaluationDao.deletePerformanceEvaluationQuestion(documentId);
	}

}
