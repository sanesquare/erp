package com.hrms.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ProfidentFundSyntaxDao;
import com.hrms.web.entities.ProfidentFundSyntax;
import com.hrms.web.service.ProfidentFundSyntaxService;
import com.hrms.web.vo.ProfidentFundSyntaxVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Service
public class ProfidentFundSyntaxServiceImpl implements ProfidentFundSyntaxService {

	@Autowired
	private ProfidentFundSyntaxDao profidentFundSyntaxDao;

	public void saveOrUpdateProfidentFundSyntax(ProfidentFundSyntaxVo profidentFundSyntaxVo) {
		profidentFundSyntaxDao.saveOrUpdateProfidentFundSyntax(profidentFundSyntaxVo);
	}

	public void deleteProfidentFundSyntax(Long pfSyntaxId) {
		profidentFundSyntaxDao.deleteProfidentFundSyntax(pfSyntaxId);
	}

	public ProfidentFundSyntax findProfidentFundSyntax(Long pfSyntaxId) {
		return profidentFundSyntaxDao.findProfidentFundSyntax(pfSyntaxId);
	}

	public ProfidentFundSyntaxVo findProfidentFundSyntaxById(Long pfSyntaxId) {
		return profidentFundSyntaxDao.findProfidentFundSyntaxById(pfSyntaxId);
	}

	public ProfidentFundSyntaxVo findProfidentFundSyntax() {
		return profidentFundSyntaxDao.findProfidentFundSyntax();
	}

}
