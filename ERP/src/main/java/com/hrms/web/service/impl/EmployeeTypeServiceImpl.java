package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EmployeeTypeDao;
import com.hrms.web.entities.EmployeeType;
import com.hrms.web.service.EmployeeTypeService;
import com.hrms.web.vo.EmployeeTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Service
public class EmployeeTypeServiceImpl implements EmployeeTypeService {

	@Autowired
	private EmployeeTypeDao employeeTypeDao;
	
	public void saveEmployeeType(EmployeeTypeVo employeeTypeVo) {
		employeeTypeDao.saveEmployeeType(employeeTypeVo);
	}

	public void updateEmployeeType(EmployeeTypeVo employeeTypeVo) {
		employeeTypeDao.updateEmployeeType(employeeTypeVo);
	}

	public void deleteEmployeeType(Long employeeTypeId) {
		employeeTypeDao.deleteEmployeeType(employeeTypeId);
	}

	public EmployeeType findEmployeeType(Long employeeTypeId) {
		return employeeTypeDao.findEmployeeType(employeeTypeId);
	}

	public EmployeeType findEmployeeType(String employeeType) {
		return employeeTypeDao.findEmployeeType(employeeType);
	}

	public EmployeeTypeVo findEmployeeTypeById(Long employeeTypeId) {
		return employeeTypeDao.findEmployeeTypeById(employeeTypeId);
	}

	public EmployeeTypeVo findEmployeeTypeByType(String employeeType) {
		return employeeTypeDao.findEmployeeTypeByType(employeeType);
	}

	public List<EmployeeTypeVo> findAllEmployeeType() {
		return employeeTypeDao.findAllEmployeeType();
	}

}
