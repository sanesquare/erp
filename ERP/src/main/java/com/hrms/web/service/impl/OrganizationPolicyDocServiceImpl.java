package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.OrganizationPolicyDocDao;
import com.hrms.web.entities.OrganizationPolicyDoc;
import com.hrms.web.service.OrganizationPolicyDocService;
import com.hrms.web.vo.OrganizationPolicyDocVo;

/**
 * 
 * @author Jithin Mohan
 * @since 5-June-2015
 *
 */
@Service
public class OrganizationPolicyDocServiceImpl implements OrganizationPolicyDocService {

	@Autowired
	private OrganizationPolicyDocDao policyDocDao;

	public void saveOrUpdateOrganizationPolicyDoc(OrganizationPolicyDocVo docVo) {
		policyDocDao.saveOrUpdateOrganizationPolicyDoc(docVo);
	}

	public void deleteOrganizationPolicyDoc(Long documentId) {
		policyDocDao.deleteOrganizationPolicyDoc(documentId);
	}

	public OrganizationPolicyDoc findOrganizationPolicyDoc(Long documentId) {
		return policyDocDao.findOrganizationPolicyDoc(documentId);
	}

	public OrganizationPolicyDocVo findOrganizationPolicyDocById(Long documentId) {
		return policyDocDao.findOrganizationPolicyDocById(documentId);
	}

	public List<OrganizationPolicyDocVo> findAllOrganizationPolicyDocs() {
		return policyDocDao.findAllOrganizationPolicyDocs();
	}

}
