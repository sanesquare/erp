package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.TerminationStatus;
import com.hrms.web.vo.TerminationStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
public interface TerminationStatusService {

	/**
	 * method to save new TerminationStatus
	 * 
	 * @param terminationStatusVo
	 */
	public void saveTerminationStatus(TerminationStatusVo terminationStatusVo);

	/**
	 * method to update TerminationStatus
	 * 
	 * @param terminationStatusVo
	 */
	public void updateTerminationStatus(TerminationStatusVo terminationStatusVo);

	/**
	 * method to delete TerminationStatus
	 * 
	 * @param terminationStatusId
	 */
	public void deleteTerminationStatus(Long terminationStatusId);

	/**
	 * method to find TerminationStatus by id
	 * 
	 * @param terminationStatusId
	 * @return TerminationStatus
	 */
	public TerminationStatus findTerminationStatus(Long terminationStatusId);

	/**
	 * method to find TerminationStatus by status
	 * 
	 * @param status
	 * @return TerminationStatus
	 */
	public TerminationStatus findTerminationStatus(String status);

	/**
	 * method to find TerminationStatus by id
	 * 
	 * @param terminationStatusId
	 * @return TerminationStatusVo
	 */
	public TerminationStatusVo findTerminationStatusById(Long terminationStatusId);

	/**
	 * method to find TerminationStatus by status
	 * 
	 * @param status
	 * @return TerminationStatusVo
	 */
	public TerminationStatusVo findTerminationStatusByStatus(String status);

	/**
	 * method to find all TerminationStatus
	 * 
	 * @return List<TerminationStatusVo>
	 */
	public List<TerminationStatusVo> findAllTerminationStatus();
}
