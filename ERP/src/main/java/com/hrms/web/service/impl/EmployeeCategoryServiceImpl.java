package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EmployeeCategoryDao;
import com.hrms.web.entities.EmployeeCategory;
import com.hrms.web.service.EmployeeCategoryService;
import com.hrms.web.vo.EmployeeCategoryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Service
public class EmployeeCategoryServiceImpl implements EmployeeCategoryService {

	@Autowired
	private EmployeeCategoryDao employeeCategoryDao;

	public void saveEmployeeCategory(EmployeeCategoryVo employeeCategoryVo) {
		employeeCategoryDao.saveEmployeeCategory(employeeCategoryVo);
	}

	public void updateEmployeeCategory(EmployeeCategoryVo employeeCategoryVo) {
		employeeCategoryDao.updateEmployeeCategory(employeeCategoryVo);
	}

	public void deleteEmployeeCategory(Long employeeCategoryId) {
		employeeCategoryDao.deleteEmployeeCategory(employeeCategoryId);
	}

	public EmployeeCategory findEmployeeCategory(Long employeeCategoryId) {
		return employeeCategoryDao.findEmployeeCategory(employeeCategoryId);
	}

	public EmployeeCategory findEmployeeCategory(String employeeCategory) {
		return employeeCategoryDao.findEmployeeCategory(employeeCategory);
	}

	public EmployeeCategoryVo findEmployeeCategoryById(Long employeeCategoryId) {
		return employeeCategoryDao.findEmployeeCategoryById(employeeCategoryId);
	}

	public EmployeeCategoryVo findEmployeeCategoryByCategory(String employeeCategory) {
		return employeeCategoryDao.findEmployeeCategoryByCategory(employeeCategory);
	}

	public List<EmployeeCategoryVo> findAllEmployeeCategory() {
		return employeeCategoryDao.findAllEmployeeCategory();
	}

}
