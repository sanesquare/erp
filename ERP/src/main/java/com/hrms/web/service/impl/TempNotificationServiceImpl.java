package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TempNotificationDao;
import com.hrms.web.service.TempNotificationService;
import com.hrms.web.vo.TempNotificationVo;

/**
 * 
 * @author Shamsheer
 * @since 19-May-2015
 *
 */
@Service
public class TempNotificationServiceImpl implements TempNotificationService{

	@Autowired
	private TempNotificationDao dao;

	public void saveNotification(TempNotificationVo notificationVo) {
		dao.saveNotification(notificationVo);
	}

	public void updateNotification(TempNotificationVo notificationVo) {
		dao.updateNotification(notificationVo);
	}

	public void deleteNotification(Long id, String employeeCode) {
		dao.deleteNotification(id,employeeCode);
	}

	public List<TempNotificationVo> listNotificationForEmployee(Long employeeId) {
		return dao.listNotificationForEmployee(employeeId);
	}

	public List<TempNotificationVo> listNotificationForEmployee(String employeeCode) {
		return dao.listNotificationForEmployee(employeeCode);
	} 
}
