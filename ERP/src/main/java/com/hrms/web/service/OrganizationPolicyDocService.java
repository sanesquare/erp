package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.OrganizationPolicyDoc;
import com.hrms.web.vo.OrganizationPolicyDocVo;

/**
 * 
 * @author Jithin Mohan
 * @since 5-June-2015
 *
 */
public interface OrganizationPolicyDocService {

	/**
	 * 
	 * @param docVo
	 */
	public void saveOrUpdateOrganizationPolicyDoc(OrganizationPolicyDocVo docVo);

	/**
	 * 
	 * @param documentId
	 */
	public void deleteOrganizationPolicyDoc(Long documentId);

	/**
	 * 
	 * @param documentId
	 * @return
	 */
	public OrganizationPolicyDoc findOrganizationPolicyDoc(Long documentId);

	/**
	 * 
	 * @param documentId
	 * @return
	 */
	public OrganizationPolicyDocVo findOrganizationPolicyDocById(Long documentId);

	/**
	 * 
	 * @return
	 */
	public List<OrganizationPolicyDocVo> findAllOrganizationPolicyDocs();
}
