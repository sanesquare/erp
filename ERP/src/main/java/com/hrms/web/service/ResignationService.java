package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.ResignationVo;

/**
 * 
 * @author Shamsheer
 * @since 24-April-2015
 */
public interface ResignationService {

	/**
	 * method to save resignation
	 * 
	 * @param ResignationVo
	 * @return
	 */
	public Long saveResignation(ResignationVo resignationVo);

	/**
	 * method to update resignation
	 * 
	 * @param ResignationVo
	 */
	public void updateResignation(ResignationVo resignationVo);

	/**
	 * method to delete resignation
	 * 
	 * @param resignationId
	 */
	public void deleteResignation(Long resignationId);

	/**
	 * method to list all resignations
	 * 
	 * @return
	 */
	public List<ResignationVo> listAllResignation();

	/**
	 * method to get resignation by id
	 * 
	 * @param resignationId
	 * @return
	 */
	public ResignationVo getResignationById(Long resignationId);

	/**
	 * method to list resignations by employee
	 * 
	 * @param employeeId
	 * @return
	 */
	public List<ResignationVo> listResignationByEmployee(Long employeeId);

	/**
	 * method to list resignations by employee
	 * 
	 * @param employeeCode
	 * @return
	 */
	public List<ResignationVo> listResignationByEmployee(String employeeCode);

	/**
	 * method to update documents
	 * 
	 * @param ResignationVo
	 */
	public void updateDocument(ResignationVo resignationVo);

	/**
	 * method to delete document
	 * 
	 * @param url
	 */
	public void deleteDocument(String url);

	/**
	 * method to update status
	 * 
	 * @param ResignationVo
	 */
	public void updateStatus(ResignationVo resignationVo);

	/**
	 * method to list rsignation between two dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ResignationVo> listResignationBetweenDates(String startDate, String endDate);

	/**
	 * method to list resignations by employee ids
	 * 
	 * @param employeeIds
	 * @return
	 */
	public List<ResignationVo> listResignationsByEmployeeIds(List<Long> employeeIds);

	/**
	 * method to find resignation between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ResignationVo> findResignationBetweenDates(String startDate, String endDate);

	/**
	 * method to find resignation between dates & branch
	 * 
	 * @param branchId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ResignationVo> findResignationByBranchBetweenDates(Long branchId, String startDate, String endDate);

	/**
	 * method to find resignation between dates &department
	 * 
	 * @param departmentId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ResignationVo> findResignationByDepartmentBetweenDates(Long departmentId, String startDate,
			String endDate);

	/**
	 * method to find resignation between dates , branch & department
	 * 
	 * @param branchId
	 * @param departmentId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ResignationVo> findResignationByBranchDepartmentBetweenDates(Long branchId, Long departmentId,
			String startDate, String endDate);

	/**
	 * method to find all resignations by employee
	 * 
	 * @param employeeCode
	 * @return
	 */
	public List<ResignationVo> findAllResignationByEmployee(String employeeCode);
}
