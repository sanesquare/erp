package com.hrms.web.service;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.AdvanceSalary;
import com.hrms.web.entities.AdvanceSalaryStatus;
import com.hrms.web.entities.Employee;
import com.hrms.web.vo.AdvanceSalaryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public interface AdvanceSalaryService {

	/**
	 * 
	 * @param advanceSalaryVo
	 */
	public void saveOrUpdateAdvanceSalary(AdvanceSalaryVo advanceSalaryVo);

	/**
	 * 
	 * @param advanceSalaryVo
	 */
	public void updateAdvanceSalaryStatus(AdvanceSalaryVo advanceSalaryVo);

	/**
	 * 
	 * @param advanceSalaryId
	 */
	public void deleteAdvanceSalary(Long advanceSalaryId);

	/**
	 * 
	 * @param advanceSalaryId
	 * @return
	 */
	public AdvanceSalary findAdvanceSalary(Long advanceSalaryId);

	/**
	 * 
	 * @param advanceSalaryId
	 * @return
	 */
	public AdvanceSalaryVo findAdvanceSalaryById(Long advanceSalaryId);

	/**
	 * 
	 * @param employee
	 * @return
	 */
	public AdvanceSalaryVo findAdvanceSalaryByEmployee(Employee employee);

	/**
	 * 
	 * @param requestedDate
	 * @return
	 */
	public List<AdvanceSalaryVo> findAdvanceSalaryByDate(Date requestedDate);

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<AdvanceSalaryVo> findAdvanceSalaryBetweenDates(Date startDate, Date endDate);

	/**
	 * 
	 * @param status
	 * @return
	 */
	public List<AdvanceSalaryVo> findAdvanceSalaryByStatus(AdvanceSalaryStatus status);

	/**
	 * 
	 * @return
	 */
	public List<AdvanceSalaryVo> findAllAdvanceSalary();

	/**
	 * 
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<AdvanceSalaryVo> findAdvanceSalaryBetweenDate(Long employeeId, Date startDate, Date endDate);

	/**
	 * 
	 * @return
	 */
	public List<AdvanceSalaryVo> findAllAdvanceSalaryByEmployee(String employeeCode);

	/**
	 * 
	 * @param status
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<AdvanceSalaryVo> findAdvanceSalaryByStatusAndDates(String employeeCode, String status, Date startDate, Date endDate);
}
