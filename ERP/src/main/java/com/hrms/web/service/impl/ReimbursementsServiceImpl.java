package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.ReimbursementDao;
import com.hrms.web.service.ReimbursementsService;
import com.hrms.web.vo.ReimbursementsVo;

/**
 * 
 * @author Shamsheer
 * @since 17-April-2015
 */
@Service
public class ReimbursementsServiceImpl implements ReimbursementsService{

	@Autowired
	private ReimbursementDao dao;

	public Long saveReimbursement(ReimbursementsVo vo) {
		return dao.saveReimbursement(vo);
	}

	public void updateReimbursement(ReimbursementsVo vo) {
		dao.updateReimbursement(vo);
	}

	public void deleteReimbursements(Long reimbursementId) {
		dao.deleteReimbursements(reimbursementId);
	}

	public void updateDocuments(ReimbursementsVo vo) {
		dao.updateDocuments(vo);		
	}

	public void deleteDocuments(String url) {
		dao.deleteDocuments(url);
	}

	public List<ReimbursementsVo> listAllReimbursements() {
		return dao.listAllReimbursements();
	}

	public List<ReimbursementsVo> listReimbursementsByEmployee(Long employeeId) {
		return dao.listReimbursementsByEmployee(employeeId);
	}

	public void updateStatus(ReimbursementsVo vo) {
		dao.updateStatus(vo);
	}

	public ReimbursementsVo getReimbursementsById(Long reimbursementsId) {
		return dao.getReimbursementsById(reimbursementsId);
	}

	public List<ReimbursementsVo> listReimbursementsByEmployeeStartDateEndDate(Long employeeId, Date startDate, Date endDate) {
		return dao.listReimbursementsByEmployeeStartDateEndDate(employeeId, startDate, endDate);
	}

	public List<ReimbursementsVo> listReimbursementsByStartDateEndDate(Date startDate, Date endDate) {
		return dao.listReimbursementsByStartDateEndDate(startDate, endDate);
	}

	public List<ReimbursementsVo> findAllReimbursementsByEmployee(String employeeCode) {
		return dao.findAllReimbursementsByEmployee(employeeCode);
	}
}
