package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.AssignmentDao;
import com.hrms.web.service.AssignmentService;
import com.hrms.web.vo.AssignmentVo;

/**
 * 
 * @author Shamsheer
 * @since 26-March-2015
 *
 */
@Service
public class AssignmentServiceImpl implements AssignmentService{

	@Autowired
	private AssignmentDao assignmentDao;

	public Long saveAssignment(AssignmentVo assignmentVo) {
		return assignmentDao.saveAssignment(assignmentVo);
	}

	public void updateAssignment(AssignmentVo assignmentVo) {
		assignmentDao.updateAssignment(assignmentVo);
	}

	public void deleteAssignment(Long id) {
		assignmentDao.deleteAssignment(id);
	}

	public AssignmentVo findAssignmentById(Long id) {
		return assignmentDao.findAssignmentById(id);
	}

	public List<AssignmentVo> findAssignmentByName(String title) {
		return assignmentDao.findAssignmentByName(title);
	}

	public List<AssignmentVo> listAllAssignment() {
		return assignmentDao.listAllAssignment();
	}

	public List<AssignmentVo> listAssignmentByEmployee(Long employeeId) {
		return assignmentDao.listAssignmentByEmployee(employeeId);
	}

	public void deleteDocument(String url) {
		assignmentDao.deleteDocument(url);
	}

	public void updateDocument(AssignmentVo assignmentVo) {
		assignmentDao.updateDocument(assignmentVo);
	}
}
