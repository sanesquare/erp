package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




import com.hrms.web.dao.AnnouncementsStatusDao;
import com.hrms.web.entities.AnnouncementsStatus;
import com.hrms.web.logger.Log;
import com.hrms.web.service.AnnouncementsStatusService;
import com.hrms.web.vo.AnnouncementsStatusVo;

/**
 * 
 * @author Shimil Babu
 * @since 11-March-2015
 *
 */
@Service
public class AnnouncementsStatusServiceImpl implements AnnouncementsStatusService{

	/**
	 * Log
	 */
	@Log
	private static Logger logger;
	
	@Autowired
	private AnnouncementsStatusDao announcementsStatusDao;

	/**
	 * method to save announcements status
	 * @param announcements statusVo 
	 * @return boolean
	 */
	public boolean saveAnnouncementsStatus(AnnouncementsStatusVo announcementsStatusVo) {
		logger.info("inside AnnouncementsStatusServiceImpl>> saveAnnouncementsStatus");
		return false;
	}

	/**
	 * method to delete announcements status
	 * @param announcements statusVo 
	 * @return boolean
	 */
	public boolean deleteAnnouncementsStatus(AnnouncementsStatusVo announcementsStatusVo) {
		logger.info("inside AnnouncementsStatusServiceImpl>> deleteAnnouncementsStatus");
		return false;
	} 

	/**
	 * method to update announcements status
	 * @param announcements statusVo 
	 * @return boolean
	 */
	public boolean updateAnnouncementsStatus(AnnouncementsStatusVo announcementsStatusVo) {
		logger.info("inside AnnouncementsStatusServiceImpl>> updateAnnouncementsStatus");
		return false;
	}

	/**
	 * method to find all announcements status
	 * 
	 * @return List of Announcements status
	 */
	public List<AnnouncementsStatusVo> findAllAnnouncementsStatus() {
		logger.info("inside AnnouncementsStatusServiceImpl>> findAllAnnouncementsStatus");
		return announcementsStatusDao.findAllAnnouncementsStatus();
	}

	/**
	 * method to find all announcements status by status name
	 * 
	 * @return List of Announcements status
	 */
	public AnnouncementsStatus findAnnouncementsStatusByStatusName(String status) {
		return announcementsStatusDao.findAnnouncementsStatusByAnnouncementsStatusName(status);
		
	}
	
	

}

