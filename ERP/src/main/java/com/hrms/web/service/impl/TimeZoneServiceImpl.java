package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TimeZoneDao;
import com.hrms.web.entities.TimeZone;
import com.hrms.web.service.TimeZoneService;
import com.hrms.web.vo.TimeZoneVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Service
public class TimeZoneServiceImpl implements TimeZoneService {

	@Autowired
	private TimeZoneDao timeZoneDao;

	/**
	 * method to save new TimeZone
	 * 
	 * @param timeZoneVo
	 */
	public void saveTimeZone(TimeZoneVo timeZoneVo) {
		timeZoneDao.saveTimeZone(timeZoneVo);
	}

	/**
	 * method to update TimeZone
	 * 
	 * @param timeZoneVo
	 */
	public void updateTimeZone(TimeZoneVo timeZoneVo) {
		timeZoneDao.updateTimeZone(timeZoneVo);
	}

	/**
	 * method to delete TimeZone
	 * 
	 * @param timeZoneId
	 */
	public void deleteTimeZone(Long timeZoneId) {
		timeZoneDao.deleteTimeZone(timeZoneId);
	}

	/**
	 * method to find TimeZone by id
	 * 
	 * @param timeZoneId
	 * @return TimeZone
	 */
	public TimeZone findTimeZone(Long timeZoneId) {
		return timeZoneDao.findTimeZone(timeZoneId);
	}

	/**
	 * method to find TimeZone by location
	 * 
	 * @param location
	 * @return TimeZone
	 */
	public TimeZone findTimeZone(String location) {
		return timeZoneDao.findTimeZone(location);
	}

	/**
	 * method to find TimeZone by id
	 * 
	 * @param timeZoneId
	 * @return TimeZoneVo
	 */
	public TimeZoneVo findTimeZoneById(Long timeZoneId) {
		return timeZoneDao.findTimeZoneById(timeZoneId);
	}

	/**
	 * method to find TimeZone by location
	 * 
	 * @param location
	 * @return TimeZoneVo
	 */
	public TimeZoneVo findTimeZoneByLocation(String location) {
		return timeZoneDao.findTimeZoneByLocation(location);
	}

	/**
	 * method to find all TimeZone
	 * 
	 * @return List<TimeZoneVo>
	 */
	public List<TimeZoneVo> findAllTimeZone() {
		return timeZoneDao.findAllTimeZone();
	}

}
