package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.OvertimeStatus;
import com.hrms.web.vo.OvertimeStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
public interface OvertimeStatusService {

	/**
	 * 
	 * @param advanceSalaryStatusVo
	 */
	public void saveOvertimeStatus(OvertimeStatusVo overtimeStatusVo);

	/**
	 * 
	 * @param advanceSalaryStatusVo
	 */
	public void updateOvertimeStatus(OvertimeStatusVo overtimeStatusVo);

	/**
	 * 
	 * @param advanceSalaryStatusId
	 */
	public void deleteOvertimeStatus(Long overtimeStatusId);

	/**
	 * 
	 * @param advanceSalaryStatusId
	 * @return
	 */
	public OvertimeStatus findOvertimeStatus(Long overtimeStatusId);

	/**
	 * 
	 * @param status
	 * @return
	 */
	public OvertimeStatus findOvertimeStatus(String status);

	/**
	 * 
	 * @param advanceSalaryStatusId
	 * @return
	 */
	public OvertimeStatusVo findOvertimeStatusById(Long overtimeStatusId);

	/**
	 * 
	 * @param status
	 * @return
	 */
	public OvertimeStatusVo findOvertimeStatusByStatus(String status);

	/**
	 * 
	 * @return
	 */
	public List<OvertimeStatusVo> findAllOvertimeStatus();
}
