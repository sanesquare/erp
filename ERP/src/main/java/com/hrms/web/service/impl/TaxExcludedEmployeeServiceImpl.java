package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.TaxExcludedEmployeeDao;
import com.hrms.web.entities.TaxExcludeEmployee;
import com.hrms.web.service.TaxExcludedEmployeeService;
import com.hrms.web.vo.TaxExcludedEmployeeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 28-April-2015
 *
 */
@Service
public class TaxExcludedEmployeeServiceImpl implements TaxExcludedEmployeeService {

	@Autowired
	private TaxExcludedEmployeeDao taxExcludedEmployeeDao;

	public void saveTaxExcludedEmployee(TaxExcludedEmployeeVo taxExcludedEmployeeVo) {
		taxExcludedEmployeeDao.saveTaxExcludedEmployee(taxExcludedEmployeeVo);
	}

	public void updateTaxExcludedEmployee(TaxExcludedEmployeeVo taxExcludedEmployeeVo) {
		taxExcludedEmployeeDao.updateTaxExcludedEmployee(taxExcludedEmployeeVo);
	}

	public void deleteTaxExcludedEmployee(Long taxExcludedId) {
		taxExcludedEmployeeDao.deleteTaxExcludedEmployee(taxExcludedId);
	}

	public TaxExcludeEmployee findTaxExcludedEmployee(Long taxExludedId) {
		return taxExcludedEmployeeDao.findTaxExcludedEmployee(taxExludedId);
	}

	public TaxExcludeEmployee findTaxExcludedEmployee(String employeeCode) {
		return taxExcludedEmployeeDao.findTaxExcludedEmployee(employeeCode);
	}

	public TaxExcludedEmployeeVo findTaxExcludedEmployeeById(Long taxExcludedId) {
		return taxExcludedEmployeeDao.findTaxExcludedEmployeeById(taxExcludedId);
	}

	public TaxExcludedEmployeeVo findTaxExcludedEmployeeByEmployee(String employeeCode) {
		return taxExcludedEmployeeDao.findTaxExcludedEmployeeByEmployee(employeeCode);
	}

	public TaxExcludedEmployeeVo findTaxExcludedEmployeeList() {
		return taxExcludedEmployeeDao.findTaxExcludedEmployeeList();
	}

	public List<TaxExcludedEmployeeVo> findAllTaxExcludedEmployee() {
		return taxExcludedEmployeeDao.findAllTaxExcludedEmployee();
	}

}
