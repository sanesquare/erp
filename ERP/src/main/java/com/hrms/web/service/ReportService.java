package com.hrms.web.service;

import java.util.List;

import com.hrms.web.form.BankStatementForm;
import com.hrms.web.form.EmpAssigmentsReportForm;
import com.hrms.web.form.EmpExpirationReportForm;
import com.hrms.web.form.EmpJoinReportForm;
import com.hrms.web.form.EmpPerformanceForm;
import com.hrms.web.form.EmpResignReportForm;
import com.hrms.web.form.EmpSummaryForm;
import com.hrms.web.form.EmpTransfersReportForm;
import com.hrms.web.form.EmpTravelReportForm;
import com.hrms.web.form.HrBranchForm;
import com.hrms.web.form.PayAdjstmntForm;
import com.hrms.web.form.PayAdvSalaryForm;
import com.hrms.web.form.PayBonusForm;
import com.hrms.web.form.PayCommisionForm;
import com.hrms.web.form.PayDailyWagesForm;
import com.hrms.web.form.PayDeductionsForm;
import com.hrms.web.form.PayEsciForm;
import com.hrms.web.form.PayHourlyWagesForm;
import com.hrms.web.form.PayLoansForm;
import com.hrms.web.form.PayOTForm;
import com.hrms.web.form.PayPFForm;
import com.hrms.web.form.PayPaySlipReportForm;
import com.hrms.web.form.PayRembOTForm;
import com.hrms.web.form.PaySalaryReportForm;
import com.hrms.web.form.PaySalarySplitForm;
import com.hrms.web.form.PayrollSummaryForm;
import com.hrms.web.form.TimeAbsentEmployeesForm;
import com.hrms.web.form.TimeEmpLeaveSummaryForm;
import com.hrms.web.form.TimeEmpTimesheetReportForm;
import com.hrms.web.form.TimeEmpWorkReportForm;
import com.hrms.web.form.TimeWorkSheetReportForm;
import com.hrms.web.form.TimeWorkShiftForm;
import com.hrms.web.vo.DetailedEmployeeReportVo;
import com.hrms.web.vo.EmpPrimaryReportVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.HrBranchDetailsVo;
import com.hrms.web.vo.HrBranchVo;
import com.hrms.web.vo.HrEmpReportViewVo;
import com.hrms.web.vo.HrEmpReportVo;
import com.hrms.web.vo.HrHolidayVo;
import com.hrms.web.vo.HrHolidaysViewVo;
import com.hrms.web.vo.HrLeavesDetailsVo;
import com.hrms.web.vo.HrLeavesViewVo;
import com.hrms.web.vo.HrLeavesVo;
import com.hrms.web.vo.HrProEmployeeViewVo;
import com.hrms.web.vo.HrSummaryViewVo;
import com.hrms.web.vo.JobCandidateReportVo;
import com.hrms.web.vo.JobInterviewReportVo;
import com.hrms.web.vo.JobPostReportVo;
import com.hrms.web.vo.PayDailyWageDetailsVo;
import com.hrms.web.vo.PayHrlyWagesDetailsVo;
import com.hrms.web.vo.PayPrimaryReportVo;
import com.hrms.web.vo.PayrollSummaryVo;
import com.hrms.web.vo.RecCandReportViewVo;
import com.hrms.web.vo.RecInterviewReportViewVo;
import com.hrms.web.vo.RecPostReportViewVo;
import com.hrms.web.vo.TimeReportPrimaryVo;

/**
 * 
 * @author Vinutha
 * @since 14-April-2015
 *
 */
public interface ReportService {

	/**
	 * Method to create list of employee details to be displayed in report
	 * @param reportVo
	 * @return List<HrEmpReportViewVo>
	 */
	public List<HrEmpReportViewVo> createEmployeeReport(HrEmpReportVo reportVo);
	/**
	 * Method to create detailed Employee Report
	 * @param reportVo
	 * @return
	 */
	public List<DetailedEmployeeReportVo> createDetailedEmployeeReport(HrEmpReportVo reportVo);

	/**
	 * Create Hr Summary View
	 * @return
	 */
	public HrSummaryViewVo createHrSummary();
	/**
	 * Method to create payroll report
	 * @return
	 */
	public PayrollSummaryForm createPayrollReport();
	/**
	 * Method to create holidays report
	 * @return
	 */
	public HrHolidaysViewVo createHolidaysReport(HrHolidayVo holidayVo);
	/**
	 * Method to create Leaves report
	 * @param birthdayVo
	 * @return
	 */
	public HrLeavesViewVo createLeavesReport(HrLeavesVo leavesVo);
	/**
	 * Method to get years
	 * @return
	 */
	public List<Integer> getYears();
	/**
	 * Method to create posts report
	 * @param reportVo
	 * @return
	 */
	public RecPostReportViewVo createPostReport(JobPostReportVo reportVo);
	/**
	 * Method to create job candidate report
	 * @param reportVo
	 * @return
	 */
	public RecCandReportViewVo createCandReport(JobCandidateReportVo reportVo);
	/**
	 * Method to create interview report
	 * @param reportVo
	 * @return
	 */
	public RecInterviewReportViewVo createInterviewReport(JobInterviewReportVo reportVo);
	/**
	 * Method to create project employee report
	 * @return
	 */
	public HrProEmployeeViewVo createProjectEmployeeReport();
	/**
	 * Method to create joining report
	 * @return
	 */
	public EmpJoinReportForm createJoiningReport(EmpPrimaryReportVo reportVo);
	/**
	 * Method to create expiration Dates report
	 * @return
	 */
	public EmpExpirationReportForm createExpirationReport();
	/**
	 * Method to create project- employee report with id
	 * @param projectId
	 * @return
	 */
	public HrProEmployeeViewVo createProjectEmployeeReportWithId(Long projectId);
	/**
	 * Method to create employee transfers report
	 * @param reportVo
	 * @return
	 */
	public EmpTransfersReportForm createTransfersReport(EmpPrimaryReportVo reportVo);
	/**
	 * Method to create resignation report
	 * @param reportVo
	 * @return
	 */
	public EmpResignReportForm createResignationReport(EmpPrimaryReportVo reportVo);
	/**
	 * Method to create travel report
	 * @param reportVo
	 * @return
	 */
	public EmpTravelReportForm createTravelReport(EmpPrimaryReportVo reportVo);
	/**
	 * Method to create employee assigments report
	 * @param reportVo
	 * @return
	 */
	public EmpAssigmentsReportForm createEmpAssgmntsReport(EmpPrimaryReportVo reportVo);
	/**
	 * Method to create loans report
	 * @param reportVo
	 * @return
	 */
	public PayLoansForm createLoansReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create advance salary report
	 * @param reportVo
	 * @return
	 */
	public PayAdvSalaryForm createAdvanceSalaryReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create ESCI report
	 * @param reportVo
	 * @return
	 */
	public PayEsciForm createESCIReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create PF Reports
	 * @param reportVo
	 * @return
	 */
	public PayPFForm createPFReports(PayPrimaryReportVo reportVo);
	/**
	 * Method to create over time reports
	 * @param reportVo
	 * @return
	 */
	public PayOTForm createOTReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create reimbursement reports
	 * @param reportVo
	 * @return
	 */
	public PayRembOTForm createReimbursementReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to createAdjustments report
	 * @param reportVo
	 * @return
	 */
	public PayAdjstmntForm createAdjstmntsReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create commision report
	 * @param reportVo
	 * @return
	 */
	public PayCommisionForm createCommissionsReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create Bonus report
	 * @param reportVo
	 * @return
	 */
	public PayBonusForm createBonusReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create deduction report
	 * @param reportVo
	 * @return
	 */
	public PayDeductionsForm createDeductionsReport (PayPrimaryReportVo reportVo);
	/**
	 * Method to create daily wages report
	 * @param reportVo
	 * @return
	 */
	public PayDailyWagesForm createDailyWagesReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create hourly wages report
	 * @param reportVo
	 * @return
	 */
	public PayHourlyWagesForm createHourlyWagesReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create work sheet report
	 * @param reportVo
	 * @return
	 */
	public TimeWorkSheetReportForm createWorkSheetReport(TimeReportPrimaryVo reportVo);
	/**
	 * Method to create employee work sheet report
	 * @param reportVo
	 * @return
	 */
	public TimeEmpWorkReportForm createEmpWorkSheetReport(TimeReportPrimaryVo reportVo);
	/**
	 * Method to create employee time sheet report
	 * @param reportVo
	 * @return
	 */
	public TimeEmpTimesheetReportForm createEmployeeTimeSheetReport(TimeReportPrimaryVo reportVo);
	/**
	 * Method to create employee leave summary
	 * @param reporVo
	 * @return
	 */
	public TimeEmpLeaveSummaryForm createEmployeeLeaveSummary(TimeReportPrimaryVo reporVo);
	/**
	 * Method to create absent employees report
	 * @param reportVo
	 * @return
	 */
	public TimeAbsentEmployeesForm createAbsentEmployeesReport(TimeReportPrimaryVo reportVo);
	/**
	 * Method to create salary report 
	 * @param reportVo
	 * @return
	 */
	public PaySalaryReportForm createSalaryReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create payslip report
	 * @param reportVo
	 * @return
	 */
	public PayPaySlipReportForm createPaySlipReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create salary split report
	 * @param reportVo
	 * @return
	 */
	public PaySalarySplitForm createSalarySplitReport(PayPrimaryReportVo reportVo);
	/**
	 * Method to create employee summary report
	 * @param reportVo
	 * @return
	 */
	public EmpSummaryForm createEmployeeSummaryReport(EmpPrimaryReportVo reportVo);
	/**
	 * Method to create work shift report
	 * @param reportVo
	 * @return
	 */
	public TimeWorkShiftForm createWorkShiftReport(TimeReportPrimaryVo reportVo);
	/**
	 * Method to create pay roll report
	 * @param reportVo
	 * @return
	 */
	 public PayrollSummaryForm createPayrollSummaryReport(PayrollSummaryVo reportVo);
	 /**
	  * Method to create branch report
	  * @param branchVo
	  * @return
	  */
	 public HrBranchForm createHrBranchReport(HrBranchVo branchVo);
	 /**
	  * Method to create performance evaluation report
	  * @param reportVo
	  * @return
	  */
	 public EmpPerformanceForm createPerformanceEvaluatioReport(EmpPrimaryReportVo reportVo);
	 
	 /**
	  * method to calculate daily wage
	  * @param id
	  * @param empCode
	  * @param empName
	  * @param startDate
	  * @param endDate
	  * @return
	  */
	 public List<PayDailyWageDetailsVo> getEmployeeDailyWageDetails(Long id,String empCode, String empName,String startDate , String endDate );
	 
	 /**
	  * method to calculate hourly wage
	  * @param emp
	  * @param startDate
	  * @param endDate
	  * @return
	  */
	 public List<PayHrlyWagesDetailsVo> getEmployeeHourlyWagesDetails(EmployeeVo emp , String startDate ,String endDate);
	 /**
	  * Method to create bank statement
	  * @param vo
	  * @return
	  */
	 public BankStatementForm createBankStatement(PayPrimaryReportVo vo);
}
