package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.AdjustmentTitleDao;
import com.hrms.web.service.AdjustmentTitleService;
import com.hrms.web.vo.AdjustmentTitleVo;

/**
 * @author Shamsheer
 * @since 14-April-2015
 */
@Service
public class AdjustmentTitleServiceImpl implements AdjustmentTitleService{

	@Autowired
	private AdjustmentTitleDao dao;

	public void saveAdjustmentTitle(AdjustmentTitleVo titleVo) {
		dao.saveAdjustmentTitle(titleVo);
	}

	public void updateAdjustmentTitle(AdjustmentTitleVo titleVo) {
		dao.updateAdjustmentTitle(titleVo);
	}

	public void deleteAdjustmentTitle(Long adjustmentTitleId) {
		dao.deleteAdjustmentTitle(adjustmentTitleId);
	}

	public void deleteAdjustmentTitle(String AdjustmentTitle) {
		dao.deleteAdjustmentTitle(AdjustmentTitle);
	}

	public List<AdjustmentTitleVo> listAllAdjustmentTitles() {
		return dao.listAllAdjustmentTitles();
	} 
	
}
