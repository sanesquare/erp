package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.AnnouncementsDao;
import com.hrms.web.entities.Announcements;
import com.hrms.web.logger.Log;
import com.hrms.web.service.AnnouncementsService;
import com.hrms.web.vo.AnnouncementsVo;

/**
 * 
 * @author Shimil Babu
 * @since 10-March-2015
 *
 */
@Service
public class AnnouncementsServiceImpl implements AnnouncementsService {
	/**
	 * Log
	 */
	@Log
	private static Logger logger;
	
	@Autowired
	private AnnouncementsDao announcementsDao;
	
	/**
	 * method to save announcements
	 * @param announcements 
	 * @return boolean
	 */
	public void saveOrUpdateAnnouncements(AnnouncementsVo announcementsVo) {
		logger.info("inside AnnouncementsServiceImpl>> saveAnnouncements");
		announcementsDao.saveOrUpdateAnnouncements(announcementsVo);
	}
	
	/**
	 * method to delete announcements
	 * @param announcements
	 * @return boolean
	 */
	public void deleteAnnouncements(Long id) {
		logger.info("inside AnnouncementsServiceImpl>> deleteAnnouncements");
		announcementsDao.deleteAnnouncements(id);
	}
	
	/**
	 * method to update announcements
	 * @param announcements
	 * @return boolean
	 */
	public void updateAnnouncements(AnnouncementsVo announcementsVo) {
		logger.info("inside AnnouncementsServiceImpl>> updateAnnouncements");
		announcementsDao.updateAnnouncements(announcementsVo);
		
	}
	
	/**
	 * method to find announcements by announcements Id 
	 * @param id
	 * @return Announcements
	 */
	public AnnouncementsVo findAnnouncementsById(Long id) {
		logger.info("inside AnnouncementsServiceImpl>> findAnnouncementsByAnnouncementsId");
		return announcementsDao.findAnnouncementsById(id);
	}
	
	/**
	 * method to find announcements by announcements title 
	 * @param title
	 * @return Announcements
	 */
	public AnnouncementsVo findAnnouncementsByAnnouncementsTitle(String title) {
		logger.info("inside AnnouncementsServiceImpl>> findAnnouncementsByAnnouncementsTitle");
		return null;
	}
	
	/**
	 * method to find announcements by announcements date 
	 * @param title
	 * @return Announcements
	 */
	public AnnouncementsVo findAnnouncementsByDate(Date date) {
		logger.info("inside AnnouncementsServiceImpl>> findAnnouncementsByDate");
		return null;
	}
	
	/**
	 * method to find all announcements 
	 * 
	 * @return List of Announcements 
	 */
	public List<AnnouncementsVo> findAllAnnouncements() {
		logger.info("inside AnnouncementsServiceImpl>> findAllAnnouncements");
		return announcementsDao.findAllAnnouncements();
		
	}

	/**
	 * method to find announcements object by title
	 * 
	 * @return  Announcements 
	 */
	public Announcements findAndReturnAnnouncementsObjectByTitle(String title) {
		
		return announcementsDao.findAndReturnAnnouncementsObjectByTitle(title);
		
	}
	/**
	 * method to update announcements status
	 * 
	 * @return 
	 */

	/*
	 * public void updateAnnouncementStatus(List<AnnouncementsVo>
	 * announcementsVo) {
	 * announcementsDao.updateAnnouncementStatus(announcementsVo); }
	 */

	/**
	 * method to find all active announcements
	 * @return announcementsVos
	 */
	public List<AnnouncementsVo> findAllActiveAnnouncements() {
		
		return announcementsDao.findAllActiveAnnouncements();
	}

	/**
	 * method to add announcements status
	 * @return announcementsVos
	 */
	public void updateAnnouncementStatus(AnnouncementsVo announcementsVo) {
		announcementsDao.updateAnnouncementStatus(announcementsVo);
	}

	public void saveAnnouncementDocuments(AnnouncementsVo announcementsVo) {
		announcementsDao.saveAnnouncementDocuments(announcementsVo);
	}

	public void deleteAnnouncementDocument(Long documentId) {
		announcementsDao.deleteAnnouncementDocument(documentId);
	}
}
