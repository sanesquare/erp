package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.WorkSheetDao;
import com.hrms.web.entities.WorkSheet;
import com.hrms.web.service.WorkSheetService;
import com.hrms.web.vo.WorkSheetVo;

/**
 * 
 * @author Vinutha
 * @since 1-April-2015
 *
 */
@Service
public class WorkSheetServiceImpl implements WorkSheetService{

	@Autowired
	private WorkSheetDao sheetDao;
	/**
	 * method to save basic info
	 * @param worksheetVo
	 * @return
	 */
	public Long saveBasicInfo(WorkSheetVo worksheetVo) {
		
		return sheetDao.saveBasicInfo(worksheetVo);
	}
	/**
	 * method to update basic info
	 * @param worksheetVo
	 */
	public void updateBasicInfo(WorkSheetVo worksheetVo) {
		sheetDao.updateBasicInfo(worksheetVo);
		
	}
	/**
	 * mehtod to save project and task
	 * @param worksheetVo
	 */
	public void saveProjectTask(WorkSheetVo worksheetVo) {
		sheetDao.saveProjectTask(worksheetVo);
		
	}
	/**
	 * mehtod to update project and task
	 * @param worksheetVo
	 */
	public void updateProjectTask(WorkSheetVo worksheetVo) {
		sheetDao.updateProjectTask(worksheetVo);
		
	}
	/**
	 * method to save description
	 * @param worksheetVo
	 */
	public void saveDesc(WorkSheetVo worksheetVo) {
		sheetDao.saveDesc(worksheetVo);
		
	}
	/**
	 * method to save additional information
	 * @param worksheetVo
	 */
	public void saveAddInfo(WorkSheetVo worksheetVo) {
		sheetDao.saveAddInfo(worksheetVo);
		
	}
	/**
	 * method to return object by id
	 * @param id
	 * @return
	 */
	public WorkSheet findAndReturnObjectById(long id) {
		
		return sheetDao.findAndReturnObjectById(id);
	}
	/**
	 * method to update documents
	 * @param workSheetVo
	 */
	public void updateDocuments(WorkSheetVo workSheetVo) {
		sheetDao.updateDocuments(workSheetVo);
		
	}
	/**
	 * method to list all worksheets
	 * @return
	 */
	public List<WorkSheetVo> listAllWorksheets() {
		
		return sheetDao.listAllWorksheets();
	}
	/**
	 * method to list employee worksheets
	 * @return
	 */
	public List<WorkSheetVo> listEmployeeWorkSheets(String employeeCode) {
		
		return sheetDao.listEmployeeWorkSheets(employeeCode);
	}
	/**
	 * method to find WorkSheetVo by id
	 * @param id
	 * @return
	 */
	public WorkSheetVo findWorksheetVoById(Long id) {

		return sheetDao.findWorksheetVoById(id);
	}
	/**
	 * method to delete documents
	 * @param url
	 */
	public void deleteDocument(String url) {
		sheetDao.deleteDocument(url);
		
	}
	/**
	 * method to delete work sheet by id
	 * @param id
	 */
	public void deleteWorkSheetById(Long id) {
		sheetDao.deleteWorkSheetById(id);
		
	}
	/**
	 * Method to list workSheet within given date range
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<WorkSheetVo> listWorkSheetWithinDateRange(String startDate , String endDate){
		return sheetDao.listWorkSheetWithinDateRange(startDate, endDate);
	}
	/**
	 * Method to list work sheet of an employee within date range
	 * @param empCode
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<WorkSheetVo> listWorkSheetWithEmployeeCodeWithinDateRange(String empCode , String startDate , String endDate ){
		return sheetDao.listWorkSheetWithEmployeeCodeWithinDateRange(empCode, startDate, endDate);
	}
}
