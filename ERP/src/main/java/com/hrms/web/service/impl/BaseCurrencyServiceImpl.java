package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.BaseCurrencyDao;
import com.hrms.web.entities.BaseCurrency;
import com.hrms.web.service.BaseCurrencyService;
import com.hrms.web.vo.BaseCurrencyVo;

/**
 * 
 * @author Jithin Mohan
 * @since 12-March-2015
 *
 */
@Service
public class BaseCurrencyServiceImpl implements BaseCurrencyService {

	@Autowired
	private BaseCurrencyDao baseCurrencyDao;

	/**
	 * method to save new BaseCurrency
	 * 
	 * @param baseCurrencyVo
	 */
	public void saveBaseCurrency(BaseCurrencyVo baseCurrencyVo) {
		baseCurrencyDao.saveBaseCurrency(baseCurrencyVo);
	}

	/**
	 * method to update BaseCurrency
	 * 
	 * @param baseCurrencyVo
	 */
	public void updateBaseCurrency(BaseCurrencyVo baseCurrencyVo) {
		baseCurrencyDao.updateBaseCurrency(baseCurrencyVo);
	}

	/**
	 * method to delete BaseCurrency
	 * 
	 * @param baseCurrencyId
	 */
	public void deleteBaseCurrency(Long baseCurrencyId) {
		baseCurrencyDao.deleteBaseCurrency(baseCurrencyId);
	}

	/**
	 * method to find BaseCurrency by id
	 * 
	 * @param baseCurrencyId
	 * @return BaseCurrency
	 */
	public BaseCurrency findBaseCurrency(Long baseCurrencyId) {
		return baseCurrencyDao.findBaseCurrency(baseCurrencyId);
	}

	/**
	 * method to find BaseCurrency by id
	 * 
	 * @param baseCurrency
	 * @return BaseCurrency
	 */
	public BaseCurrency findBaseCurrency(String baseCurrency) {
		return baseCurrencyDao.findBaseCurrency(baseCurrency);
	}

	/**
	 * method to find BaseCurrency by currency
	 * 
	 * @param baseCurrencyId
	 * @return BaseCurrencyVo
	 */
	public BaseCurrencyVo findBaseCurrencyById(Long baseCurrencyId) {
		return baseCurrencyDao.findBaseCurrencyById(baseCurrencyId);
	}

	/**
	 * method to find BaseCurrency by currency
	 * 
	 * @param baseCurrency
	 * @return BaseCurrencyVo
	 */
	public BaseCurrencyVo findBaseCurrencyByCurrency(String baseCurrency) {
		return baseCurrencyDao.findBaseCurrencyByCurrency(baseCurrency);
	}

	/**
	 * method to find all BaseCurrency
	 * 
	 * @return List<BaseCurrencyVo>
	 */
	public List<BaseCurrencyVo> findAllBaseCurrency() {
		return baseCurrencyDao.findAllBaseCurrency();
	}

}
