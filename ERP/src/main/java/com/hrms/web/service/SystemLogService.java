package com.hrms.web.service;

import com.hrms.web.vo.UserActivityVo;

/**
 * 
 * @author sangeeth
 * 13/3/2015
 *
 */
public interface SystemLogService {

	/**
	 * mathod to save SystemLog
	 * @param activity
	 */
	public void saveSystemLog(UserActivityVo systemLogVo);
	
}
