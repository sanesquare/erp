package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.DailyWagesDao;
import com.hrms.web.service.DailyWagesService;
import com.hrms.web.vo.DailyWagesVo;

/**
 * 
 * @author Shamsheer
 * @since 16-April-2015
 */
@Service
public class DailyWagesServiceImpl implements DailyWagesService{

	@Autowired
	private DailyWagesDao dao;

	public Long saveDailyWage(DailyWagesVo vo) {
		return dao.saveDailyWage(vo);
	}

	public void updateDailyWage(DailyWagesVo vo) {
		dao.updateDailyWage(vo);
	}

	public void deleteDailyWage(Long wageId) {
		dao.deleteDailyWage(wageId);
	}

	public List<DailyWagesVo> listAllDailyWages() {
		return dao.listAllDailyWages();
	}

	public DailyWagesVo getDailyWagesById(Long dailyWagesId) {
		return dao.getDailyWagesById(dailyWagesId);
	}

	public DailyWagesVo getDailyWagesByEmployeeId( String employeeCode) {
		return dao.getDailyWagesByEmployeeId(employeeCode);
	}

	public List<DailyWagesVo> findAllDailyWagesByEmployee(String employeeCode) {
		return dao.findAllDailyWagesByEmployee(employeeCode);
	}
}
