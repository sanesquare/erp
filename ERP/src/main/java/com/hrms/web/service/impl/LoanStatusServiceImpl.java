package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.LoanStatusDao;
import com.hrms.web.entities.LoanStatus;
import com.hrms.web.service.LoanStatusService;
import com.hrms.web.vo.LoanStatusVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Service
public class LoanStatusServiceImpl implements LoanStatusService {

	@Autowired
	private LoanStatusDao loanStatusDao;

	public void saveLoanStatus(LoanStatusVo loanStatusVo) {
		loanStatusDao.saveLoanStatus(loanStatusVo);
	}

	public void updateLoanStatus(LoanStatusVo loanStatusVo) {
		loanStatusDao.updateLoanStatus(loanStatusVo);
	}

	public void deleteLoanStatus(Long statusId) {
		loanStatusDao.deleteLoanStatus(statusId);
	}

	public LoanStatus findLoanStatus(Long statusId) {
		return loanStatusDao.findLoanStatus(statusId);
	}

	public LoanStatus findLoanStatus(String status) {
		return loanStatusDao.findLoanStatus(status);
	}

	public LoanStatusVo findLoanStatusById(Long statusId) {
		return loanStatusDao.findLoanStatusById(statusId);
	}

	public LoanStatusVo findLoanStatusByStatus(String status) {
		return loanStatusDao.findLoanStatusByStatus(status);
	}

	public List<LoanStatusVo> findAllLoanStatus() {
		return loanStatusDao.findAllLoanStatus();
	}

}
