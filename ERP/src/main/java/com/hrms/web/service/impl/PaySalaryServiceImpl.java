package com.hrms.web.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.constants.PayRollConstants;
import com.hrms.web.constants.PaySlipConstants;
import com.hrms.web.dao.AttendanceDao;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dao.ExtraPaySlipItemDao;
import com.hrms.web.dao.PaySalaryDao;
import com.hrms.web.dao.PaySlipItemDao;
import com.hrms.web.dao.PayslipDao;
import com.hrms.web.dao.SalaryDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.PaySalary;
import com.hrms.web.entities.PaySalaryDailyWages;
import com.hrms.web.entities.PaySalaryEmployees;
import com.hrms.web.entities.PaySalaryHourlyWage;
import com.hrms.web.entities.UserProfile;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.AttendanceService;
import com.hrms.web.service.PaySalaryService;
import com.hrms.web.service.ReportService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.SmsServiceUtil;
import com.hrms.web.vo.AttendanceVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.ExtraPaySlipItemVo;
import com.hrms.web.vo.PayDailyWageDetailsVo;
import com.hrms.web.vo.PayHrlyWagesDetailsVo;
import com.hrms.web.vo.PayPaysalaryVo;
import com.hrms.web.vo.PaySalaryDailySalaryVo;
import com.hrms.web.vo.PaySalaryEmployeeItemsVo;
import com.hrms.web.vo.PaySalaryHourlySalaryVo;
import com.hrms.web.vo.PaySalaryMonthlyTitleVo;
import com.hrms.web.vo.PaySalaryMonthlyVo;
import com.hrms.web.vo.PaySalaryVo;
import com.hrms.web.vo.PaySlipItemVo;
import com.hrms.web.vo.PaysSlipVo;
import com.hrms.web.vo.PayslipEmployeeItemsVo;
import com.hrms.web.vo.SalaryDailyWagesVo;
import com.hrms.web.vo.SalaryHourlyWageVo;

/**
 * 
 * @author Shamsheer
 * @since 08-June-2015
 *
 */
@Service
public class PaySalaryServiceImpl implements PaySalaryService {

	@Autowired
	private PaySalaryDao paySalaryDao;

	@Autowired
	private PayslipDao paySlipDao;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private ExtraPaySlipItemDao extraPayslipItemDao;

	@Autowired
	private AttendanceService attendanceService;

	@Autowired
	private SalaryDao salaryDao;

	@Autowired
	private ReportService reportService;

	@Autowired
	private PaySlipItemDao paySlipItemDao;

	@Log
	private Logger logger;

	public PaySalaryVo calculatePaySalary(PaySalaryVo paySalaryVo) {
		PaySalaryVo salaryVo = new PaySalaryVo();
		salaryVo.setMonthly(true);
		PaySalaryMonthlyVo monthlyVo = new PaySalaryMonthlyVo();
		List<PaySalaryMonthlyTitleVo> titleVos = new ArrayList<PaySalaryMonthlyTitleVo>();
		salaryVo.setDate(paySalaryVo.getDate());
		// getting previous month from given date
		Date salaryDate = DateFormatter.convertStringToDate(paySalaryVo.getDate());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(salaryDate);
		calendar.add(Calendar.MONTH, -1);
		Date previousMonth = calendar.getTime();
		calendar.setTime(previousMonth);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date endDate = calendar.getTime();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		Date startDate = calendar.getTime();

		List<ExtraPaySlipItemVo> extraPaySlipItemVos = extraPayslipItemDao.findAllExtraPaySlipItem();
		if (extraPaySlipItemVos != null && extraPaySlipItemVos.size() > 0) {
			for (ExtraPaySlipItemVo extraPaySlipItemVo : extraPaySlipItemVos) {
				salaryVo.getExtraPayslipItems().add(extraPaySlipItemVo.getTitle());
			}
		}
		List<PaySlipItemVo> paysSlips = new ArrayList<PaySlipItemVo>();
		paysSlips = paySlipItemDao.findAllPaySlipItem();
		for (PaySlipItemVo itemVo : paysSlips) {
			salaryVo.getPaySlipItems().add(itemVo.getTitle());
		}

		// fetching each employee & calculationg each salary
		for (Long id : paySalaryVo.getEmployeeIds()) {
			// Employee employee = employeeDao.findAndReturnEmployeeById(id);
			EmployeeVo employee = employeeDao.findEmployeeById(id);
			List<PaysSlipVo> paysSlipVos = paySlipDao.getPayslipDetailsByEmployee(employee.getEmployeeCode(),
					DateFormatter.convertDateToString(startDate), DateFormatter.convertDateToString(endDate));
			for (PaysSlipVo paysSlipVo : paysSlipVos) {
				PaySalaryEmployeeItemsVo itemsVo = new PaySalaryEmployeeItemsVo();
				itemsVo.setPerDayCalculation(paysSlipVo.getPerDayCalculation());
				itemsVo.setTotlaWorkingDays(paysSlipVo.getTotalWorkingDays());
				Map<String, BigDecimal> otherSalaryItems = new HashMap<String, BigDecimal>();
				itemsVo.setEmployeeCode(employee.getEmployeeCode());
				itemsVo.setName(employee.getName());
				itemsVo.setBranch(employee.getEmployeeBranch());
				itemsVo.setDepartment(employee.getEmployeeDepartment());
				itemsVo.setDesignation(employee.getDesignation());
				itemsVo.setStatus(employee.getStatus());
				for (PayslipEmployeeItemsVo vo : paysSlipVo.getItems()) {
					if (vo.getTitle().equalsIgnoreCase(PayRollConstants.GROSS_SALARY))
						itemsVo.setGrossSalary(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.ESI)) {
						itemsVo.setEsiApplicability("Yes");
						itemsVo.setEsi(vo.getAmount());
					} else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.PROVIDENT_FUND)) {
						itemsVo.setPfApplicability("Yes");
						itemsVo.setPf(vo.getAmount());
					} else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.LWF))
						itemsVo.setLwf(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.PROFESSIONAL_TAX))
						itemsVo.setProfessionalTaxs(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.DEDUCTIONS))
						itemsVo.setDeductions(itemsVo.getDeductions().add(vo.getAmount()));
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.ADJUSTMENT_ENCASHMENT))
						itemsVo.setEncashment(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.COMMISSIONS))
						itemsVo.setIncentive(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.LOAN))
						itemsVo.setLoan(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.PF_EMPLOYER))
						itemsVo.setPfEmployer(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.ESI_EMPLOYER))
						itemsVo.setEsiEmployer(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.TAX))
						itemsVo.setTax(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.ADVANCE_SALARY))
						itemsVo.setAdvanceSalary(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.BONUSES))
						itemsVo.setBonus(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.ADJUSTMENT_INCREMENT))
						itemsVo.setIncrement(vo.getAmount());
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.ADJUSTMENT_DECREMENT))
						itemsVo.setDeductions(itemsVo.getDeductions().add(vo.getAmount()));
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.LOP_COUNT))
						itemsVo.setLopCount(Double.parseDouble(vo.getAmount().toString()));
					else if (vo.getTitle().equalsIgnoreCase(PaySlipConstants.LOP_DEDUCTION))
						itemsVo.setLopDeduction(vo.getAmount());
					else if (salaryVo.getExtraPayslipItems().contains(vo.getTitle())) {
						otherSalaryItems.put(vo.getTitle(), vo.getAmount());
					} else {
						itemsVo.getItems().put(vo.getTitle(), vo.getAmount());
					}
				}
				itemsVo.setOtherSalaryItems(otherSalaryItems);
				// setting total earnings >> gross salary + incentives +
				// encashment + bonuses + increment
				itemsVo.setTotalEarnings(itemsVo.getGrossSalary().add(
						itemsVo.getIncentive().add(
								itemsVo.getEncashment().add(itemsVo.getBonus().add(itemsVo.getIncrement())))));

				// setting total deduction
				itemsVo.setTotalDeduction(itemsVo.getPf().add(
						itemsVo.getEsi().add(
								itemsVo.getLwf().add(
										itemsVo.getProfessionalTaxs().add(
												itemsVo.getTax().add(
														itemsVo.getDeductions().add(
																itemsVo.getLopDeduction().add(
																		itemsVo.getLoan().add(
																				itemsVo.getAdvanceSalary())))))))));

				// setting net pay >> total earnings - total deduction
				itemsVo.setNetPay(itemsVo.getTotalEarnings().subtract(itemsVo.getTotalDeduction()));

				// setting ctc
				itemsVo.setCtc(itemsVo.getNetPay().add(itemsVo.getPfEmployer().add(itemsVo.getEsiEmployer())));

				salaryVo.getEmployeeItemsVo().add(itemsVo);
			}
		}
		salaryVo.setMonthly(true);
		monthlyVo.setTitleVos(titleVos);
		return salaryVo;
	}

	public PaySalaryVo calculateDailyPaySalary(PaySalaryVo paySalaryVo) {
		PaySalaryVo salaryVo = new PaySalaryVo();

		// getting previous month from given date
		Date salaryDate = DateFormatter.convertStringToDate(paySalaryVo.getDate());
		for (Long id : paySalaryVo.getEmployeeIds()) {
			EmployeeVo employee = employeeDao.findEmployeeById(id);
			List<AttendanceVo> attendanceVos = attendanceService.listAttendanceByEmployeeAndDate(id,
					paySalaryVo.getDate());
			if (attendanceVos.size() > 0) {
				SalaryDailyWagesVo dailyWagesVo = salaryDao.getDailyWageForDay(employee.getEmployeeCode(), salaryDate);
				if (dailyWagesVo != null) {
					PaySalaryDailySalaryVo dailySalaryVo = new PaySalaryDailySalaryVo();
					dailySalaryVo.setDateString(paySalaryVo.getDate());
					dailySalaryVo.setEmpName(employee.getName());
					dailySalaryVo.setAmount(new BigDecimal(dailyWagesVo.getSalary()));
					dailySalaryVo.setDailyRate(new BigDecimal(dailyWagesVo.getSalary()));
					dailySalaryVo.setEmployeeCode(employee.getEmployeeCode());
					salaryVo.getDailyWageDetailsVos().add(dailySalaryVo);
				}
			}
		}
		salaryVo.setDailyWageDate(paySalaryVo.getDate());
		salaryVo.setDaily(true);
		salaryVo.setDate(paySalaryVo.getDate());
		return salaryVo;
	}

	public PaySalaryVo calculateHourlyPaySalary(PaySalaryVo paySalaryVo) {
		PaySalaryVo salaryVo = new PaySalaryVo();

		// getting previous month from given date
		Date salaryDate = DateFormatter.convertStringToDate(paySalaryVo.getDate());
		for (Long id : paySalaryVo.getEmployeeIds()) {
			EmployeeVo employee = employeeDao.findEmployeeById(id);
			AttendanceVo attendanceVo = attendanceService.calculateHoursWorkedByEmployeeOnDate(id,
					paySalaryVo.getDate());
			if (attendanceVo != null && attendanceVo.getTotalWorkedHours() != null) {
				SalaryHourlyWageVo hourlyWageVo = salaryDao.getHourlyWageForDay(employee.getEmployeeCode(), salaryDate);
				if (hourlyWageVo != null && hourlyWageVo.getRegularHourSalary() != null) {
					PaySalaryHourlySalaryVo hourlySalaryVo = new PaySalaryHourlySalaryVo();
					hourlySalaryVo.setAmount(new BigDecimal(hourlyWageVo.getRegularHourSalary()).multiply(
							new BigDecimal(attendanceVo.getTotalWorkedHours())).setScale(2, RoundingMode.HALF_UP));
					hourlySalaryVo.setEmployeeCode(employee.getEmployeeCode());
					hourlySalaryVo.setEmployeeName(employee.getName());
					hourlySalaryVo.setTotalHours(Double.parseDouble(attendanceVo.getTotalWorkedHours()));
					hourlySalaryVo.setHourlyRate(new BigDecimal(hourlyWageVo.getRegularHourSalary()));
					salaryVo.getHourlyWagesDetailsVos().add(hourlySalaryVo);
				}
			}
		}
		salaryVo.setHourlyWageDate(paySalaryVo.getDate());
		salaryVo.setHourly(true);
		salaryVo.setDate(paySalaryVo.getDate());
		return salaryVo;
	}

	public Long savePaysalaryHourly(PaySalaryVo paySalaryVo) {
		return paySalaryDao.savePaysalaryHourly(paySalaryVo);
	}

	public Long savePaysalaryDaily(PaySalaryVo paySalaryVo) {
		return paySalaryDao.savePaysalaryDaily(paySalaryVo);
	}

	public Long savePaysalaryMonthly(PaySalaryVo paySalaryVo) {
		return paySalaryDao.savePaysalaryMonthly(paySalaryVo);
	}

	public List<PaySalaryVo> listPaySalaries(String employeeCode) {
		return paySalaryDao.listPaySalaries(employeeCode);
	}

	public PaySalaryVo findDailyPaySalary(Long id) {
		return paySalaryDao.findDailyPaySalary(id);
	}

	public PaySalaryVo findHourlyPaySalary(Long id) {
		return paySalaryDao.findHourlyPaySalary(id);
	}

	public PaySalaryVo findMonthlyPaySalary(Long id) {
		return paySalaryDao.findMonthlyPaySalary(id);
	}

	public void updateDailyPaySalary(PaySalaryVo paySalaryVo) {
		paySalaryDao.updateDailyPaySalary(paySalaryVo);
	}

	public void updateHourlyPaySalary(PaySalaryVo paySalaryVo) {
		paySalaryDao.updateHourlyPaySalary(paySalaryVo);
	}

	public void deletePaysalary(Long id) {
		paySalaryDao.deletePaysalary(id);
	}

	public void updateMonthlyPaySalary(PaySalaryVo paySalaryVo) {
		paySalaryDao.updateMonthlyPaySalary(paySalaryVo);
	}

	public Boolean sendGroupSms(Long id) {
		PaySalary paySalary = paySalaryDao.findPaySalary(id);
		String msg = "";
		List<String> sendTo = new ArrayList<String>();
		if (paySalary == null)
			throw new ItemNotFoundException("Paysalary Not Exist.");

		Date salaryDate = paySalary.getDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(salaryDate);
		calendar.add(Calendar.MONTH, -1);
		Date previousMonth = calendar.getTime();
		String monthYear = DateFormatter.convertDateToMonthYearString(previousMonth);

		if (paySalary.getPaySalaryDailyWages().size() > 0) {
			msg = "Wages has been processed.";
			for (PaySalaryDailyWages dailyWages : paySalary.getPaySalaryDailyWages()) {
				UserProfile profile = dailyWages.getEmployee().getUserProfile();
				if (profile != null && profile.getMobile() != null)
					sendTo.add(profile.getMobile());
			}
		} else if (paySalary.getPaySalaryHourlyWages().size() > 0) {
			msg = "Wages has been processed.";
			for (PaySalaryHourlyWage hourlyWage : paySalary.getPaySalaryHourlyWages()) {
				UserProfile profile = hourlyWage.getEmployee().getUserProfile();
				if (profile != null && profile.getMobile() != null)
					sendTo.add(profile.getMobile());
			}
		} else if (paySalary.getPaySalaryMonthly().size() > 0) {
			msg = "Salary for the month of " + monthYear + " has been processed.";
			for (PaySalaryEmployees monthlyWage : paySalary.getPaySalaryMonthly()) {
				UserProfile profile = monthlyWage.getEmployee().getUserProfile();
				if (profile != null && profile.getMobile() != null)
					sendTo.add(profile.getMobile());
			}
		}
		try {
			SmsServiceUtil.sendSMS(sendTo, msg);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean sendIndividualSms(String empCode, Long id, String type) {
		Employee employee = employeeDao.findAndReturnEmployeeByCode(empCode);
		String msg = "";
		if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_MONTHLY)) {
			PaySalary paySalary = paySalaryDao.findPaySalary(id);
			if (paySalary == null)
				throw new ItemNotFoundException("Paysalary Not Exist.");

			Date salaryDate = paySalary.getDate();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(salaryDate);
			calendar.add(Calendar.MONTH, -1);
			Date previousMonth = calendar.getTime();
			String monthYear = DateFormatter.convertDateToMonthYearString(previousMonth);
			msg = "Salary for the month of " + monthYear + " has been processed.";
		} else
			msg = "Wages has been processed.";
		List<String> sendTo = new ArrayList<String>();
		UserProfile profile = employee.getUserProfile();
		if (profile != null && profile.getMobile() != null) {
			sendTo.add(profile.getMobile());
			try {
				SmsServiceUtil.sendSMS(sendTo, msg);
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	public List<PaySalaryHourlySalaryVo> getEmployeeHourlyWage(String employeeCode, String startDate, String endDate) {
		return paySalaryDao.getEmployeeHourlyWage(employeeCode, startDate, endDate);
	}

	public List<PaySalaryDailySalaryVo> getEmployeeDailyWage(String employeeCode, String startDate, String endDate) {
		return paySalaryDao.getEmployeeDailyWage(employeeCode, startDate, endDate);
	}

	public List<PaySalaryEmployeeItemsVo> getEmployeeMonthlyWage(String employeeCode, String startDate, String endDate) {
		return paySalaryDao.getEmployeeMonthlyWage(employeeCode, startDate, endDate);
	}

	public List<PaySalaryVo> listPaySalariesByCompany(Long companyId) {
		return paySalaryDao.listPaySalariesByCompany(companyId);
	}

	public List<PaySalaryVo> findSalariesByStatus(String status, Long companyId) {
		return paySalaryDao.findSalariesByStatus(status, companyId);
	}

	public List<PaySalaryVo> findUnpaidSalaries(Long companyId) {
		return paySalaryDao.findUnpaidSalaries(companyId);
	}

	public void updateSalaryStatus(Long salaryId, Long statusId) {
		paySalaryDao.updateSalaryStatus(salaryId, statusId);
	}

	public void paySalary(Long ledgerId, Long salaryId) {
		paySalaryDao.updateSalary(ledgerId, salaryId);
	}

	public void updatePaySalaryPay(PayPaysalaryVo vo) {
		paySalaryDao.updatePaySalaryPay(vo);

	}

}
