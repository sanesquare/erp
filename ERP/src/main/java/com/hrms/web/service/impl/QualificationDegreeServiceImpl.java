package com.hrms.web.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.QualificationDegreeDao;
import com.hrms.web.entities.QualificationDegree;
import com.hrms.web.logger.Log;
import com.hrms.web.service.QualificationDegreeService;
import com.hrms.web.vo.QualificationDegreeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
@Service
public class QualificationDegreeServiceImpl implements QualificationDegreeService {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private QualificationDegreeDao degreeDao;

	/**
	 * method to save new qualification degree
	 * 
	 * @param degreeVo
	 */
	public void saveQualificationDegree(QualificationDegreeVo degreeVo) {
		logger.info("Inside QualificationDegree Service >>> saveQualificationDegree.......");
		degreeDao.saveQualificationDegree(degreeVo);
	}

	/**
	 * method to update qualification information
	 * 
	 * @param degreeVo
	 */
	public void updateQualificationDegree(QualificationDegreeVo degreeVo) {
		logger.info("Inside QualificationDegree Service >>> updateQualificationDegree.......");
		degreeDao.updateQualificationDegree(degreeVo);
	}

	/**
	 * method to delete qualification information
	 * 
	 * @param degreeId
	 */
	public void deleteQualificationDegree(Long degreeId) {
		logger.info("Inside QualificationDegree Service >>> deleteQualificationDegree.......");
		degreeDao.deleteQualificationDegree(degreeId);
	}

	/**
	 * method to find qualification by id
	 * 
	 * @param degreeId
	 * @return qualificationdegree
	 */
	public QualificationDegree findQualification(Long degreeId) {
		logger.info("Inside QualificationDegree Service >>> findQualification.......");
		return degreeDao.findQualification(degreeId);
	}

	/**
	 * method to find qualification information
	 * 
	 * @param degreeId
	 * @return qualificationdegreevo
	 */
	public QualificationDegreeVo findQialificationDegreeById(Long degreeId) {
		logger.info("Inside QualificationDegree Service >>> findQialificationDegreeById.......");
		return degreeDao.findQialificationDegreeById(degreeId);
	}

	/**
	 * method to find qualification by name
	 * 
	 * @param degreeName
	 * @return qualificationdegree
	 */
	public QualificationDegree findQualification(String degreeName) {
		logger.info("Inside QualificationDegree Service >>> findQualification.......");
		return degreeDao.findQualification(degreeName);
	}

	/**
	 * method to find qualification by name
	 * 
	 * @param degreeName
	 * @return qualificationdegreevo
	 */
	public QualificationDegreeVo findQualificationDegreeByName(String degreeName) {
		logger.info("Inside QualificationDegree Service >>> findQualificationDegreeByName.......");
		return degreeDao.findQualificationDegreeByName(degreeName);
	}

	/**
	 * method to find all qualification
	 * 
	 * @return List<QualificationDegree>
	 */
	public List<QualificationDegreeVo> findAllQualificationDegree() {
		logger.info("Inside QualificationDegree Service >>> findAllQualificationDegree.......");
		return degreeDao.findAllQualificationDegree();
	}

}
