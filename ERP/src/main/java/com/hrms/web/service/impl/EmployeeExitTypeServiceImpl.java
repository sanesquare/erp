package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.EmployeeExitTypeDao;
import com.hrms.web.entities.EmployeeExitType;
import com.hrms.web.service.EmployeeExitTypeService;
import com.hrms.web.vo.EmployeeExitTypeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 26-March-2015
 *
 */
@Service
public class EmployeeExitTypeServiceImpl implements EmployeeExitTypeService {

	@Autowired
	private EmployeeExitTypeDao employeeExitTypeDao;

	public void saveEmployeeExitType(EmployeeExitTypeVo employeeExitTypeVo) {
		employeeExitTypeDao.saveEmployeeExitType(employeeExitTypeVo);
	}

	public void updateEmployeeExitType(EmployeeExitTypeVo employeeExitTypeVo) {
		employeeExitTypeDao.updateEmployeeExitType(employeeExitTypeVo);
	}

	public void deleteEmployeeExitType(Long exitTypeId) {
		employeeExitTypeDao.deleteEmployeeExitType(exitTypeId);
	}

	public EmployeeExitType findEmployeeExitType(Long exitTypeId) {
		return employeeExitTypeDao.findEmployeeExitType(exitTypeId);
	}

	public EmployeeExitType findEmployeeExitType(String exitType) {
		return employeeExitTypeDao.findEmployeeExitType(exitType);
	}

	public EmployeeExitTypeVo findEmployeeExitTypeById(Long exitTypeId) {
		return employeeExitTypeDao.findEmployeeExitTypeById(exitTypeId);
	}

	public EmployeeExitTypeVo findEmployeeExitTypeByType(String exitType) {
		return employeeExitTypeDao.findEmployeeExitTypeByType(exitType);
	}

	public List<EmployeeExitTypeVo> findAllEmployeeExitType() {
		return employeeExitTypeDao.findAllEmployeeExitType();
	}

}
