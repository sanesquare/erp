package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.DeductionTitleDao;
import com.hrms.web.service.DeductionTitleService;
import com.hrms.web.vo.DeductionTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 13-April-2015
 */
@Service
public class DeductionTitleServiceImpl implements DeductionTitleService{

	@Autowired
	private DeductionTitleDao titleDao;

	public void saveDeductionTitle(DeductionTitleVo titleVo) {
		titleDao.saveDeductionTitle(titleVo);
	}

	public void updateDeductionTitle(DeductionTitleVo titleVo) {
		titleDao.updateDeductionTitle(titleVo);
	}

	public void deleteDeductionTitle(Long deductionTitleId) {
		titleDao.deleteDeductionTitle(deductionTitleId);
	}

	public void deleteDeductionTitle(String deductionTitle) {
		titleDao.deleteDeductionTitle(deductionTitle);
	}

	public List<DeductionTitleVo> listAllDeductionTitles() {
		return titleDao.listAllDeductionTitles();
	}
}
