package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.MstReference;

/**
 * 
 * @author Vips
 *
 */
public interface CommonService {
	
	/**
	 * 
	 * @param refernceType
	 * @return
	 */
	public List<MstReference>  fectMstreferneceByType(String refernceType);
}
