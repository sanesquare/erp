package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.OrganisationAssetsDao;
import com.hrms.web.entities.OrganisationAssets;
import com.hrms.web.service.OrganisationAssetsService;
import com.hrms.web.vo.OrganisationAssetsVo;

/**
 * 
 * @author shamsheer
 * @since 21-March-2015
 */
@Service
public class OrganisationAssetsServiceImpl implements OrganisationAssetsService{

	@Autowired
	private OrganisationAssetsDao assetsDao;
	
	public void saveAssets(OrganisationAssetsVo assetsVo) {
		assetsDao.saveAssets(assetsVo);
	}

	public void updateAssets(OrganisationAssetsVo assetsVo) {
		assetsDao.updateAssets(assetsVo);
	}

	public void deleteAsset(Long id) {
		assetsDao.deleteAsset(id);
	}

	public OrganisationAssetsVo findAssetById(Long id) {
		return assetsDao.findAssetById(id);
	}

	public OrganisationAssetsVo findAssetByName(String name) {
		return assetsDao.findAssetByName(name);
	}

	public OrganisationAssetsVo findAssetByCode(String code) {
		return assetsDao.findAssetByCode(code);
	}

	public OrganisationAssets findAssetObjById(Long id) {
		return assetsDao.findAssetObjById(id);
	}

	public OrganisationAssets findAssetObjByName(String name) {
		return assetsDao.findAssetObjByName(name);
	}

	public OrganisationAssets findAssetObjByCode(String code) {
		return assetsDao.findAssetObjByCode(code);
	}

	public List<OrganisationAssetsVo> listAllAssets() {
		return assetsDao.listAllAssets();
	}

}
