package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.LoanDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Loan;
import com.hrms.web.entities.LoanStatus;
import com.hrms.web.service.LoanService;
import com.hrms.web.vo.LoanVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-April-2015
 *
 */
@Service
public class LoanServiceImpl implements LoanService {

	@Autowired
	private LoanDao loanDao;

	public void saveOrUpdateLoan(LoanVo loanVo) {
		loanDao.saveOrUpdateLoan(loanVo);
	}

	public void updateLoanStatus(LoanVo loanVo) {
		loanDao.updateLoanStatus(loanVo);
	}

	public void deleteLoan(Long loanId) {
		loanDao.deleteLoan(loanId);
	}

	public void saveLoanDocument(LoanVo loanVo) {
		loanDao.saveLoanDocument(loanVo);
	}

	public void deleteLoanDocument(Long documentId) {
		loanDao.deleteLoanDocument(documentId);
	}

	public Loan findLoan(Long loanId) {
		return loanDao.findLoan(loanId);
	}

	public LoanVo findLoanById(Long loanId) {
		return loanDao.findLoanById(loanId);
	}

	public List<LoanVo> findLoanByEmployee(Employee employee) {
		return loanDao.findLoanByEmployee(employee);
	}

	public List<LoanVo> findLoanByDate(Date loanDate) {
		return loanDao.findLoanByDate(loanDate);
	}

	public List<LoanVo> findLoanByRepaymentDate(Date repaymentDate) {
		return loanDao.findLoanByRepaymentDate(repaymentDate);
	}

	public List<LoanVo> findLoanByStatus(LoanStatus loanStatus) {
		return loanDao.findLoanByStatus(loanStatus);
	}

	public List<LoanVo> findAllLoan() {
		return loanDao.findAllLoan();
	}

	public List<LoanVo> findLoanByBetweenDates(Long employeeId, Date startDate, Date endDate) {
		return loanDao.findLoanByBetweenDates(employeeId, startDate, endDate);
	}

	public List<LoanVo> findAllLoanByEmployee(String employeeCode) {
		return loanDao.findAllLoanByEmployee(employeeCode);
	}

	public List<LoanVo> findLoanByStatusAndDates(String employeeCode, String status, Date startDate, Date endDate) {
		return loanDao.findLoanByStatusAndDates(employeeCode, status, startDate, endDate);
	}

}
