package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.UserActivities;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.UserActivityVo;

/**
 * 
 * @author Sangeeth
 * 15/3/2015
 *
 */
public interface UserActivitiesService {

	/**
	 * method to list distinct Dates by month and year
	 * @return List<UserActivityVo>
	 */
	public List<UserActivityVo> listDistinctDatesByMonth();
	
	/**
	 * methode to save a user activity
	 * @param activity
	 */
	public void saveUserActivity(String activity);
	
	/**
	 * method to list all user activities of users
	 * @return List<UserActivityVo>
	 */
	public List<UserActivityVo> listAllUserActivities(int month,int year);
}
