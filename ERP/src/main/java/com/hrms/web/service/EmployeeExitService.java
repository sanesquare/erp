package com.hrms.web.service;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.EmployeeExit;
import com.hrms.web.vo.EmployeeExitVo;

/**
 * 
 * @author Jithin Mohan
 * @since 18-March-2015
 *
 */
public interface EmployeeExitService {

	/**
	 * method to save EmployeeExit
	 * 
	 * @param employeeExitVo
	 * @return Long
	 */
	public Long saveEmployeeExit(EmployeeExitVo employeeExitVo);

	/**
	 * method to save EmployeeExitDocuments
	 * 
	 * @param employeeExitVo
	 */
	public void saveEmployeeExitDocuments(EmployeeExitVo employeeExitVo);

	/**
	 * method to update EmployeeExit
	 * 
	 * @param employeeExitVo
	 */
	public void updateEmployeeExit(EmployeeExitVo employeeExitVo);

	/**
	 * method to delete EmployeeExit
	 * 
	 * @param employeeExitId
	 */
	public void deleteEmployeeExit(Long employeeExitId);

	/**
	 * method to find EmployeeExit by id
	 * 
	 * @param employeeExitId
	 * @return EmployeeExit
	 */
	public EmployeeExit findEmployeeExit(Long employeeExitId);

	/**
	 * method to find EmployeeExit by date
	 * 
	 * @param exitDate
	 * @return EmployeeExit
	 */
	public EmployeeExit findEmployeeExit(Date exitDate);

	/**
	 * method to find EmployeeExit between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return EmployeeExit
	 */
	public EmployeeExit findEmployeeExit(Date startDate, Date endDate);

	/**
	 * method to find EmployeeExit by is
	 * 
	 * @param employeeExitId
	 * @return EmployeeExit
	 */
	public EmployeeExitVo findEmployeeExitById(Long employeeExitId);

	/**
	 * method to find EmployeeExit by date
	 * 
	 * @param exitDate
	 * @return EmployeeExit
	 */
	public EmployeeExitVo findEmployeeExitByDate(Date exitDate);

	/**
	 * method to find EmployeeExit between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return EmployeeExit
	 */
	public EmployeeExitVo findEmployeeExitBetweenDates(Date startDate, Date endDate);

	/**
	 * method to find all EmployeeExit
	 * 
	 * @return List<EmployeeExitVo>
	 */
	public List<EmployeeExitVo> findAllEmployeeExit();

	/**
	 * method to delete employeeExit Document
	 * 
	 * @param employeeExitDocumentId
	 */
	public void deleteEmployeeExitDocument(Long employeeExitDocumentId);

	/**
	 * method to find EmployeeExit by employeeCode
	 * 
	 * @param employeeCode
	 * @return EmployeeExitVo
	 */
	public EmployeeExitVo findEmployeeExitByEmployee(String employeeCode);

	/**
	 * method to find all EmployeeExit by employee
	 * 
	 * @return List<EmployeeExitVo>
	 */
	public List<EmployeeExitVo> findAllEmployeeExitByEmployee(String employeeCode);
}
