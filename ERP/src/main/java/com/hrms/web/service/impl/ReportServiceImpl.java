package com.hrms.web.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.constants.AccountTypeConstants;
import com.hrms.web.constants.PayRollConstants;
import com.hrms.web.constants.PaySlipConstants;
import com.hrms.web.constants.StatusConstants;
import com.hrms.web.dao.AttendanceDao;
import com.hrms.web.dao.ReportDao;
import com.hrms.web.dto.EsiShare;
import com.hrms.web.dto.PFShare;
import com.hrms.web.entities.Attendance;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.EmployeeDesignation;
import com.hrms.web.form.BankStatementForm;
import com.hrms.web.form.EmpAssigmentsReportForm;
import com.hrms.web.form.EmpExpirationReportForm;
import com.hrms.web.form.EmpJoinReportForm;
import com.hrms.web.form.EmpPerformanceForm;
import com.hrms.web.form.EmpResignReportForm;
import com.hrms.web.form.EmpSummaryForm;
import com.hrms.web.form.EmpTransfersReportForm;
import com.hrms.web.form.EmpTravelReportForm;
import com.hrms.web.form.HrBranchForm;
import com.hrms.web.form.PayAdjstmntForm;
import com.hrms.web.form.PayAdvSalaryForm;
import com.hrms.web.form.PayBonusForm;
import com.hrms.web.form.PayCommisionForm;
import com.hrms.web.form.PayDailyWagesForm;
import com.hrms.web.form.PayDeductionsForm;
import com.hrms.web.form.PayEsciForm;
import com.hrms.web.form.PayHourlyWagesForm;
import com.hrms.web.form.PayLoansForm;
import com.hrms.web.form.PayOTForm;
import com.hrms.web.form.PayPFForm;
import com.hrms.web.form.PayPaySlipReportForm;
import com.hrms.web.form.PayRembOTForm;
import com.hrms.web.form.PaySalaryReportForm;
import com.hrms.web.form.PaySalarySplitForm;
import com.hrms.web.form.PayrollSummaryForm;
import com.hrms.web.form.TimeAbsentEmployeesForm;
import com.hrms.web.form.TimeEmpLeaveSummaryForm;
import com.hrms.web.form.TimeEmpTimesheetReportForm;
import com.hrms.web.form.TimeEmpWorkReportForm;
import com.hrms.web.form.TimeWorkSheetReportForm;
import com.hrms.web.form.TimeWorkShiftForm;
import com.hrms.web.service.AdjustmentService;
import com.hrms.web.service.AdvanceSalaryService;
import com.hrms.web.service.AssignmentService;
import com.hrms.web.service.AttendanceService;
import com.hrms.web.service.BonusService;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.CommissionService;
import com.hrms.web.service.DeductionService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeCategoryService;
import com.hrms.web.service.EmployeeDesignationService;
import com.hrms.web.service.EmployeeJoiningService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.EmployeeStatusService;
import com.hrms.web.service.EmployeeTypeService;
import com.hrms.web.service.EsiService;
import com.hrms.web.service.EsiSyntaxService;
import com.hrms.web.service.ExtraPaySlipItemService;
import com.hrms.web.service.HolidayService;
import com.hrms.web.service.JobCandidateService;
import com.hrms.web.service.JobInterviewService;
import com.hrms.web.service.JobPostService;
import com.hrms.web.service.LeaveService;
import com.hrms.web.service.LoanService;
import com.hrms.web.service.LwfService;
import com.hrms.web.service.OrganisationService;
import com.hrms.web.service.OvertimeService;
import com.hrms.web.service.PayRollOptionService;
import com.hrms.web.service.PaySalaryService;
import com.hrms.web.service.PaySlipItemService;
import com.hrms.web.service.PayslipService;
import com.hrms.web.service.PerformanceEvaluationInfoService;
import com.hrms.web.service.ProfesionalTaxService;
import com.hrms.web.service.ProfidentFundService;
import com.hrms.web.service.ProfidentFundSyntaxService;
import com.hrms.web.service.ProjectService;
import com.hrms.web.service.PromotionService;
import com.hrms.web.service.ReimbursementsService;
import com.hrms.web.service.ReportService;
import com.hrms.web.service.ResignationService;
import com.hrms.web.service.SalaryService;
import com.hrms.web.service.TransferService;
import com.hrms.web.service.TravelService;
import com.hrms.web.service.WarningService;
import com.hrms.web.service.WorkSheetService;
import com.hrms.web.service.WorkShiftDetailsService;
import com.hrms.web.service.WorkShiftService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.PayRollCalculator;
import com.hrms.web.vo.*;

/**
 * 
 * @author Vinutha
 * @since 14-April-2015
 *
 */
@Service
public class ReportServiceImpl implements ReportService {

	@Autowired
	private EmployeeService empService;

	@Autowired
	private EmployeeStatusService empStsService;

	@Autowired
	private EmployeeTypeService empTypeService;

	@Autowired
	private EmployeeCategoryService empCategoryService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService depService;

	@Autowired
	private ProjectService proService;

	@Autowired
	private ReportDao reportDao;

	@Autowired
	private EmployeeDesignationService desService;

	@Autowired
	private HolidayService holidayService;

	@Autowired
	private OrganisationService orgService;

	@Autowired
	private LeaveService leaveService;

	@Autowired
	private JobPostService postService;

	@Autowired
	private JobCandidateService candidateService;

	@Autowired
	private JobInterviewService interviewService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private EmployeeJoiningService joiningService;

	@Autowired
	private AssignmentService assgmntService;

	@Autowired
	private LoanService loanService;

	@Autowired
	private AdvanceSalaryService advSalaryService;

	@Autowired
	private EsiService esiService;

	@Autowired
	private ProfidentFundService pfService;

	@Autowired
	private OvertimeService otService;

	@Autowired
	private ReimbursementsService remService;

	@Autowired
	private AdjustmentService adjstmntsService;

	@Autowired
	private CommissionService commissionService;

	@Autowired
	private BonusService bonusService;

	@Autowired
	private DeductionService deductionService;

	@Autowired
	private SalaryService salaryService;

	@Autowired
	private TransferService transferService;

	@Autowired
	private ResignationService resignService;

	@Autowired
	private TravelService travelService;

	@Autowired
	private AttendanceService attendanceService;

	@Autowired
	private WorkSheetService workService;

	@Autowired
	private WorkShiftService shiftService;

	@Autowired
	private SalaryService completeSalaryService;

	@Autowired
	private AttendanceDao attendanceDao;

	@Autowired
	private ProfidentFundSyntaxService pfSyntaxService;

	@Autowired
	private PaySlipItemService payItemService;

	@Autowired
	private PayRollOptionService payRollOptionService;

	@Autowired
	private PayslipService payslipService;

	@Autowired
	private PromotionService promotionService;

	@Autowired
	private WarningService warningService;

	@Autowired
	private EsiSyntaxService esiSyntaxService;

	@Autowired
	private WorkShiftDetailsService shiftDetailsService;

	@Autowired
	private LwfService lwfService;

	@Autowired
	private ProfesionalTaxService profService;

	@Autowired
	private PerformanceEvaluationInfoService performanceService;

	@Autowired
	private PaySalaryService paySalaryService;

	@Autowired
	private ExtraPaySlipItemService extraPaySlipItemService;

	/**
	 * Method to create employee list for report
	 * 
	 * @param reportVo
	 * @return List<HrEmpReportViewVo>
	 */
	public List<HrEmpReportViewVo> createEmployeeReport(HrEmpReportVo reportVo) {
		List<HrEmpReportViewVo> result = new ArrayList<HrEmpReportViewVo>();
		List<EmployeeVo> employees = empService.listEmployeeCompleteDetails();
		// Check condition
		int count = 0;
		boolean query1 = true;
		boolean query2 = true;
		boolean query3 = true;
		boolean query4 = true;
		boolean query5 = true;
		for (EmployeeVo employee : employees) {
			query1 = true;
			query2 = true;
			query3 = true;
			query4 = true;
			query5 = true;

			if (reportVo.getbId() != 0)
				query1 = employee.getEmployeeBranchId() == reportVo.getbId();
			if (reportVo.getDepId() != 0)
				query2 = employee.getEmployeeDepartmentId() == reportVo.getDepId();
			if (reportVo.getCateId() != 0)
				query3 = employee.getEmployeeCategoryId() == reportVo.getCateId();
			if (reportVo.getTypeId() != 0)
				query4 = employee.getEmployeeTypeId() == reportVo.getTypeId();
			if (reportVo.getStatusId() != 0)
				query5 = employee.getStatusId() == reportVo.getStatusId();

			if (query1 && query2 && query3 && query4 && query5) {
				HrEmpReportViewVo viewVo = new HrEmpReportViewVo();
				viewVo.setBranch(employee.getEmployeeBranch());
				viewVo.setCategory(employee.getEmployeeCategory());
				viewVo.setDepartment(employee.getEmployeeDepartment());
				viewVo.setDesignation(employee.getDesignation());
				viewVo.setEmail(employee.getEmail());
				viewVo.setEmployeeName(employee.getName());
				if (employee.getJoiningDate() != null) {
					viewVo.setJoinDate(DateFormatter.convertStringToDate(employee.getJoiningDate()));
					viewVo.setDateJoin(employee.getJoiningDate());
				}
				viewVo.setType(employee.getEmployeeType());
				viewVo.setSlno(++count);
				viewVo.setUserName(employee.getUserName());
				result.add(viewVo);
			}
		}
		return result;
	}

	/**
	 * Method to create HR Summary view
	 * 
	 * @return HrSummaryViewVo
	 */
	public HrSummaryViewVo createHrSummary() {
		HrSummaryViewVo resultVo = new HrSummaryViewVo();

		List<EmployeeVo> employees = empService.listEmployeeCompleteDetails();
		List<EmployeeTypeVo> employeeTypeVos = empTypeService.findAllEmployeeType();
		List<EmployeeCategoryVo> employeeCategoryVos = empCategoryService.findAllEmployeeCategory();

		HrSummaryVo summaryVo = new HrSummaryVo();
		summaryVo.setBranchNo(branchService.getbranchCount());
		summaryVo.setDepNo(reportDao.deparmentCount());
		summaryVo.setProjectNo(reportDao.projectCount());

		summaryVo.setTotalEmp(employees.size());
		setMiscFields(employees, summaryVo);

		List<EmpTypeStatusVo> typeStsVos = new ArrayList<EmpTypeStatusVo>();

		for (EmployeeTypeVo empTypeVo : employeeTypeVos) {
			EmpTypeStatusVo typeStsVo = new EmpTypeStatusVo();
			typeStsVo.setType(empTypeVo.getEmployeeType());
			typeStsVo.setNumber(getTypeCount(employees, empTypeVo.getEmployeeTypeId()));
			typeStsVos.add(typeStsVo);
		}
		summaryVo.setEmpType(typeStsVos);
		List<EmpCategoryStatusVo> cateStsVos = new ArrayList<EmpCategoryStatusVo>();

		for (EmployeeCategoryVo empCateVo : employeeCategoryVos) {
			EmpCategoryStatusVo cateStsVo = new EmpCategoryStatusVo();
			cateStsVo.setCategory(empCateVo.getEmployeeCategory());
			cateStsVo.setNumber(getCateCount(employees, empCateVo.getEmployeeCategoryId()));
			cateStsVos.add(cateStsVo);
		}
		summaryVo.setEmpCategory(cateStsVos);
		resultVo.setSummaryDetails(summaryVo);

		resultVo.setBranchDetails(setBranchEmpDetails(employees));
		resultVo.setDepartmentDetails(setDepartmentEmpDetails(employees));
		resultVo.setDesignationDetails(setDesignationEmpDetails(employees));
		resultVo.setAgeGroupDetails(setAgeGroupempDetails(employees));

		return resultVo;
	}

	/**
	 * method to get employee count according to type
	 * 
	 * @param employees
	 * @param id
	 * @return
	 */
	private int getTypeCount(List<EmployeeVo> employees, Long id) {
		int count = 0;
		for (EmployeeVo empVo : employees) {
			if (empVo.getEmployeeTypeId() == id)
				count++;
		}
		return count;
	}

	/**
	 * Method to get employee count according to category
	 * 
	 * @param employees
	 * @param id
	 * @return
	 */
	private int getCateCount(List<EmployeeVo> employees, Long id) {
		int count = 0;
		for (EmployeeVo empVo : employees) {
			if (empVo.getEmployeeCategoryId() == id)
				count++;
		}
		return count;
	}

	/**
	 * MEthod to calculate misc. statistics
	 * 
	 * @param employees
	 * @param summaryVo
	 * @return
	 */
	private HrSummaryVo setMiscFields(List<EmployeeVo> employees, HrSummaryVo summaryVo) {
		int active = 0, inactive = 0, male = 0, female = 0;
		for (EmployeeVo emp : employees) {
			System.out.println(emp.getStatus() + " " + emp.getGender());
			if (emp.getStatus() != null) {
				if (emp.getStatus().trim().equalsIgnoreCase("Active"))
					active++;
				else if (emp.getStatus().trim().equalsIgnoreCase("Inactive"))
					inactive++;
			}
			if (emp.getGender() != null) {
				if (emp.getGender().trim().equalsIgnoreCase("male")) {
					male++;
				} else if (emp.getGender().trim().equalsIgnoreCase("female"))
					female++;
			}
		}
		summaryVo.setActiveEmp(active);
		summaryVo.setInactiveEmp(inactive);
		summaryVo.setMaleEmp(male);
		summaryVo.setFemaleEmp(female);
		return summaryVo;
	}

	/**
	 * Method to get employee per branch number
	 * 
	 * @param employees
	 * @return
	 */
	private List<EmployeeByBranchVo> setBranchEmpDetails(List<EmployeeVo> employees) {
		List<EmployeeByBranchVo> branchDetails = new ArrayList<EmployeeByBranchVo>();
		for (BranchVo branch : branchService.listAllBranches()) {
			EmployeeByBranchVo result = new EmployeeByBranchVo();
			int number = 0;
			for (EmployeeVo emp : employees) {
				if (emp.getEmployeeBranchId() == branch.getBranchId()) {
					number++;
				}
			}
			result.setBranch(branch.getBranchName());
			result.setBranchEmp(number);
			branchDetails.add(result);
		}
		return branchDetails;
	}

	/**
	 * Method to get employees per departments
	 * 
	 * @param employees
	 * @return
	 */
	private List<EmployeeByDepartmentsVo> setDepartmentEmpDetails(List<EmployeeVo> employees) {
		List<EmployeeByDepartmentsVo> depDetails = new ArrayList<EmployeeByDepartmentsVo>();
		for (DepartmentVo dep : depService.listAllDepartments()) {
			EmployeeByDepartmentsVo result = new EmployeeByDepartmentsVo();
			int number = 0;
			for (EmployeeVo emp : employees) {
				if (emp.getEmployeeDepartmentId() == dep.getId()) {
					number++;
				}
			}
			result.setDepartment(dep.getName());
			result.setDepartmentEmp(number);
			depDetails.add(result);
		}
		return depDetails;
	}

	/**
	 * Method to get Designation-Employee number
	 * 
	 * @param employees
	 * @return
	 */
	private List<EmployeeByDesignationsVo> setDesignationEmpDetails(List<EmployeeVo> employees) {
		List<EmployeeByDesignationsVo> desDetails = new ArrayList<EmployeeByDesignationsVo>();
		for (EmployeeDesignation des : desService.listAllDesignations()) {
			EmployeeByDesignationsVo result = new EmployeeByDesignationsVo();
			int number = 0;
			for (EmployeeVo emp : employees) {
				if (emp.getDesignationId() == des.getId()) {
					number++;
				}
			}
			result.setDesignation(des.getDesignation());
			result.setDesignationEmp(number);
			desDetails.add(result);
		}
		return desDetails;
	}

	/**
	 * Method to set age group - employee number
	 * 
	 * @param employees
	 * @return
	 */
	private List<EmployeeByAgeVo> setAgeGroupempDetails(List<EmployeeVo> employees) {
		List<EmployeeByAgeVo> ageDetails = new ArrayList<EmployeeByAgeVo>();
		EmployeeByAgeVo age30Vo = new EmployeeByAgeVo();
		EmployeeByAgeVo age40Vo = new EmployeeByAgeVo();
		EmployeeByAgeVo age50Vo = new EmployeeByAgeVo();
		EmployeeByAgeVo age51Vo = new EmployeeByAgeVo();
		EmployeeByAgeVo age19Vo = new EmployeeByAgeVo();
		int age30 = 0;
		int age40 = 0;
		int age50 = 0;
		int age51 = 0;
		int age19 = 0;
		for (EmployeeVo emp : employees) {
			if (emp.getDateOfBirth() != null) {
				if (getEmployeeAge(emp.getDateOfBirth()) >= 20 && getEmployeeAge(emp.getDateOfBirth()) <= 30) {
					age30++;
				} else if (getEmployeeAge(emp.getDateOfBirth()) >= 31 && getEmployeeAge(emp.getDateOfBirth()) <= 40) {
					age40++;
				} else if (getEmployeeAge(emp.getDateOfBirth()) >= 41 && getEmployeeAge(emp.getDateOfBirth()) <= 50) {
					age50++;
				} else if (getEmployeeAge(emp.getDateOfBirth()) >= 51) {
					age51++;
				} else if (getEmployeeAge(emp.getDateOfBirth()) < 20) {
					age19++;
				}
			}
		}
		age19Vo.setAgeGroup("Below 20");
		age19Vo.setAgeEmp(age19);

		age30Vo.setAgeGroup("20 - 30");
		age30Vo.setAgeEmp(age30);

		age40Vo.setAgeGroup("31 - 40");
		age40Vo.setAgeEmp(age40);

		age50Vo.setAgeGroup("41 - 50");
		age50Vo.setAgeEmp(age50);

		age51Vo.setAgeGroup("Above 50");
		age51Vo.setAgeEmp(age51);

		ageDetails.add(age19Vo);
		ageDetails.add(age30Vo);
		ageDetails.add(age40Vo);
		ageDetails.add(age50Vo);
		ageDetails.add(age51Vo);

		return ageDetails;
	}

	/**
	 * Method to calculate age
	 * 
	 * @param dob
	 * @return
	 */
	private int getEmployeeAge(String dob) {
		int age = 0;
		Date bdate = DateFormatter.convertStringToDate(dob);

		Calendar bday = Calendar.getInstance();
		bday.setTime(bdate);
		Calendar today = Calendar.getInstance();
		age = today.get(Calendar.YEAR) - bday.get(Calendar.YEAR);
		if (today.get(Calendar.MONTH) < bday.get(Calendar.MONTH)) {
			age--;
		} else if (today.get(Calendar.MONTH) == bday.get(Calendar.MONTH)
				&& today.get(Calendar.DAY_OF_MONTH) < bday.get(Calendar.DAY_OF_MONTH)) {
			age--;
		}
		return age;
	}

	/**
	 * Method to create payroll report
	 * 
	 * @return PayrollReportViewVo
	 */
	public PayrollSummaryForm createPayrollReport() {
		PayrollSummaryForm resultVo = new PayrollSummaryForm();
		PayrollSummaryVo summaryVo = new PayrollSummaryVo();
		// summaryVo.setTotalSalary(100000);
		// summaryVo.setYearSalary(100000);
		// summaryVo.setAvgSalary(25000);
		// summaryVo.set
		resultVo.setSummary(summaryVo);
		return resultVo;
	}

	/**
	 * Method to create holidays report
	 * 
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public HrHolidaysViewVo createHolidaysReport(HrHolidayVo holidayVo) {
		HrHolidaysViewVo resultVo = new HrHolidaysViewVo();
		List<HrHolidaysDetailsVo> detailsVo = new ArrayList<HrHolidaysDetailsVo>();
		List<HolidayVo> holidays = new ArrayList<HolidayVo>();
		Branch branch = new Branch();
		branch.setId(holidayVo.getBranchId());
		Date startDate = DateFormatter.convertStringToDate(holidayVo.getStartDate());
		Date endDate = DateFormatter.convertStringToDate(holidayVo.getEndDate());
		if (holidayVo.getBranchId() != 0)
			holidays = reportDao.findHolidaysForBranchWithinDateRange(branch, startDate, endDate);
		else
			holidays = reportDao.findHolidayBetweenDate(startDate, endDate);
		int mCount = -1;
		int yCount = -1;
		int check = 0;
		int count = 0;
		for (HolidayVo holiday : holidays) {
			int intMonth = DateFormatter.convertStringToDate(holiday.getStartDate()).getMonth();
			int intYear = DateFormatter.convertStringToDate(holiday.getStartDate()).getYear();
			if (intMonth != mCount) {
				check = 1;
			} else if (yCount != -1 && yCount != intYear) {
				check = 1;
			}
			if (check == 1) {
				HrHolidaysDetailsVo hrHolidayVo = new HrHolidaysDetailsVo();
				switch (intMonth) {
				case 0:
					hrHolidayVo.setMonth("January");

					break;
				case 1:
					hrHolidayVo.setMonth("February");

					break;
				case 2:
					hrHolidayVo.setMonth("March");

					break;
				case 3:
					hrHolidayVo.setMonth("April");

					break;
				case 4:
					hrHolidayVo.setMonth("May");

					break;
				case 5:
					hrHolidayVo.setMonth("June");

					break;
				case 6:
					hrHolidayVo.setMonth("July");

					break;
				case 7:
					hrHolidayVo.setMonth("August");

					break;
				case 8:
					hrHolidayVo.setMonth("September");

					break;

				case 9:
					hrHolidayVo.setMonth("October");

					break;
				case 10:
					hrHolidayVo.setMonth("November");

					break;
				case 11:
					hrHolidayVo.setMonth("December");

					break;

				default:
					break;
				}
				detailsVo.add(hrHolidayVo);
				count = 0;
			}
			check = 0;
			HrHolidaysDetailsVo hrHolidayVo = new HrHolidaysDetailsVo();
			hrHolidayVo.setSlno(++count);
			hrHolidayVo.setTitle(holiday.getTitle());
			hrHolidayVo.setStartDate(DateFormatter.convertStringToDate(holiday.getStartDate()));
			hrHolidayVo.setEndDate(DateFormatter.convertStringToDate(holiday.getEndDate()));
			hrHolidayVo.setBranch(holiday.getBranch());
			hrHolidayVo.setStartDateString(holiday.getStartDate());
			hrHolidayVo.setEndDateString(holiday.getEndDate());
			mCount = intMonth;
			yCount = intYear;
			detailsVo.add(hrHolidayVo);

		}
		resultVo.setDetailsVo(detailsVo);
		return resultVo;
	}

	/**
	 * Method to create Leaves report
	 * 
	 * @param leavesVo
	 * @return
	 */
	public HrLeavesViewVo createLeavesReport(HrLeavesVo leavesVo) {
		HrLeavesViewVo resultVo = new HrLeavesViewVo();
		List<HrLeavesDetailsVo> detailsVo = new ArrayList<HrLeavesDetailsVo>();
		List<LeavesVo> leaves = new ArrayList<LeavesVo>();
		String year = String.valueOf(leavesVo.getYear());
		String startDate = "01/01/" + year;
		String endDate = "31/12/" + year;
		if (leavesVo.getEmpId() != 0) {
			EmployeeVo employee = empService.getEmployeesDetails(leavesVo.getEmpId());
			leaves.addAll(leaveService.findApprovedLeavesBetweenDates(employee.getEmployeeId(), startDate, endDate));
			int resultYear = leavesVo.getYear() - 1900;
			detailsVo = createLeavesDetailsVo(leaves, detailsVo, resultYear);

		} else {
			List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
			if (leavesVo.getBranchId() != 0 && leavesVo.getDepId() != 0) {
				employees = empService.getEmployeesByBranchAndDepartment(leavesVo.getBranchId(), leavesVo.getDepId());
			} else if (leavesVo.getBranchId() != 0 && leavesVo.getDepId() == 0) {
				employees = empService.getEmployeesByBranch(leavesVo.getBranchId());
			} else if (leavesVo.getBranchId() == 0 && leavesVo.getDepId() != 0) {
				employees = empService.getEmployeesByDepartment(leavesVo.getDepId());
			} else if (leavesVo.getBranchId() == 0 && leavesVo.getDepId() == 0) {
				employees = empService.listEmployeeCompleteDetails();
			}
			for (EmployeeVo emp : employees) {
				EmployeeVo employee = empService.getEmployeesDetails(emp.getEmployeeId());
				leaves.addAll(
						leaveService.findApprovedLeavesBetweenDates(employee.getEmployeeId(), startDate, endDate));
			}
			int resultYear = leavesVo.getYear() - 1900;
			Collections.sort(leaves);
			detailsVo = createLeavesDetailsVo(leaves, detailsVo, resultYear);
		}
		resultVo.setDetailsVo(detailsVo);
		return resultVo;

	}

	/**
	 * Method to create leaves report
	 * 
	 * @param employee
	 * @param leaves
	 * @param detailsVo
	 * @param year
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private List<HrLeavesDetailsVo> createLeavesDetailsVo(List<LeavesVo> leaves, List<HrLeavesDetailsVo> detailsVo,
			int year) {
		Set<Integer> months = new HashSet<Integer>();
		Set<Integer> years = new HashSet<Integer>();
		int count = 0;
		int check = 0;
		for (LeavesVo leave : leaves) {
			int intMonth = DateFormatter.convertStringToDate(leave.getFromDate()).getMonth();
			int intYear = DateFormatter.convertStringToDate(leave.getFromDate()).getYear();
			if (intYear == year) {
				if (!months.contains(intMonth))
					check = 1;
			}
			else if (!years.contains(intYear)) {
				check = 1;
			}
			if (check == 1) {
				HrLeavesDetailsVo hrLeaveVo = new HrLeavesDetailsVo();
				switch (intMonth) {
				case 0:
					hrLeaveVo.setMonth("January");

					break;
				case 1:
					hrLeaveVo.setMonth("February");

					break;
				case 2:
					hrLeaveVo.setMonth("March");

					break;
				case 3:
					hrLeaveVo.setMonth("April");

					break;
				case 4:
					hrLeaveVo.setMonth("May");

					break;
				case 5:
					hrLeaveVo.setMonth("June");

					break;
				case 6:
					hrLeaveVo.setMonth("July");

					break;
				case 7:
					hrLeaveVo.setMonth("August");

					break;
				case 8:
					hrLeaveVo.setMonth("September");

					break;

				case 9:
					hrLeaveVo.setMonth("October");

					break;
				case 10:
					hrLeaveVo.setMonth("November");

					break;
				case 11:
					hrLeaveVo.setMonth("December");

					break;

				default:
					break;
				}
				detailsVo.add(hrLeaveVo);
				count = 0;
			}
			check = 0;
			months.add(intMonth);
			years.add(intYear);
			HrLeavesDetailsVo leaveDetail = new HrLeavesDetailsVo();
			leaveDetail.setSlno(++count);
			leaveDetail.setLeaveDate(DateFormatter.convertStringToDate(leave.getFromDate()));
			leaveDetail.setLeaveEndDate(DateFormatter.convertStringToDate(leave.getToDate()));
			leaveDetail.setEmpName(leave.getEmployee());
			leaveDetail.setDesignation(leave.getDesignation());
			leaveDetail.setDepartment(leave.getDepartment());
			leaveDetail.setBranch(leave.getBranch());
			leaveDetail.setLeaveType(leave.getLeaveType());
			leaveDetail.setReason(leave.getReason());
			leaveDetail.setStartDateString(leave.getFromDate());
			leaveDetail.setEndDateString(leave.getToDate());
			detailsVo.add(leaveDetail);

		}
		return detailsVo;
	}

	/**
	 * Method to get years
	 * 
	 * @return
	 */
	public List<Integer> getYears() {
		List<Integer> year = new ArrayList<Integer>();
		OrganisationVo org = orgService.findOrganisationById((long) 1);
		if (org != null) {
			if (org.getOrganisationYear() != null) {
				String strtYear = org.getOrganisationYear();
				int calYear = Integer.parseInt(strtYear);
				Calendar today = Calendar.getInstance();
				int currentYear = today.get(Calendar.YEAR);
				year.add(calYear);
				while (calYear < currentYear) {
					calYear++;
					year.add(calYear);
				}
			}
		}
		return year;
	}

	/**
	 * Method to create posts report
	 * 
	 * @param reportVo
	 * @return
	 */
	public RecPostReportViewVo createPostReport(JobPostReportVo reportVo) {
		RecPostReportViewVo resultVo = new RecPostReportViewVo();
		List<RecPostReportDetailsVo> detailsVo = new ArrayList<RecPostReportDetailsVo>();
		List<JobPostVo> jobPosts = new ArrayList<JobPostVo>();
		if (reportVo.getDepId() != 0)
			jobPosts = postService.findListOfJobPostByDepartmentId(reportVo.getDepId());
		else
			jobPosts = postService.listAllJobPosts();
		int count = 0;
		for (JobPostVo post : jobPosts) {
			RecPostReportDetailsVo postReportVo = new RecPostReportDetailsVo();
			postReportVo.setSlno(++count);
			postReportVo.setJobTitle(post.getJobTitle());
			postReportVo.setJobType(post.getJobType());
			postReportVo.setPositions(Integer.parseInt(post.getPositions()));
			postReportVo.setAgeRange(post.getAgeStart() + " - " + post.getAgeEnd());
			postReportVo.setQualification(post.getQualification());
			postReportVo.setExperience(post.getExperienceYears() + " To " + post.getExperienceMonths() + " Years");
			postReportVo.setSalaryRange(post.getStartSalary() + " - " + post.getEndSalary());
			Date date = DateFormatter.convertStringToDate(post.getJobPostEndDate());

			postReportVo.setClosingDate(date);
			postReportVo.setClosingDateString(post.getJobPostEndDate());
			detailsVo.add(postReportVo);

		}
		resultVo.setDetailsVo(detailsVo);
		return resultVo;
	}

	/**
	 * Method to create job candidate report
	 * 
	 * @param reportVo
	 * @return
	 */
	public RecCandReportViewVo createCandReport(JobCandidateReportVo reportVo) {
		RecCandReportViewVo resultVo = new RecCandReportViewVo();
		List<RecCandidatesReportDetailsVo> detailsVo = new ArrayList<RecCandidatesReportDetailsVo>();
		List<JobCandidatesVo> candidates = new ArrayList<JobCandidatesVo>();
		if (reportVo.getDesignation() != "" && reportVo.getGender() != "")
			candidates = candidateService.findCandidtesByPostAndGender(reportVo.getDesignation(), reportVo.getGender());
		else if (reportVo.getDesignation() != "" && reportVo.getGender() == "")
			candidates = candidateService.findCandidtesByPost(reportVo.getDesignation());
		else if (reportVo.getDesignation() == "" && reportVo.getGender() != "") {
			candidates = candidateService.findCandidtesByGender(reportVo.getGender());
		} else {
			candidates = candidateService.listAllCandidatesForReport();
		}
		int count = 0;
		for (JobCandidatesVo candidate : candidates) {
			RecCandidatesReportDetailsVo candVo = new RecCandidatesReportDetailsVo();
			candVo.setSlno(++count);
			candVo.setJobField(candidate.getJobField());
			candVo.setfName(candidate.getFirstName());
			candVo.setlName(candidate.getLastName());
			candVo.seteMail(candidate.geteMail());
			candVo.setDob(DateFormatter.convertStringToDate(candidate.getBirthDate()));
			candVo.setGender(candidate.getGender());
			candVo.setNationality(candidate.getNationality());
			candVo.setAddress(candidate.getAddress());
			candVo.setCity(candidate.getCity());
			candVo.setState(candidate.getState());
			candVo.setPhone(candidate.getPhoneNumber());
			candVo.setMobile(candidate.getMobileNumber());
			candVo.setDobString(candidate.getBirthDate());
			detailsVo.add(candVo);
		}
		resultVo.setDetailsVo(detailsVo);
		return resultVo;
	}

	/**
	 * Method to create interview report
	 * 
	 * @param reportVo
	 * @return
	 */
	public RecInterviewReportViewVo createInterviewReport(JobInterviewReportVo reportVo) {
		RecInterviewReportViewVo resultVo = new RecInterviewReportViewVo();
		List<RecInterviewReportDetailsVo> detailsVo = new ArrayList<RecInterviewReportDetailsVo>();
		List<JobInterviewVo> interviews = new ArrayList<JobInterviewVo>();
		Date startDate = DateFormatter.convertStringToDate(reportVo.getStartDate());
		Date endDate = DateFormatter.convertStringToDate(reportVo.getEndDate());
		if (reportVo.getPostId() != 0 && reportVo.getBranchID() == 0) {
			interviews = interviewService.listInterviewsByJobPostDateRange(reportVo.getPostId(), startDate, endDate);
		} else if (reportVo.getBranchID() != 0 && reportVo.getPostId() == 0) {
			System.out.println("Inside Method>>>>>>>>>>>>>>>>>");
			interviews = interviewService.listInterviewByBranchAndDate(reportVo.getBranchID(), startDate, endDate);
			System.out.println("OutSide Method <<<<<<<<<<<<<<<<<<<<<<<<<<<<<" + interviews.size());
		} else
			interviews = interviewService.listInterviewsByDateRange(startDate, endDate);
		int count = 0;
		for (JobInterviewVo interview : interviews) {
			RecInterviewReportDetailsVo intDetails = new RecInterviewReportDetailsVo();
			intDetails.setSlno(++count);
			intDetails.setJobPost(interview.getJobPost());
			intDetails.setPlace(interview.getPlace());
			// interviewers
			intDetails.setInterviewers(createStringFromSet(interview.getInterviewers()));
			// candidates
			intDetails.setStatuses(createStringFromSet(interview.getStatusses()));
			intDetails.setCandCount(Integer.parseInt(interview.getSelectionNumber()));
			intDetails.setIntTime(DateFormatter.convertStringTimeToSqlTime(interview.getInterviewTime()));
			intDetails.setIntDate(DateFormatter.convertStringToDate(interview.getInterviewDate()));
			intDetails.setIntTimeString(interview.getInterviewTime());
			intDetails.setIntDateString(interview.getInterviewDate());
			detailsVo.add(intDetails);
		}
		resultVo.setDetailsVo(detailsVo);
		return resultVo;
	}

	/**
	 * Method to create string from set of strings
	 * 
	 * @param interviewers
	 * @return
	 */
	private String createStringFromSet(Set<String> interviewers) {
		String interviewer = null;
		for (String intCode : interviewers) {
			if (interviewer == null) {
				System.out.println("NULL");
				interviewer = intCode;
			} else {
				System.out.println("NOT NULL");
				interviewer += " , " + intCode;
			}

		}
		return interviewer;
	}

	/**
	 * Method to create project employee report
	 */
	public HrProEmployeeViewVo createProjectEmployeeReport() {
		HrProEmployeeViewVo resultVo = new HrProEmployeeViewVo();
		List<HrProEmployeeDetailsVo> detailsVo = new ArrayList<HrProEmployeeDetailsVo>();
		List<ProjectVo> projects = projectService.listAllProjects();
		for (ProjectVo project : projects) {
			int projCount = 0;
			for (String emp : project.getEmployeesCodes()) {
				if (projCount == 0) {
					HrProEmployeeDetailsVo detail = new HrProEmployeeDetailsVo();
					detail.setProject(project.getProjectTitle());
					detailsVo.add(detail);
					projCount = 1;
				}
				HrProEmployeeDetailsVo detail = new HrProEmployeeDetailsVo();
				EmployeeVo employee = reportDao.findEmployeeByCode(emp);
				if (employee.getName() != null)
					detail.setEmployee(emp + " " + employee.getName());
				else
					detail.setEmployee(emp);
				detail.setDesignation(employee.getDesignation());
				detailsVo.add(detail);
			}

		}
		resultVo.setDetailsVo(detailsVo);
		return resultVo;
	}

	/**
	 * Method to create joining report
	 * 
	 * @param reportVo
	 */
	public EmpJoinReportForm createJoiningReport(EmpPrimaryReportVo reportVo) {
		EmpJoinReportForm form = new EmpJoinReportForm();
		List<EmpJoiningReportDetailsVo> details = new ArrayList<EmpJoiningReportDetailsVo>();
		List<EmployeeJoiningVo> joinings = new ArrayList<EmployeeJoiningVo>();
		if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0) {
			joinings = joiningService.listJoiningByStartDateEndDate(reportVo.getStartDate(), reportVo.getEndDate());
		} else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0) {
			joinings = joiningService.listJoiningByBranchStartDateEndDate(reportVo.getBranchId(),
					reportVo.getStartDate(), reportVo.getEndDate());
		} else if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0) {
			joinings = joiningService.listJoiningByBranchDepartmentStartDateEndDate(reportVo.getBranchId(),
					reportVo.getDepId(), reportVo.getStartDate(), reportVo.getEndDate());
		} else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0) {
			joinings = joiningService.listJoiningByDepartmentStartDateEndDate(reportVo.getDepId(),
					reportVo.getStartDate(), reportVo.getEndDate());
		}
		int count = 0;
		for (EmployeeJoiningVo joining : joinings) {
			EmpJoiningReportDetailsVo detail = new EmpJoiningReportDetailsVo();
			detail.setSlno(++count);
			detail.setEmpName(joining.getEmployee());
			detail.setJoiningDate(DateFormatter.convertStringToDate(joining.getJoiningDate()));
			detail.setJoinDepartment(joining.getDepartmentName());
			detail.setJoinBranch(joining.getBranchName());
			detail.setCurrentDepartment(joining.getCurrentDepartment());
			detail.setCurrentBranch(joining.getCurrentBranch());
			detail.setJoiningDateString(joining.getJoiningDate());
			details.add(detail);
		}
		form.setDetails(details);

		return form;
	}

	/**
	 * Method to create expiration report
	 * 
	 * 
	 */
	public EmpExpirationReportForm createExpirationReport() {
		EmpExpirationReportForm form = new EmpExpirationReportForm();
		// List<EmpExpReportDetailsVo> details =new
		// ArrayList<EmpExpReportDetailsVo>();
		return form;
	}

	/**
	 * Method to create project-employee report using project id
	 * 
	 * @param projectId
	 * @return HrProEmployeeViewVo
	 */
	public HrProEmployeeViewVo createProjectEmployeeReportWithId(Long projectId) {
		HrProEmployeeViewVo resultVo = new HrProEmployeeViewVo();
		List<HrProEmployeeDetailsVo> detailsVo = new ArrayList<HrProEmployeeDetailsVo>();
		ProjectVo project = projectService.findProjectById(projectId);
		int projCount = 0;
		for (String emp : project.getEmployeesCodes()) {
			if (projCount == 0) {
				HrProEmployeeDetailsVo detail = new HrProEmployeeDetailsVo();
				detail.setProject(project.getProjectTitle());
				detailsVo.add(detail);
				projCount = 1;
			}
			HrProEmployeeDetailsVo detail = new HrProEmployeeDetailsVo();
			EmployeeVo employee = reportDao.findEmployeeByCode(emp);
			if (employee.getName() != null)
				detail.setEmployee(emp + " " + employee.getName());
			else
				detail.setEmployee(emp);
			detail.setDesignation(employee.getDesignation());
			detailsVo.add(detail);
		}

		resultVo.setDetailsVo(detailsVo);
		return resultVo;
	}

	/**
	 * Method to create employee transfers report
	 * 
	 * @param reportVo
	 * @return
	 */
	public EmpTransfersReportForm createTransfersReport(EmpPrimaryReportVo reportVo) {

		EmpTransfersReportForm form = new EmpTransfersReportForm();
		List<EmpTransferDetailsVo> details = new ArrayList<EmpTransferDetailsVo>();
		List<TransferVo> transfers = new ArrayList<TransferVo>();

		if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0) {
			transfers = transferService.listTransferByBranchDepartmetnAndBetweenDates(reportVo.getBranchId(),
					reportVo.getDepId(), reportVo.getStartDate(), reportVo.getEndDate());
		} else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0) {
			transfers = transferService.listTransferBetweenDatesAndDepartment(reportVo.getStartDate(),
					reportVo.getEndDate(), reportVo.getDepId());
		} else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0) {
			transfers = transferService.listTransferBetweenDatesAndBranch(reportVo.getStartDate(),
					reportVo.getEndDate(), reportVo.getBranchId());
		} else {
			transfers = transferService.listTransferBetweenDates(reportVo.getStartDate(), reportVo.getEndDate());
		}
		int count = 0;
		for (TransferVo transfer : transfers) {
			EmpTransferDetailsVo detail = new EmpTransferDetailsVo();
			detail.setSlno(++count);
			detail.setEmpName(transfer.getEmployee());
			detail.setTransferTo(transfer.getBranch());
			detail.setDateString(transfer.getDate());
			detail.setTransferDate(DateFormatter.convertStringToDate(transfer.getDate()));
			details.add(detail);
		}
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create employee assigments report
	 * 
	 * @param reportVo
	 * @return
	 */
	public EmpAssigmentsReportForm createEmpAssgmntsReport(EmpPrimaryReportVo reportVo) {
		EmpAssigmentsReportForm form = new EmpAssigmentsReportForm();
		List<EmpAssgmentReportDetailsVo> details = new ArrayList<EmpAssgmentReportDetailsVo>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<AssignmentVo> assignments = new ArrayList<AssignmentVo>();
		if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
			employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
		else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
			employees = empService.getEmployeesByDepartment(reportVo.getDepId());
		else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
			employees = empService.getEmployeesByBranch(reportVo.getBranchId());
		else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
			employees = empService.listAllEmployee();
		for (EmployeeVo emp : employees) {
			assignments.addAll(assgmntService.listAssignmentByEmployee(emp.getEmployeeId()));
		}
		int count = 0;
		for (AssignmentVo assignment : assignments) {
			EmpAssgmentReportDetailsVo detail = new EmpAssgmentReportDetailsVo();
			detail.setSlno(++count);
			detail.setProject(assignment.getProjectTitle());
			detail.setAssigmnt(assignment.getAssignmentName());
			detail.setAssignedTo(assignment.getEmployee());
			detail.setAssignedBy(assignment.getCreatedBy());
			detail.setStartDate(DateFormatter.convertStringToDate(assignment.getStartDate()));
			detail.setEndDate(DateFormatter.convertStringToDate(assignment.getEndDate()));
			detail.setStartDateString(assignment.getStartDate());
			detail.setEndDateString(assignment.getEndDate());
			details.add(detail);
		}
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create loans report
	 * 
	 * @param reportVo
	 * @return
	 */
	public PayLoansForm createLoansReport(PayPrimaryReportVo reportVo) {
		PayLoansForm form = new PayLoansForm();
		List<PayLoansDetailsVo> details = new ArrayList<PayLoansDetailsVo>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<LoanVo> loans = new ArrayList<LoanVo>();
		Date startDate = DateFormatter.createStartDateFromMonthYear(reportVo.getStartMonth(), reportVo.getStartYear());
		Date endDate = DateFormatter.createEndDateFromMonthYear(reportVo.getEndMonth(), reportVo.getEndYear());
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();
		}
		int check = 0;
		int count = 0;
		for (EmployeeVo employee : employees) {

			check = 0;
			EmployeeVo employ = empService.getEmployeeByCode(employee.getEmployeeCode());
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employ.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employ.getEmployeeCategoryId()) {
					check = 1;
				}
			}
			if (check != 1) {
				loans = loanService.findLoanByStatusAndDates(employee.getEmployeeCode(), StatusConstants.APPROVED,
						startDate, endDate);
				for (LoanVo loan : loans) {
					PayLoansDetailsVo detail = new PayLoansDetailsVo();
					detail.setSlno(++count);
					detail.setEmpName(loan.getEmployee());
					detail.setTitle(loan.getLoanTitle());
					detail.setLoanDate(DateFormatter.convertStringToDate(loan.getLoanDate()));
					detail.setLoanDateString(loan.getLoanDate());
					Date repayDate = DateFormatter.convertStringToDate(loan.getRepaymentStartDate());
					detail.setRepayStartDateString(loan.getRepaymentStartDate());
					detail.setRepayStartDate(repayDate);
					detail.setLoanAmount(loan.getLoanAmount());
					BigDecimal loanAmount = loan.getLoanAmount();
					BigDecimal repayment = loan.getRepaymentAmount();
					// subject to modification
					/*
					 * BigDecimal repaidAmount = loanAmountRepaid(loanAmount,
					 * repayment, repayDate, endDate);
					 * detail.setRepaidAmount(repaidAmount);
					 * detail.setRemainingAmount
					 * (loanAmount.subtract(repaidAmount));
					 */
					BigDecimal balance = loan.getBalanceAmount();
					detail.setRemainingAmount(balance);
					// detail.setRepaidAmount(detail.getRemainingAmount().subtract(balance));
					detail.setRepaidAmount(detail.getLoanAmount().subtract(balance));
					details.add(detail);
				}
			}
		}
		form.setDetails(details);

		return form;
	}

	/**
	 * Method to get repaid amount
	 * 
	 * @param loanAmount
	 * @param repayAmnt
	 * @param loanDate
	 * @param endDate
	 * @return
	 */
	private BigDecimal loanAmountRepaid(BigDecimal loanAmount, BigDecimal repayAmnt, Date loanDate, Date endDate) {
		BigDecimal result = new BigDecimal("0");
		Calendar endCal = Calendar.getInstance();
		endCal.setTime(endDate);
		Calendar loanCal = Calendar.getInstance();
		loanCal.setTime(loanDate);
		int months = endCal.get(Calendar.MONTH) - loanCal.get(Calendar.MONTH);
		result = repayAmnt.multiply(new BigDecimal(months));
		if (result.compareTo(loanAmount) == -1 || result.compareTo(loanAmount) == 0)
			return result;
		else
			return loanAmount;
	}

	/**
	 * Method to create advance salary report
	 * 
	 * @param reportVo
	 * @return
	 */
	public PayAdvSalaryForm createAdvanceSalaryReport(PayPrimaryReportVo reportVo) {
		PayAdvSalaryForm form = new PayAdvSalaryForm();
		List<PayAdvSalaryDetailsVo> details = new ArrayList<PayAdvSalaryDetailsVo>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<AdvanceSalaryVo> advanceSalary = new ArrayList<AdvanceSalaryVo>();
		Date startDate = DateFormatter.createStartDateFromMonthYear(reportVo.getStartMonth(),
				String.valueOf(reportVo.getStartYear()));
		Date endDate = DateFormatter.createEndDateFromMonthYear(reportVo.getEndMonth(),
				String.valueOf(reportVo.getEndYear()));
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();
		}
		int count = 0;
		int check = 0;
		for (EmployeeVo employee : employees) {

			check = 0;
			EmployeeVo employ = empService.getEmployeeByCode(employee.getEmployeeCode());
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employ.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employ.getEmployeeCategoryId()) {
					check = 1;
				}
			}
			if (check != 1) {
				advanceSalary = advSalaryService.findAdvanceSalaryByStatusAndDates(employee.getEmployeeCode(),
						StatusConstants.APPROVED, startDate, endDate);
				for (AdvanceSalaryVo advSal : advanceSalary) {
					PayAdvSalaryDetailsVo detail = new PayAdvSalaryDetailsVo();
					detail.setSlno(++count);
					detail.setEmpName(advSal.getEmployee());
					detail.setTitle(advSal.getAdvanceSalaryTitle());
					detail.setAmount(advSal.getAdvanceAmount());
					detail.setRemainingAmount(advSal.getBalance());
					if (advSal.getBalance() != null)
						detail.setRepaidAmount(detail.getAmount().subtract(advSal.getBalance()));
					else
						detail.setRepaidAmount(new BigDecimal("0.00"));
					if (advSal.getRepaymentStartDate() != null) {
						detail.setRepayDate(DateFormatter.convertStringToDate(advSal.getRepaymentStartDate()));
						detail.setRepayDateString(advSal.getRepaymentStartDate());
					}
					if (advSal.getRequestedDate() != null) {
						detail.setSalaryDate(DateFormatter.convertStringToDate(advSal.getRequestedDate()));
						detail.setSalaryDateString(advSal.getRequestedDate());
					}
					details.add(detail);
				}
			}
			form.setDetails(details);
		}

		return form;
	}

	/**
	 * Method to create ESIC report
	 * 
	 * @param reportVo
	 * @return
	 */

	public PayEsciForm createESCIReport(PayPrimaryReportVo reportVo) {
		PayEsciForm form = new PayEsciForm();
		List<PayEsiDetailsVo> details = new ArrayList<PayEsiDetailsVo>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<PaySalaryEmployeeItemsVo> paySlipVos = new ArrayList<PaySalaryEmployeeItemsVo>();
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();

		}
		int check = 0;
		int count = 0;
		BigDecimal totalEmpShare = new BigDecimal("0.00");
		BigDecimal totalOrgShare = new BigDecimal("0.00");
		BigDecimal total = new BigDecimal("0.00");
		String start = "01/" + reportVo.getStartMonth() + "/" + reportVo.getStartYear();
		String end = DateFormatter.getEndDateFromMonthAndYear(reportVo.getEndMonth(), reportVo.getEndYear());

		List<EmployeeBankAccountVo> banckAcs = new ArrayList<EmployeeBankAccountVo>();

		for (EmployeeVo employee : employees) {
			EmployeeVo employ = empService.getEmployeeByCode(employee.getEmployeeCode());

			check = 0;
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employ.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employ.getEmployeeCategoryId()) {
					check = 1;
				}
			}
			if (check != 1) {
				SalaryMonthlyVo monthlySalary = salaryService.getMonthlySalaryByEmployee(employ.getEmployeeCode());
				if (monthlySalary != null) {
					paySlipVos = payslipService.getPayslipDetailsByEmployee(employ.getEmployeeCode(), start, end);
					PayEsiDetailsVo detail = new PayEsiDetailsVo();
					banckAcs = employ.getBankAccountVo();
					if (banckAcs != null) {
						for (EmployeeBankAccountVo ac : banckAcs) {
							if (ac.getAccountType().equalsIgnoreCase(AccountTypeConstants.ESI))
								detail.setAcNo(ac.getAccountNumber());
						}
					}
					detail.setSlno(++count);
					detail.setEmpName(employ.getName());
					detail.setDepartment(employ.getEmployeeDepartment());
					detail.setEsciId(employ.getEsiId());
					for (PaySalaryEmployeeItemsVo paySlip : paySlipVos) {
						detail.setEmpContribution(detail.getEmpContribution().add(paySlip.getEsi()));
						detail.setOrgContribution(detail.getOrgContribution().add(paySlip.getEsiEmployer()));
						detail.setGrossSalary(detail.getGrossSalary().add(paySlip.getGrossSalary()));
						detail.setTotal(detail.getEmpContribution().add(detail.getOrgContribution()));

					}
					if (reportDao.listAttendanceObjectByEmployeeAndDate(employee.getEmployeeId(), start, end) != null)
						detail.setDaysPresent(reportDao
								.listAttendanceObjectByEmployeeAndDate(employee.getEmployeeId(), start, end).size());
					totalEmpShare = totalEmpShare.add(detail.getEmpContribution());
					totalOrgShare = totalOrgShare.add(detail.getOrgContribution());
					total = total.add(totalEmpShare.add(totalOrgShare));
					details.add(detail);

				}
			}

		}
		form.setDetails(details);
		form.setTotalEmpContribution(totalEmpShare);
		form.setTotalOrgContribution(totalOrgShare);
		form.setTotalTotal(total);
		return form;
	}

	/**
	 * Method to create PF Reports
	 * 
	 * @param reportVo
	 * @return
	 */

	public PayPFForm createPFReports(PayPrimaryReportVo reportVo) {
		PayPFForm form = new PayPFForm();
		List<PayPFDetails> details = new ArrayList<PayPFDetails>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<PaySalaryEmployeeItemsVo> paySlipVos = new ArrayList<PaySalaryEmployeeItemsVo>();
		String start = "01/" + reportVo.getStartMonth() + "/" + reportVo.getStartYear();
		String end = DateFormatter.getEndDateFromMonthAndYear(reportVo.getEndMonth(), reportVo.getEndYear());
		String bankAccount = "";

		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();

		}
		int check = 0;
		int count = 0;
		BigDecimal totalEmpShare = new BigDecimal("0");
		BigDecimal totalOrgShare = new BigDecimal("0");
		BigDecimal total = new BigDecimal("0");
		List<EmployeeBankAccountVo> banckAcs = new ArrayList<EmployeeBankAccountVo>();
		List<PaySlipItemVo> payItems = payItemService.findAllPaySlipItem();
		for (EmployeeVo employee : employees) {
			check = 0;
			EmployeeVo employ = empService.getEmployeeByCode(employee.getEmployeeCode());
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employ.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employ.getEmployeeCategoryId()) {
					check = 1;
				}
			}
			if (check != 1) {
				SalaryMonthlyVo monthlySalary = salaryService.getMonthlySalaryByEmployee(employ.getEmployeeCode());
				if (monthlySalary != null) {
					banckAcs = employ.getBankAccountVo();
					if (banckAcs != null) {

						for (EmployeeBankAccountVo ac : banckAcs) {
							if (ac.getAccountType().equalsIgnoreCase(AccountTypeConstants.PF))
								bankAccount = ac.getAccountNumber();
						}

					}

					PayPFDetails detail = new PayPFDetails();
					detail.setAcNo(bankAccount);

					paySlipVos = payslipService.getPayslipDetailsByEmployee(employee.getEmployeeCode(), start, end);
					detail.setSlno(++count);
					detail.setEmpName(employee.getName());
					detail.setDepartment(employee.getEmployeeDepartment());
					detail.setBranch(employ.getEmployeeBranch());
					detail.setEmployeeCode(employee.getEmployeeCode());
					detail.setPfId(employ.getPfId());
					BigDecimal basicAmount = new BigDecimal(0);
					for (PaySalaryEmployeeItemsVo paySlip : paySlipVos) {
						basicAmount = paySlip.getItems().get(PayRollConstants.BASIC_SALARY);
						basicAmount = paySlip.getItems().get(PayRollConstants.DA);
						// detail.setBasicWages(detail.getBasicWages().add(paySlip.getBasic()));
						detail.setBasicWages(detail.getBasicWages().add(basicAmount));
						detail.setEmpShare(detail.getEmpShare().add(paySlip.getPf()));
						detail.setOrgShare(detail.getOrgShare().add(paySlip.getPfEmployer()));
						detail.setTotal(detail.getEmpShare().add(detail.getOrgShare()));
					}
					totalEmpShare = totalEmpShare.add(detail.getEmpShare());
					totalOrgShare = totalOrgShare.add(detail.getOrgShare());
					total = total.add(totalOrgShare).add(totalEmpShare);
					details.add(detail);
				}

			}
		}
		form.setTotalEmpShare(totalEmpShare);
		form.setTotalOrgShare(totalOrgShare);
		form.setTotalTotal(total);
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create over time reports
	 * 
	 * @param reportVo
	 * @return
	 */
	public PayOTForm createOTReport(PayPrimaryReportVo reportVo) {
		PayOTForm form = new PayOTForm();
		List<PayOtDetailsVo> details = new ArrayList<PayOtDetailsVo>();
		List<OvertimeVo> ots = new ArrayList<OvertimeVo>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		Date startDate = DateFormatter.createStartDateFromMonthYear(reportVo.getStartMonth(), reportVo.getStartYear());
		Date endDate = DateFormatter.createEndDateFromMonthYear(reportVo.getEndMonth(), reportVo.getEndYear());
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();

		}
		int check = 0;
		int count = 0;
		for (EmployeeVo emp : employees) {
			check = 0;
			EmployeeVo employee = empService.getEmployeeByCode(emp.getEmployeeCode());
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employee.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employee.getEmployeeCategoryId()) {
					check = 1;
				}
			}
			if (check != 1) {

				ots = otService.findAllOvertimeBetweenDatesAndStatus(emp.getEmployeeCode(), "Approved", startDate,
						endDate);
				for (OvertimeVo ot : ots) {
					PayOtDetailsVo detail = new PayOtDetailsVo();
					detail.setSlno(++count);
					detail.setEmpName(ot.getEmployee());
					detail.setInTime(DateFormatter.convertStringTimeToSqlTime(ot.getTimeIn()));
					detail.setOutTime(DateFormatter.convertStringTimeToSqlTime(ot.getTimeOut()));
					detail.setHours(DateFormatter.convertMilliSecondsToHoursAndMinutes(
							(detail.getOutTime().getTime() - detail.getOutTime().getTime())));
					detail.setAmount(ot.getOvertimeAmount());
					detail.setStatus(ot.getStatus());
					detail.setOtIN(ot.getTimeIn());
					detail.setOtOut(ot.getTimeOut());
					details.add(detail);
				}
			}
		}
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create reimbursement reports
	 * 
	 * @param reportVo
	 * @return
	 */
	public PayRembOTForm createReimbursementReport(PayPrimaryReportVo reportVo) {
		PayRembOTForm form = new PayRembOTForm();
		List<PayRembDetailsVo> details = new ArrayList<PayRembDetailsVo>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<ReimbursementsVo> rems = new ArrayList<ReimbursementsVo>();
		Date startDate = DateFormatter.createStartDateFromMonthYear(reportVo.getStartMonth(), reportVo.getStartYear());
		Date endDate = DateFormatter.createEndDateFromMonthYear(reportVo.getEndMonth(), reportVo.getEndYear());

		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();
		}
		/*
		 * if(reportVo.getTypeId()!=0) { empService.get }
		 */
		int check = 0;
		int count = 0;
		BigDecimal totalAmount = new BigDecimal("0");
		for (EmployeeVo em : employees) {
			check = 0;
			EmployeeVo employee = empService.getEmployeeByCode(em.getEmployeeCode());
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employee.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employee.getEmployeeCategoryId()) {
					check = 1;
				}
			}
			if (check != 1) {
				rems = remService.listReimbursementsByEmployeeStartDateEndDate(em.getEmployeeId(), startDate, endDate);
				for (ReimbursementsVo rem : rems) {
					PayRembDetailsVo detail = new PayRembDetailsVo();
					detail.setSlno(++count);
					detail.setDate(DateFormatter.convertStringToDate(rem.getDate()));
					detail.setEmpName(rem.getEmployee());
					detail.setTitle(rem.getTitle());
					detail.setAmount(new BigDecimal(rem.getAmount()));
					totalAmount = totalAmount.add(new BigDecimal(rem.getAmount()));
					details.add(detail);
				}
			}
		}

		form.setDetails(details);
		return form;
	}

	/**
	 * Method to createAdjustments report
	 * 
	 * @param reportVo
	 * @return
	 */
	public PayAdjstmntForm createAdjstmntsReport(PayPrimaryReportVo reportVo) {
		PayAdjstmntForm form = new PayAdjstmntForm();
		List<PayAdjstmntsDetailsVo> details = new ArrayList<PayAdjstmntsDetailsVo>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<AdjustmentVo> adjustments = new ArrayList<AdjustmentVo>();
		Date startDate = DateFormatter.createStartDateFromMonthYear(reportVo.getStartMonth(), reportVo.getStartYear());
		Date endDate = DateFormatter.createEndDateFromMonthYear(reportVo.getEndMonth(), reportVo.getEndYear());
		int check = 0;
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();

		}

		int count = 0;
		BigDecimal totalAmount = new BigDecimal("0");
		for (EmployeeVo emp : employees) {
			check = 0;
			EmployeeVo employee = empService.getEmployeeByCode(emp.getEmployeeCode());
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employee.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employee.getEmployeeCategoryId()) {
					check = 1;
				}
			}
			if (check != 1) {
				adjustments = adjstmntsService.listAdjustmentsByEmployeeStartDateEndDate(emp.getEmployeeId(), startDate,
						endDate);
				for (AdjustmentVo adj : adjustments) {
					PayAdjstmntsDetailsVo detail = new PayAdjstmntsDetailsVo();
					detail.setSlno(++count);
					detail.setEmpName(adj.getEmployee());
					detail.setTitle(adj.getTitle());
					detail.setType(adj.getType());
					detail.setDate(DateFormatter.convertStringToDate(adj.getDate()));
					// detail.setStatus(adj.get)
					detail.setDateString(adj.getDate());
					detail.setAmount(new BigDecimal(adj.getAmount()));
					totalAmount = totalAmount.add(detail.getAmount());
					details.add(detail);
				}
			}
		}
		form.setTotalAmnt(totalAmount);
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create commision report
	 * 
	 * @param reportVo
	 * @return
	 */
	public PayCommisionForm createCommissionsReport(PayPrimaryReportVo reportVo) {
		PayCommisionForm form = new PayCommisionForm();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<PayCommisionsDetailsVo> details = new ArrayList<PayCommisionsDetailsVo>();
		List<CommissionVo> commissions = new ArrayList<CommissionVo>();
		Date startDate = DateFormatter.createStartDateFromMonthYear(reportVo.getStartMonth(), reportVo.getStartYear());
		Date endDate = DateFormatter.createEndDateFromMonthYear(reportVo.getEndMonth(), reportVo.getEndYear());
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();

		}
		int check = 0;
		int count = 0;
		for (EmployeeVo emp : employees) {
			check = 0;
			EmployeeVo employee = empService.getEmployeeByCode(emp.getEmployeeCode());
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employee.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employee.getEmployeeCategoryId()) {
					check = 1;
				}
			}
			if (check != 1) {
				commissions = commissionService.listCommissionesByEmployeeStartDateEndDate(emp.getEmployeeId(),
						startDate, endDate);
				for (CommissionVo com : commissions) {
					PayCommisionsDetailsVo detail = new PayCommisionsDetailsVo();
					detail.setSlno(++count);
					detail.setEmpName(com.getEmployee());
					detail.setTitle(com.getTitle());
					detail.setDate(DateFormatter.convertStringToDate(com.getDate()));
					detail.setDateString(com.getDate());
					detail.setAmount(new BigDecimal(com.getAmount()));
					details.add(detail);
				}
			}
		}
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create Bonus report
	 * 
	 * @param reportVo
	 * @return
	 */
	public PayBonusForm createBonusReport(PayPrimaryReportVo reportVo) {
		PayBonusForm form = new PayBonusForm();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<PayBonusDetailsVo> details = new ArrayList<PayBonusDetailsVo>();
		List<BonusVo> bonuses = new ArrayList<BonusVo>();
		Date startDate = DateFormatter.createStartDateFromMonthYear(reportVo.getStartMonth(), reportVo.getStartYear());
		Date endDate = DateFormatter.createEndDateFromMonthYear(reportVo.getEndMonth(), reportVo.getEndYear());
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();

		}
		int check = 0;
		int count = 0;
		BigDecimal total = new BigDecimal("0");
		for (EmployeeVo emp : employees) {
			check = 0;
			EmployeeVo employee = empService.getEmployeeByCode(emp.getEmployeeCode());
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employee.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employee.getEmployeeCategoryId()) {
					check = 1;
				}
			}
			if (check != 1) {
				bonuses = bonusService.listBonusesByEmployeeStartDateEndDate(emp.getEmployeeId(), startDate, endDate);
				for (BonusVo bonus : bonuses) {
					PayBonusDetailsVo detail = new PayBonusDetailsVo();
					detail.setSlno(++count);
					detail.setEmpName(bonus.getEmployee());
					detail.setTitle(bonus.getTitle());
					detail.setDate(DateFormatter.convertStringToDate(bonus.getDate()));
					detail.setDateString(bonus.getDate());
					detail.setAmount(new BigDecimal(bonus.getAmount()));
					total = total.add(detail.getAmount());
					details.add(detail);
				}
			}
		}
		form.setTotalAmount(total);
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create deduction report
	 * 
	 * @param reportVo
	 * @return
	 */
	public PayDeductionsForm createDeductionsReport(PayPrimaryReportVo reportVo) {
		PayDeductionsForm form = new PayDeductionsForm();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<DeductionVo> deductions = new ArrayList<DeductionVo>();
		List<PayDeductionsDetailsVo> details = new ArrayList<PayDeductionsDetailsVo>();

		Date startDate = DateFormatter.createStartDateFromMonthYear(reportVo.getStartMonth(), reportVo.getStartYear());
		Date endDate = DateFormatter.createEndDateFromMonthYear(reportVo.getEndMonth(), reportVo.getEndYear());
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();

		}
		System.out.println(reportVo.getBranchId());
		int count = 0;
		int check = 0;
		BigDecimal totalAmnt = new BigDecimal("0");
		for (EmployeeVo emp : employees) {
			check = 0;
			EmployeeVo employee = empService.getEmployeeByCode(emp.getEmployeeCode());
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employee.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employee.getEmployeeCategoryId()) {
					check = 1;
				}
			}
			if (check != 1) {
				deductions = deductionService.listDeductionByEmployeeStartDateEndDate(emp.getEmployeeId(), startDate,
						endDate);
				for (DeductionVo deduction : deductions) {
					PayDeductionsDetailsVo detail = new PayDeductionsDetailsVo();
					detail.setSlno(++count);
					detail.setEmpName(deduction.getEmployee());
					detail.setTitle(deduction.getTitle());
					detail.setDescription(deduction.getDescription());
					detail.setAmount(new BigDecimal(deduction.getAmount()));
					totalAmnt = totalAmnt.add(detail.getAmount());
					details.add(detail);
				}
			}
		}
		form.setTotalAmount(totalAmnt);
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create daily wages report
	 * 
	 * @param reportVo
	 * @return
	 */
	public int count = 0;

	public PayDailyWagesForm createDailyWagesReport(PayPrimaryReportVo reportVo) {
		PayDailyWagesForm form = new PayDailyWagesForm();
		List<PayDailyWageDetailsVo> details = new ArrayList<PayDailyWageDetailsVo>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();

		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();

		}
		int check = 0;
		count = 0;
		for (EmployeeVo emp : employees) {
			EmployeeVo employee = empService.getEmployeeByCode(emp.getEmployeeCode());
			check = 0;
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employee.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employee.getEmployeeCategoryId())
					check = 1;
			}
			if (check != 1) {
				details.addAll(getEmployeeDailyWageDetails(emp.getEmployeeId(), emp.getEmployeeCode(), emp.getName(),
						reportVo.getStartDate(), reportVo.getEndDate()));
			}
		}
		form.setDetails(details);
		return form;
	}

	public List<PayDailyWageDetailsVo> getEmployeeDailyWageDetails(Long id, String empCode, String empName,
			String startDate, String endDate) {
		/*
		 * List<Timestamp> attendances = new ArrayList<Timestamp>();
		 * List<PayDailyWageDetailsVo> details = new
		 * ArrayList<PayDailyWageDetailsVo>(); double rate = 0; double
		 * currentRate = 0; int days = 0; int count = 0; attendances =
		 * reportDao.listAttendanceObjectByEmployeeAndDate(id, startDate,
		 * endDate); PayDailyWageDetailsVo detail = new PayDailyWageDetailsVo();
		 * for (Timestamp timestamp : attendances) { if
		 * (reportDao.getDailyWageForDay(empCode, timestamp) != null) {
		 * currentRate =
		 * Double.parseDouble(reportDao.getDailyWageForDay(empCode,
		 * timestamp).getSalary()); if (rate != currentRate) { if
		 * (detail.getEmpName() != null) details.add(detail); detail = new
		 * PayDailyWageDetailsVo(); detail.setSlno(++count);
		 * detail.setDate(timestamp);
		 * detail.setDateString(DateFormatter.convertDateToString(timestamp));
		 * detail.setEmpName(empName); days = 1; detail.setDailyRate(new
		 * BigDecimal(currentRate)); detail.setDays(days); detail.setAmount(new
		 * BigDecimal(detail.getDays()).multiply(detail.getDailyRate())); rate =
		 * currentRate; } else { days++;
		 * detail.setDateString(DateFormatter.convertDateToString(timestamp));
		 * detail.setDays(days); detail.setAmount(new
		 * BigDecimal(detail.getDays()).multiply(detail.getDailyRate())); }
		 * 
		 * } else { } } if (detail.getEmpName() != null) details.add(detail);
		 * return details;
		 */

		List<PayDailyWageDetailsVo> details = new ArrayList<PayDailyWageDetailsVo>();
		List<PaySalaryDailySalaryVo> salaries = paySalaryService.getEmployeeDailyWage(empCode, startDate, endDate);
		BigDecimal amount = new BigDecimal(0);

		if (salaries.size() > 0) {

			for (PaySalaryDailySalaryVo daily : salaries) {
				PayDailyWageDetailsVo detail = new PayDailyWageDetailsVo();
				detail.setEmpName(empName);
				// amount = amount.add(daily.getAmount());
				detail.setDailyRate(daily.getDailyRate());
				detail.setDate(DateFormatter.convertStringToDate(daily.getDailyWageDate()));
				detail.setDateString(daily.getDailyWageDate());
				detail.setAmount(daily.getAmount());

				detail.setDays(salaries.size());
				detail.setSlno(++count);
				details.add(detail);
			}

		}
		return details;
	}

	/**
	 * Method to create hourly wages report
	 * 
	 * @param reportVo
	 * @return
	 */

	public PayHourlyWagesForm createHourlyWagesReport(PayPrimaryReportVo reportVo) {
		PayHourlyWagesForm form = new PayHourlyWagesForm();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<PayHrlyWagesDetailsVo> details = new ArrayList<PayHrlyWagesDetailsVo>();

		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();

		}
		int check = 0;
		count = 0;
		for (EmployeeVo emp : employees) {
			EmployeeVo employee = empService.getEmployeeByCode(emp.getEmployeeCode());
			check = 0;
			if (reportVo.getTypeId() != 0) {
				if (reportVo.getTypeId() != employee.getEmployeeTypeId()) {
					check = 1;
				}
			}
			if (reportVo.getCateId() != 0) {
				if (reportVo.getCateId() != employee.getEmployeeCategoryId())
					check = 1;
			}
			if (check != 1) {

				details.addAll(getEmployeeHourlyWagesDetails(employee, reportVo.getStartDate(), reportVo.getEndDate()));
			}
		}
		form.setDetails(details);
		return form;
	}

	public List<PayHrlyWagesDetailsVo> getEmployeeHourlyWagesDetails(EmployeeVo emp, String startDate, String endDate) {

		List<PayHrlyWagesDetailsVo> details = new ArrayList<PayHrlyWagesDetailsVo>();
		List<PaySalaryHourlySalaryVo> salaries = paySalaryService.getEmployeeHourlyWage(emp.getEmployeeCode(),
				startDate, endDate);
		BigDecimal amount = new BigDecimal(0);
		BigDecimal hours = new BigDecimal(0);
		PayHrlyWagesDetailsVo detail = new PayHrlyWagesDetailsVo();
		if (!salaries.isEmpty()) {

			detail.setEmpName(emp.getName());
			for (PaySalaryHourlySalaryVo hourly : salaries) {
				hours = new BigDecimal(hourly.getTotalHours()).setScale(2, RoundingMode.HALF_UP);
				hours = hours.add(new BigDecimal(detail.getHours())).setScale(2, RoundingMode.HALF_UP);
				amount = amount.add(hourly.getAmount());
				detail.setHourlyRate(hourly.getHourlyRate());
				detail.setDate(DateFormatter.convertStringToDate(hourly.getHourlyWageDate()));
				detail.setDateString(hourly.getHourlyWageDate());
				detail.setHours(hours.doubleValue());
			}

			detail.setAmount(amount);
			// detail.setDays(salaries.size());
			detail.setSlno(++count);
			details.add(detail);
		}
		/*
		 * attendances =
		 * reportDao.listAttendanceObjectByEmployeeAndDate(emp.getEmployeeId(),
		 * startDate, endDate); for (int i = 0; i < attendances.size(); i++) {
		 * 
		 * Date date = (Timestamp) attendances.get(i); PayHrlyWagesDetailsVo
		 * detail = new PayHrlyWagesDetailsVo(); detail.setSlno(++count);
		 * detail.setDate(date);
		 * detail.setDateString(DateFormatter.convertDateToString(date));
		 * detail.setEmpName(emp.getName());
		 * detail.setEmpCode(emp.getEmployeeCode());
		 * 
		 * detail.setHours(calculateHoursWorkedByEmployeeOnDate(emp.
		 * getEmployeeId(), DateFormatter.convertDateToString(date))); if
		 * (salaryService.getHourlyWageForDay(emp.getEmployeeCode(), date) !=
		 * null) { detail.setHourlyRate(new
		 * BigDecimal(salaryService.getHourlyWageForDay(emp.getEmployeeCode(),
		 * date) .getRegularHourSalary())); } BigDecimal dec = new
		 * BigDecimal(detail.getHours()); if (detail.getHourlyRate() != null) {
		 * BigDecimal res = detail.getHourlyRate().multiply(dec);
		 * detail.setAmount(res.setScale(2, RoundingMode.HALF_UP));
		 * details.add(detail); } }
		 */
		return details;
	}

	/**
	 * Method to create resignation report
	 * 
	 * @param reportVo
	 * @return
	 */
	public EmpResignReportForm createResignationReport(EmpPrimaryReportVo reportVo) {

		EmpResignReportForm form = new EmpResignReportForm();
		List<ResignationVo> resignations = new ArrayList<ResignationVo>();
		List<EmpResignationDetailsVo> details = new ArrayList<EmpResignationDetailsVo>();

		if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0) {
			resignations = resignService.findResignationByBranchDepartmentBetweenDates(reportVo.getBranchId(),
					reportVo.getDepId(), reportVo.getStartDate(), reportVo.getEndDate());
		} else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0) {
			resignations = resignService.findResignationByDepartmentBetweenDates(reportVo.getDepId(),
					reportVo.getStartDate(), reportVo.getEndDate());
		} else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0) {
			resignations = resignService.findResignationByBranchBetweenDates(reportVo.getBranchId(),
					reportVo.getStartDate(), reportVo.getEndDate());
		} else {

			resignations = resignService.listResignationBetweenDates(reportVo.getStartDate(), reportVo.getEndDate());
		}
		int count = 0;
		for (ResignationVo resign : resignations) {
			EmpResignationDetailsVo detail = new EmpResignationDetailsVo();
			detail.setSlno(++count);
			detail.setEmpName(resign.getEmployee());
			detail.setNoticeDate(DateFormatter.convertStringToDate(resign.getNoticeDate()));
			detail.setResignDate(DateFormatter.convertStringToDate(resign.getResignationDate()));
			detail.setNoticeDateString(resign.getNoticeDate());
			detail.setResignDateString(resign.getResignationDate());
			details.add(detail);
		}
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create travel report
	 * 
	 * @param reportVo
	 * @return
	 */
	public EmpTravelReportForm createTravelReport(EmpPrimaryReportVo reportVo) {

		EmpTravelReportForm form = new EmpTravelReportForm();
		List<TravelVo> travels = new ArrayList<TravelVo>();
		List<EmpTravelReportDetailsVo> details = new ArrayList<EmpTravelReportDetailsVo>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();

		if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
			employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
		else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
			employees = empService.getEmployeesByDepartment(reportVo.getDepId());
		else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
			employees = empService.getEmployeesByBranch(reportVo.getBranchId());
		else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
			employees = empService.listAllEmployee();

		for (EmployeeVo emp : employees) {
			travels.addAll(travelService.findTravelsByEmployeeBetweenDates(emp.getEmployeeId(), reportVo.getStartDate(),
					reportVo.getEndDate()));
		}
		int count = 0;
		for (TravelVo travel : travels) {
			EmpTravelReportDetailsVo detail = new EmpTravelReportDetailsVo();
			detail.setSlno(++count);
			detail.setEmpName(travel.getEmployee());
			detail.setExpBudget(Double.parseDouble(travel.getExpectedBudget()));
			detail.setActBudget(Double.parseDouble(travel.getActualBudget()));
			detail.setPurpose(travel.getPurposeOfVisit());
			detail.setTravelStartDate(DateFormatter.convertStringToDate(travel.getStartDate()));
			detail.setTravelEndDate(DateFormatter.convertStringToDate(travel.getEndDate()));
			details.add(detail);
		}
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create work sheet report
	 * 
	 * @param reportVo
	 * @return
	 */
	public TimeWorkSheetReportForm createWorkSheetReport(TimeReportPrimaryVo reportVo) {
		TimeWorkSheetReportForm form = new TimeWorkSheetReportForm();
		List<TimeWorkSheetDetailsVo> details = new ArrayList<TimeWorkSheetDetailsVo>();
		List<WorkSheetVo> workSheets = workService.listWorkSheetWithinDateRange(reportVo.getStartDate(),
				reportVo.getEndDate());
		int count = 0;
		for (WorkSheetVo sheet : workSheets) {

			Set<WorksheetTasksVo> tasksVo = sheet.getTasksVo();
			for (WorksheetTasksVo task : tasksVo) {
				TimeWorkSheetDetailsVo detail = new TimeWorkSheetDetailsVo();
				detail.setSlno(++count);
				detail.setEmpName(sheet.getEmployee());
				detail.setDate(DateFormatter.convertStringToDate(sheet.getDate()));
				detail.setDateString(sheet.getDate());
				detail.setProject(task.getProject());
				detail.setTask(task.getTask());
				Time startTime = DateFormatter.convertStringTimeToSqlTime(task.getStartTime());
				Time endTime = DateFormatter.convertStringTimeToSqlTime(task.getEndTime());
				Long millis = endTime.getTime() - startTime.getTime();
				detail.setTotalHours(DateFormatter.convertMilliSecondsToHoursAndMinutes(millis));
				details.add(detail);
			}
		}
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create employee worksheet
	 * 
	 * @param reportVo
	 * @return
	 */
	public TimeEmpWorkReportForm createEmpWorkSheetReport(TimeReportPrimaryVo reportVo) {

		TimeEmpWorkReportForm form = new TimeEmpWorkReportForm();
		List<TimeEmpWorkDetailsVo> details = new ArrayList<TimeEmpWorkDetailsVo>();
		List<WorkSheetVo> workSheetVos = workService.listWorkSheetWithEmployeeCodeWithinDateRange(
				reportVo.getEmployeeCode(), reportVo.getStartDate(), reportVo.getEndDate());
		Long addHours = 0L;
		Long millis = 0L;
		for (WorkSheetVo workSheet : workSheetVos) {

			double hours = 0.00;
			Set<WorksheetTasksVo> tasks = workSheet.getTasksVo();
			for (WorksheetTasksVo task : tasks) {
				TimeEmpWorkDetailsVo detail = new TimeEmpWorkDetailsVo();
				if (workSheet.getDate() != null)
					detail.setDate(DateFormatter.convertStringToDate(workSheet.getDate()));
				detail.setDateString(workSheet.getDate());
				Time startTime = new Time(0);
				Time endTime = new Time(0);
				if (task.getStartTime() != null)
					startTime = DateFormatter.convertStringTimeToSqlTime(task.getStartTime());
				if (task.getEndTime() != null)
					endTime = DateFormatter.convertStringTimeToSqlTime(task.getEndTime());
				detail.setStartTime(startTime);
				detail.setStartTimeString(task.getStartTime());
				detail.setEndTime(endTime);
				detail.setEndTimeString(task.getEndTime());
				detail.setProject(task.getProject());
				detail.setTask(task.getTask());
				millis = endTime.getTime() - startTime.getTime();
				hours = DateFormatter.convertMilliSecondsToHoursAndMinutes(millis);
				addHours += millis;
				detail.setHours(hours);
				details.add(detail);
			}
		}
		form.setDetails(details);
		EmployeeVo employee = empService.getEmployeeByCode(reportVo.getEmployeeCode());
		form.setEmpName(employee.getName());
		form.setDesignation(employee.getDesignation());
		form.setDepartment(employee.getEmployeeDepartment());
		form.setUserName(employee.getUserName());
		form.setTotalHours(DateFormatter.convertMilliSecondsToHoursAndMinutes(addHours));
		return form;
	}

	/**
	 * Method to create employee time sheet report
	 * 
	 * @param reportVo
	 * @return
	 */
	public TimeEmpTimesheetReportForm createEmployeeTimeSheetReport(TimeReportPrimaryVo reportVo) {

		TimeEmpTimesheetReportForm form = new TimeEmpTimesheetReportForm();
		List<TimeEmpTimesheetReportDetailsVo> details = new ArrayList<TimeEmpTimesheetReportDetailsVo>();
		List<AttendanceVo> attendance = new ArrayList<AttendanceVo>();
		EmployeeVo employee = empService.getEmployeeByCode(reportVo.getEmployeeCode());
		Calendar cal = Calendar.getInstance();
		Date start = DateFormatter.convertStringToDate(reportVo.getStartDate());
		Date end = DateFormatter.convertStringToDate(reportVo.getEndDate());
		cal.setTime(start);
		Double totalHours = 0.00;

		Long totalWorkHours = 0L;
		while (!cal.getTime().after(end)) {
			String selectDate = DateFormatter.convertDateToString(cal.getTime());
			// attendance =
			// attendanceService.listAttendanceByEmployeeAndDate(employee.getEmployeeId(),
			// selectDate);
			Time signIn = new Time(0);
			Time signOut = new Time(0);
			AttendanceVo attendanceVo = attendanceService.calculateHoursWorkedByEmployeeOnDate(employee.getEmployeeId(),
					selectDate);

			TimeEmpTimesheetReportDetailsVo detail = new TimeEmpTimesheetReportDetailsVo();
			detail.setSignIn(attendanceVo.getSignIn());
			detail.setSignInString(attendanceVo.getSignInTime());
			detail.setSignOut(attendanceVo.getSignOut());
			detail.setSignOutString(attendanceVo.getSignOutTime());
			if (attendanceVo.getWorkedHoursMilli() != null)
				totalWorkHours += attendanceVo.getWorkedHoursMilli();

			Double timeWorked = null;
			Double breakHours = null;
			try {
				timeWorked = Double.parseDouble(attendanceVo.getTotalWorkedHours());
			} catch (Exception ex) {
				timeWorked = 0.00;
			}
			totalHours += timeWorked;
			try {
				breakHours = Double.parseDouble(attendanceVo.getBreakHours());
			} catch (Exception ex) {
				breakHours = 0.00;
			}
			// totalHours +=
			detail.setTotalHours(timeWorked);
			detail.setBreakHours(breakHours);
			detail.setDate(cal.getTime());
			detail.setDateString(selectDate);
			details.add(detail);
			if (attendance.size() != 0) {

				/*
				 * int size = attendance.size(); TimeEmpTimesheetReportDetailsVo
				 * detail = new TimeEmpTimesheetReportDetailsVo();
				 * detail.setDate(cal.getTime()); if
				 * (attendance.get(0).getSignInTime() != null) signIn =
				 * DateFormatter.convertStringTimeToSqlTime(attendance.get(0).
				 * getSignInTime()); detail.setSignIn(signIn); if
				 * (attendance.get(size - 1).getSignOutTime() != null) signOut =
				 * DateFormatter.convertStringTimeToSqlTime(attendance.get(size
				 * - 1).getSignOutTime()); detail.setSignOut(signOut);
				 * detail.setDateString(selectDate);
				 * detail.setSignInString(attendance.get(0).getSignInTime());
				 * detail.setSignOutString(attendance.get(size -
				 * 1).getSignOutTime());
				 * 
				 * Long diff = 0L;
				 * 
				 * for (int i = 0, k = 0; i < attendance.size(); i++) {
				 * 
				 * 
				 * signOut =
				 * DateFormatter.convertStringTimeToSqlTime(attendance.get(i).
				 * getSignOutTime()); if (k < attendance.size() - 1) { signIn =
				 * DateFormatter.convertStringTimeToSqlTime(attendance.get(++k).
				 * getSignInTime()); diff += (signIn.getTime() -
				 * signOut.getTime()); } } detail.setBreakHours(DateFormatter.
				 * convertMilliSecondsToHoursAndMinutes(diff)); signIn =
				 * DateFormatter.convertStringTimeToSqlTime(attendance.get(0).
				 * getSignInTime()); signOut =
				 * DateFormatter.convertStringTimeToSqlTime(attendance.get(size
				 * - 1).getSignOutTime()); Long diff2 = signOut.getTime() -
				 * signIn.getTime(); //
				 * detail.setTotalHours(Double.parseDouble(attendanceService.
				 * calculateHoursWorkedByEmployeeOnDate(employee.getEmployeeId()
				 * ,selectDate // ).getTotalWorkedHours()));
				 * detail.setTotalHours(DateFormatter.
				 * convertMilliSecondsToHoursAndMinutes(diff2 - diff));
				 * 
				 * totalHours += (diff2 - diff);
				 * 
				 * details.add(detail);
				 */
			}
			cal.add(Calendar.DAY_OF_YEAR, 1);
		}
		form.setDetails(details);
		form.setHoursTotal(DateFormatter.convertMilliSecondsToHoursAndMinutes(totalWorkHours));
		// form.setHoursTotal(totalHours);
		form.setEmpName(employee.getName());
		form.setDesignation(employee.getDesignation());
		form.setDepartment(employee.getEmployeeDepartment());
		form.setUserName(employee.getUserName());
		return form;
	}

	/**
	 * Method to create employee leave summary
	 * 
	 * @param reporVo
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public TimeEmpLeaveSummaryForm createEmployeeLeaveSummary(TimeReportPrimaryVo reportVo) {

		TimeEmpLeaveSummaryForm form = new TimeEmpLeaveSummaryForm();
		List<TimeEmpLeaveDetailsVo> details = new ArrayList<TimeEmpLeaveDetailsVo>();
		List<TimeEmpLeaveSummaryVo> summaries = new ArrayList<TimeEmpLeaveSummaryVo>();
		EmployeeVo employee = empService.getEmployeeByCode(reportVo.getEmployeeCode());
		List<LeavesVo> leaves = new ArrayList<LeavesVo>();
		// leaveService.getLeavesTakenByEmployee(employee.getEmployeeId());
		String yearString = String.valueOf(reportVo.getYear());
		String startDate = "01/01/" + yearString;
		String endDate = "31/12/" + yearString;
		leaves = reportDao.getAllLeavesTakenByEmployee(employee.getEmployeeId(), startDate, endDate);
		List<EmployeeLeaveVo> empLeaves = employee.getLeaveVos();
		HashMap<String, String> leaveType = new HashMap<String, String>();
		HashMap<String, Integer> leavesTaken = new HashMap<String, Integer>();
		HashMap<String, Long> leavesQuota = new HashMap<String, Long>();
		int count = 0;
		for (LeavesVo leave : leaves) {
			TimeEmpLeaveDetailsVo detail = new TimeEmpLeaveDetailsVo();
			detail.setSlno(++count);
			detail.setLeaveType(leave.getLeaveType());
			detail.setReason(leave.getReason());
			detail.setFromDate(DateFormatter.convertStringToDate(leave.getFromDate()));
			detail.setFromDateString(leave.getFromDate());
			detail.setTillDate(DateFormatter.convertStringToDate(leave.getToDate()));
			detail.setTillDateString(leave.getToDate());
			detail.setDays(leave.getDuration());
			detail.setStatus(leave.getStatus());
			details.add(detail);

			if (!leaveType.containsKey(detail.getLeaveType()))
				leaveType.put(detail.getLeaveType(), "");

			if (leavesTaken.containsKey(detail.getLeaveType()))
				leavesTaken.put(detail.getLeaveType(), leavesTaken.get(detail.getLeaveType()) + detail.getDays());
			else
				leavesTaken.put(detail.getLeaveType(), detail.getDays());

		}
		for (EmployeeLeaveVo empLeave : empLeaves) {

			// if (leaveType.containsKey(empLeave.getLeaveTitle()))
			leaveType.put(empLeave.getLeaveTitle(), empLeave.getLeaveType());
			leavesQuota.put(empLeave.getLeaveTitle(), empLeave.getLeaves());

		}

		Iterator it = leaveType.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			System.out.println(pair.getKey() + " = " + pair.getValue());

			TimeEmpLeaveSummaryVo summary = new TimeEmpLeaveSummaryVo();
			summary.setLeaveType(pair.getKey().toString());
			summary.setPayType(pair.getValue().toString());
			summary.setQuota(leavesQuota.get(pair.getKey()));
			if (leavesTaken.containsKey(pair.getKey()))
				summary.setLeavesTaken((leavesTaken.get(pair.getKey())));
			else
				summary.setLeavesTaken(0);
			summary.setRemainingLeaves((int) (summary.getQuota() - summary.getLeavesTaken()));
			summaries.add(summary);
		}
		form.setDetails(details);
		form.setSummary(summaries);
		return form;
	}

	/**
	 * Method to create absent employees report
	 * 
	 * @param reportVo
	 * @return
	 */
	public TimeAbsentEmployeesForm createAbsentEmployeesReport(TimeReportPrimaryVo reportVo) {

		TimeAbsentEmployeesForm form = new TimeAbsentEmployeesForm();
		List<TimeAbsentEmployeesDetailsVo> details = new ArrayList<TimeAbsentEmployeesDetailsVo>();

		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		if (!reportVo.getEmployeeCode().equalsIgnoreCase("none"))
			employees.add(empService.getEmployeeByCode(reportVo.getEmployeeCode()));
		else {
			if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0) {
				employees = empService.listAllEmployee();
			} else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0) {
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			} else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0) {
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			} else {
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			}
		}
		List<AttendanceVo> attendance = new ArrayList<AttendanceVo>();
		Calendar cal = Calendar.getInstance();
		Date start = DateFormatter.convertStringToDate(reportVo.getStartDate());
		Date end = DateFormatter.convertStringToDate(reportVo.getEndDate());
		List<HolidayVo> holidays = new ArrayList<HolidayVo>();
		cal.setTime(start);
		int count = 0;
		while (!cal.getTime().after(end)) {

			holidays = holidayService.findHolidayBetweenDate(cal.getTime(), cal.getTime());

			if (holidays.size() == 0) {
				String selectDate = DateFormatter.convertDateToString(cal.getTime());
				for (EmployeeVo employee : employees) {
					attendance = attendanceService.listAttendanceByEmployeeAndDate(employee.getEmployeeId(),
							selectDate);
					/*
					 * if (employee.getWorkShiftId() != 0) { Long id =
					 * employee.getWorkShiftId(); }
					 */
					if (attendance.size() == 0) {
						Employee employeeObj = empService.findAndReturnEmployeeById(employee.getEmployeeId());
						TimeAbsentEmployeesDetailsVo detail = new TimeAbsentEmployeesDetailsVo();
						detail.setSlno(++count);
						detail.setDate(cal.getTime());
						detail.setDateString(DateFormatter.convertDateToString(cal.getTime()));
						detail.setEmployee(employee.getName());
						detail.setDesignation(employeeObj.getEmployeeDesignation().getDesignation());
						detail.setStatus("Absent");
						details.add(detail);
					}
				}
				cal.add(Calendar.DAY_OF_YEAR, 1);
			}
		}

		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create salary report
	 * 
	 * @param reportVo
	 * @return
	 */
	public PaySalaryReportForm createSalaryReport(PayPrimaryReportVo reportVo) {

		PaySalaryReportForm form = new PaySalaryReportForm();
		List<PaySalaryDetailsVo> details = new ArrayList<PaySalaryDetailsVo>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<SalaryPayslipItemsVo> allowances = new ArrayList<SalaryPayslipItemsVo>();
		HashMap<String, Integer> colHeads = new HashMap<String, Integer>();
		// employees = empService.listAllEmployee();
		BigDecimal totalBasic = new BigDecimal("0");
		BigDecimal totalHra = new BigDecimal("0");
		BigDecimal totalDa = new BigDecimal("0");
		BigDecimal grandTotal = new BigDecimal("0");
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();
		}
		double totalTax = 0.00;
		List<SalaryMonthlyVo> salaryList = new ArrayList<SalaryMonthlyVo>();
		int count = 0;
		int check = 0;
		HashMap<String, BigDecimal> totalAmountMap = new HashMap<String, BigDecimal>();
		for (EmployeeVo emp : employees) {
			EmployeeVo employ = empService.getEmployeeByCode(emp.getEmployeeCode());
			check = 0;
			if (reportVo.getTypeId() != 0) {
				if (employ.getEmployeeTypeId() != reportVo.getTypeId())
					check = 1;
			}
			if (reportVo.getCateId() != 0) {
				if (employ.getEmployeeCategoryId() != reportVo.getCateId())
					check = 1;
			}
			if (check == 0) {
				salaryList = completeSalaryService.getSalariesByEmployee(emp.getEmployeeId());
				int size = salaryList.size();
				if (size > 0) {
					SalaryMonthlyVo salary = salaryList.get(0);
					PaySalaryDetailsVo detail = new PaySalaryDetailsVo();
					detail.setSlno(++count);
					detail.setEmpName(emp.getName());
					detail.setTaxAmount(Double.parseDouble(salary.getMonthlyTaxDeduction()));
					totalTax += detail.getTaxAmount();
					allowances = salary.getItemsVos();
					String itemName = null;
					BigDecimal amount = new BigDecimal("0");
					BigDecimal total = new BigDecimal("0");
					HashMap<String, BigDecimal> itemsAmountMap = new HashMap<String, BigDecimal>();
					for (SalaryPayslipItemsVo item : allowances) {
						itemName = item.getItem();
						amount = item.getAmount();
						total = total.add(amount);
						if (itemsAmountMap.containsKey(itemName)) {
							itemsAmountMap.put(itemName, itemsAmountMap.get(itemName).add(amount));
						} else {
							itemsAmountMap.put(itemName, amount);
						}

						if (!colHeads.containsKey(itemName)) {
							colHeads.put(itemName, 0);
						}
						if (!totalAmountMap.containsKey(itemName)) {
							totalAmountMap.put(itemName, amount);
						} else {
							totalAmountMap.put(itemName, totalAmountMap.get(itemName).add(amount));
						}
					}

					detail.setAllowances(itemsAmountMap);
					detail.setTotalSalary(total);
					details.add(detail);
					grandTotal = grandTotal.add(total);
				}
			}
		}
		form.setTotalAmountMap(totalAmountMap);
		form.setColHeads(colHeads);
		form.setDetails(details);
		form.setTotalBasicsSalary(totalBasic);
		form.setTotalHra(totalHra);
		form.setTotalDa(totalDa);
		form.setTotalTaxAmount(totalTax);
		form.setGrandSalary(grandTotal);
		return form;
	}

	/**
	 * Method to get hours worked on date by an employee
	 * 
	 * @param employeeId
	 * @param date
	 * @return
	 */
	private double calculateHoursWorkedByEmployeeOnDate(Long employeeId, String date) {
		AttendanceVo attendanceVo = new AttendanceVo();
		List<Attendance> attendances = attendanceDao.listAttendanceObjectByEmployeeAndDate(employeeId, date);
		double hoursWorked = 0.00;
		Long millis = null;
		Time inTime = null;
		Time outTime = null;
		for (Attendance attendance : attendances) {
			attendanceVo.setDate(DateFormatter.convertDateToString(attendance.getDate()));
			if (attendance.getEmployee().getUserProfile() != null)
				attendanceVo.setEmployee(attendance.getEmployee().getUserProfile().getFirstName() + " "
						+ attendance.getEmployee().getUserProfile().getLastname());
			attendanceVo.setEmployeeCode(attendance.getEmployee().getEmployeeCode());
			attendanceVo.setEmployeeId(attendance.getEmployee().getId());

			inTime = attendance.getSignIn();
			outTime = attendance.getSignOut();
			if (inTime != null && outTime != null) {
				if (inTime.before(outTime)) {
					if (millis != null)
						millis = millis + (outTime.getTime() - inTime.getTime());
					else
						millis = (outTime.getTime() - inTime.getTime());
				} else {
					if (millis != null)
						millis = millis - (inTime.getTime() - outTime.getTime());
					else
						millis = (inTime.getTime() - outTime.getTime());
				}
			} else {
				hoursWorked = 0.00;
			}
		}
		if (millis != null) {
			hoursWorked = DateFormatter.convertMilliSecondsToHoursAndMinutes(millis);
		}
		return hoursWorked;
	}

	/**
	 * Method to create payslip report
	 * 
	 * @param reportVo
	 * @return
	 */
	public PayPaySlipReportForm createPaySlipReport(PayPrimaryReportVo reportVo) {

		PayPaySlipReportForm form = new PayPaySlipReportForm();
		EmployeeVo employee = empService.getEmployeeByCode(reportVo.getEmployeeCode());
		String startDate = "01/" + reportVo.getStartMonth() + "/" + reportVo.getStartYear();
		// String endDate =
		// DateFormatter.getEndDateFromMonthAndYear(reportVo.getEndMonth(),
		// reportVo.getEndYear());
		String endDate = DateFormatter.getEndDateFromMonthAndYear(reportVo.getStartMonth(), reportVo.getStartYear());
		Date start = DateFormatter.convertStringToDate(startDate);
		Date end = DateFormatter.convertStringToDate(endDate);

		// List<PaysSlipVo> paySlips =
		// payslipService.findPayslipOfEmployeeBetweenDates(employee.getEmployeeId(),
		// start, end);

		List<PayPaySlipDetailsVo> details = new ArrayList<PayPaySlipDetailsVo>();

		LinkedHashMap<String, BigDecimal> earningsAmountMap = new LinkedHashMap<String, BigDecimal>();
		LinkedHashMap<String, BigDecimal> deductionsAmountMap = new LinkedHashMap<String, BigDecimal>();
		form.setEmpName(employee.getName());
		form.setDesignation(employee.getDesignation());
		form.setDepartment(employee.getEmployeeDepartment());
		form.setUserName(employee.getUserName());
		form.setCode(employee.getEmployeeCode());
		form.setBranch(employee.getEmployeeBranch());
		form.setDateOfJoining(employee.getJoiningDate());
		form.setPf(employee.getPfId());
		form.setEsi(employee.getEsiId());
		form.setPan(employee.getPanCadNumber());
		form.setPayMode("Bank");
		List<EmployeeBankAccountVo> banckAcs = new ArrayList<EmployeeBankAccountVo>();
		banckAcs = employee.getBankAccountVo();
		if (banckAcs != null) {
			for (EmployeeBankAccountVo ac : banckAcs) {
				if (ac.getAccountType().equalsIgnoreCase(AccountTypeConstants.SB))
					form.setAccount(ac.getAccountNumber());
				form.setBank(ac.getBankName());
			}

		}
		// if (paySlips.size() > 0) {

		List<PaySalaryEmployeeItemsVo> paySalaries = payslipService.getPayslipDetailsByEmployee(form.getCode(),
				startDate, endDate);
		earningsAmountMap = new LinkedHashMap<String, BigDecimal>();
		deductionsAmountMap = new LinkedHashMap<String, BigDecimal>();
		for (PaySalaryEmployeeItemsVo paySalary : paySalaries) {
			PayPaySlipDetailsVo detail = new PayPaySlipDetailsVo();
			deductionsAmountMap.put("LWF", paySalary.getLwf());
			deductionsAmountMap.put("LOP Deduction", paySalary.getLopDeduction());
			deductionsAmountMap.put("ESI from employee", paySalary.getEsi());
			deductionsAmountMap.put("PF from employee", paySalary.getPf());
			deductionsAmountMap.put("Professional Tax", paySalary.getProfessionalTaxs());
			deductionsAmountMap.put("Advance", paySalary.getAdvanceSalary());
			deductionsAmountMap.put("Loan", paySalary.getLoan());
			deductionsAmountMap.put("TDS", paySalary.getTax());

			earningsAmountMap.put(PayRollConstants.GROSS_SALARY, paySalary.getGrossSalary());
			List<PaySlipItemVo> paysSlipItems = new ArrayList<PaySlipItemVo>();
			List<String> salaryItems = new ArrayList<String>();
			paysSlipItems = payItemService.findAllPaySlipItem();
			for (PaySlipItemVo itemVo : paysSlipItems) {
				salaryItems.add(itemVo.getTitle());
			}
			List<ExtraPaySlipItemVo> extraPaySlipItems = new ArrayList<ExtraPaySlipItemVo>();
			extraPaySlipItems = extraPaySlipItemService.findAllExtraPaySlipItem();
			for (ExtraPaySlipItemVo vo : extraPaySlipItems) {
				salaryItems.add(vo.getTitle());
			}
			Map<String, BigDecimal> salaryMap = paySalary.getItems();
			Map<String, BigDecimal> otherItemsMap = paySalary.getOtherSalaryItems();
			for (String item : salaryItems) {
				if (salaryMap.containsKey(item))
					earningsAmountMap.put(item, salaryMap.get(item));
				else if (otherItemsMap.containsKey(item))
					earningsAmountMap.put(item, otherItemsMap.get(item));
			}
			/*
			 * Map<String, BigDecimal> salaryMap = paySalary.getItems(); for
			 * (Map.Entry<String, BigDecimal> entry : salaryMap.entrySet()) {
			 * earningsAmountMap.put(entry.getKey(), entry.getValue()); } for
			 * (Map.Entry<String, BigDecimal> otherAllowance :
			 * otherItemsMap.entrySet()) {
			 * earningsAmountMap.put(otherAllowance.getKey(),
			 * otherAllowance.getValue()); }
			 */
			detail.setDetail(earningsAmountMap);
			detail.setDeductionDetail(deductionsAmountMap);
			detail.setMonth(paySalary.getDate());
			detail.setTotalEarnings(paySalary.getTotalEarnings());
			detail.setTotalDeductions(paySalary.getTotalDeduction());
			detail.setNetPay(paySalary.getNetPay());
			detail.setCtc(paySalary.getCtc());
			detail.setAdjAmount(paySalary.getPerDayCalculation());
			detail.setLop(paySalary.getLopCount());
			detail.setTotalDays(paySalary.getTotlaWorkingDays());
			details.add(detail);
		}
		// }
		form.setDetails(details);
		return form;

	}

	/**
	 * Method to create salary split report
	 * 
	 * @param reportVo
	 * @return
	 */

	public PaySalarySplitForm createSalarySplitReport(PayPrimaryReportVo reportVo) {
		PaySalarySplitForm form = new PaySalarySplitForm();
		List<PaySalarySplitDetailsVo> details = new ArrayList<PaySalarySplitDetailsVo>();
		List<PaySalaryEmployeeItemsVo> paySlipItems = new ArrayList<PaySalaryEmployeeItemsVo>();
		EmployeeVo employee = new EmployeeVo();
		int allowanceSpan = 0;
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		HashMap<String, BigDecimal> offeredSalary = new HashMap<String, BigDecimal>();
		List<String> allowanceColumns = new ArrayList<String>();

		String startDate = "01/" + reportVo.getStartMonth() + "/" + reportVo.getStartYear();
		String endDate = DateFormatter.getEndDateFromMonthAndYear(reportVo.getEndMonth(), reportVo.getEndYear());
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();
		}
		int count = 0;
		int check = 0;
		for (EmployeeVo emp : employees) {
			employee = empService.getEmployeeByCode(emp.getEmployeeCode());
			check = 0;
			if (reportVo.getTypeId() != 0) {
				if (employee.getEmployeeTypeId() != reportVo.getTypeId())
					check = 1;
			}
			if (reportVo.getCateId() != 0) {
				if (employee.getEmployeeCategoryId() != reportVo.getCateId())
					check = 1;
			}
			if (check != 1) {
				paySlipItems = payslipService.getPayslipDetailsByEmployee(emp.getEmployeeCode(), startDate, endDate);
				for (PaySalaryEmployeeItemsVo paySlipItem : paySlipItems) {
					PaySalarySplitDetailsVo detail = new PaySalarySplitDetailsVo();
					detail.setSlno(++count);
					detail.setEmpCode(paySlipItem.getEmployeeCode());
					detail.setEmpName(paySlipItem.getName());
					detail.setDepartment(paySlipItem.getDepartment());
					detail.setDesignation(paySlipItem.getDesignation());
					detail.setpFApp(paySlipItem.getPfApplicability());
					detail.setEsiApp(paySlipItem.getEsiApplicability());
					detail.setStatus(paySlipItem.getStatus());
					/*
					 * detail.getOfferedSalary().put(PayRollConstants.
					 * BASIC_SALARY , paySlipItem.getBasic());
					 * detail.getOfferedSalary().put(PayRollConstants.DA,
					 * paySlipItem.getDa());
					 * detail.getOfferedSalary().put(PayRollConstants.HRA,
					 * paySlipItem.getHra());
					 * detail.getOfferedSalary().put(PayRollConstants
					 * .CONVEYANCE, paySlipItem.getConveyance());
					 * detail.getOfferedSalary().put(PayRollConstants.CCA,
					 * paySlipItem.getCca());
					 */
					for (Map.Entry<String, BigDecimal> itemAmount : paySlipItem.getItems().entrySet()) {

						detail.getOfferedSalary().put(itemAmount.getKey(), itemAmount.getValue());

					}
					detail.setTotalSalary(paySlipItem.getGrossSalary());
					detail.setLopCount(new BigDecimal(paySlipItem.getLopCount()));
					detail.setLopDeduction(paySlipItem.getLopDeduction());
					detail.setCommission(paySlipItem.getIncentive());
					detail.setBonuses(paySlipItem.getBonus());
					detail.setIncrement(paySlipItem.getIncrement());
					detail.setEncashment(paySlipItem.getEncashment());
					detail.setTotalEarnings(paySlipItem.getTotalEarnings());
					detail.setPf(paySlipItem.getPf());
					detail.setEsi(paySlipItem.getEsi());
					detail.setLwf(paySlipItem.getLwf());
					detail.setProffTax(paySlipItem.getProfessionalTaxs());
					detail.setTax(paySlipItem.getTax());
					detail.setDecrement(paySlipItem.getDeductions());
					// detail.setDeduction(paySlipItem.get);
					detail.setTotalDeduction(paySlipItem.getTotalDeduction());
					detail.setLoan(paySlipItem.getLoan());
					detail.setAdvance(paySlipItem.getAdvanceSalary());
					detail.setNetPay(paySlipItem.getNetPay());
					detail.setPfEmployerContribution(paySlipItem.getPfEmployer());
					detail.setEsiEmployerContribution(paySlipItem.getEsiEmployer());
					detail.setCtc(paySlipItem.getCtc());
					detail.setOfferedSalary(detail.getOfferedSalary());
					details.add(detail);

					/*
					 * for (Map.Entry<String, BigDecimal> det :
					 * detail.getOfferedSalary().entrySet()) { if
					 * (!allowanceColumns.containsKey(det.getKey()))
					 * allowanceColumns.put(det.getKey(), 0); }
					 */

				}

			}
		}
		List<PaySlipItemVo> paysSlips = new ArrayList<PaySlipItemVo>();
		paysSlips = payItemService.findAllPaySlipItem();
		for (PaySlipItemVo itemVo : paysSlips) {
			allowanceColumns.add(itemVo.getTitle());
		}
		List<ExtraPaySlipItemVo> extraPayslipItems = new ArrayList<ExtraPaySlipItemVo>();
		extraPayslipItems = extraPaySlipItemService.findAllExtraPaySlipItem();
		for (ExtraPaySlipItemVo itemVo : extraPayslipItems) {
			allowanceColumns.add(itemVo.getTitle());
		}
		allowanceSpan = allowanceColumns.size();
		form.setAllowancesSpan(allowanceSpan + 1);
		form.setAllowanceColumns(allowanceColumns);
		form.setDetail(details);
		return form;
	}

	/**
	 * Method to create employee summary report
	 * 
	 * @param reportVo
	 * @return
	 */
	public EmpSummaryForm createEmployeeSummaryReport(EmpPrimaryReportVo reportVo) {
		EmpSummaryForm form = new EmpSummaryForm();
		EmployeeVo employee = empService.getEmployeeByCode(reportVo.getEmployeeCode());
		EmpSummaryDetailsVo details = new EmpSummaryDetailsVo();

		List<EmpSummaryTransferDetailVo> transfers = new ArrayList<EmpSummaryTransferDetailVo>();
		List<EmpSummaryLeaveDetailVo> leaves = new ArrayList<EmpSummaryLeaveDetailVo>();

		List<TransferVo> empTransfers = transferService.listTransferByEmployee(reportVo.getEmployeeCode());
		List<LeavesVo> empLeaves = leaveService.getLeavesTakenByEmployee(employee.getEmployeeId());

		details.setEmpCode(reportVo.getEmployeeCode());
		details.setfName(employee.getFirstName());
		details.setlName(employee.getLastName());
		details.setGender(employee.getGender());
		details.setDesignation(employee.getDesignation());
		details.setDepartment(employee.getEmployeeDepartment());
		details.setBranch(employee.getEmployeeBranch());
		details.setEmail(employee.getEmail());
		if (employee.getJoiningDate() != null)
			details.setJoinDate(DateFormatter.convertStringToDate(employee.getJoiningDate()));
		if (employee.getSalaryType() != null)
			details.setSalaryType(employee.getSalaryType());

		form.setDetail(details);
		int count = 0;
		for (TransferVo transfer : empTransfers) {
			EmpSummaryTransferDetailVo detail = new EmpSummaryTransferDetailVo();
			detail.setSlno(++count);
			if (transfer.getDate() != null)
				detail.setTransferDate(DateFormatter.convertStringToDate(transfer.getDate()));
			detail.setTransferDateString(transfer.getDate());
			detail.setFromBranch(transfer.getFromBranch());
			detail.setFromDep(transfer.getFromDepartment());
			detail.setToBranch(transfer.getBranch());
			detail.setToDep(transfer.getDepartment());
			transfers.add(detail);
		}
		form.setTransferDetails(transfers);

		count = 0;
		for (LeavesVo leave : empLeaves) {
			EmpSummaryLeaveDetailVo detail = new EmpSummaryLeaveDetailVo();
			detail.setSlno(++count);
			detail.setLeaveType(leave.getLeaveType());
			detail.setReason(leave.getReason());
			if (leave.getFromDate() != null)
				detail.setFromDate(DateFormatter.convertStringToDate(leave.getFromDate()));
			if (leave.getToDate() != null)
				detail.setToDate(DateFormatter.convertStringToDate(leave.getToDate()));
			detail.setFromDateString(leave.getFromDate());
			detail.setToDateString(leave.getToDate());
			leaves.add(detail);
		}
		form.setLeaveDetails(leaves);
		return form;

	}

	/**
	 * Method to create work shift report
	 * 
	 * @param reportVo
	 * @return
	 */
	public TimeWorkShiftForm createWorkShiftReport(TimeReportPrimaryVo reportVo) {
		TimeWorkShiftForm form = new TimeWorkShiftForm();
		List<TimeEmpWorkshiftDetailsVo> details = new ArrayList<TimeEmpWorkshiftDetailsVo>();
		EmployeeVo employeeVo = empService.getEmployeeByCode(reportVo.getEmployeeCode());
		Employee employee = empService.findAndReturnEmployeeByCode(reportVo.getEmployeeCode());
		form.setEmpName(employeeVo.getName());
		form.setDesignation(employeeVo.getDesignation());
		form.setDepartment(employeeVo.getEmployeeDepartment());
		form.setUserName(employeeVo.getUserName());
		List<WorkShiftDetailsVo> shiftDetails = shiftDetailsService.findWorkShiftDetailsByEmployee(employee);
		for (WorkShiftDetailsVo shift : shiftDetails) {

			TimeEmpWorkshiftDetailsVo detail = new TimeEmpWorkshiftDetailsVo();
			detail.setDay(shift.getShiftDay());
			detail.setRegInString(shift.getRegularWorkFrom());
			if (!shift.getRegularWorkFrom().equalsIgnoreCase("00:00"))
				detail.setRegIn(DateFormatter.convertStringTimeToSqlTime(shift.getRegularWorkFrom()));
			if (!shift.getRegularWorkTo().equalsIgnoreCase("00:00"))
				detail.setRegOut(DateFormatter.convertStringTimeToSqlTime(shift.getRegularWorkTo()));
			if (!shift.getRefreshmentBreakFrom().equalsIgnoreCase("00:00"))
				detail.setRefrIn(DateFormatter.convertStringTimeToSqlTime(shift.getRefreshmentBreakFrom()));
			if (!shift.getRefreshmentBreakTo().equalsIgnoreCase("00:00"))
				detail.setRefrOut(DateFormatter.convertStringTimeToSqlTime(shift.getRefreshmentBreakTo()));
			if (!shift.getAdditionalBreakFrom().equalsIgnoreCase("00:00"))
				detail.setAddIn(DateFormatter.convertStringTimeToSqlTime(shift.getAdditionalBreakFrom()));
			if (!shift.getAdditionalBreakTo().equalsIgnoreCase("00:00"))
				detail.setAddOut(DateFormatter.convertStringTimeToSqlTime(shift.getAdditionalBreakTo()));

			detail.setRegOutString(shift.getRegularWorkTo());

			detail.setRefrInString(shift.getRefreshmentBreakFrom());

			detail.setRefrOutString(shift.getRefreshmentBreakTo());

			detail.setAddInString(shift.getAdditionalBreakFrom());

			detail.setAddOuString(shift.getAdditionalBreakTo());

			details.add(detail);
		}
		form.setDetail(details);
		return form;
	}

	/**
	 * Method to create pay roll report
	 * 
	 * @param reportVo
	 * @return
	 */
	public PayrollSummaryForm createPayrollSummaryReport(PayrollSummaryVo reportVo) {
		PayrollSummaryForm form = new PayrollSummaryForm();
		/*
		 * List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		 * List<BranchVo> branches = branchService.listAllBranches();
		 * List<DepartmentVo> departments = depService.listAllDepartments();
		 * List<EmployeeDesignation> designations =
		 * desService.listAllDesignations(); // Branch Wise Result for (BranchVo
		 * branch : branches) { employees =
		 * empService.getEmployeesByBranch(branch.getBranchId()); for
		 * (EmployeeVo emp : employees) {
		 * 
		 * } } // Department Wise Result for (DepartmentVo dep : departments) {
		 * employees = empService.getEmployeesByDepartment(dep.getId()); for
		 * (EmployeeVo emp : employees) {
		 * 
		 * } } // designation wise result for (EmployeeDesignation des :
		 * designations) {
		 * 
		 * } // yearly result List<Integer> years = getYears(); for (Integer
		 * year : years) {
		 * 
		 * }
		 */
		return form;
	}

	/**
	 * Method to create branch report
	 * 
	 * @param branchVo
	 * @return
	 */
	public HrBranchForm createHrBranchReport(HrBranchVo branchVo) {
		HrBranchForm form = new HrBranchForm();
		List<BranchVo> branches = branchService.findBranchWithinDateRange(branchVo.getStartDate(),
				branchVo.getEndDate());
		List<HrBranchDetailsVo> details = new ArrayList<HrBranchDetailsVo>();
		int count = 0;
		for (BranchVo branchVo2 : branches) {
			HrBranchDetailsVo detail = new HrBranchDetailsVo();
			detail.setSlno(++count);
			detail.setBranchName(branchVo2.getBranchName());
			if (branchVo2.getStartDate() != null)
				detail.setStartDate(DateFormatter.convertStringToDate(branchVo2.getStartDate()));
			detail.setStartDateString(branchVo2.getStartDate());
			detail.setParentBranch(branchVo2.getParentBranch());
			detail.setCity(branchVo2.getCity());
			detail.setPhone(branchVo2.getPhoneNumber());
			details.add(detail);
		}
		form.setDetails(details);
		return form;

	}

	/**
	 * Method to create performance evaluation report
	 * 
	 * @param reportVo
	 * @return
	 */
	public EmpPerformanceForm createPerformanceEvaluatioReport(EmpPrimaryReportVo reportVo) {

		EmpPerformanceForm form = new EmpPerformanceForm();
		List<PerformanceVo> evaluations = performanceService.findEvaluationWithinDateRange(reportVo.getStartDate(),
				reportVo.getEndDate());
		List<EmpPerformanceEvaluationDetailsVo> details = new ArrayList<EmpPerformanceEvaluationDetailsVo>();
		int count = 0;
		for (PerformanceVo performanceVo : evaluations) {
			count = 0;
			EmpPerformanceEvaluationDetailsVo det = new EmpPerformanceEvaluationDetailsVo();
			det.setEvaluation(performanceVo.getTitle() + "  " + performanceVo.getDate());
			details.add(det);
			for (IndividualEvaluationVo individual : performanceVo.getIndividualVos()) {
				EmpPerformanceEvaluationDetailsVo detail = new EmpPerformanceEvaluationDetailsVo();
				detail.setSlno(++count);
				detail.setEmpName(individual.getEmployee());
				detail.setBranch(individual.getBranch());
				detail.setDesignation(individual.getDesignation());
				if (individual.getMaxMark() != null)
					detail.setMaxMark(Double.parseDouble(individual.getMaxMark()));
				if (individual.getScore() != null)
					detail.setScore(Double.parseDouble(individual.getScore()));
				details.add(detail);

			}
		}
		form.setDetails(details);
		return form;
	}

	/**
	 * Method to create bank statement
	 * 
	 * @param vo
	 * @return
	 */
	public BankStatementForm createBankStatement(PayPrimaryReportVo reportVo) {
		BankStatementForm form = new BankStatementForm();
		List<BankStatementDetails> details = new ArrayList<BankStatementDetails>();
		List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
		List<PaySalaryEmployeeItemsVo> paySlipVos = new ArrayList<PaySalaryEmployeeItemsVo>();
		List<EmployeeBankAccountVo> banckAcs = new ArrayList<EmployeeBankAccountVo>();

		String startDate = "01/" + reportVo.getStartMonth() + "/" + reportVo.getStartYear();
		String endDate = DateFormatter.getEndDateFromMonthAndYear(reportVo.getEndMonth(), reportVo.getEndYear());
		if (reportVo.getEmpId() != 0) {
			employees.add(empService.findEmployeeById(reportVo.getEmpId()));
		} else {
			if (reportVo.getBranchId() != 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByBranchAndDepartment(reportVo.getBranchId(), reportVo.getDepId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() != 0)
				employees = empService.getEmployeesByDepartment(reportVo.getDepId());
			else if (reportVo.getBranchId() != 0 && reportVo.getDepId() == 0)
				employees = empService.getEmployeesByBranch(reportVo.getBranchId());
			else if (reportVo.getBranchId() == 0 && reportVo.getDepId() == 0)
				employees = empService.listAllEmployee();
		}
		int count = 0;
		int check = 0;
		String bankAccount = "";
		EmployeeVo employee = new EmployeeVo();
		for (EmployeeVo emp : employees) {

			employee = empService.getEmployeeByCode(emp.getEmployeeCode());
			check = 0;
			if (reportVo.getTypeId() != 0) {
				if (employee.getEmployeeTypeId() != reportVo.getTypeId())
					check = 1;
			}
			if (reportVo.getCateId() != 0) {
				if (employee.getEmployeeCategoryId() != reportVo.getCateId())
					check = 1;
			}
			if (check != 1) {

				paySlipVos = payslipService.getPayslipDetailsByEmployee(employee.getEmployeeCode(), startDate, endDate);
				if (paySlipVos != null) {
					banckAcs = employee.getBankAccountVo();
					if (banckAcs != null) {
						for (EmployeeBankAccountVo ac : banckAcs) {
							if (ac.getAccountType().equalsIgnoreCase(AccountTypeConstants.SB))
								bankAccount = ac.getAccountNumber();
						}

					}

					BankStatementDetails detail = new BankStatementDetails();

					for (PaySalaryEmployeeItemsVo item : paySlipVos) {
						detail.setAmount(detail.getAmount().add(item.getNetPay()));
					}
					if (detail.getAmount().compareTo(BigDecimal.ZERO) != 0)

					{
						detail.setSrNo(++count);
						detail.setEmployee(employee.getName());
						detail.setEmployeeCode(employee.getEmployeeCode());
						detail.setBranch(employee.getEmployeeBranch());
						detail.setAccount(bankAccount);
						details.add(detail);

					}
				}
			}
		}

		form.setDetails(details);
		return form;

	}

	public List<DetailedEmployeeReportVo> createDetailedEmployeeReport(HrEmpReportVo reportVo) {

		List<DetailedEmployeeReportVo> result = new ArrayList<DetailedEmployeeReportVo>();
		List<EmployeeVo> employees = empService.listEmployeeCompleteDetails();
		List<Long> employeeIds = new ArrayList<Long>();
		List<ResignationVo> resignations = new ArrayList<ResignationVo>();
		// Check condition
		int count = 0;
		boolean query1 = true;
		boolean query2 = true;
		boolean query3 = true;
		boolean query4 = true;
		boolean query5 = true;
		for (EmployeeVo employee : employees) {

			query1 = true;
			query2 = true;
			query3 = true;
			query4 = true;
			query5 = true;

			if (reportVo.getbId() != 0)
				query1 = employee.getEmployeeBranchId() == reportVo.getbId();
			if (reportVo.getDepId() != 0)
				query2 = employee.getEmployeeDepartmentId() == reportVo.getDepId();
			if (reportVo.getCateId() != 0)
				query3 = employee.getEmployeeCategoryId() == reportVo.getCateId();
			if (reportVo.getTypeId() != 0)
				query4 = employee.getEmployeeTypeId() == reportVo.getTypeId();
			if (reportVo.getStatusId() != 0)
				query5 = employee.getStatusId() == reportVo.getStatusId();

			if (query1 && query2 && query3 && query4 && query5) {
				DetailedEmployeeReportVo viewVo = new DetailedEmployeeReportVo();

				viewVo.setEmpCode(employee.getEmployeeCode());
				viewVo.setBranch(employee.getEmployeeBranch());
				viewVo.setCompany(employee.getEmployeeCategory());
				viewVo.setDepartment(employee.getEmployeeDepartment());
				viewVo.setDesignation(employee.getDesignation());
				viewVo.setEmail(employee.getEmail());
				viewVo.setFirstName(employee.getFirstName());
				viewVo.setLastName(employee.getLastName());
				viewVo.setUserName(employee.getUserName());
				viewVo.setPassword(employee.getPassword());
				viewVo.setGrade(employee.getEmployeeGrade());
				viewVo.setDob(employee.getDateOfBirth());
				viewVo.setGender(employee.getGender());
				// viewVo.setMaritalStatus(employee.get);
				viewVo.setRelegion(employee.getRelegion());
				// viewVo.setFatherName(employee.get);
				viewVo.setPermanentAddress(employee.getAddress());
				// Same
				viewVo.setContactAddress(employee.getAddress());
				viewVo.setPhone(employee.getHomePhoneNumber());
				viewVo.setMobile(employee.getMobileNumber());
				viewVo.setEmergency(employee.getEmergencyMobile());
				if (employee.getSalaryType() != null) {
					if (employee.getSalaryType().equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_MONTHLY)) {
						SalaryMonthlyVo salaryMonthlyVo = salaryService
								.getMonthlySalaryByEmployee(employee.getEmployeeCode());
						if (salaryMonthlyVo != null) {
							viewVo.setGrossSalary(salaryMonthlyVo.getMonthlyGrossSalary());
							viewVo.setIncrementDates(salaryMonthlyVo.getWithEffectFrom());
						}
					}
					if (employee.getSalaryType().equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_HOURLY)) {
						SalaryHourlyWageVo salaryMonthlyVo = salaryService
								.getSalaryHourlyWage(employee.getEmployeeCode());
						if (salaryMonthlyVo != null) {
							viewVo.setGrossSalary(salaryMonthlyVo.getRegularHourSalary());
							viewVo.setIncrementDates(salaryMonthlyVo.getWithEffectFrom());
						}
					}
					if (employee.getSalaryType().equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_DAILY)) {
						SalaryDailyWagesVo salaryMonthlyVo = salaryService
								.getSalaryDailyWage(employee.getEmployeeCode());
						if (salaryMonthlyVo != null) {
							viewVo.setGrossSalary(salaryMonthlyVo.getSalary());
							viewVo.setIncrementDates(salaryMonthlyVo.getWithEffectFrom());
						}
					}
				}
				for (EmployeeBankAccountVo vo : employee.getBankAccountVo()) {
					if (vo.getAccountType().equalsIgnoreCase(AccountTypeConstants.SB)) {
						viewVo.setBank(vo.getBankName());
						viewVo.setAccountNumber(vo.getAccountNumber());
						break;
					} else {
						viewVo.setBank(vo.getBankName());
						viewVo.setAccountNumber(vo.getAccountNumber());
					}

				}
				viewVo.setPfNumber(employee.getPfId());
				viewVo.setEsicNumber(employee.getEsiId());
				viewVo.setLwfNumber(employee.getEmployeeTaxNumber());
				viewVo.setBloodGroup(employee.getBloodGroup());
				viewVo.setPanNumber(employee.getPanCadNumber());
				SuperiorSubordinateVo superiorSubordinateVo = employee.getSuperiorSubordinateVo();
				if (superiorSubordinateVo != null) {
					for (SuperiorVo vo : superiorSubordinateVo.getSuperiors()) {
						if (viewVo.getReportTo() != null) {
							if (viewVo.getReportTo().length() > 0)
								viewVo.setReportTo(viewVo.getReportTo() + " , " + vo.getSuperiorName());
						} else
							viewVo.setReportTo(vo.getSuperiorName());
					}
				}

				employeeIds.clear();
				employeeIds.add(employee.getEmployeeId());
				resignations.clear();
				resignations = resignService.listResignationsByEmployeeIds(employeeIds);
				if (resignations != null) {
					if (resignations.size() != 0) {
						ResignationVo vo = resignations.get(resignations.size() - 1);
						viewVo.setResignDate(vo.getResignationDate());
					}
				}
				viewVo.setStatus(employee.getStatus());
				if (employee.getJoiningDate() != null) {
					viewVo.setJoinDate(employee.getJoiningDate());
				}
				// viewVo.setBirthPlace(employee.getCity());
				viewVo.setSlno(++count);
				viewVo.setUserName(employee.getUserName());
				result.add(viewVo);
			}
		}
		return result;

	}

}
