package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.MemoDao;
import com.hrms.web.entities.Memo;
import com.hrms.web.service.MemoService;
import com.hrms.web.vo.MemoVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-March-2015
 *
 */
@Service
public class MemoServiceImpl implements MemoService {

	@Autowired
	private MemoDao memoDao;

	public void saveOrUpdateMemo(MemoVo memoVo) {
		memoDao.saveOrUpdateMemo(memoVo);
	}

	public void saveMemoDocuments(MemoVo memoVo) {
		memoDao.saveMemoDocuments(memoVo);
	}

	public void deleteMemo(Long memoId) {
		memoDao.deleteMemo(memoId);
	}

	public void deleteMemoDocument(Long memoDocumentId) {
		memoDao.deleteMemoDocument(memoDocumentId);
	}

	public Memo findMemo(Long memoId) {
		return memoDao.findMemo(memoId);
	}

	public MemoVo findMemoById(Long memoId) {
		return memoDao.findMemoById(memoId);
	}

	public List<MemoVo> findMemoByDate(Date memoDate) {
		return memoDao.findMemoByDate(memoDate);
	}

	public List<MemoVo> findMemoBetweenDates(Date startDate, Date endDate) {
		return memoDao.findMemoBetweenDates(startDate, endDate);
	}

	public List<MemoVo> findAllMemo() {
		return memoDao.findAllMemo();
	}

}
