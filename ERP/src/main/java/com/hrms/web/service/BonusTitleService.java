package com.hrms.web.service;

import java.util.List;

import com.hrms.web.vo.BonusTitleVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
public interface BonusTitleService {

	/**
	 * method to save bonus title
	 * @param titleVo
	 */
	public void saveBonusTitle(BonusTitleVo titleVo); 
	
	/**
	 * method to update bonus title
	 * @param titleVo
	 */
	public void updateBonusTitle(BonusTitleVo titleVo);
	
	/**
	 * method to delete bonus title
	 * @param bonusTitleId
	 */
	public void deleteBonusTitle(Long bonusTitleId);
	
	/**
	 * method to delete bonus title
	 * @param bonusTitle
	 */
	public void deleteBonusTitle(String bonusTitle);
	
	/**
	 * method to list all bonus titles
	 * @return
	 */
	public List<BonusTitleVo> listAllBonusTitles();
}
