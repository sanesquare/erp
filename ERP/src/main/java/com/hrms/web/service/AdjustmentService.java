package com.hrms.web.service;

import java.util.Date;
import java.util.List;

import com.hrms.web.entities.Adjustments;
import com.hrms.web.vo.AdjustmentVo;

/**
 * 
 * @author Shamsheer
 * @since 14-April-2015
 */
public interface AdjustmentService {

	/**
	 * method to save adjustment
	 * @param vo
	 * @return
	 */
	public Long saveAdjustment(AdjustmentVo vo);
	
	/**
	 * method to update adjustment
	 * @param vo
	 */
	public void updateAdjustment(AdjustmentVo vo);
	
	/**
	 * method to delete adjustment
	 * @param adjustmentId
	 */
	public void deleteAdjustment(Long adjustmentId);
	
	/**
	 * method to list all adjustments
	 * @return
	 */
	public List<AdjustmentVo> listAllAdjustments();
	
	/**
	 * method to list adjustments by employee
	 * @param employeeId
	 * @return
	 */
	public List<AdjustmentVo> listAdjustmentsByEmployee(Long employeeId);
	
	/**
	 * method to list adjustments by title
	 * @param employeeId
	 * @return
	 */
	public List<AdjustmentVo> listAdjustmentsByTitle(Long titleId);
	
	/**
	 * method to list adjustments by type
	 * @param employeeId
	 * @return
	 */
	public List<AdjustmentVo> listAdjustmentsByType(String type);
	
	/**
	 * method to find adjustment by id
	 * @param adjustmentId
	 * @return
	 */
	public AdjustmentVo findAdjustmentById(Long adjustmentId);
	
	/**
	 * method to get adjustment by employee and title
	 * @param employeeId
	 * @param titleId
	 * @return
	 */
	public AdjustmentVo getAdjustmentByEmployeeAndTitle(String employeeCode ,Long titleId);
	
	/**
	 * method to list adjustment by start date & end date
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<AdjustmentVo> listAdjustmentsByStartDateEndDate(Date startDate, Date endDate);
	
	/**
	 *  method to list adjustment by employee , start date & end date
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<AdjustmentVo> listAdjustmentsByEmployeeStartDateEndDate(Long employeeId , Date startDate, Date endDate);
}
