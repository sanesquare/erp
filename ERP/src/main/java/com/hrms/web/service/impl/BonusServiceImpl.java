package com.hrms.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.BonusDao;
import com.hrms.web.service.BonusService;
import com.hrms.web.vo.BonusVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
@Service
public class BonusServiceImpl implements BonusService{

	@Autowired
	private BonusDao bonusDao;

	public Long saveBonus(BonusVo bonusVo) {
		return bonusDao.saveBonus(bonusVo);
	}

	public void updateBonus(BonusVo bonusVo) {
		bonusDao.updateBonus(bonusVo);
	}

	public void deleteBonus(Long bonusId) {
		bonusDao.deleteBonus(bonusId);
	}

	public List<BonusVo> listAllBonuses() {
		return bonusDao.listAllBonuses();
	}

	public List<BonusVo> listBonusesByEmployee(Long employeeId) {
		return bonusDao.listBonusesByEmployee(employeeId);
	}

	public List<BonusVo> listBonusesByTitle(Long bonusTitleId) {
		return bonusDao.listBonusesByTitle(bonusTitleId);
	}

	public BonusVo getBonusById(Long bonusId) {
		return bonusDao.getBonusById(bonusId);
	}

	public BonusVo getBonusByEmployeeAndTitle(String employeeCode, Long BonusTitleId) {
		return bonusDao.getBonusByEmployeeAndTitle(employeeCode, BonusTitleId);
	}

	public List<BonusVo> listBonusesByEmployeeStartDateEndDate(Long employeeId, Date startDate, Date endDate) {
		return bonusDao.listBonusesByEmployeeStartDateEndDate(employeeId, startDate, endDate);
	}

	public List<BonusVo> listBonusesByStartDateEndDate(Date startDate, Date endDate) {
		return bonusDao.listBonusesByStartDateEndDate(startDate, endDate);
	}
}
