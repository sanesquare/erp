package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Project;
import com.hrms.web.vo.ProjectJsonVo;
import com.hrms.web.vo.ProjectVo;

/**
 * 
 * @author Shamsheer
 * @since 10-March-2015
 *
 */
public interface ProjectService {

	/**
	 * Method to save new Project
	 * @param projectVo
	 * @return void
	 */
	public Long saveProject(ProjectJsonVo projectVo);
	
	/**
	 * method to update project document
	 * @param projectVo
	 */
	public ProjectVo updateProjectDocument(ProjectVo projectVo);
	
	/**
	 * method to update existing project
	 * @param projectVo
	 * @return void
	 */
	public void updateProject(ProjectJsonVo projectVo);
	
	/**
	 * method to delete existing project
	 * @param projectVo
	 * @return void
	 */
	public void deleteProject(Long id);
	
	/**
	 * method to find project by using project Id
	 * @param id
	 * @return ProjectVo
	 */
	public ProjectVo findProjectById(Long projectId);
	
	/**
	 * method to find project by using project title
	 * @param title
	 * @return ProjectVo
	 */
	public ProjectVo findProjectByTitle(String projectTitle);
	
	
	/**
	 * method to list all projects
	 * @return List<ProjectVo>
	 */
	public List<ProjectVo> listAllProjects();
	
	/**
	 * method to find and return Project object by id
	 * @return
	 */
	public Project findAndReturnProjectObjByTitle(String title);
	
	/**
	 * method to list project by status
	 * @return
	 */
	public List<ProjectVo> listProjectByStatus(String status);
	
	/**
	 * method to delete project document
	 * @param path
	 */
	public void deleteProjectDocument(String path);
	
	/**
	 * method to update project status
	 * @param projectVo
	 */
	public void updateProjectStatus(ProjectJsonVo projectVo);
}
