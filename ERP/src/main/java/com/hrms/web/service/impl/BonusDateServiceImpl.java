package com.hrms.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.BonusDateDao;
import com.hrms.web.entities.BonusDate;
import com.hrms.web.service.BonusDateService;
import com.hrms.web.vo.BonusDateVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Service
public class BonusDateServiceImpl implements BonusDateService {

	@Autowired
	private BonusDateDao bonusDateDao;

	public void saveOrUpdateBonusDate(BonusDateVo bonusDateVo) {
		bonusDateDao.saveOrUpdateBonusDate(bonusDateVo);
	}

	public void deleteBonusDate(Long bonusDateId) {
		bonusDateDao.deleteBonusDate(bonusDateId);
	}

	public BonusDate findBonusDate(Long bonusDateId) {
		return bonusDateDao.findBonusDate(bonusDateId);
	}

	public BonusDateVo findBonusDateById(Long bonusDateId) {
		return bonusDateDao.findBonusDateById(bonusDateId);
	}

	public BonusDateVo findBonusDate() {
		return bonusDateDao.findBonusDate();
	}

}
