package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.Employee;
import com.hrms.web.entities.ShiftDay;
import com.hrms.web.entities.WorkShift;
import com.hrms.web.vo.WorkShiftDetailsVo;

/**
 * 
 * @author Jithin Mohan
 * @since 1-April-2015
 *
 */
public interface WorkShiftDetailsService {

	/**
	 * method to save WorkShift
	 * 
	 * @param shiftDetailsVo
	 */
	public void saveWorkShiftDetails(List<WorkShiftDetailsVo> shiftDetailsVo);

	/**
	 * method to update WorkShift
	 * 
	 * @param shiftDetailsVo
	 */
	public void updateWorkShiftDetails(WorkShiftDetailsVo shiftDetailsVo);

	/**
	 * method to delete WorkShift
	 * 
	 * @param shiftDetailsId
	 */
	public void deleteWorkShiftDetails(Long shiftDetailsId);

	/**
	 * method to find WorkShift by id
	 * 
	 * @param shiftDetailsId
	 * @return WorkShift
	 */
	public WorkShift findWorkShiftDetails(Long shiftDetailsId);

	/**
	 * method to find WorkShift by shiftDay
	 * 
	 * @param shiftDay
	 * @return List<WorkShift>
	 */
	public List<WorkShift> findWorkShiftDetails(ShiftDay shiftDay);

	/**
	 * method to find WorkShift by id
	 * 
	 * @param shiftDetailsId
	 * @return WorkShiftDetailsVo
	 */
	public WorkShiftDetailsVo findWorkShiftDetailsById(Long shiftDetailsId);

	/**
	 * method to find WorkShift by employee
	 * 
	 * @param employee
	 * @return List<WorkShiftDetailsVo>
	 */
	public List<WorkShiftDetailsVo> findWorkShiftDetailsByEmployee(Employee employee);

	/**
	 * method to find all WorkShift
	 * 
	 * @return List<WorkShiftDetailsVo>
	 */
	public List<WorkShiftDetailsVo> findAllWorkShiftDetails();

	/**
	 * 
	 * @param day
	 * @return
	 */
	public WorkShiftDetailsVo findWorkShiftByDay(String day, String employeeCode);
}
