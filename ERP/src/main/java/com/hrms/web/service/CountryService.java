package com.hrms.web.service;

import java.util.List;

import com.hrms.web.entities.Country;
import com.hrms.web.vo.CountryVo;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public interface CountryService {

	/**
	 * method to save new country
	 * 
	 * @param countryVo
	 * @return boolean
	 */
	public void saveCountry(CountryVo countryVo);

	/**
	 * method to update country information
	 * 
	 * @param countryVo
	 * @return boolean
	 */
	public void updateCountry(CountryVo countryVo);

	/**
	 * method to delete country information
	 * 
	 * @param countryId
	 * @return boolean
	 */
	public void deleteCountry(Long countryId);

	/**
	 * method to find country by id and return country object
	 * 
	 * @param countryId
	 * @return Country
	 */
	public Country findCountry(Long countryId);

	/**
	 * method to find country by id and return CountryVo Object
	 * 
	 * @param countryId
	 * @return countryVo
	 */
	public CountryVo findCountryById(Long countryId);

	/**
	 * method to find country by country name
	 * 
	 * @param countryName
	 * @return CountryVo
	 */
	public CountryVo findCountrybyName(String countryName);

	/**
	 * method to find all contries
	 * 
	 * @return List<CountryVo>
	 */
	public List<CountryVo> findAllCountry();
}
