package com.hrms.web.service;

import com.hrms.web.entities.ProfesionalTax;
import com.hrms.web.vo.ProfesionalTaxVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-May-2015
 *
 */
public interface ProfesionalTaxService {

	/**
	 * 
	 * @param profesionalTaxVo
	 */
	public void saveOrUpdate(ProfesionalTaxVo profesionalTaxVo);

	/**
	 * 
	 * @param id
	 * @return
	 */
	public ProfesionalTax findProfesionalTax(Long id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	public ProfesionalTaxVo findProfesionalTaxById(Long id);

	/**
	 * 
	 * @return
	 */
	public ProfesionalTaxVo findProfesionalTax();
}
