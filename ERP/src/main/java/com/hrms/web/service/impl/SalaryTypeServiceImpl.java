package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.impl.SalaryTypeDao;
import com.hrms.web.service.SalaryTypeService;
import com.hrms.web.vo.SalaryTypeVo;

/**
 * 
 * @author Shamsheer
 * @since 18-April-2015
 */
@Service
public class SalaryTypeServiceImpl implements SalaryTypeService{

	@Autowired
	private SalaryTypeDao dao;

	public void saveSalaryType(SalaryTypeVo salaryTypeVo) {
		dao.saveSalaryType(salaryTypeVo);
	}

	public void updateSalaryType(SalaryTypeVo salaryTypeVo) {
		dao.updateSalaryType(salaryTypeVo);
	}

	public void deleteSalaryType(Long id) {
		dao.deleteSalaryType(id);
	}

	public List<SalaryTypeVo> listAllSalaryTypes() {
		return dao.listAllSalaryTypes();
	}

	public SalaryTypeVo findSalaryType(Long id) {
		return dao.findSalaryType(id);
	}

	public SalaryTypeVo findSalaryType(String type) {
		return dao.findSalaryType(type);
	}
}
