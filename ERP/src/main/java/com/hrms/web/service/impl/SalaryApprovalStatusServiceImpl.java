package com.hrms.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.SalaryApprovalStatusDao;
import com.hrms.web.service.SalaryApprovalStatusService;
import com.hrms.web.vo.SalaryApprovalStatusVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-09-2015
 */
@Service
public class SalaryApprovalStatusServiceImpl implements SalaryApprovalStatusService{

	@Autowired
	private SalaryApprovalStatusDao dao;
	
	public List<SalaryApprovalStatusVo> findAllStatus() {
		return dao.findAllStatus();
	}

}
