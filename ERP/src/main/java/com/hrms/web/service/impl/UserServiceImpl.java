package com.hrms.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrms.web.dao.UserDao;
import com.hrms.web.entities.User;
import com.hrms.web.service.UserService;
import com.hrms.web.vo.UserVo;

/**
 * 
 * @author Jithin Mohan
 * @since 29-March-2015
 *
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	public void saveUser(UserVo userVo) {
		userDao.saveUser(userVo);
	}

	public void updateUser(UserVo userVo) {
		userDao.updateUser(userVo);
	}

	public void deleteUser(Long id) {
		userDao.deleteUser(id);
	}

	public User findUserById(Long id) {
		return userDao.findUserById(id);
	}

	public User findUserByUsername(String userName) {
		return userDao.findUserByUsername(userName);
	}

	public User findUserByEmployeeId(Long employeeId) {
		return userDao.findUserByEmployeeId(employeeId);
	}

	public UserVo findUser(String userName) {
		return userDao.findUser(userName);
	}

	public boolean changePassword(String userName,String password, String newPassword) {
		return userDao.updatePassword(userName,password ,newPassword);
	}
	

}
