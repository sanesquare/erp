package com.hrms.web.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dao.impl.EmployeeDesignationDaoImpl;
import com.hrms.web.entities.EmployeeDesignation;
import com.hrms.web.entities.EmployeeGrade;
import com.hrms.web.entities.JobCandidates;
import com.hrms.web.mail.ApplicationMailSender;
import com.hrms.web.mail.MailConfiguration;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.EmployeeDesignationService;
import com.hrms.web.service.EmployeeGradeService;
import com.hrms.web.service.JobCandidateService;
import com.hrms.web.service.JobInterviewService;
import com.hrms.web.service.JobPostService;
import com.hrms.web.service.SalaryCalculatorService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AppointmentLetterVo;
import com.hrms.web.vo.EmployeeByDesignationsVo;
import com.hrms.web.vo.JobCandidatesVo;
import com.hrms.web.vo.JobInterviewVo;
import com.hrms.web.vo.OfferLetterVo;
import com.hrms.web.vo.SalaryCalculatorVo;
import com.hrms.web.vo.SessionRolesVo;
import com.itextpdf.text.Document;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * 
 * @author Vinutha
 * @since 12-May-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class RecLettersController {

	@Autowired
	private JobPostService postService;

	@Autowired
	private JobInterviewService interviewService;

	@Autowired
	private JobCandidateService candidatesService;
	
	@Autowired
	private EmployeeGradeService gradeService;
	
	@Autowired
	private SalaryCalculatorService salaryCalculatorService;
	
	@Autowired
	private EmployeeDesignationService designationService;
	
	@Autowired
	private TaskExecutor taskExecutor;

	/**
	 * Method to handle request for recruitment letters page
	 * 
	 * @param model
	 * @param request
	 * @param rolesMap
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REC_LETTERS_URL, method = RequestMethod.GET)
	public String recLettersPageGetRequestHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("roles")SessionRolesVo rolesMap , @ModelAttribute OfferLetterVo offerLetterVo,
			@ModelAttribute AppointmentLetterVo appointmentLetterVo){
		try {
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("jobPosts", postService.listAllJobPosts());
			model.addAttribute("grades" , gradeService.findAllGrade());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.REC_LETTERS_PAGE;
	}

	/**
	 * method to send offer letter
	 * @param model
	 * @param offerLetterVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SEND_OFFER_LETTER , method = RequestMethod.POST)
	public String sendOfferLetter(final Model model , @ModelAttribute OfferLetterVo offerLetterVo){
		try {
			SalaryCalculatorVo calculatorVo = salaryCalculatorService.calculateSalary(offerLetterVo.getGross());
			calculatorVo.setGrossSalary(offerLetterVo.getGross());
			List<JobCandidatesVo> candidatesVos = new ArrayList<JobCandidatesVo>();
			List<OfferLetterVo> mailVos = new ArrayList<OfferLetterVo>();
			EmployeeDesignation designation = designationService.findDesignationById(offerLetterVo.getDesignationId());
			EmployeeGrade grade = gradeService.findGrade(offerLetterVo.getGradeId());
			for(Long id : offerLetterVo.getCandId()){
				candidatesVos.add(candidatesService.findJobCandidatesById(id));
			}
			for(JobCandidatesVo candidatesVo : candidatesVos){
				OfferLetterVo letterVo = new OfferLetterVo();
				letterVo.setSalaryCalculatorVo(calculatorVo);
				letterVo.setName(candidatesVo.getFirstName()+" "+candidatesVo.getLastName());
				letterVo.setDesignation(designation.getDesignation());
				letterVo.setGrade(grade.getGrade());
				letterVo.setEmail(candidatesVo.geteMail());
				letterVo.setBranch("Dummy Branch");
				letterVo.setSender("Test");
				letterVo.setSenderDesignation("Tester");
				mailVos.add(letterVo);
			}
			ApplicationMailSender mailSender = new ApplicationMailSender(mailVos, CommonConstants.OFFER_LETTER);
			taskExecutor.execute(mailSender);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.REC_LETTERS_URL;
	}
	
	/**
	 * method to send appointment letter
	 * @param model
	 * @param appointmentLetterVo
	 * @return
	 */
	@RequestMapping(value =RequestConstants.SEND_APPOINTMENT_LETTER,method=RequestMethod.POST)
	public String sendAppointmentLette(final Model model , @ModelAttribute AppointmentLetterVo appointmentLetterVo){
		try{
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.REC_LETTERS_URL;
	}
	
	/**
	 * Method to get candidates
	 * 
	 * @param date
	 * @param id
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REC_FIND_CANDIDATES, method = RequestMethod.GET)
	public @ResponseBody JsonResponse recFindCandidatesRequestHandler(@RequestParam(value = "date") String date,
			@RequestParam(value = "postId") Long id) {
		JsonResponse response = new JsonResponse();
		JobInterviewVo interview = interviewService.listInterviewByPostAndDate(id, date);
		List<JobCandidatesVo> candidates = new ArrayList<JobCandidatesVo>();
		if (interview != null) {
			candidates = candidatesService.findCandidatesByInterview(interview.getJobInterviewId());
		}
		response.setResult(candidates);
		return response;

	}

	/**
	 * Method to get interviews by date
	 * 
	 * @param date
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REC_FIND_INTERVIEWS_BY_DATE, method = RequestMethod.GET)
	public @ResponseBody JsonResponse recFindInterviewsRequestHandler(@RequestParam(value = "date") String date) {
		JsonResponse response = new JsonResponse();
		List<JobInterviewVo> interviews = interviewService.listInterviewsByDate(date);
		List<JobCandidatesVo> candidates = new ArrayList<JobCandidatesVo>();
		/*
		 * for(JobInterviewVo interviewVo : interviews){
		 * candidates.addAll(candidatesService
		 * .findCandidatesByInterview(interviewVo.getJobInterviewId())); }
		 */
		response.setResult(interviews);
		return response;

	}
	public void sendAppointementLetter() {
		try {
			MailConfiguration mailConfiguration = new MailConfiguration();
			Message message = new MimeMessage(mailConfiguration.mailSender());
			message.setFrom(new InternetAddress("service@snogol.net"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("shamsheerfinu@gmail.com"));
			message.setSubject("Test By Shamsheer");
			MimeMultipart multipart = new MimeMultipart("related");
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(createAppointmentLetter(), "text/html");
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String createAppointmentLetter() {
		String body = "<html><head><link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'></head><body style='background: #FFFFFF;font-family: 'Open Sans', sans-serif;'><div style='margin:auto;width:80%;height:auto;margin-top:20px;margin-bottom:20px;text-align:left;background:#fff;padding:20px;border: #999 solid 1px;min-height: 660px;'>"
				+ "<div style='width: 100%;color: #000000;font-size: 12px;font-weight: bold;text-transform: uppercase;margin-bottom: 25px;margin-top: 22px;'><img src='http://3gmobileworld.in/wp-content/uploads/2015/04/3g-logo-header-2.png' style='width: 200px;margin-left: -29px;'><br> Ref : 3G/AO/SEP-2014</div>"
				+ "<div style='width:100%;color:#333;text-align:left;line-height: 24px;font-size: 14px;'><div style='font-size: 23px;font-weight:bold;color: #24668B;width: 100%;margin-bottom: 10px;'> APPOINTMENT LETTER</div>To,<br>"
				+ "  VIPIN- Manager <br><br>"
				+ " The bearer of this letter <strong>Mr.AKHIL</strong> RAJ  has been joined in our showroom as <strong>Technician TRAINEE </strong> at  <strong>Mukkom  Branch</strong> , with effect from<strong> 21th APRIL 2015</strong>. He will be under training at <strong>Landship MaLL</strong>. Please extend him good co-operation and do the needful accordingly. <br><br>Thanking You<br> Yours faithfully <br><br><br>"
				+ " <div style=' float: left;width: 50%;'>SHINJITH P <br><strong> HR -Manager</strong> </div>"
				+ "  <div style='text-align: right;'> "
				+ "<strong>HABEEB RAHMAN</strong> <br><strong>Finance Asst.Manager</strong> </div><br>"
				+ " <p style='width:100%;text-align:center;text-decoration:underline;'>Branch Confirmation</p>"
				+ " <div style=' float: left; width: 50%;'>Branch Seal: </div>"
				+ "   <div style='float: left; width: 50%;'> Date &amp; Time of Joining:<br>Manager's Name :<br>Signature : </div></div></div>"
				+ "<div style='float: right;height:auto;margin-top: 0px;text-align: right;color: #000000;font-size: 13px;width: 233px;margin-right: 10%;margin-bottom: 15px;'> Muhammed Nadeer C.K.V<br>General  Manager - operation </div>"
				+ "</body></html>";
		return body;
	}
}
