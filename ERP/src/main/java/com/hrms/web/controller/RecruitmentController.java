package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.CandidateResume;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.CandidatesStatusService;
import com.hrms.web.service.CountryService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeDesignationService;
import com.hrms.web.service.JobCandidateService;
import com.hrms.web.service.JobFieldService;
import com.hrms.web.service.JobPostService;
import com.hrms.web.service.JobTypeService;
import com.hrms.web.service.LanguageService;
import com.hrms.web.service.QualificationDegreeService;
import com.hrms.web.service.SkillService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.CandidateDocumentsVo;
import com.hrms.web.vo.CandidateResumeVo;
import com.hrms.web.vo.CandidatesLanguageVo;
import com.hrms.web.vo.CandidatesQualificationsVo;
import com.hrms.web.vo.CandidatesSkillsVo;
import com.hrms.web.vo.InterviewDocumentsVo;
import com.hrms.web.vo.JobCandidatesVo;
import com.hrms.web.vo.JobInterviewVo;
import com.hrms.web.vo.JobPostDocumentsVo;
import com.hrms.web.vo.JobPostVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Vinutha
 * @since 19-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class RecruitmentController {

	@Autowired
	private JobPostService jobPostService;
	
	@Autowired
	private JobTypeService jobTypeService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private BranchService branchService;
	
	@Autowired
	private JobCandidateService candidateService;
	
	@Autowired
	private JobFieldService jobFieldsService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private QualificationDegreeService degreeService;
	
	@Autowired
	private SkillService skillService;
	
	@Autowired
	private LanguageService languageService;
	
	@Autowired
	private CandidatesStatusService statusService;
	
	@Autowired
	private EmployeeDesignationService designationService;
	
	@Log
	private Logger logger;
	
	/**
	 * method to handle GET request for job posts page 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.RECRUITMENT_JOB_POSTS_URL,method=RequestMethod.GET)
	public String jobPostPageGetRequestHandler(final Model model,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		String msg=request.getParameter(MessageConstants.SUCCESS);
		String errMsg=request.getParameter(MessageConstants.ERROR);
		if(msg!=null){
			model.addAttribute(MessageConstants.SUCCESS,msg);
		}
		else if(errMsg!=null)
		{
			model.addAttribute(MessageConstants.ERROR,errMsg);
		}
		try{
			model.addAttribute("jobPosts",jobPostService.listAllJobPosts());
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);			
		}
		return PageNameConstatnts.RECRUITMENT_JOB_POSTS_PAGE;
	}
	/**
	 * method to handle GET request for add job posts 
	 * @param model
	 * @param request
	 * @return
	 */
	 
	@RequestMapping(value=RequestConstants.RECRUITMENT_ADD_JOB_POSTS_URL,method=RequestMethod.GET)
	public String jobPostAddPageGetRequestHandler(final Model model,@ModelAttribute JobPostVo jobPostVo ,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try
		{
			model.addAttribute("jobTypes",jobTypeService.findAllJobType());
			model.addAttribute("departments",departmentService.listAllDepartments());
			model.addAttribute("designations",designationService.listAllDesignations());
			model.addAttribute("branches",branchService.listAllBranches());
			
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);		
			return PageNameConstatnts.RECRUITMENT_JOB_POSTS_PAGE;
		}
		return PageNameConstatnts.RECRUITMENT_ADD_JOB_POSTS_PAGE;
	}
	
	
	/**
	 * method to handle saveJobPost post request
	 * @param model
	 * @param jobPostVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.SAVE_JOB_POST_URL,method=RequestMethod.POST)
	public String saveJobPostPageGetRequestHandler(final Model model,
			@ModelAttribute JobPostVo jobPostVo,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		
				try {
					jobPostService.saveJobPost(jobPostVo);
					System.out.println(jobPostVo.getJobTitle());
				}catch(Exception e)
				{
					logger.error(e.getMessage());
				}
				return PageNameConstatnts.RECRUITMENT_JOB_POSTS_PAGE;
	
	}
	/**
	 * method to handle POST request for jobPostInfo
	 * @param jobPostVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_JOB_POST_INFO_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveJobPostInfo(@RequestBody JobPostVo jobPostVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(jobPostVo.getJobPostId()==null){
				jobPostService.saveJobPost(jobPostVo);
				System.out.println(jobPostVo.getJobTitle());
				jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_SUCCESS);
		}else{
		
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to update job post
	 * @param jobPostVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_POST_INFO_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse editJobPostInfo(@RequestBody JobPostVo jobPostVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(jobPostVo.getJobPostId()!=null){
				jobPostService.updateJobPost(jobPostVo);
				jsonResponse.setStatus(MessageConstants.JOB_POST_UPDATE_SUCCESS);
		}else{
			jsonResponse.setStatus(MessageConstants.JOB_POST_UPDATE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_POST_UPDATE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * request handler for job post edit
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.UPDATE_JOB_POST_URL,method=RequestMethod.GET)
	public String updateJobPostPage(final Model model,@ModelAttribute JobPostVo jobPostVo ,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try
		{
			model.addAttribute("jobPostVo",jobPostService.findJobPostById(Long.parseLong(request.getParameter("pid"))));
			model.addAttribute("jobTypes",jobTypeService.findAllJobType());
			model.addAttribute("departments",departmentService.listAllDepartments());
			model.addAttribute("designations",designationService.listAllDesignations());
			model.addAttribute("branches",branchService.listAllBranches());
			return PageNameConstatnts.RECRUITMENT_ADD_JOB_POSTS_PAGE;
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.WRONG);
			return PageNameConstatnts.RECRUITMENT_JOB_POSTS_PAGE;
		}
		
	}
	/**
	 * POST request handler for job post edit
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.UPDATE_JOB_POST_URL,method=RequestMethod.POST)
	public String updateJobPostPagePost(final Model model,@ModelAttribute JobPostVo jobPostVo ,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try
		{
			model.addAttribute("jobPostVo",jobPostService.findJobPostById(Long.parseLong(request.getParameter("pid"))));
			return PageNameConstatnts.RECRUITMENT_ADD_JOB_POSTS_PAGE;
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.WRONG);
			return PageNameConstatnts.RECRUITMENT_JOB_POSTS_PAGE;
		}
		
	}
	
	/**
	 * method to update job post
	 * @param jobPostVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.JOB_POST_SAVE_BASIC_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveJobPostBasicInfo(@RequestBody JobPostVo jobPostVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(jobPostVo.getJobPostId()==null){
				System.out.println(jobPostVo.getJobTitle());
				Long id=jobPostService.saveBasicInfo(jobPostVo);
				jsonResponse.setResult(id);
				jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_SUCCESS);
		}else{
			jobPostService.updateJobPost(jobPostVo);
			jsonResponse.setStatus(MessageConstants.JOB_POST_UPDATE_SUCCESS);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save job post qualification
	 * @param jobPostVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.JOB_POST_SAVE_QUALIFICATION_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveJobPostQualification(@RequestBody JobPostVo jobPostVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(jobPostVo.getJobPostId()!=null){
				jobPostService.saveQualificationInfo(jobPostVo);
				jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_SUCCESS);
		}else{
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save job post qualification
	 * @param jobPostVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.JOB_POST_SAVE_EXPERIENCE_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveJobPostExperience(@RequestBody JobPostVo jobPostVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(jobPostVo.getJobPostId()!=null){
				jobPostService.saveExperienceInfo(jobPostVo);
				jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_SUCCESS);
		}else{
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save additional info
	 * @param jobPostVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.JOB_POST_SAVE_ADDITIONAL_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveJobPostAddInfo(@RequestBody JobPostVo jobPostVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(jobPostVo.getJobPostId()!=null){
				jobPostService.saveAdditionalInfo(jobPostVo);
				jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_SUCCESS);
		}else{
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save description
	 * @param jobPostVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.JOB_POST_SAVE_DESCRIPTION_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveJobPostDescription(@RequestBody JobPostVo jobPostVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(jobPostVo.getJobPostId()!=null){
				jobPostService.savePostDescription(jobPostVo);
				jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_SUCCESS);
		}else{
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_POST_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to handle delete job post request GET
	 * @param model
	 * @param jobPostVo
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.DELETE_JOB_POST_URL,method=RequestMethod.GET)
	public String deleteJobPostPageGetRequestHandler(final Model model,
			@ModelAttribute JobPostVo jobPostVo,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		
				try {
					JobPostVo jobPostVo1 = jobPostService.findJobPostById(Long.parseLong(request.getParameter("pid")));
					jobPostService.deleteJobPost(jobPostVo1);
					model.addAttribute("jobPosts",jobPostService.listAllJobPosts());
					model.addAttribute(MessageConstants.SUCCESS,MessageConstants.JOB_POST_DELETE_SUCCESS);
				}catch(Exception e)
				{
					logger.error(e.getMessage());
					model.addAttribute(MessageConstants.ERROR,MessageConstants.JOB_POST_DELETE_FAILED);
				}
				return PageNameConstatnts.RECRUITMENT_JOB_POSTS_PAGE;
	
	}
	/**
	 * method to handle view job post request
	 * @param model
	 * @param jobPostVo
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_POST_VIEW_URL,method=RequestMethod.GET)
	public String viewJobPostPageGetRequestHandler(final Model model,
			@ModelAttribute JobPostVo jobPostVo,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		
				try {
					model.addAttribute("jobPost",jobPostService.findJobPostById(Long.parseLong(request.getParameter("pid"))));
				
				}catch(Exception e)
				{
					logger.error(e.getMessage());
					model.addAttribute(MessageConstants.ERROR,MessageConstants.WRONG);
				}
				return PageNameConstatnts.RECRUITMENT_VIEW_JOB_POST_PAGE;
	
	}
	/**
	 * method to handle request for job post document upload
	 * @param model
	 * @param jobPostVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_POST_UPLOAD_DOCS,method=RequestMethod.POST)
	public String uploadPostDocs(final Model model, @ModelAttribute  JobPostVo jobPostVo){
		try{

			if(jobPostVo.getJobPostId()!=null){
				jobPostVo.setJobPostDocumentsVos(uploadPoDocuments(jobPostVo.getUploadPostDocs()));
				jobPostService.updateDocuments(jobPostVo);
				model.addAttribute(MessageConstants.SUCCESS,MessageConstants.FILE_UPLOAD_SUCCESS);
			}
						
		}catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.WRONG);
		}
		return "redirect:"+RequestConstants.UPDATE_JOB_POST_URL+"?pid="+jobPostVo.getJobPostId();
		//return "redirect:"+RequestConstants.RECRUITMENT_JOB_POSTS_URL;
	}
	/**
	 * method to upload Job POst Documents
	 * @param files
	 * @return
	 */
	private Set<JobPostDocumentsVo> uploadPoDocuments(List<MultipartFile> files){
		Set<JobPostDocumentsVo> documentsVos=new HashSet<JobPostDocumentsVo>();
		try{
			for(MultipartFile file: files){
				
				if(!file.isEmpty()){
					String fileAccessUrl=FileUpload.uploadDocument(MessageConstants.JOB_POST_MODULE, file);
					
					if(fileAccessUrl!=null){
						JobPostDocumentsVo documentsVo=new JobPostDocumentsVo();
						documentsVo.setDocumentUrl(fileAccessUrl);
						documentsVo.setFileName(file.getOriginalFilename());
						documentsVos.add(documentsVo);
					}
				}
				
			}
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return documentsVos;
	}
	
	/**
	 * method to handle request for downloading job post document
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value=RequestConstants.JOB_POST_DWNLD_DOCS,method=RequestMethod.GET)
	public void downloadPostDocument(final Model model, HttpServletRequest request , HttpServletResponse response){
		try{
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(request.getParameter("durl"), response);
		}catch(Exception e){
			logger.error(e.getMessage());
		}
	}
	

	/** 
	 * method to delete document
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_POST_DELETE_DOCS,method=RequestMethod.GET)
	public String deletePostDocument(final Model model, HttpServletRequest request){
		try{
			String url=request.getParameter("durl");
			SFTPOperation operation = new SFTPOperation();
			Boolean result=operation.removeFileFromSFTP(url);
			if(result){
				jobPostService.deleteDocument(url);
				model.addAttribute(MessageConstants.SUCCESS,MessageConstants.POST_DOCUMENT_DELETE_SUCCESS);
			}
		}catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.POST_DOCUMENT_DELETE_FAILED);
		}
		return "redirect:"+RequestConstants.RECRUITMENT_JOB_POSTS_URL;
	}

	
	
	
	
	
	
	
/***************************************************************************************************************************************************************************/
	
	/**
	 * method to handle GET request for job candidates page
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.RECRUITMENT_JOB_CANDIDATES_URL,method=RequestMethod.GET)
	public String jobCandidatesPageGetRequestHandler(final Model model,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		String msg=request.getParameter(MessageConstants.SUCCESS);
		String errMsg=request.getParameter(MessageConstants.ERROR);
		if(msg!=null){
			model.addAttribute(MessageConstants.SUCCESS,msg);
		}
		else if(errMsg!=null)
		{
			model.addAttribute(MessageConstants.ERROR,errMsg);
		}
		try
		{
			model.addAttribute("jobCandidates",candidateService.listAllCandidates());
			return PageNameConstatnts.RECRUITMENT_JOB_CANDIDATES_PAGE;
		}
		catch(Exception e)
		{
			model.addAttribute(MessageConstants.ERROR,MessageConstants.WRONG);
			return PageNameConstatnts.RECRUITMENT_JOB_CANDIDATES_PAGE;
		}
	}
	
	/**
	 * method to handle GET request for add job candidates page
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.ADD_JOB_CANDIDATES_URL,method=RequestMethod.GET)
	public String addJobCandidatesPageGetRequestHandler(final Model model,@ModelAttribute JobCandidatesVo jobCandidatesVo ,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		String msg=request.getParameter(MessageConstants.SUCCESS);
		String errMsg=request.getParameter(MessageConstants.ERROR);
		if(msg!=null){
			model.addAttribute(MessageConstants.SUCCESS,msg);
		}
		else if(errMsg!=null)
		{
			model.addAttribute(MessageConstants.ERROR,errMsg);
		}
		
		try{
			
			model.addAttribute("jobFields",jobFieldsService.findAllJobField());
			model.addAttribute("designations",designationService.listAllDesignations());
			model.addAttribute("countries",countryService.findAllCountry());
			model.addAttribute("degrees",degreeService.findAllQualificationDegree());
			model.addAttribute("skills",skillService.findAllSkills());
			model.addAttribute("languages",languageService.findAllLanguage());
			//model.addAttribute("status",statusService.listAllStatus());
			
			
			return PageNameConstatnts.ADD_JOB_CANDIDATES_PAGE;
		}
		catch(Exception e){
			logger.error(e.getMessage());
			return PageNameConstatnts.RECRUITMENT_JOB_CANDIDATES_PAGE;
		}
		
	}
	/**
	 * method to save job candidates basic info
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIDATES_INFO_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidateInfo(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()==null){
				Long id=candidateService.saveCandidateBasicInfo(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				jsonResponse.setResult(id);
		}else{
			candidateService.updateCandidateBasicInfo(candidatesVo);
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_UPDATE_SUCCESS);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save candidate address
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIDATES_CONTACT_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidateContactInfo(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null){
				candidateService.saveContactDetails(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save candidate achievements
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIDATES_ACHIVE_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidateAchievementInfo(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null){
				candidateService.saveAchievement(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save candidates interests
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIATES_INT_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidatesInterests(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null){
				candidateService.saveInterests(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save candidates additional info
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIDATES_ADDINFO_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidateAddInfo(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null){
				candidateService.saveAdditionalInfo(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save candidate qualifications
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIDATES_QUALIFICATION_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidateQualification(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null){
				candidateService.saveCandidateQualifications(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * Method to get candidate qualification details
	 */
	@RequestMapping(value = RequestConstants.JOB_CANDIDATE_GET_QUALIFICATION, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse getCandidateQualification(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null&&candidatesVo.getQualificationId()!=null){
				CandidatesQualificationsVo qualificationVo = candidateService.getCandQualification(candidatesVo.getJobCandidateId(), candidatesVo.getQualificationId());
				/*if(qualificationVo!=null)
				{*/
					jsonResponse.setResult(qualificationVo);
					jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				/*}
				else
				{
					jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
				}*/
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to get candidate skills
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.JOB_CANDIDATE_GET_SKILLS, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse getCandidateSkill(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null&&candidatesVo.getSkillsId()!=null){
				CandidatesSkillsVo skillsVo= candidateService.getCandSkills(candidatesVo.getJobCandidateId(), candidatesVo.getSkillsId());
				jsonResponse.setResult(skillsVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to get candidate languages
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.JOB_CANDIDATE_GET_LANGUAGE, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse getCandidateLanguage(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null&&candidatesVo.getLanguageId()!=null){
				CandidatesLanguageVo languageVo = candidateService.getCandLanguage(candidatesVo.getJobCandidateId(), candidatesVo.getLanguageId());
				jsonResponse.setResult(languageVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	
	/**
	 * method to save candidate language
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIDATES_LANGUAGE_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidateLanguage(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null){
				candidateService.saveCandidateLanguages(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save candidate skills
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIDATES_SKILLS_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidateSkills(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null){
				candidateService.saveCandidatesSkills(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save candidate experience details
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIDATES_WORK_EXPERIENCE_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidateWorkExperience(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null){
				candidateService.saveCandidateExperiences(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save candidate references
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIDATES_REFERENCE_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidateReferences(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null){
				candidateService.saveCandidateReferences(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to save candidate status
	 * @param candidatesVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CANDIDATES_STATUS_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveCandidatesStatus(@RequestBody JobCandidatesVo candidatesVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(candidatesVo.getJobCandidateId()!=null){
				candidateService.saveCandidateStatus(candidatesVo);
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_SUCCESS);
				
		}else{
				jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_CANDIDATES_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to handle request for candidate view page
	 * @param model
	 * @param jobCandidatesVo
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_CANDIDATE_VIEW_URL,method=RequestMethod.GET)
	public String viewJobCandidatesPageGetRequestHandler(final Model model,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try{
			
			
			model.addAttribute("candidate" , candidateService.findJobCandidatesById(Long.parseLong(request.getParameter("cid"))));
			
			return PageNameConstatnts.VIEW_JOB_CANDIDATE_PAGE;
		}
		catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.WRONG);
			return "redirect:"+RequestConstants.RECRUITMENT_JOB_CANDIDATES_URL;
		}
		
	}
	/**
	 * method to handle request for job candidate edit page
	 * @param model
	 * @param jobCandidatesVo
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_CANDIDATE_EDIT_URL,method=RequestMethod.GET)
	public String editJobCandidatesPageGetRequestHandler(final Model model,@ModelAttribute JobCandidatesVo jobCandidatesVo,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try{
			model.addAttribute("jobCandidatesVo" , candidateService.findJobCandidatesById(Long.parseLong(request.getParameter("cid"))));
			model.addAttribute("jobFields",jobFieldsService.findAllJobField());
			model.addAttribute("designations",designationService.listAllDesignations());
			model.addAttribute("countries",countryService.findAllCountry());
			model.addAttribute("degrees",degreeService.findAllQualificationDegree());
			model.addAttribute("skills",skillService.findAllSkills());
			model.addAttribute("languages",languageService.findAllLanguage());
			//model.addAttribute("status",statusService.listAllStatus());
			
			return PageNameConstatnts.ADD_JOB_CANDIDATES_PAGE;
		}
		catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.WRONG);
			return "redirect:"+RequestConstants.RECRUITMENT_JOB_CANDIDATES_URL;
		}
		
	}
	/**
	 * method to handle job candidate delete request
	 * @param model
	 * @param jobCandidatesVo
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_CANDIDATE_DELETE_URL,method=RequestMethod.GET)
	public String deleteJobCandidatesPageGetRequestHandler(final Model model,@ModelAttribute JobCandidatesVo jobCandidatesVo,HttpServletRequest request){
		try{
			
			candidateService.deleteJobCandidateById(Long.parseLong(request.getParameter("cid")));
			model.addAttribute(MessageConstants.SUCCESS,MessageConstants.JOB_CANDIDATE_DELETE_SUCCES);
			return "redirect:"+RequestConstants.RECRUITMENT_JOB_CANDIDATES_URL;
		}
		catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,"Candidate is Assigned for Interview");
			return "redirect:"+RequestConstants.RECRUITMENT_JOB_CANDIDATES_URL;
		}
		
	}
	/**
	 * method to handle request for resume upload
	 * @param model
	 * @param candidatesVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_CANDIDATE_UPLOAD_RESUME,method=RequestMethod.POST)
	public String uploadResume(final Model model, @ModelAttribute  JobCandidatesVo candidatesVo){
		try{

			if(candidatesVo.getJobCandidateId()!=null){
				CandidateResumeVo resumeVo = uploadResume(candidatesVo.getUploadCandResume());
				if(resumeVo.getResumeFileName()!=null)
				{
					candidatesVo.setResumeVo(uploadResume(candidatesVo.getUploadCandResume()));
					candidateService.updateCandidateResume(candidatesVo);
					model.addAttribute(MessageConstants.SUCCESS,MessageConstants.CANDIDATE_RES_SAVE_SUCCESS);
				}
				else
					model.addAttribute(MessageConstants.ERROR,MessageConstants.CANDIDATE_RES_SAVE_FAIL);
			}
			else
			{
				model.addAttribute(MessageConstants.ERROR,MessageConstants.CANDIDATE_RES_SAVE_FAIL);				
			}
						
		}catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.CANDIDATE_RES_SAVE_FAIL);
		}
		return "redirect:"+RequestConstants.JOB_CANDIDATE_EDIT_URL+"?cid="+candidatesVo.getJobCandidateId();
		//return "redirect:"+RequestConstants.RECRUITMENT_JOB_CANDIDATES_URL;
	}
	
	/**
	 * method to upload resume
	 * @param files
	 * @return
	 */
	private CandidateResumeVo uploadResume (MultipartFile file){
		CandidateResumeVo resumeVo=new CandidateResumeVo();
		try{
			   if(!file.isEmpty()){
					String fileAccessUrl=FileUpload.uploadDocument(MessageConstants.CANDIDATE_MODULE, file);
					if(fileAccessUrl!=null){
						resumeVo.setResumeFileName(file.getOriginalFilename());
						resumeVo.setResumeUrl(fileAccessUrl);
					}
				}
				
			
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return resumeVo;
	}
	/**
	 * method to handle request for resume upload
	 * @param model
	 * @param candidatesVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_CANDIDATE_UPLOAD_DOCS,method=RequestMethod.POST)
	public String uploadCandDocs(final Model model, @ModelAttribute  JobCandidatesVo candidatesVo){
		try{

			if(candidatesVo.getJobCandidateId()!=null){
				
				Set<CandidateDocumentsVo> docs = uploadDocuments(candidatesVo.getUploadCandDocs());
				if(docs.size()!=0)
				{
					candidatesVo.setCandidateDocumentsVos(uploadDocuments(candidatesVo.getUploadCandDocs()));
					candidateService.updateCandidateDocuments(candidatesVo);
					model.addAttribute(MessageConstants.SUCCESS,MessageConstants.CANDIDATE_DOC_SAVE_SUCCESS);
				}
				else
					model.addAttribute(MessageConstants.ERROR,MessageConstants.CANDIDATE_DOC_SAVE_FAIL);
			}
			else
			{
				model.addAttribute(MessageConstants.ERROR,MessageConstants.CANDIDATE_DOC_SAVE_FAIL);
			}
			
						
		}catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.WRONG);
		}
		return "redirect:"+RequestConstants.JOB_CANDIDATE_EDIT_URL+"?cid="+candidatesVo.getJobCandidateId();
		//return "redirect:"+RequestConstants.RECRUITMENT_JOB_CANDIDATES_URL;
	}
	
	/**
	 * method to upload candidate documents
	 * @param files
	 * @return
	 */
	private Set<CandidateDocumentsVo> uploadDocuments(List<MultipartFile> files){
		Set<CandidateDocumentsVo> documentsVos=new HashSet<CandidateDocumentsVo>();
		try{
			for(MultipartFile file: files){
				if(!file.isEmpty()){
					String fileAccessUrl=FileUpload.uploadDocument(MessageConstants.CANDIDATE_MODULE, file);
					if(fileAccessUrl!=null){
						CandidateDocumentsVo documentsVo=new CandidateDocumentsVo();
						documentsVo.setFileName(file.getOriginalFilename());
						documentsVo.setDocumentUrl(fileAccessUrl);
						documentsVos.add(documentsVo);
					}
				}
				
			}
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return documentsVos;
	}

	/**
	 * method to download document
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_CANDIDATE_DWNLD_DOCS,method=RequestMethod.GET)
	public void downloadDocument(final Model model, HttpServletRequest request , HttpServletResponse response){
		try{
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(request.getParameter("durl"), response);
		}catch(Exception e){
			logger.error(e.getMessage());
		}
	}
	

	/** 
	 * method to delete document
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_CANDIDATE_DELETE_DOCS,method=RequestMethod.GET)
	public String deleteDocument(final Model model, HttpServletRequest request){
		try{
			String url=request.getParameter("durl");
			SFTPOperation operation = new SFTPOperation();
			Boolean result=operation.removeFileFromSFTP(url);
			if(result){
				candidateService.deleteDocument(url);
				model.addAttribute(MessageConstants.SUCCESS,MessageConstants.CANDIDATE_DOCUMENT_DELETE_SUCCESS);
			}
		}catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.CANDIDATE_DOCUMENT_DELETE_FAILED);
		}
		return "redirect:"+RequestConstants.RECRUITMENT_JOB_CANDIDATES_URL;
	}
	/**
	 * method to delete candidate resume
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_CANDIDATE_DELETE_RESUME,method=RequestMethod.GET)
	public String deleteResume(final Model model, HttpServletRequest request){
		try{
			String url=request.getParameter("durl");
			SFTPOperation operation = new SFTPOperation();
			Boolean result=operation.removeFileFromSFTP(url);
			if(result){
				candidateService.deleteResume(url);
				model.addAttribute(MessageConstants.SUCCESS,MessageConstants.CANDIDATE_RES_DEL_SUCCESS);
			}
			else
			{
				model.addAttribute(MessageConstants.ERROR,MessageConstants.CANDIDATE_RES_DEL_FAIL);
			}
		}catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.CANDIDATE_RES_DEL_FAIL);
		}
		return "redirect:"+RequestConstants.RECRUITMENT_JOB_CANDIDATES_URL;
	}
	
}
