package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.JobCandidates;
import com.hrms.web.logger.Log;
import com.hrms.web.mail.ApplicationMailSender;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.CandidatesStatusService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.JobCandidateService;
import com.hrms.web.service.JobInterviewService;
import com.hrms.web.service.JobPostService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeDocumentsVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.InterviewDocumentsVo;
import com.hrms.web.vo.JobCandidatesVo;
import com.hrms.web.vo.JobInterviewVo;
import com.hrms.web.vo.JobPostVo;
import com.hrms.web.vo.MeetingsVo;
import com.hrms.web.vo.SessionRolesVo;
import com.sun.mail.handlers.message_rfc822;

/**
 * 
 * @author Vinutha
 * @since 23-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class JobInterviewController {
	
	@Autowired
	private JobInterviewService jobInterviewService;
	
	@Autowired
	private JobPostService jobPostService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private CandidatesStatusService candidatesStatusService;
	
	@Autowired
	private JobCandidateService candidatesService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	@Qualifier(value = "taskExecutor")
	private TaskExecutor taskExecutor;
	
	@Log
	private Logger logger;
	/**
	 * method to handle GET request for job interview page
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.RECRUITMENT_JOB_INTERVIEWS_URL,method=RequestMethod.GET)
	public String jobInterviewPageGetRequestHandler(final Model model,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		String msg=request.getParameter(MessageConstants.SUCCESS);
		String errMsg=request.getParameter(MessageConstants.ERROR);
		if(msg!=null){
			model.addAttribute(MessageConstants.SUCCESS,msg);
		}
		else if(errMsg!=null)
		{
			model.addAttribute(MessageConstants.ERROR,errMsg);
		}
		try
		{
			model.addAttribute("interviews",jobInterviewService.listAllInterviews());
		}catch(Exception e)
		{
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.RECRUITMENT_JOB_INTERVIEWS_PAGE;
	}
	/**
	 * method to handle GET request for view page
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_INTERVIEW_VIEW_URL,method=RequestMethod.GET)
	public String viewInterviewPageGetRequestHandler(final Model model,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try
		{
			model.addAttribute("interview",jobInterviewService.findJobInterviewById(Long.parseLong(request.getParameter("iid"))));
			return PageNameConstatnts.VIEW_JOB_INTERVIEWS_PAGE;
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.WRONG);
			return "redirect:"+RequestConstants.RECRUITMENT_JOB_INTERVIEWS_URL;
		}
		
	}
	
	@RequestMapping(value=RequestConstants.JOB_INTERVIEW_EDIT_URL,method=RequestMethod.GET)
	public String editInterviewPageGetRequestHandler(final Model model,@ModelAttribute JobInterviewVo jobInterviewVo, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try
		{
			model.addAttribute("jobInterviewVo",jobInterviewService.findJobInterviewById(Long.parseLong(request.getParameter("iid"))));
			model.addAttribute("jobPosts",jobPostService.listAllJobPosts());
			model.addAttribute("interviewers",employeeService.listEmployeesDeetails());
			model.addAttribute("status",candidatesStatusService.listAllStatus());
			return PageNameConstatnts.ADD_JOB_INTERVIEWS_PAGE;
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.WRONG);
			return PageNameConstatnts.RECRUITMENT_JOB_INTERVIEWS_PAGE;
		}
		
	}
	/**
	 * method to handle request for add interview page GET 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.ADD_JOB_INTERVIEWS_URL,method=RequestMethod.GET)
	public String addJobInterviewPageGetRequestHandler(final Model model,@ModelAttribute JobInterviewVo jobInterviewVo,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try{
			model.addAttribute("jobPosts",jobPostService.listAllJobPosts());
			model.addAttribute("interviewers",employeeService.listEmployeesDeetails());
			model.addAttribute("status",candidatesStatusService.listAllStatus());
		}catch(Exception e)
		{
			logger.error(e.getMessage());
			return PageNameConstatnts.RECRUITMENT_JOB_INTERVIEWS_PAGE;
		}
		return PageNameConstatnts.ADD_JOB_INTERVIEWS_PAGE;
	}
	/**
	 * method to handle request for saving interview basic info
	 * @param interviewVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.JOB_INTERVIEW_SAVE_BASIC_INFO_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveJobInterviewInfo(@RequestBody JobInterviewVo jobInterviewVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(jobInterviewVo.getJobInterviewId()==null){
				Long id=jobInterviewService.saveJobInterviewInfo(jobInterviewVo);
				jobInterviewVo.setJobInterviewId(id);
				jsonResponse.setResult(id);
				jsonResponse.setStatus(MessageConstants.JOB_INTERVIEW_SAVE_SUCCESS);
		}else{
			jobInterviewService.updateJobInterviewInfo(jobInterviewVo);
			jsonResponse.setStatus(MessageConstants.JOB_INTERVIEW_UPDATE_SUCCESS);
		}
			JobInterviewVo interviewVoForMail = jobInterviewService.findJobInterviewById(jobInterviewVo.getJobInterviewId());
			this.sendInterviewInvitaionMail(interviewVoForMail);
			this.sendInterviewNotificationMail(interviewVoForMail);
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_INTERVIEW_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to handle save request for description
	 * @param interviewVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.JOB_INTERVIEW_SAVEDESCRIPTION_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveJobInterviewDesc(@RequestBody JobInterviewVo interviewVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(interviewVo.getJobInterviewId()!=null){
				jobInterviewService.saveInterviewDesc(interviewVo);
				jsonResponse.setStatus(MessageConstants.JOB_INTERVIEW_SAVE_SUCCESS);
		}else{
			jsonResponse.setStatus(MessageConstants.JOB_INTERVIEW_SAVE_FAILED);
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_INTERVIEW_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to ave additional information
	 * @param interviewVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.JOB_INTERVIEW_SAVE_ADD_INFO_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveJobInterviewAddInfo(@RequestBody JobInterviewVo interviewVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			if(interviewVo.getJobInterviewId()!=null){
				jobInterviewService.saveInterviewAddInfo(interviewVo);
				jsonResponse.setStatus(MessageConstants.JOB_INTERVIEW_SAVE_SUCCESS);
		}else{
			jsonResponse.setStatus(MessageConstants.JOB_INTERVIEW_SAVE_FAILED);
		}
			
		}catch(Exception e){
			logger.error(e.getMessage());
			jsonResponse.setStatus(MessageConstants.JOB_INTERVIEW_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * method to upload interview document
	 * @param model
	 * @param employeeVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_INTERVIEW_UPLOAD_DOCS,method=RequestMethod.POST)
	public String uploadDocs(final Model model, @ModelAttribute  JobInterviewVo interviewVo){
		try{
			if(interviewVo.getJobInterviewId()!=null){
				interviewVo.setDocumentVos(uploadDocuments(interviewVo.getIntUploadDocs()));
				jobInterviewService.updateDocuments(interviewVo);
				model.addAttribute(MessageConstants.SUCCESS,MessageConstants.INTERVIEW_DOCUMENT_SAVE_SUCCESS);
			}
						
		}catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.INTERVIEW_DOCUMENT_SAVE_FAILED);
		}
		return "redirect:"+RequestConstants.RECRUITMENT_JOB_INTERVIEWS_URL;
	}
	
	/**
	 * method to upload files
	 * @param files
	 * @return
	 */
	private List<InterviewDocumentsVo> uploadDocuments(List<MultipartFile> files){
		List<InterviewDocumentsVo> documentsVos=new ArrayList<InterviewDocumentsVo>();
		try{
			for(MultipartFile file: files){
				if(!file.isEmpty()){
					String fileAccessUrl=FileUpload.uploadDocument(MessageConstants.INTERVIEW_MODULE, file);
					if(fileAccessUrl!=null){
						InterviewDocumentsVo documentsVo=new InterviewDocumentsVo();
						documentsVo.setFileName(file.getOriginalFilename());
						documentsVo.setDocumentUrl(fileAccessUrl);
						documentsVos.add(documentsVo);
					}
				}
				
			}
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return documentsVos;
	}

	
	/** 
	 * method to delete document
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_INTERVIEW_DELETE_DOCS,method=RequestMethod.GET)
	public String deleteDocument(final Model model, HttpServletRequest request){
		try{
			String url=request.getParameter("durl");
			SFTPOperation operation = new SFTPOperation();
			Boolean result=operation.removeFileFromSFTP(url);
			if(result){
				jobInterviewService.deleteDocument(url);
				model.addAttribute(MessageConstants.SUCCESS,MessageConstants.INTERVIEW_DOCUMENT_DELETE_SUCCESS);
			}
		}catch(Exception e){
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR,MessageConstants.INTERVIEW_DOCUMENT_DELETE_FAILED);
		}
		return "redirect:"+RequestConstants.RECRUITMENT_JOB_INTERVIEWS_URL;
	}
	
	/**
	 * method to download document
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.JOB_INTERVIEW_DWNLD_DOCS,method=RequestMethod.GET)
	public void downloadDocument(final Model model, HttpServletRequest request , HttpServletResponse response){
		try{
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(request.getParameter("durl"), response);
		}catch(Exception e){
			logger.error(e.getMessage());
		}
	}
	/**
	 * method to delete job interview
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value=RequestConstants.JOB_INTERVIEW_DELETE_URL,method=RequestMethod.GET)
	public String deleteInterview(final Model model, HttpServletRequest request , HttpServletResponse response){
		try{
			jobInterviewService.deleteJobInterviewById(Long.parseLong(request.getParameter("iid")));
			model.addAttribute(MessageConstants.SUCCESS,MessageConstants.JOB_INTERVIEW_DELETE_SUCCESS);
		}catch(Exception e){
			model.addAttribute(MessageConstants.ERROR,MessageConstants.JOB_INTERVIEW_DELETE_FAILED);
			logger.error(e.getMessage());
		}
		return "redirect:"+RequestConstants.RECRUITMENT_JOB_INTERVIEWS_URL;
	}
	/**
	 * Method to create mail for interviewees
	 * @param interviewVo
	 */
	private void sendInterviewInvitaionMail(JobInterviewVo interviewVo) {
		try {
			Map<String, String> employees = new HashMap<String, String>();
			for(Long id : interviewVo.getIntervieweeIds()){
				JobCandidates can = candidatesService.findAndReturnJobCandidatesObjectWithId(id);
				String email = null;
				String name = null;
				if(can.getAddress()!=null)
					email = can.getAddress().geteMail();
					name = can.getFirstName()+" "+can.getLastName();
				if(email!=null && name!=null)
					employees.put(email, name);
			}
			String user = interviewVo.getCreatedBy();
			String sender = userService.findUser(user).getEmployee();
			String time = interviewVo.getInterviewTime();
			String venue = interviewVo.getPlace();
			Date date = DateFormatter.convertStringToDate(interviewVo.getInterviewDate());
			ApplicationMailSender applicationMailSender = new ApplicationMailSender(employees , CommonConstants.INTERVIEW_INVITAION_MAIL,
					sender , date , time , venue );
			taskExecutor.execute(applicationMailSender);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	/**
	 * Method to create mail for interviewers
	 * @param interviewVo
	 */
	private void sendInterviewNotificationMail(JobInterviewVo interviewVo) {
		try {
			Map<String, String> employees = new HashMap<String, String>();
			
			for(Long id : interviewVo.getInterViewerIds()){
				Employee interviewer = employeeService.findAndReturnEmployeeById(id);
				String email = null;
				String name = null;
				if(interviewer.getUserProfile()!=null)
					name = interviewer.getUserProfile().getFirstName()+" "+interviewer.getUserProfile().getLastname();
				if(interviewer.getUser()!=null)
						email = interviewer.getUser().getEmail();
				if(email!=null && name!=null)
					employees.put(email, name);
			}
			String candidates = "Candidates Are <Br>";
			List<String> candidateList = interviewVo.getInterviewees();
			int count =0;
			for (String string : candidateList) {
				count++;
				
				candidates +=String.valueOf(count)+") "+string+"<br>";
			}
			String user = interviewVo.getCreatedBy();
			String sender = userService.findUser(user).getEmployee();
			String time = interviewVo.getInterviewTime();
			String venue = interviewVo.getPlace();
			Date date = DateFormatter.convertStringToDate(interviewVo.getInterviewDate());
			ApplicationMailSender applicationMailSender = new ApplicationMailSender(employees , CommonConstants.INTERVIEW_NOTIFICATION_MAIL,
					sender , date , time , venue , candidates );
			taskExecutor.execute(applicationMailSender);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
}
