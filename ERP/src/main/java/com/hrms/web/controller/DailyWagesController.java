package com.hrms.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.DailyWagesService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.vo.DailyWagesVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 16-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class DailyWagesController {

	@Log
	private Logger logger;

	@Autowired
	private DailyWagesService dailyWagesService;

	@Autowired
	private EmployeeService employeeService;

	/**
	 * method to reach daily wages home
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DAILY_WAGES_URL, method = RequestMethod.GET)
	public String dailyWagesHome(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);

			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("wages",
					dailyWagesService.findAllDailyWagesByEmployee(authEmployeeDetails.getEmployeeCode()));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.DAILY_WAGES_PAGE;
	}

	/**
	 * method to add new daily wages
	 * 
	 * @param model
	 * @param dailyWagesVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DAILY_WAGES_ADD, method = RequestMethod.GET)
	public String addDailyWages(final Model model, @ModelAttribute DailyWagesVo dailyWagesVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.DAILY_WAGES_ADD_PAGE;
	}

	/**
	 * method to edit daily wages
	 * 
	 * @param model
	 * @param request
	 * @param dailyWagesVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DAILY_WAGES_EDIT, method = RequestMethod.GET)
	public String editDailyWages(final Model model, HttpServletRequest request,
			@ModelAttribute DailyWagesVo dailyWagesVo, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);

			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("dailyWagesVo",
					dailyWagesService.getDailyWagesById(Long.parseLong(request.getParameter("id"))));
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.DAILY_WAGES_ADD_PAGE;
	}

	/**
	 * method to delete daily wages
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DAILY_WAGES_DELETE, method = RequestMethod.GET)
	public String deleteDailyWages(final Model model, HttpServletRequest request) {
		try {
			dailyWagesService.deleteDailyWage(Long.parseLong(request.getParameter("id")));
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.DAILY_WAGES_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.DAILY_WAGES_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.DAILY_WAGES_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.DAILY_WAGES_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.DAILY_WAGES_DELETE_SUCCESS;
	}

	/**
	 * method to view daily wages
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DAILY_WAGES_VIEW, method = RequestMethod.GET)
	public String viewDailyWages(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("wage", dailyWagesService.getDailyWagesById(Long.parseLong(request.getParameter("id"))));
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.DAILY_WAGES_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.DAILY_WAGES_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.WRONG;
		}
		return PageNameConstatnts.DAILY_WAGES_VIEW_PAGE;
	}

	/**
	 * method to save daily wage
	 * 
	 * @param model
	 * @param dailyWagesVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DAILY_WAGES_SAVE, method = RequestMethod.POST)
	public String saveDailyWages(final Model model, @ModelAttribute DailyWagesVo dailyWagesVo) {
		try {
			if (dailyWagesVo.getId() == null) {
				dailyWagesVo.setId(dailyWagesService.saveDailyWage(dailyWagesVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.DAILY_WAGES_SAVE_SUCCESS);
			} else {
				dailyWagesService.updateDailyWage(dailyWagesVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.DAILY_WAGES_UPDATE_SUCCESS);
			}
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.DAILY_WAGES_SAVE_FAILED);
		}
		return "redirect:" + RequestConstants.DAILY_WAGES_EDIT + "?id=" + dailyWagesVo.getId();
	}

	/**
	 * method to get daily wages by employee
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DAILY_WAGES_GET_EMPLOYEE_WAGES, method = RequestMethod.POST)
	public @ResponseBody JsonResponse viewDailyWages(final Model model,
			@RequestParam(value = "employeeCode", required = true) String employeeCode) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(dailyWagesService.getDailyWagesByEmployeeId(employeeCode));
			jsonResponse.setStatus("ok");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}
}
