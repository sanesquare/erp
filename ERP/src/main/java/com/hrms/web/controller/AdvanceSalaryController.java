package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Superiors;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.notifications.NotificationContent;
import com.hrms.web.notifications.PushNotification;
import com.hrms.web.service.AdvanceSalaryService;
import com.hrms.web.service.AdvanceSalaryStatusService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.SecondLevelForwaderService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AdvanceSalaryVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Jithin Mohan
 * @since 18-April-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "thisUser" , "roles" })
public class AdvanceSalaryController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private AdvanceSalaryService advanceSalaryService;

	@Autowired
	private AdvanceSalaryStatusService advanceSalaryStatusService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private PushNotification pushNotification;

	@Autowired
	private UserService userService;

	@Autowired
	private SecondLevelForwaderService secondLevelForwaderService;

	/**
	 * method to load all the advance salary request
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADVANCE_SALARY_URL, method = RequestMethod.GET)
	public String getAdvanceSalaryListPage(final Model model, @ModelAttribute("thisUser") String thisUser,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);

		try {
			model.addAttribute("advanceSalaryList",
					advanceSalaryService.findAllAdvanceSalaryByEmployee(authEmployeeDetails.getEmployeeCode()));
			model.addAttribute("advanceSalaryVo", new AdvanceSalaryVo());
			model.addAttribute("thisUser", thisUser);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return PageNameConstatnts.GET_ADVANCE_SALARY_PAGE;
	}

	/**
	 * method to load add advance Salary page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_ADVANCE_SALARY_URL, method = RequestMethod.GET)
	public String getAddAdvanceSalaryPage(final Model model, @ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute AdvanceSalaryVo advanceSalaryVo, BindingResult result,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			advanceSalaryVo.setEmployee(authEmployeeDetails.getName()+" ("+authEmployeeDetails.getEmployeeCode()+")");
			advanceSalaryVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());

			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("advanceSalaryVo", advanceSalaryVo);
			if (advanceSalaryVo.getAdvanceSalaryId() == null) {
				advanceSalaryVo.setRecordedBy(authEmployeeDetails.getName());
				model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
			} else {
				advanceSalaryVo.setRecordedBy(userService.findUser(advanceSalaryVo.getRecordedBy()).getEmployee());
				model.addAttribute(MessageConstants.CREATED_ON, advanceSalaryVo.getRecordedOn());
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ADD_ADVANCE_SALARY_PAGE;
	}

	/**
	 * method to save advance salary
	 * 
	 * @param advanceSalaryVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_ADVANCE_SALARY_URL, method = RequestMethod.POST)
	public ModelAndView saveAdvanceSalaryPage(@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute AdvanceSalaryVo advanceSalaryVo, RedirectAttributes redirectAttributes){
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_ADVANCE_SALARY_URL));
		try {
			advanceSalaryService.saveOrUpdateAdvanceSalary(advanceSalaryVo);
			pushNotification.sendNotification(createNotificationContent(advanceSalaryVo,
					authEmployeeDetails.getProfilePic()));

			redirectAttributes.addFlashAttribute("advanceSalaryVo", advanceSalaryVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_ADVANCE_SALARY_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to create NotificationContent object
	 * 
	 * @param terminationVo
	 * @return content
	 */
	private NotificationContent createNotificationContent(AdvanceSalaryVo advanceSalaryVo, String profilePic) {
		NotificationContent content = new NotificationContent();
		List<String> recipients = new ArrayList<String>();

		recipients.addAll(advanceSalaryVo.getForwaders());
		recipients.add(advanceSalaryVo.getEmployeeCode());
		recipients.addAll(secondLevelForwaderService.findAllSecondLevelForwader().getForwaders());

		content.setContent(advanceSalaryVo.getAdvanceSalaryTitle());
		content.setUrl("ViewAdvanceSalary?otp=" + advanceSalaryVo.getAdvanceSalaryId());
		content.setPicture(profilePic);
		content.setRecipients(recipients);
		return content;
	}

	/**
	 * method to update advance salary status
	 * 
	 * @param advanceSalaryVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_ADVANCE_SALARY_STATUSURL, method = RequestMethod.POST)
	public ModelAndView updateAdvanceSalaryStatus(@ModelAttribute AdvanceSalaryVo advanceSalaryVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_ADVANCE_SALARY_URL));
		try {
			advanceSalaryService.updateAdvanceSalaryStatus(advanceSalaryVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS,
					MessageConstants.UPDATE_ADVANCE_SALARY_SUCCESS);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to edit advance salary
	 * 
	 * @param model
	 * @param advanceSalaryId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_ADVANCE_SALARY_URL, method = RequestMethod.GET)
	public String editAdvanceSalaryWithGet(final Model model, @RequestParam(value = "otp") Long advanceSalaryId,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			AdvanceSalaryVo advanceSalaryVo = advanceSalaryService.findAdvanceSalaryById(advanceSalaryId);
			advanceSalaryVo.setRecordedBy(userService.findUser(advanceSalaryVo.getRecordedBy()).getEmployee());
			model.addAttribute("advanceSalaryVo", advanceSalaryVo);
			model.addAttribute("superiorList", Superiors.getSuperiors(employeeService
					.getEmployeesDetails(
							employeeService.findAndReturnEmployeeByCode(advanceSalaryVo.getEmployeeCode()).getId())
					.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute(MessageConstants.CREATED_ON, advanceSalaryVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ADD_ADVANCE_SALARY_PAGE;
	}

	/**
	 * method to view advance salary information
	 * 
	 * @param advanceSalaryId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_ADVANCE_SALARY_URL, method = RequestMethod.GET)
	public String viewAdvanceSalaryInformationWithGet(
			@RequestParam(value = "otp", required = true) Long advanceSalaryId, final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			AdvanceSalaryVo advanceSalaryVo = advanceSalaryService.findAdvanceSalaryById(advanceSalaryId);

			model.addAttribute("advanceSalaryVo", advanceSalaryVo);
			model.addAttribute("advanceSalaryStatusList", advanceSalaryStatusService.findAllAdvanceSalaryStatus());
			model.addAttribute("superiorList", Superiors.getSuperiors(employeeService
					.getEmployeesDetails(
							employeeService.findAndReturnEmployeeByCode(advanceSalaryVo.getEmployeeCode()).getId())
					.getSuperiorSubordinateVo().getSuperiors()));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PageNameConstatnts.VIEW_ADVANCE_SALARY_PAGE;
	}

	/**
	 * method to list all advance salary
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LIST_ALL_ADVANCE_SALARY_URL, method = RequestMethod.GET)
	public String listAllAdvanceSalaryInformations(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("advanceSalaryList", advanceSalaryService.findAllAdvanceSalary());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ADVANCE_SALARY_LIST_PAGE;
	}

	/**
	 * method to delete advance salary information
	 * 
	 * @param advanceSalaryId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_ADVANCE_SALARY_URL, method = RequestMethod.POST)
	public String deleteAdvanceSalaryInformationWithPost(
			@RequestParam(value = "advanceSalaryId", required = true) Long advanceSalaryId, final Model model) {
		try {
			advanceSalaryService.deleteAdvanceSalary(advanceSalaryId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return "redirect:" + RequestConstants.LIST_ALL_ADVANCE_SALARY_URL;
	}
}
