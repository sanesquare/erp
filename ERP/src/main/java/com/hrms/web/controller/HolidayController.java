package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.HolidayService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.HolidayVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Jithin Mohan
 * @since 29-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "holidayVo" ,"roles" })
public class HolidayController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private HolidayService holidayService;

	@Autowired
	private BranchService branchService;

	/**
	 * method to load holidays home page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HOLIDAY_PAGE_URL, method = RequestMethod.GET)
	public String viewListAllHolidaysPage(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("holidayList", holidayService.findAllHoliday());
			model.addAttribute("holidayVo", new HolidayVo());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.HOLIDAYS_PAGE;
	}

	/**
	 * method to load create holiday page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.CREATE_HOLIDAY_PAGE_URL, method = RequestMethod.GET)
	public String viewCreateHolidayPage(final Model model, @ModelAttribute("holidayVo") HolidayVo holidayVo,
			BindingResult result,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		model.addAttribute("holidayVo", holidayVo);
		model.addAttribute("branchList", getBranchList(branchService.listAllBranches()));
		if (holidayVo.getHolidayId() == null) {
			model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
		} else {
			model.addAttribute(MessageConstants.CREATED_ON, holidayVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		}
		return PageNameConstatnts.CREATE_HOLIDAY_PAGE;
	}

	/**
	 * method to get all branch names
	 * 
	 * @param branchs
	 * @return
	 */
	private List<String> getBranchList(List<BranchVo> branchs) {
		List<String> branchList = new ArrayList<String>();
		for (BranchVo branch : branchs)
			branchList.add(branch.getBranchName());
		return branchList;
	}

	/**
	 * method to load holiday view page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_HOLIDAY_PAGE_URL, method = RequestMethod.GET)
	public String viewHolidayViewPage(final Model model,
			@RequestParam(value = "otp_hid", required = true) Long holidayId,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("holiday", holidayService.findHolidayById(holidayId));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.VIEW_HOLIDAY_PAGE;
	}

	/**
	 * method to save holiday
	 * 
	 * @param holidayVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_HOLIDAY_PAGE_URL, method = RequestMethod.POST)
	public ModelAndView saveHolidayDetailsWithPost(@ModelAttribute HolidayVo holidayVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.CREATE_HOLIDAY_PAGE_URL));
		try {
			holidayService.saveHoliday(holidayVo);
			redirectAttributes.addFlashAttribute("holidayVo", holidayVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_HOLIDAY_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to load update page
	 * 
	 * @param model
	 * @param holidayId
	 * @param holidayVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_HOLIDAY_PAGE_URL, method = RequestMethod.GET)
	public String updateHolidayViewPage(final Model model,
			@RequestParam(value = "otp_hid", required = true) Long holidayId, @ModelAttribute HolidayVo holidayVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			holidayVo = holidayService.findHolidayById(holidayId);
			model.addAttribute("holidayVo", holidayVo);
			model.addAttribute("branchList", getBranchList(branchService.listAllBranches()));
			model.addAttribute(MessageConstants.CREATED_ON, holidayVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.CREATE_HOLIDAY_PAGE;
	}

	/**
	 * method to list all holidays
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LIST_ALL_HOLIDAY_PAGE_URL, method = RequestMethod.GET)
	public String listAllHolidays(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("holidayList", holidayService.findAllHoliday());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.HOLIDAY_LIST_PAGE;
	}

	/**
	 * method to delete holiday
	 * 
	 * @param holidayId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_HOLIDAY_PAGE_URL, method = RequestMethod.POST)
	public String deleteHolidayWithPost(@RequestParam(value = "holidayId", required = true) Long holidayId) {
		try {
			holidayService.deleteHoliday(holidayId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.LIST_ALL_HOLIDAY_PAGE_URL;
	}
}
