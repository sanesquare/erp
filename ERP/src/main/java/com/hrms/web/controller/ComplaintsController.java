package com.hrms.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.service.ComplaintsService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.ComplaintsVo;
import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.PromotionVo;



@Controller
@SessionAttributes({ "complaintsVo" })
public class ComplaintsController {
	
	/**
	 * Log
	 */
	@Log
	private static Logger logger;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private ComplaintsService complaintsService;
	/**
	 * method to handle promotion page get request
	 * 
	 * @param model
	 * @return PageNameConstatnts.PROMOTION_PAGE
	 */
	@RequestMapping(value = RequestConstants.COMPLAINTS_URL, method = RequestMethod.GET)
	public String complaintsPageGetRequestHandler(final Model model,HttpServletRequest request) {
		logger.info("inside>> ComplaintsController... complaintsPageMappingGet");
		
		try {
			model.addAttribute("complaintsVo",new ComplaintsVo());
			model.addAttribute("complaints", complaintsService.listAllComplaintDetails());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.COMPLAINTS_PAGE;
	}
	
	
	/**
	 * method to add department
	 * 
	 * @param model
	 * @param
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_COMPLAINTS_URL, method = RequestMethod.GET)
	public String addPromotionPageGetRequestHandler(final Model model,@ModelAttribute("complaintsVo") ComplaintsVo complaintsVo) {
		logger.info("inside>> ComplaintsController... addComplaintsPageMappingGet");
		try {
			model.addAttribute("employee",employeeService.listAllEmployee());
			model.addAttribute("complaintStatus",complaintsService.listAllComplaintStatusDetails());

		} catch (Exception e) {
			logger.info(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_COMPLAINTS_PAGE;
	}
	
	
	/**
	 * method to save Department
	 * 
	 * @param model
	 * @param departmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_COMPLAINTS_URL, method = RequestMethod.POST)
	public ModelAndView savePromotion(@ModelAttribute ComplaintsVo complaintsVo,RedirectAttributes redirectAttributes) {
		logger.info("inside>> Promotion Controller... saveComplaints");
		ModelAndView view=new ModelAndView(new RedirectView(PageNameConstatnts.ADD_COMPLAINTS_PAGE));
		try {
			
			if(complaintsVo.getId()!=null)
			{				
				complaintsService.updateComplaint(complaintsVo);
			}
			else
			{
				complaintsService.saveComplaints(complaintsVo);
			}
			redirectAttributes.addFlashAttribute("complaintsVo", complaintsVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, RequestConstants.ADD_COMPLAINTS_URL);
			
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		//return PageNameConstatnts.COMPLAINTS_PAGE;
		return view;
	}
	
	/**
	 * method to delete promotion
	 * 
	 * @param model
	 * @param request
	 * @param promotiontVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_COMPLAINTS_URL, method = RequestMethod.GET)
	public String deletePromotion(final Model model, HttpServletRequest request, @ModelAttribute ComplaintsVo complaintsVo) {
		logger.info("inside>> PromotionController... deletePromotion");
		try {
			complaintsService.deleteComplaint(Long.parseLong(request.getParameter("pid")));
			model.addAttribute("complaints", complaintsService.listAllComplaintDetails());
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.COMPLAINT_DELETE_SUCCESS);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.COMPLAINTS_PAGE;

	}
	
	/**
	 * method to reach edit department page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_COMPLAINTS_URL, method = RequestMethod.GET)
	public String editPromotionDetailsGet(final Model model, HttpServletRequest request,
			@ModelAttribute ComplaintsVo complaintsVo) {
		logger.info("inside>> ComplaintsController... editComplaintsDetailsGet");
		try {
			model.addAttribute("complaintsVo",complaintsService.findComplaintById(Long.parseLong(request.getParameter("pid"))));
			model.addAttribute("employee",employeeService.listAllEmployee());
			model.addAttribute("complaintStatus",complaintsService.listAllComplaintStatusDetails());

		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.EDIT_COMPLAINTS_PAGE;
	}
	
	/**
	 * method to update department
	 * 
	 * @param model
	 * @param departmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_COMPLAINTS_URL, method = RequestMethod.POST)
	public ModelAndView updatePromotion(final Model model, HttpServletRequest request, @ModelAttribute ComplaintsVo complaintsVo,RedirectAttributes redirectAttributes) {
		logger.info("inside>> ComplaintsController... updateComplaints");
		ModelAndView view=new ModelAndView(new RedirectView(PageNameConstatnts.ADD_COMPLAINTS_PAGE));
		try {
			complaintsService.updateComplaint(complaintsVo);
			model.addAttribute("complaints", complaintsService.listAllComplaintDetails());
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.COMPLAINT_UPDATE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return view;
	}

	
	/**
	 * method to delete document
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_COMPLAINTS_DOCUMENT, method = RequestMethod.GET)
	public String deleteDocumentGet(final Model model, HttpServletRequest request) {
		String url = request.getParameter("durl");
		Long document_id = Long.parseLong(request.getParameter("doc_id"));
		Boolean result = false;
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			result = sftpOperation.removeFileFromSFTP(url);
		} catch (Exception e) {
			e.printStackTrace();
			return PageNameConstatnts.COMPLAINTS_PAGE;
		}
		if (result) {
			try {
				complaintsService.deleteComplaintsDocument(document_id);
				model.addAttribute(MessageConstants.ERROR, MessageConstants.COMPLAINT_DOCUMENT_DELETE_SUCCESS);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return PageNameConstatnts.COMPLAINTS_PAGE;
		} else {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.COMPLAINT_DOCUMENT_DELETE_FAIL);
			return PageNameConstatnts.COMPLAINTS_PAGE;
		}
	}

	/**
	 * method to download and show document
	 * 
	 * @param model
	 * @param request
	 * @param projectVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_COMPLAINTS_DOCUMENT, method = RequestMethod.GET)
	public void viewDocumentGet(final Model model, HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute ComplaintsVo complaintsVo) {
		String url = request.getParameter("durl");
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			sftpOperation.downloadFileFromSFTP(url, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
