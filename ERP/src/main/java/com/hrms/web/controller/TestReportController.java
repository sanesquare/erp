package com.hrms.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hrms.web.vo.BranchVo;

@Controller
@RequestMapping("/pdf/")
public class TestReportController {

	@RequestMapping(value = "download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public String testReport(ModelMap map, @RequestParam("type") String type) {

		Map<String, Object> parameterMap = new HashMap<String, Object>();

		List<BranchVo> branchVo = new ArrayList<BranchVo>();
		BranchVo bVo = new BranchVo();
		bVo.setBranchName("Snogol");
		branchVo.add(bVo);
		JRDataSource JRdataSource = new JRBeanCollectionDataSource(branchVo);
		parameterMap.put("datasource", JRdataSource);

		map.addAllAttributes(parameterMap);
		// modelAndView = new ModelAndView("pdfReport",parameterMap);

		/*switch (type) {
		case "pdf":
			return "pdfReport";
			
		case "xls":
			return "xlsReport";
			
		default:
			return null;
			
		}*/
		
		if("pdf".equals(type)){
			return "pdfReport";
		}else if("xls".equals(type)){
			return "xlsReport";
		}else{
			return null;
		}

	}

}
