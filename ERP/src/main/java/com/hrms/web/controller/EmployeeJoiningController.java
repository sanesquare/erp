package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeDesignationService;
import com.hrms.web.service.EmployeeJoiningService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.EmployeeTypeService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeDetailsVo;
import com.hrms.web.vo.EmployeeDocumentsVo;
import com.hrms.web.vo.EmployeeJoiningVo;
import com.hrms.web.vo.JoiningJsonVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.SuperiorVo;

/**
 * 
 * @author shamsheer
 * @since 23-March-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails" , "roles","autoApprove"})
public class EmployeeJoiningController {

	@Autowired
	private EmployeeJoiningService employeeJoiningService;
	
	@Autowired
	private EmployeeDesignationService designationService;
	
	@Autowired
	private BranchService branchService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private EmployeeTypeService employeeTypeService;
	
	
	@RequestMapping(value=RequestConstants.EMPOYEE_JOINING, method = RequestMethod.GET)
	public String employeeJoiningGet(final Model model ,  HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try{
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if(successMessage!=null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if(errorMessage!=null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			
			model.addAttribute("joinings", employeeJoiningService.listAllJoinings());
		}catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.EMPLOYEE_JOINING_PAGE;
	}
	
	/**
	 * method to reach employee_joining page
	 * @param model
	 * @param joiningVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.ADD_EMPLOYEE_JOINING,method=RequestMethod.GET)
	public String addEmployeeJoiningGet(final Model model,@ModelAttribute EmployeeJoiningVo employeeJoiningVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try{
			
			model.addAttribute("branch",branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("employeeType", employeeTypeService.findAllEmployeeType());
			model.addAttribute("designation", designationService.listAllDesignations());
			model.addAttribute("employees", employeeService.listAllEmployee());
		}catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.EMPLOYEE_JOINING_ADD_PAGE;
	}
	
	/**
	 * method to get employeed list by department and branch
	 * @param joiningVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.GET_EMPLOYEE_BY_BRANCH_AND_DEPT,method=RequestMethod.POST)
	public @ResponseBody JsonResponse getEmployeesByBranchAndDepartment(@RequestParam(value="branchId" , required=true)Long branchId,
			@RequestParam(value="departmentId" , required=true)Long departmentId){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			jsonResponse.setResult(employeeService.getEmployeesByBranchAndDepartment
					(branchId,departmentId));
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonResponse;
	}
	
	
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_DETAILS_BY_ID, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getEmployeeDetailsById(
			@RequestParam(value = "employeeCode", required = true) String employeeCode,
			@ModelAttribute("autoApprove") Boolean autoApprove) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			EmployeeDetailsVo employeeDetailsVo = employeeJoiningService.getEmployeeDetails(employeeCode);
			List<SuperiorVo> superiorVos = new ArrayList<SuperiorVo>();
			if (autoApprove) {
				SuperiorVo dto = new SuperiorVo();
				dto.setSuperiorName("Auto Approve");
				dto.setSuperiorCode("auto");
				dto.setSuperiorId(0L);
				superiorVos.add(dto);
			}
			superiorVos.addAll(employeeDetailsVo.getSuperiorSubordinateVo().getSuperiors());
			employeeDetailsVo.getSuperiorSubordinateVo().setSuperiors(superiorVos);
			jsonResponse.setResult(employeeDetailsVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}
	
	@RequestMapping(value=RequestConstants.GET_EMPLOYEE_DETAILS_BY_ID_FOR_POP,method=RequestMethod.POST)
	public @ResponseBody JsonResponse getEmployeeById(@RequestParam(value="employeeCode" , required=true)Long  id){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			jsonResponse.setResult(employeeService.findDetailedEmployee(id));
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonResponse;
	}
	
	/**
	 * method to save employee joining
	 * @param model
	 * @param employeeJoiningVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.SAVE_EMPLOYEE_JOINING,method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveEmployeeJoining(@RequestBody JoiningJsonVo employeeJoiningVo){
		JsonResponse jsonResponse = new JsonResponse();
		try{
			if(employeeJoiningVo.getJoiningId()==null){
				if(employeeJoiningService.findJoiningByEmployee(employeeJoiningVo.getEmployeeId())==null){
					Long joiningId = employeeJoiningService.saveEmployeeJoining(employeeJoiningVo);
					jsonResponse.setResult(joiningId);
					jsonResponse.setStatus("ok");
					employeeJoiningVo.setJoiningId(joiningId);
					//employeeJoiningVo.setdId(employeeJoiningVo.getDepartmentId());
					//employeeJoiningVo.setbId(employeeJoiningVo.getBranchId());
				}else{
					jsonResponse.setStatus("bad");
				}
			}else{
					employeeJoiningService.updateJoining(employeeJoiningVo);
					jsonResponse.setStatus("ok");
			}
		}catch(Exception e){
			jsonResponse.setStatus("bad");
			e.printStackTrace();
		}
		
		return jsonResponse;
	}
	
	/**
	 * method to upload docs
	 * @param model
	 * @param employeeJoiningVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.UPLOAD_EMPLOYEE_JOINING_DOC,method=RequestMethod.POST)
	public String uploadDocuments(final Model model, EmployeeJoiningVo employeeJoiningVo){
		if(employeeJoiningVo.getJoiningId()!=null){
			employeeJoiningVo.setDocuments(uploadDocuments
					(employeeJoiningVo.getEmp_join_files_upld()));
			employeeJoiningService.uploadDocuments(employeeJoiningVo);
		}
		
		return "redirect:"+RequestConstants.EMPOYEE_JOINING;
	}
	
	private List<EmployeeDocumentsVo> uploadDocuments(List<MultipartFile> files){
		List<EmployeeDocumentsVo> documentsVos=new ArrayList<EmployeeDocumentsVo>();
		try{
			for(MultipartFile file: files){
				if(!file.isEmpty()){
					String fileAccessUrl=FileUpload.uploadDocument(MessageConstants.EMPLOYEE_JOINING_MODULE, file);
					if(fileAccessUrl!=null){
						EmployeeDocumentsVo documentsVo=new EmployeeDocumentsVo();
						documentsVo.setFileName(file.getOriginalFilename());
						documentsVo.setDocumnetUrl(fileAccessUrl);
						documentsVos.add(documentsVo);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return documentsVos;
	}
	
	/**
	 * method to edit joining
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.EDIT_JOINING_URL,method=RequestMethod.GET)
	public String editJoining(final Model model, HttpServletRequest request
			,@ModelAttribute EmployeeJoiningVo employeeJoiningVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try{
			model.addAttribute("employeeJoiningVo",employeeJoiningService
					.findDetailedJoining(Long.parseLong(request.getParameter("id"))));
			model.addAttribute("branch",branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("employeeType", employeeTypeService.findAllEmployeeType());
			model.addAttribute("designation", designationService.listAllDesignations());
		}catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.EDIT_EMPLOYEE_JOINING;
	}
	
	/**
	 * method to delete joining
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.DELETE_JOININ_URL,method=RequestMethod.GET)
	public String deleteJoining(final Model model, HttpServletRequest request){
		try{
			employeeJoiningService.deleteEmployeeJoining(Long.parseLong(request.getParameter("id")));
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.EMPLOYEE_JOINING_DELETE_SUCCESS);
		}catch(Exception e){
			model.addAttribute(MessageConstants.ERROR, MessageConstants.EMPLOYEE_JOINING_DELETE_FAILED);
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.EMPOYEE_JOINING;
	}
	
	/**
	 * method to download docs
	 * @param request
	 * @param response
	 */
	@RequestMapping(value=RequestConstants.DOWNLOAD_JOINIG_DOCS,method=RequestMethod.GET)
	public void downloadDocument(HttpServletRequest  request, HttpServletResponse response){
		try{
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(request.getParameter("url"), response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * method to delete doc
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_JOINING_DOC, method= RequestMethod.GET)
	public String deletDocument(final Model model, HttpServletRequest request){
		try{
			String url=request.getParameter("url");
			SFTPOperation operation = new SFTPOperation();
			Boolean result=(operation.removeFileFromSFTP(url));
			if(result){
				employeeJoiningService.deleteDocument(url);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.EMPOYEE_JOINING;
	}
	
	/**
	 * method to view join
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.VIEW_JOINING,method=RequestMethod.GET)
	public String viewJoining(Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try{
			model.addAttribute("join", employeeJoiningService
					.findDetailedJoining(Long.parseLong(request.getParameter("id"))));
		}catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.EMPLOYEE_JOININ_VIEW;
	}
}
