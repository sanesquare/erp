package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Superiors;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.CommissionService;
import com.hrms.web.service.CommissionTitleService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ForwardApplicationStatusService;
import com.hrms.web.vo.CommissionTitleVo;
import com.hrms.web.vo.CommissionVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 14-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class CommissionController {

	@Log
	private Logger logger;

	@Autowired
	private CommissionService commissionService;

	@Autowired
	private CommissionTitleService titleService;

	@Autowired
	private ForwardApplicationStatusService statusService;

	@Autowired
	private EmployeeService employeeService;

	/**
	 * method to reach commission home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.COMMISSION_URL, method = RequestMethod.GET)
	public String commissionsHomePage(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("commissions",
					commissionService.findAllCommissionesByEmployee(authEmployeeDetails.getEmployeeCode()));
			model.addAttribute("thisCode", authEmployeeDetails.getEmployeeCode());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.COMMISSION_PAGE;
	}

	/**
	 * method to reach add commission page
	 * 
	 * @param model
	 * @param commissionVo
	 * @param commissionTitleVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.COMMISSION_ADD_URL, method = RequestMethod.GET)
	public String addCommisssionPage(final Model model, @ModelAttribute CommissionVo commissionVo,
			@ModelAttribute CommissionTitleVo commissionTitleVo, @ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			commissionVo.setEmployee(authEmployeeDetails.getName()+" ("+authEmployeeDetails.getEmployeeCode()+")");
			commissionVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("status", statusService.listAllStatus());
			model.addAttribute("titles", titleService.listAllCommissionTitles());
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.COMMISSION_ADD_PAGE;
	}

	/**
	 * method to edit commission
	 * 
	 * @param model
	 * @param request
	 * @param commissionTitleVo
	 * @param commissionVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.COMMISSION_EDIT_URL, method = RequestMethod.GET)
	public String editCommission(final Model model, HttpServletRequest request,
			@ModelAttribute CommissionTitleVo commissionTitleVo, @ModelAttribute CommissionVo commissionVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("status", statusService.listAllStatus());
			model.addAttribute("titles", titleService.listAllCommissionTitles());
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("commissionVo",
					commissionService.getCommissionById(Long.parseLong(request.getParameter("id"))));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.COMMISSION_ADD_PAGE;
	}

	/**
	 * method to view commission
	 * 
	 * @param model
	 * @param commissionVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.COMMISSION_VIEW_URL, method = RequestMethod.GET)
	public String viewCommission(final Model model, @ModelAttribute CommissionVo commissionVo,
			HttpServletRequest request, @ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			commissionVo.setEmployee(authEmployeeDetails.getName());
			commissionVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());

			model.addAttribute("status", statusService.listAllStatus());
			model.addAttribute("employees", employeeService.listAllEmployee());
			commissionVo = commissionService.getCommissionById(Long.parseLong(request.getParameter("id")));
			Boolean viewStatus = false;
			for(String code : commissionVo.getSuperiors()){
				if(code.equalsIgnoreCase(authEmployeeDetails.getEmployeeCode()))
					viewStatus = true;
			}
			commissionVo.setViewStatus(viewStatus);
			model.addAttribute("commissionVo", commissionVo);
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));

		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.COMMISSION_VIEW_PAGE;
	}

	/**
	 * method to delete commission
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.COMMISSION_DELETE_URL, method = RequestMethod.GET)
	public String deleteCommission(final Model model, HttpServletRequest request) {
		try {
			commissionService.deleteCommission(Long.parseLong(request.getParameter("id")));
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.COMMISSION_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.COMMISSION_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.COMMISSION_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.COMMISSION_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.COMMISSION_DELETE_SUCCESS;
	}

	/**
	 * method to save/update commission
	 * 
	 * @param model
	 * @param commissionVo
	 * @param commissionTitleVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.COMMISSION_SAVE_URL, method = RequestMethod.POST)
	public String saveOrUpdateCommission(final Model model, @ModelAttribute CommissionVo commissionVo,
			@ModelAttribute CommissionTitleVo commissionTitleVo) {
		try {
			if (commissionVo.getSuperiorIds() != null) {
				List<Long> superiorIds = new ArrayList<Long>();
				for (Long id : commissionVo.getSuperiorIds()) {
					superiorIds.add(id);
				}
				commissionVo.setSuperiorAjaxIds(superiorIds);
			}
			if (commissionVo.getCommissionId() != null) {
				commissionService.updateCommission(commissionVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.COMMISSION_UPDATE_SUCCESS);
			} else {
				Long id = commissionService.saveCommission(commissionVo);
				commissionVo.setId(id);
				commissionVo.setCommissionId(id);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.COMMISSION_SAVE_SUCCESS);
			}
			model.addAttribute("status", statusService.listAllStatus());
			model.addAttribute("titles", titleService.listAllCommissionTitles());
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.COMMISSION_SAVE_FAILED);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.COMMISSION_EDIT_URL + "?id=" + commissionVo.getCommissionId();
	}

	/**
	 * method to get commission by employee and title
	 * 
	 * @param model
	 * @param employeeId
	 * @param titleId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_COMMISSION_BY_TITLE_AND_EMPLOYEE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getCommissinByEmployeeAndTitle(final Model model,
			@RequestParam(value = "employeeCode", required = true) String employeeCode,
			@RequestParam(value = "titleId", required = true) Long titleId) {
		JsonResponse response = new JsonResponse();
		try {
			response.setResult(commissionService.getCommissionByEmployeeAndTitle(employeeCode, titleId));
			response.setStatus("ok");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * method to update commission status
	 * 
	 * @param model
	 * @param commissionVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.COMMISSION_UPDATE_STATUS, method = RequestMethod.POST)
	public String updateCommissionStatus(final Model model, @ModelAttribute CommissionVo commissionVo) {
		try {
			commissionService.updateCommissinStatus(commissionVo);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.COMMISSION_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.COMMISSION_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.WRONG;
		}
		return "redirect:" + RequestConstants.COMMISSION_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.COMMISSION_UPDATE_SUCCESS;
	}

	/**
	 * method to save commission title
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.COMMISSION_TITLE_SAVE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveCommissionTitle(@RequestBody CommissionTitleVo commissionTitleVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			titleService.saveCommissionTitle(commissionTitleVo);
			jsonResponse.setStatus("SUCCESS");
		} catch (DuplicateItemException e) {
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}
	
	/**
	 * method to get available commissin titles
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_AVAILABLE_COMMISSION_TITLES , method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableCommissionTitles(){
		JsonResponse jsonResponse = new JsonResponse();
		try{
			jsonResponse.setResult(titleService.listAllCommissionTitles());
			jsonResponse.setStatus("SUCCESS");
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonResponse;
	}
}
