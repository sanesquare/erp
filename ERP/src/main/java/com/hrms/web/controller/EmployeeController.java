package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dao.RelegionDao;
import com.hrms.web.dao.UserDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.User;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.AccountTypeService;
import com.hrms.web.service.BloodGroupService;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.CountryService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeCategoryService;
import com.hrms.web.service.EmployeeDesignationService;
import com.hrms.web.service.EmployeeGradeService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.EmployeeStatusService;
import com.hrms.web.service.EmployeeTypeService;
import com.hrms.web.service.JobFieldService;
import com.hrms.web.service.LanguageService;
import com.hrms.web.service.LeaveTypeService;
import com.hrms.web.service.OrganisationAssetsService;
import com.hrms.web.service.QualificationDegreeService;
import com.hrms.web.service.SalaryTypeService;
import com.hrms.web.service.SkillService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.BiometricServiceUtil;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.ReadFromExcel;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.AddressJsonVo;
import com.hrms.web.vo.EmployeeAdditionalInformationVo;
import com.hrms.web.vo.EmployeeBankAccountVo;
import com.hrms.web.vo.EmployeeDocumentsVo;
import com.hrms.web.vo.EmployeeEmergencyJsonVo;
import com.hrms.web.vo.EmployeeExtraInfoJsonVo;
import com.hrms.web.vo.EmployeeLeaveVo;
import com.hrms.web.vo.EmployeePersonalDetailsJsonVo;
import com.hrms.web.vo.EmployeePhoneNumbersVo;
import com.hrms.web.vo.EmployeeSuperiorsSubordinatesVo;
import com.hrms.web.vo.EmployeeUserJsonVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.RelegionVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.RolesVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author shamsheer, Jithin Mohan
 * @since 18-March-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class EmployeeController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private CountryService countryService;

	@Autowired
	private EmployeeCategoryService employeeCategoryService;

	@Autowired
	private BloodGroupService bloodGroupService;

	@Autowired
	private EmployeeTypeService employeeType;

	@Autowired
	private LeaveTypeService leaveTypeService;

	@Autowired
	private JobFieldService jobFieldService;

	@Autowired
	private UserService userService;

	@Autowired
	private EmployeeDesignationService employeeDesignationService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private RelegionDao relegianDao;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private EmployeeGradeService employeeGradeService;

	@Autowired
	private LanguageService languageService;

	@Autowired
	private SkillService skillService;

	@Autowired
	private QualificationDegreeService qualificationDegreeService;

	@Autowired
	private OrganisationAssetsService assetsService;

	@Autowired
	private AccountTypeService accountTypeService;

	@Autowired
	private EmployeeStatusService statusService;

	@Autowired
	private SalaryTypeService salaryTypeService;

	/**
	 * method to load employee page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_HOME_URL, method = RequestMethod.GET)
	public String getEmployeeHome(final Model model, HttpServletRequest request,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails, EmployeeVo employeeVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			
			if (authEmployeeDetails.getIsAdmin())
			{
				if(authEmployeeDetails.getEmployeeBranchId()!=null){
					model.addAttribute("employees", employeeService.getDetailedEmployeesByBranchAndChildBranches(authEmployeeDetails.getEmployeeBranchId()));
				}
				else{
					model.addAttribute("employees", employeeService.listEmployeesDeetails());
				}
				//model.addAttribute("employees", employeeService.listEmployeesDeetails());
				
			}
			else
				model.addAttribute("employees",
						employeeService.findAllEmployeesDeetailsByCode(authEmployeeDetails.getEmployeeCode()));
			/*model.addAttribute("branches", branchService.listAllBranches());*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.EMPLOYEE_PAGE;
	}

	/**
	 * method to reach employee roles page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_ROLES, method = RequestMethod.GET)
	public String getRolesPage(final Model model, HttpServletRequest request,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("rolesVo",
					employeeService.findRolesByEmployee(Long.parseLong(request.getParameter("id"))));
			model.addAttribute("role", employeeService.getRoleTypes());
			/*
			 * SessionRolesVo sessionRolesVo =
			 * employeeService.getEmployeeRoles(authEmployeeDetails.
			 * getEmployeeId()); model.addAttribute("rolesMap",sessionRolesVo);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.EMPLOYEE_ROLES_PAGE;
	}

	/**
	 * method to reach add employee page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_ADD_NEW_URL, method = RequestMethod.GET)
	public String addNewEmployeeGet(final Model model, @ModelAttribute EmployeeVo employeeVo,
			@ModelAttribute EmployeeLeaveVo employeeLeaveVo, @ModelAttribute("roles") SessionRolesVo rolesMap,
			HttpServletRequest request) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			// employeeVo.setEmployeeCode(employeeService.generateEmployeeCode());
			model.addAttribute("employeeVo", employeeVo);
			model.addAttribute("employeeType", employeeType.findAllEmployeeType());
			model.addAttribute("country", countryService.findAllCountry());
			model.addAttribute("blood", bloodGroupService.findAllBloodGroup());
			model.addAttribute("employeeCategory", employeeCategoryService.findAllEmployeeCategory());
			model.addAttribute("employeeDesignation", employeeDesignationService.listAllDesignations());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("grade", employeeGradeService.listAllGrades());
			model.addAttribute("shifts", employeeGradeService.listWorkShift());
			model.addAttribute("language", languageService.findAllLanguage());
			model.addAttribute("skills", skillService.findAllSkills());
			model.addAttribute("degree", qualificationDegreeService.findAllQualificationDegree());
			model.addAttribute("assets", assetsService.listAllAssets());
			model.addAttribute("relegion", relegianDao.listAllRelegians());
			model.addAttribute("status", statusService.listAllStatus());
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("accountType", accountTypeService.findAllAccountType());
			model.addAttribute("field", jobFieldService.findAllJobField());
			model.addAttribute("leaves", leaveTypeService.findAllLeaveType());
			model.addAttribute("salaryTypes", salaryTypeService.listAllSalaryTypes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.EMPLOYEE_ADD_PAGE;
	}

	/**
	 * method to save employee Info
	 * 
	 * @param model
	 * @param multipartFile
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_SAVE_INFO, method = RequestMethod.POST)
	public String saveEmployeeInfo(final Model model, @ModelAttribute EmployeeVo employeeVo,
			@RequestParam(value = "emloyee_profile_pic") MultipartFile multipartFile, HttpServletRequest request,
			RedirectAttributes redirect) {
		try {
			if (employeeVo.getEmployeeId() == null) {
				if (employeeService.findAndReturnEmployeeByCode(employeeVo.getEmployeeCode()) == null) {
					if (!multipartFile.isEmpty()) {
						Boolean reesult = false;
						if (employeeVo.getProfilePicHidden() != null)
							reesult = FileUpload.removeFileFromCDN(employeeVo.getProfilePicHidden());
						if (reesult) {
							String profilePic = CommonConstants.SFTP_PATH
									+ FileUpload.uploadFileToCDN(MessageConstants.EMPLOYEE_MODULE, multipartFile);
							employeeVo.setProfilePic(profilePic);
						}
					}
					Long id = employeeService.saveEmployee(employeeVo);
					employeeVo.setEmployeeId(id);
					model.addAttribute(MessageConstants.SUCCESS, MessageConstants.EMPLOYEE_SAVE_SUCCESS);
				} else {
					model.addAttribute(MessageConstants.ERROR, MessageConstants.EMPLOYEE_EXISTS);
					logger.info("already exists");
					return "redirect:" + RequestConstants.EMPLOYEE_ADD_NEW_URL + "?" + MessageConstants.ERROR + "="
							+ MessageConstants.EMPLOYEE_EXISTS;
				}
			} else {
				if (!multipartFile.isEmpty()) {
					Boolean reesult = false;
					if (employeeVo.getProfilePicHidden() != null)
						reesult = FileUpload.removeFileFromCDN(employeeVo.getProfilePicHidden());
					if (reesult) {
						String profilePic = CommonConstants.SFTP_PATH
								+ FileUpload.uploadFileToCDN(MessageConstants.EMPLOYEE_MODULE, multipartFile);
						employeeVo.setProfilePic(profilePic);
					}
				}
				employeeService.updateEmployee(employeeVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.EMPLOYEE_UPDATE_SUCCESS);
			}

		} catch (ItemNotFoundException e) {
			logger.info("EMPLOYEE SAVE: ", e);
			model.addAttribute(MessageConstants.ERROR, e);
			return "redirect:" + RequestConstants.EMPLOYEE_HOME_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			logger.info("EMPLOYEE SAVE: ", e);
			model.addAttribute(MessageConstants.ERROR, MessageConstants.EMPLOYEE_SAVE_FAILED);
			return "redirect:" + RequestConstants.EMPLOYEE_HOME_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.EMPLOYEE_SAVE_FAILED;
		}
		return "redirect:" + RequestConstants.EDIT_EMPLOYEE_DETAILS + "?eid=" + employeeVo.getEmployeeId() + "&"
				+ MessageConstants.SUCCESS + "=" + MessageConstants.EMPLOYEE_SAVE_SUCCESS;
	}

	/**
	 * method to save employee user info
	 * 
	 * @param employeeVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_SAVE_USER_INFO, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeUserInfo(@RequestBody EmployeeUserJsonVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (employeeVo.getEmployeeId() != null) {
				User user = userService.findUserByUsername(employeeVo.getUserName());
				if (user == null) {
					employeeService.saveEmployeeUserInfo(employeeVo);
				} else if (user.getEmployee().getId() == employeeVo.getEmployeeId()) {
					// update user
					employeeService.saveEmployeeUserInfo(employeeVo);
				} else {
					jsonResponse.setStatus("exists");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save employee user profile info
	 * 
	 * @param employeeVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_SAVE_PERSONAL_DETAILS, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeUserProfile(@RequestBody EmployeePersonalDetailsJsonVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (employeeVo.getEmployeeId() != null)
				employeeService.saveEmployeeUserProfileInfo(employeeVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save employee additional info
	 * 
	 * @param employeeVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_SAVE_ADDITIONAL_INFO, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveAdditionalInfo(@RequestBody EmployeeExtraInfoJsonVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (employeeVo.getEmployeeId() != null)
				employeeService.saveEmployeeAdditionalInfo(employeeVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save employee address
	 * 
	 * @param employeeVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_SAVE_ADDRESS, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeAddress(@RequestBody AddressJsonVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (employeeVo.getEmployeeId() != null)
				employeeService.saveAddress(employeeVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save employee's phone numbers
	 * 
	 * @param employeeVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_SAVE_PHONE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeContactNumber(@RequestBody EmployeePhoneNumbersVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (employeeVo.getEmployeeId() != null)
				employeeService.savePhone(employeeVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save notes
	 * 
	 * @param employeeVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_SAVE_NOTES, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveNotes(@RequestBody EmployeeExtraInfoJsonVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (employeeVo.getEmployeeId() != null)
				employeeService.saveNotes(employeeVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save notes
	 * 
	 * @param employeeVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EMERGENCY_CONTACT_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmergencyContact(@RequestBody EmployeeEmergencyJsonVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (employeeVo.getEmployeeId() != null)
				employeeService.saveEmergencyContact(employeeVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save employee qualification
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_QUALIFICATION_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeQualification(
			@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeQualification(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save employee languages
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_LANGUAGE_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeLanguage(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeLanguage(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save work experience
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EXPRNCE_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeExperience(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeExperience(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save employee bank account
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_BANK_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeBankAccount(@RequestBody EmployeeBankAccountVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeBankAccount(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save benefits
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_BENEFIT_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeBenefit(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeBenefitsDetails(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save next of kin
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_NEXTOFKIN_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeNextOfKin(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeNextOfKinDetails(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save dependents
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_DEPENDANTS_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeDependence(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeDependants(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save reference
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_REFERENCE_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeReference(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeReferenceDetails(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save leave
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_EMPLOYEE_LEAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeLeave(@RequestBody EmployeeLeaveVo employeeLeaveVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (employeeLeaveVo.getEmployeeId() != null) {
				employeeService.saveEmployeeLeave(employeeLeaveVo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to get leave
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_LEAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse getEmployeeLeave(@RequestBody EmployeeLeaveVo employeeLeaveVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (employeeLeaveVo.getEmployeeId() != null) {
				jsonResponse.setResult(employeeService.getLeaveByEmployeeAndTitle(employeeLeaveVo.getEmployeeId(),
						employeeLeaveVo.getLeaveTitleId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to get employee bank account
	 * 
	 * @param employeeLeaveVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_BANK_ACCOUNT, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse getEmployeeBankAccount(@RequestBody EmployeeBankAccountVo bankAccountVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (bankAccountVo.getEmployeeId() != null) {
				jsonResponse.setResult(employeeService.getBankAccountByEmployeeAndType(bankAccountVo.getEmployeeId(),
						bankAccountVo.getTypeId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to get qualification
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_QUALIFICATION, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse getEmployeeQualification(
			@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null) {
				jsonResponse.setResult(employeeService.getQualificationByEmployeeAndDegree(
						informationVo.getEmployeeId(), informationVo.getDegreeId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to get skill
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_SKILL, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse getEmployeeSkill(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null) {
				jsonResponse.setResult(employeeService.getSkillByEmployeeAndSkill(informationVo.getEmployeeId(),
						informationVo.getSkillId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to get language
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_LANGUAGE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse getEmployeeLanguage(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null) {
				jsonResponse.setResult(employeeService.getLanguagesByEmployeeAndLanguage(informationVo.getEmployeeId(),
						informationVo.getLanguageId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to get assets
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_ASSET, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse getEmployeeAssets(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null) {
				jsonResponse.setResult(employeeService.getAssetsByEmployeeAndAsset(informationVo.getEmployeeId(),
						informationVo.getAssetId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save skills
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_SKILL_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeSkill(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeSkillsInfo(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save uniform details
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_UNIFORM_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeUnoformSave(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeUniformDetails(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save assets details
	 * 
	 * @param informationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_ASSETS_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveEmployeeAssets(@RequestBody EmployeeAdditionalInformationVo informationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (informationVo.getEmployeeId() != null)
				employeeService.saveEmployeeOrganizationAssets(informationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to upload docs
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_DOC_UPLD, method = RequestMethod.POST)
	public String uploadDocs(final Model model, @ModelAttribute EmployeeVo employeeVo) {
		try {
			if (employeeVo.getEmployeeId() != null) {
				employeeVo.setDocuments(uploadDocuments(employeeVo.getEmp_upld_docs()));
				employeeService.updateDocuments(employeeVo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.EMPLOYEE_HOME_URL;
	}

	/**
	 * method to upload files
	 * 
	 * @param files
	 * @return
	 */
	private List<EmployeeDocumentsVo> uploadDocuments(List<MultipartFile> files) {
		List<EmployeeDocumentsVo> documentsVos = new ArrayList<EmployeeDocumentsVo>();
		try {
			for (MultipartFile file : files) {
				if (!file.isEmpty()) {
					String fileAccessUrl = FileUpload.uploadDocument(MessageConstants.EMPLOYEE_MODULE, file);
					if (fileAccessUrl != null) {
						EmployeeDocumentsVo documentsVo = new EmployeeDocumentsVo();
						documentsVo.setFileName(file.getOriginalFilename());
						documentsVo.setDocumnetUrl(fileAccessUrl);
						documentsVos.add(documentsVo);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return documentsVos;
	}

	/**
	 * method to save new relegion
	 * 
	 * @param relegionVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_NEW_RELEGION, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveNewRelegion(@RequestBody RelegionVo relegionVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			relegianDao.saveRelegion(relegionVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save new relegion
	 * 
	 * @param relegionVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_STATUS_UPDATE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveEmployeStatus(
			@RequestBody EmployeeAdditionalInformationVo additionalInformationVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (additionalInformationVo.getEmployeeId() != null)
				employeeService.saveEmployeeStatus(additionalInformationVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to update notify by email
	 * 
	 * @param employeeVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMLOYEE_NOTIFY_EMAIL_UPDATE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveEmployeNotifyByEmail(@RequestBody EmployeeExtraInfoJsonVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (employeeVo.getEmployeeId() != null)
				employeeService.updateNotifyByEmail(employeeVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save superiors subordinates
	 * 
	 * @param employeeVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_SUPERIORS_SUBORDINATE_SAVE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveEmployeSuperirsSubordinates(
			@RequestBody EmployeeSuperiorsSubordinatesVo employeeSuperiorsSubordinatesVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeService.saveEmployeeSubordinateSuperior(employeeSuperiorsSubordinatesVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to delete employee
	 * 
	 * @param model
	 * @param eid
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_EMPLOYEE_DETAILS, method = RequestMethod.GET)
	public String deleteEmployee(final Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("eid"));
		try {
			employeeService.deleteEmployee(id);
		} catch (Exception e) {
			employeeService.updateEmployeeStatus(id);
		}
		return "redirect:" + RequestConstants.EMPLOYEE_HOME_URL;
	}

	/**
	 * method to view employee details
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_EMPLOYEE_DETAILS, method = RequestMethod.GET)
	public String viewEmployeeDetails(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("employee",
					employeeService.findDetailedEmployee(Long.parseLong(request.getParameter("eid"))));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.EMPLOYEE_VIEW_PAGE;
	}

	/**
	 * method to edit employee details
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_EMPLOYEE_DETAILS, method = RequestMethod.GET)
	public String editEmployee(final Model model, HttpServletRequest request, @ModelAttribute EmployeeVo employeeVo,
			@ModelAttribute EmployeeLeaveVo employeeLeaveVo, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			model.addAttribute("employeeVo",
					employeeService.findDetailedEmployee(Long.parseLong(request.getParameter("eid"))));
			model.addAttribute("employeeType", employeeType.findAllEmployeeType());
			model.addAttribute("employeeCategory", employeeCategoryService.findAllEmployeeCategory());
			model.addAttribute("employeeDesignation", employeeDesignationService.listAllDesignations());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("grade", employeeGradeService.listAllGrades());
			model.addAttribute("shifts", employeeGradeService.listWorkShift());
			model.addAttribute("language", languageService.findAllLanguage());
			model.addAttribute("skills", skillService.findAllSkills());
			model.addAttribute("degree", qualificationDegreeService.findAllQualificationDegree());
			model.addAttribute("assets", assetsService.listAllAssets());
			model.addAttribute("relegion", relegianDao.listAllRelegians());
			model.addAttribute("status", statusService.listAllStatus());
			model.addAttribute("country", countryService.findAllCountry());
			model.addAttribute("blood", bloodGroupService.findAllBloodGroup());
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("accountType", accountTypeService.findAllAccountType());
			model.addAttribute("field", jobFieldService.findAllJobField());
			model.addAttribute("leaves", leaveTypeService.findAllLeaveType());
			model.addAttribute("salaryTypes", salaryTypeService.listAllSalaryTypes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.EMPLOYEE_ADD_PAGE;
	}

	/**
	 * method to delete document
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_EMPLOYEE_DOCUMENT, method = RequestMethod.GET)
	public String deleteDocument(final Model model, HttpServletRequest request) {
		try {
			String url = request.getParameter("durl");
			SFTPOperation operation = new SFTPOperation();
			Boolean result = operation.removeFileFromSFTP(url);
			if (result) {
				employeeService.deleteDocument(url);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.EMPLOYEE_HOME_URL;
	}

	/**
	 * method to download document
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DOWNLOAD_EMPLOYEE_DOCUMENT, method = RequestMethod.GET)
	public void downloadDocument(final Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(request.getParameter("durl"), response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * method to get superiors subordinates
	 * 
	 * @param designation
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_SUPERIOR_SUBORDINATE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getSuperiorSubordinate(@RequestParam(value = "id", required = true) Long id) {
		JsonResponse jsonResponse = new JsonResponse();
		if (id != null) {
			try {
				jsonResponse.setResult(employeeService.getSuperiorSubordinateByDesignation(id));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return jsonResponse;
	}

	/**
	 * method to save employee roles
	 * 
	 * @param model
	 * @param rolesVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_EMPLOYEE_ROLES, method = RequestMethod.POST)
	public String saveEmployeeRoles(final Model model, @ModelAttribute RolesVo rolesVo,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails, RedirectAttributes redirectAttributes) {
		try {
			employeeService.saveRoles(rolesVo);
			SessionRolesVo sessionRolesVo = employeeService.getEmployeeRoles(rolesVo.getAuthEmployeeId());
			model.addAttribute("roles", sessionRolesVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return "redirect:" + RequestConstants.EDIT_EMPLOYEE_DETAILS + "?eid="
		// + rolesVo.getEmployeeId();
		return "redirect:" + RequestConstants.EMPLOYEE_HOME_URL;
	}

	/**
	 * method to get employee by code
	 * 
	 * @param code
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_BY_CODE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getEmployeeByCode(@RequestParam(value = "code", required = true) String code) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (code != null)
				jsonResponse.setResult(employeeService.getEmployeeByCode(code));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to export employees to excel
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EXPORT_TO_EXCEL_EMPLOYEES, method = RequestMethod.POST)
	public String exportEmployeesToExcel(final Model model, HttpServletResponse response, EmployeeVo employeeVo) {
		try {
			String status = employeeService.writeEmployeesToExcel(response, employeeVo.getEmployeeBranchId());
			if (!response.isCommitted())
				return "redirect:" + RequestConstants.EMPLOYEE_HOME_URL + "?" + MessageConstants.ERROR
						+ "=No Result Found.";
		} catch (Exception e) {
		}
		return "redirect:" + RequestConstants.EMPLOYEE_HOME_URL;
	}
}
