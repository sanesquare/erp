package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dao.MeetingsStatusDao;
import com.hrms.web.dao.ProjectDao;
import com.hrms.web.entities.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.mail.ApplicationMailSender;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.MeetingsService;
import com.hrms.web.service.SecondLevelForwaderService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.MeetingDocumentsVo;
import com.hrms.web.vo.MeetingJsonVo;
import com.hrms.web.vo.MeetingsVo;
import com.hrms.web.vo.PaySalaryVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 12-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class MeetingsController {

	@Autowired
	private MeetingsService meetingsService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private MeetingsStatusDao meetingStatusDao;

	@Autowired
	private ProjectDao projectDao;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private SecondLevelForwaderService secondLevelForwaderService;

	@Autowired
	@Qualifier(value = "taskExecutor")
	private TaskExecutor taskExecutor;

	@Log
	private Logger logger;

	@Autowired
	private UserService userService;

	/**
	 * method to reach meetings home page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEETINGS_URL, method = RequestMethod.GET)
	public String meetingsPageGetRequestHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("meetings", meetingsService.listAllMeetings());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.MEETINGS_PAGE;
	}

	/**
	 * method to reach add meetings page
	 * 
	 * @param model
	 * @param meetingsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_MEETINGS_URL, method = RequestMethod.GET)
	public String addNewMeetingsGet(final Model model, @ModelAttribute MeetingsVo meetingsVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("status", meetingStatusDao.listAllMeetingStatus());
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_MEETINGS_PAGE;
	}

	/**
	 * method to edit meeting
	 * 
	 * @param model
	 * @param meetingsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_MEETING, method = RequestMethod.GET)
	public String editMeeting(final Model model, @ModelAttribute MeetingsVo meetingsVo, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("meetingsVo",
					meetingsService.findMeetingsById(Long.parseLong(request.getParameter("id"))));
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("status", meetingStatusDao.listAllMeetingStatus());
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_MEETINGS_PAGE;
	}

	/**
	 * method to delete meeting
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_MEETING, method = RequestMethod.GET)
	public String deleteMeeting(final Model model, HttpServletRequest request) {
		try {
			meetingsService.deleteMeeting(Long.parseLong(request.getParameter("id")));
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.MEETING_DELETE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.MEETING_DELETE_FAILED);
		}
		return "redirect:" + RequestConstants.MEETINGS_URL;
	}

	/**
	 * method to get employees by branches
	 * 
	 * @param jsonVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_BY_BRANCHES, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getEmployeeByBranch(@RequestBody MeetingJsonVo jsonVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(employeeService.getEmployeesByBranches(jsonVo.getBranchIds()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	@RequestMapping(value = RequestConstants.EMPLOYES_BY_BRANCH_ID, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getEmployeeByBranchId(@RequestBody PaySalaryVo vo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (vo.getDepartmentIds() == null && vo.getBranchId() != null)
				jsonResponse.setResult(employeeService.getEmployeesByBranch(vo.getBranchId()));
			else if (vo.getBranchId() == null && !vo.getDepartmentIds().isEmpty())
				jsonResponse.setResult(employeeService.getEmployeesByDepartments(vo.getDepartmentIds()));
			else if (vo.getDepartmentIds() != null && vo.getBranchId() != null) {
				jsonResponse.setResult(employeeService.getEmployeesByBranchAndDepartments(vo.getBranchId(),
						vo.getDepartmentIds()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to get employee by departments
	 * 
	 * @param jsonVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_BY_DEPARTMENTS, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse getEmployeeByDepartments(@RequestBody MeetingJsonVo jsonVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(employeeService.getEmployeesByDepartments(jsonVo.getDepartmentIds()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to get employees by branches and departments
	 * 
	 * @param jsonVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_BY_DEPART_AND_BRANCH, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse getEmployeeByBranchAndDepartment(@RequestBody MeetingJsonVo jsonVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(employeeService.getEmployeesByBranchesAndDepartments(jsonVo.getBranchIds(),
					jsonVo.getDepartmentIds()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save new meeting
	 * 
	 * @param model
	 * @param meetingsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_MEETING, method = RequestMethod.POST)
	public String saveMeeting(final Model model, @ModelAttribute MeetingsVo meetingsVo) {
		try {
			if (meetingsVo.getMeetingId() == null) {
				meetingsVo.setMeetingId(meetingsService.saveMeetings(meetingsVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.MEETING_SAVE_SUCCESS);
			} else {
				meetingsService.updateMeetings(meetingsVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.MEETING_UPDATE_SUCCESS);
			}
			MeetingsVo meetingsVoForSendingMail = meetingsService.findMeetingsById(meetingsVo.getMeetingId());
			if (meetingsVoForSendingMail.isSendEmail())
				this.sendMeetingMail(meetingsVoForSendingMail);
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("status", meetingStatusDao.listAllMeetingStatus());
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.MEETING_SAVE_FAILED);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.EDIT_MEETING + "?id=" + meetingsVo.getMeetingId();
	}

	private void sendMeetingMail(MeetingsVo sendMeetingVo) {
		try {
			Map<String, String> employees = new HashMap<String, String>();
			for (Long id : sendMeetingVo.getEmployeeIds()) {
				Employee employee = employeeService.findAndReturnEmployeeById(id);
				String email = null;
				String name = null;
				if (employee.getUser() != null)
					email = employee.getUser().getEmail();
				if (employee.getUserProfile() != null)
					name = employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname();
				if (email != null && name != null)
					employees.put(email, name);
			}
			String sender = null;
			Employee senderEmployee = userService.findUserByUsername(sendMeetingVo.getCreatedBy()).getEmployee();
			if (senderEmployee.getUserProfile() != null)
				sender = senderEmployee.getUserProfile().getFirstName() + " "
						+ senderEmployee.getUserProfile().getLastname();
			else
				sender = sendMeetingVo.getCreatedBy();
			String time = sendMeetingVo.getTime();
			String venue = sendMeetingVo.getVenue();
			Date date = DateFormatter.convertStringToDate(sendMeetingVo.getMeetingStartDate());
			String agenda = sendMeetingVo.getAgenda();
			String subject = sendMeetingVo.getMeetingsTitle();
			ApplicationMailSender applicationMailSender = new ApplicationMailSender(employees,
					CommonConstants.MEETING_MAIL, sender, date, time, venue, agenda, subject);
			taskExecutor.execute(applicationMailSender);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * method to upload meeting docs
	 * 
	 * @param model
	 * @param meetingsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPLOAD_MEETING_DOCS, method = RequestMethod.POST)
	public String uploadDocs(final Model model, @ModelAttribute MeetingsVo meetingsVo) {
		try {
			if (meetingsVo.getMeetingId() != null) {
				meetingsVo.setDocumentsVos(uploadDocuments(meetingsVo.getMeetingDocs()));
				meetingsService.updateDocument(meetingsVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.MEETING_DOC_SUCCESS);
			}
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("status", meetingStatusDao.listAllMeetingStatus());
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return "redirect:" + RequestConstants.MEETINGS_URL;
	}

	/**
	 * method to upload docs
	 * 
	 * @param files
	 * @return
	 */
	private List<MeetingDocumentsVo> uploadDocuments(List<MultipartFile> files) {
		List<MeetingDocumentsVo> vos = new ArrayList<MeetingDocumentsVo>();
		try {
			for (MultipartFile file : files) {
				if (!file.isEmpty()) {
					String url = FileUpload.uploadDocument(MessageConstants.MEETING_MODULE, file);

					if (url != null) {
						MeetingDocumentsVo documentsVo = new MeetingDocumentsVo();
						documentsVo.setUrl(url);
						documentsVo.setFilename(file.getOriginalFilename());
						vos.add(documentsVo);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vos;
	}

	/**
	 * method to view meeting
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_MEETING, method = RequestMethod.GET)
	public String viewMeeting(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("meeting", meetingsService.findMeetingsById(Long.parseLong(request.getParameter("id"))));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.VIEW_MEETING;
	}

	/**
	 * method to download doc
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = RequestConstants.DOWNLOAD_MEETING_DOC, method = RequestMethod.GET)
	public void downloadDoc(HttpServletRequest request, HttpServletResponse response) {
		try {
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(request.getParameter("url"), response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * method to delete doc
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_MEETING_DOC, method = RequestMethod.GET)
	public String deleteDoc(final Model model, HttpServletRequest request) {
		try {
			SFTPOperation operation = new SFTPOperation();
			String url = request.getParameter("url");
			Boolean result = operation.removeFileFromSFTP(url);
			if (result)
				meetingsService.deleteDocument(url);
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.MEETING_DOC_DELETE_SUCCESS);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.MEETINGS_URL;
	}
}
