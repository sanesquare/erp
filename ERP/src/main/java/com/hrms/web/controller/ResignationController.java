package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dao.ProjectStatusDao;
import com.hrms.web.dto.Superiors;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ForwardApplicationStatusService;
import com.hrms.web.service.ProjectService;
import com.hrms.web.service.ResignationService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.ResignationDocumentsVo;
import com.hrms.web.vo.ResignationVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SFTPFilePropertiesVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.TransferDocumentsVo;
import com.hrms.web.vo.TransferVo;

/**
 * 
 * @author Shamsheer
 * @since 10-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class ResignationController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private ResignationService resignationService;

	@Autowired
	private ForwardApplicationStatusService statusService;

	/**
	 * method to reach resignation home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.RESIGNATION_URL, method = RequestMethod.GET)
	public String resignationPageGetRequestHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> TravelController... travelPageGetRequestHandler");
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			model.addAttribute("resignations",
					resignationService.findAllResignationByEmployee(authEmployeeDetails.getEmployeeCode()));
			model.addAttribute("thisCode", authEmployeeDetails.getEmployeeCode());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.RESIGNATION_PAGE;
	}

	/**
	 * method to add resignation
	 * 
	 * @param model
	 * @param resignationVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_RESIGNATION_URL, method = RequestMethod.GET)
	public String addResignationPageGetRequestHandler(final Model model, @ModelAttribute ResignationVo resignationVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			resignationVo.setEmployee(authEmployeeDetails.getName()+" ("+authEmployeeDetails.getEmployeeCode()+")");
			resignationVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_RESIGNATION_PAGE;
	}

	/**
	 * method to save or update resignation
	 * 
	 * @param model
	 * @param resignationVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.RESIGNATION_SAVE, method = RequestMethod.POST)
	public String saveResignation(final Model model, @ModelAttribute ResignationVo resignationVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			resignationVo.setEmployee(authEmployeeDetails.getName());
			resignationVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			if (resignationVo.getResignationId() == null) {
				resignationVo.setResignationId(resignationService.saveResignation(resignationVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.RESIGNATION_SAVE_SUCCESS);
			} else {
				resignationService.updateResignation(resignationVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.RESIGNATION_UPDATE_SUCCESS);
			}
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return "redirect:" + RequestConstants.RESIGNATION_EDIT + "?id=" + resignationVo.getResignationId();

	}

	/**
	 * method to edit resignation
	 * 
	 * @param model
	 * @param resignationVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @param resignationId
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.RESIGNATION_EDIT, method = RequestMethod.GET)
	public String editResignation(final Model model, @ModelAttribute ResignationVo resignationVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@RequestParam(value = "id", required = true) Long resignationId, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			resignationVo = resignationService.getResignationById(resignationId);
			model.addAttribute("resignationVo", resignationVo);
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_RESIGNATION_PAGE;
	}

	/**
	 * method delete resignation
	 * 
	 * @param model
	 * @param resignationId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.RESIGNATION_DELETE, method = RequestMethod.GET)
	public String deleteResignation(Model model, @RequestParam(value = "id", required = true) Long resignationId) {
		try {
			resignationService.deleteResignation(resignationId);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.RESIGNATION_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.RESIGNATION_DELETE_SUCCESS;
	}

	/**
	 * method to upload document
	 * 
	 * @param model
	 * @param resignationVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.RESIGNATION_DOCUMENT_UPLOAD, method = RequestMethod.POST)
	public String uploadDocument(final Model model, @ModelAttribute ResignationVo resignationVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		try {
			if (resignationVo.getResignationId() != null) {
				resignationVo.setDocumentsVos(uploadDocuments(resignationVo.getFiles()));
				resignationService.updateDocument(resignationVo);
			}
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.RESIGNATION_DOCUMENTS_FAILED;
		}
		return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.RESIGNATION_DOCUMENTS_SUCCESS;
	}

	/**
	 * method to upload documents
	 * 
	 * @param files
	 * @return
	 */
	private List<ResignationDocumentsVo> uploadDocuments(List<MultipartFile> files) {
		List<ResignationDocumentsVo> documentVos = new ArrayList<ResignationDocumentsVo>();
		try {
			for (MultipartFile file : files) {
				if (!file.isEmpty()) {
					String url = FileUpload.uploadDocument(MessageConstants.RESIGNATION_MODULE, file);
					if (url != null) {
						ResignationDocumentsVo vo = new ResignationDocumentsVo();
						vo.setUrl(url);
						vo.setFileName(file.getOriginalFilename());
						documentVos.add(vo);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return documentVos;
	}

	/**
	 * method to download documents
	 * 
	 * @param request
	 * @param response
	 * @param url
	 */
	@RequestMapping(value = RequestConstants.RESIGNATION_DOCUMENT_DOWNLOAD, method = RequestMethod.GET)
	public void downloadDocument(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "url", required = true) String url) {
		try {
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(url, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * method to delete document
	 * 
	 * @param url
	 * @return
	 */
	@RequestMapping(value = RequestConstants.RESIGNATION_DOCUMENT_DELETE, method = RequestMethod.GET)
	public String deleteDocument(@RequestParam(value = "url", required = true) String url) {
		try {
			SFTPOperation operation = new SFTPOperation();
			Boolean result = operation.removeFileFromSFTP(url);
			if (result)
				resignationService.deleteDocument(url);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.RESIGNATION_DOCUMENTS_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.RESIGNATION_DOCUMENTS_DELETE_SUCCESS;
	}

	/**
	 * method to view resignation
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.RESIGNATION_VIEW, method = RequestMethod.GET)
	public String viewResignation(final Model model, @ModelAttribute ResignationVo resignationVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@RequestParam(value = "id", required = true) Long resignationId,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			resignationVo.setEmployee(authEmployeeDetails.getName());
			resignationVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			resignationVo = resignationService.getResignationById(resignationId);
			if(resignationVo.getSuperiorCode().equalsIgnoreCase(authEmployeeDetails.getEmployeeCode()))
				resignationVo.setViewStatus(true);
			else
				resignationVo.setViewStatus(false);
			model.addAttribute("resignationVo", resignationVo);
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("status", statusService.listAllStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.RESIGNATION_VIEW_PAGE;
	}

	/**
	 * method to update status
	 * 
	 * @param model
	 * @param travelVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.RESIGNATION_UPDATE_STATUS, method = RequestMethod.POST)
	public String updateTrabsferStatus(final Model model, @ModelAttribute ResignationVo resignationVo) {
		try {
			if (resignationVo.getResignationId() != null)
				resignationService.updateStatus(resignationVo);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.RESIGNATION_STATUS_FAILED;
		}
		return "redirect:" + RequestConstants.RESIGNATION_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.RESIGNATION_STATUS_SUCCESS;
	}
}