package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.ContractService;
import com.hrms.web.service.ContractTypeService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.ContractDocumentVo;
import com.hrms.web.vo.ContractJsonVo;
import com.hrms.web.vo.ContractVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 25-March-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class ContractController {

	@Autowired
	private ContractService contractService;

	@Autowired
	private ContractTypeService typeService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private EmployeeService employeeService;

	/**
	 * method to reach contract home page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.CONTRACT_URL, method = RequestMethod.GET)
	public String contractGetHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("contracts", contractService.listContractByEmployee(authEmployeeDetails.getEmployeeId()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.CONTRACT_PAGE;
	}

	/**
	 * method to reach add new contract page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.CONTRACT_ADD_URL, method = RequestMethod.GET)
	public String contractAddPage(final Model model, @ModelAttribute ContractVo contractVo,
			@ModelAttribute ContractDocumentVo contractDocumentVo, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("branch", branchService.listAllBranches());
			model.addAttribute("department", departmentService.listAllDepartments());
			model.addAttribute("type", typeService.findAllContractType());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.CONTRACT_ADD_PAGE;
	}

	/**
	 * method to save contract
	 * 
	 * @param contractVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_CONTRACT, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveContract(@RequestBody ContractJsonVo contractVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (contractVo.getContractId() == null) {
				jsonResponse.setResult(contractService.saveContract(contractVo));
				jsonResponse.setStatus("ok");
			} else {
				contractService.updateContract(contractVo);
				jsonResponse.setStatus("ok");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to upload docs
	 * 
	 * @param model
	 * @param contractDocumentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPLOAD_CONTRACT_DOCS, method = RequestMethod.POST)
	public String uploadDocument(final Model model, @ModelAttribute ContractDocumentVo contractDocumentVo) {
		try {
			ContractVo contractVo = new ContractVo();
			if (contractDocumentVo.getContractId() != null) {
				contractVo.setContractId(contractDocumentVo.getContractId());
				contractVo.setDocumentVos(uploadDocuments(contractDocumentVo.getCntrctFileUpload()));
				contractService.updateDocument(contractVo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.CONTRACT_URL+"?"+MessageConstants.ERROR+"="+MessageConstants.DOCUMENT_UPLOAD_FAILED;
		}
		return "redirect:" + RequestConstants.CONTRACT_URL+"?"+MessageConstants.SUCCESS+"="+MessageConstants.DOCUMENT_UPLOAD_SUCCESS;
	}

	private List<ContractDocumentVo> uploadDocuments(List<MultipartFile> files) {
		List<ContractDocumentVo> documentsVos = new ArrayList<ContractDocumentVo>();
		try {
			for (MultipartFile file : files) {
				
				if (!file.isEmpty()) {
					String fileAccessUrl = FileUpload.uploadDocument(MessageConstants.CONTRACT_MODULE, file);
					if (fileAccessUrl != null) {
						ContractDocumentVo documentsVo = new ContractDocumentVo();
						documentsVo.setFileName(file.getOriginalFilename());
						documentsVo.setUrl(fileAccessUrl);
						documentsVos.add(documentsVo);
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return documentsVos;
	}

	/**
	 * method to edit contract
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_CONTRACT, method = RequestMethod.GET)
	public String editContract(final Model model, HttpServletRequest request, @ModelAttribute ContractVo contractVo,
			@ModelAttribute ContractDocumentVo contractDocumentVo, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			contractVo = contractService.findContractById(Long.parseLong(request.getParameter("id")));
			contractDocumentVo.setContractId(contractVo.getContractId());
			model.addAttribute("contractVo", contractVo);
			model.addAttribute("type", typeService.findAllContractType());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return PageNameConstatnts.CONTRACT_EDIT_PAGE;
	}

	/**
	 * method to delete contract
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_CONTRACT, method = RequestMethod.GET)
	public String deleteContract(final Model model, HttpServletRequest request) {
		try {
			contractService.deleteContract(Long.parseLong(request.getParameter("id")));
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.CONTRCT_DELETE_SUCCESS);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.CONTRCT_DELETE_FAILED);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.CONTRACT_URL;
	}

	/**
	 * method to view contract
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_CONTRACT, method = RequestMethod.GET)
	public String viewContract(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("contract", contractService.findContractById(Long.parseLong(request.getParameter("id"))));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.CONTRACT_VIEW_PAGE;
	}

	/**
	 * method to download doc
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = RequestConstants.DOWNLOAD_CONTRACT_DOC, method = RequestMethod.GET)
	public void downloadDoc(HttpServletRequest request, HttpServletResponse response) {
		try {
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(request.getParameter("url"), response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * method to delete document
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELET_CONTRACT_DOC, method = RequestMethod.GET)
	public String deleteContractDoc(HttpServletRequest request) {
		try {
			SFTPOperation operation = new SFTPOperation();
			Boolean result = operation.removeFileFromSFTP(request.getParameter("url"));
			if (result)
				contractService.deleteDocument(request.getParameter("url"));
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.CONTRACT_URL+"?"+MessageConstants.ERROR+"="+MessageConstants.DOCUMENT_DELETED_FAILED;
		}
		return "redirect:" + RequestConstants.CONTRACT_URL+"?"+MessageConstants.SUCCESS+"="+MessageConstants.DOCUMENT_DELETED_SUCCESSFULLY;
	}
}
