package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.notifications.NotificationContent;
import com.hrms.web.notifications.PushNotification;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.AnnouncementsService;
import com.hrms.web.service.AnnouncementsStatusService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.AnnouncementDocumentsVo;
import com.hrms.web.vo.AnnouncementsVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shimil Babu
 * @since 10-march-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "announcementsVo" , "roles"})
public class AnnouncementsController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private AnnouncementsService announcementsService;

	@Autowired
	private AnnouncementsStatusService announcementsStatusService;

	@Autowired
	private PushNotification pushNotification;

	/**
	 * method to handle announcement page get request
	 * 
	 * @param model
	 * @return PageNameConstatnts.ANNOUNCEMENTS_PAGE
	 */
	@RequestMapping(value = RequestConstants.ANNOUNCEMENTS_URL, method = RequestMethod.GET)
	public String announcementsPageGetRequestHandler(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> announcements controller... announcementsPageMappingGet");
		try {
			model.addAttribute("announcements", announcementsService.findAllAnnouncements());
			model.addAttribute("announcementsVo", new AnnouncementsVo());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.ANNOUNCEMENTS_PAGE;
	}

	/**
	 * method to handle add announcement page get request
	 * 
	 * @param model
	 * @return PageNameConstatnts.ADD_ANNOUNCEMENTS_PAGE
	 */

	@RequestMapping(value = RequestConstants.ADD_ANNOUNCEMENTS_URL, method = RequestMethod.GET)
	public String addAnnouncementsPageGetRequestHandler(final Model model,
			@ModelAttribute("announcementsVo") AnnouncementsVo announcementsVo, BindingResult result,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> announcements controller... addAnnouncementsPageMappingGet");
		try {
			if (announcementsVo.getAnnouncementsId() == null) {
				model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
			} else {
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
				model.addAttribute(MessageConstants.CREATED_ON, announcementsVo.getCreatedOn());
			}
			model.addAttribute("announcementsVo", announcementsVo);
			model.addAttribute("status", announcementsStatusService.findAllAnnouncementsStatus());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ADD_ANNOUNCEMENTS_PAGE;
	}

	/**
	 * method to save announcements
	 * 
	 * @param model
	 * @return
	 * 
	 */
	@RequestMapping(value = RequestConstants.SAVE_ANNOUNCEMENTS_URL, method = RequestMethod.POST)
	public ModelAndView saveAnnouncements(RedirectAttributes redirectAttributes,
			@ModelAttribute AnnouncementsVo announcementsVo, BindingResult result) {
		logger.info("inside>> Announcements Controller... saveAnnouncements");
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_ANNOUNCEMENTS_URL));
		try {
			announcementsService.saveOrUpdateAnnouncements(announcementsVo);

			NotificationContent content = new NotificationContent();
			List<String> recipients = new ArrayList<String>();
			recipients.add("ALL");
			content.setRecipients(recipients);
			content.setContent(announcementsVo.getAnnouncementsTitle());
			content.setUrl("Dashboard");
			content.setPicture("");
			pushNotification.sendNotification(content);

			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.ANNOUNCEMENTS_SAVE_SUCCESS);
			redirectAttributes.addFlashAttribute("announcementsVo", announcementsVo);
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return view;
	}

	/**
	 * method to save announcement's documents
	 * 
	 * @param model
	 * @param announcementsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ANNOUNCEMENT_DOC_SAVE_URL, method = RequestMethod.POST)
	public ModelAndView saveAnnouncementDocumentsWithPost(@ModelAttribute AnnouncementsVo announcementsVo,
			@RequestParam(value = "announcementId", required = true) Long announcementId,
			MultipartHttpServletRequest request) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_ANNOUNCEMENTS_URL));
		try {
			List<AnnouncementDocumentsVo> documentsVos = uploadAnnouncementDocuments(request);
			if (!documentsVos.isEmpty()) {
				announcementsVo.setAnnouncementsId(announcementId);
				announcementsVo.setAnnouncementDocumentsVos(documentsVos);
				announcementsService.saveAnnouncementDocuments(announcementsVo);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return view;
	}

	/**
	 * method to upload announcement documents
	 * 
	 * @param multipartFiles
	 * @return documentsVos
	 */
	private List<AnnouncementDocumentsVo> uploadAnnouncementDocuments(MultipartHttpServletRequest request) {
		List<AnnouncementDocumentsVo> documentsVos = new ArrayList<AnnouncementDocumentsVo>();
		try {
			Iterator<String> itrator = request.getFileNames();
			while (itrator.hasNext()) {
				MultipartFile multipartFile = request.getFile(itrator.next());
				if (!multipartFile.isEmpty()) {
					String fileAccesUrl = FileUpload
							.uploadDocument(MessageConstants.ANNOUNCEMENT_MODULE, multipartFile);
					if (fileAccesUrl != null) {
						AnnouncementDocumentsVo documentVo = new AnnouncementDocumentsVo();
						documentVo.setDocumentUrl(fileAccesUrl);
						documentVo.setDocumentName(multipartFile.getOriginalFilename());
						documentsVos.add(documentVo);
					}
				}

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return documentsVos;
	}

	/**
	 * method to delete announcement document
	 * 
	 * @param documentId
	 * @param documentUrl
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ANNOUNCEMENT_DOC_DELETE_URL, method = RequestMethod.POST)
	public String deleteAnnouncementDocumentWithPost(
			@RequestParam(value = "documentId", required = true) Long documentId,
			@RequestParam(value = "documentUrl", required = true) String documentUrl) {
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			if (sftpOperation.removeFileFromSFTP(documentUrl))
				announcementsService.deleteAnnouncementDocument(documentId);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ANNOUNCEMENT_DOC_LIST_PAGE;
	}

	/**
	 * method to update announcements
	 * 
	 * @param model
	 * @return
	 * 
	 */
	@RequestMapping(value = RequestConstants.UPDATE_ANNOUNCEMENTS_URL, method = RequestMethod.POST)
	public String updateAnnouncements(final Model model, @ModelAttribute AnnouncementsVo announcementsVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> Announcements Controller... updateAnnouncements");
		try {
			Date announcemnetEndDate = DateFormatter.convertStringToDate(announcementsVo.getEndDate());
			Date announcemnetStartDate = DateFormatter.convertStringToDate(announcementsVo.getStartDate());
			// Date
			// announcemnetDate=DateFormatter.convertStringToDate(announcementsVo.getEndDate());
			Date dateobj = new Date();
			if (dateobj.compareTo(announcemnetEndDate) < 0 && announcemnetEndDate.compareTo(announcemnetStartDate) > 0) {
				announcementsService.updateAnnouncements(announcementsVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.ANNOUNCEMENTS_UPDATE_SUCCESS);
				model.addAttribute("announcements", announcementsService.findAllAnnouncements());
			} else {
				model.addAttribute("announcements", announcementsService.findAllAnnouncements());
				model.addAttribute(MessageConstants.ERROR, MessageConstants.ANNOUNCEMENTS_INVALID_DATE);
			}
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ANNOUNCEMENTS_PAGE;
	}

	/**
	 * method to display selected announcements details
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_ANNOUNCEMENTS_DETAILS_URL, method = RequestMethod.GET)
	public String viewAnnouncementsDetails(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> ProjecController... viewAnnouncementsDetails");
		try {
			model.addAttribute("status", announcementsStatusService.findAllAnnouncementsStatus());
			model.addAttribute("announcements",
					announcementsService.findAnnouncementsById(Long.parseLong(request.getParameter("announcementsId"))));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.VIEW_ANNOUNCEMENTS_DETAILS_PAGE;
	}

	/**
	 * method to reach edit announcements page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_ANNOUNCEMENTS_URL, method = RequestMethod.GET)
	public String editAnnouncementDetailsGet(final Model model, HttpServletRequest request,
			@ModelAttribute AnnouncementsVo announcementsVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> announcemntController... editAnnouncementDetailsGet");
		try {
			model.addAttribute("announcementsVo",
					announcementsService.findAnnouncementsById(Long.parseLong(request.getParameter("announcementsId"))));
			model.addAttribute("status", announcementsStatusService.findAllAnnouncementsStatus());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.EDIT_ANNOUNCEMENTS_PAGE;
	}

	/**
	 * method to delete announcements
	 * 
	 * @param model
	 * @param request
	 * @param announcementsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_ANNOUNCEMENTS_URL, method = RequestMethod.GET)
	public String deleteAnnouncements(final Model model, HttpServletRequest request,
			@ModelAttribute AnnouncementsVo announcementsVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> ProjecController... deleteProject");
		try {
			announcementsService.deleteAnnouncements(Long.parseLong(request.getParameter("announcementsId")));
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.ANNOUNCEMENTS_DELETE_SUCCESS);
			model.addAttribute("announcements", announcementsService.findAllAnnouncements());

		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			return PageNameConstatnts.ANNOUNCEMENTS_PAGE;
		}
		return PageNameConstatnts.ANNOUNCEMENTS_PAGE;
	}

	/**
	 * method to list all documents of an announcement
	 * 
	 * @param model
	 * @param announcementId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ANNOUNCEMENT_DOC_LIST_PAGE_URL, method = RequestMethod.POST)
	public String listAllWarningDocumentsWithPost(final Model model,
			@RequestParam(value = "announcementId", required = true) Long announcementId,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("announcementDocList", announcementsService.findAnnouncementsById(announcementId));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ANNOUNCEMENT_DOC_LIST_PAGE;
	}

	/**
	 * method to update announcements details
	 * 
	 * @param announcementsVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_ANNOUNCEMENTS_STATUS_URL, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse addAnnouncementstStatus(@RequestBody AnnouncementsVo announcementsVo,
			HttpSession session) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			System.out.println("updating announcements status..title>> " + announcementsVo.getAnnouncementsTitle());
			announcementsService.updateAnnouncementStatus(announcementsVo);
			jsonResponse.setStatus(MessageConstants.PROJECT_SAVE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			jsonResponse.setStatus(MessageConstants.PROJECT_SAVE_FAILED);
		}
		return jsonResponse;
	}
}
