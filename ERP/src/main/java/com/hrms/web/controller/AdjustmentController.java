package com.hrms.web.controller;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.AdjustmentService;
import com.hrms.web.service.AdjustmentTitleService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.vo.AdjustmentTitleVo;
import com.hrms.web.vo.AdjustmentVo;
import com.hrms.web.vo.BonusTitleVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 14-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class AdjustmentController {

	@Log
	private Logger logger;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private AdjustmentTitleService titleService;

	@Autowired
	private AdjustmentService adjustmentService;

	/**
	 * method to reach adjustment home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADJUSTMENT_URL, method = RequestMethod.GET)
	public String adjustmentHomePage(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("adjustments",
					adjustmentService.listAdjustmentsByEmployee(authEmployeeDetails.getEmployeeId()));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADJUSTMENT_PAGE;
	}

	/**
	 * method to reach add adjustment page
	 * 
	 * @param model
	 * @param adjustmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADJUSTMENT_ADD_URL, method = RequestMethod.GET)
	public String addNewAdjustment(final Model model, @ModelAttribute AdjustmentVo adjustmentVo,
			@ModelAttribute AdjustmentTitleVo adjustmentTitleVo, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("employees", employeeService.listEmployeesWithPayroll());
			model.addAttribute("titles", titleService.listAllAdjustmentTitles());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADJUSTMENT_ADD_PAGE;
	}

	/**
	 * method to edit adjustment
	 * 
	 * @param model
	 * @param request
	 * @param adjustmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADJUSTMENT_EDIT_URL, method = RequestMethod.GET)
	public String editAdjustment(final Model model, HttpServletRequest request,
			@ModelAttribute AdjustmentTitleVo adjustmentTitleVo, @ModelAttribute AdjustmentVo adjustmentVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("employees", employeeService.listEmployeesWithPayroll());
			model.addAttribute("titles", titleService.listAllAdjustmentTitles());
			model.addAttribute("adjustmentVo",
					adjustmentService.findAdjustmentById(Long.parseLong(request.getParameter("id"))));
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADJUSTMENT_ADD_PAGE;
	}

	/**
	 * method to view adjustment
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADJUSTMENT_VIEW_URL, method = RequestMethod.GET)
	public String viewAdjustment(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("adjustmentVo",
					adjustmentService.findAdjustmentById(Long.parseLong(request.getParameter("id"))));
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADJUSTMENT_VIEW_PAGE;
	}

	/**
	 * method to delete adjustment
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADJUSTMENT_DELETE_URL, method = RequestMethod.GET)
	public String deleteAdjustment(final Model model, HttpServletRequest request) {
		try {
			adjustmentService.deleteAdjustment(Long.parseLong(request.getParameter("id")));
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.ADJUSTMENT_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.ADJUSTMENT_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.ADJUSTMENT_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.ADJUSTMENT_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.ADJUSTMENT_DELETE_SUCCESS;
	}

	/**
	 * method to save adjustment title
	 * 
	 * @param model
	 * @param adjustmentVo
	 * @param adjustmentTitleVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADJUSTMENT_TITLE_SAVE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveAdjustmentTitle(@RequestBody AdjustmentTitleVo adjustmentTitleVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			titleService.saveAdjustmentTitle(adjustmentTitleVo);
			jsonResponse.setStatus("SUCCESS");
		} catch (DuplicateItemException e) {
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}
	
	/**
	 * method to get available bonus titles
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_AVAILABLE_ADJUSTMENT_TITLES , method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableAdjustmentTitles(){
		JsonResponse jsonResponse = new JsonResponse();
		try{
			jsonResponse.setResult(titleService.listAllAdjustmentTitles());
			jsonResponse.setStatus("SUCCESS");
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save or update adjustment
	 * 
	 * @param model
	 * @param adjustmentVo
	 * @param adjustmentTitleVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADJUSTMENT_SAVE_URL, method = RequestMethod.POST)
	public String saveOrUpdateAdjustment(final Model model, @ModelAttribute AdjustmentVo adjustmentVo,
			@ModelAttribute AdjustmentTitleVo adjustmentTitleVo) {
		try {
			if (adjustmentVo.getAdjustmentId() != null) {
				adjustmentService.updateAdjustment(adjustmentVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.ADJUSTMENT_UPDATE_SUCCESS);
			} else {
				Long id = adjustmentService.saveAdjustment(adjustmentVo);
				adjustmentVo.setAdjustmentId(id);
				adjustmentVo.setId(id);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.ADJUSTMENT_SAVE_SUCCESS);
			}
			model.addAttribute("employees", employeeService.listEmployeesWithPayroll());
			model.addAttribute("titles", titleService.listAllAdjustmentTitles());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.ADJUSTMENT_SAVE_FAILED);
		}
		return "redirect:" + RequestConstants.ADJUSTMENT_EDIT_URL + "?id=" + adjustmentVo.getAdjustmentId();
	}

	/**
	 * method to get adjustment by employee and title
	 * 
	 * @param model
	 * @param employeeId
	 * @param titleId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ADJUSTMENT_BY_TITLE_AND_EMPLOYEE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getAdjustmentByEmployeeAndTitle(final Model model,
			@RequestParam(value = "employeeCode") String code, @RequestParam(value = "titleId") Long titleId) {
		logger.info("------------------------------------- " + code + "   <>  " + titleId);
		JsonResponse response = new JsonResponse();
		try {
			response.setStatus("ok");
			response.setResult(adjustmentService.getAdjustmentByEmployeeAndTitle(code, titleId));
		} catch (ItemNotFoundException e) {
			logger.info(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
}
