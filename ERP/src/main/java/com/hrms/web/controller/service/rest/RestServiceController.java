package com.hrms.web.controller.service.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hrms.web.service.AttendanceService;
import com.hrms.web.vo.RestBiometricAttendanceVo;
import com.hrms.web.vo.RestServiceVo;

/**
 * 
 * @author Shamsheer
 * @since 3-July-2015
 */
@RestController
@RequestMapping(value = "/service")
public class RestServiceController {

	@Autowired
	private AttendanceService attendanceService;

	/**
	 * true if request is authentiated.. will be refered by audit interceptor on crud operations
	 */
	public static Boolean authenticatedRequest;

	/**
	 * method to validate request
	 * 
	 * @param key
	 * @param secret
	 * @return
	 */
	private static boolean validateUser(String key, String secret) {
		String uniqueTimeStmpWithString = key + "_calicut";
		String expectedSecret = DigestUtils.md5Hex(uniqueTimeStmpWithString);
		if (expectedSecret.equals(secret)) {
			authenticatedRequest=true;
			return true;
		}
		return false;
	}

	/**
	 * method to accept attendances from client application
	 * @param restServiceVo
	 * @return
	 */
	@RequestMapping(value = "uploadAttendance", method = RequestMethod.POST)
	public @ResponseBody Boolean getAttendancesFromBiometricDeviceLog(@RequestBody RestServiceVo restServiceVo) {
		if (!validateUser(restServiceVo.getKey(), restServiceVo.getSecret()))
			return false;
		else {
			Map<String, RestBiometricAttendanceVo> map = findTimes(restServiceVo.getAttendanceVos());
			attendanceService.saveAttendanceFromRestService(map);
			return true;
		}
	}

	/**
	 * method to collect and bind each employee details in a map 
	 * @param attendanceVos
	 * @return
	 */
	private Map<String, RestBiometricAttendanceVo> findTimes(List<RestBiometricAttendanceVo> attendanceVos) {
		Map<String, RestBiometricAttendanceVo> map = new HashMap<String, RestBiometricAttendanceVo>();
		for (RestBiometricAttendanceVo vo : attendanceVos) {
			RestBiometricAttendanceVo attendanceVo = map.get(vo.getEmployeeCode());
			if (attendanceVo == null) {
				attendanceVo = new RestBiometricAttendanceVo();
				attendanceVo.getTimes().add(vo.getTime());
				map.put(vo.getEmployeeCode(), attendanceVo);
			} else {
				attendanceVo.getTimes().add(vo.getTime());
			}
		}
		return map;
	}

}
