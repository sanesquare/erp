package com.hrms.web.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ForwardApplicationStatusService;
import com.hrms.web.service.PayslipService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.PaySalaryEmployeeItemsVo;
import com.hrms.web.vo.PaysSlipVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 28-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class PayslipController {

	@Autowired
	PayslipService payslipService;
	
	@Autowired
	private ForwardApplicationStatusService statusService;
	
	@Autowired
	private EmployeeService employeeService;

	@Log
	private Logger logger;

	/**
	 * method to reach payslip home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSLIP_URL, method = RequestMethod.GET)
	public String payslipHomePage(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String erroMessage = request.getParameter(MessageConstants.ERROR);
		if (successMessage != null)
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
		else if (erroMessage != null)
			model.addAttribute(MessageConstants.ERROR, erroMessage);
		try {
			model.addAttribute("payslips",
					payslipService.findAllPayslipsByEmployee(authEmployeeDetails.getEmployeeCode()));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.PAYSLIP_PAGE;
	}

	/**
	 * method to reach payslip add page
	 * 
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSLIP_ADD_URL, method = RequestMethod.GET)
	public String addPaySlip(final Model model, @ModelAttribute PaysSlipVo paysSlipVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			paysSlipVo.setEmployee(authEmployeeDetails.getName());
			paysSlipVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.PAYSLIP_ADD_PAGE;
	}

	/**
	 * method to get payslip details by employee
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSLIP_GET_DETAILS_BY_EMPLOYEE, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getPayslipDetailsByEmployee(
			@RequestParam(value = "employeeCode", required = true) String employeeCode,
			@RequestParam(value = "fromMonth", required = true) String fromMonth,
			@RequestParam(value = "toMonth", required = true) String toMonth,
			@RequestParam(value = "fromYear", required = true) String fromYear,
			@RequestParam(value = "toYear", required = true) String toYear) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			String endDate = DateFormatter.getEndDateFromMonthAndYear(toMonth, toYear);
			String startDate = "01/"+fromMonth+"/"+fromYear;
			List<PaySalaryEmployeeItemsVo> paysSlipVos = payslipService.getPayslipDetailsByEmployee(employeeCode, startDate, endDate);
			jsonResponse.setResult(paysSlipVos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save payslip
	 * 
	 * @return
	 */
	/*@RequestMapping(value = RequestConstants.PAYSLIP_SAVE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse savePayslip(@RequestBody PaysSlipVo paysSlipVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (paysSlipVo.getId() == null)
				jsonResponse.setResult(payslipService.savePayslip(paysSlipVo));
			else {
				payslipService.updatePayslip(paysSlipVo);
				jsonResponse.setResult(paysSlipVo.getId());
			}
			jsonResponse.setStatus("ok");
		} catch (Exception e) {
			jsonResponse.setStatus("error");
			e.printStackTrace();
		}
		return jsonResponse;
	}*/

	@RequestMapping(value = RequestConstants.PAYSLIP_SAVE_URL, method = RequestMethod.POST)
	public String savePaySlip(final Model model , @ModelAttribute PaysSlipVo paysSlipVo){
		try {
			paysSlipVo.setToDate(DateFormatter.getEndDateFromMonthAndYear(paysSlipVo.getToMonth(), paysSlipVo.getToYear()));
			paysSlipVo.setFromDate("01/"+paysSlipVo.getFromMonth()+"/"+paysSlipVo.getFromYear());
			if (paysSlipVo.getId() == null)
				paysSlipVo.setId(payslipService.savePayslip(paysSlipVo));
			else {
				payslipService.updatePayslip(paysSlipVo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.PAYSLIP_EDIT_URL+"?id="+paysSlipVo.getId();
	}

	/**
	 * method to delete payslip
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSLIP_DELETE_URL, method = RequestMethod.GET)
	public String deletePayslip(Model model, @RequestParam(value = "id", required = true) String id) {
		try {
			payslipService.deletPayslip(Long.parseLong(id));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.PAYSLIP_URL;
	}

	/**
	 * method to view payslip
	 * 
	 * @param model
	 * @param id
	 * @param paysSlipVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSLIP_EDIT_URL, method = RequestMethod.GET)
	public String editPayslip(Model model, @RequestParam(value = "id", required = true) String id,
			@ModelAttribute PaysSlipVo paysSlipVo, @ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			paysSlipVo = payslipService.getPayslipById(Long.parseLong(id));
			model.addAttribute("paysSlipVo", paysSlipVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.PAYSLIP_ADD_PAGE;
	}

	/**
	 * method to view payslip
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSLIP_VIEW_URL, method = RequestMethod.GET)
	public String viewPayslip(Model model, @RequestParam(value = "id", required = true) String id,
			@ModelAttribute PaysSlipVo paysSlipVo ,
			@ModelAttribute("roles") SessionRolesVo rolesMap, @ModelAttribute("authEmployeeDetails")EmployeeVo employeeVo) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("payslip", payslipService.getPayslipById(Long.parseLong(id)));
			model.addAttribute("status", statusService.listAllStatus());
			List<EmployeeVo> employeeVos = employeeService.findPayrollOptionEmployees();
			System.out.println("List size .>>>> "+employeeVos.size());
			String employeeCode = employeeVo.getEmployeeCode();
			Boolean authEmployee = false;
			for(EmployeeVo vo : employeeVos){
				if(vo.getEmployeeCode().equalsIgnoreCase(employeeCode))
					authEmployee = true;
			}
			paysSlipVo.setAuthEmployee(authEmployee);
			/*if(employeeVos.contains(employeeVo)){
				System.out.println("auth employee : true");
				paysSlipVo.setAuthEmployee(true);
			}else{
				paysSlipVo.setAuthEmployee(false);
				System.out.println("auth employee : false");
			}*/
			model.addAttribute("paysSlipVo", paysSlipVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.PAYSLIP_VIEW_PAGE;
	}
	
	/**
	 * method to update payslip status
	 * @param model
	 * @param paysSlipVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_PAYSLIP_STATUS , method = RequestMethod.POST)
	public String updateStatus(final Model model ,  @ModelAttribute PaysSlipVo paysSlipVo){
		try{
			payslipService.updatePayslipStatus(paysSlipVo);
		}catch(ItemNotFoundException e){
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.PAYSLIP_URL;
	}
}
