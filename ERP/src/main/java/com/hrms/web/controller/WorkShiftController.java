package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ShiftDayService;
import com.hrms.web.service.WorkShiftDetailsService;
import com.hrms.web.util.WeekDays;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.ShiftDayVo;
import com.hrms.web.vo.WorkShiftDetailsVo;

/**
 * 
 * @author Jithin Mohan
 * @since 31-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "thisUser","roles"  })
public class WorkShiftController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private ShiftDayService shiftDayService;

	@Autowired
	private WorkShiftDetailsService workShiftDetailsService;

	@Autowired
	private EmployeeService employeeService;

	/**
	 * method to list all workshifts
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORK_SHIFT_PAGE_URL, method = RequestMethod.GET)
	public String loadAllWorkShifts(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("workShiftList", workShiftDetailsService.findAllWorkShiftDetails());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.WORK_SHIFT_PAGE;
	}

	/**
	 * method to load create work shift page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.CREATE_WORK_SHIFT_PAGE_URL, method = RequestMethod.GET)
	public String loadCreateWorkShiftPage(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("shiftDays", shiftDayService.findAllShiftDay());
			// model.addAttribute("employeeList",
			// getEmployeeWithCode(employeeService.listAllEmployee()));
			model.addAttribute("employeeList", Employee.getEmployees(employeeService.listEmployeesWithPayroll()));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.WORK_SHIFT_SAVE_PAGE;
	}

	/**
	 * method to list employee code with name
	 * 
	 * @param employeeVos
	 * @return employeeCodes
	 */
	/*private List<String> getEmployeeWithCode(List<EmployeeVo> employeeVos) {
		List<String> employeeCodes = new ArrayList<String>();
		for (EmployeeVo employeeVo : employeeVos)
			employeeCodes.add(employeeVo.getEmployeeCode());
		return employeeCodes;
	}*/

	/**
	 * method to get work shift details of a given employee
	 * 
	 * @param employeeCode
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_WORK_SHIFT_DETAILS_PAGE_URL, method = RequestMethod.POST)
	public String getEmployeeWorkShiftDetailsWithPost(
			@RequestParam(value = "code", required = true) String employeeCode, final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("employeeCode", employeeCode);
			// model.addAttribute("employeeList",
			// getEmployeeWithCode(employeeService.listAllEmployee()));
			model.addAttribute("employeeList", Employee.getEmployees(employeeService.listEmployeesWithPayroll()));
			model.addAttribute("employeeShiftType", employeeService.getEmployeeByCode(employeeCode).getWorkShift());
			model.addAttribute(
					"workShiftList",
					createAvailableShiftDetailsInfo(shiftDayService.findAllShiftDay(), workShiftDetailsService
							.findWorkShiftDetailsByEmployee(employeeService.findAndReturnEmployeeByCode(employeeCode))));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.EMPLOYEE_WORK_SHIFT_INFO_PAGE;
	}

	/**
	 * method to create available shift details info
	 * 
	 * @param dayVos
	 * @param detailsVos
	 * @return
	 */
	private List<WorkShiftDetailsVo> createAvailableShiftDetailsInfo(List<ShiftDayVo> dayVos,
			List<WorkShiftDetailsVo> detailsVos) {
		List<WorkShiftDetailsVo> shiftDetailsVos = new ArrayList<WorkShiftDetailsVo>();
		for (ShiftDayVo dayVo : dayVos) {
			boolean isAvailable = false;
			for (WorkShiftDetailsVo detailsVo : detailsVos) {
				if (dayVo.getShiftDay().equalsIgnoreCase(detailsVo.getShiftDay())) {
					detailsVo.setAvailable(true);
					shiftDetailsVos.add(detailsVo);
					isAvailable = true;
					break;
				}
			}
			if (!isAvailable) {
				WorkShiftDetailsVo detailsVo = new WorkShiftDetailsVo();
				detailsVo.setShiftDay(dayVo.getShiftDay());
				detailsVo.setAvailable(false);
				shiftDetailsVos.add(detailsVo);
			}
		}
		return shiftDetailsVos;
	}

	/**
	 * method to save work shift details
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORK_SHIFT_SAVE_PAGE_URL, method = RequestMethod.POST)
	public ModelAndView saveWorkShiftDetailsWithPost(final Model model, final HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.CREATE_WORK_SHIFT_PAGE_URL));
		try {
			workShiftDetailsService.saveWorkShiftDetails(setShiftDetailsByDay(request));
			redirectAttributes
					.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.EMPLOYEE_WORK_SHIFT_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to set every section of a day
	 * 
	 * @param begin
	 * @param request
	 * @return shiftDetailsVo
	 */
	private WorkShiftDetailsVo setAllPartOfaShift(int begin, final HttpServletRequest request) {
		WorkShiftDetailsVo shiftDetailsVo = new WorkShiftDetailsVo();
		int count = (begin - 1) * 6;
		shiftDetailsVo.setRegularWorkFrom(request.getParameter("time_setter" + (count + 1)));
		shiftDetailsVo.setRegularWorkTo(request.getParameter("time_setter" + (count + 2)));
		shiftDetailsVo.setRefreshmentBreakFrom(request.getParameter("time_setter" + (count + 3)));
		shiftDetailsVo.setRefreshmentBreakTo(request.getParameter("time_setter" + (count + 4)));
		shiftDetailsVo.setAdditionalBreakFrom(request.getParameter("time_setter" + (count + 5)));
		shiftDetailsVo.setAdditionalBreakTo(request.getParameter("time_setter" + (count + 6)));
		shiftDetailsVo.setShiftDay(WeekDays.WeekDay.get(begin).name());
		shiftDetailsVo.setEmployee(request.getParameter("employeeCode"));
		shiftDetailsVo.setWorkShiftId(Long.parseLong(request.getParameter("workShiftId" + begin)));
		return shiftDetailsVo;
	}

	/**
	 * method to set shiftdetails by day
	 * 
	 * @param request
	 * @return shiftDetails
	 */
	private List<WorkShiftDetailsVo> setShiftDetailsByDay(HttpServletRequest request) {
		List<WorkShiftDetailsVo> shiftDetails = new ArrayList<WorkShiftDetailsVo>();
		if (request.getParameter("shiftDay1") != null) {
			shiftDetails.add(setAllPartOfaShift(1, request));
		}

		if (request.getParameter("shiftDay2") != null) {
			shiftDetails.add(setAllPartOfaShift(2, request));
		}

		if (request.getParameter("shiftDay3") != null) {
			shiftDetails.add(setAllPartOfaShift(3, request));
		}

		if (request.getParameter("shiftDay4") != null) {
			shiftDetails.add(setAllPartOfaShift(4, request));
		}

		if (request.getParameter("shiftDay5") != null) {
			shiftDetails.add(setAllPartOfaShift(5, request));
		}

		if (request.getParameter("shiftDay6") != null) {
			shiftDetails.add(setAllPartOfaShift(6, request));
		}

		if (request.getParameter("shiftDay7") != null) {
			shiftDetails.add(setAllPartOfaShift(7, request));
		}
		return shiftDetails;
	}
}
