package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.erp.web.constants.ErpConstants;
import com.erp.web.constants.ErpRequestConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.notifications.NotificationContent;
import com.hrms.web.notifications.PushNotification;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.AnnouncementsService;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.TempNotificationService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.ReadFromExcel;
import com.hrms.web.vo.AnnouncementsVo;
import com.hrms.web.vo.BiometricAttendanceVo;
import com.hrms.web.vo.ChangePasswordVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.PopUpVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles", "thisUser", "autoApprove" })
public class HrmsController {

	/**
	 * Logger
	 */
	@Log
	private static Logger logger;

	@Autowired
	private AnnouncementsService announcementsService;

	@Autowired
	private UserService userService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private PushNotification pushNotification;

	@Autowired
	@Qualifier(value = "taskExecutor")
	private TaskExecutor taskExecutor;

	@Autowired
	private TempNotificationService notificationService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	private List<Long> branchIds = new ArrayList<Long>();
	private List<Long> departmentIds = new ArrayList<Long>();

	/**
	 * method to map login page
	 * 
	 * @param model
	 * @return PageNameConstatnts.INDEX
	 */
	@RequestMapping(value = RequestConstants.BASE_URL, method = RequestMethod.GET)
	public String baseUrlMappingAsGet(final Model model, @RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			return "redirect:" + ErpRequestConstants.HOME;
		}

		if (error != null) {
			model.addAttribute("status", "Invalid Username or Password");
		}
		if (logout != null) {
			model.addAttribute("status", "You have been logged out successfully.");
		}
		return PageNameConstatnts.LOGIN_PAGE;
	}

	/**
	 * method to clear login section
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SIGN_OUT_URL, method = RequestMethod.GET)
	public String signoutUrlMapping() {
		return PageNameConstatnts.LOGIN_PAGE;
	}

	/**
	 * method to map home page
	 * 
	 * @param model
	 * @return PageNameConstatnts.INDEX
	 */
	@RequestMapping(value = RequestConstants.HOME_URL, method = RequestMethod.GET)
	public String homePageUrlMappingAsGet(final Model model, @ModelAttribute AnnouncementsVo announcementsVo,
			@RequestParam(value = "jsession", defaultValue = "SA") String jsession,
			@ModelAttribute ChangePasswordVo changePasswordVo) {
		logger.info("Inside HrmsController>>>> baseUrlMappingAsGet.........");
		this.checkAnnouncementsStatus(model, announcementsVo);

		// Test Mail
		/*
		 * List<String> emails=new ArrayList<String>();
		 * emails.add("j4jithinmohan@gmail.com");
		 * emails.add("amalams181@gmail.com"); ApplicationMailSender
		 * applicationMailSender = new ApplicationMailSender(emails); try {
		 * taskExecutor.execute(applicationMailSender); } catch (Exception e) {
		 * e.printStackTrace(); }
		 */

		// To carry over leaves
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Integer day = calendar.get(Calendar.DAY_OF_MONTH);
		Integer month = calendar.get(Calendar.MONTH);
		if (month == 11 && day == 31) {
			employeeService.leaveCarryOver();
		}

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Employee employee = userService.findUserByUsername(user.getUsername()).getEmployee();

		model.addAttribute("authEmployee", new Employee());
		model.addAttribute("announcements", announcementsService.findAllActiveAnnouncements());
		try {
			Boolean autoApprove = false;
			if (employee != null) {
				if (employee.getIsAutoApprove() != null)
					autoApprove = employee.getIsAutoApprove();
				EmployeeVo employeeVo = employeeService.getEmployeesDetails(employee.getId());
				model.addAttribute("authEmployeeDetails", employeeVo);
				model.addAttribute("thisUser", user.getUsername());
				model.addAttribute("authEmpCode", employeeVo.getEmployeeCode());
				model.addAttribute("roles", employeeService.getEmployeeRoles(employee.getId()));
				if (jsession.equalsIgnoreCase("FA")) {
					sendOnlineNotification(employeeVo.getName());
				}
				SessionRolesVo sessionRolesVo = employeeService.getEmployeeRoles(employee.getId());
				model.addAttribute("roles", sessionRolesVo);
				model.addAttribute("rolesMap", sessionRolesVo);
				model.addAttribute("autoApprove", autoApprove);
			} else {
				model.addAttribute("authEmployeeDetails", new EmployeeVo());
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
		}

		return PageNameConstatnts.INDEX_PAGE;
	}

	private void sendOnlineNotification(String userName) {
		NotificationContent content = new NotificationContent();
		List<String> recipients = new ArrayList<String>();
		recipients.add("ONLINE");
		content.setRecipients(recipients);
		if (userName == null)
			userName = "Anonymous user";
		content.setContent(userName + " is online");
		content.setUrl("");
		content.setPicture("");
		pushNotification.sendNotification(content);
	}

	/**
	 * method to check announcement status
	 * 
	 * @param announcementsVo
	 * @return
	 */
	private void checkAnnouncementsStatus(final Model model, AnnouncementsVo announcementsVo) {

		List<AnnouncementsVo> announcementsVos = announcementsService.findAllActiveAnnouncements();
		if (!announcementsVos.isEmpty()) {
			announcementsVo = announcementsVos.get(announcementsVos.size() - 1);
			Date announcemnetDate = DateFormatter.convertStringToDate(announcementsVo.getEndDate());
			Date dateobj = new Date();
			if (dateobj.compareTo(announcemnetDate) < 0) {
				model.addAttribute("announcements", announcementsVo);
			}
			// announcementsService.updateAnnouncementStatus(announcementsVos);
		}
	}

	@RequestMapping(value = "testwebsocket", method = RequestMethod.GET)
	public String testwebsocketGet(Model model) {
		logger.info("testwebsocketGet........");
		NotificationContent content = new NotificationContent();
		List<String> recipients = new ArrayList<String>();
		recipients.add("jithin");
		recipients.add("sangeeth");
		content.setRecipients(recipients);
		content.setContent("Sample");
		content.setUrl("Dashboard");
		content.setPicture("");
		pushNotification.sendNotification(content);
		return PageNameConstatnts.INDEX_PAGE;
	}

	/**
	 * method to change password
	 * 
	 * @param model
	 * @param changePasswordVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.CHANGE_PASSWORD, method = RequestMethod.POST)
	public @ResponseBody JsonResponse changePassword(final Model model, @RequestBody ChangePasswordVo changePasswordVo,
			RedirectAttributes redirectAttributes) {
		JsonResponse view = new JsonResponse();
		try {
			boolean reult = userService.changePassword(changePasswordVo.getUserName(),
					changePasswordVo.getOldPassword(), changePasswordVo.getNewPassword());
			if (reult) {
				view.setStatus("ok");
			}
		} catch (BadCredentialsException e) {
			view.setStatus(e.getMessage());
		} catch (Exception e) {
			view.setStatus("Password Change Failed");
			e.printStackTrace();
		}
		return view;
	}

	/**
	 * method to get notifications
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_NOTIFICATIONS, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getNotifications(final Model model,
			@ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(notificationService.listNotificationForEmployee(employeeVo.getEmployeeCode()));
			jsonResponse.setStatus("ok");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to delete notification
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_NOTIFICATION, method = RequestMethod.GET)
	public @ResponseBody JsonResponse deleteNotification(@RequestParam(value = "id", required = true) Long id,
			final Model model, @ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			notificationService.deleteNotification(id, employeeVo.getEmployeeCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	@RequestMapping(value = RequestConstants.POP_UP, method = RequestMethod.GET)
	public String employeePOpUp(final Model model) {
		try {
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.POP_UP;
	}

	@RequestMapping(value = "filteremp", method = RequestMethod.GET)
	public String filter(final Model model) {
		try {
			List<EmployeeVo> employees = new ArrayList<EmployeeVo>();
			if (!branchIds.isEmpty() && !departmentIds.isEmpty()) {
				employees = employeeService.getEmployeesByBranchesAndDepartments(branchIds, departmentIds);
			} else if (!branchIds.isEmpty())
				employees = employeeService.getEmployeesByBranches(branchIds);
			else if (!departmentIds.isEmpty())
				employees = employeeService.getEmployeesByDepartments(departmentIds);
			else
				employees = employeeService.listAllEmployee();
			model.addAttribute("employees", employees);
			branchIds.clear();
			departmentIds.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.POP_UP_EMPLOYEES;
	}

	@RequestMapping(value = RequestConstants.POP_UP_EMPLOYEES, method = RequestMethod.POST)
	public @ResponseBody void employeePOpUpEmployees(final Model model, @RequestBody PopUpVo popUpVo) {
		try {
			branchIds = popUpVo.getBranchIdList();
			departmentIds = popUpVo.getDepartmentIdList();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
