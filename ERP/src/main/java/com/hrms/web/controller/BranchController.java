package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BaseCurrencyService;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.BranchTypeService;
import com.hrms.web.service.CountryService;
import com.hrms.web.service.TimeZoneService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.BranchDocumentVo;
import com.hrms.web.vo.BranchTypeVo;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.ProjectDocumentsVo;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SFTPFilePropertiesVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Vinutha
 * @since 12-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails" , "roles"})
public class BranchController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private BranchService branchService;

	@Autowired
	private BranchTypeService branchTypeService;

	@Autowired
	private BaseCurrencyService baseCurrencyService;

	@Autowired
	private CountryService countryService;

	@Autowired
	private TimeZoneService timeZoneService;

	/**
	 * method to handle branch page get request
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value=RequestConstants.BRANCH_URL,method=RequestMethod.GET)
	public String branchPageGetRequestHandler(final Model model,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		logger.info("inside>> BranchController... branchPageGetRequestHandler");
		model.addAttribute("rolesMap", rolesMap);
		String msg=request.getParameter(MessageConstants.SUCCESS);
		String errMsg=request.getParameter(MessageConstants.ERROR);
		if(msg!=null){
			model.addAttribute(MessageConstants.SUCCESS,msg);
		}
		else if(errMsg!=null)
		{
			model.addAttribute(MessageConstants.ERROR,errMsg);
		}
		try {
			model.addAttribute("branches", branchService.listAllBranches());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.BRANCHES_PAGE;
	}

	/**
	 * method to handle addBranch page get request
	 * 
	 * @param model
	 * @param branchVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.ADD_BRANCH_URL,method=RequestMethod.GET)
	public String addBranchPageGetRequestHandler(final Model model,@ModelAttribute BranchVo branchVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> BranchController... addBranchPageGetRequestHandler");
		try {
			model.addAttribute("timeZones", timeZoneService.findAllTimeZone());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("branchTypes", branchTypeService.findAllBranchType());
			model.addAttribute("baseCurrencies", baseCurrencyService.findAllBaseCurrency());
			model.addAttribute("countries", countryService.findAllCountry());
			return PageNameConstatnts.ADD_BRANCHES_PAGE;
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
			return PageNameConstatnts.BRANCHES_PAGE;
		}

	}

	/**
	 * method to handle saveBranch post request
	 * 
	 * @param model
	 * @param branchVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_BRANCH_URL, method = RequestMethod.POST)
	public String saveBranchPageGetRequestHandler(final Model model, @ModelAttribute BranchVo branchVo,
			HttpServletRequest request) {
		logger.info("inside>> BranchController... saveBranchPageGetRequestHandler");

		try {
			if (request.getParameter("uploadDocuments") != null) {
				try {
					if (branchVo.getBranchId() != null) {
						branchVo.setBranchDocuments(uploadDocuments(branchVo.getBranchUploadFiles()));
						branchService.updateBranchDocument(branchVo);
						model.addAttribute(MessageConstants.SUCCESS, MessageConstants.FILE_UPLOAD_SUCCESS);
					}
				} catch (Exception e) {
					model.addAttribute(MessageConstants.ERROR, MessageConstants.FILE_UPLOAD_FAILED);
					logger.info(e.getMessage());
					e.printStackTrace();
					/*return "redirect:" + RequestConstants.ADD_BRANCH_URL + "?" + MessageConstants.ERROR + "="
							+ MessageConstants.WRONG;*/
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			/*return "redirect:" + RequestConstants.BRANCH_URL;*/
		}

		return "redirect:" + RequestConstants.BRANCH_URL;
	}

	/**
	 * method to upload branch documents
	 * 
	 * @param files
	 * @return
	 */
	private Set<BranchDocumentVo> uploadDocuments(List<MultipartFile> files) {
		Set<BranchDocumentVo> documentsVos = new HashSet<BranchDocumentVo>();
		for (MultipartFile file : files) {
			if (!file.isEmpty()) {
				String url = FileUpload.uploadDocument(MessageConstants.BRANCH_MODULE, file);
				if (url != null) {
					BranchDocumentVo vo = new BranchDocumentVo();
					vo.setDocumentUrl(url);
					vo.setName(file.getOriginalFilename());
					documentsVos.add(vo);
				}
			}
		}
		return documentsVos;
	}

	/**
	 * method to handle get request for viewing branch details
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.VIEW_BRANCH_DETAILS_URL,method=RequestMethod.GET)
	public String viewBranchDetails(final Model model,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		logger.info("inside>> BranchController... viewBranchDetails");
		model.addAttribute("rolesMap", rolesMap);
		try{
			model.addAttribute("branch", branchService.findBranchById(Long.parseLong(request.getParameter("bid"))));

		} catch (Exception e) {
			logger.info(e.getMessage());
			e.printStackTrace();
		}
		return PageNameConstatnts.VIEW_BRANCHES_DETAILS_PAGE;
	}

	/**
	 * method to handle post request for viewing branch details
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.VIEW_BRANCH_DETAILS_URL,method=RequestMethod.POST)
	public String viewBranchDetailsPost(final Model model,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> BranchController... viewBranchDetails");
		try {
			model.addAttribute("branch", branchService.findBranchById(Long.parseLong(request.getParameter("bid"))));

		} catch (Exception e) {
			logger.info(e.getMessage());
			e.printStackTrace();
		}
		return PageNameConstatnts.VIEW_BRANCHES_DETAILS_PAGE;
	}

	/**
	 * Method to handle request to delete existing branch
	 * 
	 * @param model
	 * @param request
	 * @param branchVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_BRANCH_URL, method = RequestMethod.GET)
	public String deleteBranch(final Model model, HttpServletRequest request) {
		logger.info("inside>> BranchController... deleteBranch");
		try {
			branchService.deleteBranchById(Long.parseLong(request.getParameter("bid")));
			model.addAttribute("branches", branchService.listAllBranches());
			return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.SUCCESS + "="
					+ MessageConstants.BRANCH_DELETE_SUCCESS;
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
			e.printStackTrace();

		}
		return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.WRONG;
	}

	/**
	 * Method to handle post request to delete existing branch
	 * 
	 * @param model
	 * @param request
	 * @param branchVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_BRANCH_URL, method = RequestMethod.POST)
	public String deleteBranchPost(final Model model, HttpServletRequest request) {
		logger.info("inside>> BranchController... deleteBranch");
		try {
			branchService.deleteBranchById(Long.parseLong(request.getParameter("bid")));
			model.addAttribute("branches", branchService.listAllBranches());
			return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.SUCCESS + "="
					+ MessageConstants.BRANCH_DELETE_SUCCESS;
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());

		}
		return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.WRONG;
	}

	/**
	 * Method to edit existing branch
	 * 
	 * @param model
	 * @param request
	 * @param branchVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.EDIT_BRANCH_URL,method=RequestMethod.GET)
	public String editBranch(final Model model,HttpServletRequest request,@ModelAttribute BranchVo branchVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> BranchController... editBranch");
		try {
			model.addAttribute("branchVo", branchService.findBranchById(Long.parseLong(request.getParameter("bid"))));
			model.addAttribute("timeZones", timeZoneService.findAllTimeZone());
			model.addAttribute("branches",
					branchService.listRemainingBranches(Long.parseLong(request.getParameter("bid"))));
			model.addAttribute("branchTypes", branchTypeService.findAllBranchType());
			model.addAttribute("baseCurrencies", baseCurrencyService.findAllBaseCurrency());
			model.addAttribute("countries", countryService.findAllCountry());

		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.ADD_BRANCHES_PAGE;
	}

	/**
	 * Method to edit existing branch post request
	 * 
	 * @param model
	 * @param request
	 * @param branchVo
	 * @return
	 */
	@RequestMapping(value=RequestConstants.EDIT_BRANCH_URL,method=RequestMethod.POST)
	public String editBranchDelete(final Model model,HttpServletRequest request,@ModelAttribute BranchVo branchVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> BranchController... editBranch");
		try {
			model.addAttribute("branchVo", branchService.findBranchById(Long.parseLong(request.getParameter("bid"))));
			model.addAttribute("timeZones", timeZoneService.findAllTimeZone());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("branchTypes", branchTypeService.findAllBranchType());
			model.addAttribute("baseCurrencies", baseCurrencyService.findAllBaseCurrency());
			model.addAttribute("countries", countryService.findAllCountry());

		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.ADD_BRANCHES_PAGE;
	}

	/**
	 * method to update branch
	 * 
	 * @param model
	 * @param request
	 * @param branchVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_BRANCH_URL, method = RequestMethod.GET)
	public String updateBranch(final Model model, HttpServletRequest request, @ModelAttribute BranchVo branchVo) {
		logger.info("inside>> BranchController... updateBranch");
		try {

			branchService.updateBranch(branchVo);
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.BRANCH_UPDATE_SUCCESS);
			return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.SUCCESS + "="
					+ MessageConstants.BRANCH_UPDATE_SUCCESS;
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.WRONG;
		} finally {

		}

	}

	/**
	 * method to update branch POST request
	 * 
	 * @param model
	 * @param request
	 * @param branchVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_BRANCH_URL, method = RequestMethod.POST)
	public String updateBranchPost(final Model model, HttpServletRequest request, @ModelAttribute BranchVo branchVo) {
		logger.info("inside>> BranchController... updateBranch");
		try {

			branchService.updateBranch(branchVo);
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.BRANCH_UPDATE_SUCCESS);
			return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.SUCCESS + "="
					+ MessageConstants.BRANCH_UPDATE_SUCCESS;
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.WRONG;
		} finally {

		}

	}

	/**
	 * method to save branch details
	 * 
	 * @param branchVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_BRANCH_INFO_URL, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveBranch(@RequestBody BranchVo branchVo, HttpSession session) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (branchVo.getBranchId() == null) {
				Long id = branchService.saveBranch(branchVo);
				jsonResponse.setResult(id);
				jsonResponse.setStatus(MessageConstants.BRANCH_SAVE_SUCCESS);
			} else {
				branchService.updateBranch(branchVo);
				jsonResponse.setStatus(MessageConstants.BRANCH_UPDATE_SUCCESS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonResponse.setStatus(MessageConstants.BRANCH_SAVE_FAILED);
		}
		return jsonResponse;
	}

	/**
	 * method to update branch information
	 * 
	 * @param branchVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_BRANCH_INFORMATION, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse updateBranchInformation(@RequestBody BranchVo branchVo, HttpSession session) {
		logger.info("inside>> BranchController... updateBranchInformation");
		JsonResponse jsonResponse = new JsonResponse();
		try {
			branchService.updateBranch(branchVo);
			jsonResponse.setStatus(MessageConstants.BRANCH_UPDATE_SUCCESS);
		} catch (Exception e) {
			jsonResponse.setStatus(MessageConstants.BRANCH_UPDATE_FAILED);
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to upload files
	 * 
	 * @param multipartFile
	 * @return
	 */
	private String uploadFiles(MultipartFile multipartFile) {
		String url = null;
		SFTPOperation sftpOperation = new SFTPOperation();
		SFTPFilePropertiesVo sftpFilePropertiesVo = new SFTPFilePropertiesVo();
		sftpFilePropertiesVo.setFile(multipartFile);
		sftpFilePropertiesVo.setModuleName("branch");
		sftpFilePropertiesVo.setUserName("hrms");
		sftpFilePropertiesVo.setFileName(multipartFile.getOriginalFilename());
		try {
			url = sftpOperation.sendFileToSFTPServer(sftpFilePropertiesVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return url;
	}

	/**
	 * method to download and show document
	 * 
	 * @param model
	 * @param request
	 * @param branchVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_BRANCH_DOCUMENT, method = RequestMethod.GET)
	public void viewDocumentGet(final Model model, HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute BranchVo branchVo) {
		String url = request.getParameter("durl");
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			sftpOperation.downloadFileFromSFTP(url, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * method to delete uploaded document
	 * 
	 * @param model
	 * @param request
	 * @param branchVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_BRANCH_DOCUMENT, method = RequestMethod.GET)
	public String deleteDocumentGet(final Model model, HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute BranchVo branchVo) {
		String url = request.getParameter("durl");
		Boolean result = false;
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			result = sftpOperation.removeFileFromSFTP(url);
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.DOCUMENT_DELETED_FAILED;
		}
		if (result) {
			try {
				branchService.deleteBranchDocument(url);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.SUCCESS + "="
					+ MessageConstants.DOCUMENT_DELETED_SUCCESSFULLY;
		} else {
			return "redirect:" + RequestConstants.BRANCH_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.DOCUMENT_DELETED_FAILED;
		}
	}
}
