package com.hrms.web.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.service.LedgerService;
import com.erp.web.service.CompanyService;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.PaySlipConstants;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.PaySalaryService;
import com.hrms.web.service.SalaryTypeService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.PaySalaryVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 06-June-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles", "companyId" })
public class PaySalaryController {

	@Log
	private Logger logger;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private SalaryTypeService salaryTypeService;

	@Autowired
	private PaySalaryService paySalaryService;

	@Autowired
	private CompanyService companyService;

	/**
	 * method to reach paysalary home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSALARY_URL, method = RequestMethod.GET)
	public String paySalaryHomePage(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String erroMessage = request.getParameter(MessageConstants.ERROR);
		if (successMessage != null)
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
		else if (erroMessage != null)
			model.addAttribute(MessageConstants.ERROR, erroMessage);
		try {
			model.addAttribute("salaries", paySalaryService.listPaySalaries(authEmployeeDetails.getEmployeeCode()));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.PAY_SALARY;
	}

	@RequestMapping(value = RequestConstants.APPROVED_SALARIES, method = RequestMethod.GET)
	public String approvedSalaries(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String erroMessage = request.getParameter(MessageConstants.ERROR);
		if (successMessage != null)
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
		else if (erroMessage != null)
			model.addAttribute(MessageConstants.ERROR, erroMessage);
		try {
			model.addAttribute("salaries", paySalaryService.findSalariesByStatus("Approved", companyId));
			model.addAttribute("companies", companyService.findChildCompanies(companyId));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.APPROVED_SALARIES;
	}

	@RequestMapping(value = RequestConstants.FILTER_PAY_SALARIES, method = RequestMethod.GET)
	public String approvedSalariesList(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String erroMessage = request.getParameter(MessageConstants.ERROR);
		if (successMessage != null)
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
		else if (erroMessage != null)
			model.addAttribute(MessageConstants.ERROR, erroMessage);
		try {
			model.addAttribute("salaries", paySalaryService.findSalariesByStatus("Approved", companyId));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.SALARIES_LIST;
	}

	/**
	 * method to reach paysalary add page
	 * 
	 * @param model
	 * @param rolesMap
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSALARY_ADD_URL, method = RequestMethod.GET)
	public String addPaySalary(final Model model, @ModelAttribute PaySalaryVo paySalaryVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("salaryTypes", salaryTypeService.listAllSalaryTypes());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.ADD_PAY_SALARY;
	}

	/**
	 * method to calculate paysalary
	 * 
	 * @param model
	 * @param paySalaryVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSALARY_CALCULATE, method = RequestMethod.POST)
	public String calculatePaySalary(final Model model, @ModelAttribute PaySalaryVo paySalaryVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			if (paySalaryVo.getSalaryType() != null && paySalaryVo.getEmployeeIds() != null
					&& paySalaryVo.getEmployeeIds().size() > 0 && paySalaryVo.getDate() != "") {
				String salaryType = salaryTypeService.findSalaryType(paySalaryVo.getSalaryType()).getType();
				PaySalaryVo salaryVo = null;
				if (salaryType.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_DAILY))
					salaryVo = paySalaryService.calculateDailyPaySalary(paySalaryVo);
				else if (salaryType.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_MONTHLY))
					salaryVo = paySalaryService.calculatePaySalary(paySalaryVo);
				else if (salaryType.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_HOURLY))
					salaryVo = paySalaryService.calculateHourlyPaySalary(paySalaryVo);
				model.addAttribute("paySalary", salaryVo);
			} else {
				model.addAttribute("paySalary", paySalaryVo);
				model.addAttribute(MessageConstants.ERROR, "Employee , Date , Branch & Salary Type is mandatory.");
			}
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("salaryTypes", salaryTypeService.listAllSalaryTypes());

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_PAY_SALARY;
	}

	/**
	 * method to save pay salary hourly wages
	 * 
	 * @param model
	 * @param paySalary
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSALARY_SAVE_HOURLY_SALARY, method = RequestMethod.POST)
	public String saveHourlyPaySalary(final Model model, @ModelAttribute PaySalaryVo paySalary) {
		Long id = null;
		String msg = null;
		try {
			if (paySalary.getId() != null) {
				paySalaryService.updateHourlyPaySalary(paySalary);
				id = paySalary.getId();
				msg = MessageConstants.UPDATED_SUCCESS;
			} else {
				id = paySalaryService.savePaysalaryHourly(paySalary);
				msg = MessageConstants.SAVE_SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.PAYSALARY_EDIT_URL + "?id=" + id + "&tyPe="
					+ PaySlipConstants.SALARY_TYPE_HOURLY + "&" + MessageConstants.ERROR + "="
					+ MessageConstants.SAVE_FAILED;
		}
		return "redirect:" + RequestConstants.PAYSALARY_EDIT_URL + "?id=" + id + "&tyPe="
				+ PaySlipConstants.SALARY_TYPE_HOURLY + "&" + MessageConstants.SUCCESS + "=" + msg;
	}

	/**
	 * method to save pay salary daily
	 * 
	 * @param model
	 * @param paySalary
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSALARY_SAVE_DAILY_SALARY, method = RequestMethod.POST)
	public String saveDailyPaySalary(final Model model, @ModelAttribute PaySalaryVo paySalary) {
		Long id = null;
		String msg = null;
		try {
			if (paySalary.getId() != null) {
				paySalaryService.updateDailyPaySalary(paySalary);
				id = paySalary.getId();
				msg = MessageConstants.UPDATED_SUCCESS;
			} else {
				id = paySalaryService.savePaysalaryDaily(paySalary);
				msg = MessageConstants.SAVE_SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.PAYSALARY_EDIT_URL + "?id=" + id + "&tyPe="
					+ PaySlipConstants.SALARY_TYPE_DAILY + "&" + MessageConstants.ERROR + "="
					+ MessageConstants.SAVE_FAILED;
		}
		return "redirect:" + RequestConstants.PAYSALARY_EDIT_URL + "?id=" + id + "&tyPe="
				+ PaySlipConstants.SALARY_TYPE_DAILY + "&" + MessageConstants.SUCCESS + "=" + msg;
	}

	/**
	 * method to edit pay salary
	 * 
	 * @param model
	 * @param paySalaryVo
	 * @param paySalary
	 * @param rolesMap
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSALARY_EDIT_URL, method = RequestMethod.GET)
	public String editPaysalary(final Model model, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request, @RequestParam(value = "tyPe", required = true) String type,
			@ModelAttribute PaySalaryVo paySalary, @ModelAttribute PaySalaryVo paySalaryVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String erroMessage = request.getParameter(MessageConstants.ERROR);
		if (successMessage != null)
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
		else if (erroMessage != null)
			model.addAttribute(MessageConstants.ERROR, erroMessage);
		try {
			PaySalaryVo salaryVo = null;
			if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_DAILY))
				salaryVo = paySalaryService.findDailyPaySalary(id);
			else if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_HOURLY))
				salaryVo = paySalaryService.findHourlyPaySalary(id);
			else if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_MONTHLY))
				salaryVo = paySalaryService.findMonthlyPaySalary(id);
			model.addAttribute("paySalary", salaryVo);
			model.addAttribute("paySalaryVo", salaryVo);

		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_PAY_SALARY;
	}

	/**
	 * method to delete pay salary
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSALARY_DELETE_URL, method = RequestMethod.GET)
	public String deletePaysalry(final Model model, @RequestParam(value = "id", required = true) Long id) {
		try {
			paySalaryService.deletePaysalary(id);
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.PAYSALARY_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.PAYSALARY_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.DELETE_SUCCESS;
	}

	/**
	 * method to save monthly pay salary
	 * 
	 * @param model
	 * @param paySalary
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSALARY_SAVE_MONTHLY, method = RequestMethod.POST)
	public String saveMotlhyPaySalary(final Model model, @ModelAttribute PaySalaryVo paySalary) {
		String msg = null;
		Long id = null;
		try {
			if (paySalary.getId() != null) {
				paySalaryService.updateMonthlyPaySalary(paySalary);
				id = paySalary.getId();
				msg = MessageConstants.UPDATED_SUCCESS;
			} else {
				id = paySalaryService.savePaysalaryMonthly(paySalary);
				msg = MessageConstants.SAVE_SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.PAYSALARY_EDIT_URL + "?id=" + id + "&tyPe="
					+ PaySlipConstants.SALARY_TYPE_MONTHLY + "&" + MessageConstants.ERROR + "="
					+ MessageConstants.SAVE_FAILED;
		}
		return "redirect:" + RequestConstants.PAYSALARY_EDIT_URL + "?id=" + id + "&tyPe="
				+ PaySlipConstants.SALARY_TYPE_MONTHLY + "&" + MessageConstants.SUCCESS + "=" + msg;
	}

	/**
	 * method to send sms to group
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSALARY_SMS_GROUP, method = RequestMethod.GET)
	public @ResponseBody JsonResponse sendSmsGroup(@RequestParam(value = "id", required = true) Long id) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			paySalaryService.sendGroupSms(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to send sms to individual
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSALARY_SMS_INDIVIDUAL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse sendSmsIndividual(
			@RequestParam(value = "empCode", required = true) String empCode,
			@RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "type", required = true) String type) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			paySalaryService.sendIndividualSms(empCode, id, type);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	@RequestMapping(value = RequestConstants.VIEW_SALARY, method = RequestMethod.GET)
	public String viewSalary(final Model model, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request, @RequestParam(value = "tyPe", required = true) String type,
			@ModelAttribute PaySalaryVo paySalary, @ModelAttribute PaySalaryVo paySalaryVo,
			@RequestParam(value = "compId", required = true) Long companyId,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String erroMessage = request.getParameter(MessageConstants.ERROR);
		if (successMessage != null)
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
		else if (erroMessage != null)
			model.addAttribute(MessageConstants.ERROR, erroMessage);
		try {
			PaySalaryVo salaryVo = null;
			if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_DAILY))
				salaryVo = paySalaryService.findDailyPaySalary(id);
			else if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_HOURLY))
				salaryVo = paySalaryService.findHourlyPaySalary(id);
			else if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_MONTHLY))
				salaryVo = paySalaryService.findMonthlyPaySalary(id);
			model.addAttribute("paySalary", salaryVo);
			model.addAttribute("paySalaryVo", salaryVo);
			model.addAttribute("date", DateFormatter.convertDateToString(new Date()));
			model.addAttribute("accounts", ledgerService.findAccountsForSalary(companyId));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.VIEW_SALARY;
	}
}
