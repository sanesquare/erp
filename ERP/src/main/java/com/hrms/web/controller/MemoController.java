package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Subordinates;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.MemoService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.MemoDocumentVo;
import com.hrms.web.vo.MemoVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "memoVo" })
public class MemoController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private MemoService memoService;

	@Autowired
	private EmployeeService employeeService;

	/**
	 * method to list all memos
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEMO_PAGE_URL, method = RequestMethod.GET)
	public String listAllMemosWithGet(final Model model) {
		try {
			model.addAttribute("memoList", memoService.findAllMemo());
			model.addAttribute("memoVo", new MemoVo());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.MEMO_PAGE;
	}

	/**
	 * method to load memo information create page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEMO_INFORMATION_URL, method = RequestMethod.GET)
	public String saveOrUpdateMemoInformationPage(final Model model, @ModelAttribute("memoVo") MemoVo memoVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails, BindingResult result) {
		memoVo.setEmployee(authEmployeeDetails.getName());
		memoVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
		model.addAttribute("subordinateList",
				Subordinates.getSubordinates(authEmployeeDetails.getSuperiorSubordinateVo().getSubordinates()));
		if (memoVo.getMemoId() == null) {
			model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
		} else {
			model.addAttribute(MessageConstants.CREATED_ON, memoVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		}
		return PageNameConstatnts.MEMO_CREATE_PAGE;
	}

	/**
	 * method to save or update memo information
	 * 
	 * @param redirectAttributes
	 * @param memoVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEMO_INFORMATION_SAVE_URL, method = RequestMethod.POST)
	public ModelAndView saveOrUpdateMemoInformationWithPost(RedirectAttributes redirectAttributes,
			@ModelAttribute MemoVo memoVo) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.MEMO_INFORMATION_URL));
		try {
			memoService.saveOrUpdateMemo(memoVo);
			redirectAttributes.addFlashAttribute("memoVo", memoVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_MEMO_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to getting memo information for update
	 * 
	 * @param model
	 * @param memoId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEMO_INFORMATION_UPDATE_URL, method = RequestMethod.GET)
	public String updateMemoInformationGetPage(final Model model, @RequestParam(value = "otp_mid") Long memoId) {
		try {
			MemoVo memoVo = memoService.findMemoById(memoId);
			model.addAttribute("memoVo", memoVo);
			model.addAttribute(
					"subordinateList",
					Subordinates.getSubordinates(employeeService
							.getEmployeesDetails(
									employeeService.findAndReturnEmployeeByCode(memoVo.getEmployeeCode()).getId())
							.getSuperiorSubordinateVo().getSubordinates()));
			model.addAttribute(MessageConstants.CREATED_ON, memoVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.NO_MEMO_DETAILS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.MEMO_CREATE_PAGE;
	}

	/**
	 * method to view memo information
	 * 
	 * @param model
	 * @param memoId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEMO_INFORMATION_VIEW_URL, method = RequestMethod.GET)
	public String viewMemoInformation(final Model model, @RequestParam(value = "otp_mid") Long memoId) {
		try {
			MemoVo memoVo = memoService.findMemoById(memoId);
			model.addAttribute("memoVo", memoVo);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.NO_MEMO_DETAILS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.MEMO_VIEW_PAGE;
	}

	/**
	 * method to list all memos
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEMO_INFORMATION_LIST_URL, method = RequestMethod.GET)
	public String listAllMemos(final Model model) {
		try {
			model.addAttribute("memoList", memoService.findAllMemo());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.MEMO_LIST_S_PAGE;
	}

	/**
	 * method to delete memo
	 * 
	 * @param memoId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEMO_INFORMATION_DELETE_URL, method = RequestMethod.POST)
	public String deleteWarningWothPost(@RequestParam(value = "otp_mid") Long memoId, final Model model) {
		try {
			memoService.deleteMemo(memoId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.NO_MEMO_DETAILS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return "redirect:" + RequestConstants.MEMO_INFORMATION_LIST_URL;
	}

	/**
	 * method to list all documents of a memo
	 * 
	 * @param model
	 * @param memoId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEMO_DOCUEMENT_LIST_URL, method = RequestMethod.POST)
	public String listAllMemoDocuments(final Model model, @RequestParam(value = "memoId") Long memoId) {
		try {
			model.addAttribute("memoDocList", memoService.findMemoById(memoId));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.MEMO_DOC_LIST_PAGE;
	}

	/**
	 * method to upload memo documents
	 * 
	 * @param memoId
	 * @param memoDocuments
	 * @param memoVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEMO_DOCUEMENT_UPLOAD_URL, method = RequestMethod.POST)
	public ModelAndView uploadMemoDocuments(@RequestParam(value = "memoId", required = true) Long memoId,
			MultipartHttpServletRequest request, @ModelAttribute MemoVo memoVo) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.MEMO_INFORMATION_URL));
		try {
			List<MemoDocumentVo> documentVos = uploadMemoDocuments(request);
			if (!documentVos.isEmpty()) {
				memoVo.setMemoId(memoId);
				memoVo.setDocuments(documentVos);
				memoService.saveMemoDocuments(memoVo);
			}
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return view;
	}

	/**
	 * method to upload files to SFTP
	 * 
	 * @param multipartFiles
	 * @return
	 */
	private List<MemoDocumentVo> uploadMemoDocuments(MultipartHttpServletRequest request) {
		List<MemoDocumentVo> documentsVos = new ArrayList<MemoDocumentVo>();
		try {
			Iterator<String> itrator = request.getFileNames();
			while (itrator.hasNext()) {
				MultipartFile multipartFile = request.getFile(itrator.next());
				if (!multipartFile.isEmpty()) {
					String fileAccesUrl = FileUpload.uploadDocument(MessageConstants.MEMO_MODULE, multipartFile);
					if (fileAccesUrl != null) {
						MemoDocumentVo documentVo = new MemoDocumentVo();
						documentVo.setDocumentUrl(fileAccesUrl);
						documentVo.setDocumentName(multipartFile.getOriginalFilename());
						documentsVos.add(documentVo);
					}
				}

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return documentsVos;
	}

	/**
	 * method to delete memo document
	 * 
	 * @param documentId
	 * @param documentUrl
	 * @return
	 */
	@RequestMapping(value = RequestConstants.MEMO_DOCUEMENT_DELETE_URL, method = RequestMethod.POST)
	public String deleteMemoDocument(@RequestParam(value = "documentId", required = true) Long documentId,
			@RequestParam(value = "documentUrl", required = true) String documentUrl) {
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			if (sftpOperation.removeFileFromSFTP(documentUrl))
				memoService.deleteMemoDocument(documentId);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.MEMO_DOC_LIST_PAGE;
	}
}
