package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.Project;
import com.hrms.web.entities.WorkSheet;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ProjectService;
import com.hrms.web.service.WorkSheetService;
import com.hrms.web.service.WorkSheetTaskService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.InterviewDocumentsVo;
import com.hrms.web.vo.JobInterviewVo;
import com.hrms.web.vo.OvertimeVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.WorkSheetDocumentVo;
import com.hrms.web.vo.WorkSheetVo;

/**
 * 
 * @author Vinutha
 * @since 1-April-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class WorkSheetController {

	@Autowired
	private WorkSheetService sheetService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private WorkSheetTaskService taskService;
	/**
	 * method to handle GET request for work sheets home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_PAGE_URL, method = RequestMethod.GET)
	public String workSheetPageRequestHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		String msg = request.getParameter(MessageConstants.SUCCESS);
		String errMsg = request.getParameter(MessageConstants.ERROR);
		if (msg != null) {
			model.addAttribute(MessageConstants.SUCCESS, msg);
		} else if (errMsg != null) {
			model.addAttribute(MessageConstants.ERROR, errMsg);
		}
		try {
			model.addAttribute("worksheets", sheetService.listEmployeeWorkSheets(authEmployeeDetails.getEmployeeCode()));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.WORKSHEETS_PAGE;
	}

	/**
	 * method to handle GET request for add work sheets page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_ADD_PAGE_URL, method = RequestMethod.GET)
	public String workSheetAddPageRequestHandler(final Model model, @ModelAttribute WorkSheetVo workSheetVo,HttpServletRequest request,
			@ModelAttribute("authEmployee") Employee authEmployee, BindingResult result,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		String msg = request.getParameter(MessageConstants.SUCCESS);
		String errMsg = request.getParameter(MessageConstants.ERROR);
		if (msg != null) {
			model.addAttribute(MessageConstants.SUCCESS, msg);
		} else if (errMsg != null) {
			model.addAttribute(MessageConstants.ERROR, errMsg);
		}
		try {
			workSheetVo.setEmployee(authEmployeeDetails.getName()+" ("+authEmployeeDetails.getEmployeeCode()+")");
			workSheetVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("employees", employeeService.listAllEmployee());

		} catch (Exception e) {

		}
		return PageNameConstatnts.ADD_WORKSHEETS_PAGE;
	}

	/**
	 * method to handle GET request for Edit page
	 * 
	 * @param model
	 * @param workSheetVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_EDIT_PAGE_URL, method = RequestMethod.GET)
	public String workSheetEditPageRequestHandler(final Model model, @ModelAttribute WorkSheetVo workSheetVo,
			HttpServletRequest request, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("workSheetVo",
					sheetService.findWorksheetVoById(Long.parseLong(request.getParameter("wid"))));
			model.addAttribute("employees", employeeService.listAllEmployee());

		} catch (Exception e) {

		}
		return PageNameConstatnts.ADD_WORKSHEETS_PAGE;
	}
	/**
	 * Method to handle request for deleting work sheet
	 * @param model
	 * @param workSheetVo
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.WORKSHEETS_DELETE_PAGE_URL,method=RequestMethod.GET)
	public String workSheetDeleteRequestHandler(final Model model , @ModelAttribute WorkSheetVo workSheetVo, HttpServletRequest request)
	{
		try
		{
			sheetService.deleteWorkSheetById(Long.parseLong(request.getParameter("wid")));			
			model.addAttribute("worksheets",sheetService.listAllWorksheets());
			model.addAttribute(MessageConstants.SUCCESS,MessageConstants.WORKSHEET_DOC_DEL_SUCCESS);
		}catch(Exception e)
		{
			model.addAttribute(MessageConstants.ERROR,MessageConstants.WORKSHEET_DOC_DEL_FAILED);
		}
		return "redirect:" + RequestConstants.WORKSHEETS_PAGE_URL;
	}

	/**
	 * method to handle GET request for view page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_VIEW_PAGE_URL, method = RequestMethod.GET)
	public String workSheetViewPageRequestHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {

			model.addAttribute("workSheet",
					sheetService.findWorksheetVoById(Long.parseLong(request.getParameter("wid"))));
		} catch (Exception e) {
			e.printStackTrace();

		}
		return PageNameConstatnts.VIEW_WORKSHEETS_PAGE;
	}

	/**
	 * Method to handle request for saving work sheet basic info
	 * 
	 * @param workSheetVo
	 * @param session
	 * @return
	 */
	/*
	 * @RequestMapping(value = RequestConstants.WORKSHEETS_INFORMATION_SAVE,
	 * method = RequestMethod.POST, headers={"Content-type=application/json"})
	 * public @ResponseBody JsonResponse saveWorkSheetBasicInfo(@RequestBody
	 * WorkSheetVo workSheetVo,HttpSession session){ JsonResponse
	 * jsonResponse=new JsonResponse(); try{
	 * if(workSheetVo.getWorkSheetId()==null) { Long id=
	 * sheetService.saveBasicInfo(workSheetVo); jsonResponse.setResult(id);
	 * jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_SUCCESS); }else{
	 * sheetService.updateBasicInfo(workSheetVo);
	 * jsonResponse.setStatus(MessageConstants.WORKSHEET_UPDATE_SUCCESS); }
	 * }catch(Exception e){ e.printStackTrace();
	 * jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_FAILED); } return
	 * jsonResponse; }
	 */
	/**
	 * Method to handle request for saving work sheet basic info
	 * 
	 * @param workSheetVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_INFORMATION_SAVE, method = RequestMethod.POST)
	public String saveWorkSheetBasicInfo(@ModelAttribute WorkSheetVo workSheetVo, HttpSession session, final Model model) {
		Long id = 0L;
		try {

			if (workSheetVo.getWorkSheetId() == null) {
				id = sheetService.saveBasicInfo(workSheetVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.WORKSHEET_SAVE_SUCCESS);
				return "redirect:" + RequestConstants.WORKSHEETS_EDIT_PAGE_URL + "?wid=" + id;

			} else{
				sheetService.updateBasicInfo(workSheetVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.WORKSHEET_UPDATE_SUCCESS);
				return "redirect:" + RequestConstants.WORKSHEETS_EDIT_PAGE_URL + "?wid=" + workSheetVo.getWorkSheetId();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			return "redirect:" + RequestConstants.WORKSHEETS_PAGE_URL;
		}

	}

	/**
	 * Method to save project and task
	 * 
	 * @param workSheetVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_PROJECT_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveWorkSheetProjectInfo(@RequestBody WorkSheetVo workSheetVo, HttpSession session) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (workSheetVo.getWorkSheetId() != null && workSheetVo.getTaskId() == null) {

				sheetService.saveProjectTask(workSheetVo);
				jsonResponse.setResult(sheetService.findWorksheetVoById(workSheetVo.getWorkSheetId()));
				jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_SUCCESS);
			} else if (workSheetVo.getWorkSheetId() != null && workSheetVo.getTaskId() != null) {
				sheetService.updateProjectTask(workSheetVo);
				jsonResponse.setResult(sheetService.findWorksheetVoById(workSheetVo.getWorkSheetId()));
				jsonResponse.setStatus(MessageConstants.WORKSHEET_UPDATE_SUCCESS);
			} else {
				jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_FAILED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_FAILED);
		}
		return jsonResponse;
	}
	/**
	 * Method to delete work sheet task
	 * @param model
	 * @param workSheetVo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEET_DELETE_TASK, method = RequestMethod.GET)
	public String workSheetTaskDeleteRequestHandler(final Model model, @ModelAttribute WorkSheetVo workSheetVo,
			HttpServletRequest request) {
		try {
			taskService.deleteTaskById(Long.parseLong(request.getParameter("tid")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.WORKSHEETS_EDIT_PAGE_URL + "?wid=" + Long.parseLong(request.getParameter("wid"));
	}

	/**
	 * Method to handle request for finding task by id
	 * 
	 * @param sheetId
	 * @param taskId
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEET_GET_TASK, method = RequestMethod.GET )
	public @ResponseBody JsonResponse getWorkSheetTask(@RequestParam(value = "sheetId") Long sheetId , @RequestParam(value = "taskId") Long taskId,HttpSession session){
		JsonResponse response=new JsonResponse();
		try{
			if(sheetId!=null)
			{
				response.setResult(taskService.findTaskById(taskId));
		}else{
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return response;
	}
	/**
	 * Method to save work sheet description
	 * 
	 * @param workSheetVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_DESCRIPTION_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveWorkSheetDesc(@RequestBody WorkSheetVo workSheetVo, HttpSession session) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (workSheetVo.getWorkSheetId() != null) {
				sheetService.saveDesc(workSheetVo);
				jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_SUCCESS);
			} else {
				jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_FAILED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_FAILED);
		}
		return jsonResponse;
	}

	/**
	 * method to save work sheet add info
	 * 
	 * @param workSheetVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_ADDINFO_SAVE, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JsonResponse saveWorkSheetAddInfo(@RequestBody WorkSheetVo workSheetVo, HttpSession session) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			if (workSheetVo.getWorkSheetId() != null) {
				sheetService.saveAddInfo(workSheetVo);
				jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_SUCCESS);
			} else {
				jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_FAILED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonResponse.setStatus(MessageConstants.WORKSHEET_SAVE_FAILED);
		}
		return jsonResponse;
	}

	/**
	 * method to handle request for document upload
	 * 
	 * @param model
	 * @param workSheetVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_UPLOAD_DOCS, method = RequestMethod.POST)
	public String uploadDocs(final Model model, @ModelAttribute WorkSheetVo workSheetVo) {
		try {
			if (workSheetVo.getWorkSheetId() != null) {

				Set<WorkSheetDocumentVo> documentVos = uploadDocuments(workSheetVo.getUploadSheetDocs());
				if (documentVos.size() != 0) {
					workSheetVo.setDocumentsVo(documentVos);
					sheetService.updateDocuments(workSheetVo);
					model.addAttribute(MessageConstants.SUCCESS, MessageConstants.WORKSHEET_DOC_SAVE_SUCCESS);
				} else {
					model.addAttribute(MessageConstants.ERROR, MessageConstants.WORKSHEET_DOC_SAVE_FAILED);
				}
			} else {
				model.addAttribute(MessageConstants.ERROR, MessageConstants.WORKSHEET_DOC_SAVE_FAILED);
			}
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WORKSHEET_DOC_SAVE_FAILED);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.WORKSHEETS_PAGE_URL;
	}

	/**
	 * method to upload documents
	 * 
	 * @param files
	 * @return
	 */
	private Set<WorkSheetDocumentVo> uploadDocuments(List<MultipartFile> files) {
		Set<WorkSheetDocumentVo> documentsVos = new HashSet<WorkSheetDocumentVo>();
		try {
			for (MultipartFile file : files) {
				if (!file.isEmpty()) {
					String fileAccessUrl = FileUpload.uploadDocument(MessageConstants.WORKSHEET_MODULE, file);
					if (fileAccessUrl != null) {
						WorkSheetDocumentVo documentsVo = new WorkSheetDocumentVo();
						documentsVo.setDocumentName(file.getOriginalFilename());
						documentsVo.setDocumentUrl(fileAccessUrl);
						documentsVos.add(documentsVo);
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return documentsVos;
	}

	/**
	 * method to handl erequest for document delete
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_DELETE_DOCS, method = RequestMethod.GET)
	public String deleteWorkSheetDocument(final Model model, HttpServletRequest request) {
		try {
			String url = request.getParameter("durl");
			SFTPOperation operation = new SFTPOperation();
			Boolean result = operation.removeFileFromSFTP(url);
			if (result) {
				sheetService.deleteDocument(url);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.WORKSHEET_DOC_DEL_SUCCESS);
			} else {
				model.addAttribute(MessageConstants.ERROR, MessageConstants.WORKSHEET_DOC_DEL_FAILED);
			}
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WORKSHEET_DOC_DEL_FAILED);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.WORKSHEETS_PAGE_URL;
	}

	/**
	 * method to handle request for document download
	 * 
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = RequestConstants.WORKSHEETS_VIEW_DOCS, method = RequestMethod.GET)
	public void downloadDocument(final Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(request.getParameter("durl"), response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
