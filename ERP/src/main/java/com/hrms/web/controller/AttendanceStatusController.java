package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Employee;
import com.hrms.web.service.AttendanceService;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.LeaveService;
import com.hrms.web.service.LeaveTypeService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AttendanceEmployeeVo;
import com.hrms.web.vo.AttendanceStatusVo;
import com.hrms.web.vo.AttendanceVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.LeavesVo;
import com.hrms.web.vo.SessionRolesVo;
import com.sun.org.apache.bcel.internal.generic.NEW;

/**
 * 
 * @author Vinutha
 * @since 27-May-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails" ,"roles" })
public class AttendanceStatusController {
	
	@Autowired
	private AttendanceService attendanceService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private BranchService branchService;
	
	@Autowired
	private LeaveTypeService leaveTypeService;
	
	@Autowired
	private LeaveService leavesService;
	
	@Autowired
	private DepartmentService depService;

	/**
	 * Method to handle request for attendance status add page
	 * @param model
	 * @param attendanceVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @param rolesMap
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_ATTENDANCE_STATUS_URL, method = RequestMethod.GET)
	public String addAttendanceStausGet(final Model model, @ModelAttribute AttendanceStatusVo attendanceStatusVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("showFilter", true);
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", depService.listAllDepartments());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_ATTENDANCE_STATUS_PAGE;
	}
	
	@RequestMapping(value = RequestConstants.ADD_ATTENDANCE_STATUS_URL, method = RequestMethod.POST)
	public String addAttendanceStausPost(final Model model, @ModelAttribute AttendanceStatusVo attendanceStatusVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("employees", employeeService.findEmployeesForAttendanceStatus(
					attendanceStatusVo.getBranchId(), attendanceStatusVo.getDepartmentIds()));
			attendanceStatusVo.setDate(DateFormatter.convertDateToString(new Date()));
			model.addAttribute("attendanceStatusVo", attendanceStatusVo);
			model.addAttribute("showFilter", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_ATTENDANCE_STATUS_PAGE;
	}
	
	/**
	 * Method to save attendance status
	 * @param model
	 * @param attendanceVo
	 * @param rolesMap
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_ATTENDANCE_STATUS, method = RequestMethod.POST)
	public String saveAttendanceStatus(final Model model, @ModelAttribute AttendanceStatusVo attendanceVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		AttendanceVo attenVo = new AttendanceVo();
		try {
			for (AttendanceEmployeeVo employeeVo : attendanceVo.getAttendanceEmployeeVos()) {
				if (employeeVo.getStatus().trim().equalsIgnoreCase(CommonConstants.PRESENT)) {
					attenVo.setEmployeeCode(employeeVo.getEmployeeCode());
					attenVo.setDate(attendanceVo.getDate());
					attenVo.setSignInTime("9:00 AM");
					attenVo.setSignOutTime("5:00 PM");
					attenVo.setStatus(CommonConstants.PRESENT);
					attenVo.setNotes(attendanceVo.getNotes());
					attendanceService.saveAttendance(attenVo);
				} else if (employeeVo.getStatus().trim().equalsIgnoreCase(CommonConstants.ABSENT)) {
					attenVo.setEmployeeCode(employeeVo.getEmployeeCode());
					attenVo.setDate(attendanceVo.getDate());
					attenVo.setSignInTime("9:00 AM");
					attenVo.setSignOutTime("9:00 AM");
					attenVo.setStatus(CommonConstants.ABSENT);
					attenVo.setNotes(attendanceVo.getNotes());
					attendanceService.saveAttendance(attenVo);
				} else if (employeeVo.getStatus().trim().equalsIgnoreCase(CommonConstants.HALF_DAY)) {
					attenVo.setEmployeeCode(employeeVo.getEmployeeCode());
					attenVo.setDate(attendanceVo.getDate());
					attenVo.setSignInTime("9:00 AM");
					attenVo.setSignOutTime("12:01 PM");
					attenVo.setStatus(CommonConstants.HALF_DAY);
					attenVo.setNotes(attendanceVo.getNotes());
					attendanceService.saveAttendance(attenVo);
				} else {
					LeavesVo leaveVo = new LeavesVo();
					leaveVo.setLeaveTypeAjaxId(Long.parseLong(employeeVo.getStatus()));
					leaveVo.setEmployeeCode(employeeVo.getEmployeeCode());
					leaveVo.setFromDate(attendanceVo.getDate());
					leaveVo.setToDate(attendanceVo.getDate());
					leaveVo.setLeaveTypeId(Long.parseLong(employeeVo.getStatus()));
					leaveVo.setNotes(attendanceVo.getNotes());
					List<Long> ids = new ArrayList<Long>();
					ids.add(authEmployeeDetails.getEmployeeId());
					leaveVo.setSuperiorAjaxIds(ids);
					leaveVo.setLeaveId(leavesService.saveLeave(leaveVo));
				}
			}
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.ATTENDANCE_SAVE_SUCCESS);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.ATTENDANCE_SAVE_FAILED);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.ADD_ATTENDANCE_STATUS_URL;
	}
}
