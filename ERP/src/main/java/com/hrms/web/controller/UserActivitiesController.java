package com.hrms.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.service.UserActivitiesService;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Sangeeth
 * 
 * 15/3/2015
 *
 */

@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class UserActivitiesController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;
	
	@Autowired
	private UserActivitiesService userActivitiesService;
	
	/**
	 * method to handle user activities page get request
	 * @param model
	 * @return PageNameConstatnts.PROJECTS_PAGE
	 */
	@RequestMapping(value=RequestConstants.USER_ACTIVITIES_URL,method=RequestMethod.GET)
	public String userActivitiesPageGetRequestHandler(final Model model,HttpServletRequest request, 
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> UserActivities Controller... userActivitiesPageGetRequestHandler");
		
		try{
			model.addAttribute("activities", userActivitiesService.listDistinctDatesByMonth());
		}catch(Exception e){
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.USER_ACTIVITIES_PAGE;
	}
	
	/**
	 * method to handle user activities page get request
	 * @param model
	 * @return PageNameConstatnts.PROJECTS_PAGE
	 */
	@RequestMapping(value=RequestConstants.LIST_USER_ACTIVITIES_URL,method=RequestMethod.GET)
	public String listUserActivitiesPageGetRequestHandler(final Model model,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> UserActivities Controller... listUserActivitiesPageGetRequestHandler");
		try{
			int month=Integer.parseInt(request.getParameter("month"));
			int year=Integer.parseInt(request.getParameter("year"));
			model.addAttribute("activities", userActivitiesService.listAllUserActivities(month,year));
		}catch(Exception e){
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.LIST_USER_ACTIVITIES_PAGE;
	}
	
}
