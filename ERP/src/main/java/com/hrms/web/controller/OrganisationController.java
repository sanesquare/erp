package com.hrms.web.controller;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.OrganisationService;
import com.hrms.web.vo.OrganisationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-March-2015
 *
 */
@Controller
public class OrganisationController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private OrganisationService organisationService;

	/**
	 * method to save new organisation
	 * 
	 * @param model
	 * @param organisationVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_ORGANISATION_URL, method = RequestMethod.POST)
	public ModelAndView saveOrganisationWithPost(final Model model, @ModelAttribute OrganisationVo organisationVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			organisationService.saveOrganisation(organisationVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.ORGANISATION_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}
}
