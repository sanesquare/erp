package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dao.ProjectStatusDao;
import com.hrms.web.dto.Superiors;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ForwardApplicationStatusService;
import com.hrms.web.service.ProjectService;
import com.hrms.web.service.TravelArrangementTypeService;
import com.hrms.web.service.TravelModeService;
import com.hrms.web.service.TravelService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SFTPFilePropertiesVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.TravelDestinationVo;
import com.hrms.web.vo.TravelDocumentVo;
import com.hrms.web.vo.TravelVo;

/**
 * 
 * @author sangeeth , Shamsheer
 * @since 23-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class TravelController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private TravelService travelService;

	@Autowired
	private TravelModeService travelModeService;

	@Autowired
	private TravelArrangementTypeService travelArrangementTypeService;

	@Autowired
	private ForwardApplicationStatusService statusService;

	/**
	 * method to reach travel home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRAVEL_URL, method = RequestMethod.GET)
	public String travelPageGetRequestHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		logger.info("inside>> TravelController... travelPageGetRequestHandler");
		try {
			model.addAttribute("rolesMap", rolesMap);
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			model.addAttribute("travels", travelService.findAllTravelsByEmployee(authEmployeeDetails.getEmployeeCode()));
			model.addAttribute("thisCode", authEmployeeDetails.getEmployeeCode());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.TRAVEL_PAGE;
	}

	/**
	 * method to handle addTravel page get request
	 * 
	 * @param model
	 * @return PageNameConstatnts.ADD_TRAVEL_PAGE
	 */
	@RequestMapping(value = RequestConstants.ADD_TRAVEL_URL, method = RequestMethod.GET)
	public String addTravelPageGetRequestHandler(final Model model, @ModelAttribute TravelVo travelVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			List<TravelDestinationVo> destinationVos = new ArrayList<TravelDestinationVo>();
			for (int i = 0; i < 5; i++) {
				TravelDestinationVo destinationVo = new TravelDestinationVo();
				destinationVos.add(destinationVo);
			}
			travelVo.setDestinationVos(destinationVos);
			travelVo.setEmployee(authEmployeeDetails.getName()+" ("+authEmployeeDetails.getEmployeeCode()+")");
			travelVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("travelModes", travelModeService.listAllTravelModes());
			model.addAttribute("arrangementTypes", travelArrangementTypeService.listAllArrangementType());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_TRAVEL_PAGE;
	}

	/**
	 * method to save or update travel
	 * 
	 * @param model
	 * @param travelVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRAVEL_SAVE, method = RequestMethod.POST)
	public String saveTravel(final Model model, @ModelAttribute TravelVo travelVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		try {
			travelVo.setEmployee(authEmployeeDetails.getName());
			travelVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			if (travelVo.getTravelId() == null) {
				travelVo.setTravelId(travelService.saveTravel(travelVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.TRAVEL_SAVE_SUCCESS);
			} else {
				travelService.updateTravel(travelVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.TRAVEL_UPDATE_SUCCESS);
			}
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("travelModes", travelModeService.listAllTravelModes());
			model.addAttribute("arrangementTypes", travelArrangementTypeService.listAllArrangementType());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return "redirect:" + RequestConstants.TRAVEL_EDIT + "?id=" + travelVo.getTravelId();

	}

	/**
	 * method to edit travel
	 * 
	 * @param model
	 * @param travelVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @param travelId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRAVEL_EDIT, method = RequestMethod.GET)
	public String editTravel(final Model model, @ModelAttribute TravelVo travelVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@RequestParam(value = "id", required = true) Long travelId, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			travelVo = travelService.getTravelById(travelId);
			List<TravelDestinationVo> destinationVos = travelVo.getDestinationVos();
			for (int i = destinationVos.size(); i < 5; i++) {
				TravelDestinationVo destinationVo = new TravelDestinationVo();
				destinationVos.add(destinationVo);
			}
			travelVo.setDestinationVos(destinationVos);
			/*
			 * travelVo.setEmployee(authEmployeeDetails.getName());
			 * travelVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			 */
			model.addAttribute("travelVo", travelVo);
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("travelModes", travelModeService.listAllTravelModes());
			model.addAttribute("arrangementTypes", travelArrangementTypeService.listAllArrangementType());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_TRAVEL_PAGE;
	}

	/**
	 * method to delete travel
	 * 
	 * @param model
	 * @param travelId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRAVEL_DELETE, method = RequestMethod.GET)
	public String deeletTravel(Model model, @RequestParam(value = "id", required = true) Long travelId) {
		try {
			travelService.deleteTravel(travelId);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.TRAVEL_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			return "redirect:" + RequestConstants.TRAVEL_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.TRAVEL_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.TRAVEL_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.TRAVEL_DELETE_SUCCESS;
	}

	/**
	 * method to upload document
	 * 
	 * @param model
	 * @param travelVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRAVEL_DOCUMENT_UPLOAD, method = RequestMethod.POST)
	public String uploadDocument(final Model model, @ModelAttribute TravelVo travelVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		try {
			if (travelVo.getTravelId() != null) {
				travelVo.setDocumentVos(uploadDocuments(travelVo.getTravelDocuments()));
				travelService.updateDocuments(travelVo);
			}
		} catch (ItemNotFoundException e) {

		} catch (Exception e) {

		}
		return "redirect:" + RequestConstants.TRAVEL_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.TRAVEL_DOCUMENTS_SUCCESS;
	}

	/**
	 * method to upload travel documents to server
	 * 
	 * @param files
	 * @return
	 */
	private List<TravelDocumentVo> uploadDocuments(List<MultipartFile> files) {
		List<TravelDocumentVo> documentVos = new ArrayList<TravelDocumentVo>();
		try {
			for (MultipartFile file : files) {
				if (!file.isEmpty()) {
					String url = FileUpload.uploadDocument(MessageConstants.TRAVEL_MODULE, file);
					if (url != null) {
						TravelDocumentVo vo = new TravelDocumentVo();
						vo.setUrl(url);
						vo.setFileName(file.getOriginalFilename());
						documentVos.add(vo);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return documentVos;
	}

	/**
	 * method to download document
	 * 
	 * @param request
	 * @param response
	 * @param url
	 */
	@RequestMapping(value = RequestConstants.TRAVEL_DOCUMENT_DOWNLOAD, method = RequestMethod.GET)
	public void downloadDocument(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "url", required = true) String url) {
		try {
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(url, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * method to delete document
	 * 
	 * @param url
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRAVEL_DOCUMENT_DELETE, method = RequestMethod.GET)
	public String deleteDocument(@RequestParam(value = "url", required = true) String url) {
		try {
			SFTPOperation operation = new SFTPOperation();
			Boolean result = operation.removeFileFromSFTP(url);
			if (result)
				travelService.deleteDocument(url);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.TRAVEL_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			return "redirect:" + RequestConstants.TRAVEL_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.TRAVEL_DOCUMENTS_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.TRAVEL_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.TRAVEL_DOCUMENTS_DELETE_SUCCESS;
	}

	/**
	 * method to view travel
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRAVEL_VIEW, method = RequestMethod.GET)
	public String viewTravel(final Model model, @ModelAttribute TravelVo travelVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@RequestParam(value = "id", required = true) Long travelId, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			travelVo.setEmployee(authEmployeeDetails.getName());
			travelVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			travelVo =  travelService.getTravelById(travelId);
			if(travelVo.getSuperiorCode().equalsIgnoreCase(authEmployeeDetails.getEmployeeCode()))
				travelVo.setViewStatus(true);
			else
				travelVo.setViewStatus(false);
			model.addAttribute("travelVo",travelVo);
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("status", statusService.listAllStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.TRAVEL_VIEW_PAGE;
	}

	/**
	 * method to update status
	 * 
	 * @param model
	 * @param travelVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRAVEL_UPDATE_STATUS, method = RequestMethod.POST)
	public String updateTravelStatus(final Model model, @ModelAttribute TravelVo travelVo) {
		try {
			if (travelVo.getTravelId() != null)
				travelService.updateStatus(travelVo);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.TRAVEL_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			return "redirect:" + RequestConstants.TRAVEL_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.TRAVEL_STATUS_FAILED;
		}
		return "redirect:" + RequestConstants.TRAVEL_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.TRAVEL_STATUS_SUCCESS;
	}
}