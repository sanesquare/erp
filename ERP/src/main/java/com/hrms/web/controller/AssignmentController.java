package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.mail.event.MessageChangedEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.AssignmentService;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.AssignmentDocumentsVo;
import com.hrms.web.vo.AssignmentVo;
import com.hrms.web.vo.ContractVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 26-March-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails" , "roles"})
public class AssignmentController {

	@Autowired
	private AssignmentService assignmentService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Log
	private Logger logger;

	/**
	 * method to reach assignments page
	 * @param model
	 * @return
	 */
	@RequestMapping(value= RequestConstants.ASSIGNMENTS_URL , method = RequestMethod.GET)
	public String getAssignmentPage(final Model model , HttpServletRequest request,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		try{
			model.addAttribute("rolesMap", rolesMap);
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if(successMessage!=null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if(errorMessage!=null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("assignments", assignmentService.listAssignmentByEmployee(authEmployeeDetails.getEmployeeId()));
			logger.info("------------- "+authEmployeeDetails.getEmployeeId());
		}catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.ASSIGNMENTS_PAGE;
	}

	/**
	 * method to reach add assignmetns page
	 * @param model
	 * @param assignmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_ASSIGNMENT_URL , method=RequestMethod.GET)
	public String addAssignmentGetHandler(final Model model, @ModelAttribute AssignmentVo assignmentVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try{
			model.addAttribute("branch", branchService.listAllBranches());
			model.addAttribute("department", departmentService.listAllDepartments());
		}catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_ASSIGNMENTS_PAGE;
	}

	/**
	 * method to save new assignment
	 * @param model
	 * @param assignmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_ASSIGNMENT , method = RequestMethod.POST,
			headers = {"Content-type=application/json"})
	public @ResponseBody JsonResponse saveAssignment(final Model model, @RequestBody AssignmentVo assignmentVo){
		JsonResponse jsonResponse = new JsonResponse();
		try{
			if(assignmentVo.getAssignmentId()==null){
				jsonResponse.setResult(assignmentService.saveAssignment(assignmentVo));
			}else{
				assignmentService.updateAssignment(assignmentVo);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonResponse;

	}

	/**
	 * method to upload document
	 * @param model
	 * @return
	 */
	@RequestMapping(value=RequestConstants.UPLOAD_ASSIGNMENT_DOC , method =RequestMethod.POST)
	public String uploadDocument(final Model model, @ModelAttribute AssignmentVo assignmentVo){
		try{
			if(assignmentVo.getAssignmentId()!=null){
				assignmentVo.setDocumentsVos(setDocumentsVo(assignmentVo.getUpload_assignmnt_file()));
				assignmentService.updateDocument(assignmentVo);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.ASSIGNMENTS_URL;
	}

	/**
	 * method to uplad documents
	 * @param files
	 * @return
	 */
	private List<AssignmentDocumentsVo> setDocumentsVo(List<MultipartFile> files){
		List<AssignmentDocumentsVo> vos  = new ArrayList<AssignmentDocumentsVo>();
		try{
			for(MultipartFile file : files){
				if(!file.isEmpty()){
					String url=FileUpload.uploadDocument(MessageConstants.ASSIGNMENT_MODULE, file);
					if(url!=null){
						AssignmentDocumentsVo vo = new AssignmentDocumentsVo();
						vo.setUrl(url);
						vo.setFileName(file.getOriginalFilename());
						vos.add(vo);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return vos;
	}

	/**
	 * method to edit assignment
	 * @param model
	 * @param assignmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_ASSIGNMENT , method = RequestMethod.GET)
	public String editAssignment(final Model model , @ModelAttribute AssignmentVo assignmentVo,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try{
			assignmentVo=assignmentService.
					findAssignmentById(Long.parseLong(request.getParameter("id")));
			assignmentVo.setEdit("yes");
			model.addAttribute("assignmentVo", assignmentVo);
		}catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_ASSIGNMENTS_PAGE;
	}

	/**
	 * method to delete assignment
	 * @param model
	 * @param assignmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_ASSIGNMENT , method = RequestMethod.GET)
	public String deleteAssignment(final Model model , @ModelAttribute AssignmentVo assignmentVo,HttpServletRequest request){
		try{
			assignmentService.deleteAssignment(Long.parseLong(request.getParameter("id")));
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.ASSIGNMENT_DELETE_SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.ASSIGNMENT_DELETE_FAILED);
		}
		return "redirect:"+RequestConstants.ASSIGNMENTS_URL;
	}

	/**
	 * method to download document
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = RequestConstants.DOWNLOAD_ASSIGNMENT_DOC , method = RequestMethod.GET)
	public void downloadDocument(HttpServletRequest request, HttpServletResponse response){
		try{
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(request.getParameter("url"), response);
		}catch(Exception  e){
			e.printStackTrace();
		}
	}

	/**
	 * method to delete doc
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_ASSIGNMENT_DOC , method = RequestMethod.GET)
	public String deleteDocument(HttpServletRequest request , final Model model){
		try{
			SFTPOperation  operation  = new SFTPOperation();
			Boolean result=operation.removeFileFromSFTP(request.getParameter("url"));
			if(result){
				assignmentService.deleteDocument(request.getParameter("url"));
			}
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.DOCUMENT_DELETED_SUCCESSFULLY);
		}catch(Exception e){
			model.addAttribute(MessageConstants.ERROR, MessageConstants.DOCUMENT_DELETED_FAILED);
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.ASSIGNMENTS_URL;
	}

	/**
	 * method to view assignment
	 * @param model
	 * @return
	 */
	@RequestMapping(value=RequestConstants.VIEW_ASSIGNMENT , method = RequestMethod.GET)
	public String viewAssignment(final Model model,HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		try{
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("assignmentVo", assignmentService.findAssignmentById(Long.parseLong(request.getParameter("id"))));
		}catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.VIEW_ASSIGNMENTS_PAGE;
	}
}


