package com.hrms.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.MstReference;
import com.hrms.web.entities.Notification;
import com.hrms.web.entities.NotificationType;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.AbstractService;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.CommonService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.NotificationService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FormValueChecker;
import com.hrms.web.vo.AccountTypeVo;
import com.hrms.web.vo.AdvanceSalaryStatusVo;
import com.hrms.web.vo.BaseCurrencyVo;
import com.hrms.web.vo.BloodGroupVo;
import com.hrms.web.vo.BranchTypeVo;
import com.hrms.web.vo.ContractTypeVo;
import com.hrms.web.vo.CountryVo;
import com.hrms.web.vo.CurrencyVo;
import com.hrms.web.vo.DesignationRankVo;
import com.hrms.web.vo.EmployeeCategoryVo;
import com.hrms.web.vo.EmployeeExitTypeVo;
import com.hrms.web.vo.EmployeeGradeVo;
import com.hrms.web.vo.EmployeeStatusVo;
import com.hrms.web.vo.EmployeeTypeVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.InsuranceTypeVo;
import com.hrms.web.vo.JobFieldVo;
import com.hrms.web.vo.JobTypeVo;
import com.hrms.web.vo.LanguageVo;
import com.hrms.web.vo.LeaveTypeVo;
import com.hrms.web.vo.LoanStatusVo;
import com.hrms.web.vo.ManagerReportingNotificationVo;
import com.hrms.web.vo.NotificationVo;
import com.hrms.web.vo.OrganisationAssetsVo;
import com.hrms.web.vo.OrganisationVo;
import com.hrms.web.vo.OvertimeStatusVo;
import com.hrms.web.vo.PolicyTypeVo;
import com.hrms.web.vo.PromotionStatusVo;
import com.hrms.web.vo.QualificationDegreeVo;
import com.hrms.web.vo.ReimbursementCategoryVo;
import com.hrms.web.vo.RelegionVo;
import com.hrms.web.vo.ReminderStatusVo;
import com.hrms.web.vo.RemindersVo;
import com.hrms.web.vo.SecondLevelForwardingVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.ShiftDayVo;
import com.hrms.web.vo.SkillVo;
import com.hrms.web.vo.TerminationStatusVo;
import com.hrms.web.vo.TimeZoneVo;
import com.hrms.web.vo.TrainingEventTypeVo;
import com.hrms.web.vo.TrainingTypeVo;
import com.hrms.web.vo.WorkShiftVo;

/**
 * 
 * @author Jithin Mohan
 * @since 10-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles", "autoApprove" })
public class SystemSettingsController extends AbstractService {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private CommonService commonService;

	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private BranchService branchService;
	
	private List<EmployeeVo> employees = null;

	/**
	 * method to handle settings page get request
	 * 
	 * @param model
	 * @return PageNameConstatnts.SETTINGS_PAGE
	 */
	@RequestMapping(value = RequestConstants.SETTINGS_URL, method = RequestMethod.GET)
	public String settingsPageGetRequestHandler(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		logger.info("Inside SystemSettingsController>>>> settingsPageGetRequestHandler....");

		model.addAttribute("baseCurrencies", baseCurrencyService.findAllBaseCurrency());
		model.addAttribute("countryList", countryService.findAllCountry());
		employees = employeeService.listAllEmployee();
		OrganisationVo organisationVo = organisationService.findOrganisation();
		if (organisationVo == null)
			organisationVo = new OrganisationVo();
		model.addAttribute("organisationVo", organisationVo);
		// model.addAttribute("managerRptNotify",
		// getManagerReportingNotificationStatus());
		return PageNameConstatnts.SETTINGS_PAGE;
	}

	/**
	 * method to get ManagerReportingNotificationStatus
	 * 
	 * @return
	 */
	private String getManagerReportingNotificationStatus() {
		ManagerReportingNotificationVo managerReportingNotificationVo = managerReportingNotificationService
				.findManagerReportingNotification(1L);
		if (managerReportingNotificationVo.isReport())
			return "checked";
		return "";
	}

	/**
	 * method to get payroll employees and selected second level forwarders
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SETTINGS_SECOND_LEVELS_URL, method = RequestMethod.GET)
	public String getAllPayrollEmployees(final Model model,
			@ModelAttribute SecondLevelForwardingVo secondLevelForwardingVo, BindingResult result,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			secondLevelForwardingVo
					.setForwaders(secondLevelForwaderService.findAllSecondLevelForwader().getForwaders());
			model.addAttribute("payRollEmployees", com.hrms.web.dto.Employee.getEmployees(employeeService.listEmployeesWithPayroll()));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.PAYROLL_EMPLOYEES_LIST_PAGE;
	}

	/**
	 * method to save second level forwarders
	 * 
	 * @param secondLevelForwardingVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_SECOND_LEVEL_FORWARDER_URL, method = RequestMethod.POST)
	public ModelAndView saveSecondLevelForwarders(@ModelAttribute SecondLevelForwardingVo secondLevelForwardingVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			secondLevelForwaderService.saveSecondLevelForwader(secondLevelForwardingVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS,
					MessageConstants.SECOND_LEVEL_FORWARDER_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to save new country
	 * 
	 * @param model
	 * @param countryVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_COUNTRY_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveCountryWithPost(@RequestBody CountryVo countryVo) {
		logger.info("Inside SystemSettingsController>>>> saveCountryWithPost....");
		JsonResponse jsonResponse = new JsonResponse();
		try {
			countryService.saveCountry(countryVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new language
	 * 
	 * @param model
	 * @param languageVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_LANGUAGE_URL, method = RequestMethod.POST, headers = {
			"Content-type=application/json", "Accept=application/json" })
	public @ResponseBody JsonResponse saveLanguageWithPost(@RequestBody LanguageVo languageVo) {
		logger.info("Inside SystemSettingsController>>>> savelanguageWithPost....");
		JsonResponse jsonResponse = new JsonResponse();
		try {
			languageService.saveLanguage(languageVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new skills
	 * 
	 * @param model
	 * @param skillVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_SKILL_URL, method = RequestMethod.POST, headers = {
			"Content-type=application/json", "Accept=application/json" })
	public @ResponseBody JsonResponse saveSkillWithPost(@RequestBody SkillVo skillVo) {
		logger.info("Inside SystemSettingsController>>>> saveSkillWithPost....");
		JsonResponse jsonResponse = new JsonResponse();
		try {
			skillService.saveSkill(skillVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new qualification degree
	 * 
	 * @param model
	 * @param degreeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_QUALIFICATION_URL, method = RequestMethod.POST, headers = {
			"Content-type=application/json", "Accept=application/json" })
	public @ResponseBody JsonResponse saveQualificationDegreeWithPost(final Model model,
			@RequestBody QualificationDegreeVo degreeVo) {
		logger.info("Inside SystemSettingsController>>>> saveQualificationDegreeWithPost....");
		JsonResponse jsonResponse = new JsonResponse();
		try {
			qualificationService.saveQualificationDegree(degreeVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new contract type
	 * 
	 * @param model
	 * @param contractTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_CONTRACT_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveContractTypeWithPost(final Model model,
			@RequestBody ContractTypeVo contractTypeVo) {
		logger.info("Inside SystemSettingsController>>>> saveQualificationDegreeWithPost....");
		JsonResponse jsonResponse = new JsonResponse();
		try {
			contractTypeService.saveContractType(contractTypeVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new job type
	 * 
	 * @param model
	 * @param jobTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_JOB_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveJobTypeWithPost(final Model model, @RequestBody JobTypeVo jobTypeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jobTypeService.saveJobType(jobTypeVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new job field
	 * 
	 * @param model
	 * @param jobFieldVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_JOB_FIELD_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveJobFieldWithPost(final Model model, @RequestBody JobFieldVo jobFieldVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jobFieldService.saveJobField(jobFieldVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new base currency
	 * 
	 * @param model
	 * @param baseCurrencyVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_BASE_CURRENCY_URL, method = RequestMethod.POST)
	public ModelAndView saveBaseCurrencyWithPost(final Model model, @ModelAttribute BaseCurrencyVo baseCurrencyVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			baseCurrencyService.saveBaseCurrency(baseCurrencyVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.BASE_CURRENCY_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to save new currency with decimal places
	 * 
	 * @param model
	 * @param currencyVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_CURRENCY_URL, method = RequestMethod.POST)
	public ModelAndView saveCurrencyWithPost(final Model model, @ModelAttribute CurrencyVo currencyVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			currencyService.saveCurrency(currencyVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.CURRENCY_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		} finally {
			model.addAttribute("baseCurrencies", baseCurrencyService.findAllBaseCurrency());
		}
		return view;
	}

	/**
	 * method to find base currency details
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.FETCH_BASE_CURRENCY_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getCurrencyByCurrencyName(HttpServletRequest request) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
			jsonResponse.setResult(baseCurrencyService.findBaseCurrencyByCurrency(request.getParameter("currencyName")
					.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save new time zone
	 * 
	 * @param model
	 * @param timeZoneVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_TIME_ZONE_URL, method = RequestMethod.POST)
	public ModelAndView saveTimeZoneWithPost(final Model model, @ModelAttribute TimeZoneVo timeZoneVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			timeZoneService.saveTimeZone(timeZoneVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.TIME_ZONE_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		} finally {
			model.addAttribute("baseCurrencies", baseCurrencyService.findAllBaseCurrency());
		}
		return view;
	}

	/**
	 * method to save new policy type
	 * 
	 * @param model
	 * @param policyTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_POLICY_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse savePolicyTypeWithPost(final Model model, @RequestBody PolicyTypeVo policyTypeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			policyTypeService.savePolicyType(policyTypeVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new employee type
	 * 
	 * @param model
	 * @param employeeTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_EMPLOYEE_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveEmployeeTypeWithPost(final Model model,
			@RequestBody EmployeeTypeVo employeeTypeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeTypeService.saveEmployeeType(employeeTypeVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new employee category
	 * 
	 * @param model
	 * @param employeeCategoryVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_EMPLOYEE_CATEGORY_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveEmployeeCategoryWithPost(@RequestBody EmployeeCategoryVo employeeCategoryVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeCategoryService.saveEmployeeCategory(employeeCategoryVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new insurance type
	 * 
	 * @param model
	 * @param insuranceTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_INSURANCE_TYPE_URL, method = RequestMethod.POST)
	public ModelAndView saveInsuranceTypeWithPost(final Model model, @ModelAttribute InsuranceTypeVo insuranceTypeVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			insuranceTypeService.saveInsuranceType(insuranceTypeVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.INSURANCE_TYPE_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		} finally {
			model.addAttribute("baseCurrencies", baseCurrencyService.findAllBaseCurrency());
		}
		return view;
	}

	/**
	 * method to save new branch type
	 * 
	 * @param model
	 * @param branchTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_BRANCH_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveBranchTypeWithPost(final Model model, @RequestBody BranchTypeVo branchTypeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			branchTypeService.saveBranchType(branchTypeVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new training type
	 * 
	 * @param model
	 * @param trainingTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_TRAINING_TYPE_URL, method = RequestMethod.POST)
	public ModelAndView saveTrainingTypeWithPost(final Model model, @ModelAttribute TrainingTypeVo trainingTypeVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			trainingTypeService.saveTrainingType(trainingTypeVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.TRAINING_TYPE_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to save new training event type
	 * 
	 * @param model
	 * @param trainingEventTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_TRAINING_EVENT_TYPE_URL, method = RequestMethod.POST)
	public ModelAndView saveTrainingEventTypeWithPost(final Model model,
			@ModelAttribute TrainingEventTypeVo trainingEventTypeVo, RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			trainingEventTypeService.saveTrainingEventType(trainingEventTypeVo);
			redirectAttributes
					.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.TRAINING_EVENT_TYPE_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to save new ReimbursementCategory
	 * 
	 * @param model
	 * @param reimbursementCategoryVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_REIMBURSEMENT_CATEGORY_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveReimbursementCategoryWithPost(
			@RequestBody ReimbursementCategoryVo reimbursementCategoryVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			reimbursementCategoryService.saveReimbursementCategory(reimbursementCategoryVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to update manager reporting notification status
	 * 
	 * @param model
	 * @param request
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_MANAGER_REPT_NOTIFY_URL, method = RequestMethod.POST)
	public ModelAndView saveManagerReportingNotificationStatusWithPost(final Model model,
			@ModelAttribute ManagerReportingNotificationVo managerReportingNotificationVo, BindingResult result,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			managerReportingNotificationService.updateManagerReportingNotification(FormValueChecker
					.checkBoxValue(managerReportingNotificationVo.getManagerReportingNotification()));

			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS,
					MessageConstants.MANAGER_REPRT_NOTIFY_SUCCESS);
			model.addAttribute("managerRptNotify", getManagerReportingNotificationStatus());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to save new ReminderStatus
	 * 
	 * @param model
	 * @param reminderStatusVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_REMINDER_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveReminderStatusWithPost(@RequestBody ReminderStatusVo reminderStatusVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			reminderStatusService.saveReminderStatus(reminderStatusVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save new Reminders
	 * 
	 * @param model
	 * @param remindersVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NEW_REMINDERS_URL, method = RequestMethod.POST)
	public ModelAndView saveRemindersWithPost(final Model model, @ModelAttribute RemindersVo remindersVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			List<String> timeValues = DateFormatter.getTimeValues(remindersVo.getReminderTime());
			remindersVo.setReminderTime(timeValues.get(0));
			remindersVo.setTimeMeridian(timeValues.get(1));
			reminderService.saveReminder(remindersVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.REMINDER_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_NOTIFICATION_URL, method = RequestMethod.GET)
	public String addNotification(final Model model,
			@RequestParam(value = "notificationId", required = false) Long notificationId,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			List<NotificationType> notificationTypes = notificationService.fetchAllNotificationType();
			List<MstReference> mstReferences = commonService
					.fectMstreferneceByType(CommonConstants.NOTIFICATION_SENDON_TYPE);
			List<EmployeeVo> employees = this.employees;

			NotificationVo notificationVo = new NotificationVo();
			/*
			 * For editing notification
			 */
			if (notificationId != null && notificationId > 0) {
				notificationVo = notificationService.fetchNotificationById(notificationId);
			}
			model.addAttribute("notificationVo", notificationVo);
			model.addAttribute("sendOnList", mstReferences);
			model.addAttribute("notificationTypeList", notificationTypes);
			model.addAttribute("employeeList", employees);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			model.addAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_NOTIFICATION_PAGE;
	}

	/**
	 * 
	 * @param model
	 * @param notificationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_NOTIFICATION_URL, method = RequestMethod.POST)
	public String saveNotification(final Model model, @ModelAttribute("notificationVo") NotificationVo notificationVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			logger.info("Notification Vo : " + notificationVo);
			notificationService.saveNotification(notificationVo);
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.NOTIFICATION_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			model.addAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.SETTINGS_PAGE;
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.NOTIFICATION_URL, method = RequestMethod.GET)
	public String listAllNotifications(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			List<Notification> notificationList = notificationService.fetchAllNotifications();
			model.addAttribute("notifications", notificationList);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			model.addAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.NOTIFICATION_PAGE;
	}

	/**
	 * method to fetch all the reminders
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_NOTIFICATION_URL, method = RequestMethod.POST)
	public String deleteNotification(@RequestParam(value = "notificationId", required = true) Long notificationId) {
		try {
			logger.info("DeleteNotification " + notificationId);
			notificationService.deleteNotificationById(notificationId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:/notificationList";
	}

	/**
	 * method to save blood group
	 * 
	 * @param model
	 * @param bloodGroupVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_BLOOD_GROUP_URL, method = RequestMethod.POST)
	public ModelAndView saveBloodGroupWithPost(final Model model, @ModelAttribute BloodGroupVo bloodGroupVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			bloodGroupService.saveBloodGroup(bloodGroupVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.BLOOD_GROUP_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to save Religion
	 * 
	 * @param model
	 * @param relegionVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_RELEGION_URL, method = RequestMethod.POST)
	public ModelAndView saveRelegionWithPost(final Model model, @ModelAttribute RelegionVo relegionVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			relegionService.saveRelegion(relegionVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.RELEGION_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to save accountType
	 * 
	 * @param model
	 * @param accountTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_ACCOUNT_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveAccountTypeWithPost(@RequestBody AccountTypeVo accountTypeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			accountTypeService.saveAccountType(accountTypeVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save workShift
	 * 
	 * @param model
	 * @param workShiftVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_WORK_SHIFT_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveWorkShiftWithPost(@RequestBody WorkShiftVo workShiftVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			workShiftService.saveWorkShift(workShiftVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save employeeStatus
	 * 
	 * @param model
	 * @param employeeStatusVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_EMPLOYEE_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveEmployeeStatusWithPost(@RequestBody EmployeeStatusVo employeeStatusVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeStatusService.saveStatus(employeeStatusVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save assets
	 * 
	 * @param model
	 * @param assetsVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_ASSETS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveAssetsWithPost(@RequestBody OrganisationAssetsVo assetsVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			assetsService.saveAssets(assetsVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save termination status
	 * 
	 * @param model
	 * @param terminationStatusVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_TERMINATION_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveTerminationStatusWithPost(@RequestBody TerminationStatusVo terminationStatusVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			terminationStatusService.saveTerminationStatus(terminationStatusVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save promotion Status
	 * 
	 * @param model
	 * @param promotionStatusVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_PROMOTION_STATUS_URL, method = RequestMethod.POST)
	public ModelAndView savePromotionStatusWithPost(final Model model,
			@ModelAttribute PromotionStatusVo promotionStatusVo, RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			promotionStatusService.savePromotionStatus(promotionStatusVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.PROMOTION_STATUS_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to save employeeExitType
	 * 
	 * @param model
	 * @param employeeExitTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_EMPLOYEE_EXIT_TYPE_URL, method = RequestMethod.POST)
	public ModelAndView saveEmployeeExitTypeWithPost(final Model model,
			@ModelAttribute EmployeeExitTypeVo employeeExitTypeVo, RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			employeeExitTypeService.saveEmployeeExitType(employeeExitTypeVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.EMPLOYEE_EXIT_TYPE_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to save leaveType
	 * 
	 * @param model
	 * @param leaveTypeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_LEAVET_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveLeaveTypeWithPost(@RequestBody LeaveTypeVo leaveTypeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			leaveTypeService.saveLeaveType(leaveTypeVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save employeeGrade
	 * 
	 * @param model
	 * @param employeeGradeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_EMPLOYEE_GRADE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveEmployeeGradeWithPost(@RequestBody EmployeeGradeVo employeeGradeVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeGradeService.saveGrade(employeeGradeVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save employee designation
	 * 
	 * @param model
	 * @param employeeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_EMPLOYEE_DESIGNATION_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveEmployeeDesignationWithPost(@RequestBody DesignationRankVo designationRankVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeDesignationService.saveDesignation(designationRankVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save shift day
	 * 
	 * @param model
	 * @param shiftDayVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_SHIFT_DAY_URL, method = RequestMethod.POST)
	public ModelAndView saveShiftDayWithPost(final Model model, @ModelAttribute ShiftDayVo shiftDayVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			shiftDayService.saveShiftDay(shiftDayVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.SHIFT_DAY_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to save advance salary status
	 * 
	 * @param model
	 * @param advanceSalaryStatusVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_ADVANCE_SALARY_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveAdvanceSalaryStatusWithPost(
			@RequestBody AdvanceSalaryStatusVo advanceSalaryStatusVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			advanceSalaryStatusService.saveAdvanceSalaryStatus(advanceSalaryStatusVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save overtime status
	 * 
	 * @param model
	 * @param overtimeStatusVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_OVERTIME_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveOvertimeStatusWithPost(@RequestBody OvertimeStatusVo overtimeStatusVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			overtimeStatusService.saveOvertimeStatus(overtimeStatusVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save overtime status
	 * 
	 * @param model
	 * @param loanStatusVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_LOAN_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveLoanStatusWithPost(@RequestBody LoanStatusVo loanStatusVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			loanStatusService.saveLoanStatus(loanStatusVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to list all admins
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LIST_OF_ADMINS_URL, method = RequestMethod.GET)
	public String listAllsystemAdministrators(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("adminList", employeeService.listAllAdmins());
			model.addAttribute("employeeList",this.employees);
			model.addAttribute("branches", branchService.listAllBranches());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.LIST_OF_ADMINS_PAGE;
	}

	/**
	 * method to update employee admin status
	 * 
	 * @param employeeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_ADMIN_STATUS_URL, method = RequestMethod.POST)
	public ModelAndView updateEmployeeAdminStatusWithPost(@ModelAttribute EmployeeVo employeeVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			employeeService.updateEmployeeAsAdmin(employeeVo.getEmployeeId(), true,employeeVo.getBranchIds());
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.ADMIN_EMPLOYEE_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to remove admin status from an employee
	 * 
	 * @param employeeId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.CHANGE_ADMIN_STATUS_URL, method = RequestMethod.POST)
	public String changeAdminStatusWithPost(@RequestParam(value = "employeeId", required = true) Long employeeId) {
		try {
			employeeService.updateEmployeeAsAdmin(employeeId, false,null);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.LIST_OF_ADMINS_URL;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_LANGUAGES_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableLanguages() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(languageService.findAllLanguage());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_SKILLS_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableSkills() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(skillService.findAllSkills());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_Q_DEGREES_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableQDegrees() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(qualificationService.findAllQualificationDegree());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_CONTRACT_TYPE_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableCTypes() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(contractTypeService.findAllContractType());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_JOB_TYPE_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableJTypes() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(jobTypeService.findAllJobType());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_JOB_FIELD_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableJFields() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(jobFieldService.findAllJobField());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_BRANCH_TYPE_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableBTypes() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(branchTypeService.findAllBranchType());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_POLICY_TYPE_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailablePTypes() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(policyTypeService.findAllPolicyType());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_EMPLOYEE_TYPE_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableETypes() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(employeeTypeService.findAllEmployeeType());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_EMPLOYEE_CATEGORY_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableECategories() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(employeeCategoryService.findAllEmployeeCategory());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_REIMBURSEMENT_CATEGORY_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableReimbCategories() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(reimbursementCategoryService.findAllReimbursementCategory());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_COUNTRY_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableCountries() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(countryService.findAllCountry());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_ACCOUNT_TYPE_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableAccountTypes() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(accountTypeService.findAllAccountType());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_WORK_SHIFTS_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableWorkShifts() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(workShiftService.findAllWorkShift());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_EMPLOYEE_STATUS_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableEStatus() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(employeeStatusService.findAllEmployeeStatus());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_TERMINATION_STATUS_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableTStatus() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(terminationStatusService.findAllTerminationStatus());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_LEAVE_TYPE_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableLTypes() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(leaveTypeService.findAllLeaveType());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_REMINDER_STATUS_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableRStatus() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(reminderStatusService.findAllReminderStatus());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_EMPLOYEE_GRADE_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableEGrades() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(employeeGradeService.findAllGrade());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_ADVANCE_SALARY_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableAsvncStatus() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(advanceSalaryStatusService.findAllAdvanceSalaryStatus());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_OVERTIME_STATUS_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableOStatus() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(overtimeStatusService.findAllOvertimeStatus());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_LOAN_STATUS_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableLStatus() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(loanStatusService.findAllLoanStatus());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_EMPLOYEE_DESIGNATION_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllEmployeeDesignation() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(employeeDesignationService.findAllDesignations());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_AVAILABLE_ORGANIZATION_ASSETS_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllOrganizationAssets() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(assetsService.listAllAssets());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param designationRank
	 * @param designation
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_EMPLOYEE_DESIGNATION_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteEmployeeDesignation(
			@RequestParam(value = "designationRank") Long designationRank,
			@RequestParam(value = "designation") String designation) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeDesignationService.deleteDesignation(employeeDesignationService.findEmployeeDesignation(
					designation, designationRank).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param assetCode
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_ORGANIZATION_ASSET_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteOrganizationAsset(@RequestParam(value = "assetCode") String assetCode) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			assetsService.deleteAsset(assetsService.findAssetObjByCode(assetCode).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param policyTypeId
	 * @return jsonResponse
	 */
	@RequestMapping(value = RequestConstants.DELETE_POLICY_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deletePolicyType(@RequestParam(value = "policyType") String policyType) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			policyTypeService.deletePolicyType(policyTypeService.findPolicyType(policyType).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param language
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_LANGUAGE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteLanguage(@RequestParam(value = "language") String language) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			languageService.deleteLanguage(languageService.findLanguage(language).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param skill
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_SKILL_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteSkill(@RequestParam(value = "skill") String skill) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			skillService.deleteSkill(skillService.findSkill(skill).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param qd
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_QD_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteQD(@RequestParam(value = "qd") String qd) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			qualificationService.deleteQualificationDegree(qualificationService.findQualification(qd).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param contractType
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_CONTRACT_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteContractType(@RequestParam(value = "contractType") String contractType) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			contractTypeService.deleteContractType(contractTypeService.findContractType(contractType).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param jobType
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_JOB_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteJobType(@RequestParam(value = "jobType") String jobType) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jobTypeService.deleteJobType(jobTypeService.findJobType(jobType).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param jobField
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_JOB_FIELD_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteJobField(@RequestParam(value = "jobField") String jobField) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jobFieldService.deleteJobField(jobFieldService.findJobField(jobField).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param branchType
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_BRANCH_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteBranchType(@RequestParam(value = "branchType") String branchType) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			branchTypeService.deleteBranchType(branchTypeService.findAndReturnBranchTypeByName(branchType).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param employeeType
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_EMPLOYEE_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteEmployeeType(@RequestParam(value = "employeeType") String employeeType) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeTypeService.deleteEmployeeType(employeeTypeService.findEmployeeType(employeeType).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param employeeCategory
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_EMPLOYEE_CATEGORY_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteEmployeeCategory(
			@RequestParam(value = "employeeCategory") String employeeCategory) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeCategoryService.deleteEmployeeCategory(employeeCategoryService.findEmployeeCategory(
					employeeCategory).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param reimbursementCategory
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_REIMBURSEMENT_CATEGORY_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteReimbursementCategory(
			@RequestParam(value = "reimbursementCategory") String reimbursementCategory) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			reimbursementCategoryService.deleteReimbursementCategory(reimbursementCategoryService
					.findReimbursementCategory(reimbursementCategory).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param country
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_COUNTRY_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteCountry(@RequestParam(value = "country") String country) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			countryService.deleteCountry(countryService.findCountrybyName(country).getCountryId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param accountType
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_ACCOUNT_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteAccountType(@RequestParam(value = "accountType") String accountType) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			accountTypeService.deleteAccountType(accountTypeService.findAccountType(accountType).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param workShift
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_WORK_SHIFT_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteWorkShift(@RequestParam(value = "workShift") String workShift) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			workShiftService.deleteWorkShift(workShiftService.findWorkShift(workShift).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param employeeStatus
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_EMPLOYEE_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteEmployeeStatus(@RequestParam(value = "employeeStatus") String employeeStatus) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeStatusService.deleteStatus(employeeStatusService.findStatus(employeeStatus).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param terminationStatus
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_TERMINATION_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteTerminationStatus(
			@RequestParam(value = "terminationStatus") String terminationStatus) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			terminationStatusService.deleteTerminationStatus(terminationStatusService.findTerminationStatus(
					terminationStatus).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param leaveType
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_LEAVE_TYPE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteLeaveType(@RequestParam(value = "leaveType") String leaveType) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			leaveTypeService.deleteLeaveType(leaveTypeService.findLeaveType(leaveType).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param reminderStatus
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_REMINDER_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteReminderStatus(@RequestParam(value = "reminderStatus") String reminderStatus) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			reminderStatusService
					.deleteReminderStatus(reminderStatusService.findReminderStatus(reminderStatus).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param employeeGrade
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_EMPLOYEE_GRADE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteEmployeeGrade(@RequestParam(value = "employeeGrade") String employeeGrade) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeGradeService.deleteGrade(employeeGradeService.findGrade(employeeGrade).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param advanceSalaryStatus
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_ADVANCE_SALARY_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteAdvanceSalaryStatus(
			@RequestParam(value = "advanceSalaryStatus") String advanceSalaryStatus) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			advanceSalaryStatusService.deleteAdvanceSalaryStatus(advanceSalaryStatusService.findAdvanceSalaryStatus(
					advanceSalaryStatus).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param overtimeStatus
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_OVERTIME_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteOvertimeStatus(@RequestParam(value = "overtimeStatus") String overtimeStatus) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			overtimeStatusService
					.deleteOvertimeStatus(overtimeStatusService.findOvertimeStatus(overtimeStatus).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}

	/**
	 * 
	 * @param loanStatus
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_LOAN_STATUS_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteLoanStatus(@RequestParam(value = "loanStatus") String loanStatus) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			loanStatusService.deleteLoanStatus(loanStatusService.findLoanStatus(loanStatus).getId());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}
	
	/**
	 * method to list all auto approve employees
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_AUTO_APPROVE_EMPLOYEES, method = RequestMethod.GET)
	public String listAllAutoApproveEmployees(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("approveEmployees", employeeService.listAllAutoApproveEmployees());
			model.addAttribute("employeeList", this.employees);
		} catch (Exception e) {
			logger.error(e.getMessage()); 
		}
		return PageNameConstatnts.AUTO_APPROVE_EMPLOYEES;
	}
	/**
	 * Method to update auto approval status
	 * @param employeeVo
	 * @param redirectAttributes
	 * @param employee
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_AUTO_APPROVAL_STATUS, method = RequestMethod.POST)
	public ModelAndView updateAutoApprovalStatus(@ModelAttribute EmployeeVo employeeVo,
			RedirectAttributes redirectAttributes, @ModelAttribute("authEmployee") Employee employee, final Model model) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SETTINGS_URL));
		try {
			employeeService.updateEmployeeWithAutoApprove(employeeVo.getEmployeeId(), true);
			if (employee.getId() == employeeVo.getEmployeeId())
				model.addAttribute("autoApprove", true);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.AUTO_APPROVAL_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}
	/**
	 * Method to remove auto approval status
	 * @param employeeId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.CHANGE_AUTO_APPROVAL, method = RequestMethod.POST)
	public String changeAutoApprovalStatus(@RequestParam(value = "employeeId", required = true) Long employeeId) {
		try {
			employeeService.updateEmployeeWithAutoApprove(employeeId, false);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.GET_AUTO_APPROVE_EMPLOYEES;
	}

}
