package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.SelectListDto;
import com.hrms.web.dto.Subordinates;
import com.hrms.web.dto.Superiors;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.mail.ApplicationMailSender;
import com.hrms.web.notifications.NotificationContent;
import com.hrms.web.notifications.PushNotification;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.SecondLevelForwaderService;
import com.hrms.web.service.TerminationService;
import com.hrms.web.service.TerminationStatusService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.TerminationDocumentsVo;
import com.hrms.web.vo.TerminationStatusVo;
import com.hrms.web.vo.TerminationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 19-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "employeeExitCode", "thisUser", "roles", "autoApprove" })
public class TerminationController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private TerminationService terminationService;
	
	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private TerminationStatusService terminationStatusService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private PushNotification pushNotification;

	@Autowired
	private SecondLevelForwaderService secondLevelForwaderService;

	@Autowired
	private UserService userService;

	@Autowired
	@Qualifier(value = "taskExecutor")
	private TaskExecutor taskExecutor;

	/**
	 * method to list all the created terminations
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_TERMINATION_PAGE_URL, method = RequestMethod.GET)
	public String loadEmployeeTerminationPageWithGet(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo, @ModelAttribute("thisUser") String thisUser) {
		model.addAttribute("rolesMap", rolesMap);
		model.addAttribute("terminations",
				terminationService.findAllTerminationByEmployee(employeeVo.getEmployeeId()));
		model.addAttribute("employeeExitCode", MessageConstants.DEFAULT_CODE); 
		model.addAttribute("thisUser", thisUser);
		return PageNameConstatnts.TERMINATION_PAGE;
	}

	/**
	 * method to load page for new termination
	 * 
	 * @param model
	 * @param tid
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_TERMINATION_CREATE_URL, method = RequestMethod.GET)
	public String loadCreateTerminationPageWithGet(final Model model,
			@ModelAttribute("employeeExitCode") String employeeCode, @ModelAttribute TerminationVo terminationVo,
			BindingResult result, @ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			terminationVo = terminationService.findTerminationByEmployee(employeeCode);
			if (terminationVo == null) {
				terminationVo = new TerminationVo();
				terminationVo.setRecordedBy(employeeVo.getName());
				model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
			} else {
				terminationVo.setRecordedBy(userService.findUser(terminationVo.getRecordedBy()).getEmployee());
				model.addAttribute("superiorList", Superiors.getSuperiors(employeeService
						.getEmployeesDetails(
								employeeService.findAndReturnEmployeeByCode(terminationVo.getEmployeeCode()).getId())
						.getSuperiorSubordinateVo().getSuperiors()));
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
				model.addAttribute(MessageConstants.CREATED_ON, terminationVo.getRecordedOn());
			}
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("subordinates", Subordinates.getSubordinates(employeeService.listSuperiorSubordinates(
					employeeVo.getEmployeeId()).getSubordinates()));
			model.addAttribute("statusList",
					getAllTerminationStatus(terminationStatusService.findAllTerminationStatus()));
			model.addAttribute("terminationVo", terminationVo);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.TERMINATION_CREATE_PAGE;
	}

	/**
	 * method to handle Termination Edit page
	 * 
	 * @param model
	 * @param tid
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_TERMINATION_EDIT_URL, method = RequestMethod.GET)
	public String handleEditEmployeeTerminationPage(final Model model,
			@RequestParam(value = "tid", required = false) Long tid, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			TerminationVo terminationVo = terminationService.findTerminationById(tid);
			terminationVo.setRecordedBy(userService.findUser(terminationVo.getRecordedBy()).getEmployee());
			model.addAttribute("superiorList", Superiors.getSuperiors(employeeService
					.getEmployeesDetails(
							employeeService.findAndReturnEmployeeByCode(terminationVo.getEmployeeCode()).getId())
					.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute(MessageConstants.CREATED_ON, terminationVo.getRecordedOn());
			model.addAttribute("terminationVo", terminationVo);
			model.addAttribute("statusList",
					getAllTerminationStatus(terminationStatusService.findAllTerminationStatus()));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.TERMINATION_CREATE_PAGE;
	}

	/**
	 * method to view termination information
	 * 
	 * @param model
	 * @param terminationId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_TERMINATION_VIEW_URL, method = RequestMethod.GET)
	public String viewTerminationInformation(final Model model,
			@RequestParam(value = "otp_tid", required = true) Long terminationId,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			TerminationVo terminationVo = terminationService.findTerminationById(terminationId);
			model.addAttribute("terminationVo", terminationVo);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.TERMINATION_VIEW_PAGE;
	}

	/**
	 * method to fetch all status
	 * 
	 * @param statusVos
	 * @return
	 */
	private List<String> getAllTerminationStatus(List<TerminationStatusVo> statusVos) {
		List<String> status = new ArrayList<String>();
		for (TerminationStatusVo statusVo : statusVos)
			status.add(statusVo.getStatus());
		return status;
	}

	/**
	 * method to save new termination
	 * 
	 * @param model
	 * @param terminationVo
	 * @param result
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_TERMINATION_CREATE_URL, method = RequestMethod.POST)
	public ModelAndView saveTerminationInformationWithPost(@ModelAttribute TerminationVo terminationVo,
			BindingResult result, RedirectAttributes redirectAttributes,
			@ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.EMPLOYEE_TERMINATION_CREATE_URL));
		try {
			terminationService.saveTermination(terminationVo);
			pushNotification.sendNotification(createNotificationContent(terminationVo, employeeVo.getProfilePic()));
			sendTerminationMail(terminationVo.getForwaders(), employeeVo.getName(),
					employeeService.getEmployeesDetails(terminationVo.getEmployeeCode()),
					DateFormatter.convertStringToDate(terminationVo.getTermiantedDate()));
			redirectAttributes.addFlashAttribute("employeeExitCode", terminationVo.getEmployeeCode());
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_TERMINATION_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to send termination mail to superiors
	 * 
	 * @param forwarders
	 */
	private void sendTerminationMail(List<String> forwarders, String sender, EmployeeVo withWhome, Date date) {
		List<String> emails = new ArrayList<String>();
		try {
			List<String> secondsEmails = new ArrayList<String>();
			secondsEmails = secondLevelForwaderService.findAllSecondLevelForwaderEmails().getEmail();
			if (secondsEmails != null)
				emails.addAll(secondsEmails);
			for (String employeeCode : forwarders) {
				Employee employee = employeeService.findAndReturnEmployeeByCode(employeeCode);
				if (employee != null && employee.getUser() != null)
					emails.add(employee.getUser().getEmail());
			}
			if (emails.size() > 0) {
				ApplicationMailSender applicationMailSender = new ApplicationMailSender(emails,
						CommonConstants.TERMINATION_MAIL, sender, withWhome, date);
				taskExecutor.execute(applicationMailSender);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * method to create NotificationContent object
	 * 
	 * @param terminationVo
	 * @return content
	 */
	private NotificationContent createNotificationContent(TerminationVo terminationVo, String profilePic) {
		NotificationContent content = new NotificationContent();
		List<String> recipients = new ArrayList<String>();

		recipients.addAll(terminationVo.getForwaders());
		recipients.add(terminationVo.getEmployeeCode());
		recipients.addAll(secondLevelForwaderService.findAllSecondLevelForwader().getForwaders());

		content.setContent("Employee Termination");
		content.setUrl("ViewTermination?otp_tid=" + terminationVo.getTerminationId());
		content.setPicture(profilePic);
		content.setRecipients(recipients);
		return content;
	}

	/**
	 * method to delete selected termination
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_TERMINATION_DELETE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteThisTerminationWithPost(final HttpServletRequest request) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			terminationService.deleteTermination(Long.parseLong(request.getParameter("ter_Id")));
			jsonResponse.setResult(terminationService.findAllTermination());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save termination's documents
	 * 
	 * @param model
	 * @param terminationVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_TERMINATION_DOC_SAVE_URL, method = RequestMethod.POST)
	public @ResponseBody ModelAndView saveTerminationDocumentsWithPost(@ModelAttribute TerminationVo terminationVo,
			@RequestParam(value = "terminationId", required = true) Long terminationId,
			MultipartHttpServletRequest request) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.EMPLOYEE_TERMINATION_PAGE_URL));
		try {
			List<TerminationDocumentsVo> documentsVos = uploadTerminationDocuments(request);
			if (!documentsVos.isEmpty()) {
				terminationVo.setTerminationId(terminationId);
				terminationVo.setTerminationDocumentsVos(documentsVos);
				terminationService.saveTerminationDocuments(terminationVo);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return view;
	}

	/**
	 * method to upload termination documents
	 * 
	 * @param multipartFiles
	 * @return documentsVos
	 */
	private List<TerminationDocumentsVo> uploadTerminationDocuments(MultipartHttpServletRequest request) {
		List<TerminationDocumentsVo> documentsVos = new ArrayList<TerminationDocumentsVo>();
		try {
			Iterator<String> itrator = request.getFileNames();
			while (itrator.hasNext()) {
				MultipartFile multipartFile = request.getFile(itrator.next());
				if (!multipartFile.isEmpty()) {
					String fileAccesUrl = FileUpload.uploadDocument(MessageConstants.TERMINATION_MODULE, multipartFile);
					if (fileAccesUrl != null) {
						TerminationDocumentsVo documentVo = new TerminationDocumentsVo();
						documentVo.setDocumentUrl(fileAccesUrl);
						documentVo.setDocumentName(multipartFile.getOriginalFilename());
						documentsVos.add(documentVo);
					}
				}

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return documentsVos;
	}

	/**
	 * method to list all termination documents
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_TERMINATION_DOC_LIST_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse readAllTerminationDocumentsWithPost(final HttpServletRequest request) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			TerminationVo terminationVo = terminationService.findTerminationById(Long.parseLong(request
					.getParameter("terId")));
			jsonResponse.setResult(terminationVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to delete termination document
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_TERMINATION_DOC_DELETE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteThisTerminationDocumentWithPost(final HttpServletRequest request) {
		JsonResponse jsonResponse = new JsonResponse();
		SFTPOperation sftpOperation = new SFTPOperation();
		try {
			if (sftpOperation.removeFileFromSFTP(request.getParameter("url"))) {
				terminationService.deleteTerminationDocument(Long.parseLong(request.getParameter("docId")));
				TerminationVo terminationVo = terminationService.findTerminationById(Long.parseLong(request
						.getParameter("terId")));
				jsonResponse.setResult(terminationVo);
				jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to get employee superiors
	 * 
	 * @param model
	 * @param employeeCode
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_SUPERIOR_LIST_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getMySuperiorsWithGet(
			@RequestParam(value = "employeeCode", required = true) String employeeCode,
			@ModelAttribute("autoApprove") Boolean autoApprove) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			List<SelectListDto> list = new ArrayList<SelectListDto>();
			if (autoApprove) {
				SelectListDto dto = new SelectListDto();
				dto.setLabel("Auto Approve");
				dto.setValue("auto");
				list.add(dto);
			}
			list.addAll(Superiors.getSuperiors(employeeService
					.getEmployeesDetails(employeeService.findAndReturnEmployeeByCode(employeeCode).getId())
					.getSuperiorSubordinateVo().getSuperiors()));
			jsonResponse.setResult(list);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		// return PageNameConstatnts.EMPLOYEE_SUPERIOR_LIST_PAGE;
		return jsonResponse;
	}
}

