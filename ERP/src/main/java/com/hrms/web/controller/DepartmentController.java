package com.hrms.web.controller;


import java.util.ArrayList;
import java.util.List;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
//import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ProjectService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.AdvanceSalaryVo;
import com.hrms.web.vo.DepartmentDocumentsVo;
import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.ProjectDocumentsVo;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SFTPFilePropertiesVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Sangeeth and Bibin
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails" , "roles","departmentVo"})
public class DepartmentController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private BranchService branchService;

	/**
	 * method to handle department page get request
	 * 
	 * @param model
	 * @return PageNameConstatnts.PROJECTS_PAGE
	 */
	@RequestMapping(value = RequestConstants.DEPARTMENT_URL, method = RequestMethod.GET)
	public String departmentPageGetRequestHandler(final Model model,
			HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		logger.info("inside>> DepartmentController... departmentPageMappingGet");
		model.addAttribute("rolesMap", rolesMap);
		String msg = request.getParameter(MessageConstants.SUCCESS);
		if (msg != null) {
			model.addAttribute(MessageConstants.SUCCESS, msg);
		}
		try {
			model.addAttribute("departmentVo",new DepartmentVo());
			model.addAttribute("departments", departmentService.listAllDepartmentDetails());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.DEPARTMENT_PAGE;
	}

	/**
	 * method to add department
	 * 
	 * @param model
	 * @param
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_DEPARTMENT_URL, method = RequestMethod.GET)
	
	public String addDepartmentPageGetRequestHandler(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap,@ModelAttribute("departmentVo") DepartmentVo departmentVo) {
		logger.info("inside>> DepartmentController... addDepartmentPageMappingGet");
		try {
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			if(departmentVo.getId()!=null){
				model.addAttribute("departmentVo",departmentService.findDepartmetById(departmentVo.getId()));
			}

		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_DEPARTMENT_PAGE;
	}

	/**
	 * method to add department
	 * 
	 * @param model
	 * @param
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_DEPARTMENT_URL, method = RequestMethod.GET)
	public String viewDepartmentPageGetRequestHandler(final Model model, @ModelAttribute DepartmentVo departmentVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		logger.info("inside>> DepartmentController... viewDepartmentPageMappingGet");
		try {
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_DEPARTMENT_PAGE;
	}

	/**
	 * method to save Department
	 * 
	 * @param model
	 * @param departmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_DEPARTMENT_URL, method = RequestMethod.POST)
	public ModelAndView saveDepartment(RedirectAttributes redirectAttributes, final Model model,@ModelAttribute DepartmentVo departmentVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		logger.info("inside>> Department Controller... saveDepartment");
		ModelAndView view=new ModelAndView(new RedirectView(RequestConstants.ADD_DEPARTMENT_URL));
		model.addAttribute("rolesMap", rolesMap);
		try {
			logger.info("inside>> Department Controller... parent depatment" + departmentVo.getParentDepartmentId());
			if(departmentVo.getId()!=null)
			{
				departmentService.updateDepartment(departmentVo);
				if(departmentVo.getDepartment_file_upload()!=null)
				{
				departmentVo.setDepartmentDocument(uploadDocuments(departmentVo.getDepartment_file_upload()));
				departmentService.updateDepartmentDocument(departmentVo);
				}
				
			}
			else
			{
			departmentService.saveDepartment(departmentVo);
			}
			redirectAttributes.addFlashAttribute("departmentVo", departmentVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.DEPARTMENT_SAVE_SUCCESS);

		} catch (Exception e) {			
			e.printStackTrace();
			logger.info(e.getMessage());
		}
		//return PageNameConstatnts.ADD_DEPARTMENT_PAGE;
		return view;
	}
	
	
	@RequestMapping(value = RequestConstants.SAVE_DEPARTMENT_DOCUMENT_URL, method = RequestMethod.POST)
	public String saveDepartmentDocument(final Model model, @ModelAttribute DepartmentVo departmentVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		logger.info("inside>> Department Controller... saveDepartmentDocument");
		model.addAttribute("rolesMap", rolesMap);
		try {
			/*logger.info("inside>> Department Controller... parent depatment" + departmentVo.getParentDepartmentId());
			if (departmentVo.getDepartment_file_upload().getSize() != 0) {
				departmentVo.setDocumentName(departmentVo.getDepartment_file_upload().getOriginalFilename());
				String fileAccesUrl = FileUpload.uploadDocument("department", departmentVo.getDepartment_file_upload());
				departmentVo.setDocumentAccessUrl(fileAccesUrl);
			}*/
			departmentService.saveDepartment(departmentVo);
			model.addAttribute("departments", departmentService.listAllDepartmentDetails());
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.DEPARTMENT_SAVE_SUCCESS);

		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.DEPARTMENT_PAGE;
	}

	/**
	 * method to reach edit department page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_DEPARTMENT_URL, method = RequestMethod.GET)
	public String editDepartmentDetailsGet(final Model model, HttpServletRequest request,
			@ModelAttribute DepartmentVo departmentVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> ProjecController... editDepartmentDetailsGet");
		try {
			model.addAttribute("departmentVo",departmentService.findDepartmetById(Long.parseLong(request.getParameter("pid"))));
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());

		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.EDIT_DEPARTMENT_PAGE;
	}

	/**
	 * method to delete department
	 * 
	 * @param model
	 * @param request
	 * @param departmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_DEPARTMENT_URL, method = RequestMethod.GET)
	public String deleteProject(final Model model, HttpServletRequest request, @ModelAttribute DepartmentVo departmentVo) {
		logger.info("inside>> DepartmentController... deleteDepartment");
		try {
			departmentService.deleteDepartment(Long.parseLong(request.getParameter("pid")));
			model.addAttribute("departments", departmentService.listAllDepartmentDetails());
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.DEPARTMENT_DELETE_SUCCESS);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.DEPARTMENT_PAGE;

	}

	/**
	 * method to update department
	 * 
	 * @param model
	 * @param departmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_DEPARTMENT_URL, method = RequestMethod.POST)
	public ModelAndView updateProject(final Model model, HttpServletRequest request, @ModelAttribute DepartmentVo departmentVo,RedirectAttributes redirectAttributes) {
		logger.info("inside>> DepartmentController... updateDepartment");
		ModelAndView view=new ModelAndView(new RedirectView(RequestConstants.ADD_DEPARTMENT_URL));
		try {
			departmentService.updateDepartment(departmentVo);
			if(departmentVo.getDepartment_file_upload()!=null){
			departmentVo.setDepartmentDocument(uploadDocuments(departmentVo.getDepartment_file_upload()));
			departmentService.updateDepartmentDocument(departmentVo);
			}
			redirectAttributes.addFlashAttribute("departmentVo", departmentVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.DEPARTMENT_UPDATE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return view;
	}

	/**
	 * method to reach view department page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_DEPARTMENT_EMPLOYEE_URL, method = RequestMethod.GET)
	public String viewDepartmentDetailsGet(final Model model, HttpServletRequest request,
			@ModelAttribute ProjectVo projectVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> DepartmentController... viewDepartmentDetailsGet");
		try {
			model.addAttribute("departmentVo",departmentService.findDepartmetById(Long.parseLong(request.getParameter("pid"))));
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.VIEW_DEPART_EMPLOYEE_PAGE;
	}

	/**
	 * method to upload files
	 * 
	 * @param multipartFile
	 * @return
	 */
	private String uploadFiles(MultipartFile multipartFile) {
		String url = null;
		SFTPOperation sftpOperation = new SFTPOperation();
		SFTPFilePropertiesVo sftpFilePropertiesVo = new SFTPFilePropertiesVo();
		sftpFilePropertiesVo.setFile(multipartFile);
		sftpFilePropertiesVo.setModuleName("department");
		sftpFilePropertiesVo.setUserName("hrms");
		logger.info("+++++++++++++++++++++++++++++++++++++" + multipartFile.getOriginalFilename());
		sftpFilePropertiesVo.setFileName(multipartFile.getOriginalFilename());
		try {
			url = sftpOperation.sendFileToSFTPServer(sftpFilePropertiesVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return url;
	}
	
	
	/**
	 * method to upload project documents
	 * @param files
	 * @return
	 */
	private List<DepartmentDocumentsVo> uploadDocuments(List<MultipartFile> files){
		List<DepartmentDocumentsVo> departmentDocumentsVos = new ArrayList<DepartmentDocumentsVo>();
		for(MultipartFile file : files){
			if(! file.isEmpty()){
				String url = FileUpload.uploadDocument("department", file);
				if(url!=null){
					DepartmentDocumentsVo vo = new DepartmentDocumentsVo();
					vo.setUrl(url);
					vo.setFileName(file.getOriginalFilename());
					departmentDocumentsVos.add(vo);
				}
			}
		}
		return departmentDocumentsVos;
	}

	/**
	 * method to delete document
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_DEPARTMENT_DOCUMENT, method = RequestMethod.GET)
	public String deleteDocumentGet(final Model model, HttpServletRequest request) {
		String url = request.getParameter("durl");
		Long document_id = Long.parseLong(request.getParameter("doc_id"));
		Boolean result = false;
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			result = sftpOperation.removeFileFromSFTP(url);
		} catch (Exception e) {
			e.printStackTrace();
			return PageNameConstatnts.DEPARTMENT_PAGE;
		}
		if (result) {
			try {
				departmentService.deleteDepartmentDocument(document_id);
				model.addAttribute(MessageConstants.ERROR, MessageConstants.DEPARTMENT_DOCUMENT_DELETE_SUCCESS);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return PageNameConstatnts.DEPARTMENT_PAGE;
		} else {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.DEPARTMENT_DOCUMENT_DELETE_FAIL);
			return PageNameConstatnts.DEPARTMENT_PAGE;
		}
	}

	/**
	 * method to download and show document
	 * 
	 * @param model
	 * @param request
	 * @param projectVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_DEPARTMENT_DOCUMENT, method = RequestMethod.GET)
	public void viewDocumentGet(final Model model, HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute DepartmentVo departmentVo) {
		String url = request.getParameter("durl");
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			sftpOperation.downloadFileFromSFTP(url, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
