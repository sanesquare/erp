package com.hrms.web.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.SelectedEmployee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.InsuranceService;
import com.hrms.web.service.InsuranceTypeService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.EmployeePopUp;
import com.hrms.web.vo.InsuranceTypeVo;
import com.hrms.web.vo.InsuranceVo;

/**
 * 
 * @author Jithin Mohan
 * @since 10-April-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "insuranceVo" })
public class InsuranceController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private InsuranceTypeService insuranceTypeService;

	@Autowired
	private InsuranceService insuranceService;

	/**
	 * method to list all Insurances
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.INSURANCE_URL, method = RequestMethod.GET)
	public String getAllInsurancesWithGet(final Model model) {
		try {
			model.addAttribute("insuranceList", insuranceService.findAllInsurance());
			model.addAttribute("insuranceVo", new InsuranceVo());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_INSURANCE_PAGE;
	}

	/**
	 * method to load add new insurance page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_INSURANCE_URL, method = RequestMethod.GET)
	public String getCreateInsurandePageWithGet(final Model model,
			@ModelAttribute("insuranceVo") InsuranceVo insuranceVo, BindingResult result) {
		try {
			model.addAttribute("insuranceTypeList", getInsuranceTypeList(insuranceTypeService.findAllInsuranceType()));
			model.addAttribute("insuranceVo", insuranceVo);
			if (insuranceVo.getInsuranceId() == null) {
				model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
			} else {
				model.addAttribute(MessageConstants.CREATED_ON, insuranceVo.getRecordedOn());
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ADD_INSURANCE_PAGE;
	}

	/**
	 * method to create list of insurancetype
	 * 
	 * @param insuranceTypes
	 * @return
	 */
	private List<String> getInsuranceTypeList(List<InsuranceTypeVo> insuranceTypes) {
		List<String> insuranceTypeList = new ArrayList<String>();
		for (InsuranceTypeVo insuranceType : insuranceTypes)
			insuranceTypeList.add(insuranceType.getInsuranceType());
		return insuranceTypeList;
	}

	/**
	 * method to view selected insurance details
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_INSURANCE_URL, method = RequestMethod.GET)
	public String viewInsurandePageWithGet(final Model model,
			@RequestParam(value = "otp", required = true) Long insuranceId) {
		try {
			model.addAttribute("insurance", insuranceService.findInsuranceById(insuranceId));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.VIEW_INSURANCE_PAGE;
	}

	/**
	 * method to get insurance info for edit
	 * 
	 * @param insuranceId
	 * @param model
	 */
	@RequestMapping(value = RequestConstants.EDIT_INSURANCE_URL, method = RequestMethod.GET)
	public String editInsurancePageWithGet(@RequestParam(value = "otp", required = true) Long insuranceId, Model model) {
		try {
			InsuranceVo insuranceVo = insuranceService.findInsuranceById(insuranceId);
			model.addAttribute("insuranceVo", insuranceVo);
			model.addAttribute("insuranceTypeList", getInsuranceTypeList(insuranceTypeService.findAllInsuranceType()));
			model.addAttribute(MessageConstants.CREATED_ON, insuranceVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_INSURANCE_PAGE;
	}

	/**
	 * method to list all insurances
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LIST_ALL_INSURANCE_URL, method = RequestMethod.GET)
	public String listAllInsurances(final Model model) {
		try {
			model.addAttribute("insuranceList", insuranceService.findAllInsurance());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.INSURANCE_LIST_PAGE;
	}

	/**
	 * method to delete insurances
	 * 
	 * @param insuranceId
	 * @param model
	 */
	@RequestMapping(value = RequestConstants.DELETE_INSURANCE_URL, method = RequestMethod.POST)
	public String deleteInsuranceWithPost(@RequestParam(value = "insuranceId", required = true) Long insuranceId,
			final Model model) {
		try {
			insuranceService.deleteInsurance(insuranceId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return "redirect:" + RequestConstants.LIST_ALL_INSURANCE_URL;
	}

	/**
	 * method to save new insurance
	 * 
	 * @param insuranceVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_INSURANCE_URL, method = RequestMethod.POST)
	public ModelAndView saveInsuranceWithPost(@ModelAttribute InsuranceVo insuranceVo,
			RedirectAttributes redirectAttributes, BindingResult result) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_INSURANCE_URL));
		try {
			insuranceService.saveOrUpdateInsurance(insuranceVo);
			redirectAttributes.addFlashAttribute("insuranceVo", insuranceVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_INSURANCE_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to get all branches
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_ORG_BRANCHES_URL, method = RequestMethod.GET)
	public String getAllBranchesWithGet(final Model model) {
		try {
			model.addAttribute("branchList", branchService.listAllBranches());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ORG_BRACNH_LIST_PAGE;
	}

	/**
	 * method to list all the departments from the chosen branch
	 * 
	 * @param model
	 * @param branchId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_ORG_BRANCH_DEPARTMENTS_URL, method = RequestMethod.GET)
	public String getAllBranchDepartmentsWithGet(final Model model,
			@RequestParam(value = "branchId", required = true) Long branchId) {
		try {
			model.addAttribute("branch", branchId);
			model.addAttribute("departmentList",
					departmentService.findDepartmentByBranch(branchService.findAndReturnBranchById(branchId)));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ORG_BRACNH_DEPARTMENT_LIST_PAGE;
	}

	/**
	 * method to list all the employees from the chosen branch and department
	 * 
	 * @param model
	 * @param branchId
	 * @param departmentId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_ALL_ORG_BRANCH_DEPARTMENTS_EMPLOYEE_URL, method = RequestMethod.GET)
	public String getAllBranchDepartmentsEmployeeWithGet(final Model model,
			@RequestParam(value = "branchId", required = true) Long branchId,
			@RequestParam(value = "departmentId", required = true) Long departmentId) {
		try {
			model.addAttribute("branch", branchId);
			model.addAttribute("employeeList",
					employeeService.getEmployeesByBranchAndDepartment(branchId, departmentId));
			model.addAttribute("insuranceVo", new InsuranceVo());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ORG_BRACNH_DEPARTMENT_EMP_LIST_PAGE;
	}

	/**
	 * method to list all the employees from the chosen branch and department
	 * 
	 * @param model
	 * @param branchId
	 * @param departmentId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SET_SELECTED_EMPLOYEE_TO_VIEW_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse setSelectedEmployeeWithGet(
			@RequestParam(value = "employeeCode", required = true) String employeeCode,
			@RequestParam(value = "employeeName", required = true) String employeeName) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			SelectedEmployee employee = new SelectedEmployee();
			employee.setName(employeeName);
			employee.setCode(employeeCode);
			jsonResponse.setResult(employee);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		// return PageNameConstatnts.ORG_BRACNH_DEPARTMENT_SEL_EMP_PAGE;
		return jsonResponse;
	}

	/**
	 * method to get employee popup details
	 * 
	 * @param employeeCode
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_BASIC_INFO_POPUP_URL, method = RequestMethod.GET)
	public String getEmployeePopupDetailsWithGet(
			@RequestParam(value = "employeeCode", required = true) String employeeCode, final Model model) {
		try {
			model.addAttribute("popUpInfo",
					EmployeePopUp.createEmployeePopupVo(employeeService.findAndReturnEmployeeByCode(employeeCode)));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.EMPLOYEE_BASIC_INFO_POPUP_PAGE;
	}
}
