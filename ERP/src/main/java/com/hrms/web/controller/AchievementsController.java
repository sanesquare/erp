/**
 * 
 */
package com.hrms.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.vo.AchievementsVo;

/**
 * @author shimil babu
 *
 */
@Controller
public class AchievementsController {
	
	/**
	 * method to handle achievements page get request
	 * @param model
	 * @return PageNameConstatnts.ANNOUNCEMENTS_PAGE
	 */
	@RequestMapping(value=RequestConstants.ACHIEVEMENTS_URL,method=RequestMethod.GET)
	public String achievementsPageGetRequestHandler(final Model model,@ModelAttribute AchievementsVo achievementsVo){
		//logger.info("inside>> achievements controller... achievementsPageMappingGet");
		try{
			//model.addAttribute("achievements", announcementsService.findAllAnnouncements());
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.ACHIEVEMENTS_PAGE;	
	}
	
	/**
	 * method to handle add achievements page get request
	 * @param model
	 * @return PageNameConstatnts.add_achievements_page
	 */
	
	@RequestMapping(value=RequestConstants.ADD_ACHIEVEMENTS_URL,method=RequestMethod.GET)
	public String addAchievementsPageGetRequestHandler(final Model model,@ModelAttribute AchievementsVo achievementsVo){
		//logger.info("inside>> announcements controller... addAnnouncementsPageMappingGet");
		try{
			//model.addAttribute("announcements", announcementsService.findAllAnnouncements());
			//model.addAttribute("status", announcementsStatusService.findAllAnnouncementsStatus());
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_ACHIEVEMENTS_PAGE;
	}
	

}
