package com.hrms.web.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.EsiService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.EsiVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Jithin Mohan
 * @since 16-April-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "esiVo", "thisUser" , "roles"})
public class EsiController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private EsiService esiService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private EmployeeService employeeService;

	/**
	 * method to load all the esi
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ESI_URL, method = RequestMethod.GET)
	public String getEsiListPage(final Model model, @ModelAttribute("thisUser") String thisUser,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("esiList", esiService.findAllEsiByEmployee(authEmployeeDetails.getEmployeeCode()));
			model.addAttribute("esiVo", new EsiVo());
			model.addAttribute("thisUser", thisUser);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_ESI_PAGE;
	}

	/**
	 * method to load add esi page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_ESI_URL, method = RequestMethod.GET)
	public String getAddEsiPage(final Model model, @ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo,
			@ModelAttribute("esiVo") EsiVo esiVo, BindingResult result,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		model.addAttribute("esiVo", esiVo);
		model.addAttribute("payrollEmployees", Employee.getEmployees(employeeService.listEmployeesWithPayroll()));
		if (esiVo.getEsiId() == null) {
			esiVo.setRecordedBy(employeeVo.getName());
			model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
		} else {
			esiVo.setRecordedBy(userService.findUser(esiVo.getRecordedBy()).getEmployee());
			model.addAttribute(MessageConstants.CREATED_ON, esiVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		}
		return PageNameConstatnts.ADD_ESI_PAGE;
	}

	/**
	 * method to save esi information
	 * 
	 * @param esiVo
	 * @param result
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_ESI_URL, method = RequestMethod.POST)
	public ModelAndView saveEsiInformationWithPost(@ModelAttribute EsiVo esiVo, BindingResult result,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_ESI_URL));
		try {
			esiService.saveOrUpdateEsi(esiVo);
			redirectAttributes.addFlashAttribute("esiVo", esiVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_ESI_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to edit esi information
	 * 
	 * @param esiId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_ESI_URL, method = RequestMethod.GET)
	public String editEsiInformationWithPost(@RequestParam(value = "otp", required = true) Long esiId, final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			EsiVo esiVo = esiService.findEsiById(esiId);
			esiVo.setRecordedBy(userService.findUser(esiVo.getRecordedBy()).getEmployee());
			model.addAttribute("esiVo", esiVo);
			model.addAttribute(MessageConstants.CREATED_ON, esiVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PageNameConstatnts.ADD_ESI_PAGE;
	}

	/**
	 * method to view esi information
	 * 
	 * @param esiId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_ESI_URL, method = RequestMethod.GET)
	public String viewEsiInformationWithPost(@RequestParam(value = "otp", required = true) Long esiId, final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("esiVo", esiService.findEsiById(esiId));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PageNameConstatnts.VIEW_ESI_PAGE;
	}

	/**
	 * method to list all esi information
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LIST_ALL_ESI_URL, method = RequestMethod.GET)
	public String listAllEsiInformations(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("esiList", esiService.findAllEsi());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ESI_LIST_PAGE;
	}

	/**
	 * method to delete esi information
	 * 
	 * @param esiId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_ESI_URL, method = RequestMethod.POST)
	public String deleteEsiInformationWithPost(@RequestParam(value = "esiId", required = true) Long esiId,
			final Model model) {
		try {
			esiService.deleteEsi(esiId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return "redirect:" + RequestConstants.LIST_ALL_ESI_URL;
	}
}
