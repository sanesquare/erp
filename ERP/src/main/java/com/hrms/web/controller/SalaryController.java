package com.hrms.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.avro.data.Json;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ExtraPaySlipItemService;
import com.hrms.web.service.PayRollOptionService;
import com.hrms.web.service.PaySlipItemService;
import com.hrms.web.service.SalaryService;
import com.hrms.web.service.SalaryTypeService;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.ExtraPaySlipItemVo;
import com.hrms.web.vo.PayslipItemAjaxVo;
import com.hrms.web.vo.SalaryPayslipItemsVo;
import com.hrms.web.vo.SalaryVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 18-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class SalaryController {

	@Log
	private Logger logger;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private SalaryTypeService salaryTypeService;

	@Autowired
	private SalaryService salaryService;

	@Autowired
	private ExtraPaySlipItemService extraPaySlipItemService;

	/**
	 * method to reach salary page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SALARY_URL, method = RequestMethod.GET)
	public String salaryGetUrlHandler(final Model model, @ModelAttribute SalaryVo salaryVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap, HttpServletRequest request) {
		model.addAttribute(MessageConstants.SUCCESS, request.getParameter(MessageConstants.SUCCESS));
		model.addAttribute(MessageConstants.ERROR, request.getParameter(MessageConstants.ERROR));
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("salaries", salaryService.listAllSalaries());
		} catch (Exception e) {
			logger.info("", e);
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.SALRY_HOME_PAGE;
	}

	@RequestMapping(value = RequestConstants.SALARY_ADD_URL, method = RequestMethod.GET)
	public String salaryAdd(final Model model, @ModelAttribute SalaryVo salaryVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			salaryVo.setEmployee(authEmployeeDetails.getName());
			salaryVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("salaryTypes", salaryTypeService.listAllSalaryTypes());
		} catch (Exception e) {
			logger.info("", e);
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.SALRY_PAGE;
	}

	@RequestMapping(value = RequestConstants.SALARY_DELETE, method = RequestMethod.GET)
	public String salaryDelete(final Model model, @ModelAttribute SalaryVo salaryVo,
			@RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "type", required = true) String type) {
		String msg = "";
		try {
			salaryService.deleteSalary(id, type);
			msg = "?"+MessageConstants.SUCCESS+"=Salary Deleted Successfully.";
		} catch (Exception e) {
			logger.info("", e);
			msg = "?"+MessageConstants.ERROR+"=Salary Delete Failed";
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return "redirect:" + RequestConstants.SALARY_URL + msg;
	}

	/**
	 * method to save salary
	 * 
	 * @param model
	 * @param salaryVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SALARY_SAVE, method = RequestMethod.POST)
	public ModelAndView saveSalary(final Model model, @ModelAttribute SalaryVo salaryVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SALARY_URL));
		try {
			if (salaryVo.getSalaryTypeId() == 1) // monthly salary
				salaryService.saveSalaryMonthly(salaryVo);
			else if (salaryVo.getSalaryTypeId() == 2) // daily salary
				salaryService.saveSalaryDailyWage(salaryVo);
			else if (salaryVo.getSalaryTypeId() == 3) // hourly salary
				salaryService.saveSalaryHourlyWage(salaryVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return view;
	}

	/**
	 * method to calculate and return payroll result
	 * 
	 * @param model
	 * @param grossSalry
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_PAYROLL_RESULT, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getPayrollResults(final Model model,
			@RequestParam(value = "grossSalary", required = true) BigDecimal grossSalry,
			@RequestParam(value = "employeeCode", required = true) String employeeCode) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(salaryService.calculatePayrollItems(grossSalry, employeeCode));
		} catch (Exception e) {
			logger.info("", e);
		}
		return jsonResponse;
	}

	/**
	 * method to get extra payslip items
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EXTRA_PAYSLIP_ITEMS, method = RequestMethod.GET)
	public @ResponseBody List<PayslipItemAjaxVo> getExtraItems() {
		List<ExtraPaySlipItemVo> list = extraPaySlipItemService.findAllExtraPaySlipItem();
		List<PayslipItemAjaxVo> testVo = new ArrayList<PayslipItemAjaxVo>();
		for (int i = 0; i < list.size(); i++) {
			PayslipItemAjaxVo vo = new PayslipItemAjaxVo();
			// vo.setId((Long)i);
			vo.setName(list.get(i).getTitle());
			testVo.add(vo);
		}
		return testVo;
	}

	/**
	 * method to save monthly salary
	 * 
	 * @param salaryVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_MONTHLY_SALARY, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveMonthlySalary(@RequestBody SalaryVo salaryVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			salaryService.saveSalaryMonthly(salaryVo);
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
			jsonResponse.setStatus("error");
			jsonResponse.setResult(e);
		} catch (Exception e) {
			e.printStackTrace();
			jsonResponse.setStatus("error");
			jsonResponse.setResult(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to get extra payslip item calculation
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_EXTRA_PAYSLIP_ITEM_CALCULATION, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getExtraItemCalculation(@RequestParam(value = "grossSalary") String grossSalary,
			@RequestParam(value = "item") String item) {
		JsonResponse jsonResponse = new JsonResponse();
		String amount = null;
		try {
			amount = salaryService.calculateAdditionalPayrollItems(new BigDecimal(grossSalary), item);
		} catch (Exception e) {
			e.printStackTrace();
		}
		jsonResponse.setResult(amount);
		return jsonResponse;
	}
}
