package com.hrms.web.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.AttendanceService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.util.BiometricServiceUtil;
import com.hrms.web.util.ReadFromExcel;
import com.hrms.web.vo.AttendanceVo;
import com.hrms.web.vo.BiometricAttendanceVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.SessionRolesVo;

import org.slf4j.Logger;

/**
 * 
 * @author Shamsheer
 * @since 6-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class AttendanceController {

	@Log
	private static Logger logger;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private AttendanceService attendanceService;

	/**
	 * method to reach attendance home page
	 */
	@RequestMapping(value = RequestConstants.ATTENDANCE_URL, method = RequestMethod.GET)
	public String attendandeGetRequestHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo,
			@ModelAttribute BiometricAttendanceVo biometricAttendanceVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("attendance", attendanceService.listAttendanceByEmployee(employeeVo.getEmployeeId()));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.ATTENDANCE_PAGE;
	}

	/**
	 * method to reach add attendance page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_ATTENDANCE_URL, method = RequestMethod.GET)
	public String addAttendanceGet(final Model model, @ModelAttribute AttendanceVo attendanceVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			attendanceVo
					.setEmployee(authEmployeeDetails.getName() + " (" + authEmployeeDetails.getEmployeeCode() + ")");
			attendanceVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.ATTENDANCE_ADD_PAGE;
	}

	/**
	 * method to save attendance
	 * 
	 * @param model
	 * @param attendanceVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_ATTENDANCE_URL, method = RequestMethod.POST)
	public String saveAttendance(final Model model, @ModelAttribute AttendanceVo attendanceVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			if (attendanceVo.getAttendanceId() == null) {
				attendanceVo.setAttendanceId(attendanceService.saveAttendance(attendanceVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.ATTENDANCE_SAVE_SUCCESS);
			} else {
				attendanceService.updateAttendance(attendanceVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.ATTENDANCE_UPDATE_SUCCESS);
			}
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.ATTENDANCE_SAVE_FAILED);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.EDIT_ATTENDANCE + "?id=" + attendanceVo.getAttendanceId();
	}

	/**
	 * method to edit attendance
	 * 
	 * @param model
	 * @param request
	 * @param attendanceVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_ATTENDANCE, method = RequestMethod.GET)
	public String editAttendance(final Model model, HttpServletRequest request,
			@ModelAttribute AttendanceVo attendanceVo, @ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("attendanceVo",
					attendanceService.getAttendanceByAttendanceId(Long.parseLong(request.getParameter("id"))));
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.ATTENDANCE_ADD_PAGE;
	}

	/**
	 * method to delete attendance
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_ATTENDANCE, method = RequestMethod.GET)
	public String deleteAttendance(final Model model, HttpServletRequest request) {
		try {
			attendanceService.deleteAttendance(Long.parseLong(request.getParameter("id")));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.ATTENDANCE_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.ATTENDANCE_DELETE_SUCCESS;
	}

	/**
	 * method to view attendance
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_ATTENDANCE, method = RequestMethod.GET)
	public String viewAttendance(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("attendanceVo",
					attendanceService.getAttendanceByAttendanceId(Long.parseLong(request.getParameter("id"))));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.ATTENDANCE_VIEW_PAGE;
	}

	/**
	 * method to read attendance from biometric device report
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.READ_ATTENDANCE_FROM_BIOMETRIC_REPORT, method = RequestMethod.POST)
	public String readAttendanceFromBiometricDeviceReoprt(final Model model,
			@ModelAttribute BiometricAttendanceVo biometricAttendanceVo) {
		try {
			Set<BiometricAttendanceVo> attendanceVos = new  HashSet<BiometricAttendanceVo>();
			for(MultipartFile file : biometricAttendanceVo.getAttendanceReports()){
				attendanceVos.addAll(BiometricServiceUtil.readExcelFromMultipart(file));
			}
			attendanceService.saveAttendanceFromBiometric(attendanceVos);
		}catch(ItemNotFoundException e){
			logger.info(e.getMessage());
			return "redirect:" + RequestConstants.ATTENDANCE_URL+"?"+MessageConstants.ERROR+"="+e;
		} catch (IOException e) {
			logger.info(e.getMessage());
			return "redirect:" + RequestConstants.ATTENDANCE_URL+"?"+MessageConstants.ERROR+"="+MessageConstants.ATTENDANCE_SAVE_FAILED;
		} catch (Exception e) {
			logger.info(e.getMessage());
			return "redirect:" + RequestConstants.ATTENDANCE_URL+"?"+MessageConstants.ERROR+"="+MessageConstants.ATTENDANCE_SAVE_FAILED;
		}
		return "redirect:" + RequestConstants.ATTENDANCE_URL+"?"+MessageConstants.SUCCESS+"="+MessageConstants.ATTENDANCE_SAVE_SUCCESS;
	}
}
