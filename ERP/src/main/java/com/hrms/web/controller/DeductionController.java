package com.hrms.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.DeductionService;
import com.hrms.web.service.DeductionTitleService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ForwardApplicationStatusService;
import com.hrms.web.vo.BonusTitleVo;
import com.hrms.web.vo.BonusVo;
import com.hrms.web.vo.CommissionTitleVo;
import com.hrms.web.vo.DeductionTitleVo;
import com.hrms.web.vo.DeductionVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class DeductionController {

	@Log
	private Logger logger;

	@Autowired
	private DeductionService deductionService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private ForwardApplicationStatusService statusService;

	@Autowired
	private DeductionTitleService deductionTitleService;

	/**
	 * method to reach deduction home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DEDUCTION_URL, method = RequestMethod.GET)
	public String deductionHome(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			model.addAttribute("deductions",
					deductionService.listDeductionByEmployee(authEmployeeDetails.getEmployeeCode()));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.DEDUCTION_PAGE;
	}

	/**
	 * method to add new deduction
	 * 
	 * @param model
	 * @param deductionVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DEDUCTION_ADD_URL, method = RequestMethod.GET)
	public String addDeductions(final Model model, @ModelAttribute DeductionVo deductionVo,
			@ModelAttribute DeductionTitleVo deductionTitleVo, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("status", statusService.listAllStatus());
			model.addAttribute("title", deductionTitleService.listAllDeductionTitles());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.DEDUCTION_ADD_PAGE;
	}

	/**
	 * method to edit deduction
	 * 
	 * @param model
	 * @param deductionVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DEDUCTION_EDIT_URL, method = RequestMethod.GET)
	public String editDeductions(final Model model, @ModelAttribute DeductionVo deductionVo,
			@ModelAttribute DeductionTitleVo deductionTitleVo, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			model.addAttribute("deductionVo",
					deductionService.getDeductionById(Long.parseLong(request.getParameter("id"))));
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("status", statusService.listAllStatus());
			model.addAttribute("title", deductionTitleService.listAllDeductionTitles());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.DEDUCTION_ADD_PAGE;
	}

	/**
	 * method to view deduction
	 * 
	 * @param model
	 * @param request
	 * @param deductionVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DEDUCTION_VIEW_URL, method = RequestMethod.GET)
	public String viewDeduction(final Model model, HttpServletRequest request, @ModelAttribute DeductionVo deductionVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("deductionVo",
					deductionService.getDeductionById(Long.parseLong(request.getParameter("id"))));
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.DEDUCTION_VIEW_PAGE;
	}

	/**
	 * method to save deduction title
	 * 
	 * @param model
	 * @param bonusVo
	 * @param bonusTitleVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DEDUCTION_TITLE_SAVE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveDeductionTitle(@RequestBody DeductionTitleVo deductionTitleVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			deductionTitleService.saveDeductionTitle(deductionTitleVo);
			jsonResponse.setStatus("SUCCESS");
		} catch (DuplicateItemException e) {
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}
	
	/**
	 * method to get available deduction titles
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_AVAILABLE_DEDUCTION_TITLES , method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableDeductionTitles(){
		JsonResponse jsonResponse = new JsonResponse();
		try{
			jsonResponse.setResult(deductionTitleService.listAllDeductionTitles());
			jsonResponse.setStatus("SUCCESS");
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to save or update deduction
	 * 
	 * @param model
	 * @param deductionVo
	 * @param deductionTitleVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DEDUCTION_SAVE_URL, method = RequestMethod.POST)
	public String saveDeduction(final Model model, @ModelAttribute DeductionVo deductionVo,
			@ModelAttribute DeductionTitleVo deductionTitleVo) {
		try {
			if (deductionVo.getDeductionId() == null) {
				deductionVo.setDeductionId(deductionService.saveDeduction(deductionVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.DEDUCTION_SAVE_SUCCESS);
			} else {
				deductionService.updateDeduction(deductionVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.DEDUCTION_UPDATE_SUCCESS);
			}
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("title", deductionTitleService.listAllDeductionTitles());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.DEDUCTION_SAVE_FAILED);
		}
		return "redirect:" + RequestConstants.DEDUCTION_EDIT_URL + "?id=" + deductionVo.getDeductionId();
	}

	/**
	 * method to get deduction by title and employee
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_DEDUCTION_BY_TITLE_AND_EMPLOYEE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getDeductionByEmployeeAndTitle(final Model model,
			@RequestParam(value = "employeeCode", required = true) String employeeCode,
			@RequestParam(value = "titleId", required = true) Long titleId) {
		JsonResponse response = new JsonResponse();
		logger.info("////////////**************************   " + employeeCode + "   <>  " + titleId);
		try {
			response.setResult(deductionService.getDeductionByEmployeeAndTitle(titleId, employeeCode));
			response.setStatus("ok");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * method to delete deduction
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DEDUCTION_DELETE_URL, method = RequestMethod.GET)
	public String deleteDeduction(final Model model, HttpServletRequest request) {
		try {
			deductionService.deleteDeduction(Long.parseLong(request.getParameter("id")));
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.DEDUCTION_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.DEDUCTION_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.DEDUCTION_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.DEDUCTION_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.DEDUCTION_DELETE_SUCCESS;
	}
}
