package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bouncycastle.ocsp.Req;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dao.ReimbursementDao;
import com.hrms.web.dto.Superiors;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ForwardApplicationStatusService;
import com.hrms.web.service.ReimbursementCategoryService;
import com.hrms.web.service.ReimbursementsService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.ReimbursementsDocumentVo;
import com.hrms.web.vo.ReimbursementsItemsVo;
import com.hrms.web.vo.ReimbursementsVo;
import com.hrms.web.vo.SessionRolesVo;
import com.jcraft.jsch.SftpATTRS;

/**
 * 
 * @author Shamsheer
 * @since 16-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class ReimbursementsController {

	@Log
	private Logger logger;

	@Autowired
	private ReimbursementCategoryService categoryService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private ReimbursementsService reimbursementService;

	@Autowired
	private ForwardApplicationStatusService statusService;

	/**
	 * method to reach reimbursements home page
	 * 
	 * @param mode
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REIMBURSEMENTS_URL, method = RequestMethod.GET)
	public String reimbursementsHome(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);

			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("reimbursements",
					reimbursementService.findAllReimbursementsByEmployee(authEmployeeDetails.getEmployeeCode()));
			model.addAttribute("thisCode", authEmployeeDetails.getEmployeeCode());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.REIMBURSEMENTS_PAGE;
	}

	/**
	 * method to reach reimbursements add page
	 * 
	 * @param model
	 * @param reimbursementsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REIMBURSEMENTS_ADD_URL, method = RequestMethod.GET)
	public String addReimbursements(final Model model, @ModelAttribute ReimbursementsVo reimbursementsVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			List<ReimbursementsItemsVo> vos = new ArrayList<ReimbursementsItemsVo>();
			for (int i = 1; i < 5; i++) {
				ReimbursementsItemsVo vo = new ReimbursementsItemsVo();
				vos.add(vo);
			}
			reimbursementsVo.setItemsVos(vos);
			reimbursementsVo.setEmployee(authEmployeeDetails.getName()+" ("+authEmployeeDetails.getEmployeeCode()+")");
			reimbursementsVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("categories", categoryService.findAllReimbursementCategory());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.REIMBURSEMENTS_ADD_PAGE;
	}

	/**
	 * method to save reimbursements
	 * 
	 * @param model
	 * @param reimbursementsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REIMBURSEMENTS_SAVE_URL, method = RequestMethod.POST)
	public String saveReimbursements(final Model model, @ModelAttribute ReimbursementsVo reimbursementsVo,
			RedirectAttributes redirectAttributes) {
		try {
			if (reimbursementsVo.getSuperiorIds() != null) {
				List<Long> ids = new ArrayList<Long>();
				for (Long id : reimbursementsVo.getSuperiorIds()) {
					ids.add(id);
				}
				reimbursementsVo.setSuperiorIds(ids);
			}
			if (reimbursementsVo.getReimbursementId() != null) {
				reimbursementService.updateReimbursement(reimbursementsVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.REIMBURSEMENTS_UPDATE_SUCCESS);
			} else {
				reimbursementsVo.setReimbursementId(reimbursementService.saveReimbursement(reimbursementsVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.REIMBURSEMENTS_SAVE_SUCCESS);
			}
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("categories", categoryService.findAllReimbursementCategory());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.REIMBURSEMENTS_SAVE_FAILED);
		}
		return "redirect:" + RequestConstants.REIMBURSEMENTS_EDIT_URL + "?id=" + reimbursementsVo.getReimbursementId();
	}

	/**
	 * method to edit reimbursements
	 * 
	 * @param model
	 * @param reimbursementsVo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REIMBURSEMENTS_EDIT_URL, method = RequestMethod.GET)
	public String editReimbursements(final Model model,
			@ModelAttribute("reimbursementsVo") ReimbursementsVo reimbursementsVo, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);

			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);

			reimbursementsVo = reimbursementService.getReimbursementsById(Long.parseLong(request.getParameter("id")));
			Integer count = reimbursementsVo.getItemsVos().size();
			for (int i = count; i < 4; i++) {
				ReimbursementsItemsVo vo = new ReimbursementsItemsVo();
				reimbursementsVo.getItemsVos().add(vo);
			}
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("reimbursementsVo", reimbursementsVo);
			model.addAttribute("categories", categoryService.findAllReimbursementCategory());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.REIMBURSEMENTS_ADD_PAGE;
	}

	/**
	 * method to view re imbursements
	 * 
	 * @param model
	 * @param reimbursementsVo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REIMBURSEMENTS_VIEW_URL, method = RequestMethod.GET)
	public String viewReimbursements(final Model model,
			@ModelAttribute("reimbursementsVo") ReimbursementsVo reimbursementsVo, HttpServletRequest request,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("employees", employeeService.listAllEmployee());
			reimbursementsVo.setEmployee(authEmployeeDetails.getName());
			reimbursementsVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			reimbursementsVo = reimbursementService.getReimbursementsById(Long.parseLong(request.getParameter("id")));
			Boolean viewStatus = false;
			for(String code : reimbursementsVo.getSuperiorCodes()){
				if(code.equalsIgnoreCase(authEmployeeDetails.getEmployeeCode()))
					viewStatus = true;
			}
			reimbursementsVo.setViewStatus(viewStatus);
			model.addAttribute("reimbursementsVo",reimbursementsVo);
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("status", statusService.listAllStatus());
		} catch (ItemNotFoundException e) {
			model.addAttribute(MessageConstants.ERROR, e);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.REIMBURSEMENTS_VIEW_PAGE;
	}

	/**
	 * method to delete reimbursements
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REIMBURSEMENTS_DELETE_URL, method = RequestMethod.GET)
	public String deleteReimbursements(final Model model, HttpServletRequest request) {
		try {
			reimbursementService.deleteReimbursements(Long.parseLong(request.getParameter("id")));
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.REIMBURSEMENTS_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.REIMBURSEMENTS_DELETE_SUCCESS;
	}

	/**
	 * method to update status
	 * 
	 * @param model
	 * @param reimbursementsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REIMBURSEMENTS_STATUS_UPDATE, method = RequestMethod.POST)
	public String updateStatus(final Model model, @ModelAttribute("reimbursementsVo") ReimbursementsVo reimbursementsVo) {
		try {
			reimbursementService.updateStatus(reimbursementsVo);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.REIMBURSEMENTS_STATUS_FAILED;
		}
		return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.REIMBURSEMENTS_STATUS_SUCCESS;
	}

	/**
	 * method to update documents
	 * 
	 * @param model
	 * @param reimbursementsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REIMBURSEMENTS_DOCUMENT_UPDATE, method = RequestMethod.POST)
	public String updateDocuments(final Model model,
			@ModelAttribute("reimbursementsVo") ReimbursementsVo reimbursementsVo) {
		try {
			if (reimbursementsVo.getReimbursementId() != null) {
				reimbursementsVo.setDocumentVos(uploadDocuments(reimbursementsVo.getReimbursementDocuments()));
				reimbursementService.updateDocuments(reimbursementsVo);
			}
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.REIMBURSEMENTS_DOCUMENTS_FAILED;
		}
		return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.REIMBURSEMENTS_DOCUMENTS_SUCCESS;
	}

	/**
	 * method to upload documents
	 * 
	 * @param files
	 * @return
	 */
	private List<ReimbursementsDocumentVo> uploadDocuments(List<MultipartFile> files) {
		List<ReimbursementsDocumentVo> vos = new ArrayList<ReimbursementsDocumentVo>();
		for (MultipartFile file : files) {
			String url = FileUpload.uploadDocument(MessageConstants.REIMBURSEMENTS_MODULE, file);
			if (url != null) {
				ReimbursementsDocumentVo vo = new ReimbursementsDocumentVo();
				vo.setFileName(file.getOriginalFilename());
				vo.setUrl(url);
				vos.add(vo);
			}
		}
		return vos;
	}

	/**
	 * method to download document
	 * 
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = RequestConstants.DOWNLOAD_REIMBURSEMENT_DOCUMENT, method = RequestMethod.GET)
	public void downloadDocument(final Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(request.getParameter("url"), response);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
	}

	/**
	 * method to delete document
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_REIMBURSEMENT_DOCUMENT, method = RequestMethod.GET)
	public String deleteDocument(final Model model, HttpServletRequest request) {
		try {
			SFTPOperation operation = new SFTPOperation();
			Boolean result = operation.removeFileFromSFTP(request.getParameter("url"));
			if (result)
				reimbursementService.deleteDocuments(request.getParameter("url"));
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.REIMBURSEMENTS_DOCUMENTS_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.REIMBURSEMENTS_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.REIMBURSEMENTS_DOCUMENTS_DELETE_SUCCESS;
	}
}
