package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.OrganizationPolicyDocService;
import com.hrms.web.service.OrganizationPolicyService;
import com.hrms.web.service.PolicyTypeService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.OrganizationPolicyDocVo;
import com.hrms.web.vo.OrganizationPolicyDocumentsVo;
import com.hrms.web.vo.OrganizationPolicyVo;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.TerminationDocumentsVo;
import com.hrms.web.vo.TerminationVo;

/**
 * 
 * @author Jithin Mohan
 * @since 15-May-2015
 *
 */
@Controller
@SessionAttributes({ "organizationPolicyVo", "roles" })
public class OrganizationPolicyController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private OrganizationPolicyService organizationPolicyService;

	@Autowired
	private OrganizationPolicyDocService organizationPolicyDocService;

	@Autowired
	private PolicyTypeService policyTypeService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ORGANIZATION_POLICY_URL, method = RequestMethod.GET)
	public String loadOrganizationPolicy(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("organizationPolicyVo", new OrganizationPolicyVo());
			/*
			 * model.addAttribute("policies",
			 * organizationPolicyService.findAllOrganizationPolicy());
			 */
			model.addAttribute("documents", organizationPolicyDocService.findAllOrganizationPolicyDocs());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ORGANIZATION_POLICY_PAGE;
	}

	/**
	 * 
	 * @param model
	 * @param organizationPolicyVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_ORGANIZATION_POLICY_URL, method = RequestMethod.GET)
	public String saveOrganizationPolicy(final Model model,
			@ModelAttribute("organizationPolicyVo") OrganizationPolicyVo organizationPolicyVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			if (organizationPolicyVo.getOrg_policy_id() == null) {
				model.addAttribute("organizationPolicyVo", new OrganizationPolicyVo());
				model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
			} else {
				model.addAttribute("organizationPolicyVo", organizationPolicyVo);
				model.addAttribute(MessageConstants.CREATED_ON, organizationPolicyVo.getRecordedOn());
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
			}
			model.addAttribute("policyTypes", policyTypeService.findAllPolicyType());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("documents", organizationPolicyDocService.findAllOrganizationPolicyDocs());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.SAVE_ORGANIZATION_POLICY_PAGE;
	}

	/**
	 * 
	 * @param organizationPolicyVo
	 * @param result
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_ORGANIZATION_POLICY_URL, method = RequestMethod.POST)
	public ModelAndView saveOrganizationPolicyWithPost(@ModelAttribute OrganizationPolicyVo organizationPolicyVo,
			BindingResult result, RedirectAttributes attributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SAVE_ORGANIZATION_POLICY_URL));
		try {
			organizationPolicyService.saveOrUpdateOrganizationPolicy(organizationPolicyVo);
			attributes.addFlashAttribute("organizationPolicyVo", organizationPolicyVo);
			attributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_ORGANIZATION_POLICY_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			attributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * 
	 * @param organizationPolicyId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_ORGANIZATION_POLICY_URL, method = RequestMethod.GET)
	public String viewOrganizationPolicy(@RequestParam("opi") Long organizationPolicyId, final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("organizationPolicy",
					organizationPolicyService.findOrganizationPolicyById(organizationPolicyId));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.VIEW_ORGANIZATION_POLICY_PAGE;
	}

	/**
	 * 
	 * @param organizationPolicyId
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_ORGANIZATION_POLICY_URL, method = RequestMethod.GET)
	public ModelAndView editOrganizationPolicy(@RequestParam("opi") Long organizationPolicyId,
			RedirectAttributes attributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.SAVE_ORGANIZATION_POLICY_URL));
		try {
			OrganizationPolicyVo organizationPolicyVo = organizationPolicyService
					.findOrganizationPolicyById(organizationPolicyId);
			attributes.addFlashAttribute("organizationPolicyVo", organizationPolicyVo);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return view;
	}

	/**
	 * 
	 * @param organizationPolicyId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_ORGANIZATION_POLICY_URL, method = RequestMethod.POST)
	public String deleteOrganizationPolicy(@RequestParam("opi") Long organizationPolicyId) {
		try {
			organizationPolicyService.deleteOrganizationPolicy(organizationPolicyId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.ORGANIZATION_POLICY_LIST_URL;
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ORGANIZATION_POLICY_LIST_URL, method = RequestMethod.GET)
	public String listAllOrganizationPolicy(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("policies", organizationPolicyService.findAllOrganizationPolicy());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ORGANIZATION_POLICY_LIST_PAGE;
	}

	/**
	 * 
	 * @param organizationPolicyVo
	 * @param organizationPolicyId
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ORGANIZATION_POLICY_DOC_SAVE_URL, method = RequestMethod.POST)
	public @ResponseBody ModelAndView saveOrgPolicyDocumentsWithPost(
			@ModelAttribute OrganizationPolicyDocVo organizationPolicyVo, MultipartHttpServletRequest request) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.EMPLOYEE_TERMINATION_PAGE_URL));
		try {
			List<OrganizationPolicyDocVo> documentsVos = uploadOrgPolicyDocuments(request);
			if (!documentsVos.isEmpty()) {
				// organizationPolicyVo.setOrg_policy_id(organizationPolicyId);
				// organizationPolicyVo.setDocuments(documentsVos);
				for (OrganizationPolicyDocVo docVo : documentsVos) {
					System.out.println("-------- " + docVo.getDocumentUrl());
					organizationPolicyDocService.saveOrUpdateOrganizationPolicyDoc(docVo);
				}
				// organizationPolicyService.saveOrganizationPolicyDocument(organizationPolicyVo);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return view;
	}

	/**
	 * 
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = RequestConstants.VIEW_POLICY_DOCUMENT, method = RequestMethod.GET)
	public void viewDocumentGet(final Model model, HttpServletRequest request, HttpServletResponse response) {
		String url = request.getParameter("durl");
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			sftpOperation.downloadFileFromSFTP(url, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	private List<OrganizationPolicyDocVo> uploadOrgPolicyDocuments(MultipartHttpServletRequest request) {
		List<OrganizationPolicyDocVo> documentsVos = new ArrayList<OrganizationPolicyDocVo>();
		try {
			Iterator<String> itrator = request.getFileNames();
			while (itrator.hasNext()) {
				MultipartFile multipartFile = request.getFile(itrator.next());
				if (!multipartFile.isEmpty()) {
					String fileAccesUrl = FileUpload.uploadDocument(MessageConstants.ORGANIZATION_POLICY_MODULE,
							multipartFile);
					if (fileAccesUrl != null) {
						OrganizationPolicyDocVo documentVo = new OrganizationPolicyDocVo();
						documentVo.setDocumentUrl(fileAccesUrl);
						documentVo.setDocumentName(multipartFile.getOriginalFilename());
						documentsVos.add(documentVo);
					}
				}

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return documentsVos;
	}

	/**
	 * 
	 * @param model
	 * @param organizationPolicyId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ORGANIZATION_POLICY_DOC_LIST_URL, method = RequestMethod.GET)
	public String readAllOrgPolicyDocumentsWithPost(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("documents", organizationPolicyDocService.findAllOrganizationPolicyDocs());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ORGANIZATION_POLICY_DOC_LIST_PAGE;
	}

	/**
	 * 
	 * @param model
	 * @param documentId
	 * @param documentUrl
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ORGANIZATION_POLICY_DOC_DELETE_URL, method = RequestMethod.POST)
	public String deleteOrganizationPolicyDocument(final Model model,
			@RequestParam(value = "documentId", required = true) Long documentId,
			@RequestParam(value = "documentUrl", required = true) String documentUrl,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			if (sftpOperation.removeFileFromSFTP(documentUrl))
				organizationPolicyDocService.deleteOrganizationPolicyDoc(documentId);
			// organizationPolicyService.deleteOrganizationPolicyDocument(documentId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ORGANIZATION_POLICY_DOC_LIST_PAGE;
	}
}
