package com.hrms.web.controller;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.TaxRuleService;
import com.hrms.web.vo.TaxRuleVo;

/**
 * 
 * @author Jithin Mohan
 * @since 28-April-2015
 *
 */
@Controller
public class TaxRuleController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private TaxRuleService taxRuleService;

	/**
	 * method to get tax rules by salaryTo
	 * 
	 * @param salaryTo
	 * @return jsonResponse
	 */
	@RequestMapping(value = RequestConstants.GET_TAX_WITH_SALARY_TO_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAdditionalTaxAmountFromSalary(
			@RequestParam(value = "salaryTo", required = true) Long salaryTo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			TaxRuleVo taxRuleVo = taxRuleService.findTaxRuleVoBySalaryTo(new BigDecimal(salaryTo));
			if (taxRuleVo == null) {
				taxRuleVo = new TaxRuleVo();
				taxRuleVo.setAdditionalAmount(new BigDecimal("0"));
			}
			jsonResponse.setResult(taxRuleVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to list all tax rule items
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYROLL_TAXRULE_ITEM_LIST_URL, method = RequestMethod.GET)
	public String listAllPaySlipItemList(final Model model) {
		try {
			model.addAttribute("taxRules", taxRuleService.findAllTaxRule());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_PAYROLL_TAXRULE_ITEM_LIST_PAGE;
	}

	/**
	 * method to save tax rule item
	 * 
	 * @param taxRuleVo
	 */
	@RequestMapping(value = RequestConstants.SAVE_TAX_RULE_ITEM_URL, method = RequestMethod.POST)
	public String savetaxRuleItemWithPost(@RequestBody TaxRuleVo taxRuleVo) {
		try {
			BigDecimal percent = new BigDecimal(0);
			if (!taxRuleVo.getTaxPercentage().equals(0)) {
				BigDecimal diff = taxRuleVo.getSalaryTo().subtract(taxRuleVo.getSalaryFrom());
				percent = (diff.multiply(taxRuleVo.getTaxPercentage())).divide(new BigDecimal("100"));
			}
			taxRuleVo.setRowTax(percent);
			taxRuleService.saveOrUpdateTaxRule(taxRuleVo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.PAYROLL_TAXRULE_ITEM_LIST_URL;
	}
}
