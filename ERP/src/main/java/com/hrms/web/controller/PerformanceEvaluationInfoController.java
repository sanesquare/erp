package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.AnnouncementsService;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.PerformanceEvaluationInfoService;
import com.hrms.web.service.PerformanceEvaluationService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.AnnouncementsVo;
import com.hrms.web.vo.IndividualEvaluationVo;
import com.hrms.web.vo.PerformanceEvaluationQuestionVo;
import com.hrms.web.vo.PerformanceEvaluationVo;
import com.hrms.web.vo.PerformanceVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Jithin Mohan
 * @since 24-March-2015
 *
 */
@Controller
@SessionAttributes({ "evaluationVo" , "roles"})
public class PerformanceEvaluationInfoController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private PerformanceEvaluationInfoService evaluationService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private AnnouncementsService announcementsService;

	/**
	 * method to load all performance evaluation
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PERFORMANCE_EVALUATION_PAGE_URL, method = RequestMethod.GET)
	public String loadPerformanceEvaluationList(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("evaluationList", evaluationService.listPerformanceEvaluations());
			model.addAttribute("evaluationVo", new PerformanceEvaluationVo());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.PERFORMANCE_EVALUATION_PAGE;
	}

	/**
	 * method to load performance evaluation create page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PERFORMANCE_EVALUATION_CREATE_PAGE_URL, method = RequestMethod.GET)
	public String loadPerformanceEvaluationCreatePage(final Model model,
			@ModelAttribute("performVo") PerformanceVo performVo, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		if (performVo.getEvaluationId() == null) {
			model.addAttribute("performVo", new PerformanceVo());
			//model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
		} else {
			//model.addAttribute(MessageConstants.CREATED_ON, performVo.getCreatedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
			model.addAttribute("performVo", performVo);
		}
		return PageNameConstatnts.PERFORMANCE_EVALUATION_CREATE_PAGE;
	}

	/**
	 * method to save or update performance evaluation
	 * 
	 * @param evaluationVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PERFORMANCE_EVALUATION_CREATE_PAGE_URL, method = RequestMethod.POST)
	public ModelAndView saveOrUpdatePerformanceEvaluationWithPost(@ModelAttribute PerformanceVo performanceVo,
			RedirectAttributes redirectAttributes) {
		Long id = 0L;
		ModelAndView view = new ModelAndView();
		try {
			if(performanceVo.getEvaluationId()==null)
			{
				id = evaluationService.savePerformanceEvaluation(performanceVo);
				view = new ModelAndView(new RedirectView(RequestConstants.PERFORMANCE_EVALUATION_EDIT_PAGE_URL+"?otpId="+id));
			}
			else
			{
				evaluationService.updatePerformanceEvaluation(performanceVo);
				view = new ModelAndView(new RedirectView(RequestConstants.PERFORMANCE_EVALUATION_EDIT_PAGE_URL+"?otpId="+performanceVo.getEvaluationId()));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}
	/**
	 * Method to edit evaluation get
	 * @param evaluationId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PERFORMANCE_EVALUATION_EDIT_PAGE_URL, method = RequestMethod.GET)
	public String editPerformanceEvaluationWithGet(@RequestParam(value = "otpId", required = true) Long evaluationId, final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap); 
		try {
			PerformanceVo performance = evaluationService.findEvaluationById(evaluationId);
			model.addAttribute("performVo" , performance);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.PERFORMANCE_EVALUATION_CREATE_PAGE;
	}
	/**
	 * Method to edit evaluation post
	 * @param evaluationId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PERFORMANCE_EVALUATION_EDIT_PAGE_URL, method = RequestMethod.POST)
	public String editPerformanceEvaluationWithPost(@RequestParam(value = "otpId", required = true) Long evaluationId, final Model model , @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap) ;
		try {
			PerformanceVo performance = evaluationService.findEvaluationById(evaluationId);
			model.addAttribute("performVo" , performance);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.PERFORMANCE_EVALUATION_CREATE_PAGE;
	}
	/**
	 * Method to get employee performance details
	 * @param performanceVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PERFORMANCE_EVALUATION_GET_DETAILS, method = RequestMethod.POST ,
			headers={"Content-type=application/json"})
	public  @ResponseBody JsonResponse getEmployeePerformance(@RequestBody PerformanceVo performanceVo , HttpSession session) {
		JsonResponse jsonResponse=new JsonResponse();
		try {
			IndividualEvaluationVo evaluationVo = evaluationService.finddEmployeeEvaluationInfo(performanceVo.getEmployeeCode(), performanceVo.getEvaluationId());
			jsonResponse.setResult(evaluationVo);
			jsonResponse.setStatus("SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			jsonResponse.setStatus("ERROR");
		}
		return jsonResponse;
	}
	/**
	 * Method to view performance evaluation
	 * @param evaluationId
	 * @param model
	 * @param rolesMap
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PERFORMANCE_EVALUATION_VIEW_PAGE_URL, method = RequestMethod.GET)
	public String viewPerformanceEvaluationWithGet(@RequestParam(value = "otp", required = true) Long evaluationId,
			final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("performanceEvaluation", evaluationService.findEvaluationById(evaluationId));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.PERFORMANCE_EVALUATION_VIEW_PAGE;
	}
	/**
	 * Method to delete performance evaluation
	 * @param evaluationId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PERFORMANCE_EVALUATION_DELETE_URL, method = RequestMethod.POST)
	public String deletePerformanceEvaluation(@RequestParam(value = "evaluationId", required = true) Long evaluationId) {
		try {
			evaluationService.deletePerformanceEvaluation(evaluationId);
		} catch (ItemNotFoundException ine) {
			ine.printStackTrace();
			logger.error(ine.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.PERFORMANCE_EVALUATION_LIST_URL;
	}

	/**
	 * list all performance evaluations
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PERFORMANCE_EVALUATION_LIST_URL, method = RequestMethod.GET)
	public String listAllPerformanceEvaluations(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("evaluationList", evaluationService.listPerformanceEvaluations());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.PERFORMANCE_EVALUATION_LIST_PAGE;
	}
	/**
	 * Method to delete employee performance detail
	 * @param evaluationId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PERFORMANCE_EMPLOYEE_DETAIL_DELETE, method = RequestMethod.POST)
	public String deleteEmployeePerformanceEvaluation(@RequestParam(value = "evaluationId", required = true) Long evaluationId , 
			@RequestParam(value = "individualId", required = true) Long individualId) {
		try {
			evaluationService.deleteEmployeeEvaluation(evaluationId, individualId);
		} catch (ItemNotFoundException ine) {
			ine.printStackTrace();
			logger.error(ine.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.PERFORMANCE_EVALUATION_LIST_URL;
	}
}
