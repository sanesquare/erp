package com.hrms.web.controller;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.ReminderService;
import com.hrms.web.service.ReminderStatusService;
import com.hrms.web.vo.ReminderTabVo;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
@Controller
public class ReminderController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private ReminderService reminderService;

	@Autowired
	private ReminderStatusService reminderStatusService;

	/**
	 * method to list all the reminders
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LIST_ALL_REMINDER_URL, method = RequestMethod.GET)
	public String listAllReminders(final Model model) {
		try {
			model.addAttribute("reminderList", reminderService.findAllReminder());
			model.addAttribute("reminderStatusList", reminderStatusService.findAllReminderStatus());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.REMINDER_LIST_PAGE;
	}

	/**
	 * method to delete reminder
	 * 
	 * @param reminderId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_REMINDER_URL, method = RequestMethod.POST)
	public String deleteReminderWithPost(@RequestParam(value = "reminderId", required = true) Long reminderId) {
		try {
			reminderService.deleteReminder(reminderId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.LIST_ALL_REMINDER_URL;
	}
}
