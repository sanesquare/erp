package com.hrms.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.PayRollConstants;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.entities.Branch;
import com.hrms.web.entities.Employee;
import com.hrms.web.form.BankStatementForm;
import com.hrms.web.form.EmpAssigmentsReportForm;
import com.hrms.web.form.EmpJoinReportForm;
import com.hrms.web.form.EmpPerformanceForm;
import com.hrms.web.form.EmpResignReportForm;
import com.hrms.web.form.EmpSummaryForm;
import com.hrms.web.form.EmpTransfersReportForm;
import com.hrms.web.form.EmpTravelReportForm;
import com.hrms.web.form.HREmpReportForm;
import com.hrms.web.form.HrBranchForm;
import com.hrms.web.form.PayAdjstmntForm;
import com.hrms.web.form.PayAdvSalaryForm;
import com.hrms.web.form.PayBonusForm;
import com.hrms.web.form.PayCommisionForm;
import com.hrms.web.form.PayDailyWagesForm;
import com.hrms.web.form.PayDeductionsForm;
import com.hrms.web.form.PayEsciForm;
import com.hrms.web.form.PayHourlyWagesForm;
import com.hrms.web.form.PayLoansForm;
import com.hrms.web.form.PayOTForm;
import com.hrms.web.form.PayPFForm;
import com.hrms.web.form.PayPaySlipReportForm;
import com.hrms.web.form.PayRembOTForm;
import com.hrms.web.form.PaySalaryReportForm;
import com.hrms.web.form.PaySalarySplitForm;
import com.hrms.web.form.TimeAbsentEmployeesForm;
import com.hrms.web.form.TimeEmpLeaveSummaryForm;
import com.hrms.web.form.TimeEmpTimesheetReportForm;
import com.hrms.web.form.TimeEmpWorkReportForm;
import com.hrms.web.form.TimeWorkSheetReportForm;
import com.hrms.web.form.TimeWorkShiftForm;
import com.hrms.web.logger.Log;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeCategoryService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.EmployeeStatusService;
import com.hrms.web.service.EmployeeTypeService;
import com.hrms.web.service.ExtraPaySlipItemService;
import com.hrms.web.service.JobFieldService;
import com.hrms.web.service.JobPostService;
import com.hrms.web.service.PaySlipItemService;
import com.hrms.web.service.ProjectService;
import com.hrms.web.service.ReportService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.ReadFromExcel;
import com.hrms.web.vo.BranchVo;
import com.hrms.web.vo.CustomPdfDetailVo;
import com.hrms.web.vo.EmpPrimaryReportVo;
import com.hrms.web.vo.EmployeeByBranchVo;
import com.hrms.web.vo.EmployeeByDepartmentsVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.ExtraPaySlipItemVo;
import com.hrms.web.vo.HrBranchVo;
import com.hrms.web.vo.HrEmpReportViewVo;
import com.hrms.web.vo.HrEmpReportVo;
import com.hrms.web.vo.HrHolidayVo;
import com.hrms.web.vo.HrHolidaysViewVo;
import com.hrms.web.vo.HrLeavesViewVo;
import com.hrms.web.vo.HrLeavesVo;
import com.hrms.web.vo.HrProEmployeeViewVo;
import com.hrms.web.vo.HrSummaryViewVo;
import com.hrms.web.vo.HrSummaryVo;
import com.hrms.web.vo.JobCandidateReportVo;
import com.hrms.web.vo.JobInterviewReportVo;
import com.hrms.web.vo.JobPostReportVo;
import com.hrms.web.vo.LeavesVo;
import com.hrms.web.vo.PayPaySlipDetailsVo;
import com.hrms.web.vo.PayPrimaryReportVo;
import com.hrms.web.vo.PaySlipItemVo;
import com.hrms.web.vo.RecCandReportViewVo;
import com.hrms.web.vo.RecInterviewReportViewVo;
import com.hrms.web.vo.RecPostReportViewVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.TimeEmpWorkshiftDetailsVo;
import com.hrms.web.vo.TimeReportPrimaryVo;

/**
 * 
 * @author Vinutha
 * @since 11 April 2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class ReportController {

	@Log
	private Logger logger;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private EmployeeTypeService empTypeService;

	@Autowired
	private EmployeeCategoryService empCatService;

	@Autowired
	private EmployeeStatusService empStatusService;

	@Autowired
	private ReportService reportService;

	@Autowired
	private EmployeeService empService;

	@Autowired
	private JobFieldService fieldService;

	@Autowired
	private JobPostService postService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private ServletContext context;

	@Autowired
	private PaySlipItemService payItemService;

	@Autowired
	private ExtraPaySlipItemService extraPaySlipItemService;

	public static int employeeVoSize;

	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		dataBinder.setAutoGrowCollectionLimit(employeeVoSize);
	}

	/**
	 * Method to handle request for Hr reports home page
	 * 
	 * @param model
	 * @param hrEmpReportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REPORTS_HR_HOME_URL, method = RequestMethod.GET)
	public String reportPageHandler(final Model model, @ModelAttribute HrEmpReportVo hrEmpReportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartmentDetails());
			model.addAttribute("types", empTypeService.findAllEmployeeType());
			model.addAttribute("categories", empCatService.findAllEmployeeCategory());
			model.addAttribute("status", empStatusService.listAllStatus());
			model.addAttribute("holidayVo", new HrHolidayVo());
			model.addAttribute("hrLeavesVo", new HrLeavesVo());
			model.addAttribute("hrBranchVo", new HrBranchVo());
			model.addAttribute("years", reportService.getYears());
			model.addAttribute("employees", empService.listAllEmployee());
		} catch (Exception e) {
			logger.info("REPORTS_HR_HOME_URL : ",e);
		}
		return PageNameConstatnts.REPORTS_HOME_PAGE;
	}

	/**
	 * Method to handle request for generate employee report
	 * 
	 * @param model
	 * @param hrEmpReportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DETAILED_EMP_REPORT_VIEW, method = RequestMethod.POST)
	public String viewHrEmpReportPageHandler(final Model model, @ModelAttribute HrEmpReportVo hrEmpReportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			HREmpReportForm empReportForm = new HREmpReportForm();
			empReportForm.setDetailedVos(reportService.createDetailedEmployeeReport(hrEmpReportVo));
			model.addAttribute("employeeDetails", empReportForm);
			employeeVoSize = empReportForm.getDetailedVos().size() + 1;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("viewHrEmpReportPageHandler : ",e);
		}
		return PageNameConstatnts.DETAILED_EMPLOYEE_REPORT;
	}

	@RequestMapping(value = RequestConstants.DETAILED_EMP_REPORT_VIEW, method = RequestMethod.GET)
	public String viewDetailedEmployeeReport(final Model model, @ModelAttribute HrEmpReportVo hrEmpReportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
		} catch (Exception e) {
			logger.info("DETAILED_EMP_REPORT_VIEW : ",e);
		}
		return PageNameConstatnts.DETAILED_EMPLOYEE_REPORT;
	}

	/**
	 * Method to handle request for export of employee list report
	 * 
	 * @param map
	 * @param employeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PDF_DWNLOAD_URL)
	public String employeeReportExport(ModelMap map,
			@ModelAttribute("employeeDetails") HREmpReportForm employeeDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(employeeDetails.getEmpReportVos());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = employeeDetails.getType();
			if ("pdf".equals(type)) {
				return "pdfReport";
			} else if ("xls".equals(type)) {
				return "xlsReport";
			} else if ("csv".equals(type)) {
				return "csvReport";
			} else if ("html".equals(type)) {
				return "htmlReport";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("", e);
		}
		return "redirect:" + RequestConstants.DETAILED_EMP_REPORT_VIEW;
	}

	/**
	 * Method to handle request for HR Summary view
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_HR_SUMMARY, method = RequestMethod.POST)
	public String viewHrSummaryReport(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			HrSummaryViewVo resultVo = reportService.createHrSummary();
			model.addAttribute("summaryDetails", resultVo);
			employeeVoSize = resultVo.getAgeGroupDetails().size() + resultVo.getBranchDetails().size()
					+ resultVo.getDepartmentDetails().size() + resultVo.getDesignationDetails().size();
		} catch (Exception e) {
			logger.info("viewHrSummaryReport : ", e);
		}
		return PageNameConstatnts.HR_SUMMARY_VIEW;
	}

	/**
	 * Method to handle request for exporting Hr Summary
	 * 
	 * @param map
	 * @param summaryDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EXPORT_HR_SUMMARY)
	public String hrSummaryExport(ModelMap map, @ModelAttribute("summaryDetails") HrSummaryViewVo summaryDetails) {

		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			List<HrSummaryVo> summaryList = new ArrayList<HrSummaryVo>();
			summaryList.add(summaryDetails.getSummaryDetails());

			List<EmployeeByBranchVo> branchDetails = summaryDetails.getBranchDetails();
			List<EmployeeByDepartmentsVo> depDetails = summaryDetails.getDepartmentDetails();
			JRDataSource JRdataSource1 = new JRBeanCollectionDataSource(branchDetails);

			JRDataSource JRdataSource2 = new JRBeanCollectionDataSource(depDetails);
			// JRDataSource JRdataSource2 = new
			// JRBeanCollectionDataSource(summaryDetails.getDesignationDetails());
			// JRDataSource JRdataSource3 = new
			// JRBeanCollectionDataSource(summaryDetails.getAgeGroupDetails());
			// parameterMap.put("branchCount",10);
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(summaryList);
			parameterMap.put("datasource", JRdataSource);
			parameterMap.put("JasperCustomSubReportDataSource", JRdataSource1);
			parameterMap.put("JasperCustomSubReportDataSource1", JRdataSource2);

			map.addAllAttributes(parameterMap);
			String type = summaryDetails.getType();

			if ("pdf".equals(type)) {
				return "hrSummPdf";

			} else if ("xls".equals(type)) {
				return "hrSummXls";
			} else if ("csv".equals(type)) {
				return "hrSummCsv";
			} else if ("html".equals(type)) {
				return "hrSummHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("hrSummaryExport", e);
		}
		return "redirect:" + RequestConstants.VIEW_HR_SUMMARY;
	}

	/**
	 * Method to handle request for viewing pay roll summary
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_PAYROLL_SUMMARY, method = RequestMethod.POST)
	public String viewPayrollSummaryReport(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {

		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAYROLL_SUMMARY_VIEW;
	}

	/**
	 * Method to handle request for generating holiday report
	 * 
	 * @param model
	 * @param holidayVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GENERATE_HOLIDAY_REPORT, method = RequestMethod.POST)
	public String viewHrHolidayReportPageHandler(final Model model, @ModelAttribute HrHolidayVo holidayVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			HrHolidaysViewVo result = reportService.createHolidaysReport(holidayVo);
			model.addAttribute("holidays", result);
			employeeVoSize = result.getDetailsVo().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.HOLIDAYS_REPORT_VIEW;
	}

	/**
	 * Method to handle request for export holiday report
	 * 
	 * @param map
	 * @param leaves
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HR_EXP_HOLIDAYS_REPORT, method = RequestMethod.POST)
	public String exportHrHolidaysReport(ModelMap map, @ModelAttribute("holidays") HrHolidaysViewVo holidays) {

		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(holidays.getDetailsVo());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = holidays.getType();

			if ("pdf".equals(type)) {
				return "hrHolPdf";
			} else if ("xls".equals(type)) {
				return "hrHolXls";
			} else if ("csv".equals(type)) {
				return "hrHolCsv";
			} else if ("html".equals(type)) {
				return "hrHolHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportHrHolidaysReport", e);
		}
		return "redirect:" + RequestConstants.GENERATE_HOLIDAY_REPORT;
	}

	/**
	 * Method to handle request for generating Leaves reports
	 * 
	 * @param model
	 * @param hrLeavesVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GENERATE_LEAVES_REPORT, method = RequestMethod.POST)
	public String viewHrLeavesReportPageHandler(final Model model, @ModelAttribute HrLeavesVo hrLeavesVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			HrLeavesViewVo result = new HrLeavesViewVo();
			result = reportService.createLeavesReport(hrLeavesVo);
			model.addAttribute("leaves", result);
			employeeVoSize = result.getDetailsVo().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.LEAVES_REPORT_VIEW;
	}

	/**
	 * Method to handle request for export leave report
	 * 
	 * @param map
	 * @param leaves
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HR_EXP_LEAVE_REPORT, method = RequestMethod.POST)
	public String exportHrLeaveReport(ModelMap map, @ModelAttribute("leaves") HrLeavesViewVo leaves) {

		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(leaves.getDetailsVo());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = leaves.getType();

			if ("pdf".equals(type)) {
				return "hrLeavePdf";
			} else if ("xls".equals(type)) {
				return "hrLeaveXls";
			} else if ("csv".equals(type)) {
				return "hrLeaveCsv";
			} else if ("html".equals(type)) {
				return "hrLeaveHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportHrLeaveReport", e);
		}
		return "redirect:" + RequestConstants.GENERATE_LEAVES_REPORT;
	}

	/**
	 * Method to handle request for project employee report view
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GENERATE_PRO_EMP_REPORT_URL, method = RequestMethod.POST)
	public String viewHrProEmpReportPageHandler(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			HrProEmployeeViewVo result = reportService.createProjectEmployeeReport();
			model.addAttribute("hrProjectDetails", result);
			employeeVoSize = result.getDetailsVo().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PROJECT_EMP_REPORT_PAGE;
	}

	/**
	 * Method to handle request for export Hr project employee report
	 * 
	 * @param map
	 * @param hrProjectDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HR_EXP_PROJECTS_REPORT, method = RequestMethod.POST)
	public String exportHrProjectsReport(ModelMap map,
			@ModelAttribute("projects") HrProEmployeeViewVo hrProjectDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(hrProjectDetails.getDetailsVo());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = hrProjectDetails.getType();

			if ("pdf".equals(type)) {
				return "hrProPdf";
			} else if ("xls".equals(type)) {
				return "hrProXls";
			} else if ("csv".equals(type)) {
				return "hrProCsv";
			} else if ("html".equals(type)) {
				return "hrProHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportHrProjectsReport", e);
		}
		return "redirect:" + RequestConstants.GENERATE_PRO_EMP_REPORT_URL;
	}

	/**
	 * Method to generate Hr Branch Report
	 * 
	 * @param model
	 * @param hrBranchVo
	 * @param rolesMap
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GENERATE_BRANCH_REPORT, method = RequestMethod.POST)
	public String viewHrBranchReportPageHandler(final Model model, @ModelAttribute HrBranchVo hrBranchVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			HrBranchForm result = new HrBranchForm();
			result = reportService.createHrBranchReport(hrBranchVo);
			model.addAttribute("branches", result);
			employeeVoSize = result.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.BRANCH_REPORT;
	}

	/**
	 * Method to export Hr Branch report
	 * 
	 * @param map
	 * @param branches
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HR_EXP_BRANCH_REPORT, method = RequestMethod.POST)
	public String exportHrBranchReport(ModelMap map, @ModelAttribute("branches") HrBranchForm branches) {

		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(branches.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = branches.getType();

			if ("pdf".equals(type)) {
				return "hrBranchPdf";
			} else if ("xls".equals(type)) {
				return "hrBranchXls";
			} else if ("csv".equals(type)) {
				return "hrBranchCsv";
			} else if ("html".equals(type)) {
				return "hrBranchHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportHrBranchReport", e);
		}
		return "redirect:" + RequestConstants.GENERATE_BRANCH_REPORT;
	}

	/*************************************************
	 * RECRUITMENT REPORTS
	 *************************************************************/

	/**
	 * Method to handle request for recruitment reports home page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REC_REPORT_HOME_URL, method = RequestMethod.GET)
	public String recReportPageHandler(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartmentDetails());
			model.addAttribute("jobFields", fieldService.findAllJobField());
			model.addAttribute("posts", postService.listAllJobPosts());
			model.addAttribute("recPostsVo", new JobPostReportVo());
			model.addAttribute("recCandVo", new JobCandidateReportVo());
			model.addAttribute("recIntVo", new JobInterviewReportVo());

		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.REC_REPORT_HOME;
	}

	/**
	 * Method to handle request for viewing job post report
	 * 
	 * @param model
	 * @param recPostsVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_REC_POST_REPORT, method = RequestMethod.POST)
	public String viewRecPostReportPageHandler(final Model model, @ModelAttribute JobPostReportVo recPostsVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			RecPostReportViewVo result = reportService.createPostReport(recPostsVo);
			model.addAttribute("recPostDetails", result);
			employeeVoSize = result.getDetailsVo().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.REC_POST_REPORT;
	}

	/**
	 * Method for handling request for exporting job posts report
	 * 
	 * @param map
	 * @param employeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REC_EXP_POST_REPORT)
	public String recruitmentPostsReportExport(ModelMap map,
			@ModelAttribute("recPostDetails") RecPostReportViewVo recPostDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(recPostDetails.getDetailsVo());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = recPostDetails.getType();
			if ("pdf".equals(type)) {
				return "recJobPdf";
			} else if ("xls".equals(type)) {
				return "recJobXls";
			} else if ("csv".equals(type)) {
				return "recJobCsv";
			} else if ("html".equals(type)) {
				return "recJobHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("recruitmentPostsReportExport", e);
		}
		return "redirect:" + RequestConstants.VIEW_REC_POST_REPORT;
	}

	/**
	 * Method to handle request for viewing job candidate report
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_REC_CAND_REPORT, method = RequestMethod.POST)
	public String viewRecCandReportPageHandler(final Model model, @ModelAttribute JobCandidateReportVo recCandVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			RecCandReportViewVo result = reportService.createCandReport(recCandVo);
			model.addAttribute("recCandDetails", result);
			employeeVoSize = result.getDetailsVo().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.REC_CANDIDATE_REPORT;
	}

	/**
	 * Method to handle request for export of candidates report
	 * 
	 * @param map
	 * @param recCandDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REC_EXP_CAND_REPORT)
	public String recruitmentCandidatesReportExport(ModelMap map,
			@ModelAttribute("recCandDetails") RecCandReportViewVo recCandDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(recCandDetails.getDetailsVo());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = recCandDetails.getType();
			if ("pdf".equals(type)) {
				return "recCandPdf";
			} else if ("xls".equals(type)) {
				return "recCandXls";
			} else if ("csv".equals(type)) {
				return "recCandCsv";
			} else if ("html".equals(type)) {
				return "recCandHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("recruitmentCandidatesReportExport", e);
		}
		return "redirect:" + RequestConstants.VIEW_REC_CAND_REPORT;
	}

	/**
	 * Method to handle request for viewing job interview reports
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_REC_INTERVIEW_REPORT, method = RequestMethod.POST)
	public String viewRecIntReportPageHandler(final Model model, @ModelAttribute JobInterviewReportVo recIntVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			RecInterviewReportViewVo result = reportService.createInterviewReport(recIntVo);
			model.addAttribute("recIntDetails", result);
			employeeVoSize = result.getDetailsVo().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.REC_INTERVIEW_REPORT;
	}

	/**
	 * Method to hndle request for interview report export
	 * 
	 * @param map
	 * @param recIntDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.REC_EXP_INT_REPORT)
	public String recruitmentInterviewReportExport(ModelMap map,
			@ModelAttribute("recIntDetails") RecInterviewReportViewVo recIntDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(recIntDetails.getDetailsVo());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = recIntDetails.getType();
			if ("pdf".equals(type)) {
				return "recIntPdf";
			} else if ("xls".equals(type)) {
				return "recIntXls";
			} else if ("csv".equals(type)) {
				return "recIntCsv";
			} else if ("html".equals(type)) {
				return "recIntHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("recruitmentInterviewReportExport", e);
		}
		return "redirect:" + RequestConstants.VIEW_REC_INTERVIEW_REPORT;
	}

	/********************************************
	 * EMPLOYEE REPORTS
	 ****************************************************************************/
	/**
	 * Method to handle request for employee reports home page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMP_REPORTS_HOME_URL, method = RequestMethod.GET)
	public String empReportPageHandler(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {

			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartmentDetails());
			model.addAttribute("projects", projectService.listAllProjects());
			model.addAttribute("empReportVo", new EmpPrimaryReportVo());

		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.EMP_REPORTS_HOME_PAGE;
	}

	/**
	 * Method to handle request for emp summary
	 * 
	 * @param model
	 * @param recIntVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMP_SUMMARY_URL, method = RequestMethod.POST)
	public String viewEmpSummaryPageHandler(final Model model, @ModelAttribute EmpPrimaryReportVo empReportVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			empReportVo.setEmployee(authEmployeeDetails.getName());
			empReportVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			EmpSummaryForm form = reportService.createEmployeeSummaryReport(empReportVo);
			model.addAttribute("employee", form);
			employeeVoSize = form.getLeaveDetails().size() + form.getPromoDetails().size()
					+ form.getTransferDetails().size() + form.getWarnDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.EMP_SUMMARY_PAGE;
	}

	/**
	 * Method to handle request for employee join report
	 * 
	 * @param model
	 * @param empJoiningVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_JOIN_REPORT, method = RequestMethod.POST)
	public String viewEmpJoinPageHandler(final Model model, @ModelAttribute EmpPrimaryReportVo empReportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			EmpJoinReportForm form = reportService.createJoiningReport(empReportVo);
			model.addAttribute("joiningDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.EMP_JOINING_REPORT_PAGE;
	}

	@RequestMapping(value = RequestConstants.EMP_EXP_JOIN, method = RequestMethod.POST)
	public String employeeJOinReportExport(ModelMap map,
			@ModelAttribute("joiningDetails") EmpJoinReportForm joiningDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(joiningDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = joiningDetails.getType();
			if ("pdf".equals(type)) {
				return "empJoinPdf";
			} else if ("xls".equals(type)) {
				return "empJoinXls";
			} else if ("csv".equals(type)) {
				return "empJoinCsv";
			} else if ("html".equals(type)) {
				return "empJoinHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("employeeJOinReportExport", e);
		}
		return "redirect:" + RequestConstants.VIEW_JOIN_REPORT;
	}

	/**
	 * Method to handle request for project - employee report
	 * 
	 * @param model
	 * @param viewEmpProReport
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_PROJECT_REPORT, method = RequestMethod.POST)
	public String viewEmpProjectPageHandler(final Model model, @ModelAttribute EmpPrimaryReportVo empReportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			HrProEmployeeViewVo result = reportService.createProjectEmployeeReportWithId(empReportVo.getProjectId());
			model.addAttribute("empProjectDetails", result);
			employeeVoSize = result.getDetailsVo().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.EMP_PROJECT_REPORT_PAGE;
	}

	/**
	 * Method to export employee project report
	 * 
	 * @param map
	 * @param empProjectDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMP_EXP_PROJECT, method = RequestMethod.POST)
	public String exportHrHolidaysReport(ModelMap map,
			@ModelAttribute("empProjectDetails") HrProEmployeeViewVo empProjectDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(empProjectDetails.getDetailsVo());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = empProjectDetails.getType();

			if ("pdf".equals(type)) {
				return "empProPdf";
			} else if ("xls".equals(type)) {
				return "empProXls";
			} else if ("csv".equals(type)) {
				return "empProCsv";
			} else if ("html".equals(type)) {
				return "empProHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportHrHolidaysReport ", e);
		}
		return "redirect:" + RequestConstants.VIEW_PROJECT_REPORT;
	}

	/**
	 * Method to handle request for employee transfers
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_TRANSFERS_REPORT, method = RequestMethod.POST)
	public String viewEmpTransfersPageHandler(final Model model, @ModelAttribute EmpPrimaryReportVo empReportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			EmpTransfersReportForm form = reportService.createTransfersReport(empReportVo);
			model.addAttribute("transferDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.EMP_TRANSFERS_REPORT;
	}

	/**
	 * Method to export transfer report
	 * 
	 * @param map
	 * @param transferDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMP_EXP_TRANSFER, method = RequestMethod.POST)
	public String exportEmpTransferReport(ModelMap map,
			@ModelAttribute("transferDetails") EmpTransfersReportForm transferDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(transferDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = transferDetails.getType();

			if ("pdf".equals(type)) {
				return "empTraPdf";
			} else if ("xls".equals(type)) {
				return "empTraXls";
			} else if ("csv".equals(type)) {
				return "empTraCsv";
			} else if ("html".equals(type)) {
				return "empTraHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportEmpTransferReport ", e);
		}
		return "redirect:" + RequestConstants.VIEW_TRANSFERS_REPORT;
	}

	/**
	 * Method to handle request for employee assignment
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_EMP_ASSGMNT_REPORT, method = RequestMethod.POST)
	public String viewEmpAssignmntsPageHandler(final Model model, @ModelAttribute EmpPrimaryReportVo empReportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			EmpAssigmentsReportForm form = reportService.createEmpAssgmntsReport(empReportVo);
			model.addAttribute("empAssgmnts", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.EMP_ASSGMNT_REPORT;
	}

	/**
	 * Method to export assignments report
	 * 
	 * @param map
	 * @param empAssgmnts
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMP_EXP_ASSIGNEMENTS, method = RequestMethod.POST)
	public String exportEmpAssignReport(ModelMap map,
			@ModelAttribute("empAssgmnts") EmpAssigmentsReportForm empAssgmnts) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(empAssgmnts.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = empAssgmnts.getType();

			if ("pdf".equals(type)) {
				return "empAsignPdf";
			} else if ("xls".equals(type)) {
				return "empAsignXls";
			} else if ("csv".equals(type)) {
				return "empAsignCsv";
			} else if ("html".equals(type)) {
				return "empAsignHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportEmpAssignReport ", e);
		}
		return "redirect:" + RequestConstants.VIEW_EMP_ASSGMNT_REPORT;
	}

	/**
	 * Method to handle request for employee travel report view
	 * 
	 * @param model
	 * @param empResignVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_EMP_RESIGN_REPORT, method = RequestMethod.POST)
	public String viewEmpResignationPageHandler(final Model model, @ModelAttribute EmpPrimaryReportVo empReportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			EmpResignReportForm form = reportService.createResignationReport(empReportVo);
			model.addAttribute("resignDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.EMP_RESIGN_REPORT;
	}

	/**
	 * Method to export resignations report
	 * 
	 * @param map
	 * @param resignDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMP_EXP_RESIGNATIONS, method = RequestMethod.POST)
	public String exportHrHolidaysReport(ModelMap map,
			@ModelAttribute("resignDetails") EmpResignReportForm resignDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(resignDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = resignDetails.getType();

			if ("pdf".equals(type)) {
				return "empResPdf";
			} else if ("xls".equals(type)) {
				return "empResXls";
			} else if ("csv".equals(type)) {
				return "empResCsv";
			} else if ("html".equals(type)) {
				return "empResHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportHrHolidaysReport", e);
		}
		return "redirect:" + RequestConstants.VIEW_EMP_RESIGN_REPORT;
	}

	/**
	 * Method to handle request for travel report
	 * 
	 * @param model
	 * @param empTravelVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_EMP_TRAVEL_REPORT, method = RequestMethod.POST)
	public String viewEmpTravelPageHandler(final Model model, @ModelAttribute EmpPrimaryReportVo empReportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			EmpTravelReportForm form = reportService.createTravelReport(empReportVo);
			model.addAttribute("travelDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.EMP_TRAVEL_REPORT;
	}

	/**
	 * Method to export travel report
	 * 
	 * @param map
	 * @param travelDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMP_EXP_TRAVEL, method = RequestMethod.POST)
	public String exportEmpTravelReport(ModelMap map,
			@ModelAttribute("travelDetails") EmpTravelReportForm travelDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(travelDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = travelDetails.getType();

			if ("pdf".equals(type)) {
				return "empTravelPdf";
			} else if ("xls".equals(type)) {
				return "empTravelXls";
			} else if ("csv".equals(type)) {
				return "empTravelCsv";
			} else if ("html".equals(type)) {
				return "empTravelHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportEmpTravelReport ", e);
		}
		return "redirect:" + RequestConstants.VIEW_EMP_TRAVEL_REPORT;
	}

	/**
	 * method to create evaluation report
	 * 
	 * @param model
	 * @param empReportVo
	 * @param rolesMap
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_EMP_EVALUATION_REPORT, method = RequestMethod.POST)
	public String viewEmpEvaluationPageHandler(final Model model, @ModelAttribute EmpPrimaryReportVo empReportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			EmpPerformanceForm form = reportService.createPerformanceEvaluatioReport(empReportVo);
			model.addAttribute("evaluations", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.EMP_EVALUATION_REPORT;
	}

	/**
	 * Method to export evaluation report
	 * 
	 * @param map
	 * @param evaluations
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMP_EXP_EVALUATION_REPORT, method = RequestMethod.POST)
	public String exportEvaluationsReport(ModelMap map, @ModelAttribute("evaluations") EmpPerformanceForm evaluations) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(evaluations.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = evaluations.getType();

			if ("pdf".equals(type)) {
				return "evlPdf";
			} else if ("xls".equals(type)) {
				return "evlXls";
			} else if ("csv".equals(type)) {
				return "evlCsv";
			} else if ("html".equals(type)) {
				return "evlHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportEvaluationsReport ", e);
		}
		return "redirect:" + RequestConstants.VIEW_EMP_EVALUATION_REPORT;
	}

	/**************************************
	 * PAYROLL REPORTS
	 **************************************************/
	/**
	 * Method to handle request for pay roll reports home
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_REPORTS_HOME_URL, method = RequestMethod.GET)
	public String payReportPageHandler(final Model model, HttpServletRequest request,
			@ModelAttribute PayPrimaryReportVo reportVo, @ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			reportVo.setEmpName(authEmployeeDetails.getName());
			reportVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("reportVo", reportVo);
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartmentDetails());
			model.addAttribute("types", empTypeService.findAllEmployeeType());
			model.addAttribute("categories", empCatService.findAllEmployeeCategory());
			model.addAttribute("employees", empService.listAllEmployee());
			model.addAttribute("years", reportService.getYears());

		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_REPORTS_HOME;

	}

	/**
	 * Method to handle request for loan report
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_LOAN_REPORT_URL, method = RequestMethod.POST)
	public String payLoanReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayLoansForm form = reportService.createLoansReport(reportVo);
			model.addAttribute("loanDetails", form);
			employeeVoSize = form.getDetails().size();

		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_LOANS_REPORT_PAGE;

	}

	/**
	 * Method to export Loan Report
	 * 
	 * @param map
	 * @param loanDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_LOAN, method = RequestMethod.POST)
	public String exportLoanReport(ModelMap map, @ModelAttribute("loanDetails") PayLoansForm loanDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(loanDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = loanDetails.getType();

			if ("pdf".equals(type)) {
				return "payLoanPdf";
			} else if ("xls".equals(type)) {
				return "payLoanXls";
			} else if ("csv".equals(type)) {
				return "payLoanCsv";
			} else if ("html".equals(type)) {
				return "payLoanHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportLoanReport : ", e);
		}
		return "redirect:" + RequestConstants.PAY_LOAN_REPORT_URL;
	}

	/**
	 * Method to handle request for advance salary report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_ADVSALARY_REPORT_URL, method = RequestMethod.POST)
	public String payAdvSalReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayAdvSalaryForm form = reportService.createAdvanceSalaryReport(reportVo);
			System.out.println("CONTROLLER >>>>>>>>>>>>>>>>> " + form.getDetails().size());
			model.addAttribute("advSalaries", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("payAdvSalReportPageHandler : ", e);
		}
		return PageNameConstatnts.PAY_ADVSALARY_REPORT_PAGE;

	}

	/**
	 * Method to handle request for exporting advance salary report
	 * 
	 * @param map
	 * @param advSalaries
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_ADVANCE, method = RequestMethod.POST)
	public String exportAdvanceReport(ModelMap map, @ModelAttribute("advSalaries") PayAdvSalaryForm advSalaries) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(advSalaries.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = advSalaries.getType();

			if ("pdf".equals(type)) {
				return "payAdvPdf";
			} else if ("xls".equals(type)) {
				return "payAdvXls";
			} else if ("csv".equals(type)) {
				return "payAdvCsv";
			} else if ("html".equals(type)) {
				return "payAdvHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportAdvanceReport", e);
		}
		return "redirect:" + RequestConstants.PAY_ADVSALARY_REPORT_URL;
	}

	/**
	 * Method to handle request for ESI report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_ESCI_REPORT_URL, method = RequestMethod.POST)
	public String payEsiReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayEsciForm form = reportService.createESCIReport(reportVo);
			model.addAttribute("esiDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_ESI_REPORT_PAGE;

	}

	/**
	 * Method to export ESI Report
	 * 
	 * @param map
	 * @param esiDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_ESI, method = RequestMethod.POST)
	public String exportESIReport(ModelMap map, @ModelAttribute("esiDetails") PayEsciForm esiDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(esiDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			parameterMap.put("totalEmpContribution", esiDetails.getTotalEmpContribution());
			parameterMap.put("totalOrgContribution", esiDetails.getTotalOrgContribution());
			map.addAllAttributes(parameterMap);
			String type = esiDetails.getType();

			if ("pdf".equals(type)) {
				return "payEsiPdf";
			} else if ("xls".equals(type)) {
				return "payEsiXls";
			} else if ("csv".equals(type)) {
				return "payEsiCsv";
			} else if ("html".equals(type)) {
				return "payEsiHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportESIReport ", e);
		}
		return "redirect:" + RequestConstants.PAY_ESCI_REPORT_URL;
	}

	/**
	 * Method to handle request for PF report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_PF_REPORT_URL, method = RequestMethod.POST)
	public String payPfReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayPFForm form = reportService.createPFReports(reportVo);
			model.addAttribute("payPfDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_PF_REPORT_PAGE;

	}

	/**
	 * Method to export PF report
	 * 
	 * @param map
	 * @param payOTDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_PF, method = RequestMethod.POST)
	public String exportPFReport(ModelMap map, @ModelAttribute("payPFDetails") PayPFForm payPFDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(payPFDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			parameterMap.put("totalEmpShare", payPFDetails.getTotalEmpShare());
			parameterMap.put("totalOrgShare", payPFDetails.getTotalOrgShare());
			parameterMap.put("totalShare", payPFDetails.getTotalTotal());
			map.addAllAttributes(parameterMap);
			String type = payPFDetails.getType();

			if ("pdf".equals(type)) {
				return "payPFPdf";
			} else if ("xls".equals(type)) {
				return "payPFXls";
			} else if ("csv".equals(type)) {
				return "payPFCsv";
			} else if ("html".equals(type)) {
				return "payPFHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportPFReport", e);
		}
		return "redirect:" + RequestConstants.PAY_PF_REPORT_URL;
	}

	/**
	 * Method to handle request for overtime reports
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_OVERTIME_URL, method = RequestMethod.POST)
	public String payOTReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayOTForm form = reportService.createOTReport(reportVo);
			model.addAttribute("payOTDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_OT_PAGE;

	}

	/**
	 * Method to export overtime report
	 * 
	 * @param map
	 * @param payOTDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_OT, method = RequestMethod.POST)
	public String exportOTReport(ModelMap map, @ModelAttribute("payOTDetails") PayOTForm payOTDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(payOTDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = payOTDetails.getType();

			if ("pdf".equals(type)) {
				return "payOTPdf";
			} else if ("xls".equals(type)) {
				return "payOTXls";
			} else if ("csv".equals(type)) {
				return "payOTCsv";
			} else if ("html".equals(type)) {
				return "payOTHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportOTReport", e);
		}
		return "redirect:" + RequestConstants.PAY_OVERTIME_URL;
	}

	/**
	 * Method to handle request for reimbursements report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_REIMBURSE_URL, method = RequestMethod.POST)
	public String payReimReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayRembOTForm form = reportService.createReimbursementReport(reportVo);
			model.addAttribute("payReimbDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_REIMB_PAGE;
	}

	/**
	 * Method to handle request for commission report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_COMMISSION_URL, method = RequestMethod.POST)
	public String payCommissionReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayCommisionForm form = reportService.createCommissionsReport(reportVo);
			model.addAttribute("payCommDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_COMM_PAGE;

	}

	/**
	 * Method to export commissions report
	 * 
	 * @param map
	 * @param payCommDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_COMMISSIONS, method = RequestMethod.POST)
	public String exportCommissionReport(ModelMap map,
			@ModelAttribute("payCommDetails") PayCommisionForm payCommDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(payCommDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = payCommDetails.getType();

			if ("pdf".equals(type)) {
				return "payComPdf";
			} else if ("xls".equals(type)) {
				return "payComXls";
			} else if ("csv".equals(type)) {
				return "payComCsv";
			} else if ("html".equals(type)) {
				return "payComHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportCommissionReport", e);
		}
		return "redirect:" + RequestConstants.PAY_COMMISSION_URL;
	}

	/**
	 * Method t handle request for deductions report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_DEDUCTION_URL, method = RequestMethod.POST)
	public String payDeductReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayDeductionsForm form = reportService.createDeductionsReport(reportVo);
			model.addAttribute("deductionDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_DEDUCT_PAGE;

	}

	/**
	 * Method to export deductions report
	 * 
	 * @param map
	 * @param deductionDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_DEDUCTIONS, method = RequestMethod.POST)
	public String exportDeductionReport(ModelMap map,
			@ModelAttribute("deductionDetails") PayDeductionsForm deductionDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(deductionDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			parameterMap.put("totalAmount", deductionDetails.getTotalAmount());
			map.addAllAttributes(parameterMap);
			String type = deductionDetails.getType();

			if ("pdf".equals(type)) {
				return "payDedPdf";
			} else if ("xls".equals(type)) {
				return "payDedXls";
			} else if ("csv".equals(type)) {
				return "payDedCsv";
			} else if ("html".equals(type)) {
				return "payDedHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportDeductionReport", e);
		}
		return "redirect:" + RequestConstants.PAY_DEDUCTION_URL;
	}

	/**
	 * Method to handle request for adjustments report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_ADJUSTMENTS_URL, method = RequestMethod.POST)
	public String payAdjustmentsReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayAdjstmntForm form = reportService.createAdjstmntsReport(reportVo);
			model.addAttribute("payAdjsDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_ADJST_PAGE;
	}

	/**
	 * Method to export adjustments
	 * 
	 * @param map
	 * @param payAdjsDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_ADJUSTMENT, method = RequestMethod.POST)
	public String exportAdjustmentsReport(ModelMap map,
			@ModelAttribute("payAdjsDetails") PayAdjstmntForm payAdjsDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(payAdjsDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			parameterMap.put("totalAmnt", payAdjsDetails.getTotalAmnt());
			map.addAllAttributes(parameterMap);
			String type = payAdjsDetails.getType();

			if ("pdf".equals(type)) {
				return "payAdjPdf";
			} else if ("xls".equals(type)) {
				return "payAdjXls";
			} else if ("csv".equals(type)) {
				return "payAdjCsv";
			} else if ("html".equals(type)) {
				return "payAdjHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportAdjustmentsReport", e);
		}
		return "redirect:" + RequestConstants.PAY_ADJUSTMENTS_URL;
	}

	/**
	 * Method to handle request for bonuses report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_BONUSES_URL, method = RequestMethod.POST)
	public String payBonusesReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayBonusForm form = reportService.createBonusReport(reportVo);
			model.addAttribute("bonusesDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_BONUSES_PAGE;
	}

	/**
	 * Method to export bonus report
	 * 
	 * @param map
	 * @param bonusesDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_BONUSE, method = RequestMethod.POST)
	public String exportBonusReport(ModelMap map, @ModelAttribute("bonusesDetails") PayBonusForm bonusesDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(bonusesDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			parameterMap.put("totalAmount", bonusesDetails.getTotalAmount());
			map.addAllAttributes(parameterMap);
			String type = bonusesDetails.getType();

			if ("pdf".equals(type)) {
				return "payBonPdf";
			} else if ("xls".equals(type)) {
				return "payBonXls";
			} else if ("csv".equals(type)) {
				return "payBonCsv";
			} else if ("html".equals(type)) {
				return "payBonHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportBonusReport", e);
		}
		return "redirect:" + RequestConstants.PAY_BONUSES_URL;
	}

	/**
	 * Method to handle request for daily wages report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_DAILYW_URL, method = RequestMethod.POST)
	public String payDailyReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {

			PayDailyWagesForm form = reportService.createDailyWagesReport(reportVo);
			model.addAttribute("dailyWageDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_DAILYWAGE_PAGE;
	}

	/**
	 * Method to export daily wages report
	 * 
	 * @param map
	 * @param dailyWageDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_DAILY, method = RequestMethod.POST)
	public String exportCommissionReport(ModelMap map,
			@ModelAttribute("dailyWageDetails") PayDailyWagesForm dailyWageDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(dailyWageDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = dailyWageDetails.getType();

			if ("pdf".equals(type)) {
				return "payDailyPdf";
			} else if ("xls".equals(type)) {
				return "payDailyXls";
			} else if ("csv".equals(type)) {
				return "payDailyCsv";
			} else if ("html".equals(type)) {
				return "payDailyHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportCommissionReport ", e);
		}
		return "redirect:" + RequestConstants.PAY_DAILYW_URL;
	}

	/**
	 * Method to handle request for hourly wages report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_HOURLYW_URL, method = RequestMethod.POST)
	public String payHourlyReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayHourlyWagesForm form = reportService.createHourlyWagesReport(reportVo);
			model.addAttribute("hourlyDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_HOURLYWAGE_PAGE;

	}

	/**
	 * Method to export Hourly Report
	 * 
	 * @param map
	 * @param hourlyDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_HOURLY, method = RequestMethod.POST)
	public String exportHourlyReport(ModelMap map, @ModelAttribute("hourlyDetails") PayHourlyWagesForm hourlyDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(hourlyDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = hourlyDetails.getType();

			if ("pdf".equals(type)) {
				return "payHourlyPdf";
			} else if ("xls".equals(type)) {
				return "payHourlyXls";
			} else if ("csv".equals(type)) {
				return "payHourlyCsv";
			} else if ("html".equals(type)) {
				return "payHourlyHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportHourlyReport", e);
		}
		return "redirect:" + RequestConstants.PAY_HOURLYW_URL;
	}

	/**
	 * Method to handle request for salary report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_SALARY_REPORT_URL, method = RequestMethod.POST)
	public String paySalaryReportPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PaySalaryReportForm form = reportService.createSalaryReport(reportVo);
			model.addAttribute("salaryDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_SALARY_REPORT;

	}

	/**
	 * Method to handle request for split salary
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_SALARY_SPLIT, method = RequestMethod.POST)
	public String paySalarySplitPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PaySalarySplitForm form = reportService.createSalarySplitReport(reportVo);
			model.addAttribute("salarySplitDetails", form);
			employeeVoSize = form.getDetail().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_SPLIT_SALARY_PAGE;

	}

	/**
	 * Method to handle request for pay slips
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_PAYSLIPS_URL, method = RequestMethod.POST)
	public String payPaySlipsPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			PayPaySlipReportForm form = reportService.createPaySlipReport(reportVo);
			model.addAttribute("slipDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_PAYSLIP_PAGE;

	}

	/**
	 * Method to export pay slip reports
	 * 
	 * @param map
	 * @param slipDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_EXP_PAYSLIP, method = RequestMethod.POST)
	public String exportPaySlip(final Model model, @ModelAttribute("slipDetails") PayPaySlipReportForm slipDetails) {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("NAME", slipDetails.getEmpName());
			parameters.put("CODE", slipDetails.getCode());
			parameters.put("DESIGNATION", slipDetails.getDesignation());
			parameters.put("DEPARTMENT", slipDetails.getDepartment());
			parameters.put("BRANCH", slipDetails.getBranch());
			parameters.put("DATE_OF_JOINING", slipDetails.getDateOfJoining());
			parameters.put("MODE", slipDetails.getPayMode());
			parameters.put("BANK", slipDetails.getBank());
			parameters.put("ACCOUNT", slipDetails.getAccount());
			parameters.put("ESI", slipDetails.getEsi());
			parameters.put("PF", slipDetails.getPf());
			parameters.put("PAN", slipDetails.getPan());

			List<PayPaySlipDetailsVo> details = slipDetails.getDetails();
			List<CustomPdfDetailVo> earningsData = new ArrayList<CustomPdfDetailVo>();
			List<CustomPdfDetailVo> deductionsData = new ArrayList<CustomPdfDetailVo>();
			for (PayPaySlipDetailsVo detailVo : details) {
				LinkedHashMap<String, BigDecimal> detailsMap = detailVo.getDetail();
				List<PaySlipItemVo> paysSlipItems = new ArrayList<PaySlipItemVo>();
				List<String> salaryItems = new ArrayList<String>();
				paysSlipItems = payItemService.findAllPaySlipItem();
				List<ExtraPaySlipItemVo> extraPaySlipItems = new ArrayList<ExtraPaySlipItemVo>();
				extraPaySlipItems = extraPaySlipItemService.findAllExtraPaySlipItem();
				salaryItems.add(PayRollConstants.GROSS_SALARY);
				for (PaySlipItemVo itemVo : paysSlipItems) {
					salaryItems.add(itemVo.getTitle());
				}
				for (ExtraPaySlipItemVo vo : extraPaySlipItems) {
					salaryItems.add(vo.getTitle());
				}
				for (String key : salaryItems) {
					CustomPdfDetailVo vo = new CustomPdfDetailVo();
					vo.setName(key);
					vo.setAmount(detailsMap.get(key));
					if (key.contains("GROSS"))
						earningsData.add(0, vo);
					else
						earningsData.add(vo);
				}
				for (Map.Entry<String, BigDecimal> entry : detailVo.getDeductionDetail().entrySet()) {
					CustomPdfDetailVo vo = new CustomPdfDetailVo();
					vo.setName(entry.getKey());
					vo.setAmount(entry.getValue());
					deductionsData.add(vo);
				}
				parameters.put("MONTH", detailVo.getMonth());
				parameters.put("NET_PAY", detailVo.getNetPay());
				parameters.put("CTC", detailVo.getCtc());
				Long totalDays = detailVo.getTotalDays();
				Double lop = detailVo.getLop();
				parameters.put("LOP", lop);
				parameters.put("DAYS", totalDays);
				Long actualDays = 0L;
				if(totalDays!=null && lop !=null)
					actualDays = (long) (totalDays - lop);
				parameters.put("ACTUAL_DAYS", actualDays);
				parameters.put("TOTAL_EARNING", detailVo.getTotalEarnings());
				parameters.put("TOTAL_DEDUCTION", detailVo.getTotalDeductions());
			}

			java.io.InputStream inputStream = context.getResourceAsStream("WEB-INF/jrxml/PayRoll/PayslipSubFormat.jrxml");
			parameters.put("EARNINGS_DATASOURCE", new JRBeanCollectionDataSource(earningsData));
			parameters.put("DEDUCTIONS_DATASOURCE", new JRBeanCollectionDataSource(deductionsData));
			JasperReport subReport = JasperCompileManager.compileReport(inputStream);
			parameters.put("EARNINGS_REPORT", subReport);
			parameters.put("DEDUCTIONS_REPORT", subReport);
			parameters.put("datasource", new JREmptyDataSource());
			model.addAllAttributes(parameters);
		} catch (JRException e) {
			logger.info("",e);

		}
		return "paySlipFormatPdf";

	}

	/**
	 * Method to handle request for pay roll report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAY_PAYROLL_REPORT_URL, method = RequestMethod.POST)
	public String payPayrollPageHandler(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.PAY_PAYROLL_REPORT_PAGE;

	}

	/*****************************************************
	 * TIME SHEET REPORTS
	 *****************************************************************************/

	/**
	 * Method to handle request for time sheet report
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIME_REPORTS_URL, method = RequestMethod.GET)
	public String timeReportPageHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("reportVo", new TimeReportPrimaryVo());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartmentDetails());
			model.addAttribute("types", empTypeService.findAllEmployeeType());
			model.addAttribute("categories", empCatService.findAllEmployeeCategory());
			model.addAttribute("employees", empService.listAllEmployee());
			model.addAttribute("years", reportService.getYears());

		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.TIME_REPORTS_HOME_PAGE;

	}

	/**
	 * Method to handle request for work sheet report
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIME_VIEW_WORKSHEET, method = RequestMethod.POST)
	public String timeWorksheetReportPageHandler(final Model model, @ModelAttribute TimeReportPrimaryVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			TimeWorkSheetReportForm form = reportService.createWorkSheetReport(reportVo);
			model.addAttribute("timeWorksheetDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.TIME_WORKSHEET_REPORT;

	}

	/**
	 * Method to export worksheet report
	 * 
	 * @param map
	 * @param timeWorksheetDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIM_EXP_WORKSHEET, method = RequestMethod.POST)
	public String exportTimWorkSheetReport(ModelMap map,
			@ModelAttribute("timeWorksheetDetails") TimeWorkSheetReportForm timeWorksheetDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(timeWorksheetDetails.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = timeWorksheetDetails.getType();

			if ("pdf".equals(type)) {
				return "timWSPdf";
			} else if ("xls".equals(type)) {
				return "timWSXls";
			} else if ("csv".equals(type)) {
				return "timWSCsv";
			} else if ("html".equals(type)) {
				return "timWSHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportTimWorkSheetReport ", e);
		}
		return "redirect:" + RequestConstants.TIME_VIEW_WORKSHEET;
	}

	/**
	 * Method to handle request for employee work sheet report
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIME_VIEW_EMP_WORKSHEET, method = RequestMethod.POST)
	public String timeEmpWorksheetReportPageHandler(final Model model, @ModelAttribute TimeReportPrimaryVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			TimeEmpWorkReportForm form = reportService.createEmpWorkSheetReport(reportVo);
			model.addAttribute("timeEmpWorkDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.TIME_EMP_WORK_REPORT;

	}

	/**
	 * Method to export employee work sheet
	 * 
	 * @param map
	 * @param timeEmpWorkDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIM_EXP_EMP_WORKSHEET, method = RequestMethod.POST)
	public String exportTimEmpWorkSheetReport(ModelMap map,
			@ModelAttribute("timeEmpWorkDetails") TimeEmpWorkReportForm timeEmpWorkDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(timeEmpWorkDetails.getDetails());
			parameterMap.put("empName", timeEmpWorkDetails.getEmpName());
			parameterMap.put("designation", timeEmpWorkDetails.getDesignation());
			parameterMap.put("department", timeEmpWorkDetails.getDepartment());
			parameterMap.put("userName", timeEmpWorkDetails.getUserName());
			parameterMap.put("datasource", JRdataSource);
			parameterMap.put("totalHours", timeEmpWorkDetails.getTotalHours());
			map.addAllAttributes(parameterMap);
			String type = timeEmpWorkDetails.getType();

			if ("pdf".equals(type)) {
				return "timEmWSPdf";
			} else if ("xls".equals(type)) {
				return "timEmWSXls";
			} else if ("csv".equals(type)) {
				return "timEmWSCsv";
			} else if ("html".equals(type)) {
				return "timEmWSHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportTimEmpWorkSheetReport ", e);
		}
		return "redirect:" + RequestConstants.TIME_VIEW_EMP_WORKSHEET;
	}

	/**
	 * Method to handle request for employee time sheet report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIME_VIEW_EMP_TIMESHEET, method = RequestMethod.POST)
	public String timeEmpTimesheetReportPageHandler(final Model model, @ModelAttribute TimeReportPrimaryVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			TimeEmpTimesheetReportForm form = reportService.createEmployeeTimeSheetReport(reportVo);
			model.addAttribute("timesheetDetails", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.TIME_EMP_TIMESHEET_REPORT;

	}

	/**
	 * Method
	 * 
	 * @param map
	 * @param timesheetDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIM_EXP_EMP_TIMESHEET, method = RequestMethod.POST)
	public String employeeTimeSheetExport(ModelMap map,
			@ModelAttribute("timesheetDetails") TimeEmpTimesheetReportForm timesheetDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(timesheetDetails.getDetails());
			parameterMap.put("empName", timesheetDetails.getEmpName());
			parameterMap.put("designation", timesheetDetails.getDesignation());
			parameterMap.put("department", timesheetDetails.getDepartment());
			parameterMap.put("userName", timesheetDetails.getUserName());
			parameterMap.put("datasource", JRdataSource);
			parameterMap.put("hoursTotal", timesheetDetails.getHoursTotal());
			map.addAllAttributes(parameterMap);
			String type = timesheetDetails.getType();
			if ("pdf".equals(type)) {
				return "timEmpTimPdf";
			} else if ("xls".equals(type)) {
				return "timEmpTimXls";
			} else if ("csv".equals(type)) {
				return "timEmpTimCsv";
			} else if ("html".equals(type)) {
				return "timEmpTimHtml";
			} else {
				return null;
			}
		} catch (Exception ex) {
			logger.info("",ex);
			return PageNameConstatnts.TIME_EMP_TIMESHEET_REPORT;
		}
	}

	/**
	 * Method to handle request for employee leave summary
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIME_VIEW_LEAVE_SUMMARY, method = RequestMethod.POST)
	public String timeEmpLeaveSummaryPageHandler(final Model model, @ModelAttribute TimeReportPrimaryVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			TimeEmpLeaveSummaryForm form = reportService.createEmployeeLeaveSummary(reportVo);
			model.addAttribute("timeLeavesDetails", form);
			employeeVoSize = form.getDetails().size() + form.getSummary().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.TIME_EMP_LEAVE_SUMMARY;

	}

	/**
	 * Method to handle request for absent employees report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIME_VIEW_ABSENT_EMPLOYEES, method = RequestMethod.POST)
	public String timeAbsentEmpPageHandler(final Model model, @ModelAttribute TimeReportPrimaryVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			TimeAbsentEmployeesForm form = reportService.createAbsentEmployeesReport(reportVo);
			model.addAttribute("timeAbsentEmp", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.TIME_ABSENT_EMP;

	}

	/**
	 * Method to export absent report
	 * 
	 * @param map
	 * @param timeAbsentEmp
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIM_EXP_ABSENT, method = RequestMethod.POST)
	public String exportTimAbsentReport(ModelMap map,
			@ModelAttribute("timeAbsentEmp") TimeAbsentEmployeesForm timeAbsentEmp) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(timeAbsentEmp.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = timeAbsentEmp.getType();

			if ("pdf".equals(type)) {
				return "timAbsntPdf";
			} else if ("xls".equals(type)) {
				return "timAbsntXls";
			} else if ("csv".equals(type)) {
				return "timAbsntCsv";
			} else if ("html".equals(type)) {
				return "timAbsntHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportTimAbsentReport ", e);
		}
		return "redirect:" + RequestConstants.TIME_VIEW_ABSENT_EMPLOYEES;
	}

	/**
	 * Method to handle request for employee Workshift report
	 * 
	 * @param model
	 * @param reportVo
	 * @return
	 */

	@RequestMapping(value = RequestConstants.TIME_EMP_WORKSHIFT, method = RequestMethod.POST)
	public String timeEmpAttendancePageHandler(final Model model, @ModelAttribute TimeReportPrimaryVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			TimeWorkShiftForm form = reportService.createWorkShiftReport(reportVo);
			model.addAttribute("shiftDetails", form);
			employeeVoSize = form.getDetail().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.TIME_EMP_ATTENDANCE;

	}

	/**
	 * Method to export workshift
	 * 
	 * @param map
	 * @param shiftDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TIM_EXP_WORKSHIFT, method = RequestMethod.POST)
	public String exportTimWorkshiftReport(ModelMap map,
			@ModelAttribute("shiftDetails") TimeWorkShiftForm shiftDetails) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(shiftDetails.getDetail());
			parameterMap.put("empName", shiftDetails.getEmpName());
			parameterMap.put("department", shiftDetails.getDepartment());
			parameterMap.put("designation", shiftDetails.getDesignation());
			parameterMap.put("userName", shiftDetails.getUserName());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = shiftDetails.getType();

			if ("pdf".equals(type)) {
				return "timShiftPdf";
			} else if ("xls".equals(type)) {
				return "timShiftXls";
			} else if ("csv".equals(type)) {
				return "timShiftCsv";
			} else if ("html".equals(type)) {
				return "timShiftHtml";
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.info("exportTimWorkshiftReport ", e);
		}
		return "redirect:" + RequestConstants.TIME_EMP_WORKSHIFT;
	}

	@RequestMapping(value = RequestConstants.BANK_STATEMENT, method = RequestMethod.POST)
	public String bankStatementReport(final Model model, @ModelAttribute PayPrimaryReportVo reportVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			BankStatementForm form = reportService.createBankStatement(reportVo);
			model.addAttribute("statement", form);
			employeeVoSize = form.getDetails().size();
		} catch (Exception e) {
			logger.info("",e);
		}
		return PageNameConstatnts.BANK_STATEMENT;

	}

	@RequestMapping(value = RequestConstants.BANK_HOME, method = RequestMethod.GET)
	public String bankReportHome(final Model model, HttpServletRequest request,
			@ModelAttribute PayPrimaryReportVo reportVo, @ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			reportVo.setEmpName(authEmployeeDetails.getName());
			reportVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("reportVo", reportVo);
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartmentDetails());
			model.addAttribute("types", empTypeService.findAllEmployeeType());
			model.addAttribute("categories", empCatService.findAllEmployeeCategory());
			model.addAttribute("employees", empService.listAllEmployee());
			model.addAttribute("years", reportService.getYears());
		} catch (Exception ex) {
			logger.info("",ex);
			
		}
		return PageNameConstatnts.BANK_HOME;
	}

	@RequestMapping(value = RequestConstants.EXPORT_BANK_STATEMENT, method = RequestMethod.POST)
	public String exportBankStatement(ModelMap map, @ModelAttribute("statement") BankStatementForm statement) {
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			System.out.println(statement.getDetails().size());
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(statement.getDetails());
			parameterMap.put("datasource", JRdataSource);
			map.addAllAttributes(parameterMap);
			String type = statement.getType();
			if ("pdf".equals(type)) {
				return "statPdf";
			} else if ("xls".equals(type)) {
				return "statXls";
			}

		} catch (Exception ex) {
			logger.info("",ex);
		}
		return "redirect:" + RequestConstants.BANK_HOME;
	}
}
