package com.hrms.web.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ProfidentFundService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.ProfidentFundVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Jithin Mohan
 * @since 17-April-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "profidentFundVo", "thisUser" , "roles"})
public class ProfidentFundController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private ProfidentFundService profidentFundService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private EmployeeService employeeService;

	/**
	 * method to load PF index page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PROFIDENT_FUND_URL, method = RequestMethod.GET)
	public String getProfidentFundIndexPage(final Model model, @ModelAttribute("thisUser") String thisUser,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("pfList",
					profidentFundService.findAllProfidentFundByEmployee(authEmployeeDetails.getEmployeeCode()));
			model.addAttribute("profidentFundVo", new ProfidentFundVo());
			model.addAttribute("thisUser", thisUser);
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PageNameConstatnts.GET_PROFIDENT_FUNDS_PAGE;
	}

	/**
	 * method to load Add PF page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_PROFIDENT_FUND_URL, method = RequestMethod.GET)
	public String getAddProfidentFundPage(final Model model,
			@ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo,
			@ModelAttribute("profidentFundVo") ProfidentFundVo profidentFundVo, BindingResult result,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		model.addAttribute("profidentFundVo", profidentFundVo);
		model.addAttribute("payrollEmployees", Employee.getEmployees(employeeService.listEmployeesWithPayroll()));
		if (profidentFundVo.getProfidentFundId() == null) {
			profidentFundVo.setRecordedBy(employeeVo.getName());
			model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
		} else {
			profidentFundVo.setRecordedBy(userService.findUser(profidentFundVo.getRecordedBy()).getEmployee());
			model.addAttribute(MessageConstants.CREATED_ON, profidentFundVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		}
		return PageNameConstatnts.ADD_PROFIDENT_FUND_PAGE;
	}

	/**
	 * method to save pf
	 * 
	 * @param redirectAttributes
	 * @param profidentFundVo
	 * @param result
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_PROFIDENT_FUND_URL, method = RequestMethod.POST)
	public ModelAndView saveProfidentFundWithPost(final Model model, @ModelAttribute ProfidentFundVo profidentFundVo,
			RedirectAttributes redirectAttributes, BindingResult result) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_PROFIDENT_FUND_URL));
		try {
			profidentFundService.saveOrUpdateProfidentFund(profidentFundVo);
			redirectAttributes.addFlashAttribute("profidentFundVo", profidentFundVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_PROFIDENT_FUND_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to view PF information
	 * 
	 * @param pfId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_PROFIDENT_FUND_URL, method = RequestMethod.GET)
	public String viewProfidentFundInformationWithPost(@RequestParam(value = "otp", required = true) Long pfId,
			final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("profidentFundVo", profidentFundService.findProfidentFundById(pfId));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PageNameConstatnts.VIEW_PROFIDENT_FUND_PAGE;
	}

	/**
	 * method to edit PF information
	 * 
	 * @param pfId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_PROFIDENT_FUND_URL, method = RequestMethod.GET)
	public String editProfidentFundInformationWithGet(@RequestParam(value = "otp", required = true) Long pfId,
			final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			ProfidentFundVo profidentFundVo = profidentFundService.findProfidentFundById(pfId);
			profidentFundVo.setRecordedBy(userService.findUser(profidentFundVo.getRecordedBy()).getEmployee());
			model.addAttribute("profidentFundVo", profidentFundVo);
			model.addAttribute(MessageConstants.CREATED_ON, profidentFundVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PageNameConstatnts.ADD_PROFIDENT_FUND_PAGE;
	}

	/**
	 * method to list all PF information
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LIST_ALL_PROFIDENT_FUND_URL, method = RequestMethod.GET)
	public String listAllProfidentFundInformations(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("pfList", profidentFundService.findAllProfidentFund());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.PROFIDENT_FUND_LIST_PAGE;
	}

	/**
	 * method to delete PF information
	 * 
	 * @param pfId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_PROFIDENT_FUND_URL, method = RequestMethod.POST)
	public String deleteProfidentFundInformationWithPost(@RequestParam(value = "pfId", required = true) Long pfId,
			final Model model) {
		try {
			profidentFundService.deleteProfidentFund(pfId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return "redirect:" + RequestConstants.LIST_ALL_PROFIDENT_FUND_URL;
	}
}
