package com.hrms.web.controller;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.service.SalaryCalculatorService;
import com.hrms.web.vo.SalaryCalculatorVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 04-May-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails" , "roles" })
public class SalaryCalculatorController {

	@Log
	private Logger logger;
	
	@Autowired 
	private SalaryCalculatorService calculatorService;
	
	/**
	 * method to reach salary calculator page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SALARY_CALCULATOR_URL , method = RequestMethod.GET)
	public String salaryCalculatorGet(final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		
		return PageNameConstatnts.SALARY_CALCULATOR_PAGES;
	}
	
	/**
	 * method to calculate salary
	 * @param grossSalary
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SALARY_CALCULATOR_CALCULATE , method= RequestMethod.GET)
	public  String calculateSalary(final Model model,@RequestParam(value = "grossSalary" , required = true)BigDecimal grossSalary){
		SalaryCalculatorVo calculatorVo = new SalaryCalculatorVo();
		try{
			calculatorVo = calculatorService.calculateSalary(grossSalary);
			model.addAttribute("result", calculatorVo);
		}catch(Exception e){
			e.printStackTrace();
		}
		return PageNameConstatnts.CALUCLATOR_TEMPLATE;
	}
}
