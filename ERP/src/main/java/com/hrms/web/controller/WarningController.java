package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Superiors;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.WarningService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.WarningDocumentsVo;
import com.hrms.web.vo.WarningVo;

/**
 * 
 * @author Jithin Mohan
 * @since 20-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "warningVo" })
public class WarningController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private WarningService warningService;

	@Autowired
	private EmployeeService employeeService;

	/**
	 * method to list all the warnings
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WARNING_PAGE_URL, method = RequestMethod.GET)
	public String handleWarningListPageWithGet(final Model model) {
		try {
			model.addAttribute("warningList", warningService.findAllWarning());
			model.addAttribute("warningVo", new WarningVo());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.WARNING_PAGE;
	}

	/**
	 * method to load warning registration page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WARNING_CREATE_PAGE_URL, method = RequestMethod.GET)
	public String handleWarningCreatePageWithGet(final Model model, @ModelAttribute("warningVo") WarningVo warningVo,
			BindingResult result) {
		model.addAttribute("warningVo", warningVo);
		if (warningVo.getWarningId() == null) {
			model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
		} else {
			model.addAttribute(
					"superiorList",
					Superiors.getSuperiors(employeeService
							.getEmployeesDetails(
									employeeService.findAndReturnEmployeeByCode(warningVo.getEmployeeCode()).getId())
							.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute(MessageConstants.CREATED_ON, warningVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		}
		return PageNameConstatnts.WARNING_CREATE_PAGE;
	}

	/**
	 * method to save or update warnings
	 * 
	 * @param warningVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WARNING_CREATE_PAGE_URL, method = RequestMethod.POST)
	public ModelAndView saveOrUpdateWarningsWithPost(@ModelAttribute WarningVo warningVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.WARNING_CREATE_PAGE_URL));
		try {
			warningService.saveWarning(warningVo);
			redirectAttributes.addFlashAttribute("warningVo", warningVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_WARNING_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to list all the warnings
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WARNING_LIST_PAGE_URL, method = RequestMethod.GET)
	public String listAllWarnings(final Model model) {
		try {
			model.addAttribute("warningList", warningService.findAllWarning());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.WARNING_LIST_PAGE;
	}

	/**
	 * method to fetch the details of a warning
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WARNING_UPDATE_PAGE_URL, method = RequestMethod.GET)
	public String updateWarningInfoWithGet(final Model model, final HttpServletRequest request) {
		try {
			WarningVo warningVo = warningService.findWarningById(Long.parseLong(request.getParameter("otpId")));
			model.addAttribute("warningVo", warningVo);
			model.addAttribute(
					"superiorList",
					Superiors.getSuperiors(employeeService
							.getEmployeesDetails(
									employeeService.findAndReturnEmployeeByCode(warningVo.getEmployeeCode()).getId())
							.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute(MessageConstants.CREATED_ON, warningVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.NO_WARNING_DETAILS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.WARNING_CREATE_PAGE;
	}

	/**
	 * method to to get warning information
	 * 
	 * @param model
	 * @param warningId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WARNING_VIEW_PAGE_URL, method = RequestMethod.GET)
	public String viewWarningInformation(final Model model,
			@RequestParam(value = "otpId", required = true) Long warningId) {
		try {
			WarningVo warningVo = warningService.findWarningById(warningId);
			model.addAttribute("warningVo", warningVo);
			model.addAttribute(MessageConstants.CREATED_ON, warningVo.getRecordedOn());
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.NO_WARNING_DETAILS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.WARNING_VIEW_PAGE;
	}

	/**
	 * method to delete warning
	 * 
	 * @param warningId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WARNING_DELETE_PAGE_URL, method = RequestMethod.POST)
	public String deleteThisWarningWithPost(@RequestParam(value = "warningId", required = true) Long warningId) {
		try {
			warningService.deleteWarning(warningId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.WARNING_LIST_PAGE_URL;
	}

	/**
	 * method to upload warning documents
	 * 
	 * @param warningVo
	 * @param warningId
	 * @param warningDocs
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WARNING_DOC_UPLOAD_URL, method = RequestMethod.POST)
	public ModelAndView uploadWarningDocumentsWithPost(@ModelAttribute WarningVo warningVo,
			@RequestParam(value = "warningId", required = true) Long warningId, MultipartHttpServletRequest request) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.WARNING_PAGE_URL));
		try {
			List<WarningDocumentsVo> documents = uploadWarningDocuments(request);
			if (!documents.isEmpty()) {
				warningVo.setWarningId(warningId);
				warningVo.setDocuments(documents);
				warningService.saveWarningDocuments(warningVo);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return view;
	}

	/**
	 * method to list all documents of a warning
	 * 
	 * @param model
	 * @param warningDocId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WARNING_DOC_LIST_PAGE_URL, method = RequestMethod.POST)
	public String listAllWarningDocumentsWithPost(final Model model,
			@RequestParam(value = "warningId", required = true) Long warningId) {
		try {
			model.addAttribute("warningDocList", warningService.findWarningById(warningId));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.WARNING_DOC_LIST_PAGE;
	}

	/**
	 * method to upload files to SFTP
	 * 
	 * @param request
	 * @return
	 */
	private List<WarningDocumentsVo> uploadWarningDocuments(MultipartHttpServletRequest request) {
		List<WarningDocumentsVo> documentsVos = new ArrayList<WarningDocumentsVo>();
		try {
			Iterator<String> itrator = request.getFileNames();
			while (itrator.hasNext()) {
				MultipartFile multipartFile = request.getFile(itrator.next());
				if (!multipartFile.isEmpty()) {
					String fileAccesUrl = FileUpload.uploadDocument(MessageConstants.WARNING_MODULE, multipartFile);
					if (fileAccesUrl != null) {
						WarningDocumentsVo documentVo = new WarningDocumentsVo();
						documentVo.setDocumentUrl(fileAccesUrl);
						documentVo.setDocumentName(multipartFile.getOriginalFilename());
						documentsVos.add(documentVo);
					}
				}

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return documentsVos;
	}

	/**
	 * method to delete warning document
	 * 
	 * @param documentId
	 * @param documentUrl
	 * @return
	 */
	@RequestMapping(value = RequestConstants.WARNING_DOC_DELETE_URL, method = RequestMethod.POST)
	public String deleteWarningDocumentWithPost(@RequestParam(value = "documentId", required = true) Long documentId,
			@RequestParam(value = "documentUrl", required = true) String documentUrl) {
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			if (sftpOperation.removeFileFromSFTP(documentUrl))
				warningService.deleteWarningDocument(documentId);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.WARNING_DOC_LIST_PAGE;
	}
}
