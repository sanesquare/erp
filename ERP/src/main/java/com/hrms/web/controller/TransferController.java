package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dao.ProjectStatusDao;
import com.hrms.web.dto.Superiors;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ForwardApplicationStatusService;
import com.hrms.web.service.ProjectService;
import com.hrms.web.service.TransferService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SFTPFilePropertiesVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.TransferDocumentsVo;
import com.hrms.web.vo.TransferVo;
import com.hrms.web.vo.TravelDestinationVo;
import com.hrms.web.vo.TravelDocumentVo;
import com.hrms.web.vo.TravelVo;

/**
 * 
 * @author Shamsheer
 * @since 10-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class TransferController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private TransferService transferService;

	@Autowired
	private ForwardApplicationStatusService statusService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	/**
	 * method to reach travel home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRANSFER_URL, method = RequestMethod.GET)
	public String transferPageGetRequestHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		logger.info("inside>> TravelController... travelPageGetRequestHandler");
		try {
			model.addAttribute("rolesMap", rolesMap);
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			model.addAttribute("transfers",
					transferService.findAllTransferByEmployee(authEmployeeDetails.getEmployeeCode()));
			model.addAttribute("thisCode", authEmployeeDetails.getEmployeeCode());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.TRANSFER_PAGE;
	}

	/**
	 * method to handle addTravel page get request
	 * 
	 * @param model
	 * @return PageNameConstatnts.ADD_TRAVEL_PAGE
	 */
	@RequestMapping(value = RequestConstants.ADD_TRANSFER_URL, method = RequestMethod.GET)
	public String addTransferPageGetRequestHandler(final Model model, @ModelAttribute TransferVo transferVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			transferVo.setEmployee(authEmployeeDetails.getName()+" ("+authEmployeeDetails.getEmployeeCode()+")");
			transferVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_TRANSFER_PAGE;
	}

	/**
	 * method to save or update travel
	 * 
	 * @param model
	 * @param travelVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRANSFER_SAVE, method = RequestMethod.POST)
	public String saveTransfer(final Model model, @ModelAttribute TransferVo transferVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		try {
			transferVo.setEmployee(authEmployeeDetails.getName());
			transferVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			if (transferVo.getTransferId() == null) {
				transferVo.setTransferId(transferService.saveTransfer(transferVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.TRANSFER_SAVE_SUCCESS);
			} else {
				transferService.updateTransfer(transferVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.TRANSFER_UPDATE_SUCCESS);
			}
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return "redirect:" + RequestConstants.TRANSFER_EDIT + "?id=" + transferVo.getTransferId();

	}

	/**
	 * method to edit travel
	 * 
	 * @param model
	 * @param travelVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @param travelId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRANSFER_EDIT, method = RequestMethod.GET)
	public String editTransfer(final Model model, @ModelAttribute TransferVo transferVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@RequestParam(value = "id", required = true) Long transferId, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			transferVo = transferService.getTransferById(transferId);
			model.addAttribute("transferVo", transferVo);
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_TRANSFER_PAGE;
	}

	/**
	 * method to delete travel
	 * 
	 * @param model
	 * @param travelId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRANSFER_DELETE, method = RequestMethod.GET)
	public String deleteTransfer(Model model, @RequestParam(value = "id", required = true) Long transferId) {
		try {
			transferService.deleteTransfer(transferId);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.TRANSFER_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			return "redirect:" + RequestConstants.TRANSFER_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.TRANSFER_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.TRANSFER_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.TRANSFER_DELETE_SUCCESS;
	}

	/**
	 * method to upload document
	 * 
	 * @param model
	 * @param travelVo
	 * @param authEmployee
	 * @param authEmployeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRANSFER_DOCUMENT_UPLOAD, method = RequestMethod.POST)
	public String uploadDocument(final Model model, @ModelAttribute TransferVo transferVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		try {
			if (transferVo.getTransferId() != null) {
				transferVo.setDocumentsVos(uploadDocuments(transferVo.getFiles()));
				transferService.updateDocument(transferVo);
			}
		} catch (ItemNotFoundException e) {

		} catch (Exception e) {

		}
		return "redirect:" + RequestConstants.TRANSFER_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.TRANSFER_DOCUMENTS_SUCCESS;
	}

	/**
	 * method to upload travel documents to server
	 * 
	 * @param files
	 * @return
	 */
	private List<TransferDocumentsVo> uploadDocuments(List<MultipartFile> files) {
		List<TransferDocumentsVo> documentVos = new ArrayList<TransferDocumentsVo>();
		try {
			for (MultipartFile file : files) {
				if (!file.isEmpty()) {
					String url = FileUpload.uploadDocument(MessageConstants.TRANSFER_MODULE, file);
					if (url != null) {
						TransferDocumentsVo vo = new TransferDocumentsVo();
						vo.setUrl(url);
						vo.setFileName(file.getOriginalFilename());
						documentVos.add(vo);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return documentVos;
	}

	/**
	 * method to download document
	 * 
	 * @param request
	 * @param response
	 * @param url
	 */
	@RequestMapping(value = RequestConstants.TRANSFER_DOCUMENT_DOWNLOAD, method = RequestMethod.GET)
	public void downloadDocument(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "url", required = true) String url) {
		try {
			SFTPOperation operation = new SFTPOperation();
			operation.downloadFileFromSFTP(url, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * method to delete document
	 * 
	 * @param url
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRANSFER_DOCUMENT_DELETE, method = RequestMethod.GET)
	public String deleteDocument(@RequestParam(value = "url", required = true) String url) {
		try {
			SFTPOperation operation = new SFTPOperation();
			Boolean result = operation.removeFileFromSFTP(url);
			if (result)
				transferService.deleteDocument(url);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.TRANSFER_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			return "redirect:" + RequestConstants.TRANSFER_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.TRANSFER_DOCUMENTS_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.TRANSFER_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.TRANSFER_DOCUMENTS_DELETE_SUCCESS;
	}

	/**
	 * method to view travel
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRANSFER_VIEW, method = RequestMethod.GET)
	public String viewTransfer(final Model model, @ModelAttribute TransferVo transferVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@RequestParam(value = "id", required = true) Long transferId,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			transferVo.setEmployee(authEmployeeDetails.getName());
			transferVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			transferVo = transferService.getTransferById(transferId);
			if(transferVo.getSuperiorCode().equalsIgnoreCase(authEmployeeDetails.getEmployeeCode()))
				transferVo.setViewStatus(true);
			else
				transferVo.setViewStatus(false);
			model.addAttribute("transferVo", transferVo);
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute("status", statusService.listAllStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.TRANSFER_VIEW_PAGE;
	}

	/**
	 * method to update status
	 * 
	 * @param model
	 * @param travelVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TRANSFER_UPDATE_STATUS, method = RequestMethod.POST)
	public String updateTrabsferStatus(final Model model, @ModelAttribute TransferVo transferVo) {
		try {
			if (transferVo.getTransferId() != null)
				transferService.updateStatus(transferVo);
		} catch (ItemNotFoundException e) {
			return "redirect:" + RequestConstants.TRANSFER_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			return "redirect:" + RequestConstants.TRANSFER_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.TRANSFER_STATUS_FAILED;
		}
		return "redirect:" + RequestConstants.TRANSFER_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.TRANSFER_STATUS_SUCCESS;
	}
}