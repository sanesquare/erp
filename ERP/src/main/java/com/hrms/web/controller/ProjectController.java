package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.views.DocumentView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dao.ProjectStatusDao;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.ProjectService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.ProjectDocumentsVo;
import com.hrms.web.vo.ProjectJsonVo;
import com.hrms.web.vo.ProjectVo;
import com.hrms.web.vo.RoleSessionVo;
import com.hrms.web.vo.SFTPFilePropertiesVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 10-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails" , "roles"})
public class ProjectController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private ProjectStatusDao projectStatusDao;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private EmployeeService employeeService;

	/**
	 * method to handle addProject page get request
	 * 
	 * @param model
	 * @return PageNameConstatnts.PROJECTS_PAGE
	 */
	@RequestMapping(value = RequestConstants.PROJECTS_URL, method = RequestMethod.GET)
	public String projectPageGetRequestHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		logger.info("inside>> ProjecController... projectPageGetRequestHandler");
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String erroMessage = request.getParameter(MessageConstants.ERROR);
		if(successMessage!=null)
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
		else if(erroMessage!=null)
			model.addAttribute(MessageConstants.ERROR, erroMessage);
		try {
			model.addAttribute("projects", projectService.listAllProjects());
			model.addAttribute("rolesMap", rolesMap);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.PROJECTS_PAGE;
	}


	/**
	 * method to handle addProject page get request
	 * 
	 * @param model
	 * @return PageNameConstatnts.PROJECTS_PAGE
	 */
	@RequestMapping(value = RequestConstants.ADD_PROJECTS_URL, method = RequestMethod.GET)
	public String addProjectPageGetRequestHandler(final Model model, @ModelAttribute ProjectVo projectVo
			,HttpServletRequest request , @ModelAttribute("roles") SessionRolesVo rolesMap) {
		logger.info("inside>> ProjecController... addProjectPageGetRequestHandler");
		String message=request.getParameter(MessageConstants.ERROR);
		if(message!=null){
			model.addAttribute(MessageConstants.ERROR, message);
		}
		try {
			model.addAttribute("branch", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("employees", employeeService.listEmployeesDeetails());
			model.addAttribute("status", projectStatusDao.listAllProjectStatus());
			model.addAttribute("rolesMap", rolesMap);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.ADD_PROJECTS_PAGE;
	}

	/**
	 * method to save project
	 * 
	 * @param model
	 * @param projectVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_PROJECT_URL, method = RequestMethod.POST)
	public String saveProjectPost(final Model model, @ModelAttribute ProjectVo projectVo, HttpServletRequest request) {
		logger.info("inside>> ProjecController... saveProject");
		if(request.getParameter("uploadDocuments")!=null){
			try {
				if(projectVo.getProjectId()!=null)
				{
					List<ProjectDocumentsVo> documentsVos = uploadDocuments(projectVo.getProject_file_upload());
					if(documentsVos.size()<=0){
						model.addAttribute(MessageConstants.ERROR, MessageConstants.FILE_UPLOAD_FAILED);
					}else{
						projectVo.setProjectDocuments(documentsVos);
						projectService.updateProjectDocument(projectVo);
						model.addAttribute(MessageConstants.SUCCESS, MessageConstants.FILE_UPLOAD_SUCCESS);
					}
				}
			} catch (Exception e) {
				model.addAttribute(MessageConstants.ERROR, MessageConstants.FILE_UPLOAD_FAILED);
				logger.info(e.getMessage());
				e.printStackTrace();
				return "redirect:" + RequestConstants.ADD_PROJECTS_URL + "?" + MessageConstants.ERROR + "="
				+ MessageConstants.WRONG;
			}
		}
		return "redirect:"+RequestConstants.PROJECTS_URL;
	}


	/**
	 * method to upload project documents
	 * @param files
	 * @return
	 */
	private List<ProjectDocumentsVo> uploadDocuments(List<MultipartFile> files){
		List<ProjectDocumentsVo> documentsVos = new ArrayList<ProjectDocumentsVo>();
		for(MultipartFile file : files){
			if(! file.isEmpty()){
				String url = FileUpload.uploadDocument(MessageConstants.PROJECT_MODULE, file);
				if(url!=null){
					ProjectDocumentsVo vo = new ProjectDocumentsVo();
					vo.setUrl(url);
					vo.setFileName(file.getOriginalFilename());
					documentsVos.add(vo);
				}
			}
		}
		return documentsVos;
	}


	/**
	 * method to update project
	 * 
	 * @param model
	 * @param projectVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_PROJECT_URL, method = RequestMethod.POST)
	public String updateProject(final Model model, @ModelAttribute ProjectVo projectVo) {
		try {
			//projectService.updateProject(projectVo);
			// model.addAttribute(MessageConstants.SUCCESS,
			// MessageConstants.PROJECT_UPDATE_SUCCESS);
			// model.addAttribute("projects", projectService.listAllProjects());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
			return PageNameConstatnts.ADD_PROJECTS_PAGE;
		}
		return "redirect:" + RequestConstants.PROJECTS_URL + "?" + MessageConstants.SUCCESS + "="
		+ MessageConstants.PROJECT_UPDATE_SUCCESS;
	}

	/**
	 * save project get
	 * 
	 * @param model
	 * @param projectVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_PROJECT_URL, method = RequestMethod.GET)
	public String saveProjectGet(final Model model, @ModelAttribute ProjectVo projectVo
			, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		logger.info("inside>> ProjecController... saveProject");
		try {
			model.addAttribute("projects", projectService.listAllProjects());
		} catch (Exception e) {
			logger.info(e.getMessage());

		}
		return PageNameConstatnts.PROJECTS_PAGE;
	}

	/**
	 * method to display selected project's details
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_PROJECT_DETAILS_URL, method = RequestMethod.GET)
	public String viewProjectDetails(final Model model, HttpServletRequest request
			, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		logger.info("inside>> ProjecController... viewProjectDetails");
		try {
			logger.info("inside>> ProjecController... viewProjectDetails ");
			model.addAttribute("project", projectService.findProjectById(Long.parseLong(request.getParameter("pid"))));
			model.addAttribute("rolesMap", rolesMap);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.VIEW_PROJECT_DETAILS_PAGE;
	}

	/**
	 * method to reach edit project page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_PROJECT_URL, method = RequestMethod.GET)
	public String editProjectDetailsGet(final Model model, HttpServletRequest request,
			@ModelAttribute ProjectVo projectVo , @ModelAttribute("roles") SessionRolesVo rolesMap) {
		logger.info("inside>> ProjecController... editProjectDetailsGet");
		try {
			model.addAttribute("branch", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("projectVo", projectService.findProjectById(Long.parseLong(request.getParameter("pid"))));
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("status", projectStatusDao.listAllProjectStatus());
			model.addAttribute("rolesMap", rolesMap);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.ADD_PROJECTS_PAGE;
	}

	/**
	 * method to delete project
	 * 
	 * @param model
	 * @param request
	 * @param projectVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_PROJECT_URL, method = RequestMethod.GET)
	public String deleteProject(final Model model, HttpServletRequest request, @ModelAttribute ProjectVo projectVo) {
		logger.info("inside>> ProjecController... deleteProject");
		try {
			projectService.deleteProject(Long.parseLong(request.getParameter("pid")));
			model.addAttribute("projects", projectService.listAllProjects());
			// model.addAttribute(MessageConstants.SUCCESS,
			// MessageConstants.PROJECT_DELETE_SUCCESS);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return "redirect:" + RequestConstants.PROJECTS_URL + "?" + MessageConstants.SUCCESS + "="
		+ MessageConstants.PROJECT_DELETE_SUCCESS;
	}

	/**
	 * method to download and show document
	 * 
	 * @param model
	 * @param request
	 * @param projectVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_PROJECT_DOCUMENT, method = RequestMethod.GET)
	public void viewDocumentGet(final Model model, HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute ProjectVo projectVo) {
		String url = request.getParameter("durl");
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			sftpOperation.downloadFileFromSFTP(url, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * method to delete document
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_PROJECT_DOCUMENT, method = RequestMethod.GET)
	public String deleteDocumentGet(final Model model, HttpServletRequest request) {
		String url = request.getParameter("durl");
		Boolean result = false;
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			result = sftpOperation.removeFileFromSFTP(url);
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.PROJECTS_URL + "?" + MessageConstants.ERROR + "="
			+ MessageConstants.DOCUMENT_DELETED_FAILED;
		}
		if (result) {
			try {
				projectService.deleteProjectDocument(url);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "redirect:" + RequestConstants.PROJECTS_URL + "?" + MessageConstants.SUCCESS + "="
			+ MessageConstants.DOCUMENT_DELETED_SUCCESSFULLY;
		} else {
			return "redirect:" + RequestConstants.PROJECTS_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.DOCUMENT_DELETED_FAILED;
		}
	}







	/**
	 * method to save project details
	 * @param projectVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_PROJECT_INFO_URL, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveProject(@RequestBody ProjectJsonVo projectVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			jsonResponse.setResult(projectService.saveProject(projectVo));
			jsonResponse.setStatus(MessageConstants.PROJECT_SAVE_SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			jsonResponse.setStatus(MessageConstants.PROJECT_SAVE_FAILED);
		}
		return jsonResponse;
	}


	/**
	 * method to save project details
	 * @param projectVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_PROJECT_STATUS, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse saveProjectStatus(@RequestBody ProjectJsonVo projectVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			projectService.updateProjectStatus(projectVo);
			jsonResponse.setStatus(MessageConstants.PROJECT_STATUS_SAVE_SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			jsonResponse.setStatus(MessageConstants.PROJECT_SAVE_FAILED);
		}
		return jsonResponse;
	}


	/**
	 * method to update project information
	 * @param projectVo
	 * @param session
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_PROJECT_INFORMATION, method = RequestMethod.POST,
			headers={"Content-type=application/json"})
	public @ResponseBody JsonResponse updateProjectInformation(@RequestBody ProjectJsonVo projectVo,HttpSession session){
		JsonResponse jsonResponse=new JsonResponse();
		try{
			projectService.updateProject(projectVo);
			jsonResponse.setStatus(MessageConstants.PROJECT_SAVE_SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonResponse;
	}

}