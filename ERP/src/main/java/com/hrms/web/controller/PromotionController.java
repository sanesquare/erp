package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Superiors;
import com.hrms.web.entities.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.service.EmployeeDesignationService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.PromotionService;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.DepartmentDocumentsVo;
import com.hrms.web.vo.DepartmentVo;
import com.hrms.web.vo.PromotionDocumentVo;
import com.hrms.web.vo.PromotionVo;

/**
 * 
 * @author Sangeeth and Bibin
 *
 */
@Controller
@SessionAttributes({ "promotionVo" })
public class PromotionController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private EmployeeDesignationService employeeDesignationService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private PromotionService promotionService;

	/**
	 * method to handle promotion page get request
	 * 
	 * @param model
	 * @return PageNameConstatnts.PROMOTION_PAGE
	 */
	@RequestMapping(value = RequestConstants.PROMOTION_URL, method = RequestMethod.GET)
	public String promotionPageGetRequestHandler(final Model model, HttpServletRequest request) {
		logger.info("inside>> PromotionController... promotionPageMappingGet");

		try {
			model.addAttribute("promotionVo",new PromotionVo());
			model.addAttribute("promotions", promotionService.listAllPromotionDetails());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.PROMOTION_PAGE;
	}

	/**
	 * method to get superior list of an employee
	 * 
	 * @param model
	 * @param employeeId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_SUPERIORS_LIST_URL, method = RequestMethod.GET)
	public String promotionPageGetEmployeeSuperiorRequestHandler(final Model model,
			@RequestParam(value = "employeeId", required = true) Long employeeId) {
		logger.info("inside>> PromotionController... promotionPageMappingGet");

		try {
			model.addAttribute(
					"superiorList",
					Superiors.getSuperiors(employeeService.getEmployeesDetails(employeeId).getSuperiorSubordinateVo()
							.getSuperiors()));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.SUPERIORS_LIST_PAGE;
	}

	/**
	 * method to add department
	 * 
	 * @param model
	 * @param
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_PROMOTION_URL, method = RequestMethod.GET)
	public String addPromotionPageGetRequestHandler(final Model model, 
			@ModelAttribute("promotionVo") PromotionVo promotionVo) {
		logger.info("inside>> PromotionController... addPromotionPageMappingGet");
		
		try {
			/*Employee employee = new Employee();
			employee.setId(1L);*/
			model.addAttribute("designations", employeeDesignationService.listAllDesignations());
			model.addAttribute("employee", employeeService.listAllEmployee());
/*			model.addAttribute(
					"superiors",
					Superiors.getSuperiors(employeeService.getEmployeesDetails(1L).getSuperiorSubordinateVo()
							.getSuperiors()));*/

		} catch (Exception e) {
			logger.info(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.ADD_PROMOTION_PAGE;
	}

	/**
	 * method to save Department
	 * 
	 * @param model
	 * @param departmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_PROMOTION_URL, method = RequestMethod.POST)
	public ModelAndView savePromotion(RedirectAttributes redirectAttributes,@ModelAttribute PromotionVo promotionVo) {
		logger.info("inside>> Promotion Controller... savePromotion");
		ModelAndView view=new ModelAndView(new RedirectView(RequestConstants.ADD_PROMOTION_URL));
		try {
			
			if(promotionVo.getId()!=null)
			{
				System.out.println("promotion id is----------------------------"+promotionVo.getId());
				promotionService.updatePromotion(promotionVo);
				promotionVo.setPromotionDocument(uploadDocuments(promotionVo.getPromotion_file_upload()));
				promotionService.updatePromotionDocument(promotionVo);
				
			}
			else
			{
				promotionService.savePromotion(promotionVo);
			}
			redirectAttributes.addFlashAttribute("promotionVo", promotionVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.PROMOTION_SAVE_SUCCESS);
			
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
			logger.info(e.getMessage());
		}
		return view;
	}

	/**
	 * method to delete promotion
	 * 
	 * @param model
	 * @param request
	 * @param promotiontVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_PROMOTION_URL, method = RequestMethod.GET)
	public String deletePromotion(final Model model, HttpServletRequest request, @ModelAttribute PromotionVo promotionVo) {
		logger.info("inside>> PromotionController... deletePromotion");
		try {
			promotionService.deletePromotion(Long.parseLong(request.getParameter("pid")));
			model.addAttribute("promotions", promotionService.listAllPromotionDetails());
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.PROMOTION_DELETE_SUCCESS);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.PROMOTION_PAGE;

	}

	/**
	 * method to reach edit department page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_PROMOTION_URL, method = RequestMethod.GET)
	public String editPromotionDetailsGet(final Model model, HttpServletRequest request,
			@ModelAttribute PromotionVo promotionVo) {
		logger.info("inside>> PromotionController... editPromotionDetailsGet");
		try {
			model.addAttribute("promotionVo",
					promotionService.findPromotionById(Long.parseLong(request.getParameter("pid"))));
			model.addAttribute("designations", employeeDesignationService.listAllDesignations());
			model.addAttribute("employee", employeeService.listAllEmployee());

		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return PageNameConstatnts.EDIT_PROMOTION_PAGE;
	}

	/**
	 * method to update department
	 * 
	 * @param model
	 * @param departmentVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_PROMOTION_URL, method = RequestMethod.POST)
	public ModelAndView updatePromotion(final Model model, HttpServletRequest request, RedirectAttributes redirectAttributes,@ModelAttribute PromotionVo promotionVo) {
		logger.info("inside>> PromotionController... updatePromotion");
		ModelAndView view=new ModelAndView(new RedirectView(RequestConstants.ADD_PROMOTION_URL));
		try {
			promotionService.updatePromotion(promotionVo);
			redirectAttributes.addFlashAttribute("promotionVo", promotionVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.PROMOTION_UPDATE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info(e.getMessage());
		}
		return view;
	}
	
	/**
	 * method to delete document
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_PROMOTION_DOCUMENT, method = RequestMethod.GET)
	public String deleteDocumentGet(final Model model, HttpServletRequest request) {
		String url = request.getParameter("durl");
		Long document_id = Long.parseLong(request.getParameter("doc_id"));
		Boolean result = false;
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			result = sftpOperation.removeFileFromSFTP(url);
		} catch (Exception e) {
			e.printStackTrace();
			return PageNameConstatnts.PROMOTION_PAGE;
		}
		if (result) {
			try {
				promotionService.deletePromotionDocument(document_id);
				model.addAttribute(MessageConstants.ERROR, MessageConstants.PROMOTION_DOCUMENT_DELETE_SUCCESS);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return PageNameConstatnts.PROMOTION_PAGE;
		} else {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.PROMOTION_DOCUMENT_DELETE_FAIL);
			return PageNameConstatnts.PROMOTION_PAGE;
		}
	}

	/**
	 * method to download and show document
	 * 
	 * @param model
	 * @param request
	 * @param projectVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_PROMOTION_DOCUMENT, method = RequestMethod.GET)
	public void viewDocumentGet(final Model model, HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute PromotionVo promotionVo) {
		String url = request.getParameter("durl");
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			sftpOperation.downloadFileFromSFTP(url, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * method to upload project documents
	 * @param files
	 * @return
	 */
	private List<PromotionDocumentVo> uploadDocuments(List<MultipartFile> files){
		List<PromotionDocumentVo> promotionDocumentsVos = new ArrayList<PromotionDocumentVo>();
		for(MultipartFile file : files){
			if(! file.isEmpty()){
				String url = FileUpload.uploadDocument("promotion", file);
				if(url!=null){
					PromotionDocumentVo vo = new PromotionDocumentVo();
					vo.setUrl(url);
					vo.setFileName(file.getOriginalFilename());
					promotionDocumentsVos.add(vo);
				}
			}
		}
		return promotionDocumentsVos;
	}
}
