package com.hrms.web.controller;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Employee;
import com.hrms.web.dto.EsiShare;
import com.hrms.web.dto.PFShare;
import com.hrms.web.dto.StringList;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BonusDateService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.EsiSyntaxService;
import com.hrms.web.service.ExtraPaySlipItemService;
import com.hrms.web.service.LwfService;
import com.hrms.web.service.PayRollOptionService;
import com.hrms.web.service.PaySlipItemService;
import com.hrms.web.service.ProfesionalTaxService;
import com.hrms.web.service.ProfidentFundSyntaxService;
import com.hrms.web.service.SalaryService;
import com.hrms.web.service.TaxExcludedEmployeeService;
import com.hrms.web.service.TaxRuleService;
import com.hrms.web.util.DateComponents;
import com.hrms.web.util.PayRollCalculator;
import com.hrms.web.vo.BonusDateVo;
import com.hrms.web.vo.EsiSyntaxVo;
import com.hrms.web.vo.ExtraPaySlipItemVo;
import com.hrms.web.vo.LwfVo;
import com.hrms.web.vo.PayRollOptionVo;
import com.hrms.web.vo.PaySlipItemVo;
import com.hrms.web.vo.ProfesionalTaxVo;
import com.hrms.web.vo.ProfidentFundSyntaxVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.TaxExcludedEmployeeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 22-April-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class PayRollStructureController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private PayRollOptionService payRollOptionService;

	@Autowired
	private PaySlipItemService paySlipItemService;

	@Autowired
	private ExtraPaySlipItemService extraPaySlipItemService;

	@Autowired
	private BonusDateService bonusDateService;

	@Autowired
	private ProfidentFundSyntaxService profidentFundSyntaxService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private TaxExcludedEmployeeService taxExcludedEmployeeService;

	@Autowired
	private TaxRuleService taxRuleService;

	@Autowired
	private SalaryService salaryService;

	@Autowired
	private EsiSyntaxService esiSyntaxService;

	@Autowired
	private LwfService lwfService;

	@Autowired
	private ProfesionalTaxService profesionalTaxService;

	/**
	 * method to load payroll structure index page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYROLL_STRUCTURE_URL, method = RequestMethod.GET)
	public String getPayrollIndexPage(final Model model, @ModelAttribute PayRollOptionVo payRollOption,
			BindingResult result, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			payRollOption = payRollOptionService.findPayRollOption();
			if (payRollOption == null)
				payRollOption = new PayRollOptionVo();
			model.addAttribute("payRollOption", payRollOption);
			model.addAttribute("employees", Employee.getEmployees(employeeService.listEmployeesWithPayroll()));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_PAYROLL_STRUCTURE_PAGE;
	}

	/**
	 * method to save or update payroll options
	 * 
	 * @param payRollOption
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYROLL_OPTIONS_SAVE_URL, method = RequestMethod.POST)
	public ModelAndView savePayRollOptionsWithPost(@ModelAttribute PayRollOptionVo payRollOption,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.PAYROLL_STRUCTURE_URL));
		try {
			if (payRollOption.getPerDaySalaryCalculation() == null) {
				payRollOption.setPerDaySalaryCalculation("false");
				payRollOption.setPerDaySalaryCalculationDays(0);
				payRollOption.setPerDaySalaryCalculationType("Month");
			}
			if (payRollOption.getPerDaySalaryCalculationDays() == null)
				payRollOption.setPerDaySalaryCalculationDays(0);
			payRollOptionService.saveOrUpdatePayRollOption(payRollOption);
			redirectAttributes
					.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_PAYROLL_OPTIONS_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to list all pay slip items
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYROLL_PAYSLIP_ITEM_LIST_URL, method = RequestMethod.GET)
	public String listAllPaySlipItemList(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("paySlipItems", paySlipItemService.findAllPaySlipItem());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_PAYROLL_PAYSLIP_ITEM_LIST_PAGE;
	}

	/**
	 * method to list all extra pay slip items
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYROLL_EXTRA_PAYSLIP_ITEM_LIST_URL, method = RequestMethod.GET)
	public String listAllExtraPaySlipItemList(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("extraPaySlipItems", extraPaySlipItemService.findAllExtraPaySlipItem());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_PAYROLL_EXTRA_PAYSLIP_ITEM_LIST_PAGE;
	}

	/**
	 * method to set empty page
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYROLL_PAYSLIP_ITEM_EMPTY_URL, method = RequestMethod.GET)
	public String getEmptyPaySlipItemList(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		return PageNameConstatnts.GET_PAYROLL_PAYSLIP_ITEM_EMPTY_LIST_PAGE;
	}

	/**
	 * method to load pay slip item tab
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYROLL_PAYSLIP_ITEM_TAB_URL, method = RequestMethod.GET)
	public String getPaySlipItemTabElements(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("paySlipItems", paySlipItemService.findAllPaySlipItem());
			model.addAttribute("extraPaySlipItems", extraPaySlipItemService.findAllExtraPaySlipItem());
			model.addAttribute("bonuDate", bonusDateService.findBonusDate());
			model.addAttribute("profidentFundSyntax", profidentFundSyntaxService.findProfidentFundSyntax());
			model.addAttribute("esiSyntax", esiSyntaxService.findEsiSyntax());
			model.addAttribute("yearList", DateComponents.getYear());
			model.addAttribute("monthList", DateComponents.getMonth());
			model.addAttribute("dayList", DateComponents.getDay());
			model.addAttribute("lwf", lwfService.findLwf());
			model.addAttribute("profersionalTax", profesionalTaxService.findProfesionalTax());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_PAYROLL_PAYSLIP_ITEM_TAB_PAGE;
	}

	/**
	 * method to load tax rules item tab
	 * 
	 * @param model
	 * @param taxExcludedEmployeeVo
	 * @param result
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYROLL_TAXRULE_ITEM_TAB_URL, method = RequestMethod.GET)
	public String getTaxRuleItemTabElements(final Model model,
			@ModelAttribute TaxExcludedEmployeeVo taxExcludedEmployeeVo, BindingResult result,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			taxExcludedEmployeeVo.setExcludedEmployeeCodes(taxExcludedEmployeeService.findTaxExcludedEmployeeList()
					.getExcludedEmployeeCodes());
			model.addAttribute("payRollEmployees", Employee.getEmployees(employeeService.listEmployeesWithPayroll()));
			model.addAttribute("taxRules", taxRuleService.findAllTaxRule());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_PAYROLL_TAXRULE_ITEM_TAB_PAGE;
	}

	/**
	 * method to save tax exclude employees
	 * 
	 * @param taxExcludedEmployeeVo
	 * @param result
	 * @return
	 */
	@RequestMapping(value = RequestConstants.TAXEXCLUDE_EMPLOYEES_SAVE_URL, method = RequestMethod.POST)
	public String saveTaxExcludeEmployees(final Model model,
			@ModelAttribute TaxExcludedEmployeeVo taxExcludedEmployeeVo, BindingResult result,
			@RequestBody StringList excludedEmployees, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			taxExcludedEmployeeVo.setExcludedEmployeeCodes(excludedEmployees.getItem());
			taxExcludedEmployeeService.saveTaxExcludedEmployee(taxExcludedEmployeeVo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_PAYROLL_TAXRULE_ITEM_TAB_PAGE;
	}

	/**
	 * method to save pay slip item
	 * 
	 * @param paySlipItemVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSLIP_ITEM_SAVE_URL, method = RequestMethod.POST)
	public String savePaySlipItemDetailsWithPost(@RequestBody PaySlipItemVo paySlipItemVo) {
		try {
			paySlipItemService.saveOrUpdatePaySlipItem(paySlipItemVo);
			return "redirect:" + RequestConstants.PAYROLL_PAYSLIP_ITEM_LIST_URL;
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			return "redirect:" + RequestConstants.PAYROLL_PAYSLIP_ITEM_EMPTY_URL;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return "redirect:" + RequestConstants.PAYROLL_PAYSLIP_ITEM_EMPTY_URL;
		}
	}

	/**
	 * method to save extra pay slip item
	 * 
	 * @param extraPaySlipItemVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EXTRA_PAYSLIP_ITEM_SAVE_URL, method = RequestMethod.POST)
	public String saveExtraPaySlipItemDetailsWithPost(@RequestBody ExtraPaySlipItemVo extraPaySlipItemVo) {
		try {
			extraPaySlipItemService.saveOrUpdateExtraPaySlipItem(extraPaySlipItemVo);
			return "redirect:" + RequestConstants.PAYROLL_EXTRA_PAYSLIP_ITEM_LIST_URL;
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			return "redirect:" + RequestConstants.PAYROLL_PAYSLIP_ITEM_EMPTY_URL;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return "redirect:" + RequestConstants.PAYROLL_PAYSLIP_ITEM_EMPTY_URL;
		}
	}

	/**
	 * method to update pay slip item
	 * 
	 * @param paySlipItemVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PAYSLIP_ITEM_UPDATE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse updatePaySlipItemDetailsWithPost(@RequestBody PaySlipItemVo paySlipItemVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			paySlipItemService.saveOrUpdatePaySlipItem(paySlipItemVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to update extra pay slip item
	 * 
	 * @param extraPaySlipItemVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EXTRA_PAYSLIP_ITEM_UPDATE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse updateExtraPaySlipItemDetailsWithPost(
			@RequestBody ExtraPaySlipItemVo extraPaySlipItemVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			extraPaySlipItemService.saveOrUpdateExtraPaySlipItem(extraPaySlipItemVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to list bonus date
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.BONUS_DATE_LIST_URL, method = RequestMethod.GET)
	public String listBonuDateList(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("bonuDate", bonusDateService.findBonusDate());
			model.addAttribute("yearList", DateComponents.getYear());
			model.addAttribute("monthList", DateComponents.getMonth());
			model.addAttribute("dayList", DateComponents.getDay());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_BONUS_DATE_LIST_PAGE;
	}

	/**
	 * method to save bonus date details
	 * 
	 * @param bonusDateVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.BONUS_DATE_SAVE_URL, method = RequestMethod.POST)
	public String saveBonusDateDetailsWithPost(@RequestBody BonusDateVo bonusDateVo) {
		try {
			bonusDateService.saveOrUpdateBonusDate(bonusDateVo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.BONUS_DATE_LIST_URL;
	}

	/**
	 * method to get PF syntax list
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PF_SYNTAX_LIST_URL, method = RequestMethod.GET)
	public String getPFSyntaxList(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("profidentFundSyntax", profidentFundSyntaxService.findProfidentFundSyntax());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_PF_SYNTAX_LIST_PAGE;
	}

	/**
	 * method to get ESI syntax list
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ESI_SYNTAX_LIST_URL, method = RequestMethod.GET)
	public String getESISyntaxList(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("esiSyntax", esiSyntaxService.findEsiSyntax());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_ESI_SYNTAX_LIST_PAGE;
	}

	/**
	 * method to save PF syntax details
	 * 
	 * @param profidentFundSyntaxVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PF_SYNTAX_SAVE_URL, method = RequestMethod.POST)
	public String savePFSyntaxDetailsWithPost(@RequestBody ProfidentFundSyntaxVo profidentFundSyntaxVo) {
		try {
			profidentFundSyntaxService.saveOrUpdateProfidentFundSyntax(profidentFundSyntaxVo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.PF_SYNTAX_LIST_URL;
	}

	/**
	 * method to save ESI syntax details
	 * 
	 * @param esiSyntaxVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ESI_SYNTAX_SAVE_URL, method = RequestMethod.POST)
	public String saveESISyntaxDetailsWithPost(@RequestBody EsiSyntaxVo esiSyntaxVo) {
		try {
			esiSyntaxService.saveOrUpdateEsiSyntax(esiSyntaxVo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.ESI_SYNTAX_LIST_URL;
	}

	/**
	 * method to save ESI syntax details
	 * 
	 * @param esiSyntaxVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LWF_SAVE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveLWFDetailsWithPost(@RequestBody LwfVo lwfVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			lwfService.saveLwf(lwfVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to save Profesional tax
	 * 
	 * @param profesionalTaxVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.PROFESIONAL_TAX_SAVE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveProfesionalTaxWithPost(@RequestBody ProfesionalTaxVo profesionalTaxVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			profesionalTaxService.saveOrUpdate(profesionalTaxVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to find employee pf share
	 * 
	 * @param employeeCode
	 * @return jsonResponse
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_PF_SHARE_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getEmployeePFShare(@RequestParam("employeeCode") String employeeCode) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			PayRollCalculator calculator = new PayRollCalculator();
			PFShare pfShare = PayRollCalculator.getPFShare(
					new BigDecimal(salaryService.getMonthlySalaryByEmployee(employeeCode).getMonthlyGrossSalary()),
					profidentFundSyntaxService.findProfidentFundSyntax().getEmployeeSyntax(),
					profidentFundSyntaxService.findProfidentFundSyntax().getEmployerSyntax(),
					calculator.getPayRollComponents(paySlipItemService.findAllPaySlipItem()));
			jsonResponse.setResult(pfShare);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to find employee ESI share
	 * 
	 * @param employeeCode
	 * @return jsonResponse
	 */
	@RequestMapping(value = RequestConstants.GET_EMPLOYEE_ESI_SHARE_URL, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getEmployeeESIShare(@RequestParam("employeeCode") String employeeCode) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			EsiSyntaxVo esiSyntaxVo = esiSyntaxService.findEsiSyntax();
			EsiShare esiShare = PayRollCalculator.getESIShare(
					new BigDecimal(salaryService.getMonthlySalaryByEmployee(employeeCode).getMonthlyGrossSalary()),
					esiSyntaxVo.getEmployeeSyntax(), esiSyntaxVo.getEmployerSyntax(), esiSyntaxVo);
			jsonResponse.setResult(esiShare);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}
}
