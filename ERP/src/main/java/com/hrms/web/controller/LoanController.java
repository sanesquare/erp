package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.bouncycastle.ocsp.Req;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Superiors;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.notifications.NotificationContent;
import com.hrms.web.notifications.PushNotification;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.LoanService;
import com.hrms.web.service.LoanStatusService;
import com.hrms.web.service.SecondLevelForwaderService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.LoanDocumentVo;
import com.hrms.web.vo.LoanVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Jithin Mohan
 * @since 14-April-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "loanVo", "thisUser", "roles" })
public class LoanController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private LoanService loanService;

	@Autowired
	private LoanStatusService loanStatusService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private UserService userService;

	@Autowired
	private SecondLevelForwaderService secondLevelForwaderService;

	@Autowired
	private PushNotification pushNotification;

	/**
	 * method to load loan page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LOAN_URL, method = RequestMethod.GET)
	public String loadAllLoans(final Model model, @ModelAttribute("thisUser") String thisUser,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("loanVo", new LoanVo());
			model.addAttribute("thisUser", thisUser);
			model.addAttribute("loanList", loanService.findAllLoanByEmployee(authEmployeeDetails.getEmployeeCode()));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.GET_LOAN_PAGE;
	}

	/**
	 * method to get add loan page
	 * 
	 * @param model
	 * @param authEmployee
	 * @param loanVo
	 * @param result
	 * @param authEmployeeDetails
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_LOAN_URL, method = RequestMethod.GET)
	public String createLoanPage(final Model model, @ModelAttribute("loanVo") LoanVo loanVo, BindingResult result,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			loanVo.setEmployee(authEmployeeDetails.getName() + " (" + authEmployeeDetails.getEmployeeCode() + ")");
			loanVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());

			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			if (loanVo.getLoanId() == null) {
				loanVo.setRecordedBy(authEmployeeDetails.getName());
				model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
			} else {
				loanVo.setRecordedBy(userService.findUser(loanVo.getRecordedBy()).getEmployee());
				model.addAttribute(MessageConstants.CREATED_ON, loanVo.getRecordedOn());
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
			}
			model.addAttribute("loanVo", loanVo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ADD_LOAN_PAGE;
	}

	/**
	 * method to save loan
	 * 
	 * @param loanVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_LOAN_URL, method = RequestMethod.POST)
	public ModelAndView saveLoanWithPost(@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute LoanVo loanVo, RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_LOAN_URL));
		try {
			loanService.saveOrUpdateLoan(loanVo);
			pushNotification.sendNotification(createNotificationContent(loanVo, authEmployeeDetails.getProfilePic()));
			redirectAttributes.addFlashAttribute("loanVo", loanVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_LOAN_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to create NotificationContent object
	 * 
	 * @param terminationVo
	 * @return content
	 */
	private NotificationContent createNotificationContent(LoanVo loanVo, String profilePic) {
		NotificationContent content = new NotificationContent();
		List<String> recipients = new ArrayList<String>();

		recipients.addAll(loanVo.getForwardTo());
		recipients.add(loanVo.getEmployeeCode());
		recipients.addAll(secondLevelForwaderService.findAllSecondLevelForwader().getForwaders());

		content.setContent("Loan Request");
		content.setUrl("ViewLoan?otp=" + loanVo.getLoanId());
		content.setPicture(profilePic);
		content.setRecipients(recipients);
		return content;
	}

	/**
	 * method to delete loan
	 * 
	 * @param loanId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LOAN_DELETE_PAGE_URL, method = RequestMethod.POST)
	public String deleteThisLoanWithPost(@RequestParam(value = "loanId", required = true) Long loanId) {
		try {
			loanService.deleteLoan(loanId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "redirect:" + RequestConstants.LOAN_LIST_PAGE_URL;
	}

	/**
	 * method to list all the loans
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LOAN_LIST_PAGE_URL, method = RequestMethod.GET)
	public String listAllWarnings(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("loanList", loanService.findAllLoan());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "test";
	}

	/**
	 * method to view loan information
	 * 
	 * @param loanId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_LOAN_URL, method = RequestMethod.GET)
	public String viewLoanPageWithGet(@RequestParam(value = "otp", required = true) Long loanId, final Model model,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			LoanVo loanVo = loanService.findLoanById(loanId);

			model.addAttribute("loanVo", loanVo);
			model.addAttribute("loanStatusList", loanStatusService.findAllLoanStatus());
			model.addAttribute(
					"superiorList",
					Superiors.getSuperiors(employeeService
							.getEmployeesDetails(
									employeeService.findAndReturnEmployeeByCode(loanVo.getEmployeeCode()).getId())
							.getSuperiorSubordinateVo().getSuperiors()));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PageNameConstatnts.VIEW_LOAN_PAGE;
	}

	/**
	 * method to update loan status
	 * 
	 * @param loanVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_LOAN_STATUS_URL, method = RequestMethod.POST)
	public ModelAndView updateOvertimeStatus(@ModelAttribute LoanVo loanVo, RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_LOAN_URL));
		try {
			loanService.updateLoanStatus(loanVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.UPDATE_LOAN_SUCCESS);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to edit loan
	 * 
	 * @param model
	 * @param loanId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_LOAN_URL, method = RequestMethod.GET)
	public String editLoanWithGet(final Model model, @RequestParam(value = "otp") Long loanId,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			LoanVo loanVo = loanService.findLoanById(loanId);
			loanVo.setRecordedBy(userService.findUser(loanVo.getRecordedBy()).getEmployee());
			model.addAttribute("loanVo", loanVo);
			model.addAttribute(
					"superiorList",
					Superiors.getSuperiors(employeeService
							.getEmployeesDetails(
									employeeService.findAndReturnEmployeeByCode(loanVo.getEmployeeCode()).getId())
							.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute(MessageConstants.CREATED_ON, loanVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_LOAN_PAGE;
	}

	/**
	 * method to list loan all documents
	 * 
	 * @param model
	 * @param loanId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LOAN_DOCUMENT_LIST_URL, method = RequestMethod.POST)
	public String listLoanAllDocuments(final Model model, @RequestParam(value = "loanId", required = true) Long loanId,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("loanDocumentList", loanService.findLoanById(loanId));
			model.addAttribute("loanId", loanId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.LOAN_DOCUMENTS_LIST_PAGE;
	}

	/**
	 * method to upload loan documents
	 * 
	 * @param loanId
	 * @param loanVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LOAN_DOCUMENTS_UPLOAD_URL, method = RequestMethod.POST)
	public @ResponseBody ModelAndView uploadLoanDocumentsWithPost(MultipartHttpServletRequest request,
			@ModelAttribute LoanVo loanVo, @RequestParam(value = "loanId", required = true) Long loanId) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_LOAN_URL));
		try {
			List<LoanDocumentVo> loanDocuments = uploadLoanDocuments(request);
			if (!loanDocuments.isEmpty()) {
				loanVo.setLoanId(loanId);
				loanVo.setLoanDocuments(loanDocuments);
				loanService.saveLoanDocument(loanVo);
			}
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return view;
		// return PageNameConstatnts.ADD_LOAN_PAGE;
	}

	/**
	 * method to upload files to SFTP
	 * 
	 * @param multipartFiles
	 * @return
	 */
	private List<LoanDocumentVo> uploadLoanDocuments(MultipartHttpServletRequest request) {
		List<LoanDocumentVo> questionVos = new ArrayList<LoanDocumentVo>();
		try {
			Iterator<String> itrator = request.getFileNames();
			while (itrator.hasNext()) {
				MultipartFile multipartFile = request.getFile(itrator.next());
				if (!multipartFile.isEmpty()) {
					String fileAccesUrl = FileUpload.uploadDocument(MessageConstants.LOAN_DOCUMENT_MODULE,
							multipartFile);
					if (fileAccesUrl != null) {
						LoanDocumentVo documentsVo = new LoanDocumentVo();
						documentsVo.setDocumentUrl(fileAccesUrl);
						documentsVo.setDocumentName(multipartFile.getOriginalFilename());
						questionVos.add(documentsVo);
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return questionVos;
	}

	/**
	 * method to delete loan document
	 * 
	 * @param documentId
	 * @param documentUrl
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LOAN_DOC_DELETE_URL, method = RequestMethod.POST)
	public String deleteLoanDocumentWithPost(@RequestParam(value = "documentId", required = true) Long documentId,
			@RequestParam(value = "documentUrl", required = true) String documentUrl) {
		try {
			SFTPOperation sftpOperation = new SFTPOperation();
			if (sftpOperation.removeFileFromSFTP(documentUrl))
				loanService.deleteLoanDocument(documentId);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.LOAN_DOCUMENTS_LIST_PAGE;
	}
}
