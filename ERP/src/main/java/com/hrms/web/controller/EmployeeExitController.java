package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.EmployeeExitService;
import com.hrms.web.service.EmployeeExitTypeService;
import com.hrms.web.service.UserService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeExitDocumentsVo;
import com.hrms.web.vo.EmployeeExitTypeVo;
import com.hrms.web.vo.EmployeeExitVo;
import com.hrms.web.vo.EmployeeVo;

/**
 * 
 * @author Jithin Mohan
 * @since 18-March-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "employeeExitCode", "thisUser" })
public class EmployeeExitController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private EmployeeExitService employeeExitService;

	@Autowired
	private EmployeeExitTypeService employeeExitTypeService;

	@Autowired
	private UserService userService;

	/**
	 * method to handle employeeExit list page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EXIT_LIST_URL, method = RequestMethod.GET)
	public String listEmployeeExitsWithGet(final Model model, @ModelAttribute("thisUser") String thisUser,
			@ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo) {
		model.addAttribute("employeeExits",
				employeeExitService.findAllEmployeeExitByEmployee(employeeVo.getEmployeeCode()));
		model.addAttribute("employeeExitCode", MessageConstants.DEFAULT_CODE);
		model.addAttribute("thisUser", thisUser);
		return PageNameConstatnts.EMPLOYEE_EXIT_LIST_PAGE;
	}

	/**
	 * method to handle employeeExit Create page
	 * 
	 * @param model
	 * @param employeeCode
	 * @param employeeExitVo
	 * @param result
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EXIT_CREATE_PAGE_URL, method = RequestMethod.GET)
	public String loadCreateEmployeeExitPageWithGet(final Model model,
			@ModelAttribute("employeeExitCode") String employeeCode,
			@ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo,
			@ModelAttribute EmployeeExitVo employeeExitVo, BindingResult result) {
		try {
			model.addAttribute("exitTypes", getAllExitTypes(employeeExitTypeService.findAllEmployeeExitType()));
			employeeExitVo = employeeExitService.findEmployeeExitByEmployee(employeeCode);
			if (employeeExitVo == null) {
				employeeExitVo = new EmployeeExitVo();
				employeeExitVo.setRecordedBy(employeeVo.getName());
				model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
				model.addAttribute(MessageConstants.EMP_EXIT_BUTTON_LABEL, MessageConstants.EMP_EXIT_BUTTON_1);
			} else {
				employeeExitVo.setRecordedBy(userService.findUser(employeeExitVo.getRecordedBy()).getEmployee());
				model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(DateFormatter
						.convertStringToDate(employeeExitVo.getExitDate())));
				model.addAttribute(MessageConstants.EMP_EXIT_BUTTON_LABEL, MessageConstants.EMP_EXIT_BUTTON_2);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		model.addAttribute("typeList", getTypeList());
		model.addAttribute("employeeExitVo", employeeExitVo);
		return PageNameConstatnts.EMPLOYEE_EXIT_ADD_PAGE;
	}

	/**
	 * method to save employeeExit details
	 * 
	 * @param redirectAttributes
	 * @param employeeExitVo
	 * @param result
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EXIT_SAVE_URL, method = RequestMethod.POST)
	public ModelAndView saveEmployeeExitWithPost(RedirectAttributes redirectAttributes,
			@ModelAttribute EmployeeExitVo employeeExitVo, BindingResult result) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.EMPLOYEE_EXIT_CREATE_PAGE_URL));
		try {
			employeeExitService.saveEmployeeExit(employeeExitVo);
			redirectAttributes.addFlashAttribute("employeeExitCode", employeeExitVo.getEmployeeCode());
			// model.addAttribute("employeeExits",
			// employeeExitService.findAllEmployeeExit());
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.EMPLOYEE_EXIT_SUCCESS);
		} catch (DuplicateItemException die) {
			logger.error(die.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, die.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		} /*
		 * finally { model.addAttribute("typeList", getTypeList()); }
		 */
		// return PageNameConstatnts.EMPLOYEE_EXIT_LIST_PAGE;
		// return "redirect:" + RequestConstants.EMPLOYEE_EXIT_CREATE_PAGE_URL;
		return view;
	}

	/**
	 * method to upload files to ftp server and return file informations
	 * 
	 * @param multipartFiles
	 * @return employeeExitDocumentsVos
	 */
	private List<EmployeeExitDocumentsVo> uploadEmpExtDocuments(MultipartHttpServletRequest request) {
		List<EmployeeExitDocumentsVo> employeeExitDocumentsVos = new ArrayList<EmployeeExitDocumentsVo>();
		try {
			Iterator<String> itrator = request.getFileNames();
			while (itrator.hasNext()) {
				MultipartFile multipartFile = request.getFile(itrator.next());
				if (!multipartFile.isEmpty()) {
					String fileAccesUrl = FileUpload.uploadDocument(MessageConstants.EMPLOYEE_EXIT_MODULE,
							multipartFile);
					if (fileAccesUrl != null) {
						EmployeeExitDocumentsVo documentVo = new EmployeeExitDocumentsVo();
						documentVo.setDocumentUrl(fileAccesUrl);
						documentVo.setDocumentName(multipartFile.getOriginalFilename());
						employeeExitDocumentsVos.add(documentVo);
					}
				}

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return employeeExitDocumentsVos;
	}

	/**
	 * method to handle EmployeeExit Edit page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EXIT_EDIT_URL, method = RequestMethod.GET)
	public String handleEditEmployeeExitPage(final Model model, final HttpServletRequest request) {
		EmployeeExitVo employeeExitVo = employeeExitService.findEmployeeExitById(Long.parseLong(request
				.getParameter("exid")));
		employeeExitVo.setRecordedBy(userService.findUser(employeeExitVo.getRecordedBy()).getEmployee());
		model.addAttribute("employeeExitVo", employeeExitVo);
		model.addAttribute("exitTypes", getAllExitTypes(employeeExitTypeService.findAllEmployeeExitType()));
		model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(DateFormatter
				.convertStringToDate(employeeExitVo.getExitDate())));
		model.addAttribute("typeList", getTypeList());
		model.addAttribute(MessageConstants.EMP_EXIT_BUTTON_LABEL, MessageConstants.EMP_EXIT_BUTTON_2);
		return PageNameConstatnts.EMPLOYEE_EXIT_ADD_PAGE;
	}

	/**
	 * method to handle EmployeeExit view page
	 * 
	 * @param model
	 * @param employeeExitId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EXIT_VIEW_URL, method = RequestMethod.GET)
	public String handleViewEmployeeExitPage(final Model model,
			@RequestParam(value = "otp", required = true) Long employeeExitId) {
		model.addAttribute("employeeExitVo", employeeExitService.findEmployeeExitById(employeeExitId));
		return PageNameConstatnts.EMPLOYEE_EXIT_VIEW_PAGE;
	}

	/**
	 * method to save employeeExit documents
	 * 
	 * @param model
	 * @param employeeExitVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EXIT_SAVE_DOC_URL, method = RequestMethod.POST)
	public String saveEmployeeExitDocumentsWithPost(final Model model, @ModelAttribute EmployeeExitVo employeeExitVo,
			@RequestParam(value = "employeeExitId", required = true) Long employeeExitId,
			MultipartHttpServletRequest request) {
		try {
			employeeExitVo.setEmployeeExitDocuments(uploadEmpExtDocuments(request));
			employeeExitService.saveEmployeeExitDocuments(employeeExitVo);
			model.addAttribute("employeeExitVo",
					employeeExitService.findEmployeeExitById(employeeExitVo.getEmployeeExitId()));

			model.addAttribute("employeeExits", employeeExitService.findAllEmployeeExit());
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.EMPLOYEE_EXIT_UPDATE_SUCCESS);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.EMPLOYEE_EXIT_NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.EMPLOYEE_EXIT_LIST_PAGE;
	}

	/**
	 * method to get all type list of employeeExit
	 * 
	 * @return
	 */
	private List<String> getTypeList() {
		List<String> typeList = new ArrayList<String>();
		typeList.add("-Select-");
		typeList.add("Permanent");
		typeList.add("Temporary");
		return typeList;
	}

	/**
	 * method to get all exit types
	 * 
	 * @param exitTypeList
	 * @return
	 */
	private Map<String, String> getAllExitTypes(List<EmployeeExitTypeVo> exitTypeList) {
		Map<String, String> exitTypes = new HashMap<String, String>();
		for (EmployeeExitTypeVo exitType : exitTypeList)
			exitTypes.put(exitType.getExitType(), exitType.getExitType());
		return exitTypes;
	}

	/**
	 * method to get selected employeeExit's all documents
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EXIT_GET_ALL_DOC_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getThisEmployeeExitDocuments(final HttpServletRequest request) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			EmployeeExitVo employeeExitVo = employeeExitService.findEmployeeExitById(Long.parseLong(request
					.getParameter("exid")));
			jsonResponse.setResult(employeeExitVo);
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to delete employeeExit's documnet
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EXIT_DELETE_DOC_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteThisEmpExitDoc(final HttpServletRequest request) {
		JsonResponse jsonResponse = new JsonResponse();
		SFTPOperation sftpOperation = new SFTPOperation();
		try {
			if (sftpOperation.removeFileFromSFTP(request.getParameter("url"))) {
				employeeExitService.deleteEmployeeExitDocument(Long.parseLong(request.getParameter("docId")));
				EmployeeExitVo employeeExitVo = employeeExitService.findEmployeeExitById(Long.parseLong(request
						.getParameter("extId")));
				jsonResponse.setResult(employeeExitVo);
				jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}

	/**
	 * method to delete employeeExit object
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EMPLOYEE_EXIT_DELETE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse deleteThisEmployeeExitWithPost(final HttpServletRequest request) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			employeeExitService.deleteEmployeeExit(Long.parseLong(request.getParameter("ext_id")));
			jsonResponse.setResult(employeeExitService.findAllEmployeeExit());
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}
}
