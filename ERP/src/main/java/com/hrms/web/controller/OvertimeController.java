package com.hrms.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Superiors;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.notifications.NotificationContent;
import com.hrms.web.notifications.PushNotification;
import com.hrms.web.service.AttendanceService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.HourlyWagesService;
import com.hrms.web.service.OvertimeService;
import com.hrms.web.service.OvertimeStatusService;
import com.hrms.web.service.SecondLevelForwaderService;
import com.hrms.web.service.UserService;
import com.hrms.web.service.WorkShiftDetailsService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.AttendanceVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.HourlyWagesVo;
import com.hrms.web.vo.OvertimeVo;
import com.hrms.web.vo.SessionRolesVo;
import com.hrms.web.vo.WorkShiftDetailsVo;

/**
 * 
 * @author Jithin Mohan
 * @since 20-April-2015
 *
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "overtimeVo", "thisUser", "roles" })
public class OvertimeController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private OvertimeService overtimeService;

	@Autowired
	private OvertimeStatusService overtimeStatusService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private UserService userService;

	@Autowired
	private SecondLevelForwaderService secondLevelForwaderService;

	@Autowired
	private PushNotification pushNotification;

	@Autowired
	private AttendanceService attendanceService;

	@Autowired
	private WorkShiftDetailsService workShiftDetailsService;

	@Autowired
	private HourlyWagesService hourlyWagesService;

	/**
	 * method to load overtime index page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.OVERTIME_URL, method = RequestMethod.GET)
	public String getOvertimeIndexPage(final Model model, @ModelAttribute("thisUser") String thisUser,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("overtimeList",
					overtimeService.findAllOvertimeByEmployee(authEmployeeDetails.getEmployeeCode()));
			model.addAttribute("overtimeVo", new OvertimeVo());
			model.addAttribute("thisUser", thisUser);
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PageNameConstatnts.GET_OVERTIME_PAGE;
	}

	/**
	 * method to load add overtime page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_OVERTIME_URL, method = RequestMethod.GET)
	public String getAddOvertimePage(final Model model, @ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("overtimeVo") OvertimeVo overtimeVo, BindingResult result,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			overtimeVo.setEmployee(authEmployeeDetails.getName()+" ("+authEmployeeDetails.getEmployeeCode()+")");
			overtimeVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());

			model.addAttribute("overtimeVo", overtimeVo);
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			if (overtimeVo.getOvertimeId() == null) {
				overtimeVo.setRecordedBy(authEmployeeDetails.getName());
				model.addAttribute(MessageConstants.CREATED_ON, DateFormatter.convertDateToDetailedString(new Date()));
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.SAVE_BUTTON);
			} else {
				overtimeVo.setRecordedBy(userService.findUser(overtimeVo.getRecordedBy()).getEmployee());
				model.addAttribute(MessageConstants.CREATED_ON, overtimeVo.getRecordedOn());
				model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.ADD_OVERTIME_PAGE;
	}

	/**
	 * method to save overtime
	 * 
	 * @param overtimeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.ADD_OVERTIME_URL, method = RequestMethod.POST)
	public ModelAndView saveAdvanceSalaryPage(@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute OvertimeVo overtimeVo, RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_OVERTIME_URL));
		try {
			overtimeService.saveOrUpdateOvertime(overtimeVo);
			pushNotification
					.sendNotification(createNotificationContent(overtimeVo, authEmployeeDetails.getProfilePic()));
			redirectAttributes.addFlashAttribute("overtimeVo", overtimeVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.NEW_OVERTIME_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to create NotificationContent object
	 * 
	 * @param terminationVo
	 * @return content
	 */
	private NotificationContent createNotificationContent(OvertimeVo overtimeVo, String profilePic) {
		NotificationContent content = new NotificationContent();
		List<String> recipients = new ArrayList<String>();

		recipients.addAll(overtimeVo.getForwaders());
		recipients.add(overtimeVo.getEmployeeCode());
		recipients.addAll(secondLevelForwaderService.findAllSecondLevelForwader().getForwaders());

		content.setContent("Loan Request");
		content.setUrl("ViewOvertime?otp=" + overtimeVo.getOvertimeId());
		content.setPicture(profilePic);
		content.setRecipients(recipients);
		return content;
	}

	/**
	 * method to edit advance salary
	 * 
	 * @param model
	 * @param advanceSalaryId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_OVERTIME_URL, method = RequestMethod.GET)
	public String editOvertimeWithGet(final Model model, @RequestParam(value = "otp") Long overtimeId,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			OvertimeVo overtimeVo = overtimeService.findOvertimeById(overtimeId);
			overtimeVo.setRecordedBy(userService.findUser(overtimeVo.getRecordedBy()).getEmployee());
			model.addAttribute("overtimeVo", overtimeVo);
			model.addAttribute(
					"superiorList",
					Superiors.getSuperiors(employeeService
							.getEmployeesDetails(
									employeeService.findAndReturnEmployeeByCode(overtimeVo.getEmployeeCode()).getId())
							.getSuperiorSubordinateVo().getSuperiors()));
			model.addAttribute(MessageConstants.CREATED_ON, overtimeVo.getRecordedOn());
			model.addAttribute(MessageConstants.BUTTON_LABEL, MessageConstants.UPDATE_BUTTON);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return PageNameConstatnts.ADD_OVERTIME_PAGE;
	}

	/**
	 * method to view overtime information
	 * 
	 * @param overtimeId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_OVERTIME_URL, method = RequestMethod.GET)
	public String viewAdvanceSalaryInformationWithGet(@RequestParam(value = "otp", required = true) Long overtimeId,
			final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			OvertimeVo overtimeVo = overtimeService.findOvertimeById(overtimeId);

			List<AttendanceVo> attendance = attendanceService.listAttendanceByEmployee(overtimeVo.getEmployeeCode(),
					overtimeVo.getRequestedDate());

			AttendanceVo attendanceVo = attendanceService.calculateHoursWorkedByEmployeeOnDate(
					overtimeVo.getEmployeeCode(), overtimeVo.getRequestedDate());
			WorkShiftDetailsVo workShiftDetailsVo = workShiftDetailsService.findWorkShiftByDay(
					DateFormatter.getDayOfWeek(DateFormatter.convertStringToDate(overtimeVo.getRequestedDate())),
					overtimeVo.getEmployeeCode());
			String regular = DateFormatter.getTimeDifference(
					DateFormatter.convertStringTimeToSqlTime(workShiftDetailsVo.getRegularWorkFrom()),
					DateFormatter.convertStringTimeToSqlTime(workShiftDetailsVo.getRegularWorkTo()));
			HourlyWagesVo hourlyWages = hourlyWagesService.getHourlyWagesByEmployee(overtimeVo.getEmployeeCode());
			BigDecimal actualWorkedTime = convertStringHrsToBigDecimal(attendanceVo.getTotalWorkedHours()).subtract(
					convertStringHrsToBigDecimal(regular));

			model.addAttribute("regularWorkHr", convertStringHrsToBigDecimal(regular));
			model.addAttribute("actualWorkedHr", actualWorkedTime);
			model.addAttribute("actualAmount", actualWorkedTime.multiply(new BigDecimal(hourlyWages.getOverTimeHoursAmount())));

			model.addAttribute("actualTimeIn", attendance.get(0).getSignInTime());
			model.addAttribute("actualTimeOut", attendance.get(attendance.size() - 1).getSignOutTime());
			model.addAttribute("overtimeVo", overtimeVo);
			model.addAttribute("overtimeStatusList", overtimeStatusService.findAllOvertimeStatus());
			model.addAttribute(
					"superiorList",
					Superiors.getSuperiors(employeeService
							.getEmployeesDetails(
									employeeService.findAndReturnEmployeeByCode(overtimeVo.getEmployeeCode()).getId())
							.getSuperiorSubordinateVo().getSuperiors()));
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return PageNameConstatnts.VIEW_OVERTIME_PAGE;
	}

	/**
	 * method to convert hours to bigdecimal
	 * 
	 * @param hours
	 * @return
	 */
	private BigDecimal convertStringHrsToBigDecimal(String hours) {
		List<String> parts = Arrays.asList(hours.split(":"));
		return new BigDecimal(parts.get(0) + "." + parts.get(1));
	}

	/**
	 * method to update overtime status
	 * 
	 * @param overtimeVo
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPDATE_OVERTIME_STATUS_URL, method = RequestMethod.POST)
	public ModelAndView updateOvertimeStatus(@ModelAttribute OvertimeVo overtimeVo,
			RedirectAttributes redirectAttributes) {
		ModelAndView view = new ModelAndView(new RedirectView(RequestConstants.ADD_OVERTIME_URL));
		try {
			overtimeService.updateOvertimeStatus(overtimeVo);
			redirectAttributes.addFlashAttribute(MessageConstants.SUCCESS, MessageConstants.UPDATE_OVERTIME_SUCCESS);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			redirectAttributes.addFlashAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
		}
		return view;
	}

	/**
	 * method to list all overtime
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LIST_ALL_OVERTIME_URL, method = RequestMethod.GET)
	public String listAllAdvanceSalaryInformations(final Model model, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("overtimeList", overtimeService.findAllOvertime());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return PageNameConstatnts.OVERTIME_LIST_PAGE;
	}

	/**
	 * method to delete advance salary information
	 * 
	 * @param overtimeId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_OVERTIME_URL, method = RequestMethod.POST)
	public String deleteOvertimeInformationWithPost(
			@RequestParam(value = "overtimeId", required = true) Long overtimeId, final Model model) {
		try {
			overtimeService.deleteOvertime(overtimeId);
		} catch (ItemNotFoundException ine) {
			logger.error(ine.getMessage());
			model.addAttribute(MessageConstants.ERROR, ine.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			model.addAttribute(MessageConstants.ERROR, e.getMessage());
		}
		return "redirect:" + RequestConstants.LIST_ALL_OVERTIME_URL;
	}
}
