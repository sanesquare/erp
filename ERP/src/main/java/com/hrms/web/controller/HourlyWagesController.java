package com.hrms.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BranchService;
import com.hrms.web.service.DepartmentService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.HourlyWagesService;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.HourlyWagesVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 10-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class HourlyWagesController {

	@Log
	private Logger logger;

	@Autowired
	private HourlyWagesService hourlyWagesService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private BranchService branchService;

	/**
	 * method to reach hourly wages home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HOURLY_WAGES_URL, method = RequestMethod.GET)
	public String hourlyWagesUrl(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("wages",
					hourlyWagesService.findAllHourlyWagesByEmployee(authEmployeeDetails.getEmployeeCode()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.HOURLY_WAGES_PAGE;
	}

	/**
	 * method to reach hourly wages add page
	 * 
	 * @param model
	 * @param wagesVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HOURLY_WAGES_ADD, method = RequestMethod.GET)
	public String addHourlyWages(final Model model, @ModelAttribute HourlyWagesVo hourlyWagesVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.HOURLY_WAGES_ADD_PAGE;
	}

	/**
	 * method to save or update hourly wages
	 * 
	 * @param model
	 * @param wagesVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HOURLY_WAGES_SAVE, method = RequestMethod.POST)
	public String saveHourlyWages(final Model model, @ModelAttribute HourlyWagesVo hourlyWagesVo) {
		try {
			if (hourlyWagesVo.getHourlyWagesId() != null) {
				logger.info("----------------------updating");
				hourlyWagesService.updateHourlyWage(hourlyWagesVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.HOURLY_WAGES_UPDATE_SUCCESS);
			} else {
				logger.info("----------------------saving");
				hourlyWagesVo.setHourlyWagesId(hourlyWagesService.saveHourlyWages(hourlyWagesVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.HOURLY_WAGES_SAVE_SUCCESS);
			}
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
			model.addAttribute("employees", employeeService.listAllEmployee());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.HOURLY_WAGES_SAVE_FAILED);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.HOURLY_WAGES_EDIT + "?id=" + hourlyWagesVo.getHourlyWagesId();
	}

	/**
	 * method to edit hourly wages
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HOURLY_WAGES_EDIT, method = RequestMethod.GET)
	public String editHourlyWages(final Model model, HttpServletRequest request,
			@ModelAttribute HourlyWagesVo hourlyWagesVo, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("hourlyWagesVo",
					hourlyWagesService.getHourlyWagesById(Long.parseLong(request.getParameter("id"))));
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("branches", branchService.listAllBranches());
			model.addAttribute("departments", departmentService.listAllDepartments());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.HOURLY_WAGES_ADD_PAGE;
	}

	/**
	 * method to view hourly wage details
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HOURLY_WAGES_VIEW, method = RequestMethod.GET)
	public String viewHourlyWages(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		try {
			model.addAttribute("rolesMap", rolesMap);
			model.addAttribute("wage",
					hourlyWagesService.getHourlyWagesById(Long.parseLong(request.getParameter("id"))));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.HOURLY_WAGES_VIEW_PAGE;
	}

	/**
	 * method to delete hourly wage
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HOURLY_WAGES_DELETE, method = RequestMethod.GET)
	public String deleteHourlyWage(final Model model, HttpServletRequest request) {
		try {
			hourlyWagesService.deleteHourlyWage((Long.parseLong(request.getParameter("id"))));
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.HOURLY_WAGES_DELETE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.HOURLY_WAGES_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.HOURLY_WAGES_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.HOURLY_WAGES_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.HOURLY_WAGES_DELETE_SUCCESS;
	}

	/**
	 * method to get wages details of employee
	 * 
	 * @param model
	 * @param employeeId
	 * @return
	 */
	@RequestMapping(value = RequestConstants.HOURLY_WAGES_GET_EMPLOYEE_WAGES, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getHourlyWagesByEmployee(final Model model,
			@RequestParam(value = "employeeCode", required = true) String employeeCode) {
		logger.info("dfdsfdsf--------------------- " + employeeCode);
		JsonResponse response = new JsonResponse();
		try {
			response.setResult(hourlyWagesService.getHourlyWagesByEmployee(employeeCode));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
}
