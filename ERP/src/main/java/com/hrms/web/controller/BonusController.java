package com.hrms.web.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dto.Employee;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.service.BonusDateService;
import com.hrms.web.service.BonusService;
import com.hrms.web.service.BonusTitleService;
import com.hrms.web.service.EmployeeService;
import com.hrms.web.service.SalaryService;
import com.hrms.web.util.BonusMonth;
import com.hrms.web.vo.BonusDateVo;
import com.hrms.web.vo.BonusTitleVo;
import com.hrms.web.vo.BonusVo;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 11-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "roles" })
public class BonusController {

	@Log
	private Logger logger;

	@Autowired
	private BonusService bonusService;

	@Autowired
	private BonusTitleService bonusTitleService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private BonusDateService bonusDateService;

	@Autowired
	private SalaryService salaryService;

	/**
	 * method to reach bonuses home page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.BONUS_URL, method = RequestMethod.GET)
	public String bonusHomePage(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);
			model.addAttribute("bonuses", bonusService.listBonusesByEmployee(authEmployeeDetails.getEmployeeId()));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.BONUS_PAGE;
	}

	/**
	 * method to reach bonus add page
	 * 
	 * @param model
	 * @param bonusVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.BONUS_ADD_URL, method = RequestMethod.GET)
	public String addBonus(final Model model, @ModelAttribute BonusVo bonusVo,
			@ModelAttribute BonusTitleVo bonusTitleVo, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("title", bonusTitleService.listAllBonusTitles());
			model.addAttribute("payollEmployee", Employee.getEmployees(employeeService.listEmployeesWithPayroll()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.BONUS_ADD_PAGE;
	}

	/**
	 * method to edit bonus
	 * 
	 * @param model
	 * @param request
	 * @param bonusVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.BONUS_EDIT_URL, method = RequestMethod.GET)
	public String editBonus(final Model model, HttpServletRequest request, @ModelAttribute BonusVo bonusVo,
			@ModelAttribute BonusTitleVo bonusTitleVo, @ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String erroMessage = request.getParameter(MessageConstants.ERROR);
			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (erroMessage != null)
				model.addAttribute(MessageConstants.ERROR, erroMessage);

			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("title", bonusTitleService.listAllBonusTitles());
			model.addAttribute("bonusVo", bonusService.getBonusById(Long.parseLong(request.getParameter("id"))));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.BONUS_ADD_PAGE;
	}

	/**
	 * method to view bonus
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.BONUS_VIEW_URL, method = RequestMethod.GET)
	public String viewBonus(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap) {
		model.addAttribute("rolesMap", rolesMap);
		try {
			model.addAttribute("bonus", bonusService.getBonusById(Long.parseLong(request.getParameter("id"))));
		} catch (ItemNotFoundException exception) {
			model.addAttribute(MessageConstants.ERROR, exception);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return PageNameConstatnts.BONUS_VIEW_PAGE;
	}

	/**
	 * method to delete bonus
	 * 
	 * @param model
	 * @param request
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = RequestConstants.BONUS_DELETE_URL, method = RequestMethod.GET)
	public String deleteBonus(final Model model, HttpServletRequest request) {
		try {
			bonusService.deleteBonus(Long.parseLong(request.getParameter("id")));
		} catch (ItemNotFoundException infe) {
			return "redirect:" + RequestConstants.BONUS_URL + "?" + MessageConstants.ERROR + "=" + infe;
		} catch (Exception e) {
			return "redirect:" + RequestConstants.BONUS_URL + "?" + MessageConstants.ERROR + "="
					+ MessageConstants.BONUS_DELETE_FAILED;
		}
		return "redirect:" + RequestConstants.BONUS_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.BONUS_DELETE_SUCCESS;
	}

	/**
	 * method to save bonus
	 * 
	 * @param model
	 * @param bonusVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.BONUS_SAVE_URL, method = RequestMethod.POST)
	public String saveBonus(final Model model, @ModelAttribute BonusVo bonusVo,
			@ModelAttribute BonusTitleVo bonusTitleVo) {
		try {
			if (bonusVo.getBonusId() == null) {
				bonusVo.setBonusId(bonusService.saveBonus(bonusVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.BONUS_SAVE_SUCCESS);
			} else {
				bonusService.updateBonus(bonusVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.BONUS_UPDATE_SUCCESS);
			}
			model.addAttribute("employees", employeeService.listAllEmployee());
			model.addAttribute("title", bonusTitleService.listAllBonusTitles());
		} catch (ItemNotFoundException die) {
			model.addAttribute(MessageConstants.ERROR, die);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.BONUS_SAVE_FAILED);
		}
		return "redirect:" + RequestConstants.BONUS_EDIT_URL + "?id=" + bonusVo.getBonusId();
	}

	/**
	 * method to get bonus by employee and title
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_BONUS_BY_EMPLOYEE_AND_TITLE, method = RequestMethod.POST)
	public @ResponseBody JsonResponse getBonusByEmployeeAndTitle(final Model model,
			@RequestParam(value = "employeeCode", required = true) String employeeCode,
			@RequestParam(value = "titleId", required = true) Long titleId) {
		logger.info("**********----------//////////-----------" + employeeCode + "    " + titleId);
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setStatus("ok");
			jsonResponse.setResult(bonusService.getBonusByEmployeeAndTitle(employeeCode, titleId));
		} catch (Exception e) {
			e.printStackTrace();
			jsonResponse.setStatus("error");
		}
		return jsonResponse;
	}

	/**
	 * method to save bonus title
	 * 
	 * @param model
	 * @param bonusVo
	 * @param bonusTitleVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.BONUS_TITLE_SAVE_URL, method = RequestMethod.POST)
	public @ResponseBody JsonResponse saveBonusTitle(@RequestBody BonusTitleVo bonusTitleVo) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			bonusTitleService.saveBonusTitle(bonusTitleVo);
			jsonResponse.setStatus("SUCCESS");
		} catch (DuplicateItemException e) {
			jsonResponse.setStatus("DUPLICATE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	/**
	 * method to get available bonus titles
	 * 
	 * @return
	 */
	@RequestMapping(value = RequestConstants.GET_AVAILABLE_BONUS_TITLES, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getAllAvailableBonusTitles() {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(bonusTitleService.listAllBonusTitles());
			jsonResponse.setStatus("SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonResponse;
	}

	@RequestMapping(value = RequestConstants.GET_BONUS_AMOUNT, method = RequestMethod.GET)
	public @ResponseBody JsonResponse getBonusAmountOfAnEmployee(
			@RequestParam(value = "employeeCode", required = true) String employeeCode) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			BonusDateVo bonusDate = bonusDateService.findBonusDate();
			Integer months = BonusMonth.getBonusPeriod(bonusDate.getYear(), bonusDate.getMonth(), bonusDate.getDay());
			BigDecimal salary = new BigDecimal(salaryService.getMonthlySalaryByEmployee(employeeCode)
					.getMonthlyEstimatedSalary());
			BigDecimal bonusAmount = (salary.multiply(new BigDecimal(months)));
			if (bonusDate.getDay() > 0 && bonusDate.getDay() < 30)
				bonusAmount = bonusAmount
						.add(salary.divide(new BigDecimal(bonusDate.getDay()), 2, RoundingMode.HALF_UP));
			jsonResponse.setStatus(MessageConstants.JSON_SUCCESS);
			jsonResponse.setResult(bonusAmount);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonResponse;
	}
}
