package com.hrms.web.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.slf4j.Logger;

import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.dao.EmployeeDao;
import com.hrms.web.dto.Superiors;
import com.hrms.web.entities.Employee;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.service.ForwardApplicationStatusService;
import com.hrms.web.service.LeaveService;
import com.hrms.web.service.impl.ForwardApplicationStatusServiceImpl;
import com.hrms.web.util.FileUpload;
import com.hrms.web.util.SFTPOperation;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.LeaveDocumentsVo;
import com.hrms.web.vo.LeavesVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer
 * @since 6-April-2015
 */
@Controller
@SessionAttributes({ "authEmployee", "authEmployeeDetails", "thisUser","roles"  })
public class LeavesController {

	@Log
	private static Logger logger;

	@Autowired
	private LeaveService leavesService;

	@Autowired
	private ForwardApplicationStatusService statusService;

	@Autowired
	private EmployeeDao employeeDao;

	private SFTPOperation operation = new SFTPOperation();

	/**
	 * method to reach leaves home page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LEAVE_URL, method = RequestMethod.GET)
	public String leaveGetRequestHandler(final Model model, HttpServletRequest request,
			@ModelAttribute("authEmployeeDetails") EmployeeVo employeeVo, @ModelAttribute("thisUser") String thisUser,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);

			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);
			model.addAttribute("leaves", leavesService.findAllLeaveByEmployee(employeeVo.getEmployeeCode()));
			model.addAttribute("thisUser", employeeVo.getEmployeeCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.LEAVES_PAGE;
	}

	/**
	 * method to reach add leave page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.LEAVE_ADD_URL, method = RequestMethod.GET)
	public String addLeavesGet(final Model model, @ModelAttribute LeavesVo leavesVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			leavesVo.setEmployee(authEmployeeDetails.getName()+" ("+authEmployeeDetails.getEmployeeCode()+")");
			leavesVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.LEAVES_ADD_PAGE;
	}

	/**
	 * method to edit leaves
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.EDIT_LEAVE, method = RequestMethod.GET)
	public String editLeave(final Model model, HttpServletRequest request, @ModelAttribute LeavesVo leavesVo,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);

			if (successMessage != null)
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
			else if (errorMessage != null)
				model.addAttribute(MessageConstants.ERROR, errorMessage);

			model.addAttribute("leavesVo", leavesService.getleaveById(Long.parseLong(request.getParameter("id"))));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.LEAVES_ADD_PAGE;
	}

	/**
	 * method to save leave
	 * 
	 * @param model
	 * @param leavesVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_LEAVE, method = RequestMethod.POST)
	public String saveLeave(final Model model, @ModelAttribute LeavesVo leavesVo) {
		try {
			leavesVo.setLeaveTypeAjaxId(leavesVo.getLeaveTypeId());
			List<Long> superiorIds = new ArrayList<Long>();
			for (Long id : leavesVo.getSuperiorIds()) {
				superiorIds.add(id);
			}
			leavesVo.setSuperiorAjaxIds(superiorIds);
			if (leavesVo.getLeaveId() == null) {
				leavesVo.setLeaveId(leavesService.saveLeave(leavesVo));
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.LEAVE_SAVE_SUCCESS);
			} else {
				leavesService.updateLeave(leavesVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.LEAVE_UPDATE_SUCCESS);
			}
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.LEAVE_SAVE_FAILED);
			e.printStackTrace();
		}

		return "redirect:" + RequestConstants.EDIT_LEAVE + "?id=" + leavesVo.getLeaveId();
	}

	/**
	 * method to delete leave
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_LEAVE, method = RequestMethod.GET)
	public String deleteLeave(final Model model, HttpServletRequest request) {
		try {
			leavesService.deleteLeave(Long.parseLong(request.getParameter("id")));
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.LEAVE_DELETE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute(MessageConstants.ERROR, MessageConstants.LEAVE_DELETE_FAILED);
		}
		return "redirect:" + RequestConstants.LEAVE_URL;
	}

	/**
	 * method to view leave details
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = RequestConstants.VIEW_LEAVE, method = RequestMethod.GET)
	public String viewLeave(final Model model, HttpServletRequest request, @ModelAttribute LeavesVo leavesVo,
			@ModelAttribute("authEmployee") Employee authEmployee,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("roles") SessionRolesVo rolesMap){
		model.addAttribute("rolesMap", rolesMap);
		try {
			leavesVo.setLeaveId(Long.parseLong(request.getParameter("id")));
			leavesVo.setEmployee(authEmployeeDetails.getName());
			leavesVo.setEmployeeCode(authEmployeeDetails.getEmployeeCode());
			model.addAttribute("superiorList",
					Superiors.getSuperiors(authEmployeeDetails.getSuperiorSubordinateVo().getSuperiors()));
			leavesVo = leavesService.getleaveById(Long.parseLong(request.getParameter("id")));
			Boolean viewStatus = false;
			for(String code : leavesVo.getSuperiorNames()){
				if(code.equalsIgnoreCase(authEmployeeDetails.getEmployeeCode()))
					viewStatus = true;
			}
			leavesVo.setViewStatus(viewStatus);
			model.addAttribute("leavesVo", leavesVo);
			model.addAttribute("status", statusService.listAllStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstatnts.LEAVES_VIEW_PAGE;
	}

	/**
	 * method to update leve status
	 * 
	 * @param model
	 * @param leavesVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.SAVE_LEAVE_STATUS, method = RequestMethod.POST)
	public String saveLeaveStatus(final Model model, @ModelAttribute LeavesVo leavesVo) {
		try {
			leavesService.updateStatus(leavesVo);
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.LEAVE_URL + "?" + MessageConstants.ERROR + "=" + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:" + RequestConstants.LEAVE_URL + "?" + MessageConstants.ERROR + "=" + e.getMessage();
		}
		return "redirect:" + RequestConstants.LEAVE_URL + "?" + MessageConstants.SUCCESS + "="
				+ MessageConstants.LEAVE_UPDATE_SUCCESS;
	}

	/**
	 * method to download leave document
	 * 
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = RequestConstants.DOWNLOAD_LEAVE_DOCUMENT, method = RequestMethod.GET)
	public void downloadDocument(final Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			operation.downloadFileFromSFTP(request.getParameter("url"), response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * method to delete document
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = RequestConstants.DELETE_LEAVE_DOCUMENT, method = RequestMethod.GET)
	public String deleteDocument(final Model model, HttpServletRequest request) {
		try {
			Boolean result = operation.removeFileFromSFTP(request.getParameter("url"));
			if (result)
				leavesService.deleteDocument(request.getParameter("url"));
			model.addAttribute(MessageConstants.SUCCESS, MessageConstants.LEAVE_DOC_DELETE_SUCCESS);
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.LEAVE_URL;
	}

	/**
	 * method to upload document
	 * 
	 * @param model
	 * @param leavesVo
	 * @return
	 */
	@RequestMapping(value = RequestConstants.UPLOAD_LEAV_DOCUMENT, method = RequestMethod.POST)
	public String uploadDocument(final Model model, @ModelAttribute LeavesVo leavesVo) {
		try {
			if (leavesVo.getLeaveId() != null) {
				leavesVo.setDocumentsVos(uploadFiles(leavesVo.getLeaveDocs()));
				leavesService.updateDocuments(leavesVo);
				model.addAttribute(MessageConstants.SUCCESS, MessageConstants.LEAVE_DOC_SUCCESS);
			}
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			e.printStackTrace();
		}
		return "redirect:" + RequestConstants.LEAVE_URL;
	}

	/**
	 * method to upload files
	 * 
	 * @param files
	 * @return
	 */
	private List<LeaveDocumentsVo> uploadFiles(List<MultipartFile> files) {
		List<LeaveDocumentsVo> vos = new ArrayList<LeaveDocumentsVo>();
		try {
			for (MultipartFile file : files) {
				String url = FileUpload.uploadDocument(MessageConstants.LEAVE_MODULE, file);
				if (url != null) {
					LeaveDocumentsVo vo = new LeaveDocumentsVo();
					vo.setFileName(file.getOriginalFilename());
					vo.setUrl(url);
					vos.add(vo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vos;
	}
}
