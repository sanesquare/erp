package com.hrms.web.util;

import org.slf4j.Logger;

import com.hrms.web.entities.Employee;
import com.hrms.web.logger.Log;
import com.hrms.web.vo.EmployeePopupVo;

/**
 * 
 * @author Jithin Mohan
 * @since 16-April-2015
 *
 */
public class EmployeePopUp {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	/**
	 * method to create EmployeePopupVo object
	 * 
	 * @param employee
	 * @return popupVo
	 */
	public static EmployeePopupVo createEmployeePopupVo(Employee employee) {
		EmployeePopupVo popupVo = new EmployeePopupVo();
		try {
			popupVo.setName(employee.getUserProfile().getFirstName() + " " + employee.getUserProfile().getLastname());
			popupVo.setEmail(employee.getUser().getEmail());
			popupVo.setDesignation(employee.getEmployeeDesignation().getDesignation());
			popupVo.setDepartment(employee.getDepartmentEmployees().getName());
			popupVo.setBranch(employee.getBranch().getBranchName());
			if (employee.getAdditionalInformation() == null
					|| employee.getAdditionalInformation().getJoiningDate() == null)
				popupVo.setJoinDate("Nil");
			else
				popupVo.setJoinDate(DateFormatter.convertDateToDetailedString(employee.getAdditionalInformation()
						.getJoiningDate()));
			popupVo.setPicture(employee.getProfilePic());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return popupVo;
	}
}
