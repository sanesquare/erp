package com.hrms.web.util;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;

import com.hrms.web.logger.Log;

/**
 * 
 * @author Jithin Mohan
 * @since 11-March-2015
 *
 */
public class StringSplitter {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	/**
	 * method to split string with commas seperator
	 * 
	 * @param input
	 * @return List<String>
	 */
	public static List<String> splitWithComma(String input) {
		return Arrays.asList(input.split(","));
	}
	
	/**
	 * to split string with special characters , for pay salary
	 * @param item
	 * @return
	 */
	public static Map<String, BigDecimal> splitWithSpecialCharacters(String item){
		String[] splittedString = item.split("&!=");
		Map<String, BigDecimal> map = new HashMap<String,BigDecimal>();
		map.put(splittedString[1], new BigDecimal(splittedString[0]));
		return map;
	}
}