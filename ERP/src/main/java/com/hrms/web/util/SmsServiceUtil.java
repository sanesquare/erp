package com.hrms.web.util;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import com.hrms.web.constants.SMSConstants;

/**
 * 
 * @author Shamsheer
 * @since 02-June-2015
 */
public class SmsServiceUtil {
	public static int sendSMS(List<String> sendTo, String message) throws Exception {
		String toNumbers = null;
		for (String to : sendTo) {
			if (toNumbers == null)
				toNumbers = to;
			else
				toNumbers = toNumbers + "," + to;
		}
		message = message.replace(" ", "%20");
		String url = "http://" + SMSConstants.IP_ADDRESS + "/api/smsapi.aspx?username=" + SMSConstants.USERNAME
				+ "&password=" + SMSConstants.PASSWORD + "&to=" + toNumbers + "&from=" + SMSConstants.CLIENT_ID
				+ "&message=" + message;
		URL urlObject = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) urlObject.openConnection();
		connection.setRequestMethod("GET");
		return connection.getResponseCode();
	}
	/*
	 * public static void main(String[] args) { List<String> sendTo = new
	 * ArrayList<String>(); sendTo.add("9747187148"); sendTo.add("8089094744");
	 * try { SmsServiceUtil.sendSMS(sendTo, "Testing.... ..."); } catch
	 * (Exception e) { e.printStackTrace(); } }
	 */
}
