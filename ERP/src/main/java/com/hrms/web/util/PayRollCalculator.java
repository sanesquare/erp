package com.hrms.web.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.slf4j.Logger;

import com.hrms.web.constants.PayRollConstants;
import com.hrms.web.dto.EsiShare;
import com.hrms.web.dto.PFShare;
import com.hrms.web.logger.Log;
import com.hrms.web.vo.AdditionalPayRollResultVo;
import com.hrms.web.vo.EsiSyntaxVo;
import com.hrms.web.vo.ExtraPaySlipItemVo;
import com.hrms.web.vo.PayRollResultVo;
import com.hrms.web.vo.PaySlipItemVo;

/**
 * 
 * @author Jithin Mohan
 * @since 23-April-2015
 *
 */
public class PayRollCalculator {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	private static ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
	private static ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("JavaScript");

	private List<PayRollResultVo> payRollResults = new ArrayList<PayRollResultVo>();

	/**
	 * method to get Payroll structure
	 * 
	 * @param grossSalary
	 * @return
	 */

	public List<PayRollResultVo> getPayRollResult(BigDecimal grossSalary, Map<String, String> syntax,
			List<PaySlipItemVo> paySlipItemVos) {

		initializeScriptEngine(grossSalary, syntax);
		/**
		 * section for pay slip item calculation
		 */
		for (Map.Entry<String, String> entry : syntax.entrySet()) {
			PayRollResultVo payRollResult = new PayRollResultVo();
			payRollResult.setPaySlipItem(entry.getKey());
			BigDecimal amount=new BigDecimal(Double.valueOf((expressionEvaluator(entry.getValue())).toString()));
			payRollResult.setAmount(amount.setScale(2));
			payRollResults.add(payRollResult);
		}
		List<PayRollResultVo> finalResults = new ArrayList<PayRollResultVo>();
		List<String> keys = new ArrayList<String>();
		for (PaySlipItemVo itemVo : paySlipItemVos) {
			keys.add(itemVo.getTitle());
		}
		for (String key : keys) {
			if (!key.equalsIgnoreCase(PayRollConstants.CONVEYANCE)) {
				for (PayRollResultVo resultVo : payRollResults) {
					if (resultVo.getPaySlipItem().equalsIgnoreCase(key)) {
						finalResults.add(resultVo);
						break;
					}
				}
			}
		}
		return finalResults;
	}

	/**
	 * method to calculate additional pay slip items
	 * 
	 * @param grossSalary
	 * @param syntax
	 * @param additionalSyntax
	 * @return additionalPayRollResults
	 */
	public static List<AdditionalPayRollResultVo> getAdditionalPayRollResult(BigDecimal grossSalary,
			Map<String, String> syntax, Map<String, String> additionalSyntax) {
		List<AdditionalPayRollResultVo> additionalPayRollResults = new ArrayList<AdditionalPayRollResultVo>();

		initializeScriptEngine(grossSalary, additionalSyntax);

		/**
		 * section for addition pay slip item calculation
		 */
		for (Map.Entry<String, String> entry : additionalSyntax.entrySet()) {
			AdditionalPayRollResultVo additionalPayRollResult = new AdditionalPayRollResultVo();
			additionalPayRollResult.setExtraPaySlipItem(entry.getKey());
			additionalPayRollResult.setAmount(new BigDecimal(expressionEvaluator(entry.getValue()).toString()));
			additionalPayRollResults.add(additionalPayRollResult);
		}
		return additionalPayRollResults;
	}

	/**
	 * method to initialize script engine components
	 * 
	 * @param grossSalary
	 * @param syntax
	 */
	@SuppressWarnings("restriction")
	private static void initializeScriptEngine(BigDecimal grossSalary, Map<String, String> syntax) {

		scriptEngine.put(PayRollConstants.GROSS_SALARY, grossSalary);
		for (Map.Entry<String, String> entry : syntax.entrySet()) {
			scriptEngine.put(entry.getKey(), getBasicSalary(grossSalary, entry.getValue(), entry.getKey()));
		}
	}

	/**
	 * method to calculate basic salary
	 * 
	 * @param grossSalary
	 * @param expression
	 * @return
	 */
	private static BigDecimal getBasicSalary(BigDecimal grossSalary, String expression, String item) {
		BigDecimal result = null;
		try {
			result = new BigDecimal((Double) expressionEvaluator(expression));
		} catch (Exception e) {
			// e.printStackTrace();
			result = new BigDecimal("0.00");
		}
		return result;
	}

	/**
	 * method to evaluate the expression
	 * 
	 * @param expression
	 * @return
	 */
	private static Object expressionEvaluator(String expression) {
		if (expression == null || expression.equalsIgnoreCase(""))
			return 0.0;
		expression = expression.replace("%", "/100");
		expression.replaceAll("\\s", "");
		try {
			return scriptEngine.eval(expression);
		} catch (ScriptException se) {
			logger.error(se.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0.0;
	}

	/**
	 * method to get payroll components
	 * 
	 * @param slipItems
	 * @return components
	 */
	public Map<String, String> getPayRollComponents(List<PaySlipItemVo> slipItems) {
		Map<String, String> components = new LinkedHashMap<String, String>();
		for (PaySlipItemVo item : slipItems) {
			if (item.getType().equalsIgnoreCase("calculation"))
				components.put(item.getTitle(), item.getCalculation());
			else {
				PayRollResultVo payRollResult = new PayRollResultVo();
				payRollResult.setPaySlipItem(item.getTitle());
				BigDecimal amount = null;
				try {
					amount = new BigDecimal(item.getCalculation());
				} catch (Exception e) {
					amount = new BigDecimal("0.00");
				}
				payRollResult.setAmount(amount);
				payRollResults.add(payRollResult);
				scriptEngine.put(item.getTitle(), amount);
			}
		}
		return components;
	}

	/**
	 * method to get additional payroll components
	 * 
	 * @param slipItems
	 * @return components
	 */
	public static Map<String, String> getAdditionalPayRollComponents(List<ExtraPaySlipItemVo> slipItems) {
		Map<String, String> components = new HashMap<String, String>();
		for (ExtraPaySlipItemVo item : slipItems)
			components.put(item.getTitle(), item.getCalculation());
		return components;
	}

	/**
	 * method to find PF share
	 * 
	 * @param grossSalary
	 * @param emplySyntax
	 * @param emplyrSyntax
	 * @param syntax
	 * @return
	 */
	public static PFShare getPFShare(BigDecimal grossSalary, String emplySyntax, String emplyrSyntax,
			Map<String, String> syntax) {
		PFShare pfShare = new PFShare();
		try {
			initializeScriptEngine(grossSalary, syntax);
			BigDecimal amount = new BigDecimal((Double) expressionEvaluator(emplySyntax)).setScale(2, RoundingMode.HALF_EVEN);
			pfShare.setEmployeeShare(amount);
			amount=new BigDecimal((Double) expressionEvaluator(emplyrSyntax)).setScale(2, RoundingMode.HALF_EVEN);
			pfShare.setEmployerShare(amount);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return pfShare;
	}

	/**
	 * method to find ESI share
	 * 
	 * @param grossSalary
	 * @param emplySyntax
	 * @param emplyrSyntax
	 * @return
	 */
	@SuppressWarnings("restriction")
	public static EsiShare getESIShare(BigDecimal grossSalary, String emplySyntax, String emplyrSyntax,
			EsiSyntaxVo esiVo) {
		EsiShare esiShare = new EsiShare();
		try {
			if (esiVo.getRange().equalsIgnoreCase("below")) {
				if (grossSalary.compareTo(esiVo.getAmount()) < 0) {
					scriptEngine.put(PayRollConstants.GROSS_SALARY, grossSalary);
					esiShare.setEmployeeShare(new BigDecimal((Double) expressionEvaluator(emplySyntax)));
					esiShare.setEmployerShare(new BigDecimal((Double) expressionEvaluator(emplyrSyntax)));
				} else {
					esiShare.setEmployeeShare(new BigDecimal("0.00"));
					esiShare.setEmployerShare(new BigDecimal("0.00"));
				}
			} else {
				if (grossSalary.compareTo(esiVo.getAmount()) > 0) {
					scriptEngine.put(PayRollConstants.GROSS_SALARY, grossSalary);
					esiShare.setEmployeeShare(new BigDecimal((Double) expressionEvaluator(emplySyntax)));
					esiShare.setEmployerShare(new BigDecimal((Double) expressionEvaluator(emplyrSyntax)));
				} else {
					esiShare.setEmployeeShare(new BigDecimal("0.00"));
					esiShare.setEmployerShare(new BigDecimal("0.00"));
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return esiShare;
	}
}
