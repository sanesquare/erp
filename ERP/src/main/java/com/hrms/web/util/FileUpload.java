package com.hrms.web.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.vo.SFTPFilePropertiesVo;

/**
 * 
 * @author Jithin Mohan
 * @since 18-March-2015
 *
 */
public class FileUpload {

	/**
	 * method to upload document file through FTP
	 * 
	 * @param moduleName
	 * @param multipartFile
	 * @return url
	 */
	public static String uploadDocument(String moduleName, MultipartFile multipartFile) {
		String url = null;
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		SFTPOperation sftpOperation = new SFTPOperation();
		SFTPFilePropertiesVo sftpFilePropertiesVo = new SFTPFilePropertiesVo();
		sftpFilePropertiesVo.setFile(multipartFile);
		sftpFilePropertiesVo.setModuleName(moduleName);
		sftpFilePropertiesVo.setUserName(user.getUsername());
		sftpFilePropertiesVo.setFileName(multipartFile.getOriginalFilename());
		sftpFilePropertiesVo.setSftpRootPath(CommonConstants.SFTP_DOCUMENT_ROOT_PATH);
		sftpFilePropertiesVo.setSftpIP(CommonConstants.SFTP_DOCUMENT_IP_ADDRESS);
		sftpFilePropertiesVo.setSftpUser(CommonConstants.SFTP_DOCUMENT_USER);
		sftpFilePropertiesVo.setSftpKey(CommonConstants.SFTP_DOCUMENT_RSA);
		sftpFilePropertiesVo.setSftpPort(CommonConstants.SFTP_DOCUMENT_PORT);
		try {
			url = sftpOperation.sendFileToSFTPServer(sftpFilePropertiesVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return url;
	}

	/**
	 * method to upload file through cdn FTP server
	 * 
	 * @param moduleName
	 * @param multipartFile
	 * @return url
	 */
	public static String uploadFileToCDN(String moduleName, MultipartFile multipartFile) {
		String url = null;
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		SFTPOperation sftpOperation = new SFTPOperation();
		SFTPFilePropertiesVo sftpFilePropertiesVo = new SFTPFilePropertiesVo();
		sftpFilePropertiesVo.setFile(multipartFile);
		sftpFilePropertiesVo.setModuleName(moduleName);
		sftpFilePropertiesVo.setUserName(user.getUsername());
		sftpFilePropertiesVo.setFileName(multipartFile.getOriginalFilename());
		sftpFilePropertiesVo.setSftpRootPath(CommonConstants.SFTP_CDN_ROOT_PATH);
		sftpFilePropertiesVo.setSftpIP(CommonConstants.SFTP_CDN_IP_ADDRESS);
		sftpFilePropertiesVo.setSftpUser(CommonConstants.SFTP_CDN_USER);
		sftpFilePropertiesVo.setSftpKey(CommonConstants.SFTP_CDN_RSA);
		sftpFilePropertiesVo.setSftpPort(CommonConstants.SFTP_CDN_PORT);
		try {
			url = sftpOperation.sendFileToSFTPServer(sftpFilePropertiesVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return removeRootPathFromUrl(url);
	}

	/**
	 * method to remove sftp root path from returned url
	 * @param url
	 * @return
	 */
	private static String removeRootPathFromUrl(String url){
		String finalUrl = null;
		if(url!=null)
			finalUrl = url.replace(CommonConstants.SFTP_CDN_ROOT_PATH, "");
		return finalUrl;
	}

	/**
	 * method to remove cdn file from ftp server
	 * @param url
	 * @return
	 */
	public static Boolean removeFileFromCDN(String url){
		Boolean result = false;
		String finalUrl = null;
		SFTPOperation sftpOperation = new SFTPOperation();
		try{
			if(url!=null)
				 finalUrl = addRootPathToUrl(url);
			if(finalUrl!=null)
				result = sftpOperation.removeFileFromCDN(finalUrl);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * method to add ftp path to url
	 * @param url
	 * @return
	 */
	private static String addRootPathToUrl(String url){
		String finalUrl = null;
		if(url!=null)
			finalUrl = url.replace(CommonConstants.SFTP_PATH, CommonConstants.SFTP_CDN_ROOT_PATH);
		return finalUrl;
	}
}
