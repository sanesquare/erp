package com.hrms.web.util;

/**
 * 
 * @author Jithin Mohan
 * @since 13-March-2015
 *
 */
public class FormValueChecker {

	/**
	 * method to test whether the check box is checked or not
	 * 
	 * @param value
	 * @return boolean
	 */
	public static boolean checkBoxValue(String value) {
		if (value == null)
			return false;
		return true;
	}
}
