package com.hrms.web.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.web.multipart.MultipartFile;

import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.vo.BiometricAttendanceVo;
import com.hrms.web.vo.EmployeeVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 22-June-2015
 */
public class BiometricServiceUtil {

	/**
	 * method to read biometric attendance report and convert into attendance
	 * vos
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static Set<BiometricAttendanceVo> readExcelFromMultipart(MultipartFile file) throws IOException {
		Set<BiometricAttendanceVo> attendanceVos = new HashSet<BiometricAttendanceVo>();
		String filename = file.getOriginalFilename();
		HSSFWorkbook workbook = null;
		HSSFSheet sheet;
		String fileExtension = filename.substring(filename.indexOf("."));
		if (!fileExtension.equals(".xls"))
			throw new ItemNotFoundException("Invalid File");
		workbook = new HSSFWorkbook(file.getInputStream());

		sheet = workbook.getSheetAt(0);
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			Row row = sheet.getRow(i);
			int cellCount = row.getLastCellNum();
			List<String> time = new ArrayList<String>();

			BiometricAttendanceVo attendanceVo = new BiometricAttendanceVo();
			attendanceVo.setEmployeeCode(row.getCell(1).getStringCellValue());
			attendanceVo.setDepartment(row.getCell(0).getStringCellValue());
			attendanceVo.setDate(row.getCell(4).getStringCellValue());
			time.add(row.getCell(5).getStringCellValue());

			if (cellCount > 6) {
				int n = i + 1;
				for (int k = 7; k <= cellCount; k++) {
					time.add(row.getCell(k - 1).getStringCellValue());
					n++;
				}
			}
			attendanceVo.setTime(time);
			attendanceVos.add(attendanceVo);
		}
		return attendanceVos;
	}

	/**
	 * method to export employee details into excel sheet to be imported by
	 * biometric software
	 * 
	 * @param fileName
	 * @param employeeVos
	 * @param response
	 */
	public static void writeExcel(String fileName, List<EmployeeVo> employeeVos, HttpServletResponse response) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet excelSheet = workbook.createSheet("employees");
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("Dept");
		excelHeader.createCell(1).setCellValue("Name");
		excelHeader.createCell(2).setCellValue("User ID");
		excelHeader.createCell(3).setCellValue("Enroll ID");
		excelHeader.createCell(4).setCellValue("Card No");
		excelHeader.createCell(5).setCellValue("Att Rules");
		excelHeader.createCell(6).setCellValue("Default Att");
		excelHeader.createCell(7).setCellValue("Default Weekend");
		excelHeader.createCell(8).setCellValue("Gender");
		excelHeader.createCell(9).setCellValue("Register Date");
		excelHeader.createCell(10).setCellValue("Title");
		excelHeader.createCell(11).setCellValue("Birthday");
		excelHeader.createCell(12).setCellValue("ID No");
		excelHeader.createCell(13).setCellValue("Phone");
		excelHeader.createCell(14).setCellValue("Email");
		excelHeader.createCell(15).setCellValue("Address");
		int record = 1;
		for (EmployeeVo employeeVo : employeeVos) {
			if (employeeVo.getGender() != null && employeeVo.getName() != null) {
				HSSFRow excelRow = excelSheet.createRow(record++);
				excelRow.createCell(0).setCellValue(employeeVo.getEmployeeDepartment());
				excelRow.createCell(1).setCellValue(employeeVo.getName());
				excelRow.createCell(2).setCellValue(employeeVo.getEmployeeCode());
				excelRow.createCell(3).setCellValue(employeeVo.getEmployeeId().toString());
				excelRow.createCell(8).setCellValue(employeeVo.getGender());
			}
		}
		try {
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			workbook.write(response.getOutputStream());
			response.flushBuffer();
		} catch (IOException e) {
		}
	}
}
