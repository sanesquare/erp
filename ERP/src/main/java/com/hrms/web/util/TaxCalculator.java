package com.hrms.web.util;

import java.math.BigDecimal;
import java.util.List;

import com.hrms.web.vo.TaxRuleVo;

/**
 * 
 * @author Jithin Mohan
 * @since 28-April-2015
 *
 */
public class TaxCalculator {

	/**
	 * method to calculate total tax
	 * 
	 * @param taxRules
	 * @param grossSalary
	 * @return totalTax
	 */
	public static BigDecimal getTax(List<TaxRuleVo> taxRules, BigDecimal grossSalary) {
		BigDecimal anualGS = grossSalary.multiply(new BigDecimal(12));
		BigDecimal totalTax = new BigDecimal(0);
		for (TaxRuleVo taxRule : taxRules) {
			if (anualGS.compareTo(taxRule.getSalaryTo()) <= 0) {
				BigDecimal diff = anualGS.subtract(taxRule.getSalaryFrom());
				BigDecimal percent = (diff.multiply(taxRule.getTaxPercentage())).divide(new BigDecimal("100"));
				totalTax = totalTax.add(percent);
				break;
			} else {
				totalTax = totalTax.add(taxRule.getRowTax());
			}
		}
		return totalTax;
	}
}
