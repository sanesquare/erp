package com.hrms.web.util;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Jithin Mohan
 * @since 1-April-2015
 *
 */
public class WeekDays {

	public enum WeekDay {
		SUNDAY(1), MONDAY(2), TUESDAY(3), WEDNESDAY(4), THURSDAY(5), FRIDAY(6), SATURDAY(7);

		private int value;

		WeekDay(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		/**
		 * method to get week name
		 * 
		 * @param value
		 * @return WeekDay
		 */
		public static WeekDay get(int value) {
			switch (value) {
			case 1:
				return SUNDAY;
			case 2:
				return MONDAY;
			case 3:
				return TUESDAY;
			case 4:
				return WEDNESDAY;
			case 5:
				return THURSDAY;
			case 6:
				return FRIDAY;
			case 7:
				return SATURDAY;
			default:
				return null;
			}
		}
	}
}
