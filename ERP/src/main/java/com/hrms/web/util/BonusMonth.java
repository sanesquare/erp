package com.hrms.web.util;

/**
 * 
 * @author Jithin Mohan
 * @since 14-May-2015
 *
 */
public class BonusMonth {

	/**
	 * method to get total bonus period
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @return period
	 */
	public static Integer getBonusPeriod(Integer year, Integer month, Integer day) {
		Integer period = 0;
		if (year > 0)
			period = year * 12;
		period = period + month;
		if (day >= 30)
			period = period + 1;
		return period;
	}
}
