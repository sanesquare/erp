package com.hrms.web.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.vo.BiometricAttendanceVo;
import com.hrms.web.vo.EmployeeVo;

public class ReadFromExcel {

	/**
	 * Method to read data from excel file
	 * 
	 * @param fileName
	 * @return
	 */
	public static List<BiometricAttendanceVo> readData(String fileName) {
		List<BiometricAttendanceVo> attendanceVos = new ArrayList<BiometricAttendanceVo>();
		File file = new File(fileName);
		FileInputStream fileInputStream = null;
		HSSFWorkbook workbook = null;
		HSSFSheet sheet;
		Iterator<Row> rowIterator;
		try {
			fileInputStream = new FileInputStream(file);
			String fileExtension = fileName.substring(fileName.indexOf("."));
			if (fileExtension.equals(".xls")) {
				workbook = new HSSFWorkbook(new POIFSFileSystem(fileInputStream));
			} else {
				System.out.println("Wrong File Type");
			}
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			sheet = workbook.getSheetAt(0);
			rowIterator = sheet.iterator();
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				Row row = sheet.getRow(i);
				int cellCount = row.getLastCellNum();
				System.out.println(row.getCell(1).getStringCellValue());
				List<String> time = new ArrayList<String>();

				BiometricAttendanceVo attendanceVo = new BiometricAttendanceVo();
				attendanceVo.setEmployeeCode(row.getCell(1).getStringCellValue());
				attendanceVo.setDepartment(row.getCell(0).getStringCellValue());
				attendanceVo.setDate(row.getCell(4).getStringCellValue());
				time.add(row.getCell(5).getStringCellValue());

				if (cellCount > 6) {
					int n = i++;
					for (int k = 7; k <= cellCount; k++) {
						time.add(row.getCell(k - 1).getStringCellValue());
						n++;
					}
				}
				attendanceVo.setTime(time);
				attendanceVos.add(attendanceVo);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return attendanceVos;
	}

	/**
	 * Method to write data to existing excel file
	 * 
	 * @param fileName
	 */
	public static void writeDataToExistingExcelFile(String fileName, List<EmployeeVo> employeeVos,
			HttpServletResponse respo) {
		try {
			new File(fileName).createNewFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		File file = new File(fileName);
		FileInputStream fileInputStream = null;
		HSSFWorkbook workbook = null;
		HSSFSheet sheet;
		Iterator<Row> rowIterator;
		try {
			fileInputStream = new FileInputStream(file);
			String fileExtension = fileName.substring(fileName.indexOf("."));
			if (!fileExtension.equals(".xls"))
				throw new ItemNotFoundException("Invalid File.");
			workbook = new HSSFWorkbook(new POIFSFileSystem(fileInputStream));
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			sheet = workbook.getSheetAt(0);
			rowIterator = sheet.iterator();

			Map<String, Object[]> data = new TreeMap<String, Object[]>();

			for (EmployeeVo vo : employeeVos) {
				String gender = "male";
				if (vo.getGender() != null)
					gender = vo.getGender();
				String name = "test";
				if (vo.getName() != null)
					name = vo.getName();
				data.put(vo.getEmployeeId().toString(),
						new Object[] { vo.getEmployeeDepartment(), name, vo.getEmployeeCode(),
								vo.getEmployeeId().toString(), "", "", "", "", gender, "", "", "", "", "", "" });
			}

			// Iterate over data and write to sheet
			Set<String> keyset = data.keySet();
			int rownum = sheet.getLastRowNum() + 1;
			for (String key : keyset) {
				Row row = sheet.createRow(rownum++);
				Object[] objArr = data.get(key);
				int cellnum = 0;
				for (Object obj : objArr) {
					Cell cell = row.createCell(cellnum++);
					if (obj instanceof String)
						cell.setCellValue((String) obj);
					else if (obj instanceof Integer)
						cell.setCellValue((Integer) obj);
				}
			}
			FileOutputStream out = new FileOutputStream(file);
			workbook.write(out);
			fileInputStream.close();
			respo.setContentType("application/vnd.ms-excel");
			respo.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			workbook.write(respo.getOutputStream());
			respo.flushBuffer();
			System.out.println("written successfully on disk.");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (file != null)
				file.delete();
		}
	}

	public static List<BiometricAttendanceVo> readFilesFromDirectory(String directory) {
		new File(directory.replace("\'", "\\") + "\\archive").mkdirs();
		File folder = new File(directory);
		File[] files = folder.listFiles();
		List<BiometricAttendanceVo> attendanceVos = new ArrayList<BiometricAttendanceVo>();
		for (File file : files) {
			if (file.isFile()) {
				// System.out.println("File:: "+file.getName());
				attendanceVos.addAll(readData(file.getAbsolutePath()));
				System.out.println("file is moving..");
				System.out.println("file moved : "
						+ file.renameTo(new File(directory.replace("\'", "\\") + "\\archive\\" + file.getName())));
			} else if (file.isDirectory())
				System.out.println("Directory :: " + file.getName());
		}
		// System.out.println("Full List SIze : "+attendanceVos.size());
		for (BiometricAttendanceVo vo : attendanceVos) {
			System.out.println(vo.getEmployeeCode() + "\t" + vo.getDate() + "\t" + vo.getTime().size());
		}
		return attendanceVos;
	}

	/**
	 * method to read datas from multipartfile
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static Set<BiometricAttendanceVo> readExcelFromMultipart(MultipartFile file) throws IOException {
		Set<BiometricAttendanceVo> attendanceVos = new HashSet<BiometricAttendanceVo>();
		String filename = file.getOriginalFilename();
		HSSFWorkbook workbook = null;
		HSSFSheet sheet;
		String fileExtension = filename.substring(filename.indexOf("."));
		if (!fileExtension.equals(".xls"))
			throw new ItemNotFoundException("Invalid File");
		workbook = new HSSFWorkbook(file.getInputStream());

		sheet = workbook.getSheetAt(0);
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			Row row = sheet.getRow(i);
			int cellCount = row.getLastCellNum();
			List<String> time = new ArrayList<String>();

			BiometricAttendanceVo attendanceVo = new BiometricAttendanceVo();
			attendanceVo.setEmployeeCode(row.getCell(1).getStringCellValue());
			attendanceVo.setDepartment(row.getCell(0).getStringCellValue());
			attendanceVo.setDate(row.getCell(4).getStringCellValue());
			time.add(row.getCell(5).getStringCellValue());

			if (cellCount > 6) {
				int n = i + 1;
				for (int k = 7; k <= cellCount; k++) {
					time.add(row.getCell(k - 1).getStringCellValue());
					n++;
				}
			}
			attendanceVo.setTime(time);
			attendanceVos.add(attendanceVo);
		}
		return attendanceVos;
	}

	public static void writeExcel(String fileName , List<EmployeeVo> employeeVos, HttpServletResponse response) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet excelSheet = workbook.createSheet("employees");
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("Dept");
		excelHeader.createCell(1).setCellValue("Name");
		excelHeader.createCell(2).setCellValue("User ID");
		excelHeader.createCell(3).setCellValue("Enroll ID");
		excelHeader.createCell(4).setCellValue("Card No");
		excelHeader.createCell(5).setCellValue("Att Rules");
		excelHeader.createCell(6).setCellValue("Default Att");
		excelHeader.createCell(7).setCellValue("Default Weekend");
		excelHeader.createCell(8).setCellValue("Gender");
		excelHeader.createCell(9).setCellValue("Register Date");
		excelHeader.createCell(10).setCellValue("Title");
		excelHeader.createCell(11).setCellValue("Birthday");
		excelHeader.createCell(12).setCellValue("ID No");
		excelHeader.createCell(13).setCellValue("Phone");
		excelHeader.createCell(14).setCellValue("Email");
		excelHeader.createCell(15).setCellValue("Address");
		int record = 1;
		for (EmployeeVo employeeVo : employeeVos) {
			HSSFRow excelRow = excelSheet.createRow(record++);
			excelRow.createCell(0).setCellValue(employeeVo.getEmployeeDepartment());
			excelRow.createCell(1).setCellValue(employeeVo.getName());
			excelRow.createCell(2).setCellValue(employeeVo.getEmployeeCode());
			excelRow.createCell(3).setCellValue(employeeVo.getEmployeeId().toString());
			System.out.println(employeeVo.getGender());
			excelRow.createCell(8).setCellValue(employeeVo.getGender());
		}
		try {
			/*FileOutputStream outputStream = new FileOutputStream(fileName);
			workbook.write(outputStream);*/
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			workbook.write(response.getOutputStream());
			response.flushBuffer();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			File file = new File(fileName);
			if(file!=null)
				file.delete();
		}
	}

}
