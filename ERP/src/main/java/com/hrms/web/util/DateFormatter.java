package com.hrms.web.util;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.logger.Log;

/**
 * 
 * @author Shamsheer
 * @since 11-March-2015
 *
 */
public class DateFormatter {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	/**
	 * method to convert string date to date
	 * 
	 * @param stringDate
	 * @return
	 */
	public static Date convertStringToDate(String stringDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date dateAsDate = null;
		try {
			dateAsDate = dateFormat.parse(stringDate);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dateAsDate;

	}
	
	public static Date findLastDayOfPreviousMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = 1;
		calendar.set(year, month, day);
		day = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		date = convertStringToDate(day + "/" + month + "/" + year);
		return date;
	}
	
	/**
	 * convert string to date : format: yyyy-mm-dd
	 * @param stringDate
	 * @return
	 */
	public static Date convertStringFromRestToDate(String stringDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dateAsDate = null;
		try {
			dateAsDate = dateFormat.parse(stringDate);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dateAsDate;

	}
	
	/**
	 * 
	 * @param stringDate
	 * @return
	 */
	public static Date convertFullyQualifiedStringToDate(String stringDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		Date dateAsDate = null;
		try {
			dateAsDate = dateFormat.parse(stringDate);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dateAsDate;

	}
	
	/**
	 * method to convert string date to date
	 * 
	 * @param stringDate
	 * @return
	 */
	public static Date convertStringMmDdYyyyToDate(String stringDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date dateAsDate = null;
		try {
			dateAsDate = dateFormat.parse(stringDate);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dateAsDate;

	}

	/**
	 * method for convert date to 24 hr format
	 * 
	 * @param inputDate
	 * @param time
	 * @param meridian
	 * @return date
	 */
	public static Date convert12To24(String inputDate, String time, String meridian) {
		String input = inputDate + " " + time + ":00 " + meridian;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
		DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
			date = outputformat.parse(output);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return date;
	}

	/**
	 * method for create date with time in 24 hour format
	 * 
	 * @param time
	 * @return date
	 */
	public static Date createUtilDateWithTime(String time) {
		List<String> meridian = Arrays.asList(time.split(" "));
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
		DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa");
		Date date = null;
		String output = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateAsString = dateFormat.format(new Date());
			String input = dateAsString + " " + meridian.get(0) + ":00" + " " + meridian.get(1);
			date = df.parse(input);
			output = outputformat.format(date);
			date = outputformat.parse(output);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return date;
	}

	/**
	 * method to convert date to string with time
	 * 
	 * @param date
	 * @return
	 */
	public static String createUtilDateToString(Date date) {
		String dateAsString = null;
		try {
			DateFormat outputformat = new SimpleDateFormat("hh:mm aa");
			dateAsString = outputformat.format(date);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dateAsString;
	}

	/**
	 * method to get current year
	 * 
	 * @return year
	 */
	public static int getCurrentYear() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.YEAR);
	}

	/**
	 * method to convert date to string
	 * 
	 * @param date
	 * @return
	 */
	public static String convertDateToString(Date date) {
		String dateAsString = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			dateAsString = dateFormat.format(date);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dateAsString;
	}
	
	public static String getMonthYesr(Date date) {
		String dateAsString = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");
			dateAsString = dateFormat.format(date);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dateAsString;
	}
	
	public static Date getDateFromMonthYesr(String stringDate) {
		Date date = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MMMM yyyy");
			date = dateFormat.parse(stringDate);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return date;
	}
	
	public static List<String> getMonthYearList(Date startDate,Date endDate) {
		DateFormat formater = new SimpleDateFormat("MMMM yyyy");

		Calendar beginCalendar = Calendar.getInstance();
		Calendar finishCalendar = Calendar.getInstance();

		beginCalendar.setTime(startDate);
		finishCalendar.setTime(endDate);
		List<String> dates = new ArrayList<String>();
		while (beginCalendar.before(finishCalendar)) {
			// add one month to date per loop
			String date = formater.format(beginCalendar.getTime());
			dates.add(date.trim());
			beginCalendar.add(Calendar.MONTH, 1);
		}
		return dates;
	}
	
	public static String convertDateToMonthYearString(Date date) {
		String dateAsString = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("MMMM/yyyy");
			dateAsString = dateFormat.format(date);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dateAsString;
	}

	/**
	 * retrurn date in a detailed format (eg: Monday, October 13, 2014)
	 * 
	 * @param date
	 * @return
	 */
	public static String convertDateToDetailedString(Date date) {
		String dateAsString = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM dd, yyyy");
			dateAsString = dateFormat.format(date);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dateAsString;
	}

	/**
	 * retrurn unique timestamp by refering
	 * CommonConstants.UNIQUE_TIMESTAMP_FORMAT
	 * 
	 * @param date
	 * @return
	 */
	public static String getUniqueTimestamp() {
		String timestampAsString = null;
		try {
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(CommonConstants.UNIQUE_TIMESTAMP_FORMAT);
			timestampAsString = dateFormat.format(date);
		} catch (Exception e) {
			logger.error("", e);
		}
		return timestampAsString;
	}

	/**
	 * method to find time meridian
	 * 
	 * @param time
	 * @return timeValues
	 */
	public static List<String> getTimeValues(String time) {
		List<String> values = Arrays.asList(time.split(" "));
		return values;
	}

	/**
	 * method to convert string time into sql time
	 * 
	 * @param time
	 * @return
	 */
	public static Time convertStringTimeToSqlTime(String time) {
		SimpleDateFormat format = new SimpleDateFormat("hh:mm aa");
		Time convertedTime = null;
		try {
			convertedTime = new Time(format.parse(time).getTime());
		} catch (Exception e) {
			logger.error("", e);
		}
		return convertedTime;
	}
	
	public static Time convertStringQualifiedTimeToSqlTime(String time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		Time convertedTime = null;
		try {
			convertedTime = new Time(format.parse(time).getTime());
		} catch (Exception e) {
			logger.error("", e);
		}
		return convertedTime;
	}
	
	public static Time convertString24HrTimeToSqlTime(String time) {
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		Time convertedTime = null;
		try {
			convertedTime = new Time(format.parse(time).getTime());
		} catch (Exception e) {
			logger.error("", e);
		}
		return convertedTime;
	}

	/**
	 * method to convert sql time to string
	 * 
	 * @param time
	 * @return
	 */
	public static String convertSqlTimeToString(Time time) {
		String stringTime = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("hh:mm aa");
			stringTime = format.format(time);
		} catch (Exception e) {
			logger.error("", e);
		}
		return stringTime;
	}

	/**
	 * Method to create start date from selected month and year
	 * 
	 * @param month
	 * @param year
	 * @return
	 */
	public static Date createStartDateFromMonthYear(String month, String year) {
		String startDate = null;
		startDate = "01" + "/" + month + "/" + year;
		return convertStringToDate(startDate);
	}

	/**
	 * Method to create end date from selected month and year
	 * 
	 * @param month
	 * @param year
	 * @return
	 */
	public static Date createEndDateFromMonthYear(String month, String year) {
		String endDate = null;
		Calendar calendar = Calendar.getInstance();
		int yearNew = Integer.parseInt(year);
		int monthNew = Integer.parseInt(month);
		int maxDay = 0;

		calendar.set(yearNew, monthNew - 1, 1);
		// Getting Maximum day for Given Month
		maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		endDate = maxDay + "/" + month + "/" + year;
		return convertStringToDate(endDate);

	}

	/**
	 * Method to get String end Date
	 * 
	 * @param month
	 * @param year
	 * @return
	 */
	public static String getEndDateFromMonthAndYear(String month, String year) {
		String endDate = null;
		Calendar calendar = Calendar.getInstance();

		int yearNew = Integer.parseInt(year);
		int monthNew = Integer.parseInt(month);
		int maxDay = 0;

		calendar.set(yearNew, monthNew - 1, 1);
		// Getting Maximum day for Given Month
		maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		endDate = maxDay + "/" + month + "/" + year;
		return endDate;
	}

	/**
	 * method to convert milliseconds to hour:minutes:seconds
	 * 
	 * @param millis
	 * @return
	 */
	public static String convertMilliSecondsToHours(Long millis) {
		String totalHours = String.format(
				"%02d.%02d",
				TimeUnit.MILLISECONDS.toHours(millis),
				TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The
																							// change
																							// is
																							// in
																							// this
																							// line
				TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		return totalHours;
	}

	/**
	 * Method to convert milli seconds to double value
	 * 
	 * @param millis
	 * @return
	 */
	public static double convertMilliSecondsToHoursAndMinutes(Long millis) {
		String totalHours = String.format(
				"%02d.%02d",
				TimeUnit.MILLISECONDS.toHours(millis),
				TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)));
		double result = Double.parseDouble(totalHours);
		return result;

	}

	/**
	 * Method to calculate difference between dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int getDifferenceBetweenDays(String startDate, String endDate) {
		int result = 0;
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date start = null;
		Date end = null;
		try {
			start = formatter.parse(startDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			end = formatter.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Long diff = end.getTime() - start.getTime();
		result = (int) (diff / (24 * 60 * 60 * 1000));

		return result + 1;
	}

	/**
	 * method to return day of week from a date
	 * 
	 * @param date
	 * @return dayOfWeek
	 */
	public static String getDayOfWeek(Date date) {
		String dayOfWeek = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("EEEE");
			dayOfWeek = dateFormat.format(date);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return dayOfWeek;
	}

	/**
	 * 
	 * @param inTime
	 * @param outTime
	 * @return
	 */
	public static String getTimeDifference(Time inTime, Time outTime) {
		Long millis = null;
		String hoursWorked = null;
		if (inTime != null && outTime != null) {
			if (inTime.before(outTime)) {
				millis = (outTime.getTime() - inTime.getTime());
			} else {
				// millis = (inTime.getTime() - outTime.getTime());
				hoursWorked = "0";
			}
		} else {
			hoursWorked = "0";
		}
		if (millis != null) {
			hoursWorked = DateFormatter.convertMilliSecondsToHours(millis);
		}
		return hoursWorked;
	}
	/**
	 * Method to get difference in months
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getDifferenceInMonths(String start , String end){
		Date startDate = convertStringToDate(start);
		Date endDate = convertStringToDate(end);
		Calendar startCalendar =Calendar.getInstance();
		startCalendar.setTime(startDate);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);

		int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		
		return diffMonth;
	}
	/**
	 * Method to get age
	 * @param dob
	 * @return
	 */
	public static int getAge(Date bdate) {
		int age = 0;
		Calendar bday = Calendar.getInstance();
		bday.setTime(bdate);
		Calendar today = Calendar.getInstance();
		age = today.get(Calendar.YEAR) - bday.get(Calendar.YEAR);
		if (today.get(Calendar.MONTH) < bday.get(Calendar.MONTH)) {
			age--;
		} else if (today.get(Calendar.MONTH) == bday.get(Calendar.MONTH)
				&& today.get(Calendar.DAY_OF_MONTH) < bday.get(Calendar.DAY_OF_MONTH)) {
			age--;
		}
		return age;
	}
}
