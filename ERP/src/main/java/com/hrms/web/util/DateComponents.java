package com.hrms.web.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Jithin Mohan
 * @since 25-April-2015
 *
 */
public class DateComponents {

	/**
	 * method to generate year
	 * 
	 * @return yearList
	 */
	public static List<Integer> getYear() {
		List<Integer> yearList = new ArrayList<Integer>();
		for (int i = 0; i <= 10; i++)
			yearList.add(i);
		return yearList;
	}

	/**
	 * method to generate month
	 * 
	 * @return monthList
	 */
	public static List<Integer> getMonth() {
		List<Integer> monthList = new ArrayList<Integer>();
		for (int i = 0; i <= 12; i++)
			monthList.add(i);
		return monthList;
	}

	/**
	 * method to generate day
	 * 
	 * @return dayList
	 */
	public static List<Integer> getDay() {
		List<Integer> dayList = new ArrayList<Integer>();
		for (int i = 0; i <= 31; i++)
			dayList.add(i);
		return dayList;
	}
}
