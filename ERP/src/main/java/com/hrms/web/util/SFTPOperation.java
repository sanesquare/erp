package com.hrms.web.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.hrms.web.constants.CommonConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.vo.SFTPFilePropertiesVo;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.sun.jmx.snmp.Timestamp;

/**
 * 
 * @author sangeeth 12-03-2015
 */

@Component
public class SFTPOperation {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	public static Session session = null;

	/**
	 * sftp RSA connection
	 * 
	 * @return
	 */
	static byte[] readMyPrivateKeyFromFile(String key) {
		byte[] arr = null;
		try {
			arr = key.getBytes();
		} catch (Exception e) {
			logger.error("readMyPrivateKeyFromFile:", e);
		}
		return arr;
	}

	/**
	 * methode to connect sftp linux server
	 * 
	 * @return
	 */
	private ChannelSftp getSftpConnection(String IP,String user,String key,int port) {
		ChannelSftp channelSftp = null;
		try {
			JSch jSch = new JSch();
			final byte[] prvkey = readMyPrivateKeyFromFile(key); // Private key
																// must be byte
																// array
			final byte[] emptyPassPhrase = new byte[0]; // Empty passphrase for
														// now, get real
														// passphrase from
														// MyUserInfo
			jSch.addIdentity(user, // String userName
					prvkey, // byte[] privateKey
					null, // byte[] publicKey
					emptyPassPhrase // byte[] passPhrase
			);
			
			session = jSch.getSession(user,IP,port);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			Channel channel = session.openChannel("sftp");
			channelSftp = (ChannelSftp) channel;
			
		} catch (Exception e) {
			logger.error("getSftpConnection:", e);
		}
		return channelSftp;
	}

	/**
	 * method to send file on sftp server
	 * 
	 * @param SFTPFilePropertiesVo
	 * @return file url on sftp server
	 */
	public String sendFileToSFTPServer(SFTPFilePropertiesVo sftpFilePropertiesVo) {
		ChannelSftp sftp = null;
		try {
			//logger.info("hoiiiiiiiiiiiiiii");
			sftp = getSftpConnection(sftpFilePropertiesVo.getSftpIP(),sftpFilePropertiesVo.getSftpUser(),sftpFilePropertiesVo.getSftpKey(),sftpFilePropertiesVo.getSftpPort());
			
			sftp.connect();

			String destinationPath = sftpFilePropertiesVo.getSftpRootPath() + "/" + sftpFilePropertiesVo.getModuleName() + "/"
					+ sftpFilePropertiesVo.getUserName();

			String sourceFileName = sftpFilePropertiesVo.getFileName().replace(" ", "_");
			String sourceFileBasename = FilenameUtils.getBaseName(sourceFileName);
			String sourceFileExtension = FilenameUtils.getExtension(sourceFileName);
			// Create directories if not exist
			String[] complPath = destinationPath.split("/");
			for (int i = 0; i < complPath.length; i++) {
				String dir = complPath[i];
				if (i == 0) {
					dir = "/" + dir;
				}
				if (dir.length() > 0) {
					try {
						sftp.cd(dir);
					} catch (SftpException e2) {
						sftp.mkdir(dir);
						sftp.cd(dir);
					}
				}
			}

			String targetFilename = destinationPath + "/" + sourceFileBasename + "_"
					+ DateFormatter.getUniqueTimestamp() + "." + sourceFileExtension;
			InputStream in = sftpFilePropertiesVo.getFile().getInputStream();
			sftp.put(in, targetFilename);
			in.close();
			sftp.disconnect();
			session.disconnect();

			return targetFilename;

		} catch (Exception e) {

			try {
				sftp.disconnect();
				session.disconnect();
			} catch (Exception e1) {
				logger.error("sendFileToSFTPServer:", e1);
			}
			logger.error("sendFileToSFTPServer:", e);
			//sftp.disconnect();
			return null;
		}
	}

	/**
	 * method to download document file from sftp server
	 * 
	 * @param url
	 * @param HttpServletResponse
	 */
	public void downloadFileFromSFTP(String url, HttpServletResponse response) {

		String fileName = FilenameUtils.getName(url);

		// tell browser program going to return an application file
		// instead of html page
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=" + fileName);

		ChannelSftp sftp = null;

		try {
			sftp = getSftpConnection(CommonConstants.SFTP_DOCUMENT_IP_ADDRESS,CommonConstants.SFTP_DOCUMENT_USER,
					CommonConstants.SFTP_DOCUMENT_RSA,CommonConstants.SFTP_DOCUMENT_PORT);
			sftp.connect();

			byte[] buffer = new byte[1024];
			BufferedInputStream bis = new BufferedInputStream(sftp.get(url));

			ServletOutputStream out = response.getOutputStream();

			InputStream in = sftp.get(url);

			byte[] outputByte = new byte[4096];
			// copy binary contect to output stream
			while (in.read(outputByte, 0, 4096) != -1) {
				out.write(outputByte, 0, 4096);
			}
			in.close();
			out.flush();
			out.close();

			sftp.disconnect();
			session.disconnect();
		} catch (Exception e) {
			sftp.disconnect();
			session.disconnect();
			logger.error("downloadFileFromSFTP", e);
		}
	}

	/**
	 * method to create document file from sftp server
	 * 
	 * @param url
	 * @param desination
	 *            path with filename and its extension
	 * @return created file
	 */
	public File createFileFromSFTP(String url, String destination) {

		ChannelSftp sftp = null;
		File savedFile = null;
		try {
			sftp = getSftpConnection(CommonConstants.SFTP_DOCUMENT_IP_ADDRESS,CommonConstants.SFTP_DOCUMENT_USER,
					CommonConstants.SFTP_DOCUMENT_RSA,CommonConstants.SFTP_DOCUMENT_PORT);
			sftp.connect();

			byte[] buffer = new byte[1024];
			BufferedInputStream bis = new BufferedInputStream(sftp.get(url));
			File newFile = new File(destination);
			OutputStream os = new FileOutputStream(newFile);
			BufferedOutputStream bos = new BufferedOutputStream(os);
			int readCount;
			while ((readCount = bis.read(buffer)) > 0) {
				bos.write(buffer, 0, readCount);
			}

			bis.close();
			bos.close();

			sftp.disconnect();
			session.disconnect();
		} catch (Exception e) {
			sftp.disconnect();
			session.disconnect();
			logger.error("downloadFileFromSFTP", e);
		}
		return savedFile;
	}

	/**
	 * method to remove document file from sftp server
	 * 
	 * @param url
	 * @return true/false
	 */
	public boolean removeFileFromSFTP(String url) {
		ChannelSftp sftp = null;
		Channel channel=null;
		try {

			sftp = getSftpConnection(CommonConstants.SFTP_DOCUMENT_IP_ADDRESS,CommonConstants.SFTP_DOCUMENT_USER,
					CommonConstants.SFTP_DOCUMENT_RSA,CommonConstants.SFTP_DOCUMENT_PORT);

			/*sftp.connect();
			sftp.rm(url);
*/
			sftp.connect();
			String command="rm -rf "+url  ;
		    channel=session.openChannel("exec");
		      ((ChannelExec)channel).setCommand(command);
		      

		    channel.connect();
		    channel.disconnect();
		    
			sftp.disconnect();
			session.disconnect();

			return true;
		} catch (Exception e) {
			if(channel!=null){
				channel.disconnect();
			}
			if(sftp!=null){
				sftp.disconnect();
			}
			if(session!=null){
				session.disconnect();
			}
			logger.error("removeFileFromCDN", e);
			return false;
		}
	}
	
	/**
	 * method to remove cdn file from sftp server
	 * 
	 * @param url
	 * @return true/false
	 */
	public boolean removeFileFromCDN(String url) {
		ChannelSftp sftp = null;
		Channel channel=null;
		try {

			sftp = getSftpConnection(CommonConstants.SFTP_CDN_IP_ADDRESS,CommonConstants.SFTP_CDN_USER,
					CommonConstants.SFTP_CDN_RSA,CommonConstants.SFTP_CDN_PORT);

			/*sftp.connect();
			sftp.rm(url);*/
			
			sftp.connect();
			String command="rm -rf "+url  ;
		    channel=session.openChannel("exec");
		      ((ChannelExec)channel).setCommand(command);
		      

		    channel.connect();
		    channel.disconnect();
		    
			sftp.disconnect();
			session.disconnect();

			return true;
		} catch (Exception e) {
			if(channel!=null){
				channel.disconnect();
			}
			if(sftp!=null){
				sftp.disconnect();
			}
			if(session!=null){
				session.disconnect();
			}
			logger.error("removeFileFromCDN", e);
			return false;
		}
	}
	
	/**
	 * method to remove cdn directory structure from sftp server
	 * 
	 * @param url
	 * @return true/false
	 */
	public boolean removeDirectoryFromCDN(String url) {
		ChannelSftp sftp = null;
		try {

			sftp = getSftpConnection(CommonConstants.SFTP_CDN_IP_ADDRESS,CommonConstants.SFTP_CDN_USER,
					CommonConstants.SFTP_CDN_RSA,CommonConstants.SFTP_CDN_PORT);

			sftp.connect();
			String command="rm -rf "+url  ;
					Channel channel=session.openChannel("exec");
		      ((ChannelExec)channel).setCommand(command);
		 
		      // get I/O streams for remote scp
		     // OutputStream out=channel.getOutputStream();
		     // InputStream in=channel.getInputStream();
		 
            channel.connect();
			sftp.disconnect();
			session.disconnect();

			return true;
		} catch (Exception e) {
			sftp.disconnect();
			session.disconnect();
			logger.error("removeFileFromCDN", e);
			e.printStackTrace();
			return false;
		}
	}

	public static void main(String[] args) {
		SFTPOperation fileOperation = new SFTPOperation();
		
		File file = new File("d:/Untitled-1.jpg");
	    DiskFileItem fileItem = new DiskFileItem("file", "text/plain", false, file.getName(), (int) file.length() , file.getParentFile());
	    try {
			fileItem.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
	    
		  SFTPFilePropertiesVo filePropertiesVo = new SFTPFilePropertiesVo();
		  filePropertiesVo.setFile(multipartFile);
		  filePropertiesVo.setFileName("Untitled-1.jpg");
		  filePropertiesVo.setModuleName("Employee");
		  filePropertiesVo.setUserName("sangeeth"); 
		  filePropertiesVo.setSftpRootPath(CommonConstants.SFTP_CDN_ROOT_PATH);
		  filePropertiesVo.setSftpIP(CommonConstants.SFTP_CDN_IP_ADDRESS);
		  filePropertiesVo.setSftpUser(CommonConstants.SFTP_CDN_USER);
		  filePropertiesVo.setSftpKey(CommonConstants.SFTP_CDN_RSA);
		  filePropertiesVo.setSftpPort(CommonConstants.SFTP_CDN_PORT);
		  
		//System.out.println(fileOperation.sendFileToSFTPServer(filePropertiesVo));
		 // fileOperation.removeFileFromCDN("/home/3Gftp/cdn");
		// fileOperation
		// .downloadFileFromSFTP("/home/3Gftp/incoming/Employee/sangeeth/Untitled-1_20150312_162950.jpg");

		//fileOperation.removeFileFromSFTP("/home/3Gftp/incoming/Employee/sangeeth/Untitled-1.jpg");
	}

}
