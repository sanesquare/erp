package com.hrms.web.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;

import org.slf4j.Logger;

import com.hrms.web.logger.Log;

/**
 * 
 * @author sangeeth
 *13/3/2015
 *
 */
public class IPAddress {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;
	
	/**
	 * to get public IP Address
	 * @return IP Address
	 */
	public String getPublicIPAddress(){
		String ip="";
		try{
			URL whatismyip = new URL("http://checkip.amazonaws.com");
			BufferedReader in = new BufferedReader(new InputStreamReader(
			                whatismyip.openStream()));

			ip = in.readLine(); //you get the IP as a String
		}catch(Exception e){
			logger.error("getPublicIPAddress:",e);
		}
		return ip;
	}
	
	/**
	 * to get public IP Address
	 * @return IP Address
	 */
	public String getPrivateIPAddress(){
		String ip="";
		try{
			ip= InetAddress.getLocalHost()+"";
		}catch(Exception e){
			logger.error("getPrivateIPAddress:",e);
		}
		return ip;
	}
	
	
}
