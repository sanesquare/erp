package com.accounts.web.form;

import java.util.List;

import com.accounts.web.vo.PaymentVo;

public class PaymentsForm {

	private List<PaymentVo> paymentsList;

	/**
	 * @return the paymentsList
	 */
	public List<PaymentVo> getPaymentsList() {
		return paymentsList;
	}

	/**
	 * @param paymentsList the paymentsList to set
	 */
	public void setPaymentsList(List<PaymentVo> paymentsList) {
		this.paymentsList = paymentsList;
	}
}
