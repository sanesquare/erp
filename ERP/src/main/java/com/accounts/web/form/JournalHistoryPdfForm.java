package com.accounts.web.form;

import java.util.HashSet;
import java.util.Set;

import com.accounts.web.vo.JournalHistoryPdfVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 17-August-2015
 *
 */
public class JournalHistoryPdfForm {

	private String type;
	
	private Set<JournalHistoryPdfVo> pdfVos = new HashSet<JournalHistoryPdfVo>();

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the pdfVos
	 */
	public Set<JournalHistoryPdfVo> getPdfVos() {
		return pdfVos;
	}

	/**
	 * @param pdfVos the pdfVos to set
	 */
	public void setPdfVos(Set<JournalHistoryPdfVo> pdfVos) {
		this.pdfVos = pdfVos;
	}

}
