package com.accounts.web.service;

import java.util.List;

import com.accounts.web.vo.LedgerVo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 13-August-2015
 */
public interface LedgerService {

	/**
	 * Method to save or update ledger
	 * 
	 * @param ledgerVo
	 * @return
	 */
	public Long saveOrUpdateLedger(LedgerVo ledgerVo);

	/**
	 * Method to find ledger By Id
	 * @param id
	 * @return
	 */
	public LedgerVo findLedgerById(Long id);
	/**
	 * Method to delete ledger
	 * 
	 * @param id
	 */
	public void deleteLedger(Long id);

	/**
	 * Method to list all ledgers
	 * 
	 * @return
	 */
	public List<LedgerVo> findAllLedgers();

	/**
	 * List ledgers for company
	 *  
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> findLedgersForCompany(Long companyId);
	
	/**
	 * method to find ledgers for salary
	 * @return
	 */
	public List<LedgerVo> findAccountsForSalary(Long companyId);
	
	/**
	 * method to find ledgers for contra
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> contraLedgers(Long companyId);
	
	/**
	 * method to find ledgers for journal
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> journalLedgers(Long companyId);
	
	/**
	 * method to find ledgers for payment
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> paymentLedgers(Long companyId);
	
	/**
	 * method to find ledgers for receipt
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> receiptLedgers(Long companyId);
}
