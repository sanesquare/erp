package com.accounts.web.service;

import java.util.List;

import com.accounts.web.vo.LedgerGroupVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
public interface LedgerGroupService {

	/**
	 * Method to save or update Ledger group
	 * @param groupVo
	 * @return
	 */
	public Long saveOrUpdateLedgerGroup(LedgerGroupVo groupVo);
	/**
	 * Method to delete ledger group
	 * @param id
	 */
	public void deleteLedgerGroup(Long id);
	/**
	 * Method find ledger group by id
	 * @param id
	 * @return
	 */
	public LedgerGroupVo findLedgerGroupById(Long id);
	/**
	 * Method to find ledger group by name
	 * @param name
	 * @return
	 */
	public LedgerGroupVo findLedgerGroupByName(String name , Long companyId);
	/**
	 * Method to list ledger group by name
	 * @return
	 */
	public List<LedgerGroupVo> findAllLedgerGroups();
	/**
	 * Method to find ledger groups for company
	 * @param companyId
	 * @return
	 */
	public List<LedgerGroupVo> findAllLedgerGroupsForCompany(Long companyId);
	
}
