package com.accounts.web.service;

import java.util.List;

import com.accounts.web.entities.LedgerAccountType;
import com.accounts.web.vo.LedgerAccountTypeVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
public interface LedgerAccountTypeService {

	/**
	 * Method to find account type by name
	 * @param type
	 * @return
	 */
	public LedgerAccountTypeVo findAccountTypeByName(String type);
	
	
	/**
	 * Method to list all Account types
	 * @return
	 */
	public List<LedgerAccountTypeVo> findAllAccountTypes();
	
}
