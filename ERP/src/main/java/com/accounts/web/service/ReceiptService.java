package com.accounts.web.service;

import java.util.List;

import com.accounts.web.entities.Receipts;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.ReceiptVo;

/**
 * 
 * @author Shamsheer & Vinuths
 * @since 18-August-2015
 *
 */
public interface ReceiptService {

	/**
	 * Method to save or update receipt
	 * 
	 * @param receiptVo
	 * @return
	 */
	public Long saveOrUpdateReceipt(ReceiptVo receiptVo);

	/**
	 * Method to delete receipt
	 * 
	 * @param id
	 */
	public void deleteReceipt(Long id);

	/**
	 * Method to find receipt by id
	 * 
	 * @param id
	 * @return
	 */
	public ReceiptVo findReceiptById(Long id);

	/**
	 * method to find all receipts for company
	 * 
	 * @param companyId
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForCompany(Long companyId);
	/**
	 * method to find all journals for company within date
	 * @param companyId
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForCompanyWithinDate(Long companyId , String start , String end);
	/**
	 * Method to find all receipt for by ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate);

	/**
	 * Method to find receipts for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForToLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate);

	/**
	 * Method to find receipts for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForByLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate);

	/**
	 * Method to get receipt voucher number
	 * 
	 * @param companyId
	 * @return
	 */
	public String getReceiptNumberForCompany(Long companyId);

}
