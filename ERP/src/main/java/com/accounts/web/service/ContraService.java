package com.accounts.web.service;

import java.util.List;

import com.accounts.web.entities.Contra;
import com.accounts.web.vo.ContraVo;
import com.accounts.web.vo.JournalVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
public interface ContraService {
	/**
	 * Method to save or update contra
	 * 
	 * @param contraVo
	 * @return
	 */
	public Long saveOrUpdateContra(ContraVo contraVo);

	/**
	 * Method to delete contra
	 * 
	 * @param id
	 */
	public void deleteContra(Long id);

	/**
	 * Method to find contra by id
	 * 
	 * @param id
	 * @return
	 */
	public ContraVo findContraById(Long id);

	/**
	 * method to find all contras for company
	 * 
	 * @param companyId
	 * @return
	 */
	public List<ContraVo> findAllContrasForCompany(Long companyId);

	/**
	 * method to find all contras for company within date
	 * 
	 * @param companyId
	 * @return
	 */
	public List<ContraVo> findAllContrasForCompanyWithinDate(Long companyId, String start, String end);

	/**
	 * Method to find all contra for by ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ContraVo> findAllContrasForLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate);

	/**
	 * Method to find contras for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ContraVo> findAllContrasForToLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate);

	/**
	 * Method to find contras for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ContraVo> findAllContrasForByLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate);

	/**
	 * Method to get contra voucher number
	 * 
	 * @param companyId
	 * @return
	 */
	public String getContraNumberForCompany(Long companyId);

}
