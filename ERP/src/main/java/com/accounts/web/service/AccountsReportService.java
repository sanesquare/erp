package com.accounts.web.service;

import java.util.List;
import java.util.Map;

import com.accounts.web.form.PaymentsForm;
import com.accounts.web.vo.BalanceSheetMainPdfVo;
import com.accounts.web.vo.ContraVo;
import com.accounts.web.vo.JournalEntriesPdfVo;
import com.accounts.web.vo.JournalHistoryPdfVo;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.LedgerVo;
import com.accounts.web.vo.PaymentVo;
import com.accounts.web.vo.ProfitAndLossMainPdfVo;
import com.accounts.web.vo.ProfitAndLossPdfVo;
import com.accounts.web.vo.ProfitAndLossVo;
import com.accounts.web.vo.ReceiptVo;
import com.accounts.web.vo.TrialBalanceLedgerGroupVo;
import com.accounts.web.vo.TrialBalancePdfVo;
import com.accounts.web.vo.BalanceSheetVo;

/**
 * 
 * @author Vinutha
 * @since 27-August-2015
 *
 */
public interface AccountsReportService {

	public List<JournalHistoryPdfVo> createJournalHistoryReport(List<JournalVo> journals);
	/**
	 * Method to create pdfvos from payments
	 * @param payments
	 * @return
	 */
	public List<JournalHistoryPdfVo> createPaymentHistoryReport(List<PaymentVo> payments);
	/**
	 * Method to create pdfvos from receipts
	 * @param receipts
	 * @return
	 */
	public List<JournalHistoryPdfVo> createReceiptHistoryReport(List<ReceiptVo> receipts);
	/**
	 * Method to create pdfvos from contra
	 * @param contras
	 * @return
	 */
	public List<JournalHistoryPdfVo> createContraHistoryReport(List<ContraVo> contras);
	/**
	 * Method to create Trial Balance Pdf 
	 * @param map
	 * @return
	 */
	public List<TrialBalancePdfVo> createTrialBalancePdf(Map<String, Object> params,Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> map , Boolean detail);
	/**
	 * Method to create profit and loss Pdf
	 * @param vo
	 * @return
	 */
	public ProfitAndLossMainPdfVo createProfitAndLossPdf(ProfitAndLossVo vo , Boolean detail);
	/**
	 * Method to create balancesheet pdf
	 * @param vo
	 * @return
	 */
	public BalanceSheetMainPdfVo createBalancesheetPdf(BalanceSheetVo vo , Boolean detail);
	/**
	 * Method to create journal entries pdfvo
	 * @param vo
	 * @return
	 */
	public List<JournalEntriesPdfVo> createJournalEntries(JournalVo vo);
	/**
	 * Method to create payment entries pdfvo
	 * @param vo
	 * @return
	 */
	public List<JournalEntriesPdfVo> createPaymentEntries(PaymentVo vo);
	/**
	 * Method to create receipt entries pdfvo
	 * @param vo
	 * @return
	 */
	public List<JournalEntriesPdfVo> createReceiptEntries(ReceiptVo vo);
	/**
	 * Method to create contra entries pdfvo
	 * @param vo
	 * @return
	 */
	public List<JournalEntriesPdfVo> createContraEntries(ContraVo vo);
}
