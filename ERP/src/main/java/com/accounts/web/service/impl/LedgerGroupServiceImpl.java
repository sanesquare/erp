package com.accounts.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accounts.web.dao.LedgerGroupDao;
import com.accounts.web.service.LedgerGroupService;
import com.accounts.web.vo.LedgerGroupVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
@Service
public class LedgerGroupServiceImpl implements LedgerGroupService {

	@Autowired
	private LedgerGroupDao dao;
	/**
	 * Method to save or update Ledger group
	 * @param groupVo
	 * @return
	 */
	public Long saveOrUpdateLedgerGroup(LedgerGroupVo groupVo){
		return dao.saveOrUpdateLedgerGroup(groupVo);
	}
	/**
	 * Method to delete ledger group
	 * @param id
	 */
	public void deleteLedgerGroup(Long id){
		dao.deleteLedgerGroup(id);
	}
	/**
	 * Method find ledger group by id
	 * @param id
	 * @return
	 */
	public LedgerGroupVo findLedgerGroupById(Long id){
		return dao.findLedgerGroupById(id);
	}
	/**
	 * Method to find ledger group by name
	 * @param name
	 * @return
	 */
	public LedgerGroupVo findLedgerGroupByName(String name , Long companyId){
		return dao.findLedgerGroupByName(name,companyId);
	}
	/**
	 * Method to list ledger group by name
	 * @return
	 */
	public List<LedgerGroupVo> findAllLedgerGroups(){
		return dao.findAllLedgerGroups();
	}
	/**
	 * Method to find ledger groups for company
	 * @param companyId
	 * @return
	 */
	public List<LedgerGroupVo> findAllLedgerGroupsForCompany(Long companyId){
		return dao.findAllLedgerGroupsForCompany(companyId);
	}
}
