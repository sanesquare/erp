package com.accounts.web.service.impl;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accounts.web.dao.ContraDao;
import com.accounts.web.service.ContraService;
import com.accounts.web.vo.ContraVo;
import com.accounts.web.vo.JournalVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
@Service
public class ContraServiceImpl implements ContraService {

	@Autowired
	private ContraDao dao;
	

	/**
	 * Method to save or update contra
	 * 
	 * @param contraVo
	 * @return
	 */
	public Long saveOrUpdateContra(ContraVo contraVo) {
		return dao.saveOrUpdateContra(contraVo);
	}

	

	/**
	 * Method to delete contra
	 * 
	 * @param id
	 */
	public void deleteContra(Long id) {
		dao.deleteContra(id);

	}

	/**
	 * Method to find contra by id
	 * 
	 * @param id
	 * @return
	 */
	public ContraVo findContraById(Long id) {
		return dao.findContraById(id);
	}



	/**
	 * method to find all contras for company
	 * 
	 * @param companyId
	 * @return
	 */
	public List<ContraVo> findAllContrasForCompany(Long companyId) {
		return dao.findAllContrasForCompany(companyId);
	}
	/**
	 * method to find all contras for company within date
	 * @param companyId
	 * @return
	 */
	public List<ContraVo> findAllContrasForCompanyWithinDate(Long companyId , String start , String end){
		return dao.findAllContrasForCompanyWithinDate(companyId, start, end);
	}
	/**
	 * Method to find all contra for by ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ContraVo> findAllContrasForLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		return dao.findAllContrasForLedgerWithinDateRange(companyId, ledgerId, startDate, endDate);
	}

	/**
	 * Method to find contras for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ContraVo> findAllContrasForToLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		return dao.findAllContrasForToLedgerWithinDateRange(companyId, ledgerId, startDate, endDate);
	}

	/**
	 * Method to find contras for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ContraVo> findAllContrasForByLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		return dao.findAllContrasForByLedgerWithinDateRange(companyId, ledgerId, startDate, endDate);
	}

	/**
	 * Method to get contra voucher number
	 * 
	 * @param companyId
	 * @return
	 */
	public String getContraNumberForCompany(Long companyId) {
		return dao.getContraNumberForCompany(companyId);

	}

}
