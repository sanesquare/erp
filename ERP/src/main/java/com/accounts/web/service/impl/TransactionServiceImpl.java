package com.accounts.web.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accounts.web.dao.TransactionDao;
import com.accounts.web.service.TransactionService;
import com.accounts.web.vo.BalanceSheetVo;
import com.accounts.web.vo.LedgerGroupViewVo;
import com.accounts.web.vo.LedgerViewVo;
import com.accounts.web.vo.LedgerVo;
import com.accounts.web.vo.ProfitAndLossVo;
import com.accounts.web.vo.TrialBalanceLedgerGroupVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 21-August-2015
 *
 */
@Service
public class TransactionServiceImpl implements TransactionService{

	@Autowired
	private TransactionDao dao;
	

	public Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> trialBalanceCalculations(Long companyId) {
		return dao.trialBalanceCalculations(companyId);
	}

	public Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> trialBalanceCalculations(Long companyId, String start, String end) {
		return dao.trialBalanceCalculations(companyId, start, end);
	}

	public ProfitAndLossVo profitAndLossentries(Long companyId, String start, String end) {
		return dao.profitAndLossentries(companyId, start, end);
	}

	public ProfitAndLossVo profitAndLossentries(Long companyId) {
		return dao.profitAndLossentries(companyId);
	}

	public BalanceSheetVo balanceSheetCalculation(Long companyId) {
		return dao.balanceSheetCalculation(companyId);
	}

	public BalanceSheetVo balanceSheetCalculation(Long companyId, String start, String end) {
		return dao.balanceSheetCalculation(companyId, start, end);
	}

	public LedgerViewVo findTRansactionsForLedger(String startDate, String endDate, Long ledgerId, Long companyId) {
		return dao.findTRansactionsForLedger(startDate, endDate,null, ledgerId, companyId);
	}

	public LedgerGroupViewVo findTRansactionsForLedgerGroup(String startDate, String endDate, Long groupId,
			Long companyId) {
		return dao.findTRansactionsForLedgerGroup(startDate, endDate, groupId, companyId);
	}

}
