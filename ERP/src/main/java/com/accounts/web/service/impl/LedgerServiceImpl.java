package com.accounts.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accounts.web.dao.LedgerDao;
import com.accounts.web.service.LedgerService;
import com.accounts.web.vo.LedgerVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 13-August-2015
 */
@Service
public class LedgerServiceImpl implements LedgerService {

	@Autowired
	private LedgerDao dao;
	/**
	 * Method to save or update ledger
	 * 
	 * @param ledgerVo
	 * @return
	 */
	public Long saveOrUpdateLedger(LedgerVo ledgerVo){
		return dao.saveOrUpdateLedger(ledgerVo);
	}

	/**
	 * Method to find ledger By Id
	 * @param id
	 * @return
	 */
	public LedgerVo findLedgerById(Long id){
		return dao.findLedgerById(id);
	}
	/**
	 * Method to delete ledger
	 * 
	 * @param id
	 */
	public void deleteLedger(Long id){
		 dao.deleteLedger(id);
	}

	/**
	 * Method to list all ledgers
	 * 
	 * @return
	 */
	public List<LedgerVo> findAllLedgers(){
		return dao.findAllLedgers();
	}

	/**
	 * List ledgers for company
	 *  
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> findLedgersForCompany(Long companyId){
		return dao.findLedgersForCompany(companyId);
	}

	public List<LedgerVo> findAccountsForSalary(Long companyId) {
		return dao.findAccountsForSalary(companyId);
	}

	public List<LedgerVo> contraLedgers(Long companyId) {
		return dao.contraLedgers(companyId);
	}

	public List<LedgerVo> journalLedgers(Long companyId) {
		return dao.journalLedgers(companyId);
	}

	public List<LedgerVo> paymentLedgers(Long companyId) {
		return dao.paymentLedgers(companyId);
	}

	public List<LedgerVo> receiptLedgers(Long companyId) {
		return dao.receiptLedgers(companyId);
	}
}
