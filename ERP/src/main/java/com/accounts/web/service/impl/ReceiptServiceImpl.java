package com.accounts.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accounts.web.dao.ReceiptDao;
import com.accounts.web.entities.Receipts;
import com.accounts.web.service.ReceiptService;
import com.accounts.web.vo.ReceiptVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
@Service
public class ReceiptServiceImpl implements ReceiptService {

	@Autowired
	private ReceiptDao dao;
	/**
	 * Method to save or update receipt
	 * @param receiptVo
	 * @return
	 */
	public Long saveOrUpdateReceipt(ReceiptVo receiptVo){
		return dao.saveOrUpdateReceipt(receiptVo);
	}
	/**
	 * Method to delete receipt
	 * @param id
	 */
	public void deleteReceipt(Long id){
		dao.deleteReceipt(id);
	}
	/**
	 * Method to find receipt by id
	 * @param id
	 * @return
	 */
	public ReceiptVo findReceiptById(Long id){
		return dao.findReceiptById(id);	
	}
	/**
	 * method to find all receipts for company
	 * @param companyId
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForCompany(Long companyId){
		return dao.findAllReceiptsForCompany(companyId);	
	}
	/**
	 * method to find all journals for company within date
	 * @param companyId
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForCompanyWithinDate(Long companyId , String start , String end){
		return dao.findAllReceiptsForCompanyWithinDate(companyId, start, end);
	}
	/**
	 * Method to find all receipt for by ledger within date range
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForLedgerWithinDateRange(Long companyId,Long ledgerId , String startDate , String endDate){
		return dao.findAllReceiptsForLedgerWithinDateRange(companyId, ledgerId, startDate, endDate);
	}
	/**
	 * Method to find receipts for to ledger within date range 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForToLedgerWithinDateRange(Long companyId,Long ledgerId , String startDate , String endDate){
		return dao.findAllReceiptsForToLedgerWithinDateRange(companyId, ledgerId, startDate, endDate);	
	}
	/**
	 * Method to find receipts for to ledger within date range 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForByLedgerWithinDateRange(Long companyId,Long ledgerId , String startDate , String endDate){
		return dao.findAllReceiptsForByLedgerWithinDateRange(companyId, ledgerId, startDate, endDate);	
	}
	/**
	 * Method to get receipt voucher number
	 * @param companyId
	 * @return
	 */
	public String getReceiptNumberForCompany(Long companyId){
		return dao.getReceiptNumberForCompany(companyId);	
	}





}
