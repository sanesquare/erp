package com.accounts.web.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accounts.web.dao.JournalDao;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.entities.Journal;
import com.accounts.web.service.JournalService;
import com.accounts.web.vo.JournalVo;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer &  Vinutha
 * @since 15-August-2015
 *
 */
@Service
public class JournalServiceImpl  implements JournalService{

	@Autowired
	private JournalDao dao;
	/**
	 * Method to save or update journal
	 * 
	 * @param journalVo
	 * @return
	 */
	public Long saveOrUpdateJournal(JournalVo journalVo) {
		return dao.saveOrUpdateJournal(journalVo);
	}

	/**
	 * Method to delete journal
	 * 
	 * @param id
	 */
	public void deleteJournal(Long id) {
		dao.deleteJournal(id);

	}

	/**
	 * Method to find journal by id
	 * 
	 * @param id
	 * @return
	 */
	public JournalVo findJournalById(Long id) {
		return dao.findJournalById(id);
	}


	/**
	 * method to find all journals for company
	 * 
	 * @param companyId
	 * @return
	 */
	public List<JournalVo> findAllJournalsForCompany(Long companyId) {
		return dao.findAllJournalsForCompany(companyId);
	}

	/**
	 * Method to find all journal for by ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<JournalVo> findAllJournalsForLedgerWithinDateRange(Long companyId,Long ledgerId, String startDate, String endDate) {
		return dao.findAllJournalsForLedgerWithinDateRange(companyId,ledgerId, startDate, endDate);
	}

	/**
	 * Method to find journals for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	/*public List<JournalVo> findAllJournalsForToLedgerWithinDateRange(Long ledgerId, String startDate, String endDate) {
		return dao.findAllJournalsForToLedgerWithinDateRange(ledgerId, startDate, endDate);
	}
*/

	/**
	 * Method to get journal voucher number
	 * @param companyId
	 * @return
	 */
	public String getJournalNumberForCompany(Long companyId){
		return dao.getJournalNumberForCompany(companyId);
	}

	public List<JournalVo> findAllJournalsForCompanyWithinDate(Long companyId, String start, String end) {
		return dao.findAllJournalsForCompanyWithinDate(companyId, start, end);
	}

}
