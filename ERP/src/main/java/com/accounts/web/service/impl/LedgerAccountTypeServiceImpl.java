package com.accounts.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accounts.web.dao.LedgerAccountTypeDao;
import com.accounts.web.service.LedgerAccountTypeService;
import com.accounts.web.vo.LedgerAccountTypeVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
@Service
public class LedgerAccountTypeServiceImpl implements LedgerAccountTypeService {

	@Autowired
	private LedgerAccountTypeDao dao;

	/**
	 * Method to find account type by name
	 * 
	 * @param type
	 * @return
	 */
	public LedgerAccountTypeVo findAccountTypeByName(String type) {
		return dao.findAccountTypeByName(type);
	}

	/**
	 * Method to list all Account types
	 * 
	 * @return
	 */
	public List<LedgerAccountTypeVo> findAllAccountTypes() {
		return dao.findAllAccountTypes();
	}

}
