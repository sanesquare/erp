package com.accounts.web.service;

import java.util.List;

import com.accounts.web.entities.Payment;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.PaymentVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
public interface PaymentService {

	/**
	 * Method to save or update payment
	 * 
	 * @param paymentVo
	 * @return
	 */
	public Long saveOrUpdatePayment(PaymentVo paymentVo);

	/**
	 * Method to delete payment
	 * 
	 * @param id
	 */
	public void deletePayment(Long id);

	/**
	 * Method to find payment by id
	 * 
	 * @param id
	 * @return
	 */
	public PaymentVo findPaymentById(Long id);

	/**
	 * method to find all payments for company
	 * 
	 * @param companyId
	 * @return
	 */
	public List<PaymentVo> findAllPaymentsForCompany(Long companyId);
	/**
	 * method to find all payments for company within date
	 * @param companyId
	 * @return
	 */
	public List<PaymentVo> findAllPaymentsForCompanyWithinDate(Long companyId , String start , String end);

	/**
	 * Method to find all payment for by ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PaymentVo> findAllPaymentsForLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate);

	/**
	 * Method to find payments for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PaymentVo> findAllPaymentsForToLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate);

	/**
	 * Method to find payments for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PaymentVo> findAllPaymentsForByLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate);

	/**
	 * Method to get payment voucher number
	 * 
	 * @param companyId
	 * @return
	 */
	public String getPaymentNumberForCompany(Long companyId);

}
