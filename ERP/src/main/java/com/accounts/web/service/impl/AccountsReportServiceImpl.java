package com.accounts.web.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accounts.web.dao.LedgerDao;
import com.accounts.web.service.AccountsReportService;
import com.accounts.web.vo.BalanceSheetMainPdfVo;
import com.accounts.web.vo.BalanceSheetPdfVo;
import com.accounts.web.vo.BalanceSheetVo;
import com.accounts.web.vo.ContraItemsVo;
import com.accounts.web.vo.ContraVo;
import com.accounts.web.vo.JournalEntriesPdfVo;
import com.accounts.web.vo.JournalHistoryPdfVo;
import com.accounts.web.vo.JournalItemsVo;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.LedgerVo;
import com.accounts.web.vo.PaymentItemsVo;
import com.accounts.web.vo.PaymentVo;
import com.accounts.web.vo.ProfitAndLossGroupVo;
import com.accounts.web.vo.ProfitAndLossLedgerVo;
import com.accounts.web.vo.ProfitAndLossMainPdfVo;
import com.accounts.web.vo.ProfitAndLossPdfVo;
import com.accounts.web.vo.ProfitAndLossVo;
import com.accounts.web.vo.ReceiptItemsVo;
import com.accounts.web.vo.ReceiptVo;
import com.accounts.web.vo.TrialBalanceLedgerGroupVo;
import com.accounts.web.vo.TrialBalancePdfVo;
import com.accounts.web.vo.BalanceSheetGroupVo;
import com.accounts.web.vo.BalanceSheetLedgerVo;

/**
 * 
 * @author Vinutha
 * @since 27-August-2015
 *
 */
@Service
public class AccountsReportServiceImpl implements AccountsReportService {

	@Autowired
	private LedgerDao ledgerDao;

	public List<JournalHistoryPdfVo> createPaymentHistoryReport(List<PaymentVo> payments) {
		List<JournalHistoryPdfVo> pdfVos = new ArrayList<JournalHistoryPdfVo>();
		int count = 0;
		for (PaymentVo vo : payments) {
			JournalHistoryPdfVo pdfVo = new JournalHistoryPdfVo();
			pdfVo.setSrNo(++count);
			pdfVo.setVoucherNo(vo.getVoucherNumber());
			pdfVo.setDate(vo.getDate());
			pdfVo.setLedgerBy(vo.getFromLedger());
			pdfVo.setLedgerTo(vo.getToLedger());
			pdfVo.setAmount(vo.getAmount());
			pdfVo.setNarration(vo.getNarration());
			pdfVos.add(pdfVo);
		}
		return pdfVos;
	}

	public List<JournalHistoryPdfVo> createReceiptHistoryReport(List<ReceiptVo> receipts) {
		List<JournalHistoryPdfVo> pdfVos = new ArrayList<JournalHistoryPdfVo>();
		int count = 0;
		for (ReceiptVo vo : receipts) {
			JournalHistoryPdfVo pdfVo = new JournalHistoryPdfVo();
			pdfVo.setSrNo(++count);
			pdfVo.setVoucherNo(vo.getVoucherNumber());
			pdfVo.setDate(vo.getDate());
			pdfVo.setLedgerBy(vo.getFromLedger());
			pdfVo.setLedgerTo(vo.getToLedger());
			pdfVo.setAmount(vo.getAmount());
			pdfVo.setNarration(vo.getNarration());
			pdfVos.add(pdfVo);
		}
		return pdfVos;
	}

	public List<JournalHistoryPdfVo> createContraHistoryReport(List<ContraVo> contras) {
		List<JournalHistoryPdfVo> pdfVos = new ArrayList<JournalHistoryPdfVo>();
		int count = 0;
		for (ContraVo vo : contras) {
			JournalHistoryPdfVo pdfVo = new JournalHistoryPdfVo();
			pdfVo.setSrNo(++count);
			pdfVo.setVoucherNo(vo.getVoucherNumber());
			pdfVo.setDate(vo.getDate());
			pdfVo.setLedgerBy(vo.getFromLedger());
			pdfVo.setLedgerTo(vo.getToLedger());
			pdfVo.setAmount(vo.getAmount());
			pdfVo.setNarration(vo.getNarration());
			pdfVos.add(pdfVo);
		}
		return pdfVos;
	}

	public List<TrialBalancePdfVo> createTrialBalancePdf(Map<String, Object> params,Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> map, Boolean detail) {

		List<TrialBalancePdfVo> pdfVos = new ArrayList<TrialBalancePdfVo>();
		for (Map.Entry<TrialBalanceLedgerGroupVo, List<LedgerVo>> entry : map.entrySet()) {
			
			TrialBalanceLedgerGroupVo group = entry.getKey();
			if (detail == true) {
				if (!group.getGroup().equalsIgnoreCase("grand_total!@#") && !group.getGroup().equalsIgnoreCase("closing stock")) {
					TrialBalancePdfVo groupVo = new TrialBalancePdfVo();
					groupVo.setLedgerGroup(group.getGroup());
					groupVo.setOpeningBal(group.getOpening());
					groupVo.setDebit(group.getDebit());
					groupVo.setCredit(group.getCredit());
					groupVo.setClosingBal(group.getClosing());
					pdfVos.add(groupVo);
					for (LedgerVo ledgerVo : entry.getValue()) {
						TrialBalancePdfVo vo = new TrialBalancePdfVo();
						vo.setLedgerGroup(group.getGroup());
						vo.setParticulars(ledgerVo.getLedgerName());
						vo.setOpeningBal(ledgerVo.getOpeningBalance());
						vo.setDebit(ledgerVo.getDebit());
						vo.setCredit(ledgerVo.getCredit());
						vo.setClosingBal(ledgerVo.getBalance());
						pdfVos.add(vo);
					}
				}
			} else {
				if (!group.getGroup().equalsIgnoreCase("grand_total!@#") && !group.getGroup().equalsIgnoreCase("closing stock")) {
					TrialBalancePdfVo vo = new TrialBalancePdfVo();
					vo.setLedgerGroup(group.getGroup());
					vo.setOpeningBal(group.getOpening());
					vo.setDebit(group.getDebit());
					vo.setCredit(group.getCredit());
					vo.setClosingBal(group.getClosing());
					pdfVos.add(vo);
				}
				
			}
			if (group.getGroup().equalsIgnoreCase("closing stock")) {
				if(group.getDebit().compareTo(BigDecimal.ZERO)!=0)
					params.put("CLOSING_BALANCE", group.getDebit());
				else
					params.put("CLOSING_BALANCE", group.getCredit());
			}
		}
		for (Map.Entry<TrialBalanceLedgerGroupVo, List<LedgerVo>> entry : map.entrySet()) {
			TrialBalanceLedgerGroupVo group = entry.getKey();
			if (detail == true) {
				if (group.getGroup().equalsIgnoreCase("grand_total!@#")) {
					for (LedgerVo ledgerVo : entry.getValue()) {
						TrialBalancePdfVo vo = new TrialBalancePdfVo();
						vo.setParticulars("Grand Total");
						vo.setOpeningBal(ledgerVo.getTotalOpening());
						vo.setDebit(ledgerVo.getTotalDebit());
						vo.setCredit(ledgerVo.getTotalCredit());
						vo.setClosingBal(ledgerVo.getTotalClose());
						pdfVos.add(vo);
					}
				}
			} else {
				if (entry.getKey().getGroup().equalsIgnoreCase("grand_total!@#")) {
					/*TrialBalancePdfVo vo = new TrialBalancePdfVo();
					vo.setParticulars("Grand Total");
					pdfVos.add(vo);*/
					
					for (LedgerVo ledgerVo : entry.getValue()) {
						TrialBalancePdfVo vo = new TrialBalancePdfVo();
						vo.setParticulars("Grand Total");
						vo.setOpeningBal(ledgerVo.getTotalOpening());
						vo.setDebit(ledgerVo.getTotalDebit());
						vo.setCredit(ledgerVo.getTotalCredit());
						vo.setClosingBal(ledgerVo.getTotalClose());
						pdfVos.add(vo);
					}
				}
			}
		}

		return pdfVos;
	}

	public ProfitAndLossMainPdfVo createProfitAndLossPdf(ProfitAndLossVo vo, Boolean detail) {
		ProfitAndLossMainPdfVo mainPdfVo = new ProfitAndLossMainPdfVo();
		List<ProfitAndLossPdfVo> directExpenses = new ArrayList<ProfitAndLossPdfVo>();
		List<ProfitAndLossPdfVo> directIncomes = new ArrayList<ProfitAndLossPdfVo>();
		List<ProfitAndLossPdfVo> indirectExpenses = new ArrayList<ProfitAndLossPdfVo>();
		List<ProfitAndLossPdfVo> indirectIncomes = new ArrayList<ProfitAndLossPdfVo>();

		for (Map.Entry<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> directExpense : vo.getDirectExpenses()
				.entrySet()) {
			if (detail == true) {
				for (ProfitAndLossLedgerVo ledgerVo : directExpense.getValue()) {
					ProfitAndLossPdfVo pdfVo = new ProfitAndLossPdfVo();
					pdfVo.setLedgerGroup(directExpense.getKey().getGroupName());
					pdfVo.setGroupAmount(directExpense.getKey().getAmount());
					pdfVo.setExpense(ledgerVo.getLedgerName());
					pdfVo.setExpAmount(ledgerVo.getAmount());
					directExpenses.add(pdfVo);
				}
			} else {
				ProfitAndLossPdfVo pdfVo = new ProfitAndLossPdfVo();
				pdfVo.setLedgerGroup(directExpense.getKey().getGroupName());
				pdfVo.setGroupAmount(directExpense.getKey().getAmount());
				directExpenses.add(pdfVo);
			}
		}
		for (Map.Entry<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> directIncome : vo.getDirectIncomes()
				.entrySet()) {
			if (detail == true) {
				for (ProfitAndLossLedgerVo ledgerVo : directIncome.getValue()) {
					ProfitAndLossPdfVo pdfVo = new ProfitAndLossPdfVo();
					pdfVo.setLedgerGroup(directIncome.getKey().getGroupName());
					pdfVo.setGroupAmount(directIncome.getKey().getAmount());
					pdfVo.setIncome(ledgerVo.getLedgerName());
					pdfVo.setIncAmount(ledgerVo.getAmount());
					directIncomes.add(pdfVo);
				}
			} else {
				ProfitAndLossPdfVo pdfVo = new ProfitAndLossPdfVo();
				pdfVo.setLedgerGroup(directIncome.getKey().getGroupName());
				pdfVo.setGroupAmount(directIncome.getKey().getAmount());
				directIncomes.add(pdfVo);
			}
		}
		for (Map.Entry<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> indirectExpense : vo.getIndirectExpenses()
				.entrySet()) {
			if (indirectExpense.getKey().getFirst() == true) {
				ProfitAndLossPdfVo pdfVo = new ProfitAndLossPdfVo();
				pdfVo.setLedgerGroup(indirectExpense.getKey().getGroupName());
				pdfVo.setGroupAmount(indirectExpense.getKey().getAmount());
				indirectExpenses.add(0, pdfVo);
			} else {
				if (detail == true) {
					for (ProfitAndLossLedgerVo ledgerVo : indirectExpense.getValue()) {
						ProfitAndLossPdfVo pdfVo = new ProfitAndLossPdfVo();
						pdfVo.setLedgerGroup(indirectExpense.getKey().getGroupName());
						pdfVo.setGroupAmount(indirectExpense.getKey().getAmount());
						pdfVo.setIndirectExpense(ledgerVo.getLedgerName());
						pdfVo.setIndirectExpAmount(ledgerVo.getAmount());
						indirectExpenses.add(pdfVo);
					}
				} else {
					ProfitAndLossPdfVo pdfVo = new ProfitAndLossPdfVo();
					pdfVo.setLedgerGroup(indirectExpense.getKey().getGroupName());
					pdfVo.setGroupAmount(indirectExpense.getKey().getAmount());
					indirectExpenses.add(pdfVo);
				}
			}
		}
		for (Map.Entry<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> indirectIncome : vo.getIndirectIncomes()
				.entrySet()) {
			if (indirectIncome.getKey().getFirst() == true) {
				ProfitAndLossPdfVo pdfVo = new ProfitAndLossPdfVo();
				pdfVo.setLedgerGroup(indirectIncome.getKey().getGroupName());
				pdfVo.setGroupAmount(indirectIncome.getKey().getAmount());
				indirectIncomes.add(0, pdfVo);
			} else {
				if (detail == true) {
					for (ProfitAndLossLedgerVo ledgerVo : indirectIncome.getValue()) {
						ProfitAndLossPdfVo pdfVo = new ProfitAndLossPdfVo();
						pdfVo.setLedgerGroup(indirectIncome.getKey().getGroupName());
						pdfVo.setGroupAmount(indirectIncome.getKey().getAmount());
						pdfVo.setIndirectIncome(ledgerVo.getLedgerName());
						pdfVo.setIndirectIncAmount(ledgerVo.getAmount());
						indirectIncomes.add(pdfVo);
					}
				} else {
					ProfitAndLossPdfVo pdfVo = new ProfitAndLossPdfVo();
					pdfVo.setLedgerGroup(indirectIncome.getKey().getGroupName());
					pdfVo.setGroupAmount(indirectIncome.getKey().getAmount());
					indirectIncomes.add(pdfVo);
				}
			}
		}
		mainPdfVo.setDirectExpenses(directExpenses);
		mainPdfVo.setDirectIncomes(directIncomes);
		mainPdfVo.setIndirectExpenses(indirectExpenses);
		mainPdfVo.setIndirectIncomes(indirectIncomes);
		return mainPdfVo;
	}

	public BalanceSheetMainPdfVo createBalancesheetPdf(BalanceSheetVo vo, Boolean detail) {
		BalanceSheetMainPdfVo mainVo = new BalanceSheetMainPdfVo();
		List<BalanceSheetPdfVo> equityList = new ArrayList<BalanceSheetPdfVo>();
		List<BalanceSheetPdfVo> currentLiabilityList = new ArrayList<BalanceSheetPdfVo>();
		List<BalanceSheetPdfVo> nonCurrentLiabilityList = new ArrayList<BalanceSheetPdfVo>();
		List<BalanceSheetPdfVo> currentAssetList = new ArrayList<BalanceSheetPdfVo>();
		List<BalanceSheetPdfVo> nonCurrentAssetList = new ArrayList<BalanceSheetPdfVo>();

		for (Map.Entry<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> entry : vo.getCurrentLiabilities().entrySet()) {

			if (entry.getKey().getFirst() == true) {
				BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
				pdfVo.setGroup(entry.getKey().getGroupName());
				pdfVo.setGroupAmount(entry.getKey().getAmount());
				currentLiabilityList.add(0, pdfVo);
			} else {
				if (detail == true) {
					for (BalanceSheetLedgerVo ledger : entry.getValue()) {
						BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
						pdfVo.setAmount(ledger.getAmount());
						pdfVo.setGroup(entry.getKey().getGroupName());
						pdfVo.setGroupAmount(entry.getKey().getAmount());
						pdfVo.setLedger(ledger.getLedgerName());
						currentLiabilityList.add(pdfVo);
					}
				} else {
					BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
					pdfVo.setGroup(entry.getKey().getGroupName());
					pdfVo.setGroupAmount(entry.getKey().getAmount());
					currentLiabilityList.add(pdfVo);
				}
			}
		}
		for (Map.Entry<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> entry : vo.getEquity().entrySet()) {
			if (detail == true) {
				for (BalanceSheetLedgerVo ledger : entry.getValue()) {
					BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
					pdfVo.setAmount(ledger.getAmount());
					pdfVo.setGroup(entry.getKey().getGroupName());
					pdfVo.setGroupAmount(entry.getKey().getAmount());
					pdfVo.setLedger(ledger.getLedgerName());
					equityList.add(pdfVo);
				}
			} else {
				BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
				pdfVo.setGroup(entry.getKey().getGroupName());
				pdfVo.setGroupAmount(entry.getKey().getAmount());
				equityList.add(pdfVo);
			}
		}
		for (Map.Entry<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> entry : vo.getNonCurrentLiabilities()
				.entrySet()) {
			if (detail == true) {
				for (BalanceSheetLedgerVo ledger : entry.getValue()) {
					BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
					pdfVo.setAmount(ledger.getAmount());
					pdfVo.setGroup(entry.getKey().getGroupName());
					pdfVo.setGroupAmount(entry.getKey().getAmount());
					pdfVo.setLedger(ledger.getLedgerName());
					nonCurrentLiabilityList.add(pdfVo);
				}
			} else {
				BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
				pdfVo.setGroup(entry.getKey().getGroupName());
				pdfVo.setGroupAmount(entry.getKey().getAmount());
				nonCurrentLiabilityList.add(pdfVo);
			}
		}
		for (Map.Entry<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> entry : vo.getCurrentAssets().entrySet()) {
			if (detail == true) {
				for (BalanceSheetLedgerVo ledger : entry.getValue()) {
					BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
					pdfVo.setAmount(ledger.getAmount());
					pdfVo.setGroup(entry.getKey().getGroupName());
					pdfVo.setGroupAmount(entry.getKey().getAmount());
					pdfVo.setLedger(ledger.getLedgerName());
					currentAssetList.add(pdfVo);
				}
			} else {
				BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
				pdfVo.setGroup(entry.getKey().getGroupName());
				pdfVo.setGroupAmount(entry.getKey().getAmount());
				currentAssetList.add(pdfVo);
			}
		}
		for (Map.Entry<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> entry : vo.getNonCurrentAssets().entrySet()) {
			if (detail == true) {
				for (BalanceSheetLedgerVo ledger : entry.getValue()) {
					BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
					pdfVo.setAmount(ledger.getAmount());
					pdfVo.setGroup(entry.getKey().getGroupName());
					pdfVo.setGroupAmount(entry.getKey().getAmount());
					pdfVo.setLedger(ledger.getLedgerName());
					nonCurrentAssetList.add(pdfVo);
				}
			}else{
				BalanceSheetPdfVo pdfVo = new BalanceSheetPdfVo();
				pdfVo.setGroup(entry.getKey().getGroupName());
				pdfVo.setGroupAmount(entry.getKey().getAmount());
				nonCurrentAssetList.add(pdfVo);
			}
		}

		mainVo.setAssetTotal(vo.getTotalAsset());
		mainVo.setLiabilityTotal(vo.getTotalLiability());

		mainVo.setCurrentLiabilityList(currentLiabilityList);
		// current liability and equity in one list
		mainVo.getCurrentLiabilityList().addAll(mainVo.getCurrentLiabilityList().size(), equityList);
		// mainVo.setEquityList(equityList);
		mainVo.setNonCurrentLiabilityList(nonCurrentLiabilityList);
		mainVo.setCurrentAssetList(currentAssetList);
		mainVo.setNonCurrentAssetList(nonCurrentAssetList);
		return mainVo;
	}

	public List<JournalHistoryPdfVo> createJournalHistoryReport(List<JournalVo> journals) {
		List<JournalHistoryPdfVo> pdfVos = new ArrayList<JournalHistoryPdfVo>();
		int count = 0;
		for (JournalVo vo : journals) {
			JournalHistoryPdfVo pdfVo = new JournalHistoryPdfVo();
			pdfVo.setSrNo(++count);
			pdfVo.setVoucherNo(vo.getVoucherNumber());
			pdfVo.setDate(vo.getDate());
			pdfVo.setLedgerBy(vo.getFromLedger());
			pdfVo.setLedgerTo(vo.getToLedger());
			pdfVo.setAmount(vo.getAmount());
			pdfVo.setNarration(vo.getNarration());
			pdfVos.add(pdfVo);
		}
		return pdfVos;
	}

	public List<JournalEntriesPdfVo> createJournalEntries(JournalVo vo) {
		List<JournalEntriesPdfVo> pdfVos = new ArrayList<JournalEntriesPdfVo>();
		int count = 0;
		for (JournalItemsVo itemsVo : vo.getEntries()) {
			JournalEntriesPdfVo pdfVo = new JournalEntriesPdfVo();
			pdfVo.setSrNo(++count);
			pdfVo.setLedger(ledgerDao.findLedgerById(itemsVo.getAccTypeId()).getLedgerName());
			pdfVo.setDebitAmnt(itemsVo.getDebitAmnt());
			pdfVo.setCreditAmnt(itemsVo.getCreditAmnt());
			pdfVos.add(pdfVo);
		}
		return pdfVos;
	}

	public List<JournalEntriesPdfVo> createPaymentEntries(PaymentVo vo) {
		List<JournalEntriesPdfVo> pdfVos = new ArrayList<JournalEntriesPdfVo>();
		int count = 0;
		for (PaymentItemsVo itemsVo : vo.getEntries()) {
			JournalEntriesPdfVo pdfVo = new JournalEntriesPdfVo();
			pdfVo.setSrNo(++count);
			pdfVo.setLedger(ledgerDao.findLedgerById(itemsVo.getAccTypeId()).getLedgerName());
			pdfVo.setDebitAmnt(itemsVo.getDebitAmnt());
			pdfVo.setCreditAmnt(itemsVo.getCreditAmnt());
			pdfVos.add(pdfVo);
		}
		return pdfVos;
	}

	public List<JournalEntriesPdfVo> createReceiptEntries(ReceiptVo vo) {
		List<JournalEntriesPdfVo> pdfVos = new ArrayList<JournalEntriesPdfVo>();
		int count = 0;
		for (ReceiptItemsVo itemsVo : vo.getEntries()) {
			JournalEntriesPdfVo pdfVo = new JournalEntriesPdfVo();
			pdfVo.setSrNo(++count);
			pdfVo.setLedger(ledgerDao.findLedgerById(itemsVo.getAccTypeId()).getLedgerName());
			pdfVo.setDebitAmnt(itemsVo.getDebitAmnt());
			pdfVo.setCreditAmnt(itemsVo.getCreditAmnt());
			pdfVos.add(pdfVo);
		}
		return pdfVos;
	}

	public List<JournalEntriesPdfVo> createContraEntries(ContraVo vo) {
		List<JournalEntriesPdfVo> pdfVos = new ArrayList<JournalEntriesPdfVo>();
		int count = 0;
		for (ContraItemsVo itemsVo : vo.getEntries()) {
			JournalEntriesPdfVo pdfVo = new JournalEntriesPdfVo();
			pdfVo.setSrNo(++count);
			pdfVo.setLedger(ledgerDao.findLedgerById(itemsVo.getAccTypeId()).getLedgerName());
			pdfVo.setDebitAmnt(itemsVo.getDebitAmnt());
			pdfVo.setCreditAmnt(itemsVo.getCreditAmnt());
			pdfVos.add(pdfVo);
		}
		return pdfVos;
	}
}
