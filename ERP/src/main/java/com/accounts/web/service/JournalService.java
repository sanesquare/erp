package com.accounts.web.service;

import java.util.List;

import com.accounts.web.entities.Journal;
import com.accounts.web.vo.JournalVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 15-August-2015
 *
 */
public interface JournalService {


	/**
	 * Method to save or update journal
	 * @param journalVo
	 * @return
	 */
	public Long saveOrUpdateJournal(JournalVo journalVo);
	/**
	 * Method to delete journal
	 * @param id
	 */
	public void deleteJournal(Long id);
	/**
	 * Method to find journal by id
	 * @param id
	 * @return
	 */
	public JournalVo findJournalById(Long id);
	/**
	 * method to find all journals for company
	 * @param companyId
	 * @return
	 */
	public List<JournalVo> findAllJournalsForCompany(Long companyId);
	/**
	 * method to find all journals for company within date
	 * @param companyId
	 * @return
	 */
	public List<JournalVo> findAllJournalsForCompanyWithinDate(Long companyId , String start , String end);
	/**
	 * Method to find all journal for by ledger within date range
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<JournalVo> findAllJournalsForLedgerWithinDateRange(Long companyId,Long ledgerId , String startDate , String endDate);
	/**
	 * Method to find journals for to ledger within date range 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	//public List<JournalVo> findAllJournalsForToLedgerWithinDateRange(Long ledgerId , String startDate , String endDate);
	
	/**
	 * Method to get journal voucher number
	 * @param companyId
	 * @return
	 */
	public String getJournalNumberForCompany(Long companyId);

}
