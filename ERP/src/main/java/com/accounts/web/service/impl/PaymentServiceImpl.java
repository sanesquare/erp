package com.accounts.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accounts.web.dao.PaymentDao;
import com.accounts.web.service.PaymentService;
import com.accounts.web.vo.PaymentVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
@Service
public class PaymentServiceImpl implements PaymentService{

	@Autowired
	private PaymentDao dao;
	/**
	 * Method to save or update payment
	 * 
	 * @param paymentVo
	 * @return
	 */
	public Long saveOrUpdatePayment(PaymentVo paymentVo) {
		return dao.saveOrUpdatePayment(paymentVo);
	}

	/**
	 * Method to delete payment
	 * 
	 * @param id
	 */
	public void deletePayment(Long id) {
		dao.deletePayment(id);

	}

	/**
	 * Method to find payment by id
	 * 
	 * @param id
	 * @return
	 */
	public PaymentVo findPaymentById(Long id) {
		return dao.findPaymentById(id);
	}

	/**
	 * method to find all payments for company
	 * 
	 * @param companyId
	 * @return
	 */
	public List<PaymentVo> findAllPaymentsForCompany(Long companyId) {
		return dao.findAllPaymentsForCompany(companyId);
	}

	/**
	 * Method to find all payment for by ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PaymentVo> findAllPaymentsForLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		return dao.findAllPaymentsForLedgerWithinDateRange(companyId, ledgerId, startDate, endDate);
	}

	/**
	 * Method to find payments for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PaymentVo> findAllPaymentsForToLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		return dao.findAllPaymentsForToLedgerWithinDateRange(companyId, ledgerId, startDate, endDate);
	}

	/**
	 * Method to find payments for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PaymentVo> findAllPaymentsForByLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		return dao.findAllPaymentsForByLedgerWithinDateRange(companyId, ledgerId, startDate, endDate);
	}

	/**
	 * Method to get payment voucher number
	 * 
	 * @param companyId
	 * @return
	 */
	public String getPaymentNumberForCompany(Long companyId) {
		return dao.getPaymentNumberForCompany(companyId);
	}
	/**
	 * method to find all payments for company within date
	 * @param companyId
	 * @return
	 */
	public List<PaymentVo> findAllPaymentsForCompanyWithinDate(Long companyId, String start, String end) {
		return dao.findAllPaymentsForCompanyWithinDate(companyId, start, end);
	}

}
