package com.accounts.web.utils;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import com.accounts.web.form.JournalHistoryPdfForm;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 17-August-2015
 *
 */
public class JournalPdfUtil {

	public static void createJournalHistoryPdf(JournalHistoryPdfForm form , String url) {
		try {
			JasperPrint jasperPrint;
			JasperReport jasperReport;
			//String applicationPath = new File("E:/Vinu/WEB/ERP/ERP/src/main/webapp/WEB-INF/jrxml/Accounts").getPath();
			//System.out.println("Path = " + applicationPath);
			//InputStream layoutFile = JournalPdfUtil.class.getClassLoader().getResourceAsStream(url+"/journalListFormat.jrxml");
			//jasperReport = JasperCompileManager.compileReport(layoutFile);
			JasperCompileManager.compileReportToFile(url+"/journalListFormat.jrxml");
			HashMap<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("type", "Journal History");
			JRDataSource dataSource = new JRBeanCollectionDataSource(form.getPdfVos());
			jasperPrint = JasperFillManager.fillReport(url+"/journalListFormat.jasper", parameters, dataSource);
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

}
