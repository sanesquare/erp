package com.accounts.web.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.ContraService;
import com.accounts.web.service.LedgerService;
import com.accounts.web.vo.ContraVo;
import com.accounts.web.vo.ContraVo;
import com.accounts.web.vo.PaymentVo;
import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate","companies","companyName" })
public class ContraController {

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private ContraService contraService;

	@Autowired
	private CompanyService companyService;

	private static int rows = 2;

	@Log
	private static Logger logger;

	/**
	 * method to reach contra home page
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.CONTRA_HOME, method = RequestMethod.GET)
	public String contraHome(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @ModelAttribute("currencyName") String currency,
			@ModelAttribute("companies") List<CompanyVo> companies, @ModelAttribute("companyName") String companyName,
			@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate) {
		model.addAttribute("currencyName", currency);
		model.addAttribute("companyStart", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			model.addAttribute(MessageConstants.SUCCESS, request.getParameter(MessageConstants.SUCCESS));
			try {
				model.addAttribute("title", companyName + " ( " + startDate + " to " + endDate + " )");
				model.addAttribute("contras", contraService.findAllContrasForCompany(companyId));
				model.addAttribute("companies", companies);
			} catch (Exception e) {
				logger.info("CONTRA_HOME: ", e);
				model.addAttribute(MessageConstants.SUCCESS, e.getMessage());
			}
			return AccountsPageNameConstants.CONTRA_HOME;
		}
	}

	/**
	 * method to reach add contra page
	 * 
	 * @param model
	 * @param companyId
	 * @param paymentVo
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.CONTRA_ADD, method = RequestMethod.GET)
	public String contraAddPage(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute ContraVo contraVo, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				rows = 2;
				contraVo.setEdit(false);
				contraVo.setDate(DateFormatter.convertDateToString(new Date()));
				model.addAttribute("contraVo", contraVo);
				model.addAttribute("ledgers", ledgerService.contraLedgers(companyId));
			} catch (Exception e) {
				logger.info("CONTRA_ADD: ", e);
			}
			return AccountsPageNameConstants.CONTRA_ADD;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.CONTRA_ENTRY_TABLE_ROW, method = RequestMethod.GET)
	public String contraEntryTableRow(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "rows", required = true) Integer count,
			@RequestParam(value = "credit", required = true) BigDecimal credit,
			@RequestParam(value = "debit", required = true) BigDecimal debit) {
		try {
			model.addAttribute("count", rows);
			rows++;
			BigDecimal rowCredit = new BigDecimal("0.00");
			BigDecimal rowDebit = new BigDecimal("0.00");
			if (credit.compareTo(debit) > 0) {
				rowDebit = credit.subtract(debit);
				debit = debit.add(rowDebit);
			} else {
				rowCredit = debit.subtract(credit);
				credit = credit.add(rowCredit);
			}
			model.addAttribute("rowCredit", rowCredit);
			model.addAttribute("rowDebit", rowDebit);
			model.addAttribute("debit", debit);
			model.addAttribute("credit", credit);
			model.addAttribute("ledgers", ledgerService.contraLedgers(companyId));
		} catch (Exception exception) {
			logger.info("CONTRA_ENTRY_TABLE_ROW: ", exception);
		}
		return AccountsPageNameConstants.CONTRA_ENTRY_TABLE_ROW;
	}

	/**
	 * method to save contra
	 * 
	 * @param model
	 * @param companyId
	 * @param paymentVo
	 * @return
	 */
	/*
	 * @RequestMapping(value = AccountsRequestConstants.CONTRA_SAVE, method =
	 * RequestMethod.POST) public String contraSAve(final Model model,
	 * @ModelAttribute("companyId") Long companyId,
	 * 
	 * @ModelAttribute ContraVo contraVo, BindingResult bindingResult) { if
	 * (companyId == 0) return "redirect:" +
	 * ErpRequestConstants.HOME+"?"+MessageConstants
	 * .ERROR+"=You haven't created any company yet"; else { Long id = null; try
	 * { contraVo.setCompanyId(companyId); id =
	 * contraService.saveOrUpdateContra(contraVo);
	 * model.addAttribute(MessageConstants.SUCCESS,
	 * "Contra Saved Successfully"); } catch (Exception e) {
	 * e.printStackTrace(); model.addAttribute(MessageConstants.SUCCESS,
	 * e.getMessage()); } if (id != null) return "redirect:" +
	 * AccountsRequestConstants.CONTRA_EDIT + "?id=" + id; else return
	 * AccountsPageNameConstants.CONTRA_ADD; } }
	 */

	@RequestMapping(value = AccountsRequestConstants.CONTRA_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveContra(final Model model, @RequestBody ContraVo contraVo,
			@ModelAttribute("companyId") Long companyId) {
		Long id = contraVo.getContraId();
		contraVo.setCompanyId(companyId);
		try {
			id = contraService.saveOrUpdateContra(contraVo);
			if (id != null) {
				model.addAttribute(MessageConstants.SUCCESS, "Action completed successfully");
			}
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info("CONTRA_SAVE: ", e);
		}
		return id;
	}

	/**
	 * method to edit contra
	 * 
	 * @param model
	 * @param companyId
	 * @param id
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.CONTRA_EDIT, method = RequestMethod.GET)
	public String contraEdit(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute ContraVo contraVo, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			model.addAttribute(MessageConstants.SUCCESS, request.getParameter(MessageConstants.SUCCESS));
			try {
				contraVo = contraService.findContraById(id);
				contraVo.setEdit(true);
				rows = contraVo.getEntries().size() + 1;
				model.addAttribute("contraVo", contraVo);
				model.addAttribute("ledgers", ledgerService.contraLedgers(companyId));
			} catch (Exception e) {
				logger.info("CONTRA_EDIT: ", e);
			}
			return AccountsPageNameConstants.CONTRA_ADD;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.CONTRA_ENTRIES_TABLE, method = RequestMethod.GET)
	public String contraEntriesTable(final Model model, @RequestParam("id") Long id,
			@ModelAttribute("companyId") Long companyId) {
		try {
			ContraVo contraVo = contraService.findContraById(id);
			contraVo.setEdit(true);
			rows = contraVo.getEntries().size() + 1;
			model.addAttribute("contraVo", contraVo);
			model.addAttribute("ledgers", ledgerService.contraLedgers(companyId));
		} catch (Exception e) {
			logger.info("CONTRA_ENTRIES_TABLE: ", e);
		}
		return AccountsPageNameConstants.CONTRA_ENTRIES_TABLE;
	}

	/**
	 * method to list contra
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.CONTRA_LIST, method = RequestMethod.GET)
	public String contraList(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("contras", contraService.findAllContrasForCompany(companyId));
		} catch (Exception e) {
			logger.info("CONTRA_LIST: ", e);
			model.addAttribute(MessageConstants.SUCCESS, e.getMessage());
		}
		return AccountsPageNameConstants.CONTRA_LIST;
	}

	@RequestMapping(value = AccountsRequestConstants.CONTRA_LIST_FILTER, method = RequestMethod.GET)
	public String contraListFiltered(final Model model,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) {
		try {
			model.addAttribute("contras",
					contraService.findAllContrasForCompanyWithinDate(companyId, startDate, endDate));
		} catch (Exception e) {
			logger.info("CONTRA_LIST_FILTER: ", e);
			model.addAttribute(MessageConstants.SUCCESS, e.getMessage());
		}
		return AccountsPageNameConstants.CONTRA_LIST;
	}

	/**
	 * method to delete contra
	 * 
	 * @param id
	 */
	@RequestMapping(value = AccountsRequestConstants.CONTRA_DELETE, method = RequestMethod.GET)
	public @ResponseBody ModelAndView contraDelete(@RequestParam(value = "id", required = true) Long id) {
		try {
			contraService.deleteContra(id);
		} catch (Exception e) {
			logger.info("CONTRA_DELETE: ", e);
		}
		ModelAndView modelAndView = new ModelAndView(new RedirectView(AccountsRequestConstants.CONTRA_LIST));
		return modelAndView;
	}

	@RequestMapping(value = AccountsRequestConstants.CONTRA_ENTRIES_ROW_COUNT, method = RequestMethod.GET)
	public @ResponseBody Long getContraRowCount() {
		return (long) rows;
	}
}
