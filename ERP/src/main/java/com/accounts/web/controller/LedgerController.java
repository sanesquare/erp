package com.accounts.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.LedgerAccountTypeService;
import com.accounts.web.service.LedgerGroupService;
import com.accounts.web.service.LedgerService;
import com.accounts.web.service.TransactionService;
import com.accounts.web.vo.LedgerGroupVo;
import com.accounts.web.vo.LedgerVo;
import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 10-August-2015
 *
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate", "companies", "companyName" })
public class LedgerController {

	@Autowired
	private LedgerGroupService groupService;

	@Autowired
	private LedgerService ledgerService;

	@Log
	private static Logger logger;

	@Autowired
	private TransactionService transactionService;

	@RequestMapping(value = AccountsRequestConstants.LEDGER_HOME, method = RequestMethod.GET)
	public String ledgerHome(final Model model, @ModelAttribute("companies") List<CompanyVo> companies,
			@ModelAttribute("companyName") String companyName, @ModelAttribute("companyId") Long companyId) {
		model.addAttribute("title", companyName);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				model.addAttribute("ledgers", ledgerService.findLedgersForCompany(companyId));
			} catch (Exception e) {
				logger.info("LEDGER_HOME: ", e);
			}
			return AccountsPageNameConstants.LEDGER_HOME;
		}
	}

	/**
	 * Method to handle GET request for new ledger page
	 * 
	 * @param model
	 * @param ledgerVo
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.ADD_LEDGER, method = RequestMethod.GET)
	public String addLedgerHome(final Model model, @ModelAttribute LedgerVo ledgerVo,
			@ModelAttribute("companyId") Long companyId, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		model.addAttribute("startdate", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				ledgerVo.setStartDate(startDate);
				model.addAttribute("ledgerVo", ledgerVo);
				model.addAttribute("groups", groupService.findAllLedgerGroupsForCompany(companyId));
			} catch (Exception e) {
				logger.info("ADD_LEDGER: ", e);
			}
			return AccountsPageNameConstants.ADD_LEDGER;
		}
	}

	/**
	 * Method to handle save ledger request
	 * 
	 * @param model
	 * @param ledgerVo
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.SAVE_LEDGER, method = RequestMethod.POST)
	public String saveLedger(final Model model, @ModelAttribute LedgerVo ledgerVo,
			@ModelAttribute("companyId") Long companyId) {
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			Long id = ledgerVo.getLedgerId();
			ledgerVo.setCompanyId(companyId);
			try {
				id = ledgerService.saveOrUpdateLedger(ledgerVo);
				model.addAttribute(MessageConstants.SUCCESS, "Ledger saved successfully");
			} catch (ItemNotFoundException ex) {
				model.addAttribute(MessageConstants.ERROR, ex.getMessage());
			} catch (Exception e) {
				model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
				logger.info("SAVE_LEDGER: ", e);
			}
			if (id != null)
				return "redirect:" + AccountsRequestConstants.EDIT_LEDGER + "?id=" + id;
			return AccountsPageNameConstants.LEDGER_HOME;
		}
	}

	/**
	 * Method to handle request for edit ledger
	 * 
	 * @param model
	 * @param id
	 * @param request
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.EDIT_LEDGER, method = RequestMethod.GET)
	public String editLedger(final Model model, @RequestParam(value = "id") Long id, HttpServletRequest request,
			@ModelAttribute("companyId") Long companyId, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			try {
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
				model.addAttribute(MessageConstants.ERROR, errorMessage);

				model.addAttribute("ledgerVo", ledgerService.findLedgerById(id));
				model.addAttribute("groups", groupService.findAllLedgerGroupsForCompany(companyId));
			} catch (Exception exception) {
				logger.info("EDIT_LEDGER: ", exception);
			}
			return AccountsPageNameConstants.ADD_LEDGER;
		}
	}

	/**
	 * Method to handle ledger delete request
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.DELETE_LEDGER, method = RequestMethod.GET)
	public @ResponseBody String deleteLedgerHandler(@RequestParam(value = "id", required = true) Long id) {
		try {
			ledgerService.deleteLedger(id);
		} catch (ItemNotFoundException e) {
			logger.info("DELETE_LEDGER: ", e);
			return e.getMessage();
		} catch (Exception ex) {
			logger.info("DELETE_LEDGER: ", ex);
			return "Could Not Delete Ledger ";
		}
		return "Ledger Deleted Successfully";
	}

	/**
	 * Method to list all ledgers
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.LEDGER_LIST, method = RequestMethod.GET)
	public String ledgerListHandler(final Model model, @RequestParam(value="companyId",required=true) Long companyId) {
		try {
			model.addAttribute("ledgers", ledgerService.findLedgersForCompany(companyId));
		} catch (Exception ex) {
			logger.info("LEDGER_LIST: ", ex);
		}
		return AccountsPageNameConstants.LEDGER_LIST;
	}

}
