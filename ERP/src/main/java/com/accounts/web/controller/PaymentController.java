package com.accounts.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.LedgerService;
import com.accounts.web.service.PaymentService;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.PaymentVo;
import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate" ,"companies","companyName"})
public class PaymentController {

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private CompanyService companyService;

	@Log
	private static Logger logger;

	private static int rows = 2;

	/**
	 * method to reach payment home page
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.PAYMENT_HOME, method = RequestMethod.GET)
	public String paymentHome(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @ModelAttribute("currencyName") String currency,
			@ModelAttribute("companies") List<CompanyVo> companies, @ModelAttribute("companyName") String companyName,
			@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate) {
		model.addAttribute("currencyName", currency);
		model.addAttribute("companyStart", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			model.addAttribute(MessageConstants.SUCCESS, request.getParameter(MessageConstants.SUCCESS));
			try {
				model.addAttribute("title", companyName + " ( " + startDate + " to " + endDate + " )");
				model.addAttribute("payments", paymentService.findAllPaymentsForCompany(companyId));
				model.addAttribute("companies", companies);
			} catch (Exception e) {
				logger.info("PAYMENT_HOME: ", e);
				model.addAttribute(MessageConstants.SUCCESS, e.getMessage());
			}
			return AccountsPageNameConstants.PAYMENT_HOME;
		}
	}

	/**
	 * method to reach add payment page
	 * 
	 * @param model
	 * @param companyId
	 * @param paymentVo
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.PAYMENT_ADD, method = RequestMethod.GET)
	public String paymentAddPage(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute PaymentVo paymentVo, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				rows = 2;
				paymentVo.setEdit(false);
				paymentVo.setDate(DateFormatter.convertDateToString(new Date()));
				model.addAttribute("paymentVo", paymentVo);
				model.addAttribute("ledgers", ledgerService.paymentLedgers(companyId));
			} catch (Exception e) {
				logger.info("PAYMENT_ADD: ", e);
			}
			return AccountsPageNameConstants.PAYMENT_ADD;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.PAYMENT_ENTRY_TABLE_ROW, method = RequestMethod.GET)
	public String paymentEntryTableRow(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "rows", required = true) Integer count,
			@RequestParam(value = "credit", required = true) BigDecimal credit,
			@RequestParam(value = "debit", required = true) BigDecimal debit) {
		try {
			model.addAttribute("count", rows);
			rows++;
			BigDecimal rowCredit = new BigDecimal("0.00");
			BigDecimal rowDebit = new BigDecimal("0.00");
			if (credit.compareTo(debit) > 0) {
				rowDebit = credit.subtract(debit);
				debit = debit.add(rowDebit);
			} else {
				rowCredit = debit.subtract(credit);
				credit = credit.add(rowCredit);
			}
			model.addAttribute("rowCredit", rowCredit);
			model.addAttribute("rowDebit", rowDebit);
			model.addAttribute("debit", debit);
			model.addAttribute("credit", credit);
			model.addAttribute("ledgers", ledgerService.paymentLedgers(companyId));
		} catch (Exception exception) {
			logger.info("PAYMENT_ENTRY_TABLE_ROW: ", exception);
		}
		return AccountsPageNameConstants.PAYMENT_ENTRY_TABLE_ROW;
	}

	/**
	 * method to save payment
	 * 
	 * @param model
	 * @param companyId
	 * @param paymentVo
	 * @return
	 */
	/*
	 * @RequestMapping(value = AccountsRequestConstants.PAYMENT_SAVE, method =
	 * RequestMethod.POST) public String paymentSAve(final Model model,
	 * 
	 * @ModelAttribute("companyId") Long companyId,
	 * 
	 * @ModelAttribute PaymentVo paymentVo, BindingResult bindingResult) { if
	 * (companyId == 0) return "redirect:" + ErpRequestConstants.HOME + "?" +
	 * MessageConstants.ERROR + "=You haven't created any company yet"; else {
	 * Long id = null; try { paymentVo.setCompanyId(companyId); id =
	 * paymentService.saveOrUpdatePayment(paymentVo);
	 * model.addAttribute(MessageConstants.SUCCESS,
	 * "Payment Saved Successfully"); } catch (Exception e) {
	 * e.printStackTrace(); model.addAttribute(MessageConstants.SUCCESS,
	 * e.getMessage()); } if (id != null) return "redirect:" +
	 * AccountsRequestConstants.PAYMENT_EDIT + "?id=" + id; else return
	 * AccountsPageNameConstants.PAYMENT_ADD; } }
	 */

	@RequestMapping(value = AccountsRequestConstants.PAYMENT_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long savePayment(final Model model, @RequestBody PaymentVo paymentVo,
			@ModelAttribute("companyId") Long companyId) {
		Long id = paymentVo.getPaymentId();
		paymentVo.setCompanyId(companyId);
		try {
			id = paymentService.saveOrUpdatePayment(paymentVo);
			if (id != null) {
				model.addAttribute(MessageConstants.SUCCESS, "Action completed successfully");
			}
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info("PAYMENT_SAVE: ", e);
		}
		return id;
	}

	/**
	 * method to edit payment
	 * 
	 * @param model
	 * @param companyId
	 * @param id
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.PAYMENT_EDIT, method = RequestMethod.GET)
	public String paymentEdit(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute PaymentVo paymentVo, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			model.addAttribute(MessageConstants.SUCCESS, request.getParameter(MessageConstants.SUCCESS));
			try {
				paymentVo = paymentService.findPaymentById(id);
				paymentVo.setEdit(true);
				rows = paymentVo.getEntries().size() + 1;
				model.addAttribute("paymentVo", paymentVo);
				model.addAttribute("ledgers", ledgerService.paymentLedgers(companyId));
			} catch (Exception e) {
				logger.info("PAYMENT_EDIT: ", e);
			}
			return AccountsPageNameConstants.PAYMENT_ADD;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.PAYMENT_ENTRIES_TABLE, method = RequestMethod.GET)
	public String paymentEntriesTable(final Model model, @RequestParam("id") Long id,
			@ModelAttribute("companyId") Long companyId) {
		try {
			PaymentVo paymentVo = paymentService.findPaymentById(id);
			paymentVo.setEdit(true);
			rows = paymentVo.getEntries().size() + 1;
			model.addAttribute("paymentVo", paymentVo);
			model.addAttribute("ledgers", ledgerService.paymentLedgers(companyId));
		} catch (Exception e) {
			logger.info("PAYMENT_ENTRIES_TABLE: ", e);
		}
		return AccountsPageNameConstants.PAYMENT_ENTRIES_TABLE;
	}

	/**
	 * method to list payment
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.PAYMENT_LIST, method = RequestMethod.GET)
	public String paymentList(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("payments", paymentService.findAllPaymentsForCompany(companyId));
		} catch (Exception e) {
			logger.info("PAYMENT_LIST: ", e);
			model.addAttribute(MessageConstants.SUCCESS, e.getMessage());
		}
		return AccountsPageNameConstants.PAYMENT_LIST;
	}

	@RequestMapping(value = AccountsRequestConstants.PAYMENT_LIST_FILTER, method = RequestMethod.GET)
	public String paymentListFiltered(final Model model,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) {
		try {
			model.addAttribute("payments",
					paymentService.findAllPaymentsForCompanyWithinDate(companyId, startDate, endDate));
		} catch (Exception e) {
			logger.info("PAYMENT_LIST_FILTER: ", e);
			model.addAttribute(MessageConstants.SUCCESS, e.getMessage());
		}
		return AccountsPageNameConstants.PAYMENT_LIST;
	}

	/**
	 * method to delete payment
	 * 
	 * @param id
	 */
	@RequestMapping(value = AccountsRequestConstants.PAYMENT_DELETE, method = RequestMethod.GET)
	public @ResponseBody ModelAndView paymentDelete(@RequestParam(value = "id", required = true) Long id) {
		try {
			paymentService.deletePayment(id);
			// jsonResponse.setStatus("Payment Deleted Successfully.");
		} catch (Exception e) {
			logger.info("PAYMENT_DELETE: ", e);
			// jsonResponse.setStatus(e.getMessage());
		}
		ModelAndView modelAndView = new ModelAndView(new RedirectView(AccountsRequestConstants.PAYMENT_LIST));
		return modelAndView;
	}

	@RequestMapping(value = AccountsRequestConstants.PAYMENT_ENTRIES_ROW_COUNT, method = RequestMethod.GET)
	public @ResponseBody Long getPaymentRowCount() {
		return (long) rows;
	}
}
