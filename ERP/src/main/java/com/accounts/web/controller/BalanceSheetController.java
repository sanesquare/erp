package com.accounts.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.TransactionService;
import com.accounts.web.vo.BalanceSheetVo;
import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.logger.Log;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 24-August-2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate", "companies", "companyName","balanceSheetVo" })
public class BalanceSheetController {

	@Autowired
	private TransactionService transactionService;
	@Autowired
	private CompanyService companyService;
	@Log
	private static Logger logger;

	@RequestMapping(value = AccountsRequestConstants.BALANCE_SHEET, method = RequestMethod.GET)
	public String balanceSheet(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("companies") List<CompanyVo> companies, @ModelAttribute("companyName") String companyName,
			@ModelAttribute("currencyName") String currency, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		model.addAttribute("currencyName", currency);
		model.addAttribute("companyStart", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				BalanceSheetVo balanceSheetVo = transactionService.balanceSheetCalculation(companyId);
				model.addAttribute("balanceSheetVo", balanceSheetVo);
				model.addAttribute("title", companyName + " ( " + startDate + " to " + endDate + " )");
				model.addAttribute("entries", balanceSheetVo);
				model.addAttribute("companies", companies);
			} catch (Exception e) {
				logger.info("BALANCE_SHEET: ", e);
			}
			return AccountsPageNameConstants.BALANCE_SHEET;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.BALANCE_SHEET_TABLE, method = RequestMethod.GET)
	public String balanceSheetTable(final Model model,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@ModelAttribute("currencyName") String currency,
			@RequestParam(value = "start", required = true) String start,
			@RequestParam(value = "end", required = true) String end) {
		model.addAttribute("startDate", start);
		model.addAttribute("endDate", end);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				model.addAttribute("entries", transactionService.balanceSheetCalculation(companyId, start, end));
				model.addAttribute("currencyName", currency);
			} catch (Exception e) {
				logger.info("BALANCE_SHEET_TABLE: ", e);
			}
			return AccountsPageNameConstants.BALANCE_SHEET_TABLE;
		}
	}
}
