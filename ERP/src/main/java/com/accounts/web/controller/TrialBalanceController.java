package com.accounts.web.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.TransactionService;
import com.accounts.web.vo.LedgerVo;
import com.accounts.web.vo.TrialBalanceLedgerGroupVo;
import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.logger.Log;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 21-August-2015
 *
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate" , "companies", "companyName"})
public class TrialBalanceController {

	@Autowired
	private TransactionService transactionService;

	@Log
	private static Logger logger;

	@Autowired
	private CompanyService companyService;

	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.TRIAL_BALANCE, method = RequestMethod.GET)
	public String trialBalanceHome(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("companies") List<CompanyVo> companies, @ModelAttribute("companyName") String companyName,
			@ModelAttribute("currencyName") String currency, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				model.addAttribute("title", companyName + " ( " + startDate + " to " + endDate + " )");
				Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> result = transactionService
						.trialBalanceCalculations(companyId, startDate, endDate);
				model.addAttribute("entries", result);
				model.addAttribute("companies", companies);
				model.addAttribute("currencyName", currency);
				model.addAttribute("companyStart", startDate);
				model.addAttribute("endDate", endDate);
			} catch (Exception e) {
				logger.info("TRIAL_BALANCE: ", e);
			}
			return AccountsPageNameConstants.TRIAL_BALANCE;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.TRIAL_BALANCE_TABLE, method = RequestMethod.GET)
	public String trialBalanceTable(final Model model,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "start", required = true) String start,
			@ModelAttribute("currencyName") String currency, @RequestParam(value = "end", required = true) String end) {
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> result = transactionService.trialBalanceCalculations(
						companyId, start, end);
				model.addAttribute("entries", result);
				model.addAttribute("currencyName", currency);
			} catch (Exception e) {
				logger.info("TRIAL_BALANCE_TABLE: ", e);
			}
			return AccountsPageNameConstants.TRIAL_BALANCE_TABLE;
		}
	}
}
