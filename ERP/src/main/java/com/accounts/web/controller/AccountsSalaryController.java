package com.accounts.web.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.LedgerService;
import com.erp.web.service.CompanyService;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.constants.PageNameConstatnts;
import com.hrms.web.constants.PaySlipConstants;
import com.hrms.web.constants.RequestConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.service.PaySalaryService;
import com.hrms.web.service.SalaryApprovalStatusService;
import com.hrms.web.util.DateFormatter;
import com.hrms.web.vo.EmployeeVo;
import com.hrms.web.vo.PayPaysalaryVo;
import com.hrms.web.vo.PaySalaryVo;
import com.hrms.web.vo.SessionRolesVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 04-09-2015
 *
 */
@Controller
@SessionAttributes({ "companyId" })
public class AccountsSalaryController {

	@Autowired
	private PaySalaryService paySalaryService;

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private SalaryApprovalStatusService salaryApprovalStatusService;

	@Log
	private static Logger logger;

	@RequestMapping(value = AccountsRequestConstants.SALARY_LIST, method = RequestMethod.GET)
	public String salaryHomePage(final Model model, HttpServletRequest request,
			@ModelAttribute("roles") SessionRolesVo rolesMap,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("companyId") Long companyId) {
		model.addAttribute("rolesMap", rolesMap);
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String erroMessage = request.getParameter(MessageConstants.ERROR);
		if (successMessage != null)
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
		else if (erroMessage != null)
			model.addAttribute(MessageConstants.ERROR, erroMessage);
		try {
			model.addAttribute("salaries", paySalaryService.findUnpaidSalaries(companyId));
			model.addAttribute("status", salaryApprovalStatusService.findAllStatus());
			model.addAttribute("companies", companyService.findChildCompanies(companyId));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info("SALARY_LIST: ", e);
		}
		return AccountsPageNameConstants.SALARIES;
	}
	
	@RequestMapping(value = AccountsRequestConstants.FILTER_ACCOUNT_SALARY, method = RequestMethod.GET)
	public String salaryList(final Model model, HttpServletRequest request,
			@RequestParam(value="companyId",required=true) Long companyId) {
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String erroMessage = request.getParameter(MessageConstants.ERROR);
		if (successMessage != null)
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
		else if (erroMessage != null)
			model.addAttribute(MessageConstants.ERROR, erroMessage);
		try {
			model.addAttribute("salaries", paySalaryService.findUnpaidSalaries(companyId));
			model.addAttribute("status", salaryApprovalStatusService.findAllStatus());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info("FILTER_ACCOUNT_SALARY: ", e);
		}
		return AccountsPageNameConstants.SALARY_LIST;
	}

	@RequestMapping(value = AccountsRequestConstants.SALARY_LIST_TEMPLATE, method = RequestMethod.GET)
	public String salaryList(final Model model, HttpServletRequest request,
			@ModelAttribute("authEmployeeDetails") EmployeeVo authEmployeeDetails,
			@ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("salaries", paySalaryService.findUnpaidSalaries(companyId));
			model.addAttribute("status", salaryApprovalStatusService.findAllStatus());
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info("SALARY_LIST_TEMPLATE: ", e);
		}
		return AccountsPageNameConstants.SALARY_LIST;
	}

	@RequestMapping(value = AccountsRequestConstants.UPDATE_SALARY_STATUS, method = RequestMethod.GET)
	public @ResponseBody ModelAndView updateStatus(@RequestParam(value = "salaryId") Long salaryId,
			@RequestParam(value = "statusId") Long statusId, @ModelAttribute("companyId") Long companyId) {
		try {
			paySalaryService.updateSalaryStatus(salaryId, statusId);
		} catch (Exception e) {
			logger.info("UPDATE_SALARY_STATUS: ", e);
		}
		ModelAndView view = new ModelAndView(new RedirectView(AccountsRequestConstants.SALARY_LIST_TEMPLATE));
		return view;
	}

	@RequestMapping(value = "paypop.do", method = RequestMethod.GET)
	public String payPop(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("accounts", ledgerService.findAccountsForSalary(companyId));
		} catch (Exception e) {
			logger.info("SALARY_LIST: ", e);
		}
		return AccountsPageNameConstants.PAY_POP;
	}

	@RequestMapping(value = AccountsRequestConstants.VIEW_SALARY, method = RequestMethod.GET)
	public String viewSalary(final Model model, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request, @RequestParam(value = "tyPe", required = true) String type,
			@ModelAttribute PaySalaryVo paySalary, @ModelAttribute PaySalaryVo paySalaryVo,
			@RequestParam(value="compId",required=true) Long companyId) {
		String successMessage = request.getParameter(MessageConstants.SUCCESS);
		String erroMessage = request.getParameter(MessageConstants.ERROR);
		if (successMessage != null)
			model.addAttribute(MessageConstants.SUCCESS, successMessage);
		else if (erroMessage != null)
			model.addAttribute(MessageConstants.ERROR, erroMessage);
		try {

			PaySalaryVo salaryVo = null;
			if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_DAILY))
				salaryVo = paySalaryService.findDailyPaySalary(id);
			else if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_HOURLY))
				salaryVo = paySalaryService.findHourlyPaySalary(id);
			else if (type.equalsIgnoreCase(PaySlipConstants.SALARY_TYPE_MONTHLY))
				salaryVo = paySalaryService.findMonthlyPaySalary(id);
			model.addAttribute("date",DateFormatter.convertDateToString(new Date()));
			model.addAttribute("paySalary", salaryVo);
			model.addAttribute("paySalaryVo", salaryVo);
			model.addAttribute("accounts", ledgerService.findAccountsForSalary(companyId));
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info("VIEW_SALARY: ", e);
		}
		return AccountsPageNameConstants.VIEW_SALARY;
	}

	@RequestMapping(value = AccountsRequestConstants.PAY_SALARY, method = RequestMethod.GET)
	public @ResponseBody String paySalary(final Model model,
			@RequestParam(value = "salaryId", required = true) Long salaryId,
			@RequestParam(value = "ledgerId", required = true) Long ledgerId) {
		try {
			paySalaryService.paySalary(ledgerId, salaryId);
		} catch (Exception e) {
			logger.info("PAY_SALARY: ", e);
			return e.getMessage();
		}
		return "ok";
	}
	
	@RequestMapping(value = AccountsRequestConstants.PAY_SALARY, method = RequestMethod.POST)
	public @ResponseBody String paySalaryPost(final Model model,
			@RequestBody PayPaysalaryVo vo) {
		try {
			paySalaryService.updatePaySalaryPay(vo);
		} catch (Exception e) {
			logger.info("PAY_SALARY: ", e);
			return e.getMessage();
		}
		return "ok";
	}
}
