package com.accounts.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.ContraService;
import com.accounts.web.service.JournalService;
import com.accounts.web.service.PaymentService;
import com.accounts.web.service.ReceiptService;
import com.accounts.web.service.TransactionService;
import com.accounts.web.vo.BalanceSheetGroupVo;
import com.accounts.web.vo.BalanceSheetLedgerVo;
import com.accounts.web.vo.BalanceSheetVo;
import com.accounts.web.vo.ContraVo;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.LedgerGroupViewVo;
import com.accounts.web.vo.LedgerViewMonthlyVo;
import com.accounts.web.vo.LedgerViewVo;
import com.accounts.web.vo.PaymentVo;
import com.accounts.web.vo.ReceiptVo;
import com.hrms.web.logger.Log;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 24, 2015
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate", "companies", "companyName", "balanceSheetVo" })
public class AccountsPopUpController {

	@Log
	private Logger logger;

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private JournalService journalService;

	@Autowired
	private ContraService contraService;

	@Autowired
	private PaymentService paymentService;

	private BigDecimal zero = new BigDecimal("0.00");

	@Autowired
	private ReceiptService receiptService;

	private Map<Integer, LedgerViewMonthlyVo> ledgerMonthlyDetails = new HashMap<Integer, LedgerViewMonthlyVo>();

	private Map<Integer, LedgerViewVo> ledgerViewMap = new HashMap<Integer, LedgerViewVo>();

	private List<Integer> ids = new ArrayList<Integer>();

	private LedgerGroupViewVo ledgerGroupViewVo = new LedgerGroupViewVo();

	private Boolean isFresh = false;

	private Map<Long, LedgerViewVo> ledgerMap = new HashMap<Long, LedgerViewVo>();

	@RequestMapping(value = AccountsRequestConstants.LEDGER_GROUP_VIEW_POP, method = RequestMethod.GET)
	public String ledgerGroupView(final Model model, @RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate,
			@ModelAttribute("companyId") Long companyId) {
		try {
			ledgerMonthlyDetails.clear();
			ledgerViewMap.clear();
			ids.clear();
			ids.add(0);
			ledgerMap.clear();
			LedgerGroupViewVo ledgerGroupViewVo = transactionService.findTRansactionsForLedgerGroup(startDate, endDate,
					id, companyId);
			List<LedgerViewVo> ledgerViewVos = ledgerGroupViewVo.getLedgerViewVos();
			for (LedgerViewVo ledgerViewVo : ledgerViewVos) {
				ledgerMap.put(ledgerViewVo.getLedgerId(), ledgerViewVo);
			}
			this.ledgerGroupViewVo = ledgerGroupViewVo;
			model.addAttribute("startDate", startDate);
			model.addAttribute("endDate", endDate);
			model.addAttribute("zero", zero);
			model.addAttribute("group", ledgerGroupViewVo);
		} catch (Exception e) {
			logger.info("", e);
		}
		return AccountsPageNameConstants.LEDGER_GROUP_VIEW_POP;
	}

	@RequestMapping(value = AccountsRequestConstants.LEDGER_VIEW_POP, method = RequestMethod.GET)
	public String ledgerView(final Model model, @RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate,
			@RequestParam(value = "isFresh", required = true) Boolean isFresh) {
		try {
			LedgerViewVo ledgerViewVo = null;
			this.isFresh = isFresh;
			if (isFresh) {
				ids.clear();
				ledgerViewVo = transactionService.findTRansactionsForLedger(startDate, endDate, id, id);
			} else
				ledgerViewVo = ledgerMap.get(id);
			ids.add(ledgerViewVo.getHashCode());
			ledgerViewMap.put(ledgerViewVo.getHashCode(), ledgerViewVo);
			for (LedgerViewMonthlyVo monthlyVo : ledgerViewVo.getMonthlyVos()) {
				ledgerMonthlyDetails.put(monthlyVo.getHashCode(), monthlyVo);
			}
			model.addAttribute("ledger", ledgerViewVo);
			model.addAttribute("startDate", startDate);
			model.addAttribute("endDate", endDate);
			model.addAttribute("zero", zero);
			model.addAttribute("id", ids.get(ids.size() - 1));
			if (isFresh)
				model.addAttribute("show", false);
			else
				model.addAttribute("show", true);
		} catch (Exception e) {
			logger.info("", e);
		}
		if (isFresh)
			return AccountsPageNameConstants.LEDGER_VIEW_POP;
		else
			return AccountsPageNameConstants.LEDGER_VIEW_POP_TEMP;
	}

	@RequestMapping(value = AccountsRequestConstants.LEDGER_DETAILED_VIEW_POP, method = RequestMethod.GET)
	public String ledgerDetailedView(final Model model, @RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) {
		try {
			LedgerViewMonthlyVo monthlyVo = ledgerMonthlyDetails.get(id);
			model.addAttribute("month", monthlyVo);
			model.addAttribute("startDate", startDate);
			model.addAttribute("endDate", endDate);
			int index = ids.size() - 1;
			if (index >= 0)
				id = ids.get(index);
			else
				id = 0;
			model.addAttribute("zero", zero);
			model.addAttribute("id", id);
			model.addAttribute("monthlyId", monthlyVo.getHashCode());
		} catch (Exception e) {
			logger.info("", e);
		}
		return AccountsPageNameConstants.LEDGER_DETAILED_VIEW_POP;
	}

	@RequestMapping(value = AccountsRequestConstants.LEDGER_VOUCHER_VIEW_POP, method = RequestMethod.GET)
	public String ledgerVoucherView(final Model model, @RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "voucher", required = true) String voucher,
			@RequestParam(value = "monthlyId", required = true) Integer monthlyId) {
		String page = null;
		try {
			model.addAttribute("zero", zero);
			model.addAttribute("monthlyId", monthlyId);
			if (voucher.equalsIgnoreCase("journal")) {
				JournalVo vo = journalService.findJournalById(id);
				model.addAttribute("vo", vo);
				page = AccountsPageNameConstants.JOURNAL_POP;
			} else if (voucher.equalsIgnoreCase("payment")) {
				PaymentVo vo = paymentService.findPaymentById(id);
				model.addAttribute("vo", vo);
				page = AccountsPageNameConstants.PAYMENT_POP;
			} else if (voucher.equalsIgnoreCase("receipt")) {
				ReceiptVo vo = receiptService.findReceiptById(id);
				model.addAttribute("vo", vo);
				page = AccountsPageNameConstants.RECEIPT_POP;
			} else if (voucher.equalsIgnoreCase("contra")) {
				ContraVo vo = contraService.findContraById(id);
				model.addAttribute("vo", vo);
				page = AccountsPageNameConstants.CONTRA_POP;
			}
		} catch (Exception e) {
			logger.info("", e);
		}
		return page;
	}

	@RequestMapping(value = AccountsRequestConstants.LEDGER_VIEW_BACK, method = RequestMethod.GET)
	public String ledgerViewBack(final Model model, @RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) {
		try {
			model.addAttribute("zero", zero);
			model.addAttribute("id", id);
			if (type.equalsIgnoreCase("ledger")) {
				LedgerViewVo ledgerViewVo = ledgerViewMap.get(id);
				id = ledgerViewVo.getHashCode();
				model.addAttribute("ledger", ledgerViewVo);
				if (isFresh)
					model.addAttribute("show", false);
				else
					model.addAttribute("show", true);
			} else if (type.equalsIgnoreCase("month")) {
				model.addAttribute("id", ids.get(ids.size() - 1));
				LedgerViewMonthlyVo monthlyVo = ledgerMonthlyDetails.get(id);
				model.addAttribute("month", monthlyVo);
				model.addAttribute("monthlyId", monthlyVo.getHashCode());
			} else {
				model.addAttribute("group", ledgerGroupViewVo);
				model.addAttribute("show", true);
			}

			model.addAttribute("startDate", startDate);
			model.addAttribute("endDate", endDate);
		} catch (Exception e) {
			logger.info("", e);
		}
		if (type.equalsIgnoreCase("ledger"))
			return AccountsPageNameConstants.LEDGER_VIEW_POP_TEMP;
		else if (type.equalsIgnoreCase("month"))
			return AccountsPageNameConstants.LEDGER_DETAILED_VIEW_POP_TEMP;
		else
			return AccountsPageNameConstants.LEDGER_GROUP_VIEW_POP_TEMP;
	}

}
