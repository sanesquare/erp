package com.accounts.web.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.form.JournalHistoryPdfForm;
import com.accounts.web.service.JournalService;
import com.accounts.web.service.LedgerService;
import com.accounts.web.utils.JournalPdfUtil;
import com.accounts.web.vo.JournalHistoryPdfVo;
import com.accounts.web.vo.JournalVo;
import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 14-August-2015
 *
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate", "companies", "companyName" })
public class JournalController {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private JournalService journalService;

	@Log
	private static Logger logger;

	private static int rows = 2;

	@RequestMapping(value = AccountsRequestConstants.JOURNAL_HOME, method = RequestMethod.GET)
	public String journalHome(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("companies") List<CompanyVo> companies, @ModelAttribute("companyName") String companyName,
			@ModelAttribute("currencyName") String currency, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		model.addAttribute("currencyName", currency);
		model.addAttribute("companyStart", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				model.addAttribute("title", companyName + " ( " + startDate + " to " + endDate + " )");
				model.addAttribute("journals", journalService.findAllJournalsForCompany(companyId));
				model.addAttribute("companies", companies);

			} catch (Exception ex) {
				logger.info("JOURNAL_HOME: ", ex);
			}
			return AccountsPageNameConstants.JOURNAL_HOME;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.ADD_JOURNAL, method = RequestMethod.GET)
	public String journalAddPage(final Model model, @ModelAttribute JournalVo journalVo,
			@ModelAttribute("companyId") Long companyId, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				rows = 2;
				journalVo.setEdit(false);
				journalVo.setDate(DateFormatter.convertDateToString(new Date()));
				model.addAttribute("journalVo", journalVo);
				model.addAttribute("ledgers", ledgerService.journalLedgers(companyId));

			} catch (Exception exception) {
				logger.info("ADD_JOURNAL: ", exception);
			}
			return AccountsPageNameConstants.ADD_JOURNAL;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.JOURNAL_ENTRY_TABLE_ROW, method = RequestMethod.GET)
	public String journalEntryTableRow(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "rows", required = true) Integer count,
			@RequestParam(value = "credit", required = true) BigDecimal credit,
			@RequestParam(value = "debit", required = true) BigDecimal debit) {
		try {
			model.addAttribute("count", rows);
			rows++;
			BigDecimal rowCredit = new BigDecimal("0.00");
			BigDecimal rowDebit = new BigDecimal("0.00");
			if (credit.compareTo(debit) > 0) {
				rowDebit = credit.subtract(debit);
				debit = debit.add(rowDebit);
			} else {
				rowCredit = debit.subtract(credit);
				credit = credit.add(rowCredit);
			}
			model.addAttribute("rowCredit", rowCredit);
			model.addAttribute("rowDebit", rowDebit);
			model.addAttribute("debit", debit);
			model.addAttribute("credit", credit);
			model.addAttribute("ledgers", ledgerService.journalLedgers(companyId));
		} catch (Exception exception) {
			logger.info("JOURNAL_ENTRY_TABLE_ROW: ", exception);
		}
		return AccountsPageNameConstants.JOURNAL_ENTRY_TABLE_ROW;
	}

	@RequestMapping(value = AccountsRequestConstants.SAVE_JOURNAL, method = RequestMethod.POST)
	public @ResponseBody Long saveJournal(final Model model, @RequestBody JournalVo journalVo,
			@ModelAttribute("companyId") Long companyId) {
		Long id = journalVo.getJournalId();
		journalVo.setCompanyId(companyId);
		try {
			id = journalService.saveOrUpdateJournal(journalVo);
			if (id != null) {
				model.addAttribute(MessageConstants.SUCCESS, "Action completed successfully");
			}
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info("SAVE_JOURNAL: ", e);
		}
		return id;
	}

	@RequestMapping(value = AccountsRequestConstants.EDIT_JOURNAL, method = RequestMethod.GET)
	public String editJournal(final Model model, @RequestParam("id") Long id, HttpServletRequest request,
			@ModelAttribute("companyId") Long companyId, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			String successMessage = request.getParameter(MessageConstants.SUCCESS);
			String errorMessage = request.getParameter(MessageConstants.ERROR);
			try {
				model.addAttribute(MessageConstants.SUCCESS, successMessage);
				model.addAttribute(MessageConstants.ERROR, errorMessage);
				JournalVo journalVo = journalService.findJournalById(id);
				journalVo.setEdit(true);
				rows = journalVo.getEntries().size() + 1;
				model.addAttribute("journalVo", journalVo);
				model.addAttribute("ledgers", ledgerService.journalLedgers(companyId));
			} catch (Exception e) {
				model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
				logger.info("EDIT_JOURNAL: ", e);
			}
			return AccountsPageNameConstants.ADD_JOURNAL;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.JOURNAL_ENTRIES_TABLE, method = RequestMethod.GET)
	public String journalEntriesTable(final Model model, @RequestParam("id") Long id,
			@ModelAttribute("companyId") Long companyId) {
		try {
			JournalVo journalVo = journalService.findJournalById(id);
			journalVo.setEdit(true);
			rows = journalVo.getEntries().size() + 1;
			model.addAttribute("journalVo", journalVo);
			model.addAttribute("ledgers", ledgerService.journalLedgers(companyId));
		} catch (Exception e) {
			logger.info("JOURNAL_ENTRIES_TABLE: ", e);
		}
		return AccountsPageNameConstants.JOURNAL_ENTRIES_TABLE;
	}

	@RequestMapping(value = AccountsRequestConstants.DELETE_JOURNAL, method = RequestMethod.GET)
	public @ResponseBody String deleteJournal(final Model model, @RequestParam("id") Long id,
			HttpServletRequest request, @ModelAttribute("companyId") Long companyId) {
		try {
			journalService.deleteJournal(id);
			model.addAttribute("journals", journalService.findAllJournalsForCompany(companyId));
		} catch (ItemNotFoundException e) {
			return e.getMessage();
		} catch (Exception ex) {
			logger.info("DELETE_JOURNAL: ", ex);
			return ex.getMessage();
		}
		return "ok";
	}

	@RequestMapping(value = AccountsRequestConstants.JOURNAL_LIST, method = RequestMethod.GET)
	public String journalList(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("journals", journalService.findAllJournalsForCompany(companyId));
		} catch (Exception ex) {
			logger.info("JOURNAL_LIST: ", ex);
		}
		return AccountsPageNameConstants.JOURNAL_LIST;
	}

	@RequestMapping(value = AccountsRequestConstants.JOURNAL_LIST_FILTER, method = RequestMethod.GET)
	public String journalListFiltered(final Model model,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) {
		try {
			model.addAttribute("journals",
					journalService.findAllJournalsForCompanyWithinDate(companyId, startDate, endDate));
		} catch (Exception e) {
			logger.info("JOURNAL_LIST_FILTER: ", e);
			model.addAttribute(MessageConstants.SUCCESS, e.getMessage());
		}
		return AccountsPageNameConstants.JOURNAL_LIST;
	}

	@RequestMapping(value = AccountsRequestConstants.JOURNAL_ENTRIES_ROW_COUNT, method = RequestMethod.GET)
	public @ResponseBody Long getJournalRowCount() {
		return (long) rows;
	}
}
