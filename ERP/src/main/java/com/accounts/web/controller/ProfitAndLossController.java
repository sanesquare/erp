package com.accounts.web.controller;


import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.TransactionService;
import com.accounts.web.vo.LedgerVo;
import com.accounts.web.vo.ProfitAndLossGroupVo;
import com.accounts.web.vo.ProfitAndLossLedgerVo;
import com.accounts.web.vo.ProfitAndLossVo;
import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.logger.Log;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 22-August-2015
 *
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate" ,"companies","companyName"})
public class ProfitAndLossController {

	@Autowired
	private TransactionService transactionService;
	
	@Autowired
	private CompanyService companyService;

	@Log
	private static Logger logger;

	@RequestMapping(value = AccountsRequestConstants.PROFIT_AND_LOSS, method = RequestMethod.GET)
	public String profitAnsLoss(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute("companies") List<CompanyVo> companies, @ModelAttribute("companyName") String companyName,
			@ModelAttribute("currencyName") String currency, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate) {
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				ProfitAndLossVo profitAndLossVo= transactionService.profitAndLossentries(companyId,startDate,endDate);
				model.addAttribute("title", companyName + " ( " + startDate + " to " + endDate + " )");
				model.addAttribute("entries", profitAndLossVo);
				model.addAttribute("currencyName", currency);
				model.addAttribute("companies", companyService.findChildCompanies(companyId));
				model.addAttribute("companyStart", startDate);
				model.addAttribute("endDate", endDate);
			} catch (Exception e) {
				logger.info("PROFIT_AND_LOSS: ",e);
			}
			return AccountsPageNameConstants.PROFIT_AND_LOSS;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.PROFIT_AND_LOSS_TABLE, method = RequestMethod.GET)
	public String profitAndLossTable(final Model model, @RequestParam(value="companyId",required=true) Long companyId,
			@ModelAttribute("currencyName") String currency,
			@RequestParam(value = "start", required = true) String start,
			@RequestParam(value = "end", required = true) String end) {
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				model.addAttribute("entries", transactionService.profitAndLossentries(companyId, start, end));
				model.addAttribute("currencyName", currency);
			} catch (Exception e) {
				logger.info("PROFIT_AND_LOSS_TABLE: ",e);
			}
			return AccountsPageNameConstants.PROFIT_AND_LOSS_TABLE;
		}
	}
}
