package com.accounts.web.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.LedgerService;
import com.accounts.web.service.PaymentService;
import com.accounts.web.service.ReceiptService;
import com.accounts.web.vo.PaymentVo;
import com.accounts.web.vo.ReceiptVo;
import com.erp.web.constants.ErpRequestConstants;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate" ,"companies","companyName"})
public class ReceiptController {

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private ReceiptService receiptService;

	@Autowired
	private CompanyService companyService;

	private static int rows = 2;

	@Log
	private static Logger logger;

	/**
	 * method to reach receipt home page
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.RECEIPT_HOME, method = RequestMethod.GET)
	public String receiptHome(final Model model, @ModelAttribute("companyId") Long companyId,
			HttpServletRequest request, @ModelAttribute("currencyName") String currency,
			@ModelAttribute("companies") List<CompanyVo> companies, @ModelAttribute("companyName") String companyName,
			@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate) {
		model.addAttribute("currencyName", currency);
		model.addAttribute("companyStart", startDate);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			model.addAttribute(MessageConstants.SUCCESS, request.getParameter(MessageConstants.SUCCESS));
			try {
				model.addAttribute("title", companyName + " ( " + startDate + " to " + endDate + " )");
				model.addAttribute("receipts", receiptService.findAllReceiptsForCompany(companyId));
				model.addAttribute("companies", companies);
			} catch (Exception e) {
				logger.info("RECEIPT_HOME: ", e);
				model.addAttribute(MessageConstants.SUCCESS, e.getMessage());
			}
			return AccountsPageNameConstants.RECEIPT_HOME;
		}
	}

	/**
	 * method to reach add receipt page
	 * 
	 * @param model
	 * @param companyId
	 * @param paymentVo
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.RECEIPT_ADD, method = RequestMethod.GET)
	public String receiptAddPage(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute ReceiptVo receiptVo, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("currencyName") String currencyName, @ModelAttribute("endDate") String endDate) {
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		model.addAttribute("currencyName", currencyName);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			try {
				rows = 2;
				receiptVo.setEdit(false);
				receiptVo.setDate(DateFormatter.convertDateToString(new Date()));
				model.addAttribute("receiptVo", receiptVo);
				model.addAttribute("ledgers", ledgerService.receiptLedgers(companyId));
			} catch (Exception e) {
				logger.info("RECEIPT_ADD: ", e);
			}
			return AccountsPageNameConstants.RECEIPT_ADD;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.RECEIPT_ENTRY_TABLE_ROW, method = RequestMethod.GET)
	public String receiptEntryTableRow(final Model model, @ModelAttribute("companyId") Long companyId,
			@RequestParam(value = "rows", required = true) Integer count,
			@RequestParam(value = "credit", required = true) BigDecimal credit,
			@RequestParam(value = "debit", required = true) BigDecimal debit) {
		try {
			model.addAttribute("count", rows);
			rows++;
			BigDecimal rowCredit = new BigDecimal("0.00");
			BigDecimal rowDebit = new BigDecimal("0.00");
			if (credit.compareTo(debit) > 0) {
				rowDebit = credit.subtract(debit);
				debit = debit.add(rowDebit);
			} else {
				rowCredit = debit.subtract(credit);
				credit = credit.add(rowCredit);
			}
			model.addAttribute("rowCredit", rowCredit);
			model.addAttribute("rowDebit", rowDebit);
			model.addAttribute("debit", debit);
			model.addAttribute("credit", credit);
			model.addAttribute("ledgers", ledgerService.receiptLedgers(companyId));
		} catch (Exception exception) {
			logger.info("RECEIPT_ENTRY_TABLE_ROW: ", exception);
		}
		return AccountsPageNameConstants.RECEIPT_ENTRY_TABLE_ROW;
	}

	/**
	 * method to save receipt
	 * 
	 * @param model
	 * @param companyId
	 * @param paymentVo
	 * @return
	 */
	/*
	 * @RequestMapping(value = AccountsRequestConstants.RECEIPT_SAVE, method =
	 * RequestMethod.POST) public String receiptSAve(final Model model,
	 * 
	 * @ModelAttribute("companyId") Long companyId,
	 * 
	 * @ModelAttribute ReceiptVo receiptVo, BindingResult bindingResult) { if
	 * (companyId == 0) return "redirect:" +
	 * ErpRequestConstants.HOME+"?"+MessageConstants
	 * .ERROR+"=You haven't created any company yet"; else { Long id = null; try
	 * { receiptVo.setCompanyId(companyId); id =
	 * receiptService.saveOrUpdateReceipt(receiptVo);
	 * model.addAttribute(MessageConstants.SUCCESS,
	 * "Receipt Saved Successfully"); } catch (Exception e) {
	 * e.printStackTrace(); model.addAttribute(MessageConstants.SUCCESS,
	 * e.getMessage()); } if (id != null) return "redirect:" +
	 * AccountsRequestConstants.RECEIPT_EDIT + "?id=" + id; else return
	 * AccountsPageNameConstants.RECEIPT_ADD; } }
	 */

	@RequestMapping(value = AccountsRequestConstants.RECEIPT_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveReceipt(final Model model, @RequestBody ReceiptVo receiptVo,
			@ModelAttribute("companyId") Long companyId) {
		Long id = receiptVo.getReceiptId();
		receiptVo.setCompanyId(companyId);
		try {
			id = receiptService.saveOrUpdateReceipt(receiptVo);
			if (id != null) {
				model.addAttribute(MessageConstants.SUCCESS, "Action completed successfully");
			}
		} catch (Exception e) {
			model.addAttribute(MessageConstants.ERROR, MessageConstants.WRONG);
			logger.info("RECEIPT_SAVE: ", e);
		}
		return id;
	}

	/**
	 * method to edit receipt
	 * 
	 * @param model
	 * @param companyId
	 * @param id
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.RECEIPT_EDIT, method = RequestMethod.GET)
	public String receiptEdit(final Model model, @ModelAttribute("companyId") Long companyId,
			@ModelAttribute ReceiptVo receiptVo, @RequestParam(value = "id", required = true) Long id,
			HttpServletRequest request, @ModelAttribute("startDate") String startDate,
			@ModelAttribute("endDate") String endDate, @ModelAttribute("currencyName") String currencyName) {
		model.addAttribute("startDate", startDate);
		model.addAttribute("currencyName", currencyName);
		model.addAttribute("endDate", endDate);
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME + "?" + MessageConstants.ERROR
					+ "=You haven't created any company yet";
		else {
			model.addAttribute(MessageConstants.SUCCESS, request.getParameter(MessageConstants.SUCCESS));
			try {
				receiptVo = receiptService.findReceiptById(id);
				receiptVo.setEdit(true);
				rows = receiptVo.getEntries().size() + 1;
				model.addAttribute("receiptVo", receiptVo);
				model.addAttribute("ledgers", ledgerService.receiptLedgers(companyId));
			} catch (Exception e) {
				logger.info("RECEIPT_EDIT: ", e);
			}
			return AccountsPageNameConstants.RECEIPT_ADD;
		}
	}

	@RequestMapping(value = AccountsRequestConstants.RECEIPT_ENTRIES_TABLE, method = RequestMethod.GET)
	public String receiptEntriesTable(final Model model, @RequestParam("id") Long id,
			@ModelAttribute("companyId") Long companyId) {
		try {
			ReceiptVo receiptVo = receiptService.findReceiptById(id);
			receiptVo.setEdit(true);
			rows = receiptVo.getEntries().size() + 1;
			model.addAttribute("receiptVo", receiptVo);
			model.addAttribute("ledgers", ledgerService.receiptLedgers(companyId));
		} catch (Exception e) {
			logger.info("RECEIPT_ENTRIES_TABLE: ", e);
		}
		return AccountsPageNameConstants.RECEIPT_ENTRIES_TABLE;
	}

	/**
	 * method to list receipt
	 * 
	 * @param model
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.RECEIPT_LIST, method = RequestMethod.GET)
	public String receiptList(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("receipts", receiptService.findAllReceiptsForCompany(companyId));
		} catch (Exception e) {
			logger.info("RECEIPT_LIST: ", e);
			model.addAttribute(MessageConstants.SUCCESS, e.getMessage());
		}
		return AccountsPageNameConstants.RECEIPT_LIST;
	}

	/**
	 * method to delete receipt
	 * 
	 * @param id
	 */
	@RequestMapping(value = AccountsRequestConstants.RECEIPT_DELETE, method = RequestMethod.GET)
	public @ResponseBody ModelAndView receiptDelete(@RequestParam(value = "id", required = true) Long id) {
		try {
			receiptService.deleteReceipt(id);
		} catch (Exception e) {
			logger.info("RECEIPT_DELETE: ", e);
		}
		ModelAndView modelAndView = new ModelAndView(new RedirectView(AccountsRequestConstants.RECEIPT_LIST));
		return modelAndView;
	}

	@RequestMapping(value = AccountsRequestConstants.RECEIPT_LIST_FILTER, method = RequestMethod.GET)
	public String receiptListFiltered(final Model model,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) {
		try {
			model.addAttribute("receipts",
					receiptService.findAllReceiptsForCompanyWithinDate(companyId, startDate, endDate));
		} catch (Exception e) {
			logger.info("RECEIPT_LIST_FILTER: ", e);
			model.addAttribute(MessageConstants.SUCCESS, e.getMessage());
		}
		return AccountsPageNameConstants.RECEIPT_LIST;
	}

	@RequestMapping(value = AccountsRequestConstants.RECEIPT_ENTRIES_ROW_COUNT, method = RequestMethod.GET)
	public @ResponseBody Long getReceiptRowCount() {
		return (long) rows;
	}
}
