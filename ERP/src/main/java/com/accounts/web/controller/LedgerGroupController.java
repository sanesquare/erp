
package com.accounts.web.controller;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.LedgerAccountTypeService;
import com.accounts.web.service.LedgerGroupService;
import com.accounts.web.vo.LedgerGroupVo;
import com.erp.web.constants.ErpRequestConstants;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.logger.Log;
import com.hrms.web.responses.JsonResponse;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 10-August-2015
 *
 */
@Controller
@SessionAttributes({ "companyId" })
public class LedgerGroupController {

	@Autowired
	private LedgerGroupService groupService;

	@Autowired
	private LedgerAccountTypeService accountTypeService;
	
	@Log
	private static Logger logger;

	/**
	 * Method to list all ledger groups
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.LEDGER_GROUP_LIST, method = RequestMethod.GET)
	public String ledgerGroupList(final Model model, @ModelAttribute("companyId") Long companyId) {
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME+"?"+MessageConstants.ERROR+"=You haven't created any company yet";
		else {
			try {

				model.addAttribute("accountTypes", accountTypeService.findAllAccountTypes());
				model.addAttribute("groups", groupService.findAllLedgerGroupsForCompany(companyId));
			} catch (Exception e) {
				logger.info("LEDGER_GROUP_LIST: ",e);
			}
			return AccountsPageNameConstants.LEDGER_GROUP_LIST;
		}
	}

	/**
	 * method to save ledger group
	 * 
	 * @param groupVo
	 * @return
	 */
	@RequestMapping(value = AccountsRequestConstants.SAVE_LEDGER_GROUP, method = RequestMethod.POST)
	public @ResponseBody String saveLedgerGroup(@RequestBody LedgerGroupVo groupVo,
			@ModelAttribute("companyId") Long companyId) {
		try {
			groupVo.setCompanyId(companyId);
			groupService.saveOrUpdateLedgerGroup(groupVo);
		} catch (Exception e) {
			logger.info("SAVE_LEDGER_GROUP: ",e);
			return e.getMessage();
		} 
		return "ok";
	}

	@RequestMapping(value = AccountsRequestConstants.LEDGER_GROUP_TABLE, method = RequestMethod.GET)
	public String ledgerGroupTable(final Model model, @ModelAttribute("companyId") Long companyId) {
		try {
			model.addAttribute("groups", groupService.findAllLedgerGroupsForCompany(companyId));
		} catch (Exception e) {
			logger.info("LEDGER_GROUP_TABLE: ",e);
		}
		return AccountsPageNameConstants.LEDGER_GROUP_TABLE;
	}

	@RequestMapping(value = AccountsRequestConstants.LEDGER_GROUP_LIST_ITEMS, method = RequestMethod.GET)
	public @ResponseBody JsonResponse ledgerGroupListItems(final Model model, @ModelAttribute("companyId") Long companyId) {
		JsonResponse jsonResponse = new JsonResponse();
		try {
			jsonResponse.setResult(groupService.findAllLedgerGroupsForCompany(companyId));
		} catch (Exception e) {
			logger.info("LEDGER_GROUP_LIST_ITEMS: ",e);
		}
		return jsonResponse;
	}

	@RequestMapping(value = AccountsRequestConstants.DELETE_LEDGER_GROUP, method = RequestMethod.GET)
	public @ResponseBody JsonResponse deleteLedgerGroup(final Model model,
			@RequestParam(value = "groupId", required = true) Long id) {
		JsonResponse jsonResponse = new JsonResponse();
		String message = null;
		try {
			groupService.deleteLedgerGroup(id);
			message = "Ledger Group Deleted Successfully";
		} catch (Exception e) {
			logger.info(e.getMessage());
			message = "Cannot delete ledger group";
		}
		jsonResponse.setStatus(message);
		return jsonResponse;
	}
}
