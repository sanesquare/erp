package com.accounts.web.controller;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.constants.AccountsPageNameConstants;
import com.accounts.web.constants.AccountsRequestConstants;
import com.erp.web.constants.ErpRequestConstants;
import com.hrms.web.constants.MessageConstants;
import com.hrms.web.logger.Log;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
@Controller
@SessionAttributes({"companyId"})
public class AccountsController {

	@Log
	private static Logger logger;
	
	@RequestMapping(value = AccountsRequestConstants.HOME, method = RequestMethod.GET)
	public String accountsHome(final Model model, @ModelAttribute("companyId") Long companyId) {
		if (companyId == 0)
			return "redirect:" + ErpRequestConstants.HOME+"?"+MessageConstants.ERROR+"=You haven't created any company yet";
		else {

			return AccountsPageNameConstants.HOME_PAGE;
		}
	}
}
