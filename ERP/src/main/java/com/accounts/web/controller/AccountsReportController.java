package com.accounts.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.accounts.web.constants.AccountsRequestConstants;
import com.accounts.web.service.AccountsReportService;
import com.accounts.web.service.ContraService;
import com.accounts.web.service.JournalService;
import com.accounts.web.service.PaymentService;
import com.accounts.web.service.ReceiptService;
import com.accounts.web.service.TransactionService;
import com.accounts.web.vo.BalanceSheetMainPdfVo;
import com.accounts.web.vo.ContraVo;
import com.accounts.web.vo.JournalEntriesPdfVo;
import com.accounts.web.vo.JournalHistoryPdfVo;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.LedgerVo;
import com.accounts.web.vo.PaymentVo;
import com.accounts.web.vo.ProfitAndLossMainPdfVo;
import com.accounts.web.vo.ProfitAndLossVo;
import com.accounts.web.vo.ReceiptVo;
import com.accounts.web.vo.TrialBalanceLedgerGroupVo;
import com.accounts.web.vo.TrialBalancePdfVo;
import com.accounts.web.vo.BalanceSheetVo;
import com.erp.web.util.DateUtil;
import com.erp.web.constants.DateFormatConstants;
import com.erp.web.constants.ErpConstants;
import com.erp.web.service.CompanyService;
import com.erp.web.vo.CompanyVo;
import com.hrms.web.logger.Log;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 27-August-2015
 *
 */
@Controller
@SessionAttributes({ "companyId", "currencyName", "startDate", "endDate" })
public class AccountsReportController {

	@Autowired
	private AccountsReportService reportService;

	@Autowired
	private ServletContext context;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private JournalService journalService;

	@Autowired
	private ReceiptService receiptService;

	@Autowired
	private ContraService contraService;

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private CompanyService companyService;

	@Log
	private static Logger logger;

	/**
	 * Method to download pdf report for payments history
	 * 
	 * @param response
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.PAYMENT_REPORT, method = RequestMethod.GET)
	@ResponseBody
	public void getPaymentHistoryPdf(HttpServletResponse response,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) throws JRException, IOException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
			List<PaymentVo> list = new ArrayList<PaymentVo>();
			if (startDate != "" && endDate != "") {
				list = paymentService.findAllPaymentsForCompanyWithinDate(companyId, startDate, endDate);
				params.put("operationType", "Payments History  " + startDate + " to " + endDate);
			} else {
				list = paymentService.findAllPaymentsForCompany(companyId);
				params.put("operationType", "Payments History");
			}

			List<JournalHistoryPdfVo> pdfVos = new ArrayList<JournalHistoryPdfVo>();
			pdfVos.addAll(reportService.createPaymentHistoryReport(list));

			JRDataSource dataSource = new JRBeanCollectionDataSource(pdfVos);
			InputStream streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/journalListFormat.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-disposition", "inline; filename=PaymentsHistory.pdf");
			final OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		} catch (Exception ex) {
			logger.info("PAYMENT_REPORT: ", ex);
		}
	}

	/**
	 * Method to download receipt history pdf
	 * 
	 * @param response
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.RECEIPT_REPORT, method = RequestMethod.GET)
	@ResponseBody
	public void getReceiptHistoryPdf(HttpServletResponse response,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) throws JRException, IOException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
			List<ReceiptVo> list = new ArrayList<ReceiptVo>();
			if (startDate != "" && endDate != "") {
				list = receiptService.findAllReceiptsForCompanyWithinDate(companyId, startDate, endDate);
				params.put("operationType", "Receipts History  " + startDate + " to " + endDate);
			} else {
				list = receiptService.findAllReceiptsForCompany(companyId);
				params.put("operationType", "Receipts History");
			}
			List<JournalHistoryPdfVo> pdfVos = new ArrayList<JournalHistoryPdfVo>();
			pdfVos.addAll(reportService.createReceiptHistoryReport(list));

			JRDataSource dataSource = new JRBeanCollectionDataSource(pdfVos);
			InputStream streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/journalListFormat.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-disposition", "inline; filename=ReceiptsHistory.pdf");
			final OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		} catch (Exception ex) {
			logger.info("RECEIPT_REPORT: ", ex);
		}
	}

	/**
	 * Method to download contra history pdf
	 * 
	 * @param response
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.CONTRA_REPORT, method = RequestMethod.GET)
	@ResponseBody
	public void getContraHistoryPdf(HttpServletResponse response,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) throws JRException, IOException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();

			params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
			List<ContraVo> list = new ArrayList<ContraVo>();
			if (startDate != "" && endDate != "") {
				list = contraService.findAllContrasForCompanyWithinDate(companyId, startDate, endDate);
				params.put("operationType", "Contra History  " + startDate + " to " + endDate);
			} else {
				list = contraService.findAllContrasForCompany(companyId);
				params.put("operationType", "Contra History");
			}
			List<JournalHistoryPdfVo> pdfVos = new ArrayList<JournalHistoryPdfVo>();
			pdfVos.addAll(reportService.createContraHistoryReport(list));

			JRDataSource dataSource = new JRBeanCollectionDataSource(pdfVos);
			InputStream streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/journalListFormat.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-disposition", "inline; filename=ContraHistory.pdf");
			final OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		} catch (Exception ex) {
			logger.info("CONTRA_REPORT: ", ex);
		}
	}

	/**
	 * Method to download trial balance pdf
	 * 
	 * @param response
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.TRIAL_BALANCE_PDF, method = RequestMethod.GET)
	@ResponseBody
	public void getTrialBalancePdf(HttpServletResponse response, @ModelAttribute("currencyName") String currency,
			@ModelAttribute("endDate") String end, @RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate, @RequestParam("DetAILed") Boolean detail,
			@RequestParam("opn") Boolean opn, @RequestParam("cls") Boolean cls) throws JRException, IOException {
		try {

			CompanyVo companyVo = companyService.findCompanyById(companyId);

			List<TrialBalancePdfVo> pdfVos = new ArrayList<TrialBalancePdfVo>();
			Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> map = new HashMap<TrialBalanceLedgerGroupVo, List<LedgerVo>>();
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
			if (startDate != "" && endDate != "") {
				// params.put("operationType", companyVo.getCompanyName());
				params.put("operationType", companyVo.getParent());
				params.put("line2", " Trial Balance");
				params.put(
						"line3",
						DateUtil.convertStringToLongDateString(DateFormatConstants.MMM_DD_YYYY,
								DateFormatter.convertStringToDate(endDate)));
				map = transactionService.trialBalanceCalculations(companyId, startDate, endDate);
				params.put("OPN_CURRENCY", "Opening Balance ( " + currency + " )");
				params.put("DBT_CURRENCY", "Debit ( " + currency + " )");
				params.put("CDT_CURRENCY", "Credit ( " + currency + " )");
				params.put("CLS_CURRENCY", "Closing Balance ( " + currency + " )");
			} else {
				// params.put("operationType", companyVo.getCompanyName());
				params.put("operationType", companyVo.getParent());
				params.put("line2", " Trial Balance");
				params.put(
						"line3",
						DateUtil.convertStringToLongDateString(DateFormatConstants.MMM_DD_YYYY,
								DateFormatter.convertStringToDate(end)));
				map = transactionService.trialBalanceCalculations(companyId);
				params.put("OPN_CURRENCY", "Opening Balance ( " + currency + " )");
				params.put("DBT_CURRENCY", "Debit ( " + currency + " )");
				params.put("CDT_CURRENCY", "Credit ( " + currency + " )");
				params.put("CLS_CURRENCY", "Closing Balance ( " + currency + " )");
			}
			pdfVos = reportService.createTrialBalancePdf(params,map, detail);
			if (pdfVos.size() > 0) {
				TrialBalancePdfVo vo = pdfVos.get(pdfVos.size() - 1);
				params.put("grandOpening", vo.getOpeningBal());
				params.put("grandDebit", vo.getDebit());
				params.put("grandCredit", vo.getCredit());
				params.put("grandClose", vo.getClosingBal());
				pdfVos.remove(vo);
			}
			JRDataSource dataSource = new JRBeanCollectionDataSource(pdfVos);
			InputStream streamIo = null;
			if (opn == true && cls == true)
				streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/trialBalance.jrxml");
			else if (opn == true && cls == false)
				streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/TrialBalanceOpenning.jrxml");
			else if (opn == false && cls == true)
				streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/TrialBalanceClosing.jrxml");
			else
				streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/TrialBalanceCondensed.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-disposition", "inline; filename=TrialBalance.pdf");
			final OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		} catch (Exception ex) {
			logger.info("TRIAL_BALANCE_PDF: ", ex);
		}
	}

	/**
	 * Method to download profit and loss pdf
	 * 
	 * @param response
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.PROFIT_AND_LOSS_PDF, method = RequestMethod.GET)
	@ResponseBody
	public void getProfitAndLossPdf(HttpServletResponse response, @ModelAttribute("currencyName") String currency,
			@ModelAttribute("endDate") String end, @RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate, @RequestParam("DetAILed") Boolean detail)
			throws JRException, IOException {
		try {
			CompanyVo companyVo = companyService.findCompanyById(companyId);
			ProfitAndLossMainPdfVo mainPdfVo = null;
			ProfitAndLossVo vo = null;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
			if (startDate != "" && endDate != "") {
				vo = transactionService.profitAndLossentries(companyId, startDate, endDate);
				// params.put("operationType",companyVo.getCompanyName()+"'s");
				params.put("operationType", companyVo.getParent() + "'s");
				params.put("line2", "Trading and Profit & Loss Account");
				params.put(
						"line3",
						"for the Year ended "
								+ DateUtil.convertDateToDetailedString(DateFormatter.convertStringToDate(endDate)));
				params.put("CURRENCY", "Amount ( " + currency + " )");
			} else {
				vo = transactionService.profitAndLossentries(companyId);
				// params.put("operationType",companyVo.getCompanyName()+"'s");
				params.put("operationType", companyVo.getParent() + "'s");
				params.put("line2", "Trading and Profit & Loss Account");
				params.put(
						"line3",
						"for the Year ended "
								+ DateUtil.convertDateToDetailedString(DateFormatter.convertStringToDate(end)));
				params.put("CURRENCY", "Amount ( " + currency + " )");
			}
			mainPdfVo = reportService.createProfitAndLossPdf(vo, detail);

			if (!mainPdfVo.getDirectExpenses().isEmpty()) {
				JRDataSource directExpenseData = new JRBeanCollectionDataSource(mainPdfVo.getDirectExpenses());
				params.put("EXPENSE_DATASOURCE", directExpenseData);
			}
			if (!mainPdfVo.getDirectIncomes().isEmpty()) {
				JRDataSource directIncomeData = new JRBeanCollectionDataSource(mainPdfVo.getDirectIncomes());
				params.put("INCOME_DATASOURCE", directIncomeData);
			}
			if (!mainPdfVo.getIndirectExpenses().isEmpty()) {
				JRDataSource indirectExpenseData = new JRBeanCollectionDataSource(mainPdfVo.getIndirectExpenses());
				params.put("INDIRECT_EXPENSE_DATASOURCE", indirectExpenseData);
			}
			if (!mainPdfVo.getIndirectIncomes().isEmpty()) {
				JRDataSource indirectIncomeData = new JRBeanCollectionDataSource(mainPdfVo.getIndirectIncomes());
				params.put("INDIRECT_INCOME_DATASOURCE", indirectIncomeData);
			}

			InputStream streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/profitAndLoss.jrxml");
			InputStream directIncomeStream = context
					.getResourceAsStream("/WEB-INF/jrxml/Accounts/IncomeSubReport.jrxml");
			InputStream directExpenseStream = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/Expense.jrxml");
			InputStream indirectIncomeStream = context
					.getResourceAsStream("/WEB-INF/jrxml/Accounts/IndirectIncome.jrxml");
			InputStream indirectExpenseStream = context
					.getResourceAsStream("/WEB-INF/jrxml/Accounts/IndirectExpense.jrxml");

			JasperReport directIncomeReport = JasperCompileManager.compileReport(directIncomeStream);
			JasperReport indirectIncomeReport = JasperCompileManager.compileReport(indirectIncomeStream);
			JasperReport directExpenseReport = JasperCompileManager.compileReport(directExpenseStream);
			JasperReport indirectExpenseReport = JasperCompileManager.compileReport(indirectExpenseStream);
			JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);

			params.put("INCOME_REPORT", directIncomeReport);
			params.put("EXPENSE_REPORT", directExpenseReport);
			params.put("INDIRECT_EXPENSE_REPORT", indirectExpenseReport);
			params.put("INDIRECT_INCOME_REPORT", indirectIncomeReport);

			params.put("GROSS_PROFIT", vo.getDirectExpenseGrossProfit());
			params.put("GROSS_LOSS", vo.getDirectIncomeGrossLoss());
			params.put("NET_PROFIT", vo.getIndirectExpenseNetProfit());
			params.put("NET_LOSS", vo.getIndirectIncomeNetLoss());
			params.put("TOTAL_1", vo.getDirectExpenseTotal());
			params.put("TOTAL_2", vo.getInddirectExpenseTotal());

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
			response.setContentType("application/x-pdf");
			response.setHeader("Content-disposition", "inline; filename=ProfitAndLoss.pdf");
			final OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		} catch (Exception ex) {
			logger.info("PROFIT_AND_LOSS_PDF: ", ex);
		}
	}

	/***
	 * Method to download balancesheet pdf
	 * 
	 * @param response
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.BALANCESHEET_PDF, method = RequestMethod.GET)
	@ResponseBody
	public void getBalancesheetPdf(HttpServletResponse response, @ModelAttribute("currencyName") String currency,
			@ModelAttribute("endDate") String end, @RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate, @RequestParam("DetAILed") Boolean detail)
			throws JRException, IOException {
		try {
			CompanyVo companyVo = companyService.findCompanyById(companyId);
			BalanceSheetMainPdfVo mainPdfVo = null;
			BalanceSheetVo vo = null;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
			if (startDate != "" && endDate != "") {
				vo = transactionService.balanceSheetCalculation(companyId, startDate, endDate);
				// params.put("operationType", "Balance Sheet  for " +
				// companyVo.getCompanyName());
				params.put("operationType", "Balance Sheet  for " + companyVo.getParent());
				params.put("line2",
						"as on " + DateUtil.convertDateToDetailedString(DateFormatter.convertStringToDate(endDate)));
				params.put("CURRENCY", "Amount ( " + currency + " )");
			} else {
				vo = transactionService.balanceSheetCalculation(companyId);
				// params.put("operationType", "Balance Sheet  for " +
				// companyVo.getCompanyName());
				params.put("operationType", "Balance Sheet  for " + companyVo.getParent());
				params.put("line2",
						"as on " + DateUtil.convertDateToDetailedString(DateFormatter.convertStringToDate(end)));
				params.put("CURRENCY", "Amount ( " + currency + " )");
			}
			mainPdfVo = reportService.createBalancesheetPdf(vo, detail);

			if (!mainPdfVo.getCurrentLiabilityList().isEmpty()) {
				JRDataSource currentLiabilityData = new JRBeanCollectionDataSource(mainPdfVo.getCurrentLiabilityList());
				params.put("LIABILITY_DATASOURCE", currentLiabilityData);
			}
			if (!mainPdfVo.getEquityList().isEmpty()) {
				JRDataSource equityData = new JRBeanCollectionDataSource(mainPdfVo.getEquityList());
				params.put("EQUITY_DATASOURCE", equityData);
			}
			if (!mainPdfVo.getNonCurrentLiabilityList().isEmpty()) {
				JRDataSource nonCurrentLiabilityData = new JRBeanCollectionDataSource(
						mainPdfVo.getNonCurrentLiabilityList());
				params.put("NON_CURRENT_LIABILITY_DATASOURCE", nonCurrentLiabilityData);
			}
			if (!mainPdfVo.getCurrentAssetList().isEmpty()) {
				JRDataSource currentAssetData = new JRBeanCollectionDataSource(mainPdfVo.getCurrentAssetList());
				params.put("ASSET_DATASOURCE", currentAssetData);
			}
			if (!mainPdfVo.getNonCurrentAssetList().isEmpty()) {
				JRDataSource nonCurrentAssetData = new JRBeanCollectionDataSource(mainPdfVo.getNonCurrentAssetList());
				params.put("NON_CURRENT_ASSET_DATASOURCE", nonCurrentAssetData);
			}

			InputStream streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/BalanceSheetMain.jrxml");
			InputStream currentLiabilityStream = context
					.getResourceAsStream("/WEB-INF/jrxml/Accounts/BalancesheetSubReport.jrxml");

			JasperReport currentLiabilityReport = JasperCompileManager.compileReport(currentLiabilityStream);
			JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);

			params.put("LIABILITY_REPORT", currentLiabilityReport);
			params.put("ASSETS_REPORT", currentLiabilityReport);
			params.put("NON_CURRENT_LIABILITY_REPORT", currentLiabilityReport);
			params.put("NON_CURRENT_ASSET_REPORT", currentLiabilityReport);

			params.put("TOTAL_1", vo.getTotalLiability());
			params.put("TOTAL_2", vo.getTotalAsset());

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
			response.setContentType("application/x-pdf");
			response.setHeader("Content-disposition", "inline; filename=Balancesheet.pdf");
			final OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		} catch (Exception ex) {
			logger.info("BALANCESHEET_PDF: ", ex);
		}
	}

	/**
	 * Method to get pdf of individual payment
	 * 
	 * @param response
	 * @param id
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.PAYMENT_PDF, method = RequestMethod.GET)
	@ResponseBody
	public void getPaymentPdf(HttpServletResponse response, @RequestParam("id") Long id) throws JRException,
			IOException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
			PaymentVo payment = paymentService.findPaymentById(id);
			if (payment != null) {
				params.put("operationType", "Payment Details  ");
				params.put("VOUCHER_NO", payment.getVoucherNumber());
				params.put("FROM", payment.getFromLedger());
				params.put("TO", payment.getToLedger());
				params.put("DATE", payment.getDate());
				params.put("AMOUNT", payment.getAmount());
				params.put("NARRATION", payment.getNarration());

				List<JournalEntriesPdfVo> pdfVos = reportService.createPaymentEntries(payment);
				JRDataSource dataSource = new JRBeanCollectionDataSource(pdfVos);

				InputStream streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/JournalReport.jrxml");
				JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
				response.setContentType("application/x-pdf");
				response.setHeader("Content-disposition", "inline; filename=PaymentDetails"
						+ payment.getVoucherNumber().trim() + ".pdf");
				final OutputStream outStream = response.getOutputStream();
				JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
			}

		} catch (Exception ex) {
			logger.info("PAYMENT_PDF: ", ex);
		}
	}

	/**
	 * Method to get individual receipt pdf
	 * 
	 * @param response
	 * @param id
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.RECEIPT_PDF, method = RequestMethod.GET)
	@ResponseBody
	public void getReceiptPdf(HttpServletResponse response, @RequestParam("id") Long id) throws JRException,
			IOException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
			ReceiptVo receipt = receiptService.findReceiptById(id);
			if (receipt != null) {
				params.put("operationType", "Receipt Details  ");
				params.put("VOUCHER_NO", receipt.getVoucherNumber());
				params.put("FROM", receipt.getFromLedger());
				params.put("TO", receipt.getToLedger());
				params.put("DATE", receipt.getDate());
				params.put("AMOUNT", receipt.getAmount());
				params.put("NARRATION", receipt.getNarration());

				List<JournalEntriesPdfVo> pdfVos = reportService.createReceiptEntries(receipt);
				JRDataSource dataSource = new JRBeanCollectionDataSource(pdfVos);

				InputStream streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/JournalReport.jrxml");
				JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
				response.setContentType("application/x-pdf");
				response.setHeader("Content-disposition", "inline; filename=ReceiptDetails"
						+ receipt.getVoucherNumber().trim() + ".pdf");
				final OutputStream outStream = response.getOutputStream();
				JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
			}

		} catch (Exception ex) {
			logger.info("RECEIPT_PDF: ", ex);
		}
	}

	/**
	 * Method to get individual contra pdf
	 * 
	 * @param response
	 * @param id
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.CONTRA_PDF, method = RequestMethod.GET)
	@ResponseBody
	public void getContraPdf(HttpServletResponse response, @RequestParam("id") Long id) throws JRException, IOException {
		try {
			try {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
				ContraVo contra = contraService.findContraById(id);
				if (contra != null) {
					params.put("operationType", "Contra Details  ");
					params.put("VOUCHER_NO", contra.getVoucherNumber());
					params.put("FROM", contra.getFromLedger());
					params.put("TO", contra.getToLedger());
					params.put("DATE", contra.getDate());
					params.put("AMOUNT", contra.getAmount());
					params.put("NARRATION", contra.getNarration());

					List<JournalEntriesPdfVo> pdfVos = reportService.createContraEntries(contra);
					JRDataSource dataSource = new JRBeanCollectionDataSource(pdfVos);

					InputStream streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/JournalReport.jrxml");
					JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);
					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
					response.setContentType("application/x-pdf");
					response.setHeader("Content-disposition", "inline; filename=ContraDetails"
							+ contra.getVoucherNumber().trim() + ".pdf");
					final OutputStream outStream = response.getOutputStream();
					JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
				}

			} catch (Exception ex) {
				logger.info("CONTRA_PDF: ", ex);
			}
		} catch (Exception ex) {
			logger.info("CONTRA_PDF: ", ex);
		}
	}

	/**
	 * Method to get journal history pdf
	 * 
	 * @param response
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.JOURNAL_HISTORY_PDF, method = RequestMethod.GET)
	@ResponseBody
	public void getJournalHistoryPdf(HttpServletResponse response,
			@RequestParam(value = "companyId", required = true) Long companyId,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) throws JRException, IOException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
			List<JournalVo> list = new ArrayList<JournalVo>();
			if (startDate != "" && endDate != "") {
				list = journalService.findAllJournalsForCompanyWithinDate(companyId, startDate, endDate);
				params.put("operationType", "Journals History  " + startDate + " to " + endDate);
			} else {
				list = journalService.findAllJournalsForCompany(companyId);
				params.put("operationType", "Journals History");
			}
			List<JournalHistoryPdfVo> pdfVos = new ArrayList<JournalHistoryPdfVo>();
			pdfVos.addAll(reportService.createJournalHistoryReport(list));

			JRDataSource dataSource = new JRBeanCollectionDataSource(pdfVos);
			InputStream streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/journalListFormat.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-disposition", "inline; filename=JournalHistory.pdf");
			final OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		} catch (Exception ex) {
			logger.info("JOURNAL_HISTORY_PDF: ", ex);
		}
	}

	/**
	 * Method to get pdf of individual journal
	 * 
	 * @param response
	 * @param id
	 * @throws JRException
	 * @throws IOException
	 */
	@RequestMapping(value = AccountsRequestConstants.JOURNAL_PDF, method = RequestMethod.GET)
	@ResponseBody
	public void getJournalPdf(HttpServletResponse response, @RequestParam("id") Long id) throws JRException,
			IOException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("LogoPath", context.getRealPath(ErpConstants.LOGO_PATH));
			JournalVo journal = journalService.findJournalById(id);
			if (journal != null) {
				params.put("operationType", "Journal Details  ");
				params.put("VOUCHER_NO", journal.getVoucherNumber());
				params.put("FROM", journal.getFromLedger());
				params.put("TO", journal.getToLedger());
				params.put("DATE", journal.getDate());
				params.put("AMOUNT", journal.getAmount());
				params.put("NARRATION", journal.getNarration());

				List<JournalEntriesPdfVo> pdfVos = reportService.createJournalEntries(journal);
				JRDataSource dataSource = new JRBeanCollectionDataSource(pdfVos);

				InputStream streamIo = context.getResourceAsStream("/WEB-INF/jrxml/Accounts/JournalReport.jrxml");
				JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
				response.setContentType("application/x-pdf");
				response.setHeader("Content-disposition", "inline; filename=JournalDetails"
						+ journal.getVoucherNumber().trim() + ".pdf");
				final OutputStream outStream = response.getOutputStream();
				JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
			}

		} catch (Exception ex) {
			logger.info("JOURNAL_PDF: ", ex);
		}
	}
}
