package com.accounts.web.constants;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface AccountsPageNameConstants {

	String HOME_PAGE ="accountsHome";
	String LEDGER_HOME="ledgersHome";
	String ADD_LEDGER ="addLedger";
	String LEDGER_LIST="ledgerList";
	String LEDGER_GROUP_LIST="ledgerGroupList";
	String LEDGER_GROUP_TABLE="ledgerGroupTable";
	
	String JOURNAL_HOME="journals";
	String ADD_JOURNAL="addJournals";
	String JOURNAL_ENTRY_TABLE_ROW="journalEntryTableRow";
	String JOURNAL_LIST="journalList";
	String JOURNAL_ENTRIES_TABLE="journalEntriesTable";
	
	String PAYMENT_HOME="payments";
	String PAYMENT_ADD="addPayment";
	String PAYMENT_LIST="paymentList";
	String PAYMENT_ENTRIES_TABLE="paymentEntriesTable";
	String PAYMENT_ENTRY_TABLE_ROW="paymentEntryTableRow";
	
	String RECEIPT_HOME="receipts";
	String RECEIPT_ADD="addReceipt";
	String RECEIPT_LIST="receiptList";
	String RECEIPT_ENTRIES_TABLE="receiptEntriesTable";
	String RECEIPT_ENTRY_TABLE_ROW="receiptEntryTableRow";
	
	String CONTRA_HOME="contras";
	String CONTRA_ADD="addContra";
	String CONTRA_LIST="contraList";
	String CONTRA_ENTRIES_TABLE="contraEntriesTable";
	String CONTRA_ENTRY_TABLE_ROW="contraEntryTableRow";
	
	String TRIAL_BALANCE="trialBalance";
	String TRIAL_BALANCE_TABLE="trialBalanceTable";
	
	String PROFIT_AND_LOSS="profitAndLoss";
	String PROFIT_AND_LOSS_TABLE="profitAndLossTable";
	
	String BALANCE_SHEET="balanceSheet";
	String BALANCE_SHEET_TABLE="balanceSheetTable";
	
	String SALARIES="salaries";
	String SALARY_LIST="salaryList";
	String VIEW_SALARY="viewSalary";
	String PAY_POP="paypop";
	
	String LEDGER_GROUP_VIEW_POP="ledgerGroupViewPop";
	String LEDGER_GROUP_VIEW_POP_TEMP="ledgerGroupViewPopTemp";
	String LEDGER_VIEW_POP="ledgerViewPop";
	String LEDGER_VIEW_POP_TEMP="ledgerViewPopTemp";
	String LEDGER_DETAILED_VIEW_POP="ledgerDetailedViewPop";
	String LEDGER_DETAILED_VIEW_POP_TEMP="ledgerDetailedViewPopTemp";
	
	String JOURNAL_POP="journalPop";
	String CONTRA_POP="contraPop";
	String RECEIPT_POP="receiptPop";
	String PAYMENT_POP="paymentPop";
}
