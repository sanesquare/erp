package com.accounts.web.constants;


/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public  interface AccountsConstants {

	public static final String JOURNAL_CODE="JRNL-";
	public static final String PAYMENT_CODE="PYMNT-";
	public static final String RECEIPT_CODE="RCPT-";
	public static final String CONTRA_CODE="CTR-";
	
	String BANK_ACCOUNT="Bank Account";
	String CASH="Cash";
	String CASH_AT_BANK="Cash At Bank";
	String CASH_IN_HAND="Cash In Hand";
	
	String DIRECT_INCOME="Direct Income";
	String DIRECT_EXPENSE="Direct Expense";
	String INDIRECT_INCOME="Indirect Income";
	String INDIRECT_EXPENSE="Indirect Expense";
	String CURRENT_ASSET="Current Asset";
	String NON_CURRENT_ASSET="Non-Current Asset";
	String FIXED_ASSET="Fixed Asset";
	String INTANGIBLE_ASSET="Intangible Asset";
	String CURRENT_LIABILITIES="Current Liability";
	String NON_CURRENT_LIABILITIES="Non-Current Liability";
	String EQUITY="Equity";
	String SALARY="Salary";
	String DUTIES_AND_TAXES="Duties and Taxes";
	String SUNDRY_CREDITOR="Sundry Creditor";
	
	String BILLS_PAYABLE_LEDGER="Bill Payable";
	String BILLS_RECIEVABLE_LEDGER="Bill Receivable";
	String PURCHASE_LEDGER="Purchase";
	String SALES_LEDGER="Sales";
	String TAX_RECEIVABLE_LEDGER="Tax Receivable";
	String TAX_PAYABLE_LEDGER="Tax Payable";
	String DISCOUNT_RECEIVED_LEDGER="Discount Received";
	String DISCOUNT_PAYABLE_LEDGER="Discount Allowed";
	String SALARY_LEDGER="Salary";
	String SALARY_PAYABLE_LEDGER="Salary Payable";
	String PURCHASE_RETURN_LEDGER="Purchase Return";
	String SALES_RETURN_LEDGER="Sales Return";
	
	
	String PAID_STATUS="Paid";
	
	String CASH_IN_BANK="Cash At Bank";
}
