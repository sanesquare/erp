package com.accounts.web.constants;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 30-July-2015
 *
 */
public interface AccountsRequestConstants {

	String HOME = "accountsHome.do";
	String LEDGER_HOME="ledgersHome.do";
	String ADD_LEDGER = "addLedger.do";
	String SAVE_LEDGER="saveLedger.do";
	String EDIT_LEDGER="editLedger.do";
	String DELETE_LEDGER="deleteLedger.do";
	String LEDGER_LIST="ledgerList.do";
	String SAVE_LEDGER_GROUP="saveLedgerGroup.do";
	String LEDGER_GROUP_LIST="ledgerGroupList.do";
	String LEDGER_GROUP_TABLE="ledgerGroupTable.do";
	String LEDGER_GROUP_LIST_ITEMS="ledgerGroupsListItems.do";
	String DELETE_LEDGER_GROUP="deleteLedgerGroup.do";
	
	String JOURNAL_HOME="journalHome.do";
	String ADD_JOURNAL="addJournal.do";
	String JOURNAL_ENTRY_TABLE_ROW="journalEntryTableRow.do";
	String SAVE_JOURNAL="saveJournal.do";
	String EDIT_JOURNAL="editJournal.do";
	String DELETE_JOURNAL="deleteJournal.do";
	String JOURNAL_LIST="journalList.do";
	String JOURNAL_LIST_FILTER="journalListFilter.do";
	String JOURNAL_ENTRIES_TABLE="journalEntriesTable.do";
	String JOURNAL_ENTRIES_ROW_COUNT="getJournalRowCount.do";
	
	String PAYMENT_HOME="paymentHome.do";
	String PAYMENT_ADD="addPayment.do";
	String PAYMENT_SAVE="savePayment.do";
	String PAYMENT_EDIT="editPayment.do";
	String PAYMENT_DELETE="deletePayment.do";
	String PAYMENT_LIST="paymentList.do";
	String PAYMENT_LIST_FILTER="paymentListFilter.do";
	String PAYMENT_ENTRY_TABLE_ROW="paymentEntryTableRow.do";
	String PAYMENT_ENTRIES_TABLE="paymentEntriesTable.do";
	String PAYMENT_ENTRIES_ROW_COUNT="getPaymentRowCount.do";
	
	String RECEIPT_HOME="receiptHome.do";
	String RECEIPT_ADD="addReceipt.do";
	String RECEIPT_SAVE="saveReceipt.do";
	String RECEIPT_EDIT="editReceipt.do";
	String RECEIPT_DELETE="deleteReceipt.do";
	String RECEIPT_LIST="receiptList.do";
	String RECEIPT_LIST_FILTER="receiptListFilter.do";
	String RECEIPT_ENTRY_TABLE_ROW="receiptEntryTableRow.do";
	String RECEIPT_ENTRIES_TABLE="receiptEntriesTable.do";
	String RECEIPT_ENTRIES_ROW_COUNT="getReceiptRowCount.do";
	
	String CONTRA_HOME="contraHome.do";
	String CONTRA_ADD="addContra.do";
	String CONTRA_SAVE="saveContra.do";
	String CONTRA_EDIT="editContra.do";
	String CONTRA_DELETE="deleteContra.do";
	String CONTRA_LIST="contraList.do";
	String CONTRA_LIST_FILTER="contraListFilter.do";
	String CONTRA_ENTRY_TABLE_ROW="contraEntryTableRow.do";
	String CONTRA_ENTRIES_TABLE="contraEntriesTable.do";
	String CONTRA_ENTRIES_ROW_COUNT="getContraRowCount.do";
	
	String TRIAL_BALANCE="trialBalance.do";
	String TRIAL_BALANCE_TABLE="trialBalanceTable.do";
	
	String PROFIT_AND_LOSS="profitAndLoss.do";
	String PROFIT_AND_LOSS_TABLE="profitAndLossTable.do";
	
	String BALANCE_SHEET="balanceSheet.do";
	String BALANCE_SHEET_TABLE="balanceSheetTable.do";
	
	String PAYMENT_REPORT="paymentHistoryPdf.do";
	
	String RECEIPT_REPORT="receiptHistoryPdf.do";
	
	String CONTRA_REPORT="contraHistoryReport.do";
	
	String TRIAL_BALANCE_PDF="trialBalancePdf.do";
	
	String PROFIT_AND_LOSS_PDF="profitAndLossPdf.do";
	
	String BALANCESHEET_PDF="balancesheetPdf.do";
	
	String PAYMENT_PDF="paymentPdf.do";
	
	String RECEIPT_PDF="receiptPdf.do";
	
	String CONTRA_PDF="contraPdf.do";
	
	String JOURNAL_HISTORY_PDF="journalHistoryReport.do";
	
	String JOURNAL_PDF="journalPdf.do";
	
	String SALARY_LIST="salaryAccounts.do";
	String SALARY_LIST_TEMPLATE="salaryList.do";
	String UPDATE_SALARY_STATUS="updateSalaryStatus.do";
	String VIEW_SALARY="viewSalary.do";
	String PAY_SALARY="paySalary.do";
	String FILTER_ACCOUNT_SALARY="filterAccountSlary.do";
	
	String LEDGER_GROUP_VIEW_POP="ledgerGroupViewPop.do";
	String LEDGER_VIEW_POP="ledgerViewPop.do";
	String LEDGER_DETAILED_VIEW_POP="ledgerDetailedViewPop.do";
	String LEDGER_VOUCHER_VIEW_POP="ledgerVoucherViewPop.do";
	String LEDGER_VIEW_BACK="ledgerViewBack.do";
}
