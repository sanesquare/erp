package com.accounts.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.hrms.web.entities.PaySalary;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.PurchaseReturn;
import com.sales.web.entities.Invoice;
import com.sales.web.entities.SalesReturn;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 01-August-2015
 *
 */

@Entity
@Table(name = "journals")
public class Journal implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3274475198465589175L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="amount")
	private BigDecimal amount;
	
	@Column(name="journal_date")
	private Date journalDate;
	
	@Column(name="narration" ,length = 1000)
	private String narration;
	
	@Column(name="is_archive")
	private Boolean isArchive;
	
	@Column(name="is_dependant")
	private Boolean isDependant;
	
	@Column(name="journal_number")
	private String journalNumber;
	
	@ManyToOne
	private Company company;
	
	@OneToMany(mappedBy="journal" , cascade=CascadeType.ALL , orphanRemoval=true)
	@OrderBy("id")
	private Set<JournalEntries> entries = new LinkedHashSet<JournalEntries>();

	@OneToOne
	private PaySalary paySalary;
	
	@OneToOne
	private PurchaseReturn purchaseReturn;
	
	@OneToOne
	private SalesReturn salesReturn;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@OneToOne
	private Bill bill;
	
	@OneToOne
	private Invoice invoice;
	
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the journalDate
	 */
	public Date getJournalDate() {
		return journalDate;
	}

	/**
	 * @param journalDate the journalDate to set
	 */
	public void setJournalDate(Date journalDate) {
		this.journalDate = journalDate;
	}

	/**
	 * @return the narration
	 */
	public String getNarration() {
		return narration;
	}

	/**
	 * @param narration the narration to set
	 */
	public void setNarration(String narration) {
		this.narration = narration;
	}

	/**
	 * @return the journalNumber
	 */
	public String getJournalNumber() {
		return journalNumber;
	}

	/**
	 * @param journalNumber the journalNumber to set
	 */
	public void setJournalNumber(String journalNumber) {
		this.journalNumber = journalNumber;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Set<JournalEntries> getEntries() {
		return entries;
	}

	public void setEntries(Set<JournalEntries> entries) {
		this.entries = entries;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Boolean getIsArchive() {
		return isArchive;
	}

	public void setIsArchive(Boolean isArchive) {
		this.isArchive = isArchive;
	}

	public Boolean getIsDependant() {
		return isDependant;
	}

	public void setIsDependant(Boolean isDependant) {
		this.isDependant = isDependant;
	}

	public PaySalary getPaySalary() {
		return paySalary;
	}

	public void setPaySalary(PaySalary paySalary) {
		this.paySalary = paySalary;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public PurchaseReturn getPurchaseReturn() {
		return purchaseReturn;
	}

	public void setPurchaseReturn(PurchaseReturn purchaseReturn) {
		this.purchaseReturn = purchaseReturn;
	}

	public SalesReturn getSalesReturn() {
		return salesReturn;
	}

	public void setSalesReturn(SalesReturn salesReturn) {
		this.salesReturn = salesReturn;
	}

	

	
}
