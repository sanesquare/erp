package com.accounts.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 01-August-2015
 *
 */

@Entity
@Table(name = "contra")
public class Contra implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4127794472800379292L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	
	@Column(name="amount")
	private BigDecimal amount;
	
	@Column(name="contra_date")
	private Date contraDate;

	@Column(name="narration" , length = 1000)
	private String narration;
	
	@Column(name="contra_number")
	private String contraNumber;
	
	@ManyToOne
	private Company company;
	
	@OneToMany(mappedBy="contra" , cascade=CascadeType.ALL , orphanRemoval=true)
	@OrderBy("id")
	private Set<ContraEntries> entries = new LinkedHashSet<ContraEntries>();
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the contraDate
	 */
	public Date getContraDate() {
		return contraDate;
	}

	/**
	 * @param contraDate the contraDate to set
	 */
	public void setContraDate(Date contraDate) {
		this.contraDate = contraDate;
	}

	/**
	 * @return the narration
	 */
	public String getNarration() {
		return narration;
	}

	/**
	 * @param narration the narration to set
	 */
	public void setNarration(String narration) {
		this.narration = narration;
	}

	/**
	 * @return the contraNumber
	 */
	public String getContraNumber() {
		return contraNumber;
	}

	/**
	 * @param contraNumber the contraNumber to set
	 */
	public void setContraNumber(String contraNumber) {
		this.contraNumber = contraNumber;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<ContraEntries> getEntries() {
		return entries;
	}

	public void setEntries(Set<ContraEntries> entries) {
		this.entries = entries;
	}
}
