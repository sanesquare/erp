package com.accounts.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.Charge;
import com.erp.web.entities.Company;
import com.erp.web.entities.VAT;
import com.erp.web.entities.VendorCreditMemoExpense;
import com.hrms.web.entities.Employee;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.Vendor;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 01-August-2015
 *
 */

@Entity
@Table(name = "ledger")
public class Ledger implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4549353131811609386L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private LedgerGroup ledgerGroup;

	@Column(name = "ledger_name")
	private String ledgerName;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "opening_balance")
	private BigDecimal openingBalance;

	@Column(name = "balance")
	private BigDecimal balance;

	@Column(name = "is_editable")
	private Boolean isEditable;

	@Column(name = "is_initial")
	private Boolean isInitial;

	@OneToMany(mappedBy = "ledger", orphanRemoval = true)
	private Set<VendorCreditMemoExpense> vendorCreditMemoExpenses;

	@OneToMany(mappedBy = "ledger", orphanRemoval = true)
	private Set<JournalEntries> journalDetails = new HashSet<JournalEntries>();

	@OneToMany(mappedBy = "ledger", orphanRemoval = true)
	private Set<PaymentEntries> paymentDetails = new HashSet<PaymentEntries>();

	@OneToMany(mappedBy = "ledger", orphanRemoval = true)
	private Set<ReceiptEntries> receiptDetails = new HashSet<ReceiptEntries>();

	@OneToMany(mappedBy = "ledger", orphanRemoval = true)
	private Set<ContraEntries> contraDetails = new HashSet<ContraEntries>();

	@ManyToOne
	private Company company;

	@OneToMany(mappedBy = "ledger", orphanRemoval = true)
	private Set<Transactions> transactions = new HashSet<Transactions>();

	@OneToOne
	private Employee employee;

	@OneToOne
	private VAT vat;

	@OneToOne
	private Vendor vendor;

	@OneToOne
	private Charge charge;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@OneToMany(mappedBy = "ledger")
	private Set<Bill> bills = new HashSet<Bill>();

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the ledgerGroup
	 */
	public LedgerGroup getLedgerGroup() {
		return ledgerGroup;
	}

	/**
	 * @param ledgerGroup
	 *            the ledgerGroup to set
	 */
	public void setLedgerGroup(LedgerGroup ledgerGroup) {
		this.ledgerGroup = ledgerGroup;
	}

	/**
	 * @return the ledgerName
	 */
	public String getLedgerName() {
		return ledgerName;
	}

	/**
	 * @param ledgerName
	 *            the ledgerName to set
	 */
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the openingBalance
	 */
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}

	/**
	 * @param openingBalance
	 *            the openingBalance to set
	 */
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}

	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	/**
	 * @return the isEditable
	 */
	public Boolean getIsEditable() {
		return isEditable;
	}

	/**
	 * @param isEditable
	 *            the isEditable to set
	 */
	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	/**
	 * @return the vendorCreditMemoExpenses
	 */
	public Set<VendorCreditMemoExpense> getVendorCreditMemoExpenses() {
		return vendorCreditMemoExpenses;
	}

	/**
	 * @param vendorCreditMemoExpenses
	 *            the vendorCreditMemoExpenses to set
	 */
	public void setVendorCreditMemoExpenses(Set<VendorCreditMemoExpense> vendorCreditMemoExpenses) {
		this.vendorCreditMemoExpenses = vendorCreditMemoExpenses;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getIsInitial() {
		return isInitial;
	}

	public void setIsInitial(Boolean isInitial) {
		this.isInitial = isInitial;
	}

	public Set<Transactions> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transactions> transactions) {
		this.transactions = transactions;
	}

	public Set<JournalEntries> getJournalDetails() {
		return journalDetails;
	}

	public void setJournalDetails(Set<JournalEntries> journalDetails) {
		this.journalDetails = journalDetails;
	}

	public Set<PaymentEntries> getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(Set<PaymentEntries> paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public Set<ReceiptEntries> getReceiptDetails() {
		return receiptDetails;
	}

	public void setReceiptDetails(Set<ReceiptEntries> receiptDetails) {
		this.receiptDetails = receiptDetails;
	}

	public Set<ContraEntries> getContraDetails() {
		return contraDetails;
	}

	public void setContraDetails(Set<ContraEntries> contraDetails) {
		this.contraDetails = contraDetails;
	}

	public VAT getVat() {
		return vat;
	}

	public void setVat(VAT vat) {
		this.vat = vat;
	}

	public Charge getCharge() {
		return charge;
	}

	public void setCharge(Charge charge) {
		this.charge = charge;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

}
