package com.accounts.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.hrms.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 20-August-2015
 *
 */

@Entity
@Table(name = "transactions")
public class Transactions implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3596771069185382518L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "date")
	private Date date;

	@Column(name = "plus_amouunt")
	private BigDecimal plusAmount = new BigDecimal(0);

	@Column(name = "minus_amount")
	private BigDecimal minusAmount = new BigDecimal(0);

	@ManyToOne(fetch = FetchType.LAZY)
	private Company company;

	@ManyToOne(fetch = FetchType.LAZY)
	private Ledger ledger;

	@OneToOne(fetch = FetchType.LAZY)
	private JournalEntries journalEntries;
	
	@OneToOne(fetch = FetchType.LAZY)
	private PaymentEntries paymentEntries;
	
	@OneToOne(fetch = FetchType.LAZY)
	private ReceiptEntries receiptEntries;
	
	@OneToOne(fetch = FetchType.LAZY)
	private ContraEntries contraEntries;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getPlusAmount() {
		return plusAmount;
	}

	public void setPlusAmount(BigDecimal plusAmount) {
		this.plusAmount = plusAmount;
	}

	public BigDecimal getMinusAmount() {
		return minusAmount;
	}

	public void setMinusAmount(BigDecimal minusAmount) {
		this.minusAmount = minusAmount;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JournalEntries getJournalEntries() {
		return journalEntries;
	}

	public void setJournalEntries(JournalEntries journalEntries) {
		this.journalEntries = journalEntries;
	}

	public PaymentEntries getPaymentEntries() {
		return paymentEntries;
	}

	public void setPaymentEntries(PaymentEntries paymentEntries) {
		this.paymentEntries = paymentEntries;
	}

	public ReceiptEntries getReceiptEntries() {
		return receiptEntries;
	}

	public void setReceiptEntries(ReceiptEntries receiptEntries) {
		this.receiptEntries = receiptEntries;
	}

	public ContraEntries getContraEntries() {
		return contraEntries;
	}

	public void setContraEntries(ContraEntries contraEntries) {
		this.contraEntries = contraEntries;
	}
}
