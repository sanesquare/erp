package com.accounts.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.erp.web.entities.Company;
import com.hrms.web.entities.PaySalary;
import com.hrms.web.entities.PaySalaryDailyWages;
import com.hrms.web.entities.PaySalaryEmployees;
import com.hrms.web.entities.PaySalaryHourlyWage;
import com.hrms.web.entities.User;
import com.hrms.web.interfaces.AuditEntity;
import com.purchase.web.entities.BillInstallments;
import com.sales.web.entities.InvoiceInstallments;
import com.sales.web.entities.SalesReturn;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 01-August-2015
 *
 */

@Entity
@Table(name = "payment")
public class Payment implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2325671891334270722L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "payment_date")
	private Date paymentDate;

	@Column(name = "narration", length = 1000)
	private String narration;

	@Column(name = "payment_number")
	private String paymentNumber;

	@ManyToOne
	private Company company;

	@Column(name = "is_archive")
	private Boolean isArchive;
	
	@Column(name = "is_dependant")
	private Boolean isDependant;

	@OneToMany(mappedBy = "payment", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("id")
	private Set<PaymentEntries> entries = new LinkedHashSet<PaymentEntries>();

	@OneToOne
	private SalesReturn salesReturn;
	
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updatedOn;

	@ManyToOne
	private User updatedBy;

	@ManyToOne
	private User createdBy;

	@OneToOne
	private BillInstallments billInstallments;

	@OneToOne
	private PaySalary paySalary;

	@OneToMany(mappedBy = "payment", cascade = CascadeType.ALL)
	private Set<PaySalaryDailyWages> paySalaryDailyWages = new HashSet<PaySalaryDailyWages>();

	@OneToMany(mappedBy = "payment", cascade = CascadeType.ALL)
	private Set<PaySalaryHourlyWage> paySalaryHourlyWages = new HashSet<PaySalaryHourlyWage>();

	@OneToMany(mappedBy = "payment", cascade = CascadeType.ALL)
	private Set<PaySalaryEmployees> paysalaryMonthlyWages = new HashSet<PaySalaryEmployees>();

	public Set<PaySalaryDailyWages> getPaySalaryDailyWages() {
		return paySalaryDailyWages;
	}

	public void setPaySalaryDailyWages(Set<PaySalaryDailyWages> paySalaryDailyWages) {
		this.paySalaryDailyWages = paySalaryDailyWages;
	}

	public Set<PaySalaryHourlyWage> getPaySalaryHourlyWages() {
		return paySalaryHourlyWages;
	}

	public void setPaySalaryHourlyWages(Set<PaySalaryHourlyWage> paySalaryHourlyWages) {
		this.paySalaryHourlyWages = paySalaryHourlyWages;
	}

	public Set<PaySalaryEmployees> getPaysalaryMonthlyWages() {
		return paysalaryMonthlyWages;
	}

	public void setPaysalaryMonthlyWages(Set<PaySalaryEmployees> paysalaryMonthlyWages) {
		this.paysalaryMonthlyWages = paysalaryMonthlyWages;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the paymentDate
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	/**
	 * @param paymentDate
	 *            the paymentDate to set
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the narration
	 */
	public String getNarration() {
		return narration;
	}

	/**
	 * @param narration
	 *            the narration to set
	 */
	public void setNarration(String narration) {
		this.narration = narration;
	}

	/**
	 * @return the paymentNumber
	 */
	public String getPaymentNumber() {
		return paymentNumber;
	}

	/**
	 * @param paymentNumber
	 *            the paymentNumber to set
	 */
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<PaymentEntries> getEntries() {
		return entries;
	}

	public void setEntries(Set<PaymentEntries> entries) {
		this.entries = entries;
	}

	public BillInstallments getBillInstallments() {
		return billInstallments;
	}

	public void setBillInstallments(BillInstallments billInstallments) {
		this.billInstallments = billInstallments;
	}

	public Boolean getIsArchive() {
		return isArchive;
	}

	public void setIsArchive(Boolean isArchive) {
		this.isArchive = isArchive;
	}

	public PaySalary getPaySalary() {
		return paySalary;
	}

	public void setPaySalary(PaySalary paySalary) {
		this.paySalary = paySalary;
	}

	public Boolean getIsDependant() {
		return isDependant;
	}

	public void setIsDependant(Boolean isDependant) {
		this.isDependant = isDependant;
	}

	public SalesReturn getSalesReturn() {
		return salesReturn;
	}

	public void setSalesReturn(SalesReturn salesReturn) {
		this.salesReturn = salesReturn;
	}

}
