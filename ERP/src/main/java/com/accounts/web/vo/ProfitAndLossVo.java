package com.accounts.web.vo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 22-August-2015
 *
 */
public class ProfitAndLossVo {
	
	private BigDecimal zero = new BigDecimal("0.00");

	public BigDecimal getZero() {
		return zero;
	}

	public void setZero(BigDecimal zero) {
		this.zero = zero;
	}

	private BigDecimal directExpenseGrossProfit = new BigDecimal("0.00");

	private BigDecimal directExpenseTotal = new BigDecimal("0.00");

	private BigDecimal indirectExpenseNetProfit = new BigDecimal("0.00");

	private BigDecimal inddirectExpenseTotal = new BigDecimal("0.00");

	private BigDecimal directIncomeGrossLoss = new BigDecimal("0.00");

	private BigDecimal directIncomeTotal = new BigDecimal("0.00");

	private BigDecimal indirectIncomeNetLoss = new BigDecimal("0.00");

	private BigDecimal indirectIncomeTotal = new BigDecimal("0.00");

	private BigDecimal directExpenseSum = new BigDecimal("0.00");

	private BigDecimal indirectExpenseSum = new BigDecimal("0.00");

	private BigDecimal directIncomeSum = new BigDecimal("0.00");

	private BigDecimal indirectIncomeSum = new BigDecimal("0.00");

	private String dateRange;

	private Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> directExpenses = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();

	private Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> indirectExpenses = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();

	private Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> directIncomes = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();

	private Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> indirectIncomes = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();

	public BigDecimal getDirectExpenseGrossProfit() {
		return directExpenseGrossProfit;
	}

	public void setDirectExpenseGrossProfit(BigDecimal directExpenseGrossProfit) {
		this.directExpenseGrossProfit = directExpenseGrossProfit;
	}

	public BigDecimal getDirectExpenseTotal() {
		return directExpenseTotal;
	}

	public void setDirectExpenseTotal(BigDecimal directExpenseTotal) {
		this.directExpenseTotal = directExpenseTotal;
	}

	public BigDecimal getIndirectExpenseNetProfit() {
		return indirectExpenseNetProfit;
	}

	public void setIndirectExpenseNetProfit(BigDecimal indirectExpenseNetProfit) {
		this.indirectExpenseNetProfit = indirectExpenseNetProfit;
	}

	public BigDecimal getInddirectExpenseTotal() {
		return inddirectExpenseTotal;
	}

	public void setInddirectExpenseTotal(BigDecimal inddirectExpenseTotal) {
		this.inddirectExpenseTotal = inddirectExpenseTotal;
	}

	public BigDecimal getDirectIncomeGrossLoss() {
		return directIncomeGrossLoss;
	}

	public void setDirectIncomeGrossLoss(BigDecimal directIncomeGrossLoss) {
		this.directIncomeGrossLoss = directIncomeGrossLoss;
	}

	public BigDecimal getDirectIncomeTotal() {
		return directIncomeTotal;
	}

	public void setDirectIncomeTotal(BigDecimal directIncomeTotal) {
		this.directIncomeTotal = directIncomeTotal;
	}

	public BigDecimal getIndirectIncomeNetLoss() {
		return indirectIncomeNetLoss;
	}

	public void setIndirectIncomeNetLoss(BigDecimal indirectIncomeNetLoss) {
		this.indirectIncomeNetLoss = indirectIncomeNetLoss;
	}

	public BigDecimal getIndirectIncomeTotal() {
		return indirectIncomeTotal;
	}

	public void setIndirectIncomeTotal(BigDecimal indirectIncomeTotal) {
		this.indirectIncomeTotal = indirectIncomeTotal;
	}

	public String getDateRange() {
		return dateRange;
	}

	public void setDateRange(String dateRange) {
		this.dateRange = dateRange;
	}

	public Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> getDirectExpenses() {
		return directExpenses;
	}

	public void setDirectExpenses(Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> directExpenses) {
		this.directExpenses = directExpenses;
	}

	public Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> getIndirectExpenses() {
		return indirectExpenses;
	}

	public void setIndirectExpenses(Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> indirectExpenses) {
		this.indirectExpenses = indirectExpenses;
	}

	public Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> getDirectIncomes() {
		return directIncomes;
	}

	public void setDirectIncomes(Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> directIncomes) {
		this.directIncomes = directIncomes;
	}

	public Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> getIndirectIncomes() {
		return indirectIncomes;
	}

	public void setIndirectIncomes(Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> indirectIncomes) {
		this.indirectIncomes = indirectIncomes;
	}

	public BigDecimal getDirectExpenseSum() {
		return directExpenseSum;
	}

	public void setDirectExpenseSum(BigDecimal directExpenseSum) {
		this.directExpenseSum = directExpenseSum;
	}

	public BigDecimal getIndirectExpenseSum() {
		return indirectExpenseSum;
	}

	public void setIndirectExpenseSum(BigDecimal indirectExpenseSum) {
		this.indirectExpenseSum = indirectExpenseSum;
	}

	public BigDecimal getDirectIncomeSum() {
		return directIncomeSum;
	}

	public void setDirectIncomeSum(BigDecimal directIncomeSum) {
		this.directIncomeSum = directIncomeSum;
	}

	public BigDecimal getIndirectIncomeSum() {
		return indirectIncomeSum;
	}

	public void setIndirectIncomeSum(BigDecimal indirectIncomeSum) {
		this.indirectIncomeSum = indirectIncomeSum;
	}

}
