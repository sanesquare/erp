package com.accounts.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 24, 2015
 */
public class LedgerViewVo {

	private Integer hashCode = this.hashCode();

	private String ledgerName;

	private Long ledgerId;

	private BigDecimal debitAmount;

	private String startDate;

	private String endDate;

	private BigDecimal creditAmount;

	private BigDecimal closingCredit = new BigDecimal("0.00");

	private BigDecimal closingDebit = new BigDecimal("0.00");

	private BigDecimal totalCredit = new BigDecimal("0.00");

	private BigDecimal totalDebit = new BigDecimal("0.00");

	private List<LedgerViewMonthlyVo> monthlyVos = new ArrayList<LedgerViewMonthlyVo>();

	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public List<LedgerViewMonthlyVo> getMonthlyVos() {
		return monthlyVos;
	}

	public void setMonthlyVos(List<LedgerViewMonthlyVo> monthlyVos) {
		this.monthlyVos = monthlyVos;
	}

	public Integer getHashCode() {
		return hashCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Long getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(Long ledgerId) {
		this.ledgerId = ledgerId;
	}

	public BigDecimal getClosingCredit() {
		return closingCredit;
	}

	public void setClosingCredit(BigDecimal closingCredit) {
		this.closingCredit = closingCredit;
	}

	public BigDecimal getClosingDebit() {
		return closingDebit;
	}

	public void setClosingDebit(BigDecimal closingDebit) {
		this.closingDebit = closingDebit;
	}

	public void setHashCode(Integer hashCode) {
		this.hashCode = hashCode;
	}

	public BigDecimal getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(BigDecimal totalCredit) {
		this.totalCredit = totalCredit;
	}

	public BigDecimal getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(BigDecimal totalDebit) {
		this.totalDebit = totalDebit;
	}
}
