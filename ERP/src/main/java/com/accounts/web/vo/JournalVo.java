package com.accounts.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 15-August-2015
 *
 */
public class JournalVo {

	private Boolean isDependant;
	
	private Boolean edit;
	
	private Long journalId;
	
	private Long companyId;
	
	private String date;
	
	private String voucherNumber;
	
	private BigDecimal amount;
	
	private HashMap<Long,BigDecimal> toLedgerIds = new HashMap<Long, BigDecimal>();
	
	private HashMap<Long,BigDecimal> byLedgerIds = new HashMap<Long, BigDecimal>();

	private List<JournalItemsVo> entries = new ArrayList<JournalItemsVo>();
	
	private String narration;
	
	private String createdBy;
	
	private String fromLedger;
	
	private String toLedger;

	public Long getJournalId() {
		return journalId;
	}

	public void setJournalId(Long journalId) {
		this.journalId = journalId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public HashMap<Long, BigDecimal> getToLedgerIds() {
		return toLedgerIds;
	}

	public void setToLedgerIds(HashMap<Long, BigDecimal> toLedgerIds) {
		this.toLedgerIds = toLedgerIds;
	}

	public HashMap<Long, BigDecimal> getByLedgerIds() {
		return byLedgerIds;
	}

	public void setByLedgerIds(HashMap<Long, BigDecimal> byLedgerIds) {
		this.byLedgerIds = byLedgerIds;
	}

	public List<JournalItemsVo> getEntries() {
		return entries;
	}

	public void setEntries(List<JournalItemsVo> entries) {
		this.entries = entries;
	}

	public Boolean getEdit() {
		return edit;
	}

	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	/**
	 * @return the narration
	 */
	public String getNarration() {
		return narration;
	}

	/**
	 * @param narration the narration to set
	 */
	public void setNarration(String narration) {
		this.narration = narration;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the fromLedger
	 */
	public String getFromLedger() {
		return fromLedger;
	}

	/**
	 * @param fromLedger the fromLedger to set
	 */
	public void setFromLedger(String fromLedger) {
		this.fromLedger = fromLedger;
	}

	/**
	 * @return the toLedger
	 */
	public String getToLedger() {
		return toLedger;
	}

	/**
	 * @param toLedger the toLedger to set
	 */
	public void setToLedger(String toLedger) {
		this.toLedger = toLedger;
	}

	public Boolean getIsDependant() {
		return isDependant;
	}

	public void setIsDependant(Boolean isDependant) {
		this.isDependant = isDependant;
	}
}
