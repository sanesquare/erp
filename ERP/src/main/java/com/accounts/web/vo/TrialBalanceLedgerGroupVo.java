package com.accounts.web.vo;

import java.math.BigDecimal;
/**
 * 
 * @author Shamsheer & Vinutha	
 * @since 05-09-2015
 *
 */
public class TrialBalanceLedgerGroupVo {

	private String accountType;
	
	private String group;
	
	private BigDecimal zero = new BigDecimal("0.00");
	
	private BigDecimal opening = new BigDecimal("0.00");
	
	private BigDecimal debit = new BigDecimal("0.00");
	
	private BigDecimal credit = new BigDecimal("0.00");
	
	private BigDecimal closing = new BigDecimal("0.00");
	
	private BigDecimal openingDebit = new BigDecimal("0.00");

	private BigDecimal openingCredit = new BigDecimal("0.00");

	private BigDecimal transactionDebit = new BigDecimal("0.00");

	private BigDecimal transactionCredit = new BigDecimal("0.00");

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public BigDecimal getOpening() {
		return opening;
	}

	public void setOpening(BigDecimal opening) {
		this.opening = opening;
	}

	public BigDecimal getDebit() {
		return debit;
	}

	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}

	public BigDecimal getCredit() {
		return credit;
	}

	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	public BigDecimal getClosing() {
		return closing;
	}

	public void setClosing(BigDecimal closing) {
		this.closing = closing;
	}

	public BigDecimal getZero() {
		return zero;
	}

	public void setZero(BigDecimal zero) {
		this.zero = zero;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public BigDecimal getOpeningDebit() {
		return openingDebit;
	}

	public void setOpeningDebit(BigDecimal openingDebit) {
		this.openingDebit = openingDebit;
	}

	public BigDecimal getOpeningCredit() {
		return openingCredit;
	}

	public void setOpeningCredit(BigDecimal openingCredit) {
		this.openingCredit = openingCredit;
	}

	public BigDecimal getTransactionDebit() {
		return transactionDebit;
	}

	public void setTransactionDebit(BigDecimal transactionDebit) {
		this.transactionDebit = transactionDebit;
	}

	public BigDecimal getTransactionCredit() {
		return transactionCredit;
	}

	public void setTransactionCredit(BigDecimal transactionCredit) {
		this.transactionCredit = transactionCredit;
	}
	
	
}
