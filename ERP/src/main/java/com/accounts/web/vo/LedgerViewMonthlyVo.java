package com.accounts.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 24, 2015
 */
public class LedgerViewMonthlyVo {

	private String month;

	private BigDecimal openingCredit = new BigDecimal("0.00");

	private BigDecimal openingDebit = new BigDecimal("0.00");

	private String ledgerName;

	private Integer hashCode = this.hashCode();

	private BigDecimal debitAmount;

	private BigDecimal creditAmount;

	private BigDecimal closingCredit = new BigDecimal("0.00");

	private BigDecimal closingDebit = new BigDecimal("0.00");
	
	private BigDecimal currentTotalCredit = new BigDecimal("0.00");
	
	private BigDecimal currentTotalDebit = new BigDecimal("0.00");

	private Set<LedgerTransactionViewVo> transactionViewVos = new LinkedHashSet<LedgerTransactionViewVo>();

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}


	public Set<LedgerTransactionViewVo> getTransactionViewVos() {
		return transactionViewVos;
	}

	public void setTransactionViewVos(Set<LedgerTransactionViewVo> transactionViewVos) {
		this.transactionViewVos = transactionViewVos;
	}

	public Integer getHashCode() {
		return hashCode;
	}

	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	public BigDecimal getOpeningCredit() {
		return openingCredit;
	}

	public void setOpeningCredit(BigDecimal openingCredit) {
		this.openingCredit = openingCredit;
	}

	public BigDecimal getOpeningDebit() {
		return openingDebit;
	}

	public void setOpeningDebit(BigDecimal openingDebit) {
		this.openingDebit = openingDebit;
	}

	public BigDecimal getClosingCredit() {
		return closingCredit;
	}

	public void setClosingCredit(BigDecimal closingCredit) {
		this.closingCredit = closingCredit;
	}

	public BigDecimal getClosingDebit() {
		return closingDebit;
	}

	public void setClosingDebit(BigDecimal closingDebit) {
		this.closingDebit = closingDebit;
	}

	public BigDecimal getCurrentTotalCredit() {
		return currentTotalCredit;
	}

	public void setCurrentTotalCredit(BigDecimal currentTotalCredit) {
		this.currentTotalCredit = currentTotalCredit;
	}

	public BigDecimal getCurrentTotalDebit() {
		return currentTotalDebit;
	}

	public void setCurrentTotalDebit(BigDecimal currentTotalDebit) {
		this.currentTotalDebit = currentTotalDebit;
	}

}
