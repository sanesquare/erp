package com.accounts.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 1-September-2015
 *
 */

public class ProfitAndLossMainPdfVo {

	private List<ProfitAndLossPdfVo> directExpenses = new ArrayList<ProfitAndLossPdfVo>();
	
	private List<ProfitAndLossPdfVo> directIncomes = new ArrayList<ProfitAndLossPdfVo>();
	
	private List<ProfitAndLossPdfVo> indirectExpenses = new ArrayList<ProfitAndLossPdfVo>();
	
	private List<ProfitAndLossPdfVo> indirectIncomes = new ArrayList<ProfitAndLossPdfVo>();
	
	private BigDecimal directExpenseGrossProfit = new BigDecimal("0.00");

	private BigDecimal directExpenseTotal = new BigDecimal("0.00");

	private BigDecimal indirectExpenseNetProfit = new BigDecimal("0.00");

	private BigDecimal inddirectExpenseTotal = new BigDecimal("0.00");

	private BigDecimal directIncomeGrossLoss = new BigDecimal("0.00");

	private BigDecimal directIncomeTotal = new BigDecimal("0.00");

	private BigDecimal indirectIncomeNetLoss = new BigDecimal("0.00");

	private BigDecimal indirectIncomeTotal = new BigDecimal("0.00");

	private BigDecimal directExpenseSum = new BigDecimal("0.00");

	private BigDecimal indirectExpenseSum = new BigDecimal("0.00");

	private BigDecimal directIncomeSum = new BigDecimal("0.00");

	private BigDecimal indirectIncomeSum = new BigDecimal("0.00");

	private String dateRange;

	public List<ProfitAndLossPdfVo> getDirectExpenses() {
		return directExpenses;
	}

	public void setDirectExpenses(List<ProfitAndLossPdfVo> directExpenses) {
		this.directExpenses = directExpenses;
	}

	public List<ProfitAndLossPdfVo> getDirectIncomes() {
		return directIncomes;
	}

	public void setDirectIncomes(List<ProfitAndLossPdfVo> directIncomes) {
		this.directIncomes = directIncomes;
	}

	public List<ProfitAndLossPdfVo> getIndirectExpenses() {
		return indirectExpenses;
	}

	public void setIndirectExpenses(List<ProfitAndLossPdfVo> indirectExpenses) {
		this.indirectExpenses = indirectExpenses;
	}

	public List<ProfitAndLossPdfVo> getIndirectIncomes() {
		return indirectIncomes;
	}

	public void setIndirectIncomes(List<ProfitAndLossPdfVo> indirectIncomes) {
		this.indirectIncomes = indirectIncomes;
	}

	public BigDecimal getDirectExpenseGrossProfit() {
		return directExpenseGrossProfit;
	}

	public void setDirectExpenseGrossProfit(BigDecimal directExpenseGrossProfit) {
		this.directExpenseGrossProfit = directExpenseGrossProfit;
	}

	public BigDecimal getDirectExpenseTotal() {
		return directExpenseTotal;
	}

	public void setDirectExpenseTotal(BigDecimal directExpenseTotal) {
		this.directExpenseTotal = directExpenseTotal;
	}

	public BigDecimal getIndirectExpenseNetProfit() {
		return indirectExpenseNetProfit;
	}

	public void setIndirectExpenseNetProfit(BigDecimal indirectExpenseNetProfit) {
		this.indirectExpenseNetProfit = indirectExpenseNetProfit;
	}

	public BigDecimal getInddirectExpenseTotal() {
		return inddirectExpenseTotal;
	}

	public void setInddirectExpenseTotal(BigDecimal inddirectExpenseTotal) {
		this.inddirectExpenseTotal = inddirectExpenseTotal;
	}

	public BigDecimal getDirectIncomeGrossLoss() {
		return directIncomeGrossLoss;
	}

	public void setDirectIncomeGrossLoss(BigDecimal directIncomeGrossLoss) {
		this.directIncomeGrossLoss = directIncomeGrossLoss;
	}

	public BigDecimal getDirectIncomeTotal() {
		return directIncomeTotal;
	}

	public void setDirectIncomeTotal(BigDecimal directIncomeTotal) {
		this.directIncomeTotal = directIncomeTotal;
	}

	public BigDecimal getIndirectIncomeNetLoss() {
		return indirectIncomeNetLoss;
	}

	public void setIndirectIncomeNetLoss(BigDecimal indirectIncomeNetLoss) {
		this.indirectIncomeNetLoss = indirectIncomeNetLoss;
	}

	public BigDecimal getIndirectIncomeTotal() {
		return indirectIncomeTotal;
	}

	public void setIndirectIncomeTotal(BigDecimal indirectIncomeTotal) {
		this.indirectIncomeTotal = indirectIncomeTotal;
	}

	public BigDecimal getDirectExpenseSum() {
		return directExpenseSum;
	}

	public void setDirectExpenseSum(BigDecimal directExpenseSum) {
		this.directExpenseSum = directExpenseSum;
	}

	public BigDecimal getIndirectExpenseSum() {
		return indirectExpenseSum;
	}

	public void setIndirectExpenseSum(BigDecimal indirectExpenseSum) {
		this.indirectExpenseSum = indirectExpenseSum;
	}

	public BigDecimal getDirectIncomeSum() {
		return directIncomeSum;
	}

	public void setDirectIncomeSum(BigDecimal directIncomeSum) {
		this.directIncomeSum = directIncomeSum;
	}

	public BigDecimal getIndirectIncomeSum() {
		return indirectIncomeSum;
	}

	public void setIndirectIncomeSum(BigDecimal indirectIncomeSum) {
		this.indirectIncomeSum = indirectIncomeSum;
	}

	public String getDateRange() {
		return dateRange;
	}

	public void setDateRange(String dateRange) {
		this.dateRange = dateRange;
	}
	
	
}
