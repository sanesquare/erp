package com.accounts.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 17-August-2015
 *
 */
public class JournalHistoryPdfVo {

	private Integer srNo;
	
	private String voucherNo;
	
	private String date;
	
	private String ledgerBy;
	
	private String ledgerTo;
	
	private String narration;
	
	private BigDecimal amount;

	/**
	 * @return the srNo
	 */
	public Integer getSrNo() {
		return srNo;
	}

	/**
	 * @param srNo the srNo to set
	 */
	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}

	/**
	 * @return the voucherNo
	 */
	public String getVoucherNo() {
		return voucherNo;
	}

	/**
	 * @param voucherNo the voucherNo to set
	 */
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the ledgerBy
	 */
	public String getLedgerBy() {
		return ledgerBy;
	}

	/**
	 * @param ledgerBy the ledgerBy to set
	 */
	public void setLedgerBy(String ledgerBy) {
		this.ledgerBy = ledgerBy;
	}

	/**
	 * @return the ledgerTo
	 */
	public String getLedgerTo() {
		return ledgerTo;
	}

	/**
	 * @param ledgerTo the ledgerTo to set
	 */
	public void setLedgerTo(String ledgerTo) {
		this.ledgerTo = ledgerTo;
	}

	/**
	 * @return the narration
	 */
	public String getNarration() {
		return narration;
	}

	/**
	 * @param narration the narration to set
	 */
	public void setNarration(String narration) {
		this.narration = narration;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	
	
}
