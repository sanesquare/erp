package com.accounts.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 2-August-2015
 *
 */
public class BalanceSheetPdfVo {

	private String group;
	
	private String ledger;
	
	private BigDecimal amount;
	
	private BigDecimal groupAmount;

	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * @return the ledger
	 */
	public String getLedger() {
		return ledger;
	}

	/**
	 * @param ledger the ledger to set
	 */
	public void setLedger(String ledger) {
		this.ledger = ledger;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the groupAmount
	 */
	public BigDecimal getGroupAmount() {
		return groupAmount;
	}

	/**
	 * @param groupAmount the groupAmount to set
	 */
	public void setGroupAmount(BigDecimal groupAmount) {
		this.groupAmount = groupAmount;
	}
}
