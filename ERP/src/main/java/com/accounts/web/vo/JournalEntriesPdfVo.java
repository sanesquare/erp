package com.accounts.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 2-August-2015
 *
 */
public class JournalEntriesPdfVo {

	private Integer srNo;
	
	private String ledger;

	private BigDecimal debitAmnt = new BigDecimal("0.00");

	private BigDecimal creditAmnt = new BigDecimal("0.00");

	/**
	 * @return the ledger
	 */
	public String getLedger() {
		return ledger;
	}

	/**
	 * @param ledger the ledger to set
	 */
	public void setLedger(String ledger) {
		this.ledger = ledger;
	}

	/**
	 * @return the debitAmnt
	 */
	public BigDecimal getDebitAmnt() {
		return debitAmnt;
	}

	/**
	 * @param debitAmnt the debitAmnt to set
	 */
	public void setDebitAmnt(BigDecimal debitAmnt) {
		this.debitAmnt = debitAmnt;
	}

	/**
	 * @return the creditAmnt
	 */
	public BigDecimal getCreditAmnt() {
		return creditAmnt;
	}

	/**
	 * @param creditAmnt the creditAmnt to set
	 */
	public void setCreditAmnt(BigDecimal creditAmnt) {
		this.creditAmnt = creditAmnt;
	}

	/**
	 * @return the srNo
	 */
	public Integer getSrNo() {
		return srNo;
	}

	/**
	 * @param srNo the srNo to set
	 */
	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}
	
}
