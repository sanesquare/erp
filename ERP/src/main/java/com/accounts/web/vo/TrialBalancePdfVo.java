package com.accounts.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-August-2015
 *
 */
public class TrialBalancePdfVo {

	private String ledgerGroup;
	
	private String particulars;
	
	private BigDecimal openingBal;
	
	private BigDecimal debit;
	
	private BigDecimal credit;
	
	private BigDecimal closingBal;

	/**
	 * @return the ledgerGroup
	 */
	public String getLedgerGroup() {
		return ledgerGroup;
	}

	/**
	 * @param ledgerGroup the ledgerGroup to set
	 */
	public void setLedgerGroup(String ledgerGroup) {
		this.ledgerGroup = ledgerGroup;
	}

	/**
	 * @return the particulars
	 */
	public String getParticulars() {
		return particulars;
	}

	/**
	 * @param particulars the particulars to set
	 */
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	/**
	 * @return the openingBal
	 */
	public BigDecimal getOpeningBal() {
		return openingBal;
	}

	/**
	 * @param openingBal the openingBal to set
	 */
	public void setOpeningBal(BigDecimal openingBal) {
		this.openingBal = openingBal;
	}

	/**
	 * @return the debit
	 */
	public BigDecimal getDebit() {
		return debit;
	}

	/**
	 * @param debit the debit to set
	 */
	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}

	/**
	 * @return the credit
	 */
	public BigDecimal getCredit() {
		return credit;
	}

	/**
	 * @param credit the credit to set
	 */
	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	/**
	 * @return the closingBal
	 */
	public BigDecimal getClosingBal() {
		return closingBal;
	}

	/**
	 * @param closingBal the closingBal to set
	 */
	public void setClosingBal(BigDecimal closingBal) {
		this.closingBal = closingBal;
	}
	
	
}
