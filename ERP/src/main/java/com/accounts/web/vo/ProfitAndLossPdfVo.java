package com.accounts.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 31-August-2015
 *
 */
public class ProfitAndLossPdfVo {

	private String ledgerGroup;
	
	private BigDecimal groupAmount;
	
	private String expense;
	
	private BigDecimal expAmount;
	
	private String income;
	
	private BigDecimal incAmount;
	
	private String indirectExpense;
	
	private BigDecimal indirectExpAmount;
	
	private String indirectIncome;
	
	private BigDecimal indirectIncAmount;
	

	/**
	 * @return the expense
	 */
	public String getExpense() {
		return expense;
	}

	/**
	 * @param expense the expense to set
	 */
	public void setExpense(String expense) {
		this.expense = expense;
	}

	/**
	 * @return the expAmount
	 */
	public BigDecimal getExpAmount() {
		return expAmount;
	}

	/**
	 * @param expAmount the expAmount to set
	 */
	public void setExpAmount(BigDecimal expAmount) {
		this.expAmount = expAmount;
	}

	/**
	 * @return the income
	 */
	public String getIncome() {
		return income;
	}

	/**
	 * @param income the income to set
	 */
	public void setIncome(String income) {
		this.income = income;
	}

	/**
	 * @return the incAmount
	 */
	public BigDecimal getIncAmount() {
		return incAmount;
	}

	/**
	 * @param incAmount the incAmount to set
	 */
	public void setIncAmount(BigDecimal incAmount) {
		this.incAmount = incAmount;
	}

	/**
	 * @return the ledgerGroup
	 */
	public String getLedgerGroup() {
		return ledgerGroup;
	}

	/**
	 * @param ledgerGroup the ledgerGroup to set
	 */
	public void setLedgerGroup(String ledgerGroup) {
		this.ledgerGroup = ledgerGroup;
	}

	/**
	 * @return the indirectExpense
	 */
	public String getIndirectExpense() {
		return indirectExpense;
	}

	/**
	 * @param indirectExpense the indirectExpense to set
	 */
	public void setIndirectExpense(String indirectExpense) {
		this.indirectExpense = indirectExpense;
	}

	/**
	 * @return the indirectExpAmount
	 */
	public BigDecimal getIndirectExpAmount() {
		return indirectExpAmount;
	}

	/**
	 * @param indirectExpAmount the indirectExpAmount to set
	 */
	public void setIndirectExpAmount(BigDecimal indirectExpAmount) {
		this.indirectExpAmount = indirectExpAmount;
	}

	/**
	 * @return the indirectIncome
	 */
	public String getIndirectIncome() {
		return indirectIncome;
	}

	/**
	 * @param indirectIncome the indirectIncome to set
	 */
	public void setIndirectIncome(String indirectIncome) {
		this.indirectIncome = indirectIncome;
	}

	/**
	 * @return the indirectIncAmount
	 */
	public BigDecimal getIndirectIncAmount() {
		return indirectIncAmount;
	}

	/**
	 * @param indirectIncAmount the indirectIncAmount to set
	 */
	public void setIndirectIncAmount(BigDecimal indirectIncAmount) {
		this.indirectIncAmount = indirectIncAmount;
	}

	/**
	 * @return the groupAmount
	 */
	public BigDecimal getGroupAmount() {
		return groupAmount;
	}

	/**
	 * @param groupAmount the groupAmount to set
	 */
	public void setGroupAmount(BigDecimal groupAmount) {
		this.groupAmount = groupAmount;
	}
	
	
}
