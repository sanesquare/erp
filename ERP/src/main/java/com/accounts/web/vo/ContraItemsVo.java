package com.accounts.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 19-September-2015
 *
 */
public class ContraItemsVo {

	private Long id;

	private Long contraId;

	private Long accTypeId;

	private String account;
	
	private BigDecimal debitAmnt = new BigDecimal("0.00");

	private BigDecimal creditAmnt = new BigDecimal("0.00");

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getContraId() {
		return contraId;
	}

	public void setContraId(Long contraId) {
		this.contraId = contraId;
	}

	public Long getAccTypeId() {
		return accTypeId;
	}

	public void setAccTypeId(Long accTypeId) {
		this.accTypeId = accTypeId;
	}

	public BigDecimal getDebitAmnt() {
		return debitAmnt;
	}

	public void setDebitAmnt(BigDecimal debitAmnt) {
		this.debitAmnt = debitAmnt;
	}

	public BigDecimal getCreditAmnt() {
		return creditAmnt;
	}

	public void setCreditAmnt(BigDecimal creditAmnt) {
		this.creditAmnt = creditAmnt;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
}
