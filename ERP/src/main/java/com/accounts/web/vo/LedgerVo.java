package com.accounts.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
public class LedgerVo {

	private Long ledgerId;

	private String ledgerName;

	private String ledgerGroup;

	private Long ledgerGroupId;

	private BigDecimal zero = new BigDecimal("0.00");

	private BigDecimal openingBalance = new BigDecimal("0.00");

	private BigDecimal balance;

	private String startDate;

	private Long companyId;

	private String accountType;

	private String createdBy;

	private BigDecimal credit = new BigDecimal("0.00");

	private BigDecimal debit = new BigDecimal("0.00");

	private BigDecimal totalDebit = new BigDecimal("0.00");

	private BigDecimal totalCredit = new BigDecimal("0.00");

	private BigDecimal totalOpening = new BigDecimal("0.00");

	private BigDecimal totalClose = new BigDecimal("0.00");

	private Boolean isEditable = true;

	private BigDecimal openingDebit = new BigDecimal("0.00");

	private BigDecimal openingCredit = new BigDecimal("0.00");

	private BigDecimal transactionDebit = new BigDecimal("0.00");

	private BigDecimal transactionCredit = new BigDecimal("0.00");

	private BigDecimal totalOpeningDebit = new BigDecimal("0.00");

	private BigDecimal totalOpeningCredit = new BigDecimal("0.00");

	private BigDecimal totalTransactionCredit = new BigDecimal("0.00");
	
	private BigDecimal totalTransactionDebit = new BigDecimal("0.00");

	public Boolean getIsEditable() {
		return isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	/**
	 * @return the ledgerId
	 */
	public Long getLedgerId() {
		return ledgerId;
	}

	/**
	 * @param ledgerId
	 *            the ledgerId to set
	 */
	public void setLedgerId(Long ledgerId) {
		this.ledgerId = ledgerId;
	}

	/**
	 * @return the ledgerName
	 */
	public String getLedgerName() {
		return ledgerName;
	}

	/**
	 * @param ledgerName
	 *            the ledgerName to set
	 */
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	/**
	 * @return the ledgerGroup
	 */
	public String getLedgerGroup() {
		return ledgerGroup;
	}

	/**
	 * @param ledgerGroup
	 *            the ledgerGroup to set
	 */
	public void setLedgerGroup(String ledgerGroup) {
		this.ledgerGroup = ledgerGroup;
	}

	/**
	 * @return the ledgerGroupId
	 */
	public Long getLedgerGroupId() {
		return ledgerGroupId;
	}

	/**
	 * @param ledgerGroupId
	 *            the ledgerGroupId to set
	 */
	public void setLedgerGroupId(Long ledgerGroupId) {
		this.ledgerGroupId = ledgerGroupId;
	}

	/**
	 * @return the openingBalance
	 */
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}

	/**
	 * @param openingBalance
	 *            the openingBalance to set
	 */
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	/**
	 * @return the companyId
	 */
	public Long getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId
	 *            the companyId to set
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public BigDecimal getCredit() {
		return credit;
	}

	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	public BigDecimal getDebit() {
		return debit;
	}

	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}

	public BigDecimal getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(BigDecimal totalDebit) {
		this.totalDebit = totalDebit;
	}

	public BigDecimal getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(BigDecimal totalCredit) {
		this.totalCredit = totalCredit;
	}

	public BigDecimal getTotalOpening() {
		return totalOpening;
	}

	public void setTotalOpening(BigDecimal totalOpening) {
		this.totalOpening = totalOpening;
	}

	public BigDecimal getTotalClose() {
		return totalClose;
	}

	public void setTotalClose(BigDecimal totalClose) {
		this.totalClose = totalClose;
	}

	public BigDecimal getZero() {
		return zero;
	}

	public void setZero(BigDecimal zero) {
		this.zero = zero;
	}

	public BigDecimal getOpeningDebit() {
		return openingDebit;
	}

	public void setOpeningDebit(BigDecimal openingDebit) {
		this.openingDebit = openingDebit;
	}

	public BigDecimal getOpeningCredit() {
		return openingCredit;
	}

	public void setOpeningCredit(BigDecimal openingCredit) {
		this.openingCredit = openingCredit;
	}

	public BigDecimal getTransactionDebit() {
		return transactionDebit;
	}

	public void setTransactionDebit(BigDecimal transactionDebit) {
		this.transactionDebit = transactionDebit;
	}

	public BigDecimal getTransactionCredit() {
		return transactionCredit;
	}

	public void setTransactionCredit(BigDecimal transactionCredit) {
		this.transactionCredit = transactionCredit;
	}

	public BigDecimal getTotalOpeningDebit() {
		return totalOpeningDebit;
	}

	public void setTotalOpeningDebit(BigDecimal totalOpeningDebit) {
		this.totalOpeningDebit = totalOpeningDebit;
	}

	public BigDecimal getTotalOpeningCredit() {
		return totalOpeningCredit;
	}

	public void setTotalOpeningCredit(BigDecimal totalOpeningCredit) {
		this.totalOpeningCredit = totalOpeningCredit;
	}

	public BigDecimal getTotalTransactionCredit() {
		return totalTransactionCredit;
	}

	public void setTotalTransactionCredit(BigDecimal totalTransactionCredit) {
		this.totalTransactionCredit = totalTransactionCredit;
	}

	public BigDecimal getTotalTransactionDebit() {
		return totalTransactionDebit;
	}

	public void setTotalTransactionDebit(BigDecimal totalTransactionDebit) {
		this.totalTransactionDebit = totalTransactionDebit;
	}

}
