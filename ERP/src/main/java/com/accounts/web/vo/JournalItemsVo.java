package com.accounts.web.vo;

import java.math.BigDecimal;

public class JournalItemsVo {

	private Long id;
	
	private Long journalId;
	
	private Long accTypeId;
	
	private String ledger;
	
	private BigDecimal debitAmnt=new BigDecimal("0.00");
	
	private BigDecimal creditAmnt=new BigDecimal("0.00");

	public Long getJournalId() {
		return journalId;
	}

	public void setJournalId(Long journalId) {
		this.journalId = journalId;
	}

	public Long getAccTypeId() {
		return accTypeId;
	}

	public void setAccTypeId(Long accTypeId) {
		this.accTypeId = accTypeId;
	}

	public BigDecimal getDebitAmnt() {
		return debitAmnt;
	}

	public void setDebitAmnt(BigDecimal debitAmnt) {
		this.debitAmnt = debitAmnt;
	}

	public BigDecimal getCreditAmnt() {
		return creditAmnt;
	}

	public void setCreditAmnt(BigDecimal creditAmnt) {
		this.creditAmnt = creditAmnt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLedger() {
		return ledger;
	}

	public void setLedger(String ledger) {
		this.ledger = ledger;
	}
}
