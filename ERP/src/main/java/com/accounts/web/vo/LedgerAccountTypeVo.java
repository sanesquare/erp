package com.accounts.web.vo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
public class LedgerAccountTypeVo {

	private Long typeId;
	
	private String accountType;

	/**
	 * @return the typeId
	 */
	public Long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the accountType
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	
	
	
}
