package com.accounts.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 22-August-2015
 *
 */
public class ProfitAndLossGroupVo {
	
	private Integer hashCode=this.hashCode();
	
	private String groupName;

	private BigDecimal amount=new BigDecimal("0.00");

	private Boolean first=false;
	
	private Boolean isLast=false;
	
	private Long groupId;
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	@Override
	public boolean equals(Object obj) {
		ProfitAndLossGroupVo that= (ProfitAndLossGroupVo) obj;
		System.out.println(that.groupName);
		if(this.groupName.equals(that.groupName)){
			return true;
		}
		return true;
	}

	public Boolean getFirst() {
		return first;
	}

	public void setFirst(Boolean first) {
		this.first = first;
	}

	public Boolean getIsLast() {
		return isLast;
	}

	public void setIsLast(Boolean isLast) {
		this.isLast = isLast;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Integer getHashCode() {
		return hashCode;
	}
}
