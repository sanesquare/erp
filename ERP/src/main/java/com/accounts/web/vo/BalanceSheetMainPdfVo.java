package com.accounts.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 2-August-2015
 *
 */
public class BalanceSheetMainPdfVo {

	private List<BalanceSheetPdfVo> equityList = new ArrayList<BalanceSheetPdfVo>();
	
	private List<BalanceSheetPdfVo> currentLiabilityList = new ArrayList<BalanceSheetPdfVo>();
	
	private List<BalanceSheetPdfVo> nonCurrentLiabilityList = new ArrayList<BalanceSheetPdfVo>();
	
	private List<BalanceSheetPdfVo> currentAssetList = new ArrayList<BalanceSheetPdfVo>();
	
	private List<BalanceSheetPdfVo> nonCurrentAssetList = new ArrayList<BalanceSheetPdfVo>();
	
	private BigDecimal assetTotal;
	
	private BigDecimal liabilityTotal;

	/**
	 * @return the equityList
	 */
	public List<BalanceSheetPdfVo> getEquityList() {
		return equityList;
	}

	/**
	 * @param equityList the equityList to set
	 */
	public void setEquityList(List<BalanceSheetPdfVo> equityList) {
		this.equityList = equityList;
	}

	/**
	 * @return the currentLiabilityList
	 */
	public List<BalanceSheetPdfVo> getCurrentLiabilityList() {
		return currentLiabilityList;
	}

	/**
	 * @param currentLiabilityList the currentLiabilityList to set
	 */
	public void setCurrentLiabilityList(List<BalanceSheetPdfVo> currentLiabilityList) {
		this.currentLiabilityList = currentLiabilityList;
	}

	/**
	 * @return the nonCurrentLiabilityList
	 */
	public List<BalanceSheetPdfVo> getNonCurrentLiabilityList() {
		return nonCurrentLiabilityList;
	}

	/**
	 * @param nonCurrentLiabilityList the nonCurrentLiabilityList to set
	 */
	public void setNonCurrentLiabilityList(List<BalanceSheetPdfVo> nonCurrentLiabilityList) {
		this.nonCurrentLiabilityList = nonCurrentLiabilityList;
	}

	/**
	 * @return the currentAssetList
	 */
	public List<BalanceSheetPdfVo> getCurrentAssetList() {
		return currentAssetList;
	}

	/**
	 * @param currentAssetList the currentAssetList to set
	 */
	public void setCurrentAssetList(List<BalanceSheetPdfVo> currentAssetList) {
		this.currentAssetList = currentAssetList;
	}

	/**
	 * @return the nonCurrentAssetList
	 */
	public List<BalanceSheetPdfVo> getNonCurrentAssetList() {
		return nonCurrentAssetList;
	}

	/**
	 * @param nonCurrentAssetList the nonCurrentAssetList to set
	 */
	public void setNonCurrentAssetList(List<BalanceSheetPdfVo> nonCurrentAssetList) {
		this.nonCurrentAssetList = nonCurrentAssetList;
	}

	/**
	 * @return the assetTotal
	 */
	public BigDecimal getAssetTotal() {
		return assetTotal;
	}

	/**
	 * @param assetTotal the assetTotal to set
	 */
	public void setAssetTotal(BigDecimal assetTotal) {
		this.assetTotal = assetTotal;
	}

	/**
	 * @return the liabilityTotal
	 */
	public BigDecimal getLiabilityTotal() {
		return liabilityTotal;
	}

	/**
	 * @param liabilityTotal the liabilityTotal to set
	 */
	public void setLiabilityTotal(BigDecimal liabilityTotal) {
		this.liabilityTotal = liabilityTotal;
	}
	
	
}
