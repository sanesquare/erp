package com.accounts.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
public class PaymentVo {

	private Boolean isDependant ;

	private Boolean edit;

	private Long paymentId;

	private Long companyId;

	private String date;

	private String voucherNumber;

	private BigDecimal amount;

	private Long toLedgerId;

	private Long byLedgerId;

	private String narration;

	private String byLedger;

	private String toLedger;

	private List<PaymentItemsVo> entries = new ArrayList<PaymentItemsVo>();

	private String createdBy;

	private HashMap<Long, BigDecimal> toLedgerIds = new HashMap<Long, BigDecimal>();

	private HashMap<Long, BigDecimal> byLedgerIds = new HashMap<Long, BigDecimal>();

	private String fromLedger;

	/**
	 * @return the edit
	 */
	public Boolean getEdit() {
		return edit;
	}

	/**
	 * @param edit
	 *            the edit to set
	 */
	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	/**
	 * @return the companyId
	 */
	public Long getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId
	 *            the companyId to set
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the voucherNumber
	 */
	public String getVoucherNumber() {
		return voucherNumber;
	}

	/**
	 * @param voucherNumber
	 *            the voucherNumber to set
	 */
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the toLedgerId
	 */
	public Long getToLedgerId() {
		return toLedgerId;
	}

	/**
	 * @param toLedgerId
	 *            the toLedgerId to set
	 */
	public void setToLedgerId(Long toLedgerId) {
		this.toLedgerId = toLedgerId;
	}

	/**
	 * @return the byLedgerId
	 */
	public Long getByLedgerId() {
		return byLedgerId;
	}

	/**
	 * @param byLedgerId
	 *            the byLedgerId to set
	 */
	public void setByLedgerId(Long byLedgerId) {
		this.byLedgerId = byLedgerId;
	}

	/**
	 * @return the narration
	 */
	public String getNarration() {
		return narration;
	}

	/**
	 * @param narration
	 *            the narration to set
	 */
	public void setNarration(String narration) {
		this.narration = narration;
	}

	/**
	 * @return the paymentId
	 */
	public Long getPaymentId() {
		return paymentId;
	}

	/**
	 * @param paymentId
	 *            the paymentId to set
	 */
	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	/**
	 * @return the byLedger
	 */
	public String getByLedger() {
		return byLedger;
	}

	/**
	 * @param byLedger
	 *            the byLedger to set
	 */
	public void setByLedger(String byLedger) {
		this.byLedger = byLedger;
	}

	/**
	 * @return the toLedger
	 */
	public String getToLedger() {
		return toLedger;
	}

	/**
	 * @param toLedger
	 *            the toLedger to set
	 */
	public void setToLedger(String toLedger) {
		this.toLedger = toLedger;
	}

	public List<PaymentItemsVo> getEntries() {
		return entries;
	}

	public void setEntries(List<PaymentItemsVo> entries) {
		this.entries = entries;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public HashMap<Long, BigDecimal> getToLedgerIds() {
		return toLedgerIds;
	}

	public void setToLedgerIds(HashMap<Long, BigDecimal> toLedgerIds) {
		this.toLedgerIds = toLedgerIds;
	}

	public HashMap<Long, BigDecimal> getByLedgerIds() {
		return byLedgerIds;
	}

	public void setByLedgerIds(HashMap<Long, BigDecimal> byLedgerIds) {
		this.byLedgerIds = byLedgerIds;
	}

	public String getFromLedger() {
		return fromLedger;
	}

	public void setFromLedger(String fromLedger) {
		this.fromLedger = fromLedger;
	}

	public Boolean getIsDependant() {
		return isDependant;
	}

	public void setIsDependant(Boolean isDependant) {
		this.isDependant = isDependant;
	}

}
