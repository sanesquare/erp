package com.accounts.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer & Vinutha 
 * @since 19-September-2015
 *
 */
public class ReceiptItemsVo {

	private Long id;

	private Long receiptId;

	private Long accTypeId;

	private BigDecimal debitAmnt = new BigDecimal("0.00");

	private BigDecimal creditAmnt = new BigDecimal("0.00");

	private String ledger;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(Long receiptId) {
		this.receiptId = receiptId;
	}

	public Long getAccTypeId() {
		return accTypeId;
	}

	public void setAccTypeId(Long accTypeId) {
		this.accTypeId = accTypeId;
	}

	public BigDecimal getDebitAmnt() {
		return debitAmnt;
	}

	public void setDebitAmnt(BigDecimal debitAmnt) {
		this.debitAmnt = debitAmnt;
	}

	public BigDecimal getCreditAmnt() {
		return creditAmnt;
	}

	public void setCreditAmnt(BigDecimal creditAmnt) {
		this.creditAmnt = creditAmnt;
	}

	public String getLedger() {
		return ledger;
	}

	public void setLedger(String ledger) {
		this.ledger = ledger;
	}
}
