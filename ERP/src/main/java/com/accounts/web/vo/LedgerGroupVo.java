package com.accounts.web.vo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
public class LedgerGroupVo {

	private Long groupId;
	
	private String groupName;
	
	private String groupCode;
	
	private String accounttype;
	
	private Long accountTypeId;
	
	private Long companyId;
	
	private Boolean isEditable;

	
	/**
	 * @return the groupId
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the accounttype
	 */
	public String getAccounttype() {
		return accounttype;
	}

	/**
	 * @param accounttype the accounttype to set
	 */
	public void setAccounttype(String accounttype) {
		this.accounttype = accounttype;
	}

	/**
	 * @return the accountTypeId
	 */
	public Long getAccountTypeId() {
		return accountTypeId;
	}

	/**
	 * @param accountTypeId the accountTypeId to set
	 */
	public void setAccountTypeId(Long accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	/**
	 * @return the groupCode
	 */
	public String getGroupCode() {
		return groupCode;
	}

	/**
	 * @param groupCode the groupCode to set
	 */
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
	public Boolean getIsEditable() {
		return isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

}
