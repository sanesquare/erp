package com.accounts.web.vo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 24-August-2015
 */
public class BalanceSheetVo {

	private BigDecimal totalAsset=new BigDecimal("0.00");
	
	private BigDecimal totalLiability=new BigDecimal("0.00");
	
	private String dateRange;
	
	private Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> currentLiabilities = new HashMap<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>>();
	
	private Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> nonCurrentLiabilities = new HashMap<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>>();
	
	private Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> currentAssets = new HashMap<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>>();
	
	private Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> nonCurrentAssets = new HashMap<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>>();
	
	private Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> equity = new HashMap<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>>();

	public BigDecimal getTotalAsset() {
		return totalAsset;
	}

	public void setTotalAsset(BigDecimal totalAsset) {
		this.totalAsset = totalAsset;
	}

	public BigDecimal getTotalLiability() {
		return totalLiability;
	}

	public void setTotalLiability(BigDecimal totalLiability) {
		this.totalLiability = totalLiability;
	}

	public Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> getCurrentLiabilities() {
		return currentLiabilities;
	}

	public void setCurrentLiabilities(Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> currentLiabilities) {
		this.currentLiabilities = currentLiabilities;
	}

	public Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> getNonCurrentLiabilities() {
		return nonCurrentLiabilities;
	}

	public void setNonCurrentLiabilities(Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> nonCurrentLiabilities) {
		this.nonCurrentLiabilities = nonCurrentLiabilities;
	}

	public Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> getCurrentAssets() {
		return currentAssets;
	}

	public void setCurrentAssets(Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> currentAssets) {
		this.currentAssets = currentAssets;
	}

	public Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> getNonCurrentAssets() {
		return nonCurrentAssets;
	}

	public void setNonCurrentAssets(Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> nonCurrentAssets) {
		this.nonCurrentAssets = nonCurrentAssets;
	}

	public String getDateRange() {
		return dateRange;
	}

	public void setDateRange(String dateRange) {
		this.dateRange = dateRange;
	}

	public Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> getEquity() {
		return equity;
	}

	public void setEquity(Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> equity) {
		this.equity = equity;
	}
}
