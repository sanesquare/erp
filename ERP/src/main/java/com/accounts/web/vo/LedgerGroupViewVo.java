package com.accounts.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shamsheer & Vinutha
 * @since Nov 24, 2015
 */
public class LedgerGroupViewVo {

	private List<LedgerViewVo> ledgerViewVos=new ArrayList<LedgerViewVo>();
	
	private BigDecimal totalCredit=new BigDecimal("0.00");
	
	private BigDecimal totalDebit=new BigDecimal("0.00");
	
	private String ledgerGroup;

	public List<LedgerViewVo> getLedgerViewVos() {
		return ledgerViewVos;
	}

	public void setLedgerViewVos(List<LedgerViewVo> ledgerViewVos) {
		this.ledgerViewVos = ledgerViewVos;
	}

	public String getLedgerGroup() {
		return ledgerGroup;
	}

	public void setLedgerGroup(String ledgerGroup) {
		this.ledgerGroup = ledgerGroup;
	}

	public BigDecimal getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(BigDecimal totalCredit) {
		this.totalCredit = totalCredit;
	}

	public BigDecimal getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(BigDecimal totalDebit) {
		this.totalDebit = totalDebit;
	}
}
