package com.accounts.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.ReceiptDao;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.Payment;
import com.accounts.web.entities.PaymentEntries;
import com.accounts.web.entities.ReceiptEntries;
import com.accounts.web.entities.Receipts;
import com.accounts.web.entities.Transactions;
import com.accounts.web.vo.PaymentItemsVo;
import com.accounts.web.vo.PaymentVo;
import com.accounts.web.vo.ReceiptItemsVo;
import com.accounts.web.vo.ReceiptVo;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.vo.ErpCurrencyVo;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer & vinutha
 * @since 18-August-2015
 *
 */
@Repository
public class ReceiptDaoImpl extends AbstractDao implements ReceiptDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private ErpCurrencyDao erpCurrencyDao;

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	/**
	 * Method to save or update receipt
	 * 
	 * @param receiptVo
	 * @return
	 */
	public Long saveOrUpdateReceipt(ReceiptVo receiptVo) {
		Receipts receipt = null;
		Long id = receiptVo.getReceiptId();
		if (id == null) {
			// save
			receipt = new Receipts();
			receipt.setIsDependant(false);
			Company company = companyDao.findCompany(receiptVo.getCompanyId());
			receipt.setReceiptNumber(this.getReceiptNumberForCompany(receiptVo.getCompanyId()));
			receipt.setCompany(company);
			receipt = this.setAttributesForReceipt(receipt, receiptVo,true);
			id = (Long) this.sessionFactory.getCurrentSession().save(receipt);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.RECEIPT, company);
		} else {
			// update
			receipt = findReceiptObjectById(id);
			receipt = this.setAttributesForReceipt(receipt, receiptVo,true);
			this.sessionFactory.getCurrentSession().merge(receipt);
		}
		return id;
	}

	public Receipts setAttributesForReceipt(Receipts receipt, ReceiptVo receiptVo, Boolean withCurrency) {
		ErpCurrencyVo currencyVo = erpCurrencyDao.findCurrencyForDate(
				DateFormatter.convertStringToDate(receiptVo.getDate()), receipt.getCompany().getCurrency()
						.getBaseCurrency());
		if (currencyVo != null) {
			receipt.setAmount(receiptVo.getAmount().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
		} else {
			receipt.setAmount(receiptVo.getAmount());
		}
		receipt.setReceiptDate(DateFormatter.convertStringToDate(receiptVo.getDate()));
		receipt.setNarration(receiptVo.getNarration());
		for (ReceiptItemsVo itemsVo : receiptVo.getEntries()) {
			Ledger ledger = ledgerDao.findLedgerObjectById(itemsVo.getAccTypeId());
			String accountType = ledger.getLedgerGroup().getAccountType().getType();
			BigDecimal plusAmount = new BigDecimal(0);
			BigDecimal minusAmount = new BigDecimal(0);
			// byLedger
			ReceiptEntries entries = (ReceiptEntries) this.sessionFactory.getCurrentSession()
					.createCriteria(ReceiptEntries.class).add(Restrictions.eq("id", itemsVo.getId())).uniqueResult();
			Transactions transaction =null;
			if (entries == null) {
				entries = new ReceiptEntries();
				transaction = new Transactions();
				transaction.setCompany(receipt.getCompany());
			}else
				transaction=entries.getTransactions();
			entries.setLedger(ledger);
			if (currencyVo != null) {
				entries.setDebit(itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
				entries.setCredit(itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));

				if (accountType.contains("Expense")) {
					plusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					minusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Income")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Asset")) {
					plusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					minusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Liability")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Equity")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				}

			} else {
				entries.setCredit(itemsVo.getCreditAmnt());

				if (accountType.contains("Expense")) {
					plusAmount = itemsVo.getDebitAmnt();
					minusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Income")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Asset")) {
					plusAmount = itemsVo.getDebitAmnt();
					minusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Liability")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Equity")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				}

				entries.setDebit(itemsVo.getDebitAmnt());
			}
			transaction.setLedger(ledger);
			transaction.setDate(receipt.getReceiptDate());
			transaction.setMinusAmount(minusAmount);
			transaction.setPlusAmount(plusAmount);
			transaction.setReceiptEntries(entries);
			entries.setTransactions(transaction);
			entries.setReceipt(receipt);
			receipt.getEntries().add(entries);

		}
		return receipt;
	}

	/**
	 * Method to delete receipt
	 * 
	 * @param id
	 */
	public void deleteReceipt(Long id) {
		Receipts receipt = findReceiptObjectById(id);
		if(receipt==null)
			throw new ItemNotFoundException("Receipt Not Found");
		this.sessionFactory.getCurrentSession().delete(receipt);

	}

	/**
	 * Method to find receipt by id
	 * 
	 * @param id
	 * @return
	 */
	public ReceiptVo findReceiptById(Long id) {
		Receipts receipt = findReceiptObjectById(id);
		if(receipt==null)
			throw new ItemNotFoundException("Receipt Not Found");
		return this.createReceiptVo(receipt);
	}

	/**
	 * Method to create receiptVo
	 * 
	 * @param receipt
	 * @return
	 */
	private ReceiptVo createReceiptVo(Receipts receipt) {

		ErpCurrencyVo currencyVo = erpCurrencyDao.findCurrencyForDate(receipt.getReceiptDate(), receipt.getCompany()
				.getCurrency().getBaseCurrency());
		ReceiptVo vo = new ReceiptVo();
		vo.setCreatedBy(receipt.getCreatedBy().getUserName());
		vo.setReceiptId(receipt.getId());
		vo.setCompanyId(receipt.getCompany().getId());
		vo.setDate(DateFormatter.convertDateToString(receipt.getReceiptDate()));
		vo.setVoucherNumber(receipt.getReceiptNumber());
		vo.setNarration(receipt.getNarration());
		List<ReceiptItemsVo> itemsVos = new ArrayList<ReceiptItemsVo>();
		HashMap<Long, BigDecimal> byLedgers = new HashMap<Long, BigDecimal>();
		HashMap<Long, BigDecimal> toLedgers = new HashMap<Long, BigDecimal>();
		vo.setIsDependant(receipt.getIsDependant());
		Ledger byLedger = null;
		Ledger toLedger= null;
		for (ReceiptEntries entry : receipt.getEntries()) {
			ReceiptItemsVo itemsVo = new ReceiptItemsVo();
			Ledger ledger =entry.getLedger();
			itemsVo.setAccTypeId(ledger.getId());
			itemsVo.setLedger(ledger.getLedgerName());
			if (currencyVo != null) {
				
				itemsVo.setDebitAmnt(entry.getDebit().multiply(currencyVo.getRate())
						.setScale(2, RoundingMode.HALF_EVEN));
				itemsVo.setCreditAmnt(entry.getCredit().multiply(currencyVo.getRate())
						.setScale(2, RoundingMode.HALF_EVEN));
				
			} else {
				itemsVo.setCreditAmnt(entry.getCredit());
				itemsVo.setDebitAmnt(entry.getDebit());
				
			}
			if(entry.getDebit().compareTo(new BigDecimal(0))!=0){
				byLedger=entry.getLedger();
				byLedgers.put(entry.getId(), itemsVo.getDebitAmnt());					
			}else if(entry.getCredit().compareTo(new BigDecimal(0))!=0){
				toLedger=entry.getLedger();
				toLedgers.put(entry.getId(), itemsVo.getCreditAmnt());
			}
			
			itemsVo.setId(entry.getId());
			itemsVos.add(itemsVo);
		}
		vo.setByLedgerIds(byLedgers);
		vo.setToLedgerIds(toLedgers);
		if(vo.getByLedgerIds().size()>1){
			vo.setFromLedger("Multiple Accounts");
		}else if(vo.getByLedgerIds().size()==1){
			vo.setFromLedger(byLedger.getLedgerName());
		}
		if(vo.getToLedgerIds().size()>1){
			vo.setToLedger("Multiple Accounts");
		}else if(vo.getToLedgerIds().size()==1){
			vo.setToLedger(toLedger.getLedgerName());
		}
		vo.setEntries(itemsVos);
		if (currencyVo != null) {
			vo.setAmount(receipt.getAmount().multiply(currencyVo.getRate()).setScale(2, RoundingMode.HALF_EVEN));
		} else {
			vo.setAmount(receipt.getAmount());
		}
		return vo;
	
	}

	/**
	 * method to find receipt object by id
	 * 
	 * @param id
	 * @return
	 */
	public Receipts findReceiptObjectById(Long id) {
		Receipts receipt = (Receipts) this.sessionFactory.getCurrentSession().createCriteria(Receipts.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
		return receipt;
	}

	/**
	 * method to find all receipts for company
	 * 
	 * @param companyId
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForCompany(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Does Not Exist");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Receipts.class)
				.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Receipts> receipts = criteria.list();
		List<ReceiptVo> receiptVos = new ArrayList<ReceiptVo>();
		for (Receipts receipt : receipts) {
			receiptVos.add(this.createReceiptVo(receipt));
		}
		return receiptVos;
	}

	/**
	 * method to find all journals for company within date
	 * 
	 * @param companyId
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForCompanyWithinDate(Long companyId, String start, String end) {
		Company company = companyDao.findCompany(companyId);
		Date strt = DateFormatter.convertStringToDate(start);
		Date endDate = DateFormatter.convertStringToDate(end);
		if (company == null)
			throw new ItemNotFoundException("Company Does Not Exist");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Receipts.class)
				.add(Restrictions.eq("company", company)).add(Restrictions.ge("receiptDate", strt))
				.add(Restrictions.le("receiptDate", endDate));

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Receipts> receipts = criteria.list();
		List<ReceiptVo> receiptVos = new ArrayList<ReceiptVo>();
		for (Receipts receipt : receipts) {
			receiptVos.add(this.createReceiptVo(receipt));
		}
		return receiptVos;
	}

	/**
	 * Method to find all receipt for by ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		Date start = DateFormatter.convertStringToDate(startDate);
		Date end = DateFormatter.convertStringToDate(endDate);
		Ledger ledger = ledgerDao.findLedgerObjectById(ledgerId);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Receipts.class);
		criteria.add(Restrictions.eq("company", companyDao.findCompany(companyId)));
		criteria.add(Restrictions.between("receiptDate", start, end));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		Criterion byLedger = Restrictions.eq("ledgerBy", ledger);
		Criterion toLedger = Restrictions.eq("ledgerTo", ledger);

		LogicalExpression exp = Restrictions.or(byLedger, toLedger);
		criteria.add(exp);

		List<Receipts> receipts = criteria.list();
		List<ReceiptVo> vos = new ArrayList<ReceiptVo>();
		if (receipts != null) {
			for (Receipts receipt : receipts) {
				vos.add(this.createReceiptVo(receipt));
			}
		}
		return vos;
	}

	/**
	 * Method to find receipts for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForToLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Receipts.class);
		criteria.add(Restrictions.eq("company", companyDao.findCompany(companyId)));
		criteria.add(Restrictions.eq("ledgerTo", ledgerDao.findLedgerObjectById(ledgerId)));
		List<Receipts> receipts = new ArrayList<Receipts>();
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		receipts = criteria.list();
		List<ReceiptVo> vos = new ArrayList<ReceiptVo>();
		for (Receipts receipt : receipts) {
			vos.add(this.createReceiptVo(receipt));
		}
		return vos;
	}

	/**
	 * Method to find receipts for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ReceiptVo> findAllReceiptsForByLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Receipts.class);
		criteria.add(Restrictions.eq("company", companyDao.findCompany(companyId)));
		criteria.add(Restrictions.eq("ledgerBy", ledgerDao.findLedgerObjectById(ledgerId)));
		List<Receipts> receipts = new ArrayList<Receipts>();
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		receipts = criteria.list();
		List<ReceiptVo> vos = new ArrayList<ReceiptVo>();
		for (Receipts receipt : receipts) {
			vos.add(this.createReceiptVo(receipt));
		}
		return vos;
	}

	/**
	 * Method to get receipt voucher number
	 * 
	 * @param companyId
	 * @return
	 */
	public String getReceiptNumberForCompany(Long companyId) {
		NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(NumberPropertyConstants.RECEIPT, companyId);
		String code = numberPropertyVo.getPrefix() + String.valueOf(numberPropertyVo.getNumber());
		return code;

	}

}
