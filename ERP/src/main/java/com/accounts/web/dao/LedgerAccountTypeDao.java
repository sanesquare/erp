package com.accounts.web.dao;

import java.util.List;

import com.accounts.web.entities.LedgerAccountType;
import com.accounts.web.vo.LedgerAccountTypeVo;

public interface LedgerAccountTypeDao {

	/**
	 * Method to find account type by name
	 * @param type
	 * @return
	 */
	public LedgerAccountTypeVo findAccountTypeByName(String type);
	/**
	 * Method to find account type by Id
	 * @param type
	 * @return
	 */
	public LedgerAccountTypeVo findAccountTypeById(Long id);
	
	/**
	 * Method to find account type object by name
	 * @param type
	 * @return
	 */
	public LedgerAccountType findAccountTypeObject(String type); 
	/**
	 * Method to find account type object by Id
	 * @param type
	 * @return
	 */
	public LedgerAccountType findAccountTypeObjectById(Long id);
	/**
	 * Method to list all Account types
	 * @return
	 */
	public List<LedgerAccountTypeVo> findAllAccountTypes();
}
