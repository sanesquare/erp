package com.accounts.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.bouncycastle.asn1.isismtt.x509.Restriction;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.PaymentDao;
import com.accounts.web.entities.Journal;
import com.accounts.web.entities.JournalEntries;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.Payment;
import com.accounts.web.entities.PaymentEntries;
import com.accounts.web.entities.Receipts;
import com.accounts.web.entities.Transactions;
import com.accounts.web.vo.JournalItemsVo;
import com.accounts.web.vo.JournalVo;
import com.accounts.web.vo.PaymentItemsVo;
import com.accounts.web.vo.PaymentVo;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.vo.ErpCurrencyVo;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.purchase.web.entities.Bill;
import com.purchase.web.entities.BillInstallments;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
@Repository
public class PaymentDaoImpl extends AbstractDao implements PaymentDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private ErpCurrencyDao erpCurrencyDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	public Long saveOrUpdatePayment(PaymentVo paymentVo) {
		Payment payment = null;
		Long id = paymentVo.getPaymentId();
		if (id == null) {
			// save
			payment = new Payment();
			Company company = companyDao.findCompany(paymentVo.getCompanyId());
			payment.setPaymentNumber(this.getPaymentNumberForCompany(paymentVo.getCompanyId()));
			payment.setCompany(company);
			payment.setIsDependant(false);
			payment = this.setAttributesForPayment(payment, paymentVo, true);
			id = (Long) this.sessionFactory.getCurrentSession().save(payment);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.PAYMENT, company);
		} else {
			// update
			payment = findPaymentObjectById(id);
			payment.setIsDependant(false);
			payment = this.setAttributesForPayment(payment, paymentVo, true);
			this.sessionFactory.getCurrentSession().merge(payment);
		}
		return id;
	}

	/**
	 * Method to set attributes for payment
	 * 
	 * @param payment
	 * @param paymentVo
	 * @return
	 */
	public Payment setAttributesForPayment(Payment payment, PaymentVo paymentVo, Boolean withCurrency) {
		ErpCurrencyVo currencyVo = null;
		if (withCurrency)
			currencyVo = erpCurrencyDao.findCurrencyForDate(DateFormatter.convertStringToDate(paymentVo.getDate()),
					payment.getCompany().getCurrency().getBaseCurrency());
		;
		if (currencyVo != null) {
			payment.setAmount(paymentVo.getAmount().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
		} else {
			payment.setAmount(paymentVo.getAmount());
		}
		payment.setPaymentDate(DateFormatter.convertStringToDate(paymentVo.getDate()));
		payment.setNarration(paymentVo.getNarration());
		for (PaymentItemsVo itemsVo : paymentVo.getEntries()) {
			Ledger ledger = ledgerDao.findLedgerObjectById(itemsVo.getAccTypeId());
			String accountType = ledger.getLedgerGroup().getAccountType().getType();
			BigDecimal plusAmount = new BigDecimal(0);
			BigDecimal minusAmount = new BigDecimal(0);
			// byLedger
			PaymentEntries entries = (PaymentEntries) this.sessionFactory.getCurrentSession()
					.createCriteria(PaymentEntries.class).add(Restrictions.eq("id", itemsVo.getId())).uniqueResult();
			Transactions transaction = null;
			if (entries == null) {
				entries = new PaymentEntries();
				transaction = new Transactions();
				transaction.setPaymentEntries(entries);
				transaction.setCompany(payment.getCompany());
			} else
				transaction = entries.getTransactions();
			entries.setLedger(ledger);
			if (currencyVo != null) {
				entries.setDebit(itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
				entries.setCredit(itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));

				if (accountType.contains("Expense")) {
					plusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					minusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Income")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Asset")) {
					plusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					minusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Liability")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Equity")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				}

			} else {
				entries.setCredit(itemsVo.getCreditAmnt());

				if (accountType.contains("Expense")) {
					plusAmount = itemsVo.getDebitAmnt();
					minusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Income")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Asset")) {
					plusAmount = itemsVo.getDebitAmnt();
					minusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Liability")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Equity")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				}

				entries.setDebit(itemsVo.getDebitAmnt());
			}
			transaction.setDate(payment.getPaymentDate());
			transaction.setLedger(ledger);
			transaction.setMinusAmount(minusAmount);
			transaction.setPlusAmount(plusAmount);
			transaction.setPaymentEntries(entries);
			entries.setTransactions(transaction);
			entries.setPayment(payment);
			payment.getEntries().add(entries);

		}
		return payment;
	}

	public void deletePayment(Long id) {
		Payment payment = findPaymentObjectById(id);
		if (payment == null)
			throw new ItemNotFoundException("Payment Does Not Exist");
		BillInstallments billInstallments = payment.getBillInstallments();
		this.sessionFactory.getCurrentSession().delete(payment);
		if (billInstallments != null) {
			Bill bill = billInstallments.getBill();
			bill.setIsSettled(false);
			billInstallments.setIsPaid(false);
			billInstallments.setPayment(null);
			bill.getBillInstallments().add(billInstallments);
			this.sessionFactory.getCurrentSession().merge(bill);
		}
	}

	public PaymentVo findPaymentById(Long id) {
		Payment payment = findPaymentObjectById(id);
		return this.createPaymentVo(payment);
	}

	private PaymentVo createPaymentVo(Payment payment) {

		ErpCurrencyVo currencyVo = erpCurrencyDao.findCurrencyForDate(payment.getPaymentDate(), payment.getCompany()
				.getCurrency().getBaseCurrency());
		PaymentVo vo = new PaymentVo();
		vo.setCreatedBy(payment.getCreatedBy().getUserName());
		vo.setPaymentId(payment.getId());
		vo.setCompanyId(payment.getCompany().getId());
		vo.setIsDependant(payment.getIsDependant());
		vo.setDate(DateFormatter.convertDateToString(payment.getPaymentDate()));
		vo.setVoucherNumber(payment.getPaymentNumber());
		vo.setNarration(payment.getNarration());
		List<PaymentItemsVo> itemsVos = new ArrayList<PaymentItemsVo>();
		HashMap<Long, BigDecimal> byLedgers = new HashMap<Long, BigDecimal>();
		HashMap<Long, BigDecimal> toLedgers = new HashMap<Long, BigDecimal>();
		Long byLedgerId = null;
		Long toLedgerId = null;
		Ledger byLedger = null;
		Ledger toLedger = null;
		for (PaymentEntries entry : payment.getEntries()) {
			PaymentItemsVo itemsVo = new PaymentItemsVo();
			Ledger ledger = entry.getLedger();
			itemsVo.setAccTypeId(ledger.getId());
			itemsVo.setLedger(ledger.getLedgerName());
			if (currencyVo != null) {

				itemsVo.setDebitAmnt(entry.getDebit().multiply(currencyVo.getRate())
						.setScale(2, RoundingMode.HALF_EVEN));
				itemsVo.setCreditAmnt(entry.getCredit().multiply(currencyVo.getRate())
						.setScale(2, RoundingMode.HALF_EVEN));

			} else {
				itemsVo.setCreditAmnt(entry.getCredit());
				itemsVo.setDebitAmnt(entry.getDebit());

			}
			if (entry.getDebit().compareTo(new BigDecimal(0)) != 0) {
				byLedger = entry.getLedger();
				byLedgers.put(entry.getId(), itemsVo.getDebitAmnt());
			} else if (entry.getCredit().compareTo(new BigDecimal(0)) != 0) {
				toLedger = entry.getLedger();
				toLedgers.put(entry.getId(), itemsVo.getCreditAmnt());
			}

			itemsVo.setId(entry.getId());
			itemsVos.add(itemsVo);
		}
		vo.setByLedgerIds(byLedgers);
		vo.setToLedgerIds(toLedgers);
		if (vo.getByLedgerIds().size() > 1) {
			vo.setFromLedger("Multiple Accounts");
		} else if (vo.getByLedgerIds().size() == 1) {
			vo.setFromLedger(byLedger.getLedgerName());
		}
		if (vo.getToLedgerIds().size() > 1) {
			vo.setToLedger("Multiple Accounts");
		} else if (vo.getToLedgerIds().size() == 1) {
			vo.setToLedger(toLedger.getLedgerName());
		}
		vo.setEntries(itemsVos);
		if (currencyVo != null) {
			vo.setAmount(payment.getAmount().multiply(currencyVo.getRate()).setScale(2, RoundingMode.HALF_EVEN));
		} else {
			vo.setAmount(payment.getAmount());
		}
		return vo;

	}

	public Payment findPaymentObjectById(Long id) {
		Payment payment = (Payment) this.sessionFactory.getCurrentSession().createCriteria(Payment.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
		return payment;
	}

	public List<PaymentVo> findAllPaymentsForCompany(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Does Not Exist");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Payment.class)
				.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Payment> payments = criteria.list();
		List<PaymentVo> paymentVos = new ArrayList<PaymentVo>();
		for (Payment payment : payments) {
			paymentVos.add(this.createPaymentVo(payment));
		}
		return paymentVos;
	}

	public List<PaymentVo> findAllPaymentsForCompanyWithinDate(Long companyId, String start, String end) {
		Company company = companyDao.findCompany(companyId);
		Date strt = DateFormatter.convertStringToDate(start);
		Date endDate = DateFormatter.convertStringToDate(end);
		if (company == null)
			throw new ItemNotFoundException("Company Does Not Exist");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Payment.class)
				.add(Restrictions.eq("company", company)).add(Restrictions.ge("paymentDate", strt))
				.add(Restrictions.le("paymentDate", endDate));

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Payment> payments = criteria.list();
		List<PaymentVo> paymentVos = new ArrayList<PaymentVo>();
		for (Payment payment : payments) {
			paymentVos.add(this.createPaymentVo(payment));
		}
		return paymentVos;
	}

	public List<PaymentVo> findAllPaymentsForLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		Date start = DateFormatter.convertStringToDate(startDate);
		Date end = DateFormatter.convertStringToDate(endDate);
		Ledger ledger = ledgerDao.findLedgerObjectById(ledgerId);
		if (ledger == null)
			throw new ItemNotFoundException("Ledger Not Found");
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Does Not Exist");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Payment.class)
				.add(Restrictions.eq("company", company)).add(Restrictions.between("paymentDate", start, end));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Payment> payments = criteria.list();

		List<PaymentVo> paymentVos = new ArrayList<PaymentVo>();
		for (Payment payment : payments) {
			paymentVos.add(this.createPaymentVo(payment));
		}
		return paymentVos;
	}

	public List<PaymentVo> findAllPaymentsForToLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<PaymentVo> findAllPaymentsForByLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPaymentNumberForCompany(Long companyId) {
		NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(NumberPropertyConstants.PAYMENT,
				companyId);
		String code = numberPropertyVo.getPrefix() + String.valueOf(numberPropertyVo.getNumber());
		return code;
	}

	/**
	 * Method to save or update payment
	 * 
	 * @param paymentVo
	 * @return
	 */
	/*
	 * public Long saveOrUpdatePayment(PaymentVo paymentVo) { Long id =
	 * paymentVo.getPaymentId(); Payment payment = null; if (id == null) {
	 * payment = new Payment();
	 * 
	 * Company company = companyDao.findCompany(paymentVo.getCompanyId());
	 * Transactions byTransaction = new Transactions();
	 * byTransaction.setPayment(payment); byTransaction.setCompany(company);
	 * Transactions toTransaction = new Transactions();
	 * toTransaction.setPayment(payment); toTransaction.setCompany(company);
	 * payment.setCompany(company);
	 * payment.getTransactions().add(toTransaction);
	 * payment.getTransactions().add(byTransaction);
	 * 
	 * payment.setCompany(company); payment =
	 * this.setAttributesForPayment(payment, paymentVo); id = (Long)
	 * this.sessionFactory.getCurrentSession().save(payment);
	 * numberPropertyDao.incrementNumberProperty(AccountsConstants.PAYMENT,
	 * company); } else { payment =
	 * findPaymentObjectById(paymentVo.getPaymentId()); payment =
	 * this.setAttributesForPayment(payment, paymentVo);
	 * this.sessionFactory.getCurrentSession().merge(payment); } return id; }
	 *//**
	 * Method to set attributes for payment
	 * 
	 * @param payment
	 * @param paymentVo
	 * @return
	 */
	/*
	 * private Payment setAttributesForPayment(Payment payment, PaymentVo
	 * paymentVo) {
	 * 
	 * ErpCurrencyVo currencyVo = erpCurrencyDao.findCurrencyForDate(
	 * DateFormatter.convertStringToDate(paymentVo.getDate()),
	 * payment.getCompany().getCurrency() .getBaseCurrency()); if (currencyVo !=
	 * null) {
	 * payment.setAmount(paymentVo.getAmount().divide(currencyVo.getRate(), 2,
	 * RoundingMode.HALF_EVEN)); } else {
	 * payment.setAmount(paymentVo.getAmount()); }
	 * 
	 * Ledger byLedger =
	 * ledgerDao.findLedgerObjectById(paymentVo.getByLedgerId()); Ledger
	 * toLedger = ledgerDao.findLedgerObjectById(paymentVo.getToLedgerId());
	 * String byAccount =
	 * byLedger.getLedgerGroup().getAccountType().getType().trim(); String
	 * toAccount = toLedger.getLedgerGroup().getAccountType().getType().trim();
	 * payment.setPaymentDate(DateFormatter.convertStringToDate(paymentVo.
	 * getDate())); Set<Transactions> transactions = payment.getTransactions();
	 * 
	 * Boolean byFlag = true; Boolean toFlag = true; for (Transactions
	 * transaction : transactions) { BigDecimal plusAmnt = new BigDecimal(0.00);
	 * BigDecimal minusAmnt = new BigDecimal(0.00);
	 * transaction.setDate(payment.getPaymentDate()); if (byFlag) { if
	 * (byAccount.contains("Expense")) { plusAmnt = payment.getAmount(); } else
	 * if (byAccount.contains("Income")) { minusAmnt = payment.getAmount(); }
	 * else if (byAccount.contains("Asset")) { plusAmnt = payment.getAmount(); }
	 * else if (byAccount.contains("Liability")) { minusAmnt =
	 * payment.getAmount(); } else if (byAccount.contains("Equity")) { minusAmnt
	 * = payment.getAmount(); } transaction.setLedger(byLedger);
	 * transaction.setMinusAmount(minusAmnt);
	 * transaction.setPlusAmount(plusAmnt); byFlag = false; } else if (toFlag) {
	 * if (toAccount.contains("Expense")) { minusAmnt = payment.getAmount(); }
	 * else if (toAccount.contains("Income")) { plusAmnt = payment.getAmount();
	 * } else if (toAccount.contains("Asset")) { minusAmnt =
	 * payment.getAmount(); } else if (toAccount.contains("Liability")) {
	 * plusAmnt = payment.getAmount(); } else if (toAccount.contains("Equity"))
	 * { plusAmnt = payment.getAmount(); } transaction.setLedger(toLedger);
	 * transaction.setMinusAmount(minusAmnt);
	 * transaction.setPlusAmount(plusAmnt); toFlag = false; }
	 * payment.getTransactions().add(transaction); }
	 * 
	 * payment.setPaymentNumber(paymentVo.getVoucherNumber());
	 * payment.setPaymentDate(DateFormatter.convertStringToDate(paymentVo.
	 * getDate()));
	 * payment.setLedgerBy(ledgerDao.findLedgerObjectById(paymentVo.
	 * getByLedgerId()));
	 * payment.setLedgerTo(ledgerDao.findLedgerObjectById(paymentVo.
	 * getToLedgerId())); payment.setNarration(paymentVo.getNarration()); return
	 * payment; }
	 *//**
	 * Method to delete payment
	 * 
	 * @param id
	 */
	/*
	 * public void deletePayment(Long id) { Payment payment =
	 * findPaymentObjectById(id);
	 * this.sessionFactory.getCurrentSession().delete(payment);
	 * 
	 * }
	 *//**
	 * Method to find payment by id
	 * 
	 * @param id
	 * @return
	 */
	/*
	 * public PaymentVo findPaymentById(Long id) { Payment payment =
	 * findPaymentObjectById(id); return this.createPaymentVo(payment); }
	 *//**
	 * Method to create paymentVo
	 * 
	 * @param payment
	 * @return
	 */
	/*
	 * private PaymentVo createPaymentVo(Payment payment) {
	 * 
	 * PaymentVo paymentVo = new PaymentVo();
	 * 
	 * ErpCurrencyVo currencyVo =
	 * erpCurrencyDao.findCurrencyForDate(payment.getPaymentDate(),
	 * payment.getCompany() .getCurrency().getBaseCurrency()); if (currencyVo !=
	 * null) {
	 * paymentVo.setAmount(payment.getAmount().multiply(currencyVo.getRate()).
	 * setScale(2, RoundingMode.HALF_EVEN)); } else {
	 * paymentVo.setAmount(payment.getAmount()); }
	 * 
	 * paymentVo.setPaymentId(payment.getId());
	 * paymentVo.setCompanyId(payment.getCompany().getId());
	 * paymentVo.setDate(DateFormatter.convertDateToString(payment.
	 * getPaymentDate()));
	 * 
	 * Ledger ledger = payment.getLedgerBy(); if (ledger != null) {
	 * paymentVo.setByLedgerId(ledger.getId());
	 * paymentVo.setByLedger(ledger.getLedgerName()); } ledger =
	 * payment.getLedgerTo(); if (ledger != null) {
	 * paymentVo.setToLedgerId(ledger.getId());
	 * paymentVo.setToLedger(ledger.getLedgerName()); }
	 * paymentVo.setNarration(payment.getNarration());
	 * paymentVo.setVoucherNumber(payment.getPaymentNumber()); return paymentVo;
	 * }
	 *//**
	 * method to find payment object by id
	 * 
	 * @param id
	 * @return
	 */
	/*
	 * public Payment findPaymentObjectById(Long id) { Payment payment = null;
	 * Criteria criteria =
	 * this.sessionFactory.getCurrentSession().createCriteria(Payment.class);
	 * criteria.add(Restrictions.eq("id", id)); payment = (Payment)
	 * criteria.uniqueResult(); if (payment == null) throw new
	 * ItemNotFoundException("Payment Not Found"); return payment; }
	 *//**
	 * method to find all payments for company
	 * 
	 * @param companyId
	 * @return
	 */
	/*
	 * @SuppressWarnings("unchecked") public List<PaymentVo>
	 * findAllPaymentsForCompany(Long companyId) { Criteria criteria =
	 * this.sessionFactory.getCurrentSession().createCriteria(Payment.class);
	 * criteria.add(Restrictions.eq("company",
	 * companyDao.findCompany(companyId)));
	 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	 * 
	 * List<Payment> payments = criteria.list(); List<PaymentVo> vos = new
	 * ArrayList<PaymentVo>(); if (payments != null) { for (Payment payment :
	 * payments) { vos.add(this.createPaymentVo(payment)); } } return vos; }
	 *//**
	 * method to find all journals for company within date
	 * 
	 * @param companyId
	 * @return
	 */
	/*
	 * public List<PaymentVo> findAllPaymentsForCompanyWithinDate(Long
	 * companyId, String start, String end) { Date strt =
	 * DateFormatter.convertStringToDate(start); Date endDate =
	 * DateFormatter.convertStringToDate(end); List<PaymentVo> vos = new
	 * ArrayList<PaymentVo>(); Criteria criteria =
	 * this.sessionFactory.getCurrentSession().createCriteria(Payment.class);
	 * criteria.add(Restrictions.eq("company",
	 * companyDao.findCompany(companyId))) .add(Restrictions.le("paymentDate",
	 * endDate)).add(Restrictions.ge("paymentDate", strt));
	 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	 * 
	 * List<Payment> payments = criteria.list();
	 * 
	 * if (payments != null) { for (Payment payment : payments) {
	 * vos.add(this.createPaymentVo(payment)); } } return vos; }
	 *//**
	 * Method to find all payment for by ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	/*
	 * @SuppressWarnings("unchecked") public List<PaymentVo>
	 * findAllPaymentsForLedgerWithinDateRange(Long companyId, Long ledgerId,
	 * String startDate, String endDate) { Date start =
	 * DateFormatter.convertStringToDate(startDate); Date end =
	 * DateFormatter.convertStringToDate(endDate); Ledger ledger =
	 * ledgerDao.findLedgerObjectById(ledgerId); Criteria criteria =
	 * this.sessionFactory.getCurrentSession().createCriteria(Payment.class);
	 * criteria.add(Restrictions.eq("company",
	 * companyDao.findCompany(companyId)));
	 * criteria.add(Restrictions.between("paymentDate", start, end));
	 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	 * 
	 * Criterion byLedger = Restrictions.eq("ledgerBy", ledger); Criterion
	 * toLedger = Restrictions.eq("ledgerTo", ledger);
	 * 
	 * LogicalExpression exp = Restrictions.or(byLedger, toLedger);
	 * criteria.add(exp);
	 * 
	 * List<Payment> payments = criteria.list(); List<PaymentVo> vos = new
	 * ArrayList<PaymentVo>(); if (payments != null) { for (Payment payment :
	 * payments) { vos.add(this.createPaymentVo(payment)); } } return vos; }
	 *//**
	 * Method to find payments for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	/*
	 * @SuppressWarnings("unchecked") public List<PaymentVo>
	 * findAllPaymentsForToLedgerWithinDateRange(Long companyId, Long ledgerId,
	 * String startDate, String endDate) { Criteria criteria =
	 * this.sessionFactory.getCurrentSession().createCriteria(Payment.class);
	 * criteria.add(Restrictions.eq("company",
	 * companyDao.findCompany(companyId)));
	 * criteria.add(Restrictions.eq("ledgerTo",
	 * ledgerDao.findLedgerObjectById(ledgerId))); List<Payment> payments = new
	 * ArrayList<Payment>();
	 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); payments =
	 * criteria.list(); List<PaymentVo> vos = new ArrayList<PaymentVo>(); for
	 * (Payment payment : payments) { vos.add(this.createPaymentVo(payment)); }
	 * return vos; }
	 *//**
	 * Method to find payments for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	/*
	 * @SuppressWarnings("unchecked") public List<PaymentVo>
	 * findAllPaymentsForByLedgerWithinDateRange(Long companyId, Long ledgerId,
	 * String startDate, String endDate) {
	 * 
	 * Criteria criteria =
	 * this.sessionFactory.getCurrentSession().createCriteria(Payment.class);
	 * criteria.add(Restrictions.eq("company",
	 * companyDao.findCompany(companyId)));
	 * criteria.add(Restrictions.eq("ledgerBy",
	 * ledgerDao.findLedgerObjectById(ledgerId))); List<Payment> payments = new
	 * ArrayList<Payment>();
	 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); payments =
	 * criteria.list(); List<PaymentVo> vos = new ArrayList<PaymentVo>(); for
	 * (Payment payment : payments) { vos.add(this.createPaymentVo(payment)); }
	 * return vos; }
	 *//**
	 * Method to get payment voucher number
	 * 
	 * @param companyId
	 * @return
	 */
	/*
	 * public String getPaymentNumberForCompany(Long companyId) {
	 * NumberPropertyVo numberPropertyVo =
	 * numberPropertyDao.findNumberProperty(AccountsConstants.PAYMENT,
	 * companyId); String code = numberPropertyVo.getPrefix() +
	 * String.valueOf(numberPropertyVo.getNumber()); return code;
	 * 
	 * }
	 */
}
