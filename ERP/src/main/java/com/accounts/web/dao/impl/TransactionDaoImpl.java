package com.accounts.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.LedgerGroupDao;
import com.accounts.web.dao.TransactionDao;
import com.accounts.web.entities.Contra;
import com.accounts.web.entities.ContraEntries;
import com.accounts.web.entities.Journal;
import com.accounts.web.entities.JournalEntries;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.LedgerGroup;
import com.accounts.web.entities.Payment;
import com.accounts.web.entities.PaymentEntries;
import com.accounts.web.entities.ReceiptEntries;
import com.accounts.web.entities.Receipts;
import com.accounts.web.entities.Transactions;
import com.accounts.web.vo.BalanceSheetGroupVo;
import com.accounts.web.vo.BalanceSheetLedgerVo;
import com.accounts.web.vo.BalanceSheetVo;
import com.accounts.web.vo.LedgerGroupViewVo;
import com.accounts.web.vo.LedgerTransactionViewVo;
import com.accounts.web.vo.LedgerViewMonthlyVo;
import com.accounts.web.vo.LedgerViewVo;
import com.accounts.web.vo.LedgerVo;
import com.accounts.web.vo.ProfitAndLossGroupVo;
import com.accounts.web.vo.ProfitAndLossLedgerVo;
import com.accounts.web.vo.ProfitAndLossVo;
import com.accounts.web.vo.TrialBalanceLedgerGroupVo;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.entities.Company;
import com.erp.web.vo.ErpCurrencyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.entities.BaseCurrency;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;
import com.inventory.web.dao.InventoryTransactionDao;
import com.inventory.web.vo.StockViewVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 21-August-2015
 *
 */
@Repository
public class TransactionDaoImpl extends AbstractDao implements TransactionDao {

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private LedgerGroupDao groupDao;

	@Autowired
	private ErpCurrencyDao currencyDao;

	@Autowired
	private InventoryTransactionDao inventoryTransactionDao;

	@SuppressWarnings({ "unchecked", "static-access" })
	public LedgerVo findTRansactionsForLedger(String from, String to, Ledger ledger, Company company,
			BaseCurrency baseCurrency) {
		if (company == null)
			throw new ItemNotFoundException("Company not found");
		if (ledger == null)
			throw new ItemNotFoundException("Ledger not found");
		Date startDate = DateFormatter.convertStringToDate(from);
		Date endDate = DateFormatter.convertStringToDate(to);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transactions.class);
		criteria.add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("ledger", ledger));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);

		List<Transactions> transactions = criteria.list();
		ErpCurrencyVo currecny = currencyDao.findCurrencyForDate(ledger.getStartDate(), baseCurrency);
		BigDecimal openingBalance = null;
		BigDecimal balance = null;
		if (currecny != null) {
			openingBalance = ledger.getOpeningBalance().multiply(currecny.getRate())
					.setScale(3, RoundingMode.HALF_EVEN);
			balance = ledger.getOpeningBalance().multiply(currecny.getRate()).setScale(3, RoundingMode.HALF_EVEN);
		}

		BigDecimal credit = new BigDecimal("0.00");
		BigDecimal debit = new BigDecimal("0.00");

		LedgerVo ledgerVo = new LedgerVo();
		ledgerVo.setAccountType(ledger.getLedgerGroup().getAccountType().getType());
		for (Transactions transaction : transactions) {

			ErpCurrencyVo currencyVo = currencyDao.findCurrencyForDate(transaction.getDate(), baseCurrency);
			if (currencyVo != null) {
				if (openingBalance == null)
					openingBalance = ledger.getOpeningBalance().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN);
				if (balance == null)
					balance = ledger.getOpeningBalance().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN);
			} else {
				if (openingBalance == null)
					openingBalance = new BigDecimal("0.00");
				if (balance == null)
					balance = new BigDecimal("0.00");
			}

			Date transactionDate = transaction.getDate();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(transactionDate);
			calendar.add(Calendar.DATE, 1);
			Date transactionPlusOne = calendar.getTime();
			if (transactionPlusOne.compareTo(startDate) <= 0) {
				if (currencyVo != null) {
					openingBalance = openingBalance.subtract(transaction.getMinusAmount()
							.multiply(currencyVo.getRate()));
					openingBalance = openingBalance.add(transaction.getPlusAmount().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN));
				} else {
					openingBalance = openingBalance.subtract(transaction.getMinusAmount());
					openingBalance = openingBalance.add(transaction.getPlusAmount());
				}
				balance = openingBalance;
			} else {
				if (currencyVo != null) {
					balance = balance.subtract(transaction.getMinusAmount().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN));
					balance = balance.add(transaction.getPlusAmount().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN));
					credit = credit.add(transaction.getMinusAmount().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN));
					debit = debit.add(transaction.getPlusAmount().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN));
				} else {
					balance = balance.subtract(transaction.getMinusAmount());
					balance = balance.add(transaction.getPlusAmount());
					credit = credit.add(transaction.getMinusAmount());
					debit = debit.add(transaction.getPlusAmount());
				}
			}
		}
		ledgerVo.setCredit(credit);
		ledgerVo.setDebit(debit);
		ledgerVo.setBalance(balance);
		ledgerVo.setOpeningBalance(openingBalance);
		ledgerVo.setLedgerName(ledger.getLedgerName());
		ledgerVo.setLedgerId(ledger.getId());
		ledgerVo.setLedgerGroup(ledger.getLedgerGroup().getGroupName());
		return ledgerVo;
	}

	public Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> trialBalanceCalculations(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		return this.trialBalanceCalculations(companyId, DateFormatter.convertDateToString(company.getPeriodStart()),
				DateFormatter.convertDateToString(company.getPeriodEnd()));
	}

	public Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> trialBalanceCalculations(Long companyId, String start,
			String end) {
		Company company = companyDao.findCompany(companyId);

		if (company == null)
			throw new ItemNotFoundException("Company Not Found");

		List<LedgerVo> ledgerVos = new ArrayList<LedgerVo>();
		Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> trialBalanceEntries = new HashMap<TrialBalanceLedgerGroupVo, List<LedgerVo>>();
		List<LedgerVo> subResult = new ArrayList<LedgerVo>();

		BaseCurrency baseCurrency = company.getCurrency().getBaseCurrency();
		for (Ledger ledger : company.getLedgers()) {
			ledgerVos.add(this.findTRansactionsForLedger(start, end, ledger, company, baseCurrency));
		}
		BigDecimal totalDebit = new BigDecimal("0.00");
		BigDecimal totalCredit = new BigDecimal("0.00");
		BigDecimal totalOpening = new BigDecimal("0.00");
		BigDecimal totalClose = new BigDecimal("0.00");
		BigDecimal totalOpeningDebit = new BigDecimal("0.00");
		BigDecimal totalOpeningCredit = new BigDecimal("0.00");
		BigDecimal totalTransactionDebit = new BigDecimal("0.00");
		BigDecimal totalTransactionCredit = new BigDecimal("0.00");
		for (LedgerVo vo : ledgerVos) {
			if (vo.getBalance().compareTo(BigDecimal.ZERO) == 0)
				vo.setBalance(new BigDecimal("0.00"));
			if (vo.getOpeningBalance().compareTo(BigDecimal.ZERO) == 0)
				vo.setOpeningBalance(new BigDecimal("0.00"));
			BigDecimal transactionAmount = vo.getDebit().subtract(vo.getCredit());
			BigDecimal amount = vo.getDebit().subtract(vo.getCredit()).add(vo.getOpeningBalance());
			BigDecimal opening = vo.getOpeningBalance();
			vo.setDebit(new BigDecimal("0.00"));
			vo.setCredit(new BigDecimal("0.00"));
			String groupName = vo.getLedgerGroup();
			totalOpening = totalOpening.add(vo.getOpeningBalance());
			totalClose = totalClose.add(vo.getBalance());
			Set<TrialBalanceLedgerGroupVo> keys = trialBalanceEntries.keySet();
			Boolean found = false;
			for (TrialBalanceLedgerGroupVo key : keys) {
				if (key.getGroup().equalsIgnoreCase(groupName)) {
					key.setClosing(key.getClosing().add(vo.getBalance()));
					key.setOpening(key.getOpening().add(vo.getOpeningBalance()));
					key.setAccountType(vo.getAccountType());

					// sorting according to account type
					if (vo.getAccountType().contains("Asset")) {
						if (amount.compareTo(BigDecimal.ZERO) >= 0) {
							key.setDebit(key.getDebit().add(amount));
							vo.setDebit(amount);
							totalDebit = totalDebit.add(vo.getDebit());
						} else {
							amount = amount.abs();
							key.setCredit(key.getCredit().add(amount));
							vo.setCredit(amount);
							totalCredit = totalCredit.add(vo.getCredit());
						}
						// updates 03/11/2015
						if (opening.compareTo(BigDecimal.ZERO) >= 0) {
							key.setOpeningDebit(key.getOpeningDebit().add(opening));
							vo.setOpeningDebit(opening);
							totalOpeningDebit = totalOpeningDebit.add(opening);
						} else {
							opening = opening.abs();
							key.setOpeningCredit(key.getOpeningCredit().add(opening));
							vo.setOpeningCredit(opening);
							totalOpeningCredit = totalOpeningCredit.add(opening);
						}
						if (transactionAmount.compareTo(BigDecimal.ZERO) >= 0) {
							key.setTransactionDebit(key.getTransactionDebit().add(transactionAmount));
							vo.setTransactionDebit(transactionAmount);
							totalTransactionDebit = totalTransactionDebit.add(transactionAmount);
						} else {
							transactionAmount = transactionAmount.abs();
							key.setTransactionCredit(key.getTransactionCredit().add(transactionAmount));
							vo.setTransactionCredit(transactionAmount);
							totalTransactionCredit = totalTransactionCredit.add(transactionAmount);
						}
					} else if (vo.getAccountType().contains("Liability")) {
						if (amount.compareTo(BigDecimal.ZERO) >= 0) {
							key.setCredit(key.getCredit().add(amount));
							vo.setCredit(amount);
							totalCredit = totalCredit.add(vo.getCredit());
						} else {
							amount = amount.abs();
							vo.setDebit(amount);
							key.setDebit(key.getDebit().add(amount));
							totalDebit = totalDebit.add(vo.getDebit());
						}
						if (opening.compareTo(BigDecimal.ZERO) >= 0) {
							key.setOpeningCredit(key.getOpeningCredit().add(opening));
							vo.setOpeningCredit(opening);
							totalOpeningCredit = totalOpeningCredit.add(opening);
						} else {
							opening = opening.abs();
							key.setOpeningDebit(key.getOpeningDebit().add(opening));
							vo.setOpeningDebit(opening);
							totalOpeningDebit = totalOpeningDebit.add(opening);
						}
						if (transactionAmount.compareTo(BigDecimal.ZERO) >= 0) {
							key.setTransactionCredit(key.getTransactionCredit().add(transactionAmount));
							vo.setTransactionCredit(transactionAmount);
							totalTransactionCredit = totalTransactionCredit.add(transactionAmount);
						} else {
							transactionAmount = transactionAmount.abs();
							key.setTransactionDebit(key.getTransactionDebit().add(transactionAmount));
							vo.setTransactionDebit(transactionAmount);
							totalTransactionDebit = totalTransactionDebit.add(transactionAmount);
						}
					} else if (vo.getAccountType().contains("Income")) {
						if (amount.compareTo(BigDecimal.ZERO) >= 0) {
							key.setCredit(key.getCredit().add(amount));
							vo.setCredit(amount);
							totalCredit = totalCredit.add(vo.getCredit());
						} else {
							amount = amount.abs();
							vo.setDebit(amount);
							key.setDebit(key.getDebit().add(amount));
							totalDebit = totalDebit.add(vo.getDebit());
						}
						if (opening.compareTo(BigDecimal.ZERO) >= 0) {
							key.setOpeningCredit(key.getOpeningCredit().add(opening));
							vo.setOpeningCredit(opening);
							totalOpeningCredit = totalOpeningCredit.add(opening);
						} else {
							opening = opening.abs();
							key.setOpeningDebit(key.getOpeningDebit().add(opening));
							vo.setOpeningDebit(opening);
							totalOpeningDebit = totalOpeningDebit.add(opening);
						}
						if (transactionAmount.compareTo(BigDecimal.ZERO) >= 0) {
							key.setTransactionCredit(key.getTransactionCredit().add(transactionAmount));
							vo.setTransactionCredit(transactionAmount);
							totalTransactionCredit = totalTransactionCredit.add(transactionAmount);
						} else {
							transactionAmount = transactionAmount.abs();
							key.setTransactionDebit(key.getTransactionDebit().add(transactionAmount));
							vo.setTransactionDebit(transactionAmount);
							totalTransactionDebit = totalTransactionDebit.add(transactionAmount);
						}
					} else if (vo.getAccountType().contains("Expense")) {
						if (amount.compareTo(BigDecimal.ZERO) >= 0) {
							vo.setDebit(amount);
							key.setDebit(key.getDebit().add(amount));
							totalDebit = totalDebit.add(vo.getDebit());
						} else {
							amount = amount.abs();
							key.setCredit(key.getCredit().add(amount));
							vo.setCredit(amount);
							totalCredit = totalCredit.add(vo.getCredit());
						}
						if (opening.compareTo(BigDecimal.ZERO) >= 0) {
							key.setOpeningDebit(key.getOpeningDebit().add(opening));
							vo.setOpeningDebit(opening);
							totalOpeningDebit = totalOpeningDebit.add(opening);
						} else {
							opening = opening.abs();
							key.setOpeningCredit(key.getOpeningCredit().add(opening));
							vo.setOpeningCredit(opening);
							totalOpeningCredit = totalOpeningCredit.add(opening);
						}
						if (transactionAmount.compareTo(BigDecimal.ZERO) >= 0) {
							key.setTransactionDebit(key.getTransactionDebit().add(transactionAmount));
							vo.setTransactionDebit(transactionAmount);
							totalTransactionDebit = totalTransactionDebit.add(transactionAmount);
						} else {
							transactionAmount = transactionAmount.abs();
							key.setTransactionCredit(key.getTransactionCredit().add(transactionAmount));
							vo.setTransactionCredit(transactionAmount);
							totalTransactionCredit = totalTransactionCredit.add(transactionAmount);
						}
					} else if (vo.getAccountType().contains("Equity")) {
						if (amount.compareTo(BigDecimal.ZERO) >= 0) {
							key.setCredit(key.getCredit().add(amount));
							vo.setCredit(amount);
							totalCredit = totalCredit.add(vo.getCredit());
						} else {
							amount = amount.abs();
							vo.setDebit(amount);
							key.setDebit(key.getDebit().add(amount));
							totalDebit = totalDebit.add(vo.getDebit());
						}
						if (opening.compareTo(BigDecimal.ZERO) >= 0) {
							key.setOpeningCredit(key.getOpeningCredit().add(opening));
							vo.setOpeningCredit(opening);
							totalOpeningCredit = totalOpeningCredit.add(opening);
						} else {
							opening = opening.abs();
							key.setOpeningDebit(key.getOpeningDebit().add(opening));
							vo.setOpeningDebit(opening);
							totalOpeningDebit = totalOpeningDebit.add(opening);
						}
						if (transactionAmount.compareTo(BigDecimal.ZERO) >= 0) {
							key.setTransactionCredit(key.getTransactionCredit().add(transactionAmount));
							vo.setTransactionCredit(transactionAmount);
							totalTransactionCredit = totalTransactionCredit.add(transactionAmount);
						} else {
							transactionAmount = transactionAmount.abs();
							key.setTransactionDebit(key.getTransactionDebit().add(transactionAmount));
							vo.setTransactionDebit(transactionAmount);
							totalTransactionDebit = totalTransactionDebit.add(transactionAmount);
						}
					}

					if (vo.getDebit().compareTo(BigDecimal.ZERO) == 0 && vo.getCredit().compareTo(BigDecimal.ZERO) == 0
							&& vo.getBalance().compareTo(BigDecimal.ZERO) == 0
							&& vo.getOpeningBalance().compareTo(BigDecimal.ZERO) == 0) {

					} else {
						List<LedgerVo> ledgerValues = trialBalanceEntries.get(key);
						ledgerValues.add(vo);
						trialBalanceEntries.put(key, ledgerValues);
					}
					found = true;
				}
			}
			if (!found) {
				TrialBalanceLedgerGroupVo group = new TrialBalanceLedgerGroupVo();
				group.setClosing(group.getClosing().add(vo.getBalance()));
				group.setOpening(group.getOpening().add(vo.getOpeningBalance()));
				group.setAccountType(vo.getAccountType());
				// sorting according to account type
				if (vo.getAccountType().contains("Asset")) {
					if (amount.compareTo(BigDecimal.ZERO) >= 0) {
						group.setDebit(group.getDebit().add(amount));
						vo.setDebit(amount);
						totalDebit = totalDebit.add(vo.getDebit());
					} else {
						amount = amount.abs();
						group.setCredit(group.getCredit().add(amount));
						vo.setCredit(amount.abs());
						totalCredit = totalCredit.add(vo.getCredit());
					}
					if (opening.compareTo(BigDecimal.ZERO) >= 0) {
						group.setOpeningDebit(group.getOpeningDebit().add(opening));
						vo.setOpeningDebit(opening);
						totalOpeningDebit = totalOpeningDebit.add(opening);
					} else {
						opening = opening.abs();
						group.setOpeningCredit(group.getOpeningCredit().add(opening));
						vo.setOpeningCredit(opening);
						totalOpeningCredit = totalOpeningCredit.add(opening);
					}
					if (transactionAmount.compareTo(BigDecimal.ZERO) >= 0) {
						group.setTransactionDebit(group.getTransactionDebit().add(transactionAmount));
						vo.setTransactionDebit(transactionAmount);
						totalTransactionDebit = totalTransactionDebit.add(transactionAmount);
					} else {
						transactionAmount = transactionAmount.abs();
						group.setTransactionCredit(group.getTransactionCredit().add(transactionAmount));
						vo.setTransactionCredit(transactionAmount);
						totalTransactionCredit = totalTransactionCredit.add(transactionAmount);
					}
				} else if (vo.getAccountType().contains("Liability")) {
					if (amount.compareTo(BigDecimal.ZERO) >= 0) {
						group.setCredit(group.getCredit().add(amount));
						vo.setCredit(amount);
						totalCredit = totalCredit.add(vo.getCredit());
					} else {
						amount = amount.abs();
						group.setDebit(group.getDebit().add(amount));
						vo.setDebit(amount);
						totalDebit = totalDebit.add(vo.getDebit());
					}
					if (opening.compareTo(BigDecimal.ZERO) >= 0) {
						group.setOpeningCredit(group.getOpeningCredit().add(opening));
						vo.setOpeningCredit(opening);
						totalOpeningCredit = totalOpeningCredit.add(opening);
					} else {
						opening = opening.abs();
						group.setOpeningDebit(group.getOpeningDebit().add(opening));
						vo.setOpeningDebit(opening);
						totalOpeningDebit = totalOpeningDebit.add(opening);
					}
					if (transactionAmount.compareTo(BigDecimal.ZERO) >= 0) {
						group.setTransactionCredit(group.getTransactionCredit().add(transactionAmount));
						vo.setTransactionCredit(transactionAmount);
						totalTransactionCredit = totalTransactionCredit.add(transactionAmount);
					} else {
						transactionAmount = transactionAmount.abs();
						group.setTransactionDebit(group.getTransactionDebit().add(transactionAmount));
						vo.setTransactionDebit(transactionAmount);
						totalTransactionDebit = totalTransactionDebit.add(transactionAmount);
					}
				} else if (vo.getAccountType().contains("Income")) {
					if (amount.compareTo(BigDecimal.ZERO) >= 0) {
						group.setCredit(group.getCredit().add(amount));
						vo.setCredit(amount);
						totalCredit = totalCredit.add(vo.getCredit());
					} else {
						amount = amount.abs();
						group.setDebit(group.getDebit().add(amount));
						vo.setDebit(amount);
						totalDebit = totalDebit.add(vo.getDebit());

					}
					if (opening.compareTo(BigDecimal.ZERO) >= 0) {
						group.setOpeningCredit(group.getOpeningCredit().add(opening));
						vo.setOpeningCredit(opening);
						totalOpeningCredit = totalOpeningCredit.add(opening);
					} else {
						opening = opening.abs();
						group.setOpeningDebit(group.getOpeningDebit().add(opening));
						vo.setOpeningDebit(opening);
						totalOpeningDebit = totalOpeningDebit.add(opening);
					}
					if (transactionAmount.compareTo(BigDecimal.ZERO) >= 0) {
						group.setTransactionCredit(group.getTransactionCredit().add(transactionAmount));
						vo.setTransactionCredit(transactionAmount);
						totalTransactionCredit = totalTransactionCredit.add(transactionAmount);
					} else {
						transactionAmount = transactionAmount.abs();
						group.setTransactionDebit(group.getTransactionDebit().add(transactionAmount));
						vo.setTransactionDebit(transactionAmount);
						totalTransactionDebit = totalTransactionDebit.add(transactionAmount);
					}
				} else if (vo.getAccountType().contains("Expense")) {
					if (amount.compareTo(BigDecimal.ZERO) >= 0) {
						vo.setDebit(amount);
						group.setDebit(group.getDebit().add(amount));
						totalDebit = totalDebit.add(vo.getDebit());
					} else {
						amount = amount.abs();
						group.setCredit(group.getCredit().add(amount));
						vo.setCredit(amount);
						totalCredit = totalCredit.add(vo.getCredit());
					}
					if (opening.compareTo(BigDecimal.ZERO) >= 0) {
						group.setOpeningDebit(group.getOpeningDebit().add(opening));
						vo.setOpeningDebit(opening);
						totalOpeningDebit = totalOpeningDebit.add(opening);
					} else {
						opening = opening.abs();
						group.setOpeningCredit(group.getOpeningCredit().add(opening));
						vo.setOpeningCredit(opening);
						totalOpeningCredit = totalOpeningCredit.add(opening);
					}
					if (transactionAmount.compareTo(BigDecimal.ZERO) >= 0) {
						group.setTransactionDebit(group.getTransactionDebit().add(transactionAmount));
						vo.setTransactionDebit(transactionAmount);
						totalTransactionDebit = totalTransactionDebit.add(transactionAmount);
					} else {
						transactionAmount = transactionAmount.abs();
						group.setTransactionCredit(group.getTransactionCredit().add(transactionAmount));
						vo.setTransactionCredit(transactionAmount);
						totalTransactionCredit = totalTransactionCredit.add(transactionAmount);
					}
				} else if (vo.getAccountType().contains("Equity")) {
					if (amount.compareTo(BigDecimal.ZERO) >= 0) {
						group.setCredit(group.getCredit().add(amount));
						vo.setCredit(amount);
						totalCredit = totalCredit.add(vo.getCredit());
					} else {
						amount = amount.abs();
						vo.setDebit(amount);
						group.setDebit(group.getDebit().add(amount));
						totalDebit = totalDebit.add(vo.getDebit());
					}
					if (opening.compareTo(BigDecimal.ZERO) >= 0) {
						group.setOpeningCredit(group.getOpeningCredit().add(opening));
						vo.setOpeningCredit(opening);
						totalOpeningCredit = totalOpeningCredit.add(opening);
					} else {
						opening = opening.abs();
						group.setOpeningDebit(group.getOpeningDebit().add(opening));
						vo.setOpeningDebit(opening);
						totalOpeningDebit = totalOpeningDebit.add(opening);
					}
					if (transactionAmount.compareTo(BigDecimal.ZERO) >= 0) {
						group.setTransactionCredit(group.getTransactionCredit().add(transactionAmount));
						vo.setTransactionCredit(transactionAmount);
						totalTransactionCredit = totalTransactionCredit.add(transactionAmount);
					} else {
						transactionAmount = transactionAmount.abs();
						group.setTransactionDebit(group.getTransactionDebit().add(transactionAmount));
						vo.setTransactionDebit(transactionAmount);
						totalTransactionDebit = totalTransactionDebit.add(transactionAmount);
					}
				}

				if (vo.getDebit().compareTo(BigDecimal.ZERO) == 0 && vo.getCredit().compareTo(BigDecimal.ZERO) == 0
						&& vo.getBalance().compareTo(BigDecimal.ZERO) == 0
						&& vo.getOpeningBalance().compareTo(BigDecimal.ZERO) == 0) {

				} else {
					group.setGroup(groupName);
					List<LedgerVo> ledgerVos2 = new ArrayList<LedgerVo>();
					ledgerVos2.add(vo);
					trialBalanceEntries.put(group, ledgerVos2);
				}
			}
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DateFormatter.convertStringToDate(start));
		calendar.add(Calendar.DATE, -1);
		List<StockViewVo> stockViewVosOpening = inventoryTransactionDao.findStockSummary(company.getId(),
				DateFormatter.convertDateToString(company.getPeriodStart()),
				DateFormatter.convertDateToString(calendar.getTime()));
		BigDecimal openingStockAmount = new BigDecimal("0.00");
		for (StockViewVo stockViewVo : stockViewVosOpening) {
			openingStockAmount = openingStockAmount.add(stockViewVo.getTotalAmount())
					.add(stockViewVo.getClosingValue());
		}
		TrialBalanceLedgerGroupVo openingStock = new TrialBalanceLedgerGroupVo();
		openingStock.setAccountType("Opening Stock");
		openingStock.setGroup("Opening Stock");
		if (openingStockAmount.compareTo(BigDecimal.ZERO) >= 0)
			openingStock.setDebit(openingStockAmount);
		else
			openingStock.setCredit(openingStockAmount);
		List<LedgerVo> openingStockLedgerVos = new ArrayList<LedgerVo>();
		trialBalanceEntries.put(openingStock, openingStockLedgerVos);

		List<StockViewVo> stockViewVosClosing = inventoryTransactionDao.findStockSummary(company.getId(), start, end);
		BigDecimal closingStockAmount = new BigDecimal("0.00");
		for (StockViewVo stockViewVo : stockViewVosClosing) {
			closingStockAmount = closingStockAmount.add(stockViewVo.getTotalAmount())
					.add(stockViewVo.getClosingValue());
		}
		TrialBalanceLedgerGroupVo closingStock = new TrialBalanceLedgerGroupVo();
		closingStock.setAccountType("Closing Stock");
		closingStock.setGroup("Closing Stock");
		if (closingStockAmount.compareTo(BigDecimal.ZERO) >= 0)
			closingStock.setDebit(closingStockAmount);
		else
			closingStock.setCredit(closingStockAmount);
		trialBalanceEntries.put(closingStock, openingStockLedgerVos);

		LedgerVo totalVo = new LedgerVo();
		totalVo.setTotalClose(totalClose);
		totalVo.setTotalCredit(totalCredit.add(openingStock.getCredit()));
		totalVo.setTotalDebit(totalDebit.add(openingStock.getDebit()));
		totalVo.setTotalOpening(totalOpening);
		totalVo.setTotalOpeningCredit(totalOpeningCredit);
		totalVo.setTotalOpeningDebit(totalOpeningDebit);
		totalVo.setTotalTransactionCredit(totalTransactionCredit);
		totalVo.setTotalTransactionDebit(totalTransactionDebit);
		subResult.add(totalVo);
		TrialBalanceLedgerGroupVo group = new TrialBalanceLedgerGroupVo();
		group.setGroup("grand_total!@#");
		group.setAccountType("Total");
		trialBalanceEntries.put(group, subResult);

		List<String> accountTypes = new ArrayList<String>();
		accountTypes.add("Opening Stock");
		accountTypes.add("Asset");
		accountTypes.add("Liability");
		accountTypes.add("Equity");
		accountTypes.add("Expense");
		accountTypes.add("Income");
		accountTypes.add("Total");
		accountTypes.add("Closing Stock");

		Set<TrialBalanceLedgerGroupVo> keys = trialBalanceEntries.keySet();
		Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> sortedTrialBalanceEntries = new LinkedHashMap<TrialBalanceLedgerGroupVo, List<LedgerVo>>();

		for (String accountType : accountTypes) {
			for (TrialBalanceLedgerGroupVo key : keys) {
				if (key.getAccountType().contains(accountType)) {
					sortedTrialBalanceEntries.put(key, trialBalanceEntries.get(key));
				}
			}
		}
		return sortedTrialBalanceEntries;
	}

	public ProfitAndLossVo profitAndLossentries(Long companyId, String start, String end) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		ProfitAndLossVo vo = new ProfitAndLossVo();
		vo.setDateRange(start + " - " + end);
		List<LedgerGroup> directExpenseGroups = groupDao.findLedgerGroupsOfAccountType(
				AccountsConstants.DIRECT_EXPENSE, company);
		List<LedgerGroup> indirectExpenseGroups = groupDao.findLedgerGroupsOfAccountType(
				AccountsConstants.INDIRECT_EXPENSE, company);
		List<LedgerGroup> directIncomeGroups = groupDao.findLedgerGroupsOfAccountType(AccountsConstants.DIRECT_INCOME,
				company);
		List<LedgerGroup> indirectIncomeGroups = groupDao.findLedgerGroupsOfAccountType(
				AccountsConstants.INDIRECT_INCOME, company);

		List<Ledger> directExpenseLedgers = new ArrayList<Ledger>();
		for (LedgerGroup directExpenseGroup : directExpenseGroups)
			directExpenseLedgers.addAll(directExpenseGroup.getLedgers());

		List<Ledger> indirectExpenseLedgers = new ArrayList<Ledger>();
		for (LedgerGroup indirectExpenseGroup : indirectExpenseGroups)
			indirectExpenseLedgers.addAll(indirectExpenseGroup.getLedgers());

		List<Ledger> directIncomeLedgers = new ArrayList<Ledger>();
		for (LedgerGroup directIncomeGroup : directIncomeGroups)
			directIncomeLedgers.addAll(directIncomeGroup.getLedgers());

		List<Ledger> indirectIncomeLedgers = new ArrayList<Ledger>();
		for (LedgerGroup indirectIncomeGroup : indirectIncomeGroups)
			indirectIncomeLedgers.addAll(indirectIncomeGroup.getLedgers());

		vo = calculateDirectExpense(vo, directExpenseLedgers, start, end, company);
		vo = calculateDirectIncome(vo, directIncomeLedgers, start, end, company);
		vo = calculateIndirectExpense(vo, indirectExpenseLedgers, start, end, company);
		vo = calculateIndirectIncome(vo, indirectIncomeLedgers, start, end, company);
		vo = calculateProfitAndLoss(vo);
		return vo;
	}

	private ProfitAndLossVo calculateDirectExpense(ProfitAndLossVo vo, List<Ledger> directExpenseLedgers, String start,
			String end, Company company) {
		Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> map = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();
		BigDecimal directExpenseSum = new BigDecimal("0.00");
		for (Ledger ledger : directExpenseLedgers) {
			LedgerGroup ledgerGroup = ledger.getLedgerGroup();
			String group = ledgerGroup.getGroupName();
			List<ProfitAndLossLedgerVo> ledgerVos = null;
			Set<ProfitAndLossGroupVo> keys = map.keySet();
			ProfitAndLossGroupVo mapKey = null;
			for (ProfitAndLossGroupVo key : keys) {
				if (key.getGroupName().equals(group)) {
					ledgerVos = map.get(key);
					mapKey = key;
				}
			}
			ProfitAndLossGroupVo groupVo = null;
			if (ledgerVos == null) {
				ledgerVos = new ArrayList<ProfitAndLossLedgerVo>();
				groupVo = new ProfitAndLossGroupVo();
				groupVo.setGroupName(group);
				groupVo.setGroupId(ledgerGroup.getId());
				map.put(groupVo, ledgerVos);
				mapKey = groupVo;
			}
			LedgerVo ledgerVo = findProfitAndLossForLedger(start, end, ledger, company);
			if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) != 0) {
				ProfitAndLossLedgerVo profitAndLossLedgerVo = new ProfitAndLossLedgerVo();
				profitAndLossLedgerVo.setAmount(ledgerVo.getBalance());
				profitAndLossLedgerVo.setLedgerId(ledgerVo.getLedgerId());
				profitAndLossLedgerVo.setLedgerName(ledgerVo.getLedgerName());
				mapKey.setAmount(mapKey.getAmount().add(ledgerVo.getBalance()));
				directExpenseSum = mapKey.getAmount();
				ledgerVos.add(profitAndLossLedgerVo);
			}
			map.put(mapKey, ledgerVos);
		}
		Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> newMap = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();
		Set<ProfitAndLossGroupVo> keys = map.keySet();
		for (ProfitAndLossGroupVo key : keys) {
			if (key.getAmount().compareTo(BigDecimal.ZERO) != 0)
				newMap.put(key, map.get(key));
		}
		ProfitAndLossGroupVo openingStock = new ProfitAndLossGroupVo();
		List<ProfitAndLossLedgerVo> openingStockVos = new ArrayList<ProfitAndLossLedgerVo>();
		Date endDate = DateFormatter.convertStringToDate(start);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(endDate);
		calendar.add(Calendar.DATE, -1);
		List<StockViewVo> stockViewVos = inventoryTransactionDao.findStockSummary(company.getId(),
				DateFormatter.convertDateToString(company.getPeriodStart()),
				DateFormatter.convertDateToString(calendar.getTime()));
		BigDecimal totalAmount = new BigDecimal("0.00");
		for (StockViewVo stockViewVo : stockViewVos) {
			totalAmount = totalAmount.add(stockViewVo.getTotalAmount()).add(stockViewVo.getClosingValue());
		}
		openingStock.setAmount(totalAmount);
		openingStock.setFirst(true);
		openingStock.setGroupName("Opening Stock");
		newMap.put(openingStock, openingStockVos);
		vo.setDirectExpenses(newMap);
		vo.setDirectExpenseSum(directExpenseSum.add(openingStock.getAmount()));
		return vo;
	}

	private ProfitAndLossVo calculateIndirectExpense(ProfitAndLossVo vo, List<Ledger> indirectExpenseLedgers,
			String start, String end, Company company) {
		Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> map = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();
		BigDecimal indeirectExpenseSum = new BigDecimal("0.00");
		for (Ledger ledger : indirectExpenseLedgers) {
			LedgerGroup ledgerGroup = ledger.getLedgerGroup();
			String group = ledgerGroup.getGroupName();
			List<ProfitAndLossLedgerVo> ledgerVos = null;
			Set<ProfitAndLossGroupVo> keys = map.keySet();
			ProfitAndLossGroupVo mapKey = null;
			for (ProfitAndLossGroupVo key : keys) {
				if (key.getGroupName().equals(group)) {
					ledgerVos = map.get(key);
					mapKey = key;
				}
			}
			ProfitAndLossGroupVo groupVo = null;
			if (ledgerVos == null) {
				ledgerVos = new ArrayList<ProfitAndLossLedgerVo>();
				groupVo = new ProfitAndLossGroupVo();
				groupVo.setGroupName(group);
				groupVo.setGroupId(ledgerGroup.getId());
				map.put(groupVo, ledgerVos);
				mapKey = groupVo;
			}
			LedgerVo ledgerVo = findProfitAndLossForLedger(start, end, ledger, company);
			if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) != 0) {
				ProfitAndLossLedgerVo profitAndLossLedgerVo = new ProfitAndLossLedgerVo();
				profitAndLossLedgerVo.setAmount(ledgerVo.getBalance());
				profitAndLossLedgerVo.setLedgerName(ledgerVo.getLedgerName());
				profitAndLossLedgerVo.setLedgerId(ledgerVo.getLedgerId());
				mapKey.setAmount(mapKey.getAmount().add(ledgerVo.getBalance()));
				indeirectExpenseSum = mapKey.getAmount();
				ledgerVos.add(profitAndLossLedgerVo);
			}
			map.put(mapKey, ledgerVos);
		}
		Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> newMap = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();
		Set<ProfitAndLossGroupVo> keys = map.keySet();
		for (ProfitAndLossGroupVo key : keys) {
			if (key.getAmount().compareTo(BigDecimal.ZERO) != 0)
				newMap.put(key, map.get(key));
		}
		vo.setIndirectExpenses(newMap);
		vo.setIndirectExpenseSum(indeirectExpenseSum);
		return vo;
	}

	private ProfitAndLossVo calculateIndirectIncome(ProfitAndLossVo vo, List<Ledger> indirectIncomeLedgers,
			String start, String end, Company company) {
		Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> map = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();
		BigDecimal indirectIncomeSum = new BigDecimal("0.00");
		for (Ledger ledger : indirectIncomeLedgers) {
			LedgerGroup ledgerGroup = ledger.getLedgerGroup();
			String group = ledgerGroup.getGroupName();
			List<ProfitAndLossLedgerVo> ledgerVos = null;
			Set<ProfitAndLossGroupVo> keys = map.keySet();
			ProfitAndLossGroupVo mapKey = null;
			for (ProfitAndLossGroupVo key : keys) {
				if (key.getGroupName().equals(group)) {
					ledgerVos = map.get(key);
					mapKey = key;
				}
			}
			ProfitAndLossGroupVo groupVo = null;
			if (ledgerVos == null) {
				ledgerVos = new ArrayList<ProfitAndLossLedgerVo>();
				groupVo = new ProfitAndLossGroupVo();
				groupVo.setGroupId(ledgerGroup.getId());
				groupVo.setGroupName(group);
				map.put(groupVo, ledgerVos);
				mapKey = groupVo;
			}
			LedgerVo ledgerVo = findProfitAndLossForLedger(start, end, ledger, company);
			if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) != 0) {
				ProfitAndLossLedgerVo profitAndLossLedgerVo = new ProfitAndLossLedgerVo();
				profitAndLossLedgerVo.setAmount(ledgerVo.getBalance());
				profitAndLossLedgerVo.setLedgerName(ledgerVo.getLedgerName());
				profitAndLossLedgerVo.setLedgerId(ledgerVo.getLedgerId());
				mapKey.setAmount(mapKey.getAmount().add(ledgerVo.getBalance()));
				indirectIncomeSum = mapKey.getAmount();
				ledgerVos.add(profitAndLossLedgerVo);
			}
			map.put(mapKey, ledgerVos);
		}
		Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> newMap = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();
		Set<ProfitAndLossGroupVo> keys = map.keySet();
		for (ProfitAndLossGroupVo key : keys) {
			if (key.getAmount().compareTo(BigDecimal.ZERO) != 0)
				newMap.put(key, map.get(key));
		}
		vo.setIndirectIncomes(newMap);
		vo.setIndirectIncomeSum(indirectIncomeSum);
		return vo;
	}

	private ProfitAndLossVo calculateDirectIncome(ProfitAndLossVo vo, List<Ledger> directIncomeLedgers, String start,
			String end, Company company) {
		Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> map = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();
		BigDecimal directIncomeSum = new BigDecimal("0.00");
		for (Ledger ledger : directIncomeLedgers) {
			LedgerGroup ledgerGroup = ledger.getLedgerGroup();
			String group = ledgerGroup.getGroupName();
			List<ProfitAndLossLedgerVo> ledgerVos = null;
			Set<ProfitAndLossGroupVo> keys = map.keySet();
			ProfitAndLossGroupVo mapKey = null;
			for (ProfitAndLossGroupVo key : keys) {
				if (key.getGroupName().equals(group)) {
					ledgerVos = map.get(key);
					mapKey = key;
				}
			}
			ProfitAndLossGroupVo groupVo = null;
			if (ledgerVos == null) {
				ledgerVos = new ArrayList<ProfitAndLossLedgerVo>();
				groupVo = new ProfitAndLossGroupVo();
				groupVo.setGroupName(group);
				groupVo.setGroupId(ledgerGroup.getId());
				map.put(groupVo, ledgerVos);
				mapKey = groupVo;
			}
			LedgerVo ledgerVo = findProfitAndLossForLedger(start, end, ledger, company);
			if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) != 0) {
				ProfitAndLossLedgerVo profitAndLossLedgerVo = new ProfitAndLossLedgerVo();
				profitAndLossLedgerVo.setAmount(ledgerVo.getBalance());
				profitAndLossLedgerVo.setLedgerName(ledgerVo.getLedgerName());
				profitAndLossLedgerVo.setLedgerId(ledgerVo.getLedgerId());
				mapKey.setAmount(mapKey.getAmount().add(ledgerVo.getBalance()));
				directIncomeSum = mapKey.getAmount();
				ledgerVos.add(profitAndLossLedgerVo);
			}
			map.put(mapKey, ledgerVos);
		}
		Map<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>> newMap = new HashMap<ProfitAndLossGroupVo, List<ProfitAndLossLedgerVo>>();
		Set<ProfitAndLossGroupVo> keys = map.keySet();
		for (ProfitAndLossGroupVo key : keys) {
			if (key.getAmount().compareTo(BigDecimal.ZERO) != 0)
				newMap.put(key, map.get(key));
		}
		ProfitAndLossGroupVo closingStock = new ProfitAndLossGroupVo();
		List<ProfitAndLossLedgerVo> closingStockVos = new ArrayList<ProfitAndLossLedgerVo>();
		List<StockViewVo> stockViewVos = inventoryTransactionDao.findStockSummary(company.getId(), start, end);
		BigDecimal totalAmount = new BigDecimal("0.00");
		for (StockViewVo stockViewVo : stockViewVos) {
			totalAmount = totalAmount.add(stockViewVo.getTotalAmount()).add(stockViewVo.getClosingValue());
		}
		closingStock.setAmount(totalAmount);
		closingStock.setIsLast(true);
		closingStock.setGroupName("Closing Stock");
		newMap.put(closingStock, closingStockVos);
		vo.setDirectIncomes(newMap);
		vo.setDirectIncomeSum(directIncomeSum.add(closingStock.getAmount()));
		return vo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	private LedgerVo findProfitAndLossForLedger(String from, String to, Ledger ledger, Company company) {
		if (company == null)
			throw new ItemNotFoundException("Company not found");
		BaseCurrency baseCurrency = company.getCurrency().getBaseCurrency();
		if (ledger == null)
			throw new ItemNotFoundException("Ledger not found");
		// String accountType =
		// ledger.getLedgerGroup().getAccountType().getType();
		Date startDate = DateFormatter.convertStringToDate(from);
		Date endDate = DateFormatter.convertStringToDate(to);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transactions.class);
		criteria.add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("ledger", ledger));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);

		List<Transactions> transactions = criteria.list();
		ErpCurrencyVo currecny = currencyDao.findCurrencyForDate(ledger.getStartDate(), baseCurrency);
		BigDecimal openingBalance = null;
		BigDecimal balance = null;
		if (currecny != null) {
			openingBalance = ledger.getOpeningBalance().multiply(currecny.getRate())
					.setScale(3, RoundingMode.HALF_EVEN);
			balance = openingBalance;
		} else {
			openingBalance = ledger.getOpeningBalance();
			balance = openingBalance;
		}
		BigDecimal credit = new BigDecimal("0.00");
		BigDecimal debit = new BigDecimal("0.00");

		LedgerVo ledgerVo = new LedgerVo();
		for (Transactions transaction : transactions) {

			ErpCurrencyVo currencyVo = currencyDao.findCurrencyForDate(transaction.getDate(), baseCurrency);
			if (currencyVo != null) {
				if (openingBalance == null)
					openingBalance = ledger.getOpeningBalance().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN);
				if (balance == null)
					balance = ledger.getOpeningBalance().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN);
			} else {
				if (openingBalance == null)
					openingBalance = new BigDecimal("0.00");
				if (balance == null)
					balance = new BigDecimal("0.00");
			}

			Date transactionDate = transaction.getDate();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(transactionDate);
			calendar.add(Calendar.DATE, 1);
			Date transactionPlusOne = calendar.getTime();
			if (transactionPlusOne.compareTo(startDate) < 0) {
				if (currencyVo != null) {
					openingBalance = openingBalance.subtract(transaction.getMinusAmount()
							.multiply(currencyVo.getRate()));
					openingBalance = openingBalance.add(transaction.getPlusAmount().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN));
				} else {
					openingBalance = openingBalance.subtract(transaction.getMinusAmount());
					openingBalance = openingBalance.add(transaction.getPlusAmount());
				}
				balance = openingBalance;
			} else {
				if (currencyVo != null) {
					balance = balance.subtract(transaction.getMinusAmount().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN));
					balance = balance.add(transaction.getPlusAmount().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN));
					credit = credit.add(transaction.getMinusAmount().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN));
					debit = debit.add(transaction.getPlusAmount().multiply(currencyVo.getRate())
							.setScale(3, RoundingMode.HALF_EVEN));
				} else {
					balance = balance.subtract(transaction.getMinusAmount());
					balance = balance.add(transaction.getPlusAmount());
					credit = credit.add(transaction.getMinusAmount());
					debit = debit.add(transaction.getPlusAmount());
				}
			}
		}
		ledgerVo.setCredit(credit);
		ledgerVo.setDebit(debit);
		ledgerVo.setBalance(balance);
		ledgerVo.setOpeningBalance(openingBalance);
		ledgerVo.setLedgerName(ledger.getLedgerName());
		ledgerVo.setLedgerId(ledger.getId());
		ledgerVo.setLedgerGroup(ledger.getLedgerGroup().getGroupName());
		return ledgerVo;
	}

	public ProfitAndLossVo profitAndLossentries(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Exists.");
		return profitAndLossentries(companyId, DateFormatter.convertDateToString(company.getPeriodStart()),
				DateFormatter.convertDateToString(company.getPeriodEnd()));
	}

	private ProfitAndLossVo calculateProfitAndLoss(ProfitAndLossVo vo) {

		if (vo.getDirectExpenseSum().compareTo(vo.getDirectIncomeSum()) == 0) {
			vo.setDirectExpenseTotal(vo.getDirectExpenseSum());
			vo.setDirectIncomeTotal(vo.getDirectExpenseSum());
		} else {
			if (vo.getDirectExpenseSum().compareTo(vo.getDirectIncomeSum()) < 0) {
				vo.setDirectExpenseGrossProfit(vo.getDirectIncomeSum().subtract(vo.getDirectExpenseSum()));
				vo.setDirectExpenseTotal(vo.getDirectIncomeSum());
				vo.setDirectIncomeTotal(vo.getDirectIncomeSum());

				ProfitAndLossGroupVo groupVo = new ProfitAndLossGroupVo();
				List<ProfitAndLossLedgerVo> ledgerVos = new ArrayList<ProfitAndLossLedgerVo>();
				groupVo.setGroupName("Gross Profit b/d");
				groupVo.setAmount(vo.getDirectExpenseGrossProfit());
				vo.setIndirectIncomeSum(vo.getIndirectIncomeSum().add(vo.getDirectExpenseGrossProfit()));
				groupVo.setFirst(true);
				vo.getIndirectIncomes().put(groupVo, ledgerVos);
			} else {
				vo.setDirectIncomeGrossLoss(vo.getDirectExpenseSum().subtract(vo.getDirectIncomeSum()));
				vo.setDirectExpenseTotal(vo.getDirectExpenseSum());
				vo.setDirectIncomeTotal(vo.getDirectExpenseSum());
				ProfitAndLossGroupVo groupVo = new ProfitAndLossGroupVo();
				List<ProfitAndLossLedgerVo> ledgerVos = new ArrayList<ProfitAndLossLedgerVo>();
				groupVo.setGroupName("Gross Loss b/d");
				groupVo.setFirst(true);
				groupVo.setAmount(vo.getDirectIncomeGrossLoss());
				vo.setIndirectExpenseSum(vo.getIndirectExpenseSum().add(vo.getDirectIncomeGrossLoss()));
				vo.getIndirectExpenses().put(groupVo, ledgerVos);
			}
		}

		if (vo.getIndirectExpenseSum().compareTo(vo.getIndirectIncomeSum()) == 0) {
			vo.setInddirectExpenseTotal(vo.getIndirectExpenseSum());
			vo.setIndirectIncomeTotal(vo.getIndirectExpenseSum());
		} else {
			if (vo.getIndirectExpenseSum().compareTo(vo.getIndirectIncomeSum()) < 0) {
				vo.setIndirectExpenseNetProfit(vo.getIndirectIncomeSum().subtract(vo.getIndirectExpenseSum()));
				vo.setInddirectExpenseTotal(vo.getIndirectIncomeSum());
				vo.setIndirectIncomeTotal(vo.getIndirectIncomeSum());
			} else {
				vo.setIndirectIncomeNetLoss(vo.getIndirectExpenseSum().subtract(vo.getIndirectIncomeSum()));
				vo.setInddirectExpenseTotal(vo.getIndirectExpenseSum());
				vo.setIndirectIncomeTotal(vo.getIndirectExpenseSum());
			}
		}
		return vo;
	}

	public BalanceSheetVo balanceSheetCalculation(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		return this.balanceSheetCalculation(companyId, DateFormatter.convertDateToString(company.getPeriodStart()),
				DateFormatter.convertDateToString(company.getPeriodEnd()));
	}

	public BalanceSheetVo balanceSheetCalculation(Long companyId, String start, String end) {
		ProfitAndLossVo profitAndLossVo = this.profitAndLossentries(companyId, start, end);
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found.");
		BalanceSheetVo vo = new BalanceSheetVo();
		vo.setDateRange(start + " - " + end);
		List<LedgerGroup> currentLiabilityGroups = groupDao.findLedgerGroupsOfAccountType(
				AccountsConstants.CURRENT_LIABILITIES, company);
		List<LedgerGroup> nonCurrentLiabilityGroups = groupDao.findLedgerGroupsOfAccountType(
				AccountsConstants.NON_CURRENT_LIABILITIES, company);
		List<LedgerGroup> currentAssetsGroups = groupDao.findLedgerGroupsOfAccountType(AccountsConstants.CURRENT_ASSET,
				company);
		List<LedgerGroup> fixedAssetsGroups = groupDao.findLedgerGroupsOfAccountType(AccountsConstants.FIXED_ASSET,
				company);
		List<LedgerGroup> intangibleAssetsGroups = groupDao.findLedgerGroupsOfAccountType(
				AccountsConstants.INTANGIBLE_ASSET, company);
		List<LedgerGroup> nonCurrentAssetsGroups = groupDao.findLedgerGroupsOfAccountType(
				AccountsConstants.NON_CURRENT_ASSET, company);
		List<LedgerGroup> equityGroups = groupDao.findLedgerGroupsOfAccountType(AccountsConstants.EQUITY, company);

		if (!currentLiabilityGroups.isEmpty()) {
			List<Ledger> currentLiabilityLedgers = new ArrayList<Ledger>();
			for (LedgerGroup group : currentLiabilityGroups)
				currentLiabilityLedgers.addAll(group.getLedgers());
			vo = calculateBalancesheet(vo, currentLiabilityLedgers, start, end, company,
					AccountsConstants.CURRENT_LIABILITIES);
		}
		if (!nonCurrentLiabilityGroups.isEmpty()) {
			List<Ledger> nonCurrentLiabilityLedgers = new ArrayList<Ledger>();
			for (LedgerGroup group : nonCurrentLiabilityGroups)
				nonCurrentLiabilityLedgers.addAll(group.getLedgers());
			vo = calculateBalancesheet(vo, nonCurrentLiabilityLedgers, start, end, company,
					AccountsConstants.NON_CURRENT_LIABILITIES);
		}
		if (!currentAssetsGroups.isEmpty()) {
			List<Ledger> currentAssetsLedgers = new ArrayList<Ledger>();
			for (LedgerGroup group : currentAssetsGroups)
				currentAssetsLedgers.addAll(group.getLedgers());
			vo = calculateBalancesheet(vo, currentAssetsLedgers, start, end, company, AccountsConstants.CURRENT_ASSET);
		}
		if (!nonCurrentAssetsGroups.isEmpty()) {
			List<Ledger> nonCurrentAssetsLedgers = new ArrayList<Ledger>();
			for (LedgerGroup group : nonCurrentAssetsGroups)
				nonCurrentAssetsLedgers.addAll(group.getLedgers());
			vo = calculateBalancesheet(vo, nonCurrentAssetsLedgers, start, end, company,
					AccountsConstants.NON_CURRENT_ASSET);
		}
		if (!equityGroups.isEmpty()) {
			List<Ledger> equityLedgers = new ArrayList<Ledger>();
			for (LedgerGroup group : equityGroups)
				equityLedgers.addAll(group.getLedgers());
			vo = calculateBalancesheet(vo, equityLedgers, start, end, company, AccountsConstants.EQUITY);
		}
		if (!fixedAssetsGroups.isEmpty()) {
			List<Ledger> equityLedgers = new ArrayList<Ledger>();
			for (LedgerGroup group : fixedAssetsGroups)
				equityLedgers.addAll(group.getLedgers());
			vo = calculateBalancesheet(vo, equityLedgers, start, end, company, AccountsConstants.FIXED_ASSET);
		}
		if (!intangibleAssetsGroups.isEmpty()) {
			List<Ledger> equityLedgers = new ArrayList<Ledger>();
			for (LedgerGroup group : intangibleAssetsGroups)
				equityLedgers.addAll(group.getLedgers());
			vo = calculateBalancesheet(vo, equityLedgers, start, end, company, AccountsConstants.INTANGIBLE_ASSET);
		}
		List<BalanceSheetLedgerVo> ledgerVos = new ArrayList<BalanceSheetLedgerVo>();
		BalanceSheetGroupVo groupVo = new BalanceSheetGroupVo();
		if (profitAndLossVo.getIndirectExpenseNetProfit() != null
				&& profitAndLossVo.getIndirectExpenseNetProfit().compareTo(new BigDecimal(0)) != 0) {
			groupVo.setAmount(profitAndLossVo.getIndirectExpenseNetProfit());
			groupVo.setGroupName("Profit & Loss A/c");
			groupVo.setFirst(true);
			vo.getCurrentLiabilities().put(groupVo, ledgerVos);
			vo.setTotalLiability(vo.getTotalLiability().add(profitAndLossVo.getIndirectExpenseNetProfit()));
		} else if (profitAndLossVo.getIndirectIncomeNetLoss() != null
				&& profitAndLossVo.getIndirectIncomeNetLoss().compareTo(new BigDecimal(0)) != 0) {
			groupVo.setAmount(profitAndLossVo.getIndirectIncomeNetLoss().negate());
			groupVo.setGroupName("Profit & Loss A/c");
			groupVo.setFirst(true);
			vo.getCurrentLiabilities().put(groupVo, ledgerVos);
			vo.setTotalLiability(vo.getTotalLiability().add(groupVo.getAmount()));
		}
		return vo;
	}

	private BalanceSheetVo calculateBalancesheet(BalanceSheetVo vo, List<Ledger> ledgers, String start, String end,
			Company company, String accountType) {
		Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> map = new HashMap<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>>();
		for (Ledger ledger : ledgers) {
			LedgerGroup ledgerGroup = ledger.getLedgerGroup();
			String group = ledger.getLedgerGroup().getGroupName();
			List<BalanceSheetLedgerVo> ledgerVos = null;
			Set<BalanceSheetGroupVo> keys = map.keySet();
			BalanceSheetGroupVo mapKey = null;
			for (BalanceSheetGroupVo key : keys) {
				if (key.getGroupName().equals(group)) {
					ledgerVos = map.get(key);
					mapKey = key;
				}
			}
			BalanceSheetGroupVo groupVo = null;
			if (ledgerVos == null) {
				ledgerVos = new ArrayList<BalanceSheetLedgerVo>();
				groupVo = new BalanceSheetGroupVo();
				groupVo.setGroupName(group);
				groupVo.setGroupId(ledgerGroup.getId());
				map.put(groupVo, ledgerVos);
				mapKey = groupVo;
			}
			LedgerVo ledgerVo = findProfitAndLossForLedger(start, end, ledger, company);
			/**
			 * ledgers with amount
			 */
			if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) != 0) {
				BalanceSheetLedgerVo profitAndLossLedgerVo = new BalanceSheetLedgerVo();
				profitAndLossLedgerVo.setLedgerId(ledgerVo.getLedgerId());
				profitAndLossLedgerVo.setAmount(ledgerVo.getBalance());
				profitAndLossLedgerVo.setLedgerName(ledgerVo.getLedgerName());
				if (accountType.equalsIgnoreCase(AccountsConstants.CURRENT_ASSET)) {
					if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) >= 0)
						profitAndLossLedgerVo.setIsDebit(true);
					else
						profitAndLossLedgerVo.setIsDebit(false);
					vo.setTotalAsset(vo.getTotalAsset().add(ledgerVo.getBalance()));
				} else if (accountType.equalsIgnoreCase(AccountsConstants.CURRENT_LIABILITIES)) {
					if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) >= 0)
						profitAndLossLedgerVo.setIsDebit(false);
					else
						profitAndLossLedgerVo.setIsDebit(true);
					vo.setTotalLiability(vo.getTotalLiability().add(ledgerVo.getBalance()));
				} else if (accountType.equalsIgnoreCase(AccountsConstants.NON_CURRENT_ASSET)) {
					if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) >= 0)
						profitAndLossLedgerVo.setIsDebit(true);
					else
						profitAndLossLedgerVo.setIsDebit(false);
					vo.setTotalAsset(vo.getTotalAsset().add(ledgerVo.getBalance()));
				} else if (accountType.equalsIgnoreCase(AccountsConstants.FIXED_ASSET)) {
					if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) >= 0)
						profitAndLossLedgerVo.setIsDebit(true);
					else
						profitAndLossLedgerVo.setIsDebit(false);
					vo.setTotalAsset(vo.getTotalAsset().add(ledgerVo.getBalance()));
				} else if (accountType.equalsIgnoreCase(AccountsConstants.INTANGIBLE_ASSET)) {
					if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) >= 0)
						profitAndLossLedgerVo.setIsDebit(true);
					else
						profitAndLossLedgerVo.setIsDebit(false);
					vo.setTotalAsset(vo.getTotalAsset().add(ledgerVo.getBalance()));
				} else if (accountType.equalsIgnoreCase(AccountsConstants.NON_CURRENT_LIABILITIES)) {
					if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) >= 0)
						profitAndLossLedgerVo.setIsDebit(false);
					else
						profitAndLossLedgerVo.setIsDebit(true);
					vo.setTotalLiability(vo.getTotalLiability().add(ledgerVo.getBalance()));
				} else if (accountType.equalsIgnoreCase(AccountsConstants.EQUITY)) {
					if (ledgerVo.getBalance().compareTo(BigDecimal.ZERO) >= 0)
						profitAndLossLedgerVo.setIsDebit(false);
					else
						profitAndLossLedgerVo.setIsDebit(true);
					vo.setTotalLiability(vo.getTotalLiability().add(ledgerVo.getBalance()));
				}
				mapKey.setAmount(mapKey.getAmount().add(ledgerVo.getBalance()));
				ledgerVos.add(profitAndLossLedgerVo);
				map.put(mapKey, ledgerVos);
			}
		}

		/**
		 * removing group with zero amount
		 */
		Map<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>> newmap = new HashMap<BalanceSheetGroupVo, List<BalanceSheetLedgerVo>>();
		Set<BalanceSheetGroupVo> keys = map.keySet();
		for (BalanceSheetGroupVo key : keys) {
			if (key.getAmount().compareTo(BigDecimal.ZERO) != 0)
				newmap.put(key, map.get(key));
		}

		if (accountType.equalsIgnoreCase(AccountsConstants.CURRENT_ASSET)) {
			BalanceSheetGroupVo closingStock = new BalanceSheetGroupVo();
			List<BalanceSheetLedgerVo> closingStockVos = new ArrayList<BalanceSheetLedgerVo>();
			List<StockViewVo> stockViewVos = inventoryTransactionDao.findStockSummary(company.getId(), start, end);
			BigDecimal totalAmount = new BigDecimal("0.00");
			for (StockViewVo stockViewVo : stockViewVos) {
				totalAmount = totalAmount.add(stockViewVo.getTotalAmount()).add(stockViewVo.getClosingValue());
			}
			closingStock.setAmount(totalAmount);
			closingStock.setIsLast(true);
			closingStock.setGroupName("Closing Stock");
			newmap.put(closingStock, closingStockVos);
			vo.setCurrentAssets(newmap);
			vo.setTotalAsset(vo.getTotalAsset().add(closingStock.getAmount()));
		} else if (accountType.equalsIgnoreCase(AccountsConstants.CURRENT_LIABILITIES)) {
			vo.setCurrentLiabilities(newmap);
		} else if (accountType.equalsIgnoreCase(AccountsConstants.NON_CURRENT_ASSET)) {
			vo.setNonCurrentAssets(newmap);
		} else if (accountType.equalsIgnoreCase(AccountsConstants.FIXED_ASSET)) {
			vo.getNonCurrentAssets().putAll(newmap);
		} else if (accountType.equalsIgnoreCase(AccountsConstants.INTANGIBLE_ASSET)) {
			vo.getNonCurrentAssets().putAll(newmap);
		} else if (accountType.equalsIgnoreCase(AccountsConstants.NON_CURRENT_LIABILITIES)) {
			vo.setNonCurrentLiabilities(newmap);
		} else if (accountType.equalsIgnoreCase(AccountsConstants.EQUITY)) {
			vo.setEquity(newmap);
		}
		return vo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public LedgerViewVo findTRansactionsForLedger(String startdate, String enddate, Ledger ledger, Long ledgerId,
			Long companyId) {
		if (ledger == null)
			ledger = ledgerDao.findLedgerObjectById(ledgerId);
		if (ledger == null)
			throw new ItemNotFoundException("Ledger not found");
		String accountType = ledger.getLedgerGroup().getAccountType().getType();
		Company company = ledger.getCompany();
		Date startDate = DateFormatter.convertStringToDate(startdate);
		Date endDate = DateFormatter.convertStringToDate(enddate);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transactions.class);
		criteria.add(Restrictions.le("date", endDate));
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("ledger", ledger));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		BaseCurrency baseCurrency = company.getCurrency().getBaseCurrency();
		List<Transactions> transactions = criteria.list();
		ErpCurrencyVo currecny = currencyDao.findCurrencyForDate(ledger.getStartDate(), baseCurrency);

		List<String> dates = DateFormatter.getMonthYearList(startDate, endDate);
		Map<String, Set<LedgerTransactionViewVo>> transactionMap = new HashMap<String, Set<LedgerTransactionViewVo>>();
		Set<LedgerTransactionViewVo> dummyVos = new LinkedHashSet<LedgerTransactionViewVo>();
		for (String date : dates) {
			// transactionMap.put(date, dummyVos);
		}
		Map<Journal, LedgerTransactionViewVo> journalMap = new HashMap<Journal, LedgerTransactionViewVo>();
		Map<Payment, LedgerTransactionViewVo> paymentMap = new HashMap<Payment, LedgerTransactionViewVo>();
		Map<Receipts, LedgerTransactionViewVo> receiptMap = new HashMap<Receipts, LedgerTransactionViewVo>();
		Map<Contra, LedgerTransactionViewVo> contraMap = new HashMap<Contra, LedgerTransactionViewVo>();
		BigDecimal initialopeningBalance = this.findOpeningBalance(ledger, startDate);
		LedgerViewVo vo = new LedgerViewVo();
		vo.setStartDate(startdate);
		vo.setEndDate(enddate);
		vo.setLedgerName(ledger.getLedgerName());
		for (Transactions transaction : transactions) {
			Date transactionDate = transaction.getDate();
			JournalEntries journalEntries = transaction.getJournalEntries();
			PaymentEntries paymentEntries = transaction.getPaymentEntries();
			ReceiptEntries receiptEntries = transaction.getReceiptEntries();
			ContraEntries contraEntries = transaction.getContraEntries();
			Journal journal = null;
			Receipts receipts = null;
			Contra contra = null;
			Payment payment = null;
			LedgerTransactionViewVo ledgerTransactionViewVo = null;
			LedgerTransactionViewVo oldVo = null;
			if (journalEntries != null) {
				journal = journalEntries.getJournal();
				oldVo = journalMap.get(journal);
				if (oldVo == null)
					ledgerTransactionViewVo = new LedgerTransactionViewVo();
				else
					ledgerTransactionViewVo = oldVo;
				ledgerTransactionViewVo.setVoucher("Journal");
				ledgerTransactionViewVo.setId(journal.getId());
				ledgerTransactionViewVo.setNarration(journal.getNarration());
				ledgerTransactionViewVo.setVoucherNumber(journal.getJournalNumber());
			} else if (paymentEntries != null) {
				payment = paymentEntries.getPayment();
				oldVo = paymentMap.get(payment);
				if (oldVo == null)
					ledgerTransactionViewVo = new LedgerTransactionViewVo();
				else
					ledgerTransactionViewVo = oldVo;
				ledgerTransactionViewVo.setVoucher("Payment");
				ledgerTransactionViewVo.setId(payment.getId());
				ledgerTransactionViewVo.setNarration(payment.getNarration());
				ledgerTransactionViewVo.setVoucherNumber(payment.getPaymentNumber());
			} else if (contraEntries != null) {
				contra = contraEntries.getContra();
				oldVo = contraMap.get(contra);
				if (oldVo == null)
					ledgerTransactionViewVo = new LedgerTransactionViewVo();
				else
					ledgerTransactionViewVo = oldVo;
				ledgerTransactionViewVo.setVoucher("Contra");
				ledgerTransactionViewVo.setId(contra.getId());
				ledgerTransactionViewVo.setNarration(contra.getNarration());
				ledgerTransactionViewVo.setVoucherNumber(contra.getContraNumber());
			} else if (receiptEntries != null) {
				receipts = receiptEntries.getReceipt();
				oldVo = receiptMap.get(receipts);
				if (oldVo == null)
					ledgerTransactionViewVo = new LedgerTransactionViewVo();
				else
					ledgerTransactionViewVo = oldVo;
				ledgerTransactionViewVo.setVoucher("Receipt");
				ledgerTransactionViewVo.setId(receipts.getId());
				ledgerTransactionViewVo.setNarration(receipts.getNarration());
				ledgerTransactionViewVo.setVoucherNumber(receipts.getReceiptNumber());
			}
			String date = DateFormatter.getMonthYesr(transactionDate);
			ledgerTransactionViewVo.setDate(DateFormatter.convertDateToString(transactionDate));
			if (oldVo != null) {
				ledgerTransactionViewVo.setDebitAmount(oldVo.getDebitAmount().add(transaction.getPlusAmount()));
				ledgerTransactionViewVo.setCreditAmount(oldVo.getCreditAmount().add(transaction.getMinusAmount()));
			} else {
				ledgerTransactionViewVo.setDebitAmount(transaction.getPlusAmount());
				ledgerTransactionViewVo.setCreditAmount(transaction.getMinusAmount());
			}

			if (journal != null) {
				journalMap.put(journal, ledgerTransactionViewVo);
			} else if (payment != null) {
				paymentMap.put(payment, ledgerTransactionViewVo);
			} else if (contra != null) {
				contraMap.put(contra, ledgerTransactionViewVo);
			} else if (receipts != null) {
				receiptMap.put(receipts, ledgerTransactionViewVo);
			}

			Set<LedgerTransactionViewVo> ledgerTransactionViewVos = transactionMap.get(date);
			if (ledgerTransactionViewVos == null)
				ledgerTransactionViewVos = new LinkedHashSet<LedgerTransactionViewVo>();
			ledgerTransactionViewVos.add(ledgerTransactionViewVo);
			transactionMap.put(date, ledgerTransactionViewVos);
		}

		List<LedgerViewMonthlyVo> monthlyVos = new ArrayList<LedgerViewMonthlyVo>();
		int i = 1;
		BigDecimal closingBalance = new BigDecimal("0.00");
		Set<LedgerTransactionViewVo> viewVos = new LinkedHashSet<LedgerTransactionViewVo>();
		LedgerViewMonthlyVo openingBalanceVo = new LedgerViewMonthlyVo();
		openingBalanceVo.setLedgerName(vo.getLedgerName());
		BigDecimal initialOpeningCredit = new BigDecimal("0.00");
		BigDecimal initialOpeningDebit = new BigDecimal("0.00");
		if (accountType.contains("Asset")) {
			if (initialopeningBalance.compareTo(BigDecimal.ZERO) >= 0)
				initialOpeningDebit = initialopeningBalance;
			else
				initialOpeningCredit = initialopeningBalance;
		} else if (accountType.contains("Expense")) {
			if (initialopeningBalance.compareTo(BigDecimal.ZERO) >= 0)
				initialOpeningDebit = initialopeningBalance;
			else
				initialOpeningCredit = initialopeningBalance;
		} else if (accountType.contains("Liability")) {
			if (initialopeningBalance.compareTo(BigDecimal.ZERO) >= 0)
				initialOpeningCredit = initialopeningBalance;
			else
				initialOpeningDebit = initialopeningBalance;
		} else if (accountType.contains("Income")) {
			if (initialopeningBalance.compareTo(BigDecimal.ZERO) >= 0)
				initialOpeningCredit = initialopeningBalance;
			else
				initialOpeningDebit = initialopeningBalance;
		} else if (accountType.contains("Equity")) {
			if (initialopeningBalance.compareTo(BigDecimal.ZERO) >= 0)
				initialOpeningCredit = initialopeningBalance;
			else
				initialOpeningDebit = initialopeningBalance;
		}
		openingBalanceVo.setClosingCredit(initialOpeningCredit.abs());
		openingBalanceVo.setMonth("Opening Balance");
		openingBalanceVo.setClosingDebit(initialOpeningDebit.abs());
		monthlyVos.add(openingBalanceVo);

		BigDecimal closingCredit = initialOpeningCredit.abs();
		BigDecimal closingDebit = initialOpeningDebit.abs();
		BigDecimal totalDebit = new BigDecimal("0.00");
		BigDecimal totalCredit = new BigDecimal("0.00");
		for (String date : dates) {
			LedgerViewMonthlyVo monthlyVo = new LedgerViewMonthlyVo();
			monthlyVo.setLedgerName(vo.getLedgerName());
			if (i <= 12) {
				String[] splittedDate = date.split(" ");
				monthlyVo.setMonth(splittedDate[0]);
			} else
				monthlyVo.setMonth(date);
			viewVos = transactionMap.get(date);
			date = "01_" + date;
			Date convertedDate = DateFormatter.getDateFromMonthYesr(date);
			BigDecimal openingBalance = this.findOpeningBalance(ledger, convertedDate);
			BigDecimal openingCredit = new BigDecimal("0.00");
			BigDecimal openingDebit = new BigDecimal("0.00");
			BigDecimal currentTotalCredit = new BigDecimal("0.00");
			BigDecimal currentTotalDebit = new BigDecimal("0.00");
			if (accountType.contains("Asset")) {
				if (openingBalance.compareTo(BigDecimal.ZERO) >= 0)
					openingDebit = openingBalance;
				else
					openingCredit = openingBalance;
			} else if (accountType.contains("Expense")) {
				if (openingBalance.compareTo(BigDecimal.ZERO) >= 0)
					openingDebit = openingBalance;
				else
					openingCredit = openingBalance;
			} else if (accountType.contains("Liability")) {
				if (openingBalance.compareTo(BigDecimal.ZERO) >= 0)
					openingCredit = openingBalance;
				else
					openingDebit = openingBalance;
			} else if (accountType.contains("Income")) {
				if (openingBalance.compareTo(BigDecimal.ZERO) >= 0)
					openingCredit = openingBalance;
				else
					openingDebit = openingBalance;
			} else if (accountType.contains("Equity")) {
				if (openingBalance.compareTo(BigDecimal.ZERO) >= 0)
					openingCredit = openingBalance;
				else
					openingDebit = openingBalance;
			}

			if (viewVos != null) {
				openingCredit = new BigDecimal("0.00");
				openingDebit = new BigDecimal("0.00");
				closingCredit = new BigDecimal("0.00");
				closingDebit = new BigDecimal("0.00");
				currentTotalCredit = new BigDecimal("0.00");
				currentTotalDebit = new BigDecimal("0.00");
				monthlyVo.setTransactionViewVos(viewVos);
				closingBalance = new BigDecimal("0.00");
				for (LedgerTransactionViewVo viewVo : viewVos) {
					currentTotalDebit = currentTotalDebit.add(viewVo.getDebitAmount());
					currentTotalCredit = currentTotalCredit.add(viewVo.getCreditAmount());
				}
				closingBalance = currentTotalCredit.subtract(currentTotalDebit);
				closingBalance = closingBalance.abs();
				currentTotalCredit = currentTotalCredit.add(openingCredit);
				currentTotalDebit = currentTotalDebit.add(openingDebit);
				if (currentTotalCredit.compareTo(currentTotalDebit) >= 0)
					closingCredit = closingBalance;
				else
					closingDebit = closingBalance;
				monthlyVo.setCurrentTotalCredit(currentTotalCredit.abs());
				monthlyVo.setCurrentTotalDebit(currentTotalDebit.abs());
				totalCredit = totalCredit.add(currentTotalCredit);
				totalDebit = totalDebit.add(currentTotalDebit);
			}
			monthlyVo.setOpeningCredit(openingCredit.abs());
			monthlyVo.setOpeningDebit(openingDebit.abs());
			monthlyVo.setClosingCredit(closingCredit.abs());
			monthlyVo.setClosingDebit(closingDebit.abs());
			monthlyVos.add(monthlyVo);
			i++;
		}
		vo.setClosingCredit(closingCredit.abs());
		vo.setClosingDebit(closingDebit.abs());
		vo.setLedgerId(ledgerId);
		vo.setTotalCredit(totalCredit.abs());
		vo.setTotalDebit(totalDebit.abs());
		vo.setMonthlyVos(monthlyVos);
		return vo;
	}

	public LedgerGroupViewVo findTRansactionsForLedgerGroup(String startDate, String endDate, Long groupId,
			Long companyId) {
		LedgerGroup ledgerGroup = groupDao.findLedgerGroup(groupId);
		if (ledgerGroup == null)
			throw new ItemNotFoundException("Ledger Group Not Found.");
		Set<Ledger> ledgers = ledgerGroup.getLedgers();
		LedgerGroupViewVo vo = new LedgerGroupViewVo();
		BigDecimal totalCredit = new BigDecimal("0.00");
		BigDecimal totalDebit = new BigDecimal("0.00");
		for (Ledger ledger : ledgers) {
			LedgerViewVo ledgerViewVo = findTRansactionsForLedger(startDate, endDate, ledger, ledger.getId(), companyId);
			totalCredit = totalCredit.add(ledgerViewVo.getClosingCredit());
			totalDebit = totalDebit.add(ledgerViewVo.getClosingDebit());
			vo.getLedgerViewVos().add(ledgerViewVo);
		}
		vo.setLedgerGroup(ledgerGroup.getGroupName());
		vo.setTotalCredit(totalCredit);
		vo.setTotalDebit(totalDebit);
		return vo;
	}

	private BigDecimal findOpeningBalance(Ledger ledger, Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -1);
		Date previousDay = calendar.getTime();
		BigDecimal openingBalance = new BigDecimal("0.00");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Transactions.class);
		criteria.add(Restrictions.eq("ledger", ledger));
		criteria.add(Restrictions.le("date", previousDay));
		List<Transactions> transactions = criteria.list();
		for (Transactions transaction : transactions) {
			openingBalance = openingBalance.subtract(transaction.getMinusAmount());
			openingBalance = openingBalance.add(transaction.getPlusAmount());
		}
		return openingBalance;
	}
}
