package com.accounts.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.ContraDao;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.entities.Contra;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.ContraEntries;
import com.accounts.web.entities.ContraEntries;
import com.accounts.web.entities.Transactions;
import com.accounts.web.vo.ContraVo;
import com.accounts.web.vo.ContraItemsVo;
import com.accounts.web.vo.ContraVo;
import com.accounts.web.vo.ContraItemsVo;
import com.accounts.web.vo.ContraVo;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.vo.ErpCurrencyVo;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.dao.ContractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 18-August-2015
 *
 */
@Repository
public class ContraDaoImpl extends AbstractDao implements ContraDao {

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private ErpCurrencyDao erpCurrencyDao;

	@Autowired
	private LedgerDao ledgerDao;
	
	@Autowired
	private NumberPropertyDao numberPropertyDao;

	/**
	 * Method to save or update contra
	 * 
	 * @param contraVo
	 * @return
	 */
	public Long saveOrUpdateContra(ContraVo contraVo) {
		Contra contra = null;
		Long id = contraVo.getContraId();
		if (id == null) {
			// save
			contra = new Contra();
			Company company = companyDao.findCompany(contraVo.getCompanyId());
			contra.setContraNumber(this.getContraNumberForCompany(contraVo.getCompanyId()));
			contra.setCompany(company);
			contra = this.setAttributesForContra(contra, contraVo);
			id = (Long) this.sessionFactory.getCurrentSession().save(contra);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.CONTRA, company);
		} else {
			// update
			contra = findContraObjectById(id);
			contra = this.setAttributesForContra(contra, contraVo);
			this.sessionFactory.getCurrentSession().merge(contra);
		}
		return id;
	}

	private Contra setAttributesForContra(Contra contra, ContraVo contraVo) {
		ErpCurrencyVo currencyVo = erpCurrencyDao.findCurrencyForDate(
				DateFormatter.convertStringToDate(contraVo.getDate()), contra.getCompany().getCurrency()
						.getBaseCurrency());
		if (currencyVo != null) {
			contra.setAmount(contraVo.getAmount().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
		} else {
			contra.setAmount(contraVo.getAmount());
		}
		contra.setContraDate(DateFormatter.convertStringToDate(contraVo.getDate()));
		contra.setNarration(contraVo.getNarration());
		for (ContraItemsVo itemsVo : contraVo.getEntries()) {
			Ledger ledger = ledgerDao.findLedgerObjectById(itemsVo.getAccTypeId());
			String accountType = ledger.getLedgerGroup().getAccountType().getType();
			BigDecimal plusAmount = new BigDecimal(0);
			BigDecimal minusAmount = new BigDecimal(0);
			// byLedger
			ContraEntries entries = (ContraEntries) this.sessionFactory.getCurrentSession()
					.createCriteria(ContraEntries.class).add(Restrictions.eq("id", itemsVo.getId())).uniqueResult();
			Transactions transaction = null;
			if (entries == null) {
				entries = new ContraEntries();
				transaction = new Transactions();
				transaction.setContraEntries(entries);
				transaction.setCompany(contra.getCompany());
			}else
				transaction=entries.getTransactions();
			entries.setLedger(ledger);
			if (currencyVo != null) {
				entries.setDebit(itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
				entries.setCredit(itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));

				if (accountType.contains("Expense")) {
					plusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					minusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Income")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Asset")) {
					plusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					minusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Liability")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Equity")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				}

			} else {
				entries.setCredit(itemsVo.getCreditAmnt());

				if (accountType.contains("Expense")) {
					plusAmount = itemsVo.getDebitAmnt();
					minusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Income")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Asset")) {
					plusAmount = itemsVo.getDebitAmnt();
					minusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Liability")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Equity")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				}

				entries.setDebit(itemsVo.getDebitAmnt());
			}
			transaction.setLedger(ledger);
			transaction.setDate(contra.getContraDate());
			transaction.setMinusAmount(minusAmount);
			transaction.setPlusAmount(plusAmount);
			transaction.setContraEntries(entries);
			entries.setTransactions(transaction);
			entries.setContra(contra);
			contra.getEntries().add(entries);

		}
		return contra;
	}

	/**
	 * Method to delete contra
	 * 
	 * @param id
	 */
	public void deleteContra(Long id) {
		Contra contra = findContraObjectById(id);
		this.sessionFactory.getCurrentSession().delete(contra);

	}

	/**
	 * Method to find contra by id
	 * 
	 * @param id
	 * @return
	 */
	public ContraVo findContraById(Long id) {
		Contra contra = findContraObjectById(id);
		return this.createContraVo(contra);
	}

	/**
	 * Method to create contraVo
	 * 
	 * @param contra
	 * @return
	 */
	private ContraVo createContraVo(Contra contra) {

		ErpCurrencyVo currencyVo = erpCurrencyDao.findCurrencyForDate(contra.getContraDate(), contra.getCompany()
				.getCurrency().getBaseCurrency());
		ContraVo vo = new ContraVo();
		vo.setCreatedBy(contra.getCreatedBy().getUserName());
		vo.setContraId(contra.getId());
		vo.setCompanyId(contra.getCompany().getId());
		vo.setDate(DateFormatter.convertDateToString(contra.getContraDate()));
		vo.setVoucherNumber(contra.getContraNumber());
		vo.setNarration(contra.getNarration());
		List<ContraItemsVo> itemsVos = new ArrayList<ContraItemsVo>();
		HashMap<Long, BigDecimal> byLedgers = new HashMap<Long, BigDecimal>();
		HashMap<Long, BigDecimal> toLedgers = new HashMap<Long, BigDecimal>();
		Long byLedgerId = null;
		Long toLedgerId = null;
		Ledger byLedger = null;
		Ledger toLedger= null;
		for (ContraEntries entry : contra.getEntries()) {
			ContraItemsVo itemsVo = new ContraItemsVo();
			Ledger account=entry.getLedger();
			itemsVo.setAccTypeId(account.getId());
			itemsVo.setAccount(account.getLedgerName());
			if (currencyVo != null) {
				
				itemsVo.setDebitAmnt(entry.getDebit().multiply(currencyVo.getRate())
						.setScale(2, RoundingMode.HALF_EVEN));
				itemsVo.setCreditAmnt(entry.getCredit().multiply(currencyVo.getRate())
						.setScale(2, RoundingMode.HALF_EVEN));
				
			} else {
				itemsVo.setCreditAmnt(entry.getCredit());
				itemsVo.setDebitAmnt(entry.getDebit());
				
			}
			if(entry.getDebit().compareTo(new BigDecimal(0))!=0){
				byLedger=entry.getLedger();
				byLedgers.put(entry.getId(), itemsVo.getDebitAmnt());					
			}else if(entry.getCredit().compareTo(new BigDecimal(0))!=0){
				toLedger=entry.getLedger();
				toLedgers.put(entry.getId(), itemsVo.getCreditAmnt());
			}
			
			itemsVo.setId(entry.getId());
			itemsVos.add(itemsVo);
		}
		vo.setByLedgerIds(byLedgers);
		vo.setToLedgerIds(toLedgers);
		if(vo.getByLedgerIds().size()>1){
			vo.setFromLedger("Multiple Accounts");
		}else if(vo.getByLedgerIds().size()==1){
			vo.setFromLedger(byLedger.getLedgerName());
		}
		if(vo.getToLedgerIds().size()>1){
			vo.setToLedger("Multiple Accounts");
		}else if(vo.getToLedgerIds().size()==1){
			vo.setToLedger(toLedger.getLedgerName());
		}
		vo.setEntries(itemsVos);
		if (currencyVo != null) {
			vo.setAmount(contra.getAmount().multiply(currencyVo.getRate()).setScale(2, RoundingMode.HALF_EVEN));
		} else {
			vo.setAmount(contra.getAmount());
		}
		return vo;
	
	}

	/**
	 * method to find contra object by id
	 * 
	 * @param id
	 * @return
	 */
	public Contra findContraObjectById(Long id) {
		Contra contra = null;
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Contra.class);
		criteria.add(Restrictions.eq("id", id));
		contra = (Contra) criteria.uniqueResult();
		if (contra == null)
			throw new ItemNotFoundException("Contra Not Found");
		return contra;
	}

	/**
	 * method to find all contras for company
	 * 
	 * @param companyId
	 * @return
	 */
	public List<ContraVo> findAllContrasForCompany(Long companyId) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Contra.class);
		criteria.add(Restrictions.eq("company", companyDao.findCompany(companyId)));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		List<Contra> contras = criteria.list();
		List<ContraVo> vos = new ArrayList<ContraVo>();
		if (contras != null) {
			for (Contra contra : contras) {
				vos.add(this.createContraVo(contra));
			}
		}
		return vos;
	}

	/**
	 * method to find all journals for company within date
	 * 
	 * @param companyId
	 * @return
	 */
	public List<ContraVo> findAllContrasForCompanyWithinDate(Long companyId, String start, String end) {
		Date strt = DateFormatter.convertStringToDate(start);
		Date endDate = DateFormatter.convertStringToDate(end);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Contra.class);
		criteria.add(Restrictions.eq("company", companyDao.findCompany(companyId)))
				.add(Restrictions.ge("contraDate", strt)).add(Restrictions.le("contraDate", endDate));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		List<Contra> contras = criteria.list();
		List<ContraVo> vos = new ArrayList<ContraVo>();
		if (contras != null) {
			for (Contra contra : contras) {
				vos.add(this.createContraVo(contra));
			}
		}
		return vos;
	}

	/**
	 * Method to find all contra for by ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ContraVo> findAllContrasForLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		Date start = DateFormatter.convertStringToDate(startDate);
		Date end = DateFormatter.convertStringToDate(endDate);
		Ledger ledger = ledgerDao.findLedgerObjectById(ledgerId);
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Contra.class);
		criteria.add(Restrictions.eq("company", companyDao.findCompany(companyId)));
		criteria.add(Restrictions.between("contraDate", start, end));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		Criterion byLedger = Restrictions.eq("ledgerBy", ledger);
		Criterion toLedger = Restrictions.eq("ledgerTo", ledger);

		LogicalExpression exp = Restrictions.or(byLedger, toLedger);
		criteria.add(exp);

		List<Contra> contras = criteria.list();
		List<ContraVo> vos = new ArrayList<ContraVo>();
		if (contras != null) {
			for (Contra contra : contras) {
				vos.add(this.createContraVo(contra));
			}
		}
		return vos;
	}

	/**
	 * Method to find contras for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ContraVo> findAllContrasForToLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Contra.class);
		criteria.add(Restrictions.eq("company", companyDao.findCompany(companyId)));
		criteria.add(Restrictions.eq("ledgerTo", ledgerDao.findLedgerObjectById(ledgerId)));
		List<Contra> contras = new ArrayList<Contra>();
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		contras = criteria.list();
		List<ContraVo> vos = new ArrayList<ContraVo>();
		for (Contra contra : contras) {
			vos.add(this.createContraVo(contra));
		}
		return vos;
	}

	/**
	 * Method to find contras for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ContraVo> findAllContrasForByLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Contra.class);
		criteria.add(Restrictions.eq("company", companyDao.findCompany(companyId)));
		criteria.add(Restrictions.eq("ledgerBy", ledgerDao.findLedgerObjectById(ledgerId)));
		List<Contra> contras = new ArrayList<Contra>();
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		contras = criteria.list();
		List<ContraVo> vos = new ArrayList<ContraVo>();
		for (Contra contra : contras) {
			vos.add(this.createContraVo(contra));
		}
		return vos;
	}

	/**
	 * Method to get contra voucher number
	 * 
	 * @param companyId
	 * @return
	 */
	public String getContraNumberForCompany(Long companyId) {
		NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(NumberPropertyConstants.CONTRA, companyId);
		String code = numberPropertyVo.getPrefix() + String.valueOf(numberPropertyVo.getNumber());
		return code;

	}

}


class Main{
	public static void main(String[] args) {
		String number= "dfd522-dfdsf-dsfds54545/50";
		Map<String, Long> property =check(number);
		for(Map.Entry<String, Long> entry : property.entrySet()){
			System.out.println(entry.getKey()+"\t"+String.valueOf(entry.getValue()+1));
		}
	}
	
	public static Map<String, Long> check(String number){
		char[] array = number.toCharArray();
		Map<String, Long> property=new LinkedHashMap<String, Long>();
		int limit=0;
		for(int i=array.length-1;i>=0;i--){
			int a = array[i];
			if(a>47&&a<58){
			}
			else
			{
				limit=i;
				break;
			}
		}
		String prefix = number.substring(0, limit+1);
		String suffix = number.substring(prefix.length());
		property.put(prefix, Long.parseLong(suffix));
		return property;
	}
}