package com.accounts.web.dao;

import java.util.List;

import com.accounts.web.entities.Ledger;
import com.accounts.web.vo.LedgerVo;
import com.erp.web.vo.CompanyVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
public interface LedgerDao {

	/**
	 * Method to save or update ledger
	 * 
	 * @param ledgerVo
	 * @return
	 */
	public Long saveOrUpdateLedger(LedgerVo ledgerVo);

	/**
	 * Method to find ledger By Id
	 * @param id
	 * @return
	 */
	public LedgerVo findLedgerById(Long id);
	
	/**
	 * Method to find ledger By Id
	 * @param id
	 * @return
	 */
	public Ledger findLedgerObjectById(Long id);
	
	public Ledger findLedgerObjectByName(String name,Long companyId);
	/**
	 * Method to delete ledger
	 * 
	 * @param id
	 */
	public void deleteLedger(Long id);

	/**
	 * Method to list all ledgers
	 * 
	 * @return
	 */
	public List<LedgerVo> findAllLedgers();

	/**
	 * List ledgers for company
	 *  
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> findLedgersForCompany(Long companyId);
	
	/**
	 * method to find ledgers for salary
	 * @return
	 */
	public List<LedgerVo> findAccountsForSalary(Long companyId);
	
	/**
	 * List ledgers under ledegrGroup
	 *  
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> findLedgersForLedgerGroup(Long companyId,Long groupId);
	
	/**
	 * method to find ledgers for contra
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> contraLedgers(Long companyId);
	
	/**
	 * method to find ledgers for journal
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> journalLedgers(Long companyId);
	
	/**
	 * method to find ledgers for payment
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> paymentLedgers(Long companyId);
	
	/**
	 * method to find ledgers for receipt
	 * @param companyId
	 * @return
	 */
	public List<LedgerVo> receiptLedgers(Long companyId);
}
