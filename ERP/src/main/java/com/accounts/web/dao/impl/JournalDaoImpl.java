package com.accounts.web.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.JournalDao;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.entities.Journal;
import com.accounts.web.entities.JournalEntries;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.Transactions;
import com.accounts.web.vo.JournalItemsVo;
import com.accounts.web.vo.JournalVo;
import com.erp.web.constants.NumberPropertyConstants;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.dao.NumberPropertyDao;
import com.erp.web.entities.Company;
import com.erp.web.vo.ErpCurrencyVo;
import com.erp.web.vo.NumberPropertyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 15-August-2015
 *
 */
@Repository
public class JournalDaoImpl extends AbstractDao implements JournalDao {

	@Autowired
	private LedgerDao ledgerDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private ErpCurrencyDao erpCurrencyDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	/**
	 * Method to save or update journal
	 * 
	 * @param journalVo
	 * @return
	 */
	public Long saveOrUpdateJournal(JournalVo journalVo) {
		Journal journal = null;
		Long id = journalVo.getJournalId();
		if (id == null) {
			// save
			journal = new Journal();
			journal.setIsDependant(false);
			Company company = companyDao.findCompany(journalVo.getCompanyId());
			journal.setJournalNumber(this.getJournalNumberForCompany(journalVo.getCompanyId()));
			journal.setCompany(company);
			journal.setIsDependant(false);
			journal = this.setAttributesForJournal(journal, journalVo, true);
			id = (Long) this.sessionFactory.getCurrentSession().save(journal);
			numberPropertyDao.incrementNumberProperty(NumberPropertyConstants.JOURNAL, company);
		} else {
			// update
			journal = findJournalObjectById(id);
			journal = this.setAttributesForJournal(journal, journalVo, true);
			this.sessionFactory.getCurrentSession().merge(journal);
		}
		return id;
	}

	/**
	 * Method to get journal number
	 * 
	 * @param company
	 * @return
	 */
	public String getJournalNumberForCompany(Long companyId) {
		NumberPropertyVo numberPropertyVo = numberPropertyDao.findNumberProperty(NumberPropertyConstants.JOURNAL,
				companyId);
		String code = numberPropertyVo.getPrefix() + String.valueOf(numberPropertyVo.getNumber());
		return code;
	}

	/**
	 * Method to set attributes for journal
	 * 
	 * @param journal
	 * @param journalVo
	 * @return
	 */
	public Journal setAttributesForJournal(Journal journal, JournalVo journalVo, Boolean withCurrency) {
		ErpCurrencyVo currencyVo = null;
		if (withCurrency)
			currencyVo = erpCurrencyDao.findCurrencyForDate(DateFormatter.convertStringToDate(journalVo.getDate()),
					journal.getCompany().getCurrency().getBaseCurrency());
		if (currencyVo != null) {
			journal.setAmount(journalVo.getAmount().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
		} else {
			journal.setAmount(journalVo.getAmount());
		}
		journal.setJournalDate(DateFormatter.convertStringToDate(journalVo.getDate()));
		journal.setNarration(journalVo.getNarration());
		journal.setIsArchive(false);
		for (JournalItemsVo itemsVo : journalVo.getEntries()) {
			Ledger ledger = ledgerDao.findLedgerObjectById(itemsVo.getAccTypeId());
			String accountType = ledger.getLedgerGroup().getAccountType().getType();
			BigDecimal plusAmount = new BigDecimal(0);
			BigDecimal minusAmount = new BigDecimal(0);
			// byLedger
			JournalEntries entries = (JournalEntries) this.sessionFactory.getCurrentSession()
					.createCriteria(JournalEntries.class).add(Restrictions.eq("id", itemsVo.getId())).uniqueResult();
			Transactions transaction = null;
			if (entries == null) {
				entries = new JournalEntries();
				transaction = new Transactions();
				transaction.setJournalEntries(entries);
				transaction.setCompany(journal.getCompany());
			}else 
				transaction=entries.getTransactions();
			entries.setLedger(ledger);
			if (currencyVo != null) {
				entries.setDebit(itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
				entries.setCredit(itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));

				if (accountType.contains("Expense")) {
					plusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					minusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Income")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Asset")) {
					plusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					minusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Liability")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				} else if (accountType.contains("Equity")) {
					minusAmount = itemsVo.getDebitAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
					plusAmount = itemsVo.getCreditAmnt().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN);
				}

			} else {
				entries.setCredit(itemsVo.getCreditAmnt());

				if (accountType.contains("Expense")) {
					plusAmount = itemsVo.getDebitAmnt();
					minusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Income")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Asset")) {
					plusAmount = itemsVo.getDebitAmnt();
					minusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Liability")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				} else if (accountType.contains("Equity")) {
					minusAmount = itemsVo.getDebitAmnt();
					plusAmount = itemsVo.getCreditAmnt();
				}

				entries.setDebit(itemsVo.getDebitAmnt());
			}
			transaction.setLedger(ledger);
			transaction.setDate(journal.getJournalDate());
			transaction.setMinusAmount(minusAmount);
			transaction.setPlusAmount(plusAmount);
			transaction.setJournalEntries(entries);
			entries.setTransactions(transaction);
			entries.setJournal(journal);
			journal.getEntries().add(entries);

		}
		return journal;
	}

	/**
	 * Method to delete journal
	 * 
	 * @param id
	 */
	public void deleteJournal(Long id) {

		Journal journal = findJournalObjectById(id);
		if (journal == null)
			throw new ItemNotFoundException("Journal Does Not Exist");
		this.sessionFactory.getCurrentSession().delete(journal);
	}

	/**
	 * Method to find journal by id
	 * 
	 * @param id
	 * @return
	 */
	public JournalVo findJournalById(Long id) {
		Journal journal = findJournalObjectById(id);
		if (journal == null)
			throw new ItemNotFoundException("Journal Does Not Exist");
		return this.createJournalVo(journal);
	}

	/**
	 * Method to create JournalVo
	 * 
	 * @param journal
	 * @return
	 */
	private JournalVo createJournalVo(Journal journal) {
		ErpCurrencyVo currencyVo = erpCurrencyDao.findCurrencyForDate(journal.getJournalDate(), journal.getCompany()
				.getCurrency().getBaseCurrency());
		JournalVo vo = new JournalVo();
		vo.setCreatedBy(journal.getCreatedBy().getUserName());
		vo.setJournalId(journal.getId());
		vo.setCompanyId(journal.getCompany().getId());
		vo.setDate(DateFormatter.convertDateToString(journal.getJournalDate()));
		vo.setVoucherNumber(journal.getJournalNumber());
		vo.setNarration(journal.getNarration());
		vo.setIsDependant(journal.getIsDependant());
		List<JournalItemsVo> itemsVos = new ArrayList<JournalItemsVo>();
		HashMap<Long, BigDecimal> byLedgers = new HashMap<Long, BigDecimal>();
		HashMap<Long, BigDecimal> toLedgers = new HashMap<Long, BigDecimal>();
		Long byLedgerId = null;
		Long toLedgerId = null;
		Ledger byLedger = null;
		Ledger toLedger = null;
		for (JournalEntries entry : journal.getEntries()) {
			JournalItemsVo itemsVo = new JournalItemsVo();
			Ledger ledger = entry.getLedger();
			itemsVo.setAccTypeId(ledger.getId());
			itemsVo.setLedger(ledger.getLedgerName());
			if (currencyVo != null) {

				itemsVo.setDebitAmnt(entry.getDebit().multiply(currencyVo.getRate())
						.setScale(2, RoundingMode.HALF_EVEN));
				itemsVo.setCreditAmnt(entry.getCredit().multiply(currencyVo.getRate())
						.setScale(2, RoundingMode.HALF_EVEN));

			} else {
				itemsVo.setCreditAmnt(entry.getCredit());
				itemsVo.setDebitAmnt(entry.getDebit());

			}
			if (entry.getDebit().compareTo(new BigDecimal(0)) != 0) {
				byLedger = entry.getLedger();
				byLedgers.put(entry.getId(), itemsVo.getDebitAmnt());
			} else if (entry.getCredit().compareTo(new BigDecimal(0)) != 0) {
				toLedger = entry.getLedger();
				toLedgers.put(entry.getId(), itemsVo.getCreditAmnt());
			}

			itemsVo.setId(entry.getId());
			itemsVos.add(itemsVo);
		}
		vo.setByLedgerIds(byLedgers);
		vo.setToLedgerIds(toLedgers);
		if (vo.getByLedgerIds().size() > 1) {
			vo.setFromLedger("Multiple Accounts");
		} else if (vo.getByLedgerIds().size() == 1) {
			vo.setFromLedger(byLedger.getLedgerName());
		}
		if (vo.getToLedgerIds().size() > 1) {
			vo.setToLedger("Multiple Accounts");
		} else if (vo.getToLedgerIds().size() == 1) {
			vo.setToLedger(toLedger.getLedgerName());
		}
		vo.setEntries(itemsVos);
		if (currencyVo != null) {
			vo.setAmount(journal.getAmount().multiply(currencyVo.getRate()).setScale(2, RoundingMode.HALF_EVEN));
		} else {
			vo.setAmount(journal.getAmount());
		}
		return vo;
	}

	/**
	 * method to find journal object by id
	 * 
	 * @param id
	 * @return
	 */
	public Journal findJournalObjectById(Long id) {
		Journal journal = (Journal) this.sessionFactory.getCurrentSession().createCriteria(Journal.class)
				.add(Restrictions.eq("isArchive", false)).add(Restrictions.eq("id", id)).uniqueResult();
		return journal;
	}

	/**
	 * method to find all journals for company
	 * 
	 * @param companyId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<JournalVo> findAllJournalsForCompany(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Does Not Exist");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Journal.class)
				.add(Restrictions.eq("company", company)).add(Restrictions.eq("isArchive", false));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Journal> journals = criteria.list();
		List<JournalVo> journalVos = new ArrayList<JournalVo>();
		for (Journal journal : journals) {
			journalVos.add(this.createJournalVo(journal));
		}
		return journalVos;
	}

	/**
	 * method to find all journals for company within date
	 * 
	 * @param companyId
	 * @return
	 */
	public List<JournalVo> findAllJournalsForCompanyWithinDate(Long companyId, String start, String end) {
		Company company = companyDao.findCompany(companyId);
		Date strt = DateFormatter.convertStringToDate(start);
		Date endDate = DateFormatter.convertStringToDate(end);
		if (company == null)
			throw new ItemNotFoundException("Company Does Not Exist");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Journal.class)
				.add(Restrictions.eq("company", company)).add(Restrictions.ge("journalDate", strt))
				.add(Restrictions.le("journalDate", endDate)).add(Restrictions.eq("isArchive", false));

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Journal> journals = criteria.list();
		List<JournalVo> journalVos = new ArrayList<JournalVo>();
		for (Journal journal : journals) {
			journalVos.add(this.createJournalVo(journal));
		}
		return journalVos;

	}

	/**
	 * Method to find all journal for by ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<JournalVo> findAllJournalsForLedgerWithinDateRange(Long companyId, Long ledgerId, String startDate,
			String endDate) {
		Date start = DateFormatter.convertStringToDate(startDate);
		Date end = DateFormatter.convertStringToDate(endDate);
		Ledger ledger = ledgerDao.findLedgerObjectById(ledgerId);
		if (ledger == null)
			throw new ItemNotFoundException("Ledger Not Found");
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Does Not Exist");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Journal.class)
				.add(Restrictions.eq("company", company)).add(Restrictions.between("journalDate", start, end))
				.add(Restrictions.eq("isArchive", false));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Journal> journals = criteria.list();

		/*
		 * Criteria criteria2 =
		 * this.sessionFactory.getCurrentSession().createCriteria
		 * (JournalByDetails.class) .add(Restrictions.eq("ledger", ledger))
		 * .add(Restrictions.in("byJournal", journals));
		 * criteria2.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		 * List<JournalByDetails> byDetails = criteria.list();
		 */

		List<JournalVo> journalVos = new ArrayList<JournalVo>();
		for (Journal journal : journals) {
			journalVos.add(this.createJournalVo(journal));
		}
		return journalVos;
	}

	/**
	 * Method to find journals for to ledger within date range
	 * 
	 * @param ledgerId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	/*
	 * public List<JournalVo> findAllJournalsForToLedgerWithinDateRange(Long
	 * companyId,Long ledgerId, String startDate, String endDate) {
	 * List<JournalVo> journalVos = null; return journalVos; }
	 */

}
