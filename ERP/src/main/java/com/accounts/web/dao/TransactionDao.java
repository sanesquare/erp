package com.accounts.web.dao;

import java.util.List;
import java.util.Map;

import com.accounts.web.entities.Ledger;
import com.accounts.web.vo.BalanceSheetVo;
import com.accounts.web.vo.LedgerGroupViewVo;
import com.accounts.web.vo.LedgerViewVo;
import com.accounts.web.vo.LedgerVo;
import com.accounts.web.vo.ProfitAndLossVo;
import com.accounts.web.vo.TrialBalanceLedgerGroupVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 21-August-2015
 *
 */
public interface TransactionDao {

	/**
	 * method to find transaction balance
	 * 
	 * @param startDate
	 * @param endDate
	 * @param ledgerId
	 * @param companyId
	 * @return
	 */
	public LedgerViewVo findTRansactionsForLedger(String startDate, String endDate, Ledger ledger, Long ledgerId, Long companyId);
	
	public LedgerGroupViewVo findTRansactionsForLedgerGroup(String startDate, String endDate, Long groupId,Long companyId);

	/**
	 * Method for trial Balance calculations
	 * 
	 * @param companyId
	 * @return
	 */
	public Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> trialBalanceCalculations(Long companyId);

	/**
	 * Method for trial balance with date range
	 * 
	 * @param companyId
	 * @param start
	 * @param end
	 * @return
	 */
	public Map<TrialBalanceLedgerGroupVo, List<LedgerVo>> trialBalanceCalculations(Long companyId, String start, String end);

	/**
	 * method to calculate profit and loss
	 * 
	 * @param companyId
	 * @param start
	 * @param end
	 * @return
	 */
	public ProfitAndLossVo profitAndLossentries(Long companyId, String start, String end);

	/**
	 * method to calculate profit and loss
	 * 
	 * @param companyId
	 * @return
	 */
	public ProfitAndLossVo profitAndLossentries(Long companyId);

	/**
	 * method to calculate balance sheet
	 * 
	 * @param companyId
	 * @return
	 */
	public BalanceSheetVo balanceSheetCalculation(Long companyId);

	/**
	 * method to calculate balance sheet
	 * 
	 * @param companyId
	 * @param start
	 * @param end
	 * @return
	 */
	public BalanceSheetVo balanceSheetCalculation(Long companyId, String start, String end);
}
