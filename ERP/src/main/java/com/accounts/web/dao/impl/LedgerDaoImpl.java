package com.accounts.web.dao.impl;

import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.constants.AccountsConstants;
import com.accounts.web.dao.LedgerDao;
import com.accounts.web.dao.LedgerGroupDao;
import com.accounts.web.entities.Ledger;
import com.accounts.web.entities.LedgerGroup;
import com.accounts.web.vo.LedgerVo;
import com.erp.web.dao.CompanyDao;
import com.erp.web.dao.ErpCurrencyDao;
import com.erp.web.entities.Company;
import com.erp.web.vo.ErpCurrencyVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.util.DateFormatter;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
@Repository
public class LedgerDaoImpl extends AbstractDao implements LedgerDao {

	@Autowired
	private LedgerGroupDao groupDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private ErpCurrencyDao erpCurrencyDao;

	/**
	 * Method to save or update ledger
	 * 
	 * @param ledgerVo
	 * @return
	 */
	public Long saveOrUpdateLedger(LedgerVo ledgerVo) {
		ledgerVo.setLedgerName(ledgerVo.getLedgerName().trim());
		Long id = ledgerVo.getLedgerId();
		Ledger ledger = null;
		Ledger ledgerWithSameName = this.findLedgerObjectByName(ledgerVo.getLedgerName(), ledgerVo.getCompanyId());
		if (id != null) {
			ledger = findLedgerObjectById(id);
			if (ledger != null && ledgerWithSameName != null && !ledger.equals(ledgerWithSameName))
				throw new DuplicateItemException("Ledger Already Exist.");
			ledger = this.setAttributesForLedger(ledger, ledgerVo);
		} else {
			if (ledgerWithSameName != null)
				throw new DuplicateItemException("Ledger Already Exist.");
			ledger = new Ledger();
			Company company = companyDao.findCompany(ledgerVo.getCompanyId());
			ledger.setCompany(company);
			ledger.setIsEditable(true);
			ledger = this.setAttributesForLedger(ledger, ledgerVo);
			id = (Long) this.sessionFactory.getCurrentSession().save(ledger);
		}
		return id;
	}

	/**
	 * Method to find ledger object by id
	 * 
	 * @param id
	 * @return
	 */
	public Ledger findLedgerObjectById(Long id) {
		Ledger ledger = (Ledger) this.sessionFactory.getCurrentSession().createCriteria(Ledger.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
		return ledger;
	}

	/**
	 * Method to set attributes for ledger
	 * 
	 * @param ledger
	 * @param ledgerVo
	 * @return
	 */
	private Ledger setAttributesForLedger(Ledger ledger, LedgerVo ledgerVo) {
		ledger.setStartDate(DateFormatter.convertStringToDate(ledgerVo.getStartDate()));
		ledger.setLedgerName(ledgerVo.getLedgerName());

		ErpCurrencyVo currencyVo = erpCurrencyDao.findCurrencyForDate(ledger.getStartDate(), ledger.getCompany()
				.getCurrency().getBaseCurrency());

		if (currencyVo != null) {
			ledger.setOpeningBalance(ledgerVo.getOpeningBalance().divide(currencyVo.getRate(), 2,
					RoundingMode.HALF_EVEN));
			ledger.setBalance(ledgerVo.getOpeningBalance().divide(currencyVo.getRate(), 2, RoundingMode.HALF_EVEN));
		} else {
			ledger.setOpeningBalance(ledgerVo.getOpeningBalance());
			ledger.setBalance(ledgerVo.getOpeningBalance());
		}
		ledger.setLedgerGroup(groupDao.findLedgerGroup(ledgerVo.getLedgerGroupId()));
		return ledger;

	}

	/**
	 * Method to find ledger By Id
	 * 
	 * @param id
	 * @return
	 */
	public LedgerVo findLedgerById(Long id) {
		Ledger ledger = findLedgerObjectById(id);
		if (ledger == null)
			throw new ItemNotFoundException("Ledger Does Not Exists");
		return this.createLedgerVo(ledger, ledger.getCompany());
	}

	/**
	 * Method to create ledger vo
	 * 
	 * @param ledger
	 * @return
	 */
	private LedgerVo createLedgerVo(Ledger ledger, Company company) {
		LedgerGroup group = ledger.getLedgerGroup();
		LedgerVo ledgerVo = new LedgerVo();
		ErpCurrencyVo currencyVo = null;

		if (company != null)
			currencyVo = erpCurrencyDao.findCurrencyForDate(ledger.getStartDate(), company.getCurrency()
					.getBaseCurrency());
		// MathContext context = new MathContext(2, RoundingMode.HALF_EVEN);
		if (currencyVo != null) {
			ledgerVo.setOpeningBalance(ledger.getOpeningBalance().multiply(currencyVo.getRate()));
			ledgerVo.setBalance(ledger.getOpeningBalance().multiply(currencyVo.getRate()));
		} else {
			ledgerVo.setOpeningBalance(ledger.getOpeningBalance());
			ledgerVo.setBalance(ledger.getOpeningBalance());
		}

		if (ledger.getCompany() != null)
			ledgerVo.setCompanyId(company.getId());
		ledgerVo.setLedgerId(ledger.getId());
		ledgerVo.setLedgerGroupId(group.getId());
		ledgerVo.setLedgerGroup(group.getGroupName());
		ledgerVo.setLedgerName(ledger.getLedgerName());
		ledgerVo.setStartDate(DateFormatter.convertDateToString(ledger.getStartDate()));
		ledgerVo.setAccountType(group.getAccountType().getType());
		if (ledger.getCreatedBy() != null && ledger.getCreatedOn() != null)
			ledgerVo.setCreatedBy(ledger.getCreatedBy().getUserName() + " on "
					+ DateFormatter.convertDateToString(ledger.getCreatedOn()));
		ledgerVo.setIsEditable(ledger.getIsEditable());
		return ledgerVo;
	}

	/**
	 * Method to delete ledger
	 * 
	 * @param id
	 */
	public void deleteLedger(Long id) {
		Ledger ledger = findLedgerObjectById(id);
		if (ledger == null)
			throw new ItemNotFoundException("Ledger Does Not Exists");
		else if (ledger.getIsEditable() == false)
			throw new ItemNotFoundException("Ledger Cannot be Deleted");
		this.sessionFactory.getCurrentSession().delete(ledger);
	}

	/**
	 * Method to list all ledgers
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<LedgerVo> findAllLedgers() {
		List<LedgerVo> ledgerVos = new ArrayList<LedgerVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Ledger.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("ledgerName"));
		List<Ledger> ledgers = criteria.list();
		for (Ledger ledger : ledgers) {
			ledgerVos.add(this.createLedgerVo(ledger, ledger.getCompany()));
		}
		List<LedgerVo> sortedVos = new ArrayList<LedgerVo>();
		for (LedgerVo vo : ledgerVos) {
			if (vo.getLedgerName().equals(AccountsConstants.CASH)) {
				ledgerVos.remove(vo);
				sortedVos.add(0, vo);
				break;
			}
		}
		sortedVos.addAll(ledgerVos);
		return sortedVos;
	}

	/**
	 * List ledgers for company
	 * 
	 * @param companyId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<LedgerVo> findLedgersForCompany(Long companyId) {
		List<LedgerVo> ledgerVos = new ArrayList<LedgerVo>();
		Company company = companyDao.findCompany(companyId);

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Ledger.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.addOrder(Order.asc("ledgerName"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Ledger> ledgers = criteria.list();
		for (Ledger ledger : ledgers) {
			ledgerVos.add(this.createLedgerVo(ledger, company));
		}
		List<LedgerVo> sortedVos = new ArrayList<LedgerVo>();
		for (LedgerVo vo : ledgerVos) {
			if (vo.getLedgerName().equals(AccountsConstants.CASH)) {
				ledgerVos.remove(vo);
				sortedVos.add(0, vo);
				break;
			}
		}
		sortedVos.addAll(ledgerVos);
		return sortedVos;

	}

	@SuppressWarnings("unchecked")
	public List<LedgerVo> findLedgersForLedgerGroup(Long companyId, Long groupId) {
		List<LedgerVo> vos = new ArrayList<LedgerVo>();
		LedgerGroup group = groupDao.findLedgerGroup(groupId);
		if (group == null)
			throw new ItemNotFoundException("Ledger Group Not Found");
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Ledger.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("ledgerGroup", group));
		criteria.addOrder(Order.asc("ledgerName"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Ledger> ledgers = criteria.list();
		for (Ledger ledger : ledgers) {
			vos.add(this.createLedgerVo(ledger,company));
		}
		List<LedgerVo> sortedVos = new ArrayList<LedgerVo>();
		for (LedgerVo vo : vos) {
			if (vo.getLedgerName().equals(AccountsConstants.CASH)) {
				vos.remove(vo);
				sortedVos.add(0, vo);
				break;
			}
		}
		sortedVos.addAll(vos);
		return sortedVos;
	}

	public List<LedgerVo> findAccountsForSalary(Long companyId) {
		List<LedgerVo> vos = new ArrayList<LedgerVo>();
		LedgerGroup group = groupDao.findLedgerGroup(AccountsConstants.BANK_ACCOUNT, companyId);
		if (group == null)
			throw new ItemNotFoundException("Ledger Group Not Found");
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");

		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Ledger.class);
		Criterion criterion = Restrictions.eq("ledgerGroup", group);
		Criterion criterion2 = Restrictions.eq("ledgerName", AccountsConstants.CASH);
		LogicalExpression expression = Restrictions.or(criterion, criterion2);
		criteria.add(expression);
		criteria.add(Restrictions.eq("company", company));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("ledgerName"));
		List<Ledger> ledgers = criteria.list();
		for (Ledger ledger : ledgers) {
			vos.add(this.createLedgerVo(ledger, company));
		}
		List<LedgerVo> sortedVos = new ArrayList<LedgerVo>();
		for (LedgerVo vo : vos) {
			if (vo.getLedgerName().equals(AccountsConstants.CASH)) {
				vos.remove(vo);
				sortedVos.add(0, vo);
				break;
			}
		}
		sortedVos.addAll(vos);
		return sortedVos;
	}

	@SuppressWarnings("unchecked")
	public List<LedgerVo> contraLedgers(Long companyId) {
		LedgerGroup bankAccountGroup = groupDao.findLedgerGroup(AccountsConstants.BANK_ACCOUNT, companyId);
		LedgerGroup cashAtBankGroup = groupDao.findLedgerGroup(AccountsConstants.CASH_AT_BANK, companyId);
		LedgerGroup cashInHandGroup = groupDao.findLedgerGroup(AccountsConstants.CASH_IN_HAND, companyId);
		List<LedgerGroup> bankAccountGroups = new ArrayList<LedgerGroup>();
		if (bankAccountGroup != null)
			bankAccountGroups.add(bankAccountGroup);
		if (cashAtBankGroup != null)
			bankAccountGroups.add(cashAtBankGroup);
		if (cashInHandGroup != null)
			bankAccountGroups.add(cashInHandGroup);
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		List<LedgerVo> ledgerVos = new ArrayList<LedgerVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Ledger.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.in("ledgerGroup", bankAccountGroups));
		criteria.addOrder(Order.asc("ledgerName"));
		List<Ledger> ledgers = criteria.list();
		for (Ledger ledger : ledgers) {
			ledgerVos.add(this.createLedgerVo(ledger,company));
		}
		List<LedgerVo> sortedVos = new ArrayList<LedgerVo>();
		for (LedgerVo vo : ledgerVos) {
			if (vo.getLedgerName().equals(AccountsConstants.CASH)) {
				ledgerVos.remove(vo);
				sortedVos.add(0, vo);
				break;
			}
		}
		sortedVos.addAll(ledgerVos);
		return sortedVos;
	}

	public List<LedgerVo> journalLedgers(Long companyId) {
		LedgerGroup bankAccountGroup = groupDao.findLedgerGroup(AccountsConstants.BANK_ACCOUNT, companyId);
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		List<LedgerVo> ledgerVos = new ArrayList<LedgerVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Ledger.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("company", company));
		Criterion group = Restrictions.eq("ledgerGroup", bankAccountGroup);
		Criterion cash = Restrictions.eq("ledgerName", AccountsConstants.CASH);
		LogicalExpression orExpression = Restrictions.or(group, cash);
		criteria.add(Restrictions.not(orExpression));
		criteria.addOrder(Order.asc("ledgerName"));
		List<Ledger> ledgers = criteria.list();
		for (Ledger ledger : ledgers) {
			ledgerVos.add(this.createLedgerVo(ledger,company));
		}
		List<LedgerVo> sortedVos = new ArrayList<LedgerVo>();
		for (LedgerVo vo : ledgerVos) {
			if (vo.getLedgerName().equals(AccountsConstants.CASH)) {
				ledgerVos.remove(vo);
				sortedVos.add(0, vo);
				break;
			}
		}
		sortedVos.addAll(ledgerVos);
		return sortedVos;
	}

	public List<LedgerVo> paymentLedgers(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		List<LedgerGroup> directIncomeLedgers = groupDao.findLedgerGroupsOfAccountType(AccountsConstants.DIRECT_INCOME,company);
		List<LedgerGroup> inDirectIncomeLedgers = groupDao
				.findLedgerGroupsOfAccountType(AccountsConstants.INDIRECT_INCOME,company);
		List<LedgerVo> ledgerVos = new ArrayList<LedgerVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Ledger.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.not(Restrictions.in("ledgerGroup", directIncomeLedgers)));
		criteria.add(Restrictions.not(Restrictions.in("ledgerGroup", inDirectIncomeLedgers)));
		criteria.addOrder(Order.asc("ledgerName"));
		List<Ledger> ledgers = criteria.list();
		for (Ledger ledger : ledgers) {
			ledgerVos.add(this.createLedgerVo(ledger,company));
		}
		List<LedgerVo> sortedVos = new ArrayList<LedgerVo>();
		for (LedgerVo vo : ledgerVos) {
			if (vo.getLedgerName().equals(AccountsConstants.CASH)) {
				ledgerVos.remove(vo);
				sortedVos.add(0, vo);
				break;
			}
		}
		sortedVos.addAll(ledgerVos);
		return sortedVos;
	}

	public List<LedgerVo> receiptLedgers(Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		List<LedgerGroup> directExpenseLedgers = groupDao
				.findLedgerGroupsOfAccountType(AccountsConstants.DIRECT_EXPENSE,company);
		List<LedgerGroup> inDirectExpenseLedgers = groupDao
				.findLedgerGroupsOfAccountType(AccountsConstants.INDIRECT_EXPENSE,company);
		List<LedgerVo> ledgerVos = new ArrayList<LedgerVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Ledger.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.not(Restrictions.in("ledgerGroup", inDirectExpenseLedgers)));
		criteria.add(Restrictions.not(Restrictions.in("ledgerGroup", directExpenseLedgers)));
		criteria.add(Restrictions.eq("company", company));
		criteria.addOrder(Order.asc("ledgerName"));
		List<Ledger> ledgers = criteria.list();
		for (Ledger ledger : ledgers) {
			ledgerVos.add(this.createLedgerVo(ledger,company));
		}
		List<LedgerVo> sortedVos = new ArrayList<LedgerVo>();
		for (LedgerVo vo : ledgerVos) {
			if (vo.getLedgerName().equals(AccountsConstants.CASH)) {
				ledgerVos.remove(vo);
				sortedVos.add(0, vo);
				break;
			}
		}
		sortedVos.addAll(ledgerVos);
		return sortedVos;
	}

	public Ledger findLedgerObjectByName(String name, Long companyId) {
		Company company = companyDao.findCompany(companyId);
		if (company == null)
			throw new ItemNotFoundException("Company Not Found");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Ledger.class);
		criteria.add(Restrictions.eq("company", company)).add(Restrictions.eq("ledgerName", name.trim()));
		return (Ledger) criteria.uniqueResult();
	}

}
