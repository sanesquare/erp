package com.accounts.web.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.accounts.web.dao.LedgerAccountTypeDao;
import com.accounts.web.dao.LedgerGroupDao;
import com.accounts.web.entities.LedgerAccountType;
import com.accounts.web.entities.LedgerGroup;
import com.accounts.web.vo.LedgerGroupVo;
import com.erp.web.dao.CompanyDao;
import com.erp.web.entities.Company;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.exceptions.DuplicateItemException;
import com.hrms.web.exceptions.ItemNotFoundException;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
@Repository
public class LedgerGroupDaoImpl extends AbstractDao implements LedgerGroupDao {

	@Autowired
	private LedgerAccountTypeDao accountTypeDao;

	@Autowired
	private CompanyDao companyDao;

	public Long saveOrUpdateLedgerGroup(LedgerGroupVo groupVo) {
		groupVo.setGroupName(groupVo.getGroupName().trim());
		Long id = groupVo.getGroupId();
		LedgerGroup group = null;
		if (id != null) {
			// update
			group = this.findLedgerGroup(id);
			if (group.getIsEditable() == false)
				throw new ItemNotFoundException("Ledger Group Cannot Be Edited");
			group = this.setAttributes(group, groupVo);
			this.sessionFactory.getCurrentSession().merge(group);
		} else {
			// save
			group = new LedgerGroup();
			group.setIsEditable(true);
			group.setCompany(companyDao.findCompany(groupVo.getCompanyId()));
			group = this.setAttributes(group, groupVo);
			id = (Long) this.sessionFactory.getCurrentSession().save(group);
		}
		return id;
	}

	/**
	 * Method find ledger group by id
	 * 
	 * @param id
	 * @return
	 */
	public LedgerGroup findLedgerGroup(Long id) {
		LedgerGroup group = null;
		group = (LedgerGroup) this.sessionFactory.getCurrentSession().createCriteria(LedgerGroup.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
		if (group == null)
			throw new ItemNotFoundException("Ledger Group Does Not Exist");
		return group;
	}

	private LedgerGroup setAttributes(LedgerGroup group, LedgerGroupVo groupVo) {
		LedgerGroup test = findLedgerGroup(groupVo.getGroupName(), group.getCompany().getId());
		if (test != null) {
			if (test.getId().equals(group.getId())) {
			} else {
				throw new DuplicateItemException("Group with Same Name Already Exists");
			}
		}
		group.setGroupName(groupVo.getGroupName());
		group.setAccountType(accountTypeDao.findAccountTypeObjectById(groupVo.getAccountTypeId()));
		return group;
	}

	public void deleteLedgerGroup(Long id) {
		LedgerGroup group = findLedgerGroup(id);
		if (group == null)
			throw new ItemNotFoundException("Ledger Group Does Not exist");
		else if (group.getLedgers().size() > 0)
			throw new ItemNotFoundException("Ledger Group is assigned to various ledgers");
		else if (group.getIsEditable() == false)
			throw new ItemNotFoundException("Ledger Group Cannot be Deleted");
		else
			this.sessionFactory.getCurrentSession().delete(group);

	}

	public LedgerGroupVo findLedgerGroupById(Long id) {
		LedgerGroup group = findLedgerGroup(id);
		return this.createLedgerGroupVo(group);
	}

	private LedgerGroupVo createLedgerGroupVo(LedgerGroup group) {
		LedgerGroupVo groupVo = new LedgerGroupVo();
		groupVo.setGroupId(group.getId());
		groupVo.setAccounttype(group.getAccountType().getType());
		groupVo.setAccountTypeId(group.getAccountType().getId());
		groupVo.setGroupName(group.getGroupName());
		groupVo.setIsEditable(group.getIsEditable());
		return groupVo;
	}

	public LedgerGroupVo findLedgerGroupByName(String name, Long companyId) {

		return this.createLedgerGroupVo(this.findLedgerGroup(name, companyId));
	}

	public LedgerGroup findLedgerGroup(String name, Long companyId) {
		Company company = companyDao.findCompany(companyId);
		LedgerGroup group = (LedgerGroup) this.sessionFactory.getCurrentSession().createCriteria(LedgerGroup.class)
				.add(Restrictions.eq("groupName", name)).add(Restrictions.eq("company", company)).uniqueResult();
		return group;
	}

	@SuppressWarnings("unchecked")
	public List<LedgerGroupVo> findAllLedgerGroups() {
		List<LedgerGroupVo> vos = new ArrayList<LedgerGroupVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LedgerGroup.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("groupName"));
		List<LedgerGroup> groups = criteria.list();
		for (LedgerGroup group : groups) {
			vos.add(this.createLedgerGroupVo(group));
		}
		return vos;
	}

	/**
	 * Method to find ledger groups for company
	 * 
	 * @param companyId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<LedgerGroupVo> findAllLedgerGroupsForCompany(Long companyId) {
		List<LedgerGroupVo> vos = new ArrayList<LedgerGroupVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LedgerGroup.class);
		criteria.add(Restrictions.eq("company", companyDao.findCompany(companyId)));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.asc("groupName"));
		List<LedgerGroup> groups = criteria.list();
		for (LedgerGroup group : groups) {
			vos.add(this.createLedgerGroupVo(group));
		}
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<LedgerGroup> findLedgerGroupsOfAccountType(String accountType,Company company) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LedgerGroup.class);
		criteria.add(Restrictions.eq(
				"accountType",
				this.sessionFactory.getCurrentSession().createCriteria(LedgerAccountType.class)
						.add(Restrictions.eq("type", accountType)).uniqueResult()));
		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("company", company));
		criteria.addOrder(Order.asc("groupName"));
		return criteria.list();
	}

	public List<LedgerGroup> findInitialLedgerGroups() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LedgerGroup.class);
		criteria.add(Restrictions.eq("isInitial", true));
		criteria.addOrder(Order.asc("groupName"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<LedgerGroup> groups = criteria.list();
		return groups;
	}

}
