package com.accounts.web.dao.impl;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.springframework.stereotype.Repository;

import com.accounts.web.dao.LedgerAccountTypeDao;
import com.accounts.web.entities.LedgerAccountType;
import com.accounts.web.vo.LedgerAccountTypeVo;
import com.hrms.web.dao.AbstractDao;
import com.hrms.web.entities.AccountType;
import com.hrms.web.exceptions.ItemNotFoundException;
import com.hrms.web.vo.AccountTypeVo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since 12-August-2015
 */
@Repository
public class LedgerAccountTypeDaoImpl extends AbstractDao implements LedgerAccountTypeDao {

	/**
	 * Method to find account type by name
	 * @param type
	 * @return
	 */
	public LedgerAccountTypeVo findAccountTypeByName(String type) {
		LedgerAccountType accountType = (LedgerAccountType) this.sessionFactory.getCurrentSession().createCriteria(LedgerAccountType.class)
				.add(Restrictions.eq("type", type)).uniqueResult();
		return createAccountTypeVo(accountType);
	}
	/**
	 * Method to find account type object by name
	 * @param type
	 * @return
	 */
	public LedgerAccountType findAccountTypeObject(String type) {
		LedgerAccountType accountType = (LedgerAccountType) this.sessionFactory.getCurrentSession().createCriteria(LedgerAccountType.class)
				.add(Restrictions.eq("type", type)).uniqueResult();
		return accountType;
	}
	/**
	 * mMethod to create account type vo 
	 * @param accountType
	 * @return
	 */
	private LedgerAccountTypeVo createAccountTypeVo(LedgerAccountType accountType){
		
		LedgerAccountTypeVo vo = new LedgerAccountTypeVo();
		vo.setAccountType(accountType.getType());
		vo.setTypeId(accountType.getId());
		return vo;
	}
	/**
	 * method to list all account types
	 */
	@SuppressWarnings("unchecked")
	public List<LedgerAccountTypeVo> findAllAccountTypes() {
		List<LedgerAccountTypeVo> listVos = new ArrayList<LedgerAccountTypeVo>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(LedgerAccountType.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<LedgerAccountType> accountTypeList = criteria.list();
		for(LedgerAccountType type : accountTypeList){
			listVos.add(createAccountTypeVo(type));
		}
		return listVos;
	}
	/**
	 * Method to find account type Object by Id
	 * @param type
	 * @return
	 */
	public LedgerAccountType findAccountTypeObjectById(Long id) {
		LedgerAccountType accountType = null;
		accountType = (LedgerAccountType) this.sessionFactory.getCurrentSession().createCriteria(LedgerAccountType.class).add(Restrictions.eq("id",id)).uniqueResult();
		if(accountType==null)
			throw new ItemNotFoundException("Account Type not Found");
		return accountType;
	}
	/**
	 * Method to find account type by Id
	 * @param type
	 * @return
	 */
	public LedgerAccountTypeVo findAccountTypeById(Long id) {
		LedgerAccountType accountType = (LedgerAccountType) this.sessionFactory.getCurrentSession().createCriteria(LedgerAccountType.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
		return createAccountTypeVo(accountType);
	}

}
