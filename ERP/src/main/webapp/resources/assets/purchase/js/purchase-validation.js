/**
 * @author Shamsheer & Vinutha
 * @since 4-August-2015
 */
$(document).ready(function() {
	$("#vendr-frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			name : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			},
			address : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Address is required'
					}
				}
			},
			countryId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Country is required'
					}
				}
			},
			mobile : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Mobile is required'
					}
				}
			},
			email : {
				row : '.col-sm-9',
				validators : {
					regexp : {
						regexp : '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
						message : 'The value is not a valid email address'
					},
					notEmpty : {
						message : 'Email is required'
					}
				}
			}
		}
	});

	$("#unit-frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			unit_name : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			},
			unit_symbl : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Symbol is required'
					}
				}
			},
			unit_nodp : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Decimal places is required'
					},
					stringLength:{
						max:1,
						message:"Please enter 1 digit only"
					}
				}
			}
		}
	});
	
	$("#stck_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			stck_name : {
				row : '.col-sm-4',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			}
		}
	});
	
	/**
	 * product form validation
	 */
	$("#prdct_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			/*productCode : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Code is required'
					}
				}
			},*/
			name : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			},
			unitId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Unit is required'
					}
				}
			},
			salesPrice : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Sales price is required'
					}
				}
			},
			purchasePrice : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Purchase price is required'
					}
				}
			},
			stockGroupId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Stock group is required'
					}
				}
			},
			/*openingQuantity : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Opening quantity is required'
					}
				}
			},
			openingBalance : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Opening balance is required'
					}
				}
			},*/
			weight : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Weight quantity is required'
					}
				}
			}
		}
	});
	
	
});