/**
 * js file for purchase
 * 
 * @since 30-July-2015
 */

/*
 * setTimeout(function() { $(".notification-panl").fadeOut(600); }, 3500);
 */
$('.double').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++) {
		a.push(i);
	}
	if (i = 46) {
		if($(this).val().indexOf('.') == -1)
		a.push(i);
	}
	if (!(a.indexOf(k) >= 0))
		e.preventDefault();
	//alert($(this).val().indexOf('.'));
});
$("#close-msg").click(function() {
	$(".notification-panl").fadeOut(600);
});

function closeMessage() {
	$(".notification-panl").fadeOut(600);
}

function cnfrmDelete(id) {
	$("#id-hid").val(id);
	$("#confirm-delete").css("display", "block");
}

deleteVendor = function(result) {
	$("#load-erp ").css("display", "block");
	if (result == 'true') {
		$.ajax({
			type : "GET",
			url : "deleteVendor.do",
			dataType : "html",
			cache : false,
			async : true,
			data : {
				"id" : $("#id-hid").val()
			},
			success : function(response) {
				if (response == 'ok') {
					vendorList();
					$(".notfctn-cntnnt").text(
							"Vendor Details Deleted Successfully.");
					$("#success-msg").css("display", "block");
				} else {
					$(".notfctn-cntnnt").text(response);
					$("#erro-msg ").css("display", "block");
				}
			},
			error : function(requestObject, error, errorThrown) {
				$(".notfctn-cntnnt").text("Oops! Something went wrong!");
				$("#erro-msg ").css("display", "block");
			}
		});
	}
	$("#confirm-delete").css("display", "none");
	$("#load-erp ").css("display", "none");
}

vendorList = function() {
	$.ajax({
		type : "GET",
		url : "vendorList.do",
		cache : false,
		async : true,
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to save vendor contact details
 */
$("#vndr-cntct-sve-btn").click(function() {
	var vo = new Object();
	vo.vendorId = $("#vendorId").val();
	vo.contactId = $("#contactId").val();
	vo.name = $("#vendor-name").val();
	vo.address = $("#vendor-address").val();
	vo.workPhone = $("#vendor-workPhone").val();
	vo.email = $("#vendor-email").val();

	if (vo.name != "") {
		$.ajax({
			type : "POST",
			url : "vendorContactSave.do",
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(vo),
			async : true,
			cache : false,
			success : function(response) {
				cleatVendorCotnctfrm();
				vendorContactList();
				if (response == "ok") {
				}
			},
			error : function(requestObject, error, errorThrown) {
				cleatVendorCotnctfrm();
				vendorContactList();
			}
		});

	} else {
		$("#vendor-name").css("border-color", "#A94442");
		$(".nme-requed").css("display", "block");
		$("#vendor-name").focus();
	}

});

/**
 * function to remove red marks
 */
$("#vendor-name").on("keyup", function() {
	$("#vendor-name").css("border-color", "#ccc");
	$(".nme-requed").css("display", "none");
	$("#vendor-name").css("    border", "1px solid #ccc");
});

/**
 * function to load vendor contact list
 */
vendorContactList = function() {
	$.ajax({
		type : "GET",
		url : "vendorContactList.do",
		cache : false,
		data : {
			"id" : $("#vendorId").val()
		},
		async : true,
		success : function(response) {
			$("#vndr_cntct_tbl").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to edit vendor contact
 * 
 * @param id
 */
editThisContact = function(id, name, email, address, phone) {
	$("#contactId").val(id);
	$("#vendor-name").val(name);
	$("#vendor-email").val(email);
	$("#vendor-address").val(address);
	$("#vendor-workPhone").val(phone);
}

/**
 * function to delte vendor contact
 * 
 * @param id
 */
deleteThisContact = function(id) {
	$.ajax({
		type : "GET",
		url : "deleteVendorContact.do",
		cache : false,
		data : {
			"id" : id
		},
		async : true,
		success : function(response) {
			vendorContactList();
			cleatVendorCotnctfrm();
		},
		error : function(requestObject, error, errorThrown) {
			vendorContactList();
			cleatVendorCotnctfrm();
		}
	});
}

/**
 * function to clear vendor contact form
 */
cleatVendorCotnctfrm = function() {
	$("#contactId").val("");
	$("#vendor-name").val("");
	$("#vendor-email").val("");
	$("#vendor-address").val("");
	$("#vendor-workPhone").val("");
}

/**
 * function to save unit
 */
saveUnit = function() {
	$('form#unit-frm').submit(
			function(event) {
				if ($("#unit-name").val().length > 0
						&& $("#unit-nodp").val().length > 0
						&& $("#unit-symbl").val().length > 0) {
					var vo = new Object();
					vo.id = $("#unitId").val();
					vo.name = $("#unit-name").val();
					vo.decimalPlaces = $("#unit-nodp").val();
					vo.symbol = $("#unit-symbl").val();
					$("#load-erp").css("display", "block");
					$("#unit-name").val("");
					$("#unit-nodp").val("");
					$("#unit-symbl").val("");
					$.ajax({
						type : "POST",
						url : "saveUnit.do",
						cache : false,
						async : true,
						contentType : "application/json; charset=utf-8",
						data : JSON.stringify(vo),
						success : function(response) {
							$("#notfctn-cntnnt").text(response);
							$("#successMsg").css("display", "block");
							clearFrmUnit();
							loadUnitTable();
						},
						error : function(requestObject, error, errorThrown) {
							alert(errorThrown);
						}
					});
				}
				event.preventDefault();
			});
}

/**
 * function to edit unit
 * 
 * @param id
 * @param name
 * @param symbol
 * @param decimalPlaces
 */
function editThisUnit(id, name, symbol, decimalPlaces) {
	$("#unitId").val(id);
	$("#unit-name").val(name);
	$("#unit-nodp").val(decimalPlaces);
	$("#unit-symbl").val(symbol);
}

/**
 * function to clear unit form
 */
clearFrmUnit = function() {
	$("#unitId").val('');
	$("#unit-name").val('');
	$("#unit-nodp").val('');
	$("#unit-symbl").val('');
}

/**
 * function to delete item
 * 
 * @param id
 */
function deleteThisUnit(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('unit');
	$("#id-hid").val(id);
}

/**
 * function to close confirm-delete pop
 */
function cancel() {
	$("#confirm-delete").css("display", "none");
	$("#id-hid").val('');
}

/**
 * function to confirm delete
 */
function deleteItem() {
	var item = $("#delete-item").val();
	if (item == 'unit')
		deleteUnit($("#id-hid").val());
	else if (item == 'group')
		deleteStockG($("#id-hid").val());
	else if (item == 'product')
		deleteProduct($("#id-hid").val());
	else if (item == 'bill')
		deleteBill($("#id-hid").val());
	else if (item == 'vq')
		deleteVQ($("#id-hid").val());
	else if (item == 'po')
		deletePO($("#id-hid").val());
	else if (item == 'pr')
		deletePR($("#id-hid").val());
	$("#confirm-delete").css("display", "none");
}

function deleteThisProduct(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('product');
	$("#id-hid").val(id);
}

/**
 * function to delete unit
 * 
 * @param id
 */
function deleteUnit(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "unitDelete.do",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			$("#notfctn-cntnnt").text(response);
			$("#successMsg").css("display", "block");
			loadUnitTable();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

/**
 * function to delete product
 * 
 * @param id
 */
function deleteProduct(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deleteProduct.do",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			if (response != 'ok') {
				$(".notfctn-cntnnt").text(response);
				$("#erro-msg").css("display", "block");
				$("#load-erp").css("display", "none");
			} else {
				$("#notfctn-cntnnt").text(response);
				$("#successMsg").css("display", "block");
				loadProductTable();
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

/**
 * function to load product list
 */
function loadProductTable() {
	$.ajax({
		type : "GET",
		url : 'productList.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#product_tbl_div").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}

function loadUnitTable() {
	$.ajax({
		type : "GET",
		url : 'listUnits.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#unit-table-div").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}

saveStockGroup = function() {
	$('form#stck_frm')
			.submit(
					function(event) {

						if ($("#stckgp_name").val().length > 0) {
							var vo = new Object();
							vo.id = $("#stckId").val();
							vo.parentGroupId = $("#parent_stckgp").val();
							vo.groupName = $("#stckgp_name").val();
							$("#load-erp").css("display", "block");
							$("#stckgp_name").val("");
							$
									.ajax({
										type : "POST",
										url : "saveStockGroup.do",
										cache : false,
										async : true,
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(vo),
										success : function(response) {
											$("#parent_stckgp option").remove();
											var option = "<option value='' label='--Select Parent Group--'>";
											$
													.each(
															response,
															function(i, item) {
																option += "<option value='"
																		+ item.id
																		+ "' label ='"
																		+ item.groupName
																		+ "'>";
															});

											$("#parent_stckgp").html(option);
											$("#notfctn-cntnnt")
													.text(
															"Stock Group saved Successfully");
											$("#successMsg").css("display",
													"block");
											clearFromStocks();
											loadStockGroupsTable();
										},
										error : function(requestObject, error,
												errorThrown) {
											alert(errorThrown);
										}
									});

						}
						event.preventDefault();
					});
}

clearFromStocks = function() {
	$("#stckId").val("");
	$("#parent_stckgp").val("");
	$("#stckgp_name").val("");

	$("#parent_stckgp option").each(function(i, item) {
		if (this.disabled) {
			this.disabled = false;
		}
	});
}

function deleteStockgroup(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('group');
	$("#id-hid").val(id);

}

function deleteStockG(id) {

	$("#load-erp").css("display", "block");
	$
			.ajax({
				type : "GET",
				url : "deleteStockGroup.do",
				cache : false,
				async : true,
				data : {
					"id" : id
				},
				success : function(response) {
					if (response.length != 0) {
						$("#parent_stckgp option").remove();
						var option = "<option value='' label='--Select Parent Group--'>";
						$.each(response, function(i, item) {
							option += "<option value='" + item.id
									+ "' label ='" + item.groupName + "'>";
						});

						$("#parent_stckgp").html(option);
						// $("#notfctn-cntnnt").text(response);
						// $("#successMsg").css("display", "block");
						$("#load-erp").css("display", "none");
						loadStockGroupsTable();
					} else {
						$("#load-erp").css("display", "none");
						$("#notfctn-cntnnt").text(
								"Could Not Delete Stock Group");
						$("#successMsg").css("display", "block");
					}
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

function editStockGroupValues(id, parentGroup, name, subGroups) {
	$("#stckId").val(id);
	$("#parent_stckgp").val(parentGroup);
	$("#stckgp_name").val(name);
	$("#parent_stckgp option").each(function(i, item) {
		if (this.disabled) {
			this.disabled = false;
		}

	});
	$("#parent_stckgp option[Value = " + id + "] ")
			.attr("disabled", "disabled");
	var childGps = [];
	childGps = subGroups.replace(/[[\]]/g, '').split(',');

	$.each(childGps, function(k, item) {
		$("#parent_stckgp option[Value = " + item + "] ").attr("disabled",
				"disabled");
	});

}

function loadStockGroupsTable() {
	$.ajax({
		type : "GET",
		url : 'listStockGroups.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#stck_dv").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}

/**
 * Add bill Page
 */

getProductDetails = function() {
	var ids = [];
	var idsAndQuantities = [];
	$(".prodctId").each(function() {
		ids.push(this.value);
	});

	$(".prdct_tbl tr").each(function() {
		var code = this.id;
		if (code != "") {
			var id = $("#itemId" + code).val();
			var qty = $("#product_qty" + code).val();
			idsAndQuantities.push(id + "!!!" + qty);
		}
	});
	var vo = new Object();
	vo.ids = ids;
	vo.idsAndQuantities = idsAndQuantities;
	vo.isSales = false;
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		dataType : "html",
		cache : false,
		data : JSON.stringify(vo),
		async : true,
		url : "productPopUp.do",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});

}

closePdtpo = function() {
	$("#load-erp").css("display", "block");
	var ids = [];
	var idsAndQuantities = [];
	$("input[name=product]").each(function() {
		if (this.checked){
			idsAndQuantities.push(this.value);
		}
	});
	var vo = new Object();
	vo.idsAndQuantities = idsAndQuantities;
	vo.isSales = false;
	$.ajax({
		type : 'POST',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		data : JSON.stringify(vo),
		url : "findProducts.do",
		success : function(response) {
			$(".prdct_tbl tr").remove();
			$(".prdct_tbl").append(response);
			$(".prdct_tbl tr").each(function() {
				var id = this.id;
				if(id != ""){
					$("#product_qty"+id).blur();
				}
			});

			$("#load-erp").css("display", "none");

		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
	$("#pdtPop").fadeOut("600");
}

cancelPdtpo = function() {
	$("#pdtPop").fadeOut("600");
}

function deleteProductRow(link) {
	var td = link.parentNode.parentNode;
	var row = td.parentNode;
	row.removeChild(td);
	findTotalAmounts();
}

/**
 * Function to get currencies for bill according to purchase date
 */
$(".billDate")
		.on(
				"change",
				function() {
					var date = $("#bill-date").val();
					if (date.length != 0) {
						$
								.ajax({
									type : 'GET',
									url : "getCurrenciesForBill.do",
									data : {
										"date" : date
									},
									cache : false,
									async : true,
									dataType : "json",
									contentType : "application/json;charset=utf-8",
									success : function(response) {
										var options = "<option value=''>--Select Currency--</option>";
										$.each(response, function(i, item) {
											options += '<option value="'
													+ item.erpCurrencyId + '">'
													+ item.currency
													+ '</option>';
										});
										$("#currencySel option").remove();
										$("#currencySel").html(options);

									},
									error : function(requestObject, error,
											errorThrown) {
										alert(errorThrown);
									}

								});
					}
				});
/**
 * function to save bill
 */
function saveBill() {

	$("#load-erp").css("display", "block");

	var msg = '';
	var flag = true;
	var proTax = $("#taxAmountHidden").val();
	var totTax = $("#taxTotal").text();
	var taxes = $("#taxesMulti").val();
	if (proTax != '0.00' && taxes != null) {
		msg = "Please Select Any one tax...";
		flag = false;
	} else if ($("#bill_vendr").val().length == 0) {
		msg = "Please Select Vendor...";
		flag = false;
	} else if ($("#bill_ledgr").val().length == 0) {
		msg = "Please Select Account...";
		flag = false;
	} else if ($("#paymentTermSelect").val().length == 0) {
		msg = "Please Select Payment Term...";
		flag = false;
	}

	var tax = parseFloat(proTax) + parseFloat(totTax);
	var vo = new Object();
	var productVos = [];
	var chargeVos = [];
	var installmentVos = [];

	vo.receivingNoteIds = $("#receivingNoteIds").val().replace(/[[\]]/g, '')
			.split(',');
	vo.billId = $("#billId").val();
	vo.vendorId = $("#bill_vendr").val();
	vo.billDate = $("#bill-date").val();
	vo.ledgerId = $("#bill_ledgr").val();
	vo.isSettled = $("#bill_sts").val();
	vo.paymentTermId = $("#paymentTermSelect").val();
	vo.deliveryTermId = $("#deliveryTermSelect").val();
	vo.erpCurrencyId = $("#currencySelBill").val();

	var taxIds = [];
	if (taxes != null) {
		$.each(taxes, function(i, it) {
			var rate = it.split("!!!");
			taxIds.push(rate[0]);
		});
	}
	vo.tsxIds = taxIds;
	vo.subTotal = $("#subTotal").text();
	vo.discountTotal = $("#disTotal").text();
	vo.netTotal = $("#netTotal").text();
	vo.taxTotal = $("#taxTotal").text();
	vo.grandTotal = $("#grandTotal").text();
	vo.discountRate = $("#discRate").val();
	vo.netAmountHidden = $("#netAmountHidden").val();
	vo.productTaxTotal = tax.toFixed(2);
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.unitPrice = $("#product_prc" + id).val();
			product.discount = $("#product_disc" + id).val();
			product.netAmount = $("#product_net" + id).text();
			product.billProductId = $("#itemId" + id).val();
			product.godownId = $("#godownId" + id).val();
			product.amountExcludingTax = $("#prod_tot" + id).text();
			var tax = $("#product_tx" + id).val().split("!!!");
			product.taxId = tax[0];
			productVos.push(product);
		}
	});
	if (productVos.length == 0) {
		msg = "Please Select Product...";
		flag = false;
	}
	vo.productVos = productVos;

	/**
	 * iterating charges
	 */
	$(".chrg_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var charge = new Object();
			charge.chargeId = id;
			charge.amount = $("#chrge_price" + id).val();
			chargebillChargeId = $("#billChargeId" + id).val();
			chargeVos.push(charge);
		}
	});
	vo.chargeVos = chargeVos;

	/**
	 * setting installments
	 */
	$(".instllmnt_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var installment = new Object();
			installment.date = $("#instlmnt_date" + id).text();
			installment.percentage = $("#instlmnt_perc" + id).text();
			installment.amount = $("#instlmnt_amount" + id).text();
			installment.isPaid = $("#instlmnt_stts" + id).val();
			installment.installmentId = $("#" + id).val()
			installmentVos.push(installment);
		}
	});
	vo.installmentVos = installmentVos;
	var json = JSON.stringify(vo);
	if (flag) {
		$.ajax({
			type : "POST",
			url : "saveBill.do",
			cache : false,
			async : true,
			data : json,
			dataType : "json",
			contentType : "application/json;charset=utf-8",
			success : function(response) {
				if (response == 0) {
					$("#errror-msg").css("display", "block");
					$(".modal-body").text("Oops...! Something went wrong.");
					$("#load-erp").css("display", "none");
				} else {
					var currentUrl = location.href;
					pageurl = currentUrl
							.replace('convertReceivingNoteToBill.do',
									"viewBill.do?id=" + response
											+ "&msg=Bill Saved Successfully");
					window.history.pushState({
						path : pageurl
					}, '', pageurl);
					location.reload();
				}
				$("#load-erp").css("display", "none");
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}

$(".double").on("blur", function() {
	if (this.value == "")
		this.value = "0.00";
});

function findNetAmount(id) {
	var quantity = $("#product_qty" + id).val();
	var returnedQuantity = $("#product_qty_hid" + id).val();
	var receiveddQuantity = $("#recvdQty" + id).val();
	var flag = true;
	if (parseFloat(quantity) < parseFloat(returnedQuantity)) {
		flag = false;
		$("#product_qty" + id).val(returnedQuantity);
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(
				returnedQuantity + " Items Has Been Returned for " + id);
		findNetAmount(id);
	}if (parseFloat(quantity) < parseFloat(receiveddQuantity)) {
		flag = false;
		$("#product_qty" + id).val(receiveddQuantity);
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(
				receiveddQuantity + " Items Has Been Received for " + id);
		findNetAmount(id);
	}
	if (flag) {
		var price = $("#product_prc" + id).val();
		var disc = $("#product_disc" + id).val();
		var tax = $("#product_tx" + id).val();
		if (tax != "") {
			var taxRate = tax.split('!!!');
			tax = taxRate[1];
		} else {
			tax = "0.00";
		}
		var net = parseFloat(quantity) * parseFloat(price);
		var discAmt = parseFloat(net) * parseFloat(disc) / 100;
		net = parseFloat(net) - parseFloat(discAmt);
		$("#prod_tot" + id).text(net.toFixed(2));
		var taxAmt = parseFloat(net) * parseFloat(tax) / 100;
		net = parseFloat(net) + parseFloat(taxAmt);
		$("#product_net" + id).text(net.toFixed(2));

		findTotalAmounts();
	}
}

/**
 * function to display charge pop up
 */
getChargeDetails = function() {
	var ids = [];
	$(".chargeId").each(function() {
		ids.push(this.value);
	});
	var idsAndAmount=[];
	$(".chrg_tbl tr").each(function() {
		var code = this.id;
		if (code != "") {
			var id = code;
			var amount = $("#chrge_price" + code).val();
			idsAndAmount.push(id + "!!!" + amount);
		}
	});
	var vo = new Object();
	vo.idsAndAmt = idsAndAmount;
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		dataType : "html",
		cache : false,
		data : JSON.stringify(vo),
		async : true,
		url : "chargePopUp.do",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to select charges
 */
function selectCharges() {
	$("#load-erp").css("display", "block");
	var ids = [];
	$("input[name=charge]").each(function() {
		if (this.checked)
			ids.push(this.value);
	});
	var vo = new Object();
	vo.idsAndAmt = ids;
	$.ajax({
		type : 'POST',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		data : JSON.stringify(vo),
		url : "findCharges.do",
		success : function(response) {
			$(".chrg_tbl tr").remove();
			$(".chrg_tbl").append(response);
			$("#load-erp").css("display", "none");
			findTotalAmounts();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
	$("#pdtPop").fadeOut("600");
}

/**
 * function to close charge pop up
 */
function cancelChargePOp() {
	$("#pdtPop").fadeOut("600");
}

/**
 * function to delete charge row
 * 
 * @param link
 */
function deleteChargeRow(link) {
	var td = link.parentNode.parentNode;
	var row = td.parentNode;
	row.removeChild(td);
	findTotalAmounts();
}

$('.chrgTbl tr').click(function(event) {
	if (event.target.type !== 'checkbox') {
		$(':checkbox', this).trigger('click');
	}
});

$('.proTable tr').click(function(event) {
	if (event.target.type !== 'checkbox') {
		$(':checkbox', this).trigger('click');
	}
});

/**
 * function to calculate amount
 */
function findTotalAmounts() {
	var net = 0;
	var discount = 0;
	var taxAmount = 0;
	var chargeTotal = 0;
	var netAmount = 0;
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var quantity = $("#product_qty" + id).val();
			var price = $("#product_prc" + id).val();
			var disc = $("#product_disc" + id).val();
			var tax = $("#product_tx" + id).val();

			if (tax != "") {
				var taxRate = tax.split('!!!');
				tax = taxRate[1];
			} else {
				tax = "0.00";
			}
			var thisNet = parseFloat(quantity) * parseFloat(price);
			var discAmt = parseFloat(thisNet) * parseFloat(disc) / 100;
			thisNet = parseFloat(thisNet) - parseFloat(discAmt);
			netAmount = parseFloat(netAmount) + parseFloat(thisNet);
			// $("#prod_tot"+id).text(thisNet.toFixed(2));
			var taxAmt = parseFloat(thisNet) * parseFloat(tax) / 100;
			thisNet = parseFloat(thisNet) + parseFloat(taxAmt);
			net = parseFloat(net) + parseFloat(thisNet);
			discount = parseFloat(discount) + parseFloat(discAmt);
			taxAmount = parseFloat(taxAmount) + parseFloat(taxAmt);
		}
	});

	$(".chrg_tbl tr").each(
			function() {
				var id = this.id;
				if (id != '') {

					chargeTotal = parseFloat(chargeTotal)
							+ parseFloat($("#chrge_price" + id).val());
				}
			});

	net = parseFloat(net) + parseFloat(chargeTotal);
	netAmount = parseFloat(netAmount) + parseFloat(chargeTotal);
	$("#netAmountHidden").val(netAmount.toFixed(2));
	$("#subTotal").text(net.toFixed(2));
	$("#disTotal").text(discount.toFixed(2));
	$("#taxAmountHidden").val(taxAmount.toFixed(2));
	$("#netTotal").text(net.toFixed(2));
	// var grand = parseFloat(net) + parseFloat(taxAmount);
	// $("#grandTotal").text(grand.toFixed(2));
	selectedTaxes();
	getInstallmentsForBill();
}
/**
 * function to get installment table for bill
 */
function getInstallmentsForBill() {
	var id = $("#paymentTermSelect").val();
	if (id != '') {
		$("#spinr").addClass("fa-spin");
		$.ajax({
			type : "GET",
			url : "getInstallmentsForBill.do",
			cache : false,
			async : true,
			data : {
				"date" : $("#bill-date").val(),
				"amount" : $('#grandTotal').text(),
				"paymentTermId" : id
			},
			dataType : "html",
			contentType : "application/json;charset=utf-8",
			success : function(response) {
				$("#installment_table_bill").html(response);
				findNetAmount();
				$("#spinr").removeClass("fa-spin");
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#spinr").removeClass("fa-spin");
			}
		});
	}
}

function selectedTaxes() {
	var subTotal = $("#subTotal").text();
	var discountRate = $("#discRate").val();
	if (discountRate.length == 0)
		discountRate = 0;
	var discAmount = parseFloat(subTotal) * parseFloat(discountRate) / 100;
	$("#disTotal").text(discAmount.toFixed(2));
	var net = parseFloat(subTotal) - parseFloat(discAmount);
	net = net.toFixed(2);
	$("#netTotal").text(net);
	var taxes = $("#taxesMulti").val();
	var totalTax = 0;
	if (taxes != null) {
		$.each(taxes, function(i, it) {
			var rate = it.split("!!!");
			totalTax = parseFloat(totalTax) + parseFloat(rate[1]);
		});
	}
	totalTax = totalTax.toFixed(2);
	var taxAmount = 0;
	if (totalTax != 0) {
		taxAmount = parseFloat(net) * parseFloat(totalTax) / 100;
		taxAmount = taxAmount.toFixed(2);
		$("#taxTotal").text(taxAmount);
		$("#grandTotal").text(parseFloat(net) + parseFloat(taxAmount));
	} else {
		$("#taxTotal").text(0.00);
		$("#grandTotal").text(net);
	}
}

function deleteThisBill(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('bill');
	$("#id-hid").val(id);

}
function deleteBill(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deletebill.do",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			$("#success-msg").css("display", "block");
			$(".notification-panl").css("display", "block");
			$(".notfctn-cntnnt").text(response);
			loadBillTable();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

loadBillTable = function() {
	$.ajax({
		type : "GET",
		url : 'billList.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#bill_panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}

/**
 * function to pay bill installment
 * 
 * @param value
 * @param id
 */
payBillInstallment = function(value, id) {
	if (value == "true") {
		$("#id-hid").val(id);
		$("#confirm-delete").css("display", "block");
	}
}

/**
 * function to pay bill installment
 */
confirmedBillPayment = function() {
	$("#load-erp").css("display", "block");
	var id = $("#id-hid").val();
	$("#confirm-delete").css("display", "none");
	$("#id-hid").val('');

	$.ajax({
		type : "GET",
		url : "payBillInstallment.do",
		cache : false,
		async : true,
		contentType : "application/json;charset=utf-8",
		dataType : "json",
		data : {
			"id" : id
		},
		success : function(response) {
			var msg = "Something Went Wrong.";
			if (response)
				msg = "Installment Paid Successfully.";
			setTimeout(function() {
				var currentUrl = location.href;
				var splitted = currentUrl.split("&msg");
				currentUrl = splitted[0] + "&msg=" + msg;
				window.history.pushState({
					path : currentUrl
				}, '', currentUrl);
				location.reload();
			}, 1000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");

		}
	});

}

/**
 * function to save vendor quote
 */
function saveVendorQuote() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	var proTax = $("#taxAmountHidden").val();
	var totTax = $("#taxTotal").text();
	var taxes = $("#taxesMulti").val();
	if (proTax != '0.00' && taxes != null) {
		msg = "Please Select Any one tax...";
		flag = false;
	} else if ($("#bill_vendr").val().length == 0) {
		msg = "Please Select Vendor...";
		flag = false;
	} else if ($("#vqte_expiry").val().length == 0) {
		msg = "Please Select Expiry Date...";
		flag = false;
	}

	var tax = parseFloat(proTax) + parseFloat(totTax);
	var vo = new Object();
	var productVos = [];
	vo.quoteId = $("#vendorQuoteId").val();
	vo.expiryDate = $("#vqte_expiry").val();
	vo.vendorId = $("#bill_vendr").val();
	vo.quoteDate = $("#bill-date").val();
	vo.paymentTermId = $("#paymentTermSelect").val();
	vo.deliveryTermId = $("#deliveryTermSelect").val();
	vo.erpCurrencyId = $("#currencySelBill").val();

	var taxIds = [];
	if (taxes != null) {
		$.each(taxes, function(i, it) {
			var rate = it.split("!!!");
			taxIds.push(rate[0]);
		});
	}
	vo.tsxIds = taxIds;
	vo.subTotal = $("#subTotal").text();
	vo.discountTotal = $("#disTotal").text();
	vo.netTotal = $("#netTotal").text();
	vo.taxTotal = $("#taxTotal").text();
	vo.grandTotal = $("#grandTotal").text();
	vo.discountRate = $("#discRate").val();
	vo.netAmountHidden = $("#netAmountHidden").val();
	vo.productTaxTotal = tax.toFixed(2);
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.unitPrice = $("#product_prc" + id).val();
			product.discount = $("#product_disc" + id).val();
			product.netAmount = $("#product_net" + id).text();
			product.quoteProductId = $("#itemId" + id).val();
			product.amountExcludingTax = $("#prod_tot" + id).text();
			var tax = $("#product_tx" + id).val().split("!!!");
			product.taxId = tax[0];
			productVos.push(product);
		}
	});
	
	
	if (productVos.length == 0) {
		msg = "Please Select Products...";
		flag = false;
	}
	vo.productVos = productVos;
	var json = JSON.stringify(vo);
	console.log(json);
	if (flag) {
		$
				.ajax({
					type : "POST",
					url : "saveVendorQoutes.do",
					cache : false,
					async : true,
					data : json,
					dataType : "json",
					contentType : "application/json;charset=utf-8",
					success : function(response) {
						if (response == 0) {
							$("#errror-msg").css("display", "block");
							$(".modal-body").text(
									"Oops...! Something went wrong.");
							$("#load-erp").css("display", "none");
						} else {
							var currentUrl = location.href;
							pageurl = currentUrl
									.replace(
											'addVendorQuote.do',
											"viewVendorQuote.do?id="
													+ response
													+ "&msg=Vender Quotation Saved Successfully");
							window.history.pushState({
								path : pageurl
							}, '', pageurl);
							location.reload();
						}
						$("#load-erp").css("display", "none");
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
						$("#load-erp").css("display", "none");
					}
				});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}

/**
 * function to save purchase order
 */
function savePurchaseOrder() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	var proTax = $("#taxAmountHidden").val();
	var totTax = $("#taxTotal").text();
	var taxes = $("#taxesMulti").val();
	if (proTax != '0.00' && taxes != null) {
		msg = "Please Select Any one tax...";
		flag = false;
	} else if ($("#bill_vendr").val().length == 0) {
		msg = "Please Select Vendor...";
		flag = false;
	}

	var tax = parseFloat(proTax) + parseFloat(totTax);
	var vo = new Object();
	var productVos = [];
	var chargeVos=[];
	vo.quoteId = $("#purchaseOrderId").val();
	vo.vendorId = $("#bill_vendr").val();
	vo.quoteDate = $("#bill-date").val();
	vo.paymentTermId = $("#paymentTermSelect").val();
	vo.deliveryTermId = $("#deliveryTermSelect").val();
	vo.erpCurrencyId = $("#currencySelBill").val();

	var taxIds = [];
	if (taxes != null) {
		$.each(taxes, function(i, it) {
			var rate = it.split("!!!");
			taxIds.push(rate[0]);
		});
	}
	vo.tsxIds = taxIds;
	vo.subTotal = $("#subTotal").text();
	vo.discountTotal = $("#disTotal").text();
	vo.netTotal = $("#netTotal").text();
	vo.taxTotal = $("#taxTotal").text();
	vo.grandTotal = $("#grandTotal").text();
	vo.discountRate = $("#discRate").val();
	vo.netAmountHidden = $("#netAmountHidden").val();
	vo.productTaxTotal = tax.toFixed(2);
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.unitPrice = $("#product_prc" + id).val();
			product.discount = $("#product_disc" + id).val();
			product.netAmount = $("#product_net" + id).text();
			product.quoteProductId = $("#itemId" + id).val();
			product.amountExcludingTax = $("#prod_tot" + id).text();
			var tax = $("#product_tx" + id).val().split("!!!");
			product.taxId = tax[0];
			productVos.push(product);
		}
	});
	$(".chrg_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var charge = new Object();
			charge.chargeId = id;
			charge.amount = $("#chrge_price" + id).val();
			chargebillChargeId = $("#billChargeId" + id).val();
			chargeVos.push(charge);
		}
	});
	vo.chargeVos = chargeVos;
	
	if (productVos.length == 0) {
		msg = "Please Select Products...";
		flag = false;
	}
	vo.productVos = productVos;
	var json = JSON.stringify(vo);
	if (flag) {
		$
				.ajax({
					type : "POST",
					url : "savePurchaseOrder.do",
					cache : false,
					async : true,
					data : json,
					dataType : "json",
					contentType : "application/json;charset=utf-8",
					success : function(response) {
						if (response == 0) {
							$("#errror-msg").css("display", "block");
							$(".modal-body").text(
									"Oops...! Something went wrong.");
							$("#load-erp").css("display", "none");
						} else {
							var currentUrl = location.href;
							pageurl = currentUrl
									.replace(
											'addPurchaseOrder.do',
											"viewPurchaseOrder.do?id="
													+ response
													+ "&msg=Purchase Order Saved Successfully");
							window.history.pushState({
								path : pageurl
							}, '', pageurl);
							location.reload();
						}
						$("#load-erp").css("display", "none");
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
						$("#load-erp").css("display", "none");
					}
				});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}

/**
 * function to convert vendor quote to order
 * 
 * @param id
 */
function convertVendorQuote(id) {
	$("#confirm_convert").css("display", "block");
	$("#id-hidd").val(id);
}

function confirmedConversion() {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "convertVendorQouteToOrder.do",
		dataType : "html",
		cache : false,
		async : true,
		data : {
			"id" : $("#id-hidd").val()
		},
		success : function(response) {
			$("#panelBody").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
	cancelConfirm();
}

function cancelConfirm() {
	$("#confirm_convert").css("display", "none");
	$("#id-hidd").val("");
}

/**
 * function to delete vendor quote
 * 
 * @param id
 */
function deleteVendorQuote(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('vq');
	$("#id-hid").val(id);

}
function deleteVQ(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deleteVendorQuote.do",
		cache : false,
		async : true,
		dataType : "html",
		data : {
			"id" : id
		},
		success : function(response) {
			$("#panelBody").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
}

/**
 * function to delete purchase order
 * 
 * @param id
 */
function deletePurchaseOrder(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('po');
	$("#id-hid").val(id);

}
function deletePO(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deletePurchaseOrder.do",
		cache : false,
		async : true,
		dataType : "html",
		data : {
			"id" : id
		},
		success : function(response) {
			$("#panelBody").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
}

function filterBills(vendorId) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "getBillsByVendor.do",
		data : {
			"vendorId" : vendorId
		},
		cache : false,
		async : true,
		dataType : "json",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			var data = response.result;
			if (response.status == 'ok') {
				var option = "<option value=''>-Select Bill-</option>";
				$.each(data, function(i, item) {
					option += "<option value='" + item.billId + "'>"
							+ item.billCode + "</option>";
				});
				$("#pr_bl option").remove();
				$("#pr_bl").html(option);
			}
		},
		error : function(reqObj, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function findNetAmountPR(id) {
	var quantity = $("#product_qty" + id).val();
	var quantityToReturn = $("#product_qty_hid" + id).val();
	var flag = true;
	if (parseFloat(quantity) > parseFloat(quantityToReturn)) {
		flag = false;
		$("#product_qty" + id).val(quantityToReturn);
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(
				quantityToReturn + " Items can be Returned for " + id);
		findNetAmountPR(id);
	}
	if (flag) {
		var price = $("#product_prc" + id).val();
		var net = parseFloat(quantity) * parseFloat(price);
		$("#product_net" + id).text(net.toFixed(2));
	}
}

function savePurchaseReturn() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	var vo = new Object();
	var productVos = [];
	vo.billId = $("#billId").val();
	vo.date = $("#bill-date").val();
	vo.returnId = $("#returnId").val();
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.unitPrice = $("#product_prc" + id).val();
			product.total = $("#product_net" + id).text();
			product.godownId = $("#prod_godwn_id" + id).val();
			productVos.push(product);
		}
	});
	if (productVos.length == 0) {
		msg = "Please Select Product...";
		flag = false;
	}
	vo.productVos = productVos;

	var json = JSON.stringify(vo);
	if (flag) {
		$
				.ajax({
					type : "POST",
					url : "savePurchaseReturn.do",
					cache : false,
					async : true,
					data : json,
					dataType : "json",
					contentType : "application/json;charset=utf-8",
					success : function(response) {
						if (response == 0) {
							$("#errror-msg").css("display", "block");
							$(".modal-body").text(
									"Oops...! Something went wrong.");
							$("#load-erp").css("display", "none");
						} else {
							var currentUrl = location.href;
							pageurl = currentUrl
									.replace(
											'returnThisBill.do',
											"viewPurchaseReturn.do?id="
													+ response
													+ "&msg=Purchase Return Saved Successfully");
							window.history.pushState({
								path : pageurl
							}, '', pageurl);
							location.reload();
						}
						$("#load-erp").css("display", "none");
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
						$("#load-erp").css("display", "none");
					}
				});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}

function deletePurchaseReturn(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('pr');
	$("#id-hid").val(id);

}
function deletePR(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deletePurchaseReturn.do",
		cache : false,
		async : true,
		dataType : "html",
		data : {
			"id" : id
		},
		success : function(response) {
			$("#panelBody").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
}
addQtyToId = function(id, value) {
	var oldVal = $("." + id).val();
	var split = oldVal.split("!!!");
	$("." + id).val(split[0] + "!!!" + value);
}