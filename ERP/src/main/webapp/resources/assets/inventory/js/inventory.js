/**
 * js file for inventory
 * 
 * @since 30-July-2015
 */
$("#close-msg").click(function() {
	$(".notification-panl").fadeOut(600);
});
$('.double').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++) {
		a.push(i);
	}
	if (i = 46) {
		if ($(this).val().indexOf('.') == -1)
			a.push(i);
	}
	if (!(a.indexOf(k) >= 0))
		e.preventDefault();
	// alert($(this).val().indexOf('.'));
});
function closeMessage() {
	$(".notification-panl").fadeOut(600);
}

$(".double").on("blur", function() {
	if (this.value == '')
		this.value = "0.00";
});

$(".double").on("focus", function() {
	this.select();
});

/**
 * function to save godown
 */
function savegoDown() {
	var vo = new Object();
	vo.godownId = $("#godown_id").val();
	vo.name = $("#godown_name").val();
	clearFrmGodown();
	if (vo.name != '') {
		$("#load-erp").css("display", "block");
		$
				.ajax({
					type : "POST",
					cache : false,
					dataType : "json",
					contentType : "application/json;charset=utf-8",
					data : JSON.stringify(vo),
					url : "saveGoDown.do",
					success : function(response) {
						var data = response.result;
						var items = "";
						if (data != null) {
							$
									.each(
											data,
											function(i, item) {
												items += '<label class="badge"><span onclick="editGoDown('
														+ item.godownId
														+ ',\''
														+ item.name
														+ '\')">'
														+ item.name
														+ ' </span>&nbsp;&nbsp;<span><i onclick="deleteGoDown('
														+ item.godownId
														+ ')" class="fa fa-times"></i></span> </label>';
											});
							$("#avl_gdwns").html(items);
						}
						$(".successMsg").css("display", "block");
						$(".notfctn-cntnnt").text(response.status);
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
					},
					complete : function() {
						$("#load-erp").css("display", "none");
					}
				});
	} else {
		$("#godown_name").focus();
	}
}

/**
 * function to clear godown form
 */
function clearFrmGodown() {
	$("#godown_id").val('');
	$("#godown_name").val('');
}

/**
 * function to edit godown
 * 
 * @param id
 * @param name
 */
function editGoDown(id, name) {
	$("#godown_id").val(id);
	$("#godown_name").val(name);
}

/**
 * function to delete godown
 * 
 * @param id
 */
function deleteGoDown(id) {
	clearFrmGodown();
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
	$("#delete-item").val("godown");
}

function cancel() {
	$("#id-hid").val('');
	$("#delete-item").val("");
	$("#confirm-delete").css("display", "none");
}

function deleteItem() {
	var item = $("#delete-item").val();
	var id = $("#id-hid").val();
	cancel();
	if (item == 'godown')
		deleteGodown(id);
	if (item == 'ie')
		deleteIE(id);
	if (item == 'iw')
		deleteIW(id);
	if (item == 'rn')
		deleteThisReceivingNote(id);
	if (item == 'dn')
		deleteThisDeliveryNOte(id);
	if (item == 'packingKind')
		deletePackingKind(id);
	if (item == 'pl')
		deleteThisPackingList(id);
}

function deleteGodown(id) {
	$("#load-erp").css("display", "block");
	$
			.ajax({
				type : "GET",
				cache : false,
				dataType : "json",
				contentType : "application/json;charset=utf-8",
				data : {
					"id" : id
				},
				url : "deleteGoDown.do",
				success : function(response) {
					var data = response.result;
					var items = "";
					if (data != null) {
						$
								.each(
										data,
										function(i, item) {
											items += '<label class="badge"><span onclick="editGoDown('
													+ item.godownId
													+ ',\''
													+ item.name
													+ '\')">'
													+ item.name
													+ ' </span>&nbsp;&nbsp;<span><i onclick="deleteGoDown('
													+ item.godownId
													+ ')" class="fa fa-times"></i></span> </label>';
										});
						$("#avl_gdwns").html(items);
					}
					$(".successMsg").css("display", "block");
					$(".notfctn-cntnnt").text(response.status);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				},
				complete : function() {
					$("#load-erp").css("display", "none");
				}
			});
}

getProductDetails = function() {
	var ids = [];
	var idsAndQuantities = [];
	$(".prodctId").each(function() {
		ids.push(this.value);
	});

	$(".prdct_tbl tr").each(function() {
		var code = this.id;
		if (code != "") {
			var id = $("#itemId" + code).val();
			var qty = $("#product_qty" + code).val();
			idsAndQuantities.push(id + "!!!" + qty);
		}
	});
	var vo = new Object();
	vo.ids = ids;
	vo.idsAndQuantities = idsAndQuantities;
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		dataType : "html",
		cache : false,
		data : JSON.stringify(vo),
		async : true,
		url : "inventoryProductPop.do",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

closePdtpo = function() {
	$("#load-erp").css("display", "block");
	var ids = [];
	var idsAndQuantities = [];
	$("input[name=product]").each(function() {
		if (this.checked) {
			idsAndQuantities.push(this.value);
		}
	});
	var vo = new Object();
	vo.idsAndQuantities = idsAndQuantities;
	$.ajax({
		type : 'POST',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		data : JSON.stringify(vo),
		url : "inventoryProductRow.do",
		success : function(response) {
			$(".prdct_tbl tr").remove();
			$(".prdct_tbl").append(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
	$("#pdtPop").fadeOut("600");
}

getProductDetailsForIE = function() {
	var ids = [];
	var idsAndQuantities = [];
	$(".prodctId").each(function() {
		ids.push(this.value);
	});
	$(".prdct_tbl tr").each(function() {
		var code = this.id;
		if (code != "") {
			var id = $("#itemId" + code).val();
			var qty = $("#product_qty" + code).val();
			idsAndQuantities.push(id + "!!!" + qty);
		}
	});
	var vo = new Object();
	vo.ids = ids;
	vo.idsAndQuantities = idsAndQuantities;
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		dataType : "html",
		cache : false,
		data : JSON.stringify(vo),
		async : true,
		url : "invntr_entryProductPop.do",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

closePdtpoForIE = function() {
	$("#load-erp").css("display", "block");
	var ids = [];
	var idsAndQuantities = [];
	$("input[name=product]").each(function() {
		if (this.checked)
			idsAndQuantities.push(this.value);
	});
	var vo = new Object();
	vo.idsAndQuantities = idsAndQuantities;
	$.ajax({
		type : 'POST',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		data : JSON.stringify(vo),
		url : "invntr_entryProductRow.do",
		success : function(response) {
			$(".prdct_tbl tr").remove();
			$(".prdct_tbl").append(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
	$("#pdtPop").fadeOut("600");
}

cancelPdtpo = function() {
	$("#pdtPop").fadeOut("600");
}

function deleteProductRow(link) {
	var td = link.parentNode.parentNode;
	var row = td.parentNode;
	row.removeChild(td);
	try {
		findTotalAmounts();
	} catch (e) {
	}
	try {
		findTotalWeights();
	} catch (e) {
	}
}

$('.proTable tr').click(function(event) {
	if (event.target.type !== 'checkbox') {
		$(':checkbox', this).trigger('click');
	}
});

$('.stck-tbl tr').click(function(e) {
	var $link = $(this).find("a");
	if (e.target === $link[0])
		return false;
	$link.trigger('click');
	return false;
});

function findNetAmount(id) {
	var quantity = $("#product_qty" + id).val();
	var price = $("#product_prc" + id).val();
	var disc = $("#product_disc" + id).val();
	var tax = $("#product_tx" + id).val();
	if (tax != "") {
		var taxRate = tax.split('!!!');
		tax = taxRate[1];
	} else {
		tax = "0.00";
	}
	var net = parseFloat(quantity) * parseFloat(price);
	var discAmt = parseFloat(net) * parseFloat(disc) / 100;
	net = parseFloat(net) - parseFloat(discAmt);
	$("#prod_tot" + id).text(net.toFixed(2));
	var taxAmt = parseFloat(net) * parseFloat(tax) / 100;
	net = parseFloat(net) + parseFloat(taxAmt);
	$("#product_net" + id).text(net.toFixed(2));

	findTotalAmounts();
}
function findTotalAmounts() {
	var net = 0;
	var discount = 0;
	var taxAmount = 0;
	var chargeTotal = 0;
	var netAmount = 0;
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var quantity = $("#product_qty" + id).val();
			var price = $("#product_prc" + id).val();
			var disc = $("#product_disc" + id).val();
			var tax = $("#product_tx" + id).val();

			if (tax != "") {
				var taxRate = tax.split('!!!');
				tax = taxRate[1];
			} else {
				tax = "0.00";
			}
			var thisNet = parseFloat(quantity) * parseFloat(price);
			var discAmt = parseFloat(thisNet) * parseFloat(disc) / 100;
			thisNet = parseFloat(thisNet) - parseFloat(discAmt);
			netAmount = parseFloat(netAmount) + parseFloat(thisNet);
			// $("#prod_tot"+id).text(thisNet.toFixed(2));
			var taxAmt = parseFloat(thisNet) * parseFloat(tax) / 100;
			thisNet = parseFloat(thisNet) + parseFloat(taxAmt);
			net = parseFloat(net) + parseFloat(thisNet);
			discount = parseFloat(discount) + parseFloat(discAmt);
			taxAmount = parseFloat(taxAmount) + parseFloat(taxAmt);
		}
	});

	$(".chrg_tbl tr").each(
			function() {
				var id = this.id;
				if (id != '') {

					chargeTotal = parseFloat(chargeTotal)
							+ parseFloat($("#chrge_price" + id).val());
				}
			});

	net = parseFloat(net) + parseFloat(chargeTotal);
	netAmount = parseFloat(netAmount) + parseFloat(chargeTotal);
	$("#netAmountHidden").val(netAmount.toFixed(2));
	$("#subTotal").text(net.toFixed(2));
	$("#disTotal").text(discount.toFixed(2));
	$("#taxAmountHidden").val(taxAmount.toFixed(2));
	$("#netTotal").text(net.toFixed(2));
	// var grand = parseFloat(net) + parseFloat(taxAmount);
	// $("#grandTotal").text(grand.toFixed(2));
	selectedTaxes();
}

function selectedTaxes() {
	var subTotal = $("#subTotal").text();
	var discountRate = $("#discRate").val();
	if (discountRate.length == 0)
		discountRate = 0;
	var discAmount = parseFloat(subTotal) * parseFloat(discountRate) / 100;
	$("#disTotal").text(discAmount.toFixed(2));
	var net = parseFloat(subTotal) - parseFloat(discAmount);
	net = net.toFixed(2);
	$("#netTotal").text(net);
	var taxes = $("#taxesMulti").val();
	var totalTax = 0;
	if (taxes != null) {
		$.each(taxes, function(i, it) {
			var rate = it.split("!!!");
			totalTax = parseFloat(totalTax) + parseFloat(rate[1]);
		});
	}
	totalTax = totalTax.toFixed(2);
	var taxAmount = 0;
	if (totalTax != 0) {
		taxAmount = parseFloat(net) * parseFloat(totalTax) / 100;
		taxAmount = taxAmount.toFixed(2);
		$("#taxTotal").text(taxAmount);
		$("#grandTotal").text(parseFloat(net) + parseFloat(taxAmount));
	} else {
		$("#taxTotal").text(0.00);
		$("#grandTotal").text(net);
	}
}

function saveRceivingNote() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	if ($("#bill_vendr").val().length == 0) {
		msg = "Please Select Vendor...";
		flag = false;
	}

	var vo = new Object();
	var productVos = [];
	vo.noteId = $("#noteId").val();
	vo.orderId = $("#orderId").val();
	vo.vendorId = $("#bill_vendr").val();
	vo.date = $("#bill-date").val();

	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.godownId = $("#product_godwn" + id).val();
			if (product.godownId == null) {
				msg = "Please Select Godown...";
				flag = false;
			}
			product.quoteProductId = $("#itemId" + id).val();
			productVos.push(product);
		}
	});
	if (productVos.length == 0) {
		msg = "Please Select Products...";
		flag = false;
	}
	vo.productVos = productVos;
	var json = JSON.stringify(vo);
	if (flag) {
		$
				.ajax({
					type : "POST",
					url : "saveReceivingNote.do",
					cache : false,
					async : true,
					data : json,
					dataType : "json",
					contentType : "application/json;charset=utf-8",
					success : function(response) {
						if (response == 0) {
							$("#errror-msg").css("display", "block");
							$(".modal-body").text(
									"Oops...! Something went wrong.");
							$("#load-erp").css("display", "none");
						} else {
							var currentUrl = location.href;
							if ($("#orderId").val() != '') {

								if (currentUrl
										.indexOf("purchaseOrderToReceivingNote.do") > 0) {
									var split = currentUrl
											.split("purchaseOrderToReceivingNote.do");
									pageurl = split[0]
											+ "viewReceivingNote.do?id="
											+ response
											+ "&msg=Receiving Note Saved Successfully";
								} else if (currentUrl
										.indexOf('viewReceivingNote.do') > 0) {
									var split = currentUrl
											.split("viewReceivingNote.do");
									pageurl = split[0]
											+ "viewReceivingNote.do?id="
											+ response
											+ "&msg=Receiving Note Saved Successfully";
								}
							} else if (currentUrl
									.indexOf('viewReceivingNote.do') > 0) {
								var split = currentUrl
										.split("viewReceivingNote.do");
								pageurl = split[0]
										+ "viewReceivingNote.do?id="
										+ response
										+ "&msg=Receiving Note Saved Successfully";
							} else {
								pageurl = currentUrl
										.replace(
												"addReceivingNote.do",
												"viewReceivingNote.do?id="
														+ response
														+ "&msg=Receiving Note Saved Successfully");
							}
							window.history.pushState({
								path : pageurl
							}, '', pageurl);
							location.reload();
						}
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
					},
					complete : function() {
						$("#load-erp").css("display", "none");
					}
				});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}

function findNetAmountForIE(id) {
	var qty = $("#product_qty" + id).val();
	var price = $("#product_prc" + id).val();
	var total = parseFloat(qty) * parseFloat(price);
	$("#prod_tot" + id).text(total.toFixed(2));
}

/**
 * function to save iE
 */
function saveInventoryEntry() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	var vo = new Object();
	var itemsVos = [];
	vo.date = $("#ie-date").val();
	vo.narration = $("#ie_narration").val();
	vo.godownId = $("#ie_godown").val();
	vo.inventoryEntryId = $("#inventoryEntryId").val();
	vo.type = "Manual";
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.unitPrice = $("#product_prc" + id).val();
			product.total = $("#prod_tot" + id).text();
			itemsVos.push(product);
		}
	});
	if (itemsVos.length == 0) {
		msg = "Please Select Products...";
		flag = false;
	}
	vo.itemsVos = itemsVos;
	var json = JSON.stringify(vo);
	if (flag) {
		$
				.ajax({
					type : "POST",
					url : "saveInventoryEntry.do",
					cache : false,
					async : true,
					data : json,
					dataType : "json",
					contentType : "application/json;charset=utf-8",
					success : function(response) {
						if (response == 0) {
							$("#errror-msg").css("display", "block");
							$(".modal-body").text(
									"Oops...! Something went wrong.");
							$("#load-erp").css("display", "none");
						} else {
							var currentUrl = location.href;
							pageurl = currentUrl
									.replace(
											"addInventoryEntry.do",
											"viewInventoryEntry.do?id="
													+ response
													+ "&msg=Inventory Entry Saved Successfully");
							window.history.pushState({
								path : pageurl
							}, '', pageurl);
							location.reload();
						}
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
					},
					complete : function() {
						$("#load-erp").css("display", "none");
					}
				});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}

/**
 * function to delete ie
 * 
 * @param id
 */
function deleteThisIE(id) {
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
	$("#delete-item").val("ie");
}

function deleteIE(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		cache : false,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		data : {
			"id" : id
		},
		url : "deleteInventoryEntry.do",
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

/**
 * function to delete iw
 * 
 * @param id
 */
function deleteThisIW(id) {
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
	$("#delete-item").val("iw");
}

function deleteIW(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		cache : false,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		data : {
			"id" : id
		},
		url : "deleteInventoryWithdrawal.do",
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function deleteReceivingNote(id) {
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
	$("#delete-item").val("rn");
}

function deleteDeliveryNOte(id) {
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
	$("#delete-item").val("dn");
}

function deletePackingList(id) {
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
	$("#delete-item").val("pl");
}

function deleteThisPackingList(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		cache : false,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		data : {
			"id" : id
		},
		url : "deletePackingList.do",
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function deleteThisDeliveryNOte(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		cache : false,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		data : {
			"id" : id
		},
		url : "deleteDeliveryNOte.do",
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}
function deleteThisReceivingNote(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		cache : false,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		data : {
			"id" : id
		},
		url : "deleteReceivingNOte.do",
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

/**
 * function to save inventory withdrawal
 */
function saveInventoryWithdrawal() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	var vo = new Object();
	var itemsVos = [];
	vo.date = $("#ie-date").val();
	vo.narration = $("#ie_narration").val();
	vo.godownId = $("#ie_godown").val();
	vo.inventoryWithdrawalId = $("#inventoryWithdrawalId").val();
	vo.type = "Manual";
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.unitPrice = $("#product_prc" + id).val();
			product.total = $("#prod_tot" + id).text();
			itemsVos.push(product);
		}
	});
	if (itemsVos.length == 0) {
		msg = "Please Select Products...";
		flag = false;
	}
	vo.itemsVos = itemsVos;
	var json = JSON.stringify(vo);
	if (flag) {
		$
				.ajax({
					type : "POST",
					url : "saveInventoryWithdrawal.do",
					cache : false,
					async : true,
					data : json,
					dataType : "json",
					contentType : "application/json;charset=utf-8",
					success : function(response) {
						if (response == 0) {
							$("#errror-msg").css("display", "block");
							$(".modal-body").text(
									"Oops...! Something went wrong.");
							$("#load-erp").css("display", "none");
						} else {
							var currentUrl = location.href;
							pageurl = currentUrl
									.replace(
											"addInventoryWithdrawal.do",
											"viewInventoryWithdrawal.do?id="
													+ response
													+ "&msg=Inventory Withdrawal Saved Successfully");
							window.history.pushState({
								path : pageurl
							}, '', pageurl);
							location.reload();
						}
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
					},
					complete : function() {
						$("#load-erp").css("display", "none");
					}
				});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}

function saveDeliveryNote() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	if ($("#bill_vendr").val().length == 0) {
		msg = "Please Select Customer...";
		flag = false;
	}

	var vo = new Object();
	var productVos = [];
	vo.noteId = $("#noteId").val();
	vo.invoiceId = $("#invoiceId").val();
	vo.orderId = $("#orderId").val();
	vo.customerId = $("#bill_vendr").val();
	vo.date = $("#bill-date").val();
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.godownId = $("#product_godwn" + id).val();
			if (product.godownId == "") {
				msg = "Please Select Godown...";
				flag = false;
			}
			product.quoteProductId = $("#itemId" + id).val();
			productVos.push(product);
		}
	});
	if (productVos.length == 0) {
		msg = "Please Select Products...";
		flag = false;
	}
	vo.productVos = productVos;
	var json = JSON.stringify(vo);
	if (flag) {
		$.ajax({
			type : "POST",
			url : "saveDeliveryNote.do",
			cache : false,
			async : true,
			data : json,
			dataType : "json",
			contentType : "application/json;charset=utf-8",
			success : function(response) {
				var pageurl='';
				if (response == 0) {
					$("#errror-msg").css("display", "block");
					$(".modal-body").text("Oops...! Something went wrong.");
					$("#load-erp").css("display", "none");
				} else {
					var currentUrl = location.href;
					if (currentUrl.indexOf("salesOrderToDeliveryNote.do") > 0) {
						var split = currentUrl
								.split("salesOrderToDeliveryNote.do");
						pageurl = split[0] + "viewDeliveryNote.do?id="
								+ response
								+ "&msg=Delivery Note Saved Successfully";
					} else if (currentUrl.indexOf("addDeliveryNote.do") > 0) {
						var split = currentUrl.split("addDeliveryNote.do");
						pageurl = split[0] + "viewDeliveryNote.do?id="
								+ response
								+ "&msg=Delivery Note Saved Successfully";
					}
					window.history.pushState({
						path : pageurl
					}, '', pageurl);
					location.reload();
				}
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			},
			complete : function() {
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}

}

function checkQuantity(id) {
	var hidQty = $("#product_qty_hid" + id).val();
	var qty = $("#product_qty" + id).val();
	if (parseFloat(qty) > parseFloat(hidQty)) {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(
				"Oops...! Remaining Quantity for " + id + " is only " + hidQty);
		$("#product_qty" + id).val(hidQty);
	}
}

function clearPacking() {
	$("#packing_id").val('');
	$("#packng_name").val('');
	$("#packng_weght").val('0.00');
}

function editPackingKind(id, name, weight) {
	$("#packing_id").val(id);
	$("#packng_name").val(name);
	$("#packng_weght").val(weight);
}

function savePacking() {
	var vo = new Object();
	vo.name = $("#packng_name").val();
	if (vo.name != "") {
		$("#load-erp").css("display", "block");
		vo.id = $("#packing_id").val();
		vo.weight = $("#packng_weght").val();
		clearPacking();
		$.ajax({
			type : "POST",
			url : "savePackingKinds.do",
			data : JSON.stringify(vo),
			contentType : "application/json;charset=utf-8",
			cache : false,
			async : true,
			dataType : "html",
			success : function(response) {
				$("#pckng_kind_list").html(response);
			},
			error : function(reqObj, error, errorThrown) {
				alert(errorThrown);
			},
			complete : function() {
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		$("#packng_name").focus();
	}
}

function deletePacking(id) {
	clearPacking();
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
	$("#delete-item").val("packingKind");
}

function deletePackingKind(id) {
	if (id != "") {
		$("#load-erp").css("display", "block");
		$.ajax({
			type : "GET",
			url : "deletePackingKinds.do",
			data : {
				"id" : id
			},
			contentType : "application/json;charset=utf-8",
			cache : false,
			async : true,
			dataType : "html",
			success : function(response) {
				$("#pckng_kind_list").html(response);
			},
			error : function(reqObj, error, errorThrown) {
				alert(errorThrown);
			},
			complete : function() {
				$("#load-erp").css("display", "none");
			}
		});
	}
}

function findNetWeight(id) {
	var qtyToPack = $("#prod_qty_to_pack" + id).val();
	var qty = $("#product_qty" + id).val();
	var weight = $("#product_weight" + id).val();
	var kindOfPackage = $("#kind_of_pack" + id).val();
	if (parseFloat(qty) > parseFloat(qtyToPack)) {
		qty = qtyToPack;
		$("#product_qty" + id).val(qtyToPack);
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(
				"Oops...! Remaining Quantity for " + id + " is only "
						+ qtyToPack);
	}
	var netWeight = parseFloat(qty) * parseFloat(weight);
	var grossWeight = netWeight;
	if (kindOfPackage != '') {
		var splitKind = kindOfPackage.split('!!!');
		var kindOfPackWeight = splitKind[1];
		grossWeight = parseFloat(grossWeight) + parseFloat(kindOfPackWeight);
	}
	$("#product_net_weight" + id).text(netWeight.toFixed(2));
	$("#product_gross_weight" + id).text(grossWeight.toFixed(2));
	findTotalWeights();
}

function findTotalWeights() {
	var grossWeight = "0.00";
	var netWeight = "0.00";
	var count = 0;
	$(".prdct_tbl tr")
			.each(
					function() {
						var id = this.id;
						if (id != '') {
							count++;
							grossWeight = parseFloat(grossWeight)
									+ parseFloat($("#product_gross_weight" + id)
											.text());
							netWeight = parseFloat(netWeight)
									+ parseFloat($("#product_net_weight" + id)
											.text());
						}
					});
	$("#total_gross_weigt").text(grossWeight.toFixed(2));
	$("#total_net_weigt").text(netWeight.toFixed(2));
	$("#numberOfPacks").text(count);
}

function savePackingList() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	var vo = new Object();
	var productVos = [];
	vo.packingListId = $("#packingListId").val();
	vo.invoiceId = $("#invoiceId").val();
	vo.date = $("#invoice-date").val();
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.numberOfPack = $("#product_no_of_pack" + id).text();
			product.netWeight = $("#product_net_weight" + id).text();
			product.grossWeight = $("#product_gross_weight" + id).text();
			var kindOfPackage = $("#kind_of_pack" + id).val();
			if (kindOfPackage != '') {
				var splitKind = kindOfPackage.split('!!!');
				var kindOfPackId = splitKind[0];
				product.packingKindId = kindOfPackId;
			} else {
				msg = "Please Select Packing Kind...";
				flag = false;
			}
			productVos.push(product);
		}
	});
	if (productVos.length == 0) {
		msg = "Please Select Products...";
		flag = false;
	}
	vo.itemsVos = productVos;
	vo.netWeight = $("#total_net_weigt").text();
	vo.grossWeight = $("#total_gross_weigt").text();
	vo.numberOfPacks = $("#numberOfPacks").text();
	var json = JSON.stringify(vo);
	if (flag) {
		$.ajax({
			type : "POST",
			url : "savePackingList.do",
			cache : false,
			async : true,
			data : json,
			dataType : "json",
			contentType : "application/json;charset=utf-8",
			success : function(response) {
				if (response == 0) {
					$("#errror-msg").css("display", "block");
					$(".modal-body").text("Oops...! Something went wrong.");
					$("#load-erp").css("display", "none");
				} else {
					var currentUrl = location.href;
					pageurl = currentUrl.replace("packThisInvoice.do",
							"viewPackingList.do?id=" + response
									+ "&msg=Packing List Saved Successfully");
					window.history.pushState({
						path : pageurl
					}, '', pageurl);
					location.reload();
				}
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			},
			complete : function() {
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}

}

function getDetailedView(id, prevId) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "getDetailedStockView.do",
		cache : false,
		async : true,
		data : {
			"id" : id,
			"prevId" : prevId
		},
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			$(".panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
	$("#load-erp").css("display", "none");
}

function getDetailedViewForPrevious() {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "getDetailedStockViewPrevious.do",
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			$(".panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
	$("#load-erp").css("display", "none");
}

function getDetailedMonthwiseViewForPrevious(id, prevId) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "stockSummaryMonthViseProductDetails.do",
		cache : false,
		async : true,
		data : {
			"id" : id,
			"prevId" : prevId
		},
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			$(".panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
	$("#load-erp").css("display", "none");
}

function loadVoucherForStock(type, id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "loadVoucher.do",
		cache : false,
		async : true,
		data : {
			"voucher" : type,
			"voucherId" : id
		},
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function close() {
	$("#pdtPop").css("display", "none");
}

addQtyToId = function(id, value) {
	var oldVal = $("." + id).val();
	var split = oldVal.split("!!!");
	$("." + id).val(split[0] + "!!!" + value);
}

function findNetAmountReceiving(id) {
	var quantity = $("#product_qty" + id).val();
	var quantityToReceive = $("#product_qty_hid" + id).val();
	var flag = true;
	if (parseFloat(quantity) > parseFloat(quantityToReceive)) {
		flag = false;
		$("#product_qty" + id).val(quantityToReceive);
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(
				"Only " + quantityToReceive + " Items can be Received for "
						+ id);
		findNetAmountReceiving(id);
	}
}