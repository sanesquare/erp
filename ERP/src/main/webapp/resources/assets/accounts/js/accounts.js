/**
 * js file for accounts
 * 
 * @since 30-July-2015
 */
function cnfrmDelete(id) {
	$("#id-hid").val(id);
	$("#confirm-delete").css("display", "block");
}
function convertDate(inputFormat) {
	function pad(s) {
		return (s < 10) ? '0' + s : s;
	}
	var d = new Date(inputFormat);
	return [ pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear() ]
			.join('/');
}

function process(date) {
	var parts = date.split("/");
	return new Date(parts[2], parts[1] - 1, parts[0]);
}

/**
 * Method to delete ledger
 * 
 * @param result
 */
deleteLedger = function(result) {

	if (result == 'true') {
		$("#load-erp ").css("display", "block");
		$.ajax({
			type : "GET",
			url : "deleteLedger.do",
			dataType : "html",
			cache : false,
			async : true,
			data : {
				"id" : $("#id-hid").val()
			},
			success : function(response) {
				ledgerList();
				$(".notfctn-cntnnt").text(response);
				$("#success-msg").css("display", "block");
			},
			error : function(requestObject, error, errorThrown) {
				$(".notfctn-cntnnt").text("Oops! Something went wrong!");
				$("#erro-msg ").css("display", "block");
			}
		});
	}
	$("#confirm-delete").css("display", "none");
	// $("#load-erp ").css("display", "none");
}

function test() {
	alert("test");
}
/**
 * Method to fill ledger template
 */
ledgerList = function() {
	$("#load-erp ").css("display", "block");
	$.ajax({
		type : "GET",
		url : "ledgerList.do",
		cache : false,
		data : {
			"companyId" : $("#selectedCompany").val()
		},
		async : true,
		success : function(response) {
			$(".c-name").text($("#selectedCompany option:selected").text());
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}
/**
 * Method to delete journal
 * 
 * @param result
 */
deleteJournal = function(result) {

	if (result == 'true') {
		$("#load-erp ").css("display", "block");
		$.ajax({
			type : "GET",
			url : "deleteJournal.do",
			dataType : "html",
			cache : false,
			async : true,
			data : {
				"id" : $("#id-hid").val()
			},
			success : function(response) {
				if (response == 'ok') {
					journalList();
					$(".notfctn-cntnnt").text("Journal Deleted Successfully.");
					$("#success-msg").css("display", "block");
				} else {
					$(".notfctn-cntnnt").text(response);
					$("#erro-msg ").css("display", "block");
					journalList();
				}
			},
			error : function(requestObject, error, errorThrown) {
				$(".notfctn-cntnnt").text("Oops! Something went wrong!");
				$("#erro-msg ").css("display", "block");
			}
		});
	}
	$("#confirm-delete").css("display", "none");
}
/**
 * Method to list journals
 */
journalList = function() {
	$.ajax({
		type : "GET",
		url : "journalList.do",
		cache : false,
		async : true,
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

/**
 * function to pop up
 */
function addLedgerGroup() {
	$.ajax({
		type : "GET",
		url : 'ledgerGroupList.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$(".pop-content").html(response);
			$("#ledgerGroupPop").css("display", "block");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to save ledger group
 */
function saveLedgerGroup() {
	var name = $("#ledgerGroupName").val();
	var id = $("#ledgerGroupIdHidden").val();
	var accountType = $("#ledgerGroupAcntTyp").val();
	if (name.length > 0 && accountType.length > 0) {
		$("#load-erp").css("display", "block");
		var vo = new Object();
		vo.groupId = id;
		vo.accountTypeId = accountType;
		vo.groupName = name;

		$.ajax({
			url : 'saveLedgerGroup.do',
			type : 'POST',
			cache : false,
			async : true,
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(vo),
			success : function(result) {
				if (result == "ok") {
					$("#ledgerGroupName").val('');
					$("#ledgerGroupIdHidden").val('');
					$("#ledgerGroupAcntTyp").val('');
					getLedgerGroupTable();
					$("#ledger-group-form").formValidation('revalidateField',
							'accountType');
					$("#ledger-group-form").formValidation('revalidateField',
							'name');
				} else {
					$(".successMsg").css("display", "block");
					$(".notfctn-cntnnt").text(result);
					$("#load-erp").css("display", "none");
				}

			},
			error : function(requestObject, error, errorThrown) {
				alert("ledger group save: " + errorThrown);
			}
		});
	} else {
		if (accountType.length == 0 && name.length == 0) {
			$(".type-requed").css("display", "block");
			$(".nme-requed").css("display", "block");
		} else if (name.length == 0)
			$(".nme-requed").css("display", "block");
		else {
			$(".type-requed").css("display", "block");
		}
	}
}

function closeNmeRqurd() {
	$(".nme-requed").css("display", "none");
}
function closeTypeRqurd() {
	$(".type-requed").css("display", "none");
}

/**
 * function to get ledger group table template
 */
function getLedgerGroupTable() {
	$.ajax({
		type : "GET",
		url : 'ledgerGroupTable.do',
		dataType : "html",
		cache : false,
		async : true,
		success : function(response) {
			$("#ledg-group-table").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert("get ledger group table: " + errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
}

/**
 * function to get available ledger groups
 */
function getLedgerGroups() {
	$.ajax({
		type : "GET",
		url : 'ledgerGroupsListItems.do',
		cache : false,
		async : true,
		success : function(response) {
			$("#legers_selct_bx option").remove();
			var options = '';
			$.each(response.result, function(i, item) {
				options += '<option label="' + item.groupName + '" value="'
						+ item.groupId + '"/>';
			});
			$("#legers_selct_bx").html(options);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to close up ledger group popup
 */
function closePop() {
	$("#ledgerGroupPop").css("display", "none");
	getLedgerGroups();
}

/**
 * function to edit selected ledger group
 */
function editThisLedgerGroup(name, id, accountType) {
	$("#ledgerGroupName").val(name);
	$("#ledgerGroupIdHidden").val(id);
	$("#ledgerGroupAcntTyp").val(accountType);
}

/**
 * function to delete ledger group
 * 
 * @param id
 */
function deleteThisLedgerGroup(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : 'deleteLedgerGroup.do',
		cache : false,
		async : true,
		data : {
			"groupId" : id
		},
		success : function(response) {
			var msg = response.status;
			getLedgerGroupTable();
			$(".notfctn-cntnnt").text(msg);
			$(".notification-panl").css("display", "block");
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			$("#load-erp").css("display", "none");
			alert(errorThrown);
		}
	});
}

function closeMessage() {
	$(".notification-panl").css("display", "none");
}

/**
 * function to add new row - journal
 */
var addRow = function(link) {
	$("#load-erp").css("display", "block");
	$("#totalCreditHidden").val($("#totalCredit").val());
	$("#totalDebitHidden").val($("#totalDebit").val());
	var size = $("#journalEntries tr").length;
	$("#journalEntries tr:last").remove();
	$.ajax({
		type : "GET",
		url : 'journalEntryTableRow.do',
		dataType : "html",
		data : {
			"rows" : size,
			"credit" : $("#totalCreditHidden").val(),
			"debit" : $("#totalDebitHidden").val()
		},
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#journalEntries").append(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});

}

/**
 * function to add new row - payment
 */
var addRowPayment = function(link) {
	$("#load-erp").css("display", "block");
	$("#totalCreditHiddenPayment").val($("#totalCreditPayment").val());
	$("#totalDebitHiddenPayment").val($("#totalDebitPayment").val());
	var size = $("#paymentEntries tr").length;
	$("#paymentEntries tr:last").remove();
	$.ajax({
		type : "GET",
		url : 'paymentEntryTableRow.do',
		dataType : "html",
		data : {
			"rows" : size,
			"credit" : $("#totalCreditHiddenPayment").val(),
			"debit" : $("#totalDebitHiddenPayment").val()
		},
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#paymentEntries").append(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});

}

/**
 * function to add new row - contra
 */
var addRowContra = function(link) {
	$("#load-erp").css("display", "block");
	$("#totalCreditHiddenContra").val($("#totalCreditContra").val());
	$("#totalDebitHiddenContra").val($("#totalDebitContra").val());
	var size = $("#contraEntries tr").length;
	$("#contraEntries tr:last").remove();
	$.ajax({
		type : "GET",
		url : 'contraEntryTableRow.do',
		dataType : "html",
		data : {
			"rows" : size,
			"credit" : $("#totalCreditHiddenContra").val(),
			"debit" : $("#totalDebitHiddenContra").val()
		},
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#contraEntries").append(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});

}

/**
 * function to add new row - receipt
 */
var addRowReceipt = function(link) {
	$("#load-erp").css("display", "block");
	$("#totalCreditHiddenReceipt").val($("#totalCreditReceipt").val());
	$("#totalDebitHiddenReceipt").val($("#totalDebitReceipt").val());
	var size = $("#receiptEntries tr").length;
	$("#receiptEntries tr:last").remove();
	$.ajax({
		type : "GET",
		url : 'receiptEntryTableRow.do',
		dataType : "html",
		data : {
			"rows" : size,
			"credit" : $("#totalCreditHiddenReceipt").val(),
			"debit" : $("#totalDebitHiddenReceipt").val()
		},
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#receiptEntries").append(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});

}

/**
 * function to delete specified row - journal
 */
var deleteRow = function(link, count) {
	var td = link.parentNode.parentNode;
	var row = td.parentNode;
	var table = row.parentNode;
	var credit = $("#credit" + count).val();
	var debit = $("#debit" + count).val();
	var totalCredit = parseFloat($("#totalCreditHidden").val())
			- parseFloat(credit);
	var totalDebit = parseFloat($("#totalDebitHidden").val())
			- parseFloat(debit);
	$("#totalCreditHidden").val(totalCredit);
	$("#totalDebitHidden").val(totalDebit);
	$("#totalCredit").val(totalCredit);
	$("#totalDebit").val(totalDebit);
	table.removeChild(row);
}

/**
 * function to delete specified row - payment
 */
var deleteRowPayment = function(link, count) {
	var td = link.parentNode.parentNode;
	var row = td.parentNode;
	var table = row.parentNode;
	var credit = $("#creditPayment" + count).val();
	var debit = $("#debitPayment" + count).val();
	var totalCredit = parseFloat($("#totalCreditHiddenPayment").val())
			- parseFloat(credit);
	var totalDebit = parseFloat($("#totalDebitHiddenPayment").val())
			- parseFloat(debit);
	$("#totalCreditHiddenPayment").val(totalCredit);
	$("#totalDebitHiddenPayment").val(totalDebit);
	$("#totalCreditPayment").val(totalCredit);
	$("#totalDebitPayment").val(totalDebit);
	table.removeChild(row);
}

/**
 * function to delete specified row - receipt
 */
var deleteRowReceipt = function(link, count) {
	var td = link.parentNode.parentNode;
	var row = td.parentNode;
	var table = row.parentNode;
	var credit = $("#creditReceipt" + count).val();
	var debit = $("#debitReceipt" + count).val();
	var totalCredit = parseFloat($("#totalCreditHiddenReceipt").val())
			- parseFloat(credit);
	var totalDebit = parseFloat($("#totalDebitHiddenReceipt").val())
			- parseFloat(debit);
	$("#totalCreditHiddenReceipt").val(totalCredit);
	$("#totalDebitHiddenReceipt").val(totalDebit);
	$("#totalCreditReceipt").val(totalCredit);
	$("#totalDebitReceipt").val(totalDebit);
	table.removeChild(row);
}

/**
 * function to delete specified row - contra
 */
var deleteRowContra = function(link, count) {
	var td = link.parentNode.parentNode;
	var row = td.parentNode;
	var table = row.parentNode;
	var credit = $("#creditContra" + count).val();
	var debit = $("#debitContra" + count).val();
	var totalCredit = parseFloat($("#totalCreditHiddenContra").val())
			- parseFloat(credit);
	var totalDebit = parseFloat($("#totalDebitHiddenContra").val())
			- parseFloat(debit);
	$("#totalCreditHiddenContra").val(totalCredit);
	$("#totalDebitHiddenContra").val(totalDebit);
	$("#totalCreditContra").val(totalCredit);
	$("#totalDebitContra").val(totalDebit);
	table.removeChild(row);
}

$(".double").on("blur", function() {
	if (this.value.length == 0)
		this.value = "0.00";
});

/**
 * function to calculate total credit - journal
 * 
 * @param id
 */
function addTotalCredit(id) {
	var previousAmount = $("#credit" + id + "hidden").val();
	var currentAmount = $("#credit" + id).val();
	if (currentAmount == '')
		currentAmount = "0.00";
	var totalCredit = $("#totalCreditHidden").val();
	totalCredit = parseFloat(totalCredit) - parseFloat(previousAmount);
	if (parseFloat(previousAmount) > parseFloat(currentAmount)) {
		totalCredit = parseFloat(totalCredit) + parseFloat(currentAmount);
	} else {
		totalCredit = parseFloat(totalCredit) + parseFloat(currentAmount);
	}
	$("#totalCreditHidden").val(totalCredit);
	$("#totalCredit").val(totalCredit);
	$("#credit" + id + "hidden").val($("#credit" + id).val());
	var debit = $("#debit" + id).val();
	if (currentAmount > 0) {
		if (parseFloat(debit) > 0) {
			$("#debit" + id).val("0.00");
			var prevDebit = $("#debit" + id + "hidden").val();
			var totDebit = $("#totalDebitHidden").val();
			totDebit = parseFloat(totDebit) - parseFloat(prevDebit);
			$("#totalDebitHidden").val(totDebit);
			$("#totalDebit").val(totDebit);
			$("#debit" + id + "hidden").val("0.00");
			addTotalDebit(id);
		}
	}
}

/**
 * function to calculate total credit - payment
 * 
 * @param id
 */
function addTotalCreditPayment(id) {
	var previousAmount = $("#creditPayment" + id + "hidden").val();
	var currentAmount = $("#creditPayment" + id).val();
	if (currentAmount == '')
		currentAmount = "0.00";
	var totalCredit = $("#totalCreditHiddenPayment").val();
	totalCredit = parseFloat(totalCredit) - parseFloat(previousAmount);
	if (parseFloat(previousAmount) > parseFloat(currentAmount)) {
		totalCredit = parseFloat(totalCredit) + parseFloat(currentAmount);
	} else {
		totalCredit = parseFloat(totalCredit) + parseFloat(currentAmount);
	}
	$("#totalCreditHiddenPayment").val(totalCredit);
	$("#totalCreditPayment").val(totalCredit);
	$("#creditPayment" + id + "hidden").val($("#creditPayment" + id).val());
	var debit = $("#debitPayment" + id).val();
	if (currentAmount > 0) {
		if (parseFloat(debit) > 0) {
			$("#debitPayment" + id).val("0.00");
			var prevDebit = $("#debitPayment" + id + "hidden").val();
			var totDebit = $("#totalDebitHiddenPayment").val();
			totDebit = parseFloat(totDebit) - parseFloat(prevDebit);
			$("#totalDebitHiddenPayment").val(totDebit);
			$("#totalDebitPayment").val(totDebit);
			$("#debitPayment" + id + "hidden").val("0.00");
			addTotalDebitPayment(id);
		}
	}
}

/**
 * function to calculate total credit - receipt
 * 
 * @param id
 */
function addTotalCreditReceipt(id) {
	var previousAmount = $("#creditReceipt" + id + "hidden").val();
	var currentAmount = $("#creditReceipt" + id).val();
	if (currentAmount == '')
		currentAmount = "0.00";
	var totalCredit = $("#totalCreditHiddenReceipt").val();
	totalCredit = parseFloat(totalCredit) - parseFloat(previousAmount);
	if (parseFloat(previousAmount) > parseFloat(currentAmount)) {
		totalCredit = parseFloat(totalCredit) + parseFloat(currentAmount);
	} else {
		totalCredit = parseFloat(totalCredit) + parseFloat(currentAmount);
	}
	$("#totalCreditHiddenReceipt").val(totalCredit);
	$("#totalCreditReceipt").val(totalCredit);
	$("#creditReceipt" + id + "hidden").val($("#creditReceipt" + id).val());
	var debit = $("#debitReceipt" + id).val();
	if (currentAmount > 0) {
		if (parseFloat(debit) > 0) {
			$("#debitReceipt" + id).val("0.00");
			var prevDebit = $("#debitReceipt" + id + "hidden").val();
			var totDebit = $("#totalDebitHiddenReceipt").val();
			totDebit = parseFloat(totDebit) - parseFloat(prevDebit);
			$("#totalDebitHiddenReceipt").val(totDebit);
			$("#totalDebitReceipt").val(totDebit);
			$("#debitReceipt" + id + "hidden").val("0.00");
			addTotalDebitReceipt(id);
		}
	}
}

/**
 * function to calculate total credit - contra
 * 
 * @param id
 */
function addTotalCreditContra(id) {
	var previousAmount = $("#creditContra" + id + "hidden").val();
	var currentAmount = $("#creditContra" + id).val();
	if (currentAmount == '')
		currentAmount = "0.00";
	var totalCredit = $("#totalCreditHiddenContra").val();
	totalCredit = parseFloat(totalCredit) - parseFloat(previousAmount);
	if (parseFloat(previousAmount) > parseFloat(currentAmount)) {
		totalCredit = parseFloat(totalCredit) + parseFloat(currentAmount);
	} else {
		totalCredit = parseFloat(totalCredit) + parseFloat(currentAmount);
	}
	$("#totalCreditHiddenContra").val(totalCredit);
	$("#totalCreditContra").val(totalCredit);
	$("#creditContra" + id + "hidden").val($("#creditContra" + id).val());
	var debit = $("#debitContra" + id).val();
	if (currentAmount > 0) {
		if (parseFloat(debit) > 0) {
			$("#debitContra" + id).val("0.00");
			var prevDebit = $("#debitContra" + id + "hidden").val();
			var totDebit = $("#totalDebitHiddenContra").val();
			totDebit = parseFloat(totDebit) - parseFloat(prevDebit);
			$("#totalDebitHiddenContra").val(totDebit);
			$("#totalDebitContra").val(totDebit);
			$("#debitContra" + id + "hidden").val("0.00");
			addTotalDebitContra(id);
		}
	}
}

/**
 * function to calculate total debit - journal
 * 
 * @param id
 */
function addTotalDebit(id) {
	var previousAmount = $("#debit" + id + "hidden").val();
	var currentAmount = $("#debit" + id).val();
	if (currentAmount == '')
		currentAmount = "0.00";
	var totalDebit = $("#totalDebitHidden").val();
	totalDebit = parseFloat(totalDebit) - parseFloat(previousAmount);
	if (parseFloat(previousAmount) > parseFloat(currentAmount)) {
		totalDebit = parseFloat(totalDebit) + parseFloat(currentAmount);
	} else {
		totalDebit = parseFloat(totalDebit) + parseFloat(currentAmount);
	}
	$("#totalDebitHidden").val(totalDebit);
	$("#totalDebit").val(totalDebit);
	$("#debit" + id + "hidden").val($("#debit" + id).val());
	var credit = $("#credit" + id).val();
	if (currentAmount > 0) {
		if (parseFloat(credit) > 0) {
			$("#credit" + id).val("0.00");
			var prevCredit = $("#credit" + id + "hidden").val();
			var totCredit = $("#totalCreditHidden").val();
			totCredit = parseFloat(totCredit) - parseFloat(prevCredit);
			$("#totalCreditHidden").val(totCredit);
			$("#totalCredit").val(totCredit);
			$("#credit" + id + "hidden").val("0.00");
			addTotalCredit(id);
		}
	}
}

/**
 * function to calculate total debit - payment
 * 
 * @param id
 */
function addTotalDebitPayment(id) {
	var previousAmount = $("#debitPayment" + id + "hidden").val();
	var currentAmount = $("#debitPayment" + id).val();
	if (currentAmount == '')
		currentAmount = "0.00";
	var totalDebit = $("#totalDebitHiddenPayment").val();
	totalDebit = parseFloat(totalDebit) - parseFloat(previousAmount);
	if (parseFloat(previousAmount) > parseFloat(currentAmount)) {
		totalDebit = parseFloat(totalDebit) + parseFloat(currentAmount);
	} else {
		totalDebit = parseFloat(totalDebit) + parseFloat(currentAmount);
	}
	$("#totalDebitHiddenPayment").val(totalDebit);
	$("#totalDebitPayment").val(totalDebit);
	$("#debitPayment" + id + "hidden").val($("#debitPayment" + id).val());
	var credit = $("#creditPayment" + id).val();
	if (currentAmount > 0) {
		if (parseFloat(credit) > 0) {
			$("#creditPayment" + id).val("0.00");
			var prevCredit = $("#creditPayment" + id + "hidden").val();
			var totCredit = $("#totalCreditHiddenPayment").val();
			totCredit = parseFloat(totCredit) - parseFloat(prevCredit);
			$("#totalCreditHiddenPayment").val(totCredit);
			$("#totalCreditPayment").val(totCredit);
			$("#creditPayment" + id + "hidden").val("0.00");
			addTotalCreditPayment(id);
		}
	}
}

/**
 * function to calculate total debit - receipt
 * 
 * @param id
 */
function addTotalDebitReceipt(id) {
	var previousAmount = $("#debitReceipt" + id + "hidden").val();
	var currentAmount = $("#debitReceipt" + id).val();
	if (currentAmount == '')
		currentAmount = "0.00";
	var totalDebit = $("#totalDebitHiddenReceipt").val();
	totalDebit = parseFloat(totalDebit) - parseFloat(previousAmount);
	if (parseFloat(previousAmount) > parseFloat(currentAmount)) {
		totalDebit = parseFloat(totalDebit) + parseFloat(currentAmount);
	} else {
		totalDebit = parseFloat(totalDebit) + parseFloat(currentAmount);
	}
	$("#totalDebitHiddenReceipt").val(totalDebit);
	$("#totalDebitReceipt").val(totalDebit);
	$("#debitReceipt" + id + "hidden").val($("#debitReceipt" + id).val());
	var credit = $("#creditReceipt" + id).val();
	if (currentAmount > 0) {
		if (parseFloat(credit) > 0) {
			$("#creditReceipt" + id).val("0.00");
			var prevCredit = $("#creditReceipt" + id + "hidden").val();
			var totCredit = $("#totalCreditHiddenReceipt").val();
			totCredit = parseFloat(totCredit) - parseFloat(prevCredit);
			$("#totalCreditHiddenReceipt").val(totCredit);
			$("#totalCreditReceipt").val(totCredit);
			$("#creditReceipt" + id + "hidden").val("0.00");
			addTotalCreditReceipt(id);
		}
	}
}

/**
 * function to calculate total debit - Contra
 * 
 * @param id
 */
function addTotalDebitContra(id) {
	var previousAmount = $("#debitContra" + id + "hidden").val();
	var currentAmount = $("#debitContra" + id).val();
	if (currentAmount == '')
		currentAmount = "0.00";
	var totalDebit = $("#totalDebitHiddenContra").val();
	totalDebit = parseFloat(totalDebit) - parseFloat(previousAmount);
	if (parseFloat(previousAmount) > parseFloat(currentAmount)) {
		totalDebit = parseFloat(totalDebit) + parseFloat(currentAmount);
	} else {
		totalDebit = parseFloat(totalDebit) + parseFloat(currentAmount);
	}
	$("#totalDebitHiddenContra").val(totalDebit);
	$("#totalDebitContra").val(totalDebit);
	$("#debitContra" + id + "hidden").val($("#debitContra" + id).val());
	var credit = $("#creditContra" + id).val();
	if (currentAmount > 0) {
		if (parseFloat(credit) > 0) {
			$("#creditContra" + id).val("0.00");
			var prevCredit = $("#creditContra" + id + "hidden").val();
			var totCredit = $("#totalCreditHiddenContra").val();
			totCredit = parseFloat(totCredit) - parseFloat(prevCredit);
			$("#totalCreditHiddenContra").val(totCredit);
			$("#totalCreditContra").val(totCredit);
			$("#creditContra" + id + "hidden").val("0.00");
			addTotalCreditContra(id);
		}
	}
}

/**
 * function to save journal
 */
function saveJournal() {
	$("#load-erp ").css("display", "block");
	$.ajax({
		type : "GET",
		url : "getJournalRowCount.do",
		cache : false,
		async : true,
		success : function(response) {
			saveJournalWithrowCount(response);
			$("#load-erp ").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			$("#load-erp ").css("display", "none");
		}
	});
}

/**
 * function to save journal with size
 * 
 * @param size
 */
function saveJournalWithrowCount(size) {
	var debits = $("#totalDebit").val();
	var credits = $("#totalCredit").val();
	var flag = true;
	if (parseFloat(debits) == parseFloat(credits)) {
		var vo = new Object();
		vo.journalId = $("#journalId").val();
		vo.date = $("#journal-date").val();
		vo.narration = $("#jrnl_nartn").val();
		if (vo.date == "") {
			flag = false;
			$("#errror-msg").css("display", "block");
			$(".modal-body").text("Date is required..");
			setTimeout(function() {
				$("#errror-msg").fadeOut("600");
			}, 1000);
			$("#jrnl_frm").formValidation('revalidateField', 'date');
		}
		vo.voucherNumber = $("#voucherNumber").text();
		vo.amount = $("#totalDebit").val();
		var entries = [];
		for (var i = 1; i <= size; i++) {
			if (document.getElementById('credit' + i)) {
				var entryVo = new Object();
				entryVo.debitAmnt = $("#debit" + i).val();
				entryVo.creditAmnt = $("#credit" + i).val();
				entryVo.id = $("#id" + i).val();
				var accType = $("#accType" + i).val();
				if (accType == "") {
					$("#errror-msg").css("display", "block");
					$(".modal-body").text("Account type is required..");
					setTimeout(function() {
						$("#errror-msg").fadeOut("600");
					}, 1000);
					$("#jrnl_frm").formValidation('revalidateField', 'date');
					$("#accType" + i).focus();
					$("#accType" + i).css("border-color", "#E00808");
					flag = false;
					break;
				}
				entryVo.accTypeId = accType;
				entryVo.journalId = $("#journalId").val();
				entries.push(entryVo);
			}
		}
		vo.entries = entries;
		var json = JSON.stringify(vo);
		if (flag) {
			$.ajax({
				type : "POST",
				url : 'saveJournal.do',
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(vo),
				cache : false,
				async : true,
				success : function(response) {
					var currentUrl = location.href;
					pageurl = currentUrl.replace('addJournal.do',
							'editJournal.do?id=' + response);
					window.history.pushState({
						path : pageurl
					}, '', pageurl);
					location.reload();
					$("#journalId").val(response);
					getJournalEntriesTable(response);
					$(".successMsg").css("display", "block");
					$(".notfctn-cntnnt").text("Saved successfully.");
				},
				error : function(requestObject, error, errorThrown) {
					alert("Journal  save: " + errorThrown);
				}
			});
		}
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text("Journal entries must be balanced..");
		setTimeout(function() {
			// $("#errror-msg").fadeOut("600");
		}, 1000);
	}

}

/**
 * function to save payment
 */
function savePayment() {
	$("#load-erp ").css("display", "block");
	$.ajax({
		type : "GET",
		url : "getPaymentRowCount.do",
		cache : false,
		async : true,
		success : function(response) {
			savePaymentWithRowCount(response);
			$("#load-erp ").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			$("#load-erp ").css("display", "none");
		}
	});
}

function savePaymentWithRowCount(size) {
	var debits = $("#totalDebitPayment").val();
	var credits = $("#totalCreditPayment").val();
	var flag = true;
	if (parseFloat(debits) == parseFloat(credits)) {
		var vo = new Object();
		vo.paymentId = $("#paymentId").val();
		vo.date = $("#pymntDate").val();
		vo.narration = $("#pymntNrrtin").val();
		if (vo.date == "") {
			flag = false;
			$("#errror-msg").css("display", "block");
			$(".modal-body").text("Date is required..");
			setTimeout(function() {
				$("#errror-msg").fadeOut("600");
			}, 1000);
			$("#pymnt_frm_new").formValidation('revalidateField', 'date');
		}
		vo.voucherNumber = $("#pymntVoucher").text();
		vo.amount = $("#totalDebitPayment").val();
		var entries = [];
		for (var i = 1; i <= size; i++) {
			if (document.getElementById('creditPayment' + i)) {
				var entryVo = new Object();
				entryVo.debitAmnt = $("#debitPayment" + i).val();
				entryVo.creditAmnt = $("#creditPayment" + i).val();
				entryVo.id = $("#idPayment" + i).val();
				var accType = $("#accTypePayment" + i).val();
				if (accType == "") {
					$("#errror-msg").css("display", "block");
					$(".modal-body").text("Account type is required..");
					setTimeout(function() {
						$("#errror-msg").fadeOut("600");
					}, 1000);
					$("#pymnt_frm_new").formValidation('revalidateField',
							'date');
					$("#accTypePayment" + i).focus();
					$("#accTypePayment" + i).css("border-color", "#E00808");
					flag = false;
					break;
				}
				entryVo.accTypeId = accType;
				entryVo.paymentId = $("#paymentId").val();
				entries.push(entryVo);
			}
		}
		vo.entries = entries;
		var json = JSON.stringify(vo);
		if (flag) {
			$.ajax({
				type : "POST",
				url : 'savePayment.do',
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(vo),
				cache : false,
				async : true,
				success : function(response) {
					var currentUrl = location.href;
					pageurl = currentUrl.replace('addPayment.do',
							'editPayment.do?id=' + response);
					window.history.pushState({
						path : pageurl
					}, '', pageurl);
					location.reload();
					$("#paymentId").val(response);
					getPaymentEntriesTable(response);
					$(".successMsg").css("display", "block");
					$(".notfctn-cntnnt").text("Saved successfully.");
				},
				error : function(requestObject, error, errorThrown) {
					alert("Payment  save: " + errorThrown);
				}
			});
		}
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text("Payment entries must be balanced..");
		setTimeout(function() {
			// $("#errror-msg").fadeOut("600");
		}, 1000);
	}
}

/**
 * function to save receipt
 */
function saveReceipt() {
	$("#load-erp ").css("display", "block");
	$.ajax({
		type : "GET",
		url : "getReceiptRowCount.do",
		cache : false,
		async : true,
		success : function(response) {
			saveReceiptWithRowCount(response);
			$("#load-erp ").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			$("#load-erp ").css("display", "none");
		}
	});
}

function saveReceiptWithRowCount(size) {
	var debits = $("#totalDebitReceipt").val();
	var credits = $("#totalCreditReceipt").val();
	var flag = true;
	if (parseFloat(debits) == parseFloat(credits)) {
		var vo = new Object();
		vo.receiptId = $("#receiptId").val();
		vo.date = $("#rcptDate").val();
		vo.narration = $("#rcptNrrtin").val();
		if (vo.date == "") {
			flag = false;
			$("#errror-msg").css("display", "block");
			$(".modal-body").text("Date is required..");
			setTimeout(function() {
				$("#errror-msg").fadeOut("600");
			}, 1000);
			$("#rcpt_frm_new").formValidation('revalidateField', 'date');
		}
		vo.voucherNumber = $("#rcptVoucher").text();
		vo.amount = $("#totalDebitReceipt").val();
		var entries = [];
		for (var i = 1; i <= size; i++) {
			if (document.getElementById('creditReceipt' + i)) {
				var entryVo = new Object();
				entryVo.debitAmnt = $("#debitReceipt" + i).val();
				entryVo.creditAmnt = $("#creditReceipt" + i).val();
				entryVo.id = $("#idReceipt" + i).val();
				var accType = $("#accTypeReceipt" + i).val();
				if (accType == "") {
					$("#errror-msg").css("display", "block");
					$(".modal-body").text("Account type is required..");
					setTimeout(function() {
						$("#errror-msg").fadeOut("600");
					}, 1000);
					$("#rcpt_frm_new")
							.formValidation('revalidateField', 'date');
					$("#accTypeReceipt" + i).focus();
					$("#accTypeReceipt" + i).css("border-color", "#E00808");
					flag = false;
					break;
				}
				entryVo.accTypeId = accType;
				entryVo.paymentId = $("#receiptId").val();
				entries.push(entryVo);
			}
		}
		vo.entries = entries;
		var json = JSON.stringify(vo);
		if (flag) {
			$.ajax({
				type : "POST",
				url : 'saveReceipt.do',
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(vo),
				cache : false,
				async : true,
				success : function(response) {
					var currentUrl = location.href;
					pageurl = currentUrl.replace('addReceipt.do',
							'editReceipt.do?id=' + response);
					window.history.pushState({
						path : pageurl
					}, '', pageurl);
					location.reload();
					$("#receiptId").val(response);
					getReceiptEntriesTable(response);
					$(".successMsg").css("display", "block");
					$(".notfctn-cntnnt").text("Saved successfully.");
				},
				error : function(requestObject, error, errorThrown) {
					alert("Receipt  save: " + errorThrown);
				}
			});
		}
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text("Receipt entries must be balanced..");
		setTimeout(function() {
			// $("#errror-msg").fadeOut("600");
		}, 1000);
	}
}

/**
 * function to save Contra
 */
function saveContra() {
	$("#load-erp ").css("display", "block");
	$.ajax({
		type : "GET",
		url : "getContraRowCount.do",
		cache : false,
		async : true,
		success : function(response) {
			saveContraWithRowCount(response);
			$("#load-erp ").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			$("#load-erp ").css("display", "none");
		}
	});
}

function saveContraWithRowCount(size) {
	var debits = $("#totalDebitContra").val();
	var credits = $("#totalCreditContra").val();
	var flag = true;
	if (parseFloat(debits) == parseFloat(credits)) {
		var vo = new Object();
		vo.contraId = $("#contraId").val();
		vo.date = $("#cntraDate").val();
		vo.narration = $("#cntraNrrtin").val();
		if (vo.date == "") {
			flag = false;
			$("#errror-msg").css("display", "block");
			$(".modal-body").text("Date is required..");
			setTimeout(function() {
				$("#errror-msg").fadeOut("600");
			}, 1000);
			$("#cntra_frm_new").formValidation('revalidateField', 'date');
		}
		vo.voucherNumber = $("#cntraVoucher").text();
		vo.amount = $("#totalDebitContra").val();
		var entries = [];
		for (var i = 1; i <= size; i++) {
			if (document.getElementById('creditContra' + i)) {
				var entryVo = new Object();
				entryVo.debitAmnt = $("#debitContra" + i).val();
				entryVo.creditAmnt = $("#creditContra" + i).val();
				entryVo.id = $("#idContra" + i).val();
				var accType = $("#accTypeContra" + i).val();
				if (accType == "") {
					$("#errror-msg").css("display", "block");
					$(".modal-body").text("Account type is required..");
					setTimeout(function() {
						$("#errror-msg").fadeOut("600");
					}, 1000);
					$("#cntra_frm_new").formValidation('revalidateField',
							'date');
					$("#accTypeContra" + i).focus();
					$("#accTypeContra" + i).css("border-color", "#E00808");
					flag = false;
					break;
				}
				entryVo.accTypeId = accType;
				entryVo.paymentId = $("#contraId").val();
				entries.push(entryVo);
			}
		}
		vo.entries = entries;
		var json = JSON.stringify(vo);
		if (flag) {
			$.ajax({
				type : "POST",
				url : 'saveContra.do',
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(vo),
				cache : false,
				async : true,
				success : function(response) {
					var currentUrl = location.href;
					pageurl = currentUrl.replace('addContra.do',
							'editContra.do?id=' + response);
					window.history.pushState({
						path : pageurl
					}, '', pageurl);
					location.reload();
					$("#contraId").val(response);
					getContraEntriesTable(response);
					$(".successMsg").css("display", "block");
					$(".notfctn-cntnnt").text("Saved successfully.");
				},
				error : function(requestObject, error, errorThrown) {
					alert("Contra  save: " + errorThrown);
				}
			});
		}
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text("Contra entries must be balanced..");
		setTimeout(function() {
			// $("#errror-msg").fadeOut("600");
		}, 1000);
	}
}

function getJournalEntriesTable(id) {
	$.ajax({
		type : "GET",
		url : "journalEntriesTable.do",
		dataType : "html",
		data : {
			"id" : id
		},
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			console.log(response);
			$("#journlaTableBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function getPaymentEntriesTable(id) {
	$.ajax({
		type : "GET",
		url : "paymentEntriesTable.do",
		dataType : "html",
		data : {
			"id" : id
		},
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			console.log(response);
			$("#paymentTableBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function getReceiptEntriesTable(id) {
	$.ajax({
		type : "GET",
		url : "receiptEntriesTable.do",
		dataType : "html",
		data : {
			"id" : id
		},
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			console.log(response);
			$("#receiptTableBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function getContraEntriesTable(id) {
	$.ajax({
		type : "GET",
		url : "contraEntriesTable.do",
		dataType : "html",
		data : {
			"id" : id
		},
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			console.log(response);
			$("#contraTableBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

closeError = function() {
	$("#errror-msg").fadeOut("600");
}

$(".typesss").on("change", function() {
	$(".typesss").css("border", "1px solid #ccc");
});

/**
 * function to confirm delete payment
 * 
 * @param id
 */
cnfrmDeletePayment = function(id) {
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
}

deletePayment = function(result) {
	if (result == "true") {
		$("#load-erp ").css("display", "block");
		var id = $("#id-hid").val();
		$.ajax({
			type : "GET",
			url : "deletePayment.do",
			data : {
				"id" : id
			},
			cache : false,
			async : true,
			dataType : "html",
			success : function(response) {
				$(".notfctn-cntnnt").text("Action Completed Successfully.");
				$("#panelBody").html(response);
				$("#success-msg").css("display", "block");
			},
			error : function(requestObject, error, errorThrown) {
				$(".notfctn-cntnnt").text(errorThrown);
				$("#success-msg").css("display", "block");
			}
		});
	}
	$("#confirm-delete").css("display", "none");
	$("#id-hid").val("");
}

/**
 * function to confirm delete receipt
 * 
 * @param id
 */
cnfrmDeleteReceipt = function(id) {
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
}

deleteReceipt = function(result) {
	if (result == "true") {
		$("#load-erp ").css("display", "block");
		var id = $("#id-hid").val();
		$.ajax({
			type : "GET",
			url : "deleteReceipt.do",
			data : {
				"id" : id
			},
			cache : false,
			async : true,
			dataType : "html",
			success : function(response) {
				$(".notfctn-cntnnt").text("Action Completed Successfully.");
				$("#panelBody").html(response);
				$("#success-msg").css("display", "block");
			},
			error : function(requestObject, error, errorThrown) {
				$(".notfctn-cntnnt").text(errorThrown);
				$("#success-msg").css("display", "block");
			}
		});
	}
	$("#confirm-delete").css("display", "none");
	$("#id-hid").val("");
}

/**
 * function to confirm delete contra
 * 
 * @param id
 */
cnfrmDeleteContra = function(id) {
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
}

deleteContra = function(result) {
	if (result == "true") {
		$("#load-erp ").css("display", "block");
		var id = $("#id-hid").val();
		$.ajax({
			type : "GET",
			url : "deleteContra.do",
			data : {
				"id" : id
			},
			cache : false,
			async : true,
			dataType : "html",
			success : function(response) {
				$(".notfctn-cntnnt").text("Action Completed Successfully.");
				$("#panelBody").html(response);
				$("#success-msg").css("display", "block");
			},
			error : function(requestObject, error, errorThrown) {
				$(".notfctn-cntnnt").text(errorThrown);
				$("#success-msg").css("display", "block");
			}
		});
	}
	$("#confirm-delete").css("display", "none");
	$("#id-hid").val("");
}

/**
 * function to filter contra
 */
filterContra = function() {
	var start = $("#contraStart").val();
	var end = $("#contraEnd").val();
	if (start.length > 0 && end.length > 0) {
		if (process(start) > process(end)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "GET",
				url : "contraListFilter.do",
				data : {
					"startDate" : start,
					"endDate" : end,
					"companyId" : $("#selectedCompany").val()
				},
				cache : false,
				async : true,
				dataType : "html",
				success : function(response) {
					$(".c-name").text(
							$("#selectedCompany option:selected").text()
									+ " ( " + start + " to " + end + " )");
					$("#panelBody").html(response);
					$("#load-erp").css("display", "none");
				},
				error : function(requestObject, error, errorThrown) {
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
					$("#load-erp").css("display", "none");
				}
			});
		}
	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}

/**
 * function to filter receipt
 */
filterReceipt = function() {
	var start = $("#receiptStart").val();
	var end = $("#receiptEnd").val();
	if (start.length > 0 && end.length > 0) {
		if (process(start) > process(end)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "GET",
				url : "receiptListFilter.do",
				data : {
					"startDate" : start,
					"endDate" : end,
					"companyId" : $("#selectedCompany").val()
				},
				cache : false,
				async : true,
				dataType : "html",
				success : function(response) {
					$(".c-name").text(
							$("#selectedCompany option:selected").text()
									+ " ( " + start + " to " + end + " )");
					$("#panelBody").html(response);
					$("#load-erp").css("display", "none");
				},
				error : function(requestObject, error, errorThrown) {
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
					$("#load-erp").css("display", "none");
				}
			});
		}
	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}

/**
 * function to filter payment
 */
filterPayment = function() {
	var start = $("#paymentStart").val();
	var end = $("#paymentEnd").val();
	if (start.length > 0 && end.length > 0) {
		if (process(start) > process(end)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "GET",
				url : "paymentListFilter.do",
				data : {
					"startDate" : start,
					"endDate" : end,
					"companyId" : $("#selectedCompany").val()
				},
				cache : false,
				async : true,
				dataType : "html",
				success : function(response) {
					$(".c-name").text(
							$("#selectedCompany option:selected").text()
									+ " ( " + start + " to " + end + " )");
					$("#panelBody").html(response);
					$("#load-erp").css("display", "none");
				},
				error : function(requestObject, error, errorThrown) {
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
					$("#load-erp").css("display", "none");
				}
			});
		}
	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}

/**
 * function to filter journal
 */
filterJournal = function() {
	var start = $("#journalStart").val();
	var end = $("#journalEnd").val();
	if (start.length > 0 && end.length > 0) {
		if (process(start) > process(end)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "GET",
				url : "journalListFilter.do",
				data : {
					"startDate" : start,
					"endDate" : end,
					"companyId" : $("#selectedCompany").val()
				},
				cache : false,
				async : true,
				dataType : "html",
				success : function(response) {
					$(".c-name").text(
							$("#selectedCompany option:selected").text()
									+ " ( " + start + " to " + end + " )");
					$("#panelBody").html(response);
					$("#load-erp").css("display", "none");
				},
				error : function(requestObject, error, errorThrown) {
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
					$("#load-erp").css("display", "none");
				}
			});
		}
	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}

/**
 * function to filter trial balance
 */
filterTrialBalance = function() {
	var start = $("#trialBalanceStart").val();
	var end = $("#trialBalanceEnd").val();
	if (start.length > 0 && end.length > 0) {
		if (process(start) > process(end)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "GET",
				url : "trialBalanceTable.do",
				data : {
					"start" : start,
					"end" : end,
					"companyId" : $("#selectedCompany").val()
				},
				cache : false,
				async : true,
				dataType : "html",
				success : function(response) {
					$(".c-name").text(
							$("#selectedCompany option:selected").text()
									+ " ( " + start + " to " + end + " )");
					$("#panelBody").html(response);
					$("#load-erp").css("display", "none");
				},
				error : function(requestObject, error, errorThrown) {
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
					$("#load-erp").css("display", "none");
				}
			});
		}
	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}

filterProfitAndLoss = function() {
	var start = $("#p_lStart").val();
	var end = $("#p_lEnd").val();
	var companyId = $("#selectedCompany").val();
	if (start.length > 0 && end.length > 0) {
		if (process(start) > process(end)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "GET",
				url : "profitAndLossTable.do",
				data : {
					"start" : start,
					"end" : end,
					"companyId" : companyId
				},
				cache : false,
				async : true,
				dataType : "html",
				success : function(response) {
					$(".c-name").text(
							$("#selectedCompany option:selected").text()
									+ " ( " + start + " to " + end + " )");
					$("#panelBody").html(response);
					$("#load-erp").css("display", "none");
				},
				error : function(requestObject, error, errorThrown) {
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
					$("#load-erp").css("display", "none");
				}
			});
		}
	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}

function filterBalanceSheet() {
	var start = $("#b_sStart").val();
	var end = $("#b_sEnd").val();
	var companyId = $("#selectedCompany").val();
	if (start.length > 0 && end.length > 0) {
		if (process(start) > process(end)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "GET",
				url : "balanceSheetTable.do",
				data : {
					"start" : start,
					"end" : end,
					"companyId" : companyId
				},
				cache : false,
				async : true,
				dataType : "html",
				success : function(response) {
					$(".c-name").text(
							$("#selectedCompany option:selected").text()
									+ " ( " + start + " to " + end + " )");
					$("#panelBodySM").html(response);
					$("#load-erp").css("display", "none");
				},
				error : function(requestObject, error, errorThrown) {
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
					$("#load-erp").css("display", "none");
				}

			});
		}
	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}

/**
 * function to get payment history pdf
 */
function getPaymentHistoryPdf() {
	var start = $("#paymentStart").val();
	var end = $("#paymentEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'paymentHistoryPdf.do?startDate=' + start + '&endDate='
			+ end + "&companyId=" + $("#selectedCompany").val();
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}
/**
 * function to download receipt history pdf
 */
function getReceiptHistoryPdf() {
	var start = $("#receiptStart").val();
	var end = $("#receiptEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'receiptHistoryPdf.do?startDate=' + start + '&endDate='
			+ end + "&companyId=" + $("#selectedCompany").val();
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}
/**
 * function to download contra history pdf
 */
function getContraHistoryPdf() {
	var start = $("#contraStart").val();
	var end = $("#contraEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'contraHistoryReport.do?startDate=' + start + '&endDate='
			+ end + "&companyId=" + $("#selectedCompany").val();
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}
/**
 * function to download TrialBalance pdf
 */
function getTrialBalancePdf() {

	var stst = $("#slide_btn").is(":checked");
	var opn = $("#op_slide_btn").is(":checked");
	var cls = $("#cl_slide_btn").is(":checked");
	var start = $("#trialBalanceStart").val();
	var end = $("#trialBalanceEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'trialBalancePdf.do?startDate=' + start + '&endDate='
			+ end + '&DetAILed=' + stst + '&opn=' + opn + '&cls=' + cls
			+ "&companyId=" + $("#selectedCompany").val();
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}
/**
 * function to download ProfitAndLoss pdf
 */
function getProfitAndLossPdf() {
	var stst = $(".slide_btn_pl").is(":checked");
	var start = $("#p_lStart").val();
	var end = $("#p_lEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'profitAndLossPdf.do?startDate=' + start + '&endDate='
			+ end + '&DetAILed=' + stst + "&companyId="
			+ $("#selectedCompany").val();
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}
/**
 * function to download Balancesheet pdf
 */
function getBalancesheetPdf() {
	var stst = $("#slide_btn").is(":checked");
	var start = $("#b_sStart").val();
	var end = $("#b_sEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'balancesheetPdf.do?startDate=' + start + '&endDate='
			+ end + '&DetAILed=' + stst + "&companyId="
			+ $("#selectedCompany").val();
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}
/**
 * function to download journal history pdf
 */
function getJournalsHistoryPdf() {
	var start = $("#journalStart").val();
	var end = $("#journalEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'journalHistoryReport.do?startDate=' + start
			+ '&endDate=' + end + "&companyId=" + $("#selectedCompany").val();
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}

/**
 * function to change salary status
 * 
 * @param status
 */
function changeSalaryStatus(id, status) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "updateSalaryStatus.do",
		data : {
			"salaryId" : id,
			"statusId" : status
		},
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		success : function(response) {
			salaryList();
		},
		error : function(requestObject, error, errorThrown) {
			$(".notfctn-cntnnt").text(errorThrown);
			$("#success-msg").css("display", "block");
		}
	});
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}

function salaryList() {
	$.ajax({
		type : "GET",
		url : "salaryList.do",
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			$(".notfctn-cntnnt").text(errorThrown);
			$("#success-msg").css("display", "block");
		}
	});
}

function showPopAcc(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "paypop.do",
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		success : function(response) {
			$("#poCntnt").html(response);
			$("#salaryId").val(id);
		},
		error : function(requestObject, error, errorThrown) {
			$(".notfctn-cntnnt").text(errorThrown);
		}
	});
}

$(".back-btn").click(function() {
	$("#salaryId").val('');
	$("#load-erp").css("display", "none");
});

paySalary = function() {
	var salary = $("#salaryId").val();
	var ledger = $("#ledgerSel").val();
	if (salary.length > 0 && ledger.length > 0) {
		$.ajax({
			type : "GET",
			url : "paySalary.do",
			cache : false,
			async : true,
			data : {
				"salaryId" : salary,
				"ledgerId" : ledger
			},
			success : function(response) {
				location.reload();
			},
			error : function(requestObject, error, errorThrown) {
				$(".notfctn-cntnnt").text(errorThrown);
				$("#success-msg").css("display", "block");
			}
		});
		$("#salaryId").val('');
		$("#load-erp").css("display", "none");
	} else {
		alert("Please select an account.");
	}
}

/**
 * Amount fileds auto select
 */

$(".double").on("click", function() {

	$(this).select();
});

$("#slide_btn").click(function() {
	if(this.checked)
		$(".slideDownn").show();
	else
		$(".slideDownn").hide();
});

$(".slide_btn_pl").click(function(){
	if(this.checked)
		$(".slideDownnPl").show();
	else
		$(".slideDownnPl").hide();
});

$("#op_slide_btn").click(function() {
	$(".opSlide").toggle(600);
});
$("#cl_slide_btn").click(function() {
	$(".clSlide").toggle(600);
});
$("#ts_slide_btn").click(function() {
	$(".tsSlide").toggle(600);
});
function payPaySalary() {
	var checked = [];
	var unChecked = [];
	$(".payCheck").each(function(i, item) {
		if (this.checked) {
			checked.push(this.value);
		} else {
			unChecked.push(this.value);
		}
	});

	var date = $("#pySalDate").val();
	var account = $("#ledgerSel").val();
	var id = $("#payId").val();

	var vo = new Object();
	vo.paysalaryId = id;
	vo.ledgerId = account;
	vo.date = date;

	vo.checkedValues = checked;
	vo.unCheckedValues = unChecked;

	if (vo.checkedValues.length > 0 && account == "") {
		alert("Please Select An Account");
	} else {
		$.ajax({
			type : "POST",
			url : "paySalary.do",
			dataType : "text",
			cache : false,
			async : true,
			contentType : "application/json;charset=utf-8",
			data : JSON.stringify(vo),
			success : function(response) {
				if (response == "ok") {
					alert("Salary Payment Completed Successfully");
				} else {
					alert("Oops... Something Went Wrong");
				}
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			},
			complete : function() {
				location.reload();
			}

		});
	}
}

/**
 * function to filter account salary
 * 
 * @param id
 */
function filterAccountSalary(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "filterAccountSlary.do",
		dataType : "html",
		cache : false,
		async : true,
		data : {
			"companyId" : id
		},
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(reqObj, err, errThr) {
			alert(errThr);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}
function filterHrmsSalary(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "filterHrmsSalary.do",
		dataType : "html",
		cache : false,
		async : true,
		data : {
			"companyId" : id
		},
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(reqObj, err, errThr) {
			alert(errThr);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

$('#cmpTrDiv').on('click', function() {
	if ($('#selectedCompany option').length == 1) {
		filterTrialBalance();
	}
});
$('#cmpPlDiv').on('click', function() {
	if ($('#selectedCompany option').length == 1) {
		filterProfitAndLoss();
	}
});
$('#cmpBsDiv').on('click', function() {
	if ($('#selectedCompany option').length == 1) {
		filterBalanceSheet();
	}
});
$('#cmpRcDiv').on('click', function() {
	if ($('#selectedCompany option').length == 1) {
		filterReceipt();
	}
});
$('#cmpPyDiv').on('click', function() {
	if ($('#selectedCompany option').length == 1) {
		filterPayment();
	}
});
$('#cmpJrDiv').on('click', function() {
	if ($('#selectedCompany option').length == 1) {
		filterJournal();
	}
});
$('#cmpCnDiv').on('click', function() {
	if ($('#selectedCompany option').length == 1) {
		filterContra();
	}
});

function getLedgerViewPop(id, isGroup, startDate, endDate) {
	$("#load-erp").css("display", "block");
	var url = '';
	if (isGroup)
		url = 'ledgerGroupViewPop.do';
	else
		url = 'ledgerViewPop.do';
	$.ajax({
		type : "GET",
		url : url,
		data : {
			"id" : id,
			"startDate" : startDate,
			"endDate" : endDate
		},
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			$("#newPop").html(response);
			$("#newPop").show();
		},
		error : function(reqObj, err, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function getLedgerMonthlyViewPop(id, isFresh, startDate, endDate) {
	$("#load-erp").css("display", "block");
	var url = '';
	$.ajax({
		type : "GET",
		url : "ledgerViewPop.do",
		data : {
			"id" : id,
			"startDate" : startDate,
			"endDate" : endDate,
			"isFresh" : isFresh
		},
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			if (isFresh)
				$("#newPop").html(response);
			else
				$("#pop_body").html(response);
			$("#newPop").css("display", "block");
		},
		error : function(reqObj, err, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function getLedgerDetailedViewPop(id, startDate, endDate) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "ledgerDetailedViewPop.do",
		data : {
			"id" : id,
			"startDate" : startDate,
			"endDate" : endDate,
		},
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			$("#pop_body").html(response);
			$("#newPop").css("display", "block");
		},
		error : function(reqObj, err, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function getLedgerViewBack(id,type, startDate, endDate) {
	$("#load-erp").css("display", "block");
	var url = '';
	$.ajax({
		type : "GET",
		url : "ledgerViewBack.do",
		data : {
			"id" : id,
			"type" : type,
			"startDate" : startDate,
			"endDate" : endDate,
		},
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			$("#pop_body").html(response);
			$("#newPop").css("display", "block");
		},
		error : function(reqObj, err, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function loadLedgerVoucher(id,voucher,monthlyId){
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "ledgerVoucherViewPop.do",
		data : {
			"id" : id,
			"voucher" : voucher,
			"monthlyId":monthlyId
		},
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			$("#pop_body").html(response);
			$("#newPop").css("display", "block");
		},
		error : function(reqObj, err, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function getProfitAndLoss(startDate,endDate){
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "profitAndLossTable.do",
		data : {
			"start" : startDate,
			"end":endDate,
			"companyId" : $("#selectedCompany").val()
		},
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			$(".modal-body").html(response);
			$("#plPOp").css("display", "block");
		},
		error : function(reqObj, err, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function getStockSummary(startDate,endDate){
	$("#load-erp").css("display", "block");
	var vo=new Object();
	vo.companyId=$("#selectedCompany").val();
	vo.startDate=startDate;
	vo.endDate=endDate;
	$.ajax({
		type : "POST",
		url : "stockSummaryFilterTemplate.do",
		data :JSON.stringify(vo),
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			$("#SSPOpCntnt").html(response);
			$("#SSPOp").css("display", "block");
		},
		error : function(reqObj, err, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function closeMe() {
	$("#newPop").fadeOut(600);
}

function closeMePL(){
	$("#load-erp").css("display", "none");
	$("#plPOp").fadeOut(600);
	$("#SSPOp").fadeOut(600);
}

$('.blnc_shet_tbl tr').click(function(e) {
	var $link = $(this).find("a");
	if (e.target === $link[0])
		return false;
	$link.trigger('click');
	return false;
});