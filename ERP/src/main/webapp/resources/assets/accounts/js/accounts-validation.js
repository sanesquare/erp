/**
 * @author Shamsheer & Vinutha
 * @since 13-August-2015
 */
$(document).ready(function() {
	$(".ledgDate").on("change", function() {
		$("#ledger_frm").formValidation('revalidateField', 'startDate');
		$("#pymntFrm").formValidation('revalidateField', 'date');
		$("#rcptFrm").formValidation('revalidateField', 'date');
	});

	$(".jrnl-date").on("change", function() {
		$("#jrnl_frm").formValidation('revalidateField', 'date');
		$("#pymnt_frm_new").formValidation('revalidateField', 'date');
		$("#rcpt_frm_new").formValidation('revalidateField', 'date');
		$("#cntra_frm_new").formValidation('revalidateField', 'date');
	});
	$("#ledger_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			ledgerName : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Ledger Name is required'
					}
				}
			},
			ledgerGroupId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Ledger Group  is required'
					}
				}
			},
			openingBalance : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Opening Balance is required'
					}
				}
			}/*,
			startDate : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid. Please confirm with company period.',
						format : 'DD/MM/YYYY',
						min : 'startdate',
						max:'endDate'
					}
					
				}
			}*/
		}
	});
	/**
	 * payment form validation
	 */
	$("#pymntFrm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			date : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid. Please confirm with company period.',
						format : 'DD/MM/YYYY',
						min : 'startDate',
						max:'endDate'
					}
				}
			},
			byLedgerId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'From Account  is required'
					}
				}
			},
			toLedgerId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'To Account is required'
					}
				}
			},
			amount : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Amount is required'
					}
				}
			}
		}
	});
	
	/**
	 * receipt form validation
	 */
	$("#rcptFrm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			date : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid. Please confirm with company period.',
						format : 'DD/MM/YYYY',
						min : 'startDate',
						max:'endDate'
					}
				}
			},
			byLedgerId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'From Account  is required'
					}
				}
			},
			toLedgerId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'To Account is required'
					}
				}
			},
			amount : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Amount is required'
					}
				}
			}
		}
	});
	
	/**
	 * cotntra form validation
	 */
	$("#contraFrm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			date : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid. Please confirm with company period.',
						format : 'DD/MM/YYYY',
						min : 'startDate',
						max:'endDate'
					}
				}
			},
			byLedgerId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'From Account  is required'
					}
				}
			},
			toLedgerId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'To Account is required'
					}
				}
			},
			amount : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Amount is required'
					}
				}
			}
		}
	});
	
	$("#ledger-group-form").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			name : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : ' Name is required'
					}
				}
			},
			accountType : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : ' Account Type is required'
					}
				}
			}
		}
	});
	$("#jrnl_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			date : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid. Please confirm with company period.',
						format : 'DD/MM/YYYY',
						min : 'startDate',
						max:'endDate'
					}
				}
			}
		}
	});
	
	$("#pymnt_frm_new").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			date : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid. Please confirm with company period.',
						format : 'DD/MM/YYYY',
						min : 'startDate',
						max:'endDate'
					}
				}
			}
		}
	});
	
	$("#rcpt_frm_new").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			date : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid. Please confirm with company period.',
						format : 'DD/MM/YYYY',
						min : 'startDate',
						max:'endDate'
					}
				}
			}
		}
	});
	
	$("#cntra_frm_new").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			date : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid. Please confirm with company period.',
						format : 'DD/MM/YYYY',
						min : 'startDate',
						max:'endDate'
					}
				}
			}
		}
	});
});
