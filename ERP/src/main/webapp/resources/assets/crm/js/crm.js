/**
 * js file for crm
 * @since 30-July-2015
 */

function cnfrmDeleteLead(id) {
	$("#id-hid").val(id);
	$("#confirm-delete").css("display", "block");
}

deleteLead = function(result) {
	$("#load-erp ").css("display", "block");
	if (result == 'true') {
		$.ajax({
			type : "GET",
			url : "deleteLeads.do",
			dataType : "html",
			cache : false,
			async : true,
			data : {
				"id" : $("#id-hid").val()
			},
			success : function(response) {
				if (response == 'ok') {
					leadsList();
					$(".notfctn-cntnnt").text(
							"Vendor Details Deleted Successfully.");
					$("#success-msg").css("display", "block");
				} else {
					$(".notfctn-cntnnt").text(response);
					$("#erro-msg ").css("display", "block");
				}
			},
			error : function(requestObject, error, errorThrown) {
				$(".notfctn-cntnnt").text("Oops! Something went wrong!");
				$("#erro-msg ").css("display", "block");
			}
		});
	}
	$("#confirm-delete").css("display", "none");
	$("#load-erp ").css("display", "none");
}

leadsList = function() {
	$.ajax({
		type : "GET",
		url : "leadsList.do",
		cache : false,
		async : true,
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to save lead contact details
 */
$("#lead-cntct-sve-btn").click(function() {
	var vo = new Object();
	vo.leadId = $("#leadId").val();
	vo.contactId = $("#leadContactId").val();
	vo.name = $("#lead-name").val();
	vo.address = $("#lead-address").val();
	vo.workPhone = $("#lead-workPhone").val();
	vo.email = $("#lead-email").val();

	if (vo.name != "") {
		$.ajax({
			type : "POST",
			url : "leadsContactSave.do",
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(vo),
			async : true,
			cache : false,
			success : function(response) {
				clearLeadCotnctfrm();
				leadContactList();
				if (response == "ok") {
				}
			},
			error : function(requestObject, error, errorThrown) {
				clearLeadCotnctfrm();
				leadContactList();
			}
		});
		
	} else {
		$("#lead-name").css("border-color" ,"#A94442");
		$(".nme-requed").css("display","block");
		$("#lead-name").focus();
	}

});

/**
 * function to remove red marks
 */
$("#lead-name").on("keyup",function(){
	$("#lead-name").css("border-color" ,"#ccc");
	$(".nme-requed").css("display","none");
	$("#lead-name").css("    border", "1px solid #ccc");
});

/**
 * function to load lead contact list
 */
leadContactList = function() {
	$.ajax({
		type : "GET",
		url : "leadsContactList.do",
		cache : false,
		data : {
			"id" : $("#leadId").val()
		},
		async : true,
		success : function(response) {
			$("#lead_cntct_tbl").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to edit lead contact
 * @param id
 */
editThisLeadContact = function(id,name,email,address,phone){
	$("#leadContactId").val(id);
	$("#lead-name").val(name);
	$("#lead-email").val(email);
	$("#lead-address").val(address);
	$("#lead-workPhone").val(phone);
}

/**
 * function to delte leads contact
 * @param id
 */
deleteThisLeadContact =function(id){
	$.ajax({
		type : "GET",
		url : "deleteLeadsContact.do",
		cache : false,
		data : {
			"id" : id
		},
		async : true,
		success : function(response) {
			leadContactList();
			clearLeadCotnctfrm();
		},
		error : function(requestObject, error, errorThrown) {
			leadContactList();
			clearLeadCotnctfrm();
		}
	});
}

/**
 * function to clear lead contact form
 */
clearLeadCotnctfrm=function(){
	$("#leadContactId").val("");
	$("#lead-name").val("");
	$("#lead-email").val("");
	$("#lead-address").val("");
	$("#lead-workPhone").val("");
}