/**
 * @author Shamsheer & Vinutha
 * @since 08-August-2015
 */
$(document).ready(function() {
	$("#custom-frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			name : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			},
			address : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Address is required'
					}
				}
			},
			countryId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Country is required'
					}
				}
			},
			mobile : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Mobile is required'
					}
				}
			},
			email : {
				row : '.col-sm-9',
				validators : {
                     regexp: {
                         regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                         message: 'The value is not a valid email address'
                     },notEmpty : {
 						message : 'Email is required'
 					}
				}
			}
		}
	});
});
