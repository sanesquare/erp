/**
 * js file for sales
 * 
 * @since 30-July-2015
 */

$("#close-msg").click(function() {
	$(".notification-panl").fadeOut(600);
});
$('.double').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++) {
		a.push(i);
	}
	if (i = 46) {
		if ($(this).val().indexOf('.') == -1)
			a.push(i);
	}
	if (!(a.indexOf(k) >= 0))
		e.preventDefault();
	// alert($(this).val().indexOf('.'));
});
function closeMessage() {
	$(".notification-panl").fadeOut(600);
}

function cnfrmDelete(id) {
	$("#id-hid").val(id);
	$("#confirm-delete").css("display", "block");
}

$(".double").on("focus", function() {
	this.selected;
});

/**
 * function to close confirm-delete pop
 */
function cancel() {
	$("#confirm-delete").css("display", "none");
	$("#id-hid").val('');
}

deleteCustomer = function(result) {
	$("#load-erp ").css("display", "block");
	if (result == 'true') {
		$.ajax({
			type : "GET",
			url : "deleteCustomer.do",
			dataType : "html",
			cache : false,
			async : true,
			data : {
				"id" : $("#id-hid").val()
			},
			success : function(response) {
				if (response == 'ok') {
					customerList();
					$(".notfctn-cntnnt").text(
							"Customer Details Deleted Successfully.");
					$("#success-msg").css("display", "block");
				} else {
					$(".notfctn-cntnnt").text(response);
					$("#erro-msg ").css("display", "block");
					customerList();
				}
			},
			error : function(requestObject, error, errorThrown) {
				$(".notfctn-cntnnt").text("Oops! Something went wrong!");
				$("#erro-msg ").css("display", "block");
			}
		});
	}
	$("#confirm-delete").css("display", "none");
	$("#load-erp ").css("display", "none");
}

customerList = function() {
	$.ajax({
		type : "GET",
		url : "customerList.do",
		cache : false,
		async : true,
		success : function(response) {
			$("#panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function saveCustomer() {
	var vo = new Object();
	vo.customerContactId = $("#cnt_id").val();
	vo.name = $("#cnt_name").val();
	vo.email = $("#cnt_email").val();
	vo.phone = $("#cnt_phone").val();
	vo.address = $("#cnt_address").val();
	vo.customerId = $("#cusm_id").val();
	if (vo.name != "") {
		$.ajax({
			type : "POST",
			url : "saveCustomerContact.do",
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(vo),
			async : true,
			cache : false,
			success : function(response) {
				customerContactList();
				clearCustomerCntForm();

				if (response == "ok") {
					alert("ok");
				}
			},
			error : function(requestObject, error, errorThrown) {
				clearCustomerCntForm();
				customerContactList();
			}
		});
	} else {
		$("#cnt_name").css("border-color", "#A94442");
		$("#name_reqd").css("display", "block");
		$("#cnt_name").focus();
	}

}

$("#cnt_name").on("keyup", function() {
	$("#cnt_name").css("border-color", "#ccc");
	$("#name_reqd").css("display", "none");
	$("#cnt_name").css("    border", "1px solid #ccc");
});

clearCustomerCntForm = function() {
	$("#cnt_id").val("");
	$("#cnt_name").val("");
	$("#cnt_email").val("");
	$("#cnt_phone").val("");
	$("#cnt_address").val("");
}
customerContactList = function() {
	$.ajax({
		type : "GET",
		url : "customContacts.do",
		data : {
			"id" : $("#cusm_id").val()
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#customerCnts").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}
editCustomerContact = function(id, name, email, phone, address) {

	$("#cnt_id").val(id);
	$("#cnt_name").val(name);
	$("#cnt_email").val(email);
	$("#cnt_phone").val(phone);
	$("#cnt_address").val(address);

}

function deleteContact(id) {
	$.ajax({
		type : "GET",
		url : "deleteContacts.do",
		data : {
			"id" : id
		},
		cache : false,
		async : true,
		success : function(response) {
			clearCustomerCntForm();
			customerContactList();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

$(".double").on("blur", function() {
	if (this.value == "")
		this.value = "0.00";
});

function findNetAmount(id) {
	var quantity = $("#product_qty" + id).val();
	var deliveredQuantity = $("#delivered_prod" + id).val();
	var flag = true;
	if (parseFloat(quantity) < parseFloat(deliveredQuantity)) {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(
				"Oops...! " + deliveredQuantity
						+ " Items has been delivered for " + id);
		$("#product_qty" + id).val(deliveredQuantity);
		flag = false;
		findNetAmount(id);
	}
	var returnedQuantity = $("#product_qty_hid" + id).val();
	if (parseFloat(quantity) < parseFloat(returnedQuantity)) {
		flag = false;
		$("#product_qty" + id).val(returnedQuantity);
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(
				returnedQuantity + " Items Has Been Returned for " + id);
		findNetAmount(id);
	}
	if (flag) {
		var price = $("#product_prc" + id).val();
		var disc = $("#product_disc" + id).val();
		var tax = $("#product_tx" + id).val();
		if (tax != "") {
			var taxRate = tax.split('!!!');
			tax = taxRate[1];
		} else {
			tax = "0.00";
		}
		var net = parseFloat(quantity) * parseFloat(price);
		var discAmt = parseFloat(net) * parseFloat(disc) / 100;
		net = parseFloat(net) - parseFloat(discAmt);
		$("#prod_tot" + id).text(net.toFixed(2));
		var taxAmt = parseFloat(net) * parseFloat(tax) / 100;
		net = parseFloat(net) + parseFloat(taxAmt);
		$("#product_net" + id).text(net.toFixed(2));

		findTotalAmounts();
	}
}

getProductDetails = function() {
	var ids = [];
	var idsAndQuantities = [];
	$(".prodctId").each(function() {
		ids.push(this.value);
	});
	var vo = new Object();
	vo.ids = ids;
	vo.isSales = true;
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		dataType : "html",
		cache : false,
		data : JSON.stringify(vo),
		async : true,
		url : "productPopUp.do",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

closePdtpo = function() {
	$("#load-erp").css("display", "block");
	var ids = [];
	var idsAndQuantities = [];
	$("input[name=product]").each(function() {
		if (this.checked)
			idsAndQuantities.push(this.value);
	});
	var vo = new Object();
	vo.idsAndQuantities = idsAndQuantities;
	vo.isSales = true;
	$.ajax({
		type : 'POST',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		data : JSON.stringify(vo),
		url : "findProducts.do",
		success : function(response) {
			$(".prdct_tbl tr").remove();
			$(".prdct_tbl").append(response);
			$(".prdct_tbl tr").each(function() {
				var id = this.id;
				if (id != "") {
					$("#product_qty" + id).blur();
				}
			});
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
	$("#pdtPop").fadeOut("600");
}

cancelPdtpo = function() {
	$("#pdtPop").fadeOut("600");
}

function deleteProductRow(link) {
	var td = link.parentNode.parentNode;
	var row = td.parentNode;
	row.removeChild(td);
	findTotalAmounts();
}

/**
 * function to calculate amount
 */
function findTotalAmounts() {
	var net = 0;
	var discount = 0;
	var taxAmount = 0;
	var chargeTotal = 0;
	var netAmount = 0;
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var quantity = $("#product_qty" + id).val();
			var price = $("#product_prc" + id).val();
			var disc = $("#product_disc" + id).val();
			var tax = $("#product_tx" + id).val();

			if (tax != "") {
				var taxRate = tax.split('!!!');
				tax = taxRate[1];
			} else {
				tax = "0.00";
			}
			var thisNet = parseFloat(quantity) * parseFloat(price);
			var discAmt = parseFloat(thisNet) * parseFloat(disc) / 100;
			thisNet = parseFloat(thisNet) - parseFloat(discAmt);
			netAmount = parseFloat(netAmount) + parseFloat(thisNet);
			// $("#prod_tot"+id).text(thisNet.toFixed(2));
			var taxAmt = parseFloat(thisNet) * parseFloat(tax) / 100;
			thisNet = parseFloat(thisNet) + parseFloat(taxAmt);
			net = parseFloat(net) + parseFloat(thisNet);
			discount = parseFloat(discount) + parseFloat(discAmt);
			taxAmount = parseFloat(taxAmount) + parseFloat(taxAmt);
		}
	});

	$(".chrg_tbl tr").each(
			function() {
				var id = this.id;
				if (id != '') {
					chargeTotal = parseFloat(chargeTotal)
							+ parseFloat($("#chrge_price" + id).val());
				}
			});

	net = parseFloat(net) + parseFloat(chargeTotal);
	netAmount = parseFloat(netAmount) + parseFloat(chargeTotal);
	$("#netAmountHidden").val(netAmount.toFixed(2));
	$("#subTotal").text(net.toFixed(2));
	$("#disTotal").text(discount.toFixed(2));
	$("#taxAmountHidden").val(taxAmount.toFixed(2));
	$("#netTotal").text(net.toFixed(2));
	// var grand = parseFloat(net) + parseFloat(taxAmount);
	// $("#grandTotal").text(grand.toFixed(2));
	selectedTaxes();
	getInstallmentsForInvoice();
}

function selectedTaxes() {
	var subTotal = $("#subTotal").text();
	var discountRate = $("#discRate").val();
	if (discountRate.length == 0)
		discountRate = 0;
	var discAmount = parseFloat(subTotal) * parseFloat(discountRate) / 100;
	$("#disTotal").text(discAmount.toFixed(2));
	var net = parseFloat(subTotal) - parseFloat(discAmount);
	net = net.toFixed(2);
	$("#netTotal").text(net);
	var taxes = $("#taxesMulti").val();
	var totalTax = 0;
	if (taxes != null) {
		$.each(taxes, function(i, it) {
			var rate = it.split("!!!");
			totalTax = parseFloat(totalTax) + parseFloat(rate[1]);
		});
	}
	totalTax = totalTax.toFixed(2);
	var taxAmount = 0;
	if (totalTax != 0) {
		taxAmount = parseFloat(net) * parseFloat(totalTax) / 100;
		taxAmount = taxAmount.toFixed(2);
		$("#taxTotal").text(taxAmount);
		$("#grandTotal").text(parseFloat(net) + parseFloat(taxAmount));
	} else {
		$("#taxTotal").text(0.00);
		$("#grandTotal").text(net);
	}
}

/**
 * function to save customer quote
 */
function saveCustomerQuote() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	var proTax = $("#taxAmountHidden").val();
	var totTax = $("#taxTotal").text();
	var taxes = $("#taxesMulti").val();
	if (proTax != '0.00' && taxes != null) {
		msg = "Please Select Any one tax...";
		flag = false;
	} else if ($("#bill_vendr").val().length == 0) {
		msg = "Please Select Customer...";
		flag = false;
	} else if ($("#vqte_expiry").val().length == 0) {
		msg = "Please Select Expiry Date...";
		flag = false;
	}

	var tax = parseFloat(proTax) + parseFloat(totTax);
	var vo = new Object();
	var productVos = [];
	vo.orderId = $("#vendorQuoteId").val();
	vo.expiryDate = $("#vqte_expiry").val();
	vo.customerId = $("#bill_vendr").val();
	vo.orderDate = $("#bill-date").val();
	vo.paymentTermId = $("#paymentTermSelect").val();
	vo.deliveryTermId = $("#deliveryTermSelect").val();
	vo.erpCurrencyId = $("#currencySelBill").val();

	var taxIds = [];
	if (taxes != null) {
		$.each(taxes, function(i, it) {
			var rate = it.split("!!!");
			taxIds.push(rate[0]);
		});
	}
	vo.tsxIds = taxIds;
	vo.subTotal = $("#subTotal").text();
	vo.discountTotal = $("#disTotal").text();
	vo.netTotal = $("#netTotal").text();
	vo.taxTotal = $("#taxTotal").text();
	vo.grandTotal = $("#grandTotal").text();
	vo.discountRate = $("#discRate").val();
	vo.netAmountHidden = $("#netAmountHidden").val();
	vo.productTaxTotal = tax.toFixed(2);
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.unitPrice = $("#product_prc" + id).val();
			product.discount = $("#product_disc" + id).val();
			product.netAmount = $("#product_net" + id).text();
			product.quoteProductId = $("#itemId" + id).val();
			product.amountExcludingTax = $("#prod_tot" + id).text();
			var tax = $("#product_tx" + id).val().split("!!!");
			product.taxId = tax[0];
			productVos.push(product);
		}
	});
	if (productVos.length == 0) {
		msg = "Please Select Products...";
		flag = false;
	}
	vo.productVos = productVos;
	var json = JSON.stringify(vo);
	if (flag) {
		$
				.ajax({
					type : "POST",
					url : "saveCustomerQoutes.do",
					cache : false,
					async : true,
					data : json,
					dataType : "json",
					contentType : "application/json;charset=utf-8",
					success : function(response) {
						if (response == 0) {
							$("#errror-msg").css("display", "block");
							$(".modal-body").text(
									"Oops...! Something went wrong.");
							$("#load-erp").css("display", "none");
						} else {
							var currentUrl = location.href;
							pageurl = currentUrl
									.replace(
											'addCustomerQuote.do',
											"viewCustomerQuote.do?id="
													+ response
													+ "&msg=Customer Quotation Saved Successfully");
							window.history.pushState({
								path : pageurl
							}, '', pageurl);
							location.reload();
						}
						$("#load-erp").css("display", "none");
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
						$("#load-erp").css("display", "none");
					}
				});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}

/**
 * function to save purchase order
 */
function saveSalesOrder() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	var proTax = $("#taxAmountHidden").val();
	var totTax = $("#taxTotal").text();
	var taxes = $("#taxesMulti").val();
	if (proTax != '0.00' && taxes != null) {
		msg = "Please Select Any one tax...";
		flag = false;
	} else if ($("#bill_vendr").val().length == 0) {
		msg = "Please Select Customer...";
		flag = false;
	}

	var tax = parseFloat(proTax) + parseFloat(totTax);
	var vo = new Object();
	var chargeVos = [];
	var productVos = [];
	vo.orderId = $("#purchaseOrderId").val();
	vo.customerId = $("#bill_vendr").val();
	vo.orderDate = $("#bill-date").val();
	vo.paymentTermId = $("#paymentTermSelect").val();
	vo.deliveryTermId = $("#deliveryTermSelect").val();
	vo.erpCurrencyId = $("#currencySelBill").val();

	var taxIds = [];
	if (taxes != null) {
		$.each(taxes, function(i, it) {
			var rate = it.split("!!!");
			taxIds.push(rate[0]);
		});
	}
	vo.tsxIds = taxIds;
	vo.subTotal = $("#subTotal").text();
	vo.discountTotal = $("#disTotal").text();
	vo.netTotal = $("#netTotal").text();
	vo.taxTotal = $("#taxTotal").text();
	vo.grandTotal = $("#grandTotal").text();
	vo.discountRate = $("#discRate").val();
	vo.netAmountHidden = $("#netAmountHidden").val();
	vo.productTaxTotal = tax.toFixed(2);
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.unitPrice = $("#product_prc" + id).val();
			product.discount = $("#product_disc" + id).val();
			product.netAmount = $("#product_net" + id).text();
			product.quoteProductId = $("#itemId" + id).val();
			product.amountExcludingTax = $("#prod_tot" + id).text();
			var tax = $("#product_tx" + id).val().split("!!!");
			product.taxId = tax[0];
			productVos.push(product);
		}
	});

	$(".chrg_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var charge = new Object();
			charge.chargeId = id;
			charge.amount = $("#chrge_price" + id).val();
			chargebillChargeId = $("#billChargeId" + id).val();
			chargeVos.push(charge);
		}
	});
	vo.chargeVos = chargeVos;

	if (productVos.length == 0) {
		msg = "Please Select Products...";
		flag = false;
	}
	vo.productVos = productVos;
	var json = JSON.stringify(vo);
	console.log(json);
	if (flag) {
		$.ajax({
			type : "POST",
			url : "saveSalesOrder.do",
			cache : false,
			async : true,
			data : json,
			dataType : "json",
			contentType : "application/json;charset=utf-8",
			success : function(response) {
				if (response == 0) {
					$("#errror-msg").css("display", "block");
					$(".modal-body").text("Oops...! Something went wrong.");
					$("#load-erp").css("display", "none");
				} else {
					var currentUrl = location.href;
					pageurl = currentUrl.replace('addSalesOrder.do',
							"viewSalesOrder.do?id=" + response
									+ "&msg=Sales Order Saved Successfully");
					window.history.pushState({
						path : pageurl
					}, '', pageurl);
					location.reload();
				}
				$("#load-erp").css("display", "none");
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}
/**
 * function to convert customer quote to order
 * 
 * @param id
 */
function convertCustomerQuote(id) {
	$("#confirm_convert").css("display", "block");
	$("#id-hidd").val(id);
}

function confirmedConversion() {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "convertCustomerQouteToOrder.do",
		dataType : "html",
		cache : false,
		async : true,
		data : {
			"id" : $("#id-hidd").val()
		},
		success : function(response) {
			$("#panelBody").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
	cancelConfirm();
}

function cancelConfirm() {
	$("#confirm_convert").css("display", "none");
	$("#id-hidd").val("");
}

/**
 * function to confirm delete
 */
function deleteItem() {
	var item = $("#delete-item").val();
	if (item == 'cq')
		deleteCQ($("#id-hid").val());
	else if (item == 'so')
		deleteSO($("#id-hid").val());
	else if (item == 'invoice')
		deleteInvoice($("#id-hid").val());
	else if (item == 'sr')
		deleteSR($("#id-hid").val());
	$("#confirm-delete").css("display", "none");
}

function deleteSalesReturn(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('sr');
	$("#id-hid").val(id);
}

function deleteSR(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deleteSalesReturn.do",
		cache : false,
		async : true,
		dataType : "html",
		data : {
			"id" : id
		},
		success : function(response) {
			$("#panelBody").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
}

/**
 * function to delete customer quote
 * 
 * @param id
 */
function deleteCustomerQuote(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('cq');
	$("#id-hid").val(id);

}
function deleteCQ(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deleteCustomerQuote.do",
		cache : false,
		async : true,
		dataType : "html",
		data : {
			"id" : id
		},
		success : function(response) {
			$("#panelBody").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
}

/**
 * function to delete sales order
 * 
 * @param id
 */
function deleteSalesOrder(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('so');
	$("#id-hid").val(id);

}
function deleteSO(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deleteSalesOrder.do",
		cache : false,
		async : true,
		dataType : "html",
		data : {
			"id" : id
		},
		success : function(response) {
			$("#panelBody").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
}

/**
 * function to save bill
 */
function saveInvoice() {

	$("#load-erp").css("display", "block");

	var msg = '';
	var flag = true;
	var proTax = $("#taxAmountHidden").val();
	var totTax = $("#taxTotal").text();
	var taxes = $("#taxesMulti").val();
	if (proTax != '0.00' && taxes != null) {
		msg = "Please Select Any one tax...";
		flag = false;
	} else if ($("#bill_vendr").val().length == 0) {
		msg = "Please Select Customer...";
		flag = false;
	} else if ($("#bill_ledgr").val().length == 0) {
		msg = "Please Select Account...";
		flag = false;
	} else if ($("#paymentTermSelect").val().length == 0) {
		msg = "Please Select Payment Term...";
		flag = false;
	}

	var tax = parseFloat(proTax) + parseFloat(totTax);
	var vo = new Object();
	var productVos = [];
	var chargeVos = [];
	var installmentVos = [];

	vo.deliveryNoteIds = $("#deliveryNoteIds").val().replace(/[[\]]/g, '')
			.split(',');
	vo.invoiceId = $("#invoiceId").val();
	vo.customerId = $("#bill_vendr").val();
	vo.invoiceDate = $("#bill-date").val();
	vo.ledgerId = $("#bill_ledgr").val();
	vo.isSettled = $("#bill_sts").val();
	vo.paymentTermId = $("#paymentTermSelect").val();
	vo.deliveryTermId = $("#deliveryTermSelect").val();
	vo.erpCurrencyId = $("#currencySelBill").val();

	var taxIds = [];
	if (taxes != null) {
		$.each(taxes, function(i, it) {
			var rate = it.split("!!!");
			taxIds.push(rate[0]);
		});
	}
	vo.tsxIds = taxIds;
	vo.subTotal = $("#subTotal").text();
	vo.discountTotal = $("#disTotal").text();
	vo.netTotal = $("#netTotal").text();
	vo.taxTotal = $("#taxTotal").text();
	vo.grandTotal = $("#grandTotal").text();
	vo.discountRate = $("#discRate").val();
	vo.netAmountHidden = $("#netAmountHidden").val();
	vo.productTaxTotal = tax.toFixed(2);
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.unitPrice = $("#product_prc" + id).val();
			product.discount = $("#product_disc" + id).val();
			product.netAmount = $("#product_net" + id).text();
			product.billProductId = $("#itemId" + id).val();
			product.godownId = $("#godownId" + id).val();
			product.amountExcludingTax = $("#prod_tot" + id).text();
			var tax = $("#product_tx" + id).val().split("!!!");
			product.taxId = tax[0];
			productVos.push(product);
		}
	});
	if (productVos.length == 0) {
		msg = "Please Select Product...";
		flag = false;
	}
	vo.productVos = productVos;

	/**
	 * iterating charges
	 */
	$(".chrg_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var charge = new Object();
			charge.chargeId = id;
			charge.amount = $("#chrge_price" + id).val();
			chargebillChargeId = $("#billChargeId" + id).val();
			chargeVos.push(charge);
		}
	});
	vo.chargeVos = chargeVos;

	/**
	 * setting installments
	 */
	$(".instllmnt_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var installment = new Object();
			installment.date = $("#instlmnt_date" + id).text();
			installment.percentage = $("#instlmnt_perc" + id).text();
			installment.amount = $("#instlmnt_amount" + id).text();
			installment.isPaid = $("#instlmnt_stts" + id).val();
			installment.installmentId = $("#" + id).val()
			installmentVos.push(installment);
		}
	});
	vo.installmentVos = installmentVos;
	var json = JSON.stringify(vo);
	if (flag) {
		$.ajax({
			type : "POST",
			url : "saveInvoice.do",
			cache : false,
			async : true,
			data : json,
			dataType : "json",
			contentType : "application/json;charset=utf-8",
			success : function(response) {
				if (response == 0) {
					$("#errror-msg").css("display", "block");
					$(".modal-body").text("Oops...! Something went wrong.");
					$("#load-erp").css("display", "none");
				} else {
					var currentUrl = location.href;
					if (vo.deliveryNoteIds == '') {
						pageurl = currentUrl.replace('invoiceAdd.do',
								"viewInvoice.do?id=" + response
										+ "&msg=Invoice Saved Successfully");
					} else {
						pageurl = currentUrl.replace('convertDeliveryNotToInvoice.do',
								"viewInvoice.do?id=" + response
										+ "&msg=Delivery Note Converted to Invoice Successfully");
					}
					window.history.pushState({
						path : pageurl
					}, '', pageurl);
					location.reload();
				}
				$("#load-erp").css("display", "none");
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}

/**
 * function to display charge pop up
 */
getChargeDetails = function() {
	var ids = [];
	$(".chargeId").each(function() {
		ids.push(this.value);
	});
	var vo = new Object();
	vo.ids = ids;
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		dataType : "html",
		cache : false,
		data : JSON.stringify(vo),
		async : true,
		url : "chargePopUp.do",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to select charges
 */
function selectCharges() {
	$("#load-erp").css("display", "block");
	var ids = [];
	$("input[name=charge]").each(function() {
		if (this.checked)
			ids.push(this.value);
	});
	var vo = new Object();
	vo.ids = ids;
	$.ajax({
		type : 'POST',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		data : JSON.stringify(vo),
		url : "findCharges.do",
		success : function(response) {
			$(".chrg_tbl tr:last").remove();
			$(".chrg_tbl").append(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
	$("#pdtPop").fadeOut("600");
}

/**
 * function to close charge pop up
 */
function cancelChargePOp() {
	$("#pdtPop").fadeOut("600");
}

/**
 * function to delete charge row
 * 
 * @param link
 */
function deleteChargeRow(link) {
	var td = link.parentNode.parentNode;
	var row = td.parentNode;
	row.removeChild(td);
	findTotalAmounts();
}

$('.chrgTbl tr').click(function(event) {
	if (event.target.type !== 'checkbox') {
		$(':checkbox', this).trigger('click');
	}
});

$('.proTable tr').click(function(event) {
	if (event.target.type !== 'checkbox') {
		$(':checkbox', this).trigger('click');
	}
});

/**
 * function to get installment table for invoice
 */
function getInstallmentsForInvoice() {
	var id = $("#paymentTermSelect").val();
	if (id != '') {
		$("#spinr").addClass("fa-spin");
		$.ajax({
			type : "GET",
			url : "getInstallmentsForInvoice.do",
			cache : false,
			async : true,
			data : {
				"date" : $("#bill-date").val(),
				"amount" : $('#grandTotal').text(),
				"paymentTermId" : id
			},
			dataType : "html",
			contentType : "application/json;charset=utf-8",
			success : function(response) {
				$("#installment_table_bill").html(response);
				findNetAmount();
				$("#spinr").removeClass("fa-spin");
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#spinr").removeClass("fa-spin");
			}
		});
	}
}

/**
 * function to pay bill installment
 * 
 * @param value
 * @param id
 */
receiveBillInstallment = function(value, id) {
	if (value == "true") {
		$("#id-hid").val(id);
		$("#confirm-delete").css("display", "block");
	}
}

/**
 * function to receive Invoice installment
 */
confirmedInvoiceReceipt = function() {
	$("#load-erp").css("display", "block");
	var id = $("#id-hid").val();
	$("#confirm-delete").css("display", "none");
	$("#id-hid").val('');

	$.ajax({
		type : "GET",
		url : "payInvoiceInstallment.do",
		cache : false,
		async : true,
		contentType : "application/json;charset=utf-8",
		dataType : "json",
		data : {
			"id" : id
		},
		success : function(response) {
			var msg = "Something Went Wrong.";
			if (response)
				msg = "Installment Received Successfully.";
			setTimeout(function() {
				var currentUrl = location.href;
				var splitted = currentUrl.split("&msg");
				currentUrl = splitted[0] + "&msg=" + msg;
				window.history.pushState({
					path : currentUrl
				}, '', currentUrl);
				location.reload();
			}, 1000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");

		}
	});

}

function deleteThisInvoice(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('invoice');
	$("#id-hid").val(id);

}
function deleteInvoice(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deleteInvoice.do",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			$("#success-msg").css("display", "block");
			$(".notification-panl").css("display", "block");
			$(".notfctn-cntnnt").text(response);
			loadInvoiceTable();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}
loadInvoiceTable = function() {
	$.ajax({
		type : "GET",
		url : 'invoiceList.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#bill_panelBody").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}

function findNetAmountSR(id) {
	var quantity = $("#product_qty" + id).val();
	var quantityToReturn = $("#product_qty_hid" + id).val();
	var flag = true;
	if (parseFloat(quantity) > parseFloat(quantityToReturn)) {
		flag = false;
		$("#product_qty" + id).val(quantityToReturn);
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(
				quantityToReturn + " Items can be Returned for " + id);
		findNetAmountSR(id);
	}
	if (flag) {
		var price = $("#product_prc" + id).val();
		var net = parseFloat(quantity) * parseFloat(price);
		$("#product_net" + id).text(net.toFixed(2));
	}
}

function saveSalesReturn() {
	$("#load-erp").css("display", "block");
	var msg = '';
	var flag = true;
	var vo = new Object();
	var productVos = [];
	vo.invoiceId = $("#invoiceId").val();
	vo.date = $("#invoice-date").val();
	vo.returnId = $("#returnId").val();
	/**
	 * iterating products
	 */
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var product = new Object();
			product.productCode = $("#prod_code" + id).text();
			product.productName = $("#prod_name" + id).text();
			product.quantity = $("#product_qty" + id).val();
			product.unitPrice = $("#product_prc" + id).val();
			product.total = $("#product_net" + id).text();
			product.godownId = $("#prod_godwn_id" + id).val();
			productVos.push(product);
		}
	});
	if (productVos.length == 0) {
		msg = "Please Select Product...";
		flag = false;
	}
	vo.productVos = productVos;

	var json = JSON.stringify(vo);
	if (flag) {
		$.ajax({
			type : "POST",
			url : "saveSalesReturn.do",
			cache : false,
			async : true,
			data : json,
			dataType : "json",
			contentType : "application/json;charset=utf-8",
			success : function(response) {
				if (response == 0) {
					$("#errror-msg").css("display", "block");
					$(".modal-body").text("Oops...! Something went wrong.");
					$("#load-erp").css("display", "none");
				} else {
					var currentUrl = location.href;
					pageurl = currentUrl.replace("returnThisInvoice.do",
							"viewSalesReturn.do?id=" + response
									+ "&msg=Sales Return Saved Successfully");
					window.history.pushState({
						path : pageurl
					}, '', pageurl);
					location.reload();
				}
				$("#load-erp").css("display", "none");
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}
}

addQtyToId = function(id, value) {
	var oldVal = $("." + id).val();
	var split = oldVal.split("!!!");
	$("." + id).val(split[0] + "!!!" + value);
}