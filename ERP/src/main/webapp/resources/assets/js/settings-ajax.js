/***
 * @author: Jithin Mohan
 */

/**
 * method to split string and iterate
 */
splitString = function(value, thisFunction) {
	var res = value.split(",");
	var listHtml = "";
	$.each(res, function(key, value) {
		listHtml = listHtml + "<span class='badge bg-info available_section "
				+ value + "'><span onclick='updateItem(\"" + value + "\",\""
				+ thisFunction + "\")'>" + value
				+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
				+ value + "\",\"" + thisFunction + "\")'></i></span>";
	});
	return listHtml;
}
/**
 * 
 * @param item
 * @param thisFunction
 */
updateAsset = function(code, item) {
	$("#assetNameVal").val(item);
	$("#assetCodeVal").val(code);
	$("#tempAssetCode").val(code);
	$("." + code + "").remove();
}
updateItem = function(item, thisFunction) {
	if (thisFunction == "language") {
		$("#language-val").val(item);
		$("#tempLanguage").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "skill") {
		$("#skillNameVal").val(item);
		$("#tempSkill").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "qualificationDegree") {
		$("#degreeNameVal").val(item);
		$("#tempQualification").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "contractType") {
		$("#contractTypeVal").val(item);
		$("#tempType").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "jobType") {
		$("#jobTypeVal").val(item);
		$("#tempJobType").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "jobField") {
		$("#jobFieldVal").val(item);
		$("#tempJobField").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "branchType") {
		$("#branchTypeNameVal").val(item);
		$("#tempBranchType").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "policyType") {
		$("#policyTypeVal").val(item);
		$("#tempPolicyType").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "employeeType") {
		$("#employeeTypeVal").val(item);
		$("#tempEmployeeType").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "employeeCategory") {
		$("#employeeCategoryVal").val(item);
		$("#tempEmployeeCategory").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "reimbursementCategory") {
		$("#reimbursementCategoryVal").val(item);
		$("#tempReimbursementCategory").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "country") {
		$("#countryName").val(item);
		$("#tempName").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "accountType") {
		$("#accountTypeVal").val(item);
		$("#tempAccountType").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "workShift") {
		$("#workShiftVal").val(item);
		$("#tempWorkShift").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "employeeStatus") {
		$("#employeeStatus").val(item);
		$("#tempEmployeeStatus").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "leaveType") {
		$("#leaveTypeVal").val(item);
		$("#tempLeaveType").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "reminderStatus") {
		$("#reminderStatus").val(item);
		$("#tempStatus").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "employeeGrade") {
		$("#employeeGrade").val(item);
		$("#tempGrade").val(item);
		$("." + item + "").remove();
	} else if (thisFunction == "advanceSalaryStatus") {
		$("#advanceSalaryStatus").val(item);
		$("#tempStatus").val(item);
		$("." + item + "").remove();
	}
	/*
	 * if (thisFunction == "policyType") { deletePolicyType(item); } else if
	 * (thisFunction == "language") { deleteLanguage(item); } else if
	 * (thisFunction == "skill") { deleteSkill(item); } else if (thisFunction ==
	 * "qualificationDegree") { deleteQualificationDegree(item); } else if
	 * (thisFunction == "contractType") { deleteContractType(item); } else if
	 * (thisFunction == "jobType") { deleteJobType(item); } else if
	 * (thisFunction == "jobField") { deleteJobField(item); } else if
	 * (thisFunction == "branchType") { deleteBranchType(item); } else if
	 * (thisFunction == "employeeType") { deleteEmployeeType(item); } else if
	 * (thisFunction == "employeeCategory") { deleteEmployeeCategory(item); }
	 * else if (thisFunction == "reimbursementCategory") {
	 * deleteReimbursementCategory(item); } else if (thisFunction == "country") {
	 * deleteCountry(item); } else if (thisFunction == "accountType") {
	 * deleteAccountType(item); } else if (thisFunction == "workShift") {
	 * deleteWorkShift(item); } else if (thisFunction == "employeeStatus") {
	 * deleteEmployeeStatus(item); } else if (thisFunction ==
	 * "terminationStatus") { deleteTerminationStatus(item); } else if
	 * (thisFunction == "leaveType") { deleteLeaveType(item); } else if
	 * (thisFunction == "reminderStatus") { deleteReminderStatus(item); } else
	 * if (thisFunction == "employeeGrade") { deleteEmployeeGrade(item); } else
	 * if (thisFunction == "advanceSalaryStatus") {
	 * deleteAdvanceSalaryStatus(item); } else if (thisFunction ==
	 * "overtimeStatus") { deleteOvertimeStatus(item); } else if (thisFunction ==
	 * "loanStatus") { deleteLoanStatus(item); }
	 */
}
/**
 * auto hide
 */
runEffect = function(div) {
	setTimeout(function() {
		$("#" + div).fadeOut(500);
	}, 3000);
}
/**
 * Disable enter key in textarea
 */
$(
		'#language-val, #skillNameVal, #degreeNameVal, #contractTypeVal, #jobTypeVal, #jobFieldVal, #branchTypeNameVal, #policyTypeVal, #employeeTypeVal, #employeeCategoryVal, #reimbursementCategoryVal, #countryName, #accountTypeVal, #workShiftVal, #employeeStatus, #terminationStatus, #leaveTypeVal, #reminderStatus, #employeeGrade, #advanceSalaryStatus, #overtimeStatus, #loanStatus')
		.keypress(function(event) {
			if (event.keyCode == 10 || event.keyCode == 13)
				event.preventDefault();
		});

/**
 * method to delete asset code
 * 
 * @param assetCode
 */
deleteAssetItem = function(assetCode) {
	$
			.ajax({
				type : "POST",
				url : "DeleteOrganizationAsset.do",
				cache : false,
				async : true,
				data : {
					"assetCode" : assetCode
				},
				success : function(response) {
					$("#setg-ast-pp-msg").removeClass("hideMe");
					$("#setg-ast-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-ast-pp-msg").html(
								"Asset has been successfully deleted.");
						getAllAvailableAssets();
					} else {
						$("#setg-ast-pp-msg")
								.html(
										"You can not delete this asset, because it is already used in some records.");
					}
					runEffect("setg-ast-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/**
 * method to delete employee designation
 * 
 * @param designationRank
 */
deleteEmployeeDesignation = function(designationRank, designation) {
	$
			.ajax({
				type : "POST",
				url : "DeleteEmployeeDesignation.do",
				cache : false,
				async : true,
				data : {
					"designationRank" : designationRank,
					"designation" : designation
				},
				success : function(response) {
					$("#setg-ed-pp-msg").removeClass("hideMe");
					$("#setg-ed-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-ed-pp-msg")
								.html(
										"Employee designation has been successfully deleted.");
						getAllEmployeeDesignation();
					} else {
						$("#setg-ed-pp-msg")
								.html(
										"You can not delete this designation, because it is already used in some records.");
					}
					runEffect("setg-ed-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/**
 * Section for get all available organization assets
 */
getAllAvailableAssets = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllOrganizationAssets.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var assetsLength = response.result.length;
						if (assetsLength > 0) {
							for (var i = 0; i < assetsLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].assetCode
										+ "'><span onclick='updateAsset(\""
										+ response.result[i].assetCode
										+ "\",\""
										+ response.result[i].assetName
										+ "\")'>"
										+ response.result[i].assetName
										+ "( "
										+ response.result[i].assetCode
										+ " )"
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteAssetItem(\""
										+ response.result[i].assetCode
										+ "\")'></i></span>";
							}
						}
					}
					$("#avai-l-assets").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/**
 * Section for get all available languages
 */
getAllAvailableLanguage = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableLanguages.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var languageLength = response.result.length;
						if (languageLength > 0) {
							for (var i = 0; i < languageLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].language
										+ "'><span onclick='updateItem(\""
										+ response.result[i].language
										+ "\",\"language\")'>"
										+ response.result[i].language
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].language
										+ "\",\"language\")'></i></span>";
							}
						}
					}
					$("#avai-l-anguage").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/*******************************************************************************
 * Section for new commission titles
 */
abc=function(){
	alert("ssss");
}
newLanguage = function() {
	$('#StLanFrm')
			.submit(
					function(event) {
						if ($("#language-val").val().length > 0) {
							var languageVo = new Object();
							languageVo.language = $("#language-val").val();
							languageVo.tempLanguage = $("#tempLanguage").val();
							$("#language-val").val('');
							$("#setg-pp-msg").removeClass("hideMe");
							$("#setg-pp-msg").fadeIn();
							$
									.ajax({
										type : 'POST',
										url : 'SaveLanguage.do',
										dataType : "json",
										data : JSON.stringify(languageVo),
										contentType : "application/json; charset=utf-8",
										accept : 'application/json',
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-anguage")
														.append(
																splitString(
																		languageVo.language,
																		'language'));
												$("#language-val").val('');
												$("#setg-pp-msg")
														.html(
																"Language(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-pp-msg");
						}
						event.preventDefault();
					});
};

$("#commissn_title_sve")
		.click(
				function() {
					if ($("#commission_title_textre").val() != "") {
						var commissionTitleVo = new Object();
						commissionTitleVo.title = $("#commission_title_textre")
								.val();
						$("#cmmission_title_msg").removeClass("hideMe");
						$("#cmmission_title_msg").fadeIn();
						$
								.ajax({
									type : "POST",
									url : "saveCommissionTitle.do",
									dataType : "json",
									contentType : "application/json; charset=utf-8",
									data : JSON.stringify(commissionTitleVo),
									cache : false,
									async : true,
									success : function(response) {
										if (response.status == "SUCCESS") {
											$("#avai_commission_titles")
													.append(
															splitString($(
																	"#commission_title_textre")
																	.val()));
											$("#commission_title_textre").val(
													'');
											$("#cmmission_title_msg")
													.html(
															"Commission title(s) has been successfully saved.");
											getAvailableCommissionTitles();
										} else if (response.status == "DUPLICATE") {
											$("#cmmission_title_msg").html(
													"Duplicate item found.");
										}
									},
									error : function(requestObject, error,
											errorThrown) {
										$("#cmmission_title_msg").html(
												"Oops!! Something went wrong.");
									}
								});
					}
					runEffect("cmmission_title_msg");
				});
/**
 * method to list all availabel commission
 */
getAvailableCommissionTitles = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableCommissionTitles.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var languageLength = response.result.length;
						if (languageLength > 0) {
							$("#commission_title option").remove();
							var options = "";
							for (var i = 0; i < languageLength; i++) {
								options += "<option value='"
										+ response.result[i].id + "'>"
										+ response.result[i].title
										+ "</option>";
								listHtml = listHtml
										+ "<span class='badge bg-info available_section'>"
										+ response.result[i].title + "</span>";
							}
							$("#commission_title").append(options);
						}
					}
					$("#avai_commission_titles").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/*******************************************************************************
 * Section for new deduction titles
 */
$("#deduction_title_btn")
		.click(
				function() {
					if ($("#deduction_title_txt").val() != "") {
						var deductionTitleVo = new Object();
						deductionTitleVo.title = $("#deduction_title_txt")
								.val();
						$("#deduction_title_msg").removeClass("hideMe");
						$("#deduction_title_msg").fadeIn();
						$
								.ajax({
									type : "POST",
									url : "saveDeductionTitle.do",
									dataType : "json",
									contentType : "application/json; charset=utf-8",
									data : JSON.stringify(deductionTitleVo),
									cache : false,
									async : true,
									success : function(response) {
										if (response.status == "SUCCESS") {
											$("#avai_deduction_titles")
													.append(
															splitString($(
																	"#deduction_title_txt")
																	.val()));
											$("#deduction_title_txt").val('');
											$("#deduction_title_msg")
													.html(
															"Deduction title(s) has been successfully saved.");
											getAvailableDeductionTitles();
										} else if (response.status == "DUPLICATE") {
											$("#deduction_title_msg").html(
													"Duplicate item found.");
										}
									},
									error : function(requestObject, error,
											errorThrown) {
										$("#deduction_title_msg").html(
												"Oops!! Something went wrong.");
									}
								});
					}
					runEffect("deduction_title_msg");
				});

getAvailableDeductionTitles = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableDeductionTitles.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var languageLength = response.result.length;
						if (languageLength > 0) {
							$("#deductio_title option").remove();
							var options = "";
							for (var i = 0; i < languageLength; i++) {
								options += "<option value='"
										+ response.result[i].id + "'>"
										+ response.result[i].title
										+ "</option>";
								listHtml = listHtml
										+ "<span class='badge bg-info available_section'>"
										+ response.result[i].title + "</span>";
							}
							$("#deductio_title").append(options);
						}
					}
					$("#avai_deduction_titles").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/*******************************************************************************
 * Section for new bonus titles
 */
$("#bonus_title_save_btn")
		.click(
				function() {
					if ($("#bonus_title_txt").val() != "") {
						var bonusTitleVo = new Object();
						bonusTitleVo.title = $("#bonus_title_txt").val();
						$("#bonus_title_msg").removeClass("hideMe");
						$("#bonus_title_msg").fadeIn();
						$
								.ajax({
									type : "POST",
									url : "saveBonusTitle.do",
									dataType : "json",
									contentType : "application/json; charset=utf-8",
									data : JSON.stringify(bonusTitleVo),
									cache : false,
									async : true,
									success : function(response) {
										if (response.status == "SUCCESS") {
											$("#avai_bonus_titles").append(
													splitString($(
															"#bonus_title_txt")
															.val()));
											$("#bonus_title_txt").val('');
											$("#bonus_title_msg")
													.html(
															"Bonus title(s) has been successfully saved.");
											getAvailableBonusTitles();
										} else if (response.status == "DUPLICATE") {
											$("#bonus_title_msg").html(
													"Duplicate item found.");
										}
									},
									error : function(requestObject, error,
											errorThrown) {
										$("#bonus_title_msg").html(
												"Oops!! Something went wrong.");
									}
								});
					}
					runEffect("bonus_title_msg");
				});

getAvailableBonusTitles = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableBonusTitles.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var languageLength = response.result.length;
						if (languageLength > 0) {
							$("#bonus_title option").remove();
							var options = "";
							for (var i = 0; i < languageLength; i++) {
								options += "<option value='"
										+ response.result[i].id + "'>"
										+ response.result[i].title
										+ "</option>";
								listHtml = listHtml
										+ "<span class='badge bg-info available_section'>"
										+ response.result[i].title + "</span>";
							}
							$("#bonus_title").append(options);
						}
					}
					$("#avai_bonus_titles").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/*******************************************************************************
 * Section for new adjustment titles
 */
$("#adjustment_title_save_btn")
		.click(
				function() {
					if ($("#adjustment_title_txt").val() != "") {
						var adjustmentTitleVo = new Object();
						adjustmentTitleVo.title = $("#adjustment_title_txt")
								.val();
						$("#adjustment_title_msg").removeClass("hideMe");
						$("#adjustment_title_msg").fadeIn();
						$
								.ajax({
									type : "POST",
									url : "saveAdjustmentTitle.do",
									dataType : "json",
									contentType : "application/json; charset=utf-8",
									data : JSON.stringify(adjustmentTitleVo),
									cache : false,
									async : true,
									success : function(response) {
										if (response.status == "SUCCESS") {
											$("#avai_adjustment_titles")
													.append(
															splitString($(
																	"#adjustment_title_txt")
																	.val()));
											$("#adjustment_title_txt").val('');
											$("#adjustment_title_msg")
													.html(
															"Adjustment title(s) has been successfully saved.");
											getAvailableAdjustmentTitles();
										} else if (response.status == "DUPLICATE") {
											$("#adjustment_title_msg").html(
													"Duplicate item found.");
										}
									},
									error : function(requestObject, error,
											errorThrown) {
										$("#adjustment_title_msg").html(
												"Oops!! Something went wrong.");
									}
								});
					}
					runEffect("adjustment_title_msg");
				});

getAvailableAdjustmentTitles = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableAdjustmentTitles.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var languageLength = response.result.length;
						if (languageLength > 0) {
							$("#adjustment_title option").remove();
							var options = "";
							for (var i = 0; i < languageLength; i++) {
								options += "<option value='"
										+ response.result[i].id + "'>"
										+ response.result[i].title
										+ "</option>";
								listHtml = listHtml
										+ "<span class='badge bg-info available_section'>"
										+ response.result[i].title + "</span>";
							}
							$("#adjustment_title").append(options);
						}
					}
					$("#avai_adjustment_titles").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/**
 * Section for list all available skills
 */
getAllAvailableSkill = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableSkills.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var skillLength = response.result.length;
						if (skillLength > 0) {
							for (var i = 0; i < skillLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].skillName
										+ "'><span onclick='updateItem(\""
										+ response.result[i].skillName
										+ "\",\"skill\")'>"
										+ response.result[i].skillName
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].skillName
										+ "\",\"skill\")'></i></span>";
							}
						}
					}
					$("#avai-l-skill").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new skill
 */
newSkill = function() {
	$('form#StSkillForm')
			.submit(
					function(event) {
						if ($("#skillNameVal").val().length > 0) {
							var skillVo = new Object();
							skillVo.skillName = $("#skillNameVal").val();
							skillVo.tempSkill = $("#tempSkill").val();
							$("#skillNameVal").val('');
							$("#setg-sk-pp-msg").removeClass("hideMe");
							$("#setg-sk-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveSkill.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										accept : 'application/json',
										data : JSON.stringify(skillVo),
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-skill")
														.append(
																splitString(
																		skillVo.skillName,
																		'skill'));
												$("#skillNameVal").val('');
												$("#setg-sk-pp-msg")
														.html(
																"Skill(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-sk-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-sk-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-sk-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available qualification degrees
 */
getAllAvailableQDegree = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableQDegrees.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var qdLength = response.result.length;
						if (qdLength > 0) {

							$("#emp_qulfctn_add_dgre").empty();
							var degrees = '<option value="">--Select Degree--</option>';
							for (var i = 0; i < qdLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].degreeName
										+ "'><span onclick='updateItem(\""
										+ response.result[i].degreeName
										+ "\",\"qualificationDegree\")'>"
										+ response.result[i].degreeName
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].degreeName
										+ "\",\"qualificationDegree\")'></i></span>";
								degrees += '<option value="'
										+ response.result[i].degreeId + '">'
										+ response.result[i].degreeName
										+ '</option>';
							}
							$("#emp_qulfctn_add_dgre").append(degrees);
						}
					}
					$("#avai-l-qd").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new qualification degree
 */
newQD = function() {
	$('form#StQDForm')
			.submit(
					function(event) {
						if ($("#degreeNameVal").val().length > 0) {
							var degreeVo = new Object();
							degreeVo.degreeName = $("#degreeNameVal").val();
							degreeVo.tempQualification = $("#tempQualification")
									.val();
							$("#degreeNameVal").val('');
							$("#setg-qd-pp-msg").removeClass("hideMe");
							$("#setg-qd-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveQualification.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										accept : 'application/json',
										data : JSON.stringify(degreeVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-qd")
														.append(
																splitString(
																		degreeVo.degreeName,
																		'qualificationDegree'));
												$("#degreeNameVal").val('');
												$("#setg-qd-pp-msg")
														.html(
																"Qualification Degree(s) has been successfully saved.");
												getAllAvailableQDegree();
											} else if (response.status == "DUPLICATE") {
												$("#setg-qd-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-qd-qd-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-qd-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available contract types
 */
getAllAvailableCTypes = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableCTypes.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var ctLength = response.result.length;
						if (ctLength > 0) {
							for (var i = 0; i < ctLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].contractType
										+ "'><span onclick='updateItem(\""
										+ response.result[i].contractType
										+ "\",\"contractType\")'>"
										+ response.result[i].contractType
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].contractType
										+ "\",\"contractType\")'></i></span>";
							}
						}
					}
					$("#avai-l-ct").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new job types
 */
newCT = function() {
	$('form#StCTForm')
			.submit(
					function(event) {
						if ($("#contractTypeVal").val().length > 0) {
							var contractTypeVo = new Object();
							contractTypeVo.contractType = $("#contractTypeVal")
									.val();
							contractTypeVo.tempType = $("#tempType").val();
							$("#contractTypeVal").val('');
							$("#setg-ct-pp-msg").removeClass("hideMe");
							$("#setg-ct-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveContractType.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(contractTypeVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-ct")
														.append(
																splitString(
																		contractTypeVo.contractType,
																		'contractType'));
												$("#contractTypeVal").val('');
												$("#setg-ct-pp-msg")
														.html(
																"Contract Type(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-ct-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-ct-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-ct-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available job types
 */
getAllAvailableJTypes = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableJTypes.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var jtLength = response.result.length;
						if (jtLength > 0) {
							for (var i = 0; i < jtLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].jobType
										+ "'><span onclick='updateItem(\""
										+ response.result[i].jobType
										+ "\",\"jobType\")'>"
										+ response.result[i].jobType
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].jobType
										+ "\",\"jobType\")'></i></span>";
							}
						}
					}
					$("#avai-l-jt").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new job types
 */
newJT = function() {
	$('form#StJTForm')
			.submit(
					function(event) {
						if ($("#jobTypeVal").val().length > 0) {
							var jobTypeVo = new Object();
							jobTypeVo.jobType = $("#jobTypeVal").val();
							jobTypeVo.tempJobType = $("#tempJobType").val();
							$("#jobTypeVal").val('');
							$("#setg-jt-pp-msg").removeClass("hideMe");
							$("#setg-jt-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveJobType.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(jobTypeVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-jt").append(
														splitString(jobTypeVo.jobType,
																'jobType'));
												$("#jobTypeVal").val('');
												$("#setg-jt-pp-msg")
														.html(
																"Job Type(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-jt-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-jt-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-jt-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available job fields
 */
getAllAvailableJFields = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableJFields.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var jfLength = response.result.length;
						if (jfLength > 0) {
							for (var i = 0; i < jfLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].jobField
										+ "'><span onclick='updateItem(\""
										+ response.result[i].jobField
										+ "\",\"jobField\")'>"
										+ response.result[i].jobField
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].jobField
										+ "\",\"jobField\")'></i></span>";
							}
						}
					}
					$("#avai-l-jf").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new job fields
 */
newJF = function() {
	$('form#StJFForm')
			.submit(
					function(event) {
						if ($("#jobFieldVal").val().length > 0) {
							var jobFieldVo = new Object();
							jobFieldVo.jobField = $("#jobFieldVal").val();
							jobFieldVo.tempJobField = $("#tempJobField").val();
							$("#jobFieldVal").val('');
							$("#setg-jf-pp-msg").removeClass("hideMe");
							$("#setg-jf-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveJobField.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(jobFieldVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-jf").append(
														splitString(jobFieldVo.jobField,
																'jobField'));
												$("#jobFieldVal").val('');
												$("#setg-jf-pp-msg")
														.html(
																"Job Field(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-jf-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-jf-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-jf-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available branch types
 */
getAllAvailableBTypes = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableBTypes.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var btLength = response.result.length;
						if (btLength > 0) {
							for (var i = 0; i < btLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].branchTypeName
										+ "'><span onclick='updateItem(\""
										+ response.result[i].branchTypeName
										+ "\",\"branchType\")'>"
										+ response.result[i].branchTypeName
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].branchTypeName
										+ "\",\"branchType\")'></i></span>";
							}
						}
					}
					$("#avai-l-bt").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new branch type
 */
newBT = function() {
	$('form#StBTForm')
			.submit(
					function(event) {
						if ($("#branchTypeNameVal").val().length > 0) {
							var branchTypeVo = new Object();
							branchTypeVo.branchTypeName = $(
									"#branchTypeNameVal").val();
							branchTypeVo.tempBranchType = $("#tempBranchType")
									.val();
							$("#branchTypeNameVal").val('');
							$("#setg-bt-pp-msg").removeClass("hideMe");
							$("#setg-bt-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveBranchType.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(branchTypeVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-bt")
														.append(
																splitString(
																		branchTypeVo.branchTypeName,
																		'branchType'));
												$("#branchTypeNameVal").val('');
												$("#setg-bt-pp-msg")
														.html(
																"Branch Type(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-bt-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-bt-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-bt-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available policy types
 */
getAllAvailablePTypes = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailablePTypes.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var ptLength = response.result.length;
						if (ptLength > 0) {
							for (var i = 0; i < ptLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].policyType
										+ "'><span onclick='updateItem(\""
										+ response.result[i].policyType
										+ "\",\"policyType\")'>"
										+ response.result[i].policyType
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].policyType
										+ "\",\"policyType\")'></i></span>";
							}
						}
					}
					$("#avai-l-pt").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deletePolicyType = function(policyType) {
	$
			.ajax({
				type : "POST",
				url : "DeletePolicyType.do",
				cache : false,
				async : true,
				data : {
					"policyType" : policyType
				},
				success : function(response) {
					$("#setg-pt-pp-msg").removeClass("hideMe");
					$("#setg-pt-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-pt-pp-msg").html(
								"Policy type has been successfully deleted.");
						getAllAvailablePTypes();
					} else {
						$("#setg-pt-pp-msg")
								.html(
										"You can not delete this type, because it is already used in some records.");
					}
					runEffect("setg-pt-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteLanguage = function(language) {
	$
			.ajax({
				type : "POST",
				url : "DeleteLanguage.do",
				cache : false,
				async : true,
				data : {
					"language" : language
				},
				success : function(response) {
					$("#setg-pp-msg").removeClass("hideMe");
					$("#setg-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-pp-msg").html(
								"Language has been successfully deleted.");
						getAllAvailableLanguage();
					} else {
						$("#setg-pp-msg")
								.html(
										"You can not delete this language, because it is already used in some records.");
					}
					runEffect("setg-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteLanguage = function(language) {
	$
			.ajax({
				type : "POST",
				url : "DeleteLanguage.do",
				cache : false,
				async : true,
				data : {
					"language" : language
				},
				success : function(response) {
					$("#setg-pp-msg").removeClass("hideMe");
					$("#setg-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-pp-msg").html(
								"Language has been successfully deleted.");
						getAllAvailableLanguage();
					} else {
						$("#setg-pp-msg")
								.html(
										"You can not delete this language, because it is already used in some records.");
					}
					runEffect("setg-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteSkill = function(skill) {
	$
			.ajax({
				type : "POST",
				url : "DeleteSkill.do",
				cache : false,
				async : true,
				data : {
					"skill" : skill
				},
				success : function(response) {
					$("#setg-sk-pp-msg").removeClass("hideMe");
					$("#setg-sk-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-sk-pp-msg").html(
								"Skill has been successfully deleted.");
						getAllAvailableSkill();
					} else {
						$("#setg-sk-pp-msg")
								.html(
										"You can not delete this skill, because it is already used in some records.");
					}
					runEffect("setg-sk-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteQualificationDegree = function(qd) {
	$
			.ajax({
				type : "POST",
				url : "DeleteQD.do",
				cache : false,
				async : true,
				data : {
					"qd" : qd
				},
				success : function(response) {
					$("#setg-qd-pp-msg").removeClass("hideMe");
					$("#setg-qd-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-qd-pp-msg")
								.html(
										"Qualification degree has been successfully deleted.");
						getAllAvailableQDegree();
					} else {
						$("#setg-qd-pp-msg")
								.html(
										"You can not delete this degree, because it is already used in some records.");
					}
					runEffect("setg-qd-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteContractType = function(contractType) {
	$
			.ajax({
				type : "POST",
				url : "DeleteContractType.do",
				cache : false,
				async : true,
				data : {
					"contractType" : contractType
				},
				success : function(response) {
					$("#setg-ct-pp-msg").removeClass("hideMe");
					$("#setg-ct-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-ct-pp-msg").html(
								"Contract type has been successfully deleted.");
						getAllAvailableCTypes();
					} else {
						$("#setg-ct-pp-msg")
								.html(
										"You can not delete this type, because it is already used in some records.");
					}
					runEffect("setg-ct-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteJobType = function(jobType) {
	$
			.ajax({
				type : "POST",
				url : "DeleteJobType.do",
				cache : false,
				async : true,
				data : {
					"jobType" : jobType
				},
				success : function(response) {
					$("#setg-jt-pp-msg").removeClass("hideMe");
					$("#setg-jt-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-jt-pp-msg").html(
								"Job type has been successfully deleted.");
						getAllAvailableJTypes();
					} else {
						$("#setg-jt-pp-msg")
								.html(
										"You can not delete this type, because it is already used in some records.");
					}
					runEffect("setg-jt-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteJobField = function(jobField) {
	$
			.ajax({
				type : "POST",
				url : "DeleteJobField.do",
				cache : false,
				async : true,
				data : {
					"jobField" : jobField
				},
				success : function(response) {
					$("#setg-jf-pp-msg").removeClass("hideMe");
					$("#setg-jf-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-jf-pp-msg").html(
								"Job field has been successfully deleted.");
						getAllAvailableJFields();
					} else {
						$("#setg-jf-pp-msg")
								.html(
										"You can not delete this field, because it is already used in some records.");
					}
					runEffect("setg-jf-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteBranchType = function(branchType) {
	$
			.ajax({
				type : "POST",
				url : "DeleteBranchType.do",
				cache : false,
				async : true,
				data : {
					"branchType" : branchType
				},
				success : function(response) {
					$("#setg-bt-pp-msg").removeClass("hideMe");
					$("#setg-bt-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-bt-pp-msg").html(
								"Job field has been successfully deleted.");
						getAllAvailableBTypes();
					} else {
						$("#setg-bt-pp-msg")
								.html(
										"You can not delete this field, because it is already used in some records.");
					}
					runEffect("setg-bt-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteEmployeeType = function(employeeType) {
	$
			.ajax({
				type : "POST",
				url : "DeleteEmployeeType.do",
				cache : false,
				async : true,
				data : {
					"employeeType" : employeeType
				},
				success : function(response) {
					$("#setg-et-pp-msg").removeClass("hideMe");
					$("#setg-et-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-et-pp-msg").html(
								"Employee type has been successfully deleted.");
						getAllAvailableETypes();
					} else {
						$("#setg-et-pp-msg")
								.html(
										"You can not delete this type, because it is already used in some records.");
					}
					runEffect("setg-et-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteEmployeeCategory = function(employeeCategory) {
	$
			.ajax({
				type : "POST",
				url : "DeleteEmployeeCategory.do",
				cache : false,
				async : true,
				data : {
					"employeeCategory" : employeeCategory
				},
				success : function(response) {
					$("#setg-ec-pp-msg").removeClass("hideMe");
					$("#setg-ec-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-ec-pp-msg")
								.html(
										"Employee category has been successfully deleted.");
						getAllAvailableECategories();
					} else {
						$("#setg-ec-pp-msg")
								.html(
										"You can not delete this category, because it is already used in some records.");
					}
					runEffect("setg-ec-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteReimbursementCategory = function(reimbursementCategory) {
	$
			.ajax({
				type : "POST",
				url : "DeleteReimbursementCategory.do",
				cache : false,
				async : true,
				data : {
					"reimbursementCategory" : reimbursementCategory
				},
				success : function(response) {
					$("#setg-rc-pp-msg").removeClass("hideMe");
					$("#setg-rc-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-rc-pp-msg")
								.html(
										"Reimbursement category has been successfully deleted.");
						getAllAvailableReimbCategories();
					} else {
						$("#setg-rc-pp-msg")
								.html(
										"You can not delete this category, because it is already used in some records.");
					}
					runEffect("setg-rc-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteCountry = function(country) {
	$
			.ajax({
				type : "POST",
				url : "DeleteCountry.do",
				cache : false,
				async : true,
				data : {
					"country" : country
				},
				success : function(response) {
					$("#setg-cl-pp-msg").removeClass("hideMe");
					$("#setg-cl-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-cl-pp-msg").html(
								"Country has been successfully deleted.");
						getAllAvailableCountries();
					} else {
						$("#setg-cl-pp-msg")
								.html(
										"You can not delete this country, because it is already used in some records.");
					}
					runEffect("setg-cl-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteAccountType = function(accountType) {
	$
			.ajax({
				type : "POST",
				url : "DeleteAccountType.do",
				cache : false,
				async : true,
				data : {
					"accountType" : accountType
				},
				success : function(response) {
					$("#setg-at-pp-msg").removeClass("hideMe");
					$("#setg-at-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-at-pp-msg").html(
								"Account type has been successfully deleted.");
						getAllAvailableAccountTypes();
					} else {
						$("#setg-at-pp-msg")
								.html(
										"You can not delete this type, because it is already used in some records.");
					}
					runEffect("setg-at-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteWorkShift = function(workShift) {
	$
			.ajax({
				type : "POST",
				url : "DeleteWorkShift.do",
				cache : false,
				async : true,
				data : {
					"workShift" : workShift
				},
				success : function(response) {
					$("#setg-ws-pp-msg").removeClass("hideMe");
					$("#setg-ws-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-ws-pp-msg").html(
								"Work shift has been successfully deleted.");
						getAllAvailableWorkShifts();
					} else {
						$("#setg-ws-pp-msg")
								.html(
										"You can not delete this shift, because it is already used in some records.");
					}
					runEffect("setg-ws-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteEmployeeStatus = function(employeeStatus) {
	$
			.ajax({
				type : "POST",
				url : "DeleteEmployeeStatus.do",
				cache : false,
				async : true,
				data : {
					"employeeStatus" : employeeStatus
				},
				success : function(response) {
					$("#setg-st-pp-msg").removeClass("hideMe");
					$("#setg-st-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-st-pp-msg")
								.html(
										"Employee status has been successfully deleted.");
						getAllAvailableEStatus();
					} else {
						$("#setg-st-pp-msg")
								.html(
										"You can not delete this status, because it is already used in some records.");
					}
					runEffect("setg-st-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteTerminationStatus = function(terminationStatus) {
	$
			.ajax({
				type : "POST",
				url : "DeleteTerminationStatus.do",
				cache : false,
				async : true,
				data : {
					"terminationStatus" : terminationStatus
				},
				success : function(response) {
					$("#setg-ts-pp-msg").removeClass("hideMe");
					$("#setg-ts-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-ts-pp-msg")
								.html(
										"Termination status has been successfully deleted.");
						getAllAvailableTStatus();
					} else {
						$("#setg-ts-pp-msg")
								.html(
										"You can not delete this status, because it is already used in some records.");
					}
					runEffect("setg-ts-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteLeaveType = function(leaveType) {
	$
			.ajax({
				type : "POST",
				url : "DeleteLeaveType.do",
				cache : false,
				async : true,
				data : {
					"leaveType" : leaveType
				},
				success : function(response) {
					$("#setg-lt-pp-msg").removeClass("hideMe");
					$("#setg-lt-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-lt-pp-msg").html(
								"Leave type has been successfully deleted.");
						getAllAvailableLTypes();
					} else {
						$("#setg-lt-pp-msg")
								.html(
										"You can not delete this type, because it is already used in some records.");
					}
					runEffect("setg-lt-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteReminderStatus = function(reminderStatus) {
	$
			.ajax({
				type : "POST",
				url : "DeleteReminderStatus.do",
				cache : false,
				async : true,
				data : {
					"reminderStatus" : reminderStatus
				},
				success : function(response) {
					$("#setg-rs-pp-msg").removeClass("hideMe");
					$("#setg-rs-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-rs-pp-msg")
								.html(
										"Reminder status has been successfully deleted.");
						getAllAvailableRStatus();
					} else {
						$("#setg-rs-pp-msg")
								.html(
										"You can not delete this status, because it is already used in some records.");
					}
					runEffect("setg-rs-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteEmployeeGrade = function(employeeGrade) {
	$
			.ajax({
				type : "POST",
				url : "DeleteEmployeeGrade.do",
				cache : false,
				async : true,
				data : {
					"employeeGrade" : employeeGrade
				},
				success : function(response) {
					$("#setg-eg-pp-msg").removeClass("hideMe");
					$("#setg-eg-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-eg-pp-msg")
								.html(
										"Employee grade has been successfully deleted.");
						getAllAvailableEGrades();
					} else {
						$("#setg-eg-pp-msg")
								.html(
										"You can not delete this grade, because it is already used in some records.");
					}
					runEffect("setg-eg-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteAdvanceSalaryStatus = function(advanceSalaryStatus) {
	$
			.ajax({
				type : "POST",
				url : "DeleteAdvanceSalaryStatus.do",
				cache : false,
				async : true,
				data : {
					"advanceSalaryStatus" : advanceSalaryStatus
				},
				success : function(response) {
					$("#setg-as-pp-msg").removeClass("hideMe");
					$("#setg-as-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-as-pp-msg")
								.html(
										"Advance salary status has been successfully deleted.");
						getAllAvailableAsvncStatus();
					} else {
						$("#setg-as-pp-msg")
								.html(
										"You can not delete this status, because it is already used in some records.");
					}
					runEffect("setg-as-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteOvertimeStatus = function(overtimeStatus) {
	$
			.ajax({
				type : "POST",
				url : "DeleteOvertimeStatus.do",
				cache : false,
				async : true,
				data : {
					"overtimeStatus" : overtimeStatus
				},
				success : function(response) {
					$("#setg-os-pp-msg").removeClass("hideMe");
					$("#setg-os-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-os-pp-msg")
								.html(
										"Overtime status has been successfully deleted.");
						getAllAvailableOStatus();
					} else {
						$("#setg-os-pp-msg")
								.html(
										"You can not delete this status, because it is already used in some records.");
					}
					runEffect("setg-os-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
deleteLoanStatus = function(loanStatus) {
	$
			.ajax({
				type : "POST",
				url : "DeleteLoanStatus.do",
				cache : false,
				async : true,
				data : {
					"loanStatus" : loanStatus
				},
				success : function(response) {
					$("#setg-ls-pp-msg").removeClass("hideMe");
					$("#setg-ls-pp-msg").fadeIn();
					if (response.status == "SUCCESS") {
						$("#setg-ls-pp-msg").html(
								"Loan status has been successfully deleted.");
						getAllAvailableLStatus();
					} else {
						$("#setg-ls-pp-msg")
								.html(
										"You can not delete this status, because it is already used in some records.");
					}
					runEffect("setg-ls-pp-msg");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * 
 * @param policyTypeId
 */
deleteItem = function(item, thisFunction) {
	if (thisFunction == "policyType") {
		deletePolicyType(item);
	} else if (thisFunction == "language") {
		deleteLanguage(item);
	} else if (thisFunction == "skill") {
		deleteSkill(item);
	} else if (thisFunction == "qualificationDegree") {
		deleteQualificationDegree(item);
	} else if (thisFunction == "contractType") {
		deleteContractType(item);
	} else if (thisFunction == "jobType") {
		deleteJobType(item);
	} else if (thisFunction == "jobField") {
		deleteJobField(item);
	} else if (thisFunction == "branchType") {
		deleteBranchType(item);
	} else if (thisFunction == "employeeType") {
		deleteEmployeeType(item);
	} else if (thisFunction == "employeeCategory") {
		deleteEmployeeCategory(item);
	} else if (thisFunction == "reimbursementCategory") {
		deleteReimbursementCategory(item);
	} else if (thisFunction == "country") {
		deleteCountry(item);
	} else if (thisFunction == "accountType") {
		deleteAccountType(item);
	} else if (thisFunction == "workShift") {
		deleteWorkShift(item);
	} else if (thisFunction == "employeeStatus") {
		deleteEmployeeStatus(item);
	} else if (thisFunction == "terminationStatus") {
		deleteTerminationStatus(item);
	} else if (thisFunction == "leaveType") {
		deleteLeaveType(item);
	} else if (thisFunction == "reminderStatus") {
		deleteReminderStatus(item);
	} else if (thisFunction == "employeeGrade") {
		deleteEmployeeGrade(item);
	} else if (thisFunction == "advanceSalaryStatus") {
		deleteAdvanceSalaryStatus(item);
	} else if (thisFunction == "overtimeStatus") {
		deleteOvertimeStatus(item);
	} else if (thisFunction == "loanStatus") {
		deleteLoanStatus(item);
	}
}
/**
 * method to save new policy type
 */
newPT = function() {
	$('form#StPTForm')
			.submit(
					function(event) {
						if ($("#policyTypeVal").val().length > 0) {
							var policyTypeVo = new Object();
							policyTypeVo.policyType = $("#policyTypeVal").val();
							policyTypeVo.tempPolicyType = $("#tempPolicyType")
									.val();
							$("#policyTypeVal").val('');
							$("#setg-pt-pp-msg").removeClass("hideMe");
							$("#setg-pt-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SavePolicyType.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(policyTypeVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-pt")
														.append(
																splitString(
																		policyTypeVo.policyType,
																		'policyType'));
												$("#policyTypeVal").val('');
												$("#setg-pt-pp-msg")
														.html(
																"Policy Type(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-pt-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-pt-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-pt-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available employee types
 */
getAllAvailableETypes = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableETypes.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var etLength = response.result.length;
						if (etLength > 0) {
							for (var i = 0; i < etLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].employeeType
										+ "'><span onclick='updateItem(\""
										+ response.result[i].employeeType
										+ "\",\"employeeType\")'>"
										+ response.result[i].employeeType
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].employeeType
										+ "\",\"employeeType\")'></i></span>";
							}
						}
					}
					$("#avai-l-et").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new employee type
 */
newET = function() {
	$('form#StETForm')
			.submit(
					function(event) {
						if ($("#employeeTypeVal").val().length > 0) {
							var employeeTypeVo = new Object();
							employeeTypeVo.employeeType = $("#employeeTypeVal")
									.val();
							employeeTypeVo.tempEmployeeType = $(
									"#tempEmployeeType").val();
							$("#employeeTypeVal").val('');
							$("#setg-et-pp-msg").removeClass("hideMe");
							$("#setg-et-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveEmployeeType.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(employeeTypeVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-et")
														.append(
																splitString(
																		employeeTypeVo.employeeType,
																		'employeeType'));
												$("#employeeTypeVal").val('');
												$("#setg-et-pp-msg")
														.html(
																"Employee Type(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-et-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-et-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-et-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available employee categories
 */
getAllAvailableECategories = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableECategories.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var ecLength = response.result.length;
						if (ecLength > 0) {
							for (var i = 0; i < ecLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].employeeCategory
										+ "'><span onclick='updateItem(\""
										+ response.result[i].employeeCategory
										+ "\",\"employeeCategory\")'>"
										+ response.result[i].employeeCategory
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].employeeCategory
										+ "\",\"employeeCategory\")'></i></span>";
							}
						}
					}
					$("#avai-l-ec").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new employee category
 */
newEC = function() {
	$('form#StECForm')
			.submit(
					function(event) {
						if ($("#employeeCategoryVal").val().length > 0) {
							var employeeCategoryVo = new Object();
							employeeCategoryVo.employeeCategory = $(
									"#employeeCategoryVal").val();
							employeeCategoryVo.tempEmployeeCategory = $(
									"#tempEmployeeCategory").val();
							$("#employeeCategoryVal").val(
							'');
							$("#setg-ec-pp-msg").removeClass("hideMe");
							$("#setg-ec-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveEmployeeCategory.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON
												.stringify(employeeCategoryVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-ec")
														.append(
																splitString(
																		employeeCategoryVo.employeeCategory,
																		'employeeCategory'));
												$("#employeeCategoryVal").val(
														'');
												$("#setg-ec-pp-msg")
														.html(
																"Category(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-ec-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-ec-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-ec-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available reimbursment categories
 */
getAllAvailableReimbCategories = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableReimbCategories.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var rcLength = response.result.length;
						if (rcLength > 0) {
							for (var i = 0; i < rcLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].reimbursementCategory
										+ "'><span onclick='updateItem(\""
										+ response.result[i].reimbursementCategory
										+ "\",\"reimbursementCategory\")'>"
										+ response.result[i].reimbursementCategory
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].reimbursementCategory
										+ "\",\"reimbursementCategory\")'></i></span>";
							}
						}
					}
					$("#avai-l-rc").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new country
 */
newRI = function() {
	$('form#StRIForm')
			.submit(
					function(event) {
						if ($("#reimbursementCategoryVal").val().length > 0) {
							var reimbursementCategoryVo = new Object();
							reimbursementCategoryVo.reimbursementCategory = $(
									"#reimbursementCategoryVal").val();
							reimbursementCategoryVo.tempReimbursementCategory = $(
									"#tempReimbursementCategory").val();
							$("#reimbursementCategoryVal")
							.val('');
							$("#setg-rc-pp-msg").removeClass("hideMe");
							$("#setg-rc-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveReimbursementCategory.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON
												.stringify(reimbursementCategoryVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-rc")
														.append(
																splitString(
																		reimbursementCategoryVo.reimbursementCategory,
																		'reimbursementCategory'));
												$("#reimbursementCategoryVal")
														.val('');
												$("#setg-rc-pp-msg")
														.html(
																"Reimbursement Category(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-rc-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-rc-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-rc-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all availabe countries
 */
getAllAvailableCountries = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableCountries.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].name
										+ "'><span onclick='updateItem(\""
										+ response.result[i].name
										+ "\",\"country\")'>"
										+ response.result[i].name
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].name
										+ "\",\"country\")'></i></span>";
							}
						}
					}
					$("#avai-l-cl").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new country
 */
newCTR = function() {
	$('form#StCTRForm')
			.submit(
					function(event) {
						if ($("#countryName").val().length > 0) {
							var countryVo = new Object();
							countryVo.name = $("#countryName").val();
							countryVo.tempName = $("#tempName").val();
							$("#countryName").val('');
							$("#setg-cl-pp-msg").removeClass("hideMe");
							$("#setg-cl-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "NewCountry.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(countryVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-cl").append(
														splitString(countryVo.name,
																'country'));
												$("#countryName").val('');
												$("#setg-cl-pp-msg")
														.html(
																"Country(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-cl-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-cl-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-cl-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available account type
 */
getAllAvailableAccountTypes = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableAccountTypes.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].accountType
										+ "'><span onclick='updateItem(\""
										+ response.result[i].accountType
										+ "\",\"accountType\")'>"
										+ response.result[i].accountType
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].accountType
										+ "\",\"accountType\")'></i></span>";
							}
						}
					}
					$("#avai-l-at").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new account type
 */
newAT = function() {
	$('form#StATForm')
			.submit(
					function(event) {
						if ($("#accountTypeVal").val().length > 0) {
							var accountTypeVo = new Object();
							accountTypeVo.accountType = $("#accountTypeVal")
									.val();
							accountTypeVo.tempAccountType = $(
									"#tempAccountType").val();
							$("#accountTypeVal").val('');
							$("#setg-at-pp-msg").removeClass("hideMe");
							$("#setg-at-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveAccountType.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(accountTypeVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-at")
														.append(
																splitString(
																		accountTypeVo.accountType,
																		'accountType'));
												$("#accountTypeVal").val('');
												$("#setg-at-pp-msg")
														.html(
																"Account Type(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-at-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-at-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-at-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available work shifts
 */
getAllAvailableWorkShifts = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableWorkShifts.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].workShift
										+ "'><span onclick='updateItem(\""
										+ response.result[i].workShift
										+ "\",\"workShift\")'>"
										+ response.result[i].workShift
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].workShift
										+ "\",\"workShift\")'></i></span>";
							}
						}
					}
					$("#avai-l-ws").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new work shifts
 */
newWS = function() {
	$('form#StWSForm')
			.submit(
					function(event) {
						if ($("#workShiftVal").val().length > 0) {
							var workShiftVo = new Object();
							workShiftVo.workShift = $("#workShiftVal").val();
							workShiftVo.tempWorkShift = $("#tempWorkShift")
									.val();
							$("#workShiftVal").val('');
							$("#setg-ws-pp-msg").removeClass("hideMe");
							$("#setg-ws-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveWorkShift.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(workShiftVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-ws")
														.append(
																splitString(
																		workShiftVo.workShift,
																		'workShift'));
												$("#workShiftVal").val('');
												$("#setg-ws-pp-msg")
														.html(
																"Work Shift(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-ws-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-ws-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-ws-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available employee status
 */
getAllAvailableEStatus = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableEStatus.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].status
										+ "'><span onclick='updateItem(\""
										+ response.result[i].status
										+ "\",\"employeeStatus\")'>"
										+ response.result[i].status
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].status
										+ "\",\"employeeStatus\")'></i></span>";
							}
						}
					}
					$("#avai-l-st").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new employee status
 */
newES = function() {
	$('form#StESForm')
			.submit(
					function(event) {
						if ($("#employeeStatus").val().length > 0) {
							var employeeStatusVo = new Object();
							employeeStatusVo.status = $("#employeeStatus")
									.val();
							employeeStatusVo.tempEmployeeStatus = $(
									"#tempEmployeeStatus").val();
							$("#employeeStatus").val('');
							$("#setg-st-pp-msg").removeClass("hideMe");
							$("#setg-st-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveEmployeeStatus.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(employeeStatusVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-st")
														.append(
																splitString(
																		employeeStatusVo.status,
																		'employeeStatus'));
												$("#employeeStatus").val('');
												$("#setg-st-pp-msg")
														.html(
																"Employee Status(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-st-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-st-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-st-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available termination status
 */
getAllAvailableTStatus = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableTStatus.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section'>"
										+ response.result[i].status
										+ "&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].status
										+ "\",\"terminationStatus\")'></i></span>";
							}
						}
					}
					$("#avai-l-ts").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new termination status
 */
newTS = function() {
	$('form#StTSForm')
			.submit(
					function(event) {
						if ($("#terminationStatus").val().length > 0) {
							var terminationStatusVo = new Object();
							terminationStatusVo.status = $("#terminationStatus")
									.val();
							$("#terminationStatus").val('');
							$("#setg-ts-pp-msg").removeClass("hideMe");
							$("#setg-ts-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveTerminationStatus.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON
												.stringify(terminationStatusVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-ts")
														.append(
																splitString(
																		terminationStatusVo.status,
																		'terminationStatus'));
												$("#terminationStatus").val('');
												$("#setg-ts-pp-msg")
														.html(
																"Termination Status(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-ts-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-ts-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-ts-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available leave types
 */
getAllAvailableLTypes = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableLTypes.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].leaveType
										+ "'><span onclick='updateItem(\""
										+ response.result[i].leaveType
										+ "\",\"leaveType\")'>"
										+ response.result[i].leaveType
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].leaveType
										+ "\",\"leaveType\")'></i></span>";
							}
						}
					}
					$("#avai-l-lt").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new leave type
 */
newLT = function() {
	$('form#StLTForm')
			.submit(
					function(event) {
						if ($("#leaveTypeVal").val().length > 0) {
							var leaveTypeVo = new Object();
							leaveTypeVo.leaveType = $("#leaveTypeVal").val();
							leaveTypeVo.tempLeaveType = $("#tempLeaveType")
									.val();
							$("#leaveTypeVal").val('');
							$("#setg-lt-pp-msg").removeClass("hideMe");
							$("#setg-lt-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveLaveType.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(leaveTypeVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-lt")
														.append(
																splitString(
																		leaveTypeVo.leaveType,
																		'leaveType'));
												$("#leaveTypeVal").val('');
												$("#setg-lt-pp-msg")
														.html(
																"Leave Type(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-lt-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-lt-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-lt-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available reminder status
 */
getAllAvailableRStatus = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableRStatus.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].status
										+ "'><span onclick='updateItem(\""
										+ response.result[i].status
										+ "\",\"reminderStatus\")'>"
										+ response.result[i].status
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].status
										+ "\",\"reminderStatus\")'></i></span>";
							}
						}
					}
					$("#avai-l-rs").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new reminder status
 */
newRS = function() {
	$('form#StRSForm')
			.submit(
					function(event) {
						if ($("#reminderStatus").val().length > 0) {
							var reminderStatusVo = new Object();
							reminderStatusVo.status = $("#reminderStatus")
									.val();
							reminderStatusVo.tempStatus = $("#tempStatus")
									.val();
							$("#reminderStatus").val('');
							$("#setg-rs-pp-msg").removeClass("hideMe");
							$("#setg-rs-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveReminderStatus.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(reminderStatusVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-rs")
														.append(
																splitString(
																		reminderStatusVo.status,
																		'reminderStatus'));
												$("#reminderStatus").val('');
												$("#setg-rs-pp-msg")
														.html(
																"Reminder Status(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-rs-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-rs-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-rs-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * 
 */
getAllAvailableEGrades = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableEGrades.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].grade
										+ "'><span onclick='updateItem(\""
										+ response.result[i].grade
										+ "\",\"employeeGrade\")'>"
										+ response.result[i].grade
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].grade
										+ "\",\"employeeGrade\")'></i></span>";
							}
						}
					}
					$("#avai-l-eg").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new employee grade
 */
newEG = function() {
	$('form#StEGForm')
			.submit(
					function(event) {
						if ($("#employeeGrade").val().length > 0) {
							var employeeGradeVo = new Object();
							employeeGradeVo.grade = $("#employeeGrade").val();
							employeeGradeVo.tempGrade = $("#tempGrade").val();
							$("#employeeGrade").val('');
							$("#setg-eg-pp-msg").removeClass("hideMe");
							$("#setg-eg-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveEmployeeGrade.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(employeeGradeVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-eg")
														.append(
																splitString(
																		employeeGradeVo.grade,
																		'employeeGrade'));
												$("#employeeGrade").val('');
												$("#setg-eg-pp-msg")
														.html(
																"Employee Grade(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-eg-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-eg-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-eg-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all available advance salary status
 */
getAllAvailableAsvncStatus = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableAsvncStatus.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].status
										+ "'><span onclick='updateItem(\""
										+ response.result[i].status
										+ "\",\"advanceSalaryStatus\")'>"
										+ response.result[i].status
										+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].status
										+ "\",\"advanceSalaryStatus\")'></i></span>";
							}
						}
					}
					$("#avai-l-as").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new organization assets
 */
newAST = function() {
	$('form#StASTForm')
			.submit(
					function(event) {
						if ($("#assetCodeVal").val().length > 0
								&& $("#assetNameVal").val().length > 0) {
							var assetsVo = new Object();
							assetsVo.assetName = $("#assetNameVal").val();
							assetsVo.assetCode = $("#assetCodeVal").val();
							assetsVo.tempAssetCode = $("#tempAssetCode").val();
							$("#assetNameVal").val('');
							$("#assetCodeVal").val('');
							$("#setg-ast-pp-msg").removeClass("hideMe");
							$("#setg-ast-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveAsset.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(assetsVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {

												$("#avai-l-assets")
														.append(
																"<span class='badge bg-info available_section "
																		+ assetsVo.assetCode
																		+ "'><span onclick='updateAsset(\""
																		+ assetsVo.assetCode
																		+ "\",\""
																		+ assetsVo.assetName
																		+ "\")'>"
																		+ assetsVo.assetName
																		+ "( "
																		+ assetsVo.assetCode
																		+ " )"
																		+ "</span>&nbsp;<i class='fa fa-times' onclick='deleteAssetItem(\""
																		+ assetsVo.assetCode
																		+ "\")'></i></span>");

												$("#assetNameVal").val('');
												$("#assetCodeVal").val('');
												$("#setg-ast-pp-msg")
														.html(
																"Organization asset has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-ast-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-ast-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-ast-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to save new advance salary status
 */
newED = function() {
	$('form#StEDForm')
			.submit(
					function(event) {
						if ($("#designationVal").val().length > 0) {
							var designationRankVo = new Object();
							designationRankVo.designation = $("#designationVal")
									.val();
							designationRankVo.rank = $("#designationRankVal")
									.val();
							designationRankVo.tempDes = $("#tempDess").val();
							$("#designationRankVal")
							.val('');
					$("#designationVal").val('');
							$("#setg-ed-pp-msg").removeClass("hideMe");
							$("#setg-ed-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveEmployeeDesignation.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON
												.stringify(designationRankVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-ed")
														.append(
																"<span class='badge bg-info available_section'>"
																		+ designationRankVo.designation
																		+ "( "
																		+ designationRankVo.rank
																		+ " )"
																		+ "&nbsp;<i class='fa fa-times' onclick='deleteEmployeeDesignation("
																		+ designationRankVo.rank
																		+ ",\""
																		+ designationRankVo.designation
																		+ "\")'></i></span>");

												$("#designationRankVal")
														.val('');
												$("#designationVal").val('');
												$("#setg-ed-pp-msg")
														.html(
																"Employee designation has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-ed-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-ed-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-ed-pp-msg");
							event.preventDefault();
						}
					});
}
getAllEmployeeDesignation = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllEmployeeDesignation.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section "
										+ response.result[i].designation
										+ "'>"
										+ "<span onclick='updateDesignation(\""
										+ response.result[i].designation
										+ "\",\""
										+ response.result[i].rank
										+ "\")'>"
										+ response.result[i].designation
										+ "( "
										+ response.result[i].rank
										+ " )"
										+ "</span>"
										+ "&nbsp;<i class='fa fa-times' onclick='deleteEmployeeDesignation("
										+ response.result[i].rank + ",\""
										+ response.result[i].designation
										+ "\")'></i></span>";
							}
						}
					}
					$("#avai-l-ed").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new advance salary status
 */
newADS = function() {
	$('form#StADSForm')
			.submit(
					function(event) {
						if ($("#advanceSalaryStatus").val().length > 0) {
							var advanceSalaryStatusVo = new Object();
							advanceSalaryStatusVo.status = $(
									"#advanceSalaryStatus").val();
							advanceSalaryStatusVo.tempStatus = $("#tempStatus")
									.val();
							$("#advanceSalaryStatus").val(
							'');
							$("#setg-as-pp-msg").removeClass("hideMe");
							$("#setg-as-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveAdvanceSalaryStatus.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON
												.stringify(advanceSalaryStatusVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-as")
														.append(
																splitString(
																		advanceSalaryStatusVo.status,
																		'advanceSalaryStatus'));
												$("#advanceSalaryStatus").val(
														'');
												$("#setg-as-pp-msg")
														.html(
																"Advance Salary(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-as-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-as-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-as-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all overtime status
 */
getAllAvailableOStatus = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableOStatus.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section'>"
										+ response.result[i].status
										+ "&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].status
										+ "\",\"overtimeStatus\")'></i></span>";
							}
						}
					}
					$("#avai-l-os").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new overtime status
 */
newOS = function() {
	$('form#StOSForm')
			.submit(
					function(event) {
						if ($("#overtimeStatus").val().length > 0) {
							var overtimeStatusVo = new Object();
							overtimeStatusVo.status = $("#overtimeStatus")
									.val();
							$("#overtimeStatus").val('');
							$("#setg-os-pp-msg").removeClass("hideMe");
							$("#setg-os-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveOvertimeStatus.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(overtimeStatusVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-os")
														.append(
																splitString(
																		overtimeStatusVo.status,
																		'overtimeStatus'));
												$("#overtimeStatus").val('');
												$("#setg-os-pp-msg")
														.html(
																"Overtime Status(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-os-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-os-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-os-pp-msg");
							event.preventDefault();
						}
					});
}
/**
 * method to list all availabe loan status
 */
getAllAvailableLStatus = function() {
	$
			.ajax({
				type : "GET",
				url : "getAllAvailableLStatus.do",
				cache : false,
				async : true,
				success : function(response) {
					var listHtml = "";
					if (response.status == "SUCCESS") {
						var clLength = response.result.length;
						if (clLength > 0) {
							for (var i = 0; i < clLength; i++) {
								listHtml = listHtml
										+ "<span class='badge bg-info available_section'>"
										+ response.result[i].loanStatus
										+ "&nbsp;<i class='fa fa-times' onclick='deleteItem(\""
										+ response.result[i].loanStatus
										+ "\",\"loanStatus\")'></i></span>";
							}
						}
					}
					$("#avai-l-ls").html(listHtml);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/**
 * method to save new loan status
 */
newLS = function() {
	$('form#StLSForm')
			.submit(
					function(event) {
						if ($("#loanStatus").val().length > 0) {
							var loanStatusVo = new Object();
							loanStatusVo.loanStatus = $("#loanStatus").val();
							$("#loanStatus").val('');
							$("#setg-ls-pp-msg").removeClass("hideMe");
							$("#setg-ls-pp-msg").fadeIn();
							$
									.ajax({
										type : "POST",
										url : "SaveLoanStatus.do",
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(loanStatusVo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "SUCCESS") {
												$("#avai-l-ls").append(
														splitString(loanStatusVo.loanStatus,
																'loanStatus'));
												$("#loanStatus").val('');
												$("#setg-ls-pp-msg")
														.html(
																"Loan Status(s) has been successfully saved.");
											} else if (response.status == "DUPLICATE") {
												$("#setg-ls-pp-msg")
														.html(
																"Duplicate item found.");
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											$("#setg-ls-pp-msg")
													.html(
															"Oops!! Something went wrong.");
										}
									});
							runEffect("setg-ls-pp-msg");
							event.preventDefault();
						}
					});
}

function updateDesignation(des, rank) {
	$("#designationVal").val(des);
	$("#designationRankVal").val(rank);
	$("." + des).remove();
	$("#tempDess").val(des);
}