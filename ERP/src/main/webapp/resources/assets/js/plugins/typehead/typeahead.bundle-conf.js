var engine = new Bloodhound({
	datumTokenizer : function(d) {
		return d.value;
	},
	queryTokenizer : Bloodhound.tokenizers.whitespace,
	local : [ {
		value : 'BASIC SALARY'
	}, {
		value : 'GROSS SALARY'
	}, {
		value : 'DA'
	}, {
		value : 'HRA '
	}, {
		value : 'CONVEYANCE '
	}, {
		value : 'CCA '
	} ]
});

engine.initialize();

$('.typeahead').typeahead(null, {
	name : 'animals',
	source : engine.ttAdapter()
})