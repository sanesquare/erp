$(function() {
	var availableTags = [ "BASIC_SALARY", "GROSS_SALARY", "DA", "HRA", "CONVEYANCE", "CCA" ];
	var fullTags = [ "+", "-", "*", "/", "%", "(", ")" ]
	var operators = [ "+", "-", "*", "/" ];
	var specials = [ "(", ")" ];
	$.merge(fullTags, availableTags);
	function split(val) {
		return val.split(/ +/);
	}
	function extractLast(term) {
		return split(term).pop();
	}

	checkPreChars = function(input, lastItem, priorItem) {
		if ($.inArray(lastItem.toUpperCase(), operators) < 0) {
			if ($.inArray(priorItem.toUpperCase(), operators) < 0) {
				return input.replace(lastItem, "").trim();
			}
			if (!$.isNumeric(lastItem)) {
				if ($.inArray(lastItem.toUpperCase(), fullTags) < 0) {
					return input.replace(lastItem, "").trim();
				}
			}
		}
		return input;
	}

	checkPreChar = function(lastItem) {
		var selected = input.split(' ');
		return checkPreChar($("#ps-kw-exp").val(), lastItem,
				selected[selected.length - 3]);
	}

	checkLastItem = function(input, lastItem) {
		if (!$.isNumeric(lastItem)) {
			if (lastItem == "(") {
				return input;
			} else if ($.inArray(lastItem.toUpperCase(), availableTags) < 0) {
				return input.replace(lastItem, "").trim();
			}
		}
		return input;
	}

	$.fn.trimErrorString = function(input) {
		var selected = input.split(' ');
		if (selected.length == 2)
			return checkLastItem(input, selected[0]);
		var lastItem = selected[selected.length - 2];
		if (lastItem == ")")
			return input;
		return checkPreChars(input, lastItem, selected[selected.length - 3]);
		return checkLastItem(input, lastItem);
		return input;
	}

	$("#ps-kw-exp").keyup(
			function(event) {
				if (event.keyCode === 32) {
					$("#ps-kw-exp").val(
							$("#ps-kw-exp").trimErrorString(
									$("#ps-kw-exp").val()));
				}
			});

	$("#ps-kw-exp").bind(
			"keydown",
			function(event) {
				if (event.keyCode === $.ui.keyCode.TAB
						&& $(this).autocomplete("instance").menu.active) {
					event.preventDefault();
				}
			}).autocomplete(
			{
				minLength : 0,
				source : function(request, response) {
					// delegate back to autocomplete, but extract the last term
					response($.ui.autocomplete.filter(availableTags,
							extractLast(request.term)));
				},
				focus : function() {
					// prevent value inserted on focus
					return false;
				},
				select : function(event, ui) {
					var terms = split(this.value);
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push(ui.item.value);
					// add placeholder to get the comma-and-space at the end
					terms.push("");
					this.value = terms.join(" ");
					return false;
				}
			});

	$("#ps-kw-non-exp").bind(
			"keydown",
			function(event) {
				if (event.keyCode === $.ui.keyCode.TAB
						&& $(this).autocomplete("instance").menu.active) {
					event.preventDefault();
				}
			}).autocomplete(
			{
				minLength : 0,
				source : function(request, response) {
					// delegate back to autocomplete, but extract the last term
					response($.ui.autocomplete.filter(availableTags,
							extractLast(request.term)));
				},
				focus : function() {
					// prevent value inserted on focus
					return false;
				},
				select : function(event, ui) {
					$(this).val("");
					var terms = split(this.value);
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push(ui.item.value);
					// add placeholder to get the comma-and-space at the end
					// terms.push("");
					this.value = terms;
					return false;
				}
			});

	$('body')
			.on(
					'keydown',
					"#ps-kw-exp",
					function() {
						$(this)
								.bind(
										"keydown",
										function(event) {
											if (event.keyCode === $.ui.keyCode.TAB
													&& $(this).autocomplete(
															"instance").menu.active) {
												event.preventDefault();
											}
										})
								.autocomplete(
										{
											minLength : 0,
											source : function(request, response) {
												response($.ui.autocomplete
														.filter(
																availableTags,
																extractLast(request.term)));
											},
											focus : function() {
												return false;
											},
											select : function(event, ui) {
												var terms = split(this.value);
												terms.pop();
												terms.push(ui.item.value);
												terms.push("");
												this.value = terms.join(" ");
												return false;
											}
										});
						$(this).keyup(
								function(event) {
									if (event.keyCode === 32) {
										$(this).val(
												$(this).trimErrorString(
														$(this).val()));
									}
								});
					});

	$('body')
			.on(
					'keydown',
					"#ps-kw-non-exp",
					function() {
						$(this)
								.bind(
										"keydown",
										function(event) {
											if (event.keyCode === $.ui.keyCode.TAB
													&& $(this).autocomplete(
															"instance").menu.active) {
												event.preventDefault();
											}
										})
								.autocomplete(
										{
											minLength : 0,
											source : function(request, response) {
												response($.ui.autocomplete
														.filter(
																availableTags,
																extractLast(request.term)));
											},
											focus : function() {
												return false;
											},
											select : function(event, ui) {
												$(this).val("");
												var terms = split(this.value);
												terms.pop();
												terms.push(ui.item.value);
												// terms.push("");
												this.value = terms;
												return false;
											}
										});
					});

});