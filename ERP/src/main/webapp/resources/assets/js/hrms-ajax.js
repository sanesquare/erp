/**
 * @author Jithin Mohan
 * @since 12-March-2015
 */
/*----------------------------------------------------------*/

$("#baseCrncySlct").change(function() {
	var currency = $("#baseCrncySlct").val();
	if (currency != "") {
		$.ajax({
			type : "POST",
			url : "readBaseCurrencyDetial.do",
			cache : false,
			async : true,
			data : {
				"currencyName" : currency
			},
			success : function(response) {
				if (response.status == "SUCCESS") {
					$("#crncySgn").val(response.result.sign);
					$("#crncySgn").attr("disabled", "disabled");
				}

			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
});

$("#add_promotion_employee").change(function() {
	var empId = $("#add_promotion_employee").val();
	if (empId != "") {
		$.ajax({
			type : "GET",
			url : "getEmployeeSuperiors.do",
			dataType : "html",
			cache : false,
			async : true,
			data : {
				"employeeId" : empId
			},
			success : function(response) {
				$(".emp-superior-list-div").html(response);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
});

/**
 * auto hide
 */
runEffect = function(div) {
	setTimeout(function() {
		$("#" + div).fadeOut(500);
	}, 3000);
}

saveProfTax = function() {
	var profesionalTaxVo = new Object();
	profesionalTaxVo.amount = $("#profTaxVal").val();
	profesionalTaxVo.ptId = $("#profTaxId").val();
	$("#proft-status-message").fadeIn();
	$.ajax({
		type : 'POST',
		url : 'SaveProfesionalTaxInfo.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(profesionalTaxVo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == "SUCCESS") {
				$("#proft-status-message").html(
						"Profesional tax value has been successfully updated.")
			}
		},
		error : function(requestObject, error, errorThrown) {
		}
	});
	runEffect("proft-status-message");
}

saveLWF = function() {
	var lwfVo = new Object();
	lwfVo.amount = $("#lwfAmount").val();
	lwfVo.lwfId = $("#lwfId").val();
	$("#lwf-status-message").fadeIn();
	$.ajax({
		type : 'POST',
		url : 'SaveLWFInfo.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(lwfVo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == "SUCCESS") {
				$("#lwf-status-message").html(
						"LWF value has been successfully updated.")
			}
		},
		error : function(requestObject, error, errorThrown) {
		}
	});
	runEffect("lwf-status-message");
}

/**
 * section for reminder listing
 */
$("#rm-list-add").click(function() {
	$.ajax({
		type : "GET",
		url : "ReminderList.do",
		cache : false,
		async : true,
		success : function(response) {
			$("#reminder-wrapper").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});

/**
 * section for notification listing
 */
$("#not-list-add").click(function() {
	$.ajax({
		type : "GET",
		url : "notificationList.do",
		cache : false,
		async : true,
		success : function(response) {
			$("#div-notiifcation-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});

/**
 * section for reminder delete
 */
reminderDelete = function(rid) {
	$.ajax({
		type : "POST",
		url : "DeleteReminder.do",
		cache : false,
		async : true,
		data : {
			"reminderId" : rid
		},
		success : function(response) {
			$("#reminder-wrapper").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * save project information
 */

function saveProjectInfo() {
	var projectVo = new Object();
	projectVo.projectTitle = $("#add_project_title").val();
	projectVo.projectStartDate = $("#add_project_startdate").val();
	projectVo.projectEndDate = $("#add_project_enddate").val();
	projectVo.projectDescription = $("#add_project_description").val();
	projectVo.projectAdditionalInformation = $("#add_prjct_nts").val();
	projectVo.employeesIds = $("#add_project_employees").val();
	projectVo.headIds = $("#add_project_head").val();
	$.ajax({
		type : "POST",
		url : 'saveProjectInfo.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(projectVo),
		cache : false,
		async : true,
		success : function(response) {

			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			if (response.status == "Project Successfully Saved.") {
				$("#projectId_add").val(response.result);
				$("#updt_proj_info_btn").css("display", "block");
				$("#submt_proj_info_btn").css("display", "none");
				$("#add_prjc_succs").css('display', 'block');
				setTimeout(function() {
					$('#add_prjc_succs').fadeOut(500);
				}, 3000);
				$("#sbmt_proj_stats_btn").removeAttr('disabled');
				$("#sbmt_proj_doc_btn").removeAttr('disabled');
			} else if (response.status == "Project Save Failed.") {
				$("#add_prjc_err").css('display', 'block');
				setTimeout(function() {
					$('#add_prjc_err').fadeOut(500);
				}, 3000);
			}
		}
	});
}
/**
 * save project status
 */
$("#sbmt_proj_stats_btn").click(function() {
	var projectVo = new Object();
	projectVo.projectStatusId = $("#projectStatusId").val();
	projectVo.projectId = $("#projectId_add").val();
	$.ajax({
		type : "POST",
		url : 'updateProjectStatus.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(projectVo),
		cache : false,
		async : true,
		success : function() {
			$("#prjsts_save_success").css("display", "block");
			setTimeout(function() {
				$("#prjsts_save_success").fadeOut(500);
			}, 1000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#prjsts_save_failed").css("display", "block");
			setTimeout(function() {
				$("#prjsts_save_failed").fadeOut(500);
			}, 1000);
		}
	});
});
/**
 * update project info
 */
function updateProjectInfo() {
	var projectVo = new Object();
	projectVo.projectId = $("#projectId_add").val();
	projectVo.projectTitle = $("#add_project_title").val();
	projectVo.clientId = $("#add_project_client").val();
	projectVo.projectStartDate = $("#add_project_startdate").val();
	projectVo.projectEndDate = $("#add_project_enddate").val();
	projectVo.projectDescription = $("#add_project_description").val();
	projectVo.projectAdditionalInformation = $("#add_prjct_nts").val();
	projectVo.employeesIds = $("#add_project_employees").val();
	projectVo.headIds = $("#add_project_head").val();
	$.ajax({
		type : "POST",
		url : 'updateProjectinformation.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(projectVo),
		cache : false,
		async : true,
		success : function(response) {

			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			if (response.status == "Project Successfully Saved.") {
				$("#add_prjc_succs").css('display', 'block');
				setTimeout(function() {
					$('#add_prjc_succs').fadeOut(500);
				}, 3000);
				$("#sbmt_proj_stats_btn").removeAttr('disabled');
				$("#sbmt_proj_doc_btn").removeAttr('disabled');
			} else if (response.status == "Project Save Failed.") {
				$("#add_prjc_err").css('display', 'block');
				setTimeout(function() {
					$('#add_prjc_err').fadeOut(500);
				}, 3000);
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}
/**
 * save announcements info
 * 
 */
$("#submt_announcements_info_btn").click(function() {
	var announcementsVo = new Object();
	announcementsVo.announcementsTitle = $("#add_announcements_title").val();
	announcementsVo.sendMail = $("#add_announcements_sendMail").val();
	announcementsVo.startDate = $("#add_announcements_startDate").val();
	announcementsVo.endDate = $("#add_announcements_endDate").val();
	announcementsVo.message = $("#add_announcements_message").val();
	announcementsVo.notes = $("#add_announcements_notes").val();

	$.ajax({
		type : "POST",
		url : 'saveAnnouncements.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(announcementsVo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == "Announcements Successfully Saved.") {
				$("#add_announcements_succs").append(response.status);
				$("#add_announcements_succs").css('display', 'block');
			} else if (response.status == "Announcements Save Failed.") {
				$("#add_announcements_err").append(response.status);
				$("#add_announcements_err").css('display', 'block');
			}
		}
	});
});

/**
 * save announcements status
 * 
 */
$("#sbmt_announcements_stats_btn").click(
		function() {
			var announcementsVo = new Object();
			announcementsVo.announcementsStatusId = $("#selectbox").val();
			announcementsVo.announcementsTitle = $("#add_announcements_title")
					.val();
			announcementsVo.announcementsStatusNotes = $(
					"#add_announcements_status_notes").val();
			$.ajax({
				type : "POST",
				url : 'addAnnouncementsStatus.do',
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(announcementsVo),
				cache : false,
				async : true,
				success : function() {
				}
			});
		});

/**
 * save branch information
 */

function saveBranchInfoFunction() {
	var branchVo = new Object();
	branchVo.branchId = $("#add_branch_id").val();

	branchVo.branchType = $("#add_branch_type").val();

	branchVo.branchName = $("#add_branch_name").val();

	branchVo.parentBranchId = $("#add_parent_branch").val();

	branchVo.startDate = $("#add_branch_strdt").val();

	branchVo.timeZoneLocation = $("#add_branch_timeZone").val();

	branchVo.currency = $("#baseCrncySlct").val();

	branchVo.address = $("#add_branch_address").val();

	branchVo.city = $("#add_branch_city").val();

	branchVo.state = $("#add_branch_state").val();

	branchVo.country = $("#add_branch_country").val();

	branchVo.zipCode = $("#add_branch_zipCode").val();

	branchVo.phoneNumber = $("#add_branch_phoneNumber").val();

	branchVo.faxNumber = $("#add_branch_faxNumber").val();

	branchVo.eMail = $("#add_branch_eMail").val();

	branchVo.webSite = $("#add_branch_webSite").val();

	branchVo.latitude = $("#add_branch_latitude").val();

	branchVo.longitude = $("#add_branch_longitude").val();

	branchVo.notes = $("#add_branch_notes").val();

	$.ajax({

		type : "POST",
		url : 'saveBranchInfo.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(branchVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#submt_branch_info_btn").removeAttr("disabled");
			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			if (response.status == "Branch Successfully Saved.") {

				$("#add_branch_id").val(response.result);
				$("#add_branch_success").empty();

				$("#add_branch_success").append(response.status);
				$("#add_branch_success").css('display', 'block');
				setTimeout(function() {
					$('#add_branch_success').fadeOut(500);
				}, 3000);

				$("#sbmt_branch_doc_btn").removeAttr('disabled');

			} else if (response.status == "Branch Successfully Updated.") {
				$("#add_branch_success").empty();
				$("#add_branch_success").append(response.status);
				$("#add_branch_success").css('display', 'block');
				setTimeout(function() {
					$('#add_branch_success').fadeOut(500);
				}, 3000);

				$("#sbmt_branch_doc_btn").removeAttr('disabled');
			} else if (response.status == "Branch Save Failed.") {
				$("#add_branch_error").empty();
				$("#add_branch_error").append(response.status);
				$("#add_branch_error").css('display', 'block');
				setTimeout(function() {
					$('#add_branch_error').fadeOut(500);
				}, 3000);
			}

		}
	});
}

/**
 * update branch information
 */
$("#updt_branch_info_btn").click(function() {
	var branchVo = new Object();
	branchVo.branchType = $("#add_branch_type").val();

	branchVo.branchId = $("#add_branch_id").val();

	branchVo.branchName = $("#add_branch_name").val();

	branchVo.parentBranch = $("#add_parent_branch").val();

	branchVo.timeZoneLocation = $("#add_branch_timeZone").val();

	branchVo.currency = $("#baseCrncySlct").val();

	branchVo.address = $("#add_branch_address").val();

	branchVo.city = $("#add_branch_city").val();

	branchVo.state = $("#add_branch_state").val();

	branchVo.country = $("#add_branch_country").val();

	branchVo.zipCode = $("#add_branch_zipCode").val();

	branchVo.phoneNumber = $("#add_branch_phoneNumber").val();

	branchVo.faxNumber = $("#add_branch_faxNumber").val();

	branchVo.eMail = $("#add_branch_eMail").val();

	branchVo.webSite = $("#add_branch_webSite").val();

	branchVo.latitude = $("#add_branch_latitude").val();

	branchVo.longitude = $("#add_branch_longitude").val();

	branchVo.notes = $("#add_branch_notes").val();

	$.ajax({

		type : "POST",
		url : 'updateBranchInfo.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(branchVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#submt_branch_info_btn").removeAttr("disabled");
			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			if (response.status == "Branch Successfully Updated.") {
				$("#add_branch_success").append(response.status);
				$("#add_branch_success").css('display', 'block');
				setTimeout(function() {
					$('#add_branch_success').fadeOut(500);
				}, 3000);

				$("#sbmt_branch_doc_btn").removeAttr('disabled');
			} else if (response.status == "Branch Updation Failed.") {
				$("#add_branch_error").append(response.status);
				$("#add_branch_error").css('display', 'block');
				setTimeout(function() {
					$('#add_branch_error').fadeOut(500);
				}, 3000);
			}
		}
	});
});

/**
 * Ajax to get all the documents of a selected employeeExit record
 */
$("#emp_ext_doc_tab")
		.click(
				function() {
					var exid = $("#employeeExitId").val();
					if (exid != "") {
						$
								.ajax({
									type : "POST",
									url : "readThisEmployeeExitDocuments.do",
									cache : false,
									async : true,
									data : {
										"exid" : exid
									},
									success : function(response) {
										if (response.status == "SUCCESS") {
											var docListLength = response.result.employeeExitDocuments.length;
											if (docListLength > 0) {
												var listHtml = "<table class='table no-margn'><thead><tr><th>#</th><th>Document Name</th><th>view</th><th>Delete</th></tr></thead><tbody>";
												for (var i = 0; i < docListLength; i++) {
													listHtml = listHtml
															+ "<tr><td>"
															+ (parseInt(i) + 1)
															+ "</td><td><i class='fa fa-file-o'></i> "
															+ response.result.employeeExitDocuments[i].documentName
															+ "</td><td><i class='fa fa-file-text-o sm'></i></td><td><a href='#' onClick='deleteEmpExtDoc("
															+ response.result.employeeExitDocuments[i].emp_ext_doc_id
															+ ","
															+ response.result.employeeExitId
															+ ",\""
															+ response.result.employeeExitDocuments[i].documentUrl
															+ "\")'><i class='fa fa-close ss'></i></a></td></tr>";
												}
												listHtml = listHtml
														+ "</tbody></table>";
												$("#emp_ext_doc_lst").html(
														listHtml);
											} else {
												$("#emp_ext_doc_lst")
														.html(
																"No Documents Available.");
											}
										}
									},
									error : function(requestObject, error,
											errorThrown) {
										alert(errorThrown);
									}
								});
					} else {
						$(".empl-exit-doc-btn").prop("disabled", true);
						$('input#employee-exit-docs').prop('disabled', true);
						$(".empl-exit-doc-status").html(
								"No employee exit details found.");
					}
				});

/**
 * Ajax method to delete selected document of an employeeExit record
 */
deleteEmpExtDoc = function(doc_id, ext_id, url) {
	$
			.ajax({
				type : "POST",
				url : "deleteThisEmployeeExitDocument.do",
				cache : false,
				async : true,
				data : {
					"docId" : doc_id,
					"extId" : ext_id,
					"url" : url
				},
				beforeSend : function() {
					openSpinner();
				},
				success : function(response) {
					if (response.status == "SUCCESS") {
						var docListLength = response.result.employeeExitDocuments.length;
						if (docListLength > 0) {
							var listHtml = "<table class='table no-margn'><thead><tr><th>#</th><th>Document Name</th><th>view</th><th>Delete</th></tr></thead><tbody>";
							for (var i = 0; i < docListLength; i++) {
								listHtml = listHtml
										+ "<tr><td>"
										+ (parseInt(i) + 1)
										+ "</td><td><i class='fa fa-file-o'></i> "
										+ response.result.employeeExitDocuments[i].documentName
										+ "</td><td><i class='fa fa-file-text-o sm'></i></td><td><a href='#' onClick='deleteEmpExtDoc("
										+ response.result.employeeExitDocuments[i].emp_ext_doc_id
										+ ","
										+ response.result.employeeExitId
										+ ",\""
										+ response.result.employeeExitDocuments[i].documentUrl
										+ "\")'><i class='fa fa-close ss'></i></a></td></tr>";
							}
							listHtml = listHtml + "</tbody></table>";
							$("#emp_ext_doc_lst").html(listHtml);
						} else {
							$("#emp_ext_doc_lst").html(
									"No Documents Available.");
						}
					}
					closeSpinner();
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/*******************************************************************************
 * SAVE DESIGNATION.... EMPLOYEE ADD PAGE
 */
$("#addemp_btn_sv_dsgntn").click(function() {
	var desigVo = new Object();
	desigVo.designation = $("#desgntion_employee_add_popup").val();
	$.ajax({
		type : "POST",
		url : 'empNewDesignation.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(desigVo),
		cache : false,
		async : true,
		success : function(response) {
			$(".simplePopupBackground").fadeOut("fast");
			alert(response.result.designation.length);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});
/*******************************************************************************
 * SAVE GRADE.... EMPLOYEE ADD PAGE
 */
$("#emp_btn_add_grd").click(function() {
	var gradeVo = new Object();
	gradeVo.employeeGrade = $("#emp_add_grd_pop").val();
	$.ajax({
		type : "POST",
		url : 'empNewGrade.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(gradeVo),
		cache : false,
		async : true,
		success : function(response) {
			$(".simplePopupBackground").fadeOut("fast");
			alert(response.result.grade.length);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});

/*******************************************************************************
 * SAVE USERNAME AND PASSWORD.... EMPLOYEE ADD PAGE
 */
function saveUserPassWord() {
	$("#save_emp_user_info").attr("disabled", "true");
	var userVo = new Object();
	if ($("#emp_user_allow").prop("checked")) {
		userVo.allowLogin = "true";
		a = "true";
	} else {
		userVo.allowLogin = "false";
		a = "false";
	}
	userVo.userName = $("#emp_user_name").val();
	userVo.password = $("#emp_user_pwd").val();
	userVo.email = $("#emp_user_email").val();
	userVo.employeeId = $("#empid").val();
	$.ajax({
		type : "POST",
		url : 'empNewUser.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(userVo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status != 'exists') {
				$("#user_save_success").css("display", "block");
				setTimeout(function() {
					$('#user_save_success').fadeOut(500);
				}, 3000);
			} else {
				$("#emp_user_pwd").val('');
				$("#username_exists").css("display", "block");
				setTimeout(function() {
					$('#username_exists').fadeOut(500);
				}, 2000);
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#user_save_failed").css("display", "block");
			setTimeout(function() {
				$('#user_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#save_emp_user_info").removeAttr("disabled");
}

/*******************************************************************************
 * SAVE PERSONAL INFORMATION.... EMPLOYEE ADD PAGE
 */
function saveEmpPersonalInfo() {
	$("#emp_uprofile_sbmit").attr("disabled", "true");
	var userVo = new Object();
	userVo.salutation = $("#emp_uprofile_salut").val();
	userVo.firstName = $("#emp_uprofile_first_name").val();
	userVo.lastName = $("#emp_uprofile_last_name").val();
	userVo.dateOfBirth = $("#emp_uprofile_dob").val();
	userVo.gender = $("input[name=gender]:checked").val();
	userVo.bloodGroupId = $("#emp_uprofile_blood").val();
	userVo.relegionId = $("#emp_uprofile_relegion").val();
	userVo.nationalityId = $("#emp_uprofile_nationality").val();
	userVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'empNewUserProfile.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(userVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#personal_save_success").css("display", "block");
			setTimeout(function() {
				$('#personal_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#personal_save_failed").css("display", "block");
			setTimeout(function() {
				$('#personal_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_uprofile_sbmit").removeAttr("disabled");
}

/**
 * SAVE EMPLOYEE ADDITIONAL INFO
 * 
 */

function saveAdditionalInfo() {
	$("#emp_add_info_btn").attr("disabled", "true");
	var userVo = new Object();
	userVo.joiningDate = $("#emp_add_info_joindate").val();
	userVo.passportExpiration = $("#emp_add_info_pasprtexprdate").val();
	userVo.passportNumber = $("#emp_add_info_pasprtnum").val();
	userVo.licenceExpiration = $("#emp_add_info_dlexprnum").val();
	userVo.licenceNumber = $("#emp_add_info_dlnum").val();
	userVo.goovernmentId = $("#emp_add_info_govt_idnum").val();
	userVo.employeeTaxNumber = $("#emp_add_info_govt_taxnum").val();
	userVo.pfId = $("#emp_add_info_pf").val();
	userVo.esiId = $("#emp_add_info_esi").val();
	userVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'empAdditionalInfoSave.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(userVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#additional_save_success").css("display", "block");
			setTimeout(function() {
				$('#additional_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#additional_save_failed").css("display", "block");
			setTimeout(function() {
				$('#additional_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_add_info_btn").removeAttr("disabled");
}

/**
 * SAVE EMPLOYEE ADDRESS
 * 
 */

function saveAddressEmp() {
	$("#emp_prmnt_cntct_btn").attr("disabled", "true");
	var addressVo = new Object();
	addressVo.address = $("#emp_prmnt_cntct_addr").val();
	addressVo.city = $("#emp_prmnt_cntct_city").val();
	addressVo.state = $("#emp_prmnt_cntct_stt").val();
	addressVo.zipCode = $("#emp_prmnt_cntct_zip").val();
	addressVo.countryId = $("#emp_prmnt_cntct_cntry").val();
	addressVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'empAddressSave.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(addressVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#address_save_success").css("display", "block");
			setTimeout(function() {
				$('#address_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#address_save_failed").css("display", "block");
			setTimeout(function() {
				$('#address_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_prmnt_cntct_btn").removeAttr("disabled");
}

/**
 * SAVE EMPLOYEE SUPERIORS & SUBORDINATES
 * 
 */

$("#emp_spriors_sbrdnts_btn").click(function() {
	$("#emp_spriors_sbrdnts_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.employeeId = $("#empid").val();
	empVo.subordinatesIds = $("#emp_sbrdnts_ids").val();
	empVo.superiorsIds = $("#emp_spriors_ids").val();
	$.ajax({
		type : "POST",
		url : 'empSpriosSubdntsSave.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#reporting_save_success").css("display", "block");
			setTimeout(function() {
				$('#reporting_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#reporting_save_failed").css("display", "block");
			setTimeout(function() {
				$('#reporting_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_spriors_sbrdnts_btn").removeAttr("disabled");
});

/**
 * SAVE EMPLOYEE PHONE NUMBERS
 * 
 */

$("#emp_phone_btn").click(function() {
	$("#emp_phone_btn").attr("disabled", "true");
	if ($("#emp_phone_mobie").val() != '') {
		var phoneVo = new Object();
		phoneVo.homePhoneNumber = $("#emp_phone_home").val();
		phoneVo.officePhoneNumber = $("#emp_phone_office").val();
		phoneVo.mobileNumber = $("#emp_phone_mobie").val();
		phoneVo.employeeId = $("#empid").val();

		$.ajax({
			type : "POST",
			url : 'empPhoneSave.do',
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(phoneVo),
			cache : false,
			async : true,
			success : function(response) {
				$("#phone_save_success").css("display", "block");
				setTimeout(function() {
					$('#phone_save_success').fadeOut(500);
				}, 3000);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#phone_save_failed").css("display", "block");
				setTimeout(function() {
					$('#phone_save_failed').fadeOut(500);
				}, 3000);
			}
		});
	} else {
		$("#emp_phone_mobie").focus();
	}
	$("#emp_phone_btn").removeAttr("disabled");
});

/**
 * SAVE EMPLOYEE STATUS
 * 
 */

$("#emp_stts_sve_btn").click(function() {
	var phoneVo = new Object();
	phoneVo.statusId = $("#emp_stts_sve_stst").val();
	phoneVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'empStatusUpdate.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(phoneVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#status_save_success").css("display", "block");
			setTimeout(function() {
				$('#status_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#status_save_failed").css("display", "block");
			setTimeout(function() {
				$('#status_save_failed').fadeOut(500);
			}, 3000);
		}
	});
});

/**
 * SAVE EMPLOYEE NOTIFICATION BY EMAIL
 * 
 */

$("#emp_notify_chk").click(function() {
	if ($("#empid").val() != '') {
		var phoneVo = new Object();
		if ($("#emp_notify_chk").prop('checked')) {
			phoneVo.notifyByEmail = "true";
		} else {
			phoneVo.notifyByEmail = "false";
		}
		phoneVo.employeeId = $("#empid").val();
		$.ajax({
			type : "POST",
			url : 'empNotifyByEmailUpdate.do',
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(phoneVo),
			cache : false,
			async : true,
			success : function(response) {
				$("#notification_save_success").css("display", "block");
				setTimeout(function() {
					$('#notification_save_success').fadeOut(500);
				}, 3000);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#notification_save_failed").css("display", "block");
				setTimeout(function() {
					$('#notification_save_failed').fadeOut(500);
				}, 3000);
			}
		});
	}
});

/**
 * SAVE EMPLOYEE NOTES
 * 
 */

$("#emp_add_info_btn_uodt").click(function() {
	$("#emp_add_info_btn_uodt").attr("disabled", "true");
	if ($("#emp_add_inf_notes").val() != '') {
		var empVo = new Object();
		empVo.notes = $("#emp_add_inf_notes").val();
		empVo.employeeId = $("#empid").val();

		$.ajax({
			type : "POST",
			url : 'empNotesSave.do',
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(empVo),
			cache : false,
			async : true,
			success : function(response) {
				$("#notes_save_success").css("display", "block");
				setTimeout(function() {
					$('#notes_save_success').fadeOut(500);
				}, 3000);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#notes_save_failed").css("display", "block");
				setTimeout(function() {
					$('#notes_save_failed').fadeOut(500);
				}, 3000);
			}
		});
	} else {
		$("#emp_add_inf_notes").focus();
	}
	$("#emp_add_info_btn_uodt").removeAttr("disabled");
});

/**
 * SAVE EMPLOYEE EMERGENCY CONTACT
 * 
 */

function saveEmergencyCntct() {
	$("#emp_emrgncy_cntct_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.emergencyPerson = $("#emp_emrgncy_cntct_person").val();
	empVo.emergencyRelation = $("#emp_emrgncy_cntct_relation").val();
	empVo.emergencyMobile = $("#emp_emrgncy_cntct_phone").val();
	empVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'empEmergencyContact.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#emergency_save_success").css("display", "block");
			setTimeout(function() {
				$('#emergency_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#emergency_save_failed").css("display", "block");
			setTimeout(function() {
				$('#emergency_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_emrgncy_cntct_btn").removeAttr("disabled");
}
/**
 * Ajax to delete selected termination
 */
deleteTermination = function(tid) {
	$
			.ajax({
				type : "POST",
				url : "deleteThisTermination.do",
				cache : false,
				async : true,
				data : {
					"ter_Id" : tid
				},
				success : function(response) {
					if (response.status == "SUCCESS") {
						if (response.result == null) {
							$("#termination-list")
									.html(
											"<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='basic-datatable'> <thead> <tr> <th width='20%'>Employee</th> <th width='20%'>Termination Date</th> <th width='20%'>Approval Status</th> <th width='20%'>Edit</th> <th width='20%'>Delete</th> </tr> </thead> <tbody></tbody></table><a href='NewTermination'> <button type='button' class='btn btn-primary'>Add New Termination</button> </a>");
							$('#termination-list table')
									.last()
									.dataTable(
											{
												"sDom" : "<'row'<'col-sm-6'l><'col-sm-6'f>r>"
														+ "<'row'<'col-sm-12 table-responsive't>>"
														+ "<'row'<'col-sm-6'i><'col-sm-6'p>>"
											});
						} else
							(response.result.length > 0)
						{
							var terHtml = "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='basic-datatable'> <thead> <tr> <th width='20%'>Employee</th> <th width='20%'>Termination Date</th> <th width='20%'>Approval Status</th> <th width='20%'>Edit</th> <th width='20%'>Delete</th> </tr> </thead> <tbody>";
							for (var i = 0; i < response.result.length; i++) {
								terHtml = terHtml
										+ "<tr> <td><a href='ViewTermination?otp_tid='"
										+ response.result[i].terminationId
										+ ">"
										+ response.result[i].employee
										+ "</td></a> <td><a href='ViewTermination?otp_tid='"
										+ response.result[i].terminationId
										+ ">"
										+ response.result[i].termiantedDate
										+ "</a></td> <a href='ViewTermination?otp_tid='"
										+ response.result[i].terminationId
										+ "><td>"
										+ response.result[i].terminationStatus
										+ "</td></a> <td><a href='NewTermination?tid="
										+ response.result[i].terminationId
										+ "'><i class='fa fa-edit sm'></i> </a></td> <td><a href='#' id='terminationDelId' title='"
										+ response.result[i].terminationId
										+ "'><i class='fa fa-close ss'></i></a></td> </tr>";
							}
							terHtml = terHtml
									+ "</tbody></table><a href='NewTermination'> <button type='button' class='btn btn-primary'>Add New Termination</button> </a>";
							$("#termination-list").html(terHtml);
							$('#termination-list table')
									.last()
									.dataTable(
											{
												"sDom" : "<'row'<'col-sm-6'l><'col-sm-6'f>r>"
														+ "<'row'<'col-sm-12 table-responsive't>>"
														+ "<'row'<'col-sm-6'i><'col-sm-6'p>>"
											});
						}

					}
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
/*******************************************************************************
 * 
 * @param tid
 */
deleteOrganizationPolicy = function(opid) {
	$.ajax({
		type : "POST",
		url : "DeleteOrganizationPolicy.do",
		cache : false,
		async : true,
		data : {
			"opi" : opid
		},
		success : function(response) {
			$("#organization-policy-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * SAVE EMPLOYEE QUALIFICATION INFO
 * 
 */

function saveEmpQualification() {
	$("#emp_qulfctn_add_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.degreeId = $("#emp_qulfctn_add_dgre").val();
	empVo.subject = $("#emp_qulfctn_add_sbjct").val();
	empVo.institute = $("#emp_qulfctn_add_instt").val();
	empVo.gpa = $("#emp_qulfctn_add_gpa").val();
	empVo.graduationYear = $("#emp_qulfctn_add_grdtnyr").val();
	empVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'empQualfctionSave.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#emp_qulfctn_add_dgre").val('');
			$("#emp_qulfctn_add_sbjct").val('');
			$("#emp_qulfctn_add_instt").val('');
			$("#emp_qulfctn_add_gpa").val('');
			$("#emp_qulfctn_add_grdtnyr").val('');

			$("#qlfctn_save_success").css("display", "block");
			setTimeout(function() {
				$('#qlfctn_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#qlfctn_save_failed").css("display", "block");
			setTimeout(function() {
				$('#qlfctn_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_qulfctn_add_btn").removeAttr("disabled");
}

/**
 * get qualification
 */

function getEmpQualification() {
	var empVo = new Object();
	empVo.degreeId = $("#emp_qulfctn_add_dgre").val();
	empVo.employeeId = $("#empid").val();
	$.ajax({
		type : "POST",
		url : 'empQualfctionGet.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			var d = response.result;
			$("#emp_qulfctn_add_sbjct").val(d.subject);
			$("#emp_qulfctn_add_instt").val(d.institute);
			$("#emp_qulfctn_add_gpa").val(d.gpa);
			$("#emp_qulfctn_add_grdtnyr").val(d.graduationYear);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * SAVE EMPLOYEE BANK ACCOUNT INFO
 * 
 */

function saveEmployeeBank() {
	$("#emp_bank_add_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.bankName = $("#emp_bank_add_name").val();
	empVo.branchName = $("#emp_bank_add_branch").val();
	empVo.branchCode = $("#emp_bank_add_branch_code").val();
	empVo.accountTitle = $("#emp_bank_add_titel").val();
	empVo.accountNumber = $("#emp_bank_add_acno").val();
	empVo.typeId = $("#emp_bank_add_type").val();
	empVo.employeeId = $("#empid").val();
	$.ajax({
		type : "POST",
		url : 'empBankAccount.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#bank_save_success").css("display", "block");
			setTimeout(function() {
				$('#bank_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#bank_save_failed").css("display", "block");
			setTimeout(function() {
				$('#bank_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_bank_add_btn").removeAttr("disabled");
}

/**
 * SAVE EMPLOYEE BENEFIT INFO
 * 
 */

$("#emp_benfit_btn").click(function() {
	$("#emp_benfit_btn").attr("disabled", "true");
	if ($("#emp_benfit_title").val() != '') {
		var empVo = new Object();
		empVo.benefitTitle = $("#emp_benfit_title").val();
		empVo.benefitDetails = $("#emp_benfit_details").val();
		empVo.employeeId = $("#empid").val();

		$.ajax({
			type : "POST",
			url : 'empBeneftDetails.do',
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(empVo),
			cache : false,
			async : true,
			success : function(response) {
				$("#emp_benfit_details").val('');
				$("#emp_benfit_title").val('');

				$("#benefit_save_success").css("display", "block");
				setTimeout(function() {
					$('#benefit_save_success').fadeOut(500);
				}, 3000);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#benefit_save_failed").css("display", "block");
				setTimeout(function() {
					$('#benefit_save_failed').fadeOut(500);
				}, 3000);
			}
		});
	} else {
		$("#emp_benfit_title").focus();
	}
	$("#emp_benfit_btn").removeAttr("disabled");
});

/**
 * SAVE EMPLOYEE NEXT OF KIN INFO
 * 
 */

function saveEmpNextOfKin() {
	$("#emp_add_nxtkin_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.nextOfKinName = $("#emp_add_nxtkin_name").val();
	empVo.nextOfKinDOB = $("#emp_add_nxtkin_dob").val();
	empVo.nextOFKinRelation = $("#emp_add_nxtkin_rltn").val();
	empVo.nextOfKinAddress = $("#emp_add_nxtkin_addrs").val();
	empVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'empNextOfKIn.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#emp_add_nxtkin_name").val('');
			$("#emp_add_nxtkin_dob").val('');
			$("#emp_add_nxtkin_rltn").val('');
			$("#emp_add_nxtkin_addrs").val('');

			$("#skin_save_success").css("display", "block");
			setTimeout(function() {
				$('#skin_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#skin_save_failed").css("display", "block");
			setTimeout(function() {
				$('#skin_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_add_nxtkin_btn").removeAttr("disabled");
}

/**
 * SAVE EMPLOYEE DEPENDANTS
 * 
 */

function saveEmpDepandants() {
	$("#emp_depnts_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.dependantName = $("#emp_depnts_nm").val();
	empVo.dependantDateOfBirth = $("#emp_depnts_dob").val();
	empVo.dependantRelation = $("#emp_depnts_rltn").val();
	empVo.dependantNominee = $("input[name=nominee]:checked").val();
	empVo.address = $("#emp_depnts_adrs").val();
	empVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'empDependants.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#emp_depnts_nm").val('');
			$("#emp_depnts_dob").val('');
			$("#emp_depnts_rltn").val('');
			$("input[name=nominee]:checked").prop("checked", false);
			$("#emp_depnts_adrs").val('');

			$("#dpndnts_save_success").css("display", "block");
			setTimeout(function() {
				$('#dpndnts_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#dpndnts_save_failed").css("display", "block");
			setTimeout(function() {
				$('#dpndnts_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_depnts_btn").removeAttr("disabled");
}

/**
 * SAVE EMPLOYEE REFERENCE
 * 
 */

function saveEmployeReference() {
	$("#emp_rfrnc_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.referenceName = $("#emp_rfrnc_name").val();
	empVo.referenceOrganisationName = $("#emp_rfrnc_orgnztn").val();
	empVo.referencePhoneNUmber = $("#emp_rfrnc_phn").val();
	empVo.referenceEmail = $("#emp_rfrnc_eml").val();
	empVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'emprrReferenceSave.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#rfrnce_save_success").css("display", "block");
			setTimeout(function() {
				$('#rfrnce_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#rfrnce_save_failed").css("display", "block");
			setTimeout(function() {
				$('#rfrnce_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_rfrnc_btn").removeAttr("disabled");
}

/**
 * SAVE EMPLOYEE SKILL
 * 
 */

function saveEmpSkill() {
	$("#emp_skll_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.skillId = $("#emp_skll_skill").val();
	empVo.skillLevel = $("#emp_skll_lvl").val();
	empVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'emprrSkillSave.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#emp_skll_lvl").val('');
			$("#emp_skll_skill").val('');

			$("#skill_save_success").css("display", "block");
			setTimeout(function() {
				$('#skill_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#skill_save_failed").css("display", "block");
			setTimeout(function() {
				$('#skill_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_skll_btn").removeAttr("disabled");
}

/**
 * get skill
 */
function getEmpSkill() {
	var empVo = new Object();
	empVo.skillId = $("#emp_skll_skill").val();
	empVo.employeeId = $("#empid").val();
	$.ajax({
		type : "POST",
		url : 'empSkillGet.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			var d = response.result;
			$("#emp_skll_lvl").val(d.skillLevel);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * SAVE RELEGION
 * 
 */

$("#add_emp_relgn_pop_btn").click(function() {
	var empVo = new Object();
	empVo.relegion = $("#add_emp_relgn_pop").val();

	$.ajax({
		type : "POST",
		url : 'newRelgn.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			alert("failed");
		}
	});
});

/**
 * SAVE EMPLOYEE UNIFORM
 * 
 */

function saveEmpUniform() {
	$("#emp_unfrm_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.uniformCheckOutDate = $("#emp_unfrm_dt").val();
	empVo.uniformAdvanceAmount = $("#emp_unfrm_amt").val();
	empVo.uniformDuration = $("#emp_unfrm_drtn").val();
	empVo.uniformReimbursementDate = $("#emp_unfrm_rmbrsmntdt").val();
	empVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'emprUnfrmSve.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#emp_unfrm_dt").val('');
			$("#emp_unfrm_amt").val('');
			$("#emp_unfrm_drtn").val('');
			$("#emp_unfrm_rmbrsmntdt").val('');

			$("#uniform_save_success").css("display", "block");
			setTimeout(function() {
				$('#uniform_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#uniform_save_failed").css("display", "block");
			setTimeout(function() {
				$('#uniform_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_unfrm_btn").removeAttr("disabled");
}

/**
 * SAVE EMPLOYEE ASSETS
 * 
 */

function saveEmpAssets() {
	$("#emp_assts_sve_btn").attr("disabeld", "true");
	var empVo = new Object();
	empVo.assetId = $("#emp_assts_ast").val();
	empVo.assetCheckOut = $("#emp_assts_sve_chnkout").val();
	empVo.assetCheckin = $("#emp_assts_sve_chnkin").val();
	empVo.assetDescription = $("#emp_assts_sve_dsc").val();
	empVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'emprAssetsSve.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#emp_assts_ast").val('');
			$("#emp_assts_sve_chnkout").val('');
			$("#emp_assts_sve_chnkin").val('');
			$("#emp_assts_sve_dsc").val('');

			$("#assets_save_success").css("display", "block");
			setTimeout(function() {
				$('#assets_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#assets_save_failed").css("display", "block");
			setTimeout(function() {
				$('#assets_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_assts_sve_btn").removeAttr("disabeld");
}

/*******************************************************************************
 * get assets
 */
function getEmpAssets() {
	var empVo = new Object();
	empVo.assetId = $("#emp_assts_ast").val();
	empVo.employeeId = $("#empid").val();
	$.ajax({
		type : "POST",
		url : 'emprAssetsGet.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			var d = response.result;
			$("#emp_assts_sve_chnkout").val(d.assetCheckout);
			$("#emp_assts_sve_dsc").val(d.description);
			if (d.assetCheckin != null) {
				$("#emp_assts_sve_chnkin").val(d.assetCheckin);
				$('#asset_checkin').css('display', 'block');
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * SAVE EMPLOYEE LANGUAGE INFO
 * 
 */

function saveEmployeeLanguage() {
	$("#emp_lngge_add_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.languageId = $("#emp_lngge_add_lngg").val();
	empVo.speakingLevel = $("#emp_lngge_add_spklvl").val();
	empVo.readingLevel = $("#emp_lngge_add_redlvl").val();
	empVo.writingLevel = $("#emp_lngge_add_wrtlvl").val();
	empVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'empLanguageSave.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#emp_lngge_add_lngg").val('');
			$("#emp_lngge_add_spklvl").val('');
			$("#emp_lngge_add_redlvl").val('');
			$("#emp_lngge_add_wrtlvl").val('');

			$("#lnguage_save_success").css("display", "block");
			setTimeout(function() {
				$('#lnguage_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#lnguage_save_failed").css("display", "block");
			setTimeout(function() {
				$('#lnguage_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_lngge_add_btn").removeAttr("disabled");
}

/*******************************************************************************
 * get language
 */
function getEmpLanguage() {
	var empVo = new Object();
	empVo.languageId = $("#emp_lngge_add_lngg").val();
	empVo.employeeId = $("#empid").val();
	$.ajax({
		type : "POST",
		url : 'empLanguageGet.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			var d = response.result;
			$("#emp_lngge_add_spklvl").val(d.speakingLevel);
			$("#emp_lngge_add_redlvl").val(d.readingLevel);
			$("#emp_lngge_add_wrtlvl").val(d.writingLevel);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * SAVE EMPLOYEE EXPERIENCE INFO
 * 
 */

function saveEmpExperience() {
	$("#emp_exprnc_add_btn").attr("disabled", "true");
	var empVo = new Object();
	empVo.organisationName = $("#emp_exprnc_add_orgn").val();
	empVo.designationId = $("#emp_exprnc_add_job").val();
	empVo.jobFieldId = $("#emp_exprnc_add_field").val();
	empVo.jobDescription = $("#emp_exprnc_add_desc").val();
	empVo.jobStartDate = $("#emp_exprnc_add_strtdt").val();
	empVo.jobEndDate = $("#emp_exprnc_add_enddt").val();
	empVo.startingSalary = $("#emp_exprnc_add_strtsly").val();
	empVo.endingSalary = $("#emp_exprnc_add_endsly").val();
	empVo.employeeId = $("#empid").val();

	$.ajax({
		type : "POST",
		url : 'empExprenceSave.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(empVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#emp_exprnc_add_orgn").val('');
			$("#emp_exprnc_add_job").val('');
			$("#emp_exprnc_add_field").val('');
			$("#emp_exprnc_add_desc").val('');
			$("#emp_exprnc_add_strtdt").val('');
			$("#emp_exprnc_add_enddt").val('');
			$("#emp_exprnc_add_strtsly").val('');
			$("#emp_exprnc_add_endsly").val('');

			$("#exprnce_save_success").css("display", "block");
			setTimeout(function() {
				$('#exprnce_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#exprnce_save_failed").css("display", "block");
			setTimeout(function() {
				$('#exprnce_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#emp_exprnc_add_btn").removeAttr("disabled");
}

/** *************** */

function getSuperiorsSubordinate(supIds, subIds) {
	var supId = [];
	supId = supIds.replace(/[[\]]/g, '').split(',');
	var subId = [];
	subId = subIds.replace(/[[\]]/g, '').split(',');
	if ($("#des_id_emp").val() != '') {
		var vo = new Object();
		vo.designationId = $("#des_id_emp").val();
		$.ajax({
			type : "POST",
			url : 'getSuperiorSubordinate.do',
			cache : false,
			async : true,
			data : {
				"id" : $("#des_id_emp").val()
			},
			success : function(success) {
				var sup = success.result.superiors;
				var sub = success.result.subordinates;

				$("#emp_spriors_ids option").remove();
				$("#emp_sbrdnts_ids option").remove();

				var superior = '';
				$.each(sup, function(i, item) {
					superior += "<option value=" + item.superiorId + ">"
							+ item.superiorName + " (" + item.superiorCode
							+ ") </option>";
				});

				var subordinate = '';
				$.each(sub, function(i, item) {
					subordinate += "<option value=" + item.subordinateId + ">"
							+ item.subordinateName + " ("
							+ item.subordinateCode + ") </option>";
				});
				$('#emp_spriors_ids').append(superior);
				$('#emp_sbrdnts_ids').append(subordinate);
				$('#emp_spriors_ids').val(supId);
				$('#emp_sbrdnts_ids').val(subId);

				$('#emp_spriors_ids').chosen().trigger("chosen:updated");
				$('#emp_sbrdnts_ids').chosen().trigger("chosen:updated");

				$('#emp_spriors_ids').selectpicker('refresh');
				$('#emp_sbrdnts_ids').selectpicker('refresh');

			},
			error : function(requestObject, error, errorThrown) {
				alert("failed" + errorThrown);
			}
		});
	}
}

/*******************************************************************************
 * save employee leave
 */
function saveEmployeeLeave() {
	$("#save_emp_leave_btn").attr("disabled", "true");
	var vo = new Object();
	vo.employeeId = $("#empid").val();
	vo.leaveId = $("#leaveId").val();
	vo.leaveTitleId = $("#save_emp_leave_title").val();
	vo.leaveType = $("#save_emp_leave_type").val();
	vo.leaves = $("#save_emp_leave_leaves").val();
	vo.leaveStatus = $("#save_emp_leave_status").val();
	vo.carryOver = $("input[name=leave]:checked").val();
	$.ajax({
		type : "POST",
		url : 'empLeaveSave.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(vo),
		cache : false,
		async : true,
		success : function(response) {
			$("#leave_save_success").css("display", "block");
			setTimeout(function() {
				$('#leave_save_success').fadeOut(500);
			}, 3000);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#leave_save_failed").css("display", "block");
			setTimeout(function() {
				$('#leave_save_failed').fadeOut(500);
			}, 3000);
		}
	});
	$("#save_emp_leave_btn").removeAttr("disabled");
}

function getLeaveDetails() {
	var vo = new Object();
	vo.employeeId = $("#empid").val();
	vo.leaveTitleId = $("#save_emp_leave_title").val();
	$.ajax({
		type : "POST",
		url : 'empLeaveGet.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(vo),
		cache : false,
		async : true,
		success : function(response) {
			var d = response.result;
			$("#save_emp_leave_leaves").val(d.leaves);
			$("#save_emp_leave_type").val(d.leaveType);
			$("#save_emp_leave_status").val(d.leaveStatus);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/*******************************************************************************
 * get bank details
 */
$("#emp_bank_add_type").on("change", function() {
	var vo = new Object();
	vo.employeeId = $("#empid").val();
	vo.typeId = $("#emp_bank_add_type").val();
	$.ajax({
		type : "POST",
		url : 'getEmployeeBankAccount.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(vo),
		cache : false,
		async : true,
		success : function(response) {
			var d = response.result;
			$("#emp_bank_add_name").val(d.bankName);
			$("#emp_bank_add_branch").val(d.branchName);
			$("#emp_bank_add_branch_code").val(d.branchCode);
			$("#emp_bank_add_titel").val(d.accountTitle);
			$("#emp_bank_add_acno").val(d.accountNumber);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});

/** *********************************************************************************************************************************************************** */
/**
 * function to save basic job post details
 */

function savePostInfo() {
	var jobPostVo = new Object;
	jobPostVo.jobPostId = $("#add_post_id").val();
	jobPostVo.jobTitle = $("#add_post_title").val();
	jobPostVo.departmentId = $("#add_post_dep").val();
	jobPostVo.jobTypeId = $("#add_post_jobTypes").val();
	jobPostVo.positions = $("#add_post_positions").val();
	jobPostVo.ageStart = $("#add_post_ageStart").val();
	jobPostVo.ageEnd = $("#add_post_ageEnd").val();
	jobPostVo.branchIds = $("#add_post_location").val();
	jobPostVo.startSalary = $("#add_post_salaryStrt").val();
	jobPostVo.endSalary = $("#add_post_salaryEnd").val();
	jobPostVo.jobPostEndDate = $("#add_post_endDate").val();

	$.ajax({

		type : "POST",
		url : 'saveJobPostBasicInfo.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobPostVo),
		cache : false,
		async : true,
		success : function(response) {

			if (response.status == "Job Post Successfully Saved.") {

				$("#add_post_id").val(response.result);
				$("#add_post_success").empty();

				$("#add_postInfo_success").append(response.status);
				$("#add_postInfo_success").css('display', 'block');
				setTimeout(function() {
					$("#add_postInfo_success").fadeOut(500);

				}, 2000);
				$("#add_post_qualification").focus();
				enableJobPostButtons();
			} else if (response.status == "Job Post Successfully Updated.") {
				$("#add_postInfo_success").empty();
				$("#add_postInfo_success").append(response.status);
				$("#add_postInfo_success").css('display', 'block');
				setTimeout(function() {
					$("#add_postInfo_success").fadeOut(500);
				}, 3000);
				$("#add_post_qualification").focus();

			} else if (response.status == "Job Post Save Failed.") {
				$("#add_postInfo_error").empty();
				$("#add_postInfo_error").append(response.status);
				$("#add_postInfo_error").css('display', 'block');
				setTimeout(function() {
					$("#add_postInfo_error").fadeOut(500);
				}, 3000);
				$("#add_post_dep").focus();
			}

		},
		error : function(response) {

			alert(response.status);
		}
	});
}

/**
 * method to save job post qualification
 * 
 */
function sbmtPostQual() {
	var jobPostVo = new Object();
	jobPostVo.qualification = $("#add_post_qualification").val();
	jobPostVo.jobPostId = $("#add_post_id").val();
	$.ajax({
		type : "POST",
		url : 'saveJobPostQual.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobPostVo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == "Job Post Successfully Saved.") {
				$("#add_postQual_success").empty();

				$("#add_postQual_success").append("Saved Successfully...");
				$("#add_postQual_success").css('display', 'block');
				setTimeout(function() {
					$("#add_postQual_success").fadeOut(500);

				}, 2000);
				$("#add_post_months").focus();
			}

			else if (response.status == "Job Post Save Failed.") {
				$("#add_postQual_error").empty();
				$("#add_postQual_error").append("Save Failed...");
				$("#add_postQual_error").css('display', 'block');
				setTimeout(function() {
					$("#add_postQual_error").fadeOut(500);
				}, 3000);

			}
		},
		error : function(response) {
			alert(response.status);
		}
	});

}
/**
 * method to save job post experience
 */
function submtPostExp() {
	var jobPostVo = new Object();
	jobPostVo.experienceMonths = $("#add_post_months").val();
	jobPostVo.experienceYears = $("#add_post_years").val();
	jobPostVo.jobPostId = $("#add_post_id").val();
	$.ajax({
		type : "POST",
		url : 'saveJobPostExp.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobPostVo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == "Job Post Successfully Saved.") {
				$("#add_postExp_success").empty();

				$("#add_postExp_success").append("Saved Successfully...");
				$("#add_postExp_success").css('display', 'block');
				setTimeout(function() {
					$("#add_postExp_success").fadeOut(500);

				}, 2000);
				$("#add_postExp_qualification").focus();
			}

			else if (response.status == "Job Post Save Failed.") {
				$("#add_postExp_error").empty();
				$("#add_postExp_error").append("Save Failed...");
				$("#add_postExp_error").css('display', 'block');
				setTimeout(function() {
					$("#add_postExp_error").fadeOut(500);
				}, 3000);

			}
		},
		error : function(response) {
			alert(response.status);
		}
	});
}
/**
 * method to save post description
 */
function sbmtPostDesc() {
	var jobPostVo = new Object();
	jobPostVo.postDescription = $("#add_post_description").val();
	jobPostVo.jobPostId = $("#add_post_id").val();
	$.ajax({
		type : "POST",
		url : 'savePostDescription.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobPostVo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == "Job Post Successfully Saved.") {
				$("#add_postDesc_success").empty();

				$("#add_postDesc_success").append("Saved Successfully...");
				$("#add_postDesc_success").css('display', 'block');
				setTimeout(function() {
					$("#add_postDesc_success").fadeOut(500);

				}, 2000);

			}

			else if (response.status == "Job Post Save Failed.") {
				$("#add_postDesc_error").empty();
				$("#add_postDesc_error").append("Save Failed...");
				$("#add_postDesc_error").css('display', 'block');
				setTimeout(function() {
					$("#add_postDesc_error").fadeOut(500);
				}, 3000);

			}
		},
		error : function(response) {
			alert(response.status);
		}
	});
}
/**
 * method to save additional info
 */
function sbmtPostAddInfo() {
	var jobPostVo = new Object();
	jobPostVo.postAdditionalInfo = $("#add_post_additionalInfo").val();
	jobPostVo.jobPostId = $("#add_post_id").val();
	$.ajax({
		type : "POST",
		url : 'savePostAdditionalInfo.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobPostVo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == "Job Post Successfully Saved.") {
				$("#add_postAdd_success").empty();

				$("#add_postAdd_success").append("Saved Successfully...");
				$("#add_postAdd_success").css('display', 'block');
				setTimeout(function() {
					$("#add_postAdd_success").fadeOut(500);

				}, 2000);
			}

			else if (response.status == "Job Post Save Failed.") {
				$("#add_postAdd_error").empty();
				$("#add_postAdd_error").append("SAve Failed...");
				$("#add_postAdd_error").css('display', 'block');
				setTimeout(function() {
					$("#add_postAdd_error").fadeOut(500);
				}, 3000);

			}

		},
		error : function(response) {
			alert(response.status);
		}
	});
}

deleteEmployeeExit = function(ext_id) {
	$
			.ajax({
				type : "POST",
				url : "deleteThisEmployeeExit.do",
				cache : false,
				async : true,
				data : {
					"ext_id" : ext_id
				},
				success : function(response) {
					if (response.status == "SUCCESS") {
						if (response.result == null) {
							$("#employeexit-list")
									.html(
											"<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='basic-datatable'> <thead><tr> <th width='35%'>Employee Name</th> <th width='35%'>Exit Date</th> <th width='15%'>Edit</th> <th width='15%'>Delete</th> </tr></thead> <tbody></tbody></table><a href='AddEmployeeExit'> <button type='button' class='btn btn-primary'>Add Employee Exit</button> </a>");
							$('#employeexit-list table')
									.last()
									.dataTable(
											{
												"sDom" : "<'row'<'col-sm-6'l><'col-sm-6'f>r>"
														+ "<'row'<'col-sm-12 table-responsive't>>"
														+ "<'row'<'col-sm-6'i><'col-sm-6'p>>"
											});
						} else
							(response.result.length > 0)
						{
							var terHtml = "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='basic-datatable'> <thead><tr> <th width='35%'>Employee Name</th> <th width='35%'>Exit Date</th> <th width='15%'>Edit</th> <th width='15%'>Delete</th> </tr></thead> <tbody>";
							for (var i = 0; i < response.result.length; i++) {
								terHtml = terHtml
										+ "<tr><td><a href='ViewEmployeeExit?otp="
										+ response.result[i].employeeExitId
										+ "'>"
										+ esponse.result[i].employee
										+ "</a></td> <td><a href='ViewEmployeeExit?otp="
										+ response.result[i].employeeExitId
										+ "'>"
										+ response.result[i].exitStringDate
										+ "</a></td> <td><a href='EditEmployeeExit?exid="
										+ response.result[i].employeeExitId
										+ "'><i class='fa fa-edit sm'></i> </a></td> <td><a href='#' onclick='deleteEmployeeExit("
										+ response.result[i].employeeExitId
										+ ")'><i class='fa fa-close ss'></i></a></td> </tr>";
							}
							terHtml = terHtml
									+ "</tbody></table><a href='AddEmployeeExit'> <button type='button' class='btn btn-primary'>Add Employee Exit</button> </a>";
							$("#employeexit-list").html(terHtml);
							$('#employeexit-list table')
									.last()
									.dataTable(
											{
												"sDom" : "<'row'<'col-sm-6'l><'col-sm-6'f>r>"
														+ "<'row'<'col-sm-12 table-responsive't>>"
														+ "<'row'<'col-sm-6'i><'col-sm-6'p>>"
											});
						}

					}
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/**
 * Ajax method to load all termination documents
 */
readAllTerminationDocuments = function(terId) {
	$
			.ajax({
				type : "POST",
				url : "readAllTerminationDocuments.do",
				cache : false,
				async : true,
				data : {
					"terId" : terId
				},
				success : function(response) {
					if (response.status == "SUCCESS") {
						var docListLength = response.result.terminationDocumentsVos.length;
						if (docListLength > 0) {
							var listHtml = "<table class='table no-margn'><thead><tr><th>#</th><th>Document Name</th><th>view</th><th>Delete</th></tr></thead><tbody>";
							for (var i = 0; i < docListLength; i++) {
								listHtml = listHtml
										+ "<tr><td>"
										+ (parseInt(i) + 1)
										+ "</td><td><i class='fa fa-file-o'></i> "
										+ response.result.terminationDocumentsVos[i].documentName
										+ "</td><td><i class='fa fa-file-text-o sm'></i></td><td><a href='#' onClick='deleteTerminationDoc("
										+ response.result.terminationDocumentsVos[i].ter_doc_id
										+ ","
										+ response.result.terminationId
										+ ",\""
										+ response.result.terminationDocumentsVos[i].documentUrl
										+ "\")'><i class='fa fa-close ss'></i></a></td></tr>";
							}
							listHtml = listHtml + "</tbody></table>";
							$("#emp_ter_doc_lst").html(listHtml);
						} else {
							$("#emp_ter_doc_lst").html(
									"No Documents Available.");
						}
					}
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}
$("#terminate-doc-tab").click(
		function() {
			var terId = $("#terminationId").val();
			if (terId != "") {
				readAllTerminationDocuments(terId);
			} else {
				$(".empl-termi-doc-btn").prop("disabled", true);
				$('input#employee-termination-docs').prop('disabled', true);
				$(".empl-termi-doc-status").html(
						"No employee termination details found.");
			}
		});

$("#org-policy-doc-tab").click(function() {
	/*
	 * var terId = $("#org_policy_id").val(); if (terId != "") {
	 * listAllOrgPolicyDocuments(terId); } else {
	 * $(".org-policy-doc-btn").prop("disabled", true);
	 * $('input#org-policy-docs').prop('disabled', true);
	 * $(".org-policy-doc-status").html( "No organization policy details
	 * found."); }
	 */
});

listAllOrgPolicyDocuments = function() {
	$.ajax({
		type : "GET",
		url : "readAllOrgPolicyDocuments.do",
		/*
		 * data : { "opid" : otp_wid },
		 */
		cache : false,
		async : true,
		success : function(response) {
			$("#org-policy-doc-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

uploadOrgPolicyDocument = function() {
	var data = new FormData();
	jQuery.each(jQuery('#org-policy-docs')[0].files, function(i, file) {
		data.append('file-' + i, file);
	});
	$.ajax({
		type : "POST",
		url : "UploadOrgPolicyDocument.do",
		data : data,
		enctype : 'multipart/form-data',
		cache : false,
		async : true,
		processData : false,
		contentType : false,
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listAllOrgPolicyDocuments();
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

deleteOrganizationPolicyDocument = function(docId, docUrl) {
	$.ajax({
		type : "POST",
		url : "DeleteOrganizationPolicyDocument.do",
		cache : false,
		async : true,
		data : {
			"documentId" : docId,
			"documentUrl" : docUrl
		},
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listAllOrgPolicyDocuments();
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function openSpinner() {
	document.getElementById("modal").style.display = 'block';
	document.getElementById("fade").style.display = 'block';
}

function closeSpinner() {
	document.getElementById("modal").style.display = 'none';
	document.getElementById("fade").style.display = 'none';
}
deleteTerminationDoc = function(doc_id, ter_id, url) {
	$
			.ajax({
				type : "POST",
				url : "deleteThisTerminationDocument.do",
				cache : false,
				async : true,
				data : {
					"docId" : doc_id,
					"terId" : ter_id,
					"url" : url
				},
				beforeSend : function() {
					openSpinner();
				},
				success : function(response) {
					if (response.status == "SUCCESS") {
						var docListLength = response.result.terminationDocumentsVos.length;
						if (docListLength > 0) {
							var listHtml = "<table class='table no-margn'><thead><tr><th>#</th><th>Document Name</th><th>view</th><th>Delete</th></tr></thead><tbody>";
							for (var i = 0; i < docListLength; i++) {
								listHtml = listHtml
										+ "<tr><td>"
										+ (parseInt(i) + 1)
										+ "</td><td><i class='fa fa-file-o'></i> "
										+ response.result.terminationDocumentsVos[i].documentName
										+ "</td><td><i class='fa fa-file-text-o sm'></i></td><td><a href='#' onClick='deleteTerminationDoc("
										+ response.result.terminationDocumentsVos[i].ter_doc_id
										+ ","
										+ response.result.terminationId
										+ ",\""
										+ response.result.terminationDocumentsVos[i].documentUrl
										+ "\")'><i class='fa fa-close ss'></i></a></td></tr>";
							}
							listHtml = listHtml + "</tbody></table>";
							$("#emp_ter_doc_lst").html(listHtml);
						} else {
							$("#emp_ter_doc_lst").html(
									"No Documents Available.");
						}
					}
					closeSpinner();
				}
			});
}
/**
 * section for notification delete
 */
notificationDelete = function(nid) {
	$.ajax({
		type : "POST",
		url : "deleteNotification.do",
		data : {
			notificationId : nid
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#div-notiifcation-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * section for reminder delete
 */
notificationEdit = function(nid) {
	$.ajax({
		type : "POST",
		url : "editNotification.do",
		data : {
			notificationId : nid
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#div-notiifcation-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}
/**
 * section for warning delete
 */
deleteWarning = function(ot_wid) {
	$.ajax({
		type : "POST",
		url : "deleteThisWarning.do",
		data : {
			"warningId" : ot_wid
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#warning-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * section for list all warning documents
 */
listAllWarningDocuments = function(otp_wid) {
	$.ajax({
		type : "POST",
		url : "warningDocumentsList.do",
		data : {
			"warningId" : otp_wid
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#warning-document-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

listAllAnnouncementDocuments = function(otp_aid) {
	$.ajax({
		type : "POST",
		url : "AnnouncementDocumentsList.do",
		data : {
			"announcementId" : otp_aid
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#announcement-document-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

$("#warning-doc-tab").click(function() {
	var warId = $("#warningId").val();
	if (warId != "") {
		listAllWarningDocuments(warId);
	} else {
		$(".empl-warning-doc-btn").prop("disabled", true);
		$('input#employee-warning-docs').prop('disabled', true);
		$(".empl-warning-doc-status").html("No warning details found.");
	}
});

$("#announcement-doc-tab").click(function() {
	var annId = $("#announcementsId").val();
	if (annId != "") {
		listAllAnnouncementDocuments(annId);
	} else {
		$(".announcement-doc-btn").prop("disabled", true);
		$('input#announcements-docs').prop('disabled', true);
		$(".announcement-doc-status").html("No announcement details found.");
	}
});

/**
 * section for delete warning document
 */
deleteWarningDocument = function(warId, warDocId, docUrl) {
	$.ajax({
		type : "POST",
		url : "warningDocumentDelete.do",
		cache : false,
		async : true,
		data : {
			"documentId" : warDocId,
			"documentUrl" : docUrl
		},
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listAllWarningDocuments(warId);
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * section for delete announcement document
 */
deleteAnnouncementDocument = function(annId, annDocId, docUrl) {
	$.ajax({
		type : "POST",
		url : "AnnouncementDocumentDelete.do",
		cache : false,
		async : true,
		data : {
			"documentId" : annDocId,
			"documentUrl" : docUrl
		},
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listAllAnnouncementDocuments(annId);
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * section for memo
 */
deleteThisMemo = function(memoId) {
	$.ajax({
		type : "POST",
		url : "deleteThisMemo.do",
		data : {
			"otp_mid" : memoId
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#memo-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

$("#memo-doc-tab").click(function() {
	var memoId = $("#memoId").val();
	if (memoId != "") {
		listAllMemoDocuments(memoId);
	} else {
		$(".empl-memo-doc-btn").prop("disabled", true);
		$('input#employee-memo-docs').prop('disabled', true);
		$(".empl-memo-doc-status").html("No memo details found.");
	}
});

listAllMemoDocuments = function(memoId) {
	$.ajax({
		type : "POST",
		url : "memoDocumentsList.do",
		data : {
			"memoId" : memoId
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#memo-document-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

deleteThisMemoDocument = function(memoId, docId, docUrl) {
	$.ajax({
		type : "POST",
		url : "memoDocumentDelete.do",
		cache : false,
		async : true,
		data : {
			"documentId" : docId,
			"documentUrl" : docUrl
		},
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listAllMemoDocuments(memoId);
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/*******************************************************************************
 * EMPLOYEE JOINING
 */

function getEmployees() {
	var branchId = $("#emp_join_brnch").val();
	var deptId = $("#emp_join_dprmnt").val();
	if (branchId != '' && deptId != '') {
		var obj = new Object();
		obj.branchId = branchId;
		obj.departmentId = deptId;

		$.ajax({
			type : "POST",
			url : 'getEmployeeByBranchAndDepartment.do',
			data : {
				"branchId" : branchId,
				"departmentId" : deptId
			},
			cache : false,
			async : true,
			success : function(response) {
				$("#emp_join_emp option").remove();
				$("#hrly_wges_emp option").remove();
				$("#add_project_employees option").remove();
				$("#add_project_head option").remove();
				$("#leave_report_employees option").remove();
				var data = response.result;
				var options = '';
				var reportOptions = '<option value="">All employee</option>';
				$.each(data, function(index, item) {
					options += '<option value="' + item.employeeId + '">'
							+ item.name + ' (' + item.employeeCode
							+ ')</option>';
					reportOptions += '<option value="' + item.employeeId + '">'
							+ item.name + ' (' + item.employeeCode
							+ ')</option>';
				});

				$("#emp_join_emp").append(options);
				$('#emp_join_emp').chosen().trigger("chosen:updated");

				// project page
				$("#add_project_employees").append(options);
				$('#add_project_employees').chosen().trigger("chosen:updated");

				$("#add_project_head").append(options);
				$('#add_project_head').chosen().trigger("chosen:updated");

				// hourly wages page
				$("#hrly_wges_emp").append(options);
				$('#hrly_wges_emp').chosen().trigger("chosen:updated");

				// leave report
				$("#leave_report_employees").append(reportOptions);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	} else {
		if (branchId == '') {
			$("#emp_join_brnch").focus();
		} else {
			$("#emp_join_dprmnt").focus();
		}
	}
}
function getEmployeeDetails() {
	var employeeCode = $("#employeeCode").val();
	if (employeeCode != '') {
		$.ajax({
			type : "POST",
			url : 'getEmployeeDetailsById.do',
			data : {
				"employeeCode" : employeeCode,
			},
			cache : false,
			async : true,
			success : function(response) {
				var data = response.result;
				$("#emp_join_joindate").val(data.joiningDate);
				$("#emp_join_empname").text(data.employeeName);
				$("#emp_join_empemail").text(data.email);
				$("#emp_join_empdesignation").text(data.designation);

				$("#emp_join_empdepartment").text(data.employeeDepartment);
				$("#cntrct_emp_dept").text(data.employeeDepartment);

				$("#emp_join_empbranch").text(data.employeeBranch);
				$("#cntrct_emp_brnch").text(data.employeeBranch);

				$("#cntrct_emp_grde").text(data.employeeGrade);
				$("#cntrct_emp_desg").text(data.designation);
				$("#cntrct_emp_type").text(data.employeeType);
				$("#cntrct_emp_ctgry").text(data.employeeCategory);

				$("#emp_join_empjoinfdt").text(data.joiningDate);
				$("#emp_join_emptype").val(data.employeeTypeId);
				$("#emp_join_emptdes").val(data.designationId);
				$("#emp_join_emptbrnch").val(data.employeeBranchId);
				$("#emp_join_emptdept").val(data.employeeDepartmentId);

				$("#emp-joninig_date").val(data.joiningDate);

				// project
				try {
					fillProject(data);
				} catch (e) {
				}

				// reimbursements
				try {
					fillReimbursement(data);
				} catch (e) {
				}

				// hourly wages
				try {
					fillHourlyWage(data);
				} catch (e) {
				}

				// daily wage
				try {
					fillDailyWage(data);
				} catch (e) {
				}
				// salary
				try {
					fillSalary(data);
				} catch (e) {
				}

				// Leave
				try {
					fillLeave(data);
				} catch (e) {
				}

				// Travel
				try {
					fillTravel(data);
				} catch (e) {
				}

				// Transfer
				try {
					fillTransfer(data);
				} catch (e) {
				}

				// resignation
				try {
					fillResignation(data);
				} catch (e) {
				}

				// commission
				try {
					fillCommission(data)
				} catch (e) {
				}

			},
			error : function(requestObject, error, errorThrown) {
				alert("ERROR:  " + errorThrown);
			}
		});
	}
}

/**
 * function to get payslip items by employee
 * 
 * @param employeeCode
 */
function getPayslipItemsByEmployee() {
	var employeeCode = $("#employeeCode").val();
	var fromMonth = $("#fromMOnth").val();
	var toMonth = $("#toMonth").val();
	var fromYear = $("#fromYear").val();
	var toYear = $("#toYear").val();
	$("#payslipTable tbody tr").remove();
	$
			.ajax({
				type : "GET",
				url : 'getPayslipDetailsByEmployee.do',
				data : {
					"employeeCode" : employeeCode,
					"fromMonth" : fromMonth,
					"toMonth" : toMonth,
					"fromYear" : fromYear,
					"toYear" : toYear
				},
				cache : false,
				async : true,
				success : function(response) {
					var tableData = "";
					var rowCount = $("#payslipTable tbody tr").length;
					try {
						var ite = response.result;
						$
								.each(
										ite,
										function(i, it) {
											tableData += "<tr><td> Salary : </td><td>From : "
													+ it.fromDate
													+ " - To :"
													+ it.toDate + "</td></tr>";
											var data = it.items;
											$
													.each(
															data,
															function(index,
																	item) {
																rowCount = parseInt(rowCount) + 1;
																tableData += '<tr><td>'
																		+ rowCount
																		+ '</td>'
																		+ '<td><input type="text" name="title'
																		+ rowCount
																		+ '" readOnly="readOnly" style="background-color:#fff !important" class="form-control" value="'
																		+ item.title
																		+ '"/></td>'
																		+ '<td><input type="text" name="amount'
																		+ rowCount
																		+ '" readOnly="readOnly" style="background-color:#fff !important" class="form-control" value="'
																		+ item.amount
																		+ '"/></td></tr>';
															});
											tableData += "<tr><td> Total : </td><td></td><td>"
													+ it.total + "</td></tr>";
										});
					} catch (e) {
					}
					$("#payslipTable").append(tableData);
					// $("#payslip_total_amount").val(response.result.total);
					$("#salary_payslip_table").css("display", "block");
				},
				error : function(requestObject, error, errorThrown) {
					alert("failed  " + errorThrown);
				}
			});
}

/**
 * function to fill salary details
 */
function fillSalary(data) {
	$("#salary_type").val(data.salaryTypeId);
	$("#salry_type_id").val(data.salaryTypeId);
	$("#salary_daily_rate").val(data.dailyWage);
	$("#salary_overtime_hoyr_wage").val(data.overTimeHoursAmount);
	$("#salary_regular_hoyr_wage").val(data.regularHoursAmount);
	filterSalaryUi();
}

function filterSalaryUi() {
	var type = $("#salary_type").val();
	if (type == 1) {
		$("#daily").css("display", "none");
		$("#hourly").css("display", "none");
		$("#monthly").css("display", "block");
	} else if (type == 2) {
		$("#daily").css("display", "block");
		$("#hourly").css("display", "none");
		$("#monthly").css("display", "none");
	} else if (type == 3) {
		$("#daily").css("display", "none");
		$("#hourly").css("display", "block");
		$("#monthly").css("display", "none");
	} else {
		$("#daily").css("display", "none");
		$("#hourly").css("display", "none");
		$("#monthly").css("display", "none");
	}
}

/**
 * method to fill project details
 * 
 * @param data
 */
function fillProject(data) {
	try {
		$("#assgnmt_add_prjct option").remove();
		var project = '<option value="">Select Project</option>';
		var dd = data.projectVo;
		$.each(dd, function(index, item) {
			project += '<option value="' + item.projectId + '">'
					+ item.projectTitle + '</option>';
			$("#assgnmt_add_prjct").html(project);
		});
	} catch (e) {
	}
}

/**
 * method to fil reimbursements
 * 
 * @param data
 */
function fillReimbursement(data) {
	try {
		var superiors = data.superiorSubordinateVo.superiors;

		$("#reimbursements_superiors option").remove();
		$("#reimbursements_superiors_status option").remove();
		var options = '';
		$.each(superiors, function(index, item) {
			options += "<option value='" + item.superiorId + "'>"
					+ item.superiorName + " (" + item.superiorCode + ")"
					+ "</option>";
		});
		$("#reimbursements_superiors").append(options);
		try {
			$("#reimbursements_superiors").val(
					($("#reimbursements_suprior_ajx_hidn").val()).split(","));
		} catch (e) {
		}
		$('#reimbursements_superiors').chosen().trigger("chosen:updated");

		$("#reimbursements_superiors_status").append(options);
		$('#reimbursements_superiors_status').chosen()
				.trigger("chosen:updated");

	} catch (e) {
	}
}

/**
 * method to fill hourly wage
 */
function fillHourlyWage(data) {
	$("#hrly_wges_rhamt").val(data.regularHoursAmount);
	$("#hrly_wges_otamnt").val(data.overTimeHoursAmount);
	$("#hourlyWagesId").val(data.hourlyWagesId);
	$("#hrly_wges_descrptn").val(data.hourlyWagesDescription);
	$("#hrly_wges_notes").val(data.hourlyWagesNotes);

}

/**
 * method to fill leave details
 * 
 * @param data
 */
function fillLeave(data) {
	var leave = data.leaveVos;
	$("#leave_type option").remove();

	var options = '<option value="">--Select Leave Type--</option>';
	$.each(leave, function(index, item) {
		options += "<option value='" + item.leaveTitleId + "'>"
				+ item.leaveTitle + "</option>";
	});
	$("#leave_type").append(options);
	try {
		$("#leave_type").val($("#leave_type_ajx_hidn").val());
	} catch (e) {
	}

	var superiors = data.superiorSubordinateVo.superiors;
	$("#joining_sups option").remove();
	$("#leave_superiors option").remove();
	var options = '';
	$.each(superiors, function(index, item) {
		options += "<option value='" + item.superiorId + "'>"
				+ item.superiorName + " (" + item.superiorCode + ")"
				+ "</option>";

	});
	$("#leave_superiors").append(options);
	try {
		$("#leave_superiors").val(
				($("#leave_suprior_ajx_hidn").val()).split(","));
	} catch (e) {
	}
	$('#leave_superiors').chosen().trigger("chosen:updated");
	$("#joining_sups").append(options);
	$("#joining_sups").multiselect('rebuild');
	$('#joining_sups').multiselect('refresh');
}

// function to fill commission
function fillCommission(data) {
	var superiors = data.superiorSubordinateVo.superiors;
	$("#commission_superiors option").remove();
	var options = '';
	$.each(superiors, function(index, item) {
		options += "<option value='" + item.superiorId + "'>"
				+ item.superiorName + " (" + item.superiorCode + ")"
				+ "</option>";

	});
	$("#commission_superiors").append(options);
	try {
		$("#commission_superiors").val(
				($("#leave_suprior_ajx_hidn").val()).split(","));
	} catch (e) {
	}
	$('#commission_superiors').chosen().trigger("chosen:updated");
}

/**
 * method to fill travel details
 * 
 * @param data
 */
function fillTravel(data) {
	var superiors = data.superiorSubordinateVo.superiors;
	$("#travel_superior option").remove();
	var optionsTravel = '<option value="">--Select--</option>';
	$.each(superiors, function(index, item) {
		optionsTravel += "<option value='" + item.superiorCode + "'>"
				+ item.superiorName + " (" + item.superiorCode + ")"
				+ "</option>";
	});
	$("#travel_superior").html(optionsTravel);
	try {
		$("#travel_superior").val($("#travel_sprior_hdn").val());
	} catch (e) {
	}
}

/**
 * function fill transfer details
 * 
 * @param data
 */
function fillTransfer(data) {
	var superiors = data.superiorSubordinateVo.superiors;
	$("#transfer_superior option").remove();
	var optionsTravel = '<option value="">--Select--</option>';
	$.each(superiors, function(index, item) {
		optionsTravel += "<option value='" + item.superiorCode + "'>"
				+ item.superiorName + " (" + item.superiorCode + ")"
				+ "</option>";
	});
	$("#transfer_superior").html(optionsTravel);
	try {
		$("#transfer_superior").val($("#transfer_sprior_hdn").val());
	} catch (e) {
	}
}

/**
 * function to fill resignation
 * 
 * @param data
 */
function fillResignation(data) {
	var superiors = data.superiorSubordinateVo.superiors;
	$("#resignation_superior option").remove();
	var optionsTravel = '<option value="">--Select--</option>';
	$.each(superiors, function(index, item) {
		optionsTravel += "<option value='" + item.superiorCode + "'>"
				+ item.superiorName + " (" + item.superiorCode + ")"
				+ "</option>";
	});
	$("#resignation_superior").html(optionsTravel);
	try {
		$("#resignation_superior").val($("#resignation_sprior_hdn").val());
	} catch (e) {
	}
}

/**
 * method to fill daily wage
 * 
 * @param data
 */
function fillDailyWage(data) {
	$("#daily_wges_title").val(data.dailyWageTitle);
	$("#daily_wges_wage").val(data.dailyWage);
	$("#daily_wges_descrptn").val(data.dailyWageDescription);
	$("#daily_wges_notes").val(data.dailyWageNotes);
	$("#dailyWageId").val(data.dailyWageId);
}
/*******************************************************************************
 * MEETING
 */
function getEmployeeees() {
	var branchIds = [];
	var departmentIds = [];
	var url = '';

	branchIds = $("#drop_one").val();
	departmentIds = $("#drop_two").val();
	if (branchIds != null || departmentIds != null) {
		var vo = new Object();
		vo.branchIds = branchIds;
		vo.departmentIds = departmentIds;

		if (branchIds != null && departmentIds != null) {
			url = "getEmployeesByBranchAndDepartment.do";
		} else {
			if (branchIds == null) {
				url = "getEmployeeesByDepartment.do";
			} else {
				url = "getEmployeeesByBranch.do";
			}
		}
		alert(JSON.stringify(vo));
		$.ajax({
			type : "POST",
			url : url,
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(vo),
			cache : false,
			async : true,
			success : function(response) {

				var data = response.result;

				$("#drop_three option").remove();
				$("#term_sms_emp option").remove();
				var options = '';
				$.each(data, function(index, item) {
					if (item.name != null) {
						options += '<option value="' + item.employeeId + '">'
								+ item.name + ' ( ' + item.employeeCode
								+ ' )</option>';
					}
				});
				try {
					$("#drop_three").append(options);
					$("#drop_three").multiselect('rebuild');
					$('#drop_three').multiselect('refresh');
				} catch (e) {
				}
				try {
					$("#term_sms_emp").append(options);
					$('#term_sms_emp').chosen().trigger("chosen:updated");
				} catch (e) {
				}
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

function getEmployeWithBranchId() {
	var branchId = $("#drop_one").val();
	var vo=new Object();
	vo.branchId=branchId;
	vo.departmentIds=$("#drop_two").val();
		$.ajax({
			type : "POST",
			url : "employeesByBranchId.do",
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(vo),
			cache : false,
			async : true,
			success : function(response) {

				var data = response.result;

				$("#drop_three option").remove();
				$("#term_sms_emp option").remove();
				var options = '';
				$.each(data, function(index, item) {
					if (item.name != null) {
						options += '<option value="' + item.employeeId + '">'
								+ item.name + ' ( ' + item.employeeCode
								+ ' )</option>';
					}
				});
				try {
					$("#drop_three").append(options);
					$("#drop_three").multiselect('rebuild');
					$('#drop_three').multiselect('refresh');
				} catch (e) {
				}
				try {
					$("#term_sms_emp").append(options);
					$('#term_sms_emp').chosen().trigger("chosen:updated");
				} catch (e) {
				}
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
}

/*******************************************************************************
 * HOURLY WAGES
 */
function getEmployeeWageDetails() {
	var employeeCode = $("#employeeCode").val();

	getEmployeeInformation(employeeCode);

	$("#hrly_wges_rhamt").val('');
	$("#hrly_wges_otamnt").val('');
	$("#hrly_wges_descrptn").val('');
	$("#hrly_wges_notes").val('');
	$("#hourlyWagesId").val('');
	if (employeeCode != '') {
		$.ajax({
			type : "POST",
			url : 'hourlyWagesGetEmployeeWages.do',
			data : {
				"employeeCode" : employeeCode,
			},
			cache : false,
			async : true,
			success : function(response) {

				var data = response.result;

				$("#hrly_wges_rhamt").val(data.regularHoursAmount);
				$("#hrly_wges_otamnt").val(data.overTimeHoursAmount);
				$("#hourlyWagesId").val(data.hourlyWagesId);
				$("#hrly_wges_descrptn").val(data.hourlyWagesDescription);
				$("#hrly_wges_notes").val(data.hourlyWagesNotes);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

function getEmployeeInformation(employeeCode) {
	var supId = '';
	try {
		supId = ($("#commission_suprior_ajx_hidn").val()).split(",");
	} catch (e) {

	}
	if (employeeCode != '') {
		$
				.ajax({
					type : "POST",
					url : 'getEmployeeDetailsById.do',
					data : {
						"employeeCode" : employeeCode,
					},
					cache : false,
					async : true,
					success : function(response) {
						var data = response.result;
						$("#hrly_wges_joindt").text(data.joiningDate);
						$("#emp_join_empname").text(data.employeeName);
						$("#hrly_wges_mail").text(data.email);
						$("#hrly_wges_dsgnation").text(data.designation);
						$("#hrly_wges_dprmtnt").text(data.employeeDepartment);
						$("#hrly_wges_brnch").text(data.employeeBranch);

						var superiors = response.result.superiorSubordinateVo.superiors;

						$("#commission_superiors option").remove();
						var options = '';
						$.each(superiors, function(index, item) {
							options += "<option value='" + item.superiorId
									+ "'>" + item.superiorCode + "</option>";
						});
						$("#commission_superiors").append(options);
						$("#commission_superiors").val(supId);
						$('#commission_superiors').chosen().trigger(
								"chosen:updated");
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
					}
				});
	}
}

/*******************************************************************************
 * DAILY WAGES
 */
function getEmployeeDailyWageDetails() {
	var employeeCode = $("#employeeCode").val();

	getEmployeeInformation(employeeCode);

	$("#daily_wges_title").val('');
	$("#daily_wges_wage").val('');
	$("#daily_wges_descrptn").val('');
	$("#daily_wges_notes").val('');
	$("#dailyWageId").val('');
	if (employeeCode != '') {
		$.ajax({
			type : "POST",
			url : 'dailyWagesGetEmployeeWages.do',
			data : {
				"employeeCode" : employeeCode,
			},
			cache : false,
			async : true,
			success : function(response) {

				var data = response.result;

				$("#daily_wges_title").val(data.dailyWageTitle);
				$("#daily_wges_wage").val(data.dailyWage);
				$("#daily_wges_descrptn").val(data.dailyWageDescription);
				$("#daily_wges_notes").val(data.dailyWageNotes);
				$("#dailyWageId").val(data.dailyWageId);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

/*******************************************************************************
 * BONUS
 */
function getBonusByEmployeeAndTitle() {
	var employeeCode = $("#employeeCode").val();
	var titleId = $("#bonus_title").val();

	$("#bonus_bonusid").val('');
	$("#bonus_amount").val('');
	$("#bonus_date").val('');
	$("#bonus_description").val('');
	$("#bonus_notes").val('');

	getEmployeeInformation(employeeCode);
	if (employeeCode != '' && titleId != '') {
		$.ajax({
			type : "POST",
			url : 'getBonusByEmployeeAndTitle.do',
			data : {
				"employeeCode" : employeeCode,
				"titleId" : titleId
			},
			cache : false,
			async : true,
			success : function(response) {
				if (response.status == 'ok') {
					var data = response.result;
					$("#bonus_bonusid").val(data.bonusId);
					$("#bonus_amount").val(data.amount);
					$("#bonus_date").val(data.date);
					$("#bonus_description").val(data.description);
					$("#bonus_notes").val(data.notes);
				} else {
					$("#bonus_bonusid").val($("#bonus_hidden_id").val());
				}
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

/*******************************************************************************
 * DEDUCTION
 */
function getDeductionByEmployeeAndTitle() {
	var employeeCode = $("#employeeCode").val();
	var titleId = $("#deductio_title").val();
	getEmployeeInformation(employeeCode);
	if (employeeCode != '' && titleId != '') {
		$("#deductionId").val("");
		$("#deduction_amount").val("");
		$("#deduction_date").val("");
		$("#deduction_description").val("");
		$("#deduction_notes").val("");
		$.ajax({
			type : "POST",
			url : 'getDeductionByEmployeeAndTitle.do',
			data : {
				"employeeCode" : employeeCode,
				"titleId" : titleId
			},
			cache : false,
			async : true,
			success : function(response) {
				if (response.status == 'ok') {
					var data = response.result;
					$("#deductionId").val(data.deductionId);
					$("#deduction_amount").val(data.amount);
					$("#deduction_date").val(data.date);
					$("#deduction_description").val(data.description);
					$("#deduction_notes").val(data.notes);
				} else {
					$("#deductionId").val($("#hiden_id_deduction").val());
				}
			},
			error : function(requestObject, error, errorThrown) {
				alert("error" + errorThrown);
			}
		});
	}
}

/*******************************************************************************
 * COMMISSION
 */
function getCommissionByEmployeeAndTitle() {
	var employeeCode = $("#employeeCode").val();
	var titleId = $("#commission_title").val();

	$("#commissionId").val('');
	$("#commission_amount").val('');
	$("#commission_date").val('');
	$("#commission_description").val('');
	$("#commission_notes").val('');

	getEmployeeInformation(employeeCode);
	if (employeeCode != '' && titleId != '') {
		$.ajax({
			type : "POST",
			url : 'getCommissionByEmployeeAndTitle.do',
			data : {
				"employeeCode" : employeeCode,
				"titleId" : titleId
			},
			cache : false,
			async : true,
			success : function(response) {
				if (response.status == 'ok') {
					var data = response.result;
					$("#commissionId").val(data.commissionId);
					$("#commission_amount").val(data.amount);
					$("#commission_date").val(data.date);
					$("#commission_description").val(data.description);
					$("#commission_notes").val(data.notes);

					var supId = (data.superiorIds).split(",");

					$("#commission_superiors").val(data.superiorIds);
					$('#commission_superiors').chosen().trigger(
							"chosen:updated");
				} else {
					$("#commissionId").val($("#id").val());
				}
			},
			error : function(requestObject, error, errorThrown) {
				alert("error" + errorThrown);
			}
		});
	}
}

/*******************************************************************************
 * ADJUSTMENTS
 */
function getAdjustmentByEmployeeAndTitle() {
	var employeeCode = $("#employeeCode").val();
	var titleId = $("#adjustment_title").val();

	$("#adjustment_amount").val('');
	$("#adjustment_date").val('');
	$("#adjustment_description").val('');
	$("#adjustment_notes").val('');
	$("#adjustment_type").val('');
	$("#adjustmentId").val("");

	getEmployeeInformation(employeeCode);

	if (employeeCode != '' && titleId != '') {
		$.ajax({
			type : "POST",
			url : 'getAdjustmentByEmployeeAndTitle.do',
			data : {
				"employeeCode" : employeeCode,
				"titleId" : titleId
			},
			cache : false,
			async : true,
			success : function(response) {
				if (response.status == 'ok') {
					var data = response.result;
					$("#adjustmentId").val(data.adjustmentId);
					$("#adjustment_amount").val(data.amount);
					$("#adjustment_date").val(data.date);
					$("#adjustment_description").val(data.description);
					$("#adjustment_notes").val(data.notes);
					$("#adjustment_type").val(data.type);

				} else {
					$("#adjustmentId").val($("#id").val());
				}
			},
			error : function(requestObject, error, errorThrown) {
				alert("error" + errorThrown);
			}
		});
	}
}

/*******************************************************************************
 * REIMBURSEMENTS
 */
function getDetilsOfEmployee() {
	var employeeCode = $("#employeeCode").val();
	try {
		supId = ($("#reimbursements_suprior_ajx_hidn").val()).split(",");
	} catch (e) {

	}
	if (employeeCode != '') {
		$
				.ajax({
					type : "POST",
					url : 'getEmployeeDetailsById.do',
					data : {
						"employeeCode" : employeeCode,
					},
					cache : false,
					async : true,
					success : function(response) {
						var data = response.result;
						/*
						 * $("#hrly_wges_joindt").text(data.joiningDate);
						 * $("#emp_join_empname").text(data.employeeName);
						 * $("#hrly_wges_mail").text(data.email);
						 * $("#hrly_wges_dsgnation").text(data.designation);
						 * $("#hrly_wges_dprmtnt").text(data.employeeDepartment);
						 * $("#hrly_wges_brnch").text(data.employeeBranch);
						 */

						var superiors = response.result.superiorSubordinateVo.superiors;

						$("#reimbursements_superiors option").remove();
						$("#reimbursements_superiors_status option").remove();
						var options = '';
						$.each(superiors, function(index, item) {
							options += "<option value='" + item.superiorId
									+ "'>" + item.superiorCode + "</option>";
						});
						$("#reimbursements_superiors").append(options);
						$("#reimbursements_superiors").val(supId);
						$('#reimbursements_superiors').chosen().trigger(
								"chosen:updated");

						$("#reimbursements_superiors_status").append(options);
						$('#reimbursements_superiors_status').chosen().trigger(
								"chosen:updated");
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
					}
				});
	}
}

$("#reimbursements_btn").click(
		function() {
			$("#reimbursements_suprior_ajx_hidn").val(
					$("#reimbursements_superiors").val());
			$("#reimbursements_btn").attr("type", "submit");
		});

/*******************************************************************************
 * CONTRCT
 */
function saveContractInfo() {
	var vo = new Object();
	vo.employeeCode = $("#employeeCode").val();
	vo.contractTypeId = $("#contrct_add_type").val();
	vo.title = $("#cntrt_add_title").val();
	vo.startingDate = $("#cntrt_add_strdte").val();
	vo.endDate = $("#cntrt_add_enddte").val();
	vo.description = $("#cntrct_add_descrptn").val();
	vo.notes = $("#cntrct_add_notes").val();
	vo.contractId = $("#contrctId").val();
	$.ajax({
		type : "POST",
		url : 'saveContrct.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(vo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == 'ok') {
				$("#contrct_doc_upload_btn").removeAttr('disabled');
				$("#contrctId").val(response.result);
				$("#contrctIdd").val(response.result);
				$("#emp_join_emp").attr("readOnly", "true");
				$("#emp_join_dprmnt").attr("readOnly", "true");
				$("#emp_join_brnch").attr("readOnly", "true");
				$("html, body").animate({
					scrollTop : 0
				}, "slow");
				$("#cntrtc_sve_sccs").css('display', 'block');
				setTimeout(function() {
					$('#cntrtc_sve_sccs').fadeOut(500);
				}, 3000);
			} else {
				$("#cntrtc_failed").css('display', 'block');
				setTimeout(function() {
					$('#cntrtc_failed').fadeOut(500);
				}, 3000);
			}

		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function updateContractInfo() {
	var vo = new Object();
	vo.contractTypeId = $("#contrct_add_type").val();
	vo.title = $("#cntrt_add_title").val();
	vo.startingDate = $("#cntrt_add_strdte").val();
	vo.endDate = $("#cntrt_add_enddte").val();
	vo.description = $("#cntrct_add_descrptn").val();
	vo.notes = $("#cntrct_add_notes").val();
	vo.contractId = $("#contrctId").val();
	$.ajax({
		type : "POST",
		url : 'saveContrct.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(vo),
		cache : false,
		async : true,
		success : function(response) {
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/*******************************************************************************
 * LEAVE
 */
function getEmployeeeDetails() {
	var id = $("#leave_employee").val();
	var supId = ($("#leave_suprior_ajx_hidn").val()).split(",");
	var typeId = $("#leave_type_ajx_hidn").val();
	if (id != '') {
		$
				.ajax({
					type : "POST",
					url : 'getEmployeeDetailsById.do',
					data : {
						"employeeId" : id
					},
					cache : false,
					async : true,
					success : function(response) {
						var superiors = response.result.superiorSubordinateVo.superiors;

						$("#leave_superiors option").remove();
						var options = '';
						$.each(superiors, function(index, item) {
							options += "<option value='" + item.superiorId
									+ "'>" + item.superiorCode + "</option>";
						});
						$("#leave_superiors").append(options);
						/*
						 * for(var i=0;i<supId.length;i++){ alert(supId[i]);
						 * $("#leave_superiors").val(supId[i]); }
						 */
						$("#leave_superiors").val(supId);
						$('#leave_superiors').chosen()
								.trigger("chosen:updated");

						var leave = response.result.leaveVos;
						$("#leave_type option").remove();

						var options = '<option value="">--Select Leave Type--</option>';
						$.each(leave, function(index, item) {
							options += "<option value='" + item.leaveTitleId
									+ "'>" + item.leaveTitle + "</option>";
						});
						$("#leave_type").append(options);
						$("#leave_type").val(typeId);
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
					}
				});
	}
}

/*******************************************************************************
 * ASSIGNMENTS
 */
function saveAssignments() {
	var vo = new Object();
	vo.assignmentId = $("#assignmentId").val();
	vo.employeeCode = $("#employeeCode").val();
	vo.projectId = $("#assgnmt_add_prjct").val();
	vo.assignmentName = $("#assgmnt_add_name").val();
	vo.startDate = $("#assgmnt_add_strtdt").val();
	vo.endDate = $("#assgmnt_add_enddt").val();
	vo.priority = $("#assgmnt_add_prio").val();
	vo.description = $("#assgmnt_add_dscr").val();
	vo.notes = $("#assgmnt_add_nts").val();
	$.ajax({
		type : "POST",
		url : 'saveAssignment.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(vo),
		cache : false,
		async : true,
		success : function(response) {
			$("#assignment_doc_btn").removeAttr('disabled');
			$("#assignmentId").val(response.result);
			$("#assignmentIdd").val(response.result);

			$("#asgnmnt_sve_sccs").css("display", "block");
			setTimeout(function() {
				$('#asgnmnt_sve_sccs').fadeOut(500);
			}, 3000);
			$('html,body').animate({
				scrollTop : 0
			}, "slow");

		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#asgnmnt_sve_sccs").css("display", "block");
			setTimeout(function() {
				$('#asgnmnt_failed').fadeOut(500);
			}, 3000);
			alert("error");
		}
	});
}
/** *********************************************************************** */

/**
 * section for performance evaluation
 */
deleteEvaluation = function(p_e_id) {
	$.ajax({
		type : "POST",
		url : "PerformanceEvaluationDelete.do",
		data : {
			"evaluationId" : p_e_id
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#performance-evaluation-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}
/**
 * save employee achievements information
 */

function saveAchievementsInfo() {
	var projectVo = new Object();
	projectVo.projectTitle = $("#add_project_title").val();
	projectVo.clientId = $("#add_project_client").val();
	projectVo.projectStartDate = $("#add_project_startdate").val();
	projectVo.projectEndDate = $("#add_project_enddate").val();
	projectVo.projectDescription = $("#add_project_description").val();
	projectVo.projectAdditionalInformation = $("#add_prjct_nts").val();
	projectVo.employeesIds = $("#add_project_employees").val();
	$.ajax({
		type : "POST",
		url : 'saveProjectInfo.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(projectVo),
		cache : false,
		async : true,
		success : function(response) {

			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			if (response.status == "Project Successfully Saved.") {
				$("#add_prjc_succs").append(response.status);
				$("#add_prjc_succs").css('display', 'block');
				setTimeout(function() {
					$('#add_prjc_succs').fadeOut(500);
				}, 3000);
				$("#sbmt_proj_stats_btn").removeAttr('disabled');
				$("#sbmt_proj_doc_btn").removeAttr('disabled');
			} else if (response.status == "Project Save Failed.") {
				$("#add_prjc_err").append(response.status);
				$("#add_prjc_err").css('display', 'block');
				setTimeout(function() {
					$('#add_prjc_err').fadeOut(500);
				}, 3000);
			}
		}
	});
}
function saveEmployeeJoin() {
	alert($("#employeeCode").val());
	var vo = new Object();
	vo.joiningDate = $("#emp_join_joindate").val();
	vo.employeeTypeId = $("#emp_join_emptype").val();
	vo.designationId = $("#emp_join_emptdes").val();
	vo.branchId = $("#emp_join_emptbrnch").val();
	vo.departmentId = $("#emp_join_emptdept").val();
	vo.employeeCode = $("#employeeCode").val();
	vo.superiorIds = $("#joining_sups").val();
	vo.joiningId = $("#joining_id").val();
	vo.notes = $("#empjoin_new_note").val();
	$.ajax({
		type : "POST",
		url : 'saveEmployeeJoining.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(vo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == 'ok') {
				$("#joining_id").val(response.result);
				$("#joining_idd").val(response.result);
				$("#emp_join_brnch").attr("readOnly", "true");
				$("#emp_join_dprmnt").attr("readOnly", "true");
				$("#emp_join_emp").attr("readOnly", "true");
				$("#join_sve_sccs").css('display', 'block');
				setTimeout(function() {
					$('#join_sve_sccs').fadeOut(500);
				}, 3000);

			} else {
				$("#join_failed").css('display', 'block');
				setTimeout(function() {
					$('#join_failed').fadeOut(500);
				}, 3000);

			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function updateEmployeeJoin() {
	var vo = new Object();
	vo.joiningDate = $("#emp_join_joindate").val();
	vo.employeeTypeId = $("#emp_join_emptype").val();
	vo.designationId = $("#emp_join_emptdes").val();
	vo.branchId = $("#emp_join_emptbrnch").val();
	vo.departmentId = $("#emp_join_emptdept").val();
	vo.joiningId = $("#joining_id").val();
	vo.superiorIds = $("#joining_sups").val();
	vo.notes = $("#empjoin_new_note").val();
	vo.employeeCode = $("#employeeCode").val();
	$.ajax({
		type : "POST",
		url : 'saveEmployeeJoining.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(vo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == 'ok') {
				// $("#emplo_joininig_doc_upload").css("display","block");
				$("#emplo_joininig_doc_upload").removeAttr('disabled');
				$("#joining_id").val(response.result);
				$("#joining_idd").val(response.result);
				$("#emp_join_brnch").attr("readOnly", "true");
				$("#emp_join_dprmnt").attr("readOnly", "true");
				$("#emp_join_emp").attr("readOnly", "true");
				$("#join_sve_sccs").css('display', 'block');
				setTimeout(function() {
					$('#join_sve_sccs').fadeOut(500);
				}, 3000);
			} else {
				$("#join_failed").css('display', 'block');
				setTimeout(function() {
					$('#join_failed').fadeOut(500);
				}, 3000);

			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

/** *********************************************************************** */

/**
 * method for saving initial details in interview
 */
function saveInitialInterviewInfo() {

	var jobInterviewVo = new Object();

	jobInterviewVo.jobPostId = $("#add_int_posts").val();
	jobInterviewVo.jobInterviewId = $("#add_int_id").val();
	jobInterviewVo.interviewDate = $("#add_int_Date").val();
	jobInterviewVo.interviewTime = $("#add_int_Time").val();
	jobInterviewVo.interViewerIds = $("#add_int_interviewers").val();
	jobInterviewVo.statusId = $("#add_int_sts").val();
	jobInterviewVo.selectionNumber = $("#add_int_nos").val();
	jobInterviewVo.place = $("#add_int_place").val();
	$.ajax({

		type : "POST",
		url : "saveIntBasicInfo.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobInterviewVo),
		cache : false,
		async : true,
		success : function(response) {
			if ($("#add_int_id").val() == '') {
				$("#add_int_id").val(response.result);
				$("#doc_int_id").val(response.result);
			}

			$("#add_int_desc").focus();
			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			if (response.status == "Interview Successfully Saved.") {
				$("#add_int_success").empty();
				$("#add_int_success").append(response.status);
				$("#add_int_success").css('display', 'block');
				setTimeout(function() {
					$("#add_int_success").fadeOut(500);

				}, 2000);
				enableInterviewButtons();
			} else if (response.status == "Interview Updated Successfully.") {

				$("#add_int_success").empty();
				$("#add_int_success").append(response.status);
				$("#add_int_success").css('display', 'block');
				setTimeout(function() {
					$("#add_int_success").fadeOut(500);

				}, 2000);
				enableInterviewButtons();
			} else if (response.status == "Interview Save Failed.") {

				$("#add_int_error").empty();
				$("#add_int_error").append(response.status);
				$("#add_int_error").css('display', 'block');
				setTimeout(function() {
					$("#add_int_error").fadeOut(500);

				}, 2000);

			}

		},
		error : function(response) {
			alert(response.status);
		}

	});
}

/**
 * method to save addInfo for interview
 */
function saveInterviewAddInfo() {

	var jobInterviewVo = new Object();
	jobInterviewVo.jobInterviewId = $("#add_int_id").val();
	jobInterviewVo.additionalInfo = $("#add_int_add_info").val();

	$.ajax({

		type : "POST",
		url : 'saveAdditionalInfo.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobInterviewVo),
		cache : false,
		async : true,
		success : function(response) {

			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			$("#add_int_desc").focus();
			if (response.status == "Interview Successfully Saved.") {
				$("#add_int_success").empty();
				$("#add_int_success").append(response.status);
				$("#add_int_success").css('display', 'block');
				setTimeout(function() {
					$("#add_int_success").fadeOut(500);

				}, 2000);

			} else if (response.status == "Interview Updated Successfully.") {

				$("#add_int_success").empty();
				$("#add_int_success").append(response.status);
				$("#add_int_success").css('display', 'block');
				setTimeout(function() {
					$("#add_int_success").fadeOut(500);

				}, 2000);
			} else if (response.status == "Interview Save Failed.") {

				$("#add_int_error").empty();
				$("#add_int_error").append(response.status);
				$("#add_int_error").css('display', 'block');
				setTimeout(function() {
					$("#add_int_error").fadeOut(500);

				}, 2000);

			}
		},
		error : function(response) {
			alert(response.status);
		}

	});
}

/**
 * method to save description for interview
 */
function saveInterviewDesc() {

	var jobInterviewVo = new Object();
	jobInterviewVo.jobInterviewId = $("#add_int_id").val();
	jobInterviewVo.description = $("#add_int_desc").val();

	$.ajax({

		type : "POST",
		url : 'saveDescription.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobInterviewVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#add_int_add_info").focus;
			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			$("#add_int_desc").focus();
			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			if (response.status == "Interview Successfully Saved.") {
				$("#add_int_success").empty();
				$("#add_int_success").append(response.status);
				$("#add_int_success").css('display', 'block');
				setTimeout(function() {
					$("#add_int_success").fadeOut(500);

				}, 2000);

			} else if (response.status == "Interview Updated Successfully.") {

				$("#add_int_success").empty();
				$("#add_int_success").append(response.status);
				$("#add_int_success").css('display', 'block');
				setTimeout(function() {
					$("#add_int_success").fadeOut(500);

				}, 2000);
			} else if (response.status == "Interview Save Failed.") {

				$("#add_int_error").empty();
				$("#add_int_error").append(response.status);
				$("#add_int_error").css('display', 'block');
				setTimeout(function() {
					$("#add_int_error").fadeOut(500);

				}, 2000);

			}
		},
		error : function(response) {
			alert(response.status);
		}

	});
}

uploadEvaluationDocument = function() {
	var data = new FormData();
	jQuery.each(jQuery('#performance-evaluation-docs')[0].files, function(i,
			file) {
		data.append('file-' + i, file);
	});
	$.ajax({
		type : "POST",
		url : "UploadPerformanceEvaluationQuestion.do?evaluationId="
				+ $("#evaluationId").val(),
		data : data,
		enctype : 'multipart/form-data',
		cache : false,
		async : true,
		processData : false,
		contentType : false,
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listAllQuestionsOfEvaluation($("#evaluationId").val());
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

uploadTerminationDocument = function() {
	var data = new FormData();
	jQuery.each(jQuery('#employee-termination-docs')[0].files,
			function(i, file) {
				data.append('file-' + i, file);
			});
	$.ajax({
		type : "POST",
		url : "UploadTerminationDocument.do?terminationId="
				+ $("#terminationId").val(),
		data : data,
		enctype : 'multipart/form-data',
		cache : false,
		async : true,
		processData : false,
		contentType : false,
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			readAllTerminationDocuments($("#terminationId").val());
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

uploadAnnouncementDocument = function() {
	var data = new FormData();
	jQuery.each(jQuery('#announcements-docs')[0].files, function(i, file) {
		data.append('file-' + i, file);
	});
	$.ajax({
		type : "POST",
		url : "UploadAnnouncementDocument.do?announcementId="
				+ $("#announcementsId").val(),
		data : data,
		enctype : 'multipart/form-data',
		cache : false,
		async : true,
		processData : false,
		contentType : false,
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			// readAllTerminationDocuments($("#terminationId").val());
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

uploadMemoDocument = function() {
	var data = new FormData();
	jQuery.each(jQuery('#employee-memo-docs')[0].files, function(i, file) {
		data.append('file-' + i, file);
	});
	$.ajax({
		type : "POST",
		url : "UploadMemoDocument.do?memoId=" + $("#memoId").val(),
		data : data,
		enctype : 'multipart/form-data',
		cache : false,
		async : true,
		processData : false,
		contentType : false,
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listAllMemoDocuments($("#memoId").val());
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

$("#general-settings-tab").click(function() {
	$.ajax({
		type : "GET",
		url : "GetPayrollEmployees.do",
		cache : false,
		async : true,
		success : function(response) {
			$("#second-levels-list-div").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});

uploadWarningDocument = function() {
	var data = new FormData();
	jQuery.each(jQuery('#employee-warning-docs')[0].files, function(i, file) {
		data.append('file-' + i, file);
	});
	$.ajax({
		type : "POST",
		url : "UploadWarningDocument.do?warningId=" + $("#warningId").val(),
		data : data,
		enctype : 'multipart/form-data',
		cache : false,
		async : true,
		processData : false,
		contentType : false,
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listAllWarningDocuments($("#warningId").val());
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

uploadEmployeeExitDocument = function() {
	var data = new FormData();
	jQuery.each(jQuery('#employee-exit-docs')[0].files, function(i, file) {
		data.append('file-' + i, file);
	});
	$
			.ajax({
				type : "POST",
				url : "SaveEmployeeExitDocument.do?employeeExitId="
						+ $("#employeeExitId").val(),
				data : data,
				enctype : 'multipart/form-data',
				cache : false,
				async : true,
				processData : false,
				contentType : false,
				beforeSend : function() {
					openSpinner();
				},
				success : function(response) {
					$
							.ajax({
								type : "POST",
								url : "readThisEmployeeExitDocuments.do",
								cache : false,
								async : true,
								data : {
									"exid" : $("#employeeExitId").val()
								},
								success : function(response) {
									if (response.status == "SUCCESS") {
										var docListLength = response.result.employeeExitDocuments.length;
										if (docListLength > 0) {
											var listHtml = "<table class='table no-margn'><thead><tr><th>#</th><th>Document Name</th><th>view</th><th>Delete</th></tr></thead><tbody>";
											for (var i = 0; i < docListLength; i++) {
												listHtml = listHtml
														+ "<tr><td>"
														+ (parseInt(i) + 1)
														+ "</td><td><i class='fa fa-file-o'></i> "
														+ response.result.employeeExitDocuments[i].documentName
														+ "</td><td><i class='fa fa-file-text-o sm'></i></td><td><a href='#' onClick='deleteEmpExtDoc("
														+ response.result.employeeExitDocuments[i].emp_ext_doc_id
														+ ","
														+ response.result.employeeExitId
														+ ",\""
														+ response.result.employeeExitDocuments[i].documentUrl
														+ "\")'><i class='fa fa-close ss'></i></a></td></tr>";
											}
											listHtml = listHtml
													+ "</tbody></table>";
											$("#emp_ext_doc_lst")
													.html(listHtml);
										} else {
											$("#emp_ext_doc_lst").html(
													"No Documents Available.");
										}
									}
								},
								error : function(requestObject, error,
										errorThrown) {
									alert(errorThrown);
								}
							});

					closeSpinner();
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/**
 * Section for upload loan documents
 */
uploadLoanDocument = function() {
	var data = new FormData();
	jQuery.each(jQuery('#loan-docs')[0].files, function(i, file) {
		data.append('file-' + i, file);
	});
	$.ajax({
		type : "POST",
		url : "UploadLoanDocuments.do?loanId=" + $("#loanId").val(),
		data : data,
		enctype : 'multipart/form-data',
		cache : false,
		async : true,
		processData : false,
		contentType : false,
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listLoanAllDocuments($("#loanId").val());
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * Section for list all loan documents
 */
$("#loan-doc-tab").click(function() {
	var loanId = $("#loanId").val();
	if (loanId != "") {
		listLoanAllDocuments(loanId);
	} else {
		$(".empl-loan-doc-btn").prop("disabled", true);
		$('input#loan-docs').prop('disabled', true);
		$(".empl-loan-doc-status").html("No loan details found.");
	}
});

/**
 * Section for delete loan
 * 
 * @param loanId
 */
deleteLoan = function(loanId) {
	$.ajax({
		type : "POST",
		url : "deleteThisLoan.do",
		data : {
			"loanId" : loanId
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#loan-list-div").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * method to delete loan document
 * 
 * @param loan_d_id
 */
deleteLoanDoc = function(loan_id, loan_d_id, loan_d_url) {
	$.ajax({
		type : "POST",
		url : "loanDocumentDelete.do",
		cache : false,
		async : true,
		data : {
			"documentId" : loan_d_id,
			"documentUrl" : loan_d_url
		},
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listLoanAllDocuments(loan_id);
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * method to load loan all documents
 * 
 * @param evaluationId
 */
listLoanAllDocuments = function(loanId) {
	$.ajax({
		type : "POST",
		url : "ListLoanAllDocuments.do",
		data : {
			"loanId" : loanId
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#loan-documents-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

listAllQuestionsOfEvaluation = function(evaluationId) {
	$.ajax({
		type : "POST",
		url : "PerformanceEvaluationQuesList.do",
		data : {
			"evaluationId" : evaluationId
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#performance-evaluation-questions-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * Section for PF Syntax save
 */
saveProvidentFundSyntax = function() {
	var profidentFundSyntaxVo = new Object();
	profidentFundSyntaxVo.pfSyntaxId = $("#pfSyntaxId").val();
	if ($("#pfSyntaxId").val().trim().length === 0) {
		profidentFundSyntaxVo.pfSyntaxId = 0;
	}
	profidentFundSyntaxVo.employeeSyntax = $(".employeeSyntax").val();
	profidentFundSyntaxVo.employerSyntax = $(".employerSyntax").val();

	$
			.ajax({
				type : "POST",
				url : "SavePFSyntaxInfo.do",
				dataType : "html",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(profidentFundSyntaxVo),
				cache : false,
				async : true,
				success : function(response) {
					$("#pf-syntax-list-div").html(response);
					$(".pf-status-message")
							.append(
									"<div class='success_msg'>Provident Fund Syntax successfully modified.</div>")
				},
				error : function(requestObject, error, errorThrown) {
					$(".error_msg").css("display", "block !important");
					$(".pf-status-message")
							.append(
									"<div class='error_msg'>Oops!! Something went wrong.</div>")
				}
			});
}

/**
 * Section for ESI Syntax save
 */
saveESISyntax = function() {
	var esiSyntaxVo = new Object();
	esiSyntaxVo.esiSyntaxId = $("#esiSyntaxId").val();
	if ($("#esiSyntaxId").val().trim().length === 0) {
		esiSyntaxVo.esiSyntaxId = 0;
	}
	esiSyntaxVo.employeeSyntax = $(".employeeEsiSyntax").val();
	esiSyntaxVo.employerSyntax = $(".employerEsiSyntax").val();
	esiSyntaxVo.range = $(".range").val();
	esiSyntaxVo.amount = $(".amount").val();

	$
			.ajax({
				type : "POST",
				url : "SaveESISyntaxInfo.do",
				dataType : "html",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(esiSyntaxVo),
				cache : false,
				async : true,
				success : function(response) {
					$("#esi-syntax-list-div").html(response);
					$(".esi-status-message")
							.append(
									"<div class='success_msg'>ESI Syntax successfully modified.</div>")
				},
				error : function(requestObject, error, errorThrown) {
					$(".error_msg").css("display", "block !important");
					$(".esi-status-message")
							.append(
									"<div class='error_msg'>Oops!! Something went wrong.</div>")
				}
			});
}

/**
 * Section for bonus date save
 */
saveBonusDate = function() {
	var bonusDateVo = new Object();
	bonusDateVo.bonusDateId = $("#bonusDateId").val();
	if ($("#bonusDateId").val().trim().length === 0) {
		bonusDateVo.bonusDateId = 0;
	}
	bonusDateVo.day = $("#bonus-day-count").val();
	bonusDateVo.month = $("#bonus-month-count").val();
	bonusDateVo.year = $("#bonus-year-count").val();

	$
			.ajax({
				type : "POST",
				url : "SaveBonusDateInfo.do",
				dataType : "html",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(bonusDateVo),
				cache : false,
				async : true,
				success : function(response) {
					$("#bonus-date-list-div").html(response);
					$(".status-message")
							.append(
									"<div class='success_msg'>Bonus date successfully modified.</div>")
				},
				error : function(requestObject, error, errorThrown) {
					$(".error_msg").css("display", "block !important");
					$(".status-message")
							.append(
									"<div class='error_msg'>Oops!! Something went wrong.</div>")
				}
			});
}

/**
 * Section for save extra payslip item
 */
saveThisExtraPaySlipItem = function(rowCount) {
	var extraPaySlipItemVo = new Object();
	extraPaySlipItemVo.extraPaySlipItemId = $("#itemeId" + rowCount).val();
	extraPaySlipItemVo.title = $(".itemeTitle" + rowCount).val();
	extraPaySlipItemVo.type = $(".itemeType" + rowCount).val();
	extraPaySlipItemVo.taxAllowance = $(".itemeTax" + rowCount).val();
	extraPaySlipItemVo.calculation = $(".itemeSyntax" + rowCount).val();
	extraPaySlipItemVo.effectFrom = $(".itemeFrom" + rowCount).val();

	$.ajax({
		type : "POST",
		url : "SaveExtraPaySlipItem.do",
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(extraPaySlipItemVo),
		cache : false,
		async : true,
		success : function(response) {
			if (jQuery.isEmptyObject(response)) {
				$(".epay-slp-err").html(
						" : Item with title "
								+ $('.itemeTitle' + rowCount).val()
								+ " already available.");
			} else {
				$("#extra-pay-slip-item-list-div").html(response);
				$(".epay-slp-err").html(" : Extra payslip details saved.");
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

updateThisExtraPaySlipItem = function(rowCount) {
	var extraPaySlipItemVo = new Object();
	extraPaySlipItemVo.extraPaySlipItemId = $("#itemeId" + rowCount).val();
	extraPaySlipItemVo.title = $(".itemeTitle" + rowCount).val();
	extraPaySlipItemVo.type = $(".itemeType" + rowCount).val();
	extraPaySlipItemVo.taxAllowance = $(".itemeTax" + rowCount).val();
	extraPaySlipItemVo.calculation = $(".itemeSyntax" + rowCount).val();
	extraPaySlipItemVo.effectFrom = $(".itemeFrom" + rowCount).val();
	$.ajax({
		type : "POST",
		url : "UpdateExtraPaySlipItem.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(extraPaySlipItemVo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status != "SUCCESS") {
				$(".epay-slp-err").html(
						" : Item with title "
								+ $('.itemeTitle' + rowCount).val()
								+ " already available.");
			} else {
				$(".epay-slp-err").html(" : Extra payslip details updated.");
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * Section for save payslip item
 */
saveThisPaySlipItem = function(rowCount) {
	var paySlipItemVo = new Object();
	paySlipItemVo.paySlipItemId = $("#itemId" + rowCount).val();
	paySlipItemVo.title = $(".itemTitle" + rowCount).val();
	paySlipItemVo.type = $(".itemType" + rowCount).val();
	paySlipItemVo.taxAllowance = $(".itemTax" + rowCount).val();
	paySlipItemVo.calculation = $(".itemSyntax" + rowCount).val();
	paySlipItemVo.effectFrom = $(".itemFrom" + rowCount).val();

	$.ajax({
		type : "POST",
		url : "SavePaySlipItem.do",
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(paySlipItemVo),
		cache : false,
		async : true,
		success : function(response) {
			if (jQuery.isEmptyObject(response)) {
				$(".pay-slp-err").html(
						" : Item with title "
								+ $('.itemTitle' + rowCount).val()
								+ " already available.");
			} else {
				$("#pay-slip-item-list-div").html(response);
				$(".pay-slp-err").html(" : Payslip details saved.");
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

updateThisPaySlipItem = function(rowCount) {
	var paySlipItemVo = new Object();
	paySlipItemVo.paySlipItemId = $("#itemId" + rowCount).val();
	paySlipItemVo.title = $(".itemTitle" + rowCount).val();
	paySlipItemVo.type = $(".itemType" + rowCount).val();
	paySlipItemVo.taxAllowance = $(".itemTax" + rowCount).val();
	paySlipItemVo.calculation = $(".itemSyntax" + rowCount).val();
	paySlipItemVo.effectFrom = $(".itemFrom" + rowCount).val();
	$.ajax({
		type : "POST",
		url : "UpdatePaySlipItem.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(paySlipItemVo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status != "SUCCESS") {
				$(".pay-slp-err").html(
						" : Item with title "
								+ $('.itemTitle' + rowCount).val()
								+ " already available.");
			} else {
				$(".pay-slp-err").html(" : Payslip details updated.");
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

updateThisTaxRuleItem = function(rowCount) {
	var taxRuleVo = new Object();
	taxRuleVo.taxRuleId = $("#taxId" + rowCount).val();
	taxRuleVo.salaryFrom = $(".salaryFrom" + rowCount).val();
	taxRuleVo.salaryTo = $(".salaryTo" + rowCount).val();
	taxRuleVo.taxPercentage = $(".taxPercentage" + rowCount).val();
	taxRuleVo.exemptedAmount = $(".exemptedAmount" + rowCount).val();
	taxRuleVo.additionalAmount = $(".additionalAmount" + rowCount).val();
	taxRuleVo.individual = $(".individual" + rowCount).val();

	$.ajax({
		type : "POST",
		url : "SaveTaxRuleItem.do",
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(taxRuleVo),
		cache : false,
		async : true,
		success : function(response) {
			/*
			 * if (jQuery.isEmptyObject(response)) { $(".pay-slp-err").html( " :
			 * Item with title " + $('.itemTitle' + rowCount).val() + " already
			 * available."); } else {
			 * $("#tax-rule-item-list-div").html(response);
			 * $(".tax-rul-err").html(" : Tax rule details saved."); }
			 */
			$("#tax-rule-item-list-div").html(response);
			$(".tax-rul-err").html(" : Tax rule details updated.");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

$("#pay-slp-item-tab").click(function() {
	$.ajax({
		type : "GET",
		url : "PaySlipItemTab.do",
		cache : false,
		async : true,
		success : function(response) {
			$("#pay-slip-item-tab-div").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});

$("#tax-rule-item-tab").click(function() {
	$.ajax({
		type : "GET",
		url : "TaxRuleItemTab.do",
		cache : false,
		async : true,
		success : function(response) {
			$("#tax-rule-item-tab-div").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});

saveTaxExcludeEmployeeList = function() {
	var excludedEmployees = [];

	$(".excluded-employees option:selected").each(function() {
		var $this = $(this);
		if ($this.length) {
			excludedEmployees.push($this.val());
		}
	});

	var list = {
		item : excludedEmployees
	};

	$.ajax({
		type : "POST",
		url : "SaveTaxExcludeEmployees.do",
		cache : false,
		async : true,
		data : JSON.stringify(list),
		contentType : "application/json; charset=utf-8",
		success : function(response) {
			$(".tax-exlude-status").html(
					" : Employees tax excluded list successfully updated.");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

setAdditionalAmount = function(row) {
	if ($(".salaryTo" + row).val().trim().length > 0) {
		$.ajax({
			type : "GET",
			url : "GetTaxWithSalaryTo.do",
			cache : false,
			async : true,
			dataType : "json",
			data : {
				"salaryTo" : $(".salaryFrom" + row).val()
			},
			success : function(response) {
				if (response.status == "SUCCESS") {
					$(".additionalAmount" + row).val(response.result.rowTax);
				}
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

/**
 * Section for save tax rule item
 */
saveThisTaxRuleItem = function(rowCount) {
	var taxRuleVo = new Object();
	taxRuleVo.taxRuleId = $("#taxId" + rowCount).val();
	taxRuleVo.salaryFrom = $(".salaryFrom" + rowCount).val();
	taxRuleVo.salaryTo = $(".salaryTo" + rowCount).val();
	taxRuleVo.taxPercentage = $(".taxPercentage" + rowCount).val();
	taxRuleVo.exemptedAmount = $(".exemptedAmount" + rowCount).val();
	taxRuleVo.additionalAmount = $(".additionalAmount" + rowCount).val();
	taxRuleVo.individual = $(".individual" + rowCount).val();

	$.ajax({
		type : "POST",
		url : "SaveTaxRuleItem.do",
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(taxRuleVo),
		cache : false,
		async : true,
		success : function(response) {
			/*
			 * if (jQuery.isEmptyObject(response)) { $(".pay-slp-err").html( " :
			 * Item with title " + $('.itemTitle' + rowCount).val() + " already
			 * available."); } else {
			 * $("#tax-rule-item-list-div").html(response);
			 * $(".tax-rul-err").html(" : Tax rule details saved."); }
			 */
			$("#tax-rule-item-list-div").html(response);
			$(".tax-rul-err").html(" : Tax rule details saved.");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

$("#performance-eval-doc-tab").click(function() {
	var evalId = $("#evaluationId").val();
	if (evalId != "") {
		listAllQuestionsOfEvaluation(evalId);
	}
});

deletePEvaluationQuestion = function(evaluationId, docId, docUrl) {
	$.ajax({
		type : "POST",
		url : "PerformanceEvaluationQuesDelete.do",
		cache : false,
		async : true,
		data : {
			"documentId" : docId,
			"documentUrl" : docUrl
		},
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			listAllQuestionsOfEvaluation(evaluationId);
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}
/** ****************************************************************************************************** */
/**
 * Function for submitting basic info in add candidates
 * 
 */
function submitCandBasicInfo() {

	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	// jobCandidatesVo.jobFieldId = $("#cand_job_field").val();
	jobCandidatesVo.jobField = $("#cand_job_field").val();
	jobCandidatesVo.firstName = $("#cand_fname").val();
	jobCandidatesVo.lastName = $("#cand_lname").val();
	jobCandidatesVo.birthDate = $("#cand_dob").val();
	jobCandidatesVo.gender = $("form input[type='radio']:checked").val();
	jobCandidatesVo.nationality = $("#cand_nation").val();
	$
			.ajax({

				type : "POST",
				url : "saveCandidateBasicInfo.do",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(jobCandidatesVo),
				cache : false,
				async : true,
				success : function(response) {

					if ($("#cand_id").val() == '') {
						$("#cand_id").val(response.result);
						$("#resCandId").val(response.result);
						$("#docCandId").val(response.result);
						enableCandidateButtons();
					}
					$("#cand_interests").focus();
					if (response.status == "Job Candidate Successfully Saved.") {
						$("#add_candInfo_success").empty();
						$("#add_candInfo_success").append(response.status);
						$("#add_candInfo_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candInfo_success").fadeOut(500);

						}, 2000);

					} else if (response.status == "Job Candidate Updated Successfully.") {

						$("#add_candInfo_success").empty();
						$("#add_candInfo_success").append(response.status);
						$("#add_candInfo_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candInfo_success").fadeOut(500);

						}, 2000);
					} else if (response.status == "Job Candidate Save Failed.") {

						$("#add_candInfo_error").empty();
						$("#add_candInfo_error").append(response.status);
						$("#add_candInfo_error").css('display', 'block');
						setTimeout(function() {
							$("#add_candInfo_error").fadeOut(500);

						}, 2000);

					}

				},
				error : function(response) {
					alert(response.status);
				}

			});
}
/**
 * function for saving candidate interests
 */
function submitCandInterest() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.interests = $("#cand_interests").val();
	$
			.ajax({

				type : "POST",
				url : "saveCandidatesInterests.do",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(jobCandidatesVo),
				cache : false,
				async : true,
				success : function(response) {

					if (response.status == "Job Candidate Successfully Saved.") {
						$("#add_candInt_success").empty();
						$("#add_candInt_success").append(
								"Saved Successfully...");
						$("#add_candInt_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candInt_success").fadeOut(500);

						}, 2000);
						$("#cand_achievements").focus();
					} else if (response.status == "Job Candidate Updated Successfully.") {

						$("#add_candInt_success").empty();
						$("#add_candInt_success").append(
								"Updated Successfully...");
						$("#add_candInt_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candInt_success").fadeOut(500);

						}, 2000);
						$("#cand_achievements").focus();
					} else if (response.status == "Job Candidate Save Failed.") {

						$("#add_candInt_error").empty();
						$("#add_candInt_error").append("Save Failed...");
						$("#add_candInt_error").css('display', 'block');
						setTimeout(function() {
							$("#add_candInt_error").fadeOut(500);

						}, 2000);

					}

				},
				error : function(response) {
					alert(response.status);
				}

			});

}
/**
 * Function to save Candidates Achievements
 */
function submitCandAchievements() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.achievements = $("#cand_achievements").val();
	$
			.ajax({

				type : "POST",
				url : "saveCandidatesAchievement.do",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(jobCandidatesVo),
				cache : false,
				async : true,
				success : function(response) {
					if (response.status == "Job Candidate Successfully Saved.") {
						$("#add_candAchieve_success").empty();
						$("#add_candAchieve_success").append(
								"Saved Successfully...");
						$("#add_candAchieve_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candAchieve_success").fadeOut(500);

						}, 2000);
						$("#can_address").focus();
					} else if (response.status == "Job Candidate Updated Successfully.") {

						$("#add_candAchieve_success").empty();
						$("#add_candAchieve_success").append(
								"Updated Successfully...");
						$("#add_candAchieve_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candAchieve_success").fadeOut(500);

						}, 2000);
						$("#can_address").focus();
					} else if (response.status == "Job Candidate Save Failed.") {

						$("#add_candAchieve_error").empty();
						$("#add_candAchieve_error").append("Save Failed...");
						$("#add_candAchieve_error").css('display', 'block');
						setTimeout(function() {
							$("#add_candAchieve_error").fadeOut(500);

						}, 2000);

					}
				},
				error : function(response) {
					alert(response.status);
				}

			});

}
/**
 * Function to save candidate contact details
 */
function submitCandContact() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.address = $("#can_address").val();
	jobCandidatesVo.city = $("#cand_city").val();
	jobCandidatesVo.state = $("#cand_state").val();
	jobCandidatesVo.zipCode = $("#cand_zipCode").val();
	jobCandidatesVo.countryId = $("#cand_country").val();
	jobCandidatesVo.eMail = $("#cand_email").val();
	jobCandidatesVo.phoneNumber = $("#cand_phone").val();
	jobCandidatesVo.mobileNumber = $("#cand_mobile").val();

	$
			.ajax({

				type : "POST",
				url : "saveCandidateContact.do",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(jobCandidatesVo),
				cache : false,
				async : true,
				success : function(response) {

					if (response.status == "Job Candidate Successfully Saved.") {
						$("#add_candCont_success").empty();
						$("#add_candCont_success").append(
								"Saved Successfully...");
						$("#add_candCont_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candCont_success").fadeOut(500);

						}, 2000);
						$("#cand_addInfo").focus();
					} else if (response.status == "Job Candidate Updated Successfully.") {

						$("#add_candCont_success").empty();
						$("#add_candCont_success").append(
								"Updated Successfully...");
						$("#add_candCont_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candCont_success").fadeOut(500);

						}, 2000);
						$("#cand_addInfo").focus();
					} else if (response.status == "Job Candidate Save Failed.") {

						$("#add_candCont_error").empty();
						$("#add_candCont_error").append("Save Failed...");
						$("#add_candCont_error").css('display', 'block');
						setTimeout(function() {
							$("#add_candCont_error").fadeOut(500);

						}, 2000);

					}
				},
				error : function(response) {
					alert(response.status);
				}

			});
}
/**
 * Function to save Candidate Additional Info
 */

function submitCandAddInfo() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.additionalInfo = $("#cand_addInfo").val();
	$
			.ajax({

				type : "POST",
				url : "saveCandidatesAddInfo.do",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(jobCandidatesVo),
				cache : false,
				async : true,
				success : function(response) {
					if (response.status == "Job Candidate Successfully Saved.") {
						$("#add_candAdd_success").empty();
						$("#add_candAdd_success").append(
								"Saved Successfully...");
						$("#add_candAdd_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candAdd_success").fadeOut(500);

						}, 2000);
						$("#cand_qual").focus();
					} else if (response.status == "Job Candidate Updated Successfully.") {

						$("#add_candAdd_success").empty();
						$("#add_candAdd_success").append(
								"Updated Successfully...");
						$("#add_candAdd_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candAdd_success").fadeOut(500);

						}, 2000);
						$("#cand_qual").focus();
					} else if (response.status == "Job Candidate Save Failed.") {

						$("#add_candAdd_error").empty();
						$("#add_candAdd_error").append("Save Failed...");
						$("#add_candAdd_error").css('display', 'block');
						setTimeout(function() {
							$("#add_candAdd_error").fadeOut(500);

						}, 2000);

					}
				},
				error : function(response) {
					alert(response.status);
				}

			});
}
/**
 * Function to save candidate qualification
 */
function submitCandQualification() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.qualificationId = $("#cand_qual").val();
	jobCandidatesVo.subject = $("#cand_subject").val();
	jobCandidatesVo.institute = $("#cand_institute").val();
	jobCandidatesVo.grade = $("#cand_grade").val();
	jobCandidatesVo.year = $("#cand_year").val();
	$
			.ajax({

				type : "POST",
				url : "saveCandidateQual.do",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(jobCandidatesVo),
				cache : false,
				async : true,
				success : function(response) {
					$("#cand_qual").val("");
					$("#cand_subject").val("");
					$("#cand_institute").val("");
					$("#cand_grade").val("");
					$("#cand_year").val("");

					if (response.status == "Job Candidate Successfully Saved.") {
						$("#add_candQual_success").empty();
						$("#add_candQual_success").append(
								"Saved Successfully...");
						$("#add_candQual_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candQual_success").fadeOut(500);

						}, 2000);
						$("#cand_qual").focus();
					} else if (response.status == "Job Candidate Updated Successfully.") {

						$("#add_candQual_success").empty();
						$("#add_candQual_success").append(
								"Updated Successfully...");
						$("#add_candQual_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candQual_success").fadeOut(500);

						}, 2000);
						$("#cand_qual").focus();
					} else if (response.status == "Job Candidate Save Failed.") {

						$("#add_candQual_error").empty();
						$("#add_candQual_error").append("Save Failed...");
						$("#add_candQual_error").css('display', 'block');
						setTimeout(function() {
							$("#add_candQual_error").fadeOut(500);

						}, 2000);

					}
				},
				error : function(response) {
					alert(response.status);
				}

			});

}

/**
 * Function to edit candidate qualificaion
 * 
 */
function getCandQualification() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.qualificationId = $("#cand_qual").val();
	$.ajax({

		type : "POST",
		url : "getCandQualifications.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobCandidatesVo),
		cache : false,
		async : true,
		success : function(response) {
			var qual = response.result;
			$("#cand_subject").val(qual.subject);
			$("#cand_institute").val(qual.institute);
			$("#cand_grade").val(qual.grade);
			$("#cand_year").val(qual.gradYear);

		},
		error : function(response) {
			alert(response.status);
		}

	});

}
/**
 * Function to save candidate Skills
 */
function submitCandSkill() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.skillsId = $("#cand_skills").val();
	jobCandidatesVo.skillLevel = $("#cand_skill_level").val();
	$
			.ajax({

				type : "POST",
				url : "saveCandidatesSkills.do",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(jobCandidatesVo),
				cache : false,
				async : true,
				success : function(response) {
					$("#cand_skills").val("");
					$("#cand_skill_level").val("");
					if (response.status == "Job Candidate Successfully Saved.") {
						$("#add_candSkill_success").empty();
						$("#add_candSkill_success").append(
								"Saved Successfully...");
						$("#add_candSkill_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candSkill_success").fadeOut(500);

						}, 2000);
						$("#cand_skills").focus();
					} else if (response.status == "Job Candidate Updated Successfully.") {
						$("#add_candSkill_success").empty();
						$("#add_candSkill_success").append(
								"Updated Successfully...");
						$("#add_candSkill_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candSkill_success").fadeOut(500);

						}, 2000);
						$("#cand_skills").focus();

					} else if (response.status == "Job Candidate Save Failed.") {

						$("#add_candSkill_error").empty();
						$("#add_candSkill_error").append("Save Failed...");
						$("#add_candSkill_error").css('display', 'block');
						setTimeout(function() {
							$("#add_candSkill_error").fadeOut(500);

						}, 2000);
						$("#cand_skills").focus();

					}

				},
				error : function(response) {
					alert(response.status);
				}

			});

}
/**
 * Method to get candidate skills details
 */
function getCandSkills() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.skillsId = $("#cand_skills").val();
	$.ajax({

		type : "POST",
		url : "getCandSkills.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobCandidatesVo),
		cache : false,
		async : true,
		success : function(response) {
			var skill = response.result;
			$("#cand_skill_level").val(skill.skillLevel);

		},
		error : function(response) {
			// alert(response.status);
		}

	});

}
/**
 * Function to save candidate languages
 */
function submtCandLanguages() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.languageId = $("#cand_lang").val();
	jobCandidatesVo.speaking = $("#cand_speak").val();
	jobCandidatesVo.reading = $("#cand_read").val();
	jobCandidatesVo.writing = $("#cand_write").val();
	$
			.ajax({

				type : "POST",
				url : "saveCandidatesLanguage.do",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(jobCandidatesVo),
				cache : false,
				async : true,
				success : function(response) {
					$("#cand_lang").val("");
					$("#cand_speak").val("");
					$("#cand_read").val("");
					$("#cand_write").val("");

					if (response.status == "Job Candidate Successfully Saved.") {
						$("#add_candLang_success").empty();
						$("#add_candLang_success").append(
								"Saved Successfully...");
						$("#add_candLang_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candLang_success").fadeOut(500);

						}, 2000);
						$("#cand_lang").focus();
					} else if (response.status == "Job Candidate Updated Successfully.") {
						$("#add_candLang_success").empty();
						$("#add_candLang_success").append(
								"Updated Successfully...");
						$("#add_candLang_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candLang_success").fadeOut(500);

						}, 2000);
						$("#cand_lang").focus();

					} else if (response.status == "Job Candidate Save Failed.") {

						$("#add_candLang_error").empty();
						$("#add_candLang_error").append("Save Failed...");
						$("#add_candLang_error").css('display', 'block');
						setTimeout(function() {
							$("#add_candLang_error").fadeOut(500);

						}, 2000);
						$("#cand_lang").focus();

					}
				},
				error : function(response) {
					alert(response.status);
				}

			});

}
/**
 * Method to get candidate language
 */
function getCandLanguage() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.languageId = $("#cand_lang").val();
	$.ajax({

		type : "POST",
		url : "getCandLanguage.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(jobCandidatesVo),
		cache : false,
		async : true,
		success : function(response) {
			var lang = response.result;
			$("#cand_speak").val(lang.speakLevel);
			$("#cand_read").val(lang.readLevel);
			$("#cand_write").val(lang.writeLevel);
		},
		error : function(response) {
			alert(response.status);
		}

	});
}

/**
 * Function to save candidate work experiences
 */
function submtCandExperience() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.organisationName = $("#cand_orgName").val();
	jobCandidatesVo.designation = $("#cand_designation").val();
	jobCandidatesVo.fieldId = $("#cand_expField").val();
	jobCandidatesVo.description = $("#cand_expDesc").val();
	jobCandidatesVo.startDate = $("#cand_expStrtDate").val();
	jobCandidatesVo.endDate = $("#cand_expEndDate").val();
	jobCandidatesVo.startSalary = $("#cand_expStrtSalary").val();
	jobCandidatesVo.endSalary = $("#cand_expEndSalary").val();
	$
			.ajax({

				type : "POST",
				url : "saveCandidatesExperience.do",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(jobCandidatesVo),
				cache : false,
				async : true,
				success : function(response) {
					$("#cand_orgName").val("");
					$("#cand_designation").val("");
					$("#cand_expField").val("");
					$("#cand_expDesc").val("");
					$("#cand_expStrtDate").val("");
					$("#cand_expEndDate").val("");
					$("#cand_expStrtSalary").val("");
					$("#cand_expEndSalary").val("");

					if (response.status == "Job Candidate Successfully Saved.") {
						$("#add_candExp_success").empty();
						$("#add_candExp_success").append(
								"Saved Successfully...");
						$("#add_candExp_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candExp_success").fadeOut(500);

						}, 2000);
						$("#cand_orgName").focus();
					} else if (response.status == "Job Candidate Updated Successfully.") {
						$("#add_candExp_success").empty();
						$("#add_candExp_success").append(
								"Updated Successfully...");
						$("#add_candExp_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candExp_success").fadeOut(500);

						}, 2000);
						$("#cand_orgName").focus();

					} else if (response.status == "Job Candidate Save Failed.") {

						$("#add_candExp_error").empty();
						$("#add_candExp_error").append("Save Failed...");
						$("#add_candExp_error").css('display', 'block');
						setTimeout(function() {
							$("#add_candExp_error").fadeOut(500);

						}, 2000);
						$("#cand_orgName").focus();

					}
				},

				error : function(response) {
					alert(response.status);
				}

			});

}
/**
 * Function to save candidate refernces
 */
function saveCandidateReference() {
	var jobCandidatesVo = new Object();
	jobCandidatesVo.jobCandidateId = $("#cand_id").val();
	jobCandidatesVo.refName = $("#cand_refName").val();
	jobCandidatesVo.refOrganisation = $("#cand_refOrg").val();
	jobCandidatesVo.refPhoneNumber = $("#cand_refPhone").val();
	jobCandidatesVo.refEMail = $("#cand_refEmail").val();
	$
			.ajax({

				type : "POST",
				url : "saveCandidatesReferences.do",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(jobCandidatesVo),
				cache : false,
				async : true,
				success : function(response) {
					$("#cand_refName").val("");
					$("#cand_refOrg").val("");
					$("#cand_refPhone").val("");
					$("#cand_refEmail").val("");

					if (response.status == "Job Candidate Successfully Saved.") {
						$("#add_candRef_success").empty();
						$("#add_candRef_success").append(
								"Saved Successfully...");
						$("#add_candRef_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candRef_success").fadeOut(500);

						}, 2000);
						$("#cand_refName").focus();
					} else if (response.status == "Job Candidate Updated Successfully.") {
						$("#add_candRef_success").empty();
						$("#add_candRef_success").append(
								"Updated Successfully...");
						$("#add_candRef_success").css('display', 'block');
						setTimeout(function() {
							$("#add_candRef_success").fadeOut(500);

						}, 2000);
						$("#cand_refName").focus();

					} else if (response.status == "Job Candidate Save Failed.") {

						$("#add_candRef_error").empty();
						$("#add_candRef_error").append("Save Failed...");
						$("#add_candRef_error").css('display', 'block');
						setTimeout(function() {
							$("#add_candRef_error").fadeOut(500);

						}, 2000);
						$("#cand_refName").focus();

					}

				},
				error : function(response) {
					alert(response.status);
				}

			});
}
/**
 * Function to save candidate status
 */
$("#sbmt_candStatus")
		.click(
				function saveCandidateStatus() {
					var jobCandidatesVo = new Object();
					jobCandidatesVo.jobCandidateId = $("#cand_id").val();
					jobCandidatesVo.status = $(
							"form input[name='status']:checked").val();

					$
							.ajax({

								type : "POST",
								url : "saveCandidatesStatus.do",
								dataType : "json",
								contentType : "application/json; charset=utf-8",
								data : JSON.stringify(jobCandidatesVo),
								cache : false,
								async : true,
								success : function(response) {
									if (response.status == "Job Candidate Successfully Saved.") {
										$("#add_candSts_success").empty();
										$("#add_candSts_success").append(
												"Saved Successfully...");
										$("#add_candSts_success").css(
												'display', 'block');
										setTimeout(function() {
											$("#add_candSts_success").fadeOut(
													500);

										}, 2000);

									} else if (response.status == "Job Candidate Updated Successfully.") {
										$("#add_candSts_success").empty();
										$("#add_candSts_success").append(
												"Updated Successfully...");
										$("#add_candSts_success").css(
												'display', 'block');
										setTimeout(function() {
											$("#add_candSts_success").fadeOut(
													500);

										}, 2000);

									} else if (response.status == "Job Candidate Save Failed.") {
										$("#add_candSts_error").empty();
										$("#add_candSts_error").append(
												"Save Failed...");
										$("#add_candSts_error").css('display',
												'block');
										setTimeout(function() {
											$("#add_candSts_error")
													.fadeOut(500);

										}, 2000);

									}

								},
								error : function(response) {
									alert(response.status);
								}

							});

				});

/**
 * Section for holiday delete
 */
deleteHoliday = function(holidayId) {
	$.ajax({
		type : "POST",
		url : "DeleteHoliday.do",
		cache : false,
		async : true,
		data : {
			"holidayId" : holidayId
		},
		success : function(response) {
			$("#holiday-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * Section for get employee shift category from employee code
 */
$(".wrk-shft-emp-cod").change(function() {
	$.ajax({
		type : "POST",
		url : "GetEmployeeWorkShifts.do",
		cache : false,
		async : true,
		data : {
			"code" : $(".wrk-shft-emp-cod").val()
		},
		beforeSend : function() {
			openSpinner();
		},
		success : function(response) {
			$("#emp-shft-info").html(response);
			closeSpinner();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});
/** **************************************************************************************** */

/**
 * Method to save worksheet basic info
 */

function sbmtWorkSheetInfo() {

	var workSheetVo = new Object;
	workSheetVo.workSheetId = $("#work_sheet_id").val();
	workSheetVo.employeeCode = $("#workSheetEmployeeCode").val();
	workSheetVo.date = $("#work_sheet_date").val();
	alert($("#work_sheet_date").val());

	$.ajax({
		type : "POST",
		url : "saveWorksheetsInfo.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(workSheetVo),
		cache : false,
		async : true,
		success : function(response) {
			if ($("#work_sheet_id").val() == '')
				$("#work_sheet_id").val(response.result);
			$("#work_sheet_project").focus();
			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			if (response.status == "Work Sheet Successfully Saved.") {
				$("#add_ws_success").empty();
				$("#add_ws_success").append(response.status);
				$("#add_ws_success").css('display', 'block');
				setTimeout(function() {
					$("#add_ws_success").fadeOut(500);

				}, 2000);

			} else if (response.status == "Work SheetSuccessfully Updated.") {

				$("#add_ws_success").empty();
				$("#add_ws_success").append(response.status);
				$("#add_ws_success").css('display', 'block');
				setTimeout(function() {
					$("#add_ws_success").fadeOut(500);

				}, 2000);
			} else if (response.status == "WorkSheet Save Failed.") {

				$("#add_ws_error").empty();
				$("#add_ws_error").append(response.status);
				$("#add_ws_error").css('display', 'block');
				setTimeout(function() {
					$("#add_ws_error").fadeOut(500);

				}, 2000);

			}

		},
		error : function(response) {
			alert(response.status);
		}

	});

}

function getWorkSheetEditDetails(taskId) {
	var sheetId = $("#work_sheet_id").val();
	$.ajax({
		type : "GET",
		url : 'getWorksheetTask.do',
		data : {
			"sheetId" : sheetId,
			"taskId" : taskId,

		},
		cache : false,
		async : true,
		success : function(response) {
			var task = response.result;
			$("#ws_hidden_task").val(task.workSheetTasksId);
			$("#work_sheet_start").val(task.startTime);
			$("#work_sheet_end").val(task.endTime);
			$("#work_sheet_project").val(task.projectId);
			$("#work_sheet_task").val(task.task);
		},
		error : function(response) {
			alert(response.status);

		}
	});

}

/**
 * Method to save project and task in work sheet
 */
function submtWorkSheetProject() {
	var workSheetVo = new Object;
	workSheetVo.workSheetId = $("#work_sheet_id").val();
	workSheetVo.startTime = $("#work_sheet_start").val();
	workSheetVo.endTime = $("#work_sheet_end").val();
	workSheetVo.task = $("#work_sheet_task").val();
	workSheetVo.taskId = $("#ws_hidden_task").val();
	$.ajax({
		type : "POST",
		url : "saveWorksheetProjectInfo.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(workSheetVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#work_sheet_end").val("");
			$("#work_sheet_start").val("")
			$("#work_sheet_task").val("");
			if (response.status == "Work Sheet Successfully Saved.") {
				$("#add_ws_table tbody tr").remove();
				var result = response.result.tasksVo;
				var html = "";
				$.each(result, function(i, item) {
					// html+="<tr><td
					// onclick='getWorkSheetEditDetails("+item.workSheetTasksId+")'><a
					// >"+item.project+"</a></td>" +
					html += "<tr><td>" + item.startTime + "</td>" + "<td>"
							+ item.endTime
							+ "<td onclick='getWorkSheetEditDetails("
							+ item.workSheetTasksId + ")'><a >" + item.task
							+ "</a></td>"
							+ "</td><td><a href='deleteWorkSheetTask?tid="
							+ item.workSheetTasksId + "&wid="
							+ result.workSheetId
							+ "'><i class='fa fa-close ss'></i></a></td></tr>";
				});
				html += '</table>';
				$("#add_ws_table ").append(html);
				$("#add_wsPro_success").empty();
				$("#add_wsPro_success").append("Task Saved Successfully");
				$("#add_wsPro_success").css('display', 'block');
				setTimeout(function() {
					$("#add_wsPro_success").fadeOut(500);

				}, 2000);
			} else if (response.status == "Work SheetSuccessfully Updated.") {
				$("#add_ws_table tbody tr").remove();
				var result = response.result.tasksVo;
				// var html = "<table class='table no-margn' id='add_ws_table'
				// ><thead><tr><th>Project Name</th><th>Task</th><th>Start
				// Time</th><th>End Time</th><th>Delete</th></tr></thead>";
				var html = "";
				$.each(result, function(i, item) {
					html += "<tr><td>" + item.startTime + "</td>" + "<td>"
							+ item.endTime
							+ "<td onclick='getWorkSheetEditDetails("
							+ item.workSheetTasksId + ")'><a >" + item.task
							+ "</a></td>"
							+ "</td><td><a href='deleteWorkSheetTask?tid="
							+ item.workSheetTasksId + "&wid="
							+ result.workSheetId
							+ "'><i class='fa fa-close ss'></i></a></td></tr>";
				});
				html += '</table>';
				$("#add_ws_table ").append(html);
				$("#add_wsPro_success").empty();
				$("#add_wsPro_success").append("Task Updated Successfully");
				$("#add_wsPro_success").css('display', 'block');
				setTimeout(function() {
					$("#add_wsPro_success").fadeOut(500);

				}, 2000);
			} else if (response.status == "WorkSheet Save Failed.") {

				$("#add_wsPro_error").empty();
				$("#add_wsPro_error").append(response.status);
				$("#add_wsPro_error").css('display', 'block');
				setTimeout(function() {
					$("#add_wsPro_error").fadeOut(500);

				}, 2000);

			}

		},
		error : function(response) {
			alert(response.status);
		}

	});
}

/**
 * Method to save description in work sheet
 */
function submtWorkSheetDesc() {

	var workSheetVo = new Object;
	workSheetVo.workSheetId = $("#work_sheet_id").val();
	workSheetVo.description = $("#work_sheet_desc").val();
	$.ajax({
		type : "POST",
		url : "saveWorksheetsDesc.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(workSheetVo),
		cache : false,
		async : true,
		success : function(response) {
			$("#work_sheet_addInfo").focus();
			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			if (response.status == "Work Sheet Successfully Saved.") {
				$("#add_ws_success").empty();
				$("#add_ws_success").append(response.status);
				$("#add_ws_success").css('display', 'block');
				setTimeout(function() {
					$("#add_ws_success").fadeOut(500);

				}, 2000);

			} else if (response.status == "Work SheetSuccessfully Updated.") {

				$("#add_ws_success").empty();
				$("#add_ws_success").append(response.status);
				$("#add_ws_success").css('display', 'block');
				setTimeout(function() {
					$("#add_ws_success").fadeOut(500);

				}, 2000);
			} else if (response.status == "WorkSheet Save Failed.") {

				$("#add_ws_error").empty();
				$("#add_ws_error").append(response.status);
				$("#add_ws_error").css('display', 'block');
				setTimeout(function() {
					$("#add_ws_error").fadeOut(500);

				}, 2000);

			}

		},
		error : function(response) {
			alert(response.status);
		}
	});
}

/**
 * Method to save worksheet additional info
 */
function submtWorkSheetAddInfo() {

	var workSheetVo = new Object;
	workSheetVo.workSheetId = $("#work_sheet_id").val();
	workSheetVo.addInfo = $("#work_sheet_addInfo").val();
	$.ajax({
		type : "POST",
		url : "saveWorksheetsAddInfo.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(workSheetVo),
		cache : false,
		async : true,
		success : function(response) {

			$("html, body").animate({
				scrollTop : 0
			}, "slow");
			if (response.status == "Work Sheet Successfully Saved.") {
				$("#add_ws_success").empty();
				$("#add_ws_success").append(response.status);
				$("#add_ws_success").css('display', 'block');
				setTimeout(function() {
					$("#add_ws_success").fadeOut(500);

				}, 2000);

			} else if (response.status == "Work SheetSuccessfully Updated.") {

				$("#add_ws_success").empty();
				$("#add_ws_success").append(response.status);
				$("#add_ws_success").css('display', 'block');
				setTimeout(function() {
					$("#add_ws_success").fadeOut(500);

				}, 2000);
			} else if (response.status == "WorkSheet Save Failed.") {

				$("#add_ws_error").empty();
				$("#add_ws_error").append(response.status);
				$("#add_ws_error").css('display', 'block');
				setTimeout(function() {
					$("#add_ws_error").fadeOut(500);

				}, 2000);

			}

		},
		error : function(response) {
			alert(response.status);
		}
	});

}

/**
 * 
 */
$("#sys-admin-tab").click(function() {
	$.ajax({
		type : "GET",
		url : "getAllAdmins.do",
		cache : false,
		async : true,
		success : function(response) {
			$("#admin-settings").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});

deleteAdminStatus = function(empId) {
	$.ajax({
		type : "POST",
		url : "changeAdminStatus.do",
		cache : false,
		async : true,
		data : {
			"employeeId" : empId
		},
		success : function(response) {
			$("#admin-settings").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

/**
 * Section for employee select with filter
 */
getAllBranches = function() {
	$.ajax({
		type : "GET",
		url : "getAllBranches.do",
		cache : false,
		async : true,
		success : function(response) {
			$(".branch-lists").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

getAllBranchDepartments = function(branch) {
	$.ajax({
		type : "GET",
		url : "getAllBranchDepartments.do",
		cache : false,
		async : true,
		data : {
			"branchId" : branch
		},
		success : function(response) {
			$(".branch-lists").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

getAllBranchDepartmentEmployees = function(branch, department) {
	$.ajax({
		type : "GET",
		url : "getAllBranchDepartmentsEmployee.do",
		cache : false,
		async : true,
		data : {
			"branchId" : branch,
			"departmentId" : department
		},
		success : function(response) {
			$(".branch-lists").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

selectThisEmployee = function(employeeCode, employeeName) {
	$("#employeeCode").val(employeeCode);
	$("#load-erp").css("display","block");
	$.ajax({
		type : "GET",
		url : "setSelectedEmployeeToView.do",
		cache : false,
		async : true,
		dataType : 'json',
		data : {
			"employeeCode" : employeeCode,
			"employeeName" : employeeName
		},
		success : function(response) {
			if (response.status = "SUCCESS") {
				$("#employeeSl").val(
						response.result.name + " (" + response.result.code
								+ ")");
				$("#employeeSl1").val(
						response.result.name + " (" + response.result.code
								+ ")");
				$("#employeeSl2").val(
						response.result.name + " (" + response.result.code
								+ ")");
				$("#employeeSl3").val(
						response.result.name + " (" + response.result.code
								+ ")");
				$("#employeeCode").val(response.result.code);
				$("#employeeCode1").val(response.result.code);
				$("#employeeCode2").val(response.result.code);
				$("#employeeCode3").val(response.result.code);
				$('[name=employee]').change();
				getMySuperiors(employeeCode);
				getMyPayStructure(employeeCode);
				getMyESIShare(employeeCode);
				getEmployeeDetails();
				getBonusAmount(employeeCode);
				closePopup();
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete:function(){
			$("#load-erp").css("display","none");
		}
	});
}
/**
 * Section for get bonus amount
 */
getBonusAmount = function(employeeCode) {
	$.ajax({
		type : "GET",
		url : "getBonusAmountOnPeriod.do",
		cache : false,
		async : true,
		dataType : 'json',
		data : {
			"employeeCode" : employeeCode
		},
		success : function(response) {
			if (response.status = "SUCCESS") {
				$("#bonus_amount").val(response.result);
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * method to get employee superiors
 * 
 * @param employeeCode
 */
getMySuperiors = function(employeeCode) {
	$.ajax({
		type : "GET",
		url : "getMySuperiors.do",
		cache : false,
		async : true,
		dataType : 'json',
		data : {
			"employeeCode" : employeeCode
		},
		success : function(response) {
			if (response.status = "SUCCESS") {
				$("#forwaders option").remove();
				$.each((response.result), function(key, item) {
					$('#forwaders').append(
							$("<option></option>").attr("value", item.value)
									.text(item.label));
				});
				$('#forwaders').chosen().trigger("chosen:updated");
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * method to find pf
 * 
 * @param employeeCode
 */
getMyPayStructure = function(employeeCode) {
	$.ajax({
		type : "GET",
		url : "getMyProvidentFund.do",
		cache : false,
		async : true,
		dataType : "json",
		data : {
			"employeeCode" : employeeCode
		},
		success : function(response) {
			if (response.status = "SUCCESS") {
				$("#employeePFShare").val(response.result.employeeShare);
				$("#organizationPFShare").val(response.result.employerShare);
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * method to find esi
 * 
 * @param employeeCode
 */
getMyESIShare = function(employeeCode) {
	$.ajax({
		type : "GET",
		url : "getMyESI.do",
		cache : false,
		async : true,
		dataType : "json",
		data : {
			"employeeCode" : employeeCode
		},
		success : function(response) {
			if (response.status = "SUCCESS") {
				$(".employeeESIShare").val(response.result.employeeShare);
				$(".organizationESIShare").val(response.result.employerShare);
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/*
 * $(".orgBrchSl").change(function() { $.ajax({ type : "GET", url :
 * "getAllBranchDepartments", cache : false, async : true, data : { "branchId" :
 * $(".orgBrchSl").val() }, success : function(response) {
 * $(".branch-department-lists").html(response); }, error :
 * function(requestObject, error, errorThrown) { alert(errorThrown); } }); });
 * 
 * $(".orgBrchDeptmntSl").change(function() { $.ajax({ type : "GET", url :
 * "getAllBranchDepartmentsEmployee", cache : false, async : true, data : {
 * "branchId" : $(".orgBrchSl").val(), "departmentId" :
 * $(".orgBrchDeptmntSl").val() }, success : function(response) { //
 * $(".branch-department-employee-lists").html(response);
 * $(".br-de-emp-list").html(response); // closePopup(); }, error :
 * function(requestObject, error, errorThrown) { alert(errorThrown); } }); });
 */

/**
 * Section for insurance delete
 */
deleteInsurance = function(i_id) {
	$.ajax({
		type : "POST",
		url : "deleteInsurance.do",
		cache : false,
		async : true,
		data : {
			"insuranceId" : i_id
		},
		success : function(response) {
			$("#insurance-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * Section for esi delete
 */
deleteEsi = function(e_id) {
	$.ajax({
		type : "POST",
		url : "deleteEsi.do",
		cache : false,
		async : true,
		data : {
			"esiId" : e_id
		},
		success : function(response) {
			$("#esi-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * Section for PF delete
 */
deleteProfidentFund = function(pf_id) {
	$.ajax({
		type : "POST",
		url : "deleteProfidentFund.do",
		cache : false,
		async : true,
		data : {
			"pfId" : pf_id
		},
		success : function(response) {
			$("#pro-fund-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * Section for Advance Salary delete
 */
deleteAdvanceSalary = function(as_id) {
	$.ajax({
		type : "POST",
		url : "deleteAdvanceSalary.do",
		cache : false,
		async : true,
		data : {
			"advanceSalaryId" : as_id
		},
		success : function(response) {
			$("#advance-salary-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * Section for Overtime delete
 */
deleteOvertime = function(ot_id) {
	$.ajax({
		type : "POST",
		url : "deleteOvertime.do",
		cache : false,
		async : true,
		data : {
			"overtimeId" : ot_id
		},
		success : function(response) {
			$("#over-time-list").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * Section for employee details popup
 */
fillEmployeePopup = function(code) {
	$.ajax({
		type : "GET",
		url : "getEmployeePopupDetails.do",
		cache : false,
		async : true,
		data : {
			"employeeCode" : code
		},
		success : function(response) {
			$(".empl-popup-gen").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * Section for superior details popup
 */
fillSuperiorPopup = function(code) {
	$.ajax({
		type : "GET",
		url : "getEmployeePopupDetails.do",
		cache : false,
		async : true,
		data : {
			"employeeCode" : code
		},
		success : function(response) {
			$(".superior-popup-gen").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}


function calculateAnualSal(){
	var grossSalary = $("#gross_salary").val();
	if (grossSalary != "") {
		// setting annual salary
		$("#gross_salary_annually").val(parseFloat(grossSalary) * 12);

		// ajax call to calculate payroll items
		calculatePayrollItems(grossSalary, $("#employeeCode").val());
	}
}

function calculatePayrollItems(grossSalary, employeeCode) {
	$.ajax({
		type : "GET",
		url : "getPayrollResult.do",
		cache : false,
		async : true,
		data : {
			"grossSalary" : grossSalary,
			"employeeCode" : employeeCode
		},
		success : function(response) {
			var data = response.result;
			$("#Table1 tr").remove();
			var html = "";
			var ssl = '';
			$.each(data.payRollResultVos, function(index, item) {
				var title = item.paySlipItem;
				var amount = item.amount;
				ssl = index;
				sl = index + 1;

				html += '<tr> <td>' + sl + '</td>'
						+ '<td><input type="text" name="item' + sl + '" value='
						+ title
						+ ' style="background-color:#ffffff !important" '
						+ ' readOnly=true class="form-control"></td>'
						+ '<td><input type="text" name="amount' + sl
						+ '" value=' + amount
						+ ' style="background-color:#ffffff !important" '
						+ '  class="form-control"></td>' + '<td></td>'
						+ ' </tr>';
			});
			$("#Table1").append(html);
			$('#Table1 tr:last td:last-child').append(
					'<input type="button" value="Add Row " onclick="addRoww()" id="addButton'
							+ ssl + '">');
			$("#monthly_tax_deduction").val(data.monthlyTaxDeduction);
			$("#annual_tax_deduction").val(data.annualTaxDeduction);
			$("#monthly_estimated_salary").val(data.monthlyEstimatedSalary);
			$("#annual_estimated_salary").val(data.annualEstimatedSalary);
		},
		error : function(requestObject, error, errorThrown) {
		}
	});
}

/**
 * method to save salary
 */
function saveSalary() {
	if (process($("#emp-joninig_date").val()) > process($(
			"#salary_witheffect_from").val())) {
		alert("With effect from date should be after joining date..");
	} else {
		if ($("#gross_salary").val() .length>0 && $("#employeeCode").val().length>0) {
			$("#loadd-erp").css("display", "block");
			var vo = new Object();
			vo.employeeCode = $("#employeeCode").val();
			vo.withEffectFrom = $("#salary_witheffect_from").val();
			vo.monthlyGrossSalary = $("#gross_salary").val();
			vo.annualGrossSalary = $("#gross_salary_annually").val();
			vo.monthlyTaxDeduction = $("#monthly_tax_deduction").val();
			vo.annualTaxDeduction = $("#annual_tax_deduction").val();
			vo.monthlyEstimatedSalary = $("#monthly_estimated_salary").val();
			vo.annualEstimatedSalary = $("#annual_estimated_salary").val();
			vo.itemsVos = [];
			var rowCount = $("#Table1 tr").length;
			for (var i = 1; i <= rowCount; i++) {
				var item = $('[name=item' + i + ']').val();
				var amount = $('[name = amount' + i + ']').val();
				var itemsVo = new Object();
				itemsVo.item = item;
				itemsVo.amount = amount;
				vo.itemsVos.push(itemsVo);
			}
			$.ajax({
				type : "POST",
				url : 'saveEmployeeMonthlySalary.do',
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(vo),
				cache : false,
				async : true,
				success : function(response) {
					$("#loadd-erp").css("display", "block");
					window.setTimeout(function() {
						location.reload()
					}, 1000);
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
		} else {
			alert("Please Enter Gross Salary..!!");
		}
	}
}

/**
 * function to get extra item amount
 */
function getExtraItemCalculation(item, grossSalary) {
	$.ajax({
		type : "GET",
		url : "getExtraPayslipItemCalculation.do",
		cache : false,
		async : true,
		data : {
			"grossSalary" : grossSalary,
			"item" : item
		},
		success : function(response) {
			$("#extraAmount" + parseInt($('#Table1 tr').length)).val(
					response.result);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to save payslip
 */
function savePayslip() {
	$("#payslip_success").css("display", "block");
	var vo = new Object();
	vo.id = $("#payslip_id").val();
	vo.employeeCode = $("#employeeCode").val();
	vo.total = $("#payslip_total_amount").val();
	vo.salaryDate = $("#salary_daye").val();
	vo.startDate = $("#payslip_start_date").val();
	vo.endDate = $("#payslip_end_date").val();
	vo.notes = $("#payslip_notes").val();
	vo.items = [];
	var rowCount = $("#payslipTable tr").length;
	for (var i = 1; i <= rowCount; i++) {
		var item = $('[name=title' + i + ']').val();
		var amount = $('[name = amount' + i + ']').val();
		var itemsVo = new Object();
		itemsVo.title = item;
		itemsVo.amount = amount;
		vo.items.push(itemsVo);
	}
	$.ajax({
		type : "POST",
		url : 'savePayslip.do',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(vo),
		cache : false,
		async : true,
		success : function(response) {
			if (response.status == 'ok') {
				$("#payslip_id").val(response.result);
				setTimeout(function() {
					$("#payslip_success").fadeOut(500);
				}, 1000);

				$("html, body").animate({
					scrollTop : 0
				}, "slow");
			}
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to calculate salary
 */
function calculatorCalculate(grossSalary) {
	var grossSalary = $("#gross_salary_calculator").val();
	$.ajax({
		type : "GET",
		url : "salaryCalculatorCalculate.do",
		cache : false,
		async : true,
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		data : {
			"grossSalary" : grossSalary
		},
		success : function(response) {
			$("#calc").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function getInterviewBydateAndPost() {
	var date = $("#offer_date").val();
	var postId = $("#offer_post").val();
	if (date != '' && postId != '') {
		$.ajax({
			type : "GET",
			url : 'getCandidatesByInterviewDateAndPost.do',
			data : {
				"date" : date,
				"postId" : postId
			},
			cache : false,
			async : true,
			success : function(response) {
				var data = response.result;
				$("#offer_cand option").remove();
				var options = '<option value=' + "" + '>' + "Select Candidate"
						+ '</option>';
				$.each(data, function(index, item) {
					options += '<option value=' + item.jobCandidateId + '>'
							+ item.firstName + '</option>';
				});

				$("#offer_cand").append(options);
				$('#offer_cand').chosen().trigger("chosen:updated");
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

function getCandidatesBydateAndPost() {
	var date = $("#appoint_date").val();
	var postId = $("#appoint_post").val();
	if (date != '' && postId != '') {
		$.ajax({
			type : "GET",
			url : 'getCandidatesByInterviewDateAndPost.do',
			data : {
				"date" : date,
				"postId" : postId
			},
			cache : false,
			async : true,
			success : function(response) {
				var data = response.result;
				$("#appoint_cand option").remove();
				var options = '<option value=' + " " + '>' + "Select Candidate"
						+ '</option>';
				$.each(data, function(index, item) {
					options += '<option value=' + item.jobCandidateId + '>'
							+ item.firstName + '</option>';
				});

				$("#appoint_cand").append(options);
				$('#appoint_cand').chosen().trigger("chosen:updated");
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

function findINterView(date) {
	var result;
	$.ajax({
		type : "GET",
		url : 'getInterviewsDate.do',
		data : {
			"date" : date,
		},
		cache : false,
		async : true,
		success : function(response) {
			var data = response.result;
			result = data;
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	return result;
}

function getInterviewsBydate(date) {
	if (date != '') {
		$.ajax({
			type : "GET",
			url : 'getInterviewsDate.do',
			data : {
				"date" : date,
			},
			cache : false,
			async : true,
			success : function(response) {
				var data = response.result;
				$("#offer_post option").remove();
				var options = '<option value=' + "" + '>'
						+ "Select Designation" + '</option>';
				$.each(data, function(index, item) {
					options += '<option value=' + item.jobPostId + '>'
							+ item.jobPost + '</option>';
				});

				$("#offer_post").append(options);
				$('#offer_post').chosen().trigger("chosen:updated");
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

function getInterviewsOfferBydate(date) {
	if (date != '') {
		$.ajax({
			type : "GET",
			url : 'getInterviewsDate.do',
			data : {
				"date" : date,
			},
			cache : false,
			async : true,
			success : function(response) {
				var data = response.result;
				$("#appoint_post option").remove();
				var options = '<option value=' + "" + '>'
						+ "Select Designation" + '</option>';
				;
				$.each(data, function(index, item) {
					options += '<option value=' + item.jobPostId + '>'
							+ item.jobPost + '</option>';
				});

				$("#appoint_post").append(options);
				$('#appoint_post').chosen().trigger("chosen:updated");
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

function getEmployeePerformance() {
	var performanceVo = new Object();
	performanceVo.evaluationId = $("#add_perform_id").val();
	performanceVo.employeeCode = $("#employeeCode").val();
	$.ajax({

		type : "POST",
		url : "getEmployeePerformance.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(performanceVo),
		cache : false,
		async : true,
		success : function(response) {
			var qual = response.result;
			$("#add_perform_branch").val(qual.branch);
			$("#add_perform_des").val(qual.designation);
			$("#add_perform_max").val(qual.maxMark);
			$("#add_perform_score").val(qual.score);
			$("#add_perform_individual").val(qual.individualId);
			if (qual.individualId != null)
				$("#add_perform_delete").show();

		},
		error : function(response) {
			alert(response.status);
		}

	});

}
deleteEmployeePerformance = function(id, individualId) {
	$.ajax({
		type : "POST",
		url : "EmployeePerformanceEvaluationDelete.do",
		data : {
			"evaluationId" : id,
			"individualId" : individualId
		},
		cache : false,
		async : true,
		success : function(response) {
			$("#employeeSl").val('');
			$("#add_perform_branch").val('');
			$("#add_perform_des").val('');
			$("#add_perform_max").val('');
			$("#add_perform_score").val('');
			$("#add_perform_individual").val('');
			$("#add_perform_delete").hide();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function getEmployeeForAttendance() {
	var branchIds = [];
	var departmentIds = [];
	var url = '';
	branchIds.push($("#drop_one").val());
	departmentIds.push($("#drop_two").val());
	if (branchIds.length != 0 || departmentIds.length != 0) {
		var vo = new Object();
		vo.branchIds = branchIds;
		vo.departmentIds = departmentIds;
		if (branchIds.length != 0 && departmentIds.length != 0) {
			url = "getEmployeesByBranchAndDepartment.do";
		} else {
			if (branchIds.length == 0) {
				url = "getEmployeeesByDepartment.do";
			} else if (departmentIds.length == 0) {
				url = "getEmployeeesByBranch.do";
			}
		}
		$.ajax({
			type : "POST",
			url : url,
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(vo),
			cache : false,
			async : true,
			success : function(response) {

				var data = response.result;

				$("#drop_emp option").remove();

				var options = '';
				$.each(data, function(index, item) {
					if (item.name != null) {
						options += '<option value="' + item.employeeCode + '">'
								+ item.name + '</option>';
					}
				});
				$("#drop_emp").append(options);
				/*
				 * $("#drop_emp").multiselect('rebuild');
				 * $('#drop_emp').multiselect('refresh')
				 */

			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

function fillAttendanceLeave() {
	var employeeCode = $("#drop_emp").val();
	if (employeeCode != '') {
		$.ajax({
			type : "POST",
			url : 'getEmployeeDetailsById.do',
			data : {
				"employeeCode" : employeeCode,
			},
			cache : false,
			async : true,
			success : function(response) {
				var data = response.result;
				var leave = data.leaveVos;
				$("#add_attSts_sts option").remove();

				var options = '<option value="">Present</option>';
				$.each(leave, function(index, item) {
					options += "<option value='" + item.leaveTitleId + "'>"
							+ item.leaveTitle + "</option>";
				});
				$("#add_attSts_sts").append(options);
				/*
				 * try {
				 * $("#add_attSts_sts").val($("#leave_type_ajx_hidn").val()); }
				 * catch (e) { }
				 */
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});

	}
}
/**
 * employee pop up
 */
function clikcedSelectDeprtmnt() {
	if ($("#all_dept").is(":checked") == true) {
		$('.dept_chk').each(function() { // loop through each checkbox
			this.checked = true; // select all checkboxes with class
			// "checkbox1"
		});
	} else {
		$('.dept_chk').each(function() { // loop through each checkbox
			this.checked = false; // select all checkboxes with class
			// "checkbox1"
		});
	}
}

function clikcedSelectBranch() {
	if ($("#all_brnch").is(":checked") == true) {
		$('.brnch_chk').each(function() { // loop through each checkbox
			this.checked = true; // select all checkboxes with class
			// "checkbox1"
		});
	} else {
		$('.brnch_chk').each(function() { // loop through each checkbox
			this.checked = false; // select all checkboxes with class
			// "checkbox1"
		});
	}
}

function filterEmployees() {
	$("#load-erp").css("display", "block");
	$("#btn-select-employee-pop").attr("disabled", true);

	branchIds = [];
	deparmtentIds = [];
	$('.brnch_chk').each(function() {
		if (this.checked) {
			branchIds.push(this.value);
		}
	});
	$('.dept_chk').each(function() {
		if (this.checked) {
			deparmtentIds.push(this.value);
			var uncheckedval = !this.checked ? this.value : '';
			deparmtentIds = jQuery.grep(deparmtentIds, function(value) {
				return value != uncheckedval;
			});
		}
	});
	var vo = new Object();
	 vo.branchIdList =branchIds;
	 vo.departmentIdList = deparmtentIds;


	$.ajax({
		type : "POST",
		url : "popUpemployees.do",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		data :JSON.stringify(vo),
		success : function(response) {
			finalFilter();
		},
		error : function(requestObject, error, errorThrown) {
			finalFilter();
		}
	});
	
}

var popId="";

finalFilter = function(){
	if(popId =='')
	{
		popId= $("#popIdHid").val();
		if(typeof popId == 'undefined')
		{
			popId='pop1';
		}
	}
	
	$.ajax({
		type : "GET",
		url : "filteremp.do",
		dataType : "html",
		cache : false,
		async : true,
		success : function(response) {
			
			$("#"+popId).html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete:function(){
			$("#load-erp").css("display", "none");
		}
	});
}

showPop = function(){
	 popId='pop1';
	$.ajax({
		type : "GET",
		url : "popUp.do",
		dataType : "html",
		cache : false,
		async : true,
		success : function(response) {
			$("#"+popId).html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	$("#"+popId).simplePopup();
}

showPopRport = function (pop , text){
	$("#popIdHid").val(pop);
	$.ajax({
		type : "GET",
		url : "popUp.do",
		dataType : "html",
		cache : false,
		async : true,
		success : function(response) {
			$("#"+pop).html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	$("#"+pop).simplePopup();
}

/**
 * pop end
 */

/**
 * Set Employees with auto approve privillege
 */
$("#auto-aprv-tab").click(function() {
	$.ajax({
		type : "GET",
		url : "getAutoApproveEmployees",
		cache : false,
		async : true,
		success : function(response) {
			$("#auto-aprov-settings").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});

deleteAutoApprove = function(empId) {
	$.ajax({
		type : "POST",
		url : "changeAutoApproval",
		cache : false,
		async : true,
		data : {
			"employeeId" : empId
		},
		success : function(response) {
			$("#auto-aprov-settings").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}
