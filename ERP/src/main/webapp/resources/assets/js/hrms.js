/**
 * @author Jithin Mohan
 * @since 11-March-2015
 */
/*----------------------------------------------------------*/

$("#msgs_icn").click(function() {
	$(".success_msg").fadeOut(500);
});

$("#msge_icn").click(function() {
	$(".error_msg").fadeOut(500);
});

setTimeout(function() {
	$('.success_msg').fadeOut(500);
}, 3000);
setTimeout(function() {
	$('.error_msg').fadeOut(500);
}, 3000);

/**
 * input numbers only
 */
$('.numbersonly').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++)
		a.push(i);

	if (!(a.indexOf(k) >= 0))
		e.preventDefault();

});

$(".employee_code").keypress(function(e){
	var englishAlphabetAndWhiteSpace = /[0-9A-Za-z_-]/g;
	var key = String.fromCharCode(event.which);
	if (event.keyCode == 8 ||  event.keyCode == 39
			|| englishAlphabetAndWhiteSpace.test(key)) {
		return true;
	} 
	 if(e.shiftKey && e.keyCode == 222 || e.keyCode == 222){
	        e.stopPropagation();
	    }
	return false;
});

$('.employee_code').bind("paste",function(e) {
    e.preventDefault();
});

$('.numbersonly').bind("paste",function(e) {
    e.preventDefault();
});

$('.paste').bind("paste",function(e) {
    e.preventDefault();
});
/*******************************************************************************
 * only alphabets
 */
$(".alphabets").on(
		"keypress",
		function(event) {
			var englishAlphabetAndWhiteSpace = /[A-Za-z ]/g;
			var key = String.fromCharCode(event.which);
			if (event.keyCode == 8 || event.keyCode == 37
					|| event.keyCode == 39
					|| englishAlphabetAndWhiteSpace.test(key)) {
				return true;
			}
			return false;
		});

$('.alphabets').on("paste", function(e) {
	e.preventDefault();
});

/**
 * double
 */
$('.double').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++) {
		a.push(i);
	}
	if (i = 46) {
		if($(this).val().indexOf('.') == -1)
		a.push(i);
	}
	if (!(a.indexOf(k) >= 0))
		e.preventDefault();
	//alert($(this).val().indexOf('.'));
});
$('.double').bind("paste",function(e) {
    e.preventDefault();
});

/*******************************************************************************
 * validate email
 */
function ValidateEmail(email) {
	var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	return expr.test(email);
};
/**
 * function to populate month and dates
 * 
 */
$(function() {
	var monthList = [ 'January', 'February', 'March', 'April', 'May', 'June',
			'July', 'August', 'September', 'October', 'November', 'December' ];
	var months = "<select class='form-control' name='reminderMonth' onchange='getNumberOfDays(this.value)'><option value=''>-Select-</option>";
	$.each(monthList, function(i, val) {
		months = months + "<option value='" + (parseInt(i) + 1) + "'>" + val
				+ "</option>";
	});
	months = months + "</select>";
	$("#month-list").html(months);
});
getNumberOfDays = function(month) {
	var days = "<select name='reminderDay' class='form-control'>";
	for (var i = 0; i < new Date().daysInMonth(parseInt(month) - 1); i++) {
		days = days + "<option value='" + (parseInt(i) + 1) + "'>"
				+ (parseInt(i) + 1) + "</option>";
	}
	days = days + "</select>";
	$("#day-list").html(days);
}
Date.prototype.daysInMonth = function(month) {
	return new Date(this.getFullYear(), parseInt(month) + 1, 0).getDate()
}

/**
 * Organisation
 */
$(function() {
	var monthList = [ 'January', 'February', 'March', 'April', 'May', 'June',
			'July', 'August', 'September', 'October', 'November', 'December' ];
	var months = "<select class='form-control' name='fiscalMonth' onchange='getNumberOfDaysOrg(this.value)'><option value=''>-Select-</option>";
	$.each(monthList, function(i, val) {
		months = months + "<option value='" + (parseInt(i) + 1) + "'>" + val
				+ "</option>";
	});
	months = months + "</select>";
	$("#omonth-list").html(months);

	var years = "<select class='form-control organisationYear' name='organisationYear'>";
	years = years + "<option value=''>-Select-</option>";
	for (var i = 20; i >= 0; i--) {
		years = years + "<option value='"
				+ (parseInt((new Date).getFullYear()) - i) + "'>"
				+ (parseInt((new Date).getFullYear()) - i) + "</option>";
	}
	years = years + "</select>";
	$("#oyear-list").html(years);

});
getNumberOfDaysOrg = function(month) {
	var days = "<select name='fiscalDay' class='form-control'>";
	for (var i = 0; i < new Date().daysInMonth(parseInt(month) - 1); i++) {
		days = days + "<option value='" + (parseInt(i) + 1) + "'>"
				+ (parseInt(i) + 1) + "</option>";
	}
	days = days + "</select>";
	$("#oday-list").html(days);
}

/**
 * Section for handling workshift timebox enable and disable
 */
timePickerEnable = function(begin) {
	var count = (parseInt(begin) - 1) * 6;
	for (var i = parseInt(count) + 1; i <= (parseInt(count) + 6); i++) {
		$('#time_setter' + i).removeAttr("disabled");
		$('#time_setter' + i).timepicker({
			value : $('#time_setter' + i).val()
		});
	}
}
timePickerDisable = function(begin) {
	var count = (parseInt(begin) - 1) * 6;
	for (var i = parseInt(count) + 1; i <= (parseInt(count) + 6); i++) {
		if ($('#time_setter' + i).val() == "") {
			$('#time_setter' + i).val('00:00');
			$('#time_setter' + i).timepicker({
				value : '00:00'
			});
		}
		$('#time_setter' + i).attr("disabled", "true");
	}
}
$("#shiftDay1").click(function() {
	if (this.checked) {
		timePickerEnable(1);
	} else {
		timePickerDisable(1);
	}
});
$("#shiftDay2").click(function() {
	if (this.checked) {
		timePickerEnable(2);
	} else {
		timePickerDisable(2);
	}
});
$("#shiftDay3").click(function() {
	if (this.checked) {
		timePickerEnable(3);
	} else {
		timePickerDisable(3);
	}
});
$("#shiftDay4").click(function() {
	if (this.checked) {
		timePickerEnable(4);
	} else {
		timePickerDisable(4);
	}
});
$("#shiftDay5").click(function() {
	if (this.checked) {
		timePickerEnable(5);
	} else {
		timePickerDisable(5);
	}
});
$("#shiftDay6").click(function() {
	if (this.checked) {
		timePickerEnable(6);
	} else {
		timePickerDisable(6);
	}
});
$("#shiftDay7").click(function() {
	if (this.checked) {
		timePickerEnable(7);
	} else {
		timePickerDisable(7);
	}
});

$(document).ready(function() {
	if ($("#shiftDay1").is(':checked')) {
		timePickerEnable(1);
	} else {
		timePickerDisable(1);
	}

	if ($("#shiftDay2").is(':checked')) {
		timePickerEnable(2);
	} else {
		timePickerDisable(2);
	}

	if ($("#shiftDay3").is(':checked')) {
		timePickerEnable(3);
	} else {
		timePickerDisable(3);
	}

	if ($("#shiftDay4").is(':checked')) {
		timePickerEnable(4);
	} else {
		timePickerDisable(4);
	}

	if ($("#shiftDay5").is(':checked')) {
		timePickerEnable(5);
	} else {
		timePickerDisable(5);
	}

	if ($("#shiftDay6").is(':checked')) {
		timePickerEnable(6);
	} else {
		timePickerDisable(6);
	}

	if ($("#shiftDay7").is(':checked')) {
		timePickerEnable(7);
	} else {
		timePickerDisable(7);
	}

});

function checkCalculation(id) {
	var val = $("#select" + id).val();
	if (val == 'Fixed') {
		$(".itemSyntax" + id).val('');
		$(".itemSyntax" + id).addClass("double");
	} else {
		$(".itemSyntax" + id).removeClass("double");
	}
}


function findNoOfInstallments(amount){
	var loanAmount=$("#loan_amount").val();
	var installments = parseFloat(loanAmount)/parseFloat(amount);
	$("#no_of_instllmnt").val(installments.toFixed(0));
}

function findRepaymentAmount(installments){
	var loanAmount=$("#loan_amount").val();
	var amount=parseFloat(loanAmount)/parseFloat(installments);
	$("#rpymnt_amnt").val(amount.toFixed(2));
}

function findIstallmentsAndAmount(amount){
	var repaymentAmount=$("#rpymnt_amnt").val();
	if(repaymentAmount!=''){
		var installment = parseFloat(amount)/parseFloat(repaymentAmount);
		$("#no_of_instllmnt").val(installment.toFixed(0));
	}
}