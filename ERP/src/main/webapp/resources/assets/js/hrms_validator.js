/*******************************************************************************
 * Date converter
 * 
 * @param inputFormat
 * @returns
 */
function convertDate(inputFormat) {
	function pad(s) {
		return (s < 10) ? '0' + s : s;
	}
	var d = new Date(inputFormat);
	return [ pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear() ]
			.join('/');
}

function process(date) {
	var parts = date.split("/");
	return new Date(parts[2], parts[1] - 1, parts[0]);
}

/*
 * var date=new Date(); var currentDate=convertDate(date);
 */
/**
 * validation for add branch page
 */
$("#submt_branch_info_btn,#updt_branch_info_btn").click(
		function() {

			if ($("#add_branch_type").val() != ''
					&& $("#add_branch_name") != ''
					&& $("#add_branch_strdt") != ''
					&& $("#add_branch_address").val() != ''
					&& $("#add_branch_city").val() != ''
					&& $("#add_branch_state").val() != ''
					&& $("#add_branch_country").val() != ''
					&& $("#add_branch_zipCode").val() != ''
					&& $("#add_branch_phoneNumber").val() != '') {
				if ($("#add_branch_eMail").val() != '') {
					if (ValidateEmail($("#add_branch_eMail").val()) == false) {
						$("#add_branch_eMail").focus();
					} else {
						saveBranchInfoFunction();
						$("#submt_branch_info_btn").attr("disabled", "true");
					}
				} else {
					saveBranchInfoFunction();
					$("#submt_branch_info_btn").attr("disabled", "true");
				}
			} else {

				if ($("#add_branch_type").val() == '')
					$("#add_branch_type").focus();
				else if ($("#add_branch_name").val() == '')
					$("#add_branch_name").focus();
				else if ($("#add_branch_address").val() == '')
					$("#add_branch_address").focus();
				else if ($("#add_branch_city").val() == '')
					$("#add_branch_city").focus();
				else if ($("#add_branch_state").val() == '')
					$("#add_branch_state").focus();
				else if ($("#add_branch_zipCode").val() == '')
					$("#add_branch_zipCode").focus();
				else if ($("#add_branch_country").val() == '')
					$("#add_branch_country").focus();
				else if ($("#add_branch_phoneNumber").val() == '')
					$("#add_branch_phoneNumber").focus();
			}

		});
/**
 * validates form
 */

$("#submt_proj_info_btn").click(
		function() {
			var date = new Date();
			var currentDate = convertDate(date);

			var projectStartDate = $("#add_project_startdate").val();
			var projectEndDate = $("#add_project_enddate").val();

			if ($("#add_project_title").val() != ''
					&& $("#add_project_startdate").val() != ''
					&& $("#add_project_enddate").val() != ''
					&& $("#add_project_employees").val() != null
					&& $("#add_project_description").val() != ''
					&& $("#add_project_head").val() != null) {
				if (process(projectStartDate) > process(projectEndDate)) {
					$("#prjct_end_date_err").css("display", "block");
					setTimeout(function() {
						$("#prjct_end_date_err").fadeOut(500);
					}, 1000);
					$("#add_project_enddate").focus();
				} else if (process(currentDate) > process(projectEndDate)) {
					$("#prjct_end_date_err").css("display", "block");
					setTimeout(function() {
						$("#prjct_end_date_err").fadeOut(500);
					}, 1000);
					$("#add_project_enddate").focus();
				} else {
					saveProjectInfo();
				}
			} else {
				if ($("#add_project_title").val() == '')
					$("#add_project_title").focus();
				else if ($("#add_project_startdate").val() == '')
					$("#add_project_startdate").focus();
				else if ($("#add_project_enddate").val() == '')
					$("#add_project_enddate").focus();
				else if ($("#add_project_employees").val() == null) {
					$("#prjct_emp_err").css("display", "block");
					setTimeout(function() {
						$("#prjct_emp_err").fadeOut(500);
					}, 1000);
				} else if ($("#add_project_head").val() == null) {
					$("#prjct_head_err").css("display", "block");
					setTimeout(function() {
						$("#prjct_head_err").fadeOut(500);
					}, 1000);
				} else if ($("#add_project_description").val() == '')
					$("#add_project_description").focus();

			}

		});

$("#updt_proj_info_btn").click(
		function() {
			var date = new Date();
			var currentDate = convertDate(date);

			var projectStartDate = $("#add_project_startdate").val();
			var projectEndDate = $("#add_project_enddate").val();
			if ($("#add_project_client") != ''
					&& $("#add_project_startdate").val() != ''
					&& $("#add_project_enddate").val() != ''
					&& $("#add_project_employees").val() != null
					&& $("#add_project_description").val() != '') {
				if (process(projectStartDate) > process(projectEndDate)) {
					$("#prjct_end_date_err").css("display", "block");

					$("#add_project_enddate").focus();
				} else if (process(currentDate) > process(projectEndDate)) {
					$("#prjct_end_date_err").css("display", "block");
					$("#add_project_enddate").focus();
				} else {
					updateProjectInfo();
				}
				setTimeout(function() {
					$('#prjct_end_date_err').fadeOut(500);
				}, 3000);
			} else {
				if ($("#add_project_client").val() == '')
					$("#add_project_client").focus();
				else if ($("#add_project_startdate").val() == '')
					$("#add_project_startdate").focus();
				else if ($("#add_project_enddate").val() == '')
					$("#add_project_enddate").focus();
				else if ($("#add_project_employees").val() == null) {
					$("#prjct_emp_err").css("display", "block");
					setTimeout(function() {
						$("#prjct_emp_err").fadeOut(500);
					}, 1000);
				} else if ($("#add_project_description").val() == '')
					$("#add_project_description").focus();

			}
		});
// to restrict input to alphabets only (include class name)
$('.').keypress(
		function(key) {
			if ((key.charCode < 97 || key.charCode > 122)
					&& (key.charCode < 65 || key.charCode > 90)
					&& (key.charCode != 45))
				return false;
		});

// to restrict input to numeric only (include class name)
$('.').keypress(function(key) {
	if (key.charCode < 48 || key.charCode > 57)
		return false;
});

// to avoid special characters
$('.').keyup(
		function() {
			var yourInput = $(this).val();
			re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
			var isSplChar = re.test(yourInput);
			if (isSplChar) {
				var no_spl_char = yourInput.replace(
						/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				$(this).val(no_spl_char);
			}
		});

/*******************************************************************************
 * EMPLOYEE JOINING Select employee
 */

function validateEmployeeJoin() {
	if ($("#emp_join_emp").val() != '' && $("#emp_join_joindate").val() != ''
			&& $("#emp_join_emptype").val() != ''
			&& $("#emp_join_emptdes").val()
			&& $("#emp_join_emptbrnch").val() != ''
			&& $("#emp_join_emptdept").val()) {
		updateEmployeeJoin();
	} else {
		if ($("#emp_join_emp").val() == '') {
			$("#emp_join_emp_err").css("display", "block");
			setTimeout(function() {
				$("#emp_join_emp_err").fadeOut(500);
			}, 1000);
		} else if ($("#emp_join_joindate").val() == '')
			$("#emp_join_joindate").focus();
		else if ($("#emp_join_emptype").val() == '')
			$("#emp_join_emptype").focus();
		else if ($("#emp_join_emptdes").val() == '')
			$("#emp_join_emptdes").focus();
		else if ($("#emp_join_emptbrnch").val() == '')
			$("#emp_join_emptbrnch").focus();
		else if ($("#emp_join_emptdept").val() == '')
			$("#emp_join_emptdept").focus();
	}
}
/** **************************************************************** */

/*******************************************************************************
 * ---------------- EMPLOYEE ----------------------
 */
function vldteUserPassEmp() {
	if ($("#emp_user_name").val() != '' && $("#emp_user_pwd").val() != ''
			&& $("#emp_user_email").val() != ''
			&& ValidateEmail($("#emp_user_email").val())) {
		saveUserPassWord();
	} else {
		if ($("#emp_user_name").val() == '')
			$("#emp_user_name").focus();
		else if ($("#emp_user_pwd").val() == '')
			$("#emp_user_pwd").focus();
		else if ($("#emp_user_email").val() == '')
			$("#emp_user_email").focus();
		else if (!ValidateEmail($("#emp_user_email").val())) {
			$("#valid_email_errr").css("display", "block");
			setTimeout(function() {
				$('#valid_email_errr').fadeOut(500);
			}, 2000);
			$("#emp_user_email").focus();
		}

	}
}

/** ******* */

function vldateUserPersonalEmp() {

	if ($("#emp_uprofile_salut").val() != ''
			&& $("#emp_uprofile_first_name").val() != ''
			&& $("#emp_uprofile_last_name").val() != ''
			&& $("#emp_uprofile_dob").val() != ''
			&& $("input[name=gender]").is(":checked")
			&& $("#emp_uprofile_blood").val() != ''
			&& $("#emp_uprofile_relegion").val() != ''
			&& $("#emp_uprofile_nationality").val() != '') {
		saveEmpPersonalInfo();
	} else {
		if ($("#emp_uprofile_salut").val() == '')
			$("#emp_uprofile_salut").focus();
		else if ($("#emp_uprofile_first_name").val() == '')
			$("#emp_uprofile_first_name").focus();
		else if ($("#emp_uprofile_last_name").val() == '')
			$("#emp_uprofile_last_name").focus();
		else if ($("#emp_uprofile_dob").val() == '')
			$("#emp_uprofile_dob").focus();
		else if (!$("input[name=gender]:checked").val()) {
			$("#gndr_select").css("display", "block");
			setTimeout(function() {
				$('#gndr_select').fadeOut(500);
			}, 1000);
		} else if ($("#emp_uprofile_blood").val() == '') {
			$("#blood_select").css("display", "block");
			setTimeout(function() {
				$('#blood_select').fadeOut(500);
			}, 1000);
		} else if ($("#emp_uprofile_relegion").val() == '') {
			$("#relegion_select").css("display", "block");
			setTimeout(function() {
				$('#relegion_select').fadeOut(500);
			}, 1000);
		} else if ($("#emp_uprofile_nationality").val() == '') {
			$("#country_select").css("display", "block");
			setTimeout(function() {
				$('#country_select').fadeOut(500);
			}, 1000);
		}
	}
}

/** ********************** */

/** ********************** */

function vldteAdditionalInfo() {
	if ($("#emp_add_info_joindate").val() != '') {
		saveAdditionalInfo();
	} else {
		$("#emp_add_info_joindate").focus();
	}
}
/** ****************************** */

function validateEmployeeLanguage() {
	if ($("#emp_lngge_add_lngg").val() != ''
			&& $("#emp_lngge_add_spklvl").val() != ''
			&& $("#emp_lngge_add_redlvl").val() != ''
			&& $("#emp_lngge_add_wrtlvl").val() != '') {
		saveEmployeeLanguage();
	} else {
		if ($("#emp_lngge_add_lngg").val() == '') {
			$("#lngge_select").css("display", "block");
			setTimeout(function() {
				$("#lngge_select").fadeOut(500);
			}, 1000);
		} else if ($("#emp_lngge_add_spklvl").val() == '') {
			$("#lngge_select_spk").css("display", "block");
			setTimeout(function() {
				$("#lngge_select_spk").fadeOut(500);
			}, 1000);
		} else if ($("#emp_lngge_add_redlvl").val() == '') {
			$("#lngge_select_rdlvl").css("display", "block");
			setTimeout(function() {
				$("#lngge_select_rdlvl").fadeOut(500);
			}, 1000);
		} else if ($("#emp_lngge_add_wrtlvl").val() == '') {
			$("#lngge_select_wrtlvl").css("display", "block");
			setTimeout(function() {
				$("#lngge_select_wrtlvl").fadeOut(500);
			}, 1000);
		}
	}
}

/**
 * // date comparison if(process($("#meeting_add_strdt").val()) >
 * process($("#meeting_add_enddt").val())){ $("#meeting_add_enddt").focus();
 * $("#mtng_date_err").css("display","block"); setTimeout(function(){
 * $("#mtng_date_err").fadeOut(500); },2000); }else if(process(currentDate) >
 * process($("#meeting_add_enddt").val())){ $("#meeting_add_enddt").focus();
 * $("#mtng_date_err").css("display","block"); setTimeout(function(){
 * $("#mtng_date_err").fadeOut(500); },2000);
 */

/** ****************************** */
function validateEmpExprnce() {
	if ($("#emp_exprnc_add_orgn").val() != ''
			&& $("#emp_exprnc_add_job").val() != ''
			&& $("#emp_exprnc_add_field").val() != ''
			&& $("#emp_exprnc_add_desc").val() != ''
			&& $("#emp_exprnc_add_strtdt").val() != ''
			&& $("#emp_exprnc_add_enddt").val() != ''
			&& $("#emp_exprnc_add_strtsly").val() != ''
			&& $("#emp_exprnc_add_endsly").val() != '') {
		if (process($("#emp_exprnc_add_strtdt").val()) > process($(
				"#emp_exprnc_add_enddt").val())) {
			$("#emp_exprnc_add_enddt").focus();
			$("#exprnce_date_err").css("display", "block");
			setTimeout(function() {
				$("#exprnce_date_err").fadeOut(500);
			}, 2000);
		} else {
			saveEmpExperience();
		}
	} else {
		if ($("#emp_exprnc_add_orgn").val() == '')
			$("#emp_exprnc_add_orgn").focus();
		else if ($("#emp_exprnc_add_job").val() == '') {
			$("#exprnce_select_design").css("display", "block");
			setTimeout(function() {
				$('#exprnce_select_design').fadeOut(500);
			}, 1000);
		} else if ($("#emp_exprnc_add_field").val() == '') {
			$("#exprnce_select_field").css("display", "block");
			setTimeout(function() {
				$('#exprnce_select_field').fadeOut(500);
			}, 1000);
		} else if ($("#emp_exprnc_add_desc").val() == '')
			$("#emp_exprnc_add_desc").focus();
		else if ($("#emp_exprnc_add_strtdt").val() == '')
			$("#emp_exprnc_add_strtdt").focus();
		else if ($("#emp_exprnc_add_enddt").val() == '')
			$("#emp_exprnc_add_enddt").focus();
		else if ($("#emp_exprnc_add_strtsly").val() == '')
			$("#emp_exprnc_add_strtsly").focus();
		else if ($("#emp_exprnc_add_endsly").val() == '')
			$("#emp_exprnc_add_endsly").focus();

	}
}
/** ************************************************* */

function validateBankAccountEmp() {
	if ($("#emp_bank_add_name").val() != ''
			&& $("#emp_bank_add_branch").val() != ''
			&& $("#emp_bank_add_branch_code").val() != ''
			&& $("#emp_bank_add_titel").val() != ''
			&& $("#emp_bank_add_acno").val() != ''
			&& $("#emp_bank_add_type").val() != '') {
		saveEmployeeBank();
	} else {
		if ($("#emp_bank_add_name").val() == '')
			$("#emp_bank_add_name").focus();
		else if ($("#emp_bank_add_branch").val() == '')
			$("#emp_bank_add_branch").focus();
		else if ($("#emp_bank_add_branch_code").val() == '')
			$("#emp_bank_add_branch_code").focus();
		else if ($("#emp_bank_add_titel").val() == '')
			$("#emp_bank_add_titel").focus();
		else if ($("#emp_bank_add_acno").val() == '')
			$("#emp_bank_add_acno").focus();
		else if ($("#emp_bank_add_type").val() == '') {
			$("#bank_select_type").css("display", "block");
			setTimeout(function() {
				$('#bank_select_type').fadeOut(500);
			}, 1000);
		}
	}
}
/** ************************** */

function validateDependants() {
	if ($("#emp_depnts_nm").val() != '' && $("#emp_depnts_dob").val() != ''
			&& $("#emp_depnts_rltn").val() != ''
			&& $("input[name=nominee]").is(":checked")) {
		saveEmpDepandants();
	} else {
		if ($("#emp_depnts_nm").val() == '')
			$("#emp_depnts_nm").focus();
		else if ($("#emp_depnts_dob").val() == '')
			$("#emp_depnts_dob").focus();
		else if ($("#emp_depnts_rltn").val() == '')
			$("#emp_depnts_rltn").focus();
		else if (!$("input[name=nominee]:checked").val()) {
			$("#dpndnts_select").css("display", "block");
			setTimeout(function() {
				$('#dpndnts_select').fadeOut(500);
			}, 1000);

		}
	}
}
/** ************************* */

/** *********************** */
function validateNextOfKin() {
	if ($("#emp_add_nxtkin_name").val() != ''
			&& $("#emp_add_nxtkin_dob").val() != ''
			&& $("#emp_add_nxtkin_rltn").val() != '') {
		saveEmpNextOfKin();
	} else {
		if ($("#emp_add_nxtkin_name").val() == '')
			$("#emp_add_nxtkin_name").focus();
		else if ($("#emp_add_nxtkin_dob").val() == '')
			$("#emp_add_nxtkin_dob").focus();
		else if ($("#emp_add_nxtkin_rltn").val() == '')
			$("#emp_add_nxtkin_rltn").focus();
	}
}

/** ************************ */

function validateSkillEmp() {
	if ($("#emp_skll_skill").val() != '' && $("#emp_skll_lvl").val() != '') {
		saveEmpSkill();
	} else {
		if ($("#emp_skll_skill").val() == '') {
			$("#skill_select_skill").css("display", "block");
			setTimeout(function() {
				$('#skill_select_skill').fadeOut(500);
			}, 1000);
		} else if ($("#emp_skll_lvl").val() == '') {
			$("#skill_select_level").css("display", "block");
			setTimeout(function() {
				$('#skill_select_level').fadeOut(500);
			}, 1000);
		}
	}
}

/** ************************* */
function validateEmpUniform() {
	if ($("#emp_unfrm_dt").val() != '' && $("#emp_unfrm_amt").val() != ''
			&& $("#emp_unfrm_drtn").val() != '') {
		saveEmpUniform();
	} else {
		if ($("#emp_unfrm_dt").val() == '')
			$("#emp_unfrm_dt").focus();
		else if ($("#emp_unfrm_amt").val() == '')
			$("#emp_unfrm_amt").focus();
	}
}

/** ************************ */
function validateEmpAssets() {
	if ($("#emp_assts_ast").val() != ''
			&& $("#emp_assts_sve_chnkout").val() != '') {
		saveEmpAssets();
	} else {
		if ($("#emp_assts_ast").val() == '') {
			$("#emp_select_asset").css("display", "block");
			setTimeout(function() {
				$('#emp_select_asset').fadeOut(500);
			}, 1000);
		} else if ($("#emp_assts_sve_chnkout").val() == '')
			$("#emp_assts_sve_chnkout").focus();
	}
}

/** ******************************** */
function validateQualificationEmployee() {
	if ($("#emp_qulfctn_add_dgre").val() != '') {
		saveEmpQualification();
	} else {
		if ($("#emp_qulfctn_add_dgre").val() == '') {
			$("#degree_select").css("display", "block");
			setTimeout(function() {
				$('#degree_select').fadeOut(500);
			}, 1000);
		}
	}
}

/** ****************************** */
function validateEmployeeLEave() {
	if ($("#save_emp_leave_title").val() != ''
			&& $("#save_emp_leave_leaves").val() != ''
			&& $("input[name=leave]").is(":checked")) {
		saveEmployeeLeave();
	} else {
		if ($("#save_emp_leave_title").val() == '') {
			$("#leave_select_title").css("display", "block");
			setTimeout(function() {
				$("#leave_select_title").fadeOut(500);
			}, 1000);
		} else if ($("#save_emp_leave_leaves").val() == '')
			$("#save_emp_leave_leaves").focus();
		else if (!$("input[name=leave]:checked").val()) {
			$("#leave_select").css("display", "block");
			setTimeout(function() {
				$('#leave_select').fadeOut(500);
			}, 1000);
		}
	}
}
/** **************************** */
function validatEmergencyCntct() {
	if ($("#emp_emrgncy_cntct_person").val() != ''
			&& $("#emp_emrgncy_cntct_relation").val() != ''
			&& $("#emp_emrgncy_cntct_phone").val() != '') {
		saveEmergencyCntct();
	} else {
		if ($("#emp_emrgncy_cntct_person").val() == '')
			$("#emp_emrgncy_cntct_person").focus();
		else if ($("#emp_emrgncy_cntct_relation").val() == '')
			$("#emp_emrgncy_cntct_relation").focus();
		else if ($("#emp_emrgncy_cntct_phone").val() == '')
			$("#emp_emrgncy_cntct_phone").focus();
	}
}

/** ******************************* */
function validateEmployeeReference() {
	if ($("#emp_rfrnc_name").val() != '' && $("#emp_rfrnc_orgnztn").val() != ''
			&& $("#emp_rfrnc_phn").val() != '') {
		if ($("#emp_rfrnc_eml").val() != '') {
			if (!ValidateEmail($("#emp_rfrnc_eml").val())) {
				$("#valid_email_err_rfrnce").css("display", "block");
				setTimeout(function() {
					$('#valid_email_err_rfrnce').fadeOut(500);
				}, 2000);
				$("#emp_user_email").focus();
				$("#emp_rfrnc_eml").focus();
			} else {
				saveEmployeReference();
			}
		} else {
			saveEmployeReference();
		}
	} else {
		if ($("#emp_rfrnc_name").val() == '')
			$("#emp_rfrnc_name").focus();
		if ($("#emp_rfrnc_orgnztn").val() == '')
			$("#emp_rfrnc_orgnztn").focus();
		if ($("#emp_rfrnc_phn").val() == '')
			$("#emp_rfrnc_phn").focus();
	}
}
/** ************************** */
function vlidteAddress() {
	if ($("#emp_prmnt_cntct_addr").val() != ''
			&& $("#emp_prmnt_cntct_city").val() != ''
			&& $("#emp_prmnt_cntct_stt").val() != ''
			&& $("#emp_prmnt_cntct_zip").val() != ''
			&& $("#emp_prmnt_cntct_cntry").val() != '') {
		saveAddressEmp();
	} else {
		if ($("#emp_prmnt_cntct_addr").val() == '')
			$("#emp_prmnt_cntct_addr").focus();
		if ($("#emp_prmnt_cntct_city").val() == '')
			$("#emp_prmnt_cntct_city").focus();
		if ($("#emp_prmnt_cntct_stt").val() == '')
			$("#emp_prmnt_cntct_stt").focus();
		if ($("#emp_prmnt_cntct_zip").val() == '')
			$("#emp_prmnt_cntct_zip").focus();
		if ($("#emp_prmnt_cntct_cntry").val() == '') {
			$("#cntry_cntct_select").css("display", "block");
			setTimeout(function() {
				$("#cntry_cntct_select").fadeOut(500);
			}, 1000);
		}
	}
}
/** *********************************** */

/*******************************************************************************
 * CONTRACT
 */

function validateContrctAdd() {
	var date = new Date();
	var currentDate = convertDate(date);

	var startDate = $("#cntrt_add_strdte").val();
	var endDate = $("#cntrt_add_enddte").val();

	if ($("#employeeSl").val() != '' && $("#contrct_add_type").val() != ''
			&& $("#cntrt_add_title").val() != ''
			&& $("#cntrt_add_strdte").val() != ''
			&& $("#cntrt_add_enddte").val() != '') {
		if (process(startDate) > process(endDate)) {
			$("#cntrt_add_enddte").focus();
			$("#prjct_end_date_err").css("display", "block");
			setTimeout(function() {
				$('#prjct_end_date_err').fadeOut(500);
			}, 3000);
		} else if (process(currentDate) > process(endDate)) {
			$("#cntrt_add_enddte").focus();
			$("#prjct_end_date_err").css("display", "block");
			setTimeout(function() {
				$('#prjct_end_date_err').fadeOut(500);
			}, 3000);
		} else {
			saveContractInfo();
		}
	} else if ($("#employeeSl").val() == '') {
		$("#emp_cntrt_emp_err").css("display", "block");
		setTimeout(function() {
			$('#emp_cntrt_emp_err').fadeOut(500);
		}, 1000);
	} else if ($("#contrct_add_type").val() == '')
		$("#contrct_add_type").focus();
	else if ($("#cntrt_add_title").val() == '')
		$("#cntrt_add_title").focus();
	else if ($("#cntrt_add_strdte").val() == '')
		$("#cntrt_add_strdte").focus();
	else if ($("#cntrt_add_enddte").val() == '')
		$("#cntrt_add_enddte").focus();
}

function validateContrctEdit() {
	var date = new Date();
	var currentDate = convertDate(date);

	var startDate = $("#cntrt_add_strdte").val();
	var endDate = $("#cntrt_add_enddte").val();

	if (process(startDate) > process(endDate)) {
		$("#cntrt_add_enddte").focus();
		$("#prjct_end_date_err").css("display", "block");
		setTimeout(function() {
			$('#prjct_end_date_err').fadeOut(500);
		}, 3000);
	} else if (endDate < currentDate) {
		$("#cntrt_add_enddte").focus();
		$("#prjct_end_date_err").css("display", "block");
		setTimeout(function() {
			$('#prjct_end_date_err').fadeOut(500);
		}, 3000);
	} else {
		if ($("#contrct_add_type").val != '' && $("#cntrt_add_title").val != ''
				&& $("#cntrt_add_strdte").val != ''
				&& $("#cntrt_add_enddte").val != '') {
			updateContractInfo();
		} else if ($("#contrct_add_type").val == '')
			$("#contrct_add_type").focus();
		else if ($("#cntrt_add_title").val == '')
			$("#cntrt_add_title").focus();
		else if ($("#cntrt_add_strdte").val == '')
			$("#cntrt_add_strdte").focus();
		else if ($("#cntrt_add_enddte").val == '')
			$("#cntrt_add_enddte").focus();
	}
}
/*******************************************************************************
 * ASSIGNMENTS
 */

function validateAddAssignment() {
	var date = new Date();
	var currentDate = convertDate(date);
	var assignmentStartDate = $("#assgmnt_add_strtdt").val();
	var assignmentEndDate = $("#assgmnt_add_enddt").val();

	if ($("#assignmentId").val() == '') {
		if ($("#employeeCode").val() != ''
				&& $("#assgnmt_add_prjct").val() != ''
				&& $("#assgmnt_add_name").val() != ''
				&& $("#assgmnt_add_strtdt").val() != ''
				&& $("#assgmnt_add_enddt").val() != ''
				&& $("#assgmnt_add_prio").val() != '') {
			if (process(currentDate) > process(assignmentEndDate)) {
				$("#assgnmnt_end_date").css("display", "block");
				setTimeout(function() {
					$("#assgnmnt_end_date").fadeOut(500);
				}, 1000);
			} else if (process(assignmentStartDate) > process(assignmentEndDate)) {
				$("#assgnmnt_end_date").css("display", "block");
				setTimeout(function() {
					$("#assgnmnt_end_date").fadeOut(500);
				}, 1000);
			} else {

				saveAssignments();
			}
		} else {
			if ($("#employeeCode").val() == '') {
				$("#assgnmnt_emp_slct").css("display", "block");
				setTimeout(function() {
					$("#assgnmnt_emp_slct").fadeOut(500);
				}, 1000);
			} else if ($("#assgnmt_add_prjct").val() == '') {
				$("#assgnmnt_prjct_slct").css("display", "block");
				setTimeout(function() {
					$("#assgnmnt_prjct_slct").fadeOut(500);
				}, 1000);
			} else if ($("#assgmnt_add_name").val() == '')
				$("#assgmnt_add_name").focus();
			else if ($("#assgmnt_add_strtdt").val() == '')
				$("#assgmnt_add_strtdt").focus();
			else if ($("#assgmnt_add_enddt").val() == '')
				$("#assgmnt_add_enddt").focus();
			else if ($("#assgmnt_add_prio").val() == '')
				$("#assgmnt_add_prio").focus();
		}
	} else {
		if (process(currentDate) > process(assignmentEndDate)) {
			$("#assgnmnt_end_date").css("display", "block");
			setTimeout(function() {
				$("#assgnmnt_end_date").fadeOut(500);
			}, 1000);
		} else if (process(assignmentStartDate) > process(assignmentEndDate)) {
			$("#assgnmnt_end_date").css("display", "block");
			setTimeout(function() {
				$("#assgnmnt_end_date").fadeOut(500);
			}, 1000);
		} else {
			saveAssignments();
		}
	}
}

/** ************************************************************************************ */
/* EMPLOYEE ACHIEVEMENTS */
/**
 * validates form
 */

$("#submt_achiev_info_btn").click(
		function() {
			var date = new Date();
			var currentDate = convertDate(date);

			var projectStartDate = $("#add_project_startdate").val();
			var projectEndDate = $("#add_project_enddate").val();

			if ($("#add_project_title").val() != ''
					&& $("#add_project_client") != ''
					&& $("#add_project_startdate").val() != ''
					&& $("#add_project_enddate").val() != ''
					&& $("#add_project_employees").val() != ''
					&& $("#add_project_description").val() != '') {
				if (projectEndDate < projectStartDate) {
					// alert("Please choose a valid end date");
					$("#prjct_end_date_err").css("display", "block");

					$("#add_project_enddate").focus();
				} else if (projectEndDate < currentDate) {
					// alert("Please choose a valid end date");
					$("#prjct_end_date_err").css("display", "block");
					$("#add_project_enddate").focus();
				} else {
					saveProjectInfo();
				}
				setTimeout(function() {
					$('#prjct_end_date_err').fadeOut(500);
				}, 3000);
			} else {
				if ($("#add_project_title").val() == '')
					$("#add_project_title").focus();
				else if ($("#add_project_client").val() == '')
					$("#add_project_client").focus();
				else if ($("#add_project_startdate").val() == '')
					$("#add_project_startdate").focus();
				else if ($("#add_project_enddate").val() == '')
					$("#add_project_enddate").focus();
				else if ($("#add_project_employees").val() == '')
					$("#add_project_employees").focus();
				else if ($("#add_project_description").val() == '')
					$("#add_project_description").focus();

			}

		});

/** ************************************************************************************************************************ */
/** ******VALIDATION FOR JOB POST********************** */

function validateAgesField() {

	var age = $("#add_post_ageStart").val();
	var age1 = $("#add_post_ageEnd").val();
	if (age < 18 || age > 100) {
		$("#add_post_ageStart").focus();
	} else if (age1 < 18 || age1 > 100) {
		$("#add_post_ageEnd").focus();
	} else if (age1 < age) {
		$("#add_post_ageEnd").focus();
	} else {
		validateSalaryFields();
	}
}

function validateSalaryFields() {

	var sal1 = $("#add_post_salaryStrt").val();
	var sal2 = $("#add_post_salaryEnd").val();

	if (sal2 < sal1) {
		$("#add_post_salaryEnd").focus();
	} else
		savePostInfo();
}

function validatePositions() {
	var pos = $("#add_post_positions").val();
	if (pos == 0 || pos > 1000) {
		$("#add_post_positions").focus();
	} else {
		validateAgesField();
	}
}

function validatePostBasicInfo() {

	var date = new Date();
	var currentDate = convertDate(date);
	var endDate = $("#add_post_endDate").val();

	if ($("#add_post_dep").val() == '')
		$("#add_post_dep").focus();
	else if ($("#add_post_title").val() == '')
		$("#add_post_title").focus();
	else if ($("#add_post_jobTypes").val() == '')
		$("#add_post_jobTypes").focus();
	else if ($("#add_post_positions").val() == '')
		$("#add_post_positions").focus();
	else if ($("#add_post_ageStart").val() == '')
		$("#add_post_ageStart").focus();
	else if ($("#add_post_ageEnd").val() == '')
		$("#add_post_ageEnd").focus();
	else if ($("#add_post_location").val() == null)
		alert("Select Branches")
	else if ($("#add_post_salaryStrt").val() == '')
		$("#add_post_salaryStrt").focus();
	else if ($("#add_post_salaryEnd").val() == '')
		$("#add_post_salaryEnd").focus();
	else if ($("#add_post_endDate").val() == '')
		$("#add_post_endDate").focus();
	else if (process(currentDate) > process(endDate)) {
		$("#add_post_endDate").focus();
	} else {
		validatePositions();
	}
}
/**
 * Validate job post qualification
 */
function validatePostQual() {
	if ($("#add_post_qualification").val() == '')
		$("#add_post_qualification").focus();
	else
		sbmtPostQual();
}
/**
 * Validate post experience
 */
function validatePostExp() {
	if ($("#add_post_years").val() == '')
		$("#add_post_years").focus();
	else if ($("#add_post_months").val() == '')
		$("#add_post_months").focus();
	else {
		var year1 = $("#add_post_years").val();
		var year2 = $("#add_post_months").val();
		if (year1 > year2) {
			$("#add_post_months").focus();
		} else
			submtPostExp();
	}
}
/**
 * Validate job Post Description
 */
function validatePostDesc() {
	if ($("#add_post_description").val() == '')
		$("#add_post_description").focus();
	else
		sbmtPostDesc();
}
/**
 * Validate additional info in post
 */
function validatePostNotes() {
	if ($("#add_post_additionalInfo").val() == '')
		$("#add_post_additionalInfo").focus();
	else
		sbmtPostAddInfo();
}
/** *************************************************************************************************************************************************************** */
/**
 * Validate Job Candidates Info
 */
function validateCandInfo() {
	if ($("#cand_job_field").val() == '')
		$("#cand_job_field").focus();
	else if ($("#cand_fname").val() == '')
		$("#cand_fname").focus();
	else if ($("#cand_lname").val() == '')
		$("#cand_lname").focus();
	else if ($("#cand_dob").val() == '')
		$("#cand_dob").focus();
	else if ($("form input[type='radio']:checked").val() == null)
		alert("Please Select Gender");
	else if ($("#cand_nation").val() == '')
		$("#cand_nation").focus();
	else
		submitCandBasicInfo();
}

/**
 * Validate Job Candidate Address
 */
function validateCandAddress() {
	/*
	 * if ($("#can_address").val() == '') $("#can_address").focus(); else if
	 * ($("#cand_city").val() == '') $("#cand_city").focus(); else if
	 * ($("#cand_state").val() == '') $("#cand_state").focus(); else if
	 * ($("#cand_zipCode").val() == '') $("#cand_zipCode").focus(); else if
	 * ($("#cand_country").val() == '') $("#cand_country").focus(); else if
	 * ($("#cand_email").val() == '') $("#cand_email").focus(); else if
	 * ($("#cand_phone").val() == '') $("#cand_phone").focus(); else if
	 * ($("#cand_mobile").val() == '') $("#cand_mobile").focus(); else
	 * submitCandContact();
	 */
	if (ValidateEmail($("#cand_email").val()) == false)
		$("#cand_email").focus();
	else {
		submitCandContact();
	}
}

/**
 * validate Candidate Interests
 * 
 */
function validateCandInt() {
	if ($("#cand_interests").val() == '')
		$("#cand_interests").focus();
	else
		submitCandInterest();
}
/**
 * Validate Candidate Additional Info
 */
function validateCandAddInfo() {
	if ($("#cand_addInfo").val() == '')
		$("#cand_addInfo").focus();
	else
		submitCandAddInfo();
}
/**
 * Validate Candidate Achievement
 */
function validateCandAchieve() {
	if ($("#cand_achievements").val() == '')
		$("#cand_achievements").focus();
	else
		submitCandAchievements();
}
/**
 * Validate Candidate Qualification
 */
function validateCandQual() {
	/*
	 * if ($("#cand_qual").val() == '') $("#cand_qual").focus(); else if
	 * ($("#cand_subject").val() == '') $("#cand_subject").focus(); else if
	 * ($("#cand_institute").val() == '') $("#cand_institute").focus(); else if
	 * ($("#cand_grade").val() == '') $("#cand_grade").focus(); else if
	 * ($("#cand_year").val() == '') $("#cand_year").focus(); else
	 * submitCandQualification();
	 */
	if ($("#cand_qual").val() == '')
		$("#cand_qual").focus();
	else
		submitCandQualification();
}
/**
 * Validate Candidate Skills
 */
function validateCandSkill() {
	if ($("#cand_skills").val() == '')
		$("#cand_skills").focus();
	else if ($("#cand_skill_level").val() == '')
		$("#cand_skill_level").focus();
	else
		submitCandSkill();
}

/**
 * Validate Candidate Language
 */
function validateCandLanguage() {
	if ($("#cand_lang").val() == '')
		$("#cand_lang").focus();
	else if ($("#cand_speak").val() == '')
		$("#cand_speak").focus();
	else if ($("#cand_read").val() == '')
		$("#cand_read").focus();
	else if ($("#cand_write").val() == '')
		$("#cand_write").focus();
	else
		submtCandLanguages();
}
/**
 * Validate Candidate Work Experience
 */
function validateCandExp() {

	if ($("#cand_orgName").val() == '')
		$("#cand_orgName").focus();
	else if ($("#cand_designation").val() == '')
		$("#cand_designation").focus();
	else if ($("#cand_expField").val() == '')
		$("#cand_expField").focus();
	else
		submtCandExperience();

}
/**
 * Validate Candidate Reference
 */
function validateCandRef() {
	if ($("#cand_refName").val() == '')
		$("#cand_refName").focus();
	else if ($("#cand_refOrg").val() == '')
		$("#cand_refOrg").focus();
	else if ($("#cand_refPhone").val() == '')
		$("#cand_refPhone").focus();
	else
		saveCandidateReference();
}
/** ****************************************************************************************************************************************** */
/**
 * Job Interview Validation
 */
function validateIntBasicInfo() {
	var date = new Date();
	var currentDate = convertDate(date);
	var interviewDate = $("#add_int_Date").val();
	if (process(currentDate) > process(interviewDate)) {
		$("#add_int_Date").focus();
	} else if ($("#add_int_posts").val() == null)
		$("#add_int_posts").focus();
	else if ($("#add_int_Date").val() == '')
		$("#add_int_Date").focus();
	else if ($("#add_int_Time").val() == '')
		$("#add_int_Time").focus();
	else if ($("#add_int_interviewers").val() == null) {
		alert("Please Select Interviewers");
		$("#add_int_interviewers").focus();
	} else if ($("#add_int_sts").val() == null) {
		alert("Please select Status")
		$("#add_int_sts").focus();
	} else if ($("#add_int_nos").val() == '')
		$("#add_int_nos").focus();
	else if ($("#add_int_place").val() == '')
		$("#add_int_place").focus();
	else
		saveInitialInterviewInfo();
}

/*******************************************************************************
 * MEETING VALIDATION
 */
function validateMeeting() {
	var date = new Date();
	var currentDate = convertDate(date);
	var d = $("#drop_three option:selected").length;
	if ($("#meeting_add_title").val() != ''
			&& $("#meeting_add_strdt").val() != ''
			&& $("#meeting_add_enddt").val() != ''
			&& $("#meeting_add_time").val() != ''
			&& $("#meeting_add_venue").val() != '' && d > 0
			&& $("#meeting_add_status").val() != '') {
		// date comparison
		if (process($("#meeting_add_strdt").val()) > process($(
				"#meeting_add_enddt").val())) {
			$("#meeting_add_enddt").focus();
			$("#mtng_date_err").css("display", "block");
			setTimeout(function() {
				$("#mtng_date_err").fadeOut(500);
			}, 2000);
		} else if (process(currentDate) > process($("#meeting_add_enddt").val())) {
			$("#meeting_add_enddt").focus();
			$("#mtng_date_err").css("display", "block");
			setTimeout(function() {
				$("#mtng_date_err").fadeOut(500);
			}, 2000);
		} else {
			$("#meetingAddSubmtBtn").attr("type", "submit");
		}
	} else {
		if ($("#meeting_add_title").val() == '')
			$("#meeting_add_title").focus();
		if ($("#meeting_add_strdt").val() == '')
			$("#meeting_add_strdt").focus();
		if ($("#meeting_add_enddt").val() == '')
			$("#meeting_add_enddt").focus();
		if ($("#meeting_add_time").val() == '')
			$("#meeting_add_time").focus();
		if ($("#meeting_add_venue").val() == '')
			$("#meeting_add_venue").focus();
		if (d == 0) {
			$("#mtng_emp_err").css("display", "block");
			setTimeout(function() {
				$("#mtng_emp_err").fadeOut(500);
			}, 2000);
		}
		if ($("#meeting_add_status").val() == '') {
			$("#mtng_status_err").css("display", "block");
			setTimeout(function() {
				$("#mtng_status_err").fadeOut(500);
			}, 2000);
		}
	}
}

/*******************************************************************************
 * ATTENDANCE VALIDATION
 */
function validateAttendance() {
	// $("#attndnce_add_btn").attr("type","submit");
	// alert($("#emp_join_emp").val());
	if ($("#emp_join_emp").val() != '' && $("#attndnce_date").val() != '') {
		$("#attndnce_add_btn").attr("type", "submit");
	} else {
		if ($("#emp_join_emp").val() == '') {
			$("#attndnce_emp_err").css("display", "block");
			setTimeout(function() {
				$("#attndnce_emp_err").fadeOut(500);
			}, 2000)
		} else if ($("#attndnce_date").val() == '')
			$("#attndnce_date").focus();
	}
}

/**
 * validate interview additional Info
 */
function validateIntAddInfo() {
	if ($("#add_int_add_info").val() == '')
		$("#add_int_add_info").focus();
	else {
		saveInterviewAddInfo();
	}
}
/**
 * Validate interview description
 */
function validateIntDescription() {
	if ($("#add_int_desc").val() == '')
		$("#add_int_desc").focus();
	else
		saveInterviewDesc();
}
/**
 * Validate worksheet info
 */
function validateSheetInfo() {
	if ($("#emp_join_brnch").val() == '')
		$("#emp_join_brnch").focus();
	else if ($("#emp_join_dprmnt").val() == '')
		$("#emp_join_dprmnt").focus();
	else if ($("#emp_join_emp").val() == '')
		$("#emp_join_emp").focus();
	else if ($("#work_sheet_date").val() == '')
		$("#work_sheet_date").focus();
	else
		sbmtWorkSheetInfo();
}
/**
 * Validate worksheet project-task
 */
function validateSheetProject() {
	if ($("#work_sheet_start").val() == '')
		$("#work_sheet_start").focus();
	else if ($("#work_sheet_end").val() == '')
		$("#work_sheet_end").focus();
	else if ($("#work_sheet_task").val() == '')
		$("#work_sheet_task").focus();
	else
		submtWorkSheetProject();

}
/**
 * Validate worksheet description
 */
function validateSheetDescription() {
	if ($("#work_sheet_desc").val() == '')
		$("#work_sheet_desc").focus();
	else
		submtWorkSheetDesc();
}
/**
 * Validate work sheet additional info
 */
function validateSheetAddInfo() {
	if ($("#work_sheet_addInfo").val() == '')
		$("#work_sheet_addInfo").focus();
	else
		submtWorkSheetAddInfo();
}

/**
 * 
 * function to validate payslip
 */
function validatePayslip() {
	var date = new Date();
	var currentDate = convertDate(date);
	if ($("#payslip_start_date").val() != ""
			&& $("#payslip_end_date").val() != ""
			&& $("#salary_daye").val() != ""
			&& $("#payslip_total_amount").val() != '') {
		if (process($("#payslip_start_date").val()) > process($(
				"#payslip_end_date").val())) {
			$("#payslip_start_date_error").css("display", "block");
			setTimeout(function() {
				$("#payslip_start_date_error").fadeOut(500);
			}, 1000);
		} else if (process(currentDate) > process($("#payslip_end_date").val())) {
			$("#payslip_end_date_error").css("display", "block");
			setTimeout(function() {
				$("#payslip_start_date_error").fadeOut(500);
			}, 1000);
		} else {
			savePayslip();
		}
	} else {
		if ($("#payslip_start_date").val() == "")
			$("#payslip_start_date").focus();
		else if ($("#payslip_start_date").val() == "")
			$("#payslip_start_date").focus();
		else if ($("#salary_daye").val() == "")
			$("#salary_daye").focus();
		else
			alert("Select Salary..");
	}

}
