/**
 * @author Shamsheer & Vinutha
 * @since 12/August/2015
 */
$(document).ready(function() {
	$(".from-revalid").change(function() {
		$('#curncy-frm').formValidation('revalidateField', 'curncyDate');
		$('#curncy-frm').formValidation('revalidateField', 'rate');
		/*
		 * $('#company-form').formValidation('revalidateField', 'periodStarts');
		 * $('#company-form').formValidation('revalidateField', 'currency');
		 * $('#company-form').formValidation('revalidateField', 'periodEnds');
		 */
	});
	$(".cmp_fr").change(function() {
		$('#company-form').formValidation('revalidateField', 'periodStart');
		$('#company-form').formValidation('revalidateField', 'periodEnd');
	});
	$(".ledgDate").change(function() {
		$('#stock_summary_frm').formValidation('revalidateField', 'startDate');
		$('#stock_summary_frm').formValidation('revalidateField', 'endDate');
	});
	/**
	 * currency form validation
	 */
	$("#curncy-frm").formValidation({
		framework : 'bootstrap',
		excluded : ':disabled',
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			rate : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Rate is required'
					}
				}
			},
			curncyDate : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid',
						format : 'DD/MM/YYYY',
						min : 'today'
					}
				}
			},
			baseCurrency : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Currency is required'
					}
				}
			}
		}
	});

	/**
	 * company form validation
	 */
	$("#company-form").formValidation({
		framework : 'bootstrap',
		excluded : ':disabled',
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			parentId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Parent is required'
					}
				}
			},
			periodStart : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid',
						format : 'DD/MM/YYYY',
						max : 'periodEnd'
					}
				}
			},
			email : {
				row : '.col-sm-9',
				validators : {
					regexp : {
						regexp : '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
						message : 'The value is not a valid email address'
					}
				}
			},
			periodEnd : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Date is required'
					},
					date : {
						message : 'The  date is not valid',
						format : 'DD/MM/YYYY',
						min : 'periodStart'
					}
				}
			},
			companyName : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			},
			currencyId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Currency is required'
					}
				}
			}
		}
	});

	/**
	 * Tax form validation - erp settings
	 */
	$("#tax_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			tax_name : {
				row : '.col-sm-4',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			},
			tax_rate : {
				row : '.col-sm-4',
				validators : {
					notEmpty : {
						message : 'Rate is required'
					}
				}
			}
		}
	});

	$("#chrg_frrm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			charge_name : {
				row : '.col-sm-4',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			}
		}
	});
	/**
	 * payment method form validation
	 */
	$("#pymnt_mthd_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			name : {
				row : '.col-sm-4',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			}
		}
	});
	$("#stock_summary_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			startDate : {
				row : '.col-sm-4',
				validators : {
					notEmpty : {
						message : 'Start Date is required'
					},
					date : {
						message : 'The  date is not valid',
						format : 'DD/MM/YYYY',
						max : 'endDate'
					}
				}
			},
			endDate : {
				row : '.col-sm-4',
				validators : {
					notEmpty : {
						message : 'End Date is required'
					},
					date : {
						message : 'The  date is not valid',
						format : 'DD/MM/YYYY',
						min : 'startDate'
					}
				}
			}
		}
	});
	
	$("#dlvry_term_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			method : {
				row : '.col-sm-4',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			}
		}
	});

});