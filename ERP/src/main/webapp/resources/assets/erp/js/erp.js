/**
 * js file for erp
 * 
 * @since 30-July-2015
 * @author Shamsheer & Vinutha
 */

setTimeout(function() {
	$(".notification-panl").fadeOut(600);
}, 3500);
$("#close-msg").click(function() {
	$(".notification-panl").fadeOut(600);
});

/**
 * input numbers only
 */
$('.numbersonly').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++)
		a.push(i);

	if (!(a.indexOf(k) >= 0))
		e.preventDefault();

});

$('.numbersonly').bind("paste", function(e) {
	e.preventDefault();
});

$('.paste').bind("paste", function(e) {
	e.preventDefault();
});

/*******************************************************************************
 * only alphabets
 */
$(".alphabets").on(
		"keypress",
		function(event) {
			var englishAlphabetAndWhiteSpace = /[A-Za-z ]/g;
			var key = String.fromCharCode(event.which);
			if (event.keyCode == 8 || event.keyCode == 37
					|| event.keyCode == 39
					|| englishAlphabetAndWhiteSpace.test(key)) {
				return true;
			}
			return false;
		});

$('.alphabets').on("paste", function(e) {
	e.preventDefault();
});

/**
 * double
 */
$('.double').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++) {
		a.push(i);
	}
	if (i = 46) {
		if ($(this).val().indexOf('.') == -1)
			a.push(i);
	}
	if (!(a.indexOf(k) >= 0))
		e.preventDefault();
	// alert($(this).val().indexOf('.'));
});
$('.double').bind("paste", function(e) {
	e.preventDefault();
});

/*******************************************************************************
 * validate email
 */
function ValidateEmail(email) {
	var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	return expr.test(email);
};

$("#changpwd_sbmt")
		.click(
				function() {
					var oldpassword = $("#old_pwd_changpwd").val();
					var newpassword = $("#new_pwd_changpwd").val();
					var confirmpassword = $("#confirm_pwd").val();
					if (oldpassword != '' && newpassword != ''
							&& confirmpassword != '') {
						if (newpassword == confirmpassword) {
							var vo = new Object();
							vo.userName = $("#userName_changpwd").val();
							vo.oldPassword = oldpassword;
							vo.newPassword = newpassword;
							$
									.ajax({
										type : "POST",
										url : 'changePassword',
										dataType : "json",
										contentType : "application/json; charset=utf-8",
										data : JSON.stringify(vo),
										cache : false,
										async : true,
										success : function(response) {
											if (response.status == "Invalid Password") {
												$("#old_pwd_err").css(
														"display", "block");
												$("#pwd_err").html("");
												$("#pwd_err").append(
														response.status);
												$("#pwd_err").css("display",
														"block");
											} else if (response.status == "ok") {
												$("#new_pwd_ok").css("display",
														"block");
												$("#old_pwd_ok").css("display",
														"block");
												$("#pwd_success").css(
														"display", "block");
												$(".modal-box, .modal-overlay")
														.fadeOut(
																2000,
																function() {
																	$(
																			".modal-overlay")
																			.remove();
																});
											}
										},
										error : function(requestObject, error,
												errorThrown) {
											alert(errorThrown);
										}
									});
						} else {
							$("#pwd_err").html("");
							$("#pwd_err").append("Password Doesn't Match..");
							$("#pwd_err").css("display", "block");
							$("#new_pwd_err").css("display", "block");
						}
					}
				});

$("#confirm_pwd").click(function() {
	$("#new_pwd_err").css("display", "none");
	$("#old_pwd_err").css("display", "none");
	$("#new_pwd_ok").css("display", "none");
	$("#old_pwd_ok").css("display", "none");
	$("#pwd_success").css("display", "none");
	$("#pwd_err").css("display", "none");
});

$("#old_pwd_changpwd").click(function() {
	$("#new_pwd_err").css("display", "none");
	$("#old_pwd_err").css("display", "none");
	$("#new_pwd_ok").css("display", "none");
	$("#old_pwd_ok").css("display", "none");
	$("#pwd_success").css("display", "none");
	$("#pwd_err").css("display", "none");
});

$("#new_pwd_changpwd").click(function() {
	$("#new_pwd_err").css("display", "none");
	$("#old_pwd_err").css("display", "none");
	$("#new_pwd_ok").css("display", "none");
	$("#old_pwd_ok").css("display", "none");
	$("#pwd_success").css("display", "none");
	$("#pwd_err").css("display", "none");
});

$("#chng_pwd_close_btn").click(function() {
	$("#new_pwd_err").css("display", "none");
	$("#old_pwd_err").css("display", "none");
	$("#new_pwd_ok").css("display", "none");
	$("#old_pwd_ok").css("display", "none");
	$("#pwd_success").css("display", "none");
	$("#pwd_err").css("display", "none");
	$(".modal-box, .modal-overlay").fadeOut(500, function() {
		$(".modal-overlay").remove();
	});
});

/**
 * function to get selected currency details
 */
getThisCurrencyDetails = function() {
	if ($("#base-currency-list").val().length > 0) {
		$.ajax({
			type : "GET",
			url : "getThisCurrencyDetails.do",
			cache : false,
			async : true,
			data : {
				"baseCurrencyId" : $("#base-currency-list").val()
			},
			success : function(response) {
				var vo = response.result;
				$("#exchnge-rte").val(vo.rate);
				$("#currency-wef").val(vo.date);
				$("#currency-wef-hidn").val(vo.date);
				$('#curncy-frm')
						.formValidation('revalidateField', 'curncyDate');
				$('#curncy-frm').formValidation('revalidateField', 'rate');
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}
/**
 * function to save currency
 */
function saveCurrency() {
	$('form#curncy-frm').submit(
			function(event) {
				if ($("#exchnge-rte").val().length > 0
						&& $("#base-currency-list").val().length > 0) {
					var vo = new Object();
					vo.baseCurrencyId = $("#base-currency-list").val();
					vo.date = $("#currency-wef").val();
					vo.rate = $("#exchnge-rte").val();
					$("#base-currency-list").val("");
					$("#exchnge-rte").val("");
					$("#currency-wef").val("");
					$("#currency-wef-hidn").val("");
					$.ajax({
						type : "POST",
						url : 'saveCurrency.do',
						dataType : "json",
						contentType : "application/json; charset=utf-8",
						data : JSON.stringify(vo),
						cache : false,
						async : true,
						success : function(response) {
							$(".notification-panl").css("display", "block");
							$(".notfctn-cntnnt").text(
									"Currency saved successfully.");
							setTimeout(function() {
								$(".notification-panl").fadeOut(600);
							}, 3500);
						},
						error : function(requestObject, error, errorThrown) {
							alert(errorThrown);
						}
					});
					event.preventDefault();
				}
			});
}

/**
 * function to save company info
 */
function saveCompany() {
	$("form#company-form").submit(
			function(event) {
				if ($("#company_name").val().length > 0
						&& $("#parent_company").val().length > 0
						&& $("#company-period-start").val().length > 0
						&& $("#company-period-end").val().length > 0
						&& $("#currencyOptions").val().length > 0) {
					$("#load-erp").css("display", "block");
					var vo = new Object();
					vo.companyId = $("#companyId").val();
					vo.parentId = $("#parent_company").val();
					vo.currencyId = $("#currencyOptions").val();
					vo.companyName = $("#company_name").val();
					vo.address = $("#company-address").val();
					vo.accountName = $("#company-acc-name").val();
					vo.accountNumber = $("#company-acc-nnum").val();
					vo.bank = $("#company-bank").val();
					vo.bankBranch = $("#company-bank-brnch").val();
					vo.phone = $("#company-phone").val();
					vo.email = $("#company-email").val();
					vo.periodStart = $("#company-period-start").val();
					vo.periodEnd = $("#company-period-end").val();
					vo.salesTaxNumber = $("#company-sls-tx").val();
					$("#company_name").val('');
					$("#parent_company").val('');
					$("#company-period-start").val('');
					$("#company-period-end").val('');
					$("#currencyOptions").val('');
					clearCompanyForm();
					$.ajax({
						type : "POST",
						url : 'saveCompany.do',
						dataType : "html",
						contentType : "application/json; charset=utf-8",
						data : JSON.stringify(vo),
						cache : false,
						async : true,
						success : function(response) {
							$(".notification-panl").css("display", "block");
							$(".notfctn-cntnnt").text(
									"Company saved successfully.");
							setTimeout(function() {
								$(".notification-panl").fadeOut(600);
							}, 3500);
							$("#panelBodys").html(response);
							$("#load-erp").css("display", "none");
							location.reload();
						},
						error : function(requestObject, error, errorThrown) {
							alert(errorThrown);
							$("#load-erp").css("display", "none");
						}
					});
					event.preventDefault();
				}
			});

	/*
	 * $("html, body").animate({ scrollTop : 0 }, 500);
	 */
}

/**
 * method to find active company
 */
function getActiveCompany() {
	if ($("#parent_company").val().length > 0) {
		$.ajax({
			type : "GET",
			url : "getActiveCompany.do",
			cache : false,
			async : true,
			data : {
				"parentId" : $("#parent_company").val()
			},
			success : function(response) {
				var vo = response.result;
				$("#companyId").val(vo.companyId);
				$("#currencyOptions").val(vo.currencyId).trigger(
						"chosen:updated");
				$("#company_name").val(vo.companyName);
				$("#company-address").val(vo.address);
				$("#company-acc-name").val(vo.accountName);
				$("#company-acc-nnum").val(vo.accountNumber);
				$("#company-bank").val(vo.bank);
				$("#company-bank-brnch").val(vo.bankBranch);
				$("#company-phone").val(vo.phone);
				$("#company-email").val(vo.email);
				$("#company-period-start").val(vo.periodStart);
				$("#company-period-end").val(vo.periodEnd);
				$("#company-sls-tx").val(vo.salesTaxNumber);
				if (vo.companyId != null)
					$("#delet-company-ico").css("display", "block");
				$('#company-form').formValidation('revalidateField', 'name');
				$('#company-form').formValidation('revalidateField', 'parent');
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

/**
 * function to clear company form
 */
clearCompanyForm = function() {
	$("#parent_company").val('');
	$("#companyId").val('');
	$("#currencyOptions").val("").trigger("chosen:updated");
	$("#company_name").val('');
	$("#company-address").val('');
	$("#company-acc-name").val('');
	$("#company-acc-nnum").val('');
	$("#company-bank").val('');
	$("#company-bank-brnch").val('');
	$("#company-phone").val('');
	$("#company-email").val('');
	$("#company-period-start").val('');
	$("#company-period-end").val('');
	$("#company-sls-tx").val('');

	$('#company-form').formValidation('revalidateField', 'parent');
	$('#company-form').formValidation('revalidateField', 'name');
}

/**
 * function to delete company
 */
function deleteThisCompany() {
	var id = $("#companyId").val();
	if (id != '') {
		$.ajax({
			type : "GET",
			url : "deleteCompany.do",
			cache : false,
			async : true,
			data : {
				"id" : id
			},
			success : function(response) {
				$(".notification-panl").css("display", "block");
				$(".notfctn-cntnnt").text(response.status);
				setTimeout(function() {
					$(".notification-panl").fadeOut(600);
				}, 3500);
				// listCompanies();
				location.reload();
				// clearCompanyForm();
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

function listCompanies() {
	$.ajax({
		type : "GET",
		url : 'listAllCompanies.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#panelBodys").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function confirmDelete(item) {
	$("#delete-item").val(item);
	$("#confirm-delete").css("display", "block");
}

function cancel() {
	$("#confirm-delete").css("display", "none");
	$("#delete-item").val('');
	$("#id-hid").val('');
}

function deleteItem() {
	var item = $("#delete-item").val();
	$("#confirm-delete").css("display", "none");
	if (item == 'company')
		deleteThisCompany();
	else if (item == 'tax')
		deleteThisVat();
	else if (item == 'charge')
		deleteThisCharge();
	else if (item == 'paymentMethod')
		deleteThisPaymentMethod();
	else if (item == 'paymentTerm')
		deleteThisPaymentTerm();
	else if (item == 'deliveryTerm')
		deleteThisDeliveryTerm();
	$("#delete-item").val('');
}

/**
 * function to update default browser
 */
$('input[type=radio][name=is_default]').change(function() {
	$.ajax({
		type : "GET",
		url : 'updateDefaultCompany.do',
		cache : false,
		data : {
			"companyId" : this.value
		},
		async : true,
		success : function(response) {
			location.reload();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
});

function saveTax() {
	$('form#tax_frm').submit(
			function(event) {
				if ($("#tax_name").val().length > 0
						&& $("#tax_rate").val().length > 0) {
					$("#load-erp").css("display", "block");
					var vo = new Object();
					vo.id = $("#tax_id").val();
					vo.taxName = $("#tax_name").val();
					vo.rate = $("#tax_rate").val();
					$("#tax_name").val("");
					$("#tax_rate").val("");
					$.ajax({
						type : "POST",
						url : 'saveTax.do',
						contentType : "application/json; charset=utf-8",
						data : JSON.stringify(vo),
						cache : false,
						async : true,
						success : function(response) {
							$(".notification-panl").css("display", "block");
							$(".notfctn-cntnnt").text(response);
							setTimeout(function() {
								$(".notification-panl").fadeOut(600);
							}, 3500);
							listTaxes();

						},
						error : function(requestObject, error, errorThrown) {
							alert(errorThrown);
							$("#load-erp").css("display", "none");
						}
					});
				}
				event.preventDefault();
			});
}

function listTaxes() {
	$.ajax({
		type : "GET",
		url : 'listTaxes.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#tax_tbl").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function editTaxValues(id, name, rate) {
	$("#tax_id").val(id);
	$("#tax_name").val(name);
	$("#tax_rate").val(rate)
}

function deleteTax(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('tax');
	$("#id-hid").val(id);
}

function deleteThisVat() {

	var id = $("#id-hid").val();
	if (id != '') {
		$.ajax({
			type : "GET",
			url : "deleteTax.do",
			cache : false,
			async : true,
			data : {
				"id" : id
			},
			success : function(response) {
				$(".notification-panl").css("display", "block");
				$(".notfctn-cntnnt").text(response);
				setTimeout(function() {
					$(".notification-panl").fadeOut(600);
				}, 3500);
				listTaxes();
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}

}

/**
 * function to edit charge
 * 
 * @param id
 * @param name
 */
function editChargeValues(id, name) {
	$("#charge_id").val(id);
	$("#chrg_name").val(name);
}

/**
 * function to delete charge
 */
function deleteCharge(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('charge');
	$("#id-hid").val(id);
}

/**
 * function to delete charge from server
 */
function deleteThisCharge() {

	var id = $("#id-hid").val();
	if (id != '') {
		$.ajax({
			type : "GET",
			url : "deleteCharge.do",
			cache : false,
			async : true,
			data : {
				"id" : id
			},
			success : function(response) {
				$(".notification-panl").css("display", "block");
				$(".notfctn-cntnnt").text(response);
				setTimeout(function() {
					$(".notification-panl").fadeOut(600);
				}, 3500);
				listCharges();
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			}
		});
	}
}

function listCharges() {
	$.ajax({
		type : "GET",
		url : 'listCharge.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#chrg_tbl").html(response);
			$("#load-erp").css("display", "none");

		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to save charge
 */
function saveCharge() {
	$('form#chrg_frrm').submit(function(event) {
		if ($("#chrg_name").val().length != 0) {
			var vo = new Object();
			vo.chargeId = $("#charge_id").val();
			vo.name = $("#chrg_name").val();
			$("#chrg_name").val("");
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "POST",
				url : 'saveCharge.do',
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(vo),
				cache : false,
				async : true,
				success : function(response) {
					$(".notification-panl").css("display", "block");
					$(".notfctn-cntnnt").text(response);
					setTimeout(function() {
						$(".notification-panl").fadeOut(600);
					}, 3500);
					listCharges();

				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
					$("#load-erp").css("display", "none");
				}
			});
		}
		event.preventDefault();
	});

}

/**
 * function to clear charge form
 */
function clearChargeFrm() {
	$("#charge_id").val("");
	$("#chrg_name").val("");
}

/**
 * function to save payment method
 */
function savePaymentMethod() {
	$("form#pymnt_mthd_frm").submit(function(event) {
		if ($("#pymnt_mthd_name").val().length > 0) {
			var vo = new Object();
			vo.id = $("#pymnt_mthd_id").val();
			vo.name = $("#pymnt_mthd_name").val();
			$("#pymnt_mthd_name").val("");
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "POST",
				url : 'savePaymentMethod.do',
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(vo),
				cache : false,
				async : true,
				success : function(response) {
					$(".notification-panl").css("display", "block");
					$(".notfctn-cntnnt").text(response.status);
					setTimeout(function() {
						$(".notification-panl").fadeOut(600);
					}, 3500);
					listPaymentMethods();

				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
					$("#load-erp").css("display", "none");
				}
			});
		}
		event.preventDefault();
	});
}

/**
 * function to list payment methods
 */
function listPaymentMethods() {
	$
			.ajax({
				type : "GET",
				url : 'listPaymentMethod.do',
				contentType : "application/json; charset=utf-8",
				cache : false,
				async : true,
				success : function(response) {
					var items = "";
					var options = "<option value='' label='-select payment method-'>";
					$
							.each(
									response.result,
									function(i, item) {
										items += "<label class='badge'><span onclick='editPaymentMethod(\""
												+ item.id
												+ "\",\""
												+ item.name
												+ "\")'>"
												+ item.name
												+ "</span>&nbsp;&nbsp;<span><i onclick='deletePaymentMethod(\""
												+ item.id
												+ "\")' class='fa fa-times'></i></span></label>";
										options += "<option value='" + item.id
												+ "'>" + item.name
												+ "</option>";
									});
					try {
						$(".pymntMthSelect").html(options);
					} catch (e) {
					}
					$("#avl_pymnt_mthds").html(items);
					$("#load-erp").css("display", "none");

				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
}

/**
 * function to delete payment methods
 * 
 * @param id
 */
function deletePaymentMethod(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('paymentMethod');
	$("#id-hid").val(id);
}

/**
 * function to clear payment method form
 */
function clearPaymentMethodFrm() {
	$("#pymnt_mthd_id").val('');
	$("#pymnt_mthd_name").val('');
}

/**
 * function to delete payment methods
 */
function deleteThisPaymentMethod() {
	$.ajax({
		type : "GET",
		url : "deletePaymentMethod.do",
		cache : false,
		async : true,
		data : {
			"id" : $("#id-hid").val()
		},
		success : function(response) {
			$(".notification-panl").css("display", "block");
			$(".notfctn-cntnnt").text(response.status);
			setTimeout(function() {
				$(".notification-panl").fadeOut(600);
			}, 3500);
			listPaymentMethods();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

/**
 * function to edit payment method
 * 
 * @param id
 * @param name
 */
function editPaymentMethod(id, name) {
	$("#pymnt_mthd_id").val(id);
	$("#pymnt_mthd_name").val(name);
}

function displayDetails() {
	$("#details").css("display", "block");
}

function closeDetails() {
	$("#details").css("display", "none");
}

/**
 * function to add or edit payment terms
 * 
 * @param id
 */
function addPaymentTerm(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "addPaymentTerm.do",
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			$("#addPymntTermDiv").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
}

/**
 * function add payment term table row
 */
function addPaymntTermRow() {
	$("#load-erp").css("display", "block");
	$("#pymntTermTableBody tr:last").remove();
	$.ajax({
		type : "GET",
		url : "addPaymentTermRow.do",
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#pymntTermTableBody").append(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
}

/**
 * function to delete payment terms row
 * 
 * @param link
 */
function deletePymentTermsaRow(link) {
	var td = link.parentNode.parentNode;
	var row = td.parentNode;
	// var table = row.parentNode;
	row.removeChild(td);
}

/**
 * function to save payment terms
 */
function savePaymentTerms(size) {
	var flag = false;
	var total = "100";
	var percentage = "0.00";
	var vo = new Object();
	vo.id = $("#pymntTermId").val();
	vo.name = $("#pymntTermName").val();
	// var size = $("#pymntTermTableBody tr").length;
	if (vo.name == "") {
		$("#pymntTermName").focus();
	}
	var installments = [];
	var check = false;
	for (var i = 1; i <= size; i++) {
		if (document.getElementById('percentage' + i)) {
			var installment = new Object();
			installment.id = $("#hidInstllmnts" + i).val();
			percentage = parseFloat(percentage)
					+ parseFloat($("#percentage" + i).val());
			installment.percentage = $("#percentage" + i).val();
			installment.paymentMethodId = $("#pymntMthds" + i).val();
			installment.nofdays = $("#nofDays" + i).val();
			if (installment.paymentMethodId == "") {
				$("#pymntMthds" + i).focus();
			} else if (installment.percentage == "") {
				$("#percentage" + i).focus();
			} else if (installment.nofdays == "") {
				$("#nofDays" + i).focus();
			} else {
				check = true;
			}
			installments.push(installment);
		}
	}
	flag = check;
	if (flag && percentage != total) {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text("Percentage Must Be Balanced to 100");
		setTimeout(function() {
			$("#errror-msg").fadeOut("600");
		}, 1000);
		flag = false;
	}
	vo.installmentVos = installments;
	if (flag) {
		$("#load-erp").css("display", "block");
		$.ajax({
			type : "POST",
			url : 'savePaymentTerms.do',
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(vo),
			cache : false,
			async : true,
			success : function(response) {
				$(".notification-panl").css("display", "block");
				$(".notfctn-cntnnt").text(response);
				setTimeout(function() {
					$(".notification-panl").fadeOut(600);
				}, 3500);
				listPaymentTerms();
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
				$("#load-erp").css("display", "none");
			}
		});
	}
}

/**
 * function to list payment terms
 */
function listPaymentTerms() {
	$.ajax({
		type : "GET",
		url : "paymentTermsList.do",
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#pymntTrmsSection").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
}

/**
 * function to delete payment terms
 * 
 * @param id
 */
function deletePaymentTerms(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('paymentTerm');
	$("#id-hid").val(id);
}

function deleteThisPaymentTerm() {
	$.ajax({
		type : "GET",
		url : "deletePaymentTerms.do",
		cache : false,
		async : true,
		data : {
			"id" : $("#id-hid").val()
		},
		success : function(response) {
			$(".notification-panl").css("display", "block");
			$(".notfctn-cntnnt").text(response);
			setTimeout(function() {
				$(".notification-panl").fadeOut(600);
			}, 3500);
			listPaymentTerms();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}
/**
 * get row count for save payment term
 */
function getRowCount() {
	$.ajax({
		type : "GET",
		url : "getPaymentTermsRowCount.do",
		cache : false,
		async : true,
		success : function(response) {
			savePaymentTerms(response);
		},
		error : function(requestObject, error, errorThrown) {
		}
	});
}

closeError = function() {
	$("#errror-msg").fadeOut("600");
}

/**
 * function to get number property
 * 
 * @param item
 */
function getNumberProperty(item) {
	if (item != 'PRODUCT') {
		$("#ifProduct").css("display", "none");
		if (item != '') {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "GET",
				url : "getNumberProperty.do",
				cache : false,
				async : true,
				data : {
					"property" : item
				},
				success : function(response) {
					$("#numbr_prprty_prefix").val(response.prefix);
					$("#numbr_prprty_numbr").val(response.number);
					$("#numbr_prprty_id").val(response.id);
					$("#numbr_prprty_numbr_hid").val(response.number);
					$("#numbr_prprty_preview").text(response.preview);
					$("#load-erp").css("display", "none");
				},
				error : function(requestObject, error, errorThrown) {
					$("#load-erp").css("display", "none");
				}
			});
		} else {
			$("#numbr_prprty_prefix").val("");
			$("#numbr_prprty_numbr").val("");
			$("#numbr_prprty_id").val("");
			$("#numbr_prprty_numbr_hid").val("");
			$("#numbr_prprty_preview").text("");
		}
	} else {
		$("#ifProduct").css("display", "block");
		$("#numbr_prprty_prefix").val("");
		$("#numbr_prprty_numbr").val("");
		$("#numbr_prprty_id").val("");
		$("#numbr_prprty_numbr_hid").val("");
		$("#numbr_prprty_preview").text("");
	}
}

/**
 * function to show number property preview
 */
function showPreview() {
	if (parseInt($("#numbr_prprty_numbr").val()) >= parseInt($(
			"#numbr_prprty_numbr_hid").val()))
		$("#numbr_prprty_preview").text(
				$("#numbr_prprty_prefix").val()
						+ $("#numbr_prprty_numbr").val());
	else
		$("#numbr_prprty_preview").text(
				$("#numbr_prprty_prefix").val()
						+ $("#numbr_prprty_numbr_hid").val());
}

/**
 * function to save number property
 */
function saveNumberProperty() {
	if ($("#numbr_prprty_id").val() != '') {
		$("#load-erp").css("display", "block");
		var vo = new Object();
		vo.prefix = $("#numbr_prprty_prefix").val();
		vo.id = $("#numbr_prprty_id").val();
		vo.number = $("#numbr_prprty_numbr").val();
		$.ajax({
			type : "POST",
			url : 'saveNumberProperty.do',
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(vo),
			cache : false,
			async : true,
			success : function(response) {
				$(".notification-panl").css("display", "block");
				$(".notfctn-cntnnt").text(response);
				setTimeout(function() {
					$(".notification-panl").fadeOut(600);
				}, 3500);
				$("#load-erp").css("display", "none");
			},
			error : function(requestObject, error, errorThrown) {
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		$("#nmbr_prpty_itms").focus();
		$("#nmbr_prpty_ctgrs").focus();
	}
}
function getNumberPropertyForProduct(item) {
	if (item != '') {
		$("#load-erp").css("display", "block");
		$.ajax({
			type : "GET",
			url : "getNumberProperty.do",
			cache : false,
			async : true,
			data : {
				"property" : item
			},
			success : function(response) {
				$("#numbr_prprty_prefix").val(response.prefix);
				$("#numbr_prprty_numbr").val(response.number);
				$("#numbr_prprty_id").val(response.id);
				$("#numbr_prprty_numbr_hid").val(response.number);
				$("#numbr_prprty_preview").text(response.preview);
				$("#load-erp").css("display", "none");
			},
			error : function(requestObject, error, errorThrown) {
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		$("#numbr_prprty_prefix").val("");
		$("#numbr_prprty_numbr").val("");
		$("#numbr_prprty_id").val("");
		$("#numbr_prprty_numbr_hid").val("");
		$("#numbr_prprty_preview").text("");
	}
}

/**
 * Method to save delivery term
 */
saveDeliveryTerm = function() {
	var deliveryMethodVo = new Object();
	deliveryMethodVo.method = $("#dlvry_term_name").val();
	deliveryMethodVo.deliveryMethodId = $("#dlvry_term_id").val();
	if (deliveryMethodVo.method != "") {
		$("#load-erp").css("display", "block");
		$.ajax({
			type : "POST",
			url : "saveDeliveryterms.do",
			cache : false,
			async : true,
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(deliveryMethodVo),
			success : function(response) {
				$(".notification-panl").css("display", "block");
				$(".notfctn-cntnnt").text(response);
				setTimeout(function() {
					$(".notification-panl").fadeOut(600);
				}, 3500);
				clearDeliveryTerm();
				listDeliveryTerms();
			},
			error : function(requestObject, error, errorThrown) {
				$(".notification-panl").css("display", "block");
				$(".notfctn-cntnnt").text("Something went wrong...");
				setTimeout(function() {
					$(".notification-panl").fadeOut(600);
				}, 3500);
				$("#load-erp").css("display", "none");
			}
		});
	}
}

/**
 * Method to edit delivery term
 * 
 * @param id
 */
editDeliveryTerm = function(id, method) {
	$("#dlvry_term_id").val(id);
	$("#dlvry_term_name").val(method);
}

function listDeliveryTerms() {
	$.ajax({
		type : "GET",
		url : 'listDeliveryTerms.do',
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#deliveryTrmsSection").html(response);
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

deleteDelvryTerm = function(id) {

	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('deliveryTerm');
	$("#id-hid").val(id);
}

deleteThisDeliveryTerm = function() {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : 'deleteDeliveryTerm.do',
		contentType : "application/json; charset=utf-8",
		data : {
			"did" : $("#id-hid").val()
		},
		cache : false,
		async : true,
		success : function(response) {
			if(response != 'Delivery Term Deleted Successfully'){
				response = 'Something went wrong...'
			}
			$(".notification-panl").css("display", "block");
			$(".notfctn-cntnnt").text(response);
			setTimeout(function() {
				$(".notification-panl").fadeOut(600);
			}, 3500);
			listDeliveryTerms();
		},
		error : function(requestObject, error, errorThrown) {
			$(".notification-panl").css("display", "block");
			$(".notfctn-cntnnt").text("Something went wrong...");
			setTimeout(function() {
				$(".notification-panl").fadeOut(600);
			}, 3500);
		}
	});
}
clearDeliveryTerm = function() {
	$("#dlvry_term_id").val("");
	$("#dlvry_term_name").val("");
}