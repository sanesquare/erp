/**
 * 
 * @author Jithin Mohan
 * @since 26-March-2015
 * 
 */

$(document)
		.ready(
				function() {
					$("#datepicker").change(
							function() {
								$('#performanceEvaluationForm').formValidation(
										'revalidateField', 'date');
							});
					/*
					 * $("#datepickers").change( function() {
					 * $('#performanceEvaluationForm').formValidation(
					 * 'revalidateField', 'endDate'); });
					 */
					$(".termination-date").change(
							function() {
								$('#terminationForm').formValidation(
										'revalidateField', 'termiantedDate');
							});
					$(".warning-date").change(
							function() {
								$('#warningForm').formValidation(
										'revalidateField', 'warningOn');
							});
					$(".exit-date").change(
							function() {
								$('#employeExitForm').formValidation(
										'revalidateField', 'exitDate');
							});
					$(".reminderTime").change(
							function() {
								$('#reminderForm').formValidation(
										'revalidateField', 'reminderTime');
							});
					$(".memoDate").change(
							function() {
								$('#memoForm').formValidation(
										'revalidateField', 'memoOn');
							});
					$(".holiday-start-date").change(
							function() {
								$('#holidayForm').formValidation(
										'revalidateField', 'startDate');
							});
					$(".holiday-end-date").change(
							function() {
								$('#holidayForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".loan-date").change(
							function() {
								$('#loanForm').formValidation(
										'revalidateField', 'loanDate');
							});
					$(".loan-repay-date").change(
							function() {
								$('#loanForm')
										.formValidation('revalidateField',
												'repaymentStartDate');
							});
					$(".advance-salary-date").change(
							function() {
								$('#advanceSalaryForm').formValidation(
										'revalidateField', 'requestedDate');
							});
					$(".leave-from-date").change(
							function() {
								$('#newLeaveForm').formValidation(
										'revalidateField', 'fromDate');
								$('#announcementForm').formValidation(
										'revalidateField', 'startDate');
							});
					$(".leave-to-date").change(
							function() {
								$('#newLeaveForm').formValidation(
										'revalidateField', 'toDate');
								$('#transfer_add_form').formValidation(
										'revalidateField', 'date');
								$('#announcementForm').formValidation(
										'revalidateField', 'endDate');
							});

					$(".start-date").change(
							function() {
								$('#travel_add_form').formValidation(
										'revalidateField', 'startDate');
								$('#resignation_add_form').formValidation(
										'revalidateField', 'noticeDate');
							});
					$(".end-date").change(
							function() {
								$('#travel_add_form').formValidation(
										'revalidateField', 'endDate');
								$('#resignation_add_form').formValidation(
										'revalidateField', 'resignationDate');
							});

					$(".deduction-date").change(
							function() {
								$("#deductionAddForm").formValidation(
										'revalidateField', 'date');
								$("#addBonusForm").formValidation(
										'revalidateField', 'date');
							});
					$(".amountt").change(
							function() {
								$('#deductionAddForm').formValidation(
										'revalidateField', 'amount');
							});
					/*
					 * $(".reimbursement-amouint").change( function() {
					 * $('#reimbursementsAddForm').formValidation(
					 * 'revalidateField', 'amount'); });
					 */
					$(".adjustment_date").change(
							function() {
								$('#adjustmetn_add_form').formValidation(
										'revalidateField', 'date');
								$("#commition_add_form").formValidation(
										'revalidateField', 'date');
							});
					$(".sheetDate").change(
							function() {
								$('#add_sheet_form').formValidation(
										'revalidateField', 'date');
							});
					$("#employeeSl").change(
							function() {
								$('#terminationForm').formValidation(
										'revalidateField', 'employee');
								$('#adjustmetn_add_form').formValidation(
										'revalidateField', 'employee');
								$("#commition_add_form").formValidation(
										'revalidateField', 'employee');
								$("#deductionAddForm").formValidation(
										'revalidateField', 'employee');
								$("#dailyWageAdDForm").formValidation(
										'revalidateField', 'employee');
								$("#addBonusForm").formValidation(
										'revalidateField', 'employee');
								$("#hoursWagesForm").formValidation(
										'revalidateField', 'employee');
								$("#reimbursementsAddForm").formValidation(
										'revalidateField', 'employee');
								$("#transfer_add_form").formValidation(
										'revalidateField', 'employee');
								$("#travel_add_form").formValidation(
										'revalidateField', 'employee');
								$("#resignation_add_form").formValidation(
										'revalidateField', 'employee');
								$('#timEmWorkForm').formValidation(
										'revalidateField', 'employee');
								$('#empSummaryForm').formValidation(
										'revalidateField', 'employee');
							});
					$("#forwaders").change(
							function() {
								$('#terminationForm').formValidation(
										'revalidateField', 'forwaders');
							});
					$("#datepicker").change(
							function() {
								$('#performanceEvaluationForm').formValidation(
										'revalidateField', 'date');
							});
					/*
					 * $("#datepickers").change( function() {
					 * $('#performanceEvaluationForm').formValidation(
					 * 'revalidateField', 'endDate'); });
					 */
					$(".termination-date").change(
							function() {
								$('#terminationForm').formValidation(
										'revalidateField', 'termiantedDate');
							});
					$(".warning-date").change(
							function() {
								$('#warningForm').formValidation(
										'revalidateField', 'warningOn');
							});
					$(".exit-date").change(
							function() {
								$('#employeExitForm').formValidation(
										'revalidateField', 'exitDate');
							});
					$(".reminderTime").change(
							function() {
								$('#reminderForm').formValidation(
										'revalidateField', 'reminderTime');
							});
					$(".memoDate").change(
							function() {
								$('#memoForm').formValidation(
										'revalidateField', 'memoOn');
							});
					$(".holiday-start-date").change(
							function() {
								$('#holidayForm').formValidation(
										'revalidateField', 'startDate');
							});
					$(".holiday-end-date").change(
							function() {
								$('#holidayForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".loan-date").change(
							function() {
								$('#loanForm').formValidation(
										'revalidateField', 'loanDate');
							});
					$(".loan-repay-date").change(
							function() {
								$('#loanForm')
										.formValidation('revalidateField',
												'repaymentStartDate');
							});
					$(".advance-salary-date").change(
							function() {
								$('#advanceSalaryForm').formValidation(
										'revalidateField', 'requestedDate');
							});
					$(".overtime-req-date").change(
							function() {
								$('#overtimeForm').formValidation(
										'revalidateField', 'requestedDate');
							});
					$(".overtime-time-in").change(
							function() {
								$('#overtimeForm').formValidation(
										'revalidateField', 'timeIn');
							});
					$(".overtime-time-out").change(
							function() {
								$('#overtimeForm').formValidation(
										'revalidateField', 'timeOut');
							});
					$("#employeeSl").change(
							function() {
								$('#terminationForm').formValidation(
										'revalidateField', 'employee');
							});
					$("#forwaders").change(
							function() {
								$('#terminationForm').formValidation(
										'revalidateField', 'forwaders');
							});
					$(".holDate").change(
							function() {
								$('#hrHoliForm').formValidation(
										'revalidateField', 'startDate');
								$('#hrHoliForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".intDate").change(
							function() {
								$('#recInterForm').formValidation(
										'revalidateField', 'startDate');
								$('#recInterForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".hourDate").change(
							function() {
								$('#payHourForm').formValidation(
										'revalidateField', 'startDate');
								$('#payHourForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".dayDate").change(
							function() {
								$('#payDayForm').formValidation(
										'revalidateField', 'startDate');
								$('#payDayForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".emJoinDate").change(
							function() {
								$('#empJoinForm').formValidation(
										'revalidateField', 'startDate');
								$('#empJoinForm').formValidation(
										'revalidateField', 'endDate');
							});

					$(".emTranDate").change(
							function() {
								$('#empTranForm').formValidation(
										'revalidateField', 'startDate');
								$('#empTraForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".emResDate").change(
							function() {
								$('#empResForm').formValidation(
										'revalidateField', 'startDate');
								$('#empResForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".emTravDate").change(
							function() {
								$('#empTravForm').formValidation(
										'revalidateField', 'startDate');
								$('#empTravForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".emAssinDate").change(
							function() {
								$('#empAssinForm').formValidation(
										'revalidateField', 'startDate');
								$('#empAssinForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".emPerDate").change(
							function() {
								$('#empPerformanceForm').formValidation(
										'revalidateField', 'startDate');
								$('#empPerformanceForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".timWorkDate").change(
							function() {
								$('#timWorkForm').formValidation(
										'revalidateField', 'startDate');
								$('#timWorkForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".timEmWorkDate").change(
							function() {
								$('#timEmWorkForm').formValidation(
										'revalidateField', 'startDate');
								$('#timEmWorkForm').formValidation(
										'revalidateField', 'endDate');
							});
					$("#employeeSl2").change(
							function() {
								$('#timeSheetForm').formValidation(
										'revalidateField', 'employee');
							});
					$("#employeeSl1").change(
							function() {
								$('#timeAttnForm').formValidation(
										'revalidateField', 'employee');
							});
					$(".timAttDate").change(
							function() {
								$('#timeAttnForm').formValidation(
										'revalidateField', 'startDate');
								$('#timeAttnForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".absentDate").change(
							function() {
								$('#absentForm').formValidation(
										'revalidateField', 'startDate');
								$('#absentForm').formValidation(
										'revalidateField', 'endDate');
							});
					$(".attenStsDate").change(
							function() {
								$('#attenStstForm').formValidation(
										'revalidateField', 'date');
								
							});
					$("#employeeSl3").change(
							function() {
								$('#leavSummaryForm').formValidation(
										'revalidateField', 'employee');
							});
					$("#empProject").change(
							function(){
								$('#empProjectForm').formValidation(
										'revalidateField',projectId);
								
							});
					/**
					 * Section for Settings organization assets form validation
					 */
					$("#StASTForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							assetCode : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Asset code is required'
									}
								}
							},
							assetName : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Asset name is required'
									}
								}
							}
						}
					});
					/**
					 * Section for Settings employee designation form validation
					 */
					$("#StEDForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							designation : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Designation is required'
									}
								}
							},
							rank : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Rank is required'
									},
									regexp : {
										regexp : /^[0-9]+$/i,
										message : 'Please enter valid rank'
									}
								}
							}
						}
					});

					/**
					 * Section for Settings language form validation
					 */
					$("#StLanFrm")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											language : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Is required'
													},
													regexp : {
														regexp : /^[a-z,\s]+$/i,
														message : 'Please enter valid languages'
													}
												}
											}
										}
									});
					/**
					 * Section for settings skill form validation
					 */
					$("#StSkillForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							skillName : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid skill'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Qualification Degree form validation
					 */
					$("#StQDForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							degreeName : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid degree'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StCTForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							contractType : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid type'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StJTForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							jobType : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid type'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StJFForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							jobField : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid type'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StBTForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							branchTypeName : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid type'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StPTForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							policyType : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid type'
									}
								}
							}
						}
					});
					
					$("#exportEmployee").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employeeBranchId : {
								row : '.col-sm-12',
								validators : {
									notEmpty : {
										message : 'Branch Is required'
									}
								}
							}
						}
					});
					
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StETForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employeeType : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid type'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StECForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employeeCategory : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid category'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StRIForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							reimbursementCategory : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid category'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StCTRForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							name : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid name'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StATForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							accountType : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid type'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StWSForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							workShift : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid type'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StESForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							status : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid status'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StTSForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							status : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid status'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StLTForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							leaveType : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid type'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StRSForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							status : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid status'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StEGForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							grade : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid grade'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StADSForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							status : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid status'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StOSForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							status : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid status'
									}
								}
							}
						}
					});
					/**
					 * Section for settings Contract type form validation
					 */
					$("#StLSForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							loanStatus : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Is required'
									},
									regexp : {
										regexp : /^[a-z,\s]+$/i,
										message : 'Please enter valid status'
									}
								}
							}
						}
					});

					/* Section for branch form validation */
					$("#branchForm").formValidation({
						// framework : 'bootstrap',
						// excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							branchName : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Branch name is required'
									}
								}
							}
						}
					});

					/* Section for Performance Evaluation Validation */
					$('#performanceEvaluationForm')
							.formValidation(
									{
										// message : 'This value is not valid',

										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											title : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Evaluation title is required'
													}
												}
											},
											date : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Date is required'
													}
												}
											}
										}
									});

					/* Section for Employee Termination Validation */

					/*
					 * $("#terminationForm").formValidation({ icon : { valid :
					 * 'glyphicon glyphicon-ok', invalid : 'glyphicon
					 * glyphicon-remove', validating : 'glyphicon
					 * glyphicon-refresh' }, fields : { termiantedDate : { row :
					 * '.col-sm-9', validators : { notEmpty : { message :
					 * 'Termianted date is required' } } } } });
					 */

					/* Section for Employee Memo Validation */
					$("#memoForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							memoSubject : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Memo subject is required'
									}
								}
							},
							memoOn : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Memo date is required'
									}
								}
							}
						}
					});

					/* Section for Promotion Validation */
					$("#promotion_details")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employeeId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											promotionDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Promotion date is required'
													}
												}
											},
											designationId : {
												row : '.col-sm-8',
												validators : {
													notEmpty : {
														message : 'Designation required'
													}
												}
											},
											superiorCode : {
												row : '.col-sm-8',
												validators : {
													notEmpty : {
														message : 'Atlease one superior is required'
													}
												}
											}
										}
									});

					/* Section for Complaint Validation */
					$("#complaint_details")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											complaintFromEmpId : {
												row : '.col-sm-8',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											forwardAppToEmpId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Forward Employee is required'
													}
												}
											},
											complaintDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Complaint date required'
													}
												}
											},
											complaintAganistEmp : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Complaint Aganist is required'
													}
												}
											}
										}
									});

					/* Section for department Validation */
					$("#department_details")
							.find('[name="branchId"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#department_details')
												.formValidation(
														'revalidateField',
														'branchId');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											name : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Department name is required'
													}
												}
											},
											sortingOrder : {
												row : '.col-sm-9',
												validators : {
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid decimal place'
													}
												}
											},
											branchId : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Select branch',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'branchId')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											}
										}
									});
					/* Section for WorkShift Validation */
					// $("#workShiftDetailsForm").find('[name="employeeCode"]')
					/*
					 * $("#terminationForm").find('[name="forwaders"]').chosen({
					 * width : '100%' }).change( function(e) {
					 * $('#terminationForm').formValidation( 'revalidateField',
					 * 'forwaders'); }).end().formValidation({ framework :
					 * 'bootstrap', excluded : ':disabled', icon : { valid :
					 * 'glyphicon glyphicon-ok', invalid : 'glyphicon
					 * glyphicon-remove', validating : 'glyphicon
					 * glyphicon-refresh' }, fields : { termiantedDate : { row :
					 * '.col-sm-9', validators : { notEmpty : { message :
					 * 'Termianted date is required' } } }, employee : { row :
					 * '.col-sm-8', trigger : 'change', validators : { notEmpty : {
					 * message : 'Employee is required' } } } } });
					 */

					/* Section for Esi form validation */
					$("#esiForm")
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-8',
												trigger : 'change',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											employeeShare : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee ESI share is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											},
											organizationShare : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Organization ESI share is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											}
										}
									});

					/* Section for Employee Termination Validation */
					$("#terminationForm")
							.find('[name="forwaders"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#terminationForm').formValidation(
												'revalidateField', 'forwaders');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-8',
												trigger : 'change',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											termiantedDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Termianted date is required'
													}
												}
											},
											forwaders : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Forwarder employee is required.',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'forwaders')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											}
										}
									});

					/* Section for Employee Memo Validation */
					$("#memoForm")
							.find('[name="forwaders"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#memoForm').formValidation(
												'revalidateField', 'forwaders');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											memoSubject : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Memo subject is required'
													}
												}
											},
											memoOn : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Memo date is required'
													}
												}
											},
											forwaders : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Select subordinates.',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'forwaders')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											}
										}
									});

					/* Section for provident fund form validation */
					$("#profidentFundForm")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-8',
												trigger : 'change',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											employeeShare : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee PF share is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											},
											organizationShare : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Organization PF share is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											}
										}
									});

					/* Section for loan form validation */
					$("#loanForm")
							.find('[name="forwardTo"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#loanForm').formValidation(
												'revalidateField', 'forwardTo');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											forwardTo : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Select forwarders.',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'forwardTo')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											},
											loanTitle : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Loann title is required'
													}
												}
											},
											loanAmount : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Loan amount is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											},
											loanDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Loan date is required'
													}
												}
											},
											repaymentAmount : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Repayment amount is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											},
											repaymentStartDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Loan repayment date is required'
													}
												}
											}
										}
									});

					/* Section for Overtime form validation */
					$("#overtimeForm")
							.find('[name="forwaders"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#overtimeForm').formValidation(
												'revalidateField', 'forwaders');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											forwaders : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Select forwarders.',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'forwaders')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											},
											overtimeTitle : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Overtime title is required'
													}
												}
											},
											overtimeAmount : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Overtime amount is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											},
											requestedDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Request date is required'
													}
												}
											},
											timeIn : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Time IN is required'
													}
												}
											},
											timeOut : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Time OUT is required'
													}
												}
											}
										}
									});

					/* Section for Insurance Form Validation */
					$("#insuranceForm")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-8',
												trigger : 'change',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											insuranceTitle : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Insurance title is required'
													}
												}
											},
											employeeShare : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee share is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											},
											organizationShare : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Organization share is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											}
										}
									});

					/* Section for advance salary form validation */
					$("#advanceSalaryForm")
							.find('[name="forwaders"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#advanceSalaryForm').formValidation(
												'revalidateField', 'forwaders');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											forwaders : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Select forwarders.',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'forwaders')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											},
											advanceSalaryTitle : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Advance salary title is required'
													}
												}
											},
											advanceAmount : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Advance amount is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											},
											requestedDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Request date is required'
													}
												}
											}
										}
									});

					/* Section for loan form validation */
					$("#loanForm")
							.find('[name="forwardTo"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#loanForm').formValidation(
												'revalidateField', 'forwardTo');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											forwardTo : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Select forwarders.',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'forwardTo')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											},
											loanTitle : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Loann title is required'
													}
												}
											},
											loanAmount : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Loan amount is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											},
											loanDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Loan date is required'
													}
												}
											},
											repaymentAmount : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Repayment amount is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											},
											repaymentStartDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Loan repayment date is required'
													}
												}
											}
										}
									});

					/* Section for Insurance Form Validation */
					$("#insuranceForm")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-8',
												trigger : 'change',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											insuranceTitle : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Insurance title is required'
													}
												}
											},
											employeeShare : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee share is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											},
											organizationShare : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Organization share is required'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid amount'
													}
												}
											}
										}
									});

					/* Section for WorkShift Validation */
					$("#workShiftDetailsForm")
							.find('[name="employeeCode"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#workShiftDetailsForm')
												.formValidation(
														'revalidateField',
														'employeeCode');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employeeCode : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Select employee code.',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'employeeCode')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											}
										}
									});

					/* Section for Employee Warning Validation */
					$("#warningForm")
							.find('[name="forwaders"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#warningForm').formValidation(
												'revalidateField', 'forwaders');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-8',
												trigger : 'change',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											forwaders : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Select forwarders.',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'forwaders')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											},
											warningOn : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Warning date is required'
													}
												}
											},
											subject : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Warning subject is required'
													}
												}
											}
										}
									});

					/* Section for Employee Exit Validation */
					$("#employeExitForm")
							.find('[name="exitType"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#employeExitForm').formValidation(
												'revalidateField', 'exitType');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											exitDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Exit date is required'
													}
												}
											},
											employee : {
												row : '.col-sm-8',
												trigger : 'change',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											exitType : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Select type of exit.',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'exitType')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											}
										/*
										 * , exitInterview : { row :
										 * '.col-sm-9', validators : { callback : {
										 * message : 'Select type of exit
										 * interview.', callback : function(
										 * value, validator, $field) { var
										 * options = validator
										 * .getFieldElements( 'exitInterview')
										 * .val(); return (options != null &&
										 * options.length >= 1); } } } }
										 */
										}
									});

					/* Section for holiday validator */
					$("#holidayForm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							title : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Holiday titile is required'
									}
								}
							},
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Start date is required'
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'End date is required'
									}
								}
							}
						}
					});

					/* Section for System Settings Validation */
					$("#organisationForm")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											organisationCode : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Organisation code is required'
													}
												}
											},
											organisationUrt : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Organisation URL is required'
													}
												}
											},
											organisationName : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Organisation Name is required'
													}
												}
											},
											organisationYear : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Organisation Year is required'
													}
												}
											},
											fiscalMonth : {
												row : '.col-sm-4',
												validators : {
													notEmpty : {
														message : 'Fiscal Month is required'
													}
												}
											},
											contactFirstName : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'First Name is required'
													}
												}
											},
											contactLastName : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Last Name is required'
													}
												}
											},
											contactEmail : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Email is required'
													},
													regexp : {
														regexp : '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
														message : 'The value is not a valid email address'
													}
												}
											},
											contactCountry : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Country is required'
													}
												}
											},
											contactNumber : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Contact Number required'
													},
													phone : {
														country : 'IN',
														message : 'The value is not valid %s phone number'
													}
												}
											}
										}
									});

					/* Section for Currency Settings Validation */
					$("#currencySettingsForm")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {

											employeeCode : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee code is required'
													}
												}
											},
											employeeTypeId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee type is required'
													}
												}
											},
											employeeCategoryId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee category is required'
													}
												}
											},
											designationId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee designation is required'
													}
												}
											},
											employeeBranchId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee branch is required'
													}
												}
											},
											employeeDepartmentId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee department is required'
													}
												}
											},
											employeeGradeId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Currency is required'
													}
												}
											},
											decimalPlaces : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Enter a valid decimal place'
													},
													regexp : {
														regexp : /^((\d+)((\.\d{1,2})?))$/i,
														message : 'Enter a valid decimal place'
													}
												}
											},
											workShiftId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee workshift is required'
													}
												}
											},
											withPayroll : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee payroll is required'
													}
												}
											}
										}
									});

					/* Section for reminder Validation */
					$("#reminderForm")
							.find('[name="reminderTo"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#reminderForm')
												.formValidation(
														'revalidateField',
														'reminderTo');
									})
							.end()
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											reminderTitle : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Reminder title is required'
													}
												}
											},
											reminderMonth : {
												row : '.col-sm-5',
												validators : {
													notEmpty : {
														message : 'Reminder month is required'
													}
												}
											},
											reminderTime : {
												row : '.col-sm-5',
												validators : {
													notEmpty : {
														message : 'Reminder time is required'
													}
												}
											},
											reminderStatus : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Reminder status is required'
													}
												}
											},
											reminderMessage : {
												row : '.col-md-12',
												validators : {
													notEmpty : {
														message : 'Reminder message is required'
													}
												}
											},
											reminderTo : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Reminder recipient is required',
														callback : function(
																value,
																validator,
																$field) {
															var options = validator
																	.getFieldElements(
																			'reminderTo')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											}
										}
									});
					/* Section for Employee Validation */
					$('#save_employeeInfo_form')
							.formValidation(
									{
										// message : 'This value is not valid',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employeeCode : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee code is required'
													}
												}
											},
											employeeTypeId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee type is required'
													}
												}
											},
											employeeCategoryId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee category is required'
													}
												}
											},
											designationId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee designation is required'
													}
												}
											},
											employeeBranchId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee branch is required'
													}
												}
											},
											employeeDepartmentId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee department is required'
													}
												}
											},
											employeeGradeId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee grade is required'
													}
												}
											},
											workShiftId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Work shift is required'
													}
												}
											},
											withPayroll : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'required'
													}
												}
											}
										}
									});

					/** *********** Section for hourly wages form validation *** */
					$('#hoursWagesForm').find('[name="employeeId"]').chosen({
						width : '100%'
					})
					// Revalidate the color when it is changed
					.change(
							function(e) {
								$('#hoursWagesForm').formValidation(
										'revalidateField', 'employeeId');
							}).end().formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Employee is required'
									}
								}
							}
						}
					});

					/** *********** Section for bonus form validation *** */
					$('#addBonusForm').find('[name="employeeId"]').chosen({
						width : '100%'
					})
					// Revalidate the color when it is changed
					.change(
							function(e) {
								$('#addBonusForm').formValidation(
										'revalidateField', 'employeeId');
							}).end().formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Employee is required'
									}
								}
							},
							bonusTitleId : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Bonus Title is required'
									}
								}
							},
							amount : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Amount is required'
									}
								}
							},
							date : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									}
								}
							}

						}
					});

					/** *********** Section for daily wages form validation *** */
					$('#dailyWageAdDForm').find('[name="employeeId"]').chosen({
						width : '100%'
					})
					// Revalidate the color when it is changed
					.change(
							function(e) {
								$('#dailyWageAdDForm').formValidation(
										'revalidateField', 'employeeId');
							}).end().formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Employee is required'
									}
								}
							},
							wage : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Amount is required'
									}
								}
							}
						}
					});

					/**
					 * *********************************** DEDUCTION
					 * *************
					 */
					$('#deductionAddForm').find('[name="employeeId"]').chosen({
						width : '100%'
					})
					// Revalidate the color when it is changed
					.change(
							function(e) {
								$('#deductionAddForm').formValidation(
										'revalidateField', 'employeeId');
							}).end().formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Employee is required'
									}
								}
							},
							titleId : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Deduction Title is required'
									}
								}
							},
							amount : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Amount is required'
									}
								}
							},
							date : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									}
								}
							}

						}
					});

					/**
					 * *********************************** COMMISSION
					 * *************
					 */
					$('#commition_add_form')
							.find('[name="employeeId"]')
							.chosen({
								width : '100%'
							})
							// Revalidate the color when it is changed
							.change(
									function(e) {
										$('#commition_add_form')
												.formValidation(
														'revalidateField',
														'employeeId');
									})
							.end()
							.formValidation(
									{
										// message : 'This value is not valid',
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											titleId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Commission Title is required'
													}
												}
											},
											amount : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Amount is required'
													}
												}
											},
											date : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Date is required'
													}
												}
											}

										}
									});

					/**
					 * *********************************** ADJUSTMENT
					 * *************
					 */
					$('#adjustmetn_add_form')
							.find('[name="employeeId"]')
							.chosen({
								width : '100%'
							})
							// Revalidate the color when it is changed
							.change(
									function(e) {
										$('#adjustmetn_add_form')
												.formValidation(
														'revalidateField',
														'employeeId');
									})
							.end()
							.formValidation(
									{
										// message : 'This value is not valid',
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},

											titleId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Commission Title is required'
													}
												}
											},
											amount : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Amount is required'
													}
												}
											},
											date : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Date is required'
													}
												}
											}

										}
									});

					/* Section for Leave Validation */

					$('#newLeaveForm')
							.find('[name="superiorIds"]')
							.chosen({
								width : '100%'
							})
							// Revalidate the color when it is changed
							.change(
									function(e) {
										$('#newLeaveForm').formValidation(
												'revalidateField',
												'superiorIds');
									})
							.end()
							.formValidation(
									{
										// message : 'This value is not valid',
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employeeId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											superiorIds : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Superior is required',
														callback : function(
																value,
																validator,
																$field) {
															// Get the selected
															// options
															var options = validator
																	.getFieldElements(
																			'superiorIds')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											},
											leaveTypeId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Leave Type is required'
													}
												}
											},
											reason : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Reason is required'
													}
												}
											},

											fromDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Date is required'
													},
													date : {
														message : 'The from date is not valid',
														format : 'DD/MM/YYYY',
														max : 'toDate'
													}
												},
												onSuccess : function(e, data) {
													if (!data.fv
															.isValidField('toDate')) {
														data.fv
																.revalidateField('toDate');
													}
												}
											},
											toDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Date is required'
													},
													date : {
														message : 'The to date is not valid',
														format : 'DD/MM/YYYY',
														min : 'fromDate'
													}
												},
												onSuccess : function(e, data) {
													if (!data.fv
															.isValidField('fromDate')) {
														data.fv
																.revalidateField('fromDate');
													}
												}
											},

										}
									});

					/**
					 * *************************** Reimbursement
					 * *************************************
					 */

					$('#reimbursementsAddForm')
							.find('[name="superiorIds"]')
							.chosen({
								width : '100%'
							})
							.change(
									function(e) {
										$('#reimbursementsAddForm')
												.formValidation(
														'revalidateField',
														'superiorIds');
									})
							.end()
							.formValidation(
									{
										// message : 'This value is not valid',
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},
											superiorIds : {
												row : '.col-sm-9',
												validators : {
													callback : {
														message : 'Superior is required',
														callback : function(
																value,
																validator,
																$field) {
															// Get the selected
															// options
															var options = validator
																	.getFieldElements(
																			'superiorIds')
																	.val();
															return (options != null && options.length >= 1);
														}
													}
												}
											},
											title : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Title is required'
													}
												}
											},
										/*
										 * amount : { row : '.col-sm-9',
										 * validators : { notEmpty : { message :
										 * 'Amount is required' } } },
										 */

										}
									});

					/**
					 * *************************** Transfer
					 * *************************************
					 */

					$('#transfer_add_form').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Employee is required'
									}
								}
							},

							superiorCode : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Superior is required'
									}
								}
							},
							date : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									}
								}
							},
							branchId : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Branch is required'
									}
								}
							},

						}
					});
					
					/*********************************
					 * pay salary
					 */
					$('#paysalary_form').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employeeIds : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Employee is required'
									}
								}
							},

							salaryType : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Type is required'
									}
								}
							},
							date : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									}
								}
							},

						}
					});

					/***********************************************************
					 * Announcements
					 */
					$('#announcementForm')
							.formValidation(
									{
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											announcementsTitle : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Required'
													}
												}
											},
											startDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Required'
													},
													date : {
														message : 'The start date is not valid',
														format : 'DD/MM/YYYY',
														max : 'endDate'
													}
												},
												onSuccess : function(e, data) {
													if (!data.fv
															.isValidField('endDate')) {
														data.fv
																.revalidateField('endDate');
													}
												}
											},
											endDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Required'
													},
													date : {
														message : 'The end date is not valid',
														format : 'DD/MM/YYYY',
														min : 'startDate'
													}

												},
												onSuccess : function(e, data) {
													if (!data.fv
															.isValidField('startDate')) {
														data.fv
																.revalidateField('startDate');
													}
												}
											}

										}
									});

					/**
					 * *************************** Travel
					 * *************************************
					 */

					$('#travel_add_form')
							.formValidation(
									{
										// message : 'This value is not valid',
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},

											superiorCode : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Superior is required'
													}
												}
											},
											purposeOfVisit : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Required'
													}
												}
											},
											expectedBudget : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Required'
													}
												}
											},
											actualBudget : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Required'
													}
												}
											},
											startDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Date is required'
													},
													date : {
														message : 'The start date is not valid',
														format : 'DD/MM/YYYY',
														max : 'endDate'
													}
												},
												onSuccess : function(e, data) {
													if (!data.fv
															.isValidField('endDate')) {
														data.fv
																.revalidateField('endDate');
													}
												}
											},
											endDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Date is required'
													},
													date : {
														message : 'The end date is not valid',
														format : 'DD/MM/YYYY',
														min : 'startDate'
													}
												},
												onSuccess : function(e, data) {
													if (!data.fv
															.isValidField('startDate')) {
														data.fv
																.revalidateField('startDate');
													}
												}
											},

										}
									});

					/**
					 * *************************** Resignation
					 * *************************************
					 */

					$('#resignation_add_form')
							.formValidation(
									{
										// message : 'This value is not valid',
										framework : 'bootstrap',
										excluded : ':disabled',
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											employee : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Employee is required'
													}
												}
											},

											superiorCode : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Superior is required'
													}
												}
											},
											noticeDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Date is required'
													},
													date : {
														message : 'Notice date is not valid',
														format : 'DD/MM/YYYY',
														max : 'resignationDate'
													}
												},
												onSuccess : function(e, data) {
													if (!data.fv
															.isValidField('resignationDate')) {
														data.fv
																.revalidateField('resignationDate');
													}
												}
											},
											resignationDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'Date is required'
													},
													date : {
														message : 'Resignation date is not valid',
														format : 'DD/MM/YYYY',
														min : 'noticeDate'
													}
												},
												onSuccess : function(e, data) {
													if (!data.fv
															.isValidField('noticeDate')) {
														data.fv
																.revalidateField('noticeDate');
													}
												}
											},

										}
									});

					/** *************HR Reports*************** */

					$('#hrHoliForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							bid : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Select Valid Option'
									}
								}
							},

							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					/** ********Recruitment report********** */
					$('#recInterForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					/** *********************Pay Roll Reports****************** */
					$('#payHourForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					$('#payDayForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					$('#pay_split_form').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {

							startYear : {
								row : '.col-sm-5',
								validators : {
									notEmpty : {
										message : 'Date is required'
									}

								}
							},
							endYear : {
								row : '.col-sm-5',
								validators : {
									notEmpty : {
										message : 'Date is required'
									}

								}
							}
						}
					});

					/** ************Employee Reports******************** */
					$('#empSummaryForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-8',
								trigger : 'change',
								validators : {
									notEmpty : {
										message : 'Employee Name Required'
									}
								}
							}
							
						}
					});
					
					$('#empProjectForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							projectId : {
								row : '.col-sm-9',
								trigger : 'change',
								validators : {
									notEmpty : {
										message : 'Select Project'
									}
								}
							}
							
						}
					});
					
					$('#empJoinForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					$('#empTranForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					$('#empResForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					$('#empTravForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					$('#empAssinForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});
					
					$('#empPerformanceForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					/** *************************Tmesheet Reports*************** */
					$('#timWorkForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					$('#timEmWorkForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-8',
								trigger : 'change',
								validators : {
									notEmpty : {
										message : 'Employee Name Required'
									}
								}
							},
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					$('#timeSheetForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-8',
								trigger : 'change',
								validators : {
									notEmpty : {
										message : 'Employee Name Required'
									}
								}
							}
						}
					});

					$('#timeAttnForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-8',
								trigger : 'change',
								validators : {
									notEmpty : {
										message : 'Employee Name Required'
									}
								}
							},
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});

					$('#absentForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							startDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
										max : 'endDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('endDate')) {
										data.fv.revalidateField('endDate');
									}
								}
							},
							endDate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'End date is not valid',
										format : 'DD/MM/YYYY',
										min : 'startDate'
									}
								},
								onSuccess : function(e, data) {
									if (!data.fv.isValidField('startDate')) {
										data.fv.revalidateField('startDate');
									}
								}
							},
						}
					});
					$('#leavSummaryForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-8',
								trigger : 'change',
								validators : {
									notEmpty : {
										message : 'Employee Name Required'
									}
								}
							},
							year : {
								row : '.col-sm-9',
								trigger : 'change',
								validators : {
									notEmpty : {
										message : 'Year Required'
									}
								}
							},
						}
					});
					/***********************************************************
					 * Work sheet validation
					 */
					$('#add_sheet_form').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-8',
								trigger : 'change',
								validators : {
									notEmpty : {
										message : 'Employee Name Required'
									}
								}
							},
							date : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
									}
								}
							}
						}
					});
					$('#add_sheet_form').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							employee : {
								row : '.col-sm-8',
								trigger : 'change',
								validators : {
									notEmpty : {
										message : 'Employee Name Required'
									}
								}
							},
							date : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Start date is not valid',
										format : 'DD/MM/YYYY',
									}
								}
							}
						}
					});
					$('#attenStstForm').formValidation({
						// message : 'This value is not valid',
						framework : 'bootstrap',
						excluded : ':disabled',
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							
							employeeCode : {
								row : '.col-sm-9',
								trigger : 'change',
								validators : {
									notEmpty : {
										message : 'Employee Name Required'
									}
								}
							},
							date : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Date is required'
									},
									date : {
										message : 'Date is not valid',
										format : 'DD/MM/YYYY',
									}
								}
							}
						}
					});	
					/*
					 * $('#booking').validate({ // <- attach '.validate()' to
					 * your form // any rules, options, and callbacks, framework :
					 * 'bootstrap', excluded : ':disabled', icon : { valid :
					 * 'glyphicon glyphicon-ok', invalid : 'glyphicon
					 * glyphicon-remove', validating : 'glyphicon
					 * glyphicon-refresh' }, fields : { firstname: { validators : {
					 * notEmpty : { // minlength: 2, message : 'Notice date is
					 * not valid' }, } } // more rules, }, submitHandler:
					 * function(form) { // <- only fires when form is valid
					 * $.ajax({ type: 'POST', url:
					 * 'http://www.fethiye-tours.com/book.php', data:
					 * $(form).serialize(), success: function(msg) {
					 * $(form).fadeOut(500, function(){
					 * $(form).html(msg).fadeIn(); }); } }); // <- end '.ajax()'
					 * return false; // <- block default form action } // <- end
					 * 'submitHandler' callback });
					 */
				});
