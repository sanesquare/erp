jQuery(document).ready(function($) {
	/**
	 * section for termination documents
	 */
	$("#termination-documents").uploadFile({
		url : "UploadTerminationDocument",
		multiple : true,
		showProgress : true,
		formData : {
			"terminationId" : $("#terminationId").val()
		},
		fileName : "terminationDocs",
		onSuccess : function(files, data, xhr) {
			setTimeout(function() {
				$('.ajax-file-upload-statusbar').fadeOut(500);
			}, 1000);
			readAllTerminationDocuments($("#terminationId").val());
		}
	});

	/**
	 * section for warning documents
	 */
	$("#warning-documents").uploadFile({
		url : "UploadWarningDocument",
		multiple : true,
		showProgress : true,
		formData : {
			"warningId" : $("#warningId").val()
		},
		fileName : "warningDocs",
		onSuccess : function(files, data, xhr) {
			setTimeout(function() {
				$('.ajax-file-upload-statusbar').fadeOut(500);
			}, 1000);
			listAllWarningDocuments($("#warningId").val());
		}
	});

	/**
	 * section for memo documents
	 */
	$("#memo-documents").uploadFile({
		url : "UploadMemoDocument",
		multiple : true,
		showProgress : true,
		formData : {
			"memoId" : $("#memoId").val()
		},
		fileName : "memoDocuments",
		onSuccess : function(files, data, xhr) {
			setTimeout(function() {
				$('.ajax-file-upload-statusbar').fadeOut(500);
			}, 1000);
			listAllMemoDocuments($("#memoId").val());
		}
	});
});
