<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="25%">Parent</th>
			<th width="25%">Company Name</th>
			<th width="20%">Period From</th>
			<th width="20%">Period To</th>
			<th width="5%">Status</th>
			<th width="5%">Is Default</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${companies }" var="c">
			<tr>
				<td>${c.parent }</td>
				<td>${c.companyName }</td>
				<td>${c.periodStart }</td>
				<td>${c.periodEnd }</td>
				<td>${c.status }</td>
				<c:choose>
					<c:when test="${c.status == 'Active'}">
						<c:choose>
							<c:when test="${c.companyId == companyId }">
								<td><input type="radio" class="form-control radios"
									name="is_default" checked="checked" value="${c.companyId }"
									style="border-color: black;"></td>
							</c:when>
							<c:otherwise>
								<td><input type="radio" class="form-control radios"
									name="is_default" value="${c.companyId }"
									style="border-color: black;"></td>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<td></td>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>