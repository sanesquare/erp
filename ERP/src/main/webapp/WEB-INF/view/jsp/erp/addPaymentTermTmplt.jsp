<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="col-md-12">
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-3 control-label">Payment
			Term<span class="stars">*</span>
		</label>
		<div class="col-sm-9">
			<input type="text" value="${paymentTermsVo.name }" id="pymntTermName"
				class="form-control paste">
				<input type="hidden" value="${paymentTermsVo.id }" id="pymntTermId">
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" border="0"
		class="table table-striped table-bordered erp-tbl"
		id="pymntTermTableBody">
		<thead>
			<tr>
				<th width="20%">Percentage</th>
				<th width="20%">No.Of Days</th>
				<th width="30%">Payment Method</th>
				<th width="10%">Delete</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="p" items="${paymentTermsVo.installmentVos }"
				varStatus="s">
				<tr>
				<input type="hidden" id="hidInstllmnts${s.count }" value="${p.id }">
					<td><input type="text" value="${p.percentage }"
						id="percentage${s.count }" class="form-control double"></td>
					<td><input type="text" value="${p.nofdays }"
						id="nofDays${s.count  }" class="form-control numbersonly"></td>
					<td><select class="form-control pymntMthSelect" id="pymntMthds${s.count  }">
							<option value="" label="-select payment method-">
								<c:forEach items="${paymentMethods }" var="pm">
									<c:choose>
										<c:when test="${p.paymentMethodId == pm.id}">
											<option value="${pm.id }" selected="selected" label="${pm.name }" />
										</c:when>
										<c:otherwise>
											<option value="${pm.id }" label="${pm.name }" />
										</c:otherwise>
									</c:choose>
								</c:forEach>
					</select></td>
					<td><i class="fa fa-times"
						onclick="javascript:deletePymentTermsaRow(this)"></i></td>
				</tr>
			</c:forEach>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td><button type="button" onclick="addPaymntTermRow()"
						class="btn btn-primary erp-btn">Add Row</button></td>
			</tr>
		</tbody>
	</table>
	<!-- Table For Payment Term Installments -->
	<div class="form-group wid-80">
		<!-- <button type="button" class="btn btn-primary erp-btn">New</button> -->
		<button onclick="getRowCount()" type="button"
			class="btn btn-primary erp-btn">Save</button>
	</div>
</div>
<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>