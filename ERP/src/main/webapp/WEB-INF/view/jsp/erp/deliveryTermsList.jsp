<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="col-md-12">
	<div class="panel panel-default erp-info">
		<div class="panel-heading panel-inf">Delivery Terms</div>
		<div class="panel-body tbl_scrl_bdy">

			<!-- PAYMENT TERMS -->

			<table cellpadding="0" cellspacing="0" border="0"
				class="table table-striped table-bordered erp-tbl"
				id="basic-datatablesss">
				<thead>
					<tr>
						<th width="30%">Delivery Term</th>
						<th width="30%">Edit</th>
						<th width="10%">Delete</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="p" items="${deliveryTerms }">
						<tr>
							<td>${p.method }</td>
							<td><i
								onclick="editDeliveryTerm('${p.deliveryMethodId}','${p.method }')"
								class="fa fa-pencil"></i></td>
							<td><i onclick="deleteDelvryTerm('${p.deliveryMethodId}')"
								class="fa fa-times"></i></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<form action="#" id="dlvry_term_frm">
				<input type="hidden" id="dlvry_term_id">
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Delivery
						Term<span class="stars">*</span>
					</label>
					<div class="col-sm-4">
						<input type="text" name="method" id="dlvry_term_name"
							class="form-control paste">
					</div>
				</div>
				<div class="form-group wid-80">
					<button type="button" onclick="clearDeliveryTerm()"
						class="btn btn-primary erp-btn">New</button>
					<button type="submit" onclick="saveDeliveryTerm()"
						class="btn btn-primary erp-btn">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<!-- Custom JQuery -->
<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>