<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:choose>
<c:when test="${rowId %2 ==0}">
<tr style="background-color: #f9f9f9;">
<input type="hidden" id="hidInstllmnts${s.index }" >
	<td style="padding: .5%;"><input type="text" id="percentage${rowId }"
		class="form-control double"></td>
	<td style="padding: .5%;"><input type="text" id="nofDays${rowId }"  class="form-control numbersonly"></td>
	<td style="padding: .5%;"><select class="form-control pymntMthSelect" id="pymntMthds${rowId }">
			<option value="" label="-select payment method-">
				<c:forEach items="${paymentMethods }" var="p">
					<option value="${p.id }" label="${p.name }" />
				</c:forEach>
	</select></td>
	<td style="padding: .5%;"><i class="fa fa-times" onclick="javascript:deletePymentTermsaRow(this);  return false;"></i></td>
</tr>
</c:when>
<c:otherwise>
<tr>
<input type="hidden" id="hidInstllmnts${rowId }" >
	<td style="padding: .5%;"><input type="text" id="percentage${rowId }"
		class="form-control double"></td>
	<td style="padding: .5%;"><input type="text" id="nofDays${rowId }"  class="form-control numbersonly"></td>
	<td style="padding: .5%;"><select class="form-control pymntMthSelect" id="pymntMthds${rowId }">
			<option value="" label="-select payment method-">
				<c:forEach items="${paymentMethods }" var="p">
					<option value="${p.id }" label="${p.name }" />
				</c:forEach>
	</select></td>
	<td style="padding: .5%;"><i onclick="javascript:deletePymentTermsaRow(this);  return false;" class="fa fa-times"></i></td>
</tr>
</c:otherwise>
</c:choose>

<tr>
	<td style="padding: .5%;"></td>
	<td style="padding: .5%;"></td>
	<td style="padding: .5%;"></td>
	<td style="padding: .5%;"><button type="button" onclick="addPaymntTermRow()"
			class="btn btn-primary erp-btn">Add Row</button></td>
</tr>

<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>