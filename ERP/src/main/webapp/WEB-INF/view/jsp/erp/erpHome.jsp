<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>

	<div class="monus_home">
		<div class="warper container-fluid">
			<div class="row" style="padding-top: 3%;">
				<div class="col-md-4">
					<c:choose>
						<c:when test="${ rolesMap.roles['HRMS'].view}">
							<a href="hrmsDashboard.do">
						</c:when>
						<c:otherwise>
							<a href="#">
						</c:otherwise>
					</c:choose>
					<div class="panel panel-default hrms" style="border-radius: 46px;">
						<div class="panel-body monus hrms">

							<span class="monus_title ">HRM</span> <span class="monus_icons "><i
								class="fa fa-users"></i></span>
							<div class="hrms-label">Keep a control on your employee.</div>
						</div>
					</div>
					</a>
				</div>
				<div class="col-md-4">
					<c:choose>
						<c:when test="${ companyId !=0 }">
							<c:choose>
								<c:when test="${ rolesMap.roles['Accounts'].view}">
									<a href="accountsHome.do">
								</c:when>
								<c:otherwise>
									<a href="#">
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<a onclick="javascript:$('.pop').fadeIn(300)">
						</c:otherwise>
					</c:choose>
					<div class="panel panel-default accounts"
						style="border-radius: 46px;">
						<div class="panel-body monus accounts">

							<span class="monus_title"> ACCOUNTS </span><span
								class="monus_icons"><i class="fa fa-calculator"></i></i></span>
							<div class=" acc-label">See accurate real-time reports like
								Balance Sheet and Profit & Loss.</div>
						</div>
					</div>
					</a>
				</div>
				<%-- <div class="col-md-4">
					<c:choose>
						<c:when test="${ companyId !=0 }">
							<a href="crmHome.do">
						</c:when>
						<c:otherwise>
							<a onclick="javascript:$('.pop').fadeIn(300)">
						</c:otherwise>
					</c:choose>
					<div class="panel panel-default crm" style="border-radius: 46px;">
						<div class="panel-body monus crm">

							<span class="monus_title">CRM </span><span class="monus_icons"><i
								class="fa fa-comments"></i></span>
							<div class=" crm-label">Track Opportunities and email
								personalized quotes.</div>
						</div>
					</div>
					</a>
				</div> --%>

				<div class="col-md-4">
					<c:choose>
						<c:when test="${ companyId !=0 }">
							<c:choose>
								<c:when test="${ rolesMap.roles['Purchase'].view}">
									<a href="purchaseHome.do">
								</c:when>
								<c:otherwise>
									<a href="#">
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<a onclick="javascript:$('.pop').fadeIn(300)">
						</c:otherwise>
					</c:choose>
					<div class="panel panel-default purchase"
						style="border-radius: 46px;">
						<div class="panel-body monus purchase">

							<span class="monus_title">PURCHASE</span> <span
								class="monus_icons"><i class="fa fa-shopping-cart"></i></i></span>
							<div class="pchse-label">Easily order products and pay
								vendors on time.</div>
						</div>
					</div>
					</a>
				</div>
			</div>
			<div class="row" style="padding-top: 3%;">
				<div class="col-md-4">
					<c:choose>
						<c:when test="${ companyId !=0 }">
							<c:choose>
								<c:when test="${ rolesMap.roles['Sales'].view}">
									<a href="salesHome.do">
								</c:when>
								<c:otherwise>
									<a href="#">
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<a onclick="javascript:$('.pop').fadeIn(300)">
						</c:otherwise>
					</c:choose>
					<div class="panel panel-default sales" style="border-radius: 46px;">
						<div class="panel-body monus sales">

							<span class="monus_title">SALES</span> <span class="monus_icons"><i
								class="fa fa-bar-chart-o"></i></span>
							<div class=" sls-label">Email beautiful invoices and
								receive money in a snap.</div>
						</div>
					</div>
					</a>
				</div>

				<div class="col-md-4">
					<c:choose>
						<c:when test="${ companyId !=0 }">
							<c:choose>
								<c:when test="${ rolesMap.roles['Inventory'].view}">
									<a href="inventoryHome.do">
								</c:when>
								<c:otherwise>
									<a href="#">
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<a onclick="javascript:$('.pop').fadeIn(300)">
						</c:otherwise>
					</c:choose>
					<div class="panel panel-default inventory"
						style="border-radius: 46px;">
						<div class="panel-body monus inventory">

							<span class="monus_title">INVENTORY</span> <span
								class="monus_icons"><i class="fa fa-truck"></i></span>
							<div class=" inv-label">Prepare and ship customer orders,
								keep tight control over your inventory levels.</div>

						</div>
					</div>
					</a>
				</div>
				<div class="col-md-4">
					<c:choose>
						<c:when test="${authEmployeeDetails.isAdmin }">
							<a href="settings.do">
						</c:when>
						<c:otherwise>
							<a href="#">
						</c:otherwise>
					</c:choose>
					<div class="panel panel-default crm" style="border-radius: 46px;">
						<div class="panel-body monus crm">

							<span class="monus_title"> SETTINGS </span><span
								class="monus_icons"><i class="fa fa-cog "></i></i></span>
							<div class=" crm-label">Settings for your application...</div>
						</div>
					</div>
					</a>
				</div>


			</div>

			<%-- <c:if test="${authEmployeeDetails.isAdmin }">
				<div class="row" style="padding-top: 3%;">
					<div class="col-md-4"></div>
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="settings">
							<span class=""><a href="settings.do"><i
									class="fa fa-cog settings-ico"></i></a></span>

						</div>
					</div>
				</div>
			</c:if> --%>
		</div>

	</div>
	<div class="pop" style="display: none">
		<div class="pop_content">
			<span class="pop-ico"><i class="fa fa-exclamation"></i></span> <span
				class="pop-text">oops.. You haven't created any company yet..
				please contact admin</span><span class="pop-close"
				onclick="javascript:$('.pop').fadeOut(300);"><i
				class="fa fa-times"></i></span>
		</div>
	</div>

	<c:if test="${! empty error_msg }">
		<div class="pop">
			<div class="pop_content">
				<span class="pop-ico"><i class="fa fa-exclamation"></i></span> <span
					class="pop-text">oops.. You haven't created any company
					yet.. please contact admin</span><span class="pop-close"
					onclick="javascript:$('.pop').fadeOut(300);"><i
					class="fa fa-times"></i></span>
			</div>
		</div>
	</c:if>


	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$("document").ready(function() {
			/* 	$(window).on('popstate', function() {
					var urls=[];
					urls=location.href.split("#");
					alert(urls[1]);
				}); */
			/* $(window.location.href).on("change",function(){
				alert();
			}); */
		});
	</script>
</body>
</html>