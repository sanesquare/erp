<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatabless">
	<thead>
		<tr>
			<th width="25%">Tax</th>
			<th width="25%">Rate(%)</th>
			<th width="10%">Edit</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${taxes }" var="t">
			<tr>
				<td>${t.taxName }</td>
				<td>${t.rate }</td>
				<td><a
					onclick="editTaxValues('${t.id}','${t.taxName }','${t.rate }')"><i
						class="fa fa-pencil"></i></a></td>
				<td><a onclick="deleteTax('${t.id}')"><i
						class="fa fa-times"></i></a></td>

			</tr>
		</c:forEach>

	</tbody>
</table>

<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>