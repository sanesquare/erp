<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="col-md-12">
	<div class="panel panel-default erp-info">
		<div class="panel-heading panel-inf">Payment Terms</div>
		<div class="panel-body">

			<!-- PAYMENT TERMS -->

			<table cellpadding="0" cellspacing="0" border="0"
				class="table table-striped table-bordered erp-tbl"
				id="basic-datatablesss">
				<thead>
					<tr>
						<th width="30%">Payment Term</th>
						<th width="30%">Edit</th>
						<th width="10%">Delete</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="p" items="${paymentTerms }">
						<tr>
							<td>${p.name }</td>
							<td><i onclick="addPaymentTerm('${p.id}')"
								class="fa fa-pencil"></i></td>
							<td><i onclick="deletePaymentTerms('${p.id}')" class="fa fa-times"></i></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<div class="form-group wid-80">
				<button type="button" onClick="addPaymentTerm('0')"
					class="btn btn-primary erp-btn">New</button>
				<!-- <button type="button" class="btn btn-primary erp-btn">Save</button> -->
			</div>
			<div id="addPymntTermDiv"></div>
		</div>
	</div>
</div>
<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<!-- Custom JQuery -->
<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>