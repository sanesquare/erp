<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<a id="totop" class="back-to-top"><i class="fa fa-arrow-up"></i></a>
	<div class="erp_home">
		<div class="warper container-fluid">
			<div class="page-header">
				<div class="hdr">
					<div class="home-div">
						<a href="erpHome.do"> <span class="home"><i
								class="fa fa-home"></i></span>
						</a>
					</div>
					<div class="hdr-div">
						<span class="hdr-ico"><i class="fa fa-cog settings-ico"></i></span>
						Settings <small>Let's get a quick overview...</small>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Basic Information<a href="erpHome.do"><span class="back-btn"><i
									class="fa fa-arrow-left"></i></span></a>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Company Information</div>
										<div class="row">
											<div class="col-md-6">
												<div class="panel-body">
													<form:form action="saveCompany.do"
														modelAttribute="companyVo" id="company-form">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Parent Company <span
																class="stars">*</span></label>
															<div class="col-sm-9">
																<select style="background-color: #fff !important"
																	name="parentId" id="parent_company"
																	onchange="getActiveCompany()" class="form-control ">
																	<option value="" label="-select parent-" />
																	<c:forEach items="${branches }" var="b">
																		<option value="${b.branchId }"
																			label="${b.branchName }" />
																	</c:forEach>
																</select>
															</div>
														</div>
														<input type="hidden" name="companyId" id="companyId">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Name<span
																class="stars">*</span>
															</label>
															<div class="col-sm-9">
																<input type="text" id="company_name" name="companyName"
																	class="form-control paste">
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Address </label>
															<div class="col-sm-9">
																<textarea type="text" id="company-address"
																	style="resize: none;" class="form-control paste"></textarea>
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Account Name </label>
															<div class="col-sm-9">
																<input type="text" id="company-acc-name"
																	class="form-control paste">
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Account Number </label>
															<div class="col-sm-9">
																<input type="text" id="company-acc-nnum"
																	class="form-control paste">
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Bank </label>
															<div class="col-sm-9">
																<input type="text" id="company-bank"
																	class="form-control paste">
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Bank Branch </label>
															<div class="col-sm-9">
																<input type="text" id="company-bank-brnch"
																	class="form-control paste">
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Phone </label>
															<div class="col-sm-9">
																<input type="text" id="company-phone"
																	class="form-control numbersonly">
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Email </label>
															<div class="col-sm-9">
																<input type="text" id="company-email" name="email"
																	class="form-control ">
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Period Starts
																From<span class="stars">*</span>
															</label>
															<div class="col-sm-9">
																<div class='input-group date cmp_fr' id="datepicker">
																	<input type="text" readonly="readonly"
																		name="periodStart" id="company-period-start"
																		style="background-color: #ffffff !important;"
																		data-date-format="DD/MM/YYYY" class="form-control">
																	<span class="input-group-addon"><span
																		class="glyphicon glyphicon-calendar"></span> </span>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Period Ends On<span
																class="stars">*</span>
															</label>
															<div class="col-sm-9">
																<div class='input-group date cmp_fr' id="datepickerem">
																	<input type="text" readonly="readonly" name="periodEnd"
																		id="company-period-end"
																		style="background-color: #ffffff !important;"
																		data-date-format="DD/MM/YYYY" class="form-control">
																	<span class="input-group-addon"><span
																		class="glyphicon glyphicon-calendar"></span> </span>
																</div>
															</div>

														</div>


														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Sales Tax Number
															</label>
															<div class="col-sm-9">
																<input type="text" id="company-sls-tx"
																	class="form-control paste">
															</div>
														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Currency<span
																class="stars">*</span>
															</label>
															<div class="col-sm-9">
																<select class="form-control" name="currencyId"
																	id="currencyOptions">
																	<option value="" label="-select currency-" />
																	<c:forEach items="${currencies }" var="cu">
																		<option value="${cu.baseCurrencyId }"
																			label="${cu.baseCurrency }" />
																	</c:forEach>
																</select>

															</div>
														</div>

														<div class="form-group wid-80">
															<!-- <button type="button" class="btn btn-primary erp-btn">New</button> -->
															<button type="submit" class="btn btn-primary erp-btn">Save</button>
														</div>
														<div class=" trash-div">
															<span class="trash" onclick="confirmDelete('company')"
																style="display: none;" id="delet-company-ico"><i
																class="fa fa-trash-o"></i></span>
														</div>
													</form:form>
												</div>
											</div>
											<div class="col-md-6">
												<div id="panelBodys" style="padding: 2% 2% 2% 2%;">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl"
														id="basic-datatable">
														<thead>
															<tr>
																<th width="25%">Parent</th>
																<th width="25%">Company Name</th>
																<th width="20%">Period From</th>
																<th width="20%">Period To</th>
																<th width="5%">Status</th>
																<th width="5%">Is Default</th>

															</tr>
														</thead>
														<tbody>
															<c:forEach items="${companies }" var="c">
																<tr>
																	<td>${c.parent }</td>
																	<td>${c.companyName }</td>
																	<td>${c.periodStart }</td>
																	<td>${c.periodEnd }</td>
																	<td>${c.status }</td>
																	<c:choose>
																		<c:when test="${c.status == 'Active'}">
																			<c:choose>
																				<c:when test="${c.companyId == companyId }">
																					<td><input type="radio"
																						class="form-control radios" name="is_default"
																						checked="checked" value="${c.companyId }"
																						style="border-color: black;"></td>
																				</c:when>
																				<c:otherwise>
																					<td><input type="radio"
																						class="form-control radios" name="is_default"
																						value="${c.companyId }"
																						style="border-color: black;"></td>
																				</c:otherwise>
																			</c:choose>
																		</c:when>
																		<c:otherwise>
																			<td></td>
																		</c:otherwise>
																	</c:choose>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Currency</div>
										<div class="panel-body">
											<form action="#" id="curncy-frm">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Currency<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<select id="base-currency-list" name="baseCurrency"
															onchange="getThisCurrencyDetails()" class="form-control">
															<option value="" label="-select currency-" />
															<c:forEach items="${currencies }" var="cu">
																<option value="${cu.baseCurrencyId }"
																	label="${cu.baseCurrency }" />
															</c:forEach>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Exchange
														Rate<span class="stars">*</span>
													</label>
													<div class="col-sm-4 from-revalid">
														<input type="text" name="rate" id="exchnge-rte"
															class="form-control double ">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Date<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="hidden" name="today" id="currency-wef-hidn">
														<div class='input-group date from-revalid'
															id="datepickers">
															<input type="text" data-date-format="DD/MM/YYYY"
																id="currency-wef" readonly="readonly" name="curncyDate"
																style="background-color: #ffffff !important;"
																class="form-control"> <span
																class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>
												</div>

												<div class="form-group wid-80">
													<button type="submit" onclick="saveCurrency()"
														class="btn btn-primary erp-btn">Save</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>

							<c:if test="${ companyId !=0 }">
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Taxes</div>
											<div class="panel-body">
												<div class="col-sm-12" id="tax_tbl">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl"
														id="basic-datatabless">
														<thead>
															<tr>
																<th width="25%">Tax</th>
																<th width="25%">Rate(%)</th>
																<th width="10%">Edit</th>
																<th width="10%">Delete</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${taxes }" var="t">
																<tr>
																	<td>${t.taxName }</td>
																	<td>${t.rate }</td>
																	<td><a
																		onclick="editTaxValues('${t.id}','${t.taxName }','${t.rate }')"><i
																			class="fa fa-pencil"></i></a></td>
																	<td><a onclick="deleteTax('${t.id}')"><i
																			class="fa fa-times"></i></a></td>

																</tr>
															</c:forEach>

														</tbody>
													</table>
												</div>
												<form action="#" id="tax_frm">
													<input type="hidden" id="tax_id" />
													<div class="form-group" style="padding-top: 2%">
														<label for="inputPassword3" class="col-sm-3 control-label">Tax<span
															class="stars">*</span>
														</label>
														<div class="col-sm-4">
															<input type="text" class="form-control paste"
																id="tax_name" name="tax_name">
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Rate
															(%)<span class="stars">*</span>
														</label>
														<div class="col-sm-4">
															<input type="text" class="form-control paste double"
																id="tax_rate" name="tax_rate">
														</div>
													</div>
													<div class="form-group wid-80">
														<button type="reset" class="btn btn-primary erp-btn">New</button>
														<button type="submit" onclick="saveTax()"
															class="btn btn-primary erp-btn">Save</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Charges</div>
											<div class="panel-body">
												<div class="col-sm-12" id="chrg_tbl">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl"
														id="basic-datatables">
														<thead>
															<tr>
																<th width="25%">Charge</th>
																<th width="10%">Edit</th>
																<th width="10%">Delete</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${charges }" var="t">
																<tr>
																	<td>${t.name }</td>
																	<td><a
																		onclick="editChargeValues('${t.chargeId}','${t.name }')"><i
																			class="fa fa-pencil"></i></a></td>
																	<td><a onclick="deleteCharge('${t.chargeId}')"><i
																			class="fa fa-times"></i></a></td>

																</tr>
															</c:forEach>

														</tbody>
													</table>
												</div>
												<form action="#" id="chrg_frrm">
													<input type="hidden" id="charge_id">
													<div class="form-group" style="padding-top: 2%">
														<label for="inputPassword3" class="col-sm-3 control-label">Charge<span
															class="stars">*</span>
														</label>
														<div class="col-sm-4">
															<input type="text" id="chrg_name" name="charge_name"
																class="form-control paste">
														</div>
													</div>
													<div class="form-group wid-80">
														<button type="button" onclick="clearChargeFrm()"
															class="btn btn-primary erp-btn">New</button>
														<button type="submit" onclick="saveCharge()"
															class="btn btn-primary erp-btn">Save</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Number Property</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Property<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<select class="form-control" id="nmbr_prpty_itms"
															onchange="getNumberProperty(this.value)">
															<option label="-select property-" value="" />
															<option label="BILL" value="BILL" />
															<option label="BILL_PAYMENT" value="BILL_PAYMENT" />
															<option label="CONTRA" value="CONTRA" />
															<option label="CUSTOMER" value="CUSTOMER" />
															<option label="CUSTOMER_CREDIT_MEMO"
																value="CUSTOMER_CREDIT_MEMO" />
															<option label="CUSTOMER_PAYMENT" value="CUSTOMER_PAYMENT" />
															<option label="CUSTOMER_QUOTE" value="CUSTOMER_QUOTE" />
															<option label="DELIVERY_NOTES" value="DELIVERY_NOTES" />
															<option label="EMPLOYEE" value="EMPLOYEECODE" />
															<option label="INVOICE" value="INVOICE" />
															<option label="JOURNAL" value="JOURNAL" />
															<option label="LEAD" value="LEAD" />
															<option label="LEADS_QUOTE" value="LEADS_QUOTE" />
															<option label="OPPORTUNITIES" value="OPPORTUNITIES" />
															<option label="PAYMENT" value="PAYMENT" />
															<option label="PICK_LIST" value="PICK_LIST" />
															<option label="PRODUCT" value="PRODUCT" />
															<option label="PURCHASE_ORDER" value="PURCHASE_ORDER" />
															<option label="PURCHASE_RETURN" value="PURCHASE_RETURN" />
															<option label="RECEIPT" value="RECEIPT" />
															<option label="RECEIVING_NOTES" value="RECEIVING_NOTES" />
															<option label="SALES_RETURN" value="SALES_RETURN" />
															<option label="SALES_ORDER" value="SALES_ORDER" />
															<option label="VENDOR" value="VENDOR" />
															<option label="VENDOR_QUOTE" value="VENDOR_QUOTE" />
															<option label="VENDOR_CREDIT_MEMO"
																value="VENDOR_CREDIT_MEMO" />
														</select>
													</div>
												</div>

												<div class="form-group" id="ifProduct" style="display: none">
													<label for="inputPassword3" class="col-sm-3 control-label">Stock
														Group<span class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<select class="form-control" id="nmbr_prpty_ctgrs"
															onchange="getNumberPropertyForProduct(this.value)">
															<option label="-select stock group-" value="" />
															<c:forEach items="${stockGroups }" var="s">
																<option value="${s.groupName }" label="${s.groupName }" />
															</c:forEach>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Prefix<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" onblur="showPreview()"
															id="numbr_prprty_prefix" class="form-control paste">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Starts
														From<span class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" id="numbr_prprty_numbr"
															onblur="showPreview()" class="form-control numbersonly">
													</div>
												</div>
												<input type="hidden" id="numbr_prprty_id"> <input
													type="hidden" id="numbr_prprty_numbr_hid">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Preview
													</label>
													<div class="col-sm-4">
														<label class="" id="numbr_prprty_preview"></label>
													</div>
												</div>

												<div class="form-group wid-80">
													<button type="button" onclick="saveNumberProperty()"
														class="btn btn-primary erp-btn">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Payment Methods</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Available
														Methods </label>
													<div class="col-sm-4" id="avl_pymnt_mthds">
														<c:forEach items="${paymentMethods }" var="p">
															<label class="badge"><span
																onclick="editPaymentMethod('${p.id}','${p.name } ')">
																	${p.name } </span>&nbsp;&nbsp;<span><i
																	onclick="deletePaymentMethod('${p.id}')"
																	class="fa fa-times"></i></span> </label>
														</c:forEach>
													</div>
												</div>
												<form action="#" id="pymnt_mthd_frm">
													<input type="hidden" id="pymnt_mthd_id">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Method<span
															class="stars">*</span>
														</label>
														<div class="col-sm-4">
															<input type="text" name="name" id="pymnt_mthd_name"
																class="form-control paste">
														</div>
													</div>
													<div class="form-group wid-80">
														<button type="button" onclick="clearPaymentMethodFrm()"
															class="btn btn-primary erp-btn">New</button>
														<button type="submit" onclick="savePaymentMethod()"
															class="btn btn-primary erp-btn">Save</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
								<div class="row" id="pymntTrmsSection">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Payment Terms</div>
											<div class="panel-body tbl_scrl_bdy">

												<!-- PAYMENT TERMS -->

												<table cellpadding="0" cellspacing="0" border="0"
													class="table table-striped table-bordered erp-tbl"
													id="basic-datatablesss">
													<thead>
														<tr>
															<th width="30%">Payment Term</th>
															<th width="30%">Edit</th>
															<th width="10%">Delete</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="p" items="${paymentTerms }">
															<tr>
																<td>${p.name }</td>
																<td><i onclick="addPaymentTerm('${p.id}')"
																	class="fa fa-pencil"></i></td>
																<td><i onclick="deletePaymentTerms('${p.id}')"
																	class="fa fa-times"></i></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>

												<div class="form-group wid-80">
													<button type="button" onClick="addPaymentTerm('0')"
														class="btn btn-primary erp-btn">New</button>
													<!-- <button type="button" class="btn btn-primary erp-btn">Save</button> -->
												</div>
												<div id="addPymntTermDiv"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="row" id="deliveryTrmsSection">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Delivery Terms</div>
											<div class="panel-body tbl_scrl_bdy">

												<!-- PAYMENT TERMS -->

												<table cellpadding="0" cellspacing="0" border="0"
													class="table table-striped table-bordered erp-tbl"
													id="basic-datatablesss">
													<thead>
														<tr>
															<th width="30%">Delivery Term</th>
															<th width="30%">Edit</th>
															<th width="10%">Delete</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="p" items="${deliveryTerms }">
															<tr>
																<td>${p.method }</td>
																<td><i
																	onclick="editDeliveryTerm('${p.deliveryMethodId}','${p.method }')"
																	class="fa fa-pencil"></i></td>
																<td><i
																	onclick="deleteDelvryTerm('${p.deliveryMethodId}')"
																	class="fa fa-times"></i></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
												<form action="#" id="dlvry_term_frm">
													<input type="hidden" id="dlvry_term_id">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Delivery
															Term<span class="stars">*</span>
														</label>
														<div class="col-sm-4">
															<input type="text" name="method" id="dlvry_term_name"
																class="form-control paste">
														</div>
													</div>
													<div class="form-group wid-80">
														<button type="button" onclick="clearDeliveryTerm()"
															class="btn btn-primary erp-btn">New</button>
														<button type="submit" onclick="saveDeliveryTerm()"
															class="btn btn-primary erp-btn">Save</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</c:if>
							<%-- <div class="row">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Delivery Methods</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Method<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" class="form-control paste">
													</div>
												</div>
												<div class="form-group wid-80">
													<button type="button" class="btn btn-primary erp-btn">New</button>
													<button type="button" class="btn btn-primary erp-btn">Save</button>
												</div>
												<div class=" trash-div">
													<span class="trash"><i class="fa fa-trash-o"></i></span>
												</div>
											</div>
										</div>
									</div>
								</div>

							
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Opportunity Stages</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Stage<span
													class="stars">*</span>
												</label>
												<div class="col-sm-4">
													<input type="text" class="form-control paste">
												</div>
											</div>

											<div class="form-group wid-80">
												<button type="button" class="btn btn-primary erp-btn">New</button>
												<button type="button" class="btn btn-primary erp-btn">Save</button>
											</div>
											<div class=" trash-div">
												<span class="trash"><i class="fa fa-trash-o"></i></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Inventory
											Withdrawal Type</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Withdrawal
													Type<span class="stars">*</span>
												</label>
												<div class="col-sm-4">
													<input type="text" class="form-control paste">
												</div>
											</div>
											<div class="form-group wid-80">
												<button type="button" class="btn btn-primary erp-btn">New</button>
												<button type="button" class="btn btn-primary erp-btn">Save</button>
											</div>
											<div class=" trash-div">
												<span class="trash"><i class="fa fa-trash-o"></i></span>
											</div>
										</div>
									</div>
								</div>
							</div> --%>
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>

	<div class="notification-panl successMsg" style="display: none;">
		<div class="notfctn-cntnnt">Action completed successfully..</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
	<%-- </c:if> --%>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>

	<div id="confirm-delete" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">Do you really want to delete this
						item?</div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<input type="hidden" id="delete-item">
							<button type="button" onclick="cancel()" class="btn btn-default">No</button>
							<button type="button" onclick="deleteItem()"
								class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>

	<div id="errror-msg" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Alert</h4>
					</div>
					<div class="modal-body"></div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="closeError()"
								class="btn btn-default">ok</button>
							<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- </div> -->
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<%-- <script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script> --%>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<!-- DateTime Picker -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/erp/js/erp-validation.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script type="text/javascript">
		$(window).scroll(function() {
			if ($(this).scrollTop()) {
				$('.back-to-top:hidden').stop(true, true).fadeIn();
			} else {
				$('.back-to-top').stop(true, true).fadeOut();
			}
		});

		$(".back-to-top").click(function() {
			$("html, body").animate({
				scrollTop : 0
			}, 500);
		});
	</script>
</body>
</html>