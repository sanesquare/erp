<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width="30%">Insurance Title</th>
			<th width="30%">Employee Name</th>
			<th width="10%">Employee Share</th>
			<th width="10%">Organisation Share</th>
			<th width="10%">Edit</th>
			<th width="10%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${insuranceList}" var="insurance">
			<tr>
				<td><a href="ViewInsurance.do?otp=${insurance.insuranceId}">${insurance.insuranceTitle}</a></td>
				<td><a href="ViewInsurance.do?otp=${insurance.insuranceId}">${insurance.employee}</a></td>
				<td><a href="ViewInsurance.do?otp=${insurance.insuranceId}">${insurance.employeeShare}</a></td>
				<td><a href="ViewInsurance.do?otp=${insurance.insuranceId}">${insurance.organizationShare}</a></td>
				<td><a href="EditInsurance.do?otp=${insurance.insuranceId}"><i
						class="fa fa-edit sm"></i> </a></td>
				<td><a href="#"
					onclick="deleteInsurance(${insurance.insuranceId})"><i
						class="fa fa-close ss"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<a href="AddInsurance.do">
	<button type="button" class="btn btn-primary">Add New
		Insurance</button>
</a>

<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

