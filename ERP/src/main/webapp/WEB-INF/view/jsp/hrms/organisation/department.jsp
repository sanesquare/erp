<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">


<head>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>SNOGOL HRM</title>

	<meta name="description" content="">
	<meta name="author" content="Akshay Kumar">

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap/bootstrap.css" /> 

	<!-- Calendar Styling  -->
    <link rel="stylesheet" href="assets/css/plugins/calendar/calendar.css" />
    
    <!-- Fonts  -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>
    
    <!-- Base Styling  -->
    <link rel="stylesheet" href="assets/css/app/app.v1.css" />

    <link href="assets/css/RegalCalendar.css" rel="stylesheet" type="text/css" />
</head>
<body data-ng-app>
        <div class="warper container-fluid">
            <div class="page-header"><h1>Departments </h1></div>
            
            <div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg" >
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
            
            
            <div class="row">
           <div class="col-md-12">
           
          <div class="panel panel-default">
                    <div class="panel-heading">Departments </div>
                    <div class="panel-body">
                    
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
                            <thead>
                               <tr>
                                    <th width="28%">Department Name</th>
                                    <th width="28%">Departments Head</th>
                                    <th width="24%">Parent Department</th>
                                    <c:if test="${ rolesMap.roles['Departments'].update}">
                                     <th width="10%">Edit</th>
                                     </c:if>
                                     <c:if test="${ rolesMap.roles['Departments'].delete}">
                                      <th width="10%">Delete</th>
                                      </c:if>
                                    
                                  </tr>
                            </thead>
                            <tbody>
                             <c:forEach items="${departments}" var="department">
                              <tr> 
                                    <td>${department.name}</td>
                                    <c:choose>
                                    <c:when test="${ rolesMap.roles['Departments'].view}">
                                    <td><a href="viewDepartment.do?pid=${department.id}">${department.departmentHeadName}</a></td>
                                    </c:when>
                                    <c:otherwise>
                                    <td>${department.departmentHeadName}</td>
                                    </c:otherwise>
                                    </c:choose>
                                    
                                    <td>${department.parentDepartmentName}</td> 
                                    <c:if test="${ rolesMap.roles['Departments'].update}">
                                        <td><a href="editDepartment.do?pid=${department.id}"><i class="fa fa-edit sm"></i> </a></td>
                                        </c:if>
                                        <c:if test="${ rolesMap.roles['Departments'].delete}">
                                    <td><a class="delete_department" onclick="confirmDelete(${department.id})" ><i class="fa fa-close ss"></i> </a></td>
                                    </c:if>
                                </tr>
                              </c:forEach> 
                            </tbody>
                        </table>
                        <c:if test="${ rolesMap.roles['Departments'].add}">
                       <a href="add_department.do"> <button type="button" class="btn btn-primary">Add Department</button></a>
                       </c:if>

                    </div>
                </div>
           </div>
            
            
            </div>
            
      
            
            
        </div>
        <!-- Warper Ends Here (working area) -->

    <!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>


	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="assets/js/app/custom.js" type="text/javascript"></script>
    <script type="text/javascript">
    function confirmDelete(id){
		var result=confirm("Confirm delete?");
		 if(result){
			$(".delete_department").attr("href","deleteDepartment.do?pid="+id);
		} else{
			$(".delete_department").attr("href","#");
		} 
	}
    $(document).ready(function(){
    	$("#dashbordli").removeClass("active");
		$("#organzationli").addClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").removeClass("active");
		$("#reportsli").removeClass("active");
    });
    </script>

    
    
</html>
