<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Insurance</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Insurance</div>
					<div class="panel-body" id="insurance-list">
						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="30%">Insurance Title</th>
									<th width="30%">Employee Name</th>
									<th width="10%">Employee Share</th>
									<th width="10%">Organisation Share</th>
									<th width="10%">Edit</th>
									<th width="10%">Delete</th>

								</tr>
							</thead>
							<tbody>
								<c:forEach items="${insuranceList}" var="insurance">
									<tr>
										<td><a
											href="ViewInsurance.do?otp=${insurance.insuranceId}">${insurance.insuranceTitle}</a></td>
										<td><a
											href="ViewInsurance.do?otp=${insurance.insuranceId}">${insurance.employee}</a></td>
										<td><a
											href="ViewInsurance.do?otp=${insurance.insuranceId}">${insurance.employeeShare}</a></td>
										<td><a
											href="ViewInsurance.do?otp=${insurance.insuranceId}">${insurance.organizationShare}</a></td>
										<td><a
											href="EditInsurance.do?otp=${insurance.insuranceId}"><i
												class="fa fa-edit sm"></i> </a></td>
										<td><a href="#"
											onclick="deleteInsurance(${insurance.insuranceId})"><i
												class="fa fa-close ss"></i></a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<a href="AddInsurance.do">
							<button type="button" class="btn btn-primary">Add New
								Insurance</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#dashbordli").removeClass("active");
		$("#organzationli").removeClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").addClass("active");
		$("#reportsli").removeClass("active");
	});
	</script>
</body>
</html>
