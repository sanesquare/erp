<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">


<head>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>SNOGOL HRM</title>

	<meta name="description" content="">
	<meta name="author" content="Akshay Kumar">

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap/bootstrap.css" /> 

	<!-- Calendar Styling  -->
    <link rel="stylesheet" href="assets/css/plugins/calendar/calendar.css" />
    
    <!-- Fonts  -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>
    
    <!-- Base Styling  -->
    <link rel="stylesheet" href="assets/css/app/app.v1.css" />

    <link href="assets/css/RegalCalendar.css" rel="stylesheet" type="text/css" />
</head>
<body data-ng-app>

	<!-- Aside Ends-->
	<!-- Header Ends -->
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Complaints</h1>
		</div>


<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg" >
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>


		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Complaints</div>
					<div class="panel-body">

						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="20%">Complaint From</th>
									<th width="20%">Title</th>
									<th width="20%">Complaint Date</th>
									<th width="20%">Edit</th>
									<th width="20%">Delete</th>

								</tr>
							</thead>
							<tbody>
								<c:forEach items="${complaints}" var="complaint">
                            
                              <tr> 
                                    <td>${complaint.complaintFromEmpCode}</td>
                                    <td></td>
                                     <td>${complaint.complaintDate}</td>  
                                   <td><a href="editComplaints.do?pid=${complaint.id}"><i class="fa fa-edit sm"></i> </a></td>
                                   <td><a class="delete_complaints" onclick="confirmDelete(${complaint.id})" ><i class="fa fa-close ss"></i> </a></td>
                                </tr>
                            </c:forEach>   
							</tbody>
						</table>
						<a href="add_complaints.do">
							<button type="button" class="btn btn-primary">Add New
								Complaints</button>
						</a>

					</div>
				</div>
			</div>


		</div>




	</div>
        <!-- Warper Ends Here (working area) -->
  
    
    <!-- Content Block Ends Here (right box)-->
    
    
   <!-- JQuery v1.9.1 -->
	<script src="assets/js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
    
    <!-- Bootstrap -->
    <script src="assets/js/bootstrap/bootstrap.min.js"></script>
    
    <!-- NanoScroll -->
    <script src="assets/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
    
    
	<!-- Data Table -->
    <script src="assets/js/plugins/datatables/jquery.dataTables.js"></script>
    <script src="assets/js/plugins/datatables/DT_bootstrap.js"></script>
    <script src="assets/js/plugins/datatables/jquery.dataTables-conf.js"></script>
    
    
    <!-- Custom JQuery -->
	<script src="assets/js/app/custom.js" type="text/javascript"></script>
    <script type="text/javascript">
    function confirmDelete(id){
		var result=confirm("Confirm delete?");
		 if(result){
			$(".delete_complaints").attr("href","deleteComplaints.do?pid="+id);
		} else{
			$(".delete_complaints").attr("href","#");
		} 
	}
    $(document).ready(function(){
    	$("#dashbordli").removeClass("active");
		$("#organzationli").removeClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").addClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").removeClass("active");
		$("#reportsli").removeClass("active");
    });
    </script>
</html>
