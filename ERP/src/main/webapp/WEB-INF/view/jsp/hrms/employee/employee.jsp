<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Employees</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>



		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Employees</div>
					<div class="panel-body">
						<c:if test="${!empty employees }">
							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered" id="basic-datatable">
								<thead>
									<tr>
										<th width="20%">Employee Code</th>
										<th width="30%">Name</th>
										<th width="20%">Designation</th>
										<th width="10%">Department</th>
										<c:if test="${ rolesMap.roles['Employee Status Update'].view}">
											<th width="10%">Status</th>
										</c:if>
										<c:if test="${ rolesMap.roles['Employees'].update}">
											<th width="5%">Edit</th>
										</c:if>
										<c:if test="${ rolesMap.roles['Employees'].delete}">
											<th width="5%">Delete</th>
										</c:if>

									</tr>
								</thead>
								<tbody>
									<c:forEach items="${employees}" var="e">
										<tr>
											<c:choose>
												<c:when test="${ rolesMap.roles['Employees'].view}">
													<td><a href="viewEmployee.do?eid=${e.employeeId }">${e.employeeCode }</a></td>
												</c:when>
												<c:otherwise>
													<td>${e.employeeCode }</td>
												</c:otherwise>
											</c:choose>

											<td>${e.employeeName }</td>
											<td>${e.designation }</td>
											<td>${e.employeeDepartment }</td>
											<c:if
												test="${ rolesMap.roles['Employee Status Update'].view}">
												<td>${e.status }</td>
											</c:if>
											<c:if test="${ rolesMap.roles['Employees'].update}">
												<td><a href="editEmployee.do?eid=${e.employeeId }"><i
														class="fa fa-edit sm"></i> </a></td>
											</c:if>
											<c:if test="${ rolesMap.roles['Employees'].delete}">
												<%-- <td><a href="deleteEmployee?eid=${e.employeeId }"><i
														class="fa fa-close ss"></i></a></td> --%>
												<td><a class="delete_employee"
													onclick="confirmDelete(${e.employeeId })"><i
														class="fa fa-close ss "></i></a></td>
											</c:if>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
						<c:if test="${ rolesMap.roles['Employees'].add}">
							<a href="addEmployee.do">
								<button type="button" class="btn btn-primary">Add New
									Employee</button>
							</a>
						</c:if>
						<!-- 	<a href="#">
							<button type="button" class="btn btn-primary show3">Export
								To Excel</button>
						</a> -->
					</div>
				</div>
			</div>


		</div>



		<div id="pop3" class="simplePopup">
			<div class="col-md-12">
				<div class="form-group">
					<h1 class="pop_hd">Select Branch</h1>


					<div class="col-md-12">
						<form:form action="exportToexcel.do" method="POST"
							commandName="employeeVo" id="exportEmployee">
							<div class="form-group">
								<!-- <label for="inputPassword3"
																class="col-sm-3 control-label">Branches
															</label> -->
								<select name="employeeBranchId"
									class="form-control chosen-select">
									<option value="" class="disabled">-Select Branch-</option>
									<c:forEach items="${branches }" var="b">
										<option value="${b.branchId }">${b.branchName }</option>
									</c:forEach>
								</select>
							</div>
							<div class="form-group">
								<div class="col-sm-9">
									<button type="submit" class="btn btn-info">Save</button>
								</div>
							</div>
						</form:form>
					</div>
				</div>


			</div>


		</div>
	</div>
	<!-- Warper Ends Here (working area) -->


	<!-- Content Block Ends Here (right box)-->


	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>

	

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>


	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		function confirmDelete(id) {
			var result = confirm("Are you sure to delete?");
			if (result) {
				$(".delete_employee").attr("href","deleteEmployee.do?eid=" + id);
			} else {
				$(".delete_employee").attr("href", "#");
			}
		}

		window.onload = function() {
			$('#basic-datatable').dataTable( {
				 "bSort": false
			  } );
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		}

		$('.show3').click(function() {
			$('#pop3').simplePopup();
		});
	</script>
	
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
</body>
</html>