<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Attendance</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>




		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Attendance</div>
					<div class="panel-body">
						<c:if test="${!empty attendance }">
							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered" id="basic-datatable">
								<thead>
									<tr>
										<th width="20%">Employee</th>
										<th width="20%">Date</th>
										<th width="20%">Sign in Time</th>
										<th width="20%">Sign Out Time</th>
										<c:if test="${ rolesMap.roles['Attendance'].update}">
											<th width="5%">Edit</th>
										</c:if>
										<c:if test="${ rolesMap.roles['Attendance'].delete}">
											<th width="5%">Delete</th>
										</c:if>

									</tr>
								</thead>
								<tbody>
									<c:forEach items="${attendance }" var="a">
										<tr>
											<c:choose>
												<c:when test="${ rolesMap.roles['Attendance'].view}">
													<td><a href="viewAttendance.do?id=${a.attendanceId }">
															${a.employee }</a></td>
												</c:when>
												<c:otherwise>
													<td>${a.employee }</td>
												</c:otherwise>
											</c:choose>

											<td>${a.date }</td>
											<td>${a.signInTime }</td>
											<td>${a. signOutTime}</td>
											<c:if test="${ rolesMap.roles['Attendance'].update}">
												<td><a href="editAttendance.do?id=${a.attendanceId }"><i
														class="fa fa-edit sm"></i></a></td>
											</c:if>
											<c:if test="${ rolesMap.roles['Attendance'].delete}">
												<td><a class="delete_att" onclick="confirmDelete(${a.attendanceId })" ><i
														class="fa fa-close ss"></i></a></td>
											</c:if>
										</tr>
									</c:forEach>

								</tbody>
							</table>
						</c:if>
						<c:if test="${ rolesMap.roles['Attendance'].add}">
							<a href="add_attendance.do">
								<button type="button" class="btn btn-primary">Add New
									Attendance</button>
							</a>
						</c:if>

					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">Upload Report</div>
					<form:form action="readAttendanceFromReport.do" method="POST"
						commandName="biometricAttendanceVo" enctype="multipart/form-data">

						<div class="panel-body">

							<div class="form-group">

								<div class="col-sm-8">

									<input type="file" name="attendanceReports" multiple
										accept="application/vnd.ms-excel" id="attendance_reoprt"
										class="btn btn-info">
								</div>
								<div class="col-sm-4">
									<button type="submit" class="btn btn-primary" id="attnd_doc_btn"
										style="float: right;">Upload</button>
								</div>
							</div>

						</div>
					</form:form>
				</div>
			</div>


		</div>




	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>


	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script type="text/javascript">
	function confirmDelete(id) {
		var result = confirm("Are you sure to delete?");
		if (result) {
			$(".delete_att").attr("href","deleteAttendance.do?id=" + id);
		} else {
			$(".delete_att").attr("href", "#");
		}
	}
		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").addClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
			
			$("#attnd_doc_btn").attr("disabled","true");
		});
		$("#attendance_reoprt").on("change",function(){
			$("#attnd_doc_btn").removeAttr("disabled");
		});
	</script>
</body>
</html>