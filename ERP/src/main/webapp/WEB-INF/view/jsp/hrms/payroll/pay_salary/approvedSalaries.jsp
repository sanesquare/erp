<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
#msg_success {
	display: none;
}

#msg_error {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Pay Salary</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="success_msg" id="msg_success">
					<span id="msgs_icn"><i class="fa fa-check"></i></span>Success.
				</div>
				<div class="error_msg" id="msg_error">
					<span id="msge_icn"><i class="fa fa-times"></i></span>Error.
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="panel-body">
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Company<span
							class="stars">*</span>
						</label>
						<div class="col-sm-9">
							<select class="form-control"
								onchange="filterHrmsSalary(this.value)" id="selectedCompany">
								<c:forEach items="${companies }" var="c">
									<option value="${c.companyId }">${c.companyName }</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Pay Salary</div>
					<div class="panel-body" id="panelBody">

						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>

									<th width="20%">Date</th>
									<th width="20%">Branch</th>
									<th width="20%">Salary Type</th>
									<th width="10%">Status</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${salaries }" var="salary">
									<tr>
										<td><a
											href="viewSalaryHrms.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }&compId=${salary.companyId}">${salary.date }</a></td>
										<td><a
											href="viewSalaryHrms.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }&compId=${salary.companyId}"><c:forEach
													items="${salary.branches }" var="branch">${branch } </a>
								</c:forEach>

								</td>
								<td><a
									href="viewSalaryHrms.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }">${salary.salaryTypeType }</a></td>
								<td><a
									href="viewSalaryHrms.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }">${salary.status }</a></td>


								</tr>
								</c:forEach>


							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>


		<div id="load-erp">
			<div id="poCntnt"></div>
		</div>


	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>

	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script type="text/javascript">
		function confirmDelete(id) {
			var result = confirm("Are you sure to delete?");
			if (result) {
				$(".delete_pay").attr("href", "deletPaySalary?id=" + id);
			} else {
				$(".delete_pay").attr("href", "#");
			}
		}
	</script>
</body>
</html>