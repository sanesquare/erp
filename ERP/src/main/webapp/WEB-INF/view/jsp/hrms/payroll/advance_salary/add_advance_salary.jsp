<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Advance Salary</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="AdvanceSalary.do" class="btn btn-info"
					style="float: right;"><i class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form action="AddAdvanceSalary.do" method="post"
								commandName="advanceSalaryVo" id="advanceSalaryForm">
								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Advance Salary Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Employee
															Name <span class="stars">*</span>
														</label>
														<div class="col-sm-8 col-xs-10">
															<form:input path="employee" class="form-control"
																id="employeeSl"
																style="background-color: #ffffff !important;" />
															<form:hidden path="employeeCode" id="employeeCode" />
														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div>
													</div>
													<div id="pop2" class="simplePopup empl-popup-gen"></div>
													<div id="pop1" class="simplePopup"></div>
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label"> Forward
															Application To <span class="stars">*</span>
														</label>
														<div class="col-sm-9 col-xs-10">
															<form:select path="forwaders" multiple="true"
																id="forwaders"
																class="form-control chosen-select forwaders"
																data-placeholder="Select Superior">
																<form:options items="${superiorList}" itemValue="value"
																	itemLabel="label" />
															</form:select>
														</div>
														<!-- <div class="col-sm-1 col-xs-1">
															<a href="#" class="show1"><i class="fa fa-user"></i></a>
														</div> -->
													</div>
													<!-- <div id="pop1" class="simplePopup superior-popup-gen"></div> -->
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Title <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input path="advanceSalaryTitle"
																class="form-control" placeholder="Title" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Amount <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input path="advanceAmount" class="form-control"
																onblur="findIstallmentsAndAmount(this.value)"
																id="loan_amount" placeholder="Amount" />
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label"> Date <span
															class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date advance-salary-date"
																id="datepicker">
																<form:input path="requestedDate" class="form-control"
																	data-date-format="DD/MM/YYYY" readonly="true"
																	style="background-color: #ffffff !important;" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Advance Salary Repayment</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Monthly
															Repayment Amount <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input path="repaymentAmount" class="form-control"
																id="rpymnt_amnt"
																onblur="findNoOfInstallments(this.value)"
																placeholder="Monthly Repayment Amount" />
														</div>
													</div>
													<c:if test="${! empty advanceSalaryVo.balanceAmount }">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Balance Amount </label>
															<div class="col-sm-9">
																<label>${advanceSalaryVo.balanceAmount }</label>
															</div>
														</div>
													</c:if>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">No.
															of Installments<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input id="no_of_instllmnt" path="noOfInstallments"
																onblur="findRepaymentAmount(this.value)"
																class="form-control" placeholder="No. of Installments" />
														</div>
													</div>
													<c:if
														test="${! empty advanceSalaryVo.pendingInstallments }">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">No. of Pending
																Installments </label>
															<div class="col-sm-9">
																<label>${advanceSalaryVo.pendingInstallments }</label>
															</div>
														</div>
													</c:if>
													<div class="form-group">
														<label class="col-sm-3 control-label">Repayment
															Start Date <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<div class="input-group date advance-salary-date"
																id="datepickers">
																<form:input path="repaymentStartDate"
																	class="form-control" data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Advance Salary Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea path="description"
																class="wysihtml form-control"
																placeholder="Enter text ..." style="height: 150px" />
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" class="form-control height" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${advanceSalaryVo.recordedBy}</label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${created_on} </label>
														</div>
													</div>
													<c:if test="${!advanceSalaryVo.isView }">
														<div class="form-group">
															<div class="col-sm-offset-3 col-sm-9">
																<form:hidden path="advanceSalaryId" />
																<button type="submit" class="btn btn-info">${buttonLabel}</button>
															</div>
														</div>
													</c:if>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#employeeSl').bind('click', function() {
				showPop();
			});

			$('.show2').click(function() {
				if ($("#employeeCode").val() != "") {
					fillEmployeePopup($("#employeeCode").val());
					$('#pop2').simplePopup();
				}
			});

			/* $('.forwaders').bind('change', function() {
				$("#superiorId").val($(".forwaders").val());
			}); */

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>
