<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="20%">Title</th>
									<th width="20%">Policy Type</th>
									<th width="20%">Branch</th>
									<th width="20%">Department</th>
									<c:if test="${ rolesMap.roles['Organization Policies'].update}">
									<th width="10%">Edit</th></c:if>
									<c:if test="${ rolesMap.roles['Organization Policies'].delete}">
									<th width="10%">Delete</th></c:if>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${policies}" var="policy">
									<tr>
									<c:if test="${ rolesMap.roles['Organization Policies'].view}">
										<td><a
											href="ViewOrganizationPolicy.do?opi=${policy.org_policy_id}">${policy.title}</a></td></c:if>
											<c:if test="${ rolesMap.roles['Organization Policies'].view}">
										<td><a
											href="ViewOrganizationPolicy.do?opi=${policy.org_policy_id}">${policy.policyType}</a></td></c:if>
											<c:if test="${ rolesMap.roles['Organization Policies'].view}">
										<td><a
											href="ViewOrganizationPolicy.do?opi=${policy.org_policy_id}">${policy.branches}</a></td></c:if>
											<c:if test="${ rolesMap.roles['Organization Policies'].view}">
										<td><a
											href="ViewOrganizationPolicy.do?opi=${policy.org_policy_id}">${policy.departments}</a></td></c:if>
											<c:if test="${ rolesMap.roles['Organization Policies'].update}">
										<td><a
											href="EditOrganizationPolicy.do?opi=${policy.org_policy_id}"><i
												class="fa fa-edit sm"></i> </a></td></c:if>
												<c:if test="${ rolesMap.roles['Organization Policies'].delete}">
										<td><a href="#"
											onclick="organizationPolicyDelConfirm(${policy.org_policy_id})"><i
												class="fa fa-close ss"></i></a></td></c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<c:if test="${ rolesMap.roles['Organization Policies'].add}">
						<a href="AddOrganizationPolicy.do">
							<button type="button" class="btn btn-primary">Add New
								Policy</button>
						</a>
						</c:if>

<!-- Data Table -->
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
