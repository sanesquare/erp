<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/app/app.v1.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
/* #add_ws_success {
	display: none;
}

#add_ws_error {
	display: none;
} */
#add_wsPro_success {
	display: none;
}

#add_wsPro_error {
	display: none;
}
</style>
</head>
<body>

	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Daily Work Report</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="worksheets.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${! empty success_msg}">
					<div class="success_msg" id="add_ws_success">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${! empty error_msg}">
					<div class="error_msg" id="add_ws_error">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>


		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>


				<div class="tab-content">

					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">

						<div class="panel-body">

							<div class="row">
								<form:form action="saveWorksheetsInfo" method="POST"
									commandName="workSheetVo" id="add_sheet_form">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Daily Work Report
													Information</div>
												<div class="panel-body">

													<form:hidden path="workSheetId" id="work_sheet_id" />
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">
															Employee Name <span class="stars">*</span>
														</label>
														<div class="col-sm-8 col-xs-10">
															<form:input path="employee" class="form-control"
																readonly="true"
																style="background-color: #ffffff !important;" />
															<form:hidden path="employeeCode"
																id="workSheetEmployeeCode" />
														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div>
													</div>
													<div id="pop2" class="simplePopup empl-popup-gen"></div>



													<div class="form-group">
														<label class="col-sm-3 control-label"> Date <span
															class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date sheetDate" id="datepicker">
																<form:input path="date" id="work_sheet_date" type="text"
																	class="form-control" readonly="true"
																	data-date-format="DD/MM/YYYY"
																	style="background-color: #ffffff !important;" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>
													</div>

													<!-- <div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a id="sbmt_ws_basic_info" onClick="validateSheetInfo()"
																class="btn btn-info">Save</a>
																
														</div>
													</div> -->
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">

															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>

												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Tasks</div>
												<div class="panel-body">
													<table class="table no-margn" id="add_ws_table">
														<thead>
															<tr>
																<th>Start Time</th>
																<th>End Time</th>
																<th>Task</th>
																<th>Delete</th>

															</tr>
														</thead>
														<tbody>
															<c:forEach items="${workSheetVo.tasksVo }" var="tasks">
																<tr>
																	<td>${tasks.startTime }</td>
																	<td>${tasks.endTime }</td>
																	<td
																		onclick="getWorkSheetEditDetails(${tasks.workSheetTasksId })"><a>${tasks.task }</a></td>
																	<td><a
																		href="deleteWorkSheetTask?tid=${tasks.workSheetTasksId }&wid=${workSheetVo.workSheetId}"><i
																			class="fa fa-close ss"></i></a></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
													<br> <br> <input type="hidden"
														id="ws_hidden_task" />
													<div class="form-group">
														<label class="col-sm-3 control-label">Start Time <span
															class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date" id="timepicker">
																<form:input path="startTime" id="work_sheet_start"
																	type="text" readonly="true" class="form-control"
																	style="background-color: #ffffff !important;" />
																<span class="input-group-addon"><span
																	class="glyphicon glyphicon-time"></span> </span>
															</div>

														</div>
													</div>



													<div class="form-group">
														<label class="col-sm-3 control-label">End Time <span
															class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date" id="timepickers">
																<form:input path="endTime" id="work_sheet_end"
																	type="text" readonly="true" class="form-control"
																	style="background-color: #ffffff !important;" />
																<span class="input-group-addon"><span
																	class="glyphicon glyphicon-time"></span> </span>
															</div>

														</div>
													</div>
													<%-- <div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">
															Project </label>
														<div class="col-sm-9">
															<form:select path="projectId" class="form-control"
																id="work_sheet_project">
																<form:option value="" label="Select Project"></form:option>
																<form:options items="${projects }" itemValue="projectId"
																	itemLabel="projectTitle" />
															</form:select>
														</div>
													</div> --%>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Task <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input type="text" class="form-control"
																id="work_sheet_task" path="task" />
														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a id="sbmt_work_pjct_task"
																onClick="validateSheetProject()" class="btn btn-info">Save</a>
														</div>
													</div>
													<div class="col-md-12">
														<div class="success_msg" id="add_wsPro_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
														</div>
														<div class="error_msg" id="add_wsPro_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
														</div>

													</div>
												</div>
											</div>
										</div>


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Daily Work Report
													Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea path="description" id="work_sheet_desc"
																class=" form-control" style="height: 150px" />

														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a id="sbmt_sheet_desc"
																onclick="validateSheetDescription()"
																class="btn btn-info">Save</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Daily Work Report
													Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="addInfo" id="work_sheet_addInfo"
																class="form-control height" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label path="createdBy" for="inputPassword3"
																class="col-sm-9 control-label">
																${workSheetVo.createdBy} </label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">
																${workSheetVo.createdOn} </label>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="sbmt_sheet_add_info" class="btn btn-info"
																onclick="validateSheetAddInfo()">Save</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<%-- </form:form> --%>
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Daily Work Report Reporting
													To</div>
												<div class="panel-body" style="overflow: visible;">
													<%-- <form:form action="saveWorksheetsInfo" method="POST" 
													commandName="workSheetVo"> --%>
													<form:hidden path="workSheetId" id="report_worksheet_id" />
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">
															Reporting To</label>
														<%-- <form:select id="drop_three" multiple="true"
															path="reportingCode" class="form-control"> --%>
														<form:select path="reportingCode" multiple="true"
															class="form-control chosen-select">
															<c:forEach items="${employees }" var="e">
																<c:choose>
																	<c:when test="${e.name ==null }"></c:when>

																	<c:otherwise>
																		<form:option value="${e.employeeCode }"
																			label="${e.name }" />
																	</c:otherwise>
																</c:choose>
															</c:forEach>

														</form:select>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info"
																id="worksheet_save_report">Save Report</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<form:form action="uploadWorksheetDocs.do"
								commandName="workSheetVo" method="POST"
								enctype="multipart/form-data">
								<form:hidden path="workSheetId" />
								<c:if test="${ rolesMap.roles['Worksheet Documents'].add}">
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Upload Documents</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Upload
														Documents </label>
													<div class="col-sm-9">
														<input type="file" name="uploadSheetDocs"
															id="worksheet_docs" class="btn btn-info" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-16 col-sm-12">
														<button type="submit" id="worksheet_docs_btn"
															class="btn btn-info">Save</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</c:if>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">
											<table class="table no-margn">
												<thead>
													<tr>
														<th>Document Name</th>
														<c:if
															test="${ rolesMap.roles['Worksheet Documents'].view}">
															<th>view</th>
														</c:if>
														<c:if
															test="${ rolesMap.roles['Worksheet Documents'].delete}">
															<th>Delete</th>
														</c:if>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${workSheetVo.documentsVo }" var="docs">
														<tr>
															<td><i class="fa fa-file-o"></i>${docs.documentName }</td>
															<c:if
																test="${ rolesMap.roles['Worksheet Documents'].view}">
																<td><a
																	href="viewWorksheetDoc.do?durl=${docs.documentUrl }"><i
																		class="fa fa-file-text-o sm"></i></a></td>
															</c:if>
															<c:if
																test="${ rolesMap.roles['Worksheet Documents'].delete}">
																<td><a
																	href="deleteWorksheetDoc.do?durl=${docs.documentUrl }"><i
																		class="fa fa-close ss"></i></a></td>
															</c:if>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Simple Map</div>
									<div class="panel-body">
										<div id="simpleMap" style="height: 500px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js'/>"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js'/>"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js'/>"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js'/>"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js'/>"></script>

	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/docs/js/bootstrap-multiselect.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#worksheet_docs_btn").attr("disabled", "true");

				$('#employeeSl').bind('click', function() {
					getAllBranches();
					$('#pop1').simplePopup();
				});

				$('.show2').click(function() {
					fillEmployeePopup($("#employeeCode").val());
					if ($("#employeeCode").val() != "") {
						$('#pop2').simplePopup();
					}
				});

				$('.show1').click(function() {
					$('#pop1').simplePopup();
				});

				$('.show2').click(function() {
					$('#pop2').simplePopup();
				});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").addClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");

			if ($("#work_sheet_id").val() != '') {
				enableWorksheetButtons();
			} else {
				disableWorksheetButtons();
			}
			$('#drop_three').multiselect({
				includeSelectAllOption : true
			});
		});

		$("#worksheet_docs").on("change", function() {
			if ($("#work_sheet_id").val() != '')
				$("#worksheet_docs_btn").removeAttr("disabled");
		});

		function enableWorksheetButtons() {
			$("#sbmt_work_pjct_task").removeAttr("disabled");
			$("#sbmt_sheet_desc").removeAttr("disabled");
			$("#sbmt_sheet_add_info").removeAttr("disabled");
			$("#worksheet_save_report").removeAttr("disabled");
		}
		function disableWorksheetButtons() {
			$("#sbmt_work_pjct_task").attr("disabled", "true");
			$("#sbmt_sheet_desc").attr("disabled", "true");
			$("#sbmt_sheet_add_info").attr("disabled", "true");
			$("#worksheet_save_report").attr("disabled" , "true");
		}
	</script>
</body>
</html>