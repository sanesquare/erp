<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width="20%">Title</th>
			<th width="20%">Employee Name</th>
			<th width="20%">Amount</th>
			<th width="20%">Approval Status</th>
			<c:if test="${ rolesMap.roles['Overtimes'].update}">
				<th width="10%">Edit</th>
			</c:if>
			<c:if test="${ rolesMap.roles['Overtimes'].delete}">
				<th width="10%">Delete</th>
			</c:if>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${overtimeList}" var="overtime">
			<tr>
				<c:choose>
					<c:when test="${ rolesMap.roles['Overtimes'].view}">
						<td><a href="ViewOvertime.do?otp=${overtime.overtimeId}">${overtime.overtimeTitle}</a></td>
						<td><c:choose>
								<c:when test="${overtime.recordedBy==thisUser}">
									<i class="fa fa-arrow-up up-arrow"></i>
								</c:when>
								<c:otherwise>
									<i class="fa fa-arrow-down down-arrow"></i>
								</c:otherwise>
							</c:choose><a href="ViewOvertime.do?otp=${overtime.overtimeId}">${overtime.employee}</a></td>
						<td><a href="ViewOvertime.do?otp=${overtime.overtimeId}">${overtime.overtimeAmount}</a></td>
						<td><a href="ViewOvertime.do?otp=${overtime.overtimeId}">${overtime.status}</a></td>
					</c:when>
					<c:otherwise>
						<td>${overtime.overtimeTitle}</td>
						<td><c:choose>
								<c:when test="${overtime.recordedBy==thisUser}">
									<i class="fa fa-arrow-up up-arrow"></i>
								</c:when>
								<c:otherwise>
									<i class="fa fa-arrow-down down-arrow"></i>
								</c:otherwise>
							</c:choose>${overtime.employee}</td>
						<td>${overtime.overtimeAmount}</td>
						<td>${overtime.status}</td>
					</c:otherwise>
				</c:choose>

				<c:if test="${ rolesMap.roles['Overtimes'].update}">
					<td><a href="EditOvertime.do?otp=${overtime.overtimeId}"><i
							class="fa fa-edit sm"></i> </a></td>
				</c:if>
				<c:if test="${ rolesMap.roles['Overtimes'].delete}">
					<td><a href="#"
						onclick="overtimeDelConfirm(${overtime.overtimeId})"><i
							class="fa fa-close ss"></i></a></td>
				</c:if>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${ rolesMap.roles['Overtimes'].add}">
	<a href="AddOvertime.do">
		<button type="button" class="btn btn-primary">Add New
			Overtime</button>
	</a>
</c:if>

<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

