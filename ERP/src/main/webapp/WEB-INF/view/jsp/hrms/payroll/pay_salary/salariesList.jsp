<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>

			<th width="20%">Date</th>
			<th width="20%">Branch</th>
			<th width="20%">Salary Type</th>
			<th width="10%">Status</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${salaries }" var="salary">
			<tr>
				<td><a
					href="viewSalaryHrms.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }">${salary.date }</a></td>
				<td><a
					href="viewSalaryHrms.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }"><c:forEach
							items="${salary.branches }" var="branch">${branch } </a>
		</c:forEach>

		</td>
		<td><a
			href="viewSalaryHrms.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }">${salary.salaryTypeType }</a></td>
		<td><a
			href="viewSalaryHrms.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }">${salary.status }</a></td>


		</tr>
		</c:forEach>


	</tbody>
</table>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>