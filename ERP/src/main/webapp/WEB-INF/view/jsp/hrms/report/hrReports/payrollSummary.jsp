<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />

<title>Snogol HRMS</title>
</head>
<body style="background: #FFFFFF !important;">
	<h2 class="role_hd">Payroll Summary Report</h2>
	<div class="col-xs-12">

		<div class="row">
			<div class="col-md-6">
				<div class="col-md-12">
					<table class="table table-striped no-margn line">

						<tr>
							<td class="hd_tb" colspan="2"><span class="WhiteHeading">
									Payroll Summary</span></td>
						</tr>

						<tr>
							<td>Total Salaries Paid</td>
							<td>3</td>
						</tr>

						<tr>
							<td>Salaries Paid this Year</td>
							<td>5</td>
						</tr>
						<tr>
							<td>Average Yearly Salaries</td>
							<td>4</td>
						</tr>

						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>

						<tr>
							<th colspan="2"><strong>Top Station</strong></th>
						</tr>



						<tr>
							<th colspan="2"><strong>Top Department</strong></th>
						</tr>

						<tr>
							<td>Administration</td>
							<td>4</td>
						</tr>

						<tr>
							<th colspan="2"><strong>Top Designation</strong></th>
						</tr>

						<tr>
							<td>Developer</td>
							<td>4</td>
						</tr>


					</table>

				</div>




			</div>


			<div class="col-md-6">
				<div class="col-md-12">
					<table class="table table-striped no-margn line">
						<tbody>
							<tr>
								<td class="hd_tb" colspan="2"><span class="WhiteHeading">Yearly
										Employees Salaries</span></td>
							</tr>
							<tr>
								<td>2015</td>
								<td>3</td>
							</tr>

							<tr>
								<td align="right"><strong>Total</strong></td>
								<td>5</td>
							</tr>


						</tbody>
					</table>

				</div>

				<div class="col-md-12">&nbsp;</div>

				<div class="col-md-12">
					<table class="table table-striped no-margn line">
						<tbody>
							<tr>
								<td class="hd_tb" colspan="2"><span class="WhiteHeading">Station
										Wise Salaries</span></td>
							</tr>
							<tr>
								<td>Head Office</td>
								<td>3</td>
							</tr>

							<tr>
								<td align="right"><strong>Total</strong></td>
								<td>5</td>
							</tr>



						</tbody>
					</table>

				</div>

				<div class="col-md-12">&nbsp;</div>

				<div class="col-md-12">
					<table class="table table-striped no-margn line">
						<tbody>
							<tr>
								<td class="hd_tb" colspan="2"><span class="WhiteHeading">
										Department Wise Salaries</span></td>
							</tr>
							<tr>
								<td>Administration</td>
								<td>3</td>
							</tr>

							<tr>
								<td>development</td>
								<td>5</td>
							</tr>


							<tr>
								<td align="right"><strong>Total</strong></td>
								<td>5</td>
							</tr>


						</tbody>
					</table>

				</div>


				<div class="col-md-12">&nbsp;</div>

				<div class="col-md-12">
					<table class="table table-striped no-margn line">
						<tbody>
							<tr>
								<td class="hd_tb" colspan="2"><span class="WhiteHeading">Designation
										Wise Salaries</span></td>
							</tr>
							<tr>
								<td>Administrator</td>
								<td>3</td>
							</tr>

							<tr>
								<td>Developer</td>
								<td>5</td>
							</tr>
							<tr>
								<td>marketing executive</td>
								<td>4</td>
							</tr>

							<tr>
								<td align="right"><strong>Total</strong></td>
								<td>5</td>
							</tr>


						</tbody>
					</table>

				</div>



			</div>





		</div>




		<div class="form-group" style="margin-top: 25px;">
			<div class="col-sm-offset-4 col-sm-6">
				<!-- <button type="submit" class="btn btn-info">
					Export to Pdf &nbsp;<i class="fa fa-file-pdf-o"></i>
				</button>
				<button type="submit" class="btn btn-info">
					Export to exel &nbsp; <i class="fa fa-file-excel-o"></i>
				</button>
				<button type="submit" class="btn btn-info">
					Export to CSV &nbsp; <i class="fa fa-file-text-o"></i>
				</button>
				<button type="submit" class="btn btn-info">
					Export to HTML &nbsp; <i class="fa fa-file-code-o"></i>
				</button> -->
			</div>
		</div>

	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>
	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<!-- Responsive tabs -->
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"
		type="text/javascript"></script>

	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</html>