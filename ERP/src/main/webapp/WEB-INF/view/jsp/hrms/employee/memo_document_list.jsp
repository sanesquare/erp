<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table class="table no-margn">
	<tbody>
		<c:choose>
			<c:when test="${not empty memoDocList.documents}">
				<thead>
					<tr>
						<th>#</th>
						<th>Document Name</th>
						<th>view</th>
						<th>Delete</th>
					</tr>
				</thead>
				<c:forEach items="${memoDocList.documents}" var="memoDoc"
					varStatus="status">
					<tr>
						<td>${status.count}</td>
						<td><i class="fa fa-file-o"></i> ${memoDoc.documentName}</td>
						<td><i class="fa fa-file-text-o sm"></i></td>
						<td><a href="#"
							onclick="deleteThisMemoDocument(${memoDocList.memoId}, ${memoDoc.memoDocId},'${memoDoc.documentUrl}')"><i
								class="fa fa-close ss"></i></a></td>
					</tr>
				</c:forEach>
			</c:when>

			<c:otherwise>
				<span>No Documents Available </span>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
