<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="pr-tax-rule-tbl">
	<thead>
		<tr>
			<th width="10%">S#</th>
			<th width="10%">Salary From</th>
			<th width="10%">Salary To</th>
			<th width="10%">Tax Percentage</th>
			<th width="10%">Exempted Tax Amount</th>
			<th width="10%">Additional Tax Amount</th>
			<th width="10%">Individual</th>
			<th width="10%"></th>
		</tr>
	</thead>
	<tbody id="pr-tax-rl-rw">
		<c:choose>
			<c:when test="${not empty taxRules}">
				<c:forEach items="${taxRules}" var="taxRule" varStatus="row">
					<tr>
						<td>${row.count}</td>
						<td><input type="text" value="${taxRule.salaryFrom}"
							class="form-control salaryFrom${row.count}" name="salaryFrom"
							onkeyup="setExenptedAmont(${row.count})" /></td>
						<td><input type="text" value="${taxRule.salaryTo}"
							class="form-control salaryTo${row.count}" name="salaryTo"
							onkeyup="setAdditionalAmount(${row.count})" /></td>
						<td><input type="text" value="${taxRule.taxPercentage}"
							class="form-control taxPercentage${row.count}"
							name="taxPercentage" /></td>
						<td><input type="text" value="${taxRule.exemptedAmount}"
							class="form-control exemptedAmount${row.count}"
							name="exemptedAmount" /></td>
						<td><input type="text" value="${taxRule.additionalAmount}"
							class="form-control additionalAmount${row.count}"
							name="additionalAmount" /></td>
						<td><select class="form-control individual${row.count}"
							name="individual">
								<option value="Male"
									${taxRule.individual == 'Male' ? 'selected' : ''}>Male</option>
								<option value="Female"
									${taxRule.individual == 'Female' ? 'selected' : ''}>Female</option>
								<option value="SeniorCitizen"
									${taxRule.individual == 'SeniorCitizen' ? 'selected' : ''}>Senior
									Citizen</option>
								<option value="SuperSenior"
									${taxRule.individual == 'SuperSenior' ? 'selected' : ''}>Super
									Senior</option>
						</select></td>
						<td><input type="hidden" id="taxId${row.count}"
							name="taxRuleId" value="${taxRule.taxRuleId}" /><input
							type="button" value="Update"
							onclick="updateTaxRule(${row.count})" id="addButton1"
							class="btn btn-info btn-xs"> <c:if
								test="${fn:length(taxRules) eq row.count}">
												&nbsp;<input type="button" value="Add Row"
									onclick="addTRNewRow()" id="addButton1"
									class="btn btn-danger btn-xs">
							</c:if></td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr>
					<td>1</td>
					<td><input type="text" class="form-control salaryFrom1"
						value="${taxRule.salaryFrom}" name="salaryFrom"
						onkeyup="setExenptedAmont(1)" /></td>
					<td><input type="text" class="form-control salaryTo1"
						value="${taxRule.salaryTo}" name="salaryTo"
						onkeyup="setAdditionalAmount(1)" /></td>
					<td><input type="text" class="form-control taxPercentage1"
						value="${taxRule.taxPercentage}" name="taxPercentage" /></td>
					<td><input type="text" class="form-control exemptedAmount1"
						value="${taxRule.exemptedAmount}" name="exemptedAmount" /></td>
					<td><input type="text" class="form-control additionalAmount1"
						value="${taxRule.additionalAmount}" name="additionalAmount" /></td>
					<td><select class="form-control individual1" name="individual">
							<option value="Male"
								${taxRule.individual == 'Male' ? 'selected' : ''}>Male</option>
							<option value="Female"
								${taxRule.individual == 'Female' ? 'selected' : ''}>Female</option>
							<option value="SeniorCitizen"
								${taxRule.individual == 'SeniorCitizen' ? 'selected' : ''}>Senior
								Citizen</option>
							<option value="SuperSenior"
								${taxRule.individual == 'SuperSenior' ? 'selected' : ''}>Super
								Senior</option>
					</select></td>
					<td><input type="hidden" id="taxId1" name="taxRuleId"
						value="0" /><input type="button" value="Save"
						onclick="saveTaxRule()" id="psButtonSave1"
						class="btn btn-info btn-xs"></td>
				</tr>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>

<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
<script src="<c:url value='/resources/assets/js/auto_complete.js' />"></script>
