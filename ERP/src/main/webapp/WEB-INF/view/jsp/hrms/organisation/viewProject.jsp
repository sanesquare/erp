<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>
				<spring:message code="projects.title" />
			</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<a href="projects.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>



		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab"><spring:message
								code="project.view.title" /></a></li>
					<!-- <li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
					<li role="presentation"><a href="#docs" role="tab"
						data-toggle="tab">Project Status</a></li> -->

				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<spring:message code="project.view.info_label" />
											</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3 control-label"><spring:message
															code="project.view.info.proj_title" /></label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${project.projectTitle }</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label"><spring:message
															code="project.view.info.start_date" /></label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${project.projectStartDate }</label>
													</div>
												</div>


											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Project Heads</div>
											<div class="panel-body">
												<c:forEach items="${project.headNames }" var="emp">
													<div class="form-group">
														<div class="col-sm-9">
															<label for="inputEmail3"
																class="col-sm-12 control-label ft">${emp } </label>
														</div>
													</div>
												</c:forEach>



											</div>
										</div>
									</div>


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<spring:message code="project.view.employees" />
											</div>
											<div class="panel-body">
												<c:forEach items="${project.employees }" var="emp">
													<div class="form-group">
														<div class="col-sm-9">
															<label for="inputEmail3"
																class="col-sm-12 control-label ft">${emp } </label>
														</div>
													</div>
												</c:forEach>



											</div>
										</div>
									</div>


									<div class="col-md-12">

										<div class="panel panel-default">
											<div class="panel-heading">
												<spring:message code="project.view.status" />
											</div>
											<div class="panel-body">

												<div class="form-group">

													<div class="col-sm-12">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${project.projectStatus }</label>
													</div>
												</div>


											</div>
										</div>
									</div>


								</div>

								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<spring:message code="project.view.description" />
											</div>
											<div class="panel-body">

												<div class="form-group">

													<div class="col-sm-12">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${project.projectDescription }</label>
													</div>
												</div>


											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<spring:message code="project.view.additional_info.label" />
											</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label"><spring:message
															code="project.view.additional_info.notes" /> </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${project.projectAdditionalInformation }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label"><spring:message
															code="project.view.additional_info.added_by" /></label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft">${project.createdBy }
														</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label"><spring:message
															code="project.view.additional_info.added_on" /></label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft">${project.createdOn }
														</label>
													</div>
												</div>


												<div id="pop3" class="simplePopup">
													<h1 class="pop_hd">System Administrator</h1>
													<div class="row">

														<div class="col-md-3">
															<img src="assets/images/avtar/user.png" / class="pop_img">
														</div>
														<div class="col-md-9">

															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-4 control-label">E-mail </label>
																<div class="col-sm-8">
																	<label for="inputPassword3"
																		class="col-sm-6 control-label ft">jsafs@.com</label>
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-4 control-label">Designation </label>
																<div class="col-sm-8">
																	<label for="inputPassword3"
																		class="col-sm-6 control-label ft">2545895678</label>
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-4 control-label">Department </label>
																<div class="col-sm-8">
																	<label for="inputPassword3"
																		class="col-sm-6 control-label ft">dsafdsfdsf</label>
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-4 control-label">Station </label>
																<div class="col-sm-8">
																	<label for="inputPassword3"
																		class="col-sm-6 control-label ft">2sdfds8</label>
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-4 control-label">Join Date </label>
																<div class="col-sm-8">
																	<label for="inputPassword3"
																		class="col-sm-6 control-label ft">25-2-2015</label>
																</div>
															</div>



														</div>

													</div>
												</div>


											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<spring:message code="project.view.show_doc_tab.title" />
											</div>
											<div class="panel-body">

												<c:if test="${!empty project.projectDocuments}">
													<table class="table no-margn">
														<thead>
															<tr>
																<th>#</th>
																<th><spring:message
																		code="project.view.show_doc_tab.doc_name" /></th>
																<c:if
																	test="${ rolesMap.roles['Project Documents'].view}">
																	<th><spring:message
																			code="project.view.show_doc_tab.view" /></th>
																</c:if>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${project.projectDocuments }" var="doc"
																varStatus="s">
																<tr>
																	<td>${s.count }</td>
																	<td><i class="fa fa-file-o"></i>${doc.fileName }</td>
																	<c:if
																		test="${ rolesMap.roles['Project Documents'].view}">
																		<td><a
																			href="viewProjectDocument.do?durl=${doc.url }"><i
																				class="fa fa-file-text-o sm"></i></a></td>
																	</c:if>
																</tr>
															</c:forEach>

														</tbody>
													</table>
												</c:if>

											</div>
										</div>
									</div>

								</div>
							</div>



						</div>
					</div>
					<!--<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">

							 <div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Upload Documents</div>
									<div class="panel-body">

										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Upload
												Documents </label>
											<div class="col-sm-9">
												<input type="file" class="btn btn-info" />
											</div>
										</div>


									</div>
								</div>
							</div> -->


					<%-- <div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">

									<c:if test="${!empty project.projectDocuments}">
										<table class="table no-margn">
											<thead>
												<tr>
													<th>#</th>
													<th>Document Name</th>
													<th>view</th>
												</tr>
											</thead>
											<tbody>
											<c:forEach items="${project.projectDocuments }" var="doc">
												<tr>
													<td>${doc.id }</td>
													<td><i class="fa fa-file-o"></i>${doc.name }</td>
													<td><a href="viewProjectDocument?durl=${doc.documentPath }"><i class="fa fa-file-text-o sm"></i></a></td>
												</tr>
											</c:forEach>

											</tbody>
										</table>
										</c:if>

									</div>
								</div>
							</div> 




						</div>
					</div>--%>



					<!-- <div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="docs">
						<div class="panel-body">

							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Project Status</div>
									<div class="panel-body">

										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Project
												Status </label>
											<div class="col-sm-9">

												<select class="form-control">
													<option>One</option>
													<option>One</option>
													<option>One</option>
												</select>

											</div>
										</div>


									</div>
								</div>
							</div>


							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">


										<table class="table no-margn">
											<thead>
												<tr>
													<th>Stasus</th>
													<th>Stasus Updates</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Inactive</td>
													<td>Lorem ipsum dolor sit amet 25-2-2015 22:30 pm</td>

												</tr>

											</tbody>
										</table>


									</div>
								</div>
							</div>




						</div>
					</div> -->

				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->



	</section>
	<!-- Content Block Ends Here (right box)-->


	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/google-maps/googlemap-conf.js' />"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;libraries=weather"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$('.show3').click(function() {
				$('#pop3').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").addClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>