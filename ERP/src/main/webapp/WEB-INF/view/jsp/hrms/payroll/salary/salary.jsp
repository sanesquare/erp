<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#hourly {
	display: none
}

#daily {
	display: none
}
</style>
</head>
<body>
	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Salary</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="salary.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
				</ul>
				<div class="tab-content">
					<form:form action="saveSalary.do" method="POST"
						commandName="salaryVo">
						<input type="hidden" id="emp-joninig_date">
						<div role="tabpanel"
							class="panel panel-default tab-pane tabs-up active" id="all">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Salary Information</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Employee
														Name <span class="stars">*</span>
													</label>
													<div class="col-sm-8 col-xs-10 br-de-emp-list">
														<form:input path="employee" class="form-control"
															placeholder="Search Employee" id="employeeSl" />
														<form:hidden path="employeeCode" id="employeeCode" />
													</div>
													<div class="col-sm-1 col-xs-1">
														<a href="#" class="show2"><i class="fa fa-user"></i></a>
													</div>
												</div>

												<div id="pop1" class="simplePopup"></div>

												<div id="pop2" class="simplePopup empl-popup-gen"></div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														With Effect From <span class="stars">*</span>
													</label>
													<div class="col-sm-9">

														<div class="input-group date" id="datepickers">
															<form:input path="withEffectFrom" class="form-control"
																style="background-color: #ffffff !important;"
																readonly="true" id="salary_witheffect_from"
																data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Salary Type</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Salary Type</label>
													<div class="col-sm-9">
														<form:hidden path="salaryTypeId" id="salry_type_id" />
														<form:select class="form-control" path="salaryTypeId"
															cssStyle="background-color:#fff !important"
															id="salary_type">
															<form:options items="${salaryTypes }" itemLabel="type"
																itemValue="id" />
														</form:select>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row" id="monthly">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Salary</div>
											<div class="panel-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="80%">Salary</th>
															<th width="10%">Monthly</th>
															<th width="10%">annually</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><strong>Gross Salary</strong></td>
															<td><input type="text" class="form-control double" onblur="calculateAnualSal()"
																id="gross_salary" placeholder=""></td>
															<td><input type="text" class="form-control double"
																onblur="calculateMonthlySalary()"
																id="gross_salary_annually" placeholder=""></td>
														</tr>
														<tr>
															<td><strong>Breakups</strong>

																<table class="table no-margn" id="Table1">
																	<thead>
																		<tr>
																			<th width="5%">#</th>
																			<th width="75%">Salary Item</th>
																			<th width="20%">Amount</th>
																		</tr>
																	</thead>
																	<tbody>

																	</tbody>
																</table></td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>

														<tr>
															<td><strong>Tax Deduction</strong></td>
															<td><input type="text" class="form-control "
																style="background-color: #ffffff ! important"
																id="monthly_tax_deduction" placeholder=""></td>
															<td><input type="text" class="form-control"
																style="background-color: #ffffff ! important"
																id="annual_tax_deduction" placeholder=""></td>
														</tr>

														<tr>
															<td><strong>Estimated Monthly Salary</strong></td>
															<td><input type="text" class="form-control"
																style="background-color: #ffffff ! important"
																id="monthly_estimated_salary" placeholder=""></td>
															<td><input type="text" class="form-control"
																style="background-color: #ffffff ! important"
																id="annual_estimated_salary" placeholder=""></td>
														</tr>


													</tbody>
												</table>
												<div class="form-group">
													<div class="col-sm-9">
														<button type="button" onclick="saveSalary()"
															class="btn btn-info">Save</button>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>


								<div class="row" id="daily">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Daily Wages Information</div>
											<div class="panel-body">


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Daily
														Wages Rate</label>
													<div class="col-sm-9">
														<form:input path="dailyWagesVo.salary"
															style="background-color: #ffffff !important;"
															class="form-control" id="salary_daily_rate" />
													</div>
												</div>




												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Tax
														Deduction</label>
													<div class="col-sm-8 col-xs-11">
														<form:input path="dailyWagesVo.tax" class="form-control"
															id="inputPassword3" />
													</div>

													<div class="col-sm-1 col-xs-1">
														<label>%</label>
													</div>

												</div>

												<div class="form-group">
													<div class="col-sm-offset-6 col-sm-6">
														<button type="button" id="daily_wage_save_btn"
															class="btn btn-info">Save Daily Wages</button>
													</div>
												</div>

											</div>
										</div>
									</div>


								</div>


								<div class="row" id="hourly">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Hourly Wages Information</div>
											<div class="panel-body">


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Regular
														Hourly Rate</label>
													<div class="col-sm-9">
														<form:input path="hourlyWageVo.regularHourSalary"
															style="background-color: #ffffff !important;"
															class="form-control" id="salary_regular_hoyr_wage" />
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Overtime
														Hourly Rate</label>
													<div class="col-sm-9">
														<form:input path="hourlyWageVo.overTimerHourSalary"
															style="background-color: #ffffff !important;"
															class="form-control" id="salary_overtime_hoyr_wage" />
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Tax
														Deduction</label>
													<div class="col-sm-8 col-xs-11">
														<form:input path="hourlyWageVo.tax" class="form-control"
															id="inputPassword3" />
													</div>

													<div class="col-sm-1 col-xs-1">
														<label>%</label>
													</div>

												</div>

												<div class="form-group">
													<div class="col-sm-offset-6 col-sm-6">
														<button type="button" id="hourly_wage_save_btn"
															class="btn btn-info">Save Wages</button>
													</div>
												</div>

											</div>
										</div>
									</div>


								</div>


							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/bootstrap-typeahead.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/assets/js/auto_complete.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script type="text/javascript">
		window.onload = function() {

			$("#salary_type").attr("disabled", "true");

			getEmployeeDetails();

			var date = convertDate(new Date());
			$("#salary_witheffect_from").val(date);

			$("#salary_type").on("change", function() {
				var type = $("#salary_type").val();
				if (type == 1) {
					$("#daily").css("display", "none");
					$("#hourly").css("display", "none");
					$("#monthly").css("display", "block");
				} else if (type == 2) {
					$("#daily").css("display", "block");
					$("#hourly").css("display", "none");
					$("#monthly").css("display", "none");
				} else if (type == 3) {
					$("#daily").css("display", "none");
					$("#hourly").css("display", "block");
					$("#monthly").css("display", "none");
				} else {
					$("#daily").css("display", "none");
					$("#hourly").css("display", "none");
					$("#monthly").css("display", "none");
				}
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
		}

		$('#employeeSl').bind('click', function() {
			showPop();
		});

		$('.show2').click(function() {
			fillEmployeePopup($("#employeeCode").val());
			if ($("#employeeCode").val() != "") {
				$('#pop2').simplePopup();
			}
		});
		function addRoww() {
			var rowCount = $('#Table1 tr').length + 1;
			var amount = $("#extraAmount" + parseInt($('#Table1 tr').length))
					.val();
			var item = $("#extraItem" + parseInt($('#Table1 tr').length)).val();
			if (item != "" && amount != "") {
				$("#Table1 tr:last-child td:last-child ").remove();
				$("#Table1 tr:last-child ").append("<td></td>");
				$("#Table1")
						.append(
								"<tr><td>"
										+ parseInt(rowCount)
										+ "</td> <td><input type='text' name='item"
										+ parseInt(rowCount)
										+ "' id='extraItem"
										+ parseInt(rowCount)
										+ "' onclick='getItems()'  class=' typeaheadd form-control' /></td> <td><input type='text' name='amount"
										+ parseInt(rowCount)
										+ "' id='extraAmount"
										+ parseInt(rowCount)
										+ "' class='form-control itemTitle"
										+ parseInt(rowCount)
										+ "' /></td><td><input type='button' value='Add Row ' onclick='addRoww()' id='addButton"
										+ parseInt(rowCount) + "'></td></tr>");
			}
		}

		function calculateMonthlySalary() {
			var annualSalary = $("#gross_salary_annually").val();
			if (annualSalary != "") {
				$("#gross_salary").val(
						(parseFloat(annualSalary) / 12).toFixed(2));
				calculatePayrollItems($("#gross_salary").val(), $(
						"#employeeCode").val());
			} else {
				$("#gross_salary").val();
			}
		}

		function getItems() {
			$.ajax({
				type : "GET",
				url : "getExtraPayslipsItems.do",
				cache : false,
				async : true,
				success : function(response) {
					data = response;
					hii();
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
		}
		function hii() {
			$('.typeaheadd').typeahead({
				source : data,
				itemSelected : displayResult
			});
		}
		function displayResult(item, val, text) {
			getExtraItemCalculation(text, $("#gross_salary").val());
		}
		/* function getAmount(e){
			console.log(e.value);
		} */

		$("#daily_wage_save_btn")
				.click(
						function() {
							if (process($("#emp-joninig_date").val()) > process($(
									"#salary_witheffect_from").val())) {
								alert("With effect from date should be after joining date..");
							} else {
								$("#daily_wage_save_btn")
										.attr("type", "submit");
							}
						});

		$("#hourly_wage_save_btn")
				.click(
						function() {
							if (process($("#emp-joninig_date").val()) > process($(
									"#salary_witheffect_from").val())) {
								alert("With effect from date should be after joining date..");
							} else {
								$("#hourly_wage_save_btn").attr("type",
										"submit");
							}
						});
	</script>
</body>
</html>