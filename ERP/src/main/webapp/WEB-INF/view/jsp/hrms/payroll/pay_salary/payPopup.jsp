<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="panel panel-default erp-info" style="border-radius: 4px">
	<div class="panel-heading panel-inf">
		Pay Salary <span style="float: right;" class="back-btn"><i
			class="fa fa-times"></i></span>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<%-- <form action="" id="ledger-group-form"> --%>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Select
						Account </label>
					<div class="col-sm-9">
						<select class="form-control" id="ledgerSel">
							<option value="">-Select Account-</option>
							<c:forEach items="${accounts }" var="a">
								<option value="${a.ledgerId }" label="${a.ledgerName }" />
							</c:forEach>
						</select>
					</div>
				</div>
				<input type="hidden" id="salaryId">

				<div class="form-group">
					<button type="button" onclick="paySalary()" style="float: right;"
						class="btn btn-primary erp-btn">Pay</button>
				</div>
				<%-- </form> --%>
			</div>
		</div>

	</div>
</div>
<div class="notification-panl successMsg" style="display: none;">
	<div class="notfctn-cntnnt">${success_msg }</div>
	<span id="close-msg" onclick="closeMessage()"><i
		class="fa fa-times"></i></span>
</div>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>