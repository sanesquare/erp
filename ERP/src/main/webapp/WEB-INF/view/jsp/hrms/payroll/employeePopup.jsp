<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="col-md-12">
	<div class="form-group">
		<div class="simplePopupClose">X</div>
		<h1 class="pop_hd">${popUpInfo.name}</h1>
		<div class="row">
			<div class="col-md-3">
				<img src="${popUpInfo.picture}" class="pop_img">
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">E-mail
					</label>
					<div class="col-sm-9">
						<label for="inputPassword3" class="col-sm-6 control-label ft">${popUpInfo.email}</label>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Designation
					</label>
					<div class="col-sm-9">
						<label for="inputPassword3" class="col-sm-6 control-label ft">${popUpInfo.designation}</label>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Department
					</label>
					<div class="col-sm-9">
						<label for="inputPassword3" class="col-sm-6 control-label ft">${popUpInfo.department}</label>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Branch
					</label>
					<div class="col-sm-9">
						<label for="inputPassword3" class="col-sm-6 control-label ft">${popUpInfo.branch}</label>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Join
						Date </label>
					<div class="col-sm-9">
						<label for="inputPassword3" class="col-sm-6 control-label ft">${popUpInfo.joinDate}</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$(".simplePopupClose").bind('click', function() {
			closePopup();
		});
	});
</script>
