<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
#add_branch_success {
	display: none;
}

#add_branch_error {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Branches</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<a href="branches.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="success_msg" id="add_branch_success">
					<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
				</div>
				<div class="error_msg" id="add_branch_error">
					<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>

				</ul>

				<form:form action="saveBranch.do" method="POST" commandName="branchVo"
					enctype="multipart/form-data">

					<form:hidden id="add_branch_id" path="branchId"></form:hidden>
					<div class="tab-content">
						<div role="tabpanel"
							class="panel panel-default tab-pane tabs-up active" id="all">
							<div class="panel-body">

								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Branch Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Branch
															Type <span class="stars">*</span></label>
														<div class="col-sm-9 col-xs-10">
															<form:select path="branchType" class="form-control"
																id="add_branch_type">
																<form:option value="" label="Select Branch Type"></form:option>
																<c:forEach items="${branchTypes}" var="branchType">
																	<form:option value="${branchType.branchTypeName }"
																		label="${branchType.branchTypeName }"></form:option>
																</c:forEach>
															</form:select>
														</div>

														<!-- <div class="col-sm-1 col-xs-1">
														<a href="#" class="show1"><i
															class="fa fa-plus-square-o pop_icon"></i></a>
													</div> -->
													</div>


													<%-- <div id="pop1" class="simplePopup">
													<h1 class="pop_hd">Branch Types</h1>
													<div class="row">

														<div class="col-md-12">
															<div class="form-group">

																<div class="col-sm-12">
																	<form:textarea class="form-control height" path="newBranchTypeName"></form:textarea>
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-offset-3 col-sm-9">
																	<button type="submit"  name="saveBranchType" value="saveBranchType" class="btn btn-info flr">Save</button>
																</div>
															</div>
														</div>
													</div>
												</div> --%>

													<div class="form-group">


														<label for="inputPassword3" class="col-sm-3 control-label">Branch
															Name <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="branchName" type="text"
																class="form-control" id="add_branch_name"></form:input>

														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Parent
															Branch</label>
														<div class="col-sm-9">
															<form:select path="parentBranchId" class="form-control"
																id="add_parent_branch">
																<form:option label="No Parent Branch" value=""></form:option>
																<c:forEach items="${branches}" var="branch">
																	<form:option value="${branch.branchId }"
																		label="${branch.branchName }"></form:option>
																</c:forEach>
															</form:select>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Branch
															Start Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class='input-group date' id="datepicker">
																<form:input path="startDate" readonly="true"
																	style="background-color: #ffffff !important;"
																	id="add_branch_strdt" cssClass="form-control"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon glyphicon-calendar"></span> </span>
															</div>
														</div>

													</div>

													<%-- <div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Time
														Zone</label>
													<div class="col-sm-9">
														<form:select path= "timeZoneLocation" class="form-control" id="add_branch_timeZone">
															<form:option value="">Select Time zone</form:option>
															<c:forEach items="${timeZones}" var="timeZone">
															    <form:option value="${timeZone.location}">${timeZone.offSet}</form:option>
															</c:forEach>
														</form:select>
													</div>
												</div> --%>

													<%-- <div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Currency</label>
													<div class="col-sm-9">
														<div class="input-group">
															<span class="input-group-addon" id="crncySgn">$</span>
															<form:select path="currency" class="form-control" id="baseCrncySlct">
															<form:option value="">Select Currency</form:option>
															<c:forEach items="${baseCurrencies}" var="currency">
															    <form:option value="${currency.baseCurrency}">${currency.baseCurrency}</form:option>
															</c:forEach>
														</form:select>
														</div>
													</div>
												</div> --%>

												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Address</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="add_branch_address"
															class="col-sm-3 control-label">Address <span class="stars">*</span> </label>
														<div class="col-sm-9">
															<form:input path="address" type="text"
																class="form-control" id="add_branch_address"
																placeholder="Address"></form:input>
														</div>
													</div>
													<div class="form-group">
														<label for="add_branch_city"
															class="col-sm-3 control-label">City <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="city" type="text" class="form-control"
																id="add_branch_city" placeholder="City"></form:input>
														</div>
													</div>
													<div class="form-group">
														<label for="add_branch_state"
															class="col-sm-3 control-label">State/Province <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="state" type="text" class="form-control"
																id="add_branch_state" placeholder="State/Province"></form:input>
														</div>
													</div>
													<div class="form-group">
														<label for="add_branch_zipCode"
															class="col-sm-3 control-label">Zip Code/Country
															Code <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="zipCode" type="text"
																class="numbersonly form-control" id="add_branch_zipCode"
																placeholder="Zip Code/Country Code"></form:input>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Country <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select path="country" class="form-control"
																id="add_branch_country">
																<form:option value="">Select Country</form:option>
																<c:forEach items="${countries}" var="country">
																	<form:option value="${country.name}">${country.name}</form:option>
																</c:forEach>
															</form:select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Phone
															Number <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="phoneNumber" type="text"
																class="numbersonly form-control"
																id="add_branch_phoneNumber" placeholder="Phone Number "></form:input>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Fax
															Number </label>
														<div class="col-sm-9">
															<form:input path="faxNumber" type="text"
																class="numbersonly form-control"
																id="add_branch_faxNumber" placeholder="Fax Number"></form:input>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">E-mail
														</label>
														<div class="col-sm-9">
															<form:input path="eMail" type="text" class="form-control"
																id="add_branch_eMail" placeholder="E-mail"></form:input>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Website
														</label>
														<div class="col-sm-9">
															<form:input path="webSite" type="text"
																class="form-control" id="add_branch_webSite"
																placeholder="Website"></form:input>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Geographical Location</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Latitude
														</label>
														<div class="col-sm-9">
															<form:input path="latitude" type="text"
																class="form-control" id="add_branch_latitude"
																placeholder="Latitude"></form:input>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Longitude
														</label>
														<div class="col-sm-9">
															<form:input path="longitude" type="text"
																class="form-control" id="add_branch_longitude"
																placeholder="Longitude"></form:input>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" class="form-control height"
																id="add_branch_notes"></form:textarea>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${branchVo.createdBy }</label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${branchVo.createdOn}
															</label>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-6 col-sm-6">
															<a class="btn btn-info" id="submt_branch_info_btn">Save
																Information</a> <a class="btn btn-info"
																id="updt_branch_info_btn">Update Information</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
							id="users">
							<div class="panel-body">

								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Upload Documents</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Upload
													Documents </label>
												<div class="col-sm-9">
													<input type="file" multiple class="btn btn-info"
														id="branch_file_upload" name="branchUploadFiles" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">
											<c:if test="${!empty branchVo.branchDocuments}">
												<table class="table no-margn">
													<thead>
														<tr>
															<th>Document Name</th>
															<th>view</th>
															<th>Delete</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${branchVo.branchDocuments }"
															var="document">
															<tr>
																<td><i class="fa fa-file-o"></i>${document.name }</td>
																<td><a
																	href="viewBranchDocument.do?durl=${document.documentUrl }"><i
																		class="fa fa-file-text-o sm"></i></a></td>
																<td><a
																	href="deleteBranchDocument.do?durl=${document.documentUrl }"><i
																		class="fa fa-close ss"></i></a></td>
															</tr>
														</c:forEach>

													</tbody>
												</table>
											</c:if>

										</div>
									</div>
								</div>
								<!-- </div> -->
								<c:if test="${ rolesMap.roles['Branch Documents'].add}">
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-9">
											<button type="submit" value="uploadDocuments"
												class="btn btn-info" id="sbmt_branch_doc_btn"
												name="uploadDocuments">Upload Documents</button>
										</div>
									</div>
								</c:if>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {

			if ($("#branch_file_upload").val() == '') {
				$("#sbmt_branch_doc_btn").attr('disabled', 'disabled');
			}

			$("#branch_file_upload").on("change", function() {
				if ($("#add_branch_name").val() != '')
					$("#sbmt_branch_doc_btn").removeAttr('disabled');
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			var name = $("#add_branch_name").val();
			$("#sbmt_branch_doc_btn").attr('disabled', 'disabled');
			if (name != '') {
				$("#updt_branch_info_btn").css("display", "block");
				$("#submt_branch_info_btn").css("display", "none");
			} else {
				$("#updt_branch_info_btn").css("display", "none");
				$("#submt_branch_info_btn").css("display", "block");
			}

			$("#dashbordli").removeClass("active");
			$("#organzationli").addClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->


	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>

	<script
		src="<c:url value='/resources/docs/js/bootstrap-3.3.2.min.js' />"></script>
	<script src="<c:url value='/resources/docs/js/prettify.js' />"></script>
	<script
		src="<c:url value='/resources/docs/js/bootstrap-multiselect.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<%-- 	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script> --%>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>

</html>