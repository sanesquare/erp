<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
</head>
<body>
	<div id="fade"></div>
	<div id="modal">
		<img id="loader" src="resources/images/loading.gif" />
	</div>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Organization Policies</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="OrganizationPolicy.do" class="btn btn-info"
					style="float: right;"><i class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<!-- <li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li> -->
					<li role="presentation" class="active"><a href="#users"
						role="tab" data-toggle="tab" id="org-policy-doc-tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<%-- <div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="row">
								<form:form action="AddOrganizationPolicy" method="post"
									commandName="organizationPolicyVo">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Policy Details</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3"
															class="col-sm-3 col-xs-12 control-label">Policy
															Type</label>
														<div class="col-sm-9 col-xs-10">
															<form:select path="policy_type_id" class="form-control">
																<c:forEach items="${policyTypes}" var="policyType">
																	<form:option value="${policyType.policyId}">${policyType.policyType}</form:option>
																</c:forEach>
															</form:select>
														</div>
													</div>
													<!-- <div id="pop1" class="simplePopup">
														<h1 class="pop_hd">General Policy</h1>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label for="inputPassword3"
																		class="col-sm-3 control-label">General Policy
																	</label>
																	<div class="col-sm-9">
																		<textarea class="form-control height"></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<div class="col-sm-offset-3 col-sm-9">
																		<button type="submit" class="btn btn-info">Save</button>
																	</div>
																</div>
															</div>
														</div>
													</div> -->
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Title
														</label>
														<div class="col-sm-9">
															<form:input path="title" class="form-control" id="title"
																placeholder="Title" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">Branch
														</label>
														<div class="col-sm-9">
															<form:select path="branch_id"
																class="form-control chosen-select"
																data-placeholder="Choose Branch">
																<c:forEach items="${branches}" var="branch">
																	<form:option value="${branch.branchId}">${branch.branchName}</form:option>
																</c:forEach>
															</form:select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Department</label>
														<div class="col-sm-9">
															<form:select path="department_id"
																class="form-control chosen-select"
																data-placeholder="Choose Department">
																<c:forEach items="${departments}" var="department">
																	<form:option value="${department.id}">${department.name}</form:option>
																</c:forEach>
															</form:select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Policy Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea path="description"
																class="wysihtml form-control"
																placeholder="Enter text ..."
																style="height: 150px; resize: none;" />
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="information"
																class="form-control height" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">System
																Administrator </label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${created_on}</label>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<form:hidden path="org_policy_id" id="org_policy_id" />
															<button type="submit" class="btn btn-info">${buttonLabel}</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div> --%>
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="users">
						<div class="panel-body">
							<%-- <c:if test="${ rolesMap.roles['Organization Policy Documents'].add}"> --%>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Upload Documents</div>
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Upload
												Documents </label>
											<div class="col-sm-9">
												<input type="file" name="documents" id="org-policy-docs"
													class="btn btn-info" multiple />
												<button type="button" onclick="uploadOrgPolicyDocument()"
													class="btn btn-info org-policy-doc-btn">Upload</button>
												<span class="org-policy-doc-status"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<%-- </c:if> --%>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body" id="org-policy-doc-list">
										<table class="table no-margn">
											<thead>
												<tr>
													<th>#</th>
													<th>Document Name</th>
													<%-- <c:if
														test="${ rolesMap.roles['Organization Policy Documents'].view}"> --%>
														<th>view</th>
													<%-- </c:if>
													<c:if
														test="${ rolesMap.roles['Organization Policy Documents'].delete}"> --%>
														<th>Delete</th>
													<%-- </c:if> --%>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${documents}" var="document"
													varStatus="status">
													<tr>
														<td>${status.count}</td>
														<td><i class="fa fa-file-o"></i>${document.documentName}</td>
														<%-- <c:if
															test="${ rolesMap.roles['Organization Policy Documents'].view}"> --%>
															<td><a href="viewPolicyDocument.do?durl=${document.documentUrl}"><i class="fa fa-file-text-o sm"></i></a></td>
														<%-- </c:if>
														<c:if
															test="${ rolesMap.roles['Organization Policy Documents'].delete}"> --%>
															<td><a href="#"
																onclick="deleteOrganizationPolicyDocument(${document.documentId},'${document.documentUrl}')"><i
																	class="fa fa-close ss"></i></a></td>
														<%-- </c:if> --%>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script type="text/javascript">
		$(document).ready(
				function() {
					$('#employeeSl').bind('click', function() {
						getAllBranches();
						$('#pop1').simplePopup();
					});

					$('.show2').click(function() {
						if ($("#employeeCode").val() != "") {
							fillEmployeePopup($("#employeeCode").val());
							$('#pop2').simplePopup();
						}
					});
					$("#dashbordli").removeClass("active");
					$("#organzationli").removeClass("active");
					$("#recruitmentli").removeClass("active");
					$("#employeeli").removeClass("active");
					$("#timesheetli").removeClass("active");
					$("#payrollli").addClass("active");
					$("#reportsli").removeClass("active");

					$("#chosen-employee").change(
							function() {
								$("#employeePFShare").val('');
								$("#organizationPFShare").val('');
								$("#employeeSl").val(
										$("#chosen-employee option:selected")
												.text());
								$("#employeeCode").val(
										$("#chosen-employee").val());
								getMyPayStructure($("#chosen-employee").val());
								closePopup();
							});
				});
	</script>
</body>
</html>
