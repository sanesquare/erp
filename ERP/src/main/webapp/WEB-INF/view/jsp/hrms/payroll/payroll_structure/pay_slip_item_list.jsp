<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="pay-slip-items-table">
	<thead>
		<tr>
			<th width="5%">S#</th>
			<th width="20%">Title</th>
			<th width="10%">Type</th>
			<th width="5%">Taxable Allowance</th>
			<th width="20%">Calculation</th>
			<th width="10%">With Effect From</th>
			<th width="10%"></th>
		</tr>
	</thead>
	<tbody id="ps-div">
		<c:choose>
			<c:when test="${not empty paySlipItems}">
				<c:forEach items="${paySlipItems}" var="paySlipItem" varStatus="row">
					<tr>
						<td>${row.count}</td>
						<td><input type="text" name="title"
							value="${paySlipItem.title}" id="ps-kw-non-exp"
							class="form-control itemTitle${row.count}" /></td>
						<td><select class="form-control itemType${row.count}"
							name="type">
								<option value="Calculation"
									${paySlipItem.type == 'Calculation' ? 'selected' : ''}>Calculation</option>
								<option value="Fixed"
									${paySlipItem.type == 'Fixed' ? 'selected' : ''}>Fixed</option>
						</select></td>
						<td><select class="form-control itemTax${row.count}"
							name="taxAllowance">
								<option value="true"
									${paySlipItem.taxAllowance == true ? 'selected' : ''}>Yes</option>
								<option value="false"
									${paySlipItem.taxAllowance == false ? 'selected' : ''}>No</option>
						</select></td>
						<td><input type="text" value="${paySlipItem.calculation}"
							class="form-control typeahead itemSyntax${row.count}"
							name="calculation" id="ps-kw-exp" /></td>
						<td><input type="text" name="effectFrom"
							value="${paySlipItem.effectFrom}"
							class="form-control itemFrom${row.count}"
							data-date-format="DD/MM/YYYY" id="dt-ps" /></td>
						<td><input type="hidden" id="itemId${row.count}"
							name="paySlipItemId" value="${paySlipItem.paySlipItemId}" /><input
							type="button" value="Update"
							onclick="updatePaySlip(${row.count})" id="addButton1"
							class="btn btn-info btn-xs"> <c:if
								test="${fn:length(paySlipItems) eq row.count}">
												&nbsp;<input type="button" value="Add Row"
									onclick="addPSNewRow()" id="addButton1"
									class="btn btn-danger btn-xs">
							</c:if></td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr>
					<td>1</td>
					<td><input type="text" name="title" id="ps-kw-non-exp"
						class="form-control itemTitle1" /></td>
					<td><select class="form-control itemType1" name="type">
							<option value="Calculation">Calculation</option>
							<option value="Fixed">Fixed</option>
					</select></td>
					<td><select class="form-control itemTax1" name="taxAllowance">
							<option value="true">Yes</option>
							<option value="false">No</option>
					</select></td>
					<td><input type="text"
						class="form-control typeahead itemSyntax1" name="calculation"
						id="ps-kw-exp" /></td>
					<td><input type="text" name="effectFrom"
						class="form-control itemFrom1" data-date-format="DD/MM/YYYY"
						id="dt-ps" /></td>
					<td><input type="hidden" id="itemId1" name="paySlipItemId"
						value="0" /><input type="button" value="Save"
						onclick="savePaySlipItem()" id="psButtonSave1"
						class="btn btn-info btn-xs"></td>
				</tr>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>

<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
<script src="<c:url value='/resources/assets/js/auto_complete.js' />"></script>
