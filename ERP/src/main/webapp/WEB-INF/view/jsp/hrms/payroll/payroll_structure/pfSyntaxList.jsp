<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="50%">Employee Contribution</th>
			<th width="50%">Employer Contribution</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><input type="text" name="employeeSyntax"
				class="form-control typeahead employeeSyntax"
				value="${profidentFundSyntax.employeeSyntax}" id="ps-kw-exp" /></td>
			<td><input type="text" name="employerSyntax"
				class="form-control typeahead employerSyntax"
				value="${profidentFundSyntax.employerSyntax}" id="ps-kw-exp" /></td>
		</tr>
	</tbody>
</table>
<div class="col-sm-6 col-xs-offset-6">
	<input type="hidden" name="pfSyntaxId" id="pfSyntaxId"
		value="${profidentFundSyntax.pfSyntaxId}" />
	<button type="button" onclick="saveProvidentFundSyntax()"
		class="btn btn-info">Save</button>
</div>

<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
