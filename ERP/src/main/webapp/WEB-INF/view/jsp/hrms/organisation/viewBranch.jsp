<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Branches</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<a href="branches.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">

			<div class="col-md-12">

				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active" ><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>

				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Branch Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3 control-label">Branch
														Type</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-6 control-label ft">${branch.branchType}</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Branch
														Name</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">
															${branch.branchName}</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Parent
														Branch</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.parentBranch}</label>
													</div>
												</div>
												
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Branch Start Date</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.startDate}</label>
													</div>
												</div>
												
												<%-- <div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Time
														Zone</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.timeZoneComplete}</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Currency</label>
													<div class="col-sm-9">
														<div class="input-group">
															<label for="inputPassword3"
																class="col-sm-6 control-label ft">${branch.currency}</label>
														</div>
													</div>
												</div> --%>

											</div>
										</div>
									</div>



									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Address</div>
											<div class="panel-body">


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Address
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.address}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">City
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.city}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">State/Province
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.state}</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Zip
														Code/Country Code</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">
															${branch.zipCode}</label>
													</div>
												</div>



												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Country</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.country}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Phone
														Number </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">
															${branch.phoneNumber}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Fax
														Number </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.faxNumber}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">E-mail
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.eMail}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Website
													</label>
													<div class="col-sm-9">
														<label for="inputPassword8"
															class="col-sm-6 control-label ft">${branch.webSite}</label>
													</div>
												</div>


											</div>
										</div>
									</div>



								</div>

								<div class="col-md-6">

									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Geographical Location</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Latitude
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.latitude}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Longitude
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-6 control-label ft">${branch.longitude}</label>
													</div>
												</div>


											</div>
										</div>
									</div>





									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${branch.notes}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft">${branch.createdBy}</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft">${branch.createdOn}
														</label>
													</div>
												</div>


											</div>
										</div>
									</div>

								</div>
							</div>



						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">

							<!-- <div class="col-md-6">
                	<div class="panel panel-default">
                        <div class="panel-heading">Upload Documents</div>
                        <div class="panel-body">
                        
                             <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Upload Documents </label>
                                <div class="col-sm-9">
                                  <input type="file" class="btn btn-info"/>
                                </div>
                              </div>
                            
                        
                        </div>
                    </div>
                </div> -->


							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Documents</div>
									<div class="panel-body">


										<table class="table no-margn">
											<thead>
												<tr>
													<th>Document Name</th>
													<c:if test="${ rolesMap.roles['Branch Documents'].view}">
													<th>view</th>
													</c:if>
													
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${branch.branchDocuments }" var="d">
													<tr>
														<td>${d.name }</td>
														<c:if test="${ rolesMap.roles['Branch Documents'].view}">
														<td><a href="downloadIntDoc.do?durl=${d.documentUrl }"><i
																class="fa fa-file-text-o sm"></i></a></td>
																</c:if>
													</tr>
												</c:forEach>

											</tbody>
										</table>


									</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#dashbordli").removeClass("active");
		$("#organzationli").addClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").removeClass("active");
		$("#reportsli").removeClass("active");
	});
	</script>
</body>
</html>