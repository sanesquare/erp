<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
#msg_success {
	display: none;
}

#msg_error {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Salary Processor</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="success_msg" id="msg_success">
					<span id="msgs_icn"><i class="fa fa-check"></i></span>Success.
				</div>
				<div class="error_msg" id="msg_error">
					<span id="msge_icn"><i class="fa fa-times"></i></span>Error.
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-12">
				<a href="paysalary.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body" style="overflow: visible;">
							<c:if test="${! paySalaryVo.edit }">
								<div class="row">
									<form:form method="POST" action="calculatePaySalary.do"
										commandName="paySalaryVo" id="paysalary_form">
										<div class="col-md-6">
											<div class="panel panel-default">
												<div class="panel-heading">Salary Information</div>
												<div class="panel-body" style="overflow: visible;">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">
															Branch </label>
														<div class="col-sm-9 ">
															<form:select class="form-control" id="drop_one"
																path="branchId" onchange="getEmployeWithBranchId()"
																data-placeholder=" Branch ">
																<option value="" label="-Select Branch-" />
																<c:forEach items="${branches }" var="b">
																	<form:option value="${b.branchId }">${b.branchName }</form:option>
																</c:forEach>
															</form:select>
														</div>

													</div>

													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">
															Department </label>
														<div class="col-sm-9 ">
															<form:select class="form-control" id="drop_two"
																multiple="multiple" path="departmentIds"
																onchange="getEmployeWithBranchId()"
																data-placeholder=" Department ">
																<!-- <option value="">-Select Department-</option> -->
																<c:forEach items="${departments }" var="b">
																	<form:option value="${b.id }">${b.name }</form:option>
																</c:forEach>
															</form:select>
														</div>

													</div>

													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">
															Employee Name</label>
														<div class="col-sm-9 ">
															<form:select class="form-control" id="drop_three"
																multiple="multiple" path="employeeIds"
																data-placeholder="-Select Employee-">
																<c:forEach items="${employees }" var="e">
																	<form:option value="${e.employeeId }">${e.name }(
																	${e.employeeCode } )</form:option>
																</c:forEach>
															</form:select>
														</div>

													</div>





												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="panel panel-default">
												<div class="panel-heading">Salary Type</div>
												<div class="panel-body">
													<div class="form-group">
														<label class="col-sm-3 control-label">Salary Date</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepicker">
																<form:input type="text" class="form-control"
																	readonly="true" path="date"
																	style="background-color: #fff !important;"
																	data-date-format="DD/MM/YYYY" id="dts" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>

														</div>

													</div>
													<div class="form-group">

														<label for="inputPassword3" class="col-sm-3 control-label">
															Salary Type</label>
														<div class="col-sm-9">
															<form:select class="form-control" path="salaryType"
																id="salaryselector">
																<!-- <option>Select Type</option>
															<option value="one">Monthly</option>
															<option value="two">Daily</option>
															<option value="three">Hourly</option> -->
																<option value="" label="-Select Type-" />
																<form:options items="${salaryTypes }" itemLabel="type"
																	itemValue="id" />
															</form:select>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-12">
															<button type="submit" class="btn btn-info">Process</button>
														</div>
													</div>

												</div>
											</div>
										</div>
									</form:form>
								</div>
							</c:if>
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Salary</div>
										<div class="panel-body">

											<c:if test="${paySalary.monthly }">
												<form:form commandName="paySalary"
													action="saveMontlhyPaysalary.do" method="POST">
													<div class="col-xs-12 msm" style="overflow-x: scroll;">
														<form:hidden path="date" />
														<form:hidden path="id" />
														<table class="table table-bordered split_width"
															width="100%">
															<tbody>
																<tr>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">S NO</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">Emp Code</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">Employee Name</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">Branch</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">Department</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">Designation</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">PF Applicability</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">ESI Applicability</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">Work Status</span></td>
																	<td class="hd_tb split offer_salary"><span
																		class="WhiteHeading">Offered Salary</span></td>
																	<td class="hd_tb split" colspan="2"><span
																		class="WhiteHeading">LOP</span></td>
																	<td class="hd_tb split" colspan="4"><span
																		class="WhiteHeading">Additional Earnings</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">Total Earnings</span></td>
																	<td class="hd_tb split" colspan="10"><span
																		class="WhiteHeading">Deductions </span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">Net Pay</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">PF Employer Contribution</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">ESI Employer Contribution</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">CTC</span></td>
																	<td class="hd_tb split" rowspan="2"><span
																		class="WhiteHeading">SMS</span></td>
																</tr>
																<tr>
																	<c:forEach items="${paySalary.paySlipItems }"
																		var="itms" varStatus="items">
																		<td class="hd_tb split cnt"><span
																			class="WhiteHeading">${itms }</span></td>
																	</c:forEach>
																	<!-- <td class="hd_tb split"><span class="WhiteHeading">Basic</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">DA</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">HRA</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">CCA</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">Conveyance</span></td> -->
																	<input type="hidden"
																		value="${fn:length(paySalary.extraPayslipItems)}"
																		id="itemSize" />
																	<c:forEach items="${paySalary.extraPayslipItems }"
																		var="extraItems" varStatus="items">
																		<input type="hidden" value="${extraItems }"
																			id="item${items.index }">
																		<td class="hd_tb split count"><span
																			class="WhiteHeading">${extraItems }</span></td>
																	</c:forEach>
																	<td class="hd_tb split"><span class="WhiteHeading">Gross
																			Salary</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">LOP
																			Count </span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">LOP
																			Deduction </span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">Incentive
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">Encashment
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">Bonuses
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">Increment
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">PF
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">ESI
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">LWF
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">PROF:TAX
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">TDS
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">Other
																			Deduction </span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">LOP
																			Deduction </span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">Loan
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">Advance
																	</span></td>
																	<td class="hd_tb split"><span class="WhiteHeading">Total
																			Deductions </span></td>


																</tr>
																<c:forEach items="${paySalary.employeeItemsVo }"
																	var="items" varStatus="s">
																	<tr>
																		<form:hidden
																			path="employeeItemsVo[${s.index }].monthlyId" />
																		<td>${s.count }</td>
																		<td><input type="text" readonly="readonly"
																			name="employeeItemsVo[${s.index }].employeeCode"
																			value="${items.employeeCode }" class="input_amouts" /></td>
																		<td>${items.name }</td>
																		<td>${items.branch }</td>
																		<td>${items.department }</td>
																		<td>${items.designation }</td>
																		<td><input type="text" readonly="readonly"
																			name="employeeItemsVo[${s.index }].pfApplicability"
																			value="${items.pfApplicability }"
																			class="input_amouts" /></td>
																		<td><input type="text" readonly="readonly"
																			name="employeeItemsVo[${s.index }].esiApplicability"
																			value="${items.esiApplicability }"
																			class="input_amouts" /></td>
																		<td>${items.status}</td>
																		<%-- 	<td><input type="text" value="${items.basic }"
																			id="basic${s.index }"
																			onkeyup="calculateGrossSalary('${s.index }')"
																			name="employeeItemsVo[${s.index }].basic"
																			class="input_amouts double" /></td>
																		<td><input type="text" value="${items.da }"
																			id="da${s.index }"
																			onkeyup="calculateGrossSalary('${s.index }')"
																			name="employeeItemsVo[${s.index }].da"
																			class="input_amouts double" /></td>
																		<td><input type="text" value="${items.hra }"
																			id="hra${s.index }"
																			onkeyup="calculateGrossSalary('${s.index }')"
																			name="employeeItemsVo[${s.index }].hra"
																			class="input_amouts double" /></td>
																		<td><input type="text" value="${items.cca }"
																			id="cca${s.index }"
																			onkeyup="calculateGrossSalary('${s.index }')"
																			name="employeeItemsVo[${s.index }].cca"
																			class="input_amouts double" /></td>
																		<td><input type="text" id="conveyance${s.index }"
																			onkeyup="calculateGrossSalary('${s.index }')"
																			name="employeeItemsVo[${s.index }].conveyance"
																			value="${items.conveyance }"
																			class="input_amouts double" /></td> --%>
																		<c:forEach items="${paySalary.paySlipItems }"
																			varStatus="status" var="extraItems">
																			<c:set var="isSet" scope="page" value="false" />
																			<c:forEach items="${items.items }" var="other">
																				<c:if test="${other.key ==  extraItems}">
																					<c:set var="isSet" scope="page" value="true" />
																					<td><input type="text"
																						id="txt${s.count }${extraItems}"
																						onkeyup="updatePayslipListValue('${s.count }','${extraItems }','${s.index }')"
																						name="${employeeItemsVo[s.index].items[s.index ].value }"
																						value="${other.value }"
																						class="input_amouts double payslipItem" /></td>
																					<input type="hidden"
																						id="hiden${s.count }${extraItems}"
																						name="employeeItemsVo[${s.index }].salaryItemsList[${status.index }]"
																						value="${other.value }&!=${extraItems }" />
																				</c:if>
																			</c:forEach>
																			<c:if test="${isSet == 'false'}">
																				<input type="hidden"
																					id="hiden${s.count }${extraItems}"
																					name="employeeItemsVo[${s.index }].salaryItemsList[${status.index }]"
																					value="0.00&!=${extraItems }" />
																				<td><input type="text" value="0.00"
																					id="txt${s.count }${extraItems}"
																					onkeyup="updatePayslipListValue('${s.count }','${extraItems }','${s.index }')"
																					name="${employeeItemsVo[s.index].items[s.index ].value }"
																					class="input_amouts double payslipItem" /></td>
																			</c:if>
																		</c:forEach>
																		<c:choose>
																			<c:when test="${! paySalary.edit }">
																				<c:forEach items="${paySalary.extraPayslipItems }"
																					varStatus="status" var="extraItems">
																					<c:set var="isSet" scope="page" value="false" />
																					<c:forEach items="${items.otherSalaryItems }"
																						var="other">
																						<c:if test="${other.key ==  extraItems}">
																							<c:set var="isSet" scope="page" value="true" />
																							<td><input type="text"
																								onkeyup="updateListValue('${s.count }','${extraItems }','${s.index }')"
																								id="text${s.count }${extraItems}"
																								name="${employeeItemsVo[s.index].otherSalaryItems[s.index ].value }"
																								value="${other.value }"
																								class="input_amouts double" /></td>
																							<input type="hidden"
																								id="hidden${s.count }${extraItems}"
																								name="employeeItemsVo[${s.index }].otherSalaryItemsList[${status.index }]"
																								value="${other.value }&!=${extraItems }" />
																						</c:if>
																					</c:forEach>
																					<c:if test="${isSet == 'false'}">
																						<input type="hidden"
																							id="hidden${s.count }${extraItems}"
																							name="employeeItemsVo[${s.index }].otherSalaryItemsList[${status.index }]"
																							value="0.00&!=${extraItems }" />
																						<td><input type="text" value="0.00"
																							onkeyup="updateListValue('${s.count }','${extraItems }','${s.index }')"
																							id="text${s.count }${extraItems}"
																							name="${employeeItemsVo[s.index].otherSalaryItems[s.index ].value }"
																							class="input_amouts double" /></td>
																					</c:if>
																				</c:forEach>
																			</c:when>

																			<c:otherwise>
																				<c:forEach items="${paySalary.extraPayslipItems }"
																					varStatus="status" var="extraItems">
																					<c:forEach items="${items.otherSalaryItems }"
																						var="other">
																						<c:choose>
																							<c:when test="${other.key ==  extraItems}">
																								<td><input type="text"
																									onkeyup="updateListValue('${s.count }','${extraItems }','${s.index }')"
																									id="text${s.count }${extraItems}"
																									name="${employeeItemsVo[s.index].otherSalaryItems[s.index ].value }"
																									value="${other.value }"
																									class="input_amouts double" /></td>
																								<input type="hidden"
																									id="hidden${s.count }${extraItems}"
																									name="employeeItemsVo[${s.index }].otherSalaryItemsList[${status.index }]"
																									value="${other.value }&!=${extraItems }" />
																							</c:when>
																						</c:choose>
																					</c:forEach>
																					<c:if test="${empty  items.otherSalaryItems}">
																						<input type="hidden"
																							id="hidden${s.count }${extraItems}"
																							name="employeeItemsVo[${s.index }].otherSalaryItemsList[${status.index }]"
																							value="0.00&!=${extraItems}" />
																						<td><input type="text" value="0.00"
																							onkeyup="updateListValue('${s.count }','${extraItems }','${s.index }')"
																							id="text${s.count }${extraItems}"
																							class="input_amouts double" /></td>
																					</c:if>
																				</c:forEach>
																			</c:otherwise>
																		</c:choose>

																		<td><input type="text"
																			id="gross_salary${s.index }"
																			onkeyup="calculateTotalEarnings('${s.index }')"
																			name="employeeItemsVo[${s.index }].grossSalary"
																			value="${items.grossSalary }"
																			class="input_amouts double" /></td>
																		<td><input type="text" id="lop_count${s.index }"
																			onblur="calculateLop('${s.index }')"
																			name="employeeItemsVo[${s.index }].lopCount"
																			value="${items.lopCount }"
																			class="input_amouts double" /><input type="hidden"
																			name="employeeItemsVo[${s.index }].perDayCalculation"
																			id="lop_calc${s.index }"
																			value="${items.perDayCalculation }"> <input
																			type="hidden"
																			name="employeeItemsVo[${s.index }].totlaWorkingDays"
																			value="${items.totlaWorkingDays }"></td>
																		<td><input type="text"
																			id="lop_ded_view${s.index }"
																			name="employeeItemsVo[${s.index }].lopDeduction"
																			value="${items.lopDeduction }"
																			class="input_amouts double" /></td>
																		<td><input type="text" id="incentive${s.index }"
																			onkeyup="calculateTotalEarnings('${s.index }')"
																			name="employeeItemsVo[${s.index }].incentive"
																			value="${items.incentive }"
																			class="input_amouts double" /></td>
																		<td><input type="text" id="encashment${s.index }"
																			onkeyup="calculateTotalEarnings('${s.index }')"
																			name="employeeItemsVo[${s.index }].encashment"
																			value="${items.encashment }"
																			class="input_amouts double" /></td>
																		<td><input type="text" value="${items.bonus }"
																			id="bonus${s.index }"
																			onkeyup="calculateTotalEarnings('${s.index }')"
																			name="employeeItemsVo[${s.index }].bonus"
																			class="input_amouts double" /></td>
																		<td><input type="text" id="increment${s.index }"
																			onkeyup="calculateTotalEarnings('${s.index }')"
																			name="employeeItemsVo[${s.index }].increment"
																			value="${items.increment }"
																			class="input_amouts double" /></td>
																		<td><input type="text"
																			onkeyup="calculateNetpay('${s.index }')"
																			id="total_earnings${s.index }"
																			name="employeeItemsVo[${s.index }].totalEarnings"
																			value="${items.totalEarnings }"
																			class="input_amouts double" /></td>
																		<td><input type="text" value="${items.pf }"
																			onkeyup="calculateDeduction('${s.index }')"
																			id="pf_ded${s.index }"
																			name="employeeItemsVo[${s.index }].pf"
																			class="input_amouts double deductions" /></td>
																		<td><input type="text" value="${items.esi }"
																			onkeyup="calculateDeduction('${s.index }')"
																			id="esi_ded${s.index }"
																			name="employeeItemsVo[${s.index }].esi"
																			class="input_amouts double deductions" /></td>
																		<td><input type="text" value="${items.lwf }"
																			onkeyup="calculateDeduction('${s.index }')"
																			id="lwf_ded${s.index }"
																			name="employeeItemsVo[${s.index }].lwf"
																			class="input_amouts double deductions" /></td>
																		<td><input type="text" id="pt_ded${s.index }"
																			onkeyup="calculateDeduction('${s.index }')"
																			name="employeeItemsVo[${s.index }].professionalTaxs"
																			value="${items.professionalTaxs }"
																			class="input_amouts double deductions" /></td>
																		<td><input type="text" value="${items.tax }"
																			onkeyup="calculateDeduction('${s.index }')"
																			id="tax_ded${s.index }"
																			name="employeeItemsVo[${s.index }].tax"
																			class="input_amouts double deductions" /></td>
																		<td><input type="text" id="ded_ded${s.index }"
																			onkeyup="calculateDeduction('${s.index }')"
																			name="employeeItemsVo[${s.index }].deductions"
																			value="${items.deductions }"
																			class="input_amouts double deductions" /></td>
																		<td><input type="text" id="lop_ded${s.index }"
																			onkeyup="calculateDeduction('${s.index }')"
																			name="employeeItemsVo[${s.index }].lopDeduction"
																			value="${items.lopDeduction }"
																			class="input_amouts double deductions" /></td>
																		<td><input type="text" value="${items.loan }"
																			id="loan_ded${s.index }"
																			onkeyup="calculateDeduction('${s.index }')"
																			name="employeeItemsVo[${s.index }].loan"
																			class="input_amouts double deductions" /></td>
																		<td><input type="text" id="adv_ded${s.index }"
																			onkeyup="calculateDeduction('${s.index }')"
																			name="employeeItemsVo[${s.index }].advanceSalary"
																			value="${items.advanceSalary }"
																			class="input_amouts double deductions" /></td>
																		<td><input type="text" id="total_ded${s.index }"
																			onkeyup="calculateNetpay('${s.index }')"
																			name="employeeItemsVo[${s.index }].totalDeduction"
																			value="${items.totalDeduction }"
																			class="input_amouts double deductions" /></td>
																		<td><input type="text" value="${items.netPay }"
																			id="net_pay${s.index }"
																			name="employeeItemsVo[${s.index }].netPay"
																			onkeyup="calculateCTC(${s.index })"
																			class="input_amouts double" /></td>
																		<td><input type="text"
																			onkeyup="calculateCTC(${s.index })"
																			id="pf_employer${s.index }"
																			name="employeeItemsVo[${s.index }].pfEmployer"
																			value="${items.pfEmployer }"
																			class="input_amouts double" /></td>
																		<td><input type="text"
																			onkeyup="calculateCTC(${s.index })"
																			id="esi_employer${s.index }"
																			name="employeeItemsVo[${s.index }].esiEmployer"
																			value="${items.esiEmployer }"
																			class="input_amouts double" /></td>
																		<td><input type="text" value="${items.ctc }"
																			id="ctc${s.index }"
																			name="employeeItemsVo[${s.index }].ctc"
																			class="input_amouts double" /></td>
																		<td><i
																			onclick="sendSms('${items .employeeCode}','${paySalary.id}','${paySalary.salaryTypeType}')"
																			id="sms${items .employeeCode}"
																			class="fa fa-envelope sm"></i></td>
																	</tr>
																</c:forEach>

															</tbody>
														</table>
													</div>
													<c:if test="${! empty paySalary.employeeItemsVo  }">
														<div class="form-group">
															<div class="col-sm-12">
																<button type="submit" class="btn btn-info">Save</button>
															</div>
														</div>
													</c:if>
												</form:form>
											</c:if>

											<c:if test="${paySalary.daily }">
												<div class="col-xs-12 msm" style="overflow-x: auto;">
													<form:form commandName="paySalary" action="savePayDaily.do">
														<form:hidden path="date" />
														<form:hidden path="id" />
														<table class="table table-striped no-margn line">
															<tbody>
																<tr>

																	<td class="hd_tb"><span class="WhiteHeading">S#</span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Date</span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Employee
																			Code </span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Employee
																			Name </span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Daily
																			Rate</span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Amount</span></td>
																	<td class="hd_tb"><span class="WhiteHeading">SMS</span></td>
																</tr>

																<c:set var="dailyWageDate"
																	value="${paySalary.dailyWageDate}"></c:set>
																<c:forEach items="${paySalary.dailyWageDetailsVos }"
																	var="daily" varStatus="s">
																	<tr>
																		<form:hidden
																			path="dailyWageDetailsVos[${s.index }].dailySalaryId" />
																		<td>${s.count }</td>
																		<td><form:input
																				path="dailyWageDetailsVos[${s.index }].dailyWageDate"
																				readonly="true" value="${dailyWageDate}"
																				class="input_amouts" /></td>
																		<td><form:input
																				path="dailyWageDetailsVos[${s.index }].employeeCode"
																				readonly="true" value="${daily.employeeCode }"
																				class="input_amouts" /></td>
																		<td>${daily.empName }</td>
																		<td><form:input
																				path="dailyWageDetailsVos[${s.index }].dailyRate"
																				value="${daily.dailyRate }"
																				class="input_amouts double" /></td>
																		<td><form:input
																				path="dailyWageDetailsVos[${s.index }].amount"
																				value="${daily.amount }" class="input_amouts double" /></td>
																		<td><i class="fa fa-envelope sm"
																			onclick="sendSms('${daily .employeeCode}','${paySalary.id}','${paySalary.salaryTypeType}')"
																			id="sms${daily .employeeCode}"></i></td>
																	</tr>
																</c:forEach>




															</tbody>
														</table>
														<c:if test="${! empty paySalary.dailyWageDetailsVos }">
															<div class="form-group">
																<div class="col-sm-12">
																	<button type="submit" class="btn btn-info">Save</button>
																</div>
															</div>
														</c:if>
													</form:form>
												</div>
											</c:if>
											<c:if test="${paySalary.hourly }">
												<div class="col-xs-12 msm" style="overflow-x: auto;">

													<form:form commandName="paySalary"
														action="savePayHourly.do">
														<form:hidden path="date" />
														<form:hidden path="id" />
														<table class="table table-striped no-margn line">
															<tbody>
																<tr>

																	<td class="hd_tb"><span class="WhiteHeading">S#</span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Date</span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Employee
																			Code </span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Employee
																			Name </span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Total
																			Hours</span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Hourly
																			Rate</span></td>
																	<td class="hd_tb"><span class="WhiteHeading">Amount</span></td>
																	<td class="hd_tb"><span class="WhiteHeading">SMS</span></td>
																</tr>



																<c:set var="hourlyWageDate"
																	value="${paySalary.hourlyWageDate}"></c:set>
																<c:forEach items="${paySalary.hourlyWagesDetailsVos }"
																	var="hourly" varStatus="s">
																	<tr>
																		<form:hidden
																			path="hourlyWagesDetailsVos[${s.index }].hourlySalaryId" />
																		<td>${s.count }</td>
																		<td><form:input
																				path="hourlyWagesDetailsVos[${s.index }].hourlyWageDate"
																				readonly="true" value="${hourlyWageDate }"
																				class="input_amouts" /></td>
																		<td><form:input
																				path="hourlyWagesDetailsVos[${s.index }].employeeCode"
																				readonly="true" value="${hourly .employeeCode}"
																				class="input_amouts" /></td>
																		<td><form:input
																				path="hourlyWagesDetailsVos[${s.index }].employeeName"
																				readonly="true" value="${hourly.employeeName }"
																				class="input_amouts" /></td>
																		<td><form:input
																				path="hourlyWagesDetailsVos[${s.index }].totalHours"
																				readonly="true" value="${hourly.totalHours }"
																				class="input_amouts" /></td>
																		<td><form:input
																				path="hourlyWagesDetailsVos[${s.index }].hourlyRate"
																				value="${hourly.hourlyRate }"
																				class="input_amouts double" /></td>
																		<td><form:input
																				path="hourlyWagesDetailsVos[${s.index}].amount"
																				value="${hourly.amount }"
																				class="input_amouts double" /></td>
																		<td><i
																			onclick="sendSms('${hourly .employeeCode}','${paySalary.id}','${paySalary.salaryTypeType}')"
																			id="sms${hourly .employeeCode}"
																			class="fa fa-envelope sm"></i>
																		</button></td>
																	</tr>

																</c:forEach>
															</tbody>
														</table>
														<c:if test="${! empty paySalary.hourlyWagesDetailsVos }">
															<div class="form-group">
																<div class="col-sm-12">
																	<button type="submit" class="btn btn-info">Save</button>
																</div>
															</div>
														</c:if>
													</form:form>
												</div>
											</c:if>



										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script
		src="<c:url value='/resources/docs/js/bootstrap-multiselect.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$('#process').click(function() {
				$('#pro').toggle();
			});

			$('#salaryselector').change(function() {
				$('.msm').hide();
				$('#' + $(this).val()).show();
			});

			/* $('#drop_one').multiselect({
				includeSelectAllOption : true
			}); */

			$('#drop_two').multiselect({
				includeSelectAllOption : true
			});
			$('#drop_three').multiselect({
				includeSelectAllOption : true
			});

			if ("${paySalary.monthly }") {
				$("#monthly_table").css("display", "block");
			}
			count = parseInt($(".cnt").size()) + parseInt(1);
			$(".offer_salary").attr("colspan", count);
			
		});

		var items = "${paySalary.extraPayslipItems}";
		var itemSize = $("#itemSize").val();

		/* function to set other allowance values into list */
		function updateListValue(id, item , index) {
			 var amount = "0.00";
			if($("#text" + id + item).val()!='')
				amount = $("#text" + id + item).val();
			else
				$("#text" + id + item).val("0.00");
			$("#hidden" + id + item).val(
					(amount) + "&!=" + item);
			calculateGrossSalary(index , id);
		}

		/* function to set  allowance values into list */
		function updatePayslipListValue(id, item , index){
			 var amount = "0.00";
				if($("#txt" + id + item).val()!='')
					amount = $("#txt" + id + item).val();
				else
					$("#txt" + id + item).val("0.00");
				$("#hiden" + id + item).val(
						(amount) + "&!=" + item);
				calculateGrossSalary(index , id);
		}
		
		/*  calculating deductions */
		var pfDeduction = "0.00";
		var esiDeduction = "0.00";
		var lwfDeduction = "0.00";
		var ptDeduction = "0.00";
		var taxDeduction = "0.00";
		var lopDeduction = "0.00";
		var loanDeduction = "0.00";
		var advDeduction = "0.00";
		var dedDeduction = "0.00";
		var totalDeduction = "0.00";
		var netPay = "0.00";
		var pfEmployer = "0.00";
		var esiEmployer = "0.00";
		var ctc = "0.00";
		var totalEarnings = "0.00";
		var grossSalary = "0.00";
		var incentive = "0.00";
		var encashment = "0.00";
		var bonus = "0.00";
		var increment = "0.00";
		var basic = "0.00";
		var da = "0.00";
		var hra = "0.00";
		var cca = "0.00";
		var conveyance = "0.00";

			calculateDeduction=	function(id) {
					if ($("#pf_ded"+id).val() != '')
						pfDeduction = $("#pf_ded"+id).val();
					if ($("#esi_ded"+id).val() != '')
						esiDeduction = $("#esi_ded"+id).val();
					if ($("#lwf_ded"+id).val() != '')
						lwfDeduction = $("#lwf_ded"+id).val();
					if ($("#pt_ded"+id).val() != '')
						ptDeduction = $("#pt_ded"+id).val();
					if ($("#tax_ded"+id).val() != '')
						taxDeduction = $("#tax_ded"+id).val();
					if ($("#lop_ded"+id).val() != ''){
						lopDeduction = $("#lop_ded"+id).val();
						$("#lop_ded_view"+id).val($("#lop_ded"+id).val());
					}
					if ($("#loan_ded"+id).val() != '')
						loanDeduction = $("#loan_ded"+id).val();
					if ($("#adv_ded"+id).val() != '')
						advDeduction = $("#adv_ded"+id).val();
					if ($("#ded_ded"+id).val() != '')
						dedDeduction = $("#ded_ded"+id).val();
					
					totalDeduction = parseFloat(pfDeduction)
							+ parseFloat(esiDeduction)
							+ parseFloat(lwfDeduction)
							+ parseFloat(ptDeduction)
							+ parseFloat(taxDeduction)
							+ parseFloat(lopDeduction)
							+ parseFloat(loanDeduction)
							+ parseFloat(advDeduction)
							+ parseFloat(dedDeduction);
					$("#total_ded"+id).val(totalDeduction);
					calculateNetpay(id);
				}
			
			/* ctc calculation */
			calculateCTC = function(id){
				if ($("#net_pay"+id).val() != '')
					netPay = $("#net_pay"+id).val();
				if ($("#pf_employer"+id).val() != '')
					pfEmployer = $("#pf_employer"+id).val();
				if ($("#esi_employer"+id).val() != '')
					esiEmployer = $("#esi_employer"+id).val();
				ctc = parseFloat(netPay)+parseFloat(pfEmployer)+parseFloat(esiEmployer);
				$("#ctc"+id).val(ctc);
			}
			
			/* net pay calculation */
			function calculateNetpay(id){
				if ($("#total_ded"+id).val() != '')
				totalDeduction = $("#total_ded"+id).val();
				if ($("#total_earnings"+id).val() != '')
				totalEarnings = $("#total_earnings"+id).val();
				netPay = parseFloat(totalEarnings)-parseFloat(totalDeduction);
				$("#net_pay"+id).val(netPay);
				calculateCTC(id);
			}
			
			/* total earnings calculation */
			calculateTotalEarnings = function(id){
				if ($("#gross_salary"+id).val() != '')
					grossSalary = $("#gross_salary"+id).val();
				if ($("#incentive"+id).val() != '')
					incentive = $("#incentive"+id).val();
				if ($("#encashment"+id).val() != '')
					encashment = $("#encashment"+id).val();
				if ($("#bonus"+id).val() != '')
					bonus = $("#bonus"+id).val();
				if ($("#increment"+id).val() != '')
					increment = $("#increment"+id).val();
				totalEarnings = parseFloat(grossSalary)+parseFloat(incentive)+parseFloat(encashment)+parseFloat(bonus)+parseFloat(increment);
				$("#total_earnings"+id).val(totalEarnings);
				calculateNetpay(id);
			}
			
			/* gross salary calculation */
			calculateGrossSalary = function(id){
				if ($("#basic"+id).val() != '')
					basic = $("#basic"+id).val();
				if ($("#da"+id).val() != '')
					da = $("#da"+id).val();
				if ($("#hra"+id).val() != '')
					hra = $("#hra"+id).val();
				if ($("#cca"+id).val() != '')
					cca = $("#cca"+id).val();
				if ($("#conveyance"+id).val() != '')
					conveyance = $("#conveyance"+id).val();
				grossSalary = parseFloat(basic)+parseFloat(da)+parseFloat(hra)+parseFloat(cca)+parseFloat(conveyance);
				$("#gross_salary"+id).val(grossSalary);
				calculateTotalEarnings(id);
			}
			
			/* gross salary calculation */
			calculateGrossSalary = function(id , index){
				var amount = "0.00"
					$(".payslipItem").each(function(i,item){
						amount = parseFloat(amount)+parseFloat(item.value);
					});
				console.log(amount);
					grossSalary = amount;
					$("#gross_salary"+id).val(grossSalary.toFixed(2));
				calculateTotalEarnings(id);
			}
			function calculateLop(id){
				var count = $("#lop_count"+id).val();
				var price = $("#lop_calc"+id).val();
				var lop = parseFloat(price)*parseFloat(count).toFixed(2);
				$("#lop_ded_view"+id).val(lop.toFixed(2));
				$("#lop_ded"+id).val(lop.toFixed(2));
				calculateDeduction(id);
			}
			sendSms = function(empCode,id,type){
				$("#sms"+id).fadeOut(100);
				  $.ajax({
					type : "GET",
					url : "sendPaysalarySmsForIndividual",
					dataType : "html",
					cache : false,
					async : true,
					data : {
						"empCode" : empCode,
						"id" : id,
						"type" : type
					},
					success : function(response) {
						$("#msg_success").css("display","block");
						setTimeout(function(){
							$("#msg_success").fadeOut(500);
						},2000);
						$("#sms"+id).fadeIn(1000);
					},
					error : function(requestObject, error, errorThrown) {
						$("#msg_error").css("display","block");
						setTimeout(function(){
							$("#msg_error").fadeOut(500);
						},2000);
						$("#sms"+id).fadeIn(100);$("#sms"+id).fadeIn(100);
					}
				});  
			}
			
			
	</script>

</body>
</html>