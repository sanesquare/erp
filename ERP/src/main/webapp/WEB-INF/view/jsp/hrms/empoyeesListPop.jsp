<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default erp-info" style="border-radius: 4px">
			<div class="panel-heading panel-inf">
				Select Employee <i style="float: right;" onclick="showPop()" class="fa fa-arrow-left"></i>
			</div>
			<div class="panel-body" style="min-height:300px;max-height: 300px; overflow-y: scroll;">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-sm-12"
								>
								<c:forEach items="${employees }" var="a">
									<label class="popemp" onclick="selectThisEmployee(' ${a.employeeCode }',' ${a.name }')"> ${a.name } ( ${a.employeeCode } )</label>
									<br>
								</c:forEach>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
<!-- <div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<button type="button" id="btn-select-employee-pop" onclick="selectme()" style="float: right;"
				class="btn btn-primary erp-btn">Select</button>
		</div>
	</div>
</div> -->
<div class="notification-panl successMsg" style="display: none;">
	<div class="notfctn-cntnnt">${success_msg }</div>
	<span id="close-msg" onclick="closeMessage()"><i
		class="fa fa-times"></i></span>
</div>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>