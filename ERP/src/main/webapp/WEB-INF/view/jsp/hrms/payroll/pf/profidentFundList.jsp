<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width="30%">Employee Name</th>
			<th width="30%">Employee Share</th>
			<th width="30%">Organisation Share</th>
			<c:if test="${ rolesMap.roles['Provident Fund'].update}">
				<th width="10%">Edit</th>
			</c:if>
			<c:if test="${ rolesMap.roles['Provident Fund'].delete}">
				<th width="10%">Delete</th>
			</c:if>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pfList}" var="pf">
			<tr>
				<c:choose>
					<c:when test="${ rolesMap.roles['Provident Fund'].view}">
						<td><c:choose>
								<c:when test="${pf.recordedBy==thisUser}">
									<i class="fa fa-arrow-up up-arrow"></i>
								</c:when>
								<c:otherwise>
									<i class="fa fa-arrow-down down-arrow"></i>
								</c:otherwise>
							</c:choose><a href="ViewProfidentFund.do?otp=${pf.profidentFundId}">${pf.employee}</a></td>
						<td><a href="ViewProfidentFund.do?otp=${pf.profidentFundId}">${pf.employeeShare}</a></td>
						<td><a href="ViewProfidentFund.do?otp=${pf.profidentFundId}">${pf.organizationShare}</a></td>
					</c:when>
					<c:otherwise>
						<td><c:choose>
								<c:when test="${pf.recordedBy==thisUser}">
									<i class="fa fa-arrow-up up-arrow"></i>
								</c:when>
								<c:otherwise>
									<i class="fa fa-arrow-down down-arrow"></i>
								</c:otherwise>
							</c:choose>${pf.employee}</td>
						<td>${pf.employeeShare}</td>
						<td>${pf.organizationShare}</td>
					</c:otherwise>
				</c:choose>

				<c:if test="${ rolesMap.roles['Provident Fund'].update}">
					<td><a href="EditProfidentFund.do?otp=${pf.profidentFundId}"><i
							class="fa fa-edit sm"></i> </a></td>
				</c:if>
				<c:if test="${ rolesMap.roles['Provident Fund'].delete}">
					<td><a href="#"
						onclick="profidentFundDelConfirm(${pf.profidentFundId})"><i
							class="fa fa-close ss"></i></a></td>
				</c:if>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${ rolesMap.roles['Provident Fund'].add}">
	<a href="AddProfidentFund.do">
		<button type="button" class="btn btn-primary">Add New
			Provident Fund</button>
	</a>
</c:if>

<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

