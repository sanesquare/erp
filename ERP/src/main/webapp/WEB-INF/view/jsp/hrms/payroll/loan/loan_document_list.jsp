<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table class="table no-margn">
	<c:choose>
		<c:when test="${not empty loanDocumentList.loanDocuments}">
			<thead>
				<tr>
					<th>#</th>
					<th>Document Name</th>
					<c:if test="${ rolesMap.roles['Loan Documents'].view}">
					<th>view</th>
					</c:if>
					<c:if test="${ rolesMap.roles['Loan Documents'].delete}">
					<th>Delete</th>
					</c:if>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${loanDocumentList.loanDocuments}" var="document"
					varStatus="status">
					<tr>
						<td>${status.count}</td>
						<td><i class="fa fa-file-o"></i>${document.documentName}</td>
						<c:if test="${ rolesMap.roles['Loan Documents'].view}">
						<td><i class="fa fa-file-text-o sm"></i></td>
						</c:if>
						<c:if test="${ rolesMap.roles['Loan Documents'].delete}">
						<td><i class="fa fa-close ss"
							onclick="deleteLoanDoc(${loanId}, ${document.loanDocId},'${document.documentUrl}')"></i></td></c:if>
					</tr>
				</c:forEach>
			</tbody>
		</c:when>
		<c:otherwise>
			<span>No Documents Available </span>
		</c:otherwise>
	</c:choose>

</table>