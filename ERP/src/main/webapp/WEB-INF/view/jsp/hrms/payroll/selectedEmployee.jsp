<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="col-sm-8 col-xs-10 br-de-emp-list">
	<input type="text" name="employee" class="form-control"
		placeholder="Search Employee" id="employeeSl" value="${employeeName}"
		required="required" /> <input type="hidden" name="employeeCode"
		id="employeeCode" value="${employeeCode}" />
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$('#employeeSl').bind('click', function() {
			getAllBranches();
			$('#pop1').simplePopup();
		});
		getEmployeeDetails();
	});
</script>