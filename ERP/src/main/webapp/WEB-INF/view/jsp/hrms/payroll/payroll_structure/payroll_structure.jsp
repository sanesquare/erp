<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/jquery-ui.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Payroll Structure</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Payroll Options</a></li>
					<li role="presentation"><a href="#docs" role="tab"
						data-toggle="tab" id="pay-slp-item-tab"> Payslip items</a></li>
					<li role="presentation"><a href="#tps" role="tab"
						data-toggle="tab" id="tax-rule-item-tab"> Tax Rules</a></li>
					<!-- <li role="presentation"><a href="#txl" role="tab"
						data-toggle="tab"> Auto Deductions</a></li> -->
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Payroll Options</div>
									<div class="panel-body">
										<form:form action="SavePayrollOption.do" method="post"
											commandName="payRollOption">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Auto
													Approve Pay Slip</label>
												<div class="col-sm-9">
													<form:select path="autoAprovePayRoll"
														id="autoAprovePayRoll" class="form-control"
														onchange="hideOrShowEmployee()">
														<form:option value="false">No, Payroll must always be approved by
															the following employee</form:option>
														<form:option value="true">Yes</form:option>
													</form:select>
												</div>
											</div>
											<div class="form-group" id="paySlipAprovers">
												<label for="inputPassword3" class="col-sm-3 control-label"></label>
												<div class="col-sm-9">
													<form:select path="approvers"
														class="form-control chosen-select" multiple="multiple"
														data-placeholder="Select Approvers">
														<form:options items="${employees}" itemValue="value"
															itemLabel="label" />
													</form:select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Auto
													Email Pay Slip </label>
												<div class="col-sm-9">
													<form:select path="autoEmailPaySlip" class="form-control">
														<form:option value="false">No</form:option>
														<form:option value="true">Yes, Payslip will be emailed to the
															employee when it is generated</form:option>
													</form:select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Per
													Day Salary Calculation </label>
												<div class="col-sm-9">
													<form:checkbox path="perDaySalaryCalculation" value="true"
														id="perDaySalaryCalculation" onclick="showMe('slip')" />
												</div>
											</div>
											<div class="form-group" id="slip">
												<label for="inputPassword3" class="col-sm-3 control-label"></label>
												<div class="col-sm-6 col-xs-6">
													<form:select path="perDaySalaryCalculationType"
														class="form-control" id="test">
														<form:option value="Month">Divide with Number of Days in
															the Salary Month</form:option>
														<form:option value="Days">Divide with
															Specified Number of Days</form:option>
													</form:select>
												</div>
												<div class="col-sm-3 col-xs-3">
													<form:input path="perDaySalaryCalculationDays"
														class="form-control" id="hidden_div"
														style="display: none;" />
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-9">
													<form:hidden path="payRollOptionId" />
													<button type="submit" class="btn btn-info">Save
														Payroll Option</button>
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="docs">
						<div class="panel-body" id="pay-slip-item-tab-div"></div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="tps">
						<div class="panel-body" id="tax-rule-item-tab-div"></div>
					</div>
					<!-- <div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="txl">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Auto Deductions for Late
											Attendance</div>
										<div class="panel-body">
											<input type="checkbox" />Apply Auto Deduction for Late
											Attendance
											<table cellpadding="0" cellspacing="0" border="0"
												class="table table-striped table-bordered" id="Table5">
												<thead>
													<tr>
														<th width="20%">Minutes From</th>
														<th width="20%">Minutes To</th>
														<th width="20%"></th>
														<th width="20%">Deduction Amount</th>
														<th width="20%"></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><input type="text" class="form-control"
															name="minf" /></td>
														<td><input type="text" class="form-control"
															name="mint" /></td>
														<td><select class="form-control" name="calc">
																<option>Specified Amount</option>
																<option>Employee's Per Day Salary</option>
																<option>Employee's Half Day Salary</option>
														</select></td>
														<td><input type="text" class="form-control"
															name="dda" /></td>
														<td></td>
													</tr>
													<tr>
														<td><input type="text" class="form-control"
															name="minf" /></td>
														<td><input type="text" class="form-control"
															name="mint" /></td>
														<td><select class="form-control" name="calc">
																<option>Specified Amount</option>
																<option>Employee's Per Day Salary</option>
																<option>Employee's Half Day Salary</option>
														</select></td>
														<td><input type="text" class="form-control"
															name="dda" /></td>
														<td></td>
													</tr>
													<tr>
														<td><input type="text" class="form-control"
															name="minf" /></td>
														<td><input type="text" class="form-control"
															name="mint" /></td>
														<td><select class="form-control" name="calc">
																<option>Specified Amount</option>
																<option>Employee's Per Day Salary</option>
																<option>Employee's Half Day Salary</option>
														</select></td>
														<td><input type="text" class="form-control"
															name="dda" /></td>
														<td><input type="button" value="Add Row"
															onclick="amtRow()" id="amtButton3"></td>
													</tr>
												</tbody>
											</table>
											<div class="form-group">
												<div class="col-sm-offset-6 col-sm-6">
													<button type="submit" class="btn btn-info">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Auto Deductions for Absence</div>
										<div class="panel-body">
											<div class="form-group">
												<input type="checkbox" />Apply Auto Deduction for Absence
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Deduction
													Amount</label>
												<div class="col-sm-6 col-xs-6">
													<select class="form-control" id="absence">
														<option value="0">Employee's Per Day Salary</option>
														<option value="1"onClick"abs()">Specified Amount</option>
														<option value="2">Employee's Half Day Salary</option>
													</select>
												</div>
												<div class="col-sm-3 col-xs-3">
													<input type="text" class="form-control" id="abs"
														style="display: none;" placeholder="">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-6 col-sm-6">
													<button type="submit" class="btn btn-info">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Auto Deductions for a Leave
											Without Pay</div>
										<div class="panel-body">
											<div class="form-group">
												<input type="checkbox" /> Apply Auto Deduction for Leave
												Without Pay
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Deduction
													Amount</label>
												<div class="col-sm-6 col-xs-6">
													<select class="form-control" id="pay">
														<option value="0">Employee's Per Day Salary</option>
														<option value="1"onClick"aps()">Specified Amount</option>
														<option value="2">Employee's Half Day Salary</option>
													</select>
												</div>
												<div class="col-sm-3 col-xs-3">
													<input type="text" class="form-control" id="aps"
														style="display: none;" placeholder="">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-6 col-sm-6">
													<button type="submit" class="btn btn-info">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->

	<script
		src="<c:url value='https://code.jquery.com/jquery-1.10.2.js' />"></script>
	<script
		src="<c:url value='https://code.jquery.com/ui/1.11.4/jquery-ui.js' />"></script>

	<%-- <script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script> --%>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script src="<c:url value='/resources/assets/js/auto_complete.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script language="javascript">
		hideOrShowEmployee = function() {
			if ($("#autoAprovePayRoll").val() == "false") {
				$("#paySlipAprovers").show();
			} else {
				$("#paySlipAprovers").hide();
			}
		}

		updatePaySlip = function(rowCount) {
			if (validatePaySlipItems(rowCount)) {
				updateThisPaySlipItem(rowCount);
			} else {
				$(".pay-slp-err").html(" : Please fill all the fields.");
				return false;
			}
		}

		updateTaxRule = function(rowCount) {
			if (validateTaxRuleItems(rowCount)) {
				updateThisTaxRuleItem(rowCount);
			} else {
				$(".tax-rul-err").html(" : Please fill all the fields.");
				return false;
			}
		}

		updateePaySlip = function(rowCount) {
			if (validateExtraPaySlipItems(rowCount)) {
				updateThisExtraPaySlipItem(rowCount);
			} else {
				$(".epay-slp-err").html(" : Please fill all the fields.");
				return false;
			}
		}

		savePaySlipItem = function() {
			var rowCount = $('#pay-slip-items-table tr').length;
			if (validatePaySlipItems(parseInt(rowCount) - 1)) {
				saveThisPaySlipItem(parseInt(rowCount) - 1);
			} else {
				$(".pay-slp-err").html(" : Please fill all the fields.");
				return false;
			}
		}

		saveTaxRule = function() {
			var rowCount = $('#pr-tax-rule-tbl tr').length;
			if (validateTaxRuleItems(parseInt(rowCount) - 1)) {
				saveThisTaxRuleItem(parseInt(rowCount) - 1);
			} else {
				$(".tax-rul-err").html(" : Please fill all the fields.");
				return false;
			}
		}

		saveePaySlipItem = function() {
			var rowCount = $('#extra-pay-slip-items-table tr').length;
			if (validateExtraPaySlipItems(parseInt(rowCount) - 1)) {
				saveThisExtraPaySlipItem(parseInt(rowCount) - 1);
			} else {
				$(".epay-slp-err").html(" : Please fill all the fields.");
				return false;
			}
		}

		function addTRNewRow() {
			var rowCount = $('#pr-tax-rule-tbl tr').length;
			$(".tax-rul-err").html("");
			$('#pr-tax-rule-tbl tr:last-child td:last-child')
					.html(
							"<td><input type='button' value='Update' onclick='updateTaxRule("
									+ parseInt(rowCount)
									+ ")' id='addButton1' class='btn btn-info btn-xs'></td>");
			$("#pr-tax-rl-rw")
					.append(
							"<tr> <td>"
									+ parseInt(rowCount)
									+ "</td> <td><input type='text' class='form-control salaryFrom"
									+ parseInt(rowCount)
									+ "' name='salaryFrom' onkeyup='setExenptedAmont("
									+ parseInt(rowCount)
									+ ")' /></td> <td><input type='text' class='form-control salaryTo"
									+ parseInt(rowCount)
									+ "' name='salaryTo' onkeyup='setAdditionalAmount("
									+ parseInt(rowCount)
									+ ")' /></td> <td><input type='text' class='form-control taxPercentage"
									+ parseInt(rowCount)
									+ "' name='taxPercentage' /></td> <td><input type='text' class='form-control exemptedAmount"
									+ parseInt(rowCount)
									+ "' name='exemptedAmount' /></td> <td><input type='text' class='form-control additionalAmount"
									+ parseInt(rowCount)
									+ "' name='additionalAmount' /></td> <td><select class='form-control individual"
									+ parseInt(rowCount)
									+ "' name='individual'> <option value='Male'>Male</option> <option value='Female'>Female</option><option value='SeniorCitizen'>Senior Citizen</option><option value='SuperSenior'>Super Senior</option> </select></td> <td><input type='hidden' id='taxId"
									+ parseInt(rowCount)
									+ "' name='taxRuleId' value='0' /><input type='button' value='Save' onclick='saveTaxRule()' id='psButtonSave1' class='btn btn-info btn-xs'></td> </tr>");
		}

		function addPSNewRow() {
			var rowCount = $('#pay-slip-items-table tr').length;
			$(".pay-slp-err").html("");
			$('#pay-slip-items-table tr:last-child td:last-child')
					.html(
							"<td><input type='button' value='Update' onclick='updatePaySlip("
									+ parseInt(rowCount)
									+ ")' id='addButton1' class='btn btn-info btn-xs'></td>");
			$("#ps-div")
					.append(
							"<tr><td>"
									+ parseInt(rowCount)
									+ "</td> <td><input type='text' name='title' id='ps-kw-non-exp' class='form-control itemTitle"
									+ parseInt(rowCount)
									+ "' /></td> <td><select class='form-control itemType"
									+ parseInt(rowCount)
									+ "' name='type'> <option value=Calculation>Calculation</option> <option value='Fixed'>Fixed</option> </select></td> <td><select class='form-control itemTax"
									+ parseInt(rowCount)
									+ "' name='taxAllowance'> <option value='true'>Yes</option> <option value='false'>No</option> </select></td> <td><input type='text' class='form-control typeahead itemSyntax"
									+ parseInt(rowCount)
									+ "' name='calculation' id='ps-kw-exp' /></td> <td><input type='text'  name='effectFrom' class='form-control itemFrom"
									+ parseInt(rowCount)
									+ "' data-date-format='DD/MM/YYYY' id='dt-ps' /></td> <td><input type='hidden' id='itemId"
									+ parseInt(rowCount)
									+ "' name='paySlipItemId' value='0' /><input type='button' value='Save' onclick='savePaySlipItem()' id='psButtonSave"
									+ parseInt(rowCount)
									+ "' class='btn btn-info btn-xs'></td></tr>");
		}

		function addePSNewRow() {
			var rowCount = $('#extra-pay-slip-items-table tr').length;
			$(".epay-slp-err").html("");
			$('#extra-pay-slip-items-table tr:last-child td:last-child')
					.html(
							"<td><input type='button' value='Update' onclick='updateePaySlip("
									+ parseInt(rowCount)
									+ ")' id='addButton1' class='btn btn-info btn-xs'></td>");
			$("#extr-ps-div")
					.append(
							"<tr><td>"
									+ parseInt(rowCount)
									+ "</td> <td><input type='text' name='etitle' class='form-control itemeTitle"
									+ parseInt(rowCount)
									+ "' /></td> <td><select class='form-control itemeType"
									+ parseInt(rowCount)
									+ "' name='etype'> <option value=Calculation>Calculation</option> <option value='Fixed'>Fixed</option> </select></td> <td><select class='form-control itemeTax"
									+ parseInt(rowCount)
									+ "' name='etaxAllowance'> <option value='true'>Yes</option> <option value='false'>No</option> </select></td> <td><input type='text' class='form-control typeahead itemeSyntax"
									+ parseInt(rowCount)
									+ "' name='ecalculation' id='ps-kw-exp' /></td> <td><input type='text'  name='eeffectFrom' class='form-control itemeFrom"
									+ parseInt(rowCount)
									+ "' data-date-format='DD/MM/YYYY' id='dt-ps' /></td> <td><input type='hidden' id='itemeId"
									+ parseInt(rowCount)
									+ "' name='extraePaySlipItemId' value='0' /><input type='button' value='Save' onclick='saveePaySlipItem()' id='epsButtonSave"
									+ parseInt(rowCount)
									+ "' class='btn btn-info btn-xs'></td></tr>");
		}

		setExenptedAmont = function(row) {
			$(".exemptedAmount" + row).val($(".salaryFrom" + row).val());
		}

		validatePaySlipItems = function(row) {
			if ($(".itemTitle" + row).val().trim().length === 0
					|| $(".itemSyntax" + row).val().trim().length === 0
					|| $(".itemFrom" + row).val().trim().length === 0) {
				return false;
			}
			return true;
		}

		validateTaxRuleItems = function(row) {
			if ($(".salaryFrom" + row).val().trim().length === 0
					|| $(".salaryTo" + row).val().trim().length === 0
					|| $(".taxPercentage" + row).val().trim().length === 0
					|| $(".exemptedAmount" + row).val().trim().length === 0
					|| $(".additionalAmount" + row).val().trim().length === 0) {
				return false;
			}
			return true;
		}

		validateExtraPaySlipItems = function(row) {
			if ($(".itemeTitle" + row).val().trim().length === 0
					|| $(".itemeSyntax" + row).val().trim().length === 0
					|| $(".itemeFrom" + row).val().trim().length === 0) {
				return false;
			}
			return true;
		}

		function addExtraPaySlipItem() {
			var rowCount = $('#extra-pay-slip-items-table tr').length;
			$('#extra-pay-slip-items-table tr:last-child td:last-child').html(
					"");
			$("#extr-ps-div")
					.append(
							"<tr><td>"
									+ parseInt(rowCount)
									+ "</td> <td><input type='text' class='form-control' value='DA' /></td> <td><select class='form-control' name='type'> <option>Calculation</option> <option>Fixed</option> </select></td> <td><select class='form-control' name='tax'> <option>Yes</option> <option>No</option> </select></td> <td><input type='text' class='form-control typeahead' name='calculation' id='ps-kw-exp' /></td> <td><input type='text'  name='loanDate' class='form-control' data-date-format='DD/MM/YYYY' id='dt-ps' /></td> <td><input type='button' value='Add Row' onclick='addExtraPaySlipItem()' id='addButton1'></td></tr>")
		}

		function addTaxRule() {
			var rowCount = $('#pr-tax-rule-tbl tr').length;
			$('#pr-tax-rule-tbl tr:last-child td:last-child').html("");
			$("#pr-tax-rl-rw")
					.append(
							"<tr><td>"
									+ parseInt(rowCount)
									+ "</td> <td><input type='text' class='form-control' name='salf' /></td> <td><input type='text' class='form-control' name='salt' /></td> <td><input type='text' class='form-control' name='taxp' /></td> <td><input type='text' class='form-control' name='eta' /></td> <td><input type='text' class='form-control' name='ata' /></td> <td><select class='form-control' name='indi'> <option>male</option> <option>female</option> </select></td> <td><input type='button' value='Add Row' onclick='addTaxRule()' id='adtButton3'></td></tr>")
		}
	</script>
	<script>
		$(function() {
			$('body').on('focus', "#dt-ps", function() {
				$(this).datetimepicker({
					pickTime : false
				});
			});
		});
	</script>

	<script>
		var select = document.getElementById('test'), onChange = function(event) {
			var shown = this.options[this.selectedIndex].value == "Days";

			document.getElementById('hidden_div').style.display = shown ? 'block'
					: 'none';
		};

		if (select.addEventListener) {
			select.addEventListener('change', onChange, false);
		} else {
			select.attachEvent('onchange', function() {
				onChange.apply(select, arguments);
			});
		}
	</script>

	<script>
		$(document)
				.ready(
						function() {
							if ($("#autoAprovePayRoll").val() == "false") {
								$("#paySlipAprovers").show();
							} else {
								$("#paySlipAprovers").hide();
							}
							
							$("#dashbordli").removeClass("active");
							$("#organzationli").removeClass("active");
							$("#recruitmentli").removeClass("active");
							$("#employeeli").removeClass("active");
							$("#timesheetli").removeClass("active");
							$("#payrollli").addClass("active");
							$("#reportsli").removeClass("active");
							
							showMe('slip');

							var select = document.getElementById('test');
							var shown = select.options[select.selectedIndex].value == "Days";
							document.getElementById('hidden_div').style.display = shown ? 'block'
									: 'none';
							if (select.addEventListener) {
								select.addEventListener('change', onChange,
										false);
							} else {
								select.attachEvent('onchange', function() {
									onChange.apply(select, arguments);
								});
							}
						});
	</script>


	<!-- <script>
		var select = document.getElementById('absence'), onChange = function(
				event) {
			var shown = this.options[this.selectedIndex].value == 1;

			document.getElementById('abs').style.display = shown ? 'block'
					: 'none';
		};

		if (select.addEventListener) {
			select.addEventListener('change', onChange, false);
		} else {
			select.attachEvent('onchange', function() {
				onChange.apply(select, arguments);
			});
		}
	</script> -->

	<!-- <script>
		var select = document.getElementById('pay'), onChange = function(event) {
			var shown = this.options[this.selectedIndex].value == 1;

			document.getElementById('aps').style.display = shown ? 'block'
					: 'none';
		};

		if (select.addEventListener) {
			select.addEventListener('change', onChange, false);
		} else {
			select.attachEvent('onchange', function() {
				onChange.apply(select, arguments);
			});
		}
	</script> -->

	<script type="text/javascript">
		function showMe(box) {
			var chboxs = $("#perDaySalaryCalculation");
			var vis = "none";
			for (var i = 0; i < chboxs.length; i++) {
				if (chboxs[i].checked) {
					vis = "block";
					break;
				}
			}
			document.getElementById(box).style.display = vis;
		}
	</script>

	<!-- <script>
		$('#js-smart-tabs').smartTabs();
		$('#js-smart-tabs--tabs').smartTabs({
			layout : 'tabs'
		});
		$('#js-smart-tabs--accordion').smartTabs({
			layout : 'accordion'
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#admin_ss").click(function() {
				$("#add_administartor").show();
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addfield").click(function() {
				$("#add_custom_feild").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addnot").click(function() {
				$("#add_notification").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addrm").click(function() {
				$("#add_rm").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
			if ($.isFunction($.fn.datetimepicker)) {
				$("#datepickerps").datetimepicker({
					pickTime : false
				});
			}
		});
	</script> -->
</body>
</html>
