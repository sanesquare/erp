<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Employees</h1>
		</div>


		<div class="row">
			<div class="col-md-12">
				<a href="employees.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>


		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
					<c:if
						test="${ rolesMap.roles['Employee Detailed Information'].view}">
						<li role="presentation"><a href="#docs" role="tab"
							data-toggle="tab">Additional Information</a></li>
					</c:if>
				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Employee Information</div>
											<div class="panel-body">

												<div class="form-group">
													<div class="photo_show">
														<img src='<c:url value="${employee.profilePic }" />' />
													</div>
												</div>

												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3 control-label">Employee
														Type</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.employeeType }
														</label>
													</div>


												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Company</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. employeeCategory}
														</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3 control-label">Designation</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.designation }
														</label>
													</div>

												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Branch</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.employeeBranch }
														</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Department</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. employeeDepartment}</label>
													</div>
												</div>




												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3 control-label">Grade</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. employeeGrade}
														</label>
													</div>

												</div>








												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Work
														Shift</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.workShift }</label>
													</div>
												</div>








											</div>
										</div>
									</div>



									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Employee User Information</div>
											<div class="panel-body">


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Allow
														Employee Login </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. allowLogin}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Username
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.userName }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">E-mail
														address</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. email}</label>
													</div>
												</div>










											</div>
										</div>
									</div>


									<%-- <div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Employee Notifications</div>
											<div class="panel-body">


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notification
														By email </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.notifyByEmail }</label>
													</div>
												</div>
											</div>
										</div>
									</div> --%>



									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Employee Reporting</div>
											<div class="panel-body">


												<div class="form-group">
													<label class="col-sm-3 control-label">Superiors</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label"><c:forEach
																items="${employee. superiorSubordinateVo.superiors}"
																var="s">
                                  ${s.superiorCode } , 
                                  </c:forEach></label>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Subordinate </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label"><c:forEach
																items="${employee. superiorSubordinateVo.subordinates}"
																var="s">
                                  ${s.subordinateCode } ,
                                  </c:forEach> </label>
													</div>
												</div>





											</div>
										</div>
									</div>



									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Employee Personal
												Information</div>
											<div class="panel-body">




												<div class="form-group">
													<label class="col-sm-3 control-label">Salutation</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. salutation}</label>
													</div>

												</div>



												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">First
														Name </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.firstName }</label>
													</div>
												</div>



												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Last
														Name </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.lastName }</label>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Date of Birth</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.dateOfBirth }</label>
													</div>

												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Gender
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.gender }</label>
													</div>
												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label">Blood Group</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. bloodGroup}</label>
													</div>

												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label">Religion </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.relegion }</label>
													</div>

												</div>






												<div class="form-group">
													<label class="col-sm-3 control-label">Nationality</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.nationality }</label>
													</div>
												</div>



											</div>
										</div>
									</div>


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Employee Additional
												Information</div>
											<div class="panel-body">




												<div class="form-group">
													<label class="col-sm-3 control-label">Joining Date</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. joiningDate}</label>
													</div>

												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">PF Id</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. pfId}</label>
													</div>

												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">ESI Id</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. esiId}</label>
													</div>

												</div>



												<div class="form-group">
													<label class="col-sm-3 control-label">Passport
														Expiration</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.passportExpiration }</label>
													</div>

												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Passport
														Number</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.passportNumber }</label>
													</div>
												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label">Driving
														License Expiration</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.licenceExpiration }</label>
													</div>

												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Driving
														License Number</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.licenceNumber }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Government
														Id / Social Security </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. goovernmentId}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Employee
														Tax Number </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.employeeTaxNumber }</label>
													</div>
												</div>





											</div>
										</div>
									</div>




								</div>

								<div class="col-md-6">

									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Permanent Contact
												Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Address
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.address }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">City
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.city }</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														State / Province</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.state }
														</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Zip
														Code / Postal Code </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. zipCode}</label>
													</div>
												</div>







												<div class="form-group">
													<label class="col-sm-3 control-label">Country</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. country}</label>
													</div>
												</div>






											</div>
										</div>
									</div>


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Phone Number</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Home
														Phone Number</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.homePhoneNumber }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Office
														Phone Number </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.officePhoneNumber }</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Mobile
														Number</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.mobileNumber }</label>
													</div>
												</div>





											</div>
										</div>
									</div>



									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Emergency Contact</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Emergency
														Contact Person </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.emergencyPerson }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Relationship
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.emergencyRelation }</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Phone
														Number</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. emergencyMobile}</label>
													</div>
												</div>




											</div>
										</div>
									</div>


									<div class="col-md-12">

										<div class="panel panel-default">
											<div class="panel-heading">Leaves</div>
											<div class="panel-body">

												<table class="table no-margn">
													<thead>
														<tr>
															<th>#</th>
															<th>Leave Type</th>
															<th>Leaves Allowed</th>
															<th>Paid/Unpaid</th>
															<th>Carry Over</th>
															<th>Status</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${employee.leaveVos}" var="b"
															varStatus="s">
															<tr>
																<td>${s.count}</td>
																<td>${b.leaveTitle }</td>
																<td>${b.leaves }</td>
																<td>${b.leaveType }</td>
																<td>${b.carryOver }</td>
																<td>${b.leaveStatus }</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>






											</div>
										</div>

									</div>



									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${employee.notes }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee. createdBy}</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employee.createdOn }</label>
													</div>
												</div>




											</div>
										</div>
									</div>

								</div>
							</div>



						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">



							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">


										<table class="table no-margn">
											<thead>
												<tr>
													<th>#</th>
													<th>Document Name</th>
													<c:if test="${ rolesMap.roles['Employee Documents'].view}">
														<th>view</th>
													</c:if>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${employee.documentsVos }" var="d"
													varStatus="s">
													<tr>
														<td>${s.count }</td>
														<td>${d.fileName }</td>
														<c:if test="${ rolesMap.roles['Employee Documents'].view}">
															<td><a href="downloadEmpDoc.do?durl=${d.documnetUrl }"><i
																	class="fa fa-file-text-o sm"></i></a></td>
														</c:if>
													</tr>
												</c:forEach>

											</tbody>
										</table>


									</div>
								</div>
							</div>




						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-6">
									<c:if test="${ rolesMap.roles['Employee Qualifications'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Add Qualifications</div>
												<div class="panel-body">



													<table class="table no-margn">
														<thead>
															<tr>
																<th>Degree</th>
																<th>Subject</th>
																<th>Institute</th>
																<th>Grade / GPA</th>
																<th>Graduation Year</th>
															</tr>
														</thead>
														<tbody>

															<c:forEach items="${employee.qualifications }" var="q">
																<tr>
																	<td>${q.degree }</td>
																	<td>${q.subject }</td>
																	<td>${q.institute }</td>
																	<td>${q.gpa }</td>
																	<td>${q.graduationYear }</td>
																</tr>
															</c:forEach>

														</tbody>
													</table>


												</div>
											</div>
										</div>
									</c:if>


									<c:if test="${ rolesMap.roles['Employee Languages'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Language</div>
												<div class="panel-body">

													<table class="table no-margn">
														<thead>
															<tr>
																<th>Language</th>
																<th>Speaking Level</th>
																<th>Reading Level</th>
																<th>Writing Level</th>

															</tr>
														</thead>
														<tbody>
															<c:forEach items="${employee.employeeLanguages }" var="l">
																<tr>
																	<td>${l.language }</td>
																	<td>${l.speakingLevel }</td>
																	<td>${l.readingLevel }</td>
																	<td>${l.writingLevel }</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>

												</div>
											</div>
										</div>
									</c:if>


									<c:if
										test="${ rolesMap.roles['Employee Work Experiences'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Work Experience</div>
												<div class="panel-body">

													<table class="table no-margn">
														<thead>
															<tr>
																<th>Organization Name</th>
																<th>Job Designation</th>
																<th>Job Field</th>
																<th>Job Description</th>
																<th>Start Date</th>
																<th>End Date</th>
																<th>Starting Salary</th>
																<th>Ending Salary</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${employee.experiences }" var="e">
																<tr>
																	<td>${e. organisation}</td>
																	<td>${e. designation}</td>
																	<td>${e. jobField}</td>
																	<td>${e. description}</td>
																	<td>${e. startDate}</td>
																	<td>${e. endDate}</td>
																	<td>${e. startingSalary}</td>
																	<td>${e. endingSalary}</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>


												</div>
											</div>
										</div>
									</c:if>



									<c:if test="${ rolesMap.roles['Employee Bank Accounts'].view}">
										<div class="col-md-12">

											<div class="panel panel-default">
												<div class="panel-heading">Bank Account</div>
												<div class="panel-body">

													<table class="table no-margn">
														<thead>
															<tr>
																<th>Bank Name</th>
																<th>Branch Name</th>
																<th>Branch Code / Routing Number</th>
																<th>Account Title</th>
																<th>Account Number</th>
																<th>Account Type</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${employee.bankAccountVo}" var="b">
																<tr>
																	<td>${b.bankName }</td>
																	<td>${b.branchName }</td>
																	<td>${b.branchCode }</td>
																	<td>${b.accountTitle }</td>
																	<td>${b.accountNumber }</td>
																	<td>${b.accountType }</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>






												</div>
											</div>

										</div>
									</c:if>


									<c:if test="${ rolesMap.roles['Employee Benefits'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Benefit</div>
												<div class="panel-body">

													<table class="table no-margn">
														<thead>
															<tr>
																<th>Benefit Title</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${employee.benefits }" var="b">
																<tr>
																	<td>${b.benefit }</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>






												</div>
											</div>
										</div>
									</c:if>








								</div>





								<div class="col-md-6">

									<c:if
										test="${ rolesMap.roles['Employee Organization Assets'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Organization Asset</div>
												<div class="panel-body">
													<table class="table no-margn">
														<thead>
															<tr>
																<th>Asset Name</th>
																<th>Asset Code</th>
																<th>Asset Checkout Date</th>
																<th>Asset CheckIn Date</th>

															</tr>
														</thead>
														<tbody>
															<c:forEach items="${employee.organizationAssets }"
																var="a">
																<tr>
																	<td>${a.assetName }</td>
																	<td>${a.assetCode }</td>
																	<td>${a.assetCheckout }</td>
																	<td>${a.assetCheckin }</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>

												</div>
											</div>
										</div>
									</c:if>


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Uniform</div>
											<div class="panel-body">
												<table class="table no-margn">
													<thead>
														<tr>
															<th>Checkout Date</th>
															<th>Advance Amount</th>
															<th>Duration</th>
															<th>Reimbursement Date</th>

														</tr>
													</thead>
													<tbody>
														<c:forEach items="${employee.uniforms }" var="u">
															<tr>
																<td>${u.checkout }</td>
																<td>${u. amount}</td>
																<td>${u.duration }</td>
																<td>${u.reimbursemnt }</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>

											</div>
										</div>
									</div>


									<c:if test="${ rolesMap.roles['Employee Skills'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">SKills</div>
												<div class="panel-body">
													<table class="table no-margn">
														<thead>
															<tr>
																<th>Skill</th>
																<th>Skill Level</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${employee.skills }" var="s">
																<tr>
																	<td>${s.skill }</td>
																	<td>${s.skillLevel }</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>

												</div>
											</div>
										</div>
									</c:if>



									<c:if test="${ rolesMap.roles['Employee References'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Reference</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Reference
															Name </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${employee.referenceVo.referenceName }</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Organisation
															Name </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${employee.referenceVo.organisationName }</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Phone
															Number</label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${employee.referenceVo.phone }</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Email
															Address</label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${employee.referenceVo.email }</label>
														</div>
													</div>


												</div>
											</div>
										</div>
									</c:if>


									<c:if test="${ rolesMap.roles['Employee Dependants'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Dependants</div>
												<div class="panel-body">

													<table class="table no-margn">
														<thead>
															<tr>
																<th>Dependant Name</th>
																<th>Date of Birth</th>
																<th>Relation</th>
																<th>Address</th>
																<th>Nominee</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${employee.dependants }" var="d">
																<tr>
																	<td>${d. dependantName}</td>
																	<td>${d. datoeOfBirth}</td>
																	<td>${d. relation}</td>
																	<td>${d. address}</td>
																	<td>${d. nominee}</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>



												</div>
											</div>
										</div>
									</c:if>


									<c:if test="${ rolesMap.roles['Employee Next of Kins'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Next Of Kin</div>
												<div class="panel-body">

													<table class="table no-margn">
														<thead>
															<tr>
																<th>Next Of Kin Name</th>
																<th>Date of Birth</th>
																<th>Relation</th>
																<th>Address</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${employee.nextOfKins }" var="n">
																<tr>
																	<td>${n.name }</td>
																	<td>${n.dateOfBirth }</td>
																	<td>${n.relation }</td>
																	<td>${n.address }</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>





												</div>
											</div>
										</div>
									</c:if>

									<div class="col-md-12">
										<c:if test="${ rolesMap.roles['Employee Status Update'].view}">
											<div class="panel panel-default">
												<div class="panel-heading">Status</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Status </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-4 control-label">
																${employee.status } </label>

														</div>



													</div>
												</div>
											</div>
										</c:if>


									</div>
								</div>









							</div>
						</div>
					</div>
				</div>


			</div>




		</div>
	</div>
	<!-- Content Block Ends Here (right box)-->



	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>

</body>
</html>