<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Deductions</h1>
		</div>



		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Deductions</div>
					<div class="panel-body">
						<c:if test="${! empty deductions }">
							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered" id="basic-datatable">
								<thead>
									<tr>
										<th width="20%">Title</th>
										<th width="20%">Employee Name</th>
										<th width="20%">Amount</th>
										<c:if test="${ rolesMap.roles['Deductions'].update}">
											<th width="10%">Edit</th>
										</c:if>
										<c:if test="${ rolesMap.roles['Deductions'].delete}">
											<th width="10%">Delete</th>
										</c:if>

									</tr>
								</thead>
								<tbody>
									<c:forEach items="${deductions }" var="d">
										<tr>
											<td>${d.title }</td>
											<c:choose>
												<c:when test="${ rolesMap.roles['Deductions'].view}">
													<td><a href="viewDeduction.do?id=${d.deductionId }">${d.employee }</a></td>
												</c:when>
												<c:otherwise>
													<td>${d.employee }</td>
												</c:otherwise>
											</c:choose>

											<td>${d.amount }</td>
											<c:if test="${ rolesMap.roles['Deductions'].update}">
												<td><a href="editDeduction.do?id=${d.deductionId }"><i
														class="fa fa-edit sm"></i> </a></td>
											</c:if>
											<c:if test="${ rolesMap.roles['Deductions'].delete}">
												<td><a class="delete_ded"
													onclick="confirmDelete(${d.deductionId })"><i
														class="fa fa-close ss"></i> </a></td>
											</c:if>
										</tr>
									</c:forEach>


								</tbody>
							</table>
						</c:if>
						<c:if test="${ rolesMap.roles['Deductions'].add}">
							<a href="addDeduction.do">
								<button type="button" class="btn btn-primary">Add New
									Deduction</button>
							</a>
						</c:if>
					</div>
				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->


	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>


	<!-- Custom JQuery -->

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script type="text/javascript">
	
	function confirmDelete(id){
		var result=confirm("Are you sure to delete?");
		 if(result){
			$(".delete_ded").attr("href","deleteDeduction?id="+id);
		} else{
			$(".delete_ded").attr("href","#");
		} 
	}
	
	$(document).ready(function(){
		$("#dashbordli").removeClass("active");
		$("#organzationli").removeClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").addClass("active");
		$("#reportsli").removeClass("active");
	});
	</script>
</body>
</html>