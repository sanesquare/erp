<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Salaries</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>



		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Salary</div>
					<div class="panel-body">
						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="20%">Employee</th>
									<th width="20%">With Effect From</th>
									<th width="20%">Type</th>
									<th width="20%">Salary</th>
									<th width="5%">Delete</th>
								</tr>
							</thead>
							<tbody>

								<c:forEach items="${salaries }" var="s">
									<tr>
										<td>${s.employeeCode }</td>
										<td>${s.withEffectFrom }</td>
										<td>${s.salaryType }<input type="hidden"
											value="${s.salaryType }" id="salTyp"> <input
											type="hidden" value="${s.salaryId }" id="salId">
										</td>
										<td>${s.amount }</td>
										<td><a class="delete_sal" onclick="confirmDelete()"><i
												class="fa fa-times"></i> </a></td>
									</tr>
								</c:forEach>


							</tbody>
						</table>
						<a href="addSalary.do">
							<button type="button" class="btn btn-primary">Add Salary</button>
						</a>
					</div>
				</div>
			</div>


		</div>




	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>


	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script type="text/javascript">
		function confirmDelete() {
			var result = confirm("Are you sure to delete?");
			if (result) {
				$(".delete_sal").attr(
						"href",
						"deleteSalary.do?id=" + $("#salId").val() + "&type="
								+ $("#salTyp").val());
			} else {
				$(".delete_sal").attr("href", "#");
			}
		}

		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>