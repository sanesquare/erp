<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SNOGOL HRM</title>
<meta name="description" content="">

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="assets/css/bootstrap/bootstrap.css" />

<!-- Fonts  -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet" href="assets/css/app/app.v1.css" />

<!-- Chosen Select  -->
<link rel="stylesheet" href="assets/css/plugins/bootstrap-chosen/chosen.css" />

<!-- DateTime Picker  -->
<link rel="stylesheet" href="assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css" />
<link href="assets/css/RegalCalendar.css" rel="stylesheet" type="text/css" />
</head>
<body data-ng-app>
<aside class="left-panel collapsed">
  <div class="user text-center"> <img src="assets/images/logo1.png" class="img-circle"> </div>
  <nav class="navigation">
    <ul class="list-unstyled">
      <li><a href="index.html"><i class="fa fa-home"></i><span class="nav-label">Home</span></a></li>
      <li class="has-submenu"><a href="#"><i class="fa fa-building-o"></i> <span class="nav-label">Organisation</span></a>
        <ul class="list-unstyled">	 <li><a href="stations.html"><i class="fa fa-building-o"></i>Stations</a></li>
                             <li><a href="departments.html"><i class="fa fa-sitemap"></i>Departments</a></li>
                                  <li><a href="projects.html"><i class="fa fa-file-o"></i>Projects</a></li>
                                   <li><a href="meeting.html"><i class="fa fa-users"></i>Meetings</a></li>
                            <li><a href="announcements.html"><i class="fa fa-bullhorn"></i>Announcements</a></li>
                              <li><a href="system_settings.html"><i class="fa fa-cog"></i>System Settings</a></li>
                              <li><a href="system_logs.html"><i class="glyphicon glyphicon-log-in"></i>System Log</a></li>
                            
        </ul>
      </li>
      <li class="has-submenu"><a href="#"><i class="fa fa-star-o"></i> <span class="nav-label">Recruitment</span></a>
        <ul class="list-unstyled">
            <li><a href="job_posts.html"><i class="glyphicon glyphicon-lock"></i>Job Posts</a></li> 
                              <li><a href="job_candidates.html"><i class="glyphicon glyphicon-user"></i>Job Candidates</a></li>
                           <li><a href="job_interviews.html"><i class="glyphicon glyphicon-comment"></i>Job Interviews</a></li>
        </ul>
      </li>
      <li class="has-submenu active"><a href="#"><i class="fa fa-users"></i> <span class="nav-label">Employees</span></a>
        <ul class="list-unstyled"> <li><a href="employees.html"><i class="fa fa-user"></i>Employee</a></li>
                            <li><a href="add_employee_joining.html"><i class="glyphicon glyphicon-log-in"></i>Employees Joining</a></li>
                            <li><a href="contract.html"><i class="fa fa-file-text"></i>Contracts</a></li>
                            <li><a href="assignment.html"><i class="glyphicon glyphicon-briefcase"></i>Assignments</a></li>
                            <li><a href="transfer.html"><i class="glyphicon glyphicon-refresh"></i>Transfers</a></li>
                             <li><a href="resignation.html"><i class="fa fa-comment-o"></i></i>Resignations</a></li>
                            <li><a href="Achievement.html"><i class="fa fa-trophy"></i>Achievements</a></li>
                             <li><a href="travel.html"><i class="fa fa-fighter-jet"></i>Travels</a></li>
                            <li><a href="promotion.html"><i class="fa fa-star-o"></i>Promotions</a></li> 
                            <li><a href="complaints.html"><i class="glyphicon glyphicon-warning-sign"></i>Complaints</a></li>
                            <li><a href="warning.html"><i class="glyphicon glyphicon-warning-sign"></i>Warnings</a></li> 
                            <li><a href="memos.html"><i class="fa fa-list-alt"></i>Memos</a></li>
                            <li><a href="termination.html"><i class="fa fa-close"></i>Terminations</a></li>
                            <li><a href="performance_evaluation.html"><i class="glyphicon glyphicon-star"></i>Performance Evaluations</a></li> 
                            <li><a href="employees_exit.html"><i class="glyphicon glyphicon-log-out"></i>Employee Exit</a></li>
                            
        </ul>
      </li>
      <li class="has-submenu"><a href="#"><i class="fa fa-clock-o"></i> <span class="nav-label">Timesheet</span></a>
        <ul class="list-unstyled"><li><a href="attendance.html"><i class="fa fa-clock-o"></i>Attendance </a></li>
                            <li><a href="leaves.html"><i class="fa fa-envelope-o"></i>Leaves</a></li>
                            <li><a href="worksheet.html"><i class="fa fa-columns"></i>Worksheet </a></li>
                            <li><a href="work_shift.html"><i class="glyphicon glyphicon-random"></i>Work Shifts</a></li>
                            <li><a href="holiday.html"><i class="glyphicon glyphicon-calendar"></i>Holidays </a></li>
        </ul>
      </li>
      <li class="has-submenu"><a href="#"><i class="fa fa-calculator"></i> <span class="nav-label">Payroll</span></a>
        <!--<!--<ul class="list-unstyled">
                        	<li><a href="#"><i class="fa fa-dollar"></i>Salary</a></li>
                            <li><a href="#"><i class="fa fa-money"></i>Salary Payslips</a></li>
                            <li><a href="#"><i class="fa fa-database"></i>Payroll Structure</a></li>
                            <li><a href="#"><i class="fa fa-reorder"></i>Hourly Wages</a></li>
                            <li><a href="#"><i class="fa fa-minus-square-o"></i>Deductions</a></li>
                            <li><a href="#"><i class="fa fa-plus-square-o"></i>Bonuses</a></li>
                            <li><a href="#"><i class="fa fa-calculator"></i>Commissions</a></li>
                            <li><a href="#"><i class="fa fa-money"></i>Adjustments</a></li>
                            <li><a href="#"><i class="fa fa-money"></i>Reimbursements</a></li>
                            <li><a href="#"><i class="fa fa-clock-o"></i>Overtimes</a></li>
                            <li><a href="#"><i class="fa fa-cube"></i>Provident Fund</a></li>
                            <li><a href="#"><i class="fa fa-dollar"></i>Advance Salary</a></li>
                            <li><a href="#"><i class="fa fa-calculator"></i>Loans</a></li>
                            <li><a href="#"><i class="fa fa-life-ring"></i>Insurance</a></li>
                        </ul>-->
      </li>
      <li class="has-submenu"><a href="#"><i class="fa fa-pie-chart"></i> <span class="nav-label">Reports</span></a>
        <!--<ul class="list-unstyled">
          <li><a href="#"> <i class="fa fa-user"></i> HR Report</a></li>
          <li><a href="#"><i class="glyphicon glyphicon-briefcase"></i>Recruitment Reports</a></li>
          <li><a href="#"><i class="fa fa-user"></i>Employees Reports</a></li>
          <li><a href="#"><i class="fa fa-clock-o"></i>Timesheet Reports</a></li>
          <li><a href="#"><i class="fa fa-money"></i>Payroll Reports</a></li>
          <li><a href="#"><i class="fa fa-graduation-cap"></i>Training Reports</a></li>
          <li><a href="#"><i class="fa fa-bar-chart"></i>Graphs</a></li>
          <li><a href="#"><i class="fa fa-cogs"></i>Report Generator</a></li>
        </ul>-->
      </li>
     <!-- <li class="has-submenu"><a href="#"><i class="fa fa-question-circle"></i><span class="nav-label">Help</span></a>
        <ul class="list-unstyled">
          <li><a href="#"><i class="fa fa-list"></i>Help Topics</a></li>
          <li><a href="#"><i class="fa fa-comments-o"></i>FAQ's</a></li>
          <li><a href="#"><i class="fa fa-ticket"></i>Support Tickets</a></li>
          <li><a href="#"><i class="fa fa-mobile-phone"></i>Request a Call Bank</a></li>
          <li><a href="#"><i class="fa fa-envelope-o"></i>Contact Us</a></li>
          <li><a href="#"><i class="fa fa-headphones"></i>Live Support</a></li>
        </ul>
      </li>-->
    </ul>
  </nav>
</aside>
<!-- Aside Ends-->

<section class="content">
  <header class="top-head container-fluid">
    <button type="button" class="navbar-toggle pull-left"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    <form role="search" class="navbar-left app-search pull-left hidden-xs">
      <input type="text" placeholder="Enter keywords..." class="form-control form-control-circle">
    </form>
    <nav class=" navbar-default hidden-xs" role="navigation">
      <ul class="nav navbar-nav">
        <li><a href="#">Link</a></li>
        <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#">Dropdown <span class="caret"></span></a>
          <ul role="menu" class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
    </nav>
    <ul class="nav-toolbar">
      <li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-comments-o"></i> <span class="badge bg-warning">7</span></a>
        <div class="dropdown-menu md arrow pull-right panel panel-default arrow-top-right messages-dropdown">
          <div class="panel-heading"> Messages </div>
          <div class="list-group"> <a href="#" class="list-group-item">
            <div class="media">
              <div class="user-status busy pull-left"> <img class="media-object img-circle pull-left" src="assets/images/avtar/user2.png" alt="user#1" width="40"> </div>
              <div class="media-body">
                <h5 class="media-heading">Lorem ipsum dolor sit consect....</h5>
                <small class="text-muted">23 Sec ago</small> </div>
            </div>
            </a> <a href="#" class="list-group-item">
            <div class="media">
              <div class="user-status offline pull-left"> <img class="media-object img-circle pull-left" src="assets/images/avtar/user3.png" alt="user#1" width="40"> </div>
              <div class="media-body">
                <h5 class="media-heading">Nunc elementum, enim vitae</h5>
                <small class="text-muted">23 Sec ago</small> </div>
            </div>
            </a> <a href="#" class="list-group-item">
            <div class="media">
              <div class="user-status invisibled pull-left"> <img class="media-object img-circle pull-left" src="assets/images/avtar/user4.png" alt="user#1" width="40"> </div>
              <div class="media-body">
                <h5 class="media-heading">Praesent lacinia, arcu eget</h5>
                <small class="text-muted">23 Sec ago</small> </div>
            </div>
            </a> <a href="#" class="list-group-item">
            <div class="media">
              <div class="user-status online pull-left"> <img class="media-object img-circle pull-left" src="assets/images/avtar/user5.png" alt="user#1" width="40"> </div>
              <div class="media-body">
                <h5 class="media-heading">In mollis blandit tempor.</h5>
                <small class="text-muted">23 Sec ago</small> </div>
            </div>
            </a> <a href="#" class="btn btn-info btn-flat btn-block">View All Messages</a> </div>
        </div>
      </li>
      <li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-bell-o"></i><span class="badge">3</span></a>
        <div class="dropdown-menu arrow pull-right md panel panel-default arrow-top-right notifications">
          <div class="panel-heading"> Notification </div>
          <div class="list-group"> <a href="#" class="list-group-item">
            <p>Installing App v1.2.1<small class="pull-right text-muted">45% Done</small></p>
            <div class="progress progress-xs no-margn progress-striped active">
              <div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%"> <span class="sr-only">45% Complete</span> </div>
            </div>
            </a> <a href="#" class="list-group-item"> Fusce dapibus molestie tincidunt. Quisque facilisis libero eget justo iaculis </a> <a href="#" class="list-group-item">
            <p>Server Status</p>
            <div class="progress progress-xs no-margn">
              <div class="progress-bar progress-bar-success" style="width: 35%"> <span class="sr-only">35% Complete (success)</span> </div>
              <div class="progress-bar progress-bar-warning" style="width: 20%"> <span class="sr-only">20% Complete (warning)</span> </div>
              <div class="progress-bar progress-bar-danger" style="width: 10%"> <span class="sr-only">10% Complete (danger)</span> </div>
            </div>
            </a> <a href="#" class="list-group-item">
            <div class="media"> <span class="label label-danger media-object img-circle pull-left">Danger</span>
              <div class="media-body">
                <h5 class="media-heading">Lorem ipsum dolor sit consect..</h5>
              </div>
            </div>
            </a> <a href="#" class="list-group-item">
            <p>Server Status</p>
            <div class="progress progress-xs no-margn">
              <div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-info"> <span class="sr-only">60% Complete (warning)</span> </div>
            </div>
            </a> </div>
        </div>
      </li>
      <li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-user"></i></a>
        <div class="dropdown-menu arrow pull-right md panel panel-default arrow-top-right notifications">
          <div class="list-group"> <a href="#" class="list-group-item"> Uder Profile </a> <a href="#" class="list-group-item"> Logout </a> </div>
        </div>
      </li>
      <li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
        <div class="dropdown-menu lg pull-right arrow panel panel-default arrow-top-right">
          <div class="panel-heading"> More Apps </div>
          <div class="panel-body text-center">
            <div class="row">
              <div class="col-xs-6 col-sm-4"><a href="#" class="text-green"><span class="h2"><i class="fa fa-envelope-o"></i></span>
                <p class="text-gray no-margn">Messages</p>
                </a></div>
              <div class="col-xs-6 col-sm-4"><a href="#" class="text-purple"><span class="h2"><i class="fa fa-calendar-o"></i></span>
                <p class="text-gray no-margn">Events</p>
                </a></div>
              <div class="col-xs-12 visible-xs-block">
                <hr>
              </div>
              <div class="col-xs-6 col-sm-4"><a href="#" class="text-red"><span class="h2"><i class="fa fa-comments-o"></i></span>
                <p class="text-gray no-margn">Chatting</p>
                </a></div>
              <div class="col-lg-12 col-md-12 col-sm-12  hidden-xs">
                <hr>
              </div>
              <div class="col-xs-6 col-sm-4"><a href="#" class="text-yellow"><span class="h2"><i class="fa fa-folder-open-o"></i></span>
                <p class="text-gray">Folders</p>
                </a></div>
              <div class="col-xs-12 visible-xs-block">
                <hr>
              </div>
              <div class="col-xs-6 col-sm-4"><a href="#" class="text-primary"><span class="h2"><i class="fa fa-flag-o"></i></span>
                <p class="text-gray">Task</p>
                </a></div>
              <div class="col-xs-6 col-sm-4"><a href="#" class="text-info"><span class="h2"><i class="fa fa-star-o"></i></span>
                <p class="text-gray">Favorites</p>
                </a></div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </header>
  <!-- Header Ends -->
  
  <div class="warper container-fluid">
    <div class="page-header">
      <h1>Promotions</h1>
    </div>
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#all" role="tab" data-toggle="tab">Information</a></li>
          <li role="presentation"><a href="#users" role="tab" data-toggle="tab">Documents</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="panel panel-default tab-pane tabs-up active" id="all">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">Promotion Information</div>
                      <div class="panel-body">
                      
                      <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Employee</label>
                           <div class="col-sm-9">
                                  <label for="inputPassword3" class="col-sm-9 control-label">25-2-2015 </label>
                                </div>
                        </div>
                      
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Promotion</label>
                           <div class="col-sm-9">
                                  <label for="inputPassword3" class="col-sm-9 control-label">25-2-2015 </label>
                                </div>
                        </div>
                        
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label"> Forward Application To</label>
                           <div class="col-sm-9">
                                  <label for="inputPassword3" class="col-sm-9 control-label">25-2-2015 </label>
                                </div>
                        </div>
                        
                        
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Promotion Date</label>
                           <div class="col-sm-9">
                                  <label for="inputPassword3" class="col-sm-9 control-label">25-2-2015 </label>
                                </div>
                        </div>
                             
                          </div>
                        </div>
                      </div>
                      
                      
                      
                      
                      <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">Promotion Description</div>
                      <div class="panel-body">
                        <div class="form-group">
                          <div class="col-md-12">
                            <label for="inputPassword3" class="col-sm-12 control-label ft">sdsakfsdsjf dsj idjfidsj dsj ifcjdsij dsjfidjs dsifcjidsjdsfc oidsjds fcidsjdsjfjds fjdsjfoijdsjfjsdsfj dsjfdsj</label>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                      
                  
                </div>
                <div class="col-md-6">
                  
                  
                  
                  <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">Status</div>
                      <div class="panel-body">
                      
                      <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Status </label>
                          <div class="col-sm-9">
                                 <label for="inputEmail3" class="col-sm-3 control-label ft">odsrg</label>
                          </div>
                        </div>
                      
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Description </label>
                          <div class="col-sm-9">
                             <label for="inputPassword3" class="col-sm-8 control-label ft">sdsakfsdsjf dsj idjfidsj dsj ifcjdsij dsjfidjs dsifcjidsjdsfc oidsjds fcidsjdsjfjds fjdsjfoijdsjfjsdsfj dsjfdsj</label>
                          </div>
                        </div>
                       
                        
                      </div>
                    </div>
                  </div>
                  
                  
                  <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">Status</div>
                      <div class="panel-body">
                      
                      <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Status </label>
                          <div class="col-sm-9">
                               <select class="form-control" >
                                  <option>Reject</option>
                                  <option>Forward</option>
                                  <option>Approved</option>
                                </select>
                          </div>
                        </div>
                      
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Description </label>
                          <div class="col-sm-9">
                            <textarea class="form-control height"></textarea>
                          </div>
                        </div>
                       <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label"> Forward Application To</label>
                          <div class="col-sm-8 col-xs-10">
                            <select class="form-control" >
                              <option>One</option>
                              <option>One</option>
                              <option>One</option>
                            </select>
                          </div>
                          <div class="col-sm-1 col-xs-1"><a href="#" class="show1"><i class="fa fa-user"></i></a></div>
                        </div>
                        <div id="pop1" class="simplePopup">
                          <div class="col-md-12">
                            <div class="form-group">
                              <h1 class="pop_hd">azac</h1>
                              <div class="row">
                                <div class="col-md-3"><img src="assets/images/avtar/user.png" class="pop_img"></div>
                                <div class="col-md-9">
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">E-mail </label>
                                    <div class="col-sm-9">
                                      <label for="inputPassword3" class="col-sm-6 control-label ft">jsafs@.com</label>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">Designation </label>
                                    <div class="col-sm-9">
                                      <label for="inputPassword3" class="col-sm-6 control-label ft">2545895678</label>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">Department </label>
                                    <div class="col-sm-9">
                                      <label for="inputPassword3" class="col-sm-6 control-label ft">dsafdsfdsf</label>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">Station </label>
                                    <div class="col-sm-9">
                                      <label for="inputPassword3" class="col-sm-6 control-label ft">2sdfds8</label>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">Join Date </label>
                                    <div class="col-sm-9">
                                      <label for="inputPassword3" class="col-sm-6 control-label ft">25-2-2015</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-info">Save</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  
                  <div class="col-md-12">
                	<div class="panel panel-default">
                        <div class="panel-heading">Additional Information</div>
                        <div class="panel-body">
                        
                             <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Notes </label>
                                <div class="col-sm-9">
                                  <label for="inputPassword3" class="col-sm-8 control-label ft">sdsakfsdsjf dsj idjfidsj dsj ifcjdsij dsjfidjs dsifcjidsjdsfc oidsjds fcidsjdsjfjds fjdsjfoijdsjfjsdsfj dsjfdsj</label>
                                </div>
                              </div>
                              
                              
                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Record Added By </label>
                                <div class="col-sm-9">
                                  <label for="inputPassword3" class="col-sm-9 control-label">System Administrator </label>
                                </div>
                              </div>
                               
                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Record Added on </label>
                                <div class="col-sm-9">
                                  <label for="inputPassword3" class="col-sm-9 control-label">25-2-2015 </label>
                                </div>
                              </div>
                              
                              
                         
                        
                        </div>
                    </div>
                </div>
                </div>
              </div>
            </div>
          </div>
          
          
          <div role="tabpanel" class="panel panel-default tab-pane tabs-up " id="users">
            <div class="panel-body">
              <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">Upload Documents</div>
                  <div class="panel-body">
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-3 control-label">Upload Documents </label>
                      <div class="col-sm-9">
                        <input type="file" class="btn btn-info"/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">Show Documents</div>
                  <div class="panel-body">
                    <table class="table no-margn">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Document Name</th>
                          <th>view</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td><i class="fa fa-file-o"></i> Lorem ipsum dolor sit amet</td>
                          <td><i class="fa fa-file-text-o sm"></i></td>
                          <td><i class="fa fa-close ss"></i></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="panel panel-default tab-pane tabs-up" id="docs">
            <div class="panel-body">
              <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading">Simple Map</div>
                  <div class="panel-body">
                    <div id="simpleMap" style="height:500px;"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Warper Ends Here (working area) -->
  
  <footer class="container-fluid footer"> Copyright &copy; 2014 <a href="http://freakpixels.com/" target="_blank">FreakPixels</a> <a href="#" class="pull-right scrollToTop"><i class="fa fa-chevron-up"></i></a> </footer>
</section>
<!-- Content Block Ends Here (right box)--> 

<!-- JQuery v1.9.1 --> 
<script src="assets/js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script> 
<script src="assets/js/plugins/underscore/underscore-min.js"></script> 

<!-- Bootstrap --> 
<script src="assets/js/bootstrap/bootstrap.min.js"></script> 
<script src="assets/js/globalize/globalize.min.js"></script> 
<!-- NanoScroll --> 
<script src="assets/js/plugins/nicescroll/jquery.nicescroll.min.js"></script> 
<!-- TypeaHead --> 
<script src="assets/js/plugins/typehead/typeahead.bundle.js"></script> 
<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script> 

<!-- InputMask --> 
<script src="assets/js/plugins/inputmask/jquery.inputmask.bundle.js"></script> 

<!-- TagsInput --> 
<script src="assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script> 

<!-- Chosen --> 
<script src="assets/js/plugins/bootstrap-chosen/chosen.jquery.js"></script> 

<!-- moment --> 
<script src="assets/js/moment/moment.js"></script> 
<!-- DateTime Picker --> 
<script src="assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script> 

<!-- Wysihtml5 --> 
<script src="assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script> 
<script src="assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script> 

<!-- Custom JQuery --> 
<script src="assets/js/app/custom.js" type="text/javascript"></script> 
<script src="assets/js/jquery.simplePopup.js" type="text/javascript"></script> 
<script type="text/javascript">

$(document).ready(function(){

    $('.show1').click(function(){
	$('#pop1').simplePopup();
    });
  
    $('.show2').click(function(){
	$('#pop2').simplePopup();
    });  
  
    $("#dashbordli").removeClass("active");
	$("#organzationli").removeClass("active");
	$("#recruitmentli").removeClass("active");
	$("#employeeli").addClass("active");
	$("#timesheetli").removeClass("active");
	$("#payrollli").removeClass("active");
	$("#reportsli").removeClass("active");
});

</script>
</html>
