<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />


</head>
<body style="background: #FFFFFF !important;">
	<h2 class="role_hd">Job Candidates Report</h2>
	<div class="col-xs-12">
		<form:form action="exportRecCand.do" method="POST"
			modelAttribute="recCandDetails">

			<table class="table table-striped no-margn line">
				<tbody>
					<tr>
						<td class="hd_tb"><span class="WhiteHeading">S#</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Job Post
						</span></td>
						<td class="hd_tb"><span class="WhiteHeading">First
								Name </span></td>
						<td class="hd_tb"><span class="WhiteHeading">Last Name</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Email
								Address</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Date Of
								Birth</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Gender</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Nationality</span></td>
						<!-- <td class="hd_tb"><span class="WhiteHeading">Address</span></td>
						<td class="hd_tb"><span class="WhiteHeading">City</span></td>
						<td class="hd_tb"><span class="WhiteHeading">State</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Phone
								Number</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Mobile
								Number</span></td> -->

					</tr>
					<c:choose>
						<c:when test="${! empty recCandDetails.detailsVo }">
							<c:forEach items="${recCandDetails.detailsVo }" var="details"
								varStatus="status">
								<tr>
									<td>${details.slno }<form:hidden
											path="detailsVo[${status.index }].slno" /></td>
									<td>${details.jobField }<form:hidden
											path="detailsVo[${status.index }].jobField" /></td>
									<td>${details.fName }<form:hidden
											path="detailsVo[${status.index }].fName" /></td>
									<td>${details.lName }<form:hidden
											path="detailsVo[${status.index }].lName" /></td>
									<td>${details.eMail }<form:hidden
											path="detailsVo[${status.index }].eMail" /></td>
									<td>${details.dobString }<form:hidden
											path="detailsVo[${status.index }].dob" /></td>
									<td>${details.gender }<form:hidden
											path="detailsVo[${status.index }].gender" /></td>
									<td>${details.nationality }<form:hidden
											path="detailsVo[${status.index }].nationality" /></td>
									<%-- <td>${details.address }<form:hidden
											path="detailsVo[${status.index }].address" /></td>
									<td>${details.city }<form:hidden
											path="detailsVo[${status.index }].city" /></td>
									<td>${details.state }<form:hidden
											path="detailsVo[${status.index }].state" /></td>
									<td>${details.phone }<form:hidden
											path="detailsVo[${status.index }].phone" /></td>
									<td>${details.mobile }<form:hidden
											path="detailsVo[${status.index }].mobile" /></td> --%>

								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
						<tr>
						<td>No Records To Show...</td>
						</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>


			<c:if test="${! empty recCandDetails.detailsVo }">
				<div class="form-group" style="margin-top: 25px;">
					<div class="col-sm-offset-4 col-sm-6">
						<button type="submit" class="btn btn-info" name="type" value="xls">
							Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
						</button>
						<button type="submit" class="btn btn-info" name="type" value="csv">
							Export to CSV &nbsp; <i class="fa fa-file-text-o"></i>
						</button>
						<button type="submit" class="btn btn-info" name="type" value="pdf">
							Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
						</button>
					</div>
				</div>
			</c:if>
		</form:form>
	</div>

	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>
</html>