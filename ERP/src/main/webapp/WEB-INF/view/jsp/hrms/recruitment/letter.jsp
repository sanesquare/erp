<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/app/app.v1.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Letters</h1>
		</div>

		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#all" role="tab"
				data-toggle="tab">Offer Letter</a></li>
			<li role="presentation"><a href="#appointmentLetter" role="tab"
				data-toggle="tab">Appointment Letter</a></li>

		</ul>
		<div class="tab-content" id="letter">
			<div role="tabpanel"
				class="panel panel-default tab-pane tabs-up active" id="all">
				<div class="panel-body">

					<div class="row">
						<form:form action="sendOfferLetter.do" commandName="offerLetterVo"
							method="POST">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Offer Letter</div>
									<div class="panel-body">
										<div class="col-md-6">
											<div class="form-group" id="editor">
												<label for="inputPassword3" class="col-sm-3 control-label">Date
													of Interview </label>
												<div class="col-sm-9" id="mainDiv">
													<div class="input-group date" id="datepicker">
														<form:input path="date" class="form-control"
															id="offer_date" data-date-format="DD/MM/YYYY" />
														<span class="input-group-addon"><span
															class="glyphicon glyphicon-calendar"></span> </span>
													</div>
												</div>
											</div>

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">
													Designation</label>
												<div class="col-sm-9">
													<form:select class="form-control" id="offer_post"
														path="designationId"
														onchange="getInterviewBydateAndPost()">
														<option value="" label="Select Designation"></option>
														<%-- <c:forEach items="${jobPosts}" var="post">
														<option value="${post.jobPostId }" >${post.jobTitle }</option>
													</c:forEach> --%>
													</form:select>
												</div>
											</div>


										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="col-sm-3 control-label"> Candidates
													Name </label>
												<div class="col-sm-9 ">
													<form:select class="form-control chosen-select"
														multiple="true" id="offer_cand"
														placeholder="Candidates Name" path="candId">

													</form:select>

												</div>
											</div>

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">
													Grades</label>
												<div class="col-sm-9">
													<form:select class="form-control" id="offer_grade"
														path="gradeId">
														<form:option value="" label="Select Grade"></form:option>
														<c:forEach items="${grades}" var="grade1">
															<form:option value="${grade1.gradeId }">${grade1.grade }</form:option>
														</c:forEach>
													</form:select>
												</div>
											</div>
										</div>
										<div class="col-md-12" id="tavle">
											<div class="panel-body">
												<table class="table table-bordered">
													<tr>
														<td width="70%"><strong class="amt_high">
																INPUT GROSS SALARY</strong></td>
														<td width="30%"><form:input path="gross"
																class="form-control double"
																placeholder="Enter Gross Salary"
																onkeyup="calculatorCalculate(this.value)"
																id="gross_salary_calculator" /></td>
													</tr>

												</table>
												<table class="table table-bordered">

													<thead>
														<tr>
															<th width="20%">&nbsp;</th>
															<th width="3%">S.No</th>
															<th width="48%">Description</th>
															<th width="20%" colspan="2">In Rupees</th>
															<th width="10%">Remarks</th>

														</tr>
													</thead>
													<tbody>
														<tr>
															<td><strong class="amt_high">SPLIT </strong></td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td><strong class="amt_high">salary</strong></td>
															<td><strong class="amt_high">Annual</strong></td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>BA</td>
															<td>1</td>
															<td>Basic Salary</td>
															<td id="calculator_basic_salary_monthly"></td>
															<td id="calculator_basic_salary_annually"></td>
															<td id="calculator_basic_salary_remarks"></td>
														</tr>

														<tr>
															<td>DA</td>
															<td>2</td>
															<td>Dearness Allownace</td>
															<td id="calculator_da_monthly"></td>
															<td id="calculator_da_annually"></td>
															<td id="calculator_da_remarks"></td>
														</tr>


														<tr>
															<td>HRA</td>
															<td>3</td>
															<td>HRA</td>
															<td id="calculator_hra_monthly"></td>
															<td id="calculator_hra_annual"></td>
															<td id="calculator_hra_remarks"></td>
														</tr>


														<tr>
															<td>CCA</td>
															<td>4</td>
															<td>CCA</td>
															<td id="calculator_cca_monthly"></td>
															<td id="calculator_cca_annual"></td>
															<td id="calculator_cca_remarks"></td>
														</tr>


														<tr>
															<td>conveyance</td>
															<td>5</td>
															<td>Conveyance</td>
															<td id="calculator_conveyance_monthly"></td>
															<td id="calculator_conveyance_annual"></td>
															<td id="calculator_conveyance_remarks"></td>
														</tr>

														<tr>
															<td>Other Allowance</td>
															<td>6</td>
															<td>Other Allowance(Balance)</td>
															<td id="calculator_other_monthly"></td>
															<td id="calculator_other_annual"></td>
															<td id="calculator_other_remarks"></td>
														</tr>

														<tr>
															<td></td>
															<td></td>
															<td><strong class="amt_high"> GROSS AMOUNT</strong></td>
															<td><strong class="amt_high"
																id="calculator_gross_monthly"></strong></td>
															<td><strong class="amt_high"
																id="calculator_gross_annual"></strong></td>
															<td id="calculator_gross_remarks"></td>
														</tr>

														<tr>
															<td rowspan="3">DEDUCTIONS emloyee</td>
															<td></td>
															<td>P.F. (Employee Contribution)</td>
															<td id="calculator_pf_monthly"></td>
															<td id="calculator_pf_annual"></td>
															<td id="calculator_pf_remarks"></td>
														</tr>
														<tr>

															<td></td>
															<td>E.S.I (Employee Contribution)</td>
															<td id="calculator_esi_monthly"></td>
															<td id="calculator_esi_annual"></td>
															<td id="calculator_esi_remarks"></td>
														</tr>

														<tr>

															<td></td>
															<td>LWF</td>
															<td id="calculator_lwf_monthly"></td>
															<td id="calculator_lwf_annual"></td>
															<td id="calculator_lwf_remarks"></td>
														</tr>
														<tr>
															<td></td>
															<td></td>
															<td><strong class="amt_high"> NET TAKE HOME</strong></td>
															<td><strong class="amt_high"
																id="calculator_net_monthly"></strong></td>
															<td><strong class="amt_high"
																id="calculator_net_annual"></strong></td>
															<td id="calculator_net_remarks"></td>
														</tr>

														<tr>
															<td rowspan="3">EMPLOYER CONTRIBUTION</td>
															<td></td>
															<td>P.F. (Employer Contribution)</td>
															<td id="calculator_pf_monthly_employer"></td>
															<td id="calculator_pf_annual_employer"></td>
															<td id="calculator_pf_remarks_employer"></td>
														</tr>
														<tr>

															<td></td>
															<td>E.S.I (Employer Contribution)</td>
															<td id="calculator_esi_monthly_employer"></td>
															<td id="calculator_esi_annual_employer"></td>
															<td id="calculator_esi_remarks_employer"></td>
														</tr>

														<tr>

															<td></td>
															<td>LWF</td>
															<td id="calculator_lwf_monthly_employer"></td>
															<td id="calculator_lwf_annual_employer"></td>
															<td id="calculator_lwf_remarks_employer"></td>
														</tr>

														<tr>
															<td></td>
															<td></td>
															<td><strong class="amt_high">CTC</strong></td>
															<td><strong class="amt_high"
																id="calculator_ctc_monthly"></strong></td>
															<td><strong class="amt_high"
																id="calculator_ctc_annual"></strong></td>
															<td id="calculator_ctc_remarks"></td>
														</tr>

													</tbody>
												</table>
											</div>
										</div>
										<div class="form-group">
											<div>
												<button type="submit" class="btn btn-info" id=""
													style="float: right;">Send Mail</button>
											</div>
										</div>
									</div>

								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
				id="appointmentLetter">

				<div class="row">
					<div class="col-md-12">

						<br />
						<div class="panel panel-default">
							<div class="panel-heading">Appointment Letter</div>
							<div class="panel-body">
								<form:form action="sendAppointmentLetter.do" method="POST"
									commandName="appointmentLetterVo">
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Date
												of Interview </label>
											<div class="col-sm-9" id="second_div">
												<div class="input-group date" id="datepickers">
													<form:input path="date" class="form-control"
														id="appoint_date" data-date-format="DD/MM/YYYY" />
													<span class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span> </span>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">
												Designation</label>
											<div class="col-sm-9">
												<form:select class="form-control" id="appoint_post"
													path="designationId"
													onchange="getCandidatesBydateAndPost()">
													<option value="" label="Select Designation"></option>
													<%-- <c:forEach items="${jobPosts}" var="post">
														<option value="${post.jobPostId }" >${post.jobTitle }</option>
													</c:forEach> --%>
												</form:select>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label class="col-sm-3 control-label"> Employee Name</label>
											<div class="col-sm-9 ">
												<select class="form-control chosen-select" multiple="true"
													id="appoint_cand" data-placeholder="Employee Name"
													style="width: 100% !important;">
													<option></option>
												</select>

											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<button type="button" class="btn btn-info" id="sen_mail"
												style="float: right; margin-right: 15px">Send Mail</button>
										</div>
									</div>
								</form:form>
							</div>

						</div>
					</div>
				</div>
			</div>





		</div>

	</div>

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js'/>"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js'/>"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js'/>"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js'/>"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js'/>"></script>

	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>


	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script type="text/javascript">
		$("#mainDiv").on("change", function() {
			getInterviewsBydate($("#offer_date").val());
		});

		$("#second_div").on("change", function() {
			getInterviewsOfferBydate($("#appoint_date").val());
		});

		$("#ssss").click(function() {
			$.ajax({
				type : "GET",
				url : "generatePdf",
				cache : false,
				async : true,
				success : function(response) {
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
		});

		/* $(document).ready(function() {
			var specialElementHandlers = {
				'#editor' : function(element, renderer) {
					return true;
				}
			};
			$('#ssss').click(function() {
				 var doc = new jsPDF();
				var source = $('#letter').html();
				var specialElementHandlers = {
					'#bypassme' : function(element, renderer) {
						return true;
					}
				};
				doc.fromHTML(source, 0.5, 0.5, {
					'width' : 75,
					'elementHandlers' : specialElementHandlers
				});
				doc.output("dataurlnewwindow"); 
			}); 
			
		}); */
	</script>
</body>
</html>