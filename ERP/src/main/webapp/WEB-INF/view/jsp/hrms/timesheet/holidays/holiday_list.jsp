<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width="20%">Title</th>
			<th width="20%">Holiday Start Date</th>
			<th width="20%">Holiday End Date</th>
			<c:if test="${ rolesMap.roles['Holidays'].update}">
				<th width="15%">Edit</th>
			</c:if>
			<c:if test="${ rolesMap.roles['Holidays'].delete}">
				<th width="15%">Delete</th>
			</c:if>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${holidayList}" var="holiday">
			<tr>
				<c:choose>
					<c:when test="${ rolesMap.roles['Holidays'].delete}">
						<td><a href="ViewHoliday.do?otp_hid=${holiday.holidayId}">${holiday.title}</a></td>
						<td><a href="ViewHoliday.do?otp_hid=${holiday.holidayId}">${holiday.startDate}</a></td>
						<td><a href="ViewHoliday.do?otp_hid=${holiday.holidayId}">${holiday.endDate}</a></td>
					</c:when>
					<c:otherwise>
						<td>${holiday.title}</td>
						<td>${holiday.startDate}</td>
						<td>${holiday.endDate}</td>
					</c:otherwise>
				</c:choose>

				<c:if test="${ rolesMap.roles['Holidays'].update}">
					<td><a href="UpdateHoliday.do?otp_hid=${holiday.holidayId}"><i
							class="fa fa-edit sm"></i> </a></td>
				</c:if>
				<c:if test="${ rolesMap.roles['Holidays'].delete}">
					<td><a href="#"
						onclick="holidayDelConfirm(${holiday.holidayId})"><i
							class="fa fa-close ss"></i></a></td>
				</c:if>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${ rolesMap.roles['Holidays'].add}">
	<a href="CreateHoliday.do">
		<button type="button" class="btn btn-primary">Add New Holiday</button>
	</a>
</c:if>

<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>