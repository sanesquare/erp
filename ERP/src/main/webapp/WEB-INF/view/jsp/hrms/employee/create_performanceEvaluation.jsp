<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/RegalCalendar.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div id="fade"></div>
	<div id="modal">
		<img id="loader" src="resources/images/loading.gif" />
	</div>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Performance Evaluation</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="PerformanceEvaluation.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab" id="performance-eval-doc-tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form action="CreatePerformanceEvaluation.do" method="post"
								commandName="evaluationVo" id="performanceEvaluationForm">
								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Performance Evaluation
													Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">
															Title</label>
														<div class="col-sm-9">
															<form:input path="evaluationTitle" class="form-control"
																name="evaluationTitle" placeholder="Evaluation Title" />
														</div>
													</div>
													<!-- <div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="button" class="btn btn-info">Next</button>
														</div>
													</div> -->
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Performance Evaluation
													Information</div>
												<div class="panel-body">
													<!-- <div class="form-group">
													<label class="col-sm-3 control-label">Evaluation
														For</label>
													<div class="col-sm-9 ">
														<select class="form-control chosen-select" multiple
															data-placeholder="Select Option">
															<option></option>
															<option value="United States">United States</option>
															<option value="United Kingdom">United Kingdom</option>
															<option value="Afghanistan">Afghanistan</option>
															<option value="Albania">Albania</option>
															<option value="Algeria">Algeria</option>
															<option value="American Samoa">American Samoa</option>
															<option value="Andorra">Andorra</option>
															<option value="Angola">Angola</option>
															<option value="Anguilla">Anguilla</option>
															<option value="Antarctica">Antarctica</option>
															<option value="Antigua and Barbuda">Antigua and
																Barbuda</option>
															<option value="Argentina">Argentina</option>
															<option value="Armenia">Armenia</option>
															<option value="Aruba">Aruba</option>
															<option value="Australia">Australia</option>
														</select>
													</div>
												</div> -->
													<!-- <div class="form-group">
													<label class="col-sm-3 control-label">Evaluation By</label>
													<div class="col-sm-9 ">
														<select class="form-control chosen-select" multiple
															data-placeholder="Select Option">
															<option></option>
															<option value="United States">United States</option>
															<option value="United Kingdom">United Kingdom</option>
															<option value="Afghanistan">Afghanistan</option>
															<option value="Albania">Albania</option>
															<option value="Algeria">Algeria</option>
															<option value="American Samoa">American Samoa</option>
															<option value="Andorra">Andorra</option>
															<option value="Angola">Angola</option>
															<option value="Anguilla">Anguilla</option>
															<option value="Antarctica">Antarctica</option>
															<option value="Antigua and Barbuda">Antigua and
																Barbuda</option>
															<option value="Argentina">Argentina</option>
															<option value="Armenia">Armenia</option>
															<option value="Aruba">Aruba</option>
															<option value="Australia">Australia</option>
														</select>
													</div>
												</div> -->
													<div class="form-group">
														<label class="col-sm-3 control-label">Branch</label>
														<div class="col-sm-9">
															<form:select path="branches" multiple="true"
																data-placeholder="Select Branches"
																class="form-control chosen-select">
																<form:option value="0">All</form:option>
																<c:forEach items="${branches}" var="branch">
																	<form:option value="${branch.branchId}">${branch.branchName}</form:option>
																</c:forEach>
															</form:select>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Evaluation
															Start Date</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepicker">
																<form:input path="startDate" name="startDate"
																	class="form-control" readonly="true"
																	style="background-color: #ffffff !important;"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Evaluation
															End Date</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepickers">
																<form:input path="endDate" name="endDate"
																	class="form-control" readonly="true"
																	style="background-color: #ffffff !important;"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>
													</div>
													<!-- <div class="form-group">
													<label class="col-sm-3 control-label">Send Email
														Notification</label>
													<div class="col-sm-9">
														<input type="checkbox" class="flt_l" />
													</div>
												</div> -->
													<!-- <div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="button" class="btn btn-info">Next</button>
														</div>
													</div> -->
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Performance Evaluation
													Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea path="description"
																class="wysihtml form-control"
																placeholder="Enter text ..." style="height: 150px" />
														</div>
													</div>
													<!-- <div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<button type="button" class="btn btn-info">Next</button>
														</div>
													</div> -->
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" class="form-control height" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">System
																Administrator </label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${created_on}</label>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<form:hidden path="evaluationId" id="evaluationId" />
															<button type="submit" class="btn btn-info">${buttonLabel}</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<!-- <div class="col-md-6">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Upload Documents</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Upload
													Documents </label>
												<div class="col-sm-9">
													<input type="file" class="btn btn-info" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">
											<table class="table no-margn">
												<thead>
													<tr>
														<th>#</th>
														<th>Document Name</th>
														<th>view</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td><i class="fa fa-file-o"></i> Lorem ipsum dolor
															sit amet</td>
														<td><i class="fa fa-file-text-o sm"></i></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div> -->
							<div class="col-md-6">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Upload questions</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Upload
													questions </label>
												<div class="col-sm-9">
													<input type="file" name="evaluationDocs"
														id="performance-evaluation-docs" class="btn btn-info"
														multiple />
													<button type="button" onclick="uploadEvaluationDocument()"
														class="btn btn-info">Upload</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Show questions</div>
										<div class="panel-body"
											id="performance-evaluation-questions-list"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
	<!-- <script>
		$('#js-smart-tabs').smartTabs();
		$('#js-smart-tabs--tabs').smartTabs({
			layout : 'tabs'
		});
		$('#js-smart-tabs--accordion').smartTabs({
			layout : 'accordion'
		});
	</script>
	<script>
		$(document).ready(function() {

			$("#admin_ss").click(function() {
				$("#add_administartor").show();
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addfield").click(function() {
				$("#add_custom_feild").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addrm").click(function() {
				$("#add_rm").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script> -->
</body>
</html>
