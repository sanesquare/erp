<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width="20%">Memo From</th>
			<th width="20%">Subject</th>
			<th width="20%">Memo Date</th>
			<th width="20%">Edit</th>
			<th width="20%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${memoList}" var="memo">
			<tr>
				<td><a href="ViewMemoInformation.do?otp_mid=${memo.memoId}">${memo.employee}</a></td>
				<td><a href="ViewMemoInformation.do?otp_mid=${memo.memoId}">${memo.memoSubject}</a></td>
				<td><a href="ViewMemoInformation.do?otp_mid=${memo.memoId}">${memo.memoOn}</a></td>
				<td><a href="UpdateMemoInformation.do?otp_mid=${memo.memoId}"><i
						class="fa fa-edit sm"></i> </a></td>
				<td><a href="#" onclick="deleteThisMemo(${memo.memoId})"><i
						class="fa fa-close ss"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<a href="MemoInformation.do">
	<button type="button" class="btn btn-primary">Add New Memo</button>
</a>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>