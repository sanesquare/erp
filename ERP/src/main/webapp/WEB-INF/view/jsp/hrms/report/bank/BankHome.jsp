<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />

<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style>
.resp-vtabs .resp-tabs-container {
	min-height: 850px;
}
</style>
<title>Payroll</title>
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Bank Statement</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div>
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Bank Statement</div>
							<div class="panel-body">
								<form:form action="bankStatement" method="POST"
									commandName="reportVo" target="_blank">
									<div class="form-group">
										<label for="inputEmail3"
											class="col-sm-3 col-xs-12 control-label"> Branch</label>
										<div class="col-sm-9">
											<form:select class="form-control" path="branchId">
												<form:option value="0" label="All Branches" />
												<form:options items="${branches }" itemValue="branchId"
													itemLabel="branchName" />
											</form:select>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3"
											class="col-sm-3 col-xs-12 control-label">Department</label>
										<div class="col-sm-9">
											<form:select class="form-control" path="depId">
												<form:option value="0" label="All Departments" />
												<form:options items="${departments }" itemValue="id"
													itemLabel="name" />
											</form:select>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3"
											class="col-sm-3 col-xs-12 control-label"> Employee
											Type</label>
										<div class="col-sm-9">
											<form:select class="form-control" path="typeId">
												<form:option label="All Types" value="0" />
												<form:options items="${types }" itemValue="employeeTypeId"
													itemLabel="employeeType" />
											</form:select>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3"
											class="col-sm-3 col-xs-12 control-label"> Employee
											Category</label>
										<div class="col-sm-9">
											<form:select class="form-control" path="cateId">
												<form:option label="All Types" value="0" />
												<form:options items="${categories }"
													itemValue="employeeCategoryId" itemLabel="employeeCategory" />
											</form:select>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3"
											class="col-sm-3 col-xs-12 control-label"> Employee </label>
										<div class="col-sm-9">
											<form:select class="form-control" path="empId">
												<form:option value="0" label="All Employees" />
												<form:options items="${employees }" itemLabel="name"
													itemValue="employeeId" />
											</form:select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Date (From)<span
											class="stars">*</span></label>
										<div class="col-sm-5">
											<form:select class="form-control" path="startMonth">
												<form:option value="01" label="January" />
												<option value="02" label="February" />
												<option value="03" label="March" />
												<option selected="true" value="4" label="April" />
												<option value="05" label="May" />
												<option value="06" label="June" />
												<option value="07" label="July" />
												<option value="08" label="August" />
												<option value="09" label="September" />
												<option value="10" label="October" />
												<option value="11" label="November" />
												<option value="12" label="December" />
											</form:select>
										</div>
										<div class="col-sm-4">
											<form:select class="form-control" path="startYear">
												<c:forEach items="${years }" var="year">
													<form:option value="${year}" label="${ year}" />
												</c:forEach>
											</form:select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Date (To)<span
											class="stars">*</span></label>
										<div class="col-sm-5">
											<form:select class="form-control" path="endMonth">
												<form:option value="01" label="January" />
												<option value="02" label="February" />
												<option value="03" label="March" />
												<option selected="true" value="4" label="April" />
												<option value="05" label="May" />
												<option value="06" label="June" />
												<option value="07" label="July" />
												<option value="08" label="August" />
												<option value="09" label="September" />
												<option value="10" label="October" />
												<option value="11" label="November" />
												<option value="12" label="December" />
											</form:select>
										</div>
										<div class="col-sm-4">
											<form:select class="form-control" path="endYear">
												<c:forEach items="${years }" var="year">
													<form:option value="${year}" label="${ year}" />
												</c:forEach>
											</form:select>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-6">

											<button type="submit" class="btn btn-info">Generate
												Report</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<script
		src='<c:url value="/resources/assets/js/jquery.simplePopup.js" />'></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#parentVerticalTab').easyResponsiveTabs({
				type : 'vertical', //Types: default, vertical, accordion
				width : 'auto', //auto or any width like 600px
				fit : true, // 100% fit in a container
				closed : 'accordion', // Start closed if in accordion view
				tabidentify : 'hor_1', // The tab groups identifier
				activate : function(event) { // Callback function if tab is switched
					var $tab = $(this);
					var $info = $('#nested-tabInfo2');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
			$('#employeeSl').bind('click', function() {
				showPop();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			//dates
			if ("${reportVo.startDate}" == "") {
				$("#ho_start_date").val(convertDate(new Date()));
				$("#da_start_date").val(convertDate(new Date()));
			}
			if ("${reportVo.endDate}" == "") {
				$("#ho_end_date").val(convertDate(new Date()));
				$("#da_end_date").val(convertDate(new Date()));
			}
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").addClass("active");
		});
	</script>
	<script type="text/javascript">
		$(function() {
			if ($.isFunction($.fn.datetimepicker)) {
				$('#datepicker').datetimepicker({
					pickTime : false
				});
				$('#datepickernew').datetimepicker({
					pickTime : false
				});
				$('#datepickernew1').datetimepicker({
					pickTime : false
				});
				$('#datepickernew2').datetimepicker({
					pickTime : false
				});
				$('#datepickernew3').datetimepicker({
					pickTime : false
				});
				$('#datepickernew4').datetimepicker({
					pickTime : false
				});
				$('#datepickernew5').datetimepicker({
					pickTime : false
				});
				$('#datepickernew6').datetimepicker({
					pickTime : false
				});
				$('#datepickernew7').datetimepicker({
					pickTime : false
				});
				$('#datepickernew8').datetimepicker({
					pickTime : false
				});
				$('#datepickernew9').datetimepicker({
					pickTime : false
				});
				$('#datepickernew10').datetimepicker({
					pickTime : false
				});
			}
		});
	</script>
</body>
</html>