<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">


<head>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>SNOGOL HRM</title>

	<meta name="description" content="">
	<meta name="author" content="Akshay Kumar">

	<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>	
</head>
<body data-ng-app>


    	
    
	
    <!-- Aside Ends-->
    

    	
       
        <!-- Header Ends -->
        
        
        <div class="warper container-fluid">
        	
            <div class="page-header"><h1>User Activities Logs </h1></div>
            
            
            
            
            
            <div class="row">
            	
            
           <div class="col-md-12">
           
          <div class="panel panel-default">
                    <div class="panel-heading">User Activities Logs </div>
                    <div class="panel-body">
                    
                        
                        
                   <form:form action="saveProject.do" method="POST" commandName="userActivitiesVo">
                     <table class="table table-striped no-margn">
                              <thead>
                                <tr>
                                  <th width="90%">Month</th>
                                  <th width="10%">Operations</th>
                                 
                                </tr>
                              </thead>
                              <tbody>
                              
                              <c:forEach items="${activities }" var="activities">
                              
                                <tr>
                                  <td>${activities.monthName }&nbsp; ${activities.year }</td>
                                  <td><a href="listUserActivities.do?month=${activities.monthIndex}&&year=${activities.year}"><i class="fa fa-list"></i></a></td>
                                </tr>
							</c:forEach>
							                                
                              </tbody>
                            </table>
					</form:form>
                    </div>
                </div>
           </div>
            
            
            </div>
            
      
            
            
        </div>
        <!-- Warper Ends Here (working area) -->
        
        

        
    

    <!-- Content Block Ends Here (right box)-->
    
    
  <script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script
		src="<c:url value='/resources/js/progress/jquery.uploadfile.min.js' />"></script>
	<script src="<c:url value='/resources/js/progress/file-upload.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
    
	<script type="text/javascript">
	$(document).ready(function(){
		$("#dashbordli").removeClass("active");
		$("#organzationli").addClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").removeClass("active");
		$("#reportsli").removeClass("active");
	});
	</script>
    
    
</html>
