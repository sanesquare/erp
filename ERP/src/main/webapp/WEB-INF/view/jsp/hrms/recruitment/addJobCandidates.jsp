<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/app/app.v1.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
<style type="text/css">
#add_cand_success{
	display : none;
}
#add_cand_error{
	display : none;
}
#add_candInfo_success {
	display: none;
}

#add_candInfo_error {
	display: none;
}

#add_candCont_success {
	display: none;
}

#add_candCont_error {
	display: none;
}

#add_candInt_success {
	display: none;
}

#add_candInt_error {
	display: none;
}

#add_candAchieve_success {
	display: none;
}

#add_candAchieve_error {
	display: none;
}

#add_candAdd_success {
	display: none;
}

#add_candAdd_error {
	display: none;
}

#add_candQual_success {
	display: none;
}

#add_candQual_error {
	display: none;
}

#add_candLang_success {
	display: none;
}

#add_candLang_error {
	display: none;
}

#add_candSkill_success {
	display: none;
}

#add_candSkill_error {
	display: none;
}

#add_candExp_success {
	display: none;
}

#add_candExp_error {
	display: none;
}

#add_candSts_success {
	display: none;
}

#add_candSts_error {
	display: none;
}

#add_candRef_success {
	display: none;
}

#add_candRef_error {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Job Candidates</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="jobCandidates.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
			<c:if test="${! empty success_msg}">	
					<div class="success_msg" id="add_cand_success">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>
					</div>
				</c:if>
				<c:if test="${! empty error_msg}">
					<div class="error_msg" id="add_cand_error">
						<span id="msge_icn"><i class="fa fa-times"></i></span>
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">

			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>
				<form:form action="savejobCandidate.do" method="POST"
					commandName="jobCandidatesVo" enctype="multipart/form-data">
					<form:hidden path="jobCandidateId" id="cand_id" />
					<div class="tab-content">
						<div role="tabpanel"
							class="panel panel-default tab-pane tabs-up active" id="all">
							<div class="panel-body">

								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Candidate Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Job
															Designation <span class="stars">*</span></label>
														<div class="col-sm-9">
															<%-- <form:select class="form-control" id="cand_job_field"
																path="jobFieldId">
																<form:option value="" label="Select job field"></form:option>
																<form:options items="${jobFields}"
																	itemValue="jobFieldId" itemLabel="jobField"></form:options>

															</form:select> --%>

															<form:select class="form-control" path="jobField"
																id="cand_job_field">
																<form:option value="" label="Select Designation"></form:option>
																<form:options items="${designations}"
																	itemValue="designation" itemLabel="designation" />
																<%-- <form:option value="${dep.id}">${dep.name}</form:option> --%>
																<%-- </c:forEach> --%>
															</form:select>

														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">First
															Name <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input type="text" class="alphabets form-control"
																id="cand_fname" path="firstName" />
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Last
															Name <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input type="text" class="alphabets form-control"
																id="cand_lname" path="lastName" />
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Date of
															Birth <span class="stars">*</span></label>
														<div class="col-sm-9">

															<div class="input-group date" id="datepicker">
																<form:input readonly="true" type="text"
																	style="background-color: #ffffff !important;"
																	class="form-control" data-date-format="DD/MM/YYYY"
																	id="cand_dob" path="birthDate" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>

														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Gender <span class="stars">*</span></label>
														<div class="col-sm-9">
															<label class="col-sm-4 control-label"><form:radiobutton
																	path="gender" value="Male" name="sex" />Male</label><label
																class="col-sm-4 control-label"><form:radiobutton
																	path="gender" value="female" name="sex" />Female<!-- <input type="radio" name="sex"/> Male</label>    <label class="col-sm-4 control-label"><input type="radio" name="sex"/> female --></label>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Nationality <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select class="form-control" path="nationality"
																id="cand_nation">
																<form:option value="" label="Select Country"></form:option>
																<form:options items="${countries }" itemValue="name"
																	itemLabel="name" />
															</form:select>
														</div>

													</div>



													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="sbmt_cand_basic_info" class="btn btn-info"
																onClick="validateCandInfo()">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_candInfo_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>
														</div>


														<div class="error_msg" id="add_candInfo_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>
														</div>

													</div>

												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Contact Information</div>
												<div class="panel-body">


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Address
														</label>
														<div class="col-sm-9">
															<form:input type="text" class="form-control"
																id="can_address" path="address" />
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">City</label>
														<div class="col-sm-9">
															<form:input type="text" class="form-control"
																id="cand_city" path="city" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">State/Province</label>
														<div class="col-sm-9">
															<form:input type="text" class="form-control"
																id="cand_state" path="state" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Zip
															code/Postal Code</label>
														<div class="col-sm-9">
															<form:input type="text" class="numbersonly form-control"
																id="cand_zipCode" path="zipCode" />
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Country</label>
														<div class="col-sm-9">
															<form:select class="form-control" id="cand_country"
																path="countryId">
																<form:option value="" label="Select Country"></form:option>
																<form:options items="${countries }"
																	itemValue="countryId" itemLabel="name" />
															</form:select>
														</div>

													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Email
															Address <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input type="text" class="form-control"
																id="cand_email" path="eMail" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Phone
															Number</label>
														<div class="col-sm-9">
															<form:input type="text" class="numbersonly form-control"
																id="cand_phone" path="phoneNumber" />
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Mobile
															Number</label>
														<div class="col-sm-9">
															<form:input type="text" class="numbersonly form-control"
																id="cand_mobile" path="mobileNumber" />
														</div>
													</div>



													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a class="btn btn-info" id="sbmt_cand_address"
																onClick="validateCandAddress()">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_candCont_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>
														</div>


														<div class="error_msg" id="add_candCont_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>
														</div>

													</div>


												</div>
											</div>
										</div>


									</div>

									<div class="col-md-6">


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Interests</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea class=" form-control" id="cand_interests"
																path="interests" style="height: 100px;resize:none;" />

														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a id="sbmt_cand_int" class="btn btn-info"
																onClick="validateCandInt()">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_candInt_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>
														</div>


														<div class="error_msg" id="add_candInt_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>
														</div>

													</div>



												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Achievements</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">


															<form:textarea class=" form-control"
																id="cand_achievements" path="achievements"
																style="height: 150px;resize:none;" />

														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a id="sbmt_achievements" class="btn btn-info"
																onClick="validateCandAchieve()">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_candAchieve_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>
														</div>


														<div class="error_msg" id="add_candAchieve_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>
														</div>

													</div>

												</div>
											</div>
										</div>




										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<c:if test="${ rolesMap.roles['Candidate Notes'].add}">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Notes </label>
															<div class="col-sm-9">
																<form:textarea class="form-control height"
																	id="cand_addInfo" path="additionalInfo"></form:textarea>
															</div>
														</div>
													</c:if>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label ft">${jobCandidatesVo.createdBy }
															</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label ft">${jobCandidatesVo.createdOn }
															</label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="sbmt_addInfo" class="btn btn-info"
																onClick="validateCandAddInfo()">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_candAdd_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>
														</div>


														<div class="error_msg" id="add_candAdd_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>
														</div>

													</div>


												</div>
											</div>
										</div>

									</div>
								</div>

								<div class="row">
									<div class="col-md-6">

										<c:if
											test="${ rolesMap.roles['Job Candidate Qualifications'].add}">
											<div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">Add Qualifications</div>
													<div class="panel-body">

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Degree <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:select class="form-control" path="qualificationId"
																	onchange="getCandQualification()" id="cand_qual">
																	<form:option value="" label="Select Degree"></form:option>
																	<form:options items="${degrees }" itemValue="degreeId"
																		itemLabel="degreeName" />
																</form:select>

															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Subject</label>
															<div class="col-sm-9">
																<form:input type="text" class="form-control"
																	id="cand_subject" path="subject" />
															</div>
														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Institute</label>
															<div class="col-sm-9">
																<form:input type="text" class="form-control"
																	id="cand_institute" path="institute" />
															</div>
														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Grade/GPA</label>
															<div class="col-sm-9">
																<form:input type="text" class="alpha_numeric form-control "
																	id="cand_grade" path="grade" />
															</div>
														</div>


														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Graduation Year</label>
															<div class="col-sm-9">
																<form:input type="text" class="numbersonly form-control"
																	id="cand_year" path="year" />
															</div>
														</div>

														<div class="form-group">
															<div class="col-sm-offset-3 col-sm-9">
																<a id="sbmt_qual" class="btn btn-info"
																	onClick="validateCandQual()">Add Qualification</a>
															</div>
														</div>
														<div class="col-md-12">

															<div class="success_msg" id="add_candQual_success">
																<span id="msgs_icn"><i class="fa fa-check"></i></span>
															</div>


															<div class="error_msg" id="add_candQual_error">
																<span id="msge_icn"><i class="fa fa-times"></i></span>
															</div>

														</div>



													</div>
												</div>
											</div>
										</c:if>

										<c:if test="${ rolesMap.roles['Job Candidate Skills'].add}">
											<div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">Skills</div>
													<div class="panel-body">

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Skill <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:select class="form-control" id="cand_skills"
																	onchange="getCandSkills()" path="skillsId">
																	<form:option value="" label="Select Skill"></form:option>
																	<form:options items="${skills}" itemValue="skillId"
																		itemLabel="skillName" />
																</form:select>

															</div>
														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Skill Level <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:select class="form-control" id="cand_skill_level"
																	path="skillLevel">
																	<%-- <form:options items="${skills }" itemValue="skillId" itemLabel="skillName"/> --%>
																	<form:option value="Excellent" label="Excellent"></form:option>
																	<form:option value="Good" label="Good"></form:option>
																	<form:option value="Average" label="Average"></form:option>
																</form:select>

															</div>
														</div>



														<div class="form-group">
															<div class="col-sm-offset-16 col-sm-12">
																<a id="sbmt_skill" class="btn btn-info"
																	onClick="validateCandSkill()">Add Skill</a>
															</div>
														</div>
														<div class="col-md-12">

															<div class="success_msg" id="add_candSkill_success">
																<span id="msgs_icn"><i class="fa fa-check"></i></span>
															</div>


															<div class="error_msg" id="add_candSkill_error">
																<span id="msge_icn"><i class="fa fa-times"></i></span>
															</div>

														</div>


													</div>
												</div>
											</div>
										</c:if>

									</div>


									<div class="col-md-6">



										<c:if test="${ rolesMap.roles['Job Candidate Languages'].add}">
											<div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">Language</div>
													<div class="panel-body">


														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Language <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:select class="form-control" id="cand_lang"
																	onchange="getCandLanguage()" path="languageId">
																	<form:option value="" label="Select Language"></form:option>
																	<form:options items="${languages }"
																		itemValue="languageId" itemLabel="language" />
																</form:select>

															</div>
														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Speaking Level <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:select class="form-control" id="cand_speak"
																	path="speaking">
																	<form:option value="Excellent" label="Excellent"></form:option>
																	<form:option value="Good" label="Good"></form:option>
																	<form:option value="Average" label="Average"></form:option>
																	<form:option value="Poor" label="Poor"></form:option>
																</form:select>

															</div>
														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Reading Level <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:select class="form-control" id="cand_read"
																	path="reading">
																	<form:option value="Excellent" label="Excellent"></form:option>
																	<form:option value="Good" label="Good"></form:option>
																	<form:option value="Average" label="Average"></form:option>
																	<form:option value="Poor" label="Poor"></form:option>
																</form:select>

															</div>
														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Writing Level <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:select class="form-control" id="cand_write"
																	path="writing">
																	<form:option value="Excellent" label="Excellent"></form:option>
																	<form:option value="Good" label="Good"></form:option>
																	<form:option value="Average" label="Average"></form:option>
																	<form:option value="Poor" label="Poor"></form:option>
																</form:select>

															</div>
														</div>

														<div class="form-group">
															<div class="col-sm-offset-3 col-sm-9">
																<a id="sbmt_lang" class="btn btn-info"
																	onClick="validateCandLanguage()">Add Language</a>
															</div>
														</div>
														<div class="col-md-12">

															<div class="success_msg" id="add_candLang_success">
																<span id="msgs_icn"><i class="fa fa-check"></i></span>
															</div>


															<div class="error_msg" id="add_candLang_error">
																<span id="msge_icn"><i class="fa fa-times"></i></span>
															</div>

														</div>

													</div>
												</div>
											</div>
										</c:if>

									</div>
								</div>

								<div class="row">
									<div class="col-md-6">

										<c:if
											test="${ rolesMap.roles['Job Candidate Work Experiences'].add}">
											<div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">Work Experience</div>
													<div class="panel-body">

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Organisation
																Name<span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:input type="text" class="form-control"
																	id="cand_orgName" path="organisationName" />
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Job Designation <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:input type="text" class="form-control"
																	id="cand_designation" path="designation" />
															</div>
														</div>


														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Job Field <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:select class="form-control" id="cand_expField"
																	path="fieldId">
																	<form:option value="" label="Select Job Field"></form:option>
																	<form:options items="${jobFields }"
																		itemValue="jobFieldId" itemLabel="jobField" />
																</form:select>

															</div>
														</div>
<%-- 
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Job Description</label>
															<div class="col-sm-9">
																<form:input type="text" class="form-control"
																	id="cand_expDesc" path="description" />
															</div>
														</div>



														<div class="form-group">
															<label class="col-sm-3 control-label">Job Start
																Date</label>
															<div class="col-sm-9">
																<div class='input-group date' id="datepickerss">
																	<form:input type='text' readonly="true"
																		style="background-color: #ffffff !important;"
																		class="form-control" data-date-format="DD/MM/YYYY"
																		path="startDate" id="cand_expStrtDate" />
																	<span class="input-group-addon"><span
																		class="glyphicon glyphicon-calendar"></span> </span>
																</div>
															</div>

														</div>

														<div class="form-group">
															<label class="col-sm-3 control-label">Job End
																Date</label>
															<div class="col-sm-9">
																<div class='input-group date' id="datepickers">
																	<form:input type='text' readonly="true"
																		style="background-color: #ffffff !important;"
																		class="form-control" data-date-format="DD/MM/YYYY"
																		path="endDate" id="cand_expEndDate" />
																	<span class="input-group-addon"><span
																		class="glyphicon glyphicon-calendar"></span> </span>
																</div>
															</div>

														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Starting Salary</label>
															<div class="col-sm-9">
																<form:input type="text" class="double form-control"
																	id="cand_expStrtSalary" path="startSalary" />
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="double col-sm-3 control-label">Ending
																Salary</label>
															<div class="col-sm-9">
																<form:input type="text" class="form-control"
																	id="cand_expEndSalary" path="endSalary" />
															</div>
														</div> --%>

														<div class="form-group">
															<div class="col-sm-offset-3 col-sm-9">
																<a id="sbmt_exp" class="btn btn-info"
																	onClick="validateCandExp()">Add Work Experience</a>
															</div>
														</div>
														<div class="col-md-12">

															<div class="success_msg" id="add_candExp_success">
																<span id="msgs_icn"><i class="fa fa-check"></i></span>
															</div>


															<div class="error_msg" id="add_candExp_error">
																<span id="msge_icn"><i class="fa fa-times"></i></span>
															</div>

														</div>

													</div>
												</div>
											</div>
										</c:if>


										<c:if
											test="${ rolesMap.roles['Job Candidate References'].add}">
											<div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">Reference</div>
													<div class="panel-body">


														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Reference Name <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:input type="text" class="alphabets form-control"
																	id="cand_refName" path="refName" />
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Organization
																Name<span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:input type="text" class="form-control"
																	id="cand_refOrg" path="refOrganisation" />
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Phone Number <span class="stars">*</span></label>
															<div class="col-sm-9">
																<form:input type="text" class="numbersonly form-control"
																	id="cand_refPhone" path="refPhoneNumber" />
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> Email Address </label>
															<div class="col-sm-9">
																<form:input type="text" class="form-control"
																	id="cand_refEmail" path="refEMail" />
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-offset-3 col-sm-9">
																<a id="sbmt_reference" class="btn btn-info"
																	onClick="validateCandRef()">Add Reference</a>
															</div>
														</div>
														<div class="col-md-12">

															<div class="success_msg" id="add_candRef_success">
																<span id="msgs_icn"><i class="fa fa-check"></i></span>
															</div>


															<div class="error_msg" id="add_candRef_error">
																<span id="msge_icn"><i class="fa fa-times"></i></span>
															</div>

														</div>

													</div>
												</div>
											</div>
										</c:if>

									</div>

									<div class="col-md-6">


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Status</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Status </label>
														<%-- <div class="col-sm-9">
															<c:forEach items="${status }" var="sts">
																<input type="radio" name="statusSelect"
																	value="${sts.statusId }" />${sts.status } &nbsp;
                             								</c:forEach>
														</div> --%>
														<div class="col-sm-9">
															<%-- <form:radiobuttons path="status" items="${status }"  itemLabel="status" itemValue="statusId"></form:radiobuttons> --%>
															<form:radiobutton path="status" value="Short Listed" />
															Short Listed
															<form:radiobutton path="status" value="Waiting List" />
															Waiting List
															<form:radiobutton path="status" value="Rejected" />
															Rejected
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a id="sbmt_candStatus" class="btn btn-info">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_candSts_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>
														</div>


														<div class="error_msg" id="add_candSts_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>
														</div>

													</div>


												</div>
											</div>
										</div>
				</form:form>
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Upload Resume</div>
						<div class="panel-body">
							<form:form commandName="jobCandidatesVo"
								action="uploadCandResume.do" enctype="multipart/form-data">
								<form:hidden path="jobCandidateId" id="resCandId" />
								<c:if test="${! empty jobCandidatesVo.resumeVo }">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Resume
											Details </label>
										<div class="col-sm-3">
											${jobCandidatesVo.resumeVo.resumeFileName }</div>
										<div class="col-sm-3">
											<a
												href="downloadCandDoc.do?durl=${jobCandidatesVo.resumeVo.resumeUrl }">View</a>
										</div>
										<div class="col-sm-3">
											<a
												href="deleteCandResume.do?durl=${jobCandidatesVo.resumeVo.resumeUrl }"><i
												class="fa fa-close ss"></i></a>
										</div>
									</div>
								</c:if>

								<div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Upload
										Resume </label>
									<div class="col-sm-9">
										<input type="file" name="uploadCandResume" id="can_resume_btn"
											class="btn btn-info" />
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-offset-16 col-sm-12">
										<button type="submit" id="sbmt_candResume"
											class="btn btn-info">Save</button>
									</div>
								</div>
							</form:form>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
	</div>
	<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
		id="users">
		<div class="panel-body">
			<form:form commandName="jobCandidatesVo" action="uploadCandDocs.do"
				enctype="multipart/form-data">
				<form:hidden path="jobCandidateId" id="docCandId" />
				<c:if test="${ rolesMap.roles['Candidate Documents'].add}">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Upload Documents</div>
							<div class="panel-body">

								<div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Upload
										Documents </label>
									<div class="col-sm-9">
										<input type="file" id="cand_docsss" name="uploadCandDocs"
											class="btn btn-info" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-16 col-sm-12">
										<button type="submit" id="sbmt_cand_docs" class="btn btn-info">Save</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:if>

				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">Show Documents</div>
						<div class="panel-body">
							<table class="table no-margn">
								<thead>
									<tr>
										<th>Document Name</th>
										<c:if test="${ rolesMap.roles['Candidate Documents'].view}">
											<th>view</th>
										</c:if>
										<c:if test="${ rolesMap.roles['Candidate Documents'].delete}">
											<th>Delete</th>
										</c:if>
										<!-- <th>Print</th> -->
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${jobCandidatesVo.candidateDocumentsVos}"
										var="docs">
										<tr>
											<td>${docs.fileName }</td>
											<c:if test="${ rolesMap.roles['Candidate Documents'].view}">
												<td><i class="fa fa-file-text-o sm"></i><a
													href="downloadCandDoc.do?durl=${docs.documentUrl }"> </a></td>
											</c:if>
											<c:if test="${ rolesMap.roles['Candidate Documents'].delete}">
												<td><a href="deleteCandDoc.do?durl=${docs.documentUrl }"><i
														class="fa fa-close ss"></i></a></td>
											</c:if>
										</tr>
									</c:forEach>
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>

	</div>
	<%-- </form:form> --%>
	</div>

	</div>

	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js'/>"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js'/>"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js'/>"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js'/>"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js'/>"></script>

	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>


	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").addClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});

		$("#sbmt_candResume").attr("disabled", "true");
		$("#can_resume_btn").on("change", function() {
			if ($("#cand_id").val() != '')
				$("#sbmt_candResume").removeAttr("disabled");
		});

		if ($("#cand_id").val() != '') {
			enableCandidateButtons();
		} else {
			disableCandidateButtons();
		}

		$("#sbmt_cand_docs").attr("disabled", "true");
		$("#cand_docsss").on("change", function() {
			if ($("#cand_id").val() != '')
				$("#sbmt_cand_docs").removeAttr("disabled");
		});

		function disableCandidateButtons() {
			$("#sbmt_cand_address").attr("disabled", "true");
			$("#sbmt_cand_int").attr("disabled", "true");
			$("#sbmt_achievements").attr("disabled", "true");
			$("#sbmt_addInfo").attr("disabled", "true");
			$("#sbmt_qual").attr("disabled", "true");
			$("#sbmt_skill").attr("disabled", "true");
			$("#sbmt_lang").attr("disabled", "true");
			$("#sbmt_exp").attr("disabled", "true");
			$("#sbmt_reference").attr("disabled", "true");
			$("#sbmt_candStatus").attr("disabled", "true");
		}
		function enableCandidateButtons() {
			$("#sbmt_cand_address").removeAttr("disabled");
			$("#sbmt_cand_int").removeAttr("disabled");
			$("#sbmt_achievements").removeAttr("disabled");
			$("#sbmt_addInfo").removeAttr("disabled");
			$("#sbmt_qual").removeAttr("disabled");
			$("#sbmt_skill").removeAttr("disabled");
			$("#sbmt_lang").removeAttr("disabled");
			$("#sbmt_exp").removeAttr("disabled");
			$("#sbmt_reference").removeAttr("disabled");
			$("#sbmt_candStatus").removeAttr("disabled");
		}
	</script>
</body>
</html>