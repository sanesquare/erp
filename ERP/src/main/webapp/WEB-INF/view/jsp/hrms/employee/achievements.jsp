<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Achievements</h1>
		</div>





		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Achievements</div>
					<div class="panel-body">

						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="30%">Employee Name</th>
									<th width="30%">Achievement Title</th>
									<th width="30%">Achievement Date</th>
									<th width="5%">Edit</th>
									<th width="5%">Delete</th>

								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Trident</td>
									<td><a href="view_achievement.html"> Internet Explorer
											4.0</a></td>
									<td>25-2-2015</td>
									<td>25-2-2015</td>

									<td><a href="add_achievement.html"><i
											class="fa fa-edit sm"></i> </a></td>
									<td><i class="fa fa-close ss"></i></td>
								</tr>
								<tr>
									<td>Trident</td>
									<td><a href="view_achievement.html"> InternetExplorer
											5.0</a></td>
									<td>25-2-2015</td>
									<td>25-2-2015</td>

									<td><a href="add_achievement.html"><i
											class="fa fa-edit sm"></i> </a></td>
									<td><i class="fa fa-close ss"></i></td>
								</tr>



							</tbody>
						</table>
						<a href="add_achievements.do">
							<button type="button" class="btn btn-primary">Add New
								Achievement</button>
						</a>

					</div>
				</div>
			</div>


		</div>




	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>