<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />

<div class="col-sm-9 col-xs-10 emp-pure-supr-list-div">
	<select name="forwaders" id="forwaders" multiple="multiple"
		class="form-control chosen-select forwaders"
		data-placeholder="Select Superior">
		<c:forEach items="${superiorList}" var="superior">
			<option value="${superior.value}">${superior.label}</option>
		</c:forEach>
	</select>
</div>

<script
	src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

<script>
	$(document).ready(function() {
		$(".forwaders").chosen();
	});
</script>
