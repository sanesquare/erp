<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
</head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body style="background: #FFFFFF !important;">
	<div class="col-xs-12" id="table2">
	<h2 class="role_hd" align="center">Employee Leaves Summary</h2>
		<%-- <form:form action="" method="POST" modelAttribute="timeLeavesDetails"> --%>
			<div class="col-md-12">
				<table class="table table-striped no-margn line">
					<tbody>
						<tr>
							<th class="hd_tb" colspan="7"  style="background-color:#0380B0;color: #fff;">Leaves Taken</th>
						</tr>
						<tr>
							<td><strong>S#</strong></td>
							<td><strong>Leave Type</strong></td>
							<td><strong>Reason</strong></td>
							<td><strong>From</strong></td>
							<td><strong>To</strong></td>
							<td><strong>No. of Days</strong></td>
							<td><strong>Status</strong></td>

						</tr>
						<c:choose>
							<c:when test="${! empty timeLeavesDetails.details }">
								<c:forEach items="${timeLeavesDetails.details }" var="details"
									varStatus="status">
									<tr>
										<td align="left">${details.slno }</td>
										<td align="left">${details.leaveType }</td>
										<td align="left">${details.reason }</td>
										<td align="left">${details.fromDateString }</td>
										<td align="left">${details.tillDateString }</td>
										<td align="left">${details.days }</td>
										<td align="left">${details.status }</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td>No Records To Show...</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<div class="col-md-6">
				<br> <br> <br> <br>
				<c:if test="${! empty timeLeavesDetails.summary }">
					<table class="table table-striped no-margn line">
						<tbody>
							<tr>
								<th class="hd_tb" colspan="2"  style="background-color:#0380B0;color: #fff;">Leaves Summary</th>
							</tr>

							<c:forEach items="${timeLeavesDetails.summary }" var="summary"
								varStatus="status1">
								<tr>
									<th colspan="2"><strong>${summary.leaveType }</strong></th>
								</tr>
								<tr>
									<td>Leave Type</td>
									<td>${summary.payType }</td>
								</tr>

								<tr>
									<td>Leaves Quota</td>
									<td>${summary.quota }</td>
								</tr>


								<tr>
									<td>Leaves Taken</td>
									<td>${summary.leavesTaken }</td>
								</tr>


								<tr>
									<td>Remaining Leaves</td>
									<td>${summary.remainingLeaves }</td>
								</tr>
							</c:forEach>




						</tbody>
					</table>
				</c:if>
			</div>
		<%-- </form:form> --%>
	</div>
	<div class="form-group" style="margin-top: 25px;">
		<div class="col-sm-offset-4 col-sm-6">
			<button class="btn btn-info"
				onclick="window.open('data:application/vnd.ms-excel,' + document.getElementById('table2').outerHTML.replace(/ /g, '%20'));">
				Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
			</button>
			<!-- <button type="submit" class="btn btn-info" name="type" value="pdf">
						Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
					</button> 
					 <button type="submit" class="btn btn-info" name="type" value="csv">
						Export to CSV &nbsp; <i class="fa fa-file-text-o"></i>
					</button>
					<button type="submit" class="btn btn-info" name="type" value="html">
						Export to HTML &nbsp; <i class="fa fa-file-code-o"></i>
					</button> -->
		</div>
	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>
</html>
