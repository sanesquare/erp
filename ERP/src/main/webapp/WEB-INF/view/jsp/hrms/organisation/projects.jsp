<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>
				<spring:message code="projects.title" />
			</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">
<!-- <a href="ggg">Test</a> -->
			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">
						<spring:message code="projects.title" />
					</div>
					<div class="panel-body">
						<c:if test="${!empty projects}">
							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered " id="basic-datatable">
								<thead>
									<tr>
										<th width="20%"><spring:message
												code="project.table.title" /></th>
										<th width="20%"><spring:message
												code="project.table.startdate" /></th>
										<th width="20%"><spring:message
												code="project.table.enddate" /></th>
												<c:if test="${ rolesMap.roles['Projects'].update}">
										<th width="10%"><spring:message code="project.table.edit" /></th>
										</c:if>
										<c:if test="${ rolesMap.roles['Projects'].delete}">
										<th width="10%"><spring:message
												code="project.table.delete" /></th>
												</c:if>

									</tr>
								</thead>
								<tbody>
									<c:forEach items="${projects }" var="pro">
										<tr>
										<c:choose>
										<c:when test="${ rolesMap.roles['Projects'].view}">
											<td><a href="viewProject.do?pid=${pro.projectId }">${pro.projectTitle }</a></td>
											</c:when>
											<c:otherwise>
											<td>${pro.projectTitle }</td>
											</c:otherwise>
											</c:choose>
											<td>${pro.projectStartDate }</td>
											<td>${pro.projectEndDate }</td>
											<c:if test="${ rolesMap.roles['Projects'].update}">
											<td><a href="editProject.do?pid=${pro.projectId }"><i
													class="fa fa-edit sm"></i> </a></td>
													</c:if>
														<c:if test="${rolesMap.roles['Projects'].delete}">
											<td><a class="delete_project"
												onclick="confirmDelete(${pro.projectId })"><i
													class="fa fa-close ss"></i></a></td>
													</c:if>
										</tr>
									</c:forEach>

								</tbody>

							</table>
						</c:if>
						<a href="addProjects.do">
						<c:if test="${ rolesMap.roles['Projects'].add}">
							<button type="button" class="btn btn-primary">
								<spring:message code="project.btn.addnewproject" />
							</button>
							</c:if>
						</a>

					</div>
				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->




	<!-- Content Block Ends Here (right box)-->


	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>

	<script type="text/javascript">
    function confirmDelete(id){
		var result=confirm("Confirm delete?");
		 if(result){
			$(".delete_project").attr("href","deleteProject.do?pid="+id);
		} else{
			$(".delete_project").attr("href","#");
		} 
	}
    $(document).ready(function(){
    	$("#dashbordli").removeClass("active");
		$("#organzationli").addClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").removeClass("active");
		$("#reportsli").removeClass("active");
    });
    </script>
	<!-- Custom JQuery -->
	<script src="assets/js/app/custom.js" type="text/javascript"></script>
</body>
</html>