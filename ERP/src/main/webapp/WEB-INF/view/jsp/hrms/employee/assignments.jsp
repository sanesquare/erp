<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Assignments</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>



		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Assignments</div>
					<div class="panel-body">
						<c:if test="${!empty assignments }">
							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered" id="basic-datatable">
								<thead>
									<tr>
										<th width="10%">Project</th>
										<th width="30%">Assignment Name</th>
										<th width="20%">Employee</th>
										<th width="10%">Start Date</th>
										<th width="10%">Due Date</th>
										<th width="10%">Priority</th>
										<c:if test="${ rolesMap.roles['Assignments'].update}">
											<th width="5%">Edit</th>
										</c:if>
										<c:if test="${ rolesMap.roles['Assignments'].delete}">
											<th width="5%">Delete</th>
										</c:if>

									</tr>
								</thead>
								<tbody>
									<c:forEach items="${assignments }" var="a">
										<tr>
											<td>${a.projectTitle }</td>
											<c:choose>
												<c:when test="${ rolesMap.roles['Assignments'].view}">
													<td><a href="viewAssignment.do?id=${a.assignmentId }">${a.assignmentName }</a></td>
												</c:when>
												<c:otherwise>
													<td>${a.assignmentName }</td>
												</c:otherwise>
											</c:choose>
											<td>${a.employee }</td>
											<td>${a.startDate }</td>
											<td>${a.endDate }</td>
											<td>${a.priority }</td>
											<c:if test="${ rolesMap.roles['Assignments'].update}">
												<td><a href="editAssignment.do?id=${a.assignmentId }"><i
														class="fa fa-edit sm"></i> </a></td>
											</c:if>
											<c:if test="${ rolesMap.roles['Assignments'].delete}">
												<td><a class="delete_assign"
													onclick="confirmDelete(${a.assignmentId })"><i
														class="fa fa-close ss"></i></a></td>
											</c:if>
										</tr>
									</c:forEach>


								</tbody>
							</table>
						</c:if>
						<c:if test="${ rolesMap.roles['Assignments'].add}">
							<a href="add_assignment.do">
								<button type="button" class="btn btn-primary">Add New
									Assignment</button>
							</a>
						</c:if>
					</div>
				</div>
			</div>


		</div>




	</div>
	<!-- Content Block Ends Here (right box)-->


	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script type="text/javascript">
function confirmDelete(id) {
	var result = confirm("Are you sure to delete?");
	if (result) {
		$(".delete_assign").attr("href","deleteAssignment.do?id=" + id);
	} else {
		$(".delete_assign").attr("href", "#");
	}
}

$(document).ready(function(){
	$("#dashbordli").removeClass("active");
	$("#organzationli").removeClass("active");
	$("#recruitmentli").removeClass("active");
	$("#employeeli").addClass("active");
	$("#timesheetli").removeClass("active");
	$("#payrollli").removeClass("active");
	$("#reportsli").removeClass("active");
});
</script>
</body>
</html>