<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Incentives</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<a href="commissions.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>


		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>


				</ul>



				<div class="tab-content">
					<form:form action="saveCommission.do" commandName="commissionVo"
						id="commition_add_form" method="POST">
						<form:hidden path="commissionId" id="commissionId" />
						<form:hidden path="id" id="id" />
						<div role="tabpanel"
							class="panel panel-default tab-pane tabs-up active" id="all">
							<div class="panel-body">

								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Incentive Information</div>
												<div class="panel-body">


													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Employee
															Name <span class="stars">*</span></label>
														<div class="col-sm-8 col-xs-10 br-de-emp-list">
															<form:input path="employee" class="form-control"
																placeholder="Search Employee" id="employeeSl" />
															<form:hidden path="employeeCode" id="employeeCode" />

														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div>
													</div>

													<div id="pop1" class="simplePopup">
													</div>

													<div id="pop2" class="simplePopup empl-popup-gen"></div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Title <span class="stars">*</span></label>
														<div class="col-sm-8 col-xs-11">
															<form:select path="titleId" class="form-control"
																id="commission_title">
																<option value="">--Select Incentive--</option>
																<form:options items="${titles }" itemLabel="title"
																	itemValue="id" />
															</form:select>

														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="" id="show1"><i
																class="fa fa-plus-square-o font_size"></i></a>
														</div>
													</div>


													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">
															Forward Application To <span class="stars">*</span></label>
														<div class="col-sm-9">

															<form:select cssClass="form-control chosen-select"
																path="superiorIds" id="commission_superiors"
																multiple="true">
															</form:select>
															<form:hidden path="superiorAjaxIds"
																id="commission_suprior_ajx_hidn" />
														</div>

													</div>




													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Amount <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="amount" cssClass="form-control double"
																id="commission_amount" />
														</div>
													</div>





													<div class="form-group">
														<label class="col-sm-3 control-label"> Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date adjustment_date"
																id="datepicker">
																<form:input path="date" class="form-control"
																	style="background-color: #ffffff !important;"
																	id="commission_date" readonly="true"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>

													</div>




												</div>
											</div>
										</div>






									</div>

									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Incentive Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">


															<form:textarea cssClass=" form-control"
																id="commission_description" path="description"
																cssStyle="height: 150px;resize:none" />



														</div>
													</div>





												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" class="form-control height"
																id="commission_notes" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${commissionVo.createdBy }</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${commissionVo.createdOn }</label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								</div>



							</div>
						</div>
					</form:form>



				</div>
			</div>


		</div>

		<div id="pop11" class="simplePopup">
			<div class="row">
				<form:form method="POST" commandName="commissionTitleVo"
					action="saveCommissionTitle.do">
					<div class="form-group">
						<h1 class="pop_hd">Incentives</h1>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Available
										Incentives </label>
									<div class="col-sm-9" id="avai_commission_titles"></div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">New
										Incentive </label>
									<div class="col-sm-9">
										<form:textarea path="title" id="commission_title_textre"
											cssClass="form-control height" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-9">
										<button type="button" id="commissn_title_sve"
											class="btn btn-info">Save</button>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<span id="cmmission_title_msg" class="hideMe imsuccess"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>



	</div>
	<!-- Warper Ends Here (working area) -->


	<!-- Content Block Ends Here (right box)-->



	<!-- JQuery v1.9.1 -->
	<script
		src='<c:url value="/resources/assets/js/jquery/jquery-1.9.1.min.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/plugins/underscore/underscore-min.js" />'></script>

	<!-- Bootstrap -->
	<script
		src='<c:url value="/resources/assets/js/bootstrap/bootstrap.min.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/globalize/globalize.min.js" />'></script>
	<!-- NanoScroll -->
	<script
		src='<c:url value="/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js" />'></script>
	<!-- TypeaHead -->
	<script
		src='<c:url value="/resources/assets/js/plugins/typehead/typeahead.bundle.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js" />'></script>

	<!-- InputMask -->
	<script
		src='<c:url value="/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js" />'></script>

	<!-- TagsInput -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" />'></script>

	<!-- Chosen -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js" />'></script>

	<!-- moment -->
	<script src='<c:url value="/resources/assets/js/moment/moment.js" />'></script>
	<!-- DateTime Picker -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js" />'></script>

	<!-- Wysihtml5 -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js" />'></script>

	<!-- Custom JQuery -->
	<script src='<c:url value="/resources/assets/js/app/custom.js" />'></script>


	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script src="<c:url value='/resources/assets/js/settings-ajax.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#employeeSl').bind('click', function() {
				showPop();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			getEmployeeDetails();
			$('#show1').click(function() {
				getAvailableCommissionTitles();
				$('#pop11').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>

</body>
</html>