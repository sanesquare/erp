<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/app/app.v1.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
<style type="text/css">
#add_int_success {
	display: none;
}

#add_int_error {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Job Interviews</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="jobInterviews.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="success_msg" id="add_int_success" style="display: none">
					<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
				</div>
				<div class="error_msg" id="add_int_error" style="display: none">
					<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
				</div>
			</div>
		</div>
		<div class="row">

			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>

				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<form:form action="saveJobInterview.do" commandName="jobInterviewVo"
							method="POST" enctype="multipart/form-data">
							<form:hidden id="add_int_id" path="jobInterviewId"></form:hidden>

							<div class="panel-body">

								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Job Interview Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Job
															Post <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select class="form-control required"
																id="add_int_posts" path="jobPostId">
																<c:forEach items="${jobPosts}" var="post">
																	<form:option value="${post.jobPostId }">${post.jobTitle }</form:option>
																</c:forEach>
															</form:select>

														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Interview
															Date <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepicker">
																<form:input type="text" readonly="true"
																	style="background-color: #ffffff !important;"
																	data-date-format="DD/MM/YYYY" class="form-control"
																	id="add_int_Date" path="interviewDate" />
																<span id="end_date" class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Interview
															Time <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<div class="input-group date" id="timepicker">
																<form:input type="text" readonly="true"
																	style="background-color: #ffffff !important;"
																	class="form-control" id="add_int_Time"
																	path="interviewTime" />
																<span class="input-group-addon"><span
																	class="glyphicon-time glyphicon"></span> </span>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label ">Interviewers
															(Employees)<span class="stars">*</span>
														</label>
														<div class="col-sm-9 ">
															<form:select class="required form-control chosen-select"
																multiple="true" data-placeholder="Select Option"
																id="add_int_interviewers" path="interViewerIds">
																<c:forEach items="${interviewers}" var="interviewer">
																	<form:option value="${interviewer.employeeId }" label="${interviewer.name } ( ${interviewer.employeeCode } )"></form:option>
																	<%-- <form:options items="${interviewers}" itemLabel="employeeCode" itemValue="employeeId"/> --%>
																</c:forEach>
															</form:select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Status <span class="stars">*</span>
														</label>

														<div class="col-sm-9 ">
															<form:select class="required form-control chosen-select"
																multiple="true" path="statusId" id="add_int_sts">
																<form:options items="${status}" itemValue="statusId"
																	itemLabel="status" />
															</form:select>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">No.of
															Interviewees (Per Day)<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input type="text" class="numbersonly form-control"
																id="add_int_nos" path="selectionNumber"
																placeholder="No.of Interviewees (Per Day)" />
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Place of
															Interview <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input type="text" class="form-control alphabets"
																id="add_int_place" path="place" placeholder="Place" />
														</div>

													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a class="btn btn-info" id="sbmt_int_info"
																onClick="validateIntBasicInfo()">Save</a>
														</div>
													</div>

												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Job Interview Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">


															<form:textarea class=" form-control"
																style="height: 150px;resize:none;" id="add_int_desc"
																path="description" />

														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a class="btn btn-info" id="submt_int_desc"
																onclick="validateIntDescription()">Save</a>
														</div>
													</div>

												</div>
											</div>
										</div>

									</div>

									<div class="col-md-6">

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea class="form-control height"
																id="add_int_add_info" path="additionalInfo" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<form:label for="inputPassword3"
																class="col-sm-9 control-label" path="createdBy"></form:label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<form:label for="inputPassword3"
																class="col-sm-9 control-label" path="createdOn">
															</form:label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a class="btn btn-info" id="submt_int_add_info"
																onClick="validateIntAddInfo()">Save</a>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								</div>



							</div>
						</form:form>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<form:form action="uploadDocs.do" method="POST"
								commandName="jobInterviewVo" enctype="multipart/form-data">
								<form:hidden id="doc_int_id" path="jobInterviewId"></form:hidden>
								<c:if test="${ rolesMap.roles['Job Interview Documents'].add}">
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Upload Documents</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Upload
														Documents </label>
													<div class="col-sm-9">
														<input type="file" id="interview_docs" multiple
															name="intUploadDocs" class="btn btn-info" />
													</div>
												</div>
												<button class="btn btn-info" id="interview_docs_btn"
													type="submit">Upload</button>

											</div>
										</div>
									</div>
								</c:if>

								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">


											<table class="table no-margn">
												<thead>
													<tr>
														<th>Document Name</th>
														<c:if
															test="${ rolesMap.roles['Job Interview Documents'].view}">
															<th>view</th>
														</c:if>
														<c:if
															test="${ rolesMap.roles['Job Interview Documents'].delete}">
															<th>Delete</th>
														</c:if>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${jobInterviewVo.documentVos}" var="docs">
														<tr>
															<td>${docs.fileName }</td>
															<c:if
																test="${ rolesMap.roles['Job Interview Documents'].view}">
																<td><a
																	href="downloadIntDoc.do?durl=${docs.documentUrl }"> <i
																		class="fa fa-file-text-o sm"></i></a></td>
															</c:if>
															<c:if
																test="${ rolesMap.roles['Job Interview Documents'].delete}">
																<td><a
																	href="deleteIntDoc.do?durl=${docs.documentUrl }"><i
																		class="fa fa-close ss"></i></a></td>
															</c:if>
														</tr>
													</c:forEach>
												</tbody>
											</table>


										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>

				</div>

			</div>
		</div>

	</div>

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js'/>"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js'/>"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js'/>"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js'/>"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js'/>"></script>

	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>


	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").addClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");

			$("#interview_docs_btn").attr("disabled", "true");

			if ($("#add_int_id").val() != '') {
				enableInterviewButtons();
			} else {
				disableInterviewButtons();
			}

			$("#interview_docs").on("change", function() {
				if ($("#add_int_id").val() != '')
					$("#interview_docs_btn").removeAttr("disabled");
			});
		});

		function enableInterviewButtons() {
			$("#submt_int_desc").removeAttr("disabled");
			$("#submt_int_add_info").removeAttr("disabled");
		}

		function disableInterviewButtons() {
			$("#submt_int_desc").attr("disabled", "true");
			$("#submt_int_add_info").attr("disabled", "true");
		}
	</script>
</body>
</html>