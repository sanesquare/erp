<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />

</head>


<div class="warper container-fluid">
	<div class="page-header">
		<h1>HR Report</h1>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="parentVerticalTab">
				<ul class="resp-tabs-list hor_1">
					<c:if test="${ rolesMap.roles['Employee List'].view}">
						<li>Employee List</li>
					</c:if>
					<c:if test="${ rolesMap.roles['HR Summary'].view}">
						<li>HR Summary</li>
					</c:if>
					<%-- <c:if test="${ rolesMap.roles['Payroll Summary'].view}">
						<li>Payroll Summary</li>
					</c:if> --%>
					<c:if test="${ rolesMap.roles['Holidays Calendar'].view}">
						<li>Holidays Calendar</li>
					</c:if>
					<c:if test="${ rolesMap.roles['Leave Reports'].view}">
						<li>Leaves Reports</li>
					</c:if>
					<c:if test="${ rolesMap.roles['Project Employees'].view}">
						<li>Project Employees</li>
					</c:if>
					<li>Branch Report</li>
				</ul>
				<div class="resp-tabs-container hor_1">
					<c:if test="${ rolesMap.roles['Employee List'].view}">
						<div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Employees List</div>
									<form:form commandName="hrEmpReportVo" action="viewDetailedEmpreport.do"
										method="POST" id="hrEmpListForm">
										<div class="panel-body">

											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label"> Branch</label>
												<div class="col-sm-9">
													<form:select class="form-control" path="bId">

														<form:option label="All Branches" value="0" />
														<form:options items="${branches }" itemValue="branchId"
															itemLabel="branchName" />

													</form:select>

												</div>
											</div>

											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label">Department</label>
												<div class="col-sm-9">

													<form:select class="form-control" path="depId">
														<form:option label="All Departments" value="0" />
														<form:options items="${departments }" itemValue="id"
															itemLabel="name" />

													</form:select>
												</div>
											</div>



											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label"> Employee
													Type</label>
												<div class="col-sm-9">
													<form:select class="form-control" path="typeId">
														<form:option label="All Types" value="0" />
														<form:options items="${types }" itemValue="employeeTypeId"
															itemLabel="employeeType" />

													</form:select>
												</div>
											</div>


											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label"> Employee
													Category</label>
												<div class="col-sm-9">

													<form:select class="form-control" path="cateId">
														<form:option label="All Types" value="0" />
														<form:options items="${categories }"
															itemValue="employeeCategoryId"
															itemLabel="employeeCategory" />

													</form:select>
												</div>
											</div>



											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label"> Employee
													Status</label>
												<div class="col-sm-9">

													<form:select class="form-control" path="statusId">
														<form:option label="All Types" value="0" />
														<form:options items="${status }" itemValue="id"
															itemLabel="status" />

													</form:select>
												</div>
											</div>

											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-6">
													<button type="submit" class="btn btn-info">Generate
														Report</button>
												</div>
											</div>


										</div>
									</form:form>
								</div>
							</div>
						</div>
					</c:if>

					<c:if test="${ rolesMap.roles['HR Summary'].view}">
						<div>

							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">HR Summary</div>
									<form:form action="viewHrSummary.do" >
										<div class="panel-body">
											<div class="form-group">
												<div class=" col-sm-6">
													<button type="submit" class="btn btn-info">Generate
														Report</button>

												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>

						</div>
					</c:if>
					<%-- <c:if test="${ rolesMap.roles['Payroll Summary'].view}">
						<div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Payroll Summary</div>
									<form:form action="viewPaySummary"  >
										<div class="panel-body">
											<div class="form-group">
												<div class=" col-sm-6">
													<button type="submit" class="btn btn-info">Generate
														Report</button>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</c:if> --%>
					<!-- Holidays -->
					<c:if test="${ rolesMap.roles['Holidays Calendar'].view}">
						<div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Holidays Calendar</div>
									<form:form action="generateHolidayReport.do"
										commandName="holidayVo" method="POST" id="hrHoliForm">
										<div class="panel-body">
											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label"> Branch</label>
												<div class="col-sm-9">
													<form:select class="form-control" path="branchId">
														<form:option label="All Branches" value="0" />
														<form:options items="${branches }" itemValue="branchId"
															itemLabel="branchName" />
													</form:select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Date Range
													Start<span class="stars">*</span></label>
												<div class="col-sm-9">
													<div class="input-group date holDate" id="datepicker">
														<form:input type="text" class="form-control"
															id="holStartDate" path="startDate"
															data-date-format="DD/MM/YYYY" readonly="true"
															style="background-color:#fff !important" />
														<span class="input-group-addon"><span
															class="glyphicon-calendar glyphicon"></span> </span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Date Range End<span class="stars">*</span></label>
												<div class="col-sm-9">
													<div class="input-group date holDate" id="datepickers">
														<form:input type="text" class="form-control"
															path="endDate" id="holEndDate"
															data-date-format="DD/MM/YYYY" readonly="true"
															style="background-color:#fff !important" />
														<span class="input-group-addon"><span
															class="glyphicon-calendar glyphicon"></span> </span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-6">

													<button type="submit" class="btn btn-info">Generate
														Report</button>

												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</c:if>

					<!-- Leaves -->
					<c:if test="${ rolesMap.roles['Leave Reports'].view}">
						<div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Leave Reports</div>
									<form:form action="generateLeavesReport.do"  
										commandName="hrLeavesVo" method="post">
										<div class="panel-body">
											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label"> Year <span class="stars">*</span></label>
												<div class="col-sm-9">
													<form:select class="form-control" path="year">

														<c:forEach items="${years }" var="year">
															<form:option value="${year}" label="${ year}" />
														</c:forEach>
													</form:select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Branch</label>
												<div class="col-sm-9">
													<form:select path="branchId"
														style="background-color: #ffffff !important;"
														cssClass="form-control">
														<form:option value="0" label="All Branches " />
														<form:options items="${branches }" itemLabel="branchName"
															itemValue="branchId" />
													</form:select>
												</div>

											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Department </label>
												<div class="col-sm-9">
													<form:select path="depId"
														style="background-color: #ffffff !important;"
														cssClass="form-control">
														<form:option value="0" label="All Departments" />
														<form:options items="${departments }" itemLabel="name"
															itemValue="id" />
													</form:select>
												</div>

											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Employee</label>
												<div class="col-sm-9 ">
													<form:select cssClass="form-control " path="empId"
														id="leave_report_employees">
														<form:option value="0" label="All Employees" />
														<form:options items="${employees }" itemValue="employeeId"
															itemLabel="name" />
													</form:select>

												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-6">

													<button type="submit" class="btn btn-info">Generate
														Report</button>

												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</c:if>
					<!-- Project Employee  -->
					<c:if test="${ rolesMap.roles['Project Employees'].view}">
						<div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Project Employees</div>
									<form:form action="viewEmpProjectReport.do" method="POST" >
										<div class="panel-body">
											<div class="form-group">
												<div class=" col-sm-6">

													<button type="submit" class="btn btn-info">Generate
														Report</button>

												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</c:if>
					<div>

							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Branch Report</div>
									<form:form action="generateBranchReport.do" method="POST"
									commandName="hrBranchVo">
										<div class="panel-body">
										<div class="form-group">
												<label class="col-sm-3 control-label">Date Range
													Start<span class="stars">*</span></label>
												<div class="col-sm-9">
													<div class="input-group date holDate" id="datepickerss">
														<form:input type="text" class="form-control"
															id="holStartDate" path="startDate"
															data-date-format="DD/MM/YYYY" readonly="true"
															style="background-color:#fff !important" />
														<span class="input-group-addon"><span
															class="glyphicon-calendar glyphicon"></span> </span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Date Range End<span class="stars">*</span></label>
												<div class="col-sm-9">
													<div class="input-group date holDate" id="datepickersss">
														<form:input type="text" class="form-control"
															path="endDate" id="holEndDate"
															data-date-format="DD/MM/YYYY" readonly="true"
															style="background-color:#fff !important" />
														<span class="input-group-addon"><span
															class="glyphicon-calendar glyphicon"></span> </span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class=" col-sm-6">
													<button type="submit" class="btn btn-info">Generate
														Report</button>

												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>

						</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script
	src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

<script
	src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
<script src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
<script src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
<script src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
<!-- New  -->
<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
<script>
	$(document).ready(function() {
		$('#parentVerticalTab').easyResponsiveTabs({

			type : 'vertical', //Types: default, vertical, accordion 
			width : 'auto', //auto or any width like 600px 
			fit : true, // 100% fit in a container 
			closed : 'accordion', // Start closed if in accordion view 
			tabidentify : 'hor_1', // The tab groups identifier 
			activate : function(event) { // Callback function if tab is switched 
				var $tab = $(this);
				var $info = $('#nested-tabInfo2');
				var $name = $('span', $info);
				$name.text($tab.text());
				$info.show();
			}
		});

		$("#dashbordli").removeClass("active");
		$("#organzationli").removeClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").removeClass("active");
		$("#reportsli").addClass("active");

		if ("${holidayVo.startDate}" == "")
			$("#holStartDate").val(convertDate(new Date()));
		if ("${holidayVo.endDate}" == "")
			$("#holEndDate").val(convertDate(new Date()));
	});
</script>

</body>
</html>