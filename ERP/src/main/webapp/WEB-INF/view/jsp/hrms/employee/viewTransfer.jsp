<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="com.hrms.web.constants.StatusConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Transfers</h1>
		</div>
		<input type="hidden" value="<%=StatusConstants.FORWARD%>" id="sttuss" />

		<div class="row">
			<div class="col-md-12">
				<a href="transfer.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>


		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>

				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Transfer Information</div>
											<div class="panel-body">


												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3  control-label">Employee
														to Transfer</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${transferVo.employee }</label>
													</div>


												</div>






												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3  control-label">
														Forward Application To</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${transferVo.superior }</label>
													</div>


												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Transfer Date</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${transferVo.date }</label>
													</div>

												</div>



												<div class="form-group">
													<label class="col-sm-3 control-label">Transfer To
														(Department)</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${transferVo.department }</label>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Transfer To
															(Branch)</label>
														<div class="col-sm-9">
															<label for="inputEmail3"
																class="col-sm-3 control-label ft">${transferVo.branch }</label>
														</div>





													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Transfer Description</div>
											<div class="panel-body">

												<div class="form-group">

													<div class="col-sm-12">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${transferVo.description }</label>
													</div>
												</div>


											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Status</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Status
													</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${transferVo.status }</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Description
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${transferVo.statusDescription }</label>
													</div>
												</div>


											</div>
										</div>
									</div>

									<c:if test="${transferVo.viewStatus }">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Status</div>
												<div class="panel-body">
													<form:form action="updateTransferStatus.do" method="POST"
														commandName="transferVo">
														<form:hidden path="transferId" />
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Status </label>
															<div class="col-sm-9">
																<form:select path="status" class="form-control"
																	id="transferStatsu">
																	<option value="">--Select Status--</option>
																	<form:options items="${status }" itemLabel="status"
																		itemValue="status" />
																</form:select>
															</div>
														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Description </label>
															<div class="col-sm-9">
																<form:textarea path="statusDescription"
																	class="form-control height" />
															</div>
														</div>
														<div class="form-group" id="show_forward_to">
															<label for="inputEmail3"
																class="col-sm-3 col-xs-12 control-label">
																Forward Application To</label>
															<div class="col-sm-8 col-xs-10">
																<form:select path="superiorCode" class="form-control"
																	id="spripr_list">
																	<option>--Select--</option>
																	<form:options items="${superiorList }"
																		itemLabel="label" itemValue="value" />
																</form:select>
															</div>
															<!-- <div class="col-sm-1 col-xs-1">
															<a href="#" class="show1"><i
																class="fa fa-plus-square-o pop_icon"></i></a>
														</div> -->
														</div>

														<div class="form-group">
															<div class="col-sm-offset-3 col-sm-9">
																<button type="submit" class="btn btn-info">Save</button>
															</div>
													</form:form>
												</div>
											</div>
										</div>
									</c:if>
								</div>

								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Additional Information</div>
										<div class="panel-body">

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Notes
												</label>
												<div class="col-sm-9">
													<label for="inputPassword3"
														class="col-sm-8 control-label ft">${transferVo.notes }</label>
												</div>
											</div>


											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Record
													Added By </label>
												<div class="col-sm-9">
													<label for="inputPassword3" class="col-sm-9 control-label">${transferVo.createdBy }</label>
												</div>
											</div>

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Record
													Added on </label>
												<div class="col-sm-9">
													<label for="inputPassword3" class="col-sm-9 control-label">${transferVo.createdOn }
													</label>
												</div>
											</div>




										</div>
									</div>
								</div>

							</div>
						</div>



					</div>
				</div>
				<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
					id="users">
					<div class="panel-body">



						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">Show Documents</div>
								<div class="panel-body">


									<c:if test="${! empty transferVo.documentsVos }">
										<table class="table no-margn">
											<thead>
												<tr>
													<th>#</th>
													<th>Document Name</th>
													<c:if test="${ rolesMap.roles['Transfer Documents'].view}">
														<th>view</th>
													</c:if>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${transferVo.documentsVos }" var="d"
													varStatus="s">
													<tr>
														<td>${s.count }</td>
														<td><i class="fa fa-file-o"></i>${d.fileName }</td>
														<c:if test="${ rolesMap.roles['Transfer Documents'].view}">
															<td><a href="downloadTransferDocument.do?url=${d.url }"><i
																	class="fa fa-file-text-o sm"></i></a></td>
														</c:if>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</c:if>


								</div>
							</div>
						</div>




					</div>
				</div>
				<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
					id="docs">
					<div class="panel-body">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading">Simple Map</div>
								<div class="panel-body">
									<div id="simpleMap" style="height: 500px;"></div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>


	</div>




	</div>
	<!-- Warper Ends Here (working area) -->




	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#spripr_list").val('');

			var status = $("#sttuss").val();

			if ($("#transferStatsu").val() == status)
				$("#show_forward_to").css("display", 'block');
			else
				$("#show_forward_to").css("display", 'none');
			$("#transferStatsu").on("change", function() {
				var id = $("#transferStatsu").val();
				if (id == status)
					$("#show_forward_to").css("display", 'block');
				else
					$("#show_forward_to").css("display", 'none');
			})

			/* $('#employeeSl').bind('click', function() {
				getAllBranches();
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			}); */

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>