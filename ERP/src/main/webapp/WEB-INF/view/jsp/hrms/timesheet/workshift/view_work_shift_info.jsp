<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/timepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">Worksheet Information</div>
		<form action="saveWorkShiftDetails.do" method="post"
			id="workShiftDetailsForm">
			<div class="panel-body">
				<div class="col-sm-6">
					<div class="form-group">
						<label class="col-sm-3 control-label">Employee Code</label>
						<div class="col-sm-9">
							<select class="form-control chosen-select wrk-shft-emp-cod"
								data-placeholder="Choose Employee Code" name="employeeCode">
								<option></option>
								<c:forEach items="${employeeList}" var="employee">
									<%-- <c:choose>
										<c:when test="${employeeCode==employee.value}">
											<option value="${employee.value}" selected="true">${employee.label}</option>
										</c:when>
										<c:otherwise>
											<option value="${employee.value}">${employee.label}</option>
										</c:otherwise>
									</c:choose> --%>
									<option value="${employee.value}"
										${employeeCode == employee.value ? 'selected' : ''}>${employee.label}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
				<%-- <div class="col-sm-6">
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">
							Shift Name</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="shiftType"
								id="shiftType" value="${employeeShiftType}"
								placeholder="Shift Name" readonly="true"
								style="background-color: #fff !important">
						</div>
					</div>
				</div> --%>

				<table class="table table-bordered no-margn" border="1">
					<tbody>
						<tr>
							<td colspan="2" rowspan="2" align="center">Work Days</td>
							<td colspan="2" align="center">Regular Work Hours</td>
							<td colspan="3" align="center">Refreshment Break Hours</td>
							<td colspan="3" align="center">Additional Break Hours</td>
						</tr>
						<tr>
							<td align="center">From</td>
							<td align="center">To</td>
							<td colspan="2" align="center">From</td>
							<td align="center">To</td>
							<td colspan="2" align="center">From</td>
							<td align="center">To</td>
						</tr>
						<c:forEach items="${workShiftList}" var="workShift"
							varStatus="status">
							<tr>
								<c:choose>
									<c:when test="${workShift.available}">
										<td colspan="2"><input type="checkbox" checked="true"
											name="shiftDay${status.count}" id="shiftDay${status.count}" />
											&nbsp; ${workShift.shiftDay}</td>
										<input type="hidden" name="workShiftId${status.count}"
											value="${workShift.workShiftId}" />
										<td>
											<!-- <input type="checkbox" class="flt_l" />&nbsp;  -->
											<input type="text" name="time_setter${(status.count-1)*6+1}"
											id="time_setter${(status.count-1)*6+1}"
											class="form-control tadj"
											value="${workShift.regularWorkFrom}" />
										</td>
										<td><input type="text" disabled="false"
											name="time_setter${(status.count-1)*6+2}"
											id="time_setter${(status.count-1)*6+2}"
											class="form-control tadj" value="${workShift.regularWorkTo}" /></td>
										<td colspan="2">
											<!-- <input type="checkbox" class="flt_l" />&nbsp; -->
											<input type="text" disabled="false"
											name="time_setter${(status.count-1)*6+3}"
											id="time_setter${(status.count-1)*6+3}"
											class="form-control tadj"
											value="${workShift.refreshmentBreakFrom}" />
										</td>
										<td><input type="text" disabled="false"
											name="time_setter${(status.count-1)*6+4}"
											id="time_setter${(status.count-1)*6+4}"
											class="form-control tadj"
											value="${workShift.refreshmentBreakTo}" /></td>
										<td colspan="2">
											<!-- <input type="checkbox" class="flt_l" />&nbsp; --> <input
											type="text" disabled="false"
											name="time_setter${(status.count-1)*6+5}"
											id="time_setter${(status.count-1)*6+5}"
											class="form-control tadj"
											value="${workShift.additionalBreakFrom}" />
										</td>
										<td><input type="text" disabled="false"
											name="time_setter${(status.count-1)*6+6}"
											id="time_setter${(status.count-1)*6+6}"
											class="form-control tadj"
											value="${workShift.additionalBreakTo}" /></td>
									</c:when>
									<c:otherwise>
										<td colspan="2"><input type="checkbox"
											name="shiftDay${status.count}" id="shiftDay${status.count}" />
											&nbsp; ${workShift.shiftDay}</td>
										<input type="hidden" name="workShiftId${status.count}"
											value="0" />
										<td>
											<!-- <input type="checkbox" class="flt_l" />&nbsp;  -->
											<input type="text" name="time_setter${(status.count-1)*6+1}"
											id="time_setter${(status.count-1)*6+1}"
											class="form-control tadj" value="" />
										</td>
										<td><input type="text"
											name="time_setter${(status.count-1)*6+2}"
											id="time_setter${(status.count-1)*6+2}"
											class="form-control tadj" value="" /></td>
										<td colspan="2">
											<!-- <input type="checkbox" class="flt_l" />&nbsp; -->
											<input type="text" name="time_setter${(status.count-1)*6+3}"
											id="time_setter${(status.count-1)*6+3}"
											class="form-control tadj" value="" />
										</td>
										<td><input type="text"
											name="time_setter${(status.count-1)*6+4}"
											id="time_setter${(status.count-1)*6+4}"
											class="form-control tadj" value="" /></td>
										<td colspan="2">
											<!-- <input type="checkbox" class="flt_l" />&nbsp; --> <input
											type="text" name="time_setter${(status.count-1)*6+5}"
											id="time_setter${(status.count-1)*6+5}"
											class="form-control tadj" value="" />
										</td>
										<td><input type="text"
											name="time_setter${(status.count-1)*6+6}"
											id="time_setter${(status.count-1)*6+6}"
											class="form-control tadj" value="" /></td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<br />
				<div class="form-group flr">
					<button type="submit" class="btn btn-info">Save</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

