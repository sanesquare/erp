<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Job Candidates</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="jobCandidates.do" class="btn btn-info"
					style="float: right;"><i class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Candidate Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Job
														Field </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.jobField }</label>

													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">First
														Name</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.firstName }</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Last
														Name</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.lastName }</label>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Date of Birth
													</label>
													<div class="col-sm-9">

														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.birthDate }</label>

													</div>

												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Gender</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.gender }</label>
													</div>

												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Nationality</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.nationality }</label>
													</div>

												</div>






											</div>
										</div>
									</div>




									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Contact Information</div>
											<div class="panel-body">


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Address
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.address }</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">City</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.city }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">State/Province</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.state }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Zip
														code/Postal Code</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.zipCode }</label>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Country</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.country }</label>
													</div>

												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Email
														Address</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.eMail }</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Phone
														Number</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.phoneNumber }</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Mobile
														Number</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.mobileNumber }</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Interests</div>
											<div class="panel-body">
												<div class="form-group">
													<div class="col-md-12">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${candidate.interests }</label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Achievements</div>
											<div class="panel-body">
												<div class="form-group">
													<div class="col-md-12">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${candidate.achievements }</label>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">

												<c:if test="${ rolesMap.roles['Candidate Notes'].view}">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-8 control-label ft">${candidate.additionalInfo }</label>
														</div>
													</div>
												</c:if>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft">${candidate.createdBy }
														</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft">${candidate.createdOn }</label>
													</div>
												</div>

											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<c:if
										test="${ rolesMap.roles['Job Candidate Qualifications'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">View Qualifications</div>
												<div class="panel-body">



													<table class="table no-margn">
														<thead>
															<tr>

																<th>Degree</th>
																<th>Subject</th>
																<th>Institute</th>
																<th>Grade / GPA</th>
																<th>Graduation Year</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${candidate.candidateQualifications }"
																var="qual">
																<tr>

																	<td>${qual.degree }</td>
																	<td>${qual.subject }</td>
																	<td>${qual.institute }</td>
																	<td>${qual.grade }</td>
																	<td>${qual.gradYear }</td>

																</tr>
															</c:forEach>
														</tbody>
													</table>


												</div>
											</div>
										</div>
									</c:if>

									<c:if test="${ rolesMap.roles['Job Candidate Skills'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Skills</div>
												<div class="panel-body">
													<table class="table no-margn">
														<thead>
															<tr>

																<th>Skill</th>
																<th>Skill Level</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${candidate.candidateSkills }"
																var="skill">
																<tr>

																	<td>${skill.skill }</td>
																	<td>${skill.skillLevel }</td>

																</tr>
															</c:forEach>
														</tbody>
													</table>

												</div>
											</div>
										</div>
									</c:if>
								</div>

								<c:if test="${ rolesMap.roles['Job Candidate Languages'].view}">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Language</div>
												<div class="panel-body">

													<table class="table no-margn">
														<thead>
															<tr>

																<th>Language</th>
																<th>Speaking Level</th>
																<th>Reading Level</th>
																<th>Writing Level</th>

															</tr>
														</thead>
														<tbody>
															<c:forEach items="${candidate.candidatesLanguages }"
																var="lang">
																<tr>

																	<td>${lang.language }</td>
																	<td>${lang.speakLevel }</td>
																	<td>${lang.writeLevel }</td>
																	<td>${lang.readLevel }</td>


																</tr>
															</c:forEach>
														</tbody>
													</table>

												</div>
											</div>
										</div>
									</div>
								</c:if>

							</div>
							<div class="row">
								<div class="col-md-6">
									<c:if
										test="${ rolesMap.roles['Job Candidate Work Experiences'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Work Experience</div>
												<div class="panel-body">

													<table class="table no-margn">
														<thead>
															<tr>

																<th>Organization Name</th>
																<th>Job Designation</th>
																<th>Job Field</th>


															</tr>
														</thead>
														<tbody>
															<c:forEach items="${candidate.candidateWorkExperiences}"
																var="exp">
																<tr>

																	<td>${exp.orgName }</td>
																	<td>${exp.designation }</td>
																	<td>${exp.jobField }</td>

																</tr>
															</c:forEach>
														</tbody>
													</table>


												</div>
											</div>
										</div>
									</c:if>

									<c:if
										test="${ rolesMap.roles['Job Candidate References'].view}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Reference</div>
												<div class="panel-body">
													<table class="table no-margn">
														<thead>
															<tr>

																<th>Reference Name</th>
																<th>Organisation Name</th>
																<th>Phone Number</th>
																<th>Email Address</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${candidate.candidatesReferences }"
																var="ref">
																<tr>

																	<td>${ref.name }</td>
																	<td>${ref.orgName }</td>
																	<td>${ref.phoneNumber }</td>
																	<td>${ref.eMail }</td>

																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</c:if>
								</div>
								<div class="col-md-6">


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Status</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Status </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft">${candidate.status }</label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Resume</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Resume
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-3 control-label ft"><a
															href="downloadCandDoc.do?durl=${candidate.resumeVo.resumeUrl }">
																${candidate.resumeVo.resumeFileName}</a></label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">

							<!-- <div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Upload Documents</div>
									<div class="panel-body">

										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Upload
												Documents </label>
											<div class="col-sm-9">
												<input type="file" class="btn btn-info" />
											</div>
										</div>


									</div>
								</div>
							</div> -->
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">


										<table class="table no-margn">
											<thead>
												<tr>
													<th>Document Name</th>
													<c:if test="${ rolesMap.roles['Candidate Documents'].view}">
														<th>view</th>
													</c:if>
													<!-- <th>Delete</th>
													<th>Print</th> -->
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${candidate.candidateDocumentsVos}"
													var="docs">
													<tr>
														<td>${docs.fileName }</td>
														<c:if
															test="${ rolesMap.roles['Candidate Documents'].view}">
															<td><a
																href="downloadCandDoc.do?durl=${docs.documentUrl }"> <i
																	class="fa fa-file-text-o sm"></i></a></td>
														</c:if>
														<!-- <td><a href="deleteCandDoc?durl="><i class="fa fa-close ss"></i></a></td>
														<td><a href="printCandDoc?durl="><i class="fa fa-print"></i></a></td> -->
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js'/>"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js'/>"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js'/>"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js'/>"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js'/>"></script>

	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>


	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").addClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>