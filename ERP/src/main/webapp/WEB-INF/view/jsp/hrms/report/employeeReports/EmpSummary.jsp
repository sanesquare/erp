<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
</head>
<body style="background: #FFFFFF !important;">

	<div class="col-xs-12" id="table12">
		<h2 class="role_hd" align="center">Employee Summary</h2>
		<div class="col-md-3">
			<table class="table table-striped no-margn line">
				<tbody>
					<tr>
						<td class="hd_tb" colspan="2" style="background-color:#0380B0;color: #fff;"><span class="WhiteHeading">Employee
								Information</span></td>

					</tr>
					<tr>
						<td>Employee Code</td>
						<td>${employee.detail.empCode }</td>
					</tr>
					<tr>
						<td>First Name</td>
						<td>${employee.detail.fName }</td>
					</tr>
					<tr>
						<td>Last Name</td>
						<td>${employee.detail.lName }</td>
					</tr>
					<tr>
						<td>Gender</td>
						<td>${employee.detail.gender }</td>
					</tr>
					<tr>
						<td>Designation</td>
						<td>${employee.detail.designation }</td>
					</tr>
					<tr>
						<td>Department</td>
						<td>${employee.detail.department }</td>
					</tr>
					<tr>
						<td>Branch</td>
						<td>${employee.detail.branch }</td>
					</tr>
					<tr>
						<td>E-mail Address</td>
						<td>${employee.detail.email }</td>
					</tr>

					<tr>
						<td>Joining Date</td>
						<td>${employee.detail.joinDate }</td>
					</tr>
					<tr>
						<td>Salary Type</td>
						<td>${employee.detail.salaryType }</td>
					</tr>

				</tbody>
			</table>
		</div>


		<div class="col-md-9">
			<c:if test="${! empty employee.leaveDetails }">
				<table class="table table-striped no-margn line">
					<tbody>
						<tr>
							<td class="hd_tb" colspan="5" style="background-color:#0380B0;color: #fff;"><span class="WhiteHeading"><strong>Leaves</strong>
							</span></td>

						</tr>
						<tr>
							<td>S#</td>
							<td>Leave Type</td>
							<td>Reason</td>
							<td>Leave From</td>
							<td>Leave To</td>
						</tr>
						<c:forEach items="${employee.leaveDetails }" var="leaves"
							varStatus="leaveSts">
							<tr>
								<td>${leaves.slno }</td>
								<td>${leaves.leaveType }</td>
								<td>${leaves.reason }</td>
								<td>${leaves.fromDateString }</td>
								<td>${leaves.toDateString }</td>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</c:if>
			<br /> <br />

			<c:if test="${! empty employee.transferDetails }">
				<table class="table table-striped no-margn line">
					<tbody>
						<tr>
							<td class="hd_tb" colspan="6"><span class="WhiteHeading"><strong>Transfers</strong>
							</span></td>

						</tr>
						<tr>
							<td>S#</td>
							<td>Transfer Date</td>
							<td>Transfer From(Branch)</td>
							<td>Transfer From(Department)</td>
							<td>Transfer To(Branch)</td>
							<td>Transfer To(Department)</td>



						</tr>
						<c:forEach items="${employee.transferDetails }" var="transfer"
							varStatus="transferStat">
							<tr>
								<td>${transfer.slno }</td>
								<td>${transfer.transferDateString }</td>
								<td>${transfer.fromBranch }</td>
								<td>${transfer.fromDep }</td>
								<td>${transfer.toBranch }</td>
								<td>${transfer.toDep }</td>

							</tr>
						</c:forEach>

					</tbody>
				</table>
			</c:if>
		</div>
	</div>

	<div class="form-group" style="margin-top: 25px;">
		<div class="col-sm-offset-4 col-sm-6">
			<button class="btn btn-info"
				onclick="window.open('data:application/vnd.ms-excel,' + document.getElementById('table12').outerHTML.replace(/ /g, '%20'));">
				Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
			</button>

		</div>
	</div>

	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>
</html>