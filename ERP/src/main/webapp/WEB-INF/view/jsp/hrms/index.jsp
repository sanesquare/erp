<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
</head>
<body>
	<div class="employee_back" id="popp" style="display: none;">
		<div class="employee_view" id="one">
			<div class="col-md-12">
				<div class="form-group">
					<div class="simplePopupClose"
						onclick="$('#popp').css('display','none')">X</div>
					<h1 class="pop_hd" id="sup_name"></h1>
					<div class="row">
						<div class="col-md-3">
							<img class="pop_img" src="" id="sup_img">
						</div>
						<div class="col-md-9">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputPassword3">E-mail
								</label>
								<div class="col-sm-9">
									<label class="col-sm-6 control-label ft" for="inputPassword3"
										id="sup_mail"></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputPassword3">Designation
								</label>
								<div class="col-sm-9">
									<label class="col-sm-6 control-label ft" for="inputPassword3"
										id="sup_des"></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputPassword3">Department
								</label>
								<div class="col-sm-9">
									<label class="col-sm-6 control-label ft" for="inputPassword3"
										id="sup_dep"></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputPassword3">Branch
								</label>
								<div class="col-sm-9">
									<label class="col-sm-6 control-label ft" for="inputPassword3"
										id="sup_branch"></label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>







		<div class="warper container-fluid"
			style="min-height: 682px !important;">
			<div class="noti" id="notifications"></div>

			<div class="page-header">
				<div class="hdr">
					<div class="home-div">
						<a href="erpHome.do"> <span class="home"><i
								class="fa fa-home"></i></span>
						</a>
					</div>
					<div class="hdr-div">
						<span class="hdr-ico"><i class="fa fa-users"></i></span> HRM <small>Let's
							get a quick overview...</small>
					</div>

				</div>
			</div>
			<c:if test="${not empty announcements}">
				<div class="row">
					<div class="col-md-12">
						<div class="breakingNews bn-large" id="bn10">
							<div class="bn-title">
								<h2>Announcements</h2>
								<span></span>
							</div>
							<ul id="announ_ses">
								<marquee class="mrq" onmouseover="stop();" onmouseout="start();">
									<c:forEach items="${announcements}" var="announcement">
										<i class="fa fa-asterisk"
											style="font-size: 10px; color: #15AFEA;"></i>&nbsp;&nbsp;${announcement.announcementsTitle}&nbsp;&nbsp;
								</c:forEach>
								</marquee>
							</ul>
							<!-- <div class="bn-navi">
						<span></span> <span></span>
					</div> -->
						</div>
					</div>
				</div>
				<%-- <div class="row">
				<div class="col-md-12">
					<div class="alerts" id="alert_view">
						<div class="icon_alert">
							<i class="fa fa-bullhorn"></i>
						</div>
						<div class="alert_text">
							<marquee class="mrq">
								<c:forEach items="${announcements}" var="announcement">
									&nbsp;<i class="fa fa-bullhorn"></i>&nbsp;${announcement.announcementsTitle}
							</c:forEach>
							</marquee>
						</div>
						<div class="close_icon" id="close_alert">
							<i class="fa fa-times"></i>
						</div>
					</div>

				</div>
			</div> --%>
			</c:if>
			<div class="row">
			<c:choose>
				<c:when test="${ rolesMap.roles['Salary Calculator'].view}">
					<div class="col-md-6 col-lg-3">
						<div class="panel panel-default clearfix dashboard-stats rounded">
							<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
								href="salaryCalculator.do"> <span class="hover_effect">Salary<br>
										Calculator
								</span></a> </span> <i class="fa fa-calculator bg-danger transit stats-icon"></i>
							<a href="salaryCalculator.do">
								<h3 class="transit">
									Salary<br> Calculator<small class="text-green"></small>
								</h3>
							</a>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="col-md-6 col-lg-3">
						<div class="panel panel-default clearfix dashboard-stats rounded">
							<span id="dashboard-stats-sparkline1" class="sparkline transit">
								<span class="hover_effect">Salary<br> Calculator
							</span>
							</span> <i class="fa fa-calculator bg-danger transit stats-icon"></i>
							<h3 class="transit">
								Salary<br> Calculator<small class="text-green"></small>
							</h3>
						</div>
					</div>
				</c:otherwise>
			</c:choose>

			<div class="col-md-6 col-lg-3">
				<div class="panel panel-default clearfix dashboard-stats rounded">
					<span id="dashboard-stats-sparkline1" class="sparkline transit">
						<a href="userActivities.do"><span class="hover_effect">User<br>
								Activities
						</span></a>
					</span> <i class="fa fa-user bg-danger transit stats-icon"></i> <a
						href="userActivities.do">
						<h3 class="transit">
							User<br> Activities<small class="text-green"></small>
						</h3>
					</a>
				</div>
			</div>
			<c:choose>
				<c:when test="${ rolesMap.roles['Job Posts'].view}">
					<div class="col-md-6 col-lg-3">
						<div class="panel panel-default clearfix dashboard-stats rounded">
							<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
								href="jobPosts.do"> <span class="hover_effect">Job Posts<br></span></a>
							</span> <i class="fa fa-suitcase bg-danger transit stats-icon"></i> <a
								href="jobPosts.do">
								<h3 class="transit">
									Job <br>Posts <small class="text-green"></small>
								</h3>
							</a>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="col-md-6 col-lg-3">
						<div class="panel panel-default clearfix dashboard-stats rounded">
							<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
								href="jobPosts"> <span class="hover_effect">Job Posts<br></span></a>
							</span> <i class="fa fa-suitcase bg-danger transit stats-icon"></i>
							<h3 class="transit">
								Job <br>Posts <small class="text-green"></small>
							</h3>
						</div>
					</div>
				</c:otherwise>
			</c:choose>


			<c:choose>
				<c:when test="${ rolesMap.roles['Employee Joining'].view}">
					<div class="col-md-6 col-lg-3">
						<div class="panel panel-default clearfix dashboard-stats rounded">
							<span id="dashboard-stats-sparkline1" class="sparkline transit">
								<a href="employeeJoining.do"><span class="hover_effect">Employee
										<br>Joining
								</span></a>
							</span> <i class="fa fa-trophy bg-danger transit stats-icon"></i> <a
								href="employeeJoining.do">
								<h3 class="transit">
									Employee<br> Joining<small class="text-green"></small>
								</h3>
							</a>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="col-md-6 col-lg-3">
						<div class="panel panel-default clearfix dashboard-stats rounded">
							<span id="dashboard-stats-sparkline1" class="sparkline transit">
								<a href="employeeJoining"><span class="hover_effect">Employee
										<br>Joining
								</span></a>
							</span> <i class="fa fa-trophy bg-danger transit stats-icon"></i>
							<h3 class="transit">
								Employee<br> Joining<small class="text-green"></small>
							</h3>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">My information</div>
						<div class="panel-body">
						<c:if test="${! empty authEmployeeDetails.profilePic }">
							<div class="form-group">
								<div class="photo_show">
									<img src='<c:url value="${authEmployeeDetails.profilePic }" />' />
								</div>
							</div></c:if>
							<div class="form-group">
								<label class="col-sm-4 control-label">Name</label>
								<div class="col-sm-8">
									<span>${authEmployeeDetails.name}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Code</label>
								<div class="col-sm-8">
									<span>${authEmployeeDetails.employeeCode}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">User name</label>
								<div class="col-sm-8">
									<span>${authEmployeeDetails.userName}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Branch</label>
								<div class="col-sm-8">
									<span>${authEmployeeDetails.employeeBranch}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Department</label>
								<div class="col-sm-8">
									<span>${authEmployeeDetails.employeeDepartment}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Designation</label>
								<div class="col-sm-8">
									<span>${authEmployeeDetails.designation}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Join Date</label>
								<div class="col-sm-8">
									<span>${authEmployeeDetails.joiningDate}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Shift</label>
								<div class="col-sm-8">
									<span>${authEmployeeDetails.workShift}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Alerts</div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-3 control-label"><i
									class="fa fa-envelope-o"></i></label>
								<div class="col-sm-9">
									<span>0 unread Inbox Messages</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label"><i
									class="fa fa-birthday-cake"></i></label>
								<div class="col-sm-9">
									<span>No Birthday Today</span>
								</div>
							</div>
						</div>
					</div>
				</div> -->
				<!-- <div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">With Markers</div>
						<div class="panel-body">
							<div id="demo-map-2" style="height: 250px;"></div>
						</div>
					</div>
				</div> -->
			</div>
			<div class="col-md-4">
				<div class="col-lg-12">

					<div class="panel panel-default" data-ng-controller="TodoCtrl">
						<div class="panel-heading ">Reporting</div>
						<div class="panel-body">

							<div class="form-group">
								<label class="col-sm-6 control-label">Reporting To</label>
								<div class="col-sm-6">
									<span><c:forEach
											items="${authEmployeeDetails.superiorSubordinateVo.superiors}"
											var="superiors">
											<a onclick="showPop(' ${superiors.superiorId }')">
												${superiors.superiorName } ( ${superiors.superiorCode } ) </a>,<br />
										</c:forEach></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-6 control-label">Reporting To Me</label>
								<div class="col-sm-6">
									<span><c:forEach
											items="${authEmployeeDetails.superiorSubordinateVo.subordinates}"
											var="subordinates">
									${subordinates.subordinateName } ( ${subordinates.subordinateCode } ) ,
									</c:forEach></span>
								</div>
							</div>
						</div>
					</div>
				</div>








				<%-- <div class="col-lg-12">
					<div class="panel panel-default" data-ng-controller="TodoCtrl">
						<div class="panel-heading">Things to do</div>
						<div class="panel-body">
							<ul class="list-group no-margn nicescroll todo-list"
								style="height: 205px; overflow: hidden; outline: none;"
								tabindex="5000">
								<li data-ng-repeat="todo in todos" class="list-group-item">
									<label class="cr-styled"> <input type="checkbox"
										data-ng-model="todo.done"> <i class="fa"></i>
								</label> <a href="#"> <span class="done-{{todo.done}} ssm">gfhjghj
											dsfdsfds ds fdsfds dsfdsf dsfdsf dsfdsfds dsfdsfdsf</span></a> <i
									class="fa fa-close flr"></i>
								</li>

								<li data-ng-repeat="todo in todos" class="list-group-item">
									<label class="cr-styled"> <input type="checkbox"
										data-ng-model="todo.done"> <i class="fa"></i>
								</label> <a href="#"> <span class="done-{{todo.done}} ssm">gfhjghj
											dsfdsfds ds fdsfds dsfdsf dsfdsf dsfdsfds dsfdsfdsf</span></a> <i
									class="fa fa-close flr"></i>
								</li>
								<li data-ng-repeat="todo in todos" class="list-group-item">
									<label class="cr-styled"> <input type="checkbox"
										data-ng-model="todo.done"> <i class="fa"></i>
								</label> <a href="#"> <span class="done-{{todo.done}} ssm">gfhjghj
											dsfdsfds ds fdsfds dsfdsf dsfdsf dsfdsfds dsfdsfdsf</span></a> <i
									class="fa fa-close flr"></i>
								</li>
								<li data-ng-repeat="todo in todos" class="list-group-item">
									<label class="cr-styled"> <input type="checkbox"
										data-ng-model="todo.done"> <i class="fa"></i>
								</label> <a href="#"> <span class="done-{{todo.done}} ssm">gfhjghj
											dsfdsfds ds fdsfds dsfdsf dsfdsf dsfdsfds dsfdsfdsf</span></a> <i
									class="fa fa-close flr"></i>
								</li>
							</ul>
						</div>
						<div class="panel-footer">
							<form data-ng-submit="addTodo()" role="form">
								<div class="input-group">
									<input type="text" data-ng-model="todoText"
										class="form-control" placeholder="add new todo here">
									<span class="input-group-btn">
										<button class="btn-primary btn" type="submit">Add</button>
									</span>
								</div>
							</form>
						</div>
					</div>
				</div> --%>
				<!-- <div class="col-lg-12">
					<div class="panel panel-default" data-ng-controller="TodoCtrl">
						<div class="panel-heading ">Reporting</div>
						<div class="panel-body">
							<div>
								<div id="rCalendar" class="regalcalendar"
									style="width: 350px; float: left">
									<span class="event" data-title="Event 21"
										data-location="Ecuador" data-date="21/02/2015"
										data-time="16:00" data-icon="Briefcase">Excepteur sint
										occaecat cupidatat non proident, sunt in culpa qui Lorem ipsum
										dolor sit amet, nostrud exercitation ullamco laboris nisi ut
										aliqu se cillum dolore eu fugiat nulla pariatur</span>

								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
			<div class="col-md-4">
				<!-- <div class="col-lg-12">
					<div class="panel panel-default" data-ng-controller="TodoCtrl">
						<div class="panel-heading">Today's Task</div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-12 control-label"><i
									class="fa fa-tasks"></i>&nbsp; <a href="#"> No Task for
										today</a></label>
							</div>
							<div class="form-group">
								<button type="button" id="connect" class="btn btn-primary flr_t">Connect</button>
								<button type="button" id="websocketTest"
									class="btn btn-primary flr_t">My Organizer</button>
							</div>
						</div>
					</div>
				</div> -->
				<div class="col-lg-12">
					<div class="panel panel-default" data-ng-controller="TodoCtrl">
						<div class="panel-heading">Leave Summary</div>
						<div class="panel-body">
							<table class="table table-striped no-margn">
								<thead>
									<tr>
										<th width="35%">Leaves Type</th>
										<th width="25%">Entitled</th>
										<th width="25%">Taken</th>
										<th width="25%">Balance</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${authEmployeeDetails.leaveVos }" var="l">
										<tr>
											<td>${l.leaveTitle }</td>
											<td>${l.leaves}</td>
											<td>${l.leavesTaken}</td>
											<td>${l.leavesBalance}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<!-- <div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Statistics</div>
						<div class="panel-body">
							<div id="pie-chart" style="height: 250px;"></div>
						</div>
					</div>
				</div> -->

			</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<%-- <script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script> --%>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<%-- <script src="<c:url value='/resources/js/sockjs-0.3.4.js'/>"></script>
	<script src="<c:url value='/resources/js/stomp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/pushnotification.js' />"></script> --%>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		/* $(document).ready(function() {
		$("#alert_view").hide();
		var stompClient = null;
		connect(handleNotification);
		});

		var stompClient = null;

		$("#connect").click(function() {
		connect(handleNotification);
		}); */

		$("#websocketTest").click(function() {
			$.ajax({
				type : "GET",
				url : 'testwebsocket.do',
				cache : false,
				success : function(response) {
				}
			});
		});
		function handleNotification(messsage) {
			if ($.inArray("ALL", messsage['recipients']) > -1) {
				$(".mrq").append(
						"<i class='fa fa-bullhorn'></i>&nbsp;&nbsp;"
								+ messsage['content'] + "&nbsp;&nbsp;");
				showAnnouncement("Announcement", messsage['content']);
			} else if ($.inArray("ONLINE", messsage['recipients']) > -1) {
				showAnnouncement("Online", messsage['content']);
			} else {
				if ($.inArray($("#authEmpCode").val(), messsage['recipients']) > -1) {
					$("#notification" + $("#authEmpCode").val()).show();
					if ($("#notification" + $("#authEmpCode").val()).text() != "") {
						var notifCount = $(
								"#notification" + $("#authEmpCode").val())
								.text();
						$("#notification" + $("#authEmpCode").val()).html(
								(parseInt(notifCount) + 1));
					} else {
						$("#notification" + $("#authEmpCode").val()).html("1");
					}
					$("#notification_list" + $("#authEmpCode").val())
							.append(
									"<a href='"+messsage['url']+"' class='list-group-item'><div class='pro_pic'><img src='"+messsage['picture']+"'/></div>"
											+ messsage['content'] + "</a>");
				}
			}
		}

		$('#close_alert').click(function() {
			$('#alert_view').fadeOut(300);
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#bn10").breakingNews({
				effect : "slide-h",
				autoplay : true,
				timer : 3000,
				color : "pink"
			});

			$("#dashbordli").addClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");

			getNotifications();
		});

		function deleteNotification(id) {
			deleteNotificationFromTable(id);
			$("#" + id).remove();
		}

		function getNotifications() {
			$
					.ajax({
						type : "GET",
						url : "getNotifications.do",
						cache : false,
						async : true,
						success : function(response) {
							var data = response.result;
							var notification = "";
							$
									.each(
											data,
											function(i, item) {
												notification += "<div class='noti_one' id="+item.id+">"
														+ "<div class='noti_hd'>"
														+ " <h2>Notification</h2><span onclick='deleteNotification("
														+ item.id
														+ ")'>X</span>"
														+ "</div>"
														+ "<p class='noti_cont'>"
														+ item.subject
														+ "</p>"
														+ "</div>";
											});
							$("#notifications").html(notification);
						},
						error : function(requestObject, error, errorThrown) {
							alert(errorThrown);
						}
					});
		}

		function deleteNotificationFromTable(id) {
			$.ajax({
				type : "GET",
				url : "deleteNotification.do",
				cache : false,
				async : true,
				data : {
					"id" : id
				},
				success : function(response) {
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
		}

		function showPop(code) {
			$.ajax({
				type : "POST",
				url : "getEmployeeDetailsForPopupById.do",
				cache : false,
				async : true,
				data : {
					"employeeCode" : code
				},
				success : function(response) {
					var data = response.result;

					$("#sup_name").html(data.name);
					$("#sup_img").attr("src", data.profilePic);
					$("#sup_des").text(data.designation);
					$("#sup_dep").text(data.employeeDepartment);
					$("#sup_branch").text(data.employeeBranch);
					$("#sup_mail").text(data.email);
					$("#popp").css("display", "block");
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				}
			});
		}
	</script>
	<!-- Warper Ends Here (working area) -->
</body>
</html>