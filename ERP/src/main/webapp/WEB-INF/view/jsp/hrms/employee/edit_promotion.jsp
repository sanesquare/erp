<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body data-ng-app>

	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Promotions</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg" >
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<form:form action="updatePromotion.do" method="POST" commandName="promotionVo" id="promotion_details">
						<form:hidden path="id" value="${promotionVo.id}"/>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Promotion Information</div>
											<div class="panel-body">

												 <div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Employee
													</label>
													
													<div class="col-sm-9">
														<form:select cssClass="form-control chosen-select"
																multiple="false" path="employeeId" id="add_promotion_employee" name="employeeId">
																<form:options items="${employee}" itemValue="employeeId"
																	itemLabel="employeeCode" />
														</form:select>
														
													</div>


												</div>



												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Promotion
														To</label>
													<div class="col-sm-8 col-xs-10">
															<form:select cssClass="form-control chosen-select"
																multiple="false" path="designationId" name="designationId">
																<form:options items="${designations}" itemValue="id"
																	itemLabel="designation" />
															</form:select>													
														
													</div>
													<div class="col-sm-1 col-xs-1">
														<a href="#" class="show2"><i class="fa fa-user"></i></a>
													</div>
												</div>
											<div id="pop2" class="simplePopup">
													<div class="col-md-12">
														<div class="form-group">
															<h1 class="pop_hd">Azad</h1>
															<div class="row">
																<div class="col-md-3">
																	<img src="assets/images/avtar/user.png" class="pop_img">
																</div>
																<div class="col-md-9">
																	<div class="form-group">
																		<label for="inputPassword3"
																			class="col-sm-3 control-label">E-mail </label>
																		<div class="col-sm-9">
																			<label for="inputPassword3"
																				class="col-sm-6 control-label ft">jsafs@.com</label>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputPassword3"
																			class="col-sm-3 control-label">Designation </label>
																		<div class="col-sm-9">
																			<label for="inputPassword3"
																				class="col-sm-6 control-label ft">2545895678</label>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputPassword3"
																			class="col-sm-3 control-label">Department </label>
																		<div class="col-sm-9">
																			<label for="inputPassword3"
																				class="col-sm-6 control-label ft">dsafdsfdsf</label>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputPassword3"
																			class="col-sm-3 control-label">Station </label>
																		<div class="col-sm-9">
																			<label for="inputPassword3"
																				class="col-sm-6 control-label ft">2sdfds8</label>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputPassword3"
																			class="col-sm-3 control-label">Join Date </label>
																		<div class="col-sm-9">
																			<label for="inputPassword3"
																				class="col-sm-6 control-label ft">25-2-2015</label>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Forward
														Application To</label>
													<div class="col-sm-8 col-xs-10  emp-superior-list-div">
														<form:select cssClass="form-control chosen-select"
																multiple="false" path="superiorCode" name="superiorCode" id="add_superiors">
																<form:options items="${superiors}" itemValue="value"
																	itemLabel="value" />
														</form:select>
														
													</div>
														<div class="col-sm-1 col-xs-1">
														<a href="#" class="show1"><i class="fa fa-user"></i></a>
													</div>
												</div>
												<div id="pop1" class="simplePopup">
													<div class="col-md-12">
														<div class="form-group">
															<h1 class="pop_hd">azac</h1>
															<div class="row">
																<div class="col-md-3">
																	<img src="assets/images/avtar/user.png" class="pop_img">
																</div>
																<div class="col-md-9">
																	<div class="form-group">
																		<label for="inputPassword3"
																			class="col-sm-3 control-label">E-mail </label>
																		<div class="col-sm-9">
																			<label for="inputPassword3"
																				class="col-sm-6 control-label ft">jsafs@.com</label>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputPassword3"
																			class="col-sm-3 control-label">Designation </label>
																		<div class="col-sm-9">
																			<label for="inputPassword3"
																				class="col-sm-6 control-label ft">2545895678</label>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputPassword3"
																			class="col-sm-3 control-label">Department </label>
																		<div class="col-sm-9">
																			<label for="inputPassword3"
																				class="col-sm-6 control-label ft">dsafdsfdsf</label>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputPassword3"
																			class="col-sm-3 control-label">Station </label>
																		<div class="col-sm-9">
																			<label for="inputPassword3"
																				class="col-sm-6 control-label ft">2sdfds8</label>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputPassword3"
																			class="col-sm-3 control-label">Join Date </label>
																		<div class="col-sm-9">
																			<label for="inputPassword3"
																				class="col-sm-6 control-label ft">25-2-2015</label>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Promotion
														Date</label>
													<div class="col-sm-9">
														<div class="input-group date" id="datepicker">
															<form:input cssClass="form-control" path="promotionDate" name="promotionDate"
																data-date-format="DD/MM/YYYY"/> <span
																class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-info">Next</button>
													</div>
												</div> 
											</div>
										</div>
									</div>




									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Promotion Description</div>
											<div class="panel-body">
												<div class="form-group">
													<div class="col-md-12">
														
														<form:textarea path="promotionDescription"
																cssClass="form-control height "
																id="add_promotion_description" value="${promotionVo.promotionDescription}"></form:textarea>
															
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-16 col-sm-12">
														<button type="submit" class="btn btn-info">Next</button>
													</div>
												</div>
											</div>
										</div>
									</div>


								</div>
								<div class="col-md-6">






									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<form:textarea class="form-control height" path="promotionNote" value="${promotionVo.promotionNote}"></form:textarea>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">System
															Administrator </label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">25-2-2015
														</label>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-info">Save</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</form:form>
					</div>


					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="users">
						<div class="panel-body" id="docs">
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Upload Documents</div>
									<div class="panel-body">

										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Upload
												Documents </label>
											<div class="col-sm-9">
												<input type="file" class="btn btn-info"
													id="promotion_file_upload" name="promotion_file_upload" />
											</div>
										</div>


									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">


										<c:if test="${!empty promotionVo.promotionDocuments}">
											<table class="table no-margn">
												<thead>
													<tr>
														<th>#</th>
														<th>Document Name</th>
														<th>view</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${promotionVo.promotionDocuments }"
														var="doc">
														<tr>
															<td>${doc.id }</td>
															<td><i class="fa fa-file-o"></i>${doc.name }</td>
															<td><a
																href="viewPromotionDocument.do?durl=${doc.url }"><i
																	class="fa fa-file-text-o sm"></i></a></td>
															<td><a
																href="deletePromotionDocument.do?durl=${doc.url }"><i
																	class="fa fa-close ss"></i></a></td>
														</tr>
													</c:forEach>

												</tbody>
											</table>
										</c:if>

									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<!-- <a href="#docs" data-toggle="tab" class="btn btn-info">Next</a> -->
									-
									<button type="submit" value="uploadDocuments"
										class="btn btn-info" id="sbmt_department_doc_btn"
										name="uploadDocuments">Upload Documents</button>
									<!-- <a class="btn btn-info" id="sbmt_proj_doc_btn">Upload
											Documents</a> -->
								</div>
							</div>
							
						</div>

					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Simple Map</div>
									<div class="panel-body">
										<div id="simpleMap" style="height: 500px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	</body>
	<!-- Warper Ends Here (working area) -->
	<!-- Content Block Ends Here (right box)-->

	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script
		src="<c:url value='/resources/js/progress/jquery.uploadfile.min.js' />"></script>
	<script src="<c:url value='/resources/js/progress/file-upload.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$('.show3').click(function() {
				$('#pop3').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</html>
