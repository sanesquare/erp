<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
<title>Snogol HRMS</title>
</head>
<body style="background: #FFFFFF !important;">
	<div class="col-xs-12" id="table1">

		<h2 class="role_hd" align="center">Salary Report</h2>
		<table class="table table-bordered">
			<tbody>

				<tr>
					<td class="hd_tb split"
						style="background-color: #0380B0; color: #fff;"><span
						class="WhiteHeading">S#</span></td>
					<td class="hd_tb split"
						style="background-color: #0380B0; color: #fff;"><span
						class="WhiteHeading"
						style="background-color: #0380B0; color: #fff;">Employee
							Name </span></td>
					<td class="hd_tb split"
						style="background-color: #0380B0; color: #fff;"><span
						class="WhiteHeading"
						style="background-color: #0380B0; color: #fff;">Tax Amount</span></td>
					<c:forEach var="column" items="${salaryDetails.colHeads }"
						varStatus="sts">
						<td class="hd_tb split"
							style="background-color: #0380B0; color: #fff;"><span
							class="WhiteHeading">${column.key } </span></td>
					</c:forEach>

					<td class="hd_tb split"
						style="background-color: #0380B0; color: #fff;"><span
						class="WhiteHeading">Total Salary </span></td>
				</tr>
				<tr></tr>
				<c:if test="${! empty salaryDetails.details }">
					<c:forEach items="${salaryDetails.details }" var="details"
						varStatus="status">
						<tr>
							<td>${details.slno }</td>
							<td>${details.empName }</td>
							<%-- <td>${details.basicSalary }</td> --%>
							<td>${details.taxAmount }</td>
							<%-- <c:forEach var="column" items="${salaryDetails.colHeads }"
								varStatus="sts">
								<c:forEach items="${details.allowances }" var="allowance"
									varStatus="status1">
									<c:choose>
										<c:when test="${allowance.key == column}">
											<td>${allowance.value }</td>
										</c:when>
									</c:choose>
								</c:forEach>
							</c:forEach> --%>
							<c:forEach items="${salaryDetails.colHeads }"
							var="column" varStatus="colStatus1">
							<c:set var="isData" value="false" />
							<c:forEach items="${details.allowances }" var="offered"
								varStatus="offeredSts">
								<c:set var="keys" value="${offered.key}" />

								<c:if test="${(column.key == keys) }">
									<td>${offered.value }</td>
									<c:set var="isData" value="true" />
								</c:if>
							</c:forEach>
							<c:if test="${isData == 'false'}">
								<td>0.00</td>
							</c:if>
						</c:forEach>
							<td>${details.totalSalary }</td>
						</tr>
					</c:forEach>
					<tr>
						<td colspan="2"><strong>Total</strong></td>
						<td><strong>${salaryDetails.totalTaxAmount }</strong></td>
						<%-- <c:forEach var="column" items="${salaryDetails.details }"
							varStatus="sts">
							<c:forEach items="${column.allowances }" var="labl">
								<c:choose>
									<c:when test="${sts.index < 1 }">
										<td>${labl.value }</td>
										<td></td>
									</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>
							</c:forEach>
						</c:forEach> --%>
						<c:forEach items="${salaryDetails.colHeads }"
							var="column" varStatus="colStatus1">
							<c:set var="isData" value="false" />
							<c:forEach items="${salaryDetails.totalAmountMap }" var="offered"
								varStatus="offeredSts">
								<c:set var="keys" value="${offered.key}" />

								<c:if test="${(column.key == keys) }">
									<td>${offered.value }</td>
									<c:set var="isData" value="true" />
								</c:if>
							</c:forEach>
							<c:if test="${isData == 'false'}">
								<td>0.00</td>
							</c:if>
						</c:forEach>
						<td><strong>${salaryDetails.grandSalary }</strong></td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>
	<c:if test="${! empty salaryDetails.details }">
		<div class="form-group" style="margin-top: 25px;">
			<div class="col-sm-offset-4 col-sm-6">
				<button class="btn btn-info"
					onclick="window.open('data:application/vnd.ms-excel,' + document.getElementById('table1').outerHTML.replace(/ /g, '%20'));">
					Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
				</button>
			</div>
		</div>
	</c:if>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>
</html>