<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body data-ng-app>

		<div class="warper container-fluid">
			<div class="page-header">
				<h1>Complaints</h1>
			</div>
			<div class="row">
			<form:form action="updateComplaints.do" method="POST"	commandName="complaintsVo" id="complaint_details">
				<form:hidden path="id" value="${complaintsVo.id}" />
				<div class="col-md-12">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#all"
							role="tab" data-toggle="tab">Information</a></li>
						<li role="presentation"><a href="#users" role="tab"
							data-toggle="tab">Documents</a></li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel"
							class="panel panel-default tab-pane tabs-up active" id="all">
								
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Complaint Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Complaint
															From </label>
														<div class="col-sm-8 col-xs-10">

															<form:select cssClass="form-control chosen-select"
																multiple="false" path="complaintFromEmpId" name="complaintFromEmpId">
																<form:options items="${employee}" itemValue="employeeId"
																	itemLabel="employeeCode" />
															</form:select>
														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i
																class="fa fa-plus-square-o pop_icon"></i></a>
														</div>
													</div>
													<div id="pop2" class="simplePopup">
														<div class="col-md-12">
															<div class="form-group">
																<h1 class="pop_hd">Azad</h1>
																<div class="row">
																	<div class="col-md-3">
																		<img src="assets/images/avtar/user.png"
																			class="pop_img">
																	</div>
																	<div class="col-md-9">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">E-mail </label>
																			<div class="col-sm-9">
																				<label for="inputPassword3"
																					class="col-sm-6 control-label ft">jsafs@.com</label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Designation </label>
																			<div class="col-sm-9">
																				<label for="inputPassword3"
																					class="col-sm-6 control-label ft">2545895678</label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Department </label>
																			<div class="col-sm-9">
																				<label for="inputPassword3"
																					class="col-sm-6 control-label ft">dsafdsfdsf</label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Station </label>
																			<div class="col-sm-9">
																				<label for="inputPassword3"
																					class="col-sm-6 control-label ft">2sdfds8</label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Join Date </label>
																			<div class="col-sm-9">
																				<label for="inputPassword3"
																					class="col-sm-6 control-label ft">25-2-2015</label>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">
															Forward Application To</label>
														<div class="col-sm-9">

															<form:select cssClass="form-control chosen-select"
																multiple="false" path="forwardAppToEmpId" name="forwardAppToEmpId">
																<form:options items="${employee}" itemValue="employeeId"
																	itemLabel="employeeCode" />
															</form:select>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Complaint
															Date</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepicker">
																<form:input cssClass="form-control" path="complaintDate"
																	name="complaintDate" data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>


														</div>
													</div>






													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">Next</button>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Complaint Against</div>
												<div class="panel-body">


													<div class="form-group">
														<label class="col-sm-3 control-label">Complaint
															Against</label>
														<div class="col-sm-9 ">


															<form:select cssClass="form-control chosen-select"
																multiple="false" path="complaintAganistEmpId" name="complaintAganistEmpId">
																<form:options items="${employee}"  itemValue="employeeId"
																	itemLabel="employeeCode" />
															</form:select>
														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>
												</div>
											</div>
										</div>









										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Complaint Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea path="complaintDescription"
																cssClass="form-control height "
																id="add_complaints_description" />
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<button type="submit" class="btn btn-info">Next</button>
														</div>
													</div>
												</div>
											</div>
										</div>


									</div>
									<div class="col-md-6">



										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Status</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Status
														</label>
														<div class="col-sm-9">
															<form:select cssClass="form-control chosen-select"
																multiple="false" path="complaintStatusId" id="add_complaints_status">
																<form:options items="${complaintStatus}"  itemValue="id"
																	itemLabel="statusDescription" />
															</form:select>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Description
														</label>
														<div class="col-sm-9">
															<form:textarea path="statusDescription"
																cssClass="form-control height "
																id="add_status_description" />
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>
												</div>
											</div>
										</div>


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea cssClass="form-control height" path="note" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">System
																Administrator </label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">25-2-2015 </label>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>


						<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
							id="users">
							<div class="panel-body">
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Upload Documents</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Upload
													Documents </label>
												<div class="col-sm-9">
													<input type="file" class="btn btn-info" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">
											<table class="table no-margn">
												<thead>
													<tr>
														<th>#</th>
														<th>Document Name</th>
														<th>view</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td><i class="fa fa-file-o"></i> Lorem ipsum dolor
															sit amet</td>
														<td><i class="fa fa-file-text-o sm"></i></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
							id="docs">
							<div class="panel-body">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Simple Map</div>
										<div class="panel-body">
											<div id="simpleMap" style="height: 500px;"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</form:form>
			</div>
			
		</div>
		
	</body>

	<!-- Warper Ends Here (working area) -->
	<!-- Content Block Ends Here (right box)-->

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script
		src="<c:url value='/resources/js/progress/jquery.uploadfile.min.js' />"></script>
	<script src="<c:url value='/resources/js/progress/file-upload.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script type="text/javascript">
		function confirmDelete(id) {
			var result = confirm("Confirm delete?");
			if (result) {
				$(".delete_promotion")
						.attr("href", "deletePromotion?pid=" + id);
			} else {
				$(".delete_promotion").attr("href", "#");
			}
		}
		$(document).ready(function(){
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</html>
