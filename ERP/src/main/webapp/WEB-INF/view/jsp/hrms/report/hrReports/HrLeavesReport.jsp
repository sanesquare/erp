<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />

</head>
<body style="background: #FFFFFF !important;">
	<h2 class="role_hd">Leaves Report</h2>
	<div class="col-xs-12">
		<form:form action="exportHrLeave.do" method="POST"
			modelAttribute="leaves" target="_blank">
			<table class="table table-striped no-margn line">
				<tbody>
					<tr>

						<td class="hd_tb"><span class="WhiteHeading">S#</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Leave
								Start Date </span></td>
						<td class="hd_tb"><span class="WhiteHeading">Leave End
								Date </span></td>
						<td class="hd_tb"><span class="WhiteHeading"> Employee
								Name</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Designation</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Department</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Branch</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Leave
								Type</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Reason</span></td>

					</tr>

					<c:forEach items="${leaves.detailsVo}" var="details"
						varStatus="status">
						<c:choose>
							<c:when test="${! empty details.month }">
								<tr>
									<td colspan="5"><strong>${details.month }</strong>
									<form:hidden path="detailsVo[${status.index }].month" /></td>

								</tr>

							</c:when>
							<c:otherwise>
								<tr>
									<td>${details.slno }<form:hidden
											path="detailsVo[${status.index}].slno" /></td>
									<td>${details.startDateString }<form:hidden
											path="detailsVo[${status.index}].leaveDate" /></td>
									<td>${details.endDateString }<form:hidden
											path="detailsVo[${status.index}].leaveEndDate" /></td>
									<td>${details.empName }<form:hidden
											path="detailsVo[${status.index}].empName" /></td>
									<td>${details.designation }<form:hidden
											path="detailsVo[${status.index}].designation" /></td>
									<td>${details.department }<form:hidden
											path="detailsVo[${status.index}].department" /></td>
									<td>${details.branch }<form:hidden
											path="detailsVo[${status.index}].branch" /></td>
									<td>${details.leaveType }<form:hidden
											path="detailsVo[${status.index}].leaveType" /></td>
									<td>${details.reason }<form:hidden
											path="detailsVo[${status.index}].reason" /></td>
								</tr>
							</c:otherwise>
						</c:choose>
					</c:forEach>

				</tbody>
			</table>

			<div class="form-group" style="margin-top: 25px;">
				<div class="col-sm-offset-4 col-sm-6">
					<button type="submit" class="btn btn-info" name="type" value="xls">
						Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
					</button>
					<button type="submit" class="btn btn-info" name="type" value="csv">
						Export to CSV &nbsp; <i class="fa fa-file-text-o"></i>
					</button>
					<button type="submit" class="btn btn-info" name="type" value="pdf">
						Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
					</button>
				</div>
			</div>
		</form:form>
	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>

	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js'/>"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>
	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<!-- Responsive tabs -->
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"
		type="text/javascript"></script>

	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</html>