<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />


</head>
<body>
<body style="background: #FFFFFF !important;">
	<h2 class="role_hd">Daily Wages</h2>
	<div class="col-xs-12">

		<form:form action="exportPayDaily.do" method="POST"
		modelAttribute="dailyWageDetails">
			<table class="table table-striped no-margn line">
				<tbody>
					<tr>

						<td class="hd_tb"><span class="WhiteHeading">S#</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Date (Up
								To)</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Employee
						</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Total
								Days</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Daily
								Rate</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Amount</span></td>
					</tr>
					<c:choose>
						<c:when test="${! empty dailyWageDetails.details }">
							<c:forEach items="${dailyWageDetails.details }" var="detail"
								varStatus="status">
								<tr>
									<td>${detail.slno }<form:hidden
											path="details[${status.index }].slno" /></td>
									<td>${detail.dateString }<form:hidden
											path="details[${status.index }].date" /></td>
									<td>${detail.empName }<form:hidden
											path="details[${status.index }].empName" /></td>
									<td>${detail.days }<form:hidden
											path="details[${status.index }].days" /></td>
									<td>${detail.dailyRate }<form:hidden
											path="details[${status.index }].dailyRate" /></td>
									<td>${detail.amount }<form:hidden
											path="details[${status.index }].amount" /></td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<td>No records to show...</td>
							</tr>
						</c:otherwise>
					</c:choose>

				</tbody>
			</table>



			<c:if test="${! empty dailyWageDetails.details }">
				<div class="form-group" style="margin-top: 25px;">
					<div class="col-sm-offset-4 col-sm-6">
						<button type="submit" class="btn btn-info" name="type" value="xls">
							Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
						</button>
						<button type="submit" class="btn btn-info" name="type" value="csv">
							Export to CSV &nbsp; <i class="fa fa-file-text-o"></i>
						</button>
						<button type="submit" class="btn btn-info" name="type" value="pdf">
							Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
						</button>
					</div>
				</div>
			</c:if>
		</form:form>
	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>


</html>