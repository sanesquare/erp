<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style>
.resp-vtabs .resp-tabs-container {
	min-height: 850px;
}
</style>

</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Employees Reports</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="parentVerticalTab">
					<ul class="resp-tabs-list hor_1">
						<c:if test="${ rolesMap.roles['Employee Summary'].view}">
							<li>Employee Summary</li>
						</c:if>
						<c:if test="${ rolesMap.roles['Project Employee'].view}">
							<li>Project Employees</li>
						</c:if>
						<c:if test="${ rolesMap.roles['Joining Reports'].view}">
							<li>Employees Joining</li>
						</c:if>
						<c:if test="${ rolesMap.roles['Transfers Reports'].view}">
							<li>Transfers</li>
						</c:if>
						<c:if test="${ rolesMap.roles['Resignation Reports'].view}">
							<li>Resignations</li>
						</c:if>
						<c:if test="${ rolesMap.roles['Travel Reports'].view}">
							<li>Travels</li>
						</c:if>
						<c:if test="${ rolesMap.roles['Assignment Reports'].view}">
							<li>Assignments</li>
						</c:if>
						<li>Performance Evaluation Report</li>

					</ul>
					<div class="resp-tabs-container hor_1">
						<c:if test="${ rolesMap.roles['Employee Summary'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Employee Summary</div>
										<form:form action="viewEmpSummary.do" method="POST"
											id="empSummaryForm" commandName="empReportVo">
											<div class="panel-body">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Employee
														Name</label>
													<div class="col-sm-8 col-xs-10 br-de-emp-list">
														<form:input path="employee" class="form-control"
															placeholder="Search Employee" id="employeeSl" />
														<form:hidden path="employeeCode" id="employeeCode" />
													</div>
													<div class="col-sm-1 col-xs-1">
														<a href="#" class="show2"><i class="fa fa-user"></i></a>
													</div>
												</div>
												<div id="pop1" class="simplePopup">
												</div>
												<div id="pop2" class="simplePopup empl-popup-gen"></div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">
														<button type="submit" class="btn btn-info">Generate
															Report</button>
													</div>
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Project Employee'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Project Employees</div>
										<form:form action="viewEmpProReport.do" method="POST"
											id="empProjectForm" commandName="empReportVo">
											<div class="panel-body">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Project
													</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="projectId" id="empProject">
															<form:options items="${projects }" itemValue="projectId"
																itemLabel="projectTitle" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">
														<button type="submit" class="btn btn-info">Generate
															Report</button>
													</div>
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Joining Reports'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Employees Joining</div>
										<form:form action="viewEmpJoinReport.do" method="POST"
											commandName="empReportVo" id="empJoinForm">
											<div class="panel-body">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Branch</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="branchId">
															<form:option value="0" label="All Branches" />
															<form:options items="${branches }" itemLabel="branchName"
																itemValue="branchId" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Department</label>
													<div class="col-sm-9">
														<form:select path="depId" class="form-control">
															<form:option value="0" label="All Departments" />
															<form:options items="${departments }" itemValue="id"
																itemLabel="name" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Join Date
														Start<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date emJoinDate" id="datepickers">
															<form:input path="startDate" type="text"
																class="form-control" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Join Date End<span
														class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date emJoinDate" id="datepickerss">
															<form:input path="endDate" type="text"
																class="form-control" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">
														<button type="submit" class="btn btn-info">Generate
															Report</button>
													</div>
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Transfers Reports'].view}">

							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Transfers</div>
										<form:form action="viewTransfersReport.do" method="POST"
											commandName="empReportVo" id="empTranForm">
											<div class="panel-body">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Branch</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="branchId">
															<form:option value="0" label="All Branches" />
															<form:options items="${branches }" itemLabel="branchName"
																itemValue="branchId" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Department</label>
													<div class="col-sm-9">
														<form:select path="depId" class="form-control">
															<form:option value="0" label="All Departments" />
															<form:options items="${departments }" itemValue="id"
																itemLabel="name" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Transfer Date
														Start<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date emTranDate" id="datepicker">
															<form:input type="text" class="form-control"
																path="startDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Transfer Date
														End<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date emTranDate"
															id="datepickernew">
															<form:input type="text" class="form-control"
																path="endDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">
														<button type="submit" class="btn btn-info">Generate
															Report</button>
													</div>
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Resignation Reports'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Resignations</div>
										<div class="panel-body">
											<form:form action="viewEmpResignReport.do" method="POST"
												commandName="empReportVo" id="empResForm">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Branch</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="branchId">
															<form:option value="0" label="All Branches" />
															<form:options items="${branches }" itemLabel="branchName"
																itemValue="branchId" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Department</label>
													<div class="col-sm-9">
														<form:select path="depId" class="form-control">
															<form:option value="0" label="All Departments" />
															<form:options items="${departments }" itemValue="id"
																itemLabel="name" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Resignation
														Date Start<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date emResDate" id="datepickeremp">
															<form:input type="text" class="form-control"
																path="startDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Resignation
														Date End<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date emResDate"
															id="datepickerempl">
															<form:input type="text" class="form-control"
																path="endDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">

														<button type="submit" class="btn btn-info">Generate
															Report</button>

													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Travel Reports'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Travels</div>
										<div class="panel-body">
											<form:form action="viewEmpTravelReport.do" method="POST"
												commandName="empReportVo" id="empTravForm">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Branch</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="branchId">
															<form:option value="0" label="All Branches" />
															<form:options items="${branches }" itemLabel="branchName"
																itemValue="branchId" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Department</label>
													<div class="col-sm-9">
														<form:select path="depId" class="form-control">
															<form:option value="0" label="All Departments" />
															<form:options items="${departments }" itemValue="id"
																itemLabel="name" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Travel Date
														Start<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date emTravDate"
															id="datepickeremployees">
															<form:input type="text" class="form-control"
																path="startDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Travel Date<br>End<span
														class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date emTravDate"
															id="datepickersss">
															<form:input type="text" class="form-control"
																path="endDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">
														<button type="submit" class="btn btn-info">Generate
															Report</button>
													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Assignment Reports'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Assignments</div>
										<form:form action="viewAssigmentReport" method="POST"
											commandName="empReportVo" id="empAssinForm">

											<div class="panel-body">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Branch</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="branchId">
															<form:option value="0" label="All Branches" />
															<form:options items="${branches }" itemLabel="branchName"
																itemValue="branchId" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Department</label>
													<div class="col-sm-9">
														<form:select path="depId" class="form-control">
															<form:option value="0" label="All Departments" />
															<form:options items="${departments }" itemValue="id"
																itemLabel="name" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Assignment
														Date Start<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date emAssinDate"
															id="datepickernew9">
															<form:input type="text" class="form-control"
																path="startDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Assignment
														Date End<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date emAssinDate"
															id="datepickernew10">
															<form:input type="text" class="form-control"
																path="endDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">
														<button type="submit" class="btn btn-info">Generate
															Report</button>
													</div>
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Performance Evaluation Report</div>
										<form:form action="viewEmpEvaluationReport.do" method="POST"
											  commandName="empReportVo" id="empPerformanceForm">

											<div class="panel-body">
												<div class="form-group">
													<label class="col-sm-3 control-label">Evaluation
														Date Start<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date emPerDate" id="datepickernew3">
															<form:input type="text" class="form-control"
																path="startDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Evaluation
														Date End<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date emPerDate" id="datepickernew4">
															<form:input type="text" class="form-control"
																path="endDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">
														<button type="submit" class="btn btn-info">Generate
															Report</button>
													</div>
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'
/>"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<script
		src='<c:url value="/resources/assets/js/jquery.simplePopup.js" />'></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script>
		$(document).ready(function() {
			$('#parentVerticalTab').easyResponsiveTabs({

				type : 'vertical', //Types: default, vertical, accordion 
				width : 'auto', //auto or any width like 600px 
				fit : true, // 100% fit in a container 
				closed : 'accordion', // Start closed if in accordion view 
				tabidentify : 'hor_1', // The tab groups identifier 
				activate : function(event) { // Callback function if tab is switched 
					var $tab = $(this);
					var $info = $('#nested-tabInfo2');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
		});
	</script>
	<script type="text/javascript">
		$(function() {

			if ($.isFunction($.fn.datetimepicker)) {
				$('#datepicker').datetimepicker({
					pickTime : false
				});
				$('#datepickernew').datetimepicker({
					pickTime : false
				});
				$('#datepickernew1').datetimepicker({
					pickTime : false
				});
				$('#datepickernew2').datetimepicker({
					pickTime : false
				});
				$('#datepickernew3').datetimepicker({
					pickTime : false
				});
				$('#datepickernew4').datetimepicker({
					pickTime : false
				});
				$('#datepickernew5').datetimepicker({
					pickTime : false
				});
				$('#datepickernew6').datetimepicker({
					pickTime : false
				});
				$('#datepickernew7').datetimepicker({
					pickTime : false
				});
				$('#datepickernew8').datetimepicker({
					pickTime : false
				});
				$('#datepickernew9').datetimepicker({
					pickTime : false
				});
				$('#datepickernew10').datetimepicker({
					pickTime : false
				});

			}
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#employeeSl').bind('click', function() {
				showPop();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").addClass("active");
		});
	</script>
</body>
</html>