<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#cntrtc_sve_sccs {
	display: none
}

#cntrtc_failed {
	display: none
}
</style>
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Contracts</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<a href="contracts.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="success_msg" id="cntrtc_sve_sccs">
					<span id="msgs_icn"><i class="fa fa-check"></i></span>Saved
					Successfully.
				</div>
				<div class="error_msg" id="cntrtc_failed">
					<span id="msge_icn"><i class="fa fa-times"></i></span>Failed...
				</div>
			</div>
		</div>


		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>

				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form action="saveContract.do" method="POST"
								commandName="contractVo">

								<form:hidden path="contractId" id="contrctId" />

								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Contract Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Employee</label>
														<div class="col-sm-8 col-xs-10">

															<%-- <form:select cssClass="form-control"
																onchange="getEmployeeDetails()" path="employeeId"
																id="emp_join_emp">
																<option></option>
															</form:select> --%>
															<label for="inputEmail3"
																class="col-sm-3 col-xs-12 control-label">${contractVo.employeeCode }</label>

														</div>

														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div>
													</div>


													<div id="pop2" class="simplePopup">
														<div class="col-md-12">
															<div class="form-group">
																<h1 class="pop_hd">
																	<label>${contractVo.employeeVo.employeeName }</label>
																</h1>

																<div class="row">

																	<div class="col-md-3">
																		<img src="assets/images/avtar/user.png"
																			class="pop_img">
																	</div>
																	<div class="col-md-9">

																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">E-mail </label>
																			<div class="col-sm-9">
																				<label for="inputPassword3"
																					class="col-sm-6 control-label ft">${contractVo.employeeVo.email }</label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Designation </label>
																			<div class="col-sm-9">
																				<label for="inputPassword3"
																					class="col-sm-6 control-label ft">${contractVo.employeeVo.designation }</label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Department </label>
																			<div class="col-sm-9">
																				<label for="inputPassword3"
																					class="col-sm-6 control-label ft">${contractVo.employeeVo.employeeDepartment }</label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Branch </label>
																			<div class="col-sm-9">
																				<label for="inputPassword3"
																					class="col-sm-6 control-label ft">${contractVo.employeeVo.employeeBranch }</label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Join Date </label>
																			<div class="col-sm-9">
																				<label for="inputPassword3"
																					class="col-sm-6 control-label ft">${contractVo.employeeVo.joiningDate }</label>
																			</div>
																		</div>



																	</div>

																</div>
															</div>


														</div>


													</div>



													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Contract
															Type</label>
														<div class="col-sm-9 col-xs-10">
															<form:select path="contractTypeId" id="contrct_add_type"
																cssClass="form-control">
																<form:options items="${ type}" itemLabel="contractType"
																	itemValue="contractTypeId" />
															</form:select>

														</div>

														<!-- <div class="col-sm-1 col-xs-1">
															<a href="#" class="show1"><i
																class="fa fa-plus-square-o pop_icon"></i></a>
														</div> -->
													</div>

													<div id="pop1" class="simplePopup">
														<div class="col-md-12">
															<div class="form-group">
																<h1 class="pop_hd">Contract Type</h1>

																<div class="row">

																	<div class="col-md-12">
																		<div class="form-group">

																			<div class="col-sm-12">
																				<input type="text" class="form-control"
																					id="inputPassword3" placeholder=" Contract Type">

																			</div>
																		</div>

																		<!-- <div class="form-group">
																		<div class="col-sm-offset-3 col-sm-9">
																			<button type="submit" class="btn btn-info flr">Save</button>
																		</div>
																	</div> -->


																	</div>


																</div>
															</div>


														</div>


													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Contract
															Title <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input id="cntrt_add_title" path="title"
																cssClass="form-control" />
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Contract
															Start Date<span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepicker">
																<form:input path="startingDate" readonly="true"
																	style="background-color: #ffffff !important;"
																	cssClass="form-control" id="cntrt_add_strdte"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Contract End
															Date<span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepickers">
																<form:input path="endDate" readonly="true"
																	style="background-color: #ffffff !important;"
																	cssClass="form-control" id="cntrt_add_enddte"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
																<div id="prjct_end_date_err" class="alert_box">
																	<div class="triangle-up"></div>
																	<spring:message code="project.info.date.err_msg" />
																</div>
															</div>
														</div>

													</div>






													<!-- <div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-info">Next</button>
													</div>
												</div> -->


												</div>
											</div>
										</div>



										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Employee Details</div>
												<div class="panel-body">

													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Designation</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_desg">${contractVo.employeeVo.designation }</label>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Type</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_type">${contractVo.employeeVo.employeeType }</label>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Category</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_ctgry">${contractVo.employeeVo.employeeCategory }</label>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Grade</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_grde">${contractVo.employeeVo.employeeGrade }</label>
														</div>

													</div>



													<div class="form-group">
														<label class="col-sm-3 control-label">Branch</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_brnch">${contractVo.employeeVo.employeeBranch }</label>
														</div>

													</div>




													<div class="form-group">
														<label class="col-sm-3 control-label">Department</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_dept">${contractVo.employeeVo.employeeDepartment }</label>
														</div>

													</div>







													<!-- 
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-info">Next</button>
													</div>
												</div>
 -->

												</div>
											</div>
										</div>

									</div>

									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Contract Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">


															<form:textarea path="description"
																id="cntrct_add_descrptn" cssClass="form-control"
																cssStyle="height: 150px;resize:none" />




														</div>
													</div>


													<!-- <div class="form-group">
													<div class="col-sm-offset-16 col-sm-12">
														<button type="submit" class="btn btn-info">Next</button>
													</div>
												</div> -->

												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" id="cntrct_add_notes"
																cssClass="form-control height" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${contractVo.createdBy }</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${contractVo.createdOn }</label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="contrct_info_save_btn"
																onclick="validateContrctEdit()" class="btn btn-info">Save</a>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								</div>
							</form:form>



						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<form:form action="uploadContrctDocs.do" method="POST"
								commandName="contractDocumentVo" enctype="multipart/form-data">
								<form:hidden path="contractId" id="cntract-iddd" />
								<c:if test="${ rolesMap.roles['Contract Document'].add}">
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Upload Documents</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Upload
														Documents </label>
													<div class="col-sm-9">
														<input type="file" multiple name="cntrctFileUpload"
															id="contract-docs" class="btn btn-info" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-16 col-sm-12">
														<button type="submit" class="btn btn-info"
															id="contract-docs-btn">Upload</button>
													</div>
												</div>


											</div>
										</div>
									</div>
								</c:if>


								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">

											<c:if test="${!empty contractVo.documentVos }">
												<table class="table no-margn">
													<thead>
														<tr>
															<th>Document Name</th>
															<c:if test="${ rolesMap.roles['Contract Document'].view}">
																<th>view</th>
															</c:if>
															<c:if
																test="${ rolesMap.roles['Contract Document'].delete}">
																<th>Delete</th>
															</c:if>
														</tr>
													</thead>
													<tbody>
														<!-- <tr>
															<td><i class="fa fa-file-o"></i> Lorem ipsum dolor
																sit amet</td>
															<td><i class="fa fa-file-text-o sm"></i></td>
															<td><i class="fa fa-close ss"></i></td>
														</tr> -->
														<c:forEach items="${contractVo.documentVos }" var="d">
															<tr>
																<td>${d. fileName}</td>
																<c:if
																	test="${ rolesMap.roles['Contract Document'].view}">
																	<td><a href="viewDocCOntrct.do?url=${d. url}"><i
																			class="fa fa-close ss"></i></a></td>
																</c:if>
																<c:if
																	test="${ rolesMap.roles['Contract Document'].delete}">
																	<td><a href="deleteDocCOntrct.do?url=${d. url}"><i
																			class="fa fa-close ss"></i></a></td>
																</c:if>
															</tr>

														</c:forEach>
													</tbody>
												</table>
											</c:if>

										</div>
									</div>
								</div>


							</form:form>

						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Simple Map</div>
									<div class="panel-body">
										<div id="simpleMap" style="height: 500px;"></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->


	<footer class="container-fluid footer"> Copyright &copy; 2014
	<a href="http://freakpixels.com/" target="_blank">FreakPixels</a> <a
		href="#" class="pull-right scrollToTop"><i
		class="fa fa-chevron-up"></i></a> </footer>


	</section>
	<!-- Content Block Ends Here (right box)-->



	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			if ($("#contract-docs").val() == '') {
				$("#contract-docs-btn").attr("disabled", "true");
			}
			$("#contract-docs").on("change", function() {
				if ($("#cntract-iddd").val() != '')
					$("#contract-docs-btn").removeAttr("disabled");
			})

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>