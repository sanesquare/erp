<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#add_prjc_succs {
	display: none;
}

#add_prjc_err {
	display: none;
}

#prjsts_save_success {
	display: none;
}

#prjsts_save_failed {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>
				<spring:message code="projects.title" />
			</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="projects.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="success_msg" id="add_prjc_succs">
					<span id="msgs_icn"><i class="fa fa-check"></i></span> Project
					Saved Successfully.
				</div>
				<div class="error_msg" id="add_prjc_err">
					<span id="msge_icn"><i class="fa fa-times"></i></span> Project Save
					Failed.
				</div>
			</div>
		</div>



		<div class="row">


			<div class="col-md-12">

				<form:form action="saveProject.do" method="POST"
					commandName="projectVo" enctype="multipart/form-data">
					<form:hidden path="projectId" id="projectId_add" />
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#all"
							role="tab" data-toggle="tab"><spring:message
									code="project.tab.information" /></a></li>
						<li role="presentation"><a href="#users" role="tab"
							data-toggle="tab"><spring:message
									code="project.tab.documents" /></a></li>
						<li role="presentation"><a href="#docs" role="tab"
							data-toggle="tab"><spring:message code="project.tab.status" /></a></li>
					</ul>



					<div class="tab-content">
						<div role="tabpanel"
							class="panel panel-default tab-pane tabs-up active" id="all">

							<div class="panel-body">


								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">
													<spring:message code="project.info.title" />
												</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label"><spring:message
																code="project.info.project_title" /> <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="projectTitle" class="form-control"
																label="Project Title" id="add_project_title"></form:input>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label"><spring:message
																code="project.info.start_date" /> <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class='input-group date' id="datepicker">
																<form:input path="projectStartDate" class="form-control"
																	style="background-color: #ffffff !important;"
																	data-date-format="DD/MM/YYYY" readonly="true"
																	id="add_project_startdate" />
																<span class="input-group-addon"><i
																	class="glyphicon glyphicon-calendar"></i> </span>
															</div>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label"><spring:message
																code="project.info.end_date" /> <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class='input-group date' id="datepickers">
																<form:input path="projectEndDate" type="text"
																	readonly="true"
																	style="background-color: #ffffff !important;"
																	class="form-control" data-date-format="DD/MM/YYYY"
																	id="add_project_enddate" />
																<span id="end_date" class="input-group-addon"><span
																	class="glyphicon glyphicon-calendar"></span> </span>
															</div>
															<div id="prjct_end_date_err" class="alert_box">
																<div class="triangle-up"></div>
																<spring:message code="project.info.date.err_msg" />
															</div>
														</div>

													</div>






												</div>
											</div>
										</div>






										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">
													<spring:message code="project.info.employee.title" />
												</div>
												<div class="panel-body">

													<div class="form-group">
														<label class="col-sm-3 control-label">Branch</label>
														<div class="col-sm-9">
															<form:select path="branchId" onchange="getEmployees()"
																style="background-color: #ffffff !important;"
																id="emp_join_brnch" cssClass="form-control">
																<form:option value="" label="Select Branch" />
																<form:options items="${branch }" itemLabel="branchName"
																	itemValue="branchId" />
															</form:select>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Department </label>
														<div class="col-sm-9">
															<form:select path="departmentID"
																onchange="getEmployees()"
																style="background-color: #ffffff !important;"
																id="emp_join_dprmnt" cssClass="form-control">
																<form:option value="" label="Select Department" />
																<form:options items="${departments }" itemLabel="name"
																	itemValue="id" />
															</form:select>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label"><spring:message
																code="project.info.projecthead.title" /> <span class="stars">*</span></label>
														<div class="col-sm-9 ">
															<form:select cssClass="form-control chosen-select"
																itemLabel="" multiple="true" path="headIds"
																data-placeholder="--Project Head--"
																id="add_project_head">
																<c:forEach items="${employees }" var="emp">
																<form:option 
																	value="${emp.employeeId }" label="${emp.name } (${emp.employeeCode })" />
																	<%-- <form:options items="${employees }"
																	itemValue="employeeId" itemLabel="name" /> --%>
																</c:forEach>
															</form:select>
															<div id="prjct_head_err" class="alert_box">
																<div class="triangle-up"></div>
																Please Select Project Head
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label"><spring:message
																code="project.info.select_employee" /> <span class="stars">*</span></label>
														<div class="col-sm-9 ">
															<form:select cssClass="form-control chosen-select"
																itemLabel="" multiple="true" path="employeesIds"
																data-placeholder="--Project Employees--"
																id="add_project_employees">
																<c:forEach items="${employees }" var="emp">
																<form:option 
																	value="${emp.employeeId }" label="${emp.name } (${emp.employeeCode })" />
																</c:forEach>
																<%-- <form:options items="${employees }"
																	itemValue="employeeId" itemLabel="name" /> --%>
															</form:select>
															<div id="prjct_emp_err" class="alert_box">
																<div class="triangle-up"></div>
																Please Select Employee
															</div>
														</div>
													</div>




												</div>
											</div>
										</div>



									</div>

									<div class="col-md-6">


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">
													<spring:message code="project.info.project_description" /> <span class="stars">*</span>
												</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">

															<!-- <textarea name="projectDescription" class="wysihtml form-control" style="height: 150px"></textarea> -->
															<%-- <form:textarea path="projectDescription" cssClass="wysihtml form-control"
																cssStyle="height: 150px">
																</form:textarea> --%>
															<form:textarea path="projectDescription"
																cssClass="form-control height "
																id="add_project_description" />



														</div>
													</div>



												</div>
											</div>
										</div>




										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">
													<spring:message code="project.info.additional_info" />
												</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label"><spring:message
																code="project.info.notes" /> </label>
														<div class="col-sm-9">
															<!-- <textarea name="projectAdditionalInformation" class="form-control" style="height: 150px;resize:none;"></textarea> -->
															<%-- <form:textarea path="projectAdditionalInformation" cssClass="form-control height">
															</form:textarea> --%>
															<form:textarea path="projectAdditionalInformation"
																id="add_prjct_nts" cssClass="form-control height" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label"><spring:message
																code="project.info.record_by" /></label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${projectVo.createdBy }
															</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label"><spring:message
																code="project.info.record_on" /></label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${projectVo.createdOn }
															</label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<!-- <a id="next_button_addproject"  class="btn btn-info">Next</a> -->
															<!-- <button type="submit" value="SaveInformation" class="btn btn-info"
															id="submt_proj_info_btn" name="SaveInformation">Save Information</button> -->
															<a class="btn btn-info" id="submt_proj_info_btn"><spring:message
																	code="project.info.btn.save_info" /></a> <a
																class="btn btn-info" id="updt_proj_info_btn"><spring:message
																	code="project.info.btn.updt_info" /></a>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								</div>



							</div>
						</div>
						<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
							id="users">
							<div class="panel-body">


								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<spring:message code="project.doc.title" />
										</div>
										<div class="panel-body">

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label"><spring:message
														code="project.doc.label" /></label>
												<div class="col-sm-9">
													<input type="file" multiple class="btn btn-info"
														id="project_docs" name="project_file_upload" />
												</div>
											</div>


										</div>
									</div>
								</div>


								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<spring:message code="project.doc.table_title" />
										</div>
										<div class="panel-body">


											<c:if test="${!empty projectVo.projectDocuments}">
												<table class="table no-margn">
													<thead>
														<tr>
															<th>#</th>
															<th><spring:message
																	code="project.doc.table.document_name" /></th>
															<c:if test="${ rolesMap.roles['Project Documents'].view}">
																<th><spring:message code="project.doc.table.view" /></th>
															</c:if>
															<c:if
																test="${ rolesMap.roles['Project Documents'].delete}">
																<th><spring:message code="project.doc.table.delete" /></th>
															</c:if>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${projectVo.projectDocuments }"
															var="doc" varStatus="s">
															<tr>
																<td>${s.count }</td>
																<td><i class="fa fa-file-o"></i>${doc.fileName }</td>
																<c:if
																	test="${ rolesMap.roles['Project Documents'].view}">
																	<td><a href="viewProjectDocument.do?durl=${doc.url }"><i
																			class="fa fa-file-text-o sm"></i></a></td>
																</c:if>
																<c:if
																	test="${ rolesMap.roles['Project Documents'].delete}">
																	<td><a
																		href="deleteProjectDocument.do?durl=${doc.url }"><i
																			class="fa fa-close ss"></i></a></td>
																</c:if>
															</tr>
														</c:forEach>

													</tbody>
												</table>
											</c:if>

										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<!-- <a href="#docs" data-toggle="tab" class="btn btn-info">Next</a> -->
										<c:if test="${ rolesMap.roles['Project Documents'].add}">
											<button type="submit" value="uploadDocuments"
												class="btn btn-info" id="sbmt_proj_doc_btn"
												name="uploadDocuments">
												<spring:message code="project.doc.btn.upload_doc" />
											</button>
										</c:if>
										<!-- <a class="btn btn-info" id="sbmt_proj_doc_btn">Upload
											Documents</a> -->
									</div>
								</div>

							</div>
						</div>





						<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
							id="docs">
							<div class="panel-body">
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading"
											<spring:message code="project.status.title"/>></div>
										<div class="panel-body">

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label"><spring:message
														code="project.status.label" /></label>
												<div class="col-sm-9">

													<!-- <select class="form-control">
														<option>One</option>
														<option>One</option>
														<option>One</option>
													</select> -->
													<form:select path="projectStatusId" cssClass="form-control">
														<form:option value="" label="Select status" />
														<c:forEach items="${status }" var="status">
															<form:option value="${status.projectStatusId }"
																label="${status.projectStatus }"></form:option>
														</c:forEach>

													</form:select>

												</div>
											</div>


										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-9">
											<a class="btn btn-info" id="sbmt_proj_stats_btn"><spring:message
													code="project.status.btn.update_status" /></a>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12" id="prjsts_save_success">
											<div class="box_success">
												<i class="fa fa-check ffss"></i>Saved successfully...
											</div>
										</div>
										<div class="col-sm-12" id="prjsts_save_failed">
											<div class="box_error">
												<i class="fa fa-times ffss"></i>Failed to save..!!
											</div>
										</div>
									</div>
								</div>


							</div>
						</div>


					</div>
				</form:form>
			</div>


		</div>




	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<!-- Custom JQuery -->
	<!-- 	<script src="assets/js/app/custom.js" type="text/javascript"></script> -->

	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(
				function() {
					var date = new Date();
					var currentDate = date.getDate() + "/"
							+ (date.getMonth() + 1) + "/" + date.getFullYear();
					$('.show1').click(function() {
						$('#pop1').simplePopup();
					});

					$('.show2').click(function() {
						$('#pop2').simplePopup();
					});

					$("#sbmt_proj_stats_btn").attr('disabled', 'disabled');
					$("#sbmt_proj_doc_btn").attr('disabled', 'disabled');

					var id = $("#projectId_add").val();
					if (id != '') {
						$("#updt_proj_info_btn").css("display", "block");
						$("#submt_proj_info_btn").css("display", "none");
						$("#sbmt_proj_stats_btn").removeAttr('disabled');
					} else {
						$("#updt_proj_info_btn").css("display", "none");
						$("#submt_proj_info_btn").css("display", "block");
					}

					if ($("#project_docs").val() == '') {
						$("#sbmt_proj_doc_btn").attr('disabled', 'disabled');
					}

					$("#project_docs").on("change", function() {
						if ($("#projectId_add").val() != '')
							$("#sbmt_proj_doc_btn").removeAttr('disabled');
					});

					$("#dashbordli").removeClass("active");
					$("#organzationli").addClass("active");
					$("#recruitmentli").removeClass("active");
					$("#employeeli").removeClass("active");
					$("#timesheetli").removeClass("active");
					$("#payrollli").removeClass("active");
					$("#reportsli").removeClass("active");
				});
	</script>
</body>
</html>