<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form action="SaveSecondLevelForwarders.do" method="post"
	commandName="secondLevelForwardingVo">
	<form:select path="forwaders" multiple="true"
		class="form-control chosen-select forwaders"
		data-placeholder="Select Superior">
		<form:options items="${payRollEmployees}" itemValue="value"
			itemLabel="label" />
	</form:select>
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="submit" class="btn btn-info">Save</button>
		</div>
	</div>
</form:form>

<script>
	$(document).ready(function() {
		$(".forwaders").chosen();
	});
</script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>