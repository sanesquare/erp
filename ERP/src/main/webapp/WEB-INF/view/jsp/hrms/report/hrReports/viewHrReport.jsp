<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
</head>

<body style="background: #FFFFFF !important;">
	<h2 class="role_hd">Employee List</h2>

	<form:form id="mainForm" action="pdfRequest.do"
		modelAttribute="employeeDetails" method="post">
		<c:if test="${! empty employeeDetails}">
			<table class="table table-striped no-margn line">
				<tbody>
					<tr>
						<td class="hd_tb"><span class="WhiteHeading">S#</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Employee
								Name</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Designation</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Branch</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Department</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Type</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Category</span></td>
						<td class="hd_tb"><span class="WhiteHeading">User Name</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Email
								Address</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Joining
								Date</span></td>
					</tr>
					<c:forEach items="${employeeDetails.empReportVos}" var="emp"
						varStatus="status">
						<tr>
							<td>${emp.slno}<form:hidden
									path="empReportVos[${status.index}].slno" value="${emp.slno}" /></td>
							<td align="center">${emp.employeeName}<form:hidden
									path="empReportVos[${status.index}].employeeName" /></td>
							<td>${emp.designation}<form:hidden
									path="empReportVos[${status.index}].designation" /></td>
							<td>${emp.branch}<form:hidden
									path="empReportVos[${status.index}].branch" /></td>
							<td>${emp.department}<form:hidden
									path="empReportVos[${status.index}].department" /></td>
							<td>${emp.type}<form:hidden
									path="empReportVos[${status.index}].type" /></td>
							<td>${emp.category}<form:hidden
									path="empReportVos[${status.index}].category" /></td>
							<td>${emp.userName}<form:hidden
									path="empReportVos[${status.index}].userName" /></td>
							<td>${emp.email}<form:hidden
									path="empReportVos[${status.index}].email" /></td>
							<td>${emp.dateJoin}<form:hidden
									path="empReportVos[${status.index}].joinDate" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
		<div class="form-group" style="margin-top: 25px;">
			<div class="col-sm-offset-4 col-sm-6">
				<button type="submit" class="btn btn-info" name="type" value="xls">
					Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
				</button>
				<button type="submit" class="btn btn-info" name="type" value="csv">
					Export to CSV &nbsp; <i class="fa fa-file-text-o"></i>
				</button>
				<button type="submit" class="btn btn-info" name="type" value="pdf">
					Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
				</button>
			</div>
		</div>
	</form:form>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>
	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<!-- Responsive tabs -->
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"
		type="text/javascript"></script>

	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</html>