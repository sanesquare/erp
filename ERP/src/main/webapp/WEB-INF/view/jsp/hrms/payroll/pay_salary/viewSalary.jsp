<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">

		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default erp-panle">
					<div class="panel-heading  panel-inf">
						Pay Salary<a href="approvedSalaries"><span class="back-btn"
							style="float: right"><i class="fa fa-arrow-left"></i></span></a>
					</div>
					<div class="panel-body" id="panelBody">
						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Date
									</label>
									<div class="col-sm-9">
										<div class='input-group date cmp_fr jrnl-date' id="datepicker">
											<input type="text" readonly="true" value="${date }"
												name="periodStarts" id="pySalDate"
												style="background-color: #ffffff !important;"
												data-date-format="DD/MM/YYYY" class="form-control" /> <span
												class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span> </span>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-5">
								<%-- <form action="" id="ledger-group-form"> --%>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Select
										Account </label>
									<div class="col-sm-9">
										<select class="form-control" id="ledgerSel">
											<option value="">-Select Account-</option>
											<c:forEach items="${accounts }" var="a">
												<option value="${a.ledgerId }" label="${a.ledgerName }" />
											</c:forEach>
										</select>
									</div>
								</div>
								<input type="hidden" id="salaryId">
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<button type="button" onclick="payPaySalary()"
										style="float: right;" class="btn btn-primary erp-btn">Pay</button>
								</div>
								<%-- </form> --%>
							</div>
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-body">

										<c:if test="${paySalary.monthly }">
											<form:form commandName="paySalary"
												action="saveMontlhyPaysalary.do" method="POST">
												<div class="col-xs-12 msm" style="overflow-x: scroll;">
													<form:hidden path="date" />
													<form:hidden path="id" id="payId" />
													<table class="table table-bordered split_width"
														width="100%">
														<tbody>
															<tr>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">Pay</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">S NO</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">Emp Code</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">Employee Name</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">Branch</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">Department</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">Designation</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">PF Applicability</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">ESI Applicability</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">Work Status</span></td>
																<td class="hd_tb split offer_salary" colspan="6"><span
																	class="WhiteHeading">Offered Salary</span></td>
																<td class="hd_tb split" colspan="2"><span
																	class="WhiteHeading">LOP</span></td>
																<td class="hd_tb split" colspan="4"><span
																	class="WhiteHeading">Additional Earnings</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">Total Earnings</span></td>
																<td class="hd_tb split" colspan="10"><span
																	class="WhiteHeading">Deductions </span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">Net Pay</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">PF Employer Contribution</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">ESI Employer Contribution</span></td>
																<td class="hd_tb split" rowspan="2"><span
																	class="WhiteHeading">CTC</span></td>
															</tr>
															<tr>
																<td class="hd_tb split"><span class="WhiteHeading">Basic</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">DA</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">HRA</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">CCA</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">Conveyance</span></td>
																<input type="hidden"
																	value="${fn:length(paySalary.extraPayslipItems)}"
																	id="itemSize" />
																<c:forEach items="${paySalary.extraPayslipItems }"
																	var="extraItems" varStatus="items">
																	<input type="hidden" value="${extraItems }"
																		id="item${items.index }">
																	<td class="hd_tb split count"><span
																		class="WhiteHeading">${extraItems }</span></td>
																</c:forEach>
																<td class="hd_tb split"><span class="WhiteHeading">Gross
																		Salary</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">LOP
																		Count </span></td>
																<td class="hd_tb split"><span class="WhiteHeading">LOP
																		Deduction </span></td>
																<td class="hd_tb split"><span class="WhiteHeading">Incentive
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">Encashment
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">Bonuses
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">Increment
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">PF
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">ESI
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">LWF
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">PROF:TAX
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">TDS
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">Other
																		Deduction </span></td>
																<td class="hd_tb split"><span class="WhiteHeading">LOP
																		Deduction </span></td>
																<td class="hd_tb split"><span class="WhiteHeading">Loan
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">Advance
																</span></td>
																<td class="hd_tb split"><span class="WhiteHeading">Total
																		Deductions </span></td>


															</tr>
															<c:forEach items="${paySalary.employeeItemsVo }"
																var="items" varStatus="s">
																<tr>
																	<form:hidden
																		path="employeeItemsVo[${s.index }].monthlyId" />
																	<td><c:choose>
																			<c:when test="${items.isPaid }">
																				<input type="checkbox" class="payCheck"
																					checked="checked" value=" ${items.checkBoxValue }">
																			</c:when>
																			<c:otherwise>
																				<input type="checkbox" class="payCheck"
																					value=" ${items.checkBoxValue }">
																			</c:otherwise>
																		</c:choose></td>
																	<td>${s.count }</td>
																	<td><input type="text" readonly="readonly"
																		name="employeeItemsVo[${s.index }].employeeCode"
																		value="${items.employeeCode }" class="input_amouts" /></td>
																	<td>${items.name }</td>
																	<td>${items.branch }</td>
																	<td>${items.department }</td>
																	<td>${items.designation }</td>
																	<td><input type="text" readonly="readonly"
																		name="employeeItemsVo[${s.index }].pfApplicability"
																		value="${items.pfApplicability }" class="input_amouts" /></td>
																	<td><input type="text" readonly="readonly"
																		name="employeeItemsVo[${s.index }].esiApplicability"
																		value="${items.esiApplicability }"
																		class="input_amouts" /></td>
																	<td>${items.status}</td>
																	<td><input type="text" value="${items.basic }"
																		id="basic${s.index }"
																		onkeyup="calculateGrossSalary('${s.index }')"
																		name="employeeItemsVo[${s.index }].basic"
																		class="input_amouts double" /></td>
																	<td><input type="text" value="${items.da }"
																		id="da${s.index }"
																		onkeyup="calculateGrossSalary('${s.index }')"
																		name="employeeItemsVo[${s.index }].da"
																		class="input_amouts double" /></td>
																	<td><input type="text" value="${items.hra }"
																		id="hra${s.index }"
																		onkeyup="calculateGrossSalary('${s.index }')"
																		name="employeeItemsVo[${s.index }].hra"
																		class="input_amouts double" /></td>
																	<td><input type="text" value="${items.cca }"
																		id="cca${s.index }"
																		onkeyup="calculateGrossSalary('${s.index }')"
																		name="employeeItemsVo[${s.index }].cca"
																		class="input_amouts double" /></td>
																	<td><input type="text" id="conveyance${s.index }"
																		onkeyup="calculateGrossSalary('${s.index }')"
																		name="employeeItemsVo[${s.index }].conveyance"
																		value="${items.conveyance }"
																		class="input_amouts double" /></td>
																	<c:choose>
																		<c:when test="${! paySalary.edit }">
																			<c:forEach items="${paySalary.extraPayslipItems }"
																				varStatus="status" var="extraItems">
																				<c:set var="isSet" scope="page" value="false" />
																				<c:forEach items="${items.otherSalaryItems }"
																					var="other">
																					<c:if test="${other.key ==  extraItems}">
																						<c:set var="isSet" scope="page" value="true" />
																						<td><input type="text"
																							onkeyup="updateListValue('${s.count }','${extraItems }','${s.index }')"
																							id="text${s.count }${extraItems}"
																							name="${employeeItemsVo[s.index].otherSalaryItems[s.index ].value }"
																							value="${other.value }"
																							class="input_amouts double" /></td>
																						<input type="hidden"
																							id="hidden${s.count }${extraItems}"
																							name="employeeItemsVo[${s.index }].otherSalaryItemsList[${status.index }]"
																							value="${other.value }&!=${extraItems }" />
																					</c:if>
																				</c:forEach>
																				<c:if test="${isSet == 'false'}">
																					<input type="hidden"
																						id="hidden${s.count }${extraItems}"
																						name="employeeItemsVo[${s.index }].otherSalaryItemsList[${status.index }]"
																						value="0.00&!=${extraItems }" />
																					<td><input type="text" value="0.00"
																						onkeyup="updateListValue('${s.count }','${extraItems }','${s.index }')"
																						id="text${s.count }${extraItems}"
																						name="${employeeItemsVo[s.index].otherSalaryItems[s.index ].value }"
																						class="input_amouts double" /></td>
																				</c:if>
																			</c:forEach>
																		</c:when>

																		<c:otherwise>
																			<c:forEach items="${paySalary.extraPayslipItems }"
																				varStatus="status" var="extraItems">
																				<c:forEach items="${items.otherSalaryItems }"
																					var="other">
																					<c:choose>
																						<c:when test="${other.key ==  extraItems}">
																							<td><input type="text"
																								onkeyup="updateListValue('${s.count }','${extraItems }','${s.index }')"
																								id="text${s.count }${extraItems}"
																								name="${employeeItemsVo[s.index].otherSalaryItems[s.index ].value }"
																								value="${other.value }"
																								class="input_amouts double" /></td>
																							<input type="hidden"
																								id="hidden${s.count }${extraItems}"
																								name="employeeItemsVo[${s.index }].otherSalaryItemsList[${status.index }]"
																								value="${other.value }&!=${extraItems }" />
																						</c:when>
																					</c:choose>
																				</c:forEach>
																				<c:if test="${empty  items.otherSalaryItems}">
																					<input type="hidden"
																						id="hidden${s.count }${extraItems}"
																						name="employeeItemsVo[${s.index }].otherSalaryItemsList[${status.index }]"
																						value="0.00&!=${extraItems}" />
																					<td><input type="text" value="0.00"
																						onkeyup="updateListValue('${s.count }','${extraItems }','${s.index }')"
																						id="text${s.count }${extraItems}"
																						class="input_amouts double" /></td>
																				</c:if>
																			</c:forEach>
																		</c:otherwise>
																	</c:choose>

																	<td><input type="text"
																		id="gross_salary${s.index }"
																		onkeyup="calculateTotalEarnings('${s.index }')"
																		name="employeeItemsVo[${s.index }].grossSalary"
																		value="${items.grossSalary }"
																		class="input_amouts double" /></td>
																	<td><input type="text"
																		name="employeeItemsVo[${s.index }].lopCount"
																		value="${items.lopCount }" readonly="readonly"
																		class="input_amouts double" /></td>
																	<td><input type="text"
																		id="lop_ded_view${s.index }" readonly="readonly"
																		name="employeeItemsVo[${s.index }].lopDeduction"
																		value="${items.lopDeduction }"
																		class="input_amouts double" /></td>
																	<td><input type="text" id="incentive${s.index }"
																		onkeyup="calculateTotalEarnings('${s.index }')"
																		name="employeeItemsVo[${s.index }].incentive"
																		value="${items.incentive }"
																		class="input_amouts double" /></td>
																	<td><input type="text" id="encashment${s.index }"
																		onkeyup="calculateTotalEarnings('${s.index }')"
																		name="employeeItemsVo[${s.index }].encashment"
																		value="${items.encashment }"
																		class="input_amouts double" /></td>
																	<td><input type="text" value="${items.bonus }"
																		id="bonus${s.index }"
																		onkeyup="calculateTotalEarnings('${s.index }')"
																		name="employeeItemsVo[${s.index }].bonus"
																		class="input_amouts double" /></td>
																	<td><input type="text" id="increment${s.index }"
																		onkeyup="calculateTotalEarnings('${s.index }')"
																		name="employeeItemsVo[${s.index }].increment"
																		value="${items.increment }"
																		class="input_amouts double" /></td>
																	<td><input type="text"
																		onkeyup="calculateNetpay('${s.index }')"
																		id="total_earnings${s.index }"
																		name="employeeItemsVo[${s.index }].totalEarnings"
																		value="${items.totalEarnings }"
																		class="input_amouts double" /></td>
																	<td><input type="text" value="${items.pf }"
																		onkeyup="calculateDeduction('${s.index }')"
																		id="pf_ded${s.index }"
																		name="employeeItemsVo[${s.index }].pf"
																		class="input_amouts double deductions" /></td>
																	<td><input type="text" value="${items.esi }"
																		onkeyup="calculateDeduction('${s.index }')"
																		id="esi_ded${s.index }"
																		name="employeeItemsVo[${s.index }].esi"
																		class="input_amouts double deductions" /></td>
																	<td><input type="text" value="${items.lwf }"
																		onkeyup="calculateDeduction('${s.index }')"
																		id="lwf_ded${s.index }"
																		name="employeeItemsVo[${s.index }].lwf"
																		class="input_amouts double deductions" /></td>
																	<td><input type="text" id="pt_ded${s.index }"
																		onkeyup="calculateDeduction('${s.index }')"
																		name="employeeItemsVo[${s.index }].professionalTaxs"
																		value="${items.professionalTaxs }"
																		class="input_amouts double deductions" /></td>
																	<td><input type="text" value="${items.tax }"
																		onkeyup="calculateDeduction('${s.index }')"
																		id="tax_ded${s.index }"
																		name="employeeItemsVo[${s.index }].tax"
																		class="input_amouts double deductions" /></td>
																	<td><input type="text" id="ded_ded${s.index }"
																		onkeyup="calculateDeduction('${s.index }')"
																		name="employeeItemsVo[${s.index }].deductions"
																		value="${items.deductions }"
																		class="input_amouts double deductions" /></td>
																	<td><input type="text" id="lop_ded${s.index }"
																		onkeyup="calculateDeduction('${s.index }')"
																		name="employeeItemsVo[${s.index }].lopDeduction"
																		value="${items.lopDeduction }"
																		class="input_amouts double deductions" /></td>
																	<td><input type="text" value="${items.loan }"
																		id="loan_ded${s.index }"
																		onkeyup="calculateDeduction('${s.index }')"
																		name="employeeItemsVo[${s.index }].loan"
																		class="input_amouts double deductions" /></td>
																	<td><input type="text" id="adv_ded${s.index }"
																		onkeyup="calculateDeduction('${s.index }')"
																		name="employeeItemsVo[${s.index }].advanceSalary"
																		value="${items.advanceSalary }"
																		class="input_amouts double deductions" /></td>
																	<td><input type="text" id="total_ded${s.index }"
																		onkeyup="calculateNetpay('${s.index }')"
																		name="employeeItemsVo[${s.index }].totalDeduction"
																		value="${items.totalDeduction }"
																		class="input_amouts double deductions" /></td>
																	<td><input type="text" value="${items.netPay }"
																		id="net_pay${s.index }"
																		name="employeeItemsVo[${s.index }].netPay"
																		onkeyup="calculateCTC(${s.index })"
																		class="input_amouts double" /></td>
																	<td><input type="text"
																		onkeyup="calculateCTC(${s.index })"
																		id="pf_employer${s.index }"
																		name="employeeItemsVo[${s.index }].pfEmployer"
																		value="${items.pfEmployer }"
																		class="input_amouts double" /></td>
																	<td><input type="text"
																		onkeyup="calculateCTC(${s.index })"
																		id="esi_employer${s.index }"
																		name="employeeItemsVo[${s.index }].esiEmployer"
																		value="${items.esiEmployer }"
																		class="input_amouts double" /></td>
																	<td><input type="text" value="${items.ctc }"
																		id="ctc${s.index }"
																		name="employeeItemsVo[${s.index }].ctc"
																		class="input_amouts double" /></td>
																</tr>
															</c:forEach>

														</tbody>
													</table>
												</div>
											</form:form>
										</c:if>

										<c:if test="${paySalary.daily }">
											<div class="col-xs-12 msm" style="overflow-x: auto;">
												<form:form commandName="paySalary" action="savePayDaily.do">
													<form:hidden path="date" />
													<form:hidden path="id" />
													<table class="table table-striped no-margn line">
														<tbody>
															<tr>

																<td class="hd_tb"><span class="WhiteHeading">S#</span></td>
																<td class="hd_tb"><span class="WhiteHeading">Date</span></td>
																<td class="hd_tb"><span class="WhiteHeading">Employee
																		Code </span></td>
																<td class="hd_tb"><span class="WhiteHeading">Employee
																		Name </span></td>
																<td class="hd_tb"><span class="WhiteHeading">Daily
																		Rate</span></td>
																<td class="hd_tb"><span class="WhiteHeading">Amount</span></td>
															</tr>

															<c:set var="dailyWageDate"
																value="${paySalary.dailyWageDate}"></c:set>
															<c:forEach items="${paySalary.dailyWageDetailsVos }"
																var="daily" varStatus="s">
																<tr>
																	<form:hidden
																		path="dailyWageDetailsVos[${s.index }].dailySalaryId" />
																	<td>${s.count }</td>
																	<td><form:input
																			path="dailyWageDetailsVos[${s.index }].dailyWageDate"
																			readonly="true" value="${dailyWageDate}"
																			class="input_amouts" /></td>
																	<td><form:input
																			path="dailyWageDetailsVos[${s.index }].employeeCode"
																			readonly="true" value="${daily.employeeCode }"
																			class="input_amouts" /></td>
																	<td>${daily.empName }</td>
																	<td><form:input
																			path="dailyWageDetailsVos[${s.index }].dailyRate"
																			value="${daily.dailyRate }"
																			class="input_amouts double" /></td>
																	<td><form:input
																			path="dailyWageDetailsVos[${s.index }].amount"
																			value="${daily.amount }" class="input_amouts double" /></td>
																</tr>
															</c:forEach>




														</tbody>
													</table>
												</form:form>
											</div>
										</c:if>
										<c:if test="${paySalary.hourly }">
											<div class="col-xs-12 msm" style="overflow-x: auto;">

												<form:form commandName="paySalary" action="savePayHourly.do">
													<form:hidden path="date" />
													<form:hidden path="id" />
													<table class="table table-striped no-margn line">
														<tbody>
															<tr>

																<td class="hd_tb"><span class="WhiteHeading">S#</span></td>
																<td class="hd_tb"><span class="WhiteHeading">Date</span></td>
																<td class="hd_tb"><span class="WhiteHeading">Employee
																		Code </span></td>
																<td class="hd_tb"><span class="WhiteHeading">Employee
																		Name </span></td>
																<td class="hd_tb"><span class="WhiteHeading">Total
																		Hours</span></td>
																<td class="hd_tb"><span class="WhiteHeading">Hourly
																		Rate</span></td>
																<td class="hd_tb"><span class="WhiteHeading">Amount</span></td>
															</tr>



															<c:set var="hourlyWageDate"
																value="${paySalary.hourlyWageDate}"></c:set>
															<c:forEach items="${paySalary.hourlyWagesDetailsVos }"
																var="hourly" varStatus="s">
																<tr>
																	<form:hidden
																		path="hourlyWagesDetailsVos[${s.index }].hourlySalaryId" />
																	<td>${s.count }</td>
																	<td><form:input
																			path="hourlyWagesDetailsVos[${s.index }].hourlyWageDate"
																			readonly="true" value="${hourlyWageDate }"
																			class="input_amouts" /></td>
																	<td><form:input
																			path="hourlyWagesDetailsVos[${s.index }].employeeCode"
																			readonly="true" value="${hourly .employeeCode}"
																			class="input_amouts" /></td>
																	<td><form:input
																			path="hourlyWagesDetailsVos[${s.index }].employeeName"
																			readonly="true" value="${hourly.employeeName }"
																			class="input_amouts" /></td>
																	<td><form:input
																			path="hourlyWagesDetailsVos[${s.index }].totalHours"
																			readonly="true" value="${hourly.totalHours }"
																			class="input_amouts" /></td>
																	<td><form:input
																			path="hourlyWagesDetailsVos[${s.index }].hourlyRate"
																			value="${hourly.hourlyRate }"
																			class="input_amouts double" /></td>
																	<td><form:input
																			path="hourlyWagesDetailsVos[${s.index}].amount"
																			value="${hourly.amount }" class="input_amouts double" /></td>
																</tr>

															</c:forEach>
														</tbody>
													</table>
												</form:form>
											</div>
										</c:if>



									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


	<c:if test="${! empty success_msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>

	<div id="success-msg">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>

	<div id="erro-msg">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>

	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>

	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script type="text/javascript">
		$(".input_amouts").attr("readOnly",true);
		</script>
</body>
</html>