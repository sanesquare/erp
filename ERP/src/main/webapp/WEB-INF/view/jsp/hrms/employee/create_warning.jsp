<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div id="fade"></div>
	<div id="modal">
		<img id="loader" src="resources/images/loading.gif" />
	</div>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Warning</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="Warnings.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab" id="warning-doc-tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="row">
								<form:form action="CreateWarning.do" method="post"
									commandName="warningVo" id="warningForm">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Warning Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Warning
															To </label>
														<div class="col-sm-8 col-xs-10 br-de-emp-list">
															<form:input path="employee" class="form-control"
																placeholder="Search Employee" id="employeeSl" />
															<form:hidden path="employeeCode" id="employeeCode" />
														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div>
													</div>
													<div id="pop2" class="simplePopup empl-popup-gen"></div>
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label"> Warning
															By </label>
														<div class="col-sm-9 col-xs-10 emp-pure-supr-list-div">
															<form:select path="forwaders"
																class="form-control chosen-select forwaders"
																data-placeholder="Select Superior">
																<form:options items="${superiorList}" itemValue="value"
																	itemLabel="label" />
															</form:select>
														</div>
													</div>
													<div id="pop1" class="simplePopup">
														<div class="col-md-12">
															<span class="pop_hd_wht">Select Employee</span>
															<div class="form-group">
																<div class="col-md-12  pop_hd" style="color: #000">
																	<div class="col-md-6">
																		<select class="form-control chosen-select">
																			<option></option>
																			<option value="United States">United States<span>(designing)</span></option>
																			<option value="United Kingdom">United
																				Kingdom<span>(designing)</span></option>
																			<option value="Afghanistan">Afghanistan</option>
																		</select>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-12 branch-lists"></div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Warning Date</label>
														<div class="col-sm-9">
															<div class="input-group date warning-date"
																id="datepicker">
																<form:input path="warningOn" name="warningOn"
																	class="form-control" data-date-format="DD/MM/YYYY"
																	readonly="true"
																	style="background-color: #ffffff !important;" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Subject</label>
														<div class="col-sm-9">
															<form:input path="subject" name="subject"
																class="form-control" placeholder="Subject" />
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea path="description"
																class="wysihtml form-control"
																placeholder="Enter text ..." style="height: 150px" />
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" class="form-control height" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${warningVo.recordedBy}</label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${created_on}</label>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<form:hidden path="warningId" id="warningId" />
															<button type="submit" class="btn btn-info">${buttonLabel}</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Upload Documents</div>
									<div class="panel-body">
										<%-- <form:form action="UploadWarningDocument" method="post"
											commandName="warningVo" enctype="multipart/form-data">
											<div id="warning-documents">Upload</div>
											<form:hidden path="warningId" />
										</form:form> --%>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Upload
												Documents </label>
											<div class="col-sm-9">
												<input type="file" name="warningDocs"
													id="employee-warning-docs" class="btn btn-info" multiple />
												<button type="button" onclick="uploadWarningDocument()"
													class="btn btn-info empl-warning-doc-btn">Upload</button>
												<span class="empl-warning-doc-status"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body" id="warning-document-list"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<%-- <script
		src="<c:url value='/resources/js/progress/jquery.uploadfile.min.js' />"></script>
	<script src="<c:url value='/resources/js/progress/file-upload.js' />"></script> --%>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		$(document).ready(function() {
			$('#employeeSl').bind('click', function() {
				getAllBranches();
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				if ($("#employeeCode").val() != "") {
					fillEmployeePopup($("#employeeCode").val());
					$('#pop2').simplePopup();
				}
			});
			
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
	
</body>
</html>
