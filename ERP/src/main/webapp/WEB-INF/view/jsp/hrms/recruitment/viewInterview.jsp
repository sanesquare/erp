<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/app/app.v1.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Job Interviews</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="jobInterviews.do" class="btn btn-info"
					style="float: right;"><i class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Job Interview Information</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Job
														Post </label>
													<div class="col-sm-9">
														<label for="inputEmail3"
															class="col-sm-12 control-label ft">${interview.jobPost }</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Interview
														Date & Time</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-12 control-label ft">${interview.interviewDate }
															${interview.interviewTime }</label>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Interviewers
														(Employees)</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-12 control-label ft"> <c:forEach
																items="${interview.interviewers }" var="int">
                          	${int } <br>
															</c:forEach>
														</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Status </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-12 control-label ft"> <c:forEach
																items="${interview.status }" var="sts">
	                            ${sts.status} <br>
															</c:forEach>
														</label>
													</div>
												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label">No.of
														Interviewees (Per Day)</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-12 control-label ft">${interview.selectionNumber }</label>
													</div>

												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Place of
														Interview</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-12 control-label ft">${interview.place }</label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Job Interview Description</div>
											<div class="panel-body">
												<div class="form-group">
													<div class="col-sm-12">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${interview.description }</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${interview.additionalInfo }</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft">${interview.createdBy }
														</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft">${interview.createdOn }</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<!-- <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">Upload Documents</div>
                  <div class="panel-body">
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-3 control-label">Upload Documents </label>
                      <div class="col-sm-9">
                        <input type="file" class="btn btn-info"/>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">
										<table class="table no-margn">
											<thead>
												<tr>
													<th>Document Name</th>
													<c:if
														test="${ rolesMap.roles['Job Interview Documents'].view}">
														<th>view</th>
													</c:if>

												</tr>
											</thead>
											<tbody>
												<c:forEach items="${interview.documentVos }" var="d">
													<tr>
														<td>${d.fileName }</td>
														<c:if
															test="${ rolesMap.roles['Job Interview Documents'].view}">
															<td><a
																href="downloadIntDoc.do?durl=${d.documentUrl }"><i
																	class="fa fa-file-text-o sm"></i></a></td>
														</c:if>
													</tr>
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js'/>"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js'/>"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js'/>"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js'/>"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js'/>"></script>

	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>


	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").addClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>