<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/dialog/bootstrap-dialog.min.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Holidays</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Holidays</div>
					<div class="panel-body" id="holiday-list">
						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="20%">Title</th>
									<th width="20%">Holiday Start Date</th>
									<th width="20%">Holiday End Date</th>
									<c:if test="${ rolesMap.roles['Holidays'].update}">
										<th width="15%">Edit</th>
									</c:if>
									<c:if test="${ rolesMap.roles['Holidays'].delete}">
										<th width="15%">Delete</th>
									</c:if>

								</tr>
							</thead>
							<tbody>
								<c:forEach items="${holidayList}" var="holiday">
									<tr>
										<c:choose>
											<c:when test="${ rolesMap.roles['Holidays'].delete}">
												<td><a href="ViewHoliday.do?otp_hid=${holiday.holidayId}">${holiday.title}</a></td>
												<td><a href="ViewHoliday.do?otp_hid=${holiday.holidayId}">${holiday.startDate}</a></td>
												<td><a href="ViewHoliday.do?otp_hid=${holiday.holidayId}">${holiday.endDate}</a></td>
											</c:when>
											<c:otherwise>
												<td>${holiday.title}</td>
												<td>${holiday.startDate}</td>
												<td>${holiday.endDate}</td>
											</c:otherwise>
										</c:choose>

										<c:if test="${ rolesMap.roles['Holidays'].update}">
											<td><a href="UpdateHoliday.do?otp_hid=${holiday.holidayId}"><i
													class="fa fa-edit sm"></i> </a></td>
										</c:if>
										<c:if test="${ rolesMap.roles['Holidays'].delete}">
											<td><a href="#"
												onclick="holidayDelConfirm(${holiday.holidayId})"><i
													class="fa fa-close ss"></i></a></td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<c:if test="${ rolesMap.roles['Holidays'].add}">
							<a href="CreateHoliday.do">
								<button type="button" class="btn btn-primary">Add New
									Holiday</button>
							</a>
						</c:if>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/js/dialog/bootstrap-dialog.min.js' />"></script>
	<script>
	holidayDelConfirm=function(id){
					BootstrapDialog.confirm(
							'Are you sure you want to delete this record?',
							function(result) {
								if (result) {
									deleteHoliday(id); 
								}
							});
				};
				$(document).ready(function(){
					$("#dashbordli").removeClass("active");
					$("#organzationli").removeClass("active");
					$("#recruitmentli").removeClass("active");
					$("#employeeli").removeClass("active");
					$("#timesheetli").addClass("active");
					$("#payrollli").removeClass("active");
					$("#reportsli").removeClass("active");
				});
	</script>
</body>
</html>
