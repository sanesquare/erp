<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
</head>
<body style="background: #FFFFFF !important;">

	<div class="col-xs-12" style="overflow-x: scroll" id="table1">
		<h2 class="role_hd" align="center">Salary Split Report</h2>

		<table class="table table-bordered split_width" width="100%"
			border="1" cellpadding="2" align="center">
			<tbody>
				<tr>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">S
							NO</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">Emp
							Code</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">Name</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">Department</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">Designation</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">PF
							Applicability</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">ESI
							Applicability</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">Work
							Status</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"
						colspan="${salarySplitDetails.allowancesSpan }"><span
						class="WhiteHeading">Earned Salary</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" colspan="2"><span class="WhiteHeading">LOP</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" colspan="4"><span class="WhiteHeading">Additional
							Earnings</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">Total
							Earnings</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" colspan="10"><span class="WhiteHeading">Deductions
					</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">Net
							Pay</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">PF
							Employer Contribution</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">ESI
							Employer Contribution</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split" rowspan="2"><span class="WhiteHeading">CTC</span></td>
				</tr>
				<tr>
					<c:if test="${! empty salarySplitDetails.allowanceColumns }">
						<c:forEach items="${salarySplitDetails.allowanceColumns }"
							var="cols" varStatus="colStatus">
							<td style="background-color: #0380B0; color: #fff;"
								class="hd_tb split"><span class="WhiteHeading">${cols.key }</span></td>
						</c:forEach>
					</c:if>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">Total
							Salary</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">LOP
							Count </span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">LOP
							Deduction </span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">Incentive
					</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">Bonuses
					</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">Increment
					</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">Encashment
					</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">PF </span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">ESI </span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">LWF </span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">PROF:TAX
					</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">TDS </span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">Other
							Deduction </span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">LOP
							Deduction </span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">Loan </span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">Advance
					</span></td>
					<td style="background-color: #0380B0; color: #fff;"
						class="hd_tb split"><span class="WhiteHeading">Total
							Deductions </span></td>
				</tr>
				<c:forEach items="${salarySplitDetails.detail }" var="detail1"
					varStatus="status">
					<tr>
						<td>${detail1.slno }</td>
						<td>${detail1.empCode }</td>
						<td>${detail1.empName }</td>
						<td>${detail1.department }</td>
						<td>${detail1.designation }</td>
						<td>${detail1.pFApp }</td>
						<td>${detail1.esiApp }</td>
						<td>${detail1.status }</td>

						<c:forEach items="${salarySplitDetails.allowanceColumns }"
							var="column" varStatus="colStatus1">
							<c:set var="isData" value="false" />
							<c:forEach items="${detail1.offeredSalary }" var="offered"
								varStatus="offeredSts">
								<c:set var="keys" value="${offered.key}" />

								<c:if test="${(column.key == keys) }">
									<td>${offered.value }</td>
									<c:set var="isData" value="true" />
								</c:if>
							</c:forEach>
							<c:if test="${isData == 'false'}">
								<td>0.00</td>
							</c:if>
						</c:forEach>

						<td>${detail1.totalSalary }</td>
						<td>${detail1.lopCount }</td>
						<td>${detail1.lopDeduction }</td>

						<td>${detail1.commission }</td>
						<td>${detail1.bonuses }</td>
						<td>${detail1.increment }</td>
						<td>${detail1.encashment }</td>
						<td>${detail1.totalEarnings }</td>

						<td>${detail1.pf }</td>
						<td>${detail1.esi }</td>
						<td>${detail1.lwf }</td>
						<td>${detail1.proffTax }</td>
						<td>${detail1.tax }</td>
						<td>${detail1.decrement }</td>
						<td>${detail1.lopDeduction }</td>
						<td>${detail1.loan }</td>
						<td>${detail1.advance }</td>
						<td>${detail1.totalDeduction }</td>
						<td>${detail1.netPay }</td>
						<td>${detail1.pfEmployerContribution }</td>
						<td>${detail1.esiEmployerContribution }</td>
						<td>${detail1.ctc }</td>
					</tr>
				</c:forEach>

			</tbody>
		</table>
	</div>
	<div class="col-xs-12">
		<div class="form-group" style="margin-top: 55px;">
			<div class="col-sm-offset-4 col-sm-6">
				<button class="btn btn-info"
					onclick="window.open('data:application/vnd.ms-excel,' + document.getElementById('table1').outerHTML.replace(/ /g, '%20'));">
					Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
				</button>
				<!-- <button type="submit" class="btn btn-info" name="type" value="html">
					Export to HTML &nbsp; <i class="fa fa-file-code-o"></i>
				</button> -->
				<!-- <button type="submit" class="btn btn-info" name="type" value="csv">
					Export to CSV &nbsp; <i class="fa fa-file-text-o"></i>
				</button>
				<button type="submit" class="btn btn-info" name="type" value="xls">
					Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
				</button>
				<button type="submit" class="btn btn-info" name="type" value="pdf">
					Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
				</button> -->

			</div>
		</div>
	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>
</html>