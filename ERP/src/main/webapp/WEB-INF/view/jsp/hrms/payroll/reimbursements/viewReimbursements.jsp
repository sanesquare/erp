<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="com.hrms.web.constants.StatusConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Reimbursements</h1>
		</div>
		<input type="hidden" value="<%=StatusConstants.FORWARD%>" id="sttuss" />
		<div class="row">
			<div class="col-md-12">
				<a href="reimbursementsHome.do" class="btn btn-info"
					style="float: right;"><i class="fa fa-arrow-left"></i></a>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<!-- 	<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
 -->

				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="row">


								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Overtime Information</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label"> Employee
													Name</label>
												<div class="col-sm-9">
													<label for="inputPassword3" class="col-sm-9 control-label">${reimbursementsVo.employee}
													</label>
												</div>

											</div>





											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label"> Forward
													Application To</label>
												<div class="col-sm-9">
													<label for="inputPassword3" class="col-sm-9 control-label"><c:forEach
															items="${reimbursementsVo.superiorName }" var="s">
                                  ${s },
                                  </c:forEach> </label>
												</div>

											</div>




											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">
													Title</label>
												<div class="col-sm-9">
													<label for="inputPassword3" class="col-sm-9 control-label">${reimbursementsVo.title }
													</label>
												</div>
											</div>

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">
													Amount</label>
												<div class="col-sm-9">
													<label for="inputPassword3" class="col-sm-9 control-label">${reimbursementsVo.amount }
													</label>
												</div>
											</div>


											<div class="form-group">
												<label class="col-sm-3 control-label"> Date</label>
												<div class="col-sm-9">
													<label for="inputPassword3" class="col-sm-9 control-label">${reimbursementsVo.date }</label>
												</div>
											</div>



										</div>
									</div>
								</div>




								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Overtime Description</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-md-12">
													<label for="inputPassword3"
														class="col-sm-12 control-label ft">${reimbursementsVo.description }</label>
												</div>
											</div>


										</div>
									</div>
								</div>



							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Reimbursement Items</div>
										<div class="panel-body">
											<c:if test="${! empty reimbursementsVo.itemsVos }">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%">#</th>
															<th width="20%">Date</th>
															<th width="10%">Category</th>
															<th width="35%">Item</th>
															<th width="10%">Receipt #</th>
															<th width="10%">Country</th>
															<th width="10%">Amount</th>

														</tr>
													</thead>
													<tbody>

														<c:forEach items="${reimbursementsVo.itemsVos }" var="i"
															varStatus="s">
															<td>${s.count }</td>
															<td>${i.date }</td>
															<td>${i.category }</td>
															<td>${i.item }</td>
															<td>${i.receipt }</td>
															<td>${i.country }</td>
															<td>${i.amount }</td>
														</c:forEach>


													</tbody>
												</table>
											</c:if>
										</div>
									</div>
								</div>
							</div>





							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Status</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Status
													</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${reimbursementsVo.status }</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Description
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${reimbursementsVo.statusDescription }</label>
													</div>
												</div>


											</div>
										</div>
									</div>

									<c:if test="${reimbursementsVo.viewStatus}">
										<div class="col-md-12">
											<form:form commandName="reimbursementsVo"
												action="updateReimbursementsStatus" method="POST">
												<form:hidden path="reimbursementId" />
												<div class="panel panel-default">
													<div class="panel-heading">Status</div>
													<div class="panel-body">


														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Status </label>
															<div class="col-sm-9">
																<form:select path="status" id="reimbursement_status"
																	cssClass="form-control">
																	<option value="">--Status--</option>
																	<form:options items="${status }" itemLabel="status"
																		itemValue="status" />
																</form:select>
															</div>
														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Description </label>
															<div class="col-sm-9">
																<form:textarea path="statusDescription"
																	cssClass="form-control height" />
															</div>
														</div>
														<div class="form-group" id="show_forward_to">
															<label for="inputEmail3" class="col-sm-3 control-label">
																Forward Application To</label>
															<div class="col-sm-9">

																<form:select cssClass="form-control chosen-select"
																	path="superiorCodes"
																	id="reimbursements_superiors_status" multiple="true">
																	<form:options items="${superiorList }"
																		itemLabel="label" itemValue="value" />
																</form:select>
																<form:hidden path="superiorAjaxIds"
																	id="reimbursements_suprior_ajx_hidn" />
															</div>

														</div>

														<div class="form-group">
															<div class="col-sm-offset-3 col-sm-9">
																<button type="submit" class="btn btn-info">Save</button>
															</div>
														</div>
													</div>
												</div>
											</form:form>
										</div>
									</c:if>


								</div>

								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${reimbursementsVo.notes}</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${reimbursementsVo.createdBy}
														</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${reimbursementsVo.createdOn}
														</label>
													</div>
												</div>




											</div>
										</div>
									</div>
									<%-- 	<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Show Documents</div>
											<div class="panel-body">


												<c:if test="${! empty reimbursementsVo.documentVos }">
													<table class="table no-margn">
														<thead>
															<tr>
																<th>#</th>
																<th>Document Name</th>
																<th>view</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${ reimbursementsVo.documentVos }"
																var="d" varStatus="s">
																<tr>
																	<td>${s.count }</td>
																	<td><i class="fa fa-file-o"></i>${d.fileName }</td>
																	<td><a
																		href="downloadReimbursementDocument?url=${d.url }"><i
																			class="fa fa-file-text-o sm"></i></a></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>

												</c:if>


											</div>
										</div>
									</div> --%>
								</div>
							</div>





						</div>
					</div>


					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">



							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">


										<c:if test="${! empty reimbursementsVo.documentVos }">
											<table class="table no-margn">
												<thead>
													<tr>
														<th>#</th>
														<th>Document Name</th>
														<c:if
															test="${ rolesMap.roles['Reimbursements Documents'].view}">
															<th>view</th>
														</c:if>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${ reimbursementsVo.documentVos }"
														var="d" varStatus="s">
														<tr>
															<td>${s.count }</td>
															<td><i class="fa fa-file-o"></i>${d.fileName }</td>
															<c:if
																test="${ rolesMap.roles['Reimbursements Documents'].view}">
																<td><a
																	href="downloadReimbursementDocument.do?url=${d.url }"><i
																		class="fa fa-file-text-o sm"></i></a></td>
															</c:if>
														</tr>
													</c:forEach>
												</tbody>
											</table>

										</c:if>


									</div>
								</div>
							</div>




						</div>
					</div>



				</div>
			</div>
		</div>
	</div>

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#reimbursements_superiors_status").val('');

			var status = $("#sttuss").val();

			if ($("#reimbursement_status").val() == status)
				$("#show_forward_to").css("display", 'block');
			else
				$("#show_forward_to").css("display", 'none');

			$("#reimbursement_status").on("change", function() {
				if ($("#reimbursement_status").val() == status)
					$("#show_forward_to").css("display", 'block');
				else
					$("#show_forward_to").css("display", 'none');
			})

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>