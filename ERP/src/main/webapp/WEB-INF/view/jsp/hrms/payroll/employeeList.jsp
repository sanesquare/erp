<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<a href="#" onclick="getAllBranchDepartments(${branch})">Back to
	department</a>

<ul class="select_whole">
	<c:forEach items="${employeeList}" var="employee">
		<a href="#"
			onclick="selectThisEmployee('${employee.employeeCode}', '${employee.name}')"><li>${employee.name}</li></a>
	</c:forEach>
</ul>

<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

