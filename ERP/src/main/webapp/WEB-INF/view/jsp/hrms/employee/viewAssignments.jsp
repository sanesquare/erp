<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Assignments</h1>
		</div>



		<div class="row">
			<div class="col-md-12">
				<a href="assignments.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>

		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>

				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Assignment Information</div>
											<div class="panel-body">


												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3 control-label">Assigned
														To</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${assignmentVo.employee }</label>
													</div>


												</div>


												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3 ">Project</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${assignmentVo.projectTitle }</label>
													</div>


												</div>




												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Assignment
														Name </label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${assignmentVo. assignmentName}</label>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label"> Start Date</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${assignmentVo.startDate }</label>
													</div>

												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label"> End Date</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${assignmentVo.endDate }</label>
													</div>

												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label"> Priority</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${assignmentVo. priority}</label>
													</div>

												</div>






											</div>
										</div>
									</div>


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Assignment Description</div>
											<div class="panel-body">


												<div class="form-group">

													<div class="col-sm-12">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${assignmentVo.description }</label>
													</div>
												</div>




											</div>
										</div>
									</div>


								</div>

								<div class="col-md-6">


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${assignmentVo.notes }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft">${assignmentVo.createdBy }</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft">${assignmentVo.createdOn }</label>
													</div>
												</div>





											</div>
										</div>
									</div>

								</div>
							</div>



						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">



							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">

										<c:if test="${!empty assignmentVo.documentsVos}">
											<table class="table no-margn">
												<thead>
													<tr>
													<th>#</th>
														<th>Document Name</th>
														<c:if test="${ rolesMap.roles['Assignment Documents'].view}">
														<th>view</th>
														</c:if>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${assignmentVo.documentsVos}" var="d" varStatus="s">
														<tr>
														<td>${s.count }</td>
															<td>${d.fileName }</td>
															<c:if test="${ rolesMap.roles['Assignment Documents'].view}">
															<td><a href="downloadAssignmentDoc.do?url=${d.url }"><i
																	class="fa fa-file-text-o sm"></i></a></td>
																	</c:if>
														</tr>
													</c:forEach>

												</tbody>
											</table>
										</c:if>

									</div>
								</div>
							</div>




						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Simple Map</div>
									<div class="panel-body">
										<div id="simpleMap" style="height: 500px;"></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->



	</section>
	<!-- Content Block Ends Here (right box)-->



	<!-- JQuery v1.9.1 -->
	<script src="assets/js/jquery/jquery-1.9.1.min.js"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script src="assets/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- TypeaHead -->
	<script src="assets/js/plugins/typehead/typeahead.bundle.js"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script src="assets/js/plugins/inputmask/jquery.inputmask.bundle.js"></script>

	<!-- TagsInput -->
	<script
		src="assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

	<!-- Chosen -->
	<script src="assets/js/plugins/bootstrap-chosen/chosen.jquery.js"></script>

	<!-- moment -->
	<script src="assets/js/moment/moment.js"></script>
	<!-- DateTime Picker -->
	<script
		src="assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>

	<!-- Wysihtml5 -->
	<script
		src="assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
	<script
		src="assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script>

	<!-- Custom JQuery -->
	<script src="assets/js/app/custom.js" type="text/javascript"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script src="assets/js/jquery.simplePopup.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>