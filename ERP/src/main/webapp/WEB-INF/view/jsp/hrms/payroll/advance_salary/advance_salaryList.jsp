<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width="20%">Employee Name</th>
			<th width="20%">Title</th>
			<th width="20%">Amount</th>
			<th width="20%">Date</th>
			<th width="10%">Edit</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${advanceSalaryList}" var="advanceSalary">
			<tr>
				<td><c:choose>
						<c:when test="${advanceSalary.recordedBy==thisUser}">
							<i class="fa fa-arrow-up up-arrow"></i>
						</c:when>
						<c:otherwise>
							<i class="fa fa-arrow-down down-arrow"></i>
						</c:otherwise>
					</c:choose><a href="ViewAdvanceSalary.do?otp=${advanceSalary.advanceSalaryId}">${advanceSalary.employee}</a></td>
				<td><a
					href="ViewAdvanceSalary.do?otp=${advanceSalary.advanceSalaryId}">${advanceSalary.advanceSalaryTitle}</a></td>
				<td><a
					href="ViewAdvanceSalary.do?otp=${advanceSalary.advanceSalaryId}">${advanceSalary.advanceAmount}</a></td>
				<td><a
					href="ViewAdvanceSalary.do?otp=${advanceSalary.advanceSalaryId}">${advanceSalary.requestedDate}</a></td>
				<td><a
					href="EditAdvanceSalary.do?otp=${advanceSalary.advanceSalaryId}"><i
						class="fa fa-edit sm"></i> </a></td>
				<td><a href="#"
					onclick="advanceSalaryDelConfirm(${advanceSalary.advanceSalaryId})"><i
						class="fa fa-close ss"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<a href="AddAdvanceSalary.do">
	<button type="button" class="btn btn-primary">Add New Advance
		Salary</button>
</a>

<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

