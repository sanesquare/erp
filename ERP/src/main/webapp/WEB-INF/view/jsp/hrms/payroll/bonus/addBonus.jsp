<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Bonuses</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="bonuses.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>


		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>


				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form commandName="bonusVo" id="addBonusForm" method="POST"
								action="saveBonus.do">
								<form:hidden path="bonusId" id="bonus_bonusid" />
								<form:hidden path="id" id="bonus_hidden_id" />
								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Bonus Information</div>
												<div class="panel-body">


													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Employee
															Name <span class="stars">*</span></label>
														<div class="col-sm-8 col-xs-10 br-de-emp-list">
															<form:input path="employee" class="form-control"
																placeholder="Search Employee" id="employeeSl" />
															<form:hidden path="employeeCode" id="employeeCode" />

														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div>
													</div>

													<div id="pop1" class="simplePopup">
													</div>

													<div id="pop2" class="simplePopup empl-popup-gen"></div>



													<div class="form-group">
														<label for="inputPassword3"
															class="col-sm-3 col-xs-12 control-label"> Title <span class="stars">*</span></label>
														<div class="col-sm-8 col-xs-10">
															<form:select path="bonusTitleId" cssClass="form-control"
																id="bonus_title">
																<option value="">--Select Bonus--</option>
																<form:options id="bonus_title" itemLabel="title"
																	items="${title }" itemValue="id" />
															</form:select>
														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show3"><i
																class="fa fa-plus-square-o font_size"></i></a>
														</div>

													</div>




													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Amount <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="amount" cssClass="form-control double"
																id="bonus_amount" />
														</div>
													</div>





													<div class="form-group">
														<label class="col-sm-3 control-label"> Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date deduction-date"
																id="datepicker">
																<form:input path="date" cssClass="form-control"
																	style="background-color: #ffffff !important;"
																	id="bonus_date" readonly="true"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>

													</div>




												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Bonus Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">


															<form:textarea path="description" id="bonus_description"
																cssClass=" form-control"
																style="height: 150px;resize:none" />




														</div>
													</div>





												</div>
											</div>
										</div>




									</div>

									<div class="col-md-6">


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" id="bonus_notes"
																cssClass="form-control height" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${bonusVo.creatdBy }</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${bonusVo.createdOn }</label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								</div>
							</form:form>

							<div id="pop3" class="simplePopup">
								<div class="col-md-12">
									<div class="form-group">
										<h1 class="pop_hd">Bonus</h1>

										<div class="row">
											<form:form method="POST" commandName="bonusTitleVo"
												action="saveBonusTitle.do">

												<div class="row">
													<div class="col-md-12">

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Available Bonuses
															</label>
															<div class="col-sm-9" id="avai_bonus_titles"></div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">New Bonuses </label>
															<div class="col-sm-9">
																<form:textarea path="title" id="bonus_title_txt"
																	cssClass="form-control height" />
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-9">
																<button type="button" id="bonus_title_save_btn"
																	class="btn btn-info">Save</button>
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-offset-3 col-sm-9">
																<span id="bonus_title_msg" class="hideMe imsuccess"></span>
															</div>
														</div>
													</div>
												</div>
											</form:form>
										</div>
									</div>


								</div>


							</div>

						</div>
					</div>




				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->


	<!-- Content Block Ends Here (right box)-->


	<script src="<c:url value='/resources/assets/js/settings-ajax.js' />"></script>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#employeeSl').bind('click', function() {
				showPop();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			getEmployeeInformation($("#employeeCode").val());

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show3').click(function() {
				getAvailableBonusTitles();
				$('#pop3').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
			
			$("#chosen-employee").change(
					function() {
						$("#bonus_amount").val('');
						$("#employeeSl").val(
								$("#chosen-employee option:selected")
										.text());
						$("#employeeCode").val(
								$("#chosen-employee").val());
						getBonusAmount($("#chosen-employee").val());
						closePopup();
					});
		});
	</script>

</body>
</html>