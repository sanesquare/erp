<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
	<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div id="fade"></div>
	<div id="modal">
		<img id="loader" src="resources/images/loading.gif" />
	</div>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Announcement</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="announcements.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<!-- <div class="row">
			<div class="col-md-12">
				<div class="success_msg" id="add_announcements_succs">
					<span id="msgs_icn"><i class="fa fa-check"></i></span>
				</div>
				<div class="error_msg" id="add_announcements_err">
					<span id="msge_icn"><i class="fa fa-times"></i></span>
				</div>
			</div>
		</div> -->
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab" id="announcement-doc-tab">Documents</a></li>
					<li role="presentation"><a href="#docs" role="tab"
						data-toggle="tab"> Status</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="row">
								<form:form action="saveAnnouncements.do" method="post"
									commandName="announcementsVo" id="announcementForm">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Announcement Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Title <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="announcementsTitle"
																cssClass="form-control" id="add_announcements_title" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Send by e-mail</label>
														<div class="col-sm-9">
															<form:checkbox path="sendMail"
																id="add_announcements_sendMail" />
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Announcement Dates</div>
												<div class="panel-body">
													<div class="form-group">
														<label class="col-sm-3 control-label">Announcement
															Start Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class='input-group date leave-from-date' id="datepicker">
																<form:input path="startDate" cssClass="form-control"
																	data-date-format="DD/MM/YYYY"
																	id="add_announcements_startDate" readonly="true"
																	style="background-color: #ffffff !important;" />

																<span class="input-group-addon"><span
																	class="glyphicon glyphicon-calendar"></span> </span>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Announcement
															End Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class='input-group date leave-to-date' id="datepickers">
																<form:input path="endDate" cssClass="form-control"
																	data-date-format="DD/MM/YYYY"
																	id="add_announcements_endDate" readonly="true"
																	style="background-color: #ffffff !important;" />
																<span class="input-group-addon"><span
																	class="glyphicon glyphicon-calendar"></span> </span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Message</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea path="message" cssClass="form-control"
																style="height: 150px" id="add_announcements_message" />
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes"
																cssClass="form-control height"
																id="add_announcements_notes" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${announcementsVo.createdBy}</label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${created_on}</label>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<form:hidden path="announcementsId" id="announcementsId" />
															<button type="submit" class="btn btn-info">${buttonLabel}</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form:form>
								<%-- <form action="#" id="booking">
								
								<input type="text" name="firstname">
								<button type="submit">Submit</button>
								</form> --%>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
						<%-- <c:if test="${ rolesMap.roles['Announcements'].add}"> --%>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Upload Documents</div>
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Upload
												Documents </label>
											<div class="col-sm-9">
												<input type="file" name="announcementDocs"
													id="announcements-docs" class="btn btn-info" multiple />
												<button type="button" onclick="uploadAnnouncementDocument()"
													class="btn btn-info announcement-doc-btn">Upload</button>
												<span class="announcement-doc-status"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<%-- </c:if> --%>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body" id="announcement-document-list">
										<!-- <table class="table no-margn">
											<thead>
												<tr>
													<th>#</th>
													<th>Document Name</th>
													<th>view</th>
													<th>Delete</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td><i class="fa fa-file-o"></i> Lorem ipsum dolor sit
														amet</td>
													<td><i class="fa fa-file-text-o sm"></i></td>
													<td><i class="fa fa-close ss"></i></td>
												</tr>
											</tbody>
										</table> -->
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="docs">
						<div class="panel-body">
							<form:form action="saveAnnouncements.do" method="post"
								commandName="announcementsVo" id="announcementForm">
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Status</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">
													Status </label>
												<div class="col-sm-9">
													<form:select id="selectbox" cssClass="form-control"
														path="announcementsStatus">
														<c:forEach items="${status}" var="status">
															<form:option value="${status.announcementsStatusId }">${status.announcementsStatus }</form:option>
														</c:forEach>
													</form:select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Notes
												</label>
												<div class="col-sm-9">
													<form:textarea class="form-control height"
														path="announcementsStatusNotes"
														id="add_announcements_status_notes" />
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-9">
													<form:hidden path="announcementsId" id="announcementsId" />
													<button type="submit" class="btn btn-info">Update
														Status</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- <div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Status</div>
										<div class="panel-body">
											<table class="table no-margn">
												<thead>
													<tr>
														<th>#S</th>
														<th>Date/Time</th>
														<th>Updated By</th>
														<th>Stasus</th>
														<th>Comments</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>2-2-2015 12:20</td>
														<td>Amal</td>
														<td>Inactive</td>
														<td>Lorem ipsum dolor sit amet 25-2-2015 22:30 pm</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div> -->
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Warper Ends Here (working area) -->
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js" type="text/javascript' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});
			var announcementsTitle = $("#add_announcements_title").val();
			
			$("#dashbordli").removeClass("active");
			$("#organzationli").addClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});

		function displayNotesStatus(notes) {
			$("#display_notes_here").val(notes);

		}
		
		function changeFunc() {
			var selectBox = document.getElementById("selectbox");
			var selectedValue = selectBox.options[selectBox.selectedIndex].value;
			document.getElementById("display_notes_here").value = selectedValue;

		}
	</script>
</body>
</html>