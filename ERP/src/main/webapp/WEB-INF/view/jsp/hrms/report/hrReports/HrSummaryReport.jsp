<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />


</head>
<body style="background: #FFFFFF !important;">

	<div class="col-xs-12" id="table1">
		<h2 class="role_hd">HR Summary Report</h2>
		<%-- <form:form action="exportHrSummary" method="post"
			modelAttribute="summaryDetails"> --%>
		<%-- <c:if test="${! empty summaryDetails }"> --%>
		<div class="row">
			<div class="col-md-6">
				<div class="col-md-12">
					<table class="table table-striped no-margn line">

						<tr>
							<td class="hd_tb" colspan="2" style="background-color:#0380B0;color: #fff;"><span class="WhiteHeading">HR
									Summary</span></td>
						</tr>

						<tr>
							<th colspan="2"><strong>Statistics</strong></th>
						</tr>
						<tr>
							<td>Number of Branches</td>
							<td>${summaryDetails.summaryDetails.branchNo }<%-- <form:hidden
										path="summaryDetails.branchNo"
										value="${summaryDetails.summaryDetails.branchNo }" /> --%></td>
						</tr>

						<tr>
							<td>Number of Departments</td>
							<td>${summaryDetails.summaryDetails.depNo }</td>
						</tr>
						<tr>
							<td>Number of Projects</td>
							<td>${summaryDetails.summaryDetails.projectNo }</td>
						</tr>

						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>

						<tr>
							<th colspan="2"><strong>Employees</strong></th>
						</tr>

						<tr>
							<td>Total Employees</td>
							<td>${summaryDetails.summaryDetails.totalEmp }</td>
						</tr>

						<tr>
							<td>Active Employees</td>
							<td>${summaryDetails.summaryDetails.activeEmp }</td>
						</tr>

						<tr>
							<td>Inactive Employees</td>
							<td>${summaryDetails.summaryDetails.inactiveEmp }</td>
						</tr>


						<tr>
							<td>Male Employees</td>
							<td>${summaryDetails.summaryDetails.maleEmp }</td>
						</tr>


						<tr>
							<td>Female Employees</td>
							<td>${summaryDetails.summaryDetails.femaleEmp }</td>
						</tr>
						<c:if test="${! empty summaryDetails.summaryDetails.empType }">
							<tr>
								<th colspan="2"><strong>Employees by Employee
										Types</strong></th>
							</tr>
							<c:forEach items="${summaryDetails.summaryDetails.empType }"
								var="types">
								<tr>
									<td>${types.type }</td>
									<td>${types.number }</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${! empty summaryDetails.summaryDetails.empCategory }">
							<tr>
								<th colspan="2"><strong>Employees by Employee
										Categories</strong></th>
							</tr>
							<c:forEach items="${summaryDetails.summaryDetails.empCategory }"
								var="cate">
								<tr>
									<td>${cate.category }</td>
									<td>${cate.number }</td>
								</tr>
							</c:forEach>
						</c:if>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<c:if test="${! empty summaryDetails.branchDetails}">
					<div class="col-md-12">

						<table class="table table-striped no-margn line">
							<tbody>
								<tr>
									<td class="hd_tb" colspan="2" style="background-color:#0380B0;color: #fff;"><span class="WhiteHeading">Employees
											by Branch</span></td>
								</tr>
								<c:forEach items="${summaryDetails.branchDetails}" var="branch"
									varStatus="status">
									<tr>
										<td>${branch.branch }<%-- <form:hidden
													path="branchDetails[${status.index }].branch" /> --%></td>
										<td>${branch.branchEmp }<%-- <form:hidden
													path="branchDetails[${status.index }].branchEmp" /> --%></td>
									</tr>

								</c:forEach>
							</tbody>
						</table>

					</div>
				</c:if>
				<div class="col-md-12">&nbsp;</div>

				<c:if test="${! empty summaryDetails.departmentDetails}">
					<div class="col-md-12">
						<table class="table table-striped no-margn line">
							<tbody>
								<tr>
									<td class="hd_tb" colspan="2" style="background-color:#0380B0;color: #fff;"><span class="WhiteHeading">Employees
											by Departments</span></td>
								</tr>
								<c:forEach items="${summaryDetails.departmentDetails}" var="dep"
									varStatus="status1">
									<tr>
										<td>${dep.department }<%-- <form:hidden
													path="departmentDetails[${status1.index}].department" /> --%></td>
										<td>${dep.departmentEmp }<%-- <form:hidden
													path="departmentDetails[${status1.index }].departmentEmp" /> --%></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>

					</div>
				</c:if>
				<c:if test="${! empty summaryDetails.designationDetails}">
					<div class="col-md-12">
						<table class="table table-striped no-margn line">
							<tbody>
								<tr>
									<td class="hd_tb" colspan="2" style="background-color:#0380B0;color: #fff;"><span class="WhiteHeading">Employees
											by Designations</span></td>
								</tr>
								<c:forEach items="${summaryDetails.designationDetails}"
									var="des">
									<tr>
										<td>${des.designation }</td>
										<td>${des.designationEmp }</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>
				<div class="col-md-12">&nbsp;</div>

				<c:if test="${! empty summaryDetails.ageGroupDetails}">
					<div class="col-md-12">
						<table class="table table-striped no-margn line">
							<tbody>
								<tr>
									<td class="hd_tb" colspan="2" style="background-color:#0380B0;color: #fff;"><span class="WhiteHeading">Age
											Groups </span></td>
								</tr>
								<c:forEach items="${ summaryDetails.ageGroupDetails}" var="age">
									<tr>
										<td>${age.ageGroup }</td>
										<td>${age.ageEmp }</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>

					</div>
				</c:if>

				<div class="col-md-12">&nbsp;</div>
			</div>
		</div>
		<%-- </c:if> --%>
		<%-- </form:form> --%>
	</div>
	<div class="form-group" style="margin-top: 25px;">
		<div class="col-sm-offset-4 col-sm-6">
			<button class="btn btn-info"
				onclick="window.open('data:application/vnd.ms-excel,' + document.getElementById('table1').outerHTML.replace(/ /g, '%20'));">
				Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
			</button>
		</div>
	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>
	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<!-- Responsive tabs -->
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"
		type="text/javascript"></script>

	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</html>