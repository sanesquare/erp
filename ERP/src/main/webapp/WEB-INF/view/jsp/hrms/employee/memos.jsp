<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Memos</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Memos</div>
					<div class="panel-body" id="memo-list">
						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="20%">Memo From</th>
									<th width="20%">Subject</th>
									<th width="20%">Memo Date</th>
									<th width="20%">Edit</th>
									<th width="20%">Delete</th>

								</tr>
							</thead>
							<tbody>
								<c:forEach items="${memoList}" var="memo">
									<tr>
										<td><a href="ViewMemoInformation.do?otp_mid=${memo.memoId}">${memo.employee}</a></td>
										<td><a href="ViewMemoInformation.do?otp_mid=${memo.memoId}">${memo.memoSubject}</a></td>
										<td><a href="ViewMemoInformation.do?otp_mid=${memo.memoId}">${memo.memoOn}</a></td>
										<td><a
											href="UpdateMemoInformation.do?otp_mid=${memo.memoId}"><i
												class="fa fa-edit sm"></i> </a></td>
										<td><a href="#" onclick="deleteThisMemo(${memo.memoId})"><i
												class="fa fa-close ss"></i></a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<a href="MemoInformation.do">
							<button type="button" class="btn btn-primary">Add New
								Memo</button>
						</a>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		$(document).ready(function() {
			/* $('.show1').click(function() {
				$('#pop1').simplePopup();
			});
			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});
			$('.show3').click(function() {
				$('#pop3').simplePopup();
			});
			$('.show4').click(function() {
				$('#pop4').simplePopup();
			});
			$('.show5').click(function() {
				$('#pop5').simplePopup();
			});
			$('.show6').click(function() {
				$('#pop6').simplePopup();
			});
			$('.show7').click(function() {
				$('#pop7').simplePopup();
			});
			$('.show8').click(function() {
				$('#pop8').simplePopup();
			});
			$('.show9').click(function() {
				$('#pop9').simplePopup();
			});
			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});
			$('.show10').click(function() {
				$('#pop10').simplePopup();
			});
			$('.show11').click(function() {
				$('#pop11').simplePopup();
			});
			$('.show12').click(function() {
				$('#pop12').simplePopup();
			});
			$('.show13').click(function() {
				$('#pop13').simplePopup();
			});
			$('.show14').click(function() {
				$('#pop14').simplePopup();
			});
			$('.show15').click(function() {
				$('#pop15').simplePopup();
			});
			$('.show16').click(function() {
				$('#pop16').simplePopup();
			});
			$('.show17').click(function() {
				$('#pop17').simplePopup();
			}); */
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
	<script>
		$('#js-smart-tabs').smartTabs();
		$('#js-smart-tabs--tabs').smartTabs({
			layout : 'tabs'
		});
		$('#js-smart-tabs--accordion').smartTabs({
			layout : 'accordion'
		});
	</script>
	<script>
		$(document).ready(function() {

			$("#admin_ss").click(function() {
				$("#add_administartor").show();
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addfield").click(function() {
				$("#add_custom_feild").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>
	<!-- <script>
		$(document).ready(function() {
			$("#addnot").click(function() {
				$("#add_notification").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
				//
				$.ajax({
					url: "addNotification",
					type: "GET",
					success: function(data) {
						$("#div_notification_form").html(data);
					}
				});
			});
		});
	</script> -->
	<script>
		$(document).ready(function() {
			$("#addrm").click(function() {
				$("#add_rm").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>
</body>
</html>
