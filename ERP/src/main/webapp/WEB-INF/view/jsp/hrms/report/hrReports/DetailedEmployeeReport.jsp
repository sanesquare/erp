<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
</head>

<body style="background: #FFFFFF !important;">
	<div class="col-xs-12" style="max-height:700px; overflow-x: scroll" id="table1">
		<h2 class="role_hd">Employee List</h2>

		<%-- <form:form id="mainForm" action="pdfRequest"
			modelAttribute="employeeDetails" method="post"> --%>
		<c:if test="${! empty employeeDetails}">
			<table class="table table-bordered split_width" width="100%"
				border="1" cellpadding="2" align="center">
				<tbody >
					<tr>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">S#</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Employee
								Code</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Company</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">User
								Name</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">First
								Name</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Second
								Name</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Password</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Branch</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Department</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Designation</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Grade</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">DOB</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Gender</span></td>
						<!-- <td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Marital
								Status</span></td> -->
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Religion</span></td>
						<!-- <td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Father's
								Name</span></td> -->
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Permanent
								Address</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Contact
								Address</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Phone</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Mobile</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Contact
								<br>In Case of <br>Emergency
						</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Email
								Address</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Gross
								Salary</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Salary
								Increment<br> Dates
						</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Bank</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Account
								Number</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">PF
								Number</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">ESIC
								Number</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">LWF
								Number</span></td>
						<!-- <td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Birth
								Place</span></td> -->
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Blood
								Group</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">PAN
								Number</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Report
								To</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Join
								Date</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Resign
								Date</span></td>
						<td style="background-color: #0380B0; color: #fff;"
							class="hd_tb split"><span class="WhiteHeading">Status</span></td>
					</tr>
					<c:forEach items="${employeeDetails.detailedVos}" var="emp"
						varStatus="status">
						<tr>
							<td>${emp.slno}</td>
							<td>${emp.empCode}</td>
							<td>${emp.company}</td>
							<td>${emp.userName}</td>
							<td>${emp.firstName}</td>
							<td>${emp.lastName}</td>
							<td>${emp.password}</td>
							<td>${emp.branch}</td>
							<td>${emp.department}</td>
							<td>${emp.designation}</td>
							<td>${emp.grade}</td>
							<td>${emp.dob}</td>
							<td>${emp.gender}</td>
							<%-- <td>${emp.maritalStatus}</td> --%>
							<td>${emp.relegion}</td>
							<%-- <td>${emp.fatherName}</td> --%>
							<td>${emp.permanentAddress}</td>
							<td>${emp.contactAddress}</td>
							<td>${emp.phone}</td>
							<td>${emp.mobile}</td>
							<td>${emp.emergency}</td>
							<td>${emp.email}</td>
							<td>${emp.grossSalary}</td>
							<td>${emp.incrementDates}</td>
							<td>${emp.bank}</td>
							<td>${emp.accountNumber}</td>
							<td>${emp.pfNumber}</td>
							<td>${emp.esicNumber}</td>
							<td>${emp.lwfNumber}</td>
							<%-- <td>${emp.birthPlace}</td> --%>
							<td>${emp.bloodGroup}</td>
							<td>${emp.panNumber}</td>
							<td>${emp.reportTo}</td>
							<td>${emp.joinDate}</td>
							<td>${emp.resignDate}</td>
							<td>${emp.status}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
		<%-- </form:form> --%>
	</div>

	<div class="form-group" style="margin-top: 25px;">
		<div class="col-sm-offset-4 col-sm-6">
			<button class="btn btn-info"
				onclick="window.open('data:application/vnd.ms-excel,' + document.getElementById('table1').outerHTML.replace(/ /g, '%20'));">
				Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
			</button>
			<!-- <button type="submit" class="btn btn-info" name="type" value="xls">
				Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
			</button>
			<button type="submit" class="btn btn-info" name="type" value="csv">
				Export to CSV &nbsp; <i class="fa fa-file-text-o"></i>
			</button>
			<button type="submit" class="btn btn-info" name="type" value="pdf">
				Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
			</button> -->
		</div>
	</div>

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>
	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<!-- Responsive tabs -->
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"
		type="text/javascript"></script>

	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</html>