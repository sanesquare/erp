<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Time Sheet Reports</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="parentVerticalTab">
					<ul class="resp-tabs-list hor_1">
						<c:if test="${ rolesMap.roles['Work Sheet Reports'].view}">
							<li>Work Sheets</li>
						</c:if>
						<c:if test="${ rolesMap.roles['Employee Work Sheets'].view}">
							<li>Employees Work Sheets</li>

						</c:if>
						<c:if test="${ rolesMap.roles['Employee Time Sheet'].view}">
							<li>Employee Time Sheet</li>
						</c:if>
						<c:if
							test="${ rolesMap.roles['Employee Attendance Reports'].view}">
							<li>Employee Attendance</li>
						</c:if>
						<c:if test="${ rolesMap.roles['Absent Employees'].view}">
							<li>Absent Employees</li>
						</c:if>
						<c:if test="${ rolesMap.roles['Employee Leave Summary'].view}">
							<li>Employee Leaves Summary</li>
						</c:if>
						<!-- <li>Leaves</li>
						<li>Half Days Leaves</li> -->
					</ul>
					<div class="resp-tabs-container hor_1">

						<c:if test="${ rolesMap.roles['Work Sheet Reports'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Work Sheets</div>
										<div class="panel-body">
											<form:form action="viewWorkSheetReport.do" method="POST"
												commandName="reportVo" id="timWorkForm">
												<div class="form-group">
													<label class="col-sm-3 control-label">Work Sheet
														Start Date <span class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date timWorkDate" id="datepickers">
															<form:input type="text" class="form-control"
																path="startDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Work Sheet
														End Date <span class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date timWorkDate" id="datepickerss">
															<form:input type="text" class="form-control"
																path="endDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">

														<button type="submit" class="btn btn-info">Generate
															Report</button>

													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Employee Work Sheets'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Employees Work Sheets</div>
										<div class="panel-body">
											<form:form action="viewEmpWorkSheet.do" method="POST"
												commandName="reportVo" id="timEmWorkForm">


												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Employee
														Name<span class="stars">*</span></label>
													<div class="col-sm-8 col-xs-10 br-de-emp-list">
														<form:input path="employee" class="form-control"
															placeholder="Search Employee" id="employeeSl" />
														<form:hidden path="employeeCode" id="employeeCode" />
													</div>
													<div class="col-sm-1 col-xs-1">
														<a href="#" class="show2"><i class="fa fa-user"></i></a>
													</div>
												</div>
												<div id="pop1" class="simplePopup">
												</div>
												<div id="pop2" class="simplePopup empl-popup-gen"></div>


												<div class="form-group">
													<label class="col-sm-3 control-label">Start Date<span class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date timEmWorkDate" id="datepickersss">
															<form:input type="text" class="form-control"
																path="startDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">End Date<span class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date timEmWorkDate" id="datepickerem">
															<form:input type="text" class="form-control"
																path="endDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">

														<button type="submit" class="btn btn-info">Generate
															Report</button>

													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Employee Time Sheet'].view}">
							<div>

								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Employees Time Sheet</div>
										<div class="panel-body">
											<form:form action="viewEmpAttendance.do" method="POST"
												commandName="reportVo" id="timeSheetForm">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Employee
														Name<span class="stars">*</span></label>
													<div class="col-sm-8 col-xs-10 br-de-emp-list">
														<form:input path="employee" class="form-control"
															placeholder="Search Employee" id="employeeSl2" />
														<form:hidden path="employeeCode" id="employeeCode2" />
													</div>
													<div class="col-sm-1 col-xs-1">
														<a href="#" class="show2"><i class="fa fa-user"></i></a>
													</div>
												</div>
												<div id="pop12" class="simplePopup">
													<div class="col-md-12">
														<span class="pop_hd_wht">Select Employee</span>
														<div class="form-group">
															<div class="col-md-12  pop_hd" style="color: #000">
																<div class="col-md-6">
																	<select class="form-control chosen-select">
																		<option></option>
																	</select>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12 branch-lists"></div>
															</div>
														</div>
													</div>
												</div>
												<div id="pop22" class="simplePopup empl-popup-gen"></div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">
														<button type="submit" class="btn btn-info">Generate
															Report</button>
													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<c:if
							test="${ rolesMap.roles['Employee Attendance Reports'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Employee Attendance</div>
										<div class="panel-body">
											<form:form action="viewEmpTimeReport.do" method="POST"
												commandName="reportVo" id="timeAttnForm">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Employee
														Name<span class="stars">*</span></label>
													<div class="col-sm-8 col-xs-10 br-de-emp-list">
														<form:input path="employee" class="form-control"
															placeholder="Search Employee" id="employeeSl1" />
														<form:hidden path="employeeCode" id="employeeCode1" />
													</div>
													<div class="col-sm-1 col-xs-1">
														<a href="#" class="show2"><i class="fa fa-user"></i></a>
													</div>
												</div>
												<div id="pop11" class="simplePopup">
													<div class="col-md-12">
														<span class="pop_hd_wht">Select Employee</span>
														<div class="form-group">
															<div class="col-md-12  pop_hd" style="color: #000">
																<div class="col-md-6">
																	<select class="form-control chosen-select">
																		<option></option>
																	</select>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12 branch-lists"></div>
															</div>
														</div>
													</div>
												</div>
												<div id="pop21" class="simplePopup empl-popup-gen"></div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Start Date <span class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date timAttDate" id="datepickeremp">
															<form:input type="text" class="form-control"
																path="startDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">End Date <span class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date timAttDate" id="datepickerempl">
															<form:input type="text" class="form-control"
																path="endDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">
														<button type="submit" class="btn btn-info">Generate
															Report</button>
													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Absent Employees'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Absent Employees</div>
										<div class="panel-body">
											<form:form action="viewAbsentEmployees.do" method="POST"
												commandName="reportVo" id="absentForm">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Branch</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="branchId">
															<form:option value="0" label="All Branches" />
															<form:options items="${branches }" itemLabel="branchName"
																itemValue="branchId" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Department</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="depId">
															<form:option value="0" label="All Departments" />
															<form:options items="${departments }" itemValue="id"
																itemLabel="name" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Employee</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="employeeCode">
															<form:option value="none" label="All employees" />
															<form:options items="${employees }" itemLabel="name"
																itemValue="employeeCode" />
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Start Date <span class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date absentDate" id="datepickeremployees">
															<form:input type="text" class="form-control"
																path="startDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">End Date <span class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date absentDate" id="datepickernew">
															<form:input type="text" class="form-control"
																path="endDate" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>


												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">

														<button type="submit" class="btn btn-info">Generate
															Report</button>

													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Employee Leave Summary'].view}">
							<div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Employee Leaves Summary</div>
										<div class="panel-body">
											<form:form action="viewEmpLeaves.do" method="POST"
												commandName="reportVo" id="leavSummaryForm">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Employee
														Name<span class="stars">*</span></label>
													<div class="col-sm-8 col-xs-10 br-de-emp-list">
														<form:input path="employee" class="form-control"
															placeholder="Search Employee" id="employeeSl3" />
														<form:hidden path="employeeCode" id="employeeCode3" />
													</div>
													<div class="col-sm-1 col-xs-1">
														<a href="#" class="show2"><i class="fa fa-user"></i></a>
													</div>
												</div>
												<div id="pop13" class="simplePopup">
													<div class="col-md-12">
														<span class="pop_hd_wht">Select Employee</span>
														<div class="form-group">
															<div class="col-md-12  pop_hd" style="color: #000">
																<div class="col-md-6">
																	<select class="form-control chosen-select">
																		<option></option>
																	</select>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12 branch-lists"></div>
															</div>
														</div>
													</div>
												</div>
												<div id="pop23" class="simplePopup empl-popup-gen"></div>

												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Year <span class="stars">*</span></label>
													<div class="col-sm-9">
														<form:select class="form-control" path="year">
															<c:forEach items="${years }" var="year">
																<form:option value="${year}" label="${ year}" />
															</c:forEach>
														</form:select>
													</div>
												</div>


												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-6">

														<button type="submit" class="btn btn-info">Generate
															Report</button>

													</div>
												</div>
											</form:form>
											<input type="hidden" id="popIdHid">
										</div>
									</div>
								</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src='<c:url value="/resources/assets/js/jquery.simplePopup.js" />'></script>


	<script type="text/javascript">
		$(document).ready(function() {

			$('#parentVerticalTab').easyResponsiveTabs({
				type : 'vertical', //Types: default, vertical, accordion
				width : 'auto', //auto or any width like 600px
				fit : true, // 100% fit in a container
				closed : 'accordion', // Start closed if in accordion view
				tabidentify : 'hor_1', // The tab groups identifier
				activate : function(event) { // Callback function if tab is switched
					var $tab = $(this);
					var $info = $('#nested-tabInfo2');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
		});
	</script>
	<script type="text/javascript">
		$(function() {

			if ($.isFunction($.fn.datetimepicker)) {
				$('#datepicker').datetimepicker({
					pickTime : false
				});
				$('#datepickernew').datetimepicker({
					pickTime : false
				});
				$('#datepickernew1').datetimepicker({
					pickTime : false
				});
				$('#datepickernew2').datetimepicker({
					pickTime : false
				});
				$('#datepickernew3').datetimepicker({
					pickTime : false
				});
				$('#datepickernew4').datetimepicker({
					pickTime : false
				});
				$('#datepickernew5').datetimepicker({
					pickTime : false
				});
				$('#datepickernew6').datetimepicker({
					pickTime : false
				});
				$('#datepickernew7').datetimepicker({
					pickTime : false
				});
				$('#datepickernew8').datetimepicker({
					pickTime : false
				});
				$('#datepickernew9').datetimepicker({
					pickTime : false
				});
				$('#datepickernew10').datetimepicker({
					pickTime : false
				});

			}
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#employeeSl').bind('click', function() {
				showPopRport('pop1','employeeSl');
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			//1	pop11
			$('#employeeSl1').bind('click', function() {
			showPopRport('pop11','employeeSl1');
		});

			$('.show21').click(function() {
				fillEmployeePopup($("#employeeCode1").val());
				if ($("#employeeCode1").val() != "") {
					$('#pop21').simplePopup();
				}
			});

			//2 pop12

			$('#employeeSl2').bind('click', function() {
				showPopRport('pop12','employeeSl2');
		});

			$('.show22').click(function() {
				fillEmployeePopup($("#employeeCode2").val());
				if ($("#employeeCode2").val() != "") {
					$('#pop22').simplePopup();
				}
			});
 
			//3 pop13
			$('#employeeSl3').bind('click', function() {
				showPopRport('pop13','employeeSl3');
		});

			$('.show23').click(function() {
				fillEmployeePopup($("#employeeCode3").val());
				if ($("#employeeCode3").val() != "") {
					$('#pop23').simplePopup();
				}
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").addClass("active");

		});
	</script>

</body>
</html>