<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<select class="form-control" name="superiorCode" id="add_superiors">
	<option value="">-Select-</option>
	<c:forEach items="${superiorList}" var="superior">
		<option value="${superior.value}">${superior.label}</option>
	</c:forEach>
</select>
