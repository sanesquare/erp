<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>


	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Announcement</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="announcements.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>


		<form:form action="updateAnnouncements.do" method="POST"
			commandName="announcementsVo">

			<div class="row">


				<div class="col-md-12">


					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#all"
							role="tab" data-toggle="tab">Information</a></li>
						<li role="presentation"><a href="#users" role="tab"
							data-toggle="tab">Documents</a></li>
						<li role="presentation"><a href="#docs" role="tab"
							data-toggle="tab"> Status</a></li>
					</ul>



					<div class="tab-content">
						<div role="tabpanel"
							class="panel panel-default tab-pane tabs-up active" id="all">
							<div class="panel-body">

								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Announcement Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Title <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="announcementsTitle"
																cssClass="form-control" id="inputPassword3"
																required="required" />

														</div>
													</div>
													<form:hidden path="announcementsId" />
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Send by e-mail</label>
														<div class="col-sm-9">
															<!--  <input type="checkbox"/> -->
															<form:checkbox path="sendMail" />
														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<!-- <button type="submit" class="btn btn-info">Next</button> -->
														</div>
													</div>


												</div>
											</div>
										</div>






										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Announcement Dates</div>
												<div class="panel-body">
													<div class="form-group">
														<label class="col-sm-3 control-label">Announcement
															Start Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class='input-group date' id="datepicker">
																<form:input path="startDate" cssClass="form-control"
																	data-date-format="DD/MM/YYYY" required="required" />

																<span class="input-group-addon"><span
																	class="glyphicon glyphicon-calendar"></span> </span>
															</div>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Announcement
															End Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class='input-group date' id="datepickers">
																<form:input path="endDate" cssClass="form-control"
																	data-date-format="DD/MM/YYYY" required="required" />
																<span class="input-group-addon"><span
																	class="glyphicon glyphicon-calendar"></span> </span>
															</div>
														</div>

													</div>


													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<!-- <button type="submit" class="btn btn-info">Next</button> -->
														</div>
													</div>


												</div>
											</div>
										</div>



									</div>

									<div class="col-md-6">


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Message</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">


															<form:textarea path="message" cssClass=" form-control"
																style="height: 150px" required="required" />





														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<!-- <button type="submit" class="btn btn-info">Next</button> -->
														</div>
													</div>

												</div>
											</div>
										</div>




										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes"
																cssClass="form-control height" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">System
																Administrator </label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">25-2-2015 </label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<!-- <button type="submit" class="btn btn-info">Next</button> -->
															<a href="#users" role="tab" data-toggle="tab"
																class="btn btn-info">Next</a>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								</div>



							</div>
						</div>
						<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
							id="users">
							<div class="panel-body">

								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Upload Documents</div>
										<div class="panel-body">

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Upload
													Documents </label>
												<div class="col-sm-9">
													<input type="file" class="btn btn-info" multiple />
												</div>
											</div>



										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-9">
											<!-- <button type="submit" class="btn btn-info">Next</button> -->
											<a href="#docs" role="tab" data-toggle="tab"
												class="btn btn-info">Next</a>
										</div>
									</div>
								</div>


								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">


											<table class="table no-margn">
												<thead>
													<tr>
														<th>#</th>
														<th>Document Name</th>
														<th>view</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td><i class="fa fa-file-o"></i> Lorem ipsum dolor
															sit amet</td>
														<td><i class="fa fa-file-text-o sm"></i></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>

												</tbody>
											</table>


										</div>
									</div>
								</div>




							</div>
						</div>





						<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
							id="docs">
							<div class="panel-body">

								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Status</div>
										<div class="panel-body">

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">
													Status </label>
												<div class="col-sm-9">

													<form:select id="selectbox" cssClass="form-control"
														path="announcementsStatusNotes" onchange="changeFunc();"
														required="required">
														<form:option value="" label="select status"></form:option>
														<c:forEach items="${status }" var="status">
															<form:option value="${status.announcementsStatusNotes }"
																label="${status.announcementsStatus }"></form:option>


														</c:forEach>

													</form:select>

												</div>
											</div>

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Notes
												</label>
												<div class="col-sm-9">
													<form:textarea id="display_notes_here"
														path="announcementsStatusNotes"
														class="form-control height" required="required"
														disabled="true" />
													<!-- <textarea id="display_notes_here" class="form-control height" disabled="disabled"></textarea>  -->
												</div>
											</div>

											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-9">
													<button type="submit" class="btn btn-info">Update</button>
												</div>
											</div>

										</div>
									</div>
								</div>


								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Status</div>
										<div class="panel-body">


											<table class="table no-margn">
												<thead>
													<tr>
														<th>#S</th>
														<th>Date/Time</th>
														<th>Updated By</th>
														<th>Stasus</th>
														<th>Comments</th>

													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>2-2-2015 12:20</td>
														<td>Amal</td>
														<td>Inactive</td>
														<td>Lorem ipsum dolor sit amet 25-2-2015 22:30 pm</td>

													</tr>

												</tbody>
											</table>


										</div>
									</div>
								</div>




							</div>
						</div>


					</div>

				</div>


			</div>

			<!-- form ends here -->
		</form:form>


	</div>

	<!-- Warper Ends Here (working area) -->
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js" type="text/javascript' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").addClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
		function changeFunc() {
			var selectBox = document.getElementById("selectbox");
			var selectedValue = selectBox.options[selectBox.selectedIndex].value;

			document.getElementById("display_notes_here").value = selectedValue;

		}
	</script>
</body>
</html>