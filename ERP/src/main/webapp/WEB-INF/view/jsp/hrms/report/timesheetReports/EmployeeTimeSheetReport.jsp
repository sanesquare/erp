<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
</head>
<body style="background: #FFFFFF !important;">
	<form:form action="exportEmplTimeSheet.do" method="POST"
		modelAttribute="timesheetDetails">
		<h2 class="role_hd">Employee Attendance</h2>
		<div class="col-xs-12">

			<table class="table no-margn line">
				<tbody>

					<tr>
						<td>Employee Name : <strong>${timesheetDetails.empName }<form:hidden
										path="empName" /></strong></td>
						<td>Designation : <strong>${timesheetDetails.designation }<form:hidden
										path="designation" /></strong></td>
					</tr>

					<tr>
						<td>Department: <strong>${timesheetDetails.department }<form:hidden
										path="department" /></strong></td>
						<td>User Name: <strong>${timesheetDetails.userName }<form:hidden
										path="userName" /></strong></td>
					</tr>


				</tbody>
			</table>
			<br /> <br /> <br />

			<table class="table table-striped no-margn line">
				<tbody>
					<tr>

						<td class="hd_tb"><span class="WhiteHeading">Date</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Sign In </span></td>
						<td class="hd_tb"><span class="WhiteHeading">Sign Out
						</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Total
								Break Hours</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Total
								Work Hours </span></td>


					</tr>
					<c:choose>
					<c:when test="${! empty timesheetDetails.details }">
					<c:forEach items="${timesheetDetails.details }" var="details1"
						varStatus="status">
						<tr>
							<td>${details1.dateString }<form:hidden
									path="details[${status.index }].date" /></td>
							<td>${details1.signInString }<form:hidden
									path="details[${status.index }].signIn" /></td>
							<td>${details1.signOutString }<form:hidden
									path="details[${status.index }].signOut" /></td>
							<td>${details1.breakHours }<form:hidden
									path="details[${status.index }].breakHours" /></td>
							<td>${details1.totalHours }<form:hidden
									path="details[${status.index }].totalHours" /></td>
						</tr>
					</c:forEach>
					<tr>
						<td colspan="4"><strong>Total Time</strong></td>
						<td>${timesheetDetails.hoursTotal }<form:hidden
								path="hoursTotal" /></td>
					</tr>
					</c:when>
					<c:otherwise>
					<tr><td>No Records To Show...</td></tr>
					</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<c:if test="${! empty timesheetDetails.details }">
				<div class="form-group" style="margin-top: 25px;">
					<div class="col-sm-offset-4 col-sm-6">
						<button type="submit" class="btn btn-info" name="type" value="xls">
							Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
						</button>
						<button type="submit" class="btn btn-info" name="type" value="pdf">
							Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
						</button>
					</div>
				</div>
			</c:if>
		</div>
	</form:form>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>
</html>