<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Branches</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>


		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Branches</div>
					<div class="panel-body">
						<c:if test="${!empty branches}">

							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered" id="basic-datatable">

								<thead>
									<tr>
										<th width="20%">Branch Name</th>
										<th width="20%">Branch Type</th>
										<th width="20%">City</th>
										<c:if test="${ rolesMap.roles['Branch'].update}">
											<th width="10%">Edit</th>
										</c:if>
										<c:if test="${ rolesMap.roles['Branch'].delete}">
											<th width="10%">Delete</th>
										</c:if>

									</tr>
								</thead>
								<tbody>
									<c:forEach items="${branches}" var="branch">
										<tr>
										<c:choose>
										<c:when test="${ rolesMap.roles['Branch'].view}">
										<td><a href="viewBranch.do?bid=${branch.branchId}">${branch.branchName}</a></td>
										</c:when>
										
											<c:otherwise>
											
											<td>${branch.branchName}</td>
											</c:otherwise>
											</c:choose>
											<td>${branch.branchType}</td>
											<td>${branch.city}</td>
											<c:if test="${ rolesMap.roles['Branch'].update}">
												<td><a href="editBranch.do?bid=${branch.branchId}"><i
														class="fa fa-edit sm"></i> </a></td>
											</c:if>
											<c:if test="${ rolesMap.roles['Branch'].delete}">
												<td><a class="delete_branch"
													onclick="confirmDelete(${branch.branchId})"><i
														class="fa fa-close ss"></i></a></td>
											</c:if>
										</tr>
									</c:forEach>

								</tbody>

							</table>
						</c:if>
						<c:if test="${ rolesMap.roles['Branch'].add}">
							<a href="addBranches.do">
								<button type="button" class="btn btn-primary">Add New
									Branch</button>
							</a>
						</c:if>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>


	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>

	<script type="text/javascript">
    function confirmDelete(id){
		var result=confirm("Are you sure to delete?");
		 if(result){
			$(".delete_branch").attr("href","deleteBranch.do?bid="+id);
		} else{
			$(".delete_branch").attr("href","#");
		} 
	}
    $(document).ready(function(){
    	$("#dashbordli").removeClass("active");
		$("#organzationli").addClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").removeClass("active");
		$("#reportsli").removeClass("active");
    });
    </script>
	<!-- Custom JQuery -->
	<script src="assets/js/app/custom.js" type="text/javascript"></script>
</body>


</html>