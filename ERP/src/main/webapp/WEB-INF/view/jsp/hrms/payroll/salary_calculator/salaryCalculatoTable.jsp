<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<table class="table table-bordered">
	<thead>
		<tr>
			<th width="20%">&nbsp;</th>
			<th width="3%">S.No</th>
			<th width="48%">Description</th>
			<th width="20%" colspan="2">In Rupees</th>
			<th width="10%">Remarks</th>

		</tr>
	</thead>
	<tbody>
		<tr>
			<td><strong class="amt_high">SPLIT </strong></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><strong class="amt_high">salary</strong></td>
			<td><strong class="amt_high">Annual</strong></td>
			<td>&nbsp;</td>
		</tr>
		
		<c:forEach items="${result.basicsVo.resultVo }" var="r" varStatus="s">
		<tr>
			<td>${r.paySlipItem }</td>
			<td>${s.count }</td>
			<td>${r.paySlipItem }</td>
			<td id="calculator_other_monthly">${r.amount }</td>
			<td id="calculator_other_annual">${r.amount *12 }</td>
			<td id="calculator_other_remarks"></td>
		</tr>
		</c:forEach>
		<tr>
			<td></td>
			<td></td>
			<td><strong class="amt_high"> GROSS AMOUNT</strong></td>
			<td><strong class="amt_high" id="calculator_gross_monthly">${result.basicsVo.grossMonthly }</strong></td>
			<td><strong class="amt_high" id="calculator_gross_annual">${result.basicsVo.grossMonthly *12 }</strong></td>
			<td id="calculator_gross_remarks"></td>
		</tr>

		<tr>
			<td rowspan="3">DEDUCTIONS emloyee</td>
			<td></td>
			<td>P.F. (Employee Contribution)</td>
			<td id="calculator_pf_monthly">${result.pfShare.pfEmployeeMonthly }</td>
			<td id="calculator_pf_annual">${result.pfShare.pfEmployeeAnnual }</td>
			<td id="calculator_pf_remarks"></td>
		</tr>
		<tr>

			<td></td>
			<td>E.S.I (Employee Contribution)</td>
			<td id="calculator_esi_monthly">${result.esiVo.esiEmployeeMonthly }</td>
			<td id="calculator_esi_annual">${result.esiVo.esiEmployeeAnnual }</td>
			<td id="calculator_esi_remarks"></td>
		</tr>

		<tr>

			<td></td>
			<td>LWF</td>
			<td id="calculator_lwf_monthly">${result.lwfVo.lwfEmployeeMonthly }</td>
			<td id="calculator_lwf_annual">${result.lwfVo.lwfEmployeeAnnual }</td>
			<td id="calculator_lwf_remarks"></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><strong class="amt_high"> NET TAKE HOME</strong></td>
			<td><strong class="amt_high" id="calculator_net_monthly">${result.netMonthly }</strong></td>
			<td><strong class="amt_high" id="calculator_net_annual">${result.netAnnual }</strong></td>
			<td id="calculator_net_remarks"></td>
		</tr>

		<tr>
			<td rowspan="3">EMPLOYER CONTRIBUTION</td>
			<td></td>
			<td>P.F. (Employer Contribution)</td>
			<td id="calculator_pf_monthly_employer">${result.pfShare.pfEmployerMonthly }</td>
			<td id="calculator_pf_annual_employer">${result.pfShare.pfEmployerAnnual }</td>
			<td id="calculator_pf_remarks_employer"></td>
		</tr>
		<tr>

			<td></td>
			<td>E.S.I (Employer Contribution)</td>
			<td id="calculator_esi_monthly_employer">${result.esiVo.esiEmployerMonthly }</td>
			<td id="calculator_esi_annual_employer">${result.esiVo.esiEmployerAnnual }</td>
			<td id="calculator_esi_remarks_employer"></td>
		</tr>

		<tr>

			<td></td>
			<td>LWF</td>
			<td id="calculator_lwf_monthly_employer">${result.lwfVo.lwfEmployerMonthly }</td>
			<td id="calculator_lwf_annual_employer">${result.lwfVo.lwfEmployerAnnual }</td>
			<td id="calculator_lwf_remarks_employer"></td>
		</tr>

		<tr>
			<td></td>
			<td></td>
			<td><strong class="amt_high">CTC</strong></td>
			<td><strong class="amt_high" id="calculator_ctc_monthly">${result.ctcMonthly }</strong></td>
			<td><strong class="amt_high" id="calculator_ctc_annual">${result.ctcAnnual }</strong></td>
			<td id="calculator_ctc_remarks"></td>
		</tr>

	</tbody>
</table>


<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
<script src="<c:url value='/resources/assets/js/auto_complete.js' />"></script>