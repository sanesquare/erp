<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/app/app.v1.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />

<style type="text/css">
#add_postInfo_success {
	display: none;
}

#add_postInfo_error {
	display: none;
}

#add_postQual_success {
	display: none;
}

#add_postQual_error {
	display: none;
}

#add_postExp_success {
	display: none;
}

#add_postExp_error {
	display: none;
}

#add_postDesc_success {
	display: none;
}

#add_postDesc_error {
	display: none;
}

#add_postAdd_success {
	display: none;
}

#add_postAdd_error {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Job Posts</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="jobPosts" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">

				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form action="saveJobPost.do" method="POST"
								commandName="jobPostVo" enctype="multipart/form-data">

								<form:hidden id="add_post_id" path="jobPostId"></form:hidden>

								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Job Post Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Department
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select class="form-control" path="departmentId"
																id="add_post_dep">
																<form:option value="" label="Select Department"></form:option>
																<c:forEach items="${departments}" var="dep">
																	<form:option value="${dep.id}">${dep.name}</form:option>
																</c:forEach>
															</form:select>

														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Job
															Designation <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<%-- <form:input type="text" class="form-control"
																id="add_post_title" path="jobTitle"></form:input> --%>

															<form:select class="form-control" path="jobTitle"
																id="add_post_title">
																<form:option value="" label="Select Designation"></form:option>
																<form:options items="${designations}" var="des"
																	itemValue="designation" itemLabel="designation" />
																<%-- <form:option value="${dep.id}">${dep.name}</form:option> --%>
																<%-- </c:forEach> --%>
															</form:select>


														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Job Type <span
															class="stars">*</span></label>
														<div class="col-sm-9">

															<form:select class="form-control" path="jobTypeId"
																id="add_post_jobTypes">
																<form:option value="" label="Select Job Type"></form:option>
																<c:forEach items="${jobTypes}" var="jobType">
																	<form:option value="${jobType.jobTypeId}">${jobType.jobType}</form:option>
																</c:forEach>
															</form:select>

														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">No.of
															Positions <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input type="text" class="numbersonly form-control"
																id="add_post_positions" path="positions" />
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Candidate
															Age Range (Start) <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input type="text" class="numbersonly  form-control"
																path="ageStart" id="add_post_ageStart"></form:input>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Candidate
															Age Range (End) <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input type="text" class="numbersonly form-control"
																id="add_post_ageEnd" path="ageEnd"></form:input>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Branches <span
															class="stars">*</span></label>
														<div class="col-sm-9 ">
															<form:select class="form-control chosen-select"
																multiple="true" id="add_post_location" path="branchIds"
																data-placeholder="Select Branch">
																<c:forEach items="${branches}" var="branch">
																	<form:option value="${branch.branchId}">${branch.branchName}</form:option>
																</c:forEach>
															</form:select>
														</div>
													</div>



													<div class="form-group">
														<label class="col-sm-3 control-label">Salary Start
															Range <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input type="text" class="double form-control"
																id="add_post_salaryStrt" path="startSalary"></form:input>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Salary End
															Age Range <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input type="text" class="double form-control"
																id="add_post_salaryEnd" path="endSalary"></form:input>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Job Post
															Closing Date <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepicker">
																<form:input type="text" readonly="true"
																	class="form-control" id="add_post_endDate"
																	path="jobPostEndDate" data-date-format="DD/MM/YYYY"
																	style="background-color: #ffffff !important;" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>

															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<!-- <button type="submit" class="btn btn-info">Next</button> -->
															<a class="btn btn-info" id="submt_basic_post_info"
																onClick="validatePostBasicInfo()">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_postInfo_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
														</div>

														<div class="error_msg" id="add_postInfo_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
														</div>

													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Candidate Qualification</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea class=" form-control"
																id="add_post_qualification" path="qualification"
																style="height: 100px;resize:none;"></form:textarea>

														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a class="btn btn-info" id="submt_post_qua_btn"
																onClick="validatePostQual()">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_postQual_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
														</div>

														<div class="error_msg" id="add_postQual_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>Error...
														</div>

													</div>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Candidate Experience</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">


															<label class="col-sm-3 control-label"> Total
																Experience</label>

															<div class="col-sm-4">
																<form:select class="form-control" id="add_post_years"
																	path="experienceYears">
																	<form:option value="0" label="0 Yr"></form:option>
																	<form:option value="1" label="1 Yr"></form:option>
																	<form:option value="2" label="2 Yrs"></form:option>
																	<form:option value="3" label="3 Yrs"></form:option>
																	<form:option value="4" label="4 Yrs"></form:option>
																	<form:option value="5" label="5 Yrs"></form:option>
																	<form:option value="6" label="6 Yrs"></form:option>
																	<form:option value="7" label="7 Yrs"></form:option>
																	<form:option value="8" label="8 Yrs"></form:option>
																	<form:option value="9" label="9 Yrs"></form:option>
																	<form:option value="10" label="10 Yrs"></form:option>
																	<form:option value="11" label="11 Yrs"></form:option>
																	<form:option value="12" label="12 Yrs"></form:option>
																	<form:option value="13" label="13 Yrs"></form:option>
																	<form:option value="14" label="14 Yrs"></form:option>
																	<form:option value="15" label="15 Yrs"></form:option>
																	<form:option value="16" label="16 Yrs"></form:option>
																	<form:option value="17" label="17 Yrs"></form:option>
																	<form:option value="18" label="18 Yrs"></form:option>
																	<form:option value="19" label="19 Yrs"></form:option>
																	<form:option value="20" label="20 Yrs"></form:option>
																	<form:option value="21" label="21 Yrs"></form:option>
																	<form:option value="22" label="22 Yrs"></form:option>
																	<form:option value="23" label="23 Yrs"></form:option>
																	<form:option value="24" label="24 Yrs"></form:option>
																	<form:option value="25" label="&gt; 25 Yrs"></form:option>
																</form:select>

															</div>
															<div class="col-sm-1" style="margin-top: 9px;">To</div>

															<div class="col-sm-4">


																<form:select class="form-control" id="add_post_months"
																	path="experienceMonths">

																	<form:option value="0" label="0 Yr"></form:option>
																	<form:option value="1" label="1 Yr"></form:option>
																	<form:option value="2" label="2 Yrs"></form:option>
																	<form:option value="3" label="3 Yrs"></form:option>
																	<form:option value="4" label="4 Yrs"></form:option>
																	<form:option value="5" label="5 Yrs"></form:option>
																	<form:option value="6" label="6 Yrs"></form:option>
																	<form:option value="7" label="7 Yrs"></form:option>
																	<form:option value="8" label="8 Yrs"></form:option>
																	<form:option value="9" label="9 Yrs"></form:option>
																	<form:option value="10" label="10 Yrs"></form:option>
																	<form:option value="11" label="11 Yrs"></form:option>
																	<form:option value="12" label="12 Yrs"></form:option>
																	<form:option value="13" label="13 Yrs"></form:option>
																	<form:option value="14" label="14 Yrs"></form:option>
																	<form:option value="15" label="15 Yrs"></form:option>
																	<form:option value="16" label="16 Yrs"></form:option>
																	<form:option value="17" label="17 Yrs"></form:option>
																	<form:option value="18" label="18 Yrs"></form:option>
																	<form:option value="19" label="19 Yrs"></form:option>
																	<form:option value="20" label="20 Yrs"></form:option>
																	<form:option value="21" label="21 Yrs"></form:option>
																	<form:option value="22" label="22 Yrs"></form:option>
																	<form:option value="23" label="23 Yrs"></form:option>
																	<form:option value="24" label="24 Yrs"></form:option>
																	<form:option value="25" label="&gt; 25 Yrs"></form:option>
																</form:select>

															</div>

														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a class="btn btn-info" id="submt_post_exp_btn"
																onclick="validatePostExp()">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_postExp_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>
														</div>

														<div class="error_msg" id="add_postExp_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>
														</div>

													</div>

												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Job Post Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea class=" form-control"
																id="add_post_description" path="postDescription"
																style="height: 150px;resize:none;"></form:textarea>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a class="btn btn-info" id="submt_post_des_btn"
																onClick="validatePostDesc()">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_postDesc_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>Saved
															Successfully.
														</div>

														<div class="error_msg" id="add_postDesc_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>Error!!!
														</div>

													</div>

												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea id="add_post_additionalInfo"
																path="postAdditionalInfo" class="form-control height"></form:textarea>
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${jobPostVo.createdBy}</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${jobPostVo.createdOn}</label>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a class="btn btn-info" id="submt_post_addInfo_btn"
																onClick="validatePostNotes()">Save</a>
														</div>
													</div>
													<div class="col-md-12">

														<div class="success_msg" id="add_postAdd_success">
															<span id="msgs_icn"><i class="fa fa-check"></i></span>Saved
															Successfully.
														</div>

														<div class="error_msg" id="add_postAdd_error">
															<span id="msge_icn"><i class="fa fa-times"></i></span>Error!!!
														</div>

													</div>

												</div>
											</div>
										</div>

									</div>
								</div>
							</form:form>


						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<form:form action="uploadPostDocs.do" method="POST"
							commandName="jobPostVo" enctype="multipart/form-data">

							<form:hidden id="add_post_doc_id" path="jobPostId"></form:hidden>
							<div class="panel-body">
								<c:if test="${ rolesMap.roles['Job Post Documents'].add}">
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Upload Documents</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Upload
														Documents </label>
													<div class="col-sm-9">
														<input type="file" class="btn btn-info" id="jobpost_docs"
															multiple name="uploadPostDocs" />
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-info"
															id="sbmt_post_doc_btn">Upload Documents</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</c:if>

								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">


											<table class="table no-margn">
												<thead>
													<tr>
														<th>Document Name</th>
														<c:if test="${ rolesMap.roles['Job Post Documents'].view}">
															<th>view</th>
														</c:if>
														<c:if
															test="${ rolesMap.roles['Job Post Documents'].delete}">
															<th>Delete</th>
														</c:if>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${jobPostVo.jobPostDocumentsVos }"
														var="docs">
														<tr>
															<td><i class="fa fa-file-o"></i>${docs.fileName }</td>
															<c:if
																test="${ rolesMap.roles['Job Post Documents'].view}">
																<td><a
																	href="downloadPostDoc.do?durl=${docs.documentUrl }"><i
																		class="fa fa-file-text-o sm"></i></a></td>
															</c:if>
															<c:if
																test="${ rolesMap.roles['Job Post Documents'].delete}">
																<td><a
																	href="deletePostDoc.do?durl=${docs.documentUrl }"><i
																		class="fa fa-close ss"></i></a></td>
															</c:if>
														</tr>
													</c:forEach>
												</tbody>
											</table>


										</div>
									</div>
								</div>
								<!-- <div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<button type="submit" value="uploadDocuments"
											class="btn btn-info" id="sbmt_post_doc_btn"
											name="uploadDocuments">Upload Documents</button>
									</div>
						  </div> -->

							</div>
						</form:form>
					</div>


				</div>
				<%-- </form:form> --%>
			</div>


		</div>




	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js'/>"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js'/>"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js'/>"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js'/>"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js'/>"></script>

	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>


	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").addClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");

			var title = $("#add_post_title").val();
			//$("#sbmt_post_doc_btn").attr('disabled', 'disabled');
			if (title != '') {
				$("#updt_post_info_btn").css("display", "block");
				$("#submt_post_info_btn").css("display", "none");

				$("#submt_post_info_btn").removeAttr('disabled');
			} else {
				$("#updt_post_info_btn").css("display", "none");
				$("#submt_post_info_btn").css("display", "block");
			}

			$("#sbmt_post_doc_btn").attr("disabled", "true");
			$("#jobpost_docs").on("change", function() {
				if ($("#add_post_id").val() != '')
					$("#sbmt_post_doc_btn").removeAttr("disabled");
			});
			if ($("#add_post_id").val() != '') {
				enableJobPostButtons();
			} else {
				disableJobPostButtons();
			}
		});

		function enableJobPostButtons() {
			$("#submt_post_qua_btn").removeAttr("disabled");
			$("#submt_post_exp_btn").removeAttr("disabled");
			$("#submt_post_des_btn").removeAttr("disabled");
			$("#submt_post_addInfo_btn").removeAttr("disabled");
		}

		function disableJobPostButtons() {
			$("#submt_post_qua_btn").attr("disabled", "true");
			$("#submt_post_exp_btn").attr("disabled", "true");
			$("#submt_post_des_btn").attr("disabled", "true");
			$("#submt_post_addInfo_btn").attr("disabled", "true");
		}
	</script>

</body>
</html>