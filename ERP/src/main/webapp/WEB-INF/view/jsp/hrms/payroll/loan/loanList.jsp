<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width="20%">Title</th>
			<th width="20%">Employee Name</th>
			<th width="10%">Loan Date</th>
			<th width="10%">Repayment Start Date</th>
			<th width="10%">Amount</th>
			<th width="10%">Remaining Amount</th>
			<th width="10%">Status</th>
			<c:if test="${ rolesMap.roles['Loans'].update}">
				<th width="5%">Edit</th>
			</c:if>
			<c:if test="${ rolesMap.roles['Loans'].delete}">
				<th width="5%">Delete</th>
			</c:if>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${loanList}" var="loan">
			<tr>
				<c:choose>
					<c:when test="${ rolesMap.roles['Loans'].delete}">
						<td><a href="ViewLoan.do?otp=${loan.loanId}">${loan.loanTitle}</a></td>
						<td><c:choose>
								<c:when test="${loan.recordedBy==thisUser}">
									<i class="fa fa-arrow-up up-arrow"></i>
								</c:when>
								<c:otherwise>
									<i class="fa fa-arrow-down down-arrow"></i>
								</c:otherwise>
							</c:choose><a href="ViewLoan?otp=${loan.loanId}">${loan.employee}</a></td>
						<td><a href="ViewLoan.do?otp=${loan.loanId}">${loan.loanDate}</a></td>
						<td><a href="ViewLoan.do?otp=${loan.loanId}">${loan.repaymentStartDate}</a></td>
						<td><a href="ViewLoan.do?otp=${loan.loanId}">${loan.loanAmount}</a></td>
						<td><a href="ViewLoan.do?otp=${loan.loanId}">remaining</a></td>
						<td><a href="ViewLoan.do?otp=${loan.loanId}">${loan.status}</a></td>
					</c:when>
					<c:otherwise>
						<td>${loan.loanTitle}</td>
						<td><c:choose>
								<c:when test="${loan.recordedBy==thisUser}">
									<i class="fa fa-arrow-up up-arrow"></i>
								</c:when>
								<c:otherwise>
									<i class="fa fa-arrow-down down-arrow"></i>
								</c:otherwise>
							</c:choose> ${loan.employee}</td>
						<td>${loan.loanDate}</td>
						<td>${loan.repaymentStartDate}</td>
						<td>${loan.loanAmount}</td>
						<td>remaining</td>
						<td>${loan.status}</td>
					</c:otherwise>
				</c:choose>

				<c:if test="${ rolesMap.roles['Loans'].update}">
					<td><a href="EditLoan.do?otp=${loan.loanId}"><i
							class="fa fa-edit sm"></i> </a></td>
				</c:if>
				<c:if test="${ rolesMap.roles['Loans'].delete}">
					<td><a href="#" onclick="loanDelConfirm(${loan.loanId})"><i
							class="fa fa-close ss"></i></a></td>
				</c:if>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${ rolesMap.roles['Loans'].add}">
	<a href="AddLoan.do">
		<button type="button" class="btn btn-primary">Add New Loan</button>
	</a>
</c:if>

<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

