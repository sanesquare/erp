<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/dialog/bootstrap-dialog.min.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Termination</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Terminations</div>
					<div class="panel-body" id="termination-list">
						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="20%">Employee</th>
									<th width="20%">Termination Date</th>
									<th width="20%">Approval Status</th>
									<c:if test="${ rolesMap.roles['Terminations'].update}">
										<th width="20%">Edit</th>
									</c:if>
									<c:if test="${ rolesMap.roles['Terminations'].delete}">
										<th width="20%">Delete</th>
									</c:if>

								</tr>
							</thead>
							<tbody>
								<c:forEach items="${terminations}" var="termination">
									<tr>
										<c:choose>
											<c:when test="${ rolesMap.roles['Terminations'].view}">
												<td><c:choose>
														<c:when test="${termination.recordedBy==thisUser}">
															<i class="fa fa-arrow-up up-arrow"></i>
														</c:when>
														<c:otherwise>
															<i class="fa fa-arrow-down down-arrow"></i>
														</c:otherwise>
													</c:choose><a
													href="ViewTermination.do?otp_tid=${termination.terminationId}">${termination.employee}</a></td>
												<td><a
													href="ViewTermination.do?otp_tid=${termination.terminationId}">${termination.termiantedDate}</a></td>
												<td><a
													href="ViewTermination.do?otp_tid=${termination.terminationId}">${termination.terminationStatus}</a></td>

											</c:when>
											<c:otherwise>
												<td><c:choose>
														<c:when test="${termination.recordedBy==thisUser}">
															<i class="fa fa-arrow-up up-arrow"></i>
														</c:when>
														<c:otherwise>
															<i class="fa fa-arrow-down down-arrow"></i>
														</c:otherwise>
													</c:choose>${termination.employee}</td>
												<td>${termination.termiantedDate}</td>
												<td>${termination.terminationStatus}</td>
											</c:otherwise>
										</c:choose>

										<c:if test="${ rolesMap.roles['Terminations'].update}">
											<td><a
												href="EditTermination.do?tid=${termination.terminationId}"><i
													class="fa fa-edit sm"></i> </a></td>
										</c:if>
										<c:if test="${ rolesMap.roles['Terminations'].delete}">
											<td><a href="#"
												onclick="terminationDelConfirm(${termination.terminationId})"><i
													class="fa fa-close ss"></i></a></td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<c:if test="${ rolesMap.roles['Terminations'].add}">
							<a href="NewTermination.do">
								<button type="button" class="btn btn-primary">Add New
									Termination</button>
							</a>
						</c:if>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/js/dialog/bootstrap-dialog.min.js' />"></script>

	<script>
	terminationDelConfirm=function(id) {
					BootstrapDialog.confirm(
							'Are you sure you want to delete this record?',
							function(result) {
								if (result) {
									deleteTermination(id);
								}
							});
				};
				$(document).ready(function(){
					$("#dashbordli").removeClass("active");
					$("#organzationli").removeClass("active");
					$("#recruitmentli").removeClass("active");
					$("#employeeli").addClass("active");
					$("#timesheetli").removeClass("active");
					$("#payrollli").removeClass("active");
					$("#reportsli").removeClass("active");
				});
	</script>

</body>
</html>
