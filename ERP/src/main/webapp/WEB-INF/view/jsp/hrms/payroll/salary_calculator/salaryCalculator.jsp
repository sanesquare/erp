<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Salary Calculator</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Salary</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<tr>
								<td width="70%"><strong class="amt_high"> INPUT
										GROSS SALARY</strong></td>
								<td width="30%"><input type="text"
									class="form-control double" placeholder="Enter Gross Salary"
									id="gross_salary_calculator" /></td>
							</tr>
							<tr>
								<td width="70%"></td>
								<td width="30%">
									<button onclick="calculatorCalculate()" class="btn btn-info">Go</button>
								</td>
							</tr>

						</table>
						<div id="calc"></div>


					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Content Block Ends Here (right box)-->

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
			$('#parentVerticalTab').easyResponsiveTabs({
				type : 'vertical', //Types: default, vertical, accordion
				width : 'auto', //auto or any width like 600px
				fit : true, // 100% fit in a container
				closed : 'accordion', // Start closed if in accordion view
				tabidentify : 'hor_1', // The tab groups identifier
				activate : function(event) { // Callback function if tab is switched
					var $tab = $(this);
					var $info = $('#nested-tabInfo2');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
		});
	</script>


	<script type="text/javascript">
		$(function() {

			if ($.isFunction($.fn.datetimepicker)) {
				$('#datepicker').datetimepicker({
					pickTime : false
				});
				$('#datepickernew').datetimepicker({
					pickTime : false
				});
				$('#datepickernew1').datetimepicker({
					pickTime : false
				});
				$('#datepickernew2').datetimepicker({
					pickTime : false
				});
				$('#datepickernew3').datetimepicker({
					pickTime : false
				});
				$('#datepickernew4').datetimepicker({
					pickTime : false
				});
				$('#datepickernew5').datetimepicker({
					pickTime : false
				});
				$('#datepickernew6').datetimepicker({
					pickTime : false
				});
				$('#datepickernew7').datetimepicker({
					pickTime : false
				});
				$('#datepickernew8').datetimepicker({
					pickTime : false
				});
				$('#datepickernew9').datetimepicker({
					pickTime : false
				});
				$('#datepickernew10').datetimepicker({
					pickTime : false
				});

			}
		});
	</script>
</body>
</html>