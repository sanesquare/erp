<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/dialog/bootstrap-dialog.min.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Overtimes</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Overtimes</div>
					<div class="panel-body" id="over-time-list">
						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="20%">Title</th>
									<th width="20%">Employee Name</th>
									<th width="20%">Amount</th>
									<th width="20%">Approval Status</th>
									<c:if test="${ rolesMap.roles['Overtimes'].update}">
										<th width="10%">Edit</th>
									</c:if>
									<c:if test="${ rolesMap.roles['Overtimes'].delete}">
										<th width="10%">Delete</th>
									</c:if>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${overtimeList}" var="overtime">
									<tr>
										<c:choose>
											<c:when test="${ rolesMap.roles['Overtimes'].view}">
												<td><a
													href="ViewOvertime.do?otp=${overtime.overtimeId}">${overtime.overtimeTitle}</a></td>
												<td><c:choose>
														<c:when test="${overtime.recordedBy==thisUser}">
															<i class="fa fa-arrow-up up-arrow"></i>
														</c:when>
														<c:otherwise>
															<i class="fa fa-arrow-down down-arrow"></i>
														</c:otherwise>
													</c:choose><a href="ViewOvertime.do?otp=${overtime.overtimeId}">${overtime.employee}</a></td>
												<td><a
													href="ViewOvertime.do?otp=${overtime.overtimeId}">${overtime.overtimeAmount}</a></td>
												<td><a
													href="ViewOvertime.do?otp=${overtime.overtimeId}">${overtime.status}</a></td>
											</c:when>
											<c:otherwise>
												<td>${overtime.overtimeTitle}</td>
												<td><c:choose>
														<c:when test="${overtime.recordedBy==thisUser}">
															<i class="fa fa-arrow-up up-arrow"></i>
														</c:when>
														<c:otherwise>
															<i class="fa fa-arrow-down down-arrow"></i>
														</c:otherwise>
													</c:choose>${overtime.employee}</td>
												<td>${overtime.overtimeAmount}</td>
												<td>${overtime.status}</td>
											</c:otherwise>
										</c:choose>

										<c:if test="${ rolesMap.roles['Overtimes'].update}">
											<td><a href="EditOvertime.do?otp=${overtime.overtimeId}"><i
													class="fa fa-edit sm"></i> </a></td>
										</c:if>
										<c:if test="${ rolesMap.roles['Overtimes'].delete}">
											<td><a href="#"
												onclick="overtimeDelConfirm(${overtime.overtimeId})"><i
													class="fa fa-close ss"></i></a></td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<c:if test="${ rolesMap.roles['Overtimes'].add}">
							<a href="AddOvertime.do">
								<button type="button" class="btn btn-primary">Add New
									Overtime</button>
							</a>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/js/dialog/bootstrap-dialog.min.js' />"></script>
	<script>
	overtimeDelConfirm=function(id){
					BootstrapDialog.confirm(
							'Are you sure you want to delete this record?',
							function(result) {
								if (result) {
									deleteOvertime(id); 
								}
							});
				};
				$(document).ready(function(){
					$("#dashbordli").removeClass("active");
					$("#organzationli").removeClass("active");
					$("#recruitmentli").removeClass("active");
					$("#employeeli").removeClass("active");
					$("#timesheetli").removeClass("active");
					$("#payrollli").addClass("active");
					$("#reportsli").removeClass("active");
				});
	</script>
</body>
</html>
