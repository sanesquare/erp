<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div id="fade"></div>
	<div id="modal">
		<img id="loader" src="resources/images/loading.gif" />
	</div>
	<div class="warper container-fluid">
		<div class="page-header">Employees Exit</div>
		<div class="row">
			<div class="col-md-12">
				<a href="EmployeeExit.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab" id="emp_ext_doc_tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Employee Exit Information</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Employee</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employeeExitVo.employee}
														</label>
													</div>
												</div>
												<input type="hidden" name="employeeExitId"
													id="employeeExitId"
													value="${employeeExitVo.employeeExitId}" />
												<div class="form-group">
													<label class="col-sm-3 control-label">Exit Date</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employeeExitVo.exitDate}
														</label>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Type Of Exit</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employeeExitVo.exitType}
														</label>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Conducted
														Exit Interview</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employeeExitVo.exitStringDate}
														</label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Exit Reason</div>
											<div class="panel-body">
												<div class="form-group">
													<div class="col-md-12">
														<label for="inputPassword3"
															class="col-sm-12 control-label ft">${employeeExitVo.exitReason}</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${employeeExitVo.exitNotes}</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employeeExitVo.recordedBy}</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employeeExitVo.recordedOn}
														</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Upload Documents</div>
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Upload
												Documents </label>
											<%-- <form:form action="SaveEmployeeExitDocument" method="post"
												commandName="employeeExitVo" enctype="multipart/form-data">
												<div class="col-sm-9">
													<!-- <input type="file" name="emp_ext_docs" class="btn btn-info" /> -->
													<form:input type="file" path="emp_ext_docs" class="btn btn-info"/>
													<input type="file" name="emp_ext_docs" class="btn btn-info"
														multiple />
													<form:hidden path="employeeExitId" />
												</div>
												<div class="col-sm-offset-3 col-sm-9">
													<button type="submit" class="btn btn-info">Upload</button>
												</div>
											</form:form> --%>
											<div class="col-sm-9">
												<input type="file" name="employeeExitDocs"
													id="employee-exit-docs" class="btn btn-info" multiple />
												<button type="button" onclick="uploadEmployeeExitDocument()"
													class="btn btn-info">Upload</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body" id="emp_ext_doc_lst"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		$(document).ready(function() {
			$('#employeeSl').bind('click', function() {
				getAllBranches();
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
	<!-- <script>
		$('#js-smart-tabs').smartTabs();
		$('#js-smart-tabs--tabs').smartTabs({
			layout : 'tabs'
		});
		$('#js-smart-tabs--accordion').smartTabs({
			layout : 'accordion'
		});
	</script>
	<script>
		$(document).ready(function() {

			$("#admin_ss").click(function() {
				$("#add_administartor").show();
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addfield").click(function() {
				$("#add_custom_feild").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addnot").click(function() {
				$("#add_notification").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
				//
				$.ajax({
					url: "addNotification",
					type: "GET",
					success: function(data) {
						$("#div_notification_form").html(data);
					}
				});
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addrm").click(function() {
				$("#add_rm").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script> -->
</body>
</html>
