<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>


	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Daily Work Report</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="worksheets.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Daily Work Report
												Information</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Employee</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${workSheet.employee}
														</label>
													</div>
												</div>




												<div class="form-group">
													<label class="col-sm-3 control-label"> Date</label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${workSheet.date }
														</label>
													</div>
												</div>

											</div>
										</div>
									</div>


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Tasks</div>
											<div class="panel-body">


												<table class="table no-margn">
													<thead>
														<tr>
															<th>Start Time</th>
															<th>End Time</th>
															<th>Task</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${workSheet.tasksVo }" var="tasks">
															<tr>
																<td>${tasks.startTime }</td>
																<td>${tasks.endTime }</td>
																<td>${tasks.task }</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Daily Work Report
												Description</div>
											<div class="panel-body">
												<div class="form-group">
													<div class="col-md-12">
														<label for="inputPassword3"
															class="col-sm-12 control-label ft">${workSheet.description }</label>
													</div>
												</div>

											</div>
										</div>
									</div>


								</div>
								<div class="col-md-6">






									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${workSheet.addInfo }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${workSheet.createdBy }
														</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${workSheet.createdOn }</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<!-- <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">Upload Documents</div>
                  <div class="panel-body">
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-3 control-label">Upload Documents </label>
                      <div class="col-sm-9">
                        <input type="file" class="btn btn-info"/>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">
										<table class="table no-margn">
											<thead>
												<tr>
													<th>Document Name</th>
													<c:if test="${ rolesMap.roles['Worksheet Documents'].view}">
														<th>view</th>
													</c:if>
													<!-- <th>Delete</th> -->
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${workSheet.documentsVo }" var="docs">
													<tr>
														<td><i class="fa fa-file-o"></i> ${docs.documentName }</td>
														<c:if
															test="${ rolesMap.roles['Worksheet Documents'].view}">
															<td><a
																href="viewWorksheetDoc.do?durl=${docs.documentUrl }"><i
																	class="fa fa-file-text-o sm"></i></a></td>
														</c:if>
														<!-- <td><i class="fa fa-close ss"></i></td> -->
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Simple Map</div>
									<div class="panel-body">
										<div id="simpleMap" style="height: 500px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js'/>"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js'/>"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js'/>"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js'/>"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js'/>"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js'/>"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js'/>"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js'/>"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js'/>"></script>

	<!-- Custom JQuery -->
	<script
		src="<c:url value='/resources/assets/js/app/custom.js" type="text/javascript'/>"></script>


	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js'/>"
		type="text/javascript"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").addClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>