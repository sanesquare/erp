<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
#add_perform_delete {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Performance Evaluation</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="PerformanceEvaluation" class="btn btn-info"
					style="float: right;"><i class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form action="CreatePerformanceEvaluation.do"
								id="performanceEvaluationForm" method="POST" commandName="performVo">
								<form:hidden path="evaluationId" id="add_perform_id" />
								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Performance Evaluation
													Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">
															Title <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input type="text" class="form-control" path="title"
																id="add_perform_title" placeholder=" Title" />
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label"> Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepicker">
																<form:input type="text" class="form-control" path="date"
																	id="add_perform_date" data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>
													</div>


												</div>
											</div>
										</div>


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Performance Evaluation
													Information</div>
												<div class="panel-body">



													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Employee
															Name</label>
														<form:hidden path="individualId"
															id="add_perform_individual" />
														<div class="col-sm-8 col-xs-10 br-de-emp-list">
															<form:input path="employee" class="form-control"
																onchange="getEmployeePerformance()"
																placeholder="Search Employee" id="employeeSl" />
															<form:hidden path="employeeCode" id="employeeCode" />

														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div>
													</div>
													<div id="pop1" class="simplePopup">
														<div class="col-md-12">
															<span class="pop_hd_wht">Select Employee</span>
															<div class="form-group">
																<div class="col-md-12  pop_hd" style="color: #000">
																	<div class="col-md-6">
																		<select class="form-control chosen-select">
																			<option></option>
																		</select>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-12 branch-lists"></div>
																</div>
															</div>
														</div>
													</div>
													<div id="pop2" class="simplePopup empl-popup-gen"></div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Branch</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" readonly="true"
																id="add_perform_branch" placeholder=" Branch">
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Designation</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" readonly="true"
																id="add_perform_des" placeholder=" Designation">
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Maximum Mark</label>
														<div class="col-sm-9 ">
															<form:input type="text" class="form-control"
																path="maxMark" id="add_perform_max"
																placeholder=" Maximum Mark" />
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Scored Mark</label>
														<div class="col-sm-9 ">
															<form:input type="text" class="form-control" path="score"
																id="add_perform_score" placeholder=" Scored Mark" />
														</div>
													</div>

													<div class="form-group">
														<div class=" col-sm-6">
															<a id="add_perform_delete" href="#"
																onclick="evaluationDelConfirm()" class="btn btn-info">Delete</a>
														</div>
														<div class="col-sm-6">
															<button type="submit" class="btn btn-info">Add</button>
														</div>
													</div>


												</div>
											</div>
										</div>




									</div>
									<div class="col-md-6">

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<textarea class="form-control height"></textarea>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">System
																Administrator </label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">12/2/2015 </label>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>




				</div>
			</div>
		</div>
	</div>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/js/dialog/bootstrap-dialog.min.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<%-- 	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script> --%>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script>
		$(document).ready(function() {
			$("#add_perform_delete").hide();

			$('#employeeSl').bind('click', function() {
				getAllBranches();
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#addrm").click(function() {
				$("#add_rm").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");

			//$("#add_perform_delete").attr("disabled","true");

		});
		evaluationDelConfirm = function() {
			BootstrapDialog
					.confirm('Are you sure you want to delete this record?',
							function(result) {
								if (result) {
									deleteEmployeePerformance($(
											"#add_perform_id").val(), $(
											"#add_perform_individual").val());
								}
							});
		};
	</script>
</body>
</html>