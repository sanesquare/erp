<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>System Settings</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>


		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">System Settings</a></li>
					<!-- <li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Custom Fields</a></li> -->
					<li role="presentation"><a href="#docs" role="tab"
						data-toggle="tab" id="not-list-add"> Notifications</a></li>
					<li role="presentation"><a href="#tps" role="tab"
						data-toggle="tab" id="rm-list-add"> Reminders</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<dl id="js-smart-tabs" class="smart-tabs">
								<dt>
									<a href="#">Organisation Details</a>
								</dt>
								<dd>
									<div class="row">
										<form:form action="SaveOrganisation.do" method="post"
											commandName="organisationVo" id="organisationForm">
											<div class="col-md-6">
												<div class="col-md-12">
													<div class="panel panel-default">
														<div class="panel-heading">Organisation Details</div>
														<div class="panel-body">
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-3 control-label">Organisation
																	Code<span class="stars">*</span>
																</label>
																<div class="col-sm-9">
																	<form:input path="organisationCode"
																		class="form-control" placeholder="Organisation Code" />
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Organisation Url<span
																	class="stars">*</span></label>
																<div class="col-sm-9">
																	<form:input path="organisationUrt" class="form-control"
																		placeholder="Organisation Url" />
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Organisation
																	Name<span class="stars">*</span>
																</label>
																<div class="col-sm-9">
																	<form:input path="organisationName"
																		class="form-control" placeholder="Station Name" />
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Organisation
																	Year<span class="stars">*</span>
																</label>
																<div class="col-sm-9">
																	<div id="oyear-list"></div>
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Fiscal Year
																	Start Date<span class="stars">*</span>
																</label>
																<div class="col-sm-4">
																	<div id="omonth-list"></div>
																</div>
																<div class="col-sm-4">
																	<div id="oday-list"></div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="col-md-12">
													<div class="panel panel-default">
														<div class="panel-heading">Contact Person</div>
														<div class="panel-body">
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">First Name <span
																	class="stars">*</span>
																</label>
																<div class="col-sm-9">
																	<form:input path="contactFirstName"
																		class="form-control" placeholder="First Name" />
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Last Name <span
																	class="stars">*</span>
																</label>
																<div class="col-sm-9">
																	<form:input path="contactLastName" class="form-control"
																		placeholder="Last Name" />
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Email Address<span
																	class="stars">*</span>
																</label>
																<div class="col-sm-9">
																	<form:input path="contactEmail" class="form-control"
																		placeholder="Email Address" />
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Country <span
																	class="stars">*</span>
																</label>
																<div class="col-sm-9">
																	<form:select path="contactCountry" class="form-control">
																		<form:option value="">-Select-</form:option>
																		<c:forEach items="${countryList}" var="country">
																			<form:option value="${country.name}">${country.name}</form:option>
																		</c:forEach>
																	</form:select>
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Phone Number <span
																	class="stars">*</span>
																</label>
																<div class="col-sm-9">
																	<form:input path="contactNumber" class="form-control"
																		placeholder="Phone Number" />
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-offset-3 col-sm-9">
																	<form:hidden path="organisationId" id="organisationId" />
																	<button type="submit" class="btn btn-info">Save
																		Organisation</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</form:form>
									</div>
								</dd>
								<dt>
									<a href="#" id="general-settings-tab">General Settings</a>
								</dt>
								<dd>
									<div class="row">
										<div class="col-md-6">
											<div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">General Settings</div>
													<div class="panel-body">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Second Level
																Forwarding </label>
															<div class="col-sm-9" id="second-levels-list-div">
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label"> </label>
															<div class="col-sm-9">
																<p>Second Level Forwarding refers to a request that
																	has already been forwarded to the Line Manager, but can
																	further be forwarded to either the Line Manager of the
																	person who has received the request or to other
																	selected employees.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<%-- <div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">Currency</div>
													<div class="panel-body">
														<form:form action="SaveCurrency" method="post"
															commandName="currencyVo" id="currencySettingsForm">
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Base Currency </label>
																<div class="col-sm-9">
																	<select name="baseCurrency" id="baseCrncySlct"
																		class="form-control">
																		<option value="">Select Currency</option>
																		<c:forEach items="${baseCurrencies}" var="currency">
																			<option>${currency.baseCurrency}</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Currency Sign </label>
																<div class="col-sm-9">
																	<input type="text" class="form-control" id="crncySgn"
																		placeholder="$">
																</div>
															</div>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">No.of Decimal
																	Places </label>
																<div class="col-sm-9">
																	<input type="text" name="decimalPlaces"
																		class="form-control" id="inputPassword3"
																		placeholder="0">
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-offset-3 col-sm-9">
																	<button type="submit" class="btn btn-info">Save</button>
																</div>
															</div>
														</form:form>
													</div>
												</div>
											</div> --%>
											<div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">Date & Time</div>
													<div class="panel-body">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Time Format </label>
															<div class="col-sm-9">
																<select class="form-control">
																	<option value="">-Select-</option>
																	<option value="12">12 Hour</option>
																	<option value="24">24 Hour</option>
																</select>
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-offset-3 col-sm-9">
																<button type="submit" class="btn btn-info">Save
																	Settings</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</dd>
								<dt>
									<a href="#" id="sys-admin-tab">System Administrators </a>
								</dt>
								<dd>
									<div class="row">
										<div class="col-md-6">
											<div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">System Administrators</div>
													<div class="panel-body" id="admin-settings"></div>
												</div>
											</div>
										</div>
									</div>
								</dd>
								<dt>
									<a href="#" id="auto-aprv-tab">Auto Approve </a>
								</dt>
								<dd>
									<div class="row">
										<div class="col-md-6">
											<div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">Auto Approve</div>
													<div class="panel-body" id="auto-aprov-settings"></div>
												</div>
											</div>
										</div>
									</div>
								</dd>
								<dt>
									<a href="#">Constants </a>
								</dt>
								<dd>
									<div class="row">
										<div class="col-md-6">
											<div class="col-md-12">
												<div class="panel panel-default">
													<div class="panel-heading">Constants</div>
													<div class="panel-body">
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Languages
															</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show1"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Languages</label>
															</div>
														</div>
														<div id="pop1" class="simplePopup">
															<h1 class="pop_hd">Manage Languages</h1>
															<div class="row">
																<form id="StLanFrm" onsubmit="newLanguage()">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Languages </label>
																			<div class="col-sm-9" id="avai-l-anguage"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">New Languages
																			</label>
																			<div class="col-sm-9">
																				<textarea name="language" id="language-val"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempLanguage"
																					id="tempLanguage" />
																				<button type="submit" id="new-language"
																					class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Skills
															</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show2"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Skills</label>
															</div>
														</div>
														<div id="pop2" class="simplePopup">
															<h1 class="pop_hd">Manage Skills</h1>
															<div class="row">
																<form action="#" id="StSkillForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Skills </label>
																			<div class="col-sm-9" id="avai-l-skill"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Skills </label>
																			<div class="col-sm-9">
																				<textarea name="skillName" id="skillNameVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempSkill" id="tempSkill" />
																				<button type="submit" onclick="newSkill()"
																					id="new-skill" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-sk-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Qualification
																Degrees </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show3"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Qualification Degrees</label>
															</div>
														</div>
														<div id="pop3" class="simplePopup">
															<h1 class="pop_hd">Manage Qualification Degrees</h1>
															<div class="row">
																<form action="#" id="StQDForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Degrees </label>
																			<div class="col-sm-9" id="avai-l-qd"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Qualification
																				Degrees </label>
																			<div class="col-sm-9">
																				<textarea name="degreeName" id="degreeNameVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempQualification"
																					id="tempQualification" />
																				<button type="submit" onclick="newQD()" id="new-qd"
																					class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-qd-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Contract
																Type </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show4"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Contract Type </label>
															</div>
														</div>
														<div id="pop4" class="simplePopup">
															<h1 class="pop_hd">Manage Contract Type</h1>
															<div class="row">
																<form action="#" id="StCTForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Contract Types </label>
																			<div class="col-sm-9" id="avai-l-ct"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Contract Type
																			</label>
																			<div class="col-sm-9">
																				<textarea name="contractType" id="contractTypeVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempType" id="tempType" />
																				<button type="submit" onclick="newCT()"
																					id="new-contract-type" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-ct-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Job
																Type </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show5"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage Job
																	Type</label>
															</div>
														</div>
														<div id="pop5" class="simplePopup">
															<h1 class="pop_hd">Manage Job Type</h1>
															<div class="row">
																<form action="#" id="StJTForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available Job
																				Types </label>
																			<div class="col-sm-9" id="avai-l-jt"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Job Type </label>
																			<div class="col-sm-9">
																				<textarea name="jobType" id="jobTypeVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempJobType"
																					id="tempJobType" />
																				<button type="submit" onclick="newJT()"
																					id="new-job-type" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-jt-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Job
																Fields </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show6"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage Job
																	Fields</label>
															</div>
														</div>
														<div id="pop6" class="simplePopup">
															<h1 class="pop_hd">Manage Job Fields</h1>
															<div class="row">
																<form action="#" id="StJFForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available Job
																				Fields </label>
																			<div class="col-sm-9" id="avai-l-jf"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Job Fields </label>
																			<div class="col-sm-9">
																				<textarea name="jobField" id="jobFieldVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempJobField"
																					id="tempJobField" />
																				<button type="submit" onclick="newJF()"
																					id="new-job-fields" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-jf-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Branch
																Types </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show7"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Branch Types</label>
															</div>
														</div>
														<div id="pop7" class="simplePopup">
															<h1 class="pop_hd">Manage Branch Types</h1>
															<div class="row">
																<form action="#" id="StBTForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Branch Types </label>
																			<div class="col-sm-9" id="avai-l-bt"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Branch Types
																			</label>
																			<div class="col-sm-9">
																				<textarea name="branchTypeName"
																					id="branchTypeNameVal" class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempBranchType"
																					id="tempBranchType" />
																				<button type="submit" onclick="newBT()"
																					id="new-branch-type" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-bt-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Policy
																Types </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show8"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Policy Types</label>
															</div>
														</div>
														<div id="pop8" class="simplePopup">
															<h1 class="pop_hd">Manage Policy Types</h1>
															<div class="row">
																<form action="#" id="StPTForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Policy Types </label>
																			<div class="col-sm-9" id="avai-l-pt"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Policy Types
																			</label>
																			<div class="col-sm-9">
																				<textarea name="policyType" id="policyTypeVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempPolicyType"
																					id="tempPolicyType" />
																				<button type="submit" onclick="newPT()"
																					id="new-policy-type" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-pt-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Employee
																Types </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show9"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Employee Types</label>
															</div>
														</div>
														<div id="pop9" class="simplePopup">
															<h1 class="pop_hd">Manage Employee Types</h1>
															<div class="row">
																<form action="#" id="StETForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Employee Types </label>
																			<div class="col-sm-9" id="avai-l-et"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Employee
																				Types </label>
																			<div class="col-sm-9">
																				<textarea name="employeeType" id="employeeTypeVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempEmployeeType"
																					id="tempEmployeeType" />
																				<button type="submit" onclick="newET()"
																					id="new-employee-type" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-et-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Employee
																Category </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show10"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Categories</label>
															</div>
														</div>
														<div id="pop10" class="simplePopup">
															<h1 class="pop_hd">Manage Categories</h1>
															<div class="row">
																<form action="#" id="StECForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Categories</label>
																			<div class="col-sm-9" id="avai-l-ec"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Categories</label>
																			<div class="col-sm-9">
																				<textarea name="employeeCategory"
																					id="employeeCategoryVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempEmployeeCategory"
																					id="tempEmployeeCategory" />
																				<button type="submit" onclick="newEC()"
																					id="new-employee-category" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-ec-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<!-- <div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Insurance
																Type </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show11"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Insurance Type</label>
															</div>
														</div> -->
														<div id="pop11" class="simplePopup">
															<h1 class="pop_hd">Manage Insurance Type</h1>
															<div class="row">
																<form:form action="SaveInsuranceType" method="post"
																	commandName="insuranceTypeVo">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Insurance
																				Type </label>
																			<div class="col-sm-9">
																				<textarea name="insuranceType"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" class="btn btn-info">Save</button>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<!-- <div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Training
																Types </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show12"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Training Types</label>
															</div>
														</div> -->
														<div id="pop12" class="simplePopup">
															<h1 class="pop_hd">Manage Training Types</h1>
															<div class="row">
																<form:form action="SaveTrainingType" method="post"
																	commandName="trainingTypeVo">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Training
																				Types </label>
																			<div class="col-sm-9">
																				<textarea name="trainingType"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" class="btn btn-info">Save</button>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<!-- <div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Training
																Event Types </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show13"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Training Event Types </label>
															</div>
														</div> -->
														<div id="pop13" class="simplePopup">
															<h1 class="pop_hd">Manage Training Event Types</h1>
															<div class="row">
																<form:form action="SaveTrainingEventType" method="post"
																	commandName="trainingEventTypeVo">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Training
																				Event Types </label>
																			<div class="col-sm-9">
																				<textarea name="trainingEventType"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" class="btn btn-info">Save</button>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Reimbursement
																Categories </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show14"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Reimbursement Categories</label>
															</div>
														</div>
														<div id="pop14" class="simplePopup">
															<h1 class="pop_hd">Manage Reimbursement Categories</h1>
															<div class="row">
																<form action="#" id="StRIForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Categories </label>
																			<div class="col-sm-9" id="avai-l-rc"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Reimbursement
																				Categories </label>
																			<div class="col-sm-9">
																				<textarea name="reimbursementCategory"
																					id="reimbursementCategoryVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden"
																					name="tempReimbursementCategory"
																					id="tempReimbursementCategory" />
																				<button type="submit" onclick="newRI()"
																					id="new-reimb-catg" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-rc-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Country
															</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show15"><i
																		class="fa fa-plus-square-o font_size"></i></a>Add New
																	Country to the List</label>
															</div>
														</div>
														<div id="pop15" class="simplePopup">
															<h1 class="pop_hd">Add New Country</h1>
															<div class="row">
																<form action="#" id="StCTRForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Countries </label>
																			<div class="col-sm-9" id="avai-l-cl"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Country Name
																			</label>
																			<div class="col-sm-9">
																				<textarea name="name" id="countryName"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempName" id="tempName" />
																				<button type="submit" onclick="newCTR()"
																					id="new-country" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-cl-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<!-- <div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Base
																Currency</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show16"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage Base
																	Currency </label>
															</div>
														</div> -->
														<div id="pop16" class="simplePopup">
															<h1 class="pop_hd">Manage Base Currency</h1>
															<div class="row">
																<form:form action="SaveBaseCurrency" method="post"
																	commandName="baseCurrencyVo">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Base Currency
																			</label>
																			<div class="col-sm-9">
																				<div class="col-sm-9">
																					<input type="text" class="form-control"
																						id="inputPassword3" name="baseCurrency"
																						placeholder="Base Currency">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label"> Currency
																				Sign </label>
																			<div class="col-sm-9">
																				<div class="col-sm-9">
																					<select name="sign" class="form-control">
																						<option value="&#164;">&#164;</option>
																						<option value="&#36;">&#36;</option>
																						<option value="&#162;">&#162;</option>
																						<option value="&#163;">&#163;</option>
																						<option value="&#165;">&#165;</option>
																						<option value="&#8355;">&#8355;</option>
																						<option value="&#8356;">&#8356;</option>
																						<option value="&#8359;">&#8359;</option>
																						<option value="&#128;">&#128;</option>
																						<option value="&#x20B9;">&#x20B9;</option>
																						<option value="&#8361;">&#8361;</option>
																						<option value="&#8372;">&#8372;</option>
																						<option value="&#8367;">&#8367;</option>
																						<option value="&#8366;">&#8366;</option>
																						<option value="&#8368;">&#8368;</option>
																						<option value="&#8370;">&#8370;</option>
																						<option value="&#8369;">&#8369;</option>
																						<option value="&#8371;">&#8371;</option>
																						<option value="&#8373;">&#8373;</option>
																						<option value="&#8365;">&#8365;</option>
																						<option value="&#8362;">&#8362;</option>
																						<option value="&#8363;">&#8363;</option>
																					</select>
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" class="btn btn-info">Save</button>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<!-- <div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Time
																Zone</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show17"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage Time
																	Zone </label>
															</div>
														</div> -->
														<div id="pop17" class="simplePopup">
															<h1 class="pop_hd">Add New Time Zone</h1>
															<div class="row">
																<form:form action="SaveTimeZone" method="post"
																	commandName="timeZoneVo">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">TimeZone
																				Offset </label>
																			<div class="col-sm-9">
																				<div class="col-sm-9">
																					<input type="text" class="form-control"
																						id="inputPassword3" name="offSet"
																						placeholder="TimeZone Offset">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">TimeZone
																				Location </label>
																			<div class="col-sm-9">
																				<div class="col-sm-9">
																					<input type="text" class="form-control"
																						id="inputPassword3" name="location"
																						placeholder="TimeZone Location">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" class="btn btn-info">Save</button>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<!-- <div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Blood
																Group</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show18"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage Blood
																	Group</label>
															</div>
														</div> -->
														<div id="pop18" class="simplePopup">
															<h1 class="pop_hd">Add New Blood Group</h1>
															<div class="row">
																<form:form action="SaveBloodGroup" method="post"
																	commandName="bloodGroupVo">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Blood Groups
																			</label>
																			<div class="col-sm-9">
																				<textarea name="groupName"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" class="btn btn-info">Save</button>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<!-- <div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Relegion</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show19"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Relegion</label>
															</div>
														</div> -->
														<div id="pop19" class="simplePopup">
															<h1 class="pop_hd">Add New Relegion</h1>
															<div class="row">
																<form:form action="SaveRelegion" method="post"
																	commandName="relegionVo">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Relegion </label>
																			<div class="col-sm-9">
																				<textarea name="relegion"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" class="btn btn-info">Save</button>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Account
																Type</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show20"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Account Type</label>
															</div>
														</div>
														<div id="pop20" class="simplePopup">
															<h1 class="pop_hd">Add New Account Type</h1>
															<div class="row">
																<form action="#" id="StATForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Account Types </label>
																			<div class="col-sm-9" id="avai-l-at"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Account Type
																			</label>
																			<div class="col-sm-9">
																				<textarea name="accountType" id="accountTypeVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempAccountType"
																					id="tempAccountType" />
																				<button type="submit" onclick="newAT()"
																					id="new-account-type" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-at-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Work
																Shift</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show21"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage Work
																	Shift</label>
															</div>
														</div>
														<div id="pop21" class="simplePopup">
															<h1 class="pop_hd">Add New Work Shift</h1>
															<div class="row">
																<form action="#" id="StWSForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Work Shifts</label>
																			<div class="col-sm-9" id="avai-l-ws"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Work Shifts </label>
																			<div class="col-sm-9">
																				<textarea name="workShift" id="workShiftVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempWorkShift"
																					id="tempWorkShift" />
																				<button type="submit" onclick="newWS()"
																					id="new-work-shifts" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-ws-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Employee
																Status</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show22"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Employee Status</label>
															</div>
														</div>
														<div id="pop22" class="simplePopup">
															<h1 class="pop_hd">Add New Employee Status</h1>
															<div class="row">
																<form action="#" id="StESForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Status</label>
																			<div class="col-sm-9" id="avai-l-st"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Employee
																				Status </label>
																			<div class="col-sm-9">
																				<textarea name="status" id="employeeStatus"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempEmployeeStatus"
																					id="tempEmployeeStatus" />
																				<button type="submit" onclick="newES()"
																					id="new-employee-status" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-st-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Assets</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show23"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Organization Assets</label>
															</div>
														</div>
														<div id="pop23" class="simplePopup">
															<h1 class="pop_hd">Add New Assets</h1>
															<div class="row">
																<form action="#" id="StASTForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Assets</label>
																			<div class="col-sm-9" id="avai-l-assets"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Asset Code </label>
																			<div class="col-sm-9">
																				<div class="col-sm-9">
																					<input type="text" class="form-control"
																						id="assetCodeVal" name="assetCode"
																						placeholder="Asset Code">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Asset Name </label>
																			<div class="col-sm-9">
																				<div class="col-sm-9">
																					<input type="text" class="form-control"
																						id="assetNameVal" name="assetName"
																						placeholder="Asset Name">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempAssetCode"
																					id="tempAssetCode" />
																				<button type="submit" onclick="newAST()"
																					class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-ast-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Termination
																Status</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show24"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Termination Status</label>
															</div>
														</div>
														<div id="pop24" class="simplePopup">
															<h1 class="pop_hd">Add New Termination Status</h1>
															<div class="row">
																<form action="#" id="StTSForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Status</label>
																			<div class="col-sm-9" id="avai-l-ts"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Termination
																				Status </label>
																			<div class="col-sm-9">
																				<textarea name="status" id="terminationStatus"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" onclick="newTS()"
																					id="new-termination-status" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-ts-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<!-- <div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Promotion
																Status</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show25"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Promotion Status</label>
															</div>
														</div> -->
														<div id="pop25" class="simplePopup">
															<h1 class="pop_hd">Add New Promotion Status</h1>
															<div class="row">
																<form:form action="SavePromotionStatus" method="post"
																	commandName="promotionStatusVo">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Promotion
																				Status </label>
																			<div class="col-sm-9">
																				<textarea name="promotionStatus"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" class="btn btn-info">Save</button>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<!-- <div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Employee
																Exit Type</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show26"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Employee Exit Types</label>
															</div>
														</div> -->
														<div id="pop26" class="simplePopup">
															<h1 class="pop_hd">Add New Employee Exit Type</h1>
															<div class="row">
																<form:form action="SaveEmployeeExitType" method="post"
																	commandName="employeeExitTypeVo">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Employee Exit
																				Types </label>
																			<div class="col-sm-9">
																				<textarea name="exitType"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" class="btn btn-info">Save</button>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Leave
																Types </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show27"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage Leave
																	Types</label>
															</div>
														</div>
														<div id="pop27" class="simplePopup">
															<h1 class="pop_hd">Add New Leave Type</h1>
															<div class="row">
																<form action="#" id="StLTForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Types</label>
																			<div class="col-sm-9" id="avai-l-lt"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Leave Types </label>
																			<div class="col-sm-9">
																				<textarea name="leaveType" id="leaveTypeVal"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempLeaveType"
																					id="tempLeaveType" />
																				<button type="submit" onclick="newLT()"
																					id="new-leave-type" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-lt-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Reminder
																Status </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show28"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Reminder Status</label>
															</div>
														</div>
														<div id="pop28" class="simplePopup">
															<h1 class="pop_hd">Add New Reminder Status</h1>
															<div class="row">
																<form action="#" id="StRSForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Status</label>
																			<div class="col-sm-9" id="avai-l-rs"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Reminder
																				Status </label>
																			<div class="col-sm-9">
																				<textarea name="status" id="reminderStatus"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempStatus"
																					id="tempStatus" />
																				<button type="submit" onclick="newRS()"
																					id="new-reminder-status" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-rs-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Employee
																Grade</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show29"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Employee Grade</label>
															</div>
														</div>
														<div id="pop29" class="simplePopup">
															<h1 class="pop_hd">Add New Employee Grade</h1>
															<div class="row">
																<form action="#" id="StEGForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Grades</label>
																			<div class="col-sm-9" id="avai-l-eg"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Employee
																				Grade</label>
																			<div class="col-sm-9">
																				<textarea name="grade" id="employeeGrade"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempGrade" id="tempGrade" />
																				<button type="submit" onclick="newEG()"
																					id="new-employee-grade" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-eg-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<!-- <div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Shift
																Days</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show31"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage Shift
																	Days</label>
															</div>
														</div> -->
														<div id="pop31" class="simplePopup">
															<h1 class="pop_hd">Add New Shift Days</h1>
															<div class="row">
																<form:form action="SaveShiftDay" method="post"
																	commandName="shiftDayVo">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Shift Days</label>
																			<div class="col-sm-9">
																				<textarea name="shiftDay"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" class="btn btn-info">Save</button>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Employee
																Designation</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show30"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Employee Designation</label>
															</div>
														</div>
														<div id="pop30" class="simplePopup">
															<h1 class="pop_hd">Add New Employee Designation</h1>
															<div class="row">
																<form action="#" id="StEDForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Designations</label>
																			<div class="col-sm-9" id="avai-l-ed"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Employee
																				Designation</label>
																			<div class="col-sm-9">
																				<input type="text" name="designation"
																					class="form-control" id="designationVal"
																					placeholder="Employee Designation">
																			</div>
																		</div>

																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Rank</label>
																			<div class="col-sm-9">
																				<input type="text" name="rank"
																					class="form-control numbersonly"
																					id="designationRankVal" placeholder="Rank">
																			</div>
																		</div>

																		<div class="form-group">
																			<input type="hidden" name="tempDes" id="tempDess" />
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" onclick="newED()"
																					class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-ed-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Advance
																Salary Status</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show32"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Advance Salary Status</label>
															</div>
														</div>
														<div id="pop32" class="simplePopup">
															<h1 class="pop_hd">Add New Advance Salary Status</h1>
															<div class="row">
																<form action="#" id="StADSForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Status</label>
																			<div class="col-sm-9" id="avai-l-as"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Advance
																				Salary Status</label>
																			<div class="col-sm-9">
																				<textarea name="status" id="advanceSalaryStatus"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<input type="hidden" name="tempStatus"
																					id="tempStatus" />
																				<button type="submit" onclick="newADS()"
																					id="new-advance-sal-status" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-as-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Overtime
																Status</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show33"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage
																	Overtime Status</label>
															</div>
														</div>
														<div id="pop33" class="simplePopup">
															<h1 class="pop_hd">Add New Overtime Status</h1>
															<div class="row">
																<form action="#" id="StOSForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Status</label>
																			<div class="col-sm-9" id="avai-l-os"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Overtime
																				Status</label>
																			<div class="col-sm-9">
																				<textarea name="status" id="overtimeStatus"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" onclick="newOS()"
																					id="new-overtime-status" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-os-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-3 control-label">Loan
																Status</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-9 control-label ft"><a href="#"
																	class="show34"><i
																		class="fa fa-plus-square-o font_size"></i></a>Manage Loan
																	Status</label>
															</div>
														</div>
														<div id="pop34" class="simplePopup">
															<h1 class="pop_hd">Add New Loan Status</h1>
															<div class="row">
																<form action="#" id="StLSForm">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Available
																				Status</label>
																			<div class="col-sm-9" id="avai-l-ls"></div>
																		</div>
																		<div class="form-group">
																			<label for="inputPassword3"
																				class="col-sm-3 control-label">Loan Status</label>
																			<div class="col-sm-9">
																				<textarea name="loanStatus" id="loanStatus"
																					class="form-control height"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<button type="submit" onclick="newLS()"
																					id="new-loan-status" class="btn btn-info">Save</button>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-offset-3 col-sm-9">
																				<span id="setg-ls-pp-msg" class="hideMe imsuccess"></span>
																			</div>
																		</div>
																	</div>
																</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</dd>
								<dt>
									<a href="#">Notifications </a>
								</dt>
								<dd>
									<div class="row">
										<div class="col-md-6">
											<div class="col-md-12">
												<div class="panel panel-default">
													<form:form action="UpdateManagerReportingNotification"
														method="post" commandName="managerReportingNotificationVo">
														<div class="panel-heading">Notifications</div>
														<div class="panel-body">
															<div class="form-group">
																<label for="inputEmail3" class="col-sm-12 control-label">Notifications
																	to Reporting Manager/Supervisor</label>
																<div class="col-sm-9">
																	<label for="inputEmail3"
																		class="col-sm-12 control-label ft"> <c:choose>
																			<c:when test="${not empty managerRptNotify}">
																				<input type="checkbox" checked
																					name="managerReportingNotification"
																					id="managerReportingNotification" />
																			</c:when>
																			<c:otherwise>
																				<input type="checkbox"
																					name="managerReportingNotification"
																					id="managerReportingNotification" />
																			</c:otherwise>
																		</c:choose> When a employee submits performance evaluation form
																	</label>
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-3">
																	<button type="submit" class="btn btn-info">Save
																		Settings</button>
																</div>
															</div>
														</div>
													</form:form>
												</div>
											</div>
										</div>
										<div class="col-md-6"></div>
									</div>
								</dd>

								<!--
    <dt><a href="#">Logos </a></dt>
    <dd>f </dd>
    <dt><a href="#">IP Restrictions </a></dt>
    <dd>g</dd>
    <dt><a href="#">Data Backup </a></dt>
    <dd>h</dd>
    <dt><a href="#">Interface Language </a></dt>
    <dd>i</dd>-->

							</dl>
						</div>
					</div>
					<!-- <div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Custom Field</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-12">
													<button type="button" class="btn btn-primary flr mtft"
														id="addfield">Add Custom Field</button>
													<br />
												</div>
											</div>
											<table cellpadding="0" cellspacing="0" border="0"
												class="table table-striped table-bordered">
												<thead>
													<tr>
														<th width="10%">S#</th>
														<th width="35%">Custom Field Name</th>
														<th width="35%">Custom Field Type</th>
														<th width="10%">Edit</th>
														<th width="10%">Delete</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
													<tr>
														<td>Trident</td>
														<td><a href="view_project.html"> Internet
																Explorer 4.0</a></td>
														<td>25-2-2015</td>
														<td><a href="add_project.html"><i
																class="fa fa-edit sm"></i> </a></td>
														<td><i class="fa fa-close ss"></i></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="col-md-6" id="add_custom_feild">
									<div class="panel panel-default">
										<div class="panel-heading">Add Custom Fields</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Component
													Name </label>
												<div class="col-sm-9">
													<select class="form-control">
														<option>Shimil</option>
														<option>One</option>
														<option>One</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Field
													Name</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" id="inputPassword3"
														placeholder="Field Name">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Field
													Type </label>
												<div class="col-sm-9">
													<select class="form-control">
														<option>Shimil</option>
														<option>One</option>
														<option>One</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Field
													Order </label>
												<div class="col-sm-9">
													<select class="form-control">
														<option>Shimil</option>
														<option>One</option>
														<option>One</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-9">
													<button type="submit" class="btn btn-info">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> -->
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="docs">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Notifications</div>
										<div class="panel-body" id="div-notiifcation-list">
											<!-- Notification List  -->
										</div>
									</div>
								</div>
								<div class="col-md-6" id="add_notification">
									<div class="panel panel-default">
										<div class="panel-heading">Add Notification</div>
										<div class="panel-body">

											<!-- Notification Form -->
											<div id="div_notification_form"></div>


										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="tps">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Reminders</div>
										<div class="panel-body" id="reminder-wrapper">
											<!-- <button type="button" class="btn btn-primary flr mtft"
												id="addrm">Add Reminder</button>
											<div id="reminder-wrapper"></div> -->
										</div>
									</div>
								</div>
								<div class="row" id="add_rm">
									<form:form action="SaveReminder" method="post"
										commandName="remindersVo" id="reminderForm">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="panel panel-default">
													<div class="panel-heading">Reminders</div>
													<div class="panel-body">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Reminder Title </label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="reminderTitle" placeholder="Field Name">
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Reminder Date </label>
															<div class="col-sm-5">
																<div id="month-list"></div>
															</div>
															<div class="col-sm-4">
																<div id="day-list"></div>
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Reminder Time </label>
															<div class="col-sm-5">
																<div class="input-group date reminderTime"
																	id="timepicker">
																	<input type="text" class="form-control"
																		name="reminderTime" /> <span
																		class="input-group-addon"><span
																		class="glyphicon-time glyphicon"></span> </span>
																</div>
															</div>

														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">Send
																Reminder to</label>
															<div class="col-sm-9 ">
																<select class="form-control chosen-select" multiple
																	data-placeholder="Options" name="reminderTo">
																	<option value=''>-Select-</option>
																	<option value="abc">abc</option>
																	<option value="bca">bca</option>
																</select>
															</div>
														</div>
														<!-- <div id="reminder-status-warpper"></div> -->
														<div class="form-group">
															<label class="col-sm-3 control-label">Reminder
																Status</label>
															<div class="col-sm-9 ">
																<div id="reminder-status-warpper"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="panel panel-default">
													<div class="panel-heading">Reminder Message</div>
													<div class="panel-body">
														<div class="form-group">
															<div class="col-md-12">
																<textarea name="reminderMessage" class=" form-control"
																	placeholder="Enter text ..." style="height: 150px"></textarea>
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-offset-0 col-sm-9">
																<button type="submit" class="btn btn-info">Save</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/settings-ajax.js' />"></script>

	<%-- <script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script> --%>

	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				getAllAvailableLanguage();
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				getAllAvailableSkill();
				$('#pop2').simplePopup();
			});

			$('.show3').click(function() {
				getAllAvailableQDegree();
				$('#pop3').simplePopup();
			});

			$('.show4').click(function() {
				getAllAvailableCTypes();
				$('#pop4').simplePopup();
			});
			$('.show5').click(function() {
				getAllAvailableJTypes();
				$('#pop5').simplePopup();
			});
			$('.show6').click(function() {
				getAllAvailableJFields();
				$('#pop6').simplePopup();
			});
			$('.show7').click(function() {
				getAllAvailableBTypes();
				$('#pop7').simplePopup();
			});
			$('.show8').click(function() {
				getAllAvailablePTypes();
				$('#pop8').simplePopup();
			});
			$('.show9').click(function() {
				getAllAvailableETypes();
				$('#pop9').simplePopup();
			});
			/* $('.show2').click(function() {
				$('#pop2').simplePopup();
			}); */
			$('.show10').click(function() {
				getAllAvailableECategories();
				$('#pop10').simplePopup();
			});
			$('.show11').click(function() {
				$('#pop11').simplePopup();
			});
			$('.show12').click(function() {
				$('#pop12').simplePopup();
			});
			$('.show13').click(function() {
				$('#pop13').simplePopup();
			});
			$('.show14').click(function() {
				getAllAvailableReimbCategories();
				$('#pop14').simplePopup();
			});
			$('.show15').click(function() {
				getAllAvailableCountries();
				$('#pop15').simplePopup();
			});
			$('.show16').click(function() {
				$('#pop16').simplePopup();
			});
			$('.show17').click(function() {
				$('#pop17').simplePopup();
			});
			$('.show18').click(function() {
				$('#pop18').simplePopup();
			});
			$('.show19').click(function() {
				$('#pop19').simplePopup();
			});
			$('.show20').click(function() {
				getAllAvailableAccountTypes();
				$('#pop20').simplePopup();
			});
			$('.show21').click(function() {
				getAllAvailableWorkShifts();
				$('#pop21').simplePopup();
			});
			$('.show22').click(function() {
				getAllAvailableEStatus();
				$('#pop22').simplePopup();
			});
			$('.show23').click(function() {
				getAllAvailableAssets();
				$('#pop23').simplePopup();
			});
			$('.show24').click(function() {
				getAllAvailableTStatus();
				$('#pop24').simplePopup();
			});
			$('.show25').click(function() {
				$('#pop25').simplePopup();
			});
			$('.show26').click(function() {
				$('#pop26').simplePopup();
			});
			$('.show27').click(function() {
				getAllAvailableLTypes();
				$('#pop27').simplePopup();
			});
			$('.show28').click(function() {
				getAllAvailableRStatus();
				$('#pop28').simplePopup();
			});
			$('.show29').click(function() {
				getAllAvailableEGrades();
				$('#pop29').simplePopup();
			});
			$('.show30').click(function() {
				getAllEmployeeDesignation();
				$('#pop30').simplePopup();
			});
			$('.show31').click(function() {
				$('#pop31').simplePopup();
			});
			$('.show32').click(function() {
				getAllAvailableAsvncStatus();
				$('#pop32').simplePopup();
			});
			$('.show33').click(function() {
				getAllAvailableOStatus();
				$('#pop33').simplePopup();
			});
			$('.show34').click(function() {
				getAllAvailableLStatus();
				$('#pop34').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").addClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
	<script>
		$('#js-smart-tabs').smartTabs();
		$('#js-smart-tabs--tabs').smartTabs({
			layout : 'tabs'
		});
		$('#js-smart-tabs--accordion').smartTabs({
			layout : 'accordion'
		});
	</script>
	<script>
		$(document).ready(function() {

			$("#admin_ss").click(function() {
				$("#add_administartor").show();
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addfield").click(function() {
				$("#add_custom_feild").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addrm").click(function() {
				$("#add_rm").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>
</body>
</html>
