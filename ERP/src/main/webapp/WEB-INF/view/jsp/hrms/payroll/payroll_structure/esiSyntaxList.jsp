<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="form-group">
	<label for="inputPassword3" class="col-sm-3 control-label"> ESI
		Eligibility Range</label>
	<div class="col-sm-4">
		<select class="form-control range" name="range">
			<option value="Below" ${esiSyntax.range == 'Below' ? 'selected' : ''}>Below</option>
			<option value="Above" ${esiSyntax.range == 'Above' ? 'selected' : ''}>Above</option>
		</select>
	</div>
	<div class="col-sm-5">
		<input type="text" name="amount" class="form-control amount"
			placeholder="Amount" value="${esiSyntax.amount}" />
	</div>
</div>
<br />
<br />
<br />
<br />
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="50%">Employee Contribution</th>
			<th width="50%">Employer Contribution</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><input type="text" name="employeeSyntax" id="ps-kw-exp"
				class="form-control typeahead employeeSyntax"
				value="${esiSyntax.employeeSyntax}" /></td>
			<td><input type="text" name="employerSyntax" id="ps-kw-exp"
				class="form-control typeahead employerSyntax"
				value="${esiSyntax.employerSyntax}" /></td>
	</tbody>
</table>
<div class="col-sm-6 col-xs-offset-6">
	<input type="hidden" name="esiSyntaxId" id="esiSyntaxId"
		value="${esiSyntax.esiSyntaxId}" />
	<button type="submit" onclick="saveESISyntax()" class="btn btn-info">Save</button>
</div>

<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
