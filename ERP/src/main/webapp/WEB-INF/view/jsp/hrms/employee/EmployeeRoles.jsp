<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<h2 class="role_hd">Employee Roles</h2>
		<!-- <h3>Select All : <input type="checkbox" id="selecctall"></h3> -->
		<form:form method="POST" action="saveEmployeeRoles.do"
			commandName="rolesVo">
			<form:hidden path="employeeId" />
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapseOne">
							<h4 class="panel-title">Home</h4> <input type="hidden"
							name="authEmployeeId" value="${authEmployeeDetails.employeeId}">
						</a>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body">



							<form:hidden path="employeeId" />
							<table class="table table-striped no-margn line">
								<tbody>
									<tr>

										<td class="hd_tb"><span class="WhiteHeading">Role
												Type</span></td>
										<!-- <td class="hd_tb"><span class="WhiteHeading"><input type="checkbox" id="selecctall"></span></td> -->
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">View</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Add</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Update</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Delete</span></td>
									</tr>

									<tr>
										<td><span style="float: right;"><strong>Select
													All</strong></span></td>
										<td><input type="checkbox" id="selecctall_home"></td>
									</tr>

									<c:forEach items="${role.homeVos }" var="v">
										<tr>
											<td colspan="5"><span class="title3"><strong>${v.title }</strong></span>
											</td>
										</tr>
										<c:forEach items="${v.list }" var="r">
											<tr>
												<td>${r.roleType }</td>
												<td align="center"><form:checkbox path="viewIds"
														cssClass="checkbox1_home" value="${r.roleTypeId }" /></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</c:forEach>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapseTwo">
							<h4 class="panel-title">Organization</h4>
						</a>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table table-striped no-margn line">
								<tbody>
									<tr>
										<td class="hd_tb"><span class="WhiteHeading">Role
												Type</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">View</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Add</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Update</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Delete</span></td>
									</tr>
									<tr>
										<td><span style="float: right;"><strong>Select
													All</strong></span></td>
										<td><input type="checkbox" id="selecctall_organization"></td>
									</tr>
									<c:forEach items="${role.organisationVos }" var="v">
										<tr>
											<td colspan="5"><span class="title3"><strong>${v.title }</strong></span></td>

										</tr>
										<c:forEach items="${v.list  }" var="r">
											<c:choose>
												<c:when test="${r.roleType == 'Department Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
												 cssClass="checkbox1_organization"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Project Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_organization"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when
													test="${r.roleType == 'Organization Policy Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_organization"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Branch Map'}">
												</c:when>
												<c:when test="${r.roleType == 'Meeting Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_organization"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'System Log'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="addIds"
													 cssClass="checkbox1_organization"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_organization"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_organization"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Branch Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_organization"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:otherwise>
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="updateIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_organization"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</c:forEach>
								</tbody>
							</table>

						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapseThree">
							<h4 class="panel-title">Recruitment</h4>
						</a>
					</div>
					<div id="collapseThree" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table table-striped no-margn line">
								<tbody>
									<tr>
										<td class="hd_tb"><span class="WhiteHeading">Role
												Type</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">View</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Add</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Update</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Delete</span></td>
									</tr>
									<tr>
										<td><span style="float: right;"><strong>Select
													All</strong></span></td>
										<td><input type="checkbox" id="selecctall_recruitment"></td>
									</tr>
									<c:forEach items="${role.recruitmentVos }" var="v">
										<tr>
											<td colspan="5"><span class="title3"><strong>${v.title }</strong></span></td>
										</tr>
										<c:forEach items="${v.list  }" var="r">
											<c:choose>
												<c:when test="${r.roleType == 'Job Post Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
												 cssClass="checkbox1_recruitment"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Candidate Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
												 cssClass="checkbox1_recruitment"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Job Test Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
												 cssClass="checkbox1_recruitment"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Job Interview Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_recruitment"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:otherwise>
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="updateIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_recruitment"
																value="${r.roleTypeId }" /></td>
													</tr>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</c:forEach>
								</tbody>
							</table>

						</div>
					</div>
				</div>


				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapsefive">
							<h4 class="panel-title">Employees</h4>
						</a>
					</div>
					<div id="collapsefive" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table table-striped no-margn line">
								<tbody>
									<tr>
										<td class="hd_tb"><span class="WhiteHeading">Role
												Type</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">View</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Add</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Update</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Delete</span></td>
									</tr>
									<tr>
										<td><span style="float: right;"><strong>Select
													All</strong></span></td>
										<td><input type="checkbox" id="selecctall_Employees"></td>
									</tr>
									<c:forEach items="${role.employeesVos }" var="v">
										<tr>
											<td colspan="5"><span class="title3"><strong>${v.title }</strong></span></td>
										</tr>
										<c:forEach items="${v.list  }" var="r">
											<c:choose>
												<c:when test="${r.roleType == 'Employee Roles'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center">
															<%-- <form:checkbox path="viewIds"
												 cssClass="checkbox1_Employees"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
												 cssClass="checkbox1_Employees"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Employee Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Resignation Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Termination Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Transfer Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Employee Joining Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
												 cssClass="checkbox1_Employees"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Resignation Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Assignment Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
												 cssClass="checkbox1_Employees"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Travel Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Employee Notes'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Employee Status Update'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Employee Qualifications'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Employee Work Experiences'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Employee Languages'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_Employees"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:otherwise>
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="updateIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Employees" value="${r.roleTypeId }" /></td>
													</tr>
												</c:otherwise>
											</c:choose>

										</c:forEach>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapsesix">
							<h4 class="panel-title">Time Sheet</h4>
						</a>
					</div>
					<div id="collapsesix" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table table-striped no-margn line">
								<tbody>
									<tr>
										<td class="hd_tb"><span class="WhiteHeading">Role
												Type</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">View</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Add</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Update</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Delete</span></td>
									</tr>
									<tr>
										<td><span style="float: right;"><strong>Select
													All</strong></span></td>
										<td><input type="checkbox" id="selecctall_time_sheet"></td>
									</tr>
									<c:forEach items="${role.timeSheetVos }" var="v">
										<tr>
											<td colspan="5"><span class="title3"><strong>${v.title }</strong></span></td>
										</tr>
										<c:forEach items="${v.list  }" var="r">
											<c:choose>
												<c:when test="${r.roleType == 'Work Shifts'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center">
															<%-- <form:checkbox path="viewIds"
												 cssClass="checkbox1_time_sheet"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_time_sheet" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
												 cssClass="checkbox1_time_sheet"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_time_sheet"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Attendance Status'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center">
															<%-- <form:checkbox path="viewIds"
												 cssClass="checkbox1_time_sheet"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_time_sheet" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
												 cssClass="checkbox1_time_sheet"		value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_time_sheet"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:otherwise>
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_time_sheet" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_time_sheet" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="updateIds"
																cssClass="checkbox1_time_sheet" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_time_sheet" value="${r.roleTypeId }" /></td>
													</tr>
												</c:otherwise>
											</c:choose>

										</c:forEach>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>


				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapseeight">
							<h4 class="panel-title">Payroll</h4>
						</a>
					</div>
					<div id="collapseeight" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table table-striped no-margn line">
								<tbody>
									<tr>
										<td class="hd_tb"><span class="WhiteHeading">Role
												Type</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">View</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Add</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Update</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Delete</span></td>
									</tr>
									<tr>
										<td><span style="float: right;"><strong>Select
													All</strong></span></td>
										<td><input type="checkbox" id="selecctall_Payroll"></td>
									</tr>
									<c:forEach items="${role.payrollVos }" var="v">
										<tr>
											<td colspan="5"><span class="title3"><strong>${v.title }</strong></span></td>
										</tr>
										<c:forEach items="${v.list  }" var="r">
											<c:choose>
												<c:when test="${r.roleType =='Payroll Structure' }">
													<tr>
														<td>${r.roleType }</td>
														<td align="center">
															<%-- <form:checkbox path="viewIds"
													 cssClass="checkbox1_Payroll"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Payroll"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_Payroll"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Loan Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Payroll"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Pay Salary'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center">
															<%-- <form:checkbox path="viewIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="updateIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" />
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType == 'Reimbursements Documents'}">
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Payroll"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
													</tr>
												</c:when>
												<c:when test="${r.roleType =='Salary' }">
													<tr>
														<td>${r.roleType }</td>
														<td align="center">
															<%-- <form:checkbox path="viewIds"
													 cssClass="checkbox1_Payroll"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
														<td align="center">
															<%-- <form:checkbox path="updateIds"
													 cssClass="checkbox1_Payroll"	value="${r.roleTypeId }" /> --%>
														</td>
														<td align="center">
															<%-- <form:checkbox path="deleteIds"
													 cssClass="checkbox1_Payroll"	value="${r.roleTypeId }" /> --%>
														</td>
													</tr>
												</c:when>
												<c:otherwise>
													<tr>
														<td>${r.roleType }</td>
														<td align="center"><form:checkbox path="viewIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="addIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="updateIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
														<td align="center"><form:checkbox path="deleteIds"
																cssClass="checkbox1_Payroll" value="${r.roleTypeId }" /></td>
													</tr>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>



				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapseseven">
							<h4 class="panel-title">Reports</h4>
						</a>
					</div>
					<div id="collapseseven" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table table-striped no-margn line">
								<tbody>
									<tr>
										<td class="hd_tb"><span class="WhiteHeading">Role
												Type</span></td>
										<!-- <td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">View</span></td> -->
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Generate</span></td>
										<!-- <td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Update</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Delete</span></td> -->
									</tr>
									<tr>
										<td><span style="float: right;"><strong>Select
													All</strong></span></td>
										<td><input type="checkbox" id="selecctall_Reports"></td>
									</tr>
									<c:forEach items="${role.reportsVos }" var="v">
										<tr>
											<td colspan="5"><span class="title3"><strong>${v.title }</strong></span></td>
										</tr>
										<c:forEach items="${v.list }" var="r">
											<tr>
												<td>${r.roleType }</td>
												<td align="center"><form:checkbox path="viewIds"
														cssClass="checkbox1_Reports" value="${r.roleTypeId }" /></td>
												<%-- <td align="center"><form:checkbox path="addIds"
													 cssClass="checkbox1_Reports"	value="${r.roleTypeId }" /></td>
												<td align="center"><form:checkbox path="updateIds"
													 cssClass="checkbox1_Reports"	value="${r.roleTypeId }" /></td>
												<td align="center"><form:checkbox path="deleteIds"
													 cssClass="checkbox1_Reports"	value="${r.roleTypeId }" /></td> --%>
											</tr>
										</c:forEach>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#erpCollapse">
							<h4 class="panel-title">ERP</h4>
						</a>
					</div>
					<div id="erpCollapse" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table table-striped no-margn line">
								<tbody>
									<tr>
										<td class="hd_tb"><span class="WhiteHeading">Role
												Type</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">View</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Add</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Update</span></td>
										<td width="2%" align="center" class="hd_tb"><span
											class="WhiteHeading">Delete</span></td>
									</tr>
									<tr>
										<td><span style="float: right;"><strong>Select
													All</strong></span></td>
										<td><input type="checkbox" id="selecctall_erp"></td>
									</tr>
									<c:forEach items="${role.erpVos }" var="v">
										<tr>
											<td colspan="5"><span class="title3"><strong>${v.title }</strong></span></td>

										</tr>
										<c:forEach items="${v.list  }" var="r">
											<tr>
												<td>${r.roleType }</td>
												<td align="center"><form:checkbox path="viewIds"
														cssClass="checkbox1_erp" value="${r.roleTypeId }" /></td>
												<td align="center"><%-- <form:checkbox path="addIds"
														cssClass="checkbox1_organization" value="${r.roleTypeId }" /> --%></td>
												<td align="center">
													<%-- <form:checkbox path="updateIds"
												 cssClass="checkbox1_organization"		value="${r.roleTypeId }" /> --%>
												</td>
												<td align="center"><%-- <form:checkbox path="deleteIds"
														cssClass="checkbox1_organization" value="${r.roleTypeId }" /> --%></td>
											</tr>
										</c:forEach>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>


				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<button type="submit" class="btn btn-info flr">Save</button>
					</div>
				</div>
			</div>
		</form:form>
	</div>





	<!-- JQuery v1.9.1 -->

	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script src=""></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<%-- <script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script> --%>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");

			$('#selecctall').click(function(event) { //on click 
				if (this.checked) { // check select status
					$('.checkbox1').each(function() { //loop through each checkbox
						this.checked = true; //select all checkboxes with class "checkbox1"               
					});
				} else {
					$('.checkbox1').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                       
					});
				}
			});

			//Home
			$('#selecctall_home').click(function(event) { //on click 
				if (this.checked) { // check select status
					$('.checkbox1_home').each(function() { //loop through each checkbox
						this.checked = true; //select all checkboxes with class "checkbox1"               
					});
				} else {
					$('.checkbox1_home').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                       
					});
				}
			});

			//Organization
			$('#selecctall_organization').click(function(event) { //on click 
				if (this.checked) { // check select status
					$('.checkbox1_organization').each(function() { //loop through each checkbox
						this.checked = true; //select all checkboxes with class "checkbox1"               
					});
				} else {
					$('.checkbox1_organization').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                       
					});
				}
			});

			//Recruitment
			$('#selecctall_recruitment').click(function(event) { //on click 
				if (this.checked) { // check select status
					$('.checkbox1_recruitment').each(function() { //loop through each checkbox
						this.checked = true; //select all checkboxes with class "checkbox1"               
					});
				} else {
					$('.checkbox1_recruitment').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                       
					});
				}
			});

			//Employees
			$('#selecctall_Employees').click(function(event) { //on click 
				if (this.checked) { // check select status
					$('.checkbox1_Employees').each(function() { //loop through each checkbox
						this.checked = true; //select all checkboxes with class "checkbox1"               
					});
				} else {
					$('.checkbox1_Employees').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                       
					});
				}
			});

			//Time Sheet
			$('#selecctall_time_sheet').click(function(event) { //on click 
				if (this.checked) { // check select status
					$('.checkbox1_time_sheet').each(function() { //loop through each checkbox
						this.checked = true; //select all checkboxes with class "checkbox1"               
					});
				} else {
					$('.checkbox1_time_sheet').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                       
					});
				}
			});

			//Payroll
			$('#selecctall_Payroll').click(function(event) { //on click 
				if (this.checked) { // check select status
					$('.checkbox1_Payroll').each(function() { //loop through each checkbox
						this.checked = true; //select all checkboxes with class "checkbox1"               
					});
				} else {
					$('.checkbox1_Payroll').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                       
					});
				}
			});

			//Reports
			$('#selecctall_Reports').click(function(event) { //on click 
				if (this.checked) { // check select status
					$('.checkbox1_Reports').each(function() { //loop through each checkbox
						this.checked = true; //select all checkboxes with class "checkbox1"               
					});
				} else {
					$('.checkbox1_Reports').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                       
					});
				}
			});
			$('#selecctall_erp').click(function(event) { //on click 
				if (this.checked) { // check select status
					$('.checkbox1_erp').each(function() { //loop through each checkbox
						this.checked = true; //select all checkboxes with class "checkbox1"               
					});
				} else {
					$('.checkbox1_erp').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                       
					});
				}
			});
		});
	</script>
</body>
</html>