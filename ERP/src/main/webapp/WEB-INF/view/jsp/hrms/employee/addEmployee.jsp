<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
#user_save_success {
	display: none
}

#user_save_failed {
	display: none
}

#notification_save_success {
	display: none
}

#notification_save_failed {
	display: none
}

#reporting_save_failed {
	display: none
}

#reporting_save_success {
	display: none
}

#personal_save_success {
	display: none
}

#personal_save_failed {
	display: none
}

#leave_save_success {
	display: none
}

#leave_save_failed {
	display: none
}

#additional_save_success {
	display: none
}

#additional_save_failed {
	display: none
}

#address_save_success {
	display: none
}

#address_save_failed {
	display: none
}

#phone_save_success {
	display: none
}

#phone_save_failed {
	display: none
}

#emergency_save_success {
	display: none
}

#emergency_save_failed {
	display: none
}

#notes_save_success {
	display: none
}

#notes_save_failed {
	display: none
}

#qlfctn_save_success {
	display: none
}

#qlfctn_save_failed {
	display: none
}

#lnguage_save_success {
	display: none
}

#lnguage_save_failed {
	display: none
}

#exprnce_save_success {
	display: none
}

#exprnce_save_failed {
	display: none
}

#username_exists {
	display: none
}

#bank_save_success {
	display: none
}

#bank_save_failed {
	display: none
}

#benefit_save_success {
	display: none
}

#benefit_save_failed {
	display: none
}

#assets_save_success {
	display: none
}

#assets_save_failed {
	display: none
}

#uniform_save_success {
	display: none
}

#uniform_save_failed {
	display: none
}

#skill_save_success {
	display: none
}

#skill_save_failed {
	display: none
}

#rfrnce_save_success {
	display: none
}

#rfrnce_save_failed {
	display: none
}

#dpndnts_save_success {
	display: none
}

#dpndnts_save_failed {
	display: none
}

#skin_save_success {
	display: none
}

#skin_save_failed {
	display: none
}

#status_save_success {
	display: none
}

#status_save_failed {
	display: none
}
</style>
</head>
<body>

	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Employees</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<a href="employees.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>




		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<c:if
						test="${ rolesMap.roles['Employee Detailed Information'].add}">
						<li role="presentation"><a href="#docs" role="tab"
							data-toggle="tab">Additional Information</a></li>
					</c:if>

					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>



				<div class="tab-content">

					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">

										<div class="panel panel-default">
											<div class="panel-heading">Employee Information</div>
											<div class="panel-body">

												<form:form action="saveEmployeeInfo.do" method="POST"
													id="save_employeeInfo_form" commandName="employeeVo"
													enctype="multipart/form-data">
													<form:hidden path="employeeId" id="empid" />
													<form:hidden path="profilePicHidden" />

													<div class="form-group">
														<div class="photo_show">
															<img id="image" src='<c:url value="${employeeVo.profilePic }" />' />
														</div>
														<input type="file" accept="image/*" id="files"
															name="emloyee_profile_pic" class="choose_btn" />
													</div>
													<c:if test="${! empty employeeVo.employeeCode }">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Employee Code <span
																class="stars">*</span>
															</label>
															<div class="col-sm-9">
																<form:input class="form-control " id="emp_code"
																	style="background-color: #ffffff !important;"
																	path="employeeCode" />
															</div>
														</div>
													</c:if>

													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">Employee
															Type <span class="stars">*</span>
														</label>
														<div class="col-sm-9 ">
															<form:select path="employeeTypeId"
																data-placeholder="-Select Employee Type-"
																cssClass="form-control">
																<option value="">-Select Employee Type-</option>
																<form:options itemLabel="employeeType"
																	items="${employeeType }" itemValue="employeeTypeId" />
															</form:select>
														</div>


													</div>



													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Company<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select path="employeeCategoryId"
																data-placeholder="-Select Category-"
																cssClass="form-control">
																<option value="">-Select Category-</option>
																<form:options items="${employeeCategory }"
																	itemLabel="employeeCategory"
																	itemValue="employeeCategoryId" />
															</form:select>
														</div>
													</div>


													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">Designation
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select path="designationId" cssClass="form-control"
																data-placeholder="-Select Designation-" id="des_id_emp"
																onchange="getSuperiorsSubordinate()">
																<option value="">-Select Designation-</option>
																<form:options itemLabel="designation"
																	items="${employeeDesignation }" itemValue="id" />
															</form:select>
														</div>


													</div>





													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Branch <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select path="employeeBranchId"
																data-placeholder="-Select Branch-"
																cssClass="form-control ">
																<option value="">-Select Branch-</option>
																<form:options itemLabel="branchName"
																	items="${branches }" itemValue="branchId" />
															</form:select>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Department <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select path="employeeDepartmentId"
																data-placeholder="-Select Department-"
																cssClass="form-control ">
																<option value="">-Select Department-</option>
																<form:options itemLabel="name" items="${departments }"
																	itemValue="id" />
															</form:select>
														</div>
													</div>




													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3  control-label">Grade
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select path="employeeGradeId"
																data-placeholder="-Select Grade-"
																cssClass="form-control ">
																<option value="">-Select Grade-</option>
																<form:options itemLabel="grade" items="${grade }"
																	itemValue="id" />
															</form:select>
														</div>


													</div>






													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Work
															Shift <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select path="workShiftId"
																data-placeholder="-Select Work Shift-"
																cssClass="form-control">
																<option value="">-Select Work Shift-</option>
																<form:options itemLabel="workShift" items="${shifts }"
																	itemValue="id" />
															</form:select>
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">With
															Payroll <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<!-- <input type="radio" value="true" name="payroll">Yes $nbsp;
													<input type="radio" value="false" name="payroll">No -->
															<form:radiobutton path="withPayroll" value="true" />
															Yes &nbsp;
															<form:radiobutton path="withPayroll" value="false" />
															No &nbsp;
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Salary Type <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select path="salaryTypeId"
																data-placeholder="-Select Slary Type-"
																cssClass="form-control chosen-select">
																<option></option>
																<form:options itemLabel="type" items="${salaryTypes }"
																	itemValue="id" />
															</form:select>
														</div>
													</div>



													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" id="emp_inf_sve_button"
																class="btn btn-info">Save</button>
														</div>
													</div>
												</form:form>


											</div>
										</div>
									</div>

									<form:form commandName="employeeVo">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Employee Personal
													Information</div>
												<div class="panel-body">



													<div class="form-group">
														<label class="col-sm-3 control-label">Salutation <span
															class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select id="emp_uprofile_salut" path="salutation"
																data-placeholder="-Salutation-" cssClass="form-control">
																<form:option value="Mr.">Mr</form:option>
																<form:option value="Mrs.">Mrs</form:option>
																<form:option value="Miss">Miss</form:option>
															</form:select>
														</div>

													</div>



													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">First
															Name <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input path="firstName"
																cssClass="form-control alphabets"
																id="emp_uprofile_first_name" />
														</div>
													</div>



													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Last
															Name <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input path="lastName"
																cssClass="form-control alphabets"
																id="emp_uprofile_last_name"></form:input>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Date of
															Birth <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<div class='input-group date' id="datepickersss">
																<form:input path="dateOfBirth" cssClass="form-control"
																	style="background-color: #ffffff !important;"
																	readonly="true" id="emp_uprofile_dob"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon glyphicon-calendar"></span> </span>
															</div>
														</div>

													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Gender
															<span class="stars">*</span>
														</label>
														<div class="col-sm-4">
															<form:radiobutton path="gender" value="Male" />
															&nbsp;Male
															<div id="gndr_select" class="alert_box">
																<div class="triangle-up"></div>
																Please select gender
															</div>
														</div>
														<div class="col-sm-4">
															<form:radiobutton path="gender" value="Female" />
															&nbsp;Female
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Blood Group
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select id="emp_uprofile_blood" path="bloodGroupId"
																data-placeholder="--Select Blood Group--"
																cssClass="form-control chosen-select">
																<option></option>
																<form:options items="${blood }" itemLabel="groupName"
																	itemValue="bloodGroupId" />

															</form:select>
															<div id="blood_select" class="alert_box">
																<div class="triangle-up"></div>
																Please select blood group
															</div>

														</div>

													</div>




													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Religion
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select id="emp_uprofile_relegion" path="relegionId"
																data-placeholder="--Select Relegion--"
																cssClass="form-control chosen-select">
																<option></option>
																<form:options items="${relegion }" itemLabel="relegion"
																	itemValue="relegionId" />
															</form:select>
															<div id="relegion_select" class="alert_box">
																<div class="triangle-up"></div>
																Please select religion
															</div>
														</div>

													</div>






													<div class="form-group">
														<label class="col-sm-3 control-label">Nationality
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select id="emp_uprofile_nationality"
																path="nationalityId"
																data-placeholder="--Select Nationality--"
																cssClass="form-control chosen-select">
																<option></option>
																<form:options items="${country }" itemLabel="name"
																	itemValue="countryId" />
															</form:select>
															<div id="country_select" class="alert_box">
																<div class="triangle-up"></div>
																Please select country
															</div>
														</div>
													</div>







													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_uprofile_sbmit"
																onclick="vldateUserPersonalEmp()" class="btn btn-info">Save</a>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-12" id="personal_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="personal_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Employee User Information</div>
												<div class="panel-body">


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Allow
															Employee Login </label>
														<div class="col-sm-9">
															<form:checkbox path="allowLogin" id="emp_user_allow"
																class=" flt_l" />
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">E-mail
															address <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input path="email" cssClass="form-control paste"
																id="emp_user_email"  onblur="setUserName(this.value)" ></form:input>
															<div id="valid_email_errr" class="alert_box">
																<div class="triangle-up"></div>
																Please enter valid email address
															</div>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Username
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input path="userName" cssClass="form-control paste"
																id="emp_user_name" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Password
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:password path="password"
																cssClass="form-control paste" id="emp_user_pwd" />
														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="save_emp_user_info" onclick="vldteUserPassEmp()"
																class="btn btn-info">Save</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="user_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="user_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
														<div class="col-sm-12" id="username_exists">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Username already
																exists..!!
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>


										<%-- <div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Employee Notifications</div>
												<div class="panel-body">


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notification
															By email </label>
														<div class="col-sm-9">
															<form:checkbox path="notifyByEmail" cssClass="flt_l"
																id="emp_notify_chk" />
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="notification_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="notification_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>
												</div>
											</div>
										</div> --%>



										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Employee Reporting</div>
												<div class="panel-body">

													<div class="form-group">
														<label class="col-sm-3 control-label"> Superiors</label>
														<div class="col-sm-9 ">
															<form:select cssClass="form-control chosen-select"
																multiple="multiple" path="superiorsIds"
																data-placeholder="-Superiors-" id="emp_spriors_ids">
																<%-- <form:options items="${employees }"
																	itemLabel="employeeCode" itemValue="employeeId" /> --%>
															</form:select>
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label"> Subordinate
														</label>
														<div class="col-sm-9 ">
															<form:select cssClass="form-control chosen-select"
																multiple="multiple" path="subordinatesIds"
																data-placeholder="-Subordinates-" id="emp_sbrdnts_ids">
																<%-- <form:options items="${employees }"
																	itemLabel="employeeCode" itemValue="employeeId" /> --%>

															</form:select>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_spriors_sbrdnts_btn" class="btn btn-info">Save</a>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-12" id="reporting_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="reporting_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>






										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Leave</div>
												<div class="panel-body">
													<form:hidden path="leaveId" id="leaveId" />
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label"> Title <span
															class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select path="leaveTitleId"
																data-placeholder="-Select Leave-"
																onchange="getLeaveDetails()" id="save_emp_leave_title"
																cssClass="form-control chosen-select">
																<option></option>
																<c:forEach items="${leaves }" var="l">
																	<option value="${l.leaveTypeId }">${l.leaveType }</option>
																</c:forEach>
															</form:select>
															<div id="leave_select_title" class="alert_box">
																<div class="triangle-up"></div>
																Please select leave
															</div>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label "> Leaves
															Allowed Per Year <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input path="leaves"
																class="form-control numbersonly"
																id="save_emp_leave_leaves" />
														</div>
													</div>

													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label"> Leave
															type <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select path="leaveType" id="save_emp_leave_type"
																data-placeholder="-Select Leave Type-"
																cssClass="form-control chosen-select">
																<option value="Paid">Paid</option>
																<option value="Unpaid">Unpaid</option>
															</form:select>
														</div>
													</div>

													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label"> Leave
															Carry Over <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="radio" name="leave" value="yes"> Yes
															&nbsp; <input type="radio" name="leave" value="No">
															No
															<div id="leave_select" class="alert_box">
																<div class="triangle-up"></div>
																Please select any option
															</div>
														</div>
													</div>


													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label"> Status
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select path="leaveStatus"
																id="save_emp_leave_status" cssClass="form-control">
																<option value="Active">Active</option>
																<option value="Inactive">Inactive</option>

															</form:select>
														</div>
													</div>



													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="save_emp_leave_btn"
																onclick="validateEmployeeLEave()" class="btn btn-info">Save</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="leave_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="leave_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
								</div>

								<div class="col-md-6">

									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Employee Additional
												Information</div>
											<div class="panel-body">




												<div class="form-group">
													<label class="col-sm-3 control-label">Joining Date
														<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class='input-group date' id="datepicker">
															<form:input path="joiningDate" cssClass="form-control"
																style="background-color: #ffffff !important;"
																readonly="true" id="emp_add_info_joindate"
																data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>

												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">PF
														ID</label>
													<div class="col-sm-9">
														<form:input path="pfId" cssClass="form-control"
															id="emp_add_info_pf" />
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">ESI
														ID</label>
													<div class="col-sm-9">
														<form:input path="esiId" cssClass="form-control"
															id="emp_add_info_esi" />
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Employee
														Tax Number </label>
													<div class="col-sm-9">
														<form:input path="employeeTaxNumber"
															cssClass="form-control" id="emp_add_info_govt_taxnum" />
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Government
														Id / Social Security </label>
													<div class="col-sm-9">
														<form:input path="goovernmentId" cssClass="form-control"
															id="emp_add_info_govt_idnum" />
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Passport
														Expiration</label>
													<div class="col-sm-9">
														<div class='input-group date' id="datepickers">
															<form:input path="passportExpiration"
																cssClass="form-control" readonly="true"
																style="background-color: #ffffff !important;"
																id="emp_add_info_pasprtexprdate"
																data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>

												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Passport
														Number</label>
													<div class="col-sm-9">
														<form:input path="passportNumber" cssClass="form-control"
															id="emp_add_info_pasprtnum" />
													</div>
												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label">Driving
														License Expiration</label>
													<div class="col-sm-9">
														<div class='input-group date' id="datepickerss">
															<form:input path="licenceExpiration"
																cssClass="form-control" readonly="true"
																style="background-color: #ffffff !important;"
																id="emp_add_info_dlexprnum"
																data-date-format="DD/MM/YYYY"></form:input>
															<span class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>

												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Driving
														License Number</label>
													<div class="col-sm-9">
														<form:input path="licenceNumber" cssClass="form-control"
															id="emp_add_info_dlnum" />
													</div>
												</div>











												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<a id="emp_add_info_btn" onclick="vldteAdditionalInfo()"
															class="btn btn-info">Save</a>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12" id="additional_save_success">
														<div class="box_success">
															<i class="fa fa-check ffss"></i>Saved successfully...
														</div>
													</div>
													<div class="col-sm-12" id="additional_save_failed">
														<div class="box_error">
															<i class="fa fa-times ffss"></i>Failed to save..!!
														</div>
													</div>
												</div>


											</div>
										</div>
									</div>


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Permanent Contact
												Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Address
														<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="address" cssClass="form-control"
															id="emp_prmnt_cntct_addr" />
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">City
														<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="city" cssClass="form-control alphabets"
															id="emp_prmnt_cntct_city" />
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														State / Province <span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="state" cssClass="form-control alphabets"
															id="emp_prmnt_cntct_stt" />
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Zip
														Code / Postal Code <span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="zipCode"
															cssClass="form-control numbersonly"
															id="emp_prmnt_cntct_zip" />
													</div>
												</div>



												<div class="form-group">
													<label class="col-sm-3 control-label">Country <span
														class="stars">*</span></label>
													<div class="col-sm-9">
														<form:select class="form-control chosen-select"
															id="emp_prmnt_cntct_cntry" path="countryId"
															data-placeholder="--Select Country--">
															<option></option>
															<form:options items="${country }" itemLabel="name"
																itemValue="countryId" />
														</form:select>
														<div id="cntry_cntct_select" class="alert_box">
															<div class="triangle-up"></div>
															Please choose country
														</div>
													</div>
												</div>


												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<a id="emp_prmnt_cntct_btn" onclick="vlidteAddress()"
															class="btn btn-info">Save</a>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12" id="address_save_success">
														<div class="box_success">
															<i class="fa fa-check ffss"></i>Saved successfully...
														</div>
													</div>
													<div class="col-sm-12" id="address_save_failed">
														<div class="box_error">
															<i class="fa fa-times ffss"></i>Failed to save..!!
														</div>
													</div>
												</div>

											</div>
										</div>
									</div>


									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Phone Number</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Home
														Phone Number</label>
													<div class="col-sm-9">
														<form:input path="homePhoneNumber"
															cssClass="form-control numbersonly" id="emp_phone_home" />
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Office
														Phone Number </label>
													<div class="col-sm-9">
														<form:input path="officePhoneNumber"
															cssClass="form-control numbersonly" id="emp_phone_office" />
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Mobile
														Number <span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="mobileNumber"
															cssClass="form-control numbersonly" id="emp_phone_mobie" />
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<a id="emp_phone_btn" class="btn btn-info">Save</a>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12" id="phone_save_success">
														<div class="box_success">
															<i class="fa fa-check ffss"></i>Saved successfully...
														</div>
													</div>
													<div class="col-sm-12" id="phone_save_failed">
														<div class="box_error">
															<i class="fa fa-times ffss"></i>Failed to save..!!
														</div>
													</div>
												</div>

											</div>
										</div>
									</div>



									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Emergency Contact</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Emergency
														Contact Person <span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="emergencyPerson"
															cssClass="form-control alphabets"
															id="emp_emrgncy_cntct_person" />
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Relationship
														<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="emergencyRelation"
															cssClass="form-control alphabets"
															id="emp_emrgncy_cntct_relation" />
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Phone
														Number <span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="emergencyMobile"
															cssClass="form-control numbersonly"
															id="emp_emrgncy_cntct_phone" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<a id="emp_emrgncy_cntct_btn"
															onclick="validatEmergencyCntct()" class="btn btn-info">Save</a>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12" id="emergency_save_success">
														<div class="box_success">
															<i class="fa fa-check ffss"></i>Saved successfully...
														</div>
													</div>
													<div class="col-sm-12" id="emergency_save_failed">
														<div class="box_error">
															<i class="fa fa-times ffss"></i>Failed to save..!!
														</div>
													</div>
												</div>

											</div>
										</div>
									</div>






									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
														<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:textarea path="notes" cssClass="form-control height"
															id="emp_add_inf_notes" />
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employeeVo. createdBy}</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${employeeVo.createdOn }
														</label>
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<a id="emp_add_info_btn_uodt" class="btn btn-info">Save</a>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12" id="notes_save_success">
														<div class="box_success">
															<i class="fa fa-check ffss"></i>Saved successfully...
														</div>
													</div>
													<div class="col-sm-12" id="notes_save_failed">
														<div class="box_error">
															<i class="fa fa-times ffss"></i>Failed to save..!!
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>



						</div>
					</div>
					</form:form>

					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-6">
									<c:if test="${ rolesMap.roles['Employee Qualifications	'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Add Qualifications</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Degree <span class="stars">*</span>
														</label>
														<div class="col-sm-8 col-xs-11">
															<select class="form-control" id="emp_qulfctn_add_dgre"
																onchange="getEmpQualification()">
																<option value="">--Select Degree--</option>
																<c:forEach items="${degree }" var="s">
																	<option value="${s.degreeId }" label="${s.degreeName }" />
																</c:forEach>
															</select>
															<div id="degree_select" class="alert_box">
																<div class="triangle-up"></div>
																Please select degree
															</div>

														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="" id="addDegree"><i
																class="fa fa-plus-square-o font_size"></i></a>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Subject</label>
														<div class="col-sm-9">
															<input type="text" class="form-control alphabets"
																id="emp_qulfctn_add_sbjct" placeholder="Subject">
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Institute</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_qulfctn_add_instt" placeholder="Institute">
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Grade/GPA</label>
														<div class="col-sm-9">
															<input type="text" class="form-control "
																id="emp_qulfctn_add_gpa" placeholder="Grade/GPA">
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Graduation
															Year</label>
														<div class="col-sm-9">
															<input type="text" class="form-control numbersonly"
																id="emp_qulfctn_add_grdtnyr"
																placeholder="Graduation Year">
														</div>
													</div>





													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_qulfctn_add_btn"
																onclick="validateQualificationEmployee()"
																class="btn btn-info">Add Qualification</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="qlfctn_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="qlfctn_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</c:if>


									<c:if test="${ rolesMap.roles['Employee Languages'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Language</div>
												<div class="panel-body">


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Language <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<select class="form-control" onchange="getEmpLanguage()"
																id="emp_lngge_add_lngg">
																<option value="">--Select Language--</option>
																<c:forEach items="${language }" var="s">
																	<option value="${s.languageId }" label="${s.language }" />
																</c:forEach>
															</select>
															<div id="lngge_select" class="alert_box">
																<div class="triangle-up"></div>
																Please select language
															</div>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Speaking Level <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<select class="form-control" id="emp_lngge_add_spklvl">
																<option value="">--Select Speaking Level--</option>
																<option value="excellent">Excellent</option>
																<option value="good">Good</option>
																<option value="average">Average</option>
																<option value=poor>Poor</option>
															</select>
															<div id="lngge_select_spk" class="alert_box">
																<div class="triangle-up"></div>
																Please select speaking level
															</div>
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Reading Level <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<select class="form-control" id="emp_lngge_add_redlvl">
																<option value="">--Select Reading Level--</option>
																<option value="excellent">Excellent</option>
																<option value="good">Good</option>
																<option value="average">Average</option>
																<option value=poor>Poor</option>
															</select>
															<div id="lngge_select_rdlvl" class="alert_box">
																<div class="triangle-up"></div>
																Please select reading level
															</div>
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Writing Level <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<select class="form-control" id="emp_lngge_add_wrtlvl">
																<option value="">--Select Writing Level--</option>
																<option value="excellent">Excellent</option>
																<option value="good">Good</option>
																<option value="average">Average</option>
																<option value=poor>Poor</option>
															</select>
															<div id="lngge_select_wrtlvl" class="alert_box">
																<div class="triangle-up"></div>
																Please select writing level
															</div>
														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_lngge_add_btn"
																onclick="validateEmployeeLanguage()"
																class="btn btn-info">Add Language</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="lnguage_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="lnguage_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</c:if>


									<c:if
										test="${ rolesMap.roles['Employee Work Experiences'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Work Experience</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Organisation Name<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_exprnc_add_orgn"
																placeholder="Organisation Name ">
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Job
															Designation <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<!-- <input type="text" class="form-control"
															id="emp_exprnc_add_job" placeholder="Job Designation"> -->
															<select class="form-control" id="emp_exprnc_add_job">
																<option value="">--Select Designation--</option>
																<c:forEach items="${employeeDesignation }" var="j">
																	<option value="${j. id}" label="${j.designation}">
																</c:forEach>
															</select>
															<div id="exprnce_select_design" class="alert_box">
																<div class="triangle-up"></div>
																Please select designation
															</div>
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Job Field <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<select class="form-control" id="emp_exprnc_add_field">
																<option value="">--Select Job Field--</option>
																<c:forEach items="${field }" var="j">
																	<option value="${j. jobFieldId}" label="${j.jobField}">
																</c:forEach>
															</select>
															<div id="exprnce_select_field" class="alert_box">
																<div class="triangle-up"></div>
																Please select job field
															</div>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Job
															Description <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_exprnc_add_desc" placeholder="Job Description">
														</div>
													</div>



													<div class="form-group">
														<label class="col-sm-3 control-label">Job Start
															Date <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepickerempl">
																<input type="text" class="form-control"
																	style="background-color: #ffffff !important;"
																	readonly="readonly" id="emp_exprnc_add_strtdt"
																	data-date-format="DD/MM/YYYY"> <span
																	class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Job End Date
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepickeremplo">
																<input type="text" class="form-control"
																	style="background-color: #ffffff !important;"
																	readonly="readonly" id="emp_exprnc_add_enddt"
																	data-date-format="DD/MM/YYYY"> <span
																	class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
																<div id="exprnce_date_err" class="alert_box">
																	<div class="triangle-up"></div>
																	Please select valid date..
																</div>
															</div>
														</div>

													</div>



													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Starting
															Salary <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control double"
																id="emp_exprnc_add_strtsly"
																placeholder="Starting Salary">
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Ending
															Salary <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control double"
																id="emp_exprnc_add_endsly" placeholder="Ending Salary">
														</div>
													</div>




													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_exprnc_add_btn" onclick="validateEmpExprnce()"
																class="btn btn-info">Work Experience</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="exprnce_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="exprnce_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
									</c:if>




									<c:if test="${ rolesMap.roles['Employee Bank Accounts'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Bank Account</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Account Type<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<select id="emp_bank_add_type" class="form-control">
																<option value="">--Select Account Type--</option>
																<c:forEach items="${accountType }" var="a">
																	<option value="${a.accountTypeId }"
																		label="${a.accountType }">
																</c:forEach>
															</select>
															<div id="bank_select_type" class="alert_box">
																<div class="triangle-up"></div>
																Please select account type
															</div>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Bank Name <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_bank_add_name" placeholder=" Bank Name  ">
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Branch
															Name <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control alphabets"
																id="emp_bank_add_branch" placeholder="Branch Name">
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Branch
															Code / Routing Number<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_bank_add_branch_code"
																placeholder="Branch Code / Routing Number">
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Account
															Title <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_bank_add_titel" placeholder="Account Title">
														</div>
													</div>



													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Account
															Number <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_bank_add_acno" placeholder="Account Number">
														</div>
													</div>



													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_bank_add_btn"
																onclick="validateBankAccountEmp()" class="btn btn-info">Add
																Bank Account <span class="stars">*</span>
															</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="bank_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="bank_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>

									</c:if>



									<c:if test="${ rolesMap.roles['Employee Benefits'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Benefit</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Benefit Title <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_benfit_title" placeholder=" Benefit Title  ">
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Benefit
															Details</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_benfit_details" placeholder="Benefit Details">
														</div>
													</div>



													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_benfit_btn" class="btn btn-info">Add
																Benefit</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="benefit_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="benefit_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

									</c:if>






								</div>





								<div class="col-md-6">

									<c:if
										test="${ rolesMap.roles['Employee Organization Assets'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Organization Asset</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Asset
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<select class="form-control" onchange="getEmpAssets()"
																id="emp_assts_ast">
																<option value="">--Select Asset--</option>
																<c:forEach items="${assets }" var="s">
																	<option value="${s.assetId }" label="${s.assetName }" />
																</c:forEach>
															</select>
															<div id="emp_select_asset" class="alert_box">
																<div class="triangle-up"></div>
																Please select asset
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Asset
															Checkout Date</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepickeremploy">
																<input type="text" class="form-control"
																	style="background-color: #ffffff !important;"
																	readonly="readonly" id="emp_assts_sve_chnkout"
																	data-date-format="DD/MM/YYYY"> <span
																	class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>

													</div>





													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Asset CheckIn </label>
														<div class="col-sm-9">
															<input type="radio" name="CheckIn"
																onclick="$('#asset_checkin').css('display','block')" />
															yes <input type="radio" name="CheckIn"
																onclick="$('#asset_checkin').css('display','none');$('#emp_assts_sve_chnkin').val('');" />
															No
														</div>
													</div>

													<div class="form-group" id="asset_checkin">
														<label class="col-sm-3 control-label">Asset
															CheckIn Date</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepickeremploye">
																<input type="text" class="form-control"
																	style="background-color: #ffffff !important;"
																	readonly="readonly" id="emp_assts_sve_chnkin"
																	data-date-format="DD/MM/YYYY"> <span
																	class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>

													</div>



													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Description</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_assts_sve_dsc" placeholder="Description">
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_assts_sve_btn" onclick="validateEmpAssets()"
																class="btn btn-info">Add Organization Asset</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="assets_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="assets_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
									</c:if>




									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Uniform</div>
											<div class="panel-body">

												<div class="form-group">
													<label class="col-sm-3 control-label"> Checkout
														Date <span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class="input-group date" id="datepickerem">
															<input type="text" class="form-control" id="emp_unfrm_dt"
																style="background-color: #ffffff !important;"
																readonly="readonly" data-date-format="DD/MM/YYYY">
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>

												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Advance Amount<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<input type="text" class="form-control double"
															id="emp_unfrm_amt" placeholder="Advance Amount">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Duration
													</label>
													<div class="col-sm-9">
														<select class="form-control" id="emp_unfrm_drtn">
															<option value="6 months">6 Month</option>
															<option value="1 year">1 year</option>
														</select>
													</div>
												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label">Reimbursement
														Date</label>
													<div class="col-sm-9">
														<div class="input-group date" id="datepickeremp">
															<input type="text" class="form-control"
																style="background-color: #ffffff !important;"
																readonly="readonly" id="emp_unfrm_rmbrsmntdt"
																data-date-format="DD/MM/YYYY"> <span
																class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>

												</div>






												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<a id="emp_unfrm_btn" onclick="validateEmpUniform()"
															class="btn btn-info">Add Uniform</a>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12" id="uniform_save_success">
														<div class="box_success">
															<i class="fa fa-check ffss"></i>Saved successfully...
														</div>
													</div>
													<div class="col-sm-12" id="uniform_save_failed">
														<div class="box_error">
															<i class="fa fa-times ffss"></i>Failed to save..!!
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<c:if test="${ rolesMap.roles['Employee Skills	'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Skills</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Skill <span class="stars">*</span>
														</label>
														<div class="col-sm-9">

															<select class="form-control" onchange="getEmpSkill()"
																id="emp_skll_skill">
																<option value="">--Select Skill--</option>
																<c:forEach items="${skills }" var="s">
																	<option value="${s.skillId }" label="${s.skillName }" />
																</c:forEach>
															</select>
															<div id="skill_select_skill" class="alert_box">
																<div class="triangle-up"></div>
																Please select skill
															</div>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Skill Level <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<select class="form-control" id="emp_skll_lvl">
																<option value="">--Select Skill Level--</option>
																<option value="excellent">Excellent</option>
																<option value="good">Good</option>
																<option value="average">Average</option>
															</select>
															<div id="skill_select_level" class="alert_box">
																<div class="triangle-up"></div>
																Please select skill level
															</div>

														</div>
													</div>



													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a id="emp_skll_btn" onclick="validateSkillEmp()"
																class="btn btn-info">Add Skill</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="skill_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="skill_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</c:if>




									<c:if test="${ rolesMap.roles['Employee References'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Reference</div>
												<div class="panel-body">


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Reference Name <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control alphabets"
																id="emp_rfrnc_name" placeholder="Reference Name ">
														</div>
													</div>



													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Organisation Name<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control"
																id="emp_rfrnc_orgnztn" placeholder="Organisation Name">
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Phone Number <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control numbersonly"
																id="emp_rfrnc_phn" placeholder="Phone Number">
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Email Address </label>
														<div class="col-sm-9">
															<input type="text" class="form-control "
																id="emp_rfrnc_eml" placeholder=" Email Address">
															<div id="valid_email_err_rfrnce" class="alert_box">
																<div class="triangle-up"></div>
																Please enter valid email address
															</div>
														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_rfrnc_btn"
																onclick="validateEmployeeReference()"
																class="btn btn-info">Add Reference</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="rfrnce_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="rfrnce_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
									</c:if>





									<c:if test="${ rolesMap.roles['Employee Dependants'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Dependants</div>
												<div class="panel-body">


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Dependant Name <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control alphabets"
																id="emp_depnts_nm" placeholder="Dependant Name ">
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Date of
															Birth <span class="stars">*</span>
														</label>
														<div class="col-sm-9">

															<div class="input-group date" id="datepickeremployee">
																<input type="text" class="form-control"
																	style="background-color: #ffffff !important;"
																	readonly="readonly" id="emp_depnts_dob"
																	data-date-format="DD/MM/YYYY"> <span
																	class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>

														</div>

													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Relation <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control alphabets"
																id="emp_depnts_rltn" placeholder="Relation">
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Nominee <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="radio" value="true" name="nominee" /> yes <input
																type="radio" value="false" name="nominee" /> No
															<div id="dpndnts_select" class="alert_box">
																<div class="triangle-up"></div>
																Please select nominee
															</div>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Address
														</label>
														<div class="col-sm-9">
															<textarea id="emp_depnts_adrs"
																class="form-control height"></textarea>
														</div>
													</div>



													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_depnts_btn" onclick="validateDependants()"
																class="btn btn-info">Add Dependant</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="dpndnts_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="dpndnts_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
									</c:if>



									<c:if test="${ rolesMap.roles['Employee Next of Kins'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Next Of Kin</div>
												<div class="panel-body">


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Next Of Kin Name <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control alphabets"
																id="emp_add_nxtkin_name"
																placeholder=" Next Of Kin Name ">
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Date of
															Birth <span class="stars">*</span>
														</label>
														<div class="col-sm-9">

															<div class="input-group date" id="datepickeremployees">
																<input type="text" class="form-control"
																	style="background-color: #ffffff !important;"
																	id="emp_add_nxtkin_dob" readonly="readonly"
																	data-date-format="DD/MM/YYYY"> <span
																	class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>

														</div>

													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Relation <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<input type="text" class="form-control alphabets"
																id="emp_add_nxtkin_rltn" placeholder="Relation">
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Address
														</label>
														<div class="col-sm-9">
															<textarea id="emp_add_nxtkin_addrs"
																class="form-control height"></textarea>
														</div>
													</div>





													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_add_nxtkin_btn" onclick="validateNextOfKin()"
																class="btn btn-info">Add Next Of Kin</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="skin_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="skin_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</c:if>


									<c:if test="${ rolesMap.roles['Employee Roles'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Employee Roles</div>
												<div class="panel-body">

													<div class="form-group">
														<a href="employee_rules.do?id=${employeeVo.employeeId }">
															<button type="button" id="emp_add_roles_btn"
																class="btn btn-primary">Employee Roles</button>
														</a>
													</div>



												</div>
											</div>
										</div>
									</c:if>


									<c:if test="${ rolesMap.roles['Employee Status Update'].add}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Status</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Status </label>
														<div class="col-sm-9">
															<!-- <label for="inputPassword3" class="col-sm-4 control-label">
														<input
															type="radio" name="select"> Active </label> <label
															for="inputPassword3" class="col-sm-4 control-label"><input
															type="radio" name="select"> Inactive </label> -->

															<select class="form-control" id="emp_stts_sve_stst">
																<c:forEach items="${status }" var="s">
																	<option value="${s.id }" label="${s.status }">
																</c:forEach>
															</select>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-16 col-sm-12">
															<a id="emp_stts_sve_btn" class="btn btn-info">Save</a>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12" id="status_save_success">
															<div class="box_success">
																<i class="fa fa-check ffss"></i>Saved successfully...
															</div>
														</div>
														<div class="col-sm-12" id="status_save_failed">
															<div class="box_error">
																<i class="fa fa-times ffss"></i>Failed to save..!!
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</c:if>


								</div>
							</div>









						</div>
					</div>

					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<form:form action="uplodEmpDocs.do" enctype="multipart/form-data"
								commandName="employeeVo" method="POST">
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Upload Documents</div>
										<div class="panel-body">
											<form:hidden path="employeeId" id="emp_uld_id" />

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Upload
													Documents </label>
												<div class="col-sm-9">
													<input type="file" multiple name="emp_upld_docs"
														id="employee-file" class="btn btn-info" />
												</div>
											</div>
											<c:if test="${ rolesMap.roles['Employee Documents'].add}">
												<button class="btn btn-info" id="emp_doc_upld_btn"
													type="submit">Upload</button>
											</c:if>

										</div>
									</div>
								</div>


								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">


											<table class="table no-margn">
												<thead>
													<tr>
														<th>#</th>
														<th>Document Name</th>
														<c:if test="${ rolesMap.roles['Employee Documents'].view}">
															<th>view</th>
														</c:if>
														<c:if
															test="${ rolesMap.roles['Employee Documents'].delete}">
															<th>Delete</th>
														</c:if>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${employeeVo.documentsVos }" var="d"
														varStatus="s">
														<tr>
															<td>${s.count }</td>
															<td>${d.fileName }</td>
															<c:if
																test="${ rolesMap.roles['Employee Documents'].view}">
																<td><a href="downloadEmpDoc.do?durl=${d.documnetUrl }">
																		<i class="fa fa-file-text-o sm"></i>
																</a></td>
															</c:if>
															<c:if
																test="${ rolesMap.roles['Employee Documents'].delete}">
																<td><a href="deleteEmpDoc.do?durl=${d.documnetUrl }"><i
																		class="fa fa-close ss"></i></a></td>
															</c:if>
														</tr>
													</c:forEach>

												</tbody>
											</table>


										</div>
									</div>
								</div>



							</form:form>
						</div>
					</div>

				</div>
			</div>


		</div>

		<div id="pop3" class="simplePopup">
			<h1 class="pop_hd">Manage Qualification Degrees</h1>
			<div class="row">
				<form:form action="SaveQualification.do" method="post" id="StQDForm">
					<div class="col-md-12">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Available
								Degrees </label>
							<div class="col-sm-9" id="avai-l-qd"></div>
						</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Qualification
								Degrees </label>
							<div class="col-sm-9">
								<textarea name="degreeName" id="degreeNameVal"
									class="form-control height"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="submit" onclick="newQD()" id="new-qd"
									class="btn btn-info">Save</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<span id="setg-qd-pp-msg" class="hideMe imsuccess"></span>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>


	</div>
	<!-- Warper Ends Here (working area) -->



	</section>
	<!-- Content Block Ends Here (right box)-->



	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script src="<c:url value='/resources/assets/js/settings-ajax.js' />"></script>

	<script type="text/javascript">
	document.getElementById("files").onchange = function () {
	    var reader = new FileReader();

	    reader.onload = function (e) {
	        // get loaded data and render thumbnail.
	        document.getElementById("image").src = e.target.result;
	    };

	    // read the image file as a data URL.
	    reader.readAsDataURL(this.files[0]);
	};
		$(document).ready(
				function() {
					getSuperiorsSubordinate("${employeeVo.superiorsIds }",
							"${employeeVo.subordinatesIds }");

					if ($("#empid").val() == '') {
						$("#save_emp_leave_btn").attr("disabled", "true");
						$("#emp_add_info_btn_uodt").attr("disabled", "true");
						$("#emp_uprofile_sbmit").attr("disabled", "true");
						$("#emp_emrgncy_cntct_btn").attr("disabled", "true");
						$("#emp_spriors_sbrdnts_btn").attr("disabled", "true");
						$("#emp_phone_btn").attr("disabled", "true");
						$("#emp_prmnt_cntct_btn").attr("disabled", "true");
						$("#save_emp_user_info").attr("disabled", "true");
						$("#emp_add_info_btn").attr("disabled", "true");

						$("#emp_assts_sve_btn").attr("disabled", "true");
						$("#emp_qulfctn_add_btn").attr("disabled", "true");
						$("#emp_unfrm_btn").attr("disabled", "true");
						$("#emp_lngge_add_btn").attr("disabled", "true");
						$("#emp_skll_btn").attr("disabled", "true");
						$("#emp_exprnc_add_btn").attr("disabled", "true");
						$("#emp_rfrnc_btn").attr("disabled", "true");
						$("#emp_bank_add_btn").attr("disabled", "true");
						$("#emp_depnts_btn").attr("disabled", "true");
						$("#emp_benfit_btn").attr("disabled", "true");
						$("#emp_add_nxtkin_btn").attr("disabled", "true");
						$("#emp_stts_sve_btn").attr("disabled", "true");
						$("#emp_add_roles_btn").attr("disabled", "true");

						$("#emp_doc_upld_btn").attr("disabled", "true");
					}

					$('#asset_checkin').css('display', 'none')

					$('.show1').click(function() {
						$('#pop1').simplePopup();
					});

					$('.show2').click(function() {
						$('#pop2').simplePopup();
					});

					$('.show3').click(function() {
						$('#pop3').simplePopup();
					});

					$('.show15').click(function() {
						$('#pop15').simplePopup();
					});

					$("#dashbordli").removeClass("active");
					$("#organzationli").removeClass("active");
					$("#recruitmentli").removeClass("active");
					$("#employeeli").addClass("active");
					$("#timesheetli").removeClass("active");
					$("#payrollli").removeClass("active");
					$("#reportsli").removeClass("active");

				});

		if ($("#emp_code").val() != '') {
			$("#emp_code").attr("readOnly", "readOnly");
		}

		if ($('#employee-file').val() == '') {
			$("#emp_doc_upld_btn").attr("disabled", "true");
		}

		$('#employee-file').on("change", function() {
			if ($("#empid").val() != '')
				$("#emp_doc_upld_btn").removeAttr("disabled");
		});

		$('#addDegree').click(function() {
			getAllAvailableQDegree();
			$('#pop3').simplePopup();
		});
		
		setUserName = function(value){
			$("#emp_user_name").val(value);	
		}
	</script>
</body>
</html>