<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Meetings</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<a href="meetings.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>




		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
					<!-- <li role="presentation"><a href="#docs" role="tab"
						data-toggle="tab"> Status</a></li> -->
				</ul>



				<div class="tab-content">

					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<form:form commandName="meetingsVo" method="POST" id="meetingFrm"
							action="saveMeeting.do">
							<form:hidden path="meetingId" id="meetingId" />
							<div class="panel-body">

								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Meeting Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Title <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="meetingsTitle" id="meeting_add_title"
																cssClass="form-control" />
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
														</label>
														<div class="col-sm-9">
															<form:checkbox path="sendEmail" id="meeting_add_email"
																label="Send E-mail" />
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
														</label>
														<div class="col-sm-9">
															<form:checkbox path="sendNotification"
																id="meeting_add_notification" label="Send Notification" />
														</div>
													</div>

												</div>
											</div>
										</div>






										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Meeting Dates</div>
												<div class="panel-body">
													<div class="form-group">
														<label class="col-sm-3 control-label">Meeting
															Start Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class='input-group date' id="datepicker">
																<form:input path="meetingStartDate" readonly="true"
																	style="background-color: #ffffff !important;"
																	id="meeting_add_strdt" cssClass="form-control"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon glyphicon-calendar"></span> </span>
															</div>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Meeting End
															Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class='input-group date' id="datepickers">
																<form:input path="meetingEndDate" readonly="true"
																	id="meeting_add_enddt"
																	style="background-color: #ffffff !important;"
																	cssClass="form-control" data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon glyphicon-calendar"></span> </span>
															</div>
															<div id="mtng_date_err" class="alert_box">
																<div class="triangle-up"></div>
																Please Select Valid Date..
															</div>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Meeting Time <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class='input-group date' id="timepicker">
																<form:input path="time" cssClass="form-control"
																	readonly="true" id="meeting_add_time"
																	style="background-color: #ffffff !important;" />
																<span class="input-group-addon"><i
																	class="fa fa-clock-o"></i> </span>
															</div>
														</div>

													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Venue <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="venue" cssClass="form-control"
																id="meeting_add_venue" />
														</div>
													</div>






												</div>
											</div>
										</div>



										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Participants</div>
												<div class="panel-body" style="overflow: visible;">
													<div class="form-group">
														<label class="col-sm-3 control-label">Branches <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select id="drop_one" multiple="true"
																onchange="getEmployeeees()" path="branchIds"
																cssClass="form-control">
																<form:options items="${branches }"
																	itemLabel="branchName" itemValue="branchId" />
															</form:select>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Departments <span class="stars">*</span></label>
														<div class="col-sm-9">

															<form:select id="drop_two" multiple="true"
																onchange="getEmployeeees()" path="departmentIds"
																cssClass="form-control">
																<form:options items="${departments }" itemLabel="name"
																	itemValue="id" />
															</form:select>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Employees <span class="stars">*</span></label>
														<div class="col-sm-9">

															<form:select id="drop_three" multiple="true"
																path="employeeIds" class="form-control">
																<c:forEach items="${employees }" var="e">
																	<c:choose>
																		<c:when test="${e.name ==null }"></c:when>

																		<c:otherwise>
																			<form:option value="${e.employeeId }"
																				label="${e.name }" />
																		</c:otherwise>
																	</c:choose>
																</c:forEach>
																<%-- <form:options items="${employees }"
																	id="meeting_add_prtcpnts" itemLabel="name"
																	itemValue="employeeId" /> --%>
															</form:select>
															<div id="mtng_emp_err" class="alert_box">
																<div class="triangle-up"></div>
																Please Select Participants
															</div>
														</div>

													</div>



												</div>
											</div>
										</div>



										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Status</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Status <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select path="statusId" id="meeting_add_status"
																cssClass="form-control">
																<option value="">--Select Status--</option>
																<form:options items="${status }" itemLabel="status"
																	itemValue="statusId" />
															</form:select>
															<div id="mtng_status_err" class="alert_box">
																<div class="triangle-up"></div>
																Please Select Status
															</div>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Description
														</label>
														<div class="col-sm-9">
															<form:textarea path="statusDescription"
																cssClass="form-control height" />
														</div>
													</div>



												</div>
											</div>
										</div>


									</div>

									<div class="col-md-6">


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Agenda</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">


															<form:textarea class="form-control" path="agenda"
																cssStyle="height: 150px;resize:none" />




														</div>
													</div>





												</div>
											</div>
										</div>




										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="additionalInformation"
																cssClass="form-control height" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${meetingsVo.createdBy }</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${meetingsVo.createdOn }</label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="button" id="meetingAddSubmtBtn"
																onclick="validateMeeting()" class="btn btn-info">Save</button>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								</div>



							</div>
						</form:form>
					</div>

					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<c:if test="${ rolesMap.roles['Meeting Documents'].add}">
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Upload Documents</div>
										<div class="panel-body">
											<form:form commandName="meetingsVo"
												action="uploadMeetingsDocs.do" method="POST"
												enctype="Multipart/form-data">
												<form:hidden path="meetingId" />
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Upload
														Documents </label>
													<div class="col-sm-9">
														<input type="file" multiple name="meetingDocs"
															id="meeting-docs" class="btn btn-info" />
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-info"
															id="sbmit_meeting_docs_btn">Upload</button>
													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</c:if>


							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">

										<c:if test="${!empty meetingsVo.documentsVos }">
											<table class="table no-margn">
												<thead>
													<tr>
														<th>Document Name</th>
														<c:if test="${ rolesMap.roles['Meeting Documents'].view}">
															<th>view</th>
														</c:if>
														<c:if
															test="${ rolesMap.roles['Meeting Documents'].delete}">
															<th>Delete</th>
														</c:if>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${meetingsVo.documentsVos }" var="d">
														<tr>
															<td><i class="fa fa-file-o"></i>${d.filename }</td>
															<c:if test="${ rolesMap.roles['Meeting Documents'].view}">
																<td><a href="downloadMeetingDoc.do?url=${d.url }"><i
																		class="fa fa-file-text-o sm"></i></a></td>
															</c:if>
															<c:if
																test="${ rolesMap.roles['Meeting Documents'].delete}">
																<td><a href="deleteMeetingDoc.do?url=${d.url }"><i
																		class="fa fa-close ss"></i></a></td>
															</c:if>
														</tr>
													</c:forEach>

												</tbody>
											</table>
										</c:if>

									</div>
								</div>
							</div>




						</div>
					</div>





				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->


	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->


	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>

	<script
		src="<c:url value='/resources/docs/js/bootstrap-3.3.2.min.js' />"></script>
	<script src="<c:url value='/resources/docs/js/prettify.js' />"></script>
	<script
		src="<c:url value='/resources/docs/js/bootstrap-multiselect.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<%-- <script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script> --%>
	<%-- 	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script> --%>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			if ($("#meeting-docs").val() == "") {
				$("#sbmit_meeting_docs_btn").attr("disabled", "true");
			}
			$("#meeting-docs").on("change", function() {
				if ($("#meetingId").val() != '')
					$("#sbmit_meeting_docs_btn").removeAttr("disabled");
			});

			$('#drop_one').multiselect({
				includeSelectAllOption : true
			});

			$('#drop_two').multiselect({
				includeSelectAllOption : true
			});
			$('#drop_three').multiselect({
				includeSelectAllOption : true
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").addClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>