<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Job Posts</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">

			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Job Posts</div>
					<div class="panel-body">
						<c:if test="${! empty jobPosts}">
							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered" id="basic-datatable">
								<thead>
									<tr>
										<th width="20%">Job Title</th>
										<th width="20%">Department</th>
										<th width="20%">Job Type</th>
										<th width="10%">No.of Positions</th>
										<th width="20%">Job Post Closing Date</th>
										<c:if test="${ rolesMap.roles['Job Posts'].update}">
											<th width="5%">Edit</th>
										</c:if>
										<c:if test="${ rolesMap.roles['Job Posts'].delete}">
											<th width="5%">Delete</th>
										</c:if>

									</tr>
								</thead>
								<tbody>
									<c:forEach items="${jobPosts}" var="post">
										<tr>
											<c:choose>
												<c:when test="${ rolesMap.roles['Job Posts'].view}">
													<td><a href="viewJobPost.do?pid=${post.jobPostId}">${post.jobTitle}</a></td>
												</c:when>
												<c:otherwise>
													<td>${post.jobTitle}</td>
												</c:otherwise>
											</c:choose>

											<td>${post.department }</td>
											<td>${post.jobType }</td>
											<td>${post.positions}</td>
											<td>${post.jobPostEndDate}</td>
											<c:if test="${ rolesMap.roles['Job Posts'].update}">
												<td><a href="editJobPosts.do?pid=${post.jobPostId}"><i
														class="fa fa-edit sm"></i> </a></td>
											</c:if>
											<c:if test="${ rolesMap.roles['Job Posts'].delete}">
												<td><a onclick="confirmDelete(${post.jobPostId})"
													class="delete_post"><i class="fa fa-close ss"></i></a></td>
											</c:if>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
						<c:if test="${ rolesMap.roles['Job Posts'].add}">
							<a href="addJobPosts.do">
								<button type="button" class="btn btn-primary">Add New
									Job Post</button>
							</a>
						</c:if>
					</div>

				</div>
			</div>

		</div>

	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>


	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">

   function confirmDelete(id){
		var result=confirm("Confirm delete?");
		 if(result){
			$(".delete_post").attr("href","deleteJobPosts?pid="+id);
		} else{
			$(".delete_post").attr("href","#");
		} 
	} 
   $(document).ready(function(){
	   $("#dashbordli").removeClass("active");
		$("#organzationli").removeClass("active");
		$("#recruitmentli").addClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").removeClass("active");
		$("#reportsli").removeClass("active"); 
   });
    </script>
	<!-- Custom JQuery -->
	<script src="assets/js/app/custom.js" type="text/javascript"></script>
</body>
</html>