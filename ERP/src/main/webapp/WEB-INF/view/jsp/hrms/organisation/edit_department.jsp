<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">


<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>SNOGOL HRM</title>

<meta name="description" content="">

<!-- Bootstrap core CSS -->
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</head>
<body data-ng-app>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Departments</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg" >
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">
		<form:form action="updateDepartment.do" method="POST"
									commandName="departmentVo" id="department_details" enctype="multipart/form-data">
		<form:hidden path="id" id="departmentId_add" />

			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#docs" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">

							<div class="row">
								
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Department Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputEmail3" class="col-sm-3 control-label">Branch <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select cssClass="form-control chosen-select"
																multiple="false" path="branchId" name="branchId">
																<form:option value="" label="Select Branch"></form:option>
																<form:options items="${branches}" itemValue="branchId"
																	itemLabel="branchName" />
															</form:select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Department
															Name <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="name" type="text" name="name"
																class="form-control" id="inputPassword3"
																placeholder="${departmentVo.name}" disabled="true"></form:input>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3"
															class="col-sm-3 col-xs-12 control-label">Department
															Head</label>
														<div class="col-sm-8 col-xs-10">
															<form:select cssClass="form-control chosen-select"
																multiple="false" path="departmentHeadId">
																<form:option value="" label="Select Employee"></form:option>
																<form:options items="${employees}"
																	itemValue="employeeId" itemLabel="employeeCode" />
															</form:select>

														</div>
														
													</div>



													<div id="pop1" class="simplePopup">
														<h1 class="pop_hd">Azad</h1>
														<div class="row">

															<div class="col-md-3">
																<img src="assets/images/avtar/user.png"
																	/ class="pop_img">
															</div>
															<div class="col-md-9">

																<div class="form-group">
																	<label for="inputPassword3"
																		class="col-sm-3 control-label">E-mail </label>
																	<div class="col-sm-9">
																		<label for="inputPassword3"
																			class="col-sm-6 control-label ft">jsafs@.com</label>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputPassword3"
																		class="col-sm-3 control-label">Designation </label>
																	<div class="col-sm-9">
																		<label for="inputPassword3"
																			class="col-sm-6 control-label ft">2545895678</label>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputPassword3"
																		class="col-sm-3 control-label">Department </label>
																	<div class="col-sm-9">
																		<label for="inputPassword3"
																			class="col-sm-6 control-label ft">dsafdsfdsf</label>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputPassword3"
																		class="col-sm-3 control-label">Station </label>
																	<div class="col-sm-9">
																		<label for="inputPassword3"
																			class="col-sm-6 control-label ft">2sdfds8</label>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputPassword3"
																		class="col-sm-3 control-label">Join Date </label>
																	<div class="col-sm-9">
																		<label for="inputPassword3"
																			class="col-sm-6 control-label ft">25-2-2015</label>
																	</div>
																</div>



															</div>

														</div>
													</div>





													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Parent
															Department</label>
														<div class="col-sm-9">

															<form:select cssClass="form-control chosen-select"
																multiple="false" path="parentDepartmentId">
																<form:option value="" label="Select Department"></form:option>
																<form:options items="${departments}" itemValue="id"
																	itemLabel="name" />
															</form:select>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Department
															Sorting Order <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="sortingOrder" name="sortingOrder"
																type="text" class="form-control" id="inputPassword3"
																placeholder="${departmentVo.sortingOrder}"></form:input>
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>

									<div class="col-md-6">

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<textarea class="form-control height"
																name="additionalInformation">${departmentVo.additionalInformation}</textarea>
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${departmentVo.createdBy }</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${departmentVo.createdOn}</label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								

							</div>



						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="docs">
						<div class="panel-body" id="docs">
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Upload Documents</div>
										<div class="panel-body">

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Upload
													Documents </label>
												<div class="col-sm-9">
													<input type="file" class="btn btn-info"
														id="department_file_upload" name="department_file_upload" />
												</div>
											</div>


										</div>
									</div>
								</div>






								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">


											<c:if test="${!empty departmentVo.departmentDocuments}">
												<table class="table no-margn">
													<thead>
														<tr>
															<th>#</th>
															<th>Document Name</th>
															<c:if test="${ rolesMap.roles['Department Documents'].view}">
															<th>view</th>
															</c:if>
															<c:if test="${ rolesMap.roles['Department Documents'].delete}">
															<th>Delete</th>
															</c:if>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${departmentVo.departmentDocuments}"
															var="doc">
															<tr>
																<td>${doc.id }</td>
																<td><i class="fa fa-file-o"></i>${doc.documentName }</td>
																<c:if test="${ rolesMap.roles['Department Documents'].view}">
																<td><a
																	href="viewDepartmentDocument.do?durl=${doc.url}&doc_id=${doc.id}"><i
																		class="fa fa-file-text-o sm"></i></a></td>
																		</c:if>
																		<c:if test="${ rolesMap.roles['Department Documents'].delete}">
																<td><a
																	href="deleteDepartmentDocument.do?durl=${doc.url}&doc_id=${doc.id}"><i
																		class="fa fa-close ss"></i></a></td>
																		</c:if>
															</tr>
														</c:forEach>

													</tbody>
												</table>
											</c:if>

										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<!-- <a href="#docs" data-toggle="tab" class="btn btn-info">Next</a> -->
										<c:if test="${ rolesMap.roles['Department Documents'].add}">
										<button type="submit" value="uploadDocuments"
											class="btn btn-info" id="sbmt_department_doc_btn"
											name="uploadDocuments">Upload Documents</button>
											</c:if>
										<!-- <a class="btn btn-info" id="sbmt_proj_doc_btn">Upload
											Documents</a> -->
									</div>
								</div>
						</div>
					</div>

				</div>
			</div>

		</form:form>
		</div>




	</div>
	<!-- Warper Ends Here (working area) -->
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script
		src="<c:url value='/resources/js/progress/jquery.uploadfile.min.js' />"></script>
	<script src="<c:url value='/resources/js/progress/file-upload.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").addClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</html>
