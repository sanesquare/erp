<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Job Interviews</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">

			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Job Interviews</div>
					<div class="panel-body">
						<c:if test="${! empty interviews}">
							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered" id="basic-datatable">
								<thead>
									<tr>
										<th width="30%">Job Title</th>
										<th width="20%">Interview Date</th>
										<th width="20%">Interview Time</th>
										<th width="10%">Place of Interview</th>
										<c:if test="${ rolesMap.roles['Job Interviews'].update}">
											<th width="10%">Edit</th>
										</c:if>
										<c:if test="${ rolesMap.roles['Job Interviews'].delete}">
											<th width="10%">Delete</th>
										</c:if>

									</tr>
								</thead>
								<tbody>
									<c:forEach items="${interviews }" var="interview">
										<tr>
											<c:choose>
												<c:when test="${ rolesMap.roles['Job Interviews'].delete}">
													<td><a
														href="viewInterview.do?iid=${interview.jobInterviewId }">${interview.jobPost }</a></td>
												</c:when>
												<c:otherwise>
													<td>${interview.jobPost }</td>
												</c:otherwise>
											</c:choose>

											<td>${interview.interviewDate }</td>
											<td>${interview.interviewTime }</td>
											<td>${interview.place }</td>
											<c:if test="${ rolesMap.roles['Job Interviews'].update}">
												<td><a
													href="editJobInterview.do?iid=${interview.jobInterviewId }"><i
														class="fa fa-edit sm"></i> </a></td>
											</c:if>
											<c:if test="${ rolesMap.roles['Job Interviews'].delete}">
												<td><a class="delete_interview"
													onClick="confirmDelete(${interview.jobInterviewId})"><i
														class="fa fa-close ss"></i></a></td>
											</c:if>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
						<c:if test="${ rolesMap.roles['Job Interviews'].add}">
							<a href="addJobInterviews.do">
								<button type="button" class="btn btn-primary">Add New
									Job Interview</button>
							</a>
						</c:if>
					</div>
				</div>
			</div>

		</div>

	</div>

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>

	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
	 function confirmDelete(id){
			var result=confirm("Confirm delete?");
			 if(result){
				$(".delete_interview").attr("href","deleteJobInterview?iid="+id);
			} else{
				$(".delete_interview").attr("href","#");
			} 
		} 
	 $(document).ready(function(){
		 $("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").addClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active"); 
	 });
	</script>
	<!-- Custom JQuery -->
	<script src="assets/js/app/custom.js" type="text/javascript"></script>
</body>
</html>