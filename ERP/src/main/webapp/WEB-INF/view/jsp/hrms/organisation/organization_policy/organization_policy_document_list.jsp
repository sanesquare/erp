<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table class="table no-margn">
	<thead>
		<tr>
			<th>#</th>
			<th>Document Name</th>
			<%-- <c:if test="${ rolesMap.roles['Organization Policy Documents'].view}"> --%>
			<th>view</th><%-- </c:if> --%>
			<%-- <c:if test="${ rolesMap.roles['Organization Policy Documents'].delete}"> --%>
			<th>Delete</th><%-- </c:if> --%>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${documents}" var="document"
			varStatus="status">
			<tr>
				<td>${status.count}</td>
				<td><i class="fa fa-file-o"></i>${document.documentName}</td>
				<%-- <c:if test="${ rolesMap.roles['Organization Policy Documents'].view}"> --%>
				<td><a href="viewPolicyDocument.do?durl=${document.documentUrl}"><i class="fa fa-file-text-o sm"></i></a></td><%-- </c:if> --%>
				<%-- <c:if test="${ rolesMap.roles['Organization Policy Documents'].delete}"> --%>
				<td><a href="#"
					onclick="deleteOrganizationPolicyDocument(${document.documentId},'${document.documentUrl}')"><i
						class="fa fa-close ss"></i></a></td><%-- </c:if> --%>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>