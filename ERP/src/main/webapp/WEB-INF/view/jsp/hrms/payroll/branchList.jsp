<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<ul class="select_whole">
	<c:forEach items="${branchList}" var="branch">
		<a href="#" onclick="getAllBranchDepartments(${branch.branchId})"><li>${branch.branchName}</li></a>
	</c:forEach>
</ul>

<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
