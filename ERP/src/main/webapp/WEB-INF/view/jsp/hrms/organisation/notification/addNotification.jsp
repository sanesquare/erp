<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<form:form id="id_notification_form" action="saveNotification" commandName="notificationVo">
<form:hidden path="id"/>
<div class="form-group">
	<label for="inputPassword3" class="col-sm-3 control-label">Notification
		Type </label>
	<div class="col-sm-9">
			<form:select path="notificationType.id">
				<form:option value="">Please Select</form:option>
				<form:options items="${notificationTypeList}" itemLabel="type"
					itemValue="id" />
			</form:select>
			<!-- <select class="form-control">
			<option>Shimil</option>
			<option>One</option>
			<option>One</option>
		</select> -->
	</div>
</div>
<div class="form-group">
	<label for="inputPassword3" class="col-sm-3 control-label">Send
		to(Employee) </label>
	<div class="col-sm-9">
		<form:select path="employeeIds" multiple="true">
				<form:option value="">Please Select</form:option>
				<form:options items="${employeeList}" itemLabel="employeeName" itemValue="employeeId" />
		</form:select>
	</div>
</div>
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-3 control-label">Send
			to (Self)</label>
		<div class="col-sm-9">
			<form:radiobutton path="isStoS" value="Y" />
			YES&nbsp;
			<form:radiobutton path="isStoS" value="N" />
			NO
		</div>
	</div>
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-3 control-label">Notification
			Send On </label>
		<div class="col-sm-9">

			<form:radiobuttons path="mstReference.id" items="${sendOnList}"
				itemLabel="referenceVal" itemValue="id" />

		</div>
	</div>
	<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
		<button type="submit" class="btn btn-info">Save</button>
	</div>
</div>
</form:form>