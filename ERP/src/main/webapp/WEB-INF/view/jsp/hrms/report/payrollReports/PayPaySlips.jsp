<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
</head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Snogol HRMS</title>
</head>
<body style="background: #FFFFFF !important;">
	<form:form action="exportPaySlip" method="POST"
		modelAttribute="slipDetails">
		<div class="col-xs-12" id="table3">
			<h2 class="role_hd" align="center">Pay Slip</h2>

			<table class="table no-margn line">
				<tbody>

					<tr>
						<td>Employee Name : <strong>${slipDetails.empName}<form:hidden
									path="empName" /></strong></td>
						<td>Pay Mode : <strong>${slipDetails.payMode}<form:hidden
									path="payMode" /></strong></td>
					</tr>

					<tr>
						<td>Employee Code : <strong>${slipDetails.code}<form:hidden
									path="code" /></strong></td>
						<td>Bank : <strong>${slipDetails.bank}<form:hidden
									path="bank" /></strong></td>
					</tr>
					<tr>
						<td>Designation : <strong>${slipDetails.designation}<form:hidden
									path="designation" /></strong></td>
						<td>Bank A/c No : <strong>${slipDetails.account}<form:hidden
									path="account" /></strong></td>
					</tr>
					<tr>
						<td>Department : <strong>${slipDetails.department}<form:hidden
									path="department" /></strong></td>
						<td>P.F No : <strong>${slipDetails.pf}<form:hidden
									path="pf" /></strong></td>
					</tr>
					<tr>
						<td>Store : <strong>${slipDetails.branch}<form:hidden
									path="branch" /></strong></td>
						<td>ESI No : <strong>${slipDetails.esi}<form:hidden
									path="esi" /></strong></td>
					</tr>
					<tr>
						<td>Date of joining : <strong>${slipDetails.dateOfJoining}<form:hidden
									path="dateOfJoining" /></strong></td>
						<td>PAN No : <strong>${slipDetails.pan}<form:hidden
									path="pan" /></strong></td>
					</tr>


				</tbody>
			</table>
			<br /> <br />


			<table class="table table-striped no-margn line">
				<tbody>
					<tr>
					</tr>
					<c:choose>
						<c:when test="${! empty slipDetails.details }">
							<c:forEach items="${slipDetails.details }" var="detailM"
								varStatus="sts1">
								<td class="hd_tb" colspan="2"
									style="background-color: #0380B0; color: #fff;"><span
									class="WhiteHeading">Salary Details ${detailM.month } <form:hidden
											path="details[${sts1.index }].month" />
								</span></td>
								<c:forEach items="${detailM.detail }" var="itemMap"
									varStatus="sts2">
									<tr>
										<td>${itemMap.key }</td>
										<td>${itemMap.value }<form:hidden
												path="details[${sts1.index }].detail[${itemMap.key }]" />
										</td>
									</tr>
								</c:forEach>

								<%-- <c:if test="${! empty detailM.adjustment }">
									<tr>
										<td>${detailM.adjustment }<form:hidden
												path="details[${sts1.index }].adjustment" /></td>
										<td>${detailM.adjAmount }<form:hidden
												path="details[${sts1.index }].adjAmount" /></td>
									</tr>
								</c:if> --%>
								<tr>
									<td><strong>Total Earnings </strong></td>
									<td><strong>${detailM.totalEarnings}<form:hidden
												path="details[${sts1.index }].totalEarnings" /></strong></td>
								</tr>
								<c:forEach items="${detailM.deductionDetail }" var="itemMap"
									varStatus="sts2">
									<tr>
										<td>${itemMap.key }</td>
										<td>${itemMap.value }<form:hidden
												path="details[${sts1.index }].deductionDetail[${itemMap.key }]" />
										</td>
									</tr>
								</c:forEach>
								<tr>
									<td><strong>Total Deductions</strong></td>
									<td><strong>${detailM.totalDeductions}<form:hidden
												path="details[${sts1.index }].totalDeductions" /></strong></td>
								</tr>
								<tr>
									<td><strong>Net Pay</strong></td>
									<td><strong>${detailM.netPay}<form:hidden
												path="details[${sts1.index }].netPay" /></strong></td>
								</tr>
								<tr>
									<td><strong>CTC</strong></td>
									<td><strong>${detailM.ctc}<form:hidden
												path="details[${sts1.index }].ctc" /></strong></td>
								</tr>
								<tr>
									<td><strong>LOP Days</strong></td>
									<td><strong>${detailM.lop}<form:hidden
												path="details[${sts1.index }].lop" /></strong></td>
								</tr>
								<tr>
									<td><strong>Actual Days</strong></td>
									<td><strong>${detailM.totalDays}<form:hidden
												path="details[${sts1.index }].totalDays" /></strong></td>
								</tr>

							</c:forEach>
						</c:when>
						<c:otherwise>
							<td>No Records To Show...</td>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		<c:if test="${! empty slipDetails.details }">
			<div class="form-group" style="margin-top: 25px;">
				<div class="col-sm-offset-4 col-sm-6">
					<button type="submit" class="btn btn-info" name="type" value="pdf">
						Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
					</button>
					<!--<button type="submit" class="btn btn-info" name="type" value="xls">
						Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
					</button>
					<button type="submit" class="btn btn-info" name="type" value="csv">
						Export to CSV &nbsp; <i class="fa fa-file-text-o"></i>
					</button>
					 <button type="submit" class="btn btn-info" name="type" value="html">
						Export to HTML &nbsp; <i class="fa fa-file-code-o"></i>
					</button> -->

					<!-- <button class="btn btn-info"
					onclick="window.open('data:application/vnd.ms-excel,' + document.getElementById('table3').outerHTML.replace(/ /g, '%20'));">
					Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
				</button> -->
				</div>
			</div>
		</c:if>
	</form:form>
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>
</html>