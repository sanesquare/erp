<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
</head>
<body style="background: #FFFFFF !important;">
<form:form action="exportEmpWS.do" method="POST"
			modelAttribute="timeEmpWorkDetails">
	<div class="col-xs-12" id="table4">
		<h2 class="role_hd">Employees Worksheet</h2>
			<table class="table no-margn line">
				<tbody>
					<tr>
						<td>Employee Name : <strong>${timeEmpWorkDetails.empName }<form:hidden
									path="empName" /></strong></td>
						<td>Designation : <strong>${timeEmpWorkDetails.designation }<form:hidden
									path="designation" /></strong></td>
					</tr>
					<tr>
						<td>Department: <strong>${timeEmpWorkDetails.department }<form:hidden
									path="department" /></strong></td>
						<td>User Name: <strong>${timeEmpWorkDetails.userName }<form:hidden
									path="userName" /></strong></td>
					</tr>
				</tbody>
			</table>
			<br />
			<br />
			<br />
			<table class="table table-striped no-margn line">
				<tbody>
					<tr>
						<td class="hd_tb"><span class="WhiteHeading">Date</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Start
								Time </span></td>
						<td class="hd_tb"><span class="WhiteHeading">End Time
						</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Project </span></td>
						<td class="hd_tb"><span class="WhiteHeading">Task </span></td>
						<td class="hd_tb"><span class="WhiteHeading">No. of
								Hours </span></td>
					</tr>
					<c:choose>
						<c:when test="${! empty timeEmpWorkDetails.details }">
							<c:forEach items="${timeEmpWorkDetails.details }" var="det"
								varStatus="status">
								<tr>
									<td>${det.dateString }<form:hidden
											path="details[${status.index }].date" /></td>
									<td>${det.startTimeString }<form:hidden
											path="details[${status.index }].startTime" /></td>
									<td>${det.endTimeString }<form:hidden
											path="details[${status.index }].endTime" /></td>
									<td>${det.project }<form:hidden
											path="details[${status.index }].project" /></td>
									<td>${det.task }<form:hidden
											path="details[${status.index }].task" /></td>
									<td>${det.hours }<form:hidden
											path="details[${status.index }].hours" /></td>
								</tr>
							</c:forEach>
							<tr>
								<td colspan="5"><strong>Total</strong></td>
								<td>${timeEmpWorkDetails.totalHours }<form:hidden
										path="totalHours" /></td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td>No Records To Show...</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>

		
	</div>
	<c:if test="${! empty timeEmpWorkDetails.details }">
		<div class="form-group" style="margin-top: 25px;">
			<div class="col-sm-offset-4 col-sm-6">
				<button type="submit" class="btn btn-info" name="type" value="xls">
					Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
				</button>
				<button type="submit" class="btn btn-info" name="type" value="csv">
					Export to CSV &nbsp; <i class="fa fa-file-text-o"></i>
				</button>
				<button type="submit" class="btn btn-info" name="type" value="pdf">
					Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
				</button>
			</div>
		</div>
	</c:if>
	</form:form>
	<!-- <button class="btn btn-info"
		onclick="window.open('data:application/vnd.ms-excel,' + document.getElementById('table4').outerHTML.replace(/ /g, '%20'));">
		Export to XLS &nbsp; <i class="fa fa-file-excel-o"></i>
	</button> -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>
</html>