<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
#asgnmnt_failed {
	display: none
}

#asgnmnt_sve_sccs {
	display: none
}
</style>
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Assignments</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="assignments.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="success_msg" id="asgnmnt_sve_sccs">
					<span id="msgs_icn"><i class="fa fa-check"></i></span>Saved
					Successfully.
				</div>
				<div class="error_msg" id="asgnmnt_failed">
					<span id="msge_icn"><i class="fa fa-times"></i></span>Failed...
				</div>
			</div>
		</div>



		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>

				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form commandName="assignmentVo">
								<form:hidden path="assignmentId" id="assignmentId" />
								<form:hidden path="edit" id="edit" />
								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Assignment Information</div>
												<div class="panel-body">

													<div id="hide_edit">

														<div class="form-group">
															<label for="inputEmail3"
																class="col-sm-3 col-xs-12 control-label">Employee
																Name <span class="stars">*</span></label>
															<div class="col-sm-8 col-xs-10 br-de-emp-list">
																<form:input path="employee" class="form-control"
																	placeholder="Search Employee" id="employeeSl" />
																<form:hidden path="employeeCode" id="employeeCode" />
																<div id="assgnmnt_emp_slct" class="alert_box">
																	<div class="triangle-up"></div>
																	Please select employee..
																</div>
															</div>
															<div class="col-sm-1 col-xs-1">
																<a href="#" class="show2"><i class="fa fa-user"></i></a>
															</div>
														</div>

														<div id="pop1" class="simplePopup">
														</div>

														<div id="pop2" class="simplePopup empl-popup-gen"></div>



														<div class="form-group">
															<label for="inputEmail3"
																class="col-sm-3 col-xs-12 control-label">Project <span class="stars">*</span></label>
															<div class="col-sm-8 col-xs-10">
																<select class="form-control" id="assgnmt_add_prjct">
																</select>
																<div id="assgnmnt_prjct_slct" class="alert_box">
																	<div class="triangle-up"></div>
																	Please select project..
																</div>
															</div>

															<!-- <div class="col-sm-1 col-xs-1">
															<a href="#" class="show1"><i
																class="fa fa-plus-square-o pop_icon"></i></a>
														</div> -->
														</div>

														<div id="pop1" class="simplePopup">
															<div class="col-md-12">
																<div class="form-group">
																	<h1 class="pop_hd">Contract Type</h1>

																	<div class="row">

																		<div class="col-md-12">
																			<div class="form-group">

																				<div class="col-sm-12">
																					<input type="text" class="form-control"
																						id="inputPassword3" placeholder=" Contract Type">

																				</div>
																			</div>

																			<div class="form-group">
																				<div class="col-sm-offset-3 col-sm-9">
																					<button type="submit" class="btn btn-info flr">Save</button>
																				</div>
																			</div>


																		</div>


																	</div>
																</div>


															</div>


														</div>





													</div>
													<div id="id_add">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Employee</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-3 control-label ft">${assignmentVo.employee }</label>
															</div>
														</div>

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Project</label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-3 control-label ft">${assignmentVo.projectTitle }</label>
															</div>
														</div>

													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Assignment
															Name <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input path="assignmentName" cssClass="form-control"
																id="assgmnt_add_name" />
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label"> Start Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepicker">
																<form:input path="startDate" cssClass="form-control"
																	readonly="true"
																	style="background-color: #ffffff !important;"
																	id="assgmnt_add_strtdt" data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label"> End Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepickers">
																<form:input path="endDate" cssClass="form-control"
																	readonly="true"
																	style="background-color: #ffffff !important;"
																	id="assgmnt_add_enddt" data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
															<div id="assgnmnt_end_date" class="alert_box">
																<div class="triangle-up"></div>
																Please select valid date..
															</div>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label"> Priority <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select path="priority" id="assgmnt_add_prio"
																cssClass="form-control">
																<form:option value="Highest">Highest</form:option>
																<form:option value="High">High</form:option>
																<form:option value="Medium">Medium</form:option>
																<form:option value="Low">Low</form:option>
																<form:option value="Lowest">Lowest</form:option>
															</form:select>
														</div>

													</div>




												</div>
											</div>
										</div>


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Assignment Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">


															<form:textarea path="description" cssClass="form-control"
																id="assgmnt_add_dscr"
																cssStyle="height: 150px;resize:none" />




														</div>
													</div>



												</div>
											</div>
										</div>


									</div>

									<div class="col-md-6">


										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" id="assgmnt_add_nts"
																cssClass="form-control height" />
														</div>
													</div>


													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${assignmentVo.createdBy }</label>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${assignmentVo.createdOn }</label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="submit" onclick="validateAddAssignment()"
																class="btn btn-info">Save</a>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								</div>

							</form:form>

						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">

							<c:if test="${ rolesMap.roles['Assignment Documents'].add}">
								<div class="col-md-6">
									<form:form action="uploadAssignmntDoc.do" method="POST"
										commandName="assignmentVo" enctype="multipart/form-data">
										<form:hidden path="assignmentId" id="assignmentIdd" />
										<div class="panel panel-default">
											<div class="panel-heading">Upload Documents</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Upload
														Documents </label>
													<div class="col-sm-9">
														<input type="file" multiple name="upload_assignmnt_file"
															id="assignment-docs" class="btn btn-info" />
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" id="assignment_doc_btn"
															class="btn btn-info">Save</button>
													</div>
												</div>

											</div>
										</div>
									</form:form>
								</div>
							</c:if>


							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">

										<c:if test="${!empty assignmentVo.documentsVos}">
											<table class="table no-margn">
												<thead>
													<tr>
														<th>#</th>
														<th>Document Name</th>
														<c:if
															test="${ rolesMap.roles['Assignment Documents'].view}">
															<th>view</th>
														</c:if>
														<c:if
															test="${ rolesMap.roles['Assignment Documents'].delete}">
															<th>Delete</th>
														</c:if>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${assignmentVo.documentsVos}" var="d"
														varStatus="s">
														<tr>
															<td>${s.count }</td>
															<td>${d.fileName }</td>
															<c:if
																test="${ rolesMap.roles['Assignment Documents'].view}">
																<td><a href="downloadAssignmentDoc.do?url=${d.url }"><i
																		class="fa fa-file-text-o sm"></i></a></td>
															</c:if>
															<c:if
																test="${ rolesMap.roles['Assignment Documents'].delete}">
																<td><a href="deleteAssignmentDoc.do?url=${d.url }"><i
																		class="fa fa-close ss"></i></a></td>
															</c:if>
														</tr>
													</c:forEach>
													<!-- <tr>
													<td><i class="fa fa-file-o"></i> Lorem ipsum dolor sit
														amet</td>
													<td><i class="fa fa-file-text-o sm"></i></td>
													<td><i class="fa fa-close ss"></i></td>
												</tr> -->

												</tbody>
											</table>
										</c:if>

									</div>
								</div>
							</div>




						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Simple Map</div>
									<div class="panel-body">
										<div id="simpleMap" style="height: 500px;"></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->

	</section>
	<!-- Content Block Ends Here (right box)-->



	<!-- JQuery v1.9.1 -->

	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#assignment_doc_btn").attr('disabled', 'true');

			if ($("#assignment-docs").val() == '') {
					$("#assignment_doc_btn").attr('disabled', 'true');
			}

			$("#assignment-docs").on("change", function() {
				if ($("#assignmentIdd").val() != '')
				$("#assignment_doc_btn").removeAttr('disabled');
			});

			$('#employeeSl').bind('click', function() {
				showPop();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			if ($("#edit").val() != '') {
				$("#id_add").show();
				$("#hide_edit").hide();
			} else {
				$("#hide_edit").show();
				$("#id_add").hide();
			}

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>

</body>
</html>