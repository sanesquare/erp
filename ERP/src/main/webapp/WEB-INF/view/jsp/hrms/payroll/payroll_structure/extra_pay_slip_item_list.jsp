<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered"
	id="extra-pay-slip-items-table">
	<thead>
		<tr>
			<th width="5%">S#</th>
			<th width="20%">Title</th>
			<th width="10%">Type</th>
			<th width="5%">Taxable Allowance</th>
			<th width="20%">Calculation</th>
			<th width="10%">With Effect From</th>
			<th width="10%"></th>
		</tr>
	</thead>
	<tbody id="extr-ps-div">
		<c:choose>
			<c:when test="${not empty extraPaySlipItems}">
				<c:forEach items="${extraPaySlipItems}" var="extraPaySlipItem"
					varStatus="row">
					<tr>
						<td>${row.count}</td>
						<td><input type="text" name="etitle"
							value="${extraPaySlipItem.title}"
							class="form-control itemeTitle${row.count}" /></td>
						<td><select class="form-control itemeType${row.count}"
							name="etype">
								<option value="Calculation"
									${extraPaySlipItem.type == 'Calculation' ? 'selected' : ''}>Calculation</option>
								<option value="Fixed"
									${extraPaySlipItem.type == 'Fixed' ? 'selected' : ''}>Fixed</option>
						</select></td>
						<td><select class="form-control itemeTax${row.count}"
							name="etaxAllowance">
								<option value="true"
									${extraPaySlipItem.taxAllowance == true ? 'selected' : ''}>Yes</option>
								<option value="false"
									${extraPaySlipItem.taxAllowance == false ? 'selected' : ''}>No</option>
						</select></td>
						<td><input type="text"
							value="${extraPaySlipItem.calculation}"
							class="form-control typeahead itemeSyntax${row.count}"
							name="ecalculation" id="ps-kw-exp" /></td>
						<td><input type="text" name="eeffectFrom"
							value="${extraPaySlipItem.effectFrom}"
							class="form-control itemeFrom${row.count}"
							data-date-format="DD/MM/YYYY" id="dt-ps" /></td>
						<td><input type="hidden" id="itemeId${row.count}"
							name="epaySlipItemId" value="${extraPaySlipItem.extraPaySlipItemId}" /><input
							type="button" value="Update"
							onclick="updateePaySlip(${row.count})" id="addButton1"
							class="btn btn-info btn-xs"> <c:if
								test="${fn:length(extraPaySlipItems) eq row.count}">
												&nbsp;<input type="button" value="Add Row"
									onclick="addePSNewRow()" id="addButton1"
									class="btn btn-danger btn-xs">
							</c:if></td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr>
					<td>1</td>
					<td><input type="text" name="etitle"
						class="form-control itemeTitle1" /></td>
					<td><select class="form-control itemeType1" name="etype">
							<option value="Calculation">Calculation</option>
							<option value="Fixed">Fixed</option>
					</select></td>
					<td><select class="form-control itemeTax1"
						name="etaxAllowance">
							<option value="true">Yes</option>
							<option value="false">No</option>
					</select></td>
					<td><input type="text"
						class="form-control typeahead itemeSyntax1" name="ecalculation"
						id="ps-kw-exp" /></td>
					<td><input type="text" name="eeffectFrom"
						class="form-control itemeFrom1" data-date-format="DD/MM/YYYY"
						id="dt-ps" /></td>
					<td><input type="hidden" id="itemeId1" name="epaySlipItemId"
						value="0" /><input type="button" value="Save"
						onclick="saveePaySlipItem()" id="epsButtonSave1"
						class="btn btn-info btn-xs"></td>
				</tr>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>

<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
<script src="<c:url value='/resources/assets/js/auto_complete.js' />"></script>
