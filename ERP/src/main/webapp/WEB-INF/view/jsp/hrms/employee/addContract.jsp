<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
#cntrtc_sve_sccs {
	display: none
}

#cntrtc_failed {
	display: none
}
</style>
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Contracts</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="contracts.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="success_msg" id="cntrtc_sve_sccs">
					<span id="msgs_icn"><i class="fa fa-check"></i></span>Saved
					Successfully.
				</div>
				<div class="error_msg" id="cntrtc_failed">
					<span id="msge_icn"><i class="fa fa-times"></i></span>Failed...
				</div>
			</div>
		</div>


		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>

				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form action="saveContract.do" method="POST"
								commandName="contractVo">

								<form:hidden path="contractId" id="contrctId" />

								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Contract Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Employee
															Name <span class="stars">*</span></label>
														<div class="col-sm-8 col-xs-10 br-de-emp-list">
															<form:input path="employee" class="form-control"
																placeholder="Search Employee" id="employeeSl" />
															<form:hidden path="employeeCode" id="employeeCode" />
															<div id="emp_cntrt_emp_err" class="alert_box">
																<div class="triangle-up"></div>
																Please select employee..
															</div>
														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div>
													</div>

													<div id="pop1" class="simplePopup">
													</div>

													<div id="pop2" class="simplePopup empl-popup-gen"></div>





													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Contract
															Type <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select path="contractTypeId" id="contrct_add_type"
																cssClass="form-control">
																<option value="">--Select Type--</option>
																<form:options items="${ type}" itemLabel="contractType"
																	itemValue="contractTypeId" />
															</form:select>

														</div>

													</div>



													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Contract
															Title <span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:input id="cntrt_add_title" path="title"
																cssClass="form-control" />
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Contract
															Start Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepicker">
																<form:input path="startingDate" readonly="true"
																	style="background-color: #ffffff !important;"
																	cssClass="form-control" id="cntrt_add_strdte"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Contract End
															Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepickers">
																<form:input path="endDate" readonly="true"
																	style="background-color: #ffffff !important;"
																	cssClass="form-control" id="cntrt_add_enddte"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
																<div id="prjct_end_date_err" class="alert_box">
																	<div class="triangle-up"></div>
																	<spring:message code="project.info.date.err_msg" />
																</div>
															</div>
														</div>

													</div>


												</div>
											</div>
										</div>



										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Employee Details</div>
												<div class="panel-body">

													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Designation</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_desg"></label>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Type</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_type"></label>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Category</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_ctgry"></label>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Grade</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_grde"></label>
														</div>

													</div>



													<div class="form-group">
														<label class="col-sm-3 control-label">Branch</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_brnch"></label>
														</div>

													</div>




													<div class="form-group">
														<label class="col-sm-3 control-label">Department</label>
														<div class="col-sm-9">
															<label class="col-sm-3 control-label"
																id="cntrct_emp_dept"></label>
														</div>

													</div>



												</div>
											</div>
										</div>

									</div>

									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Contract Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">


															<form:textarea path="description"
																id="cntrct_add_descrptn" cssClass="form-control"
																cssStyle="height: 150px;resize:none" />




														</div>
													</div>

												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" id="cntrct_add_notes"
																cssClass="form-control height" />
														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="contrct_info_save_btn"
																onclick="validateContrctAdd()" class="btn btn-info">Save</a>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								</div>
							</form:form>



						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<form:form action="uploadContrctDocs.do" method="POST"
								commandName="contractDocumentVo" enctype="multipart/form-data">
								<form:hidden path="contractId" id="contrctIdd" />
								<c:if test="${ rolesMap.roles['Contract Document'].add}">
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Upload Documents</div>
										<div class="panel-body">

											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Upload
													Documents </label>
												<div class="col-sm-9">
													<input type="file" multiple name="cntrctFileUpload"
														class="btn btn-info" />
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-16 col-sm-12">
													<button type="submit" id="contrct_doc_upload_btn"
														class="btn btn-info">Upload</button>
												</div>
											</div>

										</div>
									</div>
								</div>
								</c:if>


								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Show Documents</div>
										<div class="panel-body">

											<c:if test="${!empty contractVo.documentVos }">
												<table class="table no-margn">
													<thead>
														<tr>
														<th>#</th>
															<th>Document Name</th>
															<c:if test="${ rolesMap.roles['Contract Document'].view}">
															<th>view</th>
															</c:if>
															<c:if test="${ rolesMap.roles['Contract Document'].delete}">
															<th>Delete</th>
															</c:if>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${contractVo.documentVos }" var="d" varStatus="s">
															<tr>
															<td>${s.count }</td>
																<td>${d. fileName}</td>
																<c:if test="${ rolesMap.roles['Contract Document'].view}">
																<td><a href="viewDocCOntrct.do?url=${d. url}"><i
																		class="fa fa-close ss"></i></a></td>
																		</c:if>
																		<c:if test="${ rolesMap.roles['Contract Document'].delete}">
																<td><a href="deleteDocCOntrct.do?url=${d. url}"><i
																		class="fa fa-close ss"></i></a></td>
																		</c:if>
															</tr>

														</c:forEach>
													</tbody>
												</table>
											</c:if>

										</div>
									</div>
								</div>


							</form:form>

						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Simple Map</div>
									<div class="panel-body">
										<div id="simpleMap" style="height: 500px;"></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->

	</section>
	<!-- Content Block Ends Here (right box)-->



	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#contrct_doc_upload_btn").attr('disabled', 'true');

			$('#employeeSl').bind('click', function() {
				showPop();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>