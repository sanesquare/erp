<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#payslip_start_date_error {
	display: none
}

#payslip_end_date_error {
	display: none
}

#salary_payslip_table {
	display: none
}
</style>
</head>
<body>

	<div class="success_full" id="payslip_success">
		<div class="success_msgss">Saving...</div>
	</div>



	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Salary Payslips</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<a href="payslip.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form action="savePayslip.do" method="POST"
								commandName="paysSlipVo">
								<form:hidden path="id" id="payslip_id" />
								<div class="row">
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Salary Payslip Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Employee
														Name <span class="stars">*</span></label>
													<div class="col-sm-8 col-xs-10 br-de-emp-list">
														<form:input path="employee" class="form-control"
															placeholder="Search Employee" id="employeeSl" />
														<form:hidden path="employeeCode" id="employeeCode" />
													</div>
													<div class="col-sm-1 col-xs-1">
														<a href="#" class="show2"><i class="fa fa-user"></i></a>
													</div>
												</div>

												<div id="pop1" class="simplePopup">
												</div>

												<div id="pop2" class="simplePopup empl-popup-gen"></div>


												<div class="form-group">
													<label class="col-sm-3 control-label">Salary Date <span class="stars">*</span></label>
													<div class="col-sm-9">
														<div class="input-group date" id="datepicker">
															<form:input path="salaryDate" class="form-control"
																readOnly="readOnly"
																style="background-color:#fff !important"
																id="salary_daye" data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>

													</div>

												</div>







											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Salary Payslip Period</div>
											<div class="panel-body">
												<%-- <div class="form-group">
													<label class="col-sm-3 control-label">Start Date</label>
													<div class="col-sm-9">
														<div class="input-group date" id="datepickers">
															<form:input path="startDate" class="form-control"
																readOnly="readOnly"
																style="background-color:#fff !important"
																id="payslip_start_date" data-date-format="DD/MM/YYYY" />
															<div id="payslip_start_date_error" class="alert_box">
																<div class="triangle-up"></div>
																Please select valid date..
															</div>
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>

													</div>

												</div> --%>
												
												<div class="form-group">
												<label class="col-sm-3 control-label">Date (From) <span class="stars">*</span></label>
												<div class="col-sm-5">
													<form:select class="form-control" path="fromMonth"  id="fromMOnth">
														<option value="1">January</option>
														<option value="2">February</option>
														<option value="3">March</option>
														<option selected="true" value="4">April</option>
														<option value="5">May</option>
														<option value="6">June</option>
														<option value="7">July</option>
														<option value="8">August</option>
														<option value="9">September</option>
														<option value="10">October</option>
														<option value="11">November</option>
														<option value="12">December</option>
													</form:select>
												</div>
												<div class="col-sm-4">
													<form:select path="fromYear" class="form-control" id="fromYear">
														<option value="2000">2000</option>
														<option value="2001">2001</option>
														<option value="2002">2002</option>
														<option value="2003">2003</option>
														<option value="2004">2004</option>
														<option value="2005">2005</option>
														<option value="2006">2006</option>
														<option value="2007">2007</option>
														<option value="2008">2008</option>
														<option value="2009">2009</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option selected="true" value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
														<option value="2023">2023</option>
														<option value="2024">2024</option>
														<option value="2025">2025</option>
														<option value="2026">2026</option>
														<option value="2027">2027</option>
														<option value="2028">2028</option>
														<option value="2029">2029</option>
														<option value="2030">2030</option>
													</form:select>
												</div>
											</div>


												<%-- <div class="form-group">
													<label class="col-sm-3 control-label">End Date</label>
													<div class="col-sm-9">
														<div class="input-group date" id="datepickerss">
															<form:input path="endDate" class="form-control"
																readOnly="readOnly"
																style="background-color:#fff !important"
																id="payslip_end_date" data-date-format="DD/MM/YYYY" />
															<div id="payslip_end_date_error" class="alert_box">
																<div class="triangle-up"></div>
																Please select valid date..
															</div>
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>

													</div>

												</div> --%>
												
												<div class="form-group">
												<label class="col-sm-3 control-label">Date (To) <span class="stars">*</span></label>
												<div class="col-sm-5">
													<form:select path="toMonth" class="form-control" id="toMonth">
														<option value="1">January</option>
														<option value="2">February</option>
														<option value="3">March</option>
														<option selected="true" value="4">April</option>
														<option value="5">May</option>
														<option value="6">June</option>
														<option value="7">July</option>
														<option value="8">August</option>
														<option value="9">September</option>
														<option value="10">October</option>
														<option value="11">November</option>
														<option value="12">December</option>
													</form:select>
												</div>
												<div class="col-sm-4">
													<form:select path="toYear" class="form-control" id="toYear">
														<option value="2000">2000</option>
														<option value="2001">2001</option>
														<option value="2002">2002</option>
														<option value="2003">2003</option>
														<option value="2004">2004</option>
														<option value="2005">2005</option>
														<option value="2006">2006</option>
														<option value="2007">2007</option>
														<option value="2008">2008</option>
														<option value="2009">2009</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option selected="true" value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
														<option value="2023">2023</option>
														<option value="2024">2024</option>
														<option value="2025">2025</option>
														<option value="2026">2026</option>
														<option value="2027">2027</option>
														<option value="2028">2028</option>
														<option value="2029">2029</option>
														<option value="2030">2030</option>
													</form:select>
												</div>
											</div>
											
											
												<!-- <div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="button"
															onclick="getPayslipItemsByEmployee()"
															class="btn btn-info">Go</button>
													</div>
												</div> -->

											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<%-- <div class="col-md-12" id="salary_payslip_table">
										<div class="panel panel-default">
											<div class="panel-heading">Salary</div>
											<div class="panel-body">
												<table class="table no-margn" id="payslipTable">
													<thead>
														<tr>
															<th width="5%">#</th>
															<th width="75%">Salary Item</th>
															<th width="20%">Amount</th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
												<table class="table no-margn">
													<tr>
														<td width="5%"></td>
														<td width="70%"><strong>Total</strong></td>
														<td width="25%"><form:input type="text" path="total"
																readOnly="readOnly"
																style="background-color: #fff !important"
																class="form-control" id="payslip_total_amount"
																placeholder="" /></td>

													</tr>
												</table>

											</div>
										</div>
									</div> --%>
									<c:if test="${! empty paysSlipVo.items}">
										<div class="col-md-12" id="salary_payslip_table2">
											<div class="panel panel-default">
												<div class="panel-heading">Salary</div>
												<div class="panel-body">
													<table class="table no-margn" id="payslipTable">
														<thead>
															<tr>
																<th width="5%">#</th>
																<th width="75%">Salary Item</th>
																<th width="20%">Amount</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${ paysSlipVo.items}" var="i"
																varStatus="s">
																<tr>
																	<td><input type="text" readonly="readonly"
																		style="background-color: #fff !important"
																		class="form-control" value="${s.count }"></td>
																	<td><input type="text" readonly="readonly"
																		style="background-color: #fff !important"
																		class="form-control" value="${i.title }"></td>
																	<td><input type="text" readonly="readonly"
																		style="background-color: #fff !important"
																		class="form-control" value="${i.amount }"></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
													<table class="table no-margn">
														<tr>
															<td width="5%"></td>
															<td width="70%"><strong>Total</strong></td>
															<td width="25%"><form:input type="text" path="total"
																	readOnly="readOnly"
																	style="background-color: #fff !important"
																	class="form-control" id="payslip_total_amounttt"
																	placeholder="" /></td>

														</tr>
													</table>

												</div>
											</div>
										</div>
									</c:if>
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<form:textarea path="notes" class="form-control height"
															id="payslip_notes" />
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${paysSlipVo.createdBy }</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${paysSlipVo.createdOn }
														</label>
													</div>
												</div>

												<!-- <div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="button" onclick="validatePayslip()"
															class="btn btn-info">Save</button>
													</div>
												</div> -->
												
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" 
															class="btn btn-info">Save</button>
													</div>
												</div>

											</div>
										</div>
									</div>

								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>


	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			getEmployeeDetails();

			if ("${paysSlipVo.salaryDate}" == "")
				$("#salary_daye").val(convertDate(new Date()));

			$('#employeeSl').bind('click', function() {
				showPop();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>