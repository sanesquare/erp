<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />


</head>
<body>

	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Recruitment Reports</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="parentVerticalTab">
					<ul class="resp-tabs-list hor_1">
						<c:if test="${ rolesMap.roles['Job Post Report'].view}">
            <li>Job Post </li>
            </c:if>
            <c:if test="${ rolesMap.roles['Job Candidates Report'].view}">
            <li>Job Candidates </li>
            </c:if>
            <c:if test="${ rolesMap.roles['Job Interviews Report'].view}">
            <li>Job Interviews </li>
            </c:if>


					</ul>
					<div class="resp-tabs-container hor_1">
					<c:if test="${ rolesMap.roles['Job Post Report'].view}">
						<div>
							<div class="col-md-6">
								<div class="panel panel-default">
										<div class="panel-heading">Job Posts</div>
										<form:form action="viewPostReport.do" method="POST"
										target="_blank" commandName="recPostsVo">
										<div class="panel-body">

											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label">Department</label>
												<div class="col-sm-9">
													<form:select path="depId" class="form-control">
														<form:option value="0" label="All Departments" />
														<form:options items="${departments }" itemValue="id"
															itemLabel="name" />

													</form:select>
												</div>
											</div>


											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-6">
													<button type="submit" class="btn btn-info">Generate
														Report</button>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Job Candidates Report'].view}">
						<div>
							<div class="col-md-6">
								<div class="panel panel-default">
										<div class="panel-heading">Job Candidates</div>
										<form:form action="viewCandReport.do" method="POST"
										target="_blank" commandName="recCandVo">
										<div class="panel-body">
											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label">Job Post
													 </label>
												<div class="col-sm-9">
													<form:select path="designation" class="form-control">
														<form:option value="" label="All Job Post" />
														<form:options items="${posts }" itemValue="jobTitle"
															itemLabel="jobTitle" />
													</form:select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label">Gender</label>
												<div class="col-sm-9">
													<form:select path="gender" class="form-control">
														<form:option value="" label="Any Gender" />
														<form:option value="male" label="Male Only" />
														<option value="female" label="Female Only" />
													</form:select>
												</div>
											</div>


											<div class="form-group">
												<div class=" col-sm-6">
													<button type="submit" class="btn btn-info">Generate
														Report</button>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
						</c:if>
						<c:if test="${ rolesMap.roles['Job Interviews Report'].view}">
						<div>
							<div class="col-md-6">
								<div class="panel panel-default">
										<div class="panel-heading">Job Interviews</div>
										<form:form action="viewInterviewReport.do" method="POST"
										 commandName="recIntVo" id="recInterForm">
										<div class="panel-body">
											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label"> Branch</label>
												<div class="col-sm-9">
													<form:select path="branchID"
														style="background-color: #ffffff !important;"
														cssClass="form-control">
														<form:option value="0" label="All Branches " />
														<form:options items="${branches }" itemLabel="branchName"
															itemValue="branchId" />
													</form:select>
												</div>
											</div>



											<div class="form-group">
												<label for="inputEmail3"
													class="col-sm-3 col-xs-12 control-label">Job Post
													(Applied On)</label>
												<div class="col-sm-9">
													<form:select path="postId" class="form-control">
														<form:option value="0" label="All Job Post" />
														<form:options items="${posts }" itemValue="jobPostId"
															itemLabel="jobTitle" />

													</form:select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label"> Interview
													Date Start<span class="stars">*</span> </label>
												<div class="col-sm-9">
													<div class="input-group date intDate" id="datepicker">
														<form:input path="startDate" type="text"
															class="form-control" data-date-format="DD/MM/YYYY"
															id="recStartDate" readonly="true"
															style="background-color: #ffffff !important;" />
														<span class="input-group-addon"><span
															class="glyphicon-calendar glyphicon"></span> </span>
													</div>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Interview Date
													End<span class="stars">*</span></label>
												<div class="col-sm-9">
													<div class="input-group date intDate" id="datepickers">
														<form:input path="endDate" type="text"
															class="form-control" data-date-format="DD/MM/YYYY"
															id="recEndDate" readonly="true"
															style="background-color: #ffffff !important;" />
														<span class="input-group-addon"><span
															class="glyphicon-calendar glyphicon"></span> </span>
													</div>
												</div>
											</div>

											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-6">
													<button type="submit" class="btn btn-info">Generate
														Report</button>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
						</c:if>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<script src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script>
		$(document).ready(function() {
			$('#parentVerticalTab').easyResponsiveTabs({

				type : 'vertical', //Types: default, vertical, accordion 
				width : 'auto', //auto or any width like 600px 
				fit : true, // 100% fit in a container 
				closed : 'accordion', // Start closed if in accordion view 
				tabidentify : 'hor_1', // The tab groups identifier 
				activate : function(event) { // Callback function if tab is switched 
					var $tab = $(this);
					var $info = $('#nested-tabInfo2');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
			if ("${recIntVo.startDate}" == "")
				$("#recStartDate").val(convertDate(new Date()));
			if ("${recIntVo.endDate}" == "")
				$("#recEndDate").val(convertDate(new Date()));
			
			$("#dashbordli").removeClass("active");
			 $("#organzationli").removeClass("active");
			   $("#recruitmentli").removeClass("active");
			   $("#employeeli").removeClass("active");
			   $("#timesheetli").removeClass("active");
			   $("#payrollli").removeClass("active");
			   $("#reportsli").addClass("active");
		});
	</script>

</body>
</html>