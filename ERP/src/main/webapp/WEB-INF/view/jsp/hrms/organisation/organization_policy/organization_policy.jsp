<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/dialog/bootstrap-dialog.min.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Organization Policies</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Organization Policies</div>
					<div class="panel-body" id="organization-policy-list">
						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="20%">Title</th>
									<th width="20%">Policy Type</th>
									<th width="20%">Branch</th>
									<th width="20%">Department</th>
									<c:if test="${ rolesMap.roles['Organization Policies'].update}">
									<th width="10%">Edit</th></c:if>
									<c:if test="${ rolesMap.roles['Organization Policies'].delete}">
									<th width="10%">Delete</th></c:if>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${policies}" var="policy">
									<tr>
									<c:if test="${ rolesMap.roles['Organization Policies'].view}">
										<td><a
											href="ViewOrganizationPolicy.do?opi=${policy.org_policy_id}">${policy.title}</a></td></c:if>
											<c:if test="${ rolesMap.roles['Organization Policies'].view}">
										<td><a
											href="ViewOrganizationPolicy.do?opi=${policy.org_policy_id}">${policy.policyType}</a></td></c:if>
											<c:if test="${ rolesMap.roles['Organization Policies'].view}">
										<td><a
											href="ViewOrganizationPolicy.do?opi=${policy.org_policy_id}">${policy.branches}</a></td></c:if>
											<c:if test="${ rolesMap.roles['Organization Policies'].view}">
										<td><a
											href="ViewOrganizationPolicy.do?opi=${policy.org_policy_id}">${policy.departments}</a></td></c:if>
											<c:if test="${ rolesMap.roles['Organization Policies'].update}">
										<td><a
											href="EditOrganizationPolicy.do?opi=${policy.org_policy_id}"><i
												class="fa fa-edit sm"></i> </a></td></c:if>
												<c:if test="${ rolesMap.roles['Organization Policies'].delete}">
										<td><a href="#"
											onclick="organizationPolicyDelConfirm(${policy.org_policy_id})"><i
												class="fa fa-close ss"></i></a></td></c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<c:if test="${ rolesMap.roles['Organization Policies'].add}">
						<a href="AddOrganizationPolicy.do">
							<button type="button" class="btn btn-primary">Add New
								Policy</button>
						</a>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>


	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/js/dialog/bootstrap-dialog.min.js' />"></script>

	<script type="text/javascript">
	organizationPolicyDelConfirm=function(id) {
		BootstrapDialog.confirm(
				'Are you sure you want to delete this record?',
				function(result) {
					if (result) {
						deleteOrganizationPolicy(id);
					}
				});
	};
		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").addClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
	<!-- Custom JQuery -->
	<script src="assets/js/app/custom.js" type="text/javascript"></script>
</body>


</html>