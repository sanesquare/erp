<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
<div class="warper container-fluid">
        	
            <div class="page-header"><h1>Announcements </h1></div>
            
            
            
            
            
            <div class="row">
            	
            
           <div class="col-md-12">
           
          
             <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" role="tab" data-toggle="tab">Information</a></li>
              <li role="presentation"><a href="#users" role="tab" data-toggle="tab">Documents</a></li>
             <li role="presentation"><a href="#docs" role="tab" data-toggle="tab"> Status</a></li>
             
            </ul>
          
          
          
          <div class="tab-content">
              <div role="tabpanel" class="panel panel-default tab-pane tabs-up active" id="all">
              	<div class="panel-body">
                	
                    <div class="row">
                    <div class="col-md-6">
                    <div class="col-md-12">
                	<div class="panel panel-default">
                        <div class="panel-heading">Announcement Information</div>
                        <div class="panel-body">
                        
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label"> Title </label>
                                <div class="col-sm-9">
                               <label for="inputEmail3" class="col-sm-3 control-label ft">${announcements.announcementsTitle }</label>
                                </div>
                              </div>
                            
                       
                           
                        </div>
                    </div>
                </div>
                
                
                
                <div class="col-md-12">
                	<div class="panel panel-default">
                        <div class="panel-heading">Announcement Dates</div>
                        <div class="panel-body">
                          <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Announcement Start Date</label>
                                <div class="col-sm-9">
                                <label for="inputPassword3" class="col-sm-3 control-label ft"> ${announcements.startDate }</label>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Announcement End Date</label>
                                <div class="col-sm-9">
                             <label for="inputPassword3" class="col-sm-3 control-label ft">${announcements.endDate }</label>
                                </div>
                              </div>
                       
                              
                       
                           
                        </div>
                    </div>
                </div>
                    
                
                    </div>
                    
                    <div class="col-md-6">
                    <div class="col-md-12">
                	<div class="panel panel-default">
                        <div class="panel-heading">Message</div>
                        <div class="panel-body">
                        
                             <div class="form-group">
                              
                                <div class="col-sm-12">
                                  <label for="inputPassword3" class="col-sm-8 control-label ft">${announcements.message }</label>
                                </div>
                              </div>
                              
                       
                        </div>
                    </div>
                </div>
                
                 <div class="col-md-12">
                	<div class="panel panel-default">
                        <div class="panel-heading">Additional Information</div>
                        <div class="panel-body">
                        
                             <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Notes </label>
                                <div class="col-sm-9">
                                  <label for="inputPassword3" class="col-sm-8 control-label ft">${announcements.notes }</label>
                                </div>
                              </div>
                              
                              
                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Record Added By </label>
                                <div class="col-sm-9">
                                  <label for="inputPassword3" class="col-sm-9 control-label ft">${announcements.createdBy } </label>
                                </div>
                              </div>
                               
                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Record Added on </label>
                                <div class="col-sm-9">
                               <label for="inputPassword3" class="col-sm-9 control-label ft">${announcements.createdOn }<a href="#"class="show3"><i class="fa fa-user"></i> </a> </label>
                                </div>
                              </div>
                              
                              
                              <div id="pop3" class="simplePopup">
    <h1 class="pop_hd">System Administrator</h1>
    <div class="row">
    
     <div class="col-md-3"><img src="assets/images/avtar/user.png"/ class="pop_img"></div>
      <div class="col-md-9">  
      
       <div class="form-group">
       <label for="inputPassword3" class="col-sm-4 control-label">E-mail </label>
        <div class="col-sm-8">
         <label for="inputPassword3" class="col-sm-6 control-label ft">jsafs@.com</label>
          </div>
           </div>
             <div class="form-group">
       <label for="inputPassword3" class="col-sm-4 control-label">Designation </label>
        <div class="col-sm-8">
         <label for="inputPassword3" class="col-sm-6 control-label ft">2545895678</label>
          </div>
           </div>
             <div class="form-group">
       <label for="inputPassword3" class="col-sm-4 control-label">Department </label>
        <div class="col-sm-8">
         <label for="inputPassword3" class="col-sm-6 control-label ft">dsafdsfdsf</label>
          </div>
           </div>
             <div class="form-group">
       <label for="inputPassword3" class="col-sm-4 control-label">Station </label>
        <div class="col-sm-8">
         <label for="inputPassword3" class="col-sm-6 control-label ft">2sdfds8</label>
          </div>
           </div>
             <div class="form-group">
       <label for="inputPassword3" class="col-sm-4 control-label">Join Date </label>
        <div class="col-sm-8">
         <label for="inputPassword3" class="col-sm-6 control-label ft">25-2-2015</label>
          </div>
           </div>
           
                              
       
      </div>
    
    </div>
</div>
                           
                        
                        </div>
                    </div>
                </div>
                
                </div>
                    </div>
                    
                    
                    
              </div></div>
              <div role="tabpanel" class="panel panel-default tab-pane tabs-up " id="users">
              	<div class="panel-body">
                
                <div class="col-md-6">
                	<div class="panel panel-default">
                        <div class="panel-heading">Upload Documents</div>
                        <div class="panel-body">
                        
                             <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Upload Documents </label>
                                <div class="col-sm-9">
                                  <input type="file" class="btn btn-info"/>
                                </div>
                              </div>
                            
                        
                        </div>
                    </div>
                </div>
                
                
                <div class="col-md-6">
                	<div class="panel panel-default">
                        <div class="panel-heading">Show Documents</div>
                        <div class="panel-body">
                        
                             
                          <table class="table no-margn">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Document Name</th>
                                  <th>view</th>
                                  <th>Delete</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>1</td>
                                  <td><i class="fa fa-file-o"></i> Lorem ipsum dolor sit amet</td>
                                  <td><i class="fa fa-file-text-o sm"></i> </td>
                                   <td><i class="fa fa-close ss"></i> </td>
                                </tr>
                               
                              </tbody>
                            </table>    
                              
                         
                        </div>
                    </div>
                </div>
                
                 
                    
                    
                </div>
              </div>
              
              
              
              <div role="tabpanel" class="panel panel-default tab-pane tabs-up " id="docs">
              	<div class="panel-body">
                
                <div class="col-md-6">
                	<div class="panel panel-default">
                        <div class="panel-heading"> Status</div>
                        <div class="panel-body">
                        
                             <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label"> Status </label>
                                <div class="col-sm-9">
         
                                 <!-- <select class="form-control" >
                                <option>One</option>
                                  <option>One</option>
                                    <option>One</option>
                                </select> -->
                          <%--   <form:select cssClass="form-control" path="announcementsStatus">
                                 <c:forEach items="${status }" var="status">
                                <option>${status.announcementsStatus }</option>
                                  </c:forEach>
                                   
                                </form:select> --%>
                                <label>${announcements.announcementsStatus }</label>
                                </div>
                              </div>
                            
                        <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Notes </label>
                                <div class="col-sm-9">
                                  <textarea disabled="disabled" class="form-control height">${announcements.announcementsStatusNotes }</textarea>
                                </div>
                              </div>
                              
                              <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                 <!--  <button type="submit" class="btn btn-info">Save</button> -->
                                </div>
                              </div>
                              
                        </div>
                    </div>
                </div>
                
                
                <!-- <div class="col-md-6">
                	<div class="panel panel-default">
                        <div class="panel-heading">Show Status</div>
                        <div class="panel-body">
                        
                             
                          <table class="table no-margn">
                              <thead>
                                <tr>
                                  <th>#S</th>
                                  <th>Date/Time</th>
                                   <th>Updated By</th>
                                  <th>Stasus </th>
                                   <th>Comments</th>
                                 
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                <td>1</td>
                                <td>2-2-2015 12:20</td>
                                <td>Amal</td>
                                  <td>Inactive</td>
                                  <td>	Lorem ipsum dolor sit amet 25-2-2015 22:30 pm</td>
                             
                                </tr>
                               
                              </tbody>
                            </table>    
                              
                         
                        </div>
                    </div>
                </div>
                 -->
                 
                    
                    
                </div>
              </div>
              
            </div>
           </div>
            
            
            </div>
            
      
            
            
        </div>
        <script type="text/javascript">
        $(document).ready(function(){
        	$("#dashbordli").removeClass("active");
			$("#organzationli").addClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
        });
        </script>
</body>
</html>