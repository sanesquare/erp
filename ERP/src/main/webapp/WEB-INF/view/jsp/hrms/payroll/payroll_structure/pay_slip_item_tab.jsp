<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Payslip Items <span class="pay-slp-err"></span>
			</div>
			<div class="panel-body" id="pay-slip-item-list-div">
				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered"
					id="pay-slip-items-table">
					<thead>
						<tr>
							<th width="5%">S#</th>
							<th width="20%">Title</th>
							<th width="10%">Type</th>
							<th width="5%">Taxable Allowance</th>
							<th width="20%">Calculation</th>
							<th width="10%">With Effect From</th>
							<th width="10%"></th>
						</tr>
					</thead>
					<tbody id="ps-div">
						<c:choose>
							<c:when test="${not empty paySlipItems}">
								<c:forEach items="${paySlipItems}" var="paySlipItem"
									varStatus="row">
									<tr>
										<td>${row.count}</td>
										<td><input type="text" name="title"
											value="${paySlipItem.title}" id="ps-kw-non-exp"
											class="form-control itemTitle${row.count}" /></td>
										<td><select class="form-control itemType${row.count}"
											name="type">
												<option value="Calculation"
													${paySlipItem.type == 'Calculation' ? 'selected' : ''}>Calculation</option>
												<option value="Fixed"
													${paySlipItem.type == 'Fixed' ? 'selected' : ''}>Fixed</option>
										</select></td>
										<td><select class="form-control itemTax${row.count}"
											name="taxAllowance">
												<option value="true"
													${paySlipItem.taxAllowance == true ? 'selected' : ''}>Yes</option>
												<option value="false"
													${paySlipItem.taxAllowance == false ? 'selected' : ''}>No</option>
										</select></td>
										<td><input type="text" value="${paySlipItem.calculation}"
											class="form-control typeahead itemSyntax${row.count}"
											name="calculation" id="ps-kw-exp" /></td>
										<td><input type="text" name="effectFrom"
											value="${paySlipItem.effectFrom}"
											class="form-control itemFrom${row.count}"
											data-date-format="DD/MM/YYYY" id="dt-ps" /></td>
										<td><input type="hidden" id="itemId${row.count}"
											name="paySlipItemId" value="${paySlipItem.paySlipItemId}" /><input
											type="button" value="Update"
											onclick="updatePaySlip(${row.count})" id="addButton1"
											class="btn btn-info btn-xs"> <c:if
												test="${fn:length(paySlipItems) eq row.count}">
												&nbsp;<input type="button" value="Add Row"
													onclick="addPSNewRow()" id="addButton1"
													class="btn btn-danger btn-xs">
											</c:if></td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td>1</td>
									<td><input type="text" name="title" id="ps-kw-non-exp"
										class="form-control itemTitle1" /></td>
									<td><select class="form-control itemType1" name="type">
											<option value="Calculation">Calculation</option>
											<option value="Fixed">Fixed</option>
									</select></td>
									<td><select class="form-control itemTax1"
										name="taxAllowance">
											<option value="true">Yes</option>
											<option value="false">No</option>
									</select></td>
									<td><input type="text"
										class="form-control typeahead itemSyntax1" name="calculation"
										id="ps-kw-exp" /></td>
									<td><input type="text" name="effectFrom"
										class="form-control itemFrom1" data-date-format="DD/MM/YYYY"
										id="dt-ps" /></td>
									<td><input type="hidden" id="itemId1" name="paySlipItemId"
										value="0" /><input type="button" value="Save"
										onclick="savePaySlipItem()" id="psButtonSave1"
										class="btn btn-info btn-xs"></td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Additional Payslip Items <span class="epay-slp-err"></span>
			</div>
			<div class="panel-body" id="extra-pay-slip-item-list-div">
				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered"
					id="extra-pay-slip-items-table">
					<thead>
						<tr>
							<th width="5%">S#</th>
							<th width="20%">Title</th>
							<th width="10%">Type</th>
							<th width="5%">Taxable Allowance</th>
							<th width="20%">Calculation</th>
							<th width="10%">With Effect From</th>
							<th width="10%"></th>
						</tr>
					</thead>
					<tbody id="extr-ps-div">
						<c:choose>
							<c:when test="${not empty extraPaySlipItems}">
								<c:forEach items="${extraPaySlipItems}" var="extraPaySlipItem"
									varStatus="row">
									<tr>
										<td>${row.count}</td>
										<td><input type="text" name="etitle"
											value="${extraPaySlipItem.title}"
											class="form-control itemeTitle${row.count}" /></td>
										<td><select class="form-control itemeType${row.count}"
											name="etype">
												<option value="Calculation"
													${extraPaySlipItem.type == 'Calculation' ? 'selected' : ''}>Calculation</option>
												<option value="Fixed"
													${extraPaySlipItem.type == 'Fixed' ? 'selected' : ''}>Fixed</option>
										</select></td>
										<td><select class="form-control itemeTax${row.count}"
											name="etaxAllowance">
												<option value="true"
													${extraPaySlipItem.taxAllowance == true ? 'selected' : ''}>Yes</option>
												<option value="false"
													${extraPaySlipItem.taxAllowance == false ? 'selected' : ''}>No</option>
										</select></td>
										<td><input type="text"
											value="${extraPaySlipItem.calculation}"
											class="form-control typeahead itemeSyntax${row.count}"
											name="ecalculation" id="ps-kw-exp" /></td>
										<td><input type="text" name="eeffectFrom"
											value="${extraPaySlipItem.effectFrom}"
											class="form-control itemeFrom${row.count}"
											data-date-format="DD/MM/YYYY" id="dt-ps" /></td>
										<td><input type="hidden" id="itemeId${row.count}"
											name="epaySlipItemId"
											value="${extraPaySlipItem.extraPaySlipItemId}" /><input
											type="button" value="Update"
											onclick="updateePaySlip(${row.count})" id="addButton1"
											class="btn btn-info btn-xs"> <c:if
												test="${fn:length(extraPaySlipItems) eq row.count}">
												&nbsp;<input type="button" value="Add Row"
													onclick="addePSNewRow()" id="addButton1"
													class="btn btn-danger btn-xs">
											</c:if></td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td>1</td>
									<td><input type="text" name="etitle"
										class="form-control itemeTitle1" /></td>
									<td><select class="form-control itemeType1" name="etype">
											<option value="Calculation">Calculation</option>
											<option value="Fixed">Fixed</option>
									</select></td>
									<td><select class="form-control itemeTax1"
										name="etaxAllowance">
											<option value="true">Yes</option>
											<option value="false">No</option>
									</select></td>
									<td><input type="text"
										class="form-control typeahead itemeSyntax1"
										name="ecalculation" id="ps-kw-exp" /></td>
									<td><input type="text" name="eeffectFrom"
										class="form-control itemeFrom1" data-date-format="DD/MM/YYYY"
										id="dt-ps" /></td>
									<td><input type="hidden" id="itemeId1"
										name="epaySlipItemId" value="0" /><input type="button"
										value="Save" onclick="saveePaySlipItem()" id="epsButtonSave1"
										class="btn btn-info btn-xs"></td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">Bonus</div>
			<div class="panel-body" id="bonus-date-list-div">
				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="30%">Year</th>
							<th width="30%">Month</th>
							<th width="30%">Day</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><select class="form-control" id="bonus-year-count">
									<c:forEach items="${yearList}" var="year">
										<option value="${year}"
											${bonuDate.year == year ? 'selected' : ''}>${year}</option>
									</c:forEach>
							</select></td>
							<td><select class="form-control" id="bonus-month-count">
									<c:forEach items="${monthList}" var="month">
										<option value="${month}"
											${bonuDate.month == month ? 'selected' : ''}>${month}</option>
									</c:forEach>
							</select></td>
							<td><select class="form-control" id="bonus-day-count">
									<c:forEach items="${dayList}" var="day">
										<option value="${day}"
											${bonuDate.day == day ? 'selected' : ''}>${day}</option>
									</c:forEach>
							</select></td>
					</tbody>
				</table>
				<div class="col-sm-6 col-xs-offset-6">
					<input type="hidden" name="bonusDateId" id="bonusDateId"
						value="${bonuDate.bonusDateId}" />
					<button type="button" onclick="saveBonusDate()"
						class="btn btn-info">Save</button>
				</div>
			</div>
			<div class="status-message"></div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">Provident Fund</div>
			<div class="panel-body" id="pf-syntax-list-div">
				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="50%">Employee Contribution</th>
							<th width="50%">Employer Contribution</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="text" name="employeeSyntax"
								class="form-control typeahead employeeSyntax"
								value="${profidentFundSyntax.employeeSyntax}" id="ps-kw-exp" /></td>
							<td><input type="text" name="employerSyntax"
								class="form-control typeahead employerSyntax"
								value="${profidentFundSyntax.employerSyntax}" id="ps-kw-exp" /></td>
						</tr>
					</tbody>
				</table>
				<div class="col-sm-6 col-xs-offset-6">
					<input type="hidden" name="pfSyntaxId" id="pfSyntaxId"
						value="${profidentFundSyntax.pfSyntaxId}" />
					<button type="button" onclick="saveProvidentFundSyntax()"
						class="btn btn-info">Save</button>
				</div>
			</div>
			<div class="pf-status-message"></div>
		</div>
	</div>






	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">ESI</div>
			<div class="panel-body" id="esi-syntax-list-div">

				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">
						ESI Eligibility Range</label>
					<div class="col-sm-4">
						<select class="form-control range" name="range">
							<option value="Below"
								${esiSyntax.range == 'Below' ? 'selected' : ''}>Below</option>
							<option value="Above"
								${esiSyntax.range == 'Above' ? 'selected' : ''}>Above</option>
						</select>
					</div>
					<div class="col-sm-5">
						<input type="text" name="amount" class="form-control amount"
							placeholder="Amount" value="${esiSyntax.amount}" />
					</div>
				</div>
				<br /> <br /> <br /> <br />
				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="50%">Employee Contribution</th>
							<th width="50%">Employer Contribution</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="text" name="employeeSyntax" id="ps-kw-exp"
								class="form-control typeahead employeeEsiSyntax"
								value="${esiSyntax.employeeSyntax}" /></td>
							<td><input type="text" name="employerSyntax" id="ps-kw-exp"
								class="form-control typeahead employerEsiSyntax"
								value="${esiSyntax.employerSyntax}" /></td>
					</tbody>
				</table>
				<div class="col-sm-6 col-xs-offset-6">
					<input type="hidden" name="esiSyntaxId" id="esiSyntaxId"
						value="${esiSyntax.esiSyntaxId}" />
					<button type="submit" onclick="saveESISyntax()"
						class="btn btn-info">Save</button>
				</div>
			</div>
			<div class="esi-status-message"></div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">Labour Welfare Fund</div>
			<div class="panel-body">
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">LWF</label>
					<div class="col-sm-9">
						<input type="text" value="${lwf.amount}" name="amount"
							id="lwfAmount" class="form-control" />
					</div>
				</div>
				<div class="col-sm-6 col-xs-offset-6">
					<input type="hidden" name="lwfId" id="lwfId" value="1">
					<button type="button" onclick="saveLWF()" class="btn btn-info">Save</button>
				</div>
				<div id="lwf-status-message"></div>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">Professional Tax</div>
			<div class="panel-body">
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Professional
						Tax</label>
					<div class="col-sm-9">
						<input type="text" name="amount" id="profTaxVal"
							value="${profersionalTax.amount}" class="form-control" />
					</div>
				</div>
				<div class="col-sm-6 col-xs-offset-6">
					<input type="hidden" name="profTaxId" id="profTaxId" value="1">
					<button type="button" onclick="saveProfTax()" class="btn btn-info">Save</button>
				</div>
				<div id="proft-status-message"></div>
			</div>
		</div>
	</div>
</div>

<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
<script src="<c:url value='/resources/assets/js/auto_complete.js' />"></script>
