<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>
				<spring:message code="meetings.title" />
			</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>



		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">
						<spring:message code="meetings.table_title" />
					</div>
					<div class="panel-body">
						<c:if test="${ !empty meetings}">
							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered" id="basic-datatable">
								<thead>
									<tr>
										<th width="20%"><spring:message
												code="meetings.table.title" /></th>
										<th width="20%"><spring:message
												code="meetings.table.start_date" /></th>
										<th width="20%"><spring:message
												code="meetings.table.end_date" /></th>
										<th width="20%"><spring:message
												code="meetings.table.venue" /></th>
										<c:if test="${ rolesMap.roles['Meetings'].update}">
											<th width="10%"><spring:message
													code="meetings.table.edit" /></th>
										</c:if>
										<c:if test="${ rolesMap.roles['Meetings'].delete}">
											<th width="10%"><spring:message
													code="meetings.table.delete" /></th>
										</c:if>

									</tr>
								</thead>
								<tbody>
									<c:forEach items="${meetings }" var="m">
										<tr>
											<c:choose>
												<c:when test="${ rolesMap.roles['Meetings'].view}">
													<td><a href="viewMeeting.do?id=${m.meetingId }">
															${m.meetingsTitle }</a></td>
												</c:when>
												<c:otherwise>
													<td>${m.meetingsTitle }</td>
												</c:otherwise>
											</c:choose>

											<td>${m.meetingStartDate }</td>
											<td>${m.meetingEndDate }</td>
											<td>${m.venue }</td>
											<c:if test="${ rolesMap.roles['Meetings'].update}">
												<td><a href="editMeeting.do?id=${m.meetingId }"><i
														class="fa fa-edit sm"></i> </a></td>
											</c:if>
											<c:if test="${ rolesMap.roles['Meetings'].delete}">
												<td><a href="deleteMeeting.do?id=${m.meetingId }"><i
														class="fa fa-close ss"></i></a></</td>
											</c:if>
										</tr>
									</c:forEach>

								</tbody>
							</table>
						</c:if>
						<a href="add_meeting.do">
						<c:if test="${ rolesMap.roles['Meetings'].add}">
							<button type="button" class="btn btn-primary">
								<spring:message code="meetings.btn.add_new_meeting" />
							</button>
							</c:if>
						</a>

					</div>
				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->



	<!-- Content Block Ends Here (right box)-->


	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>


	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		$("#dashbordli").removeClass("active");
		$("#organzationli").addClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").removeClass("active");
		$("#reportsli").removeClass("active");
	});
	</script>
</body>
</html>