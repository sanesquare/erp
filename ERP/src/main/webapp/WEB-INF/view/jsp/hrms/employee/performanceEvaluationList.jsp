<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width="70%">Title</th>
			<th width="15%">Edit</th>
			<th width="15%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${evaluationList}" var="evaluation">
			<tr>
				<td><a
					href="ViewPerformanceEvaluation.do?otp=${evaluation.evaluationId}">${evaluation.title}</a></td>
				<td><a
					href="UpdatePerformanceEvaluation.do?otpId=${evaluation.evaluationId}"><i
						class="fa fa-edit sm"></i> </a></td>
				<td><a href="#"
					onclick="deleteEvaluation(${evaluation.evaluationId})"><i
						class="fa fa-close ss"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<a href="CreatePerformanceEvaluation.do">
	<button type="button" class="btn btn-primary">Add Performance
		Evaluation</button>
</a>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>