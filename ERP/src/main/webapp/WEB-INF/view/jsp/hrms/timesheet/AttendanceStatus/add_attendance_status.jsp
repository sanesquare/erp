<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Attendance Status</h1>
		</div>
		<c:if test="${! showFilter }">
			<div class="row">
				<div class="col-md-12">
					<a href="addAttenStatus" class="btn btn-info" style="float: right;"><i
						class="fa fa-arrow-left"></i></a>
				</div>
			</div>
		</c:if>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>

				</ul>
				<c:choose>
					<c:when test="${showFilter }">
						<form:form commandName="attendanceStatusVo" method="POST"
							action="addAttenStatus.do" id="attenStstForm">
							<div class="tab-content">
								<div role="tabpanel"
									class="panel panel-default tab-pane tabs-up active" id="all">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-12">
													<div class="panel panel-default">
														<div class="panel-heading">Employee Attendance
															Status Information</div>
														<div class="panel-body">

															<div class="form-group">
																<label for="inputEmail3"
																	class="col-sm-3 col-xs-12 control-label">
																	Branch <span class="stars">*</span>
																</label>
																<div class="col-sm-9">
																	<form:select cssClass="form-control " path="branchId"
																		id="drop_one">
																		<form:options items="${branches }"
																			itemValue="branchId" itemLabel="branchName" />
																	</form:select>
																</div>
															</div>
															<div class="form-group">
																<label for="inputEmail3"
																	class="col-sm-3 col-xs-12 control-label">Department</label>
																<div class="col-sm-9">
																	<form:select  required="required"
																		class="form-control chosen-select"
																		path="departmentIds" id="drop_two">
																		<form:options items="${departments }" itemValue="id"
																			itemLabel="name" />

																	</form:select>
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-offset-3 col-sm-9">
																	<button type="submit" class="btn btn-info">Filter</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form:form>
					</c:when>
					<c:otherwise>
						<form:form commandName="attendanceStatusVo" method="POST"
							action="saveAttendanceStatus.do" id="attenStstForm">
							<div class="tab-content">
								<div role="tabpanel"
									class="panel panel-default tab-pane tabs-up active" id="all">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-12">
													<div class="panel panel-default">
														<div class="panel-heading">Employee Attendance
															Status Information</div>
														<div class="panel-body">
															<div class="form-group">
																<label class="col-sm-3 control-label">Attendance
																	Date <span class="stars">*</span>
																</label>
																<div class="col-sm-9">
																	<div class="input-group date attenStsDate"
																		id="datepicker">
																		<form:input type="text" cssClass="form-control"
																			style="background-color: #ffffff !important;"
																			readonly="true" path="date" id="add_attStsDate"
																			data-date-format="DD/MM/YYYY" />
																		<span class="input-group-addon"><span
																			class="glyphicon-calendar glyphicon"></span> </span>
																	</div>
																</div>
															</div>
															<table cellpadding="0" cellspacing="0" border="0"
																class="table table-striped table-bordered" id="">
																<thead>
																	<tr>
																		<th width="30%">Employee</th>
																		<th width="30%">Status</th>
																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${employees }" var="emp"
																		varStatus="s">
																		<tr>
																			<td>${emp.name}&nbsp;(&nbsp;${emp.employeeCode }&nbsp;)
																				<input type="hidden" value="${emp.employeeCode }"
																				name="attendanceEmployeeVos[${s.index }].employeeCode">
																			</td>
																			<td><select class="form-control chosen-select"
																				name="attendanceEmployeeVos[${s.index }].status">
																					<option value="Present">Present</option>
																					<option value="Absent">Absent</option>
																					<option value="Half Day">Half Day</option>
																					<c:forEach items="${emp.leaveVos }" var="leave">
																						<option value="${leave.leaveId }">${leave.leaveTitle }</option>
																					</c:forEach>
																			</select></td>
																		</tr>
																	</c:forEach>
																</tbody>
															</table>
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-sm-3 control-label">Notes </label>
																<div class="col-sm-9">
																	<form:textarea path="notes" id="add_attSts_notes"
																		cssClass="form-control height" />
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-offset-3 col-sm-9">
																	<button type="submit" class="btn btn-info">Save</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form:form>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->

	<!-- Content Block Ends Here (right box)-->

	<!-- JQuery v1.9.1 -->
	<script
		src='<c:url value="/resources/assets/js/jquery/jquery-1.9.1.min.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/plugins/underscore/underscore-min.js" />'></script>

	<!-- Bootstrap -->
	<script
		src='<c:url value="/resources/assets/js/bootstrap/bootstrap.min.js" />'></script>
	<!-- TagsInput -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" />'></script>

	<!-- Chosen -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js" />'></script>

	<!-- moment -->
	<script src='<c:url value="/resources/assets/js/moment/moment.js" />'></script>
	<!-- DateTime Picker -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js" />'></script>

	<!-- Custom JQuery -->
	<script src='<c:url value="/resources/assets/js/app/custom.js" />'></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src='<c:url value="/resources/assets/js/hrms_validator.js" />'></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script
		src='<c:url value="/resources/assets/js/jquery.simplePopup.js" />'></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#employeeSl').bind('click', function() {
				getAllBranches();
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").addClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>