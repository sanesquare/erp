<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Adjustments</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="adjustments.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>




		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>


				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Adjustment Information</div>
											<div class="panel-body">


												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3 control-label">Employee
														Name</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-9 control-label ft">${adjustmentVo.employee }</label>
													</div>


												</div>




												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Title </label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${adjustmentVo.title }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3 control-label">Adjustment
														Type</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${adjustmentVo.type }</label>
													</div>


												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Amount </label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${adjustmentVo.amount }</label>
													</div>
												</div>





												<div class="form-group">
													<label class="col-sm-3 control-label"> Date</label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-3 control-label ft">${adjustmentVo.date }</label>
													</div>

												</div>




											</div>
										</div>
									</div>


								</div>

								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Adjustment Description</div>
											<div class="panel-body">
												<div class="form-group">
													<div class="col-md-12">
														<label for="inputPassword3"
															class="col-sm-12 control-label ft">${adjustmentVo.description }</label>
													</div>
												</div>


											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-8 control-label ft">${adjustmentVo.notes }</label>
													</div>
												</div>


												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${adjustmentVo.createdBy }</label>
													</div>
												</div>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${adjustmentVo.createdOn }</label>
													</div>
												</div>




											</div>
										</div>
									</div>

								</div>
							</div>



						</div>
					</div>




				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->

	<!-- Content Block Ends Here (right box)-->



	<!-- JQuery v1.9.1 -->
	<script
		src='<c:url value="/resources/assets/js/jquery/jquery-1.9.1.min.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/plugins/underscore/underscore-min.js" />'></script>

	<!-- Bootstrap -->
	<script
		src='<c:url value="/resources/assets/js/bootstrap/bootstrap.min.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/globalize/globalize.min.js" />'></script>

	<!-- NanoScroll -->
	<script
		src='<c:url value="/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js" />'></script>
	<!-- TypeaHead -->
	<script
		src='<c:url value="/resources/assets/js/plugins/typehead/typeahead.bundle.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js" />'></script>

	<!-- InputMask -->
	<script
		src='<c:url value="/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js" />'></script>

	<!-- TagsInput -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" />'></script>

	<!-- Chosen -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js" />'></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<!-- moment -->
	<script src='<c:url value="/resources/assets/js/moment/moment.js" />'></script>
	<!-- DateTime Picker -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js" />'></script>

	<!-- Wysihtml5 -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js" />'></script>

	<!-- Custom JQuery -->
	<script src='<c:url value="/resources/assets/js/app/custom.js" />'></script>

	<script
		src='<c:url value="/resources/assets/js/jquery.simplePopup.js" />'></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>


</body>
</html>