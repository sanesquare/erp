<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width='10%'>S#</th>
			<th width='20%'>Reminder Title</th>
			<th width='25%'>Reminder Date</th>
			<th width='25%'>Status</th>
			<th width='10%'>Edit</th>
			<th width='10%'>Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${reminderList}" var="reminder" varStatus="status">
			<tr>
				<td>${status.count}</td>
				<td>${reminder.reminderTitle}</td>
				<td>${reminder.reminderDate}</td>
				<td>${reminder.reminderStatus}</td>
				<td><a href="#"><i class="fa fa-edit sm"></i> </a></td>
				<td><a href="#"
					onclick="reminderDelete(${reminder.reminderId})"><i
						class="fa fa-close ss"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<div id="reminderStatusList" style="display: none;">
	<select class="form-control" name="reminderStatus">
		<c:forEach items="${reminderStatusList}" var="remStatus">
			<option value="${remStatus.status}">${remStatus.status}</option>
		</c:forEach>
	</select>
</div>

<button type="button" class="btn btn-primary flr mtft" id="addrm">Add
	Reminder</button>

<script>
		$(document).ready(function() {
			$("#addrm").click(function() {
				$("#add_rm").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
			
			$("#reminder-status-warpper").html($("#reminderStatusList").html());
		});
</script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>