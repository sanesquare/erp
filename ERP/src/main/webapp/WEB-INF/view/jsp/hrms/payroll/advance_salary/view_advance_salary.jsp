<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Advance Salary</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Advance Salary Information</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3  control-label">Employee
														Name</label>

													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-9 control-label ft">${advanceSalaryVo.employee}</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label"> Forward
														Application To</label>
													<div class="col-sm-9">
														<label for="inputPassword3"
															class="col-sm-9 control-label ft"> <c:forEach
																items="${advanceSalaryVo.forwadersName}"
																var="forwaderName">
																<c:out value="${forwaderName}" />,&nbsp;
															</c:forEach>
														</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Title </label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-9 control-label ft">${advanceSalaryVo.advanceSalaryTitle}</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Amount </label>
													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-9 control-label ft">${advanceSalaryVo.advanceAmount}</label>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label"> Date</label>

													<div class="col-sm-9">
														<label for="inputEmail3" class="col-sm-9 control-label ft">${advanceSalaryVo.requestedDate}</label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Advance Salary Description</div>
											<div class="panel-body">
												<div class="form-group">
													<div class="col-md-12">
														<label for="inputPassword3"
															class="col-sm-12 control-label ft">${advanceSalaryVo.description}</label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Status</div>
											<div class="panel-body">
												<c:choose>
													<c:when test="${not empty advanceSalaryVo.status}">
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Status </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-3 control-label ft">${advanceSalaryVo.status}</label>
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Description </label>
															<div class="col-sm-9">
																<label for="inputPassword3"
																	class="col-sm-8 control-label ft">${advanceSalaryVo.statusDescription}</label>
															</div>
														</div>
													</c:when>
													<c:otherwise>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Status </label>
															<div class="col-sm-9">
																<label for="inputEmail3"
																	class="col-sm-3 control-label ft">Pending</label>
															</div>
														</div>
													</c:otherwise>
												</c:choose>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Status</div>
											<form:form action="AdvanceSalaryStatusUpdate.do" method="post"
												commandName="advanceSalaryVo">
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Status
														</label>
														<div class="col-sm-9">
															<form:select path="status"
																class="form-control chosen-select">
																<form:options items="${advanceSalaryStatusList}"
																	itemLabel="status" itemValue="status" />
															</form:select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Description
														</label>
														<div class="col-sm-9">
															<form:textarea path="statusDescription"
																class="form-control height" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label"> Forward
															Application To</label>
														<div class="col-sm-9 col-xs-10">
															<form:select path="forwaders" multiple="true"
																class="form-control chosen-select forwaders"
																data-placeholder="Select Superior">
																<form:options items="${superiorList}" itemValue="value"
																	itemLabel="label" />
															</form:select>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<form:hidden path="advanceSalaryId" />
															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Additional Information</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Notes
												</label>
												<div class="col-sm-9">
													<label for="inputPassword3"
														class="col-sm-8 control-label ft">${advanceSalaryVo.notes}</label>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Record
													Added By </label>
												<div class="col-sm-9">
													<label for="inputPassword3" class="col-sm-9 control-label">${advanceSalaryVo.recordedBy}</label>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Record
													Added on </label>
												<div class="col-sm-9">
													<label for="inputPassword3" class="col-sm-9 control-label">${advanceSalaryVo.recordedOn}
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</body>
</html>
