<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width="30%">Employee Name</th>
			<th width="30%">Employee Share</th>
			<th width="30%">Organisation Share</th>
			<c:if test="${ rolesMap.roles['ESI'].update}">
				<th width="10%">Edit</th>
			</c:if>
			<c:if test="${ rolesMap.roles['ESI'].delete}">
				<th width="10%">Delete</th>
			</c:if>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${esiList}" var="esi">
			<tr>
				<c:choose>
					<c:when test="${ rolesMap.roles['ESI'].view}">
						<td><c:choose>
								<c:when test="${esi.recordedBy==thisUser}">
									<i class="fa fa-arrow-up up-arrow"></i>
								</c:when>
								<c:otherwise>
									<i class="fa fa-arrow-down down-arrow"></i>
								</c:otherwise>
							</c:choose><a href="ViewEsi.do?otp=${esi.esiId}">${esi.employee}</a></td>
						<td><a href="ViewEsi.do?otp=${esi.esiId}">${esi.employeeShare}</a></td>
						<td><a href="ViewEsi.do?otp=${esi.esiId}">${esi.organizationShare}</a></td>
					</c:when>
					<c:otherwise>

						<td><c:choose>
								<c:when test="${esi.recordedBy==thisUser}">
									<i class="fa fa-arrow-up up-arrow"></i>
								</c:when>
								<c:otherwise>
									<i class="fa fa-arrow-down down-arrow"></i>
								</c:otherwise>
							</c:choose>${esi.employee}</td>
						<td>${esi.employeeShare}</td>
						<td>${esi.organizationShare}</td>
					</c:otherwise>
				</c:choose>

				<c:if test="${ rolesMap.roles['ESI'].update}">
					<td><a href="EditEsi.do?otp=${esi.esiId}"><i
							class="fa fa-edit sm"></i></a></td>
				</c:if>
				<c:if test="${ rolesMap.roles['ESI'].delete}">
					<td><a href="#" onclick="esiDelConfirm(${esi.esiId})"><i
							class="fa fa-close ss"></i></a></td>
				</c:if>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${ rolesMap.roles['ESI'].add}">
	<a href="AddEsi.do">
		<button type="button" class="btn btn-primary">Add New ESI</button>
	</a>
</c:if>

<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

