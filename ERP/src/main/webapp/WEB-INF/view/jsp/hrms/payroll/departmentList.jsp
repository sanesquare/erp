<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<a href="#" onclick="getAllBranches()">Back to branch</a>

<ul class="select_whole">
	<c:forEach items="${departmentList}" var="department">
		<a href="#"
			onclick="getAllBranchDepartmentEmployees(${branch}, ${department.id})"><li>${department.name}</li></a>
	</c:forEach>
</ul>

<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
