\<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />

<link rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />

<link href="<c:url value='/resources/assets/css/RegalCalendar.css'/>"
	rel="stylesheet" type="text/css" />


</head>
<body>
<body style="background: #FFFFFF !important;">
	<h2 class="role_hd">Bank Statement</h2>
	<div class="col-xs-12">
		<form:form action="exportBankStatement" method="POST"
			modelAttribute="statement">

			<table class="table table-striped no-margn line">
				<thead>
					<tr>
						<td class="hd_tb"><span class="WhiteHeading">S#</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Employee
								Code</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Name</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Account</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Amount</span></td>
						<td class="hd_tb"><span class="WhiteHeading">Branch</span></td>

					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${! empty statement.details }">
							<c:forEach items="${statement.details }" var="det"
								varStatus="status">
								<tr>
									<td>${det.srNo }<form:hidden
											path="details[${status.index }].srNo" /></td>
									<td>${det.employeeCode } sample<form:hidden
											path="details[${status.index }].employeeCode" /></td>
									<td>${det.employee }<form:hidden
											path="details[${status.index }].employee" /></td>
									<td>${det.account }<form:hidden
											path="details[${status.index }].account" /></td>
									<td>${det.amount }<form:hidden
											path="details[${status.index }].amount" /></td>
									<td>${det.branch }<form:hidden
											path="details[${status.index }].branch" /></td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<td>No Records To Show...</td>
							</tr>
						</c:otherwise>
					</c:choose>
					<tr>Sample</tr>
				</tbody>
			</table>



			<c:if test="${! empty statement.details }">
				<div class="form-group" style="margin-top: 25px;">
					<div class="col-sm-offset-4 col-sm-6">
						<button type="submit" class="btn btn-info" name="type" value="pdf">
							Export to PDF &nbsp;<i class="fa fa-file-pdf-o"></i>
						</button>
						<button type="submit" class="btn btn-info" name="type" value="xls">
							Export to XLS &nbsp;<i class="fa fa-file-excel-o"></i>
						</button>
					</div>
				</div>
			</c:if>
		</form:form>
	</div>

	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>

	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js'/>"></script>
	<script
		src="<c:url value='/resources/assets/js/easyResponsiveTabs.js'/>"></script>
	<!-- New  -->
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
</body>


</html>