<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="/resources/assets/css/bootstrap/bootstrap.css" />

<!-- Calendar Styling  -->
<link rel="stylesheet"
	href="/resources/assets/css/plugins/calendar/calendar.css" />

<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet" href="/resources/assets/css/app/app.v1.css" />

<link href="/resources/assets/css/RegalCalendar.css" rel="stylesheet"
	type="text/css" />
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Announcements</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>



		<div class="row">


			<div class="col-md-12">

				<div class="panel panel-default">
					<div class="panel-heading">Announcements</div>
					<div class="panel-body">

						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered" id="basic-datatable">
							<thead>
								<tr>
									<th width="28%">Title</th>
									<th width="28%">Announcement Start Date</th>
									<th width="24%">Announcement End Date</th>
									<c:if test="${ rolesMap.roles['Announcements'].update}">
										<th width="10%">Edit</th>
									</c:if>
									<c:if test="${ rolesMap.roles['Announcements'].delete}">
										<th width="10%">Delete</th>
									</c:if>

								</tr>
							</thead>
							<tbody>
								<c:forEach items="${announcements}" var="announcements">
									<tr>
										<c:choose>
											<c:when test="${ rolesMap.roles['Announcements'].view}">
												<td><a
													href="viewAnnouncements.do?announcementsId=${announcements.announcementsId }">${announcements.announcementsTitle }</a></td>
											</c:when>
											<c:otherwise>
												<td>${announcements.announcementsTitle }</td>
											</c:otherwise>
										</c:choose>

										<td>${announcements.startDate}</td>
										<td>${announcements.endDate}</td>
										<c:if test="${ rolesMap.roles['Announcements'].update}">
											<td><a
												href="edit_announcement.do?announcementsId=${announcements.announcementsId }"><i
													class="fa fa-edit sm"></i> </a></td>
										</c:if>
										<c:if test="${ rolesMap.roles['Announcements'].delete}">
											<td><a class="delete_announcement"
												onclick="confirmDelete(${announcements.announcementsId })"><i
													class="fa fa-close ss"></i> </a></td>
										</c:if>
									</tr>
								</c:forEach>


							</tbody>
						</table>
						<c:if test="${ rolesMap.roles['Announcements'].add}">
							<a href="add_announcements.do">
								<button type="button" class="btn btn-primary">Add New
									Announcement</button>
							</a>
						</c:if>

					</div>
				</div>
			</div>


		</div>




	</div>
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>


	<!-- Data Table -->
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>


	<!-- Custom JQuery -->
	<script src="assets/js/app/custom.js" type="text/javascript"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script type="text/javascript">
    function confirmDelete(id){
  var result=confirm("Confirm delete?");
   if(result){
   $(".delete_announcement").attr("href","delete_announcement.do?announcementsId="+id);
  } else{
   $(".delete_announcement").attr("href","#");
  } 
 }
    $(document).ready(function(){
    	$("#dashbordli").removeClass("active");
		$("#organzationli").addClass("active");
		$("#recruitmentli").removeClass("active");
		$("#employeeli").removeClass("active");
		$("#timesheetli").removeClass("active");
		$("#payrollli").removeClass("active");
		$("#reportsli").removeClass("active");
    });
    </script>

</body>
</html>