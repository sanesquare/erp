<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table class="table no-margn">
	<tbody>
		<c:choose>
			<c:when test="${not empty approveEmployees}">
				<thead>
					<tr>
						<th>S#</th>
						<th>Employee Name</th>
						<th>Username</th>
						<th>Delete</th>
					</tr>
				</thead>
				<c:forEach items="${approveEmployees}" var="admin" varStatus="status">
					<tr>
						<td>${status.count}</td>
						<td>${admin.employeeName}</td>
						<td>${admin.userName}</td>
						<td><a href="#"
							onclick="deleteAutoApprove(${admin.employeeId})"><i
								class="fa fa-close ss"></i></a></td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<span>No Employees to List </span>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
<form:form action="updateAutoApproval" method="post"
	commandName="employeeVo">
	<div class="col-md-12" id="add_auto_approval">
		<div class="panel panel-default">
			<div class="panel-heading">Add Auto Approval Employee </div>
			<div class="panel-body">
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Employee
					</label>
					<div class="col-sm-9">
						<select class="form-control employeeId" name="employeeId">
							<option value="">--Select--</option>
							<c:forEach items="${employeeList}" var="employee">
								<option value="${employee.employeeId}">${employee.name}&nbsp;(&nbsp;${employee.employeeCode }&nbsp;)</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="submit" class="btn btn-info" id="add_auto">Add
				Auto Approval Employee</button>
		</div>
	</div>
</form:form>
<script>
	$(document).ready(function() {
		$("#add_auto").click(function() {
			$("#add_auto_approval").show();
			if ($(".employeeId").val() == "")
				return false;
			else
				return true;
		});
	});
</script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>