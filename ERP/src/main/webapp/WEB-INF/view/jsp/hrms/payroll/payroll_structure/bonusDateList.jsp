<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="30%">Year</th>
			<th width="30%">Month</th>
			<th width="30%">Day</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><select class="form-control" id="bonus-year-count">
					<c:forEach items="${yearList}" var="year">
						<option value="${year}" ${bonuDate.year == year ? 'selected' : ''}>${year}</option>
					</c:forEach>
			</select></td>
			<td><select class="form-control" id="bonus-month-count">
					<c:forEach items="${monthList}" var="month">
						<option value="${month}"
							${bonuDate.month == month ? 'selected' : ''}>${month}</option>
					</c:forEach>
			</select></td>
			<td><select class="form-control" id="bonus-day-count">
					<c:forEach items="${dayList}" var="day">
						<option value="${day}" ${bonuDate.day == day ? 'selected' : ''}>${day}</option>
					</c:forEach>
			</select></td>
	</tbody>
</table>
<div class="col-sm-6 col-xs-offset-6">
	<input type="hidden" name="bonusDateId" id="bonusDateId"
		value="${bonuDate.bonusDateId}" />
	<button type="button" onclick="saveBonusDate()" class="btn btn-info">Save</button>
</div>

<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
