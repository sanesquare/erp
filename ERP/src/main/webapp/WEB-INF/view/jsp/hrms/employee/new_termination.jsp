<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
#sms {
	display: none;
}
</style>
</head>
<body>
	<div id="fade"></div>
	<div id="modal">
		<img id="loader" src="resources/images/loading.gif" />
	</div>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Termination</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="Termination.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab" id="terminate-doc-tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form action="NewTermination.do" method="post"
								commandName="terminationVo" id="terminationForm">
								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Termination Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Employee
															Terminated <span class="stars">*</span>
														</label>
														<div class="col-sm-9 col-xs-10 br-de-emp-list">
															<%-- <form:input path="employee" class="form-control"
																placeholder="Search Employee" id="employeeSl" /> --%>
															<form:select path="employee" id="employeeSl"
																class="form-control chosen-select"
																data-placeholder="Select Subordinate">
																<form:option value=""></form:option>
																<form:options items="${subordinates}" itemValue="value"
																	itemLabel="label" />
															</form:select>
															<form:hidden path="employeeCode" id="employeeCode" />
														</div>
														<!-- <div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div> -->
													</div>
													<div id="pop2" class="simplePopup empl-popup-gen"></div>
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label"> Forward
															Application To<span class="stars">*</span>
														</label>
														<div class="col-sm-9 col-xs-10 emp-pure-supr-list-div">
															<form:select path="forwaders" multiple="true"
																id="forwaders"
																class="form-control chosen-select forwaders"
																data-placeholder="Select Superior">
																<form:options items="${superiorList}" itemValue="value"
																	itemLabel="label" />
															</form:select>
														</div>
													</div>
													<%-- <div id="pop1" class="simplePopup">
														<div class="col-md-12">
															<span class="pop_hd_wht">Select Employee</span>
															<div class="form-group">
																<div class="col-md-12  pop_hd" style="color: #000">
																	<div class="col-md-6">
																		<select class="form-control chosen-select"
																			id="chosen-employee"
																			data-placeholder="Select Subordinates">
																			<option></option>
																			<c:forEach items="${subordinates}" var="pe">
																				<option value="${pe.value}">${pe.label}</option>
																			</c:forEach>
																		</select>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-12 branch-lists"></div>
																</div>
															</div>
														</div>
													</div> --%>
													<div class="form-group">
														<label class="col-sm-3 control-label">Termination
															Date <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<div class="input-group date termination-date"
																id="datepicker">
																<form:input path="termiantedDate" name="termiantedDate"
																	class="form-control" data-date-format="DD/MM/YYYY"
																	readonly="true"
																	style="background-color: #ffffff !important;" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Termination Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea path="description"
																class="wysihtml form-control"
																placeholder="Enter text ..." style="height: 150px" />
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Status</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Status
														</label>
														<div class="col-sm-9">
															<form:select class="form-control chosen-select"
																id="statuss" path="terminationStatus"
																items="${statusList}" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Description
														</label>
														<div class="col-sm-9">
															<form:textarea path="terminationStatusDescription"
																class="form-control height" />
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12" id="sms">
											<div class="panel panel-default">
												<div class="panel-heading">SMS Integration</div>
												<div class="panel-body" style="overflow: visible;">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Branch
														</label>
														<div class="col-sm-9">
															<select class="form-control chosen-select" multiple
																onchange="getEmployeeees()" id="drop_one"
																data-placeholder="Select Branch">
																<c:forEach items="${branches }" var="b">
																	<option value="${b.branchId }">${b.branchName }</option>
																</c:forEach>
															</select>

														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Department
														</label>
														<div class="col-sm-9">
															<select class="form-control chosen-select" multiple
																id="drop_two" onchange="getEmployeeees()"
																data-placeholder="Select Department">
																<c:forEach items="${departments }" var="b">
																	<option value="${b.id }">${b.name }</option>
																</c:forEach>
															</select>

														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Employee
														</label>
														<div class="col-sm-9">
															<select class="form-control chosen-select" multiple name="smsEmployees"
																id="term_sms_emp" data-placeholder="Select Employee">
																<c:forEach items="${employees }" var="e">
																<option value="${e.employeeId }">${e.name } ( ${e.employeeCode } )</option>
																</c:forEach>
															</select>

														</div>
													</div>


												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" class="form-control height" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${terminationVo.recordedBy}</label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${created_on}</label>
														</div>
													</div>
													<div class="form-group">
														<form:hidden path="terminationId" id="terminationId" />
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">${buttonLabel}</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>

					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Upload Documents</div>
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Upload
												Documents </label>
											<div class="col-sm-9">
												<input type="file" name="terminationDocs"
													id="employee-termination-docs" class="btn btn-info"
													multiple />

												<c:if test="${ rolesMap.roles['Termination Documents'].add}">
													<button type="button" onclick="uploadTerminationDocument()"
														class="btn btn-info empl-termi-doc-btn">Upload</button>
												</c:if>
												<span class="empl-termi-doc-status"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body" id="emp_ter_doc_lst"></div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Simple Map</div>
									<div class="panel-body">
										<div id="simpleMap" style="height: 500px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script
		src="<c:url value='/resources/js/progress/jquery.uploadfile.min.js' />"></script>
	<script src="<c:url value='/resources/js/progress/file-upload.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>


	<script>
		$(document).ready(function() {
			/* $('#employeeSl').bind('click', function() {
				getAllBranches();
				$('#pop1').simplePopup();
			}); */

			/* $('.show2').click(function() {
				if ($("#employeeCode").val() != "") {
					fillEmployeePopup($("#employeeCode").val());
					$('#pop2').simplePopup();
				}
			}); */
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
		$("#employeeSl").change(function() {
			getMySuperiors($("#employeeSl").val());
		});

		$("#statuss").on("change", function() {
			if ($("#statuss").val() == 'Approved') {
				$("#sms").css("display", "block");
			} else {
				$("#sms").css("display", "none");
			}
		});
	</script>
	<!-- <script>
		$('#js-smart-tabs').smartTabs();
		$('#js-smart-tabs--tabs').smartTabs({
			layout : 'tabs'
		});
		$('#js-smart-tabs--accordion').smartTabs({
			layout : 'accordion'
		});
	</script>
	<script>
		$(document).ready(function() {

			$("#admin_ss").click(function() {
				$("#add_administartor").show();
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addfield").click(function() {
				$("#add_custom_feild").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>

	<script>
		$(document).ready(function() {
			$("#addrm").click(function() {
				$("#add_rm").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script> -->
</body>
</html>
