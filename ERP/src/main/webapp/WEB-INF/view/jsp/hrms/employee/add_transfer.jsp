<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>

	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Transfers</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="transfer.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form action="saveTransfer.do" commandName="transferVo"
								method="POST" id="transfer_add_form">
								<form:hidden path="transferId" id="transfer_id" />
								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Transfer Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Employee
															Name <span class="stars">*</span></label>
														<div class="col-sm-8 col-xs-10 br-de-emp-list">
															<form:input path="employee" class="form-control"
																placeholder="Search Employee" id="employeeSl" />
															<form:hidden path="employeeCode" id="employeeCode" />
															<div id="emp_cntrt_emp_err" class="alert_box">
																<div class="triangle-up"></div>
																Please select employee..
															</div>
														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div>
													</div>

													<div id="pop1" class="simplePopup">
													</div>

													<div id="pop2" class="simplePopup empl-popup-gen"></div>
													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label"> Forward
															Application To<span class="stars">*</span></label>
														<div class="col-sm-8 col-xs-10">
															<form:select path="superiorCode" id="transfer_superior"
																class="form-control">
																<option value="">--Select--</option>
																<form:options items="${superiorList }" itemLabel="label"
																	itemValue="value" />
															</form:select>
															<form:hidden path="superiorCodeAjax"
																id="transfer_sprior_hdn" />
														</div>
														<!-- <div class="col-sm-1 col-xs-1">
															<a href="#" class="show1"><i
																class="fa fa-plus-square-o pop_icon"></i></a>
														</div> -->
													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Transfer
															Date <span class="stars">*</span></label>
														<div class="col-sm-9">
															<div class="input-group date leave-to-date"
																id="datepicker">
																<form:input path="date" class="form-control"
																	readonly="true"
																	cssStyle="background-color:#fff !important"
																	data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Transfer To
															(Branch)<span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select path="branchId" class="form-control">
																<option value="">--Select Branch--</option>
																<form:options items="${branches }"
																	itemLabel="branchName" itemValue="branchId" />
															</form:select>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Transfer To
															(Department)<span class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select path="departmentId" class="form-control">
																<option value="">--Select Department--</option>
																<form:options items="${departments }" itemLabel="name"
																	itemValue="id" />
															</form:select>
														</div>
													</div>
												</div>
											</div>
										</div>




										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Transfer Description</div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-md-12">
															<form:textarea class=" form-control" path="description"
																placeholder="Enter text ..."
																style="height: 150px;resize:none" />

														</div>
													</div>
												</div>
											</div>
										</div>


									</div>
									<div class="col-md-6">





										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" class="form-control height" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added By </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${transferVo.createdBy }
															</label>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Record
															Added on </label>
														<div class="col-sm-9">
															<label for="inputPassword3"
																class="col-sm-9 control-label">${transferVo.createdOn }
															</label>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>


					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">
							<c:if test="${ rolesMap.roles['Transfer Documents'].add}">
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Upload Documents</div>
										<div class="panel-body">
											<form:form action="uploadTransferDocument.do" method="POST"
												commandName="transferVo" enctype="multipart/form-data">
												<form:hidden path="transferId" />
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Upload
														Documents </label>
													<div class="col-sm-9">
														<input type="file" mutiple name="files" id="transfer-docs"
															class="btn btn-info" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" id="trnsfer_doc_btn"
															class="btn btn-info">Save</button>
													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</c:if>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">
										<c:if test="${! empty transferVo.documentsVos }">
											<table class="table no-margn">
												<thead>
													<tr>
														<th>#</th>
														<th>Document Name</th>
														<c:if test="${ rolesMap.roles['Transfer Documents'].view}">
															<th>view</th>
														</c:if>
														<c:if
															test="${ rolesMap.roles['Transfer Documents'].delete}">
															<th>Delete</th>
														</c:if>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${transferVo.documentsVos }" var="d"
														varStatus="s">
														<tr>
															<td>${s.count }</td>
															<td><i class="fa fa-file-o"></i>${d.fileName }</td>
															<c:if
																test="${ rolesMap.roles['Transfer Documents'].view}">
																<td><a
																	href="downloadTransferDocument.do?url=${d.url }"><i
																		class="fa fa-file-text-o sm"></i></a></td>
															</c:if>
															<c:if
																test="${ rolesMap.roles['Transfer Documents'].delete}">
																<td><a href="deleteTransferDocument.do?url=${d.url }"><i
																		class="fa fa-close ss"></i></a></td>
															</c:if>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</c:if>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Simple Map</div>
									<div class="panel-body">
										<div id="simpleMap" style="height: 500px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->


	<!-- Content Block Ends Here (right box)-->

	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			getEmployeeDetails();

			if ($("#transfer-docs").val() == '') {
				$("#trnsfer_doc_btn").attr('disabled', 'true');
			}

			$("#transfer-docs").on("change", function() {
				if ($("#transfer_id").val() != '')
					$("#trnsfer_doc_btn").removeAttr('disabled');
			});

			if ($("#transfer_id").val() == '')
				$("#trnsfer_doc_btn").attr('disabled', 'true');

			$('#employeeSl').bind('click', function() {
				showPop();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
</html>
