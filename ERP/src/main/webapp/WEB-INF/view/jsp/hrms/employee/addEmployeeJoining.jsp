<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#join_sve_sccs {
	display: none;
}

#join_updt_sccs {
	display: none;
}

#join_failed {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">

		<div class="page-header">
			<h1>Employees Joining</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<a href="employeeJoining.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="success_msg" id="join_sve_sccs">
					<span id="msgs_icn"><i class="fa fa-check"></i></span>Saved
					Successfully.
				</div>
				<div class="success_msg" id="join_updt_sccs">
					<span id="msgs_icn"><i class="fa fa-check"></i></span>Updated
					Successfully.
				</div>
				<div class="error_msg" id="join_failed">
					<span id="msge_icn"><i class="fa fa-times"></i></span>Failed...
				</div>
			</div>
		</div>




		<div class="row">


			<div class="col-md-12">


				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>

				</ul>



				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form commandName="employeeJoiningVo" method="POST" id="aaa"
								action="saveEmployeeJoining.do">
								<form:hidden path="joiningId" id="joining_id" />
								<div class="row">
									<div class="col-md-6">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Employee Joining
													Information</div>
												<div class="panel-body" style="overflow: visible;">



													<div class="form-group">
														<label for="inputEmail3"
															class="col-sm-3 col-xs-12 control-label">Employee
															Name <span class="stars">*</span>
														</label>
														<div class="col-sm-8 col-xs-10 br-de-emp-list">
															<form:input path="employee" class="form-control"
																placeholder="Search Employee" id="employeeSl" />
															<form:hidden path="employeeCode" id="employeeCode" />

														</div>
														<div class="col-sm-1 col-xs-1">
															<a href="#" class="show2"><i class="fa fa-user"></i></a>
														</div>
													</div>

													<div id="pop1" class="simplePopup">
													</div>

													<div id="pop2" class="simplePopup empl-popup-gen"></div>




													<div class="form-group">
														<label class="col-sm-3 control-label">Joining Date
															<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<div class="input-group date" id="datepickersss">
																<form:input path="joiningDate" id="emp_join_joindate"
																	style="background-color: #ffffff !important;"
																	cssClass="form-control" required="required"
																	readonly="true" data-date-format="DD/MM/YYYY" />
																<span class="input-group-addon"><span
																	class="glyphicon-calendar glyphicon"></span> </span>
															</div>
														</div>

													</div>




													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<input type="checkbox" value="notify"
																onclick="clickNotify()" id="send_notif">Send
															Notification</input>
														</div>
													</div>
													
													<div class="form-group" id="sups">
														<label class="col-sm-3 control-label">Superiors</label>
														<div class="col-sm-9">
															<select id="joining_sups" multiple="true" 
															 class="form-control">
															</select>
														</div>
													</div>
													

												</div>
											</div>
										</div>



										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">At the Time of Joining</div>
												<div class="panel-body">

													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Type <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select cssClass="form-control"
																path="employeeTypeId" id="emp_join_emptype">
																<form:option value="">Select Employee Type</form:option>
																<form:options items="${employeeType }"
																	itemLabel="employeeType" itemValue="employeeTypeId" />
															</form:select>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Designation <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:select cssClass="form-control" path="designationId"
																id="emp_join_emptdes">
																<form:option value="">Select Designation</form:option>
																<form:options items="${designation }"
																	itemLabel="designation" itemValue="id" />
															</form:select>
														</div>

													</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Branch <span
															class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select cssClass="form-control" path="branchId"
																id="emp_join_emptbrnch">
																<form:option value="">Select Branch</form:option>
																<form:options items="${branch }" itemLabel="branchName"
																	itemValue="branchId" />
															</form:select>
														</div>

													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">Department <span
															class="stars">*</span></label>
														<div class="col-sm-9">
															<form:select cssClass="form-control" path="departmentId"
																id="emp_join_emptdept">
																<form:option value="">Select Department</form:option>
																<form:options items="${departments }" itemLabel="name"
																	itemValue="id" />
															</form:select>
														</div>

													</div>










												</div>
											</div>
										</div>

									</div>

									<div class="col-md-6">

										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Additional Information</div>
												<div class="panel-body">

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Notes
														</label>
														<div class="col-sm-9">
															<form:textarea path="notes" id="empjoin_new_note"
																cssClass="form-control height" />
														</div>
													</div>


													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<a id="emp_join_save_btn"
																onclick="validateEmployeeJoin()" class="btn btn-info">Save</a>
														</div>
													</div>


												</div>
											</div>
										</div>

									</div>
								</div>

							</form:form>
						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up "
						id="users">
						<div class="panel-body">

							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Upload Documents</div>
									<div class="panel-body">
										<form:form action="uploadEmployeeJoinDocs.do" method="POST"
											enctype="multipart/form-data" commandName="employeeJoiningVo">
											<form:hidden path="joiningId" id="joining_idd" />
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Upload
													Documents </label>
												<div class="col-sm-9">
													<input type="file" multiple name="emp_join_files_upld"
														id="joining-docs" class="btn btn-info" />
												</div>
											</div>
											<c:if
												test="${ rolesMap.roles['Employee Joining Documents'].add}">
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" id="emplo_joininig_doc_upload"
															class="btn btn-info">Upload</button>
													</div>
												</div>
											</c:if>
										</form:form>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="docs">
						<div class="panel-body">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">Simple Map</div>
									<div class="panel-body">
										<div id="simpleMap" style="height: 500px;"></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>


		</div>




	</div>
	<!-- Warper Ends Here (working area) -->


	<!-- Content Block Ends Here (right box)-->



	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>

	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<!-- TypeaHead -->
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>

	<!-- InputMask -->
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>

	<!-- TagsInput -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>

	<!-- Chosen -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<!-- moment -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>

	<!-- DateTime Picker -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<!-- Wysihtml5 -->
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
		
		
		<script
		src="<c:url value='/resources/docs/js/bootstrap-3.3.2.min.js' />"></script>
	<script src="<c:url value='/resources/docs/js/prettify.js' />"></script>
	<script
		src="<c:url value='/resources/docs/js/bootstrap-multiselect.js' />"></script>
		
		
	<script src="<c:url value='/resources/assets/js/hrms_validator.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			clickNotify();
			$("#emplo_joininig_doc_upload").attr('disabled', 'true');
			$('#employeeSl').bind('click', function() {
				showPop();
			});

			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			if ($("#joining_id").val() != '') {
				$("#emp_join_brnch").attr("readOnly", "true");
				$("#emp_join_dprmnt").attr("readOnly", "true");
				$("#emp_join_emp").attr("readOnly", "true");
			}

			if ($("#joining-docs").val() == '') {
				$("#emplo_joininig_doc_upload").attr('disabled', 'true');
			}

			$("#joining-docs").on("change", function() {
				if ($("#joining_id").val() != '')
					$("#emplo_joininig_doc_upload").removeAttr('disabled');
			});

			$('#employeeSl').bind('input', function() {
				alert("chbged");
			});
			
			$('#joining_sups').multiselect({
				includeSelectAllOption : true
			});
			$(".btn-group").click(function(){
				$(".btn-group").addClass("open");
			});
			
			getEmployeeDetails();

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").addClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});

		function clickNotify() {
			if (document.getElementById('send_notif').checked) {
				//getJoiningSuperiors();
				$("#sups").css("display","block");
			} else {
				$("#sups").css("display","none");
			}
		}
	</script>

</body>
</html>