<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/timepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/progress/uploadfile.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div id="fade"></div>
	<div id="modal">
		<img id="loader" src="resources/images/loading.gif" />
	</div>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Work Shifts</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="row" id="emp-shft-info">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Worksheet Information</div>
										<form action="saveWorkShiftDetails.do" method="post"
											id="workShiftDetailsForm">
											<div class="panel-body">
												<div class="col-sm-6">
													<div class="form-group">
														<label class="col-sm-3 control-label">Employee
															Code <span class="stars">*</span></label>
														<div class="col-sm-9">
															<select
																class="form-control chosen-select wrk-shft-emp-cod"
																data-placeholder="Choose Employee" name="employeeCode">
																<option></option>
																<c:forEach items="${employeeList}" var="employee">
																	<option value="${employee.value}">${employee.label}</option>
																</c:forEach>
															</select>
														</div>
													</div>
												</div>
												<!-- <div class="col-sm-6">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">
															Shift Name</label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="shiftType"
																id="shiftType" placeholder="Shift Name" readonly="true"
																style="background-color: #fff !important">
														</div>
													</div>
												</div> -->

												<table class="table table-bordered no-margn" border="1">
													<tbody>
														<tr>
															<td colspan="2" rowspan="2" align="center">Work Days</td>
															<td colspan="2" align="center">Regular Work Hours</td>
															<td colspan="3" align="center">Refreshment Break
																Hours</td>
															<td colspan="3" align="center">Additional Break
																Hours</td>
														</tr>
														<tr>
															<td align="center">From</td>
															<td align="center">To</td>
															<td colspan="2" align="center">From</td>
															<td align="center">To</td>
															<td colspan="2" align="center">From</td>
															<td align="center">To</td>
														</tr>
														<c:forEach items="${shiftDays}" var="shiftDay"
															varStatus="status">
															<tr>
																<td colspan="2"><input type="checkbox"
																	name="shiftDay${status.count}"
																	id="shiftDay${status.count}" /> &nbsp;
																	${shiftDay.shiftDay}</td>
																<td>
																	<!-- <input type="checkbox" class="flt_l" />&nbsp; -->
																	<input type="text"
																	name="time_setter${(status.count-1)*6+1}"
																	id="time_setter${(status.count-1)*6+1}"
																	class="form-control tadj" value="" />
																</td>
																<td><input type="text"
																	name="time_setter${(status.count-1)*6+2}"
																	id="time_setter${(status.count-1)*6+2}"
																	class="form-control tadj" value="" /></td>
																<td colspan="2">
																	<!-- <input type="checkbox"
																	class="flt_l" />&nbsp; -->
																	<input type="text"
																	name="time_setter${(status.count-1)*6+3}"
																	id="time_setter${(status.count-1)*6+3}"
																	class="form-control tadj" value="" />
																</td>
																<td><input type="text"
																	name="time_setter${(status.count-1)*6+4}"
																	id="time_setter${(status.count-1)*6+4}"
																	class="form-control tadj" value="" /></td>
																<td colspan="2">
																	<!-- <input type="checkbox"
																	class="flt_l" />&nbsp;  --> <input type="text"
																	name="time_setter${(status.count-1)*6+5}"
																	id="time_setter${(status.count-1)*6+5}"
																	class="form-control tadj" value="" />
																</td>
																<td><input type="text"
																	name="time_setter${(status.count-1)*6+6}"
																	id="time_setter${(status.count-1)*6+6}"
																	class="form-control tadj" value="" /></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
												<br />
												<div class="form-group flr">
													<button type="submit" class="btn btn-info">Save</button>
												</div>
											</div>
										</form>
									</div>
								</div>

								<!-- <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">Additional Information</div>
                    <div class="panel-body">
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Notes </label>
                        <div class="col-sm-9">
                          <textarea class="form-control height"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Record Added By </label>
                        <div class="col-sm-9">
                          <label for="inputPassword3" class="col-sm-9 control-label">System Administrator </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Record Added on </label>
                        <div class="col-sm-9">
                          <label for="inputPassword3" class="col-sm-9 control-label">25-2-2015 </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" class="btn btn-info">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Warper Ends Here (working area) -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.smartTabs.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script type="text/javascript"
		src="<c:url value='http://code.jquery.com/ui/1.11.0/jquery-ui.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery-ui-sliderAccess.js' />"
		type="text/javascript"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery-ui-timepicker-addon.js' />"
		type="text/javascript"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery-ui-timepicker-addon-i18n.min.js' />"
		type="text/javascript"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		$(document).ready(function() {
			/* $('.show1').click(function() {
				$('#pop1').simplePopup();
			});
			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});
			$('.show3').click(function() {
				$('#pop3').simplePopup();
			});
			$('.show4').click(function() {
				$('#pop4').simplePopup();
			});
			$('.show5').click(function() {
				$('#pop5').simplePopup();
			});
			$('.show6').click(function() {
				$('#pop6').simplePopup();
			});
			$('.show7').click(function() {
				$('#pop7').simplePopup();
			});
			$('.show8').click(function() {
				$('#pop8').simplePopup();
			});
			$('.show9').click(function() {
				$('#pop9').simplePopup();
			});
			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});
			$('.show10').click(function() {
				$('#pop10').simplePopup();
			});
			$('.show11').click(function() {
				$('#pop11').simplePopup();
			});
			$('.show12').click(function() {
				$('#pop12').simplePopup();
			});
			$('.show13').click(function() {
				$('#pop13').simplePopup();
			});
			$('.show14').click(function() {
				$('#pop14').simplePopup();
			});
			$('.show15').click(function() {
				$('#pop15').simplePopup();
			});
			$('.show16').click(function() {
				$('#pop16').simplePopup();
			});
			$('.show17').click(function() {
				$('#pop17').simplePopup();
			}); */
			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").addClass("active");
			$("#payrollli").removeClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
	<script>
		$('#js-smart-tabs').smartTabs();
		$('#js-smart-tabs--tabs').smartTabs({
			layout : 'tabs'
		});
		$('#js-smart-tabs--accordion').smartTabs({
			layout : 'accordion'
		});
	</script>
	<script>
		$(document).ready(function() {

			$("#admin_ss").click(function() {
				$("#add_administartor").show();
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addfield").click(function() {
				$("#add_custom_feild").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#addrm").click(function() {
				$("#add_rm").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
			});
		});
	</script>
</body>
</html>
