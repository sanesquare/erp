<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SNOGOL HRM</title>
<meta name="description" content="">

<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#add_prjc_succs {
	display: none;
}

#add_prjc_err {
	display: none;
}
</style>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="assets/css/bootstrap/bootstrap.css" />

<!-- Fonts  -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>

<!-- Base Styling  -->
<link rel="stylesheet" href="assets/css/app/app.v1.css" />

<!-- Chosen Select  -->
<link rel="stylesheet" href="assets/css/plugins/bootstrap-chosen/chosen.css" />

<!-- DateTime Picker  -->
<link rel="stylesheet" href="assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css" />
<link href="assets/css/RegalCalendar.css" rel="stylesheet" type="text/css" />

</head>
<body data-ng-app>



  <!-- Header Ends -->
  
  <div class="warper container-fluid">
    <div class="page-header">
      <h1>Achievements</h1>
    </div>
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#all" role="tab" data-toggle="tab">Information</a></li>
          <li role="presentation"><a href="#users" role="tab" data-toggle="tab">Documents</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="panel panel-default tab-pane tabs-up active" id="all">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">Achievement Information</div>
                      <div class="panel-body">
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label"> Employee</label>
                          <div class="col-sm-8 col-xs-10">
                            <select class="form-control chosen-select" data-placeholder=" Employee ">
                              <option></option>
                              <option value="United States">United States<span>(designing)</span></option>
                              <option value="United Kingdom">United Kingdom<span>(designing)</span></option>
                              <option value="Afghanistan">Afghanistan</option>
                              <option value="Albania">Albania</option>
                              <option value="Algeria">Algeria <span>(designing)</span></option>
                              <option value="American Samoa">American Samoa</option>
                              <option value="Andorra">Andorra</option>
                              <option value="Angola">Angola</option>
                              <option value="Anguilla">Anguilla</option>
                              <option value="Antarctica">Antarctica</option>
                              <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                              <option value="Argentina">Argentina</option>
                              <option value="Armenia">Armenia</option>
                              <option value="Aruba">Aruba</option>
                              <option value="Australia">Australia</option>
                            </select>
                          </div>
                          <div class="col-sm-1 col-xs-1"><a href="#" class="show2"><i class="fa fa-plus-square-o pop_icon"></i></a></div>
                        </div>
                        <div id="pop2" class="simplePopup">
                          <div class="col-md-12">
                            <div class="form-group">
                              <h1 class="pop_hd">Azad</h1>
                              <div class="row">
                                <div class="col-md-3"><img src="assets/images/avtar/user.png" class="pop_img"></div>
                                <div class="col-md-9">
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">E-mail </label>
                                    <div class="col-sm-9">
                                      <label for="inputPassword3" class="col-sm-6 control-label ft">jsafs@.com</label>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">Designation </label>
                                    <div class="col-sm-9">
                                      <label for="inputPassword3" class="col-sm-6 control-label ft">2545895678</label>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">Department </label>
                                    <div class="col-sm-9">
                                      <label for="inputPassword3" class="col-sm-6 control-label ft">dsafdsfdsf</label>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">Station </label>
                                    <div class="col-sm-9">
                                      <label for="inputPassword3" class="col-sm-6 control-label ft">2sdfds8</label>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">Join Date </label>
                                    <div class="col-sm-9">
                                      <label for="inputPassword3" class="col-sm-6 control-label ft">25-2-2015</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label"> Forward Application To</label>
                          <div class="col-sm-9">
                                 <input type="text" class="form-control" id="inputPassword3" placeholder="Job Title">
                                </div>
                          
                        </div>
                        
                        
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Achievement Date </label>
                          <div class="col-sm-9">
                            <div class="input-group date" id="datepicker">
                              <input type="text" class="form-control" data-date-format="YYYY/MM/DD">
                              <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span> </span> </div>
                          </div>
                        </div>
                        
                        
                        
                     
                            <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-info">Next</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      
                      
                      
                      <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">Achievement Description</div>
                      <div class="panel-body">
                        <div class="form-group">
                          <div class="col-md-12">
                            <textarea class="wysihtml form-control" placeholder="Enter text ..." style="height: 150px">
                            	
                            </textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-16 col-sm-12">
                            <button type="submit" class="btn btn-info">Next</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                      
                  
                </div>
                <div class="col-md-6">
             <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">Additional Information</div>
                      <div class="panel-body">
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Notes </label>
                          <div class="col-sm-9">
                            <textarea class="form-control height"></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Record Added By </label>
                          <div class="col-sm-9">
                            <label for="inputPassword3" class="col-sm-9 control-label">System Administrator </label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Record Added on </label>
                          <div class="col-sm-9">
                            <label for="inputPassword3" class="col-sm-9 control-label">25-2-2015 </label>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <!-- <button type="submit" class="btn btn-info">Save</button> -->
                            <a class="btn btn-info" id="submt_achiev_info_btn"><spring:message code="achievements.info.btn.save_info"/></a> 
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          
          <div role="tabpanel" class="panel panel-default tab-pane tabs-up " id="users">
            <div class="panel-body">
              <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">Upload Documents</div>
                  <div class="panel-body">
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-3 control-label">Upload Documents </label>
                      <div class="col-sm-9">
                        <input type="file" class="btn btn-info"/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">Show Documents</div>
                  <div class="panel-body">
                    <table class="table no-margn">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Document Name</th>
                          <th>view</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td><i class="fa fa-file-o"></i> Lorem ipsum dolor sit amet</td>
                          <td><i class="fa fa-file-text-o sm"></i></td>
                          <td><i class="fa fa-close ss"></i></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="panel panel-default tab-pane tabs-up" id="docs">
            <div class="panel-body">
              <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading">Simple Map</div>
                  <div class="panel-body">
                    <div id="simpleMap" style="height:500px;"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Warper Ends Here (working area) -->
  

<!-- Content Block Ends Here (right box)--> 

<!-- JQuery v1.9.1 --> 
<script src="assets/js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script> 
<script src="assets/js/plugins/underscore/underscore-min.js"></script> 

<!-- Bootstrap --> 
<script src="assets/js/bootstrap/bootstrap.min.js"></script> 
<script src="assets/js/globalize/globalize.min.js"></script> 
<!-- NanoScroll --> 
<script src="assets/js/plugins/nicescroll/jquery.nicescroll.min.js"></script> 
<!-- TypeaHead --> 
<script src="assets/js/plugins/typehead/typeahead.bundle.js"></script> 
<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script> 

<!-- InputMask --> 
<script src="assets/js/plugins/inputmask/jquery.inputmask.bundle.js"></script> 

<!-- TagsInput --> 
<script src="assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script> 

<!-- Chosen --> 
<script src="assets/js/plugins/bootstrap-chosen/chosen.jquery.js"></script> 

<!-- moment --> 
<script src="assets/js/moment/moment.js"></script> 
<!-- DateTime Picker --> 
<script src="assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script> 

<!-- Wysihtml5 --> 
<script src="assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script> 
<script src="assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script> 

<!-- Custom JQuery --> 
<script src="assets/js/app/custom.js" type="text/javascript"></script> 
<script src="assets/js/jquery.simplePopup.js" type="text/javascript"></script> 
<script type="text/javascript">

$(document).ready(function(){

    $('.show1').click(function(){
	$('#pop1').simplePopup();
    });
  
    $('.show2').click(function(){
	$('#pop2').simplePopup();
    });  
  
    $("#dashbordli").removeClass("active");
	$("#organzationli").removeClass("active");
	$("#recruitmentli").removeClass("active");
	$("#employeeli").addClass("active");
	$("#timesheetli").removeClass("active");
	$("#payrollli").removeClass("active");
	$("#reportsli").removeClass("active");
});

</script>
</html>
