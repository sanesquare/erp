<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	<button type="button" class="btn btn-primary flr mtft" id="addnot">Add
		Notification</button>
	<table cellpadding="0" cellspacing="0" border="0"
		class="table table-striped table-bordered">
		<thead>
			<tr>
				<th width="10%">S#</th>
				<th width="20%">Notification Type</th>
				<!-- <th width="25%">Send To</th> -->
				<th width="25%">Days to Send Notification</th>
				<th width="10%">Edit</th>
				<th width="10%">Delete</th>
			</tr>
		</thead>
		<tbody>
			 <c:forEach items="${notifications }" var="notification" varStatus="status">
                            	<tr>
                            		<td>${status.index +1 }</td>
                            		<td>${notification.notificationType.type }</td>
                            		<td>${notification.mstReference.referenceVal }</td>
                            		<td><a href="#"  onclick="editNotification(${notification.id })"><i class="fa fa-edit sm"></i> </a></td>
                                    <td><a class="delete_project" onclick="notificationDelete(${notification.id })" ><i class="fa fa-close ss"></i></a> </td>
                            	</tr>
                            </c:forEach> 
		</tbody>
	</table>
	<script>
		$(document).ready(function() {
			$("#addnot").click(function() {				
				showNotification(0);
			});
			
			showNotification = function(nid) {
				$("#add_notification").toggle(600);
				window.scrollTo(0, document.body.scrollHeight);
				$.ajax({
					url: "addNotification.do",
					type: "GET",
					data : { notificationId  :  nid },
					success: function(data) {
						$("#div_notification_form").html(data);
					}
				});
			}
			
			editNotification = function(nid) {
				showNotification(nid);
			}
			
		});
	</script>