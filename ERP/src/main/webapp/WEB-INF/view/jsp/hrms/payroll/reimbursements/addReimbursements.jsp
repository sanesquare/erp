<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Reimbursements</h1>
		</div>

		<div class="row">
			<div class="col-md-12">
				<a href="reimbursementsHome.do" class="btn btn-info"
					style="float: right;"><i class="fa fa-arrow-left"></i></a>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty success_msg}">
					<div class="success_msg">
						<span id="msgs_icn"><i class="fa fa-check"></i></span>${success_msg}
					</div>
				</c:if>
				<c:if test="${not empty error_msg}">
					<div class="error_msg">
						<span id="msge_icn"><i class="fa fa-times"></i></span>${error_msg}
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#users" role="tab"
						data-toggle="tab">Documents</a></li>


				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<form:form action="saveReimbursements.do" method="POST"
								id="reimbursementsAddForm" modelAttribute="reimbursementsVo">
								<form:hidden path="reimbursementId" id="reimbursementId" />
								<div class="row">


									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Reimbursement Information</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputEmail3"
														class="col-sm-3 col-xs-12 control-label">Employee
														Name</label>
													<div class="col-sm-8 col-xs-10 br-de-emp-list">
														<form:input path="employee" class="form-control"
															placeholder="Search Employee" id="employeeSl" />
														<form:hidden path="employeeCode" id="employeeCode" />

													</div>
													<div class="col-sm-1 col-xs-1">
														<a href="#" class="show2"><i class="fa fa-user"></i></a>
													</div>
												</div>

												<div id="pop1" class="simplePopup"></div>

												<div id="pop2" class="simplePopup empl-popup-gen"></div>


												<div class="form-group">
													<label for="inputEmail3" class="col-sm-3 control-label">
														Forward Application To</label>
													<div class="col-sm-9">

														<form:select cssClass="form-control chosen-select"
															path="superiorIds" id="reimbursements_superiors"
															multiple="true">
														</form:select>
														<form:hidden path="superiorAjaxIds"
															id="reimbursements_suprior_ajx_hidn" />
													</div>

												</div>



												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Title</label>
													<div class="col-sm-9">
														<form:input path="title" cssClass="form-control"
															id="inputPassword3" />
													</div>
												</div>

												<div class="form-group reimbursement-amouint">
													<label for="inputPassword3" class="col-sm-3 control-label">
														Amount</label>
													<div class="col-sm-9">
														<form:input path="amount" cssClass="form-control double"
															style="background-color: #ffffff !important;"
															readonly="true" id="reimbursement_amount" />
													</div>
												</div>


												<div class="form-group">
													<label class="col-sm-3 control-label"> Date</label>
													<div class="col-sm-9">
														<div class="input-group date" id="datepicker">
															<form:input path="date" class="form-control"
																style="background-color: #ffffff !important;"
																id="reimbursement_date" readonly="true"
																data-date-format="DD/MM/YYYY" />
															<span class="input-group-addon"><span
																class="glyphicon-calendar glyphicon"></span> </span>
														</div>
													</div>
												</div>



											</div>
										</div>
									</div>




									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Overtime Description</div>
											<div class="panel-body">
												<div class="form-group">
													<div class="col-md-12">

														<form:textarea cssClass=" form-control"
															id="reimbursement_description" path="description"
															cssStyle="height: 150px;resize:none" />

													</div>
												</div>


											</div>
										</div>
									</div>



								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">Reimbursement Items</div>
											<div class="panel-body">

												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%">#</th>
															<th width="20%">Date</th>
															<th width="10%">Category</th>
															<th width="35%">Item</th>
															<th width="10%">Receipt</th>
															<th width="10%">Destination</th>
															<th width="10%">Amount</th>

														</tr>
													</thead>
													<tbody>
														<c:forEach items="${reimbursementsVo.itemsVos}" var="r"
															varStatus="s">
															<tr>
																<td>${s.count }</td>
																<td><form:hidden path="itemsVos[${s.index}].id" />
																	<div class="input-group date"
																		id="datepickers${s.count }">
																		<form:input path="itemsVos[${s.index}].date"
																			class="form-control"
																			style="background-color: #ffffff !important;"
																			readonly="true" data-date-format="DD/MM/YYYY" />
																		<span class="input-group-addon"><span
																			class="glyphicon-calendar glyphicon"></span> </span>
																	</div></td>
																<td><form:select
																		path="itemsVos[${s.index}].categoryId"
																		cssClass="form-control">
																		<option value="">--Select--</option>
																		<form:options items="${categories }"
																			itemLabel="reimbursementCategory"
																			itemValue="reimbursementCategoryId" />
																	</form:select></td>
																<td><form:input cssClass="form-control"
																		path="itemsVos[${s.index}].item" value="${r.item }" />
																</td>
																<td><form:input path="itemsVos[${s.index}].receipt"
																		cssClass="form-control" id="inputPassword3" /></td>
																<td><form:input path="itemsVos[${s.index}].country"
																		cssClass="form-control" id="inputPassword3" /></td>
																<td><form:input path="itemsVos[${s.index}].amount"
																		onkeypress="calculateReImbursement()"
																		onfocus="calculateReImbursement()"
																		onblur="calculateReImbursement()"
																		onkeyup="calculateReImbursement()"
																		cssClass="form-control double"
																		id="reimbursementAmount${s.count }" /></td>
															</tr>

														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>





								<div class="row">







									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Additional Information</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Notes
													</label>
													<div class="col-sm-9">
														<form:textarea cssClass=" form-control"
															id="reimbursement_notes" path="notes"
															cssStyle="height: 150px;resize:none" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added By </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${reimbursementsVo.createdBy }
														</label>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Record
														Added on </label>
													<div class="col-sm-9">
														<label for="inputPassword3" class="col-sm-9 control-label">${reimbursementsVo.createdOn }
														</label>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="button" id="reimbursements_btn"
															class="btn btn-info">Save</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>




							</form:form>
						</div>
					</div>



					<div role="tabpanel" class="panel panel-default tab-pane tabs-up"
						id="users">
						<div class="panel-body">
							<c:if test="${ rolesMap.roles['Reimbursements Documents'].add}">
								<form:form action="updateReimbursementsDocument.do"
									commandName="reimbursementsVo" method="POST"
									enctype="multipart/form-data">
									<form:hidden path="reimbursementId" />
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Upload Documents</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Upload
														Documents </label>
													<div class="col-sm-9">
														<input type="file" multiple name="reimbursementDocuments"
															id="reimbursement-docs" class="btn btn-info" />
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" id="reimbursement_doc_upload"
															class="btn btn-info">Save</button>
													</div>
												</div>

											</div>
										</div>
									</div>
								</form:form>
							</c:if>

							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">Show Documents</div>
									<div class="panel-body">

										<c:if test="${! empty reimbursementsVo.documentVos }">
											<table class="table no-margn">
												<thead>
													<tr>
														<th>#</th>
														<th>Document Name</th>
														<c:if
															test="${ rolesMap.roles['Reimbursements Documents'].view}">
															<th>view</th>
														</c:if>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${ reimbursementsVo.documentVos }"
														var="d" varStatus="s">
														<tr>
															<td>${s.count }</td>
															<td><i class="fa fa-file-o"></i>${d.fileName }</td>
															<c:if
																test="${ rolesMap.roles['Reimbursements Documents'].view}">
																<td><a
																	href="downloadReimbursementDocument.do?url=${d.url }"><i
																		class="fa fa-file-text-o sm"></i></a></td>
															</c:if>
															<c:if
																test="${ rolesMap.roles['Reimbursements Documents'].delete}">
																<td><a
																	href="deleteReimbursementDocument.do?url=${d.url }"><i
																		class="fa fa-close ss"></i></a></td>
															</c:if>
														</tr>
													</c:forEach>
												</tbody>
											</table>

										</c:if>
									</div>
								</div>
							</div>




						</div>
					</div>



				</div>
			</div>
		</div>
	</div>
	<script
		src='<c:url value="/resources/assets/js/jquery/jquery-1.9.1.min.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/plugins/underscore/underscore-min.js" />'></script>

	<!-- Bootstrap -->
	<script
		src='<c:url value="/resources/assets/js/bootstrap/bootstrap.min.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/globalize/globalize.min.js" />'></script>
	<!-- NanoScroll -->
	<script
		src='<c:url value="/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js" />'></script>
	<!-- TypeaHead -->
	<script
		src='<c:url value="/resources/assets/js/plugins/typehead/typeahead.bundle.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/plugins/typehead/typeahead.bundle-conf.js" />'></script>

	<!-- InputMask -->
	<script
		src='<c:url value="/resources/assets/js/plugins/inputmask/jquery.inputmask.bundle.js" />'></script>

	<!-- TagsInput -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" />'></script>

	<!-- Chosen -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js" />'></script>

	<!-- moment -->
	<script src='<c:url value="/resources/assets/js/moment/moment.js" />'></script>
	<!-- DateTime Picker -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js" />'></script>

	<!-- Wysihtml5 -->
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js" />'></script>
	<script
		src='<c:url value="/resources/assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js" />'></script>

	<!-- Custom JQuery -->
	<script src='<c:url value="/resources/assets/js/app/custom.js" />'></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.simplePopup.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/hrms.js' />"></script>
	<script src="<c:url value='/resources/assets/js/hrms-ajax.js' />"></script>

	<script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		window.onload = function() {
			$('#employeeSl').bind('click', function() {
				showPop();
			});
			$('.show2').click(function() {
				fillEmployeePopup($("#employeeCode").val());
				if ($("#employeeCode").val() != "") {
					$('#pop2').simplePopup();
				}
			});

			getEmployeeInformation($("#employeeCode").val());

			getDetilsOfEmployee();
			if ($("#reimbursementId").val() == '') {
				$("#reimbursement_doc_upload").attr("disabled", "true");
			}

			$("#reimbursement_doc_upload").attr("disabled", "true");

			if ($("#reimbursement-docs").val == '') {
				$("#reimbursement_doc_upload").attr("disabled", "true");
			}

			$("#reimbursement-docs").on("change", function() {
				if ($("#reimbursementId").val() != '')
					$("#reimbursement_doc_upload").removeAttr("disabled");
			});

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
		}

		function calculateReImbursement() {
			var amount1 = 0;
			var amount2 = 0;
			var amount3 = 0;
			var amount4 = 0;
			var total = 0;
			if ($("#reimbursementAmount1").val() != "")
				amount1 = $("#reimbursementAmount1").val();
			if ($("#reimbursementAmount2").val() != "")
				amount2 = $("#reimbursementAmount2").val();
			if ($("#reimbursementAmount3").val() != "")
				amount3 = $("#reimbursementAmount3").val();
			if ($("#reimbursementAmount4").val() != "")
				amount4 = $("#reimbursementAmount4").val();

			total = parseFloat(amount1) + parseFloat(amount2)
					+ parseFloat(amount3) + parseFloat(amount4);
			$("#reimbursement_amount").val(total);

		}
	</script>
</body>
</html>