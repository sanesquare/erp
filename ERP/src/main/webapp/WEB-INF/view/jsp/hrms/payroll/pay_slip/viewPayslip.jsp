<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
.visible {
	display: none;
}
</style>
</head>
<body>
	<div class="warper container-fluid">
		<div class="page-header">
			<h1>Salary Payslips</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="payslip.do" class="btn btn-info" style="float: right;"><i
					class="fa fa-arrow-left"></i></a>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#all"
						role="tab" data-toggle="tab">Information</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel"
						class="panel panel-default tab-pane tabs-up active" id="all">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Salary Payslip Information</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputEmail3" class="col-xs-3 control-label">
													Employee Name</label>
												<div class="col-xs-9">
													<label for="inputPassword3" class="col-xs-9 control-label">${payslip.employee }
													</label>
												</div>

											</div>




											<div class="form-group">
												<label class="col-xs-3 control-label">Salary Date</label>
												<div class="col-xs-9">
													<label for="inputPassword3" class="col-xs-9 control-label">${payslip.salaryDate }
													</label>
												</div>

											</div>







										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Salary Payslip Period</div>
										<div class="panel-body">
											<div class="form-group">
												<label class="col-xs-3 control-label">Start Date</label>
												<div class="col-xs-9">
													<label for="inputPassword3" class="col-xs-9 control-label">${payslip.startDate }
													</label>
												</div>

											</div>



											<div class="form-group">
												<label class="col-xs-3 control-label">End Date</label>
												<div class="col-xs-9">
													<label for="inputPassword3" class="col-xs-9 control-label">${payslip.endDate }
													</label>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row" id="Table1">

								<%-- <div class="col-md-12 visible">
									<table class="table table-bordered">
										<tr>
											<td colspan="4" align="center"><img
												src="http://3gmobileworld.in/wp-content/uploads/2015/04/3g-logo-header-2.png" width="150px"
												   /></td>
										</tr>

										<tr>
											<td colspan="4" align="center">
												<h2>PAYSLIP</h2> <span>${payslip.startDate }</span> To <span>
													${payslip.endDate }</span>
											</td>
										</tr>
										<tr>
											<td colspan="4"></td>
										</tr>

										<tr>
											<td>Employee Name : <span>${payslip.employee }</span></td>
											<td>Designation : <span>${payslip.designation }</span></td>
										</tr>

										<tr>
											<td>Department : <span>${payslip.department }</span></td>
											<td>Branch : <span>${payslip.branch }</span></td>
										</tr>
									</table>
								</div>

								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Salary</div>
										<div class="panel-body">
											<table class="table no-margn">
												<thead>
													<tr>
														<th width="5%">#</th>
														<th width="75%">Salary Item</th>
														<th width="15%">Amount</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${payslip.items }" var="i" varStatus="s">
														<tr>
															<td>${s.count }</td>
															<td>${i.title }</td>
															<td>${i.amount }</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
											<table class="table no-margn">
												<tr>
													<td width="5%"></td>
													<td width="75%"><strong>Total</strong></td>
													<td width="15%"><strong>${payslip.total }</strong></td>

												</tr>
											</table>



										</div>
									</div>
								</div> --%>


								<div class="col-md-12 not_visible">
									<div class="panel panel-default">
										<div class="panel-heading">Additional Information</div>
										<div class="panel-body">

											<div class="form-group">
												<label for="inputPassword3" class="col-xs-3 control-label">Notes
												</label>
												<div class="col-xs-9">
													<label for="inputPassword3"
														class="col-xs-8 control-label ft">${payslip.notes }</label>
												</div>
											</div>


											<div class="form-group">
												<label for="inputPassword3" class="col-xs-3 control-label">Record
													Added By </label>
												<div class="col-xs-9">
													<label for="inputPassword3" class="col-xs-9 control-label">${payslip.createdBy }</label>
												</div>
											</div>

											<div class="form-group">
												<label for="inputPassword3" class="col-xs-3 control-label">Record
													Added on </label>
												<div class="col-xs-9">
													<label for="inputPassword3" class="col-xs-9 control-label">${payslip.createdOn }
													</label>
												</div>
											</div>
											<!-- <div class="form-group not_visible">
												<a class="btn btn-info" onclick="PrintElem()">Print</a>
											</div>
 -->
										</div>
									</div>
								</div>
								<form:form method="POST" commandName="paysSlipVo"
									action="updatePayslipStatus.do">
									<c:if test="${ paysSlipVo.authEmployee}">
										<div class="col-md-12">
											<div class="panel panel-default">
												<form:hidden path="id" id="id" />
												<div class="panel-heading">Status</div>
												<div class="panel-body">




													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Status
														</label>
														<div class="col-sm-9">
															<form:select cssClass="form-control"
																id="commisssion_status" onchange="alertTest()"
																path="status">
																<option value="">--Select Status--</option>
																<form:options items="${status }" itemLabel="status"
																	itemValue="status" />
															</form:select>
														</div>
													</div>

													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Description
														</label>
														<div class="col-sm-9">
															<form:textarea cssClass=" form-control"
																id="commission_description" path="statusdescription"
																cssStyle="height: 150px;resize:none" />
														</div>
													</div>
													<%-- <div class="form-group" id="frwrd_commition_application">
														<label for="inputEmail3" class="col-sm-3 control-label">
															Forward Application To</label>
														<div class="col-sm-9">

															<form:select cssClass="form-control chosen-select"
																path="superiorCodes" id="commission_superiors"
																multiple="true">
																<form:options items="${superiorList }" itemLabel="label"
																	itemValue="value" />
															</form:select>

														</div>

													</div> --%>

													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<button type="submit" class="btn btn-info">Save</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</c:if>
								</form:form>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Warper Ends Here (working area) -->

	<!-- JQuery v1.9.1 -->
	<script src="assets/js/jquery/jquery-1.9.1.min.js"
		type="text/javascript"></script>
	<script src="assets/js/plugins/underscore/underscore-min.js"></script>

	<!-- Bootstrap -->
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/globalize/globalize.min.js"></script>
	<!-- NanoScroll -->
	<script src="assets/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- TypeaHead -->
	<script src="assets/js/plugins/typehead/typeahead.bundle.js"></script>
	<script src="assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>

	<!-- InputMask -->
	<script src="assets/js/plugins/inputmask/jquery.inputmask.bundle.js"></script>

	<!-- TagsInput -->
	<script
		src="assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

	<!-- Chosen -->
	<script src="assets/js/plugins/bootstrap-chosen/chosen.jquery.js"></script>

	<!-- moment -->
	<script src="assets/js/moment/moment.js"></script>
	<!-- DateTime Picker -->
	<script
		src="assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>

	<!-- Wysihtml5 -->
	<script
		src="assets/js/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
	<script
		src="assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script>

	<!-- Custom JQuery -->
	<script src="assets/js/app/custom.js" type="text/javascript"></script>
	<script src="assets/js/jquery.simplePopup.js" type="text/javascript"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.show1').click(function() {
				$('#pop1').simplePopup();
			});

			$('.show2').click(function() {
				$('#pop2').simplePopup();
			});

			$("#dashbordli").removeClass("active");
			$("#organzationli").removeClass("active");
			$("#recruitmentli").removeClass("active");
			$("#employeeli").removeClass("active");
			$("#timesheetli").removeClass("active");
			$("#payrollli").addClass("active");
			$("#reportsli").removeClass("active");
		});
	</script>
	<script language="javascript">
		function PrintElem() {

			Popup();
		}

		function Popup(data) {

			var mywindow = window.open('', 'Pay Slip', 'height=400,width=600');
			mywindow.document
					.write('<html><head><title>Salary Payslip</title>');
			mywindow.document
					.write('<link rel="stylesheet" href="resources/assets/css/app/app.v1.css" type="text/css" />');
			mywindow.document
					.write('<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300" type="text/css" />');
			mywindow.document
					.write('<link rel="stylesheet" href="resources/assets/css/bootstrap/bootstrap.css" />');
			mywindow.document
					.write('<link rel="stylesheet" href="resources/assets/css/jquery.growl.css" />');
			mywindow.document
					.write('<link rel="stylesheet" href="resources/assets/css/plugins/bootstrap-chosen/chosen.css" />');
			mywindow.document
					.write('<style type="text/css">.not_visible{display:none} </style>');
			mywindow.document
					.write('<style type="text/css">#Table1 td{font-size: 13px;} </style>');
			mywindow.document.write('</head><body >');
			mywindow.document.write($("#Table1").html());
			mywindow.document.write('</body></html>');

			mywindow.document.close(); // necessary for IE >= 10
			mywindow.focus(); // necessary for IE >= 10

			mywindow.print();
			mywindow.close();

			return true;
		}
	</script>
</body>
</html>