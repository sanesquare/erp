<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>
			<th width="20%">Employee Name</th>
			<th width="20%">Warning Date</th>
			<th width="20%">Subject</th>
			<th width="20%">Edit</th>
			<th width="20%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${warningList}" var="warning">
			<tr>
				<td><a href="ViewWarning.do?otpId=${warning.warningId}">${warning.employee}</a></td>
				<td><a href="ViewWarning.do?otpId=${warning.warningId}">${warning.warningOn}</a></td>
				<td><a href="ViewWarning.do?otpId=${warning.warningId}">${warning.subject}</a></td>
				<td><a href="UpdateWarning.do?otpId=${warning.warningId}"><i
						class="fa fa-edit sm"></i> </a></td>
				<td><a href="deleteWarning.do?otpId=${warning.warningId}"><i
						class="fa fa-close ss"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<a href="CreateWarning.do">
	<button type="button" class="btn btn-primary">Add New Warning</button>
</a>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>