<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">Code</th>
			<th width="20%">Name</th>
			<th width="20%">Address</th>
			<th width="20%">Email</th>
			<th width="20%">Phone</th>
			<th width="5%">Edit</th>
			<th width="5%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${leads }" var="v">
			<tr>
				<td>${v.leadCode }</td>
				<td>${v.name }</td>
				<td>${v.address }</td>
				<td>${v.email }</td>
				<td>${v.mobile }</td>
				<td><a href="editLeads.do?id=${v.leadId }"><i
						class="fa fa-pencil"></i> </a></td>
				<td><a href="#?id=${v.vendorId }"
					onclick="cnfrmDeleteLead('${v.leadId }')"><i
						class="fa fa-times"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>