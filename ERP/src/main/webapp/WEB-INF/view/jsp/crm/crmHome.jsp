<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
</head>
<body>
	<div class="crm_home">
		<div class="warper container-fluid">
			<div class="noti" id="notifications"></div>

			<div class="page-header">
				<div class="hdr">
					<div class="home-div">
					<a href="erpHome.do">
						<span class="home"><i class="fa fa-home"></i></span>
						</a>
					</div>
					<div class="hdr-div">
						<span class="hdr-ico"><i class="fa fa-comments"></i></span> CRM
						<small>Let's get a quick overview...</small>
					</div>

				</div>
			</div>
			<div class="row"
				style="padding-top: 3%; margin-left: 4%; margin-right: 4%">
				<div class="col-md-4">
					<div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<a href="leads.do">
						<div class="panel-body  ">

							<span class="dashboard-title ">Leads</span> <span
								class="dashboard-icons "><i class="fa fa-users"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div>
						</a>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<div class="panel-body  ">

							<span class="dashboard-title ">Opportunities</span> <span
								class="dashboard-icons "><i class="fa fa-comments-o"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<div class="panel-body  ">

							<span class="dashboard-title ">Quotes</span> <span
								class="dashboard-icons "><i class="fa fa-pencil-square-o"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div>
					</div>
				</div>
				</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		
	</script>
	<script>
		
	</script>
	<!-- Warper Ends Here (working area) -->
</body>
</html>