<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="crm_home">
		<div class="warper container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Lead's Information<a href="leads.do"><span class="back-btn"><i
									class="fa fa-arrow-left"></i></span></a>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<form:form action="saveLeads.do" commandName="leadsVo"
										id="lead-frm" method="POST">
										<form:hidden path="leadId" id="leadId" />
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Basic Information</div>
											<div class="panel-body">
											<c:if test="${! empty leadsVo.leadCode }">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Code
													</label>
													<div class="col-sm-9">
														<form:input readonly="true" id="code"
															cssStyle="background-color:#fff !important"
															path="leadCode" class="form-control paste" />
													</div>
												</div></c:if>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Name<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="name" class="form-control paste" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Address<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:textarea path="address" style="resize: none;"
															class="form-control paste" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Delivery
														Address<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:textarea path="deliveryAddress"
															style="resize: none;" class="form-control paste" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Country<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="countryId">
															<option value="">-Select Country-</option>
															<c:forEach items="${country }" var="c">
																<form:option value="${c.countryId}" label="${c.name}" />
															</c:forEach>
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Mobile<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="mobile"
															class="form-control  numbersonly" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Email<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="email" class="form-control paste" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Land
														Phone<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="landPhone"
															class="form-control numbersonly" />
													</div>
												</div>
												<div class="form-group">
													<a href="#">
														<button type="submit" class="btn btn-primary erp-btn">Save</button>
													</a>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
							<c:if test="${! empty leadsVo.leadId}">
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Contacts</div>
											<div class="panel-body">
												<div id="lead_cntct_tbl">
													<c:if test="${!empty leadsVo.contactVos }">
														<table cellpadding="0" cellspacing="0" border="0"
															class="table table-striped table-bordered erp-tbl"
															id="basic-datatable">
															<thead>
																<tr>
																	<th width="30%">Name</th>
																	<th width="30%">Email</th>
																	<th width="30%">Work Phone</th>
																	<th width="5%">Edit</th>
																	<th width="5%">Delete</th>
																</tr>
															</thead>
															<tbody>
																<c:forEach items="${leadsVo.contactVos }" var="c">
																	<tr>
																		<td>${c.name }</td>
																		<td>${c.email }</td>
																		<td>${c.workPhone }</td>
																		<td><a
																			onclick="editThisLeadContact('${c.contactId}','${c.name}','${c.email }','${c.address }','${c.workPhone }')"><i
																				class="fa fa-pencil"></i></a></td>
																		<td><a
																			onclick="deleteThisLeadContact('${c.contactId}')"><i
																				class="fa fa-times"></i></a></td>
																	</tr>
																</c:forEach>
															</tbody>
														</table>
													</c:if>
												</div>
												<div class="form-group" style="padding-top: 2%;">
													<label for="inputPassword3" class="col-sm-3 control-label">Name<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<i
															class="form-control-feedback fv-icon-no-label glyphicon glyphicon-remove nme-requed"
															style="display: none; color: #D01515;"></i> <input
															type="text" id="lead-name" class="form-control paste">
														<small class="help-block nme-requed"
															style="display: none; color: #D01515;">Name is
															required</small>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Address
													</label>
													<div class="col-sm-9">
														<textarea type="text" style="resize: none;"
															id="lead-address" class="form-control paste"></textarea>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Email
													</label>
													<div class="col-sm-9">
														<input type="text" id="lead-email"
															class="form-control paste">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Work
														Phone </label>
													<div class="col-sm-9">
														<input type="text" id="lead-workPhone"
															class="form-control numbersonly">
													</div>
												</div>
												<input type="hidden" id="leadContactId">
												<div class="form-group">
													<button type="button" id="lead-cntct-sve-btn"
														class="btn btn-primary erp-btn">Save</button>
													<button type="button" onclick="clearLeadCotnctfrm()"
														class="btn btn-primary erp-btn">Clear</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</c:if>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${! empty success_msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			if ("${leadsVo.leadId}" == "") {
				$("#code").val("${code}");
			}
		});
	</script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>

	<script src="<c:url value='/resources/assets/crm/js/crm.js' />"></script>
	<script
		src="<c:url value='/resources/assets/crm/js/crm-validation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

</body>
</html>