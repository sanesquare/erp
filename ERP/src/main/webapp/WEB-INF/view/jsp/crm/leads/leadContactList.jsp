<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:if test="${!empty contacts }">
	<table cellpadding="0" cellspacing="0" border="0"
		class="table table-striped table-bordered erp-tbl"
		id="basic-datatable">
		<thead>
			<tr>
				<th width="30%">Name</th>
				<th width="30%">Email</th>
				<th width="30%">Work Phone</th>
				<th width="5%">Edit</th>
				<th width="5%">Delete</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${contacts }" var="c">
				<tr>
					<td>${c.name }</td>
					<td>${c.email }</td>
					<td>${c.workPhone }</td>
					<td><a
						onclick="editThisLeadContact('${c.contactId}','${c.name}','${c.email }','${c.address }','${c.workPhone }')"><i
							class="fa fa-pencil"></i></a></td>
					<td><a onclick="deleteThisLeadContact('${c.contactId}')"><i
							class="fa fa-times"></i></a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>