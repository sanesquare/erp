<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">#</th>
			<th width="10%">Code</th>
			<th width="10%">Invoice</th>
			<th width="10%">Date</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${packingLists }" var="r" varStatus="s">
			<tr>
				<td><a href="viewPackingList.do?id=${r.packingListId}">
						${s.count}</a></td>
				<td><a href="viewPackingList.do?id=${r.packingListId}">
						${r.code }</a></td>
				<td><a href="viewPackingList.do?id=${r.packingListId}">
						${r.invoiceCode }</a></td>
				<td><a href="viewPackingList.do?id=${r.packingListId}">
						${r.date }</a></td>
				<td><i class="fa fa-times"
					onclick="deletePackingList('${r.packingListId}')"></i></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
</c:if>

<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>