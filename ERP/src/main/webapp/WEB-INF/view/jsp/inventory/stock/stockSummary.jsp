<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}
</style>
</head>
<body>
	<div class="inventory_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">Stock Summary</div>
						<div class="panel-body">
							<form:form commandName="filterVo" method="POST"
								action="stockSummaryFilter.do" id="stock_summary_frm">
								<div class="row">
									<div class="col-md-4">
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Start
													Date<span class="stars">*</span>
												</label>
												<div class="col-sm-9">
													<div class='input-group date ledgDate' id="datepicker">
														<input type="text" readonly="true" value="${startDate }"
															data-date-format="DD/MM/YYYY" id="p_lStart"
															name="startDate"
															style="background-color: #ffffff !important;"
															class="form-control" /> <span class="input-group-addon"><span
															class="glyphicon glyphicon-calendar"></span> </span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">End
													Date<span class="stars">*</span>
												</label>
												<div class="col-sm-9">
													<div class='input-group date ledgDate' id="datepickers">
														<input type="text" readonly="true" value="${endDate }"
															name="endDate" data-date-format="DD/MM/YYYY" id="p_lEnd"
															style="background-color: #ffffff !important;"
															class="form-control" /> <span class="input-group-addon"><span
															class="glyphicon glyphicon-calendar"></span> </span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Company<span
													class="stars">*</span>
												</label>
												<div class="col-sm-9" id="cmpPlDiv">
													<select class="form-control" name="companyId"
														id="selectedCompany">
														<c:forEach items="${companies }" var="c">
															<option value="${c.companyId }">${c.companyName }</option>
														</c:forEach>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-8"></div>
									<div class="col-md-4">
										<div class="form-group">
											<button type="submit" class="btn btn-default right">Go</button>
										</div>
									</div>
								</div>
							</form:form>
							<div class="row">
								<div class="col-md-12">
									<div class="panelBody">
										<div id="load-erp">
											<div id="spinneeer-erp"></div>
										</div>
										<div id="newPop"></div>
										<table cellpadding="0" cellspacing="0" border="0"
											class="table table-striped table-bordered erp-tbl stck-tbl">
											<thead>
												<tr>
													<th width="55%" style="text-align: center;">Particulars</th>
													<th width="15%" style="text-align: center;">Quantity</th>
													<th width="15%" style="text-align: center;">Rate</th>
													<th width="15%" style="text-align: center;">Value</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${stock }" var="s">
													<tr>
														<c:choose>
															<c:when test="${s.isProduct }">
																<td>
																	<%-- <a onclick="getDetailedView('${s.hashCode}')"> --%>${s.name }<!-- </a> -->
																</td>
																<td style="text-align: right;">${s.closingQuantity }<i>${s.unit }</i>
																</td>
																<td style="text-align: right;">${s.closingRate }</td>
																<td style="text-align: right;"><b>${s.closingValue }</b></td>
															</c:when>
															<c:otherwise>
																<c:if test="${ s.totalQuantity != zero }">
																	<td><a
																		onclick="getDetailedView('${s.hashCode}',0)">${s.name }
																	</a></td>
																	<td style="text-align: right;"><c:choose>
																			<c:when test="${s.isSameUnit }">
																${s.totalQuantity } <i>${s.unit }</i>
																			</c:when>
																			<c:otherwise>
																		--
																		</c:otherwise>
																		</c:choose></td>
																	<td style="text-align: center;">--</td>
																	<td style="text-align: right;"><b>${s.totalAmount }</b></td>
																</c:if>
															</c:otherwise>
														</c:choose>
													</tr>
												</c:forEach>
											</tbody>
											<tfoot>
												<tr>
													<th colspan="4" style="text-align: right;"><b>${totalAmount }</b></th>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="confirm-delete" style="display: none;">
		<div class="modal " style="display: block;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">Do you really want to delete this
						item?</div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<input type="hidden" id="delete-item">
							<button type="button" onclick="cancel()" class="btn btn-default">No</button>
							<button type="button" onclick="deleteItem()"
								class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<c:if test="${! empty msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>

	<div id="success-msg">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>

	<div id="erro-msg">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>


	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script type="text/javascript">
		
	</script>
	<script
		src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/erp/js/erp-validation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

</body>
</html>