<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl"
	id="basic-datatables">
	<thead>
		<tr>
			<th width="40%">Name</th>
			<th width="20%">Weight</th>
			<th width="10%">Edit</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${packings }" var="p">
			<tr>
				<td>${p.name }</td>
				<td>${p.weight }</td>
				<td><i
					onclick="editPackingKind('${p.id}','${p.name }','${p.weight }')"
					class="fa fa-pencil"></i></td>
				<td><i class="fa fa-times"
					onclick="deletePacking('${p.id}')"></i></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span id="close-msg" onclick="closeMessage()"><i
			class="fa fa-times"></i></span>
	</div>
</c:if>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>