<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="5%">#</th>
			<th width="10%">Code</th>
			<th width="10%">Godown</th>
			<th width="10%">Type</th>
			<th width="10%">Date</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${entries }" var="e" varStatus="s">
			<tr>
				<td><a
					href="viewInventoryWithdrawal.do?id=${e.inventoryWithdrawalId }">${s.count }</a></td>
				<td><a
					href="viewInventoryWithdrawal.do?id=${e.inventoryWithdrawalId }">${e.code }</a></td>
				<td><a
					href="viewInventoryWithdrawal.do?id=${e.inventoryWithdrawalId }">${e.godown }</a></td>
				<td><a
					href="viewInventoryWithdrawal.do?id=${e.inventoryWithdrawalId }"><i>${e.type }</i></a></td>
				<td><a
					href="viewInventoryWithdrawal.do?id=${e.inventoryWithdrawalId }">${e.date }</a></td>
				<td><i onclick="deleteThisIW('${e.inventoryWithdrawalId}')"
					class="fa fa-times"></i></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span id="close-msg" onclick="closeMessage()"><i
			class="fa fa-times"></i></span>
	</div>
</c:if>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>