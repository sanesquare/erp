<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="load-erp">
	<div id="spinneeer-erp"></div>
</div>
<div id="newPop"></div>
<c:if test="${show }">
	<span onclick="getDetailedViewForPrevious()"
		class="fa fa-arrow-left back"></span>
</c:if>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl stck-tbl">
	<thead>
		<tr>
			<th width="55%" style="text-align: center;">Particulars</th>
			<th width="15%" style="text-align: center;">Quantity</th>
			<th width="15%" style="text-align: center;">Rate</th>
			<th width="15%" style="text-align: center;">Value</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${stock }" var="s">
			<tr>
				<c:choose>
					<c:when test="${s.isProduct }">
						<td><a onclick="getDetailedView('${s.hashCode}','${id }')">${s.name }</a>
						</td>
						<td style="text-align: right;">${s.closingQuantity }<i>${s.unit }</i>
						</td>
						<td style="text-align: right;">${s.closingRate }</td>
						<td style="text-align: right;"><b>${s.closingValue }</b></td>
					</c:when>
					<c:otherwise>
						<c:if test="${ s.totalQuantity != zero }">
							<td><a onclick="getDetailedView('${s.hashCode}','${id }')">${s.name } 
							</a></td>
							<td style="text-align: right;"><c:choose>
									<c:when test="${s.isSameUnit }">
																${s.totalQuantity } <i>${s.unit }</i>
									</c:when>
									<c:otherwise>
																		--
																		</c:otherwise>
								</c:choose></td>
							<td style="text-align: center;">--</td>
							<td style="text-align: right;"><b>${s.totalAmount }</b></td>
						</c:if>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="4" style="text-align: right;"><b>${totalAmount }</b></th>
		</tr>
	</tfoot>
</table>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>