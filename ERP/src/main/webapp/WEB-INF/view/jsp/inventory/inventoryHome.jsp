<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
</head>
<body>
	<div class="inventory_home">
		<div class="warper container-fluid">
			<div class="noti" id="notifications"></div>

			<div class="page-header">
				<div class="hdr">
					<div class="home-div">
						<a href="erpHome.do"> <span class="home"><i
								class="fa fa-home"></i></span>
						</a>
					</div>
					<div class="hdr-div">
						<span class="hdr-ico"><i class="fa fa-truck"></i></span> Inventory
						<small>Let's get a quick overview...</small>
					</div>

				</div>
			</div>
			<div class="row"
				style="padding-top: 3%; margin-left: 4%; margin-right: 4%">
				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="stockSummary.do"> <span class="hover_effect dashboard-title">Stock
							</span></a> </span> <i class="fa fa-building-o bg-inventory transit stats-icon"></i>
						<a href="#">
							<h3 class="transit dashboard-title">
								Stock <br> <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>

				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="packingList.do"> <span
								class="hover_effect dashboard-title">Packing List </span></a> </span> <i
							class="fa fa-list-ol bg-inventory transit stats-icon"></i> <a
							href="packingList.do">
							<h3 class="transit dashboard-title">
								Packing List <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>

				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="deliveryNotes.do"> <span
								class="hover_effect dashboard-title">Delivery <br>Notes
							</span></a> </span> <i class="fa fa-sign-out bg-inventory transit stats-icon"></i>
						<a href="deliveryNotes.do">
							<h3 class="transit dashboard-title">
								Delivery <br>Notes <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>

				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="receivingNotes.do"> <span
								class="hover_effect dashboard-title">Receiving<br>Notes
							</span></a> </span> <i
							class="fa fa-sign-in flip-ico bg-inventory transit stats-icon"></i>
						<a href="receivingNotes.do">
							<h3 class="transit dashboard-title">
								Receiving <br> Notes <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>


			</div>
			<div class="row"
				style="padding-top: 3%; margin-left: 4%; margin-right: 4%">
				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="inventoryEntry.do"> <span
								class="hover_effect dashboard-title">Inventory<br>Entry
							</span></a> </span> <i
							class="fa fa-sign-in flip-ico bg-inventory transit stats-icon"></i>
						<a href="#">
							<h3 class="transit dashboard-title">
								Inventory<br>Entry <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>

				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="inventoryWithdrawal.do"> <span
								class="hover_effect dashboard-title">Inventory<br>Withdrawal
							</span></a> </span> <i class="fa fa-sign-out bg-inventory transit stats-icon"></i>
						<a href="#">
							<h3 class="transit dashboard-title">
								Inventory<br>Withdrawal<small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>
			</div>

			<div class="row" style="padding-top: 3%;">
				<div class="col-md-4"></div>
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<div class="settings">
						<span class=""><a href="inventorySettings.do"><i
								class="fa fa-cog settings-ico"></i></a></span>

					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		
	</script>
	<script>
		
	</script>
	<!-- Warper Ends Here (working area) -->
</body>
</html>