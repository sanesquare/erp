<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="inventory_home">
		<div class="warper container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Inventory Settings<a href="inventoryHome.do"><span
								class="back-btn"><i class="fa fa-arrow-left"></i></span></a>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<!-- <div class="panel-heading panel-inf">Miscellaneous
											Details</div>
										<div class="panel-body"> -->
										<div class="panel-heading panel-inf">Godowns</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-sm-12" id="unit-table-div">
													<div id="load-erp">
														<div id="spinneeer-erp"></div>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Available
														Godowns </label>
													<div class="col-sm-4" id="avl_gdwns">
														<c:forEach items="${godowns }" var="g">
															<label class="badge"><span
																onclick="editGoDown('${g.godownId}','${g.name } ')">
																	${g.name } </span>&nbsp;&nbsp;<span><i
																	onclick="deleteGoDown('${g.godownId}')"
																	class="fa fa-times"></i></span> </label>
														</c:forEach>
													</div>
												</div>
												<input type="hidden" id="unitId">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Name<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" id="godown_name" name="godown_name"
															class="form-control paste"> <input type="hidden"
															id="godown_id">
													</div>
												</div>
												<div class="form-group wid-80">
													<button type="reset" onclick="clearFrmGodown()"
														class="btn btn-primary erp-btn">New</button>
													<button type="button" onclick="savegoDown()"
														class="btn btn-primary erp-btn">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Packing</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-sm-12" id="pckng_kind_list">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl"
														id="basic-datatables">
														<thead>
															<tr>
																<th width="40%">Name</th>
																<th width="20%">Weight</th>
																<th width="10%">Edit</th>
																<th width="10%">Delete</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${packings }" var="p">
																<tr>
																	<td>${p.name }</td>
																	<td>${p.weight }</td>
																	<td><i
																		onclick="editPackingKind('${p.id}','${p.name }','${p.weight }')"
																		class="fa fa-pencil"></i></td>
																	<td><i class="fa fa-times"
																		onclick="deletePacking('${p.id}')"></i></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
												<input type="hidden" id="packing_id">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Packing
														Name<span class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" id="packng_name"
															class="form-control paste">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Weight<span
														class="stars">*</span>
													</label>
													<div class="col-sm-3">
														<input type="text" value="0.00" id="packng_weght"
															style="text-align: right;" class="form-control double">
													</div>
													<div class="col-sm-1">
														<label>Kg.</label>
													</div>
												</div>
												<div class="form-group wid-80">
													<button type="button" onclick="clearPacking()"
														class="btn btn-primary erp-btn">New</button>
													<button type="button" onclick="savePacking()"
														class="btn btn-primary erp-btn">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="confirm-delete" style="display: none;">
			<div class="modal " data-backdrop="static" data-keyboard="false"
				tabindex="-1" aria-hidden="false" style="display: block;"
				role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Confirmation</h4>
						</div>
						<div class="modal-body">Do you really want to delete this
							item?</div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<input type="hidden" id="delete-item">
								<button type="button" onclick="cancel()" class="btn btn-default">No</button>
								<button type="button" onclick="deleteItem()"
									class="btn btn-primary">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<c:if test="${! empty msg }">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${msg }</div>
				<span id="close-msg" onclick="closeMessage()"><i
					class="fa fa-times"></i></span>
			</div>
		</c:if>
		<div id="load-erp">
			<div id="spinneeer-erp"></div>
		</div>
		<div class="notification-panl successMsg" style="display: none;">
			<div class="notfctn-cntnnt"></div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
		<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
		<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
		<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
		<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
		<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
		<script
			src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>

		<script type="text/javascript"
			src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
		<script type="text/javascript"
			src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
		<script type="text/javascript"
			src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
</body>
</html>