<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<thead>
	<tr>
		<th width="10%">Product Code</th>
		<th width="10%">Item</th>
		<th width="10%">Quantity</th>
		<th width="10%">Godown</th>
		<th width="5%">Delete</th>
	</tr>
</thead>
<tbody>
	<c:forEach items="${products }" var="p">
		<tr class="productTableRow" id="${p.productCode }">
			<input type="hidden" class="prodctId" value="${p.productId }">
			<td style="padding: .5%;"><input type="hidden" class="prodctId"
				value="${p.productId }"> <input type="hidden"
				id="itemId${p.productCode }" value="${p.productId }"> <label
				id="prod_code${p.productCode }">${p.productCode }</label></td>
			<td style="padding: .5%;"><label id="prod_name${p.productCode }">${p.name }</label></td>
			<td style="padding: .5%;"><input type="text"
				value="${p.availableQty }"
				onblur="findNetAmount('${p.productCode }')"
				class="form-control double " id="product_qty${p.productCode }" /></td>
			<td style="padding: .5%;"><select class="form-control"
				id="product_godwn${p.productCode }">
					<c:forEach items="${godown }" var="g">
						<option value="${g.godownId}" label="${g.name }"></option>
					</c:forEach>
			</select></td>
			<td style="padding: .5%;"><i
				onclick="javascript:deleteProductRow(this)" class="fa fa-times"></i></td>
		</tr>
	</c:forEach>
	<tr>
		<td colspan="10" style="padding: .5%;"><button type="button"
				onclick="getProductDetails()" class="btn btn-primary erp-btn right">Add
				Item</button></td>
	</tr>
</tbody>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
