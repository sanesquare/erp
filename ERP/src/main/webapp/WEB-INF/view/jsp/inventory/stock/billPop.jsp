<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="modal prodModal" data-backdrop="static"
	data-keyboard="false" tabindex="-1" aria-hidden="false"
	style="display: block;" role="dialog">
	<div class="prodModal-dialogue">
		<div class="modal-content"
			style="max-height: 500px; min-width: 200%; overflow: scroll;">
			<div class="modal-header">
				<h4 class="modal-title">Bill - ( ${bill.billCode } )<span
					class="back-btn"><i class="fa fa-times" onclick="javascript:$('#pdtPop').hide()"></i></span></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Vendor
							</label>
							<div class="col-sm-9">
								<label>${bill.vendor }</label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Account
							</label>
							<div class="col-sm-9">
								<label>${bill.account }</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Date
							</label>
							<div class="col-sm-9">
								<label>${bill.billDate }</label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Status
							</label>
							<div class="col-sm-9">
								<c:choose>
									<c:when test="${bill.isSettled }">
										<label>Settled</label>
									</c:when>
									<c:otherwise>
										<label>Pending</label>
									</c:otherwise>
								</c:choose>

							</div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Payment
								Term </label>
							<div class="col-sm-9">
								<label>${bill.paymentTerm }</label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Currency
							</label>
							<div class="col-sm-9">
								<label>${bill.currecny }</label>
							</div>
						</div>
					</div>
				</div>

				<c:if test="${! empty bill.productVos  }">
					<div class="row">
						<div class="panel-body">
							<div class="col-sm-12">
								<table cellpadding="0" cellspacing="0" border="0"
									class="table table-striped table-bordered erp-tbl prdct_tbl">
									<thead>
										<tr>
											<th width="10%">Product Code</th>
											<th width="20%">Item</th>
											<th width="10%">Quantity</th>
											<th width="10%">Unit Price</th>
											<th width="10%">Discount %</th>
											<th width="10%">Net Amount</th>
											<th width="10%">Tax</th>
											<th width="10%">Total</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${bill.productVos }" var="p">
											<tr>
												<td><label>${p.productCode }</label></td>
												<td><label>${p.productName }</label></td>
												<td><label>${p.quantity }</label></td>
												<td><label>${p.unitPrice }</label></td>
												<td><label>${p.discount }</label></td>
												<td><label>${p.netAmount }</label></td>
												<td><label> <c:choose>
															<c:when test="${! empty p.tax }">
																	${p.tax }</c:when>
															<c:otherwise>
																<i>--</i>
															</c:otherwise>
														</c:choose>
												</label></td>
												<td><label>${p.netAmount }</label></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</c:if>
				<c:if test="${! empty bill.chargeVos }">
					<div class="row">
						<div class="panel-body">
							<div class="col-sm-12">
								<table cellpadding="0" cellspacing="0" border="0"
									class="table table-striped table-bordered erp-tbl prdct_tbl">
									<thead>
										<tr>
											<th width="10%">Charge</th>
											<th width="20%">Amount</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${bill.chargeVos }" var="p">
											<tr>
												<td><label>${p.charge }</label></td>
												<td><label>${p.amount }</label></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</c:if>
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-4">
						<div class="form-group">
							<div class="col-sm-6">
								<label>Sub Total</label>
							</div>
							<div class="col-sm-6">
								<label id="subTotal">${bill. subTotal}</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<label>Discount Rate</label>
							</div>
							<div class="col-sm-6">
								<label id="subTotal">${bill. discountRate}</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<label>Discount Amount</label>
							</div>
							<div class="col-sm-6">
								<label id="subTotal">${bill. discountTotal}</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<label>Gross Total</label>
							</div>
							<div class="col-sm-6">
								<label id="subTotal">${bill. netTotal}</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<label>Tax: </label>
							</div>
							<div class="col-sm-6">
								<c:choose>
									<c:when test="${! empty bill.taxes }">
										<c:forEach items="${bill.taxes }" var="t">
											<label>${t }</label>
											<br>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<label><i>None</i></label>
									</c:otherwise>
								</c:choose>

							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<label>Tax Total</label>
							</div>
							<div class="col-sm-6">
								<label id="subTotal">${bill. taxTotal}</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<label>Grand Total</label>
							</div>
							<div class="col-sm-6">
								<label id="subTotal">${bill. grandTotal}</label>
							</div>
						</div>
					</div>
				</div>

				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered erp-tbl instllmnt_tbl"
					id="">
					<thead>
						<tr>
							<th width="10%">Date</th>
							<th width="10%">Installment</th>
							<th width="20%">Amount</th>
							<th width="20%">Status</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${bill.installmentVos }" var="inst">
							<tr>
								<td><label>${inst.date }</label></td>
								<td><label>${inst.percentage }</label></td>
								<td><label><fmt:formatNumber type="number"
											pattern="0.00" maxFractionDigits="2" value="${inst.amount }" /></label></td>
								<td><c:choose>
										<c:when test="${inst.isPaid }">
											<label>Paid</label>
										</c:when>
										<c:otherwise>
											<label>Pending</label>
										</c:otherwise>
									</c:choose></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>