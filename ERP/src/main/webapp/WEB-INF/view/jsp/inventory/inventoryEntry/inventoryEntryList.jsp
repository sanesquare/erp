<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="5%">#</th>
			<th width="10%">Code</th>
			<th width="10%">Godown</th>
			<th width="10%">Type</th>
			<th width="10%">Date</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${entries }" var="e" varStatus="s">
			<tr>
				<td><a href="viewInventoryEntry.do?id=${e.inventoryEntryId }">${s.count }</a></td>
				<td><a href="viewInventoryEntry.do?id=${e.inventoryEntryId }">${e.code }</a></td>
				<td><a href="viewInventoryEntry.do?id=${e.inventoryEntryId }">${e.godown }</a></td>
				<td><a href="viewInventoryEntry.do?id=${e.inventoryEntryId }"><i>${e.type }</i></a></td>
				<td><a href="viewInventoryEntry.do?id=${e.inventoryEntryId }">${e.date }</a></td>
				<td><c:choose>
						<c:when test="${e.editable }">
							<i onclick="deleteThisIE('${e.inventoryEntryId}')"
								class="fa fa-times"></i>
						</c:when>
						<c:otherwise>
							<i>Can't Delete</i>
						</c:otherwise>
					</c:choose></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span id="close-msg" onclick="closeMessage()"><i
			class="fa fa-times"></i></span>
	</div>
</c:if>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>