<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<form:form action="convertDeliveryNotToInvoice.do" method="POST"
	commandName="invoiceVo">
	<table cellpadding="0" cellspacing="0" border="0"
		class="table table-striped table-bordered erp-tbl"
		id="basic-datatable">
		<thead>
			<tr>
				<th width="10%">#</th>
				<th width="10%">Code</th>
				<th width="10%">Customer</th>
				<th width="10%">Date</th>
				<th width="10%">Convert</th>
				<th width="10%">Delete</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${notes }" var="r" varStatus="s">
				<tr>
					<td><a href="viewDeliveryNote.do?id=${r.noteId}">
							${s.count}</a></td>
					<td><a href="viewDeliveryNote.do?id=${r.noteId}">
							${r.noteCode }</a></td>
					<td><a href="viewDeliveryNote.do?id=${r.noteId}">
							${r.customer }</a></td>
					<td><a href="viewDeliveryNote.do?id=${r.noteId}"> ${r.date }</a></td>
					<td><c:choose>
							<c:when test="${r.isConverted }">
								<a href="viewInvoice.do?id=${r.invoiceId }"><i class="badge">${r.invoiceCode }</i></a>
							</c:when>
							<c:otherwise>
								<form:checkbox path="deliveryNoteIds" value="${r.noteId}" />
							</c:otherwise>
						</c:choose></td>
					<td><c:choose>
							<c:when test="${r.isConverted }">
								<i>Can't Delete</i>
							</c:when>
							<c:otherwise>
								<i class="fa fa-times"
									onclick="deleteDeliveryNOte('${r.noteId}')"></i>
							</c:otherwise>
						</c:choose></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<button type="submit" class="btn btn-primary erp-btn"
		style="margin-left: 50%">
		<i class="fa fa-arrow-right"></i> Convert to Invoice
	</button>
</form:form>
<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
</c:if>

<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>