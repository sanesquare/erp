<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="modal prodModal" data-backdrop="static"
	data-keyboard="false" tabindex="-1" aria-hidden="false"
	style="display: block;" role="dialog">
	<div class="prodModal-dialogue">
		<div class="modal-content"
			style="max-height: 500px; min-width: 200%; overflow: scroll;">
			<div class="modal-header">
				<h4 class="modal-title">Inventory Entry ${vo.code }<span class="back-btn"><i
					class="fa fa-times" onclick="javascript:$('#pdtPop').hide()"></i></span></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Date
							</label>
							<div class="col-sm-9">
								<label>${vo.date }</label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Narration
							</label>
							<div class="col-sm-9">
								<label>${vo.narration }</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Godown
							</label>
							<div class="col-sm-9">
								<label>${vo.godown }</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel-body">
						<div class="col-sm-12">
							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered erp-tbl prdct_tbl">
								<thead>
									<tr>
										<th width="10%">Product Code</th>
										<th width="10%">Item</th>
										<th width="10%">Quantity</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${vo.itemsVos }" var="p">
										<tr class="productTableRow" id="${p.productCode }">
											<td style="padding: .5%;"><label
												id="prod_code${p.productCode }">${p.productCode }</label></td>
											<td style="padding: .5%;"><label
												id="prod_name${p.productCode }">${p.productName }</label></td>
											<td style="padding: .5%;"><label>${p.quantity }</label>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>