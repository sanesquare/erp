<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="load-erp">
	<div id="spinneeer-erp"></div>
</div>
<div id="newPop"></div>
<c:if test="${show }">
	<span onclick="getDetailedViewForPrevious()"
		class="fa fa-arrow-left back"></span>
</c:if>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl stck-tbl">
	<thead>
		<tr>
			<td colspan="7">Stock Item: <b>${stock.name }</b>
			</td>
		</tr>
		<tr>
			<th width="55%" rowspan="2">Particulars</th>
			<th width="15%" style="text-align: center;" rowspan="1" colspan="2">Inwards</th>
			<th width="15%" style="text-align: center;" rowspan="1" colspan="2">Outwards</th>
			<th width="15%" style="text-align: center;" rowspan="1" colspan="2">Closing</th>
		</tr>
		<tr>
			<th>Quantity</th>
			<th>Value</th>
			<th>Quantity</th>
			<th>Value</th>
			<th>Quantity</th>
			<th>Value</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${stock.monthlyVos }" var="s">
			<tr>
				<td><c:if test="${s.date != 'Opening Balance' }">
						<a
							onclick="getDetailedMonthwiseViewForPrevious('${s.hashCode }','${stock.hashCode }')"></c:if>${s.date }</a>
					</td>
				<td><c:if test="${s.inwardsQuantity != zero }"> ${s.inwardsQuantity } <i>${s.unit }</i>
					</c:if></td>
				<td><c:if test="${s.inwardsValue != zero }">
						<b>${s.inwardsValue }</b>
					</c:if></td>
				<td><c:if test="${s.outwardsQuantity != zero }">${s.outwardsQuantity } <i>${s.unit }</i>
					</c:if></td>
				<td><c:if test="${s.outwardsValue != zero }">
						<b>${s.outwardsValue }</b>
					</c:if></td>
				<td><c:if test="${s.closingQuantity != zero }">${s.closingQuantity } <i>${s.unit }</i>
					</c:if></td>
				<td><c:if test="${s.closingValue != zero }">
						<b>${s.closingValue }</b>
					</c:if></td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="1"><b>Totals:</b></th>
			<th><c:if test="${stock.inwardsQuantity != zero }">
					<b> ${stock.inwardsQuantity } <i>${stock.unit }</i></b>
				</c:if></th>
			<th><c:if test="${stock.inwardsValue != zero }">
					<b>${stock.inwardsValue }</b>
				</c:if></th>
			<th><c:if test="${stock.outwardsQuantity != zero }">
					<b> ${stock.outwardsQuantity } <i>${stock.unit }</i></b>
				</c:if></th>
			<th><c:if test="${stock.outwardsValue != zero }">
					<b> ${stock.outwardsValue }</b>
				</c:if></th>
			<th><c:if test="${stock.closingQuantity != zero }">
					<b> ${stock.closingQuantity } <i>${stock.unit }</i></b>
				</c:if></th>
			<th><c:if test="${stock.closingValue != zero }">
					<b> ${stock.closingValue }</b>
				</c:if></th>
		</tr>
	</tfoot>
</table>
<div id="pdtPop" style="display: none;"></div>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>