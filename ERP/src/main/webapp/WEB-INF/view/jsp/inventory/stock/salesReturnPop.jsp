<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="modal prodModal" data-backdrop="static"
	data-keyboard="false" tabindex="-1" aria-hidden="false"
	style="display: block;" role="dialog">
	<div class="prodModal-dialogue">
		<div class="modal-content"
			style="max-height: 500px; min-width: 200%; overflow: scroll;">
			<div class="modal-header">
				<h4 class="modal-title">Sales Return- ( ${invoice.code } )
				<span class="back-btn"><i class="fa fa-times"
					onclick="javascript:$('#pdtPop').hide()"></i></span></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Customer<span
								class="stars">*</span>
							</label>
							<div class="col-sm-9">
								<label>${invoice.customer }</label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Account<span
								class="stars">*</span>
							</label>
							<div class="col-sm-9">
								<label>${invoice.account }</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Date<span
								class="stars">*</span>
							</label>
							<div class="col-sm-9">
								<label>${date }</label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Status<span
								class="stars">*</span>
							</label>
							<div class="col-sm-9">
								<c:choose>
									<c:when test="${invoice.isSettled }">
										<label>Settled</label>
									</c:when>
									<c:otherwise>
										<label>Pending</label>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Currency<span
								class="stars">*</span>
							</label>
							<div class="col-sm-9">
								<label>${invoice.currecny }</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel-body">
						<div class="col-sm-12">
							<table cellpadding="0" cellspacing="0" border="0"
								class="table table-striped table-bordered erp-tbl prdct_tbl">
								<thead>
									<tr>
										<th width="10%">Product Code</th>
										<th width="20%">Item</th>
										<th width="10%">Quantity</th>
										<th width="10%">Unit Price</th>
										<th width="10%">Total</th>
										<th width="10%">Godown</th>
										<th width="10%">Delete</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${invoice.productVos }" var="p">
										<tr class="productTableRow" id="${p.productCode }">
											<td style="padding: .5%;"><label
												id="prod_code${p.productCode }">${p.productCode }</label></td>
											<td style="padding: .5%;"><label
												id="prod_name${p.productCode }">${p.productName }</label></td>
											<td style="padding: .5%;"><label>${p.quantity }</label> </td>
											<td style="padding: .5%;"><label>${p.unitPrice }</label> </td>
											<td style="padding: .5%;"><label
												id="product_net${p.productCode }">${p.total }</label></td>
											<td style="padding: .5%;"><label>${p.godown }</label><input
												type="hidden" value="${p.godownId }"
												id="prod_godwn_id${p.productCode }"></td>
											<td style="padding: .5%;"><i
												onclick="javascript:deleteProductRow(this)"
												class="fa fa-times"></i></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>