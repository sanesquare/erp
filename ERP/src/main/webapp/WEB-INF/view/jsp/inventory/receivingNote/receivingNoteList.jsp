<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<form:form action="convertReceivingNoteToBill.do" method="POST"
	commandName="billVo">
	<table cellpadding="0" cellspacing="0" border="0"
		class="table table-striped table-bordered erp-tbl"
		id="basic-datatable">
		<thead>
			<tr>
				<th width="10%">#</th>
				<th width="10%">Code</th>
				<th width="10%">Vendor</th>
				<th width="10%">Purchase Order</th>
				<th width="10%">Date</th>
				<th width="10%">Convert</th>
				<th width="10%">Delete</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${notes }" var="r" varStatus="s">
				<tr>
					<td><a href="viewReceivingNote.do?id=${r.noteId}">
							${s.count}</a></td>
					<td><a href="viewReceivingNote.do?id=${r.noteId}">
							${r.noteCode }</a></td>
					<td><a href="viewReceivingNote.do?id=${r.noteId}">
							${r.vendor }</a></td>
					<td><c:choose>
							<c:when test="${! empty r.orderCode }"></c:when>
							<c:otherwise>
								<i>None</i>
							</c:otherwise>
						</c:choose> <a href="viewReceivingNote.do?id=${r.noteId}"> ${r.orderCode }</a></td>
					<td><a href="viewReceivingNote.do?id=${r.noteId}">
							${r.date }</a></td>
					<td><c:choose>
							<c:when test="${r.isConverted }">
								<a href="viewBill.do?id=${r.billId }"><i class="badge">${r.billCode }</i></a>
							</c:when>
							<c:otherwise>
								<form:checkbox path="receivingNoteIds" value="${r.noteId}" />
							</c:otherwise>
						</c:choose></td>
					<td><c:choose>
							<c:when test="${r.isConverted }">
								<i>Can't Delete</i>
							</c:when>
							<c:otherwise>
								<i class="fa fa-times"
									onclick="deleteReceivingNote('${r.noteId}')"></i>
							</c:otherwise>
						</c:choose></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<button type="submit" class="btn btn-primary erp-btn"
		style="margin-left: 50%">
		<i class="fa fa-arrow-right"></i> Convert to Bill
	</button>
</form:form>
<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
</c:if>

<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>