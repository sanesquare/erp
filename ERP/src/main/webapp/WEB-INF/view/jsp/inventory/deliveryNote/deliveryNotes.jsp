<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}
</style>
</head>
<body>
	<div class="inventory_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">Delivery Notes</div>
						<div class="panel-body">
							<div id="panelBody">
								<form:form action="convertDeliveryNotToInvoice.do" method="POST"
									commandName="invoiceVo">
									<table cellpadding="0" cellspacing="0" border="0"
										class="table table-striped table-bordered erp-tbl"
										id="basic-datatable">
										<thead>
											<tr>
												<th width="10%">#</th>
												<th width="10%">Code</th>
												<th width="10%">Customer</th>
												<th width="10%">Date</th>
												<th width="10%">Convert</th>
												<th width="10%">Delete</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${notes }" var="r" varStatus="s">
												<tr>
													<td><a href="viewDeliveryNote.do?id=${r.noteId}">
															${s.count}</a></td>
													<td><a href="viewDeliveryNote.do?id=${r.noteId}">
															${r.noteCode }</a></td>
													<td><a href="viewDeliveryNote.do?id=${r.noteId}">
															${r.customer }</a></td>
													<td><a href="viewDeliveryNote.do?id=${r.noteId}">
															${r.date }</a></td>
													<td><c:choose>
															<c:when test="${r.isConverted }">
																<a href="viewInvoice.do?id=${r.invoiceId }"><i
																	class="badge">${r.invoiceCode }</i></a>
															</c:when>
															<c:otherwise>
																<form:checkbox path="deliveryNoteIds"
																	value="${r.noteId}" />
															</c:otherwise>
														</c:choose></td>
													<td><c:choose>
															<c:when test="${r.isConverted }">
																<i>Can't Delete</i>
															</c:when>
															<c:otherwise>
																<i class="fa fa-times"
																	onclick="deleteDeliveryNOte('${r.noteId}')"></i>
															</c:otherwise>
														</c:choose></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<button type="submit" class="btn btn-primary erp-btn"
										style="margin-left: 50%">
										<i class="fa fa-arrow-right"></i> Convert to Invoice
									</button>
								</form:form>
							</div>
							<a href="addDeliveryNote.do">
								<button type="button" class="btn btn-primary erp-btn">New
									Delivery Note</button>
							</a>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="confirm-delete" style="display: none;">
			<div class="modal " style="display: block;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Confirmation</h4>
						</div>
						<div class="modal-body">Do you really want to delete this
							item?</div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<input type="hidden" id="delete-item">
								<button type="button" onclick="cancel()" class="btn btn-default">No</button>
								<button type="button" onclick="deleteItem()"
									class="btn btn-primary">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<c:if test="${! empty msg }">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</c:if>
		<c:if test="${! empty error_msg }">
			<div class="notification-panl er-msg">
				<div class="notfctn-cntnnt">${error_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</c:if>

		<div id="success-msg">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${success_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</div>

		<div id="erro-msg">
			<div class="notification-panl er-msg">
				<div class="notfctn-cntnnt">${error_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</div>

		<div id="load-erp">
			<div id="spinneeer-erp">
				<span class="loading-text-erp">Loading.. </span><span
					class="spinr-erp"><i class="fa fa-spinner fa-spin "></i></span>
			</div>
		</div>

		<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
		<script
			src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
		<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
		<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
		<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
		<script type="text/javascript">
			
		</script>
</body>
</html>