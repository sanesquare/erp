<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">

			<div class="row">
				<c:choose>
					<c:when test="${note.isConverted}">
						<div class="panel-body">
							<div class="panel panel-default erp-panle">
								<div class="panel-heading  panel-inf">
									Delivery Note- ( ${note.noteCode } )<a href="deliveryNotes.do"><span
										class="back-btn"><i class="fa fa-arrow-left"></i></span></a>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Customer
												</label>
												<div class="col-sm-9">
													<label>${note.customer }</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Date
												</label>
												<div class="col-sm-9">
													<label>${note.date }</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="panel-body">
											<div class="col-sm-12">
												<c:if test="${! empty note.quoteCode }">
													<a href="viewCustomerQuote.do?id=${note.orderId }"><span
														class="badge">${note.quoteCode }</span></a>
												</c:if>
												<c:if test="${! empty note.orderCode }">
													<a style="margin-left: 1%;"
														href="viewSalesOrder.do?id=${note.orderId }"><span
														class="badge"> ${note.orderCode }</span></a>
												</c:if>
												<c:if test="${! empty note.invoiceId }">
													<a style="margin-left: 1%;"
														href="viewInvoice.do?id=${note.invoiceId }"><span
														class="badge">${note.invoiceCode }</span></a>
												</c:if>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="panel-body tbl_scrl_bdy">
											<div class="col-sm-12">
												<table cellpadding="0" cellspacing="0" border="0"
													class="table table-striped table-bordered erp-tbl prdct_tbl">
													<thead>
														<tr>
															<th width="10%">Product Code</th>
															<th width="20%">Item</th>
															<th width="20%">Godown</th>
															<th width="10%">Quantity</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${note.productVos }" var="p">
															<tr class="productTableRow" id="${p.productCode }">
																<td style="padding: .5%;">${p.productCode }</td>
																<td style="padding: .5%;">${p.productName }</td>
																<td style="padding: .5%;">${p.godown }</td>
																<td style="padding: .5%;">${p.quantity }</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12">
											<div class="right">
												<a href="addReceivingNote.do">
													<button type="button" class="btn btn-primary erp-btn">New
														Receiving Note</button>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="panel-body">
							<div class="panel panel-default erp-panle">
								<div class="panel-heading  panel-inf">
									Delivery Notes ${note.noteCode }<a href="deliveryNotes.do"><span
										class="back-btn"><i class="fa fa-arrow-left"></i></span></a>
								</div>
								<div class="panel-body">
									<form:form action="#" method="POST" commandName="note">
										<form:hidden path="noteId" id="noteId" />
										<form:hidden path="invoiceId" id="invoiceId" />
										<form:hidden path="orderId" id="orderId" />
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Customer<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select class="form-control" id="bill_vendr"
															path="customerId">
															<option value="" label="--Select Customer--"></option>
															<c:forEach items="${customers }" var="v">
																<form:option value="${v.customerId }" label="${v.name }"></form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Date<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class='input-group date billDate' id="datepicker">
															<form:input type="text" readonly="true" path="date"
																id="bill-date" value="${date }"
																style="background-color: #ffffff !important;"
																data-date-format="DD/MM/YYYY" class="form-control" />
															<span class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="panel-body tbl_scrl_bdy">
												<div class="col-sm-12">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl prdct_tbl">
														<thead>
															<tr>
																<th width="10%">Product Code</th>
																<th width="10%">Item</th>
																<th width="10%">Quantity</th>
																<th width="15%">Godown</th>
																<th width="5%">Delete</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${note.productVos }" var="p">
																<tr class="productTableRow" id="${p.productCode }">
																	<td style="padding: .5%;"><label
																		id="prod_code${p.productCode }">${p.productCode }</label>
																		<input type="hidden" class="prodctId"
																		value="${p.productId }"></td>
																	<td style="padding: .5%;"><label
																		id="prod_name${p.productCode }">${p.productName }</label></td>
																	<td style="padding: .5%;"><c:choose>
																			<c:when test="${! empty note.orderCode }">
																				<input type="text"
																					onblur="checkQuantity('${p.productCode }')"
																					value="${p.quantity }" class="form-control double "
																					id="product_qty${p.productCode }" />
																				<input type="hidden" value="${p.quantityToDeliver }"
																					id="product_qty_hid${p.productCode }" />
																			</c:when>
																			<c:otherwise>
																				<input type="text" value="${p.quantity }"
																					class="form-control double "
																					id="product_qty${p.productCode }" />
																				<input type="hidden" value="${p.quantity }"
																					id="product_qty_hid${p.productCode }" />
																			</c:otherwise>
																		</c:choose></td>
																	<td style="padding: .5%;"><select
																		class="form-control"
																		id="product_godwn${p.productCode }">
																			<c:forEach items="${godown }" var="g">
																				<c:choose>
																					<c:when test="${p.godownId eq g.godownId }">
																						<option value="${g.godownId }" selected="selected"
																							label="${g.name }"></option>
																					</c:when>
																					<c:otherwise>
																						<option value="${g.godownId }" label="${g.name }"></option>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																	</select></td>
																	<td style="padding: .5%;"><i
																		onclick="javascript:deleteProductRow(this)"
																		class="fa fa-times"></i></td>
																</tr>
															</c:forEach>
															<c:if test="${! note.isFromInvoice  }">
																<tr>
																	<td colspan="10"><button type="button"
																			onclick="getProductDetails()"
																			class="btn btn-primary erp-btn right">Add
																			Item</button></td>
																</tr>
															</c:if>
														</tbody>
													</table>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<button type="button" onclick="saveDeliveryNote()"
														class="btn btn-primary erp-btn ">Save</button>


												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="right">
													<a href="addDeliveryNote.do">
														<button type="button" class="btn btn-primary erp-btn">New
															Delivery Note</button>
													</a>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>

		<div id="pdtPop" style="display: none;"></div>

		<div id="errror-msg" style="display: none;">
			<div class="modal " data-backdrop="static" data-keyboard="false"
				tabindex="-1" aria-hidden="false" style="display: block;"
				role="dialog">
				<div class="modal-dialog ">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Alert</h4>
						</div>
						<div class="modal-body"></div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<button type="button" onclick="closeError()"
									class="btn btn-default">Ok</button>
								<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="confirm-delete" style="display: none;">
			<div class="modal " style="display: block;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Confirmation</h4>
						</div>
						<div class="modal-body">Do you want to convert this
							installment to payment?</div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<input type="hidden" id="delete-item">
								<button type="button" onclick="cancel()" class="btn btn-default">No</button>
								<button type="button" onclick="confirmedBillPayment()"
									class="btn btn-primary">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
	<%-- <c:if test="${! empty success_msg }"> --%>
	<div class="notification-panl successMsg" style="display: none">
		<div class="notfctn-cntnnt">${success_msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
	<%-- </c:if> --%>
	<c:if test="${! empty msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${msg }</div>
			<span id="close-msg" onclick="closeMessage()"><i
				class="fa fa-times"></i></span>
		</div>
	</c:if>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script type="text/javascript">
		
	</script>
	<%-- <script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script> --%>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<script
		src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts-validation.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<script type="text/javascript">
		
	</script>
</body>
</html>