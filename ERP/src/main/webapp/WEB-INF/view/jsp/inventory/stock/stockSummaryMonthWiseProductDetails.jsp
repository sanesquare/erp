<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="load-erp">
	<div id="spinneeer-erp"></div>
</div>
<div id="newPop"></div>
<c:if test="${show }">
	<span onclick="getDetailedViewForPrevious()"
		class="fa fa-arrow-left back"></span>
</c:if>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl stck-tbl">
	<thead>
		<tr>
			<td colspan="14">Stock Item: <b>${stock.name }</b> <span
				style="float: right;"><b>${stock.date }</b></span>
			</td>
		</tr>
		<tr>
			<th width="10%" rowspan="2">Date</th>
			<th width="20%" rowspan="2">Particulars</th>
			<th width="15%" rowspan="2">Voucher Type</th>
			<th width="10%" rowspan="2">Voucher No.</th>
			<th width="5%" style="text-align: center;" rowspan="2">To Be
				Issued</th>
			<th width="15%" style="text-align: center;" rowspan="1" colspan="3">Inwards</th>
			<th width="15%" style="text-align: center;" rowspan="1" colspan="3">Outwards</th>
			<th width="15%" style="text-align: center;" rowspan="1" colspan="3">Closing</th>
		</tr>
		<tr>
			<th>Quantity</th>
			<th>Rate</th>
			<th>Value</th>
			<th>Quantity</th>
			<th>Rate</th>
			<th>Value</th>
			<th>Quantity</th>
			<th>Rate</th>
			<th>Value</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td><b>Opening Balance</b></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>${stock.openingQuantity }&nbsp;<i>${stock.unit }</i></td>
			<td>${stock.openingRate }</td>
			<td><b>${stock.openingValue }</b></td>
		</tr>
		<c:forEach items="${stock.productDetailedVos }" var="s">
			<tr>
				<td><a
					onclick="loadVoucherForStock('${s.voucher }','${s.voucherId }')">${s.date }
				</a></td>
				<td><b>${s.ledger }</b></td>
				<td>${s.voucher }</td>
				<td>${s.voucherNumber }</td>
				<td><c:if test="${s.toBeIssuedQuantity != zero }"> ${s.toBeIssuedQuantity } 
					</c:if></td>
				<td><c:if test="${s.inwardsQuantity != zero }"> ${s.inwardsQuantity } <i>${s.unit }</i>
					</c:if></td>
				<td><c:if test="${s.inwardsRate != zero }">${s.inwardsRate }</c:if></td>
				<td><c:if test="${s.inwardsValue != zero }">
						<b>${s.inwardsValue }</b>
					</c:if></td>
				<td><c:if test="${s.outwardsQuantity != zero }">${s.outwardsQuantity } <i>${s.unit }</i>
					</c:if></td>
				<td><c:if test="${s.outwardsRate != zero }">${s.outwardsRate }</c:if></td>
				<td><c:if test="${s.outwardsValue != zero }">
						<b>${s.outwardsValue }</b>
					</c:if></td>
				<td><c:if test="${s.closingQuantity != zero }">${s.closingQuantity } <i>${s.unit }</i>
					</c:if></td>
				<td><c:if test="${s.closingRate != zero }">${s.closingRate }</c:if></td>
				<td><c:if test="${s.closingValue != zero }">
						<b>${s.closingValue }</b>
					</c:if></td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="5"><b>Totals:</b></th>
			<th><c:if test="${stock.inwardsQuantity != zero }">
					<b> ${stock.inwardsQuantity } <i>${stock.unit }</i></b>
				</c:if></th>
			<th><c:if test="${stock.inwardsRate != zero }">
					<b> ${stock.inwardsRate }</b>
				</c:if></th>
			<th><c:if test="${stock.inwardsValue != zero }">
					<b>${stock.inwardsValue }</b>
				</c:if></th>
			<th><c:if test="${stock.outwardsQuantity != zero }">
					<b> ${stock.outwardsQuantity } <i>${stock.unit }</i></b>
				</c:if></th>
			<th><c:if test="${stock.outwardsRate != zero }">
					<b> ${stock.outwardsRate }</b>
				</c:if></th>
			<th><c:if test="${stock.outwardsValue != zero }">
					<b> ${stock.outwardsValue }</b>
				</c:if></th>
			<th><c:if test="${stock.closingQuantity != zero }">
					<b> ${stock.closingQuantity } <i>${stock.unit }</i></b>
				</c:if></th>
			<th><c:if test="${stock.closingRate != zero }">
					<b> ${stock.closingRate }</b>
				</c:if></th>
			<th><c:if test="${stock.closingValue != zero }">
					<b> ${stock.closingValue }</b>
				</c:if></th>
		</tr>
	</tfoot>
</table>
<div id="pdtPop" style="display: none;"></div>
<script
	src="<c:url value='/resources/assets/inventory/js/inventory.js' />"></script>