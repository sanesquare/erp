<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html >
<html>
<head>
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />
</head>
<body>
	<div class="logo_strip">
		<div class="col-md-3 col-md-offset-1">
			<div class="logo_imgs">
				<img src="resources/assets/images/logo1.png">
			</div>
		</div>
	</div>
	<div class="containers">
		<div class="row">
			<div class="col-md-12 padding_ers">
				<h1 class="text-center text-white">Error 401</h1>
				<h3 class="text-center text-white">Authorization Required.</h3>
				<p class="text-center text-white">
					<br>This server could not verify that you are authorized to<br>
					access the document requested. <br>Either you supplied the
					wrong credentials( e.g., bad password) or your<br> browser
					doesn't understand how to supply the credentials required.
				</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<p class="text-center">
				<br> <br> Go back to <a href="#"><strong>Home
						page</strong></a>
			</p>
		</div>
	</div>
</body>
</html>