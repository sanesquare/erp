<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="modal prodModal" data-backdrop="static"
	data-keyboard="false" tabindex="-1" aria-hidden="false"
	style="display: block;" role="dialog">
	<div class="prodModal-dialogue">
		<div class="modal-content" style="min-width: 200%;">
			<div class="modal-header">
				<h4 class="modal-title">Charges</h4>
			</div>
			<div class="modal-body">
				<form action="#">
					<table cellpadding="0" cellspacing="0" border="0"
						class="table  table-bordered erp-tbl chrgTbl" id="basic-datatables">
						<thead>
							<tr>
								<th width="10%">Charge</th>
								<th width="10%">Amount</th>
								<th width="10%">Select</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${charges }" var="p">
								<tr class="pdtRow">
									<td>${p.name }</td>
									<td><input type="text" value="${p.amount }"
										class="double td_txt"
										onblur="addQtyToId('${p.chargeId }',this.value)" /></td>
									<td><c:choose>
											<c:when test="${p.selected }">
												<input type="checkbox" checked="checked" class="${p.chargeId }"
													name="charge" value="${p.chargeId }!!!${p.amount }" />
											</c:when>
											<c:otherwise>
												<input type="checkbox" name="charge" class="${p.chargeId }"
													value="${p.chargeId }" />
											</c:otherwise>
										</c:choose></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</form>
			</div>

			<div class="modal-footer clearfix">
				<div class="btn-toolbar pull-right">
					<button type="button" onclick="selectCharges()"
						class="btn btn-default">Ok</button>
					<button type="button" onclick="cancelChargePOp()"
						class="btn btn-default">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>