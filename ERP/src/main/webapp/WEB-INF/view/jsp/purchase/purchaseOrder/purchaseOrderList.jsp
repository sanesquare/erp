<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">Code</th>
			<th width="10%">Vendor</th>
			<th width="10%">Receiving Note</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${orders }" var="po">
			<tr>
				<%-- <c:choose>
					<c:when test="${! empty po.noteCode }">
						<td><a href="viewPurchaseOrder.do?id=${po.quoteId }">
								${po.orderCode }</a></td>
						<td>${po.vendor }</td>
						<td><a href="viewReceivingNote.do?id=${po.noteId}"><span
								class="badge ">${po.noteCode }</span></a></td>
						<td><i>Received</i></td>
					</c:when>
					<c:otherwise>
						<td><a href="viewPurchaseOrder.do?id=${po.quoteId }">

								${po.orderCode }</a></td>
						<td>${po.vendor }</td>
						<td><a
							href="purchaseOrderToReceivingNote.do?id=${po.quoteId}"><button
									class="btn ">Receiving Note</button></a></td>
						<td><i class="fa fa-times"
							onclick="deletePurchaseOrder('${po.quoteId}')"></i></td>
					</c:otherwise>
				</c:choose> --%>

				<td><a href="viewPurchaseOrder.do?id=${po.quoteId }">
						${po.orderCode }</a></td>
				<td>${po.vendor }</td>
				<td><c:forEach items="${po.receivingNoteIdCode}" var="r">
						<a href="viewReceivingNote.do?id=${r.key}"><span
							class="badge ">${r.value }</span></a>
					</c:forEach> <c:if test="${! po.hasReceived }">
						<a href="purchaseOrderToReceivingNote.do?id=${po.quoteId}"><button
								class="btn-sm btn-primary right">Create</button></a>
					</c:if></td>
				<td><c:choose>
						<c:when test="${ po.hasReceived }">
							<i>Completely Received</i>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${! empty po.receivingNoteIdCode}">
									<i>Cannot Delete</i>
								</c:when>
								<c:otherwise>
									<i class="fa fa-times"
										onclick="deletePurchaseOrder('${po.quoteId}')"></i>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
</c:if>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>