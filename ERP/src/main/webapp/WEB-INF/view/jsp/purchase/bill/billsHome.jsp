<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}
</style>
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">Bills</div>
						<div class="panel-body">
							<div id="bill_panelBody">
								<table cellpadding="0" cellspacing="0" border="0"
									class="table table-striped table-bordered erp-tbl"
									id="basic-datatable">
									<thead>
										<tr>
											<th width="10%">Code</th>
											<th width="10%">Date</th>
											<th width="10%">Vendor</th>
											<th width="10%">Amount</th>
											<th width="10%">Outstanding</th>
											<th width="10%">Status</th>
											<th width="10%">Return</th>
											<th width="5%">Delete</th>

										</tr>
									</thead>
									<tbody>
										<c:forEach items="${bills }" var="b">
											<tr>
												<td><a href="viewBill.do?id=${b.billId }">${b.billCode }</a></td>
												<td><a href="viewBill.do?id=${b.billId }">${b.billDate }</a></td>
												<td><a href="viewBill.do?id=${b.billId }">${b.vendor }</a></td>
												<td><a href="viewBill.do?id=${b.billId }">${b.grandTotal }</a></td>
												<td><a href="viewBill.do?id=${b.billId }">${b.outStanding }</a></td>
												<td><c:choose>
														<c:when test="${b.isSettled }">
															<a href="viewBill.do?id=${b.billId }">Settled</a>
														</c:when>
														<c:otherwise>
															<a href="viewBill.do?id=${b.billId }">Pending</a>
														</c:otherwise>
													</c:choose></td>
												<td><c:choose>
														<c:when test="${b.isReturned }">
															<a href="viewBill.do?id=${b.billId }"><i>Returned</i>
															</a>
														</c:when>
														<c:otherwise>
															<a href="returnThisBill.do?billId=${b.billId }">Return</a>
														</c:otherwise>
													</c:choose></td>
												<td><a onclick="deleteThisBill('${b.billId }')"><i
														class="fa fa-times"></i></a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- <a href="addBill.do">
								<button type="button" class="btn btn-primary erp-btn">New
									Bill</button>
							</a> -->

						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="confirm-delete" style="display: none;">
			<div class="modal " style="display: block;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Confirmation</h4>
						</div>
						<div class="modal-body">Do you really want to delete this
							item?</div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<input type="hidden" id="delete-item">
								<button type="button" onclick="cancel()" class="btn btn-default">No</button>
								<button type="button" onclick="deleteItem()"
									class="btn btn-primary">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>




		<c:if test="${! empty success_msg }">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${success_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</c:if>

		<div id="success-msg" style="display: none">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt"></div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</div>


		<div id="load-erp">
			<div id="spinneeer-erp">
				<span class="loading-text-erp">Loading.. </span><span
					class="spinr-erp"><i class="fa fa-spinner fa-spin "></i></span>
			</div>
		</div>

		<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
		<script
			src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
		<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
		<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
		<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
		<script type="text/javascript">
			
		</script>
</body>
</html>