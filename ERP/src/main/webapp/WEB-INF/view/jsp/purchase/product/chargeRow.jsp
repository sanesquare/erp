<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<thead>
	<tr>
		<th width="10%">Charges</th>
		<th width="20%">Amount</th>
		<th width="10%">Delete</th>
	</tr>
</thead>
<tbody>
	<c:forEach items="${charges }" var="p">
		<tr class="productTableRow" id="${p.chargeId }">
			<input type="hidden" class="chargeId" value="${p.chargeId }">
			<td style="padding: .5%;"><input type="hidden"
				id="billChargeId${p.chargeId }" value=""> <label>${p.name }</label></td>
			<td style="padding: .5%;"><input type="text"
				value="${p.amount }" onblur="findTotalAmounts()"
				class="form-control double " id="chrge_price${p.chargeId }" /></td>
			<td style="padding: .5%;"><i
				onclick="javascript:deleteChargeRow(this)" class="fa fa-times"></i></td>
		</tr>
	</c:forEach>
	<tr>
		<td colspan="8" style="padding: .5%;"><button type="button"
				onclick="getChargeDetails()" class="btn btn-primary erp-btn right">Add
				Charge</button></td>
	</tr>
</tbody>
<script
	src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>