<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">Code</th>
			<th width="10%">Date</th>
			<th width="10%">Vendor</th>
			<th width="10%">Amount</th>
			<th width="10%">Outstanding</th>
			<th width="10%">Status</th>
			<th width="10%">Return</th>
			<th width="5%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${bills }" var="b">
			<tr>
				<td><a href="viewBill.do?id=${b.billId }">${b.billCode }</a></td>
				<td><a href="viewBill.do?id=${b.billId }">${b.billDate }</a></td>
				<td><a href="viewBill.do?id=${b.billId }">${b.vendor }</a></td>
				<td><a href="viewBill.do?id=${b.billId }">${b.grandTotal }</a></td>
				<td><a href="viewBill.do?id=${b.billId }">${b.outStanding }</a></td>
				<td><c:choose>
						<c:when test="${b.isSettled }">
							<a href="viewBill.do?id=${b.billId }">Settled</a>
						</c:when>
						<c:otherwise>
							<a href="viewBill.do?id=${b.billId }">Pending</a>
						</c:otherwise>
					</c:choose></td>
				<td><c:choose>
						<c:when test="${b.isReturned }">
							<a href="viewBill.do?id=${b.billId }"><i>Returned</i> </a>
						</c:when>
						<c:otherwise>
							<a href="returnThisBill.do?billId=${b.billId }">Return</a>
						</c:otherwise>
					</c:choose></td>
				<td><a onclick="deleteThisBill('${b.billId }')"><i
						class="fa fa-times"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script
	src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
