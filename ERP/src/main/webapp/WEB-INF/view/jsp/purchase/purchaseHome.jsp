<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">
			<div class="noti" id="notifications"></div>

			<div class="page-header">
				<div class="hdr">
					<div class="home-div">
						<a href="erpHome.do"> <span class="home"><i
								class="fa fa-home"></i></span>
						</a>
					</div>
					<div class="hdr-div">
						<span class="hdr-ico"><i class="fa fa-shopping-cart"></i></span>
						Purchase <small>Let's get a quick overview...</small>
					</div>

				</div>
			</div>
			<div class="row"
				style="padding-top: 3%; margin-left: 4%; margin-right: 4%">
				<div class="col-md-3">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<a href="products.do">
						<div class="panel-body  ">

							<span class="dashboard-title ">Products</span> <span
								class="dashboard-icons "><i class="fa fa-cubes"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div>
						</a>
					</div> -->
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="products.do"> <span
								class="hover_effect dashboard-title">Products </span></a> </span> <i
							class="fa fa-cubes bg-purchase transit stats-icon"></i> <a
							href="products.do">
							<h3 class="transit dashboard-title">
								Products<br>
								<small class="text-green"></small>
							</h3>
						</a>
					</div>
				</div>

				<div class="col-md-3">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<a href="vendors.do">
						<div class="panel-body  ">
							<span class="dashboard-title ">Vendors</span> <span
								class="dashboard-icons "><i class="fa fa-users"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div>
						</a>
					</div> -->
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="vendors.do"> <span class="hover_effect dashboard-title">Vendors
							</span></a> </span> <i class="fa fa-users bg-purchase transit stats-icon"></i> <a
							href="vendors.do">
							<h3 class="transit dashboard-title">
								Vendors<br> <small class="text-green"></small>
							</h3>
						</a>
					</div>
				</div>

				<div class="col-md-3">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<div class="panel-body  ">

							<span class="dashboard-title ">Vendor Quotes</span> <span
								class="dashboard-icons "><i class="fa fa-pencil-square-o"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div>
					</div> -->

					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="vendorQuotesHome.do"> <span class="hover_effect dashboard-title">Vendor<br>Quotes
							</span></a> </span> <i class="fa fa-pencil-square-o bg-purchase transit stats-icon"></i> <a
							href="vendorQuotesHome.do">
							<h3 class="transit dashboard-title">
								Vendor<br>Quotes
								<small class="text-green"></small>
							</h3>
						</a>
					</div>
				</div>

				<div class="col-md-3">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<div class="panel-body  ">

							<span class="dashboard-title ">Purchase Order</span> <span
								class="dashboard-icons "><i class="fa fa-list-alt"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div>
					</div> -->
					
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="purchaseOrderHome.do"> <span class="hover_effect dashboard-title">Purchase<br>Order
							</span></a> </span> <i class="fa fa-list-alt bg-purchase transit stats-icon"></i> <a
							href="purchaseOrderHome.do">
							<h3 class="transit dashboard-title">
								Purchase<br>Order
								<small class="text-green"></small>
							</h3>
						</a>
					</div>
				</div>
			</div>

			<div class="row"
				style="padding-top: 3%; margin-left: 4%; margin-right: 4%">
				<div class="col-md-3">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<div class="panel-body  ">

							<span class="dashboard-title ">Bill</span> <span
								class="dashboard-icons "><i class="fa fa-inr"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div>
					</div>-->
					
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="billsHome.do"> <span class="hover_effect dashboard-title">Bill<br>
							</span></a> </span> <i class="fa fa-inr bg-purchase transit stats-icon"></i> <a
							href="billsHome.do">
							<h3 class="transit dashboard-title">
								Bill<br>
								<small class="text-green"></small>
							</h3>
						</a>
					</div>
				</div> 
				
				

			<!-- 	<div class="col-md-3">
					<div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<div class="panel-body  ">

							<span class="dashboard-title ">Credit Memo</span> <span
								class="dashboard-icons "><i class="fa fa-files-o"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div>
					</div>
					
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="#"> <span class="hover_effect dashboard-title">Credit<br>Memo
							</span></a> </span> <i class="fa fa-files-o bg-purchase transit stats-icon"></i> <a
							href="#">
							<h3 class="transit dashboard-title">
								Credit<br>Memo
								<small class="text-green"></small>
							</h3>
						</a>
					</div>
				</div> -->


				<div class="col-md-3">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<div class="panel-body  ">

							<span class="dashboard-title ">Purchase Return</span> <span
								class="dashboard-icons "><i class="fa fa-files-o"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div>
					</div> -->
					
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="purchaseReturnHome.do"> <span class="hover_effect dashboard-title">Purchase<br>Return
							</span></a> </span> <i class="fa fa-refresh bg-purchase transit stats-icon"></i> <a
							href="purchaseReturnHome.do">
							<h3 class="transit dashboard-title">
								Purchase<br>Return
								<small class="text-green"></small>
							</h3>
						</a>
					</div>
				</div>


			</div>
			<div class="row" style="padding-top: 3%;">
					<div class="col-md-4"></div>
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="settings">
							<span class=""><a href="purchaseSettings.do"><i
									class="fa fa-cog settings-ico"></i></a></span>

						</div>
					</div>
				</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		
	</script>
	<script>
		
	</script>
	<!-- Warper Ends Here (working area) -->
</body>
</html>