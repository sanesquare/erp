<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl instllmnt_tbl" id="">
	<thead>
		<tr>
			<th width="10%">Date</th>
			<th width="10%">Installment</th>
			<th width="20%">Amount</th>
			<th width="20%">Status <span class="right"><i id="spinr"
					class="fa fa-spinner "></i> </span>
			</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${paymentTerm }" var="inst">
			<tr id="${inst.id }">
				<td><input type="hidden" value="" id="instllmntId${inst.id }"/>
				<label id="instlmnt_date${inst.id }">${inst.date }</label></td>
				<td><label id="instlmnt_perc${inst.id }">${inst.percentage }</label></td>
				<td><label id="instlmnt_amount${inst.id }"><fmt:formatNumber type="number"  
							pattern="0.00" maxFractionDigits="2" value="${inst.amount }" /></label></td>
				<td><select class="form-control"  id="instlmnt_stts${inst.id }">
						<option value="true" label="Convert Installment To Payment">
						<option value="false" selected="selected" label="Pending">
				</select></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script
	src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>