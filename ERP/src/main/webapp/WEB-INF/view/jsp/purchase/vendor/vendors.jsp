<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}
</style>
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">Vendors</div>
						<div class="panel-body">
							<div id="panelBody">
								<table cellpadding="0" cellspacing="0" border="0"
									class="table table-striped table-bordered erp-tbl"
									id="basic-datatable">
									<thead>
										<tr>
											<th width="10%">Code</th>
											<th width="20%">Name</th>
											<th width="20%">Address</th>
											<th width="20%">Email</th>
											<th width="20%">Phone</th>
											<th width="5%">Edit</th>
											<th width="5%">Delete</th>

										</tr>
									</thead>
									<tbody>
										<c:forEach items="${vendors }" var="v">
											<tr>
												<td>${v.vendorCode }</td>
												<td>${v.name }</td>
												<td>${v.address }</td>
												<td>${v.email }</td>
												<td>${v.mobile }</td>
												<td><a href="editVendor.do?id=${v.vendorId }"><i
														class="fa fa-pencil"></i> </a></td>
												<td><a href="#?id=${v.vendorId }"
													onclick="cnfrmDelete('${v.vendorId }')"><i
														class="fa fa-times"></i></a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<a href="addVendors.do">
								<button type="button" class="btn btn-primary erp-btn">New
									Vendor</button>
							</a>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="confirm-delete">
			<div class="modal " data-backdrop="static" data-keyboard="false"
				tabindex="-1" aria-hidden="false" style="display: block;"
				role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Confirmation</h4>
						</div>
						<div class="modal-body">Do you really want to delete this
							Vendor?</div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<button type="button" onclick="deleteVendor('false')"
									class="btn btn-default">No</button>
								<button type="button" onclick="deleteVendor('true')"
									class="btn btn-primary">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<c:if test="${! empty success_msg }">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${success_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</c:if>
		<c:if test="${! empty error_msg }">
			<div class="notification-panl er-msg">
				<div class="notfctn-cntnnt">${error_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</c:if>

		<div id="success-msg">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${success_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</div>

		<div id="erro-msg">
			<div class="notification-panl er-msg">
				<div class="notfctn-cntnnt">${error_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</div>

		<div id="load-erp">
				<div id="spinneeer-erp">
					<span class="loading-text-erp">Loading.. </span><span class="spinr-erp"><i
						class="fa fa-spinner fa-spin "></i></span>
				</div>
		</div>

		<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
		<script
			src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
		<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
		<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
		<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
		<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
		<script type="text/javascript">
			
		</script>
</body>
</html>