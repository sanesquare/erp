<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}
</style>
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">Purchase Orders</div>
						<div class="panel-body">
							<div id="panelBody">
								<table cellpadding="0" cellspacing="0" border="0"
									class="table table-striped table-bordered erp-tbl"
									id="basic-datatable">
									<thead>
										<tr>
											<th width="10%">Code</th>
											<th width="10%">Vendor</th>
											<th width="10%">Receiving Note</th>
											<th width="10%">Delete</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${orders }" var="po">
											<tr>
												<%-- <c:choose>
													<c:when test="${! empty po.noteCode }">
														<td><a href="viewPurchaseOrder.do?id=${po.quoteId }">
																${po.orderCode }</a></td>
														<td>${po.vendor }</td>
														<td><a href="viewReceivingNote.do?id=${po.noteId}"><span
																class="badge ">${po.noteCode }</span></a></td>
														<td><i>Received</i></td>
													</c:when>
													<c:otherwise>
														<td><a href="viewPurchaseOrder.do?id=${po.quoteId }">
																${po.orderCode }</a></td>
														<td>${po.vendor }</td>
														<td><a
															href="purchaseOrderToReceivingNote.do?id=${po.quoteId}"><button
																	class="btn ">Receiving Note</button></a></td>
														<td><i class="fa fa-times"
															onclick="deletePurchaseOrder('${po.quoteId}')"></i></td>
													</c:otherwise>
												</c:choose> --%>
												<td><a href="viewPurchaseOrder.do?id=${po.quoteId }">
														${po.orderCode }</a></td>
												<td>${po.vendor }</td>
												<td><c:forEach items="${po.receivingNoteIdCode}"
														var="r">
														<a href="viewReceivingNote.do?id=${r.key}"><span
															class="badge ">${r.value }</span></a>
													</c:forEach> <c:if test="${! po.hasReceived }">
														<a href="purchaseOrderToReceivingNote.do?id=${po.quoteId}"><button
																class="btn-sm btn-primary right">Create</button></a>
													</c:if></td>
												<td><c:choose>
														<c:when test="${ po.hasReceived }">
															<i>Completely Received</i>
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${! empty po.receivingNoteIdCode}">
																	<i>Cannot Delete</i>
																</c:when>
																<c:otherwise>
																	<i class="fa fa-times"
																		onclick="deletePurchaseOrder('${po.quoteId}')"></i>
																</c:otherwise>
															</c:choose>
														</c:otherwise>
													</c:choose></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<a href="addPurchaseOrder.do">
								<button type="button" class="btn btn-primary erp-btn">New
									Purchase Order</button>
							</a>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="confirm-delete" style="display: none;">
			<div class="modal " style="display: block;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Confirmation</h4>
						</div>
						<div class="modal-body">Do you really want to delete this
							item?</div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<input type="hidden" id="delete-item">
								<button type="button" onclick="cancel()" class="btn btn-default">No</button>
								<button type="button" onclick="deleteItem()"
									class="btn btn-primary">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<c:if test="${! empty success_msg }">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${success_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</c:if>
		<c:if test="${! empty error_msg }">
			<div class="notification-panl er-msg">
				<div class="notfctn-cntnnt">${error_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</c:if>

		<div id="success-msg">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${success_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</div>

		<div id="erro-msg">
			<div class="notification-panl er-msg">
				<div class="notfctn-cntnnt">${error_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</div>

		<div id="load-erp"></div>

		<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
		<script
			src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
		<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
		<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
		<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
		<script type="text/javascript">
			
		</script>
</body>
</html>