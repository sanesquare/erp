<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Purchase Settings<a href="purchaseHome.do"><span
								class="back-btn"><i class="fa fa-arrow-left"></i></span></a>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<!-- <div class="panel-heading panel-inf">Miscellaneous
											Details</div>
										<div class="panel-body"> -->
										<div class="panel-heading panel-inf">Units</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-sm-12" id="unit-table-div">
													<div id="load-erp">
														<div id="spinneeer-erp"></div>
													</div>
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl"
														id="basic-datatables">
														<thead>
															<tr>
																<th width="20%">Name</th>
																<th width="25%">Symbol</th>
																<th width="25%">No. of decimal places</th>
																<th width="10%">Edit</th>
																<th width="10%">Delete</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${units }" var="u">
																<tr>
																	<td>${u.name }</td>
																	<td>${u.symbol }</td>
																	<td>${u.decimalPlaces }</td>
																	<td><a
																		onclick="editThisUnit('${u.id}','${u.name }','${u.symbol }','${u.decimalPlaces }')"><i
																			class="fa fa-pencil"></i></a></td>
																	<td><a onclick="deleteThisUnit('${u.id}')"><i
																			class="fa fa-times"></i></a></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
											<form action="#" id="unit-frm">
												<input type="hidden" id="unitId">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Name<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" id="unit-name" name="unit_name"
															class="form-control paste">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Symbol<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" id="unit-symbl" name="unit_symbl"
															class="form-control paste">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">No.
														of decimal places<span class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" id="unit-nodp" name="unit_nodp"
															class="form-control paste numbersonly">
													</div>
												</div>
												<div class="form-group wid-80">
													<button type="reset" onclick="clearFrmUnit()"
														class="btn btn-primary erp-btn">New</button>
													<button type="submit" onclick="saveUnit()"
														class="btn btn-primary erp-btn">Save</button>
												</div>
											</form>
											<!-- <div class=" trash-div">
												<span class="trash"><i class="fa fa-trash-o"></i></span>
											</div> -->
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Pack Units</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-sm-12">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl"
														id="basic-datatables">
														<thead>
															<tr>
																<th width="20%">Type</th>
																<th width="25%">Name</th>
																<th width="25%">Unit Reference</th>
																<th width="20%">Conversion Factor</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>dsfdsf</td>
																<td>dsfdsf</td>
																<td>dsfdsf</td>
																<td>dsfdsf</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Name<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" class="form-control paste">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Unit<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<select class="form-control">
															<option>--Select Unit--</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Conversion
														Factor<span class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" class="form-control paste">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Number<span
														class="stars">*</span>
													</label>
													<div class="col-sm-4">
														<input type="text" class="form-control paste">
													</div>
												</div>
												<div class="form-group wid-80">
													<button type="button" class="btn btn-primary erp-btn">New</button>
													<button type="button" class="btn btn-primary erp-btn">Save</button>
												</div>
												<div class=" trash-div">
													<span class="trash"><i class="fa fa-trash-o"></i></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> -->

							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Stock Groups</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-sm-12" id="stck_dv">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl"
														id="basic-datatabless">
														<thead>
															<tr>
																<th width="20%">Name</th>
																<th width="25%">Parent Group</th>
																<th width="25%">Edit</th>
																<th width="20%">Delete</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${stockGroups}" var="stc">
																<tr>
																	<td>${stc.groupName }</td>
																	<td>${stc.parentGroup }</td>
																	<td><a
																		onclick="editStockGroupValues('${stc.id}','${stc.parentGroupId }','${stc.groupName }','${stc.subGroupIds }')"><i
																			class="fa fa-pencil"></i></a></td>
																	<td><a onclick="deleteStockgroup('${stc.id}')"><i
																			class="fa fa-times"></i></a></td>
																</tr>
															</c:forEach>

														</tbody>
													</table>
												</div>

												<form action="#" id="stck_frm">
													<input type="hidden" id="stckId">
													
													
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Parent
															Group<span class="stars">*</span>
														</label>
														<div class="col-sm-4">

															<select class="form-control" id="parent_stckgp" name="stck_parent">
																<option value="">--Select Parent Group--</option>
																<c:forEach items="${stockGroups }" var="sg">
																	<option value="${sg.id }" label="${sg.groupName }">
																</c:forEach>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Name<span
															class="stars">*</span>
														</label>
														<div class="col-sm-4">
															<input type="text" class="form-control paste" name="stck_name"
																id="stckgp_name">
														</div>
													</div>
													<div class="form-group wid-80">
														<button type="reset" class="btn btn-primary erp-btn" onclick="clearFromStocks()">New</button>
														<button type="submit" class="btn btn-primary erp-btn"
															onclick="saveStockGroup()">Save</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="confirm-delete" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">Do you really want to delete this
						item?</div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<input type="hidden" id="delete-item">
							<button type="button" onclick="cancel()" class="btn btn-default">No</button>
							<button type="button" onclick="deleteItem()"
								class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="notification-panl successMsg" style="display: none;">
		<div class="notfctn-cntnnt">Action completed successfully..</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script
	 src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
	<script
		src="<c:url value='/resources/assets/purchase/js/purchase-validation.js' />"></script>

	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
</body>
</html>