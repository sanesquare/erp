<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Product's Information<a href="products.do"><span
								class="back-btn"><i class="fa fa-arrow-left"></i></span></a>
						</div>
						<div class="panel-body">
							<form:form action="saveProduct.do" commandName="productVo"
								id="prdct_frm">
								<form:hidden path="productId" />
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Basic Information</div>
											<div class="panel-body">
												<c:if test="${! empty productVo.productCode }">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Product
															Code </label>
														<div class="col-sm-9">
															<form:label for="inputPassword3"
																class="col-sm-3 control-label" path="productCode">${productVo.productCode }</form:label>
														</div>
													</div>

												</c:if>

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Name<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="name" class="form-control paste" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Unit<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="unitId">
															<form:option value="" label="-Select Unit-" />
															<c:forEach var="s" items="${units }">
																<form:option value="${s.id }" label="${s.name }" />
															</c:forEach>
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Sales
														Price<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="salesPrice" class="form-control double" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Purchase
														Price<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="purchasePrice"
															class="form-control double" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Stock
														Group<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="stockGroupId">
															<form:option value="" label="-Select Stock Group-" />
															<c:forEach var="s" items="${stockGroups }">
																<form:option value="${s.id }" label="${s.groupName }" />
															</c:forEach>
														</form:select>
													</div>
												</div>
												<%-- <div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Opening
														Quantity <span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="openingQuantity"
															class="form-control double" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Opening
														Balance <span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="openingBalance"
															class="form-control double" />
													</div>
												</div> --%>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Weight
														<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="weight" class="form-control double" />
													</div>
												</div>
												<div class="form-group">
													<button type="submit" class="btn btn-primary erp-btn">Save</button>
												</div>

												<a href="addProduct.do">
													<button type="button" class="btn btn-primary erp-btn right">New
														Product</button>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form:form>
							<!-- <div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Advanced
											Information</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Available
													Quantity</label>
												<div class="col-sm-9">
													<input type="text" class="form-control double">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Weight</label>
												<div class="col-sm-9">
													<input type="text" class="form-control double">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Height</label>
												<div class="col-sm-9">
													<input type="text" class="form-control double">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Width</label>
												<div class="col-sm-9">
													<input type="text" class="form-control double">
												</div>
											</div>

											<div class="form-group">
												<a href="#">

													<button type="button" class="btn btn-primary erp-btn">Save</button>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div> -->
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${! empty msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${msg }</div>
			<span id="close-msg" onclick="closeMessage()"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<%-- <script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script> --%>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>

	<script
		src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
	<script
		src="<c:url value='/resources/assets/purchase/js/purchase-validation.js' />"></script>

	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
</body>
</html>