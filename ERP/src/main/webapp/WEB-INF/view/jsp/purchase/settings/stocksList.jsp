<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl"
	id="basic-datatabless">
	<thead>
		<tr>
			<th width="20%">Name</th>
			<th width="25%">Parent Group</th>
			<th width="25%">Edit</th>
			<th width="20%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${stockGroups}" var="stc">
			<tr>
				<td>${stc.groupName }</td>
				<td>${stc.parentGroup }</td>
				<td><a
					onclick="editStockGroupValues('${stc.id}','${stc.parentGroupId }','${stc.groupName }','${stc.subGroupIds }')"><i
						class="fa fa-pencil"></i></a></td>
				<td><a onclick="deleteStockgroup('${stc.id}')"><i
						class="fa fa-times"></i></a></td>
			</tr>
		</c:forEach>

	</tbody>
</table>
