<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<thead>
	<tr>
		<th width="10%">Product Code</th>
		<th width="20%">Item</th>
		<th width="10%">Quantity</th>
		<th width="10%">Unit Price</th>
		<th width="10%">Discount %</th>
		<th width="10%">Net Amount</th>
		<th width="10%">Tax</th>
		<th width="10%">Total</th>
		<th width="10%">Delete</th>
	</tr>
</thead>
<tbody>
	<c:forEach items="${products }" var="p">
		<tr class="productTableRow" id="${p.productCode }">
			<input type="hidden" class="prodctId" value="${p.productId }">
			<td style="padding: .5%;"><input type="hidden" class="prodctId"
				value="${p.productId }"> <input type="hidden"
				id="itemId${p.productCode }" value="${p.productId }"> <label
				id="prod_code${p.productCode }">${p.productCode }</label></td>
			<td style="padding: .5%;"><label id="prod_name${p.productCode }">${p.name }</label></td>
			<td style="padding: .5%;"><input type="text"
				value="${p.availableQty }"
				onblur="findNetAmount('${p.productCode }')"
				class="form-control double " id="product_qty${p.productCode }" /></td>
			<td style="padding: .5%;"><c:choose>
					<c:when test="${isSales }">
						<input type="text" value="${p.salesPrice }"
							onblur="findNetAmount('${p.productCode }')"
							class="form-control double " id="product_prc${p.productCode }" />
					</c:when>
					<c:otherwise>
						<input type="text" value="${p.purchasePrice }"
							onblur="findNetAmount('${p.productCode }')"
							class="form-control double " id="product_prc${p.productCode }" />
					</c:otherwise>
				</c:choose></td>
			<td style="padding: .5%;"><input type="text" value="0.00"
				onblur="findNetAmount('${p.productCode }')"
				class="form-control double" id="product_disc${p.productCode }" /></td>
			<td style="padding: .5%;"><label id="prod_tot${p.productCode }">0.00</label></td>
			<td style="padding: .5%;"><select class="form-control"
				onchange="findNetAmount('${p.productCode }')"
				id="product_tx${p.productCode }">
					<option value="" label="--Select Tax--"></option>
					<c:forEach items="${taxes }" var="tx">
						<option value="${tx.id }!!!${tx.rate}" label="${tx.taxName }"></option>
					</c:forEach>
			</select></td>
			<td style="padding: .5%;"><label
				id="product_net${p.productCode }">0.00</label></td>
			<td style="padding: .5%;"><i
				onclick="javascript:deleteProductRow(this)" class="fa fa-times"></i></td>
		</tr>
	</c:forEach>
</tbody>
<tr>
	<td colspan="9" style="padding: .5%;"><button type="button"
			onclick="getProductDetails()" class="btn btn-primary erp-btn right">Add
			Item</button></td>
</tr>
<script
	src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>