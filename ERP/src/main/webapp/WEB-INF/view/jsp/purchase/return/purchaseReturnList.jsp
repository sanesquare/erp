<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">Code</th>
			<th width="10%">Vendor</th>
			<th width="10%">Date</th>
			<th width="5%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${returns }" var="p">
			<tr>
				<td><a href="viewPurchaseReturn.do?id=${p.returnId }">${p.code }</a></td>
				<td><a href="viewPurchaseReturn.do?id=${p.returnId }">${p.vendor }</a></td>
				<td><a href="viewPurchaseReturn.do?id=${p.returnId }">${p.date }</a></td>
				<td><a onclick="deletePurchaseReturn('${b.returnId }')"><i
						class="fa fa-times"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
</c:if>

<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>