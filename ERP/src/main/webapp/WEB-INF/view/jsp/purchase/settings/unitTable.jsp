<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="load-erp">
	<div id="spinneeer-erp"></div>
</div>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl"
	id="basic-datatables">
	<thead>
		<tr>
			<th width="20%">Name</th>
			<th width="25%">Symbol</th>
			<th width="25%">No. of decimal places</th>
			<th width="10%">Edit</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${units }" var="u">
			<tr>
				<td>${u.name }</td>
				<td>${u.symbol }</td>
				<td>${u.decimalPlaces }</td>
				<td><a
					onclick="editThisUnit('${u.id}','${u.name }','${u.symbol }','${u.decimalPlaces }')"><i
						class="fa fa-pencil"></i></a></td>
				<td><a onclick="deleteThisUnit('${u.id}')"><i
						class="fa fa-times"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script
	src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>