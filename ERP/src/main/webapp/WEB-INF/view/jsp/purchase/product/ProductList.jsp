<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">Code</th>
			<th width="20%">Name</th>
			<th width="20%">Stock Group</th>
			<th width="5%">Edit</th>
			<th width="5%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${products }" var="p">
			<tr>
				<td>${p.productCode }</td>
				<td>${p.name }</td>
				<td>${p.stockGroup }</td>
				<td><a href="editProduct.do?id=${p.productId }"><i
						class="fa fa-pencil"></i></a></td>
				<td><a onclick="deleteThisProduct('${p.productId}')"><i
						class="fa fa-times"></i></a></td>
			</tr>
		</c:forEach>


	</tbody>
</table>
<script
	src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>