<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="30%">Name</th>
			<th width="30%">Email</th>
			<th width="30%">Work Phone</th>
			<th width="10%">Edit</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<c:if test="${! empty customerContacts }">
		<tbody>
			<c:forEach items="${customerContacts }" var="vo">
				<tr>
					<td>${vo.name }</td>
					<td>${vo.email }</td>
					<td>${vo.phone }</td>
					<td><a
						onclick="editCustomerContact('${vo.customerContactId }','${vo.name }','${vo.email }','${vo.phone }','${vo.address }')"><i
							class="fa fa-pencil"></i> </a></td>
					<td><a onclick="deleteContact('${vo.customerContactId }')"><i
							class="fa fa-times"></i></a></td>
				</tr>
			</c:forEach>
		</tbody>
	</c:if>
</table>
<script type="text/javascript"
	src='<c:url value="/resources/assets/sales/js/sales.js"/>'></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>