<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
</head>
<body>
	<div class="sales_home">
		<div class="warper container-fluid">
			<div class="noti" id="notifications"></div>

			<div class="page-header">
				<div class="hdr">

					<div class="home-div">
						<a href="erpHome.do"> <span class="home"><i
								class="fa fa-home"></i></span>
						</a>
					</div>

					<div class="hdr-div">
						<span class="hdr-ico"><i class="fa fa-bar-chart-o"></i></span>
						Sales <small>Let's get a quick overview...</small>
					</div>

				</div>
			</div>
			<div class="row"
				style="padding-top: 3%; margin-left: 4%; margin-right: 4%">
				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="customer.do"> <span
								class="hover_effect dashboard-title">Customer </span></a> </span> <i
							class="fa fa-users bg-sales transit stats-icon"></i> <a
							href="customer.do">
							<h3 class="transit dashboard-title">
								Customer <br> <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>

				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="customerQuotesHome.do"> <span
								class="hover_effect dashboard-title">Quotes </span></a> </span> <i
							class="fa fa-list-alt bg-sales transit stats-icon"></i> <a
							href="customerQuotesHome.do">
							<h3 class="transit dashboard-title">
								Quotes <br> <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>

				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="salesOrderHome.do"> <span
								class="hover_effect dashboard-title">Sales Order </span></a> </span> <i
							class="fa fa-file-o bg-sales transit stats-icon"></i> <a
							href="salesOrderHome.do">
							<h3 class="transit dashboard-title">
								Sales Order  <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>

				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="invoiceHome.do"> <span
								class="hover_effect dashboard-title">Invoice </span></a> </span> <i
							class="fa fa-check-square-o bg-sales transit stats-icon"></i> <a
							href="invoiceHome.do">
							<h3 class="transit dashboard-title">
								Invoice <br> <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>
			</div>
			<div class="row"
				style="padding-top: 3%; margin-left: 4%; margin-right: 4%">
				<!-- <div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="#"> <span
								class="hover_effect dashboard-title">Credit Memo </span></a> </span> <i
							class="fa fa-file-text-o bg-sales transit stats-icon"></i> <a
							href="#">
							<h3 class="transit dashboard-title">
								Credit Memo <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div> -->


				<div class="col-md-3">
					<div
						class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
						<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
							href="#"> <span
								class="hover_effect dashboard-title">Sales Return </span></a> </span> <i
							class="fa fa-refresh bg-sales transit stats-icon"></i> <a
							href="#">
							<h3 class="transit dashboard-title">
								Sales Return  <small class="text-invent"></small>
							</h3>
						</a>
					</div>
				</div>


			</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		
	</script>
	<script>
		
	</script>
	<!-- Warper Ends Here (working area) -->
</body>
</html>