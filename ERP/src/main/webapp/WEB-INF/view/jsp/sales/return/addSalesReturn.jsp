<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
</head>
<body>
	<div class="sales_home">
		<div class="warper container-fluid">

			<div class="row">

				<div class="panel-body">
					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Sales Return
							<c:if test="${! empty invoice.code }">- ( ${invoice.code } ) </c:if>
							<a href="salesReturnHome.do"><span class="back-btn"><i
									class="fa fa-arrow-left"></i></span></a>
						</div>
						<c:choose>
							<c:when test="${showFilter }">
								<form:form action="returnThisInvoice.do" method="POST"
									commandName="salesReturnVo">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-5">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Customer<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="customerId"
															onchange="filterInvoice(this.value)" id="sl_cust">
															<option value="" label="--Select Customer"></option>
															<c:forEach items="${customer }" var="c">
																<form:option value="${c.customerId}" label="${c.name }"></form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
											<div class="col-md-5">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Invoice<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="invoiceId"
															id="pr_bl">
															<option value="" label="--Select Invoice--"></option>
															<c:forEach items="${invoices }" var="i">
																<form:option value="${i.invoiceId }"
																	label="${i.invoiceCode }"></form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
											<div class="col-md-2">
												<div class="form-group">
													<button type="submit" class="btn btn-primary erp-btn right">Go</button>
												</div>
											</div>
										</div>
									</div>
								</form:form>
							</c:when>
							<c:otherwise>
								<form:form action="#" commandName="invoice">
									<form:hidden path="invoiceId" id="invoiceId" />
									<form:hidden path="returnId" id="returnId" />
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Customer<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<label>${invoice.customer }</label>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Account<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<label>${invoice.account }</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Date<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class='input-group date invoiceDate' id="datepicker">
															<input type="text" readonly="true" name="periodStarts"
																id="invoice-date" value="${date }"
																style="background-color: #ffffff !important;"
																data-date-format="DD/MM/YYYY" class="form-control" /> <span
																class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Status<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<c:choose>
															<c:when test="${invoice.isSettled }">
																<label>Settled</label>
															</c:when>
															<c:otherwise>
																<label>Pending</label>
															</c:otherwise>
														</c:choose>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Currency<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<label>${invoice.currecny }</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="panel-body tbl_scrl_bdy">
												<div class="col-sm-12">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl prdct_tbl">
														<thead>
															<tr>
																<th width="10%">Product Code</th>
																<th width="20%">Item</th>
																<th width="10%">Quantity</th>
																<th width="10%">Unit Price</th>
																<th width="10%">Total</th>
																<th width="10%">Godown</th>
																<th width="10%">Delete</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${invoice.productVos }" var="p">
																<tr class="productTableRow" id="${p.productCode }">
																	<td style="padding: .5%;"><label
																		id="prod_code${p.productCode }">${p.productCode }</label>
																	</td>
																	<td style="padding: .5%;"><label
																		id="prod_name${p.productCode }">${p.productName }</label></td>
																	<td style="padding: .5%;"><input type="text"
																		value="${p.quantity }"
																		onblur="findNetAmountSR('${p.productCode }')"
																		class="form-control double "
																		id="product_qty${p.productCode }" /> <input
																		type="hidden" value="${p.quantityToReturn }"
																		id="product_qty_hid${p.productCode }"></td>
																	<td style="padding: .5%;"><input type="text"
																		readonly="readonly" value="${p.unitPrice }"
																		onblur="findNetAmountSR('${p.productCode }')"
																		class="form-control double "
																		id="product_prc${p.productCode }" /></td>
																	<td style="padding: .5%;"><label
																		id="product_net${p.productCode }">${p.total }</label></td>
																	<td style="padding: .5%;"><label>${p.godown }</label><input
																		type="hidden" value="${p.godownId }"
																		id="prod_godwn_id${p.productCode }"></td>
																	<td style="padding: .5%;"><i
																		onclick="javascript:deleteProductRow(this)"
																		class="fa fa-times"></i></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
										</div>


										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<button type="button" onclick="saveSalesReturn()"
														class="btn btn-primary erp-btn ">Save</button>


												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="right">
													<a href="addSalesReturn.do">
														<button type="button" class="btn btn-primary erp-btn">New
															Return</button>
													</a>
												</div>
											</div>
										</div>

									</div>
									<!-- </div> -->
								</form:form>
							</c:otherwise>
						</c:choose>


					</div>
				</div>
			</div>
		</div>

		<div id="pdtPop" style="display: none;"></div>

		<div id="errror-msg" style="display: none;">
			<div class="modal " data-backdrop="static" data-keyboard="false"
				tabindex="-1" aria-hidden="false" style="display: block;"
				role="dialog">
				<div class="modal-dialog ">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Alert</h4>
						</div>
						<div class="modal-body"></div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<button type="button" onclick="closeError()"
									class="btn btn-default">Ok</button>
								<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="confirm-delete" style="display: none;">
			<div class="modal " style="display: block;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Confirmation</h4>
						</div>
						<div class="modal-body">Do you want to convert this
							installment to payment?</div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<input type="hidden" id="delete-item">
								<button type="button" onclick="cancel()" class="btn btn-default">No</button>
								<button type="button" onclick="confirmedBillPayment()"
									class="btn btn-primary">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
	<%-- <c:if test="${! empty success_msg }"> --%>
	<div class="notification-panl successMsg" style="display: none">
		<div class="notfctn-cntnnt">${success_msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
	<%-- </c:if> --%>
	<c:if test="${! empty msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${msg }</div>
			<span id="close-msg" onclick="closeMessage()"><i
				class="fa fa-times"></i></span>
		</div>
	</c:if>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script type="text/javascript">
		
	</script>
<%-- 	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script> --%>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script src="<c:url value='/resources/assets/sales/js/sales.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<script type="text/javascript">
		$("document").ready(function() {

		});
	</script>
</body>
</html>