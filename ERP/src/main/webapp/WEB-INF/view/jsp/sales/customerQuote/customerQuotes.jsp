<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">Code</th>
			<th width="10%">Customer</th>
			<th width="10%">Order</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${quotes }" var="qo">
			<tr>
				<td><a href="viewCustomerQuote.do?id=${qo.orderId }">
						${qo.quoteCode }</a></td>
						<td>${qo.customer }</td>
				<c:choose>
					<c:when test="${qo.isQuote }">
						<td>
							<button onclick="convertCustomerQuote('${qo.orderId}')"
								class="btn ">Convert to Order</button>
						</td>
						<td><i class="fa fa-times"
							onclick="deleteCustomerQuote('${qo.orderId}')"></i></td>
					</c:when>
					<c:otherwise>
						<td><a href="viewSalesOrder.do?id=${qo.orderId }"
							class="badge">${qo.orderCode }</a></td>
						<td><i>Ordered</i></td>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
</c:if>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script src="<c:url value='/resources/assets/sales/js/sales.js' />"></script>