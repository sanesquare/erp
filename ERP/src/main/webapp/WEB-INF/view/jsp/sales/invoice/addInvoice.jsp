<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
</head>
<body>
	<div class="sales_home">
		<div class="warper container-fluid">

			<div class="row">

				<div class="panel-body">
					<div class="panel panel-default erp-panle">
						<c:choose>
							<c:when test="${invoice.isEdit }">
								<div class="panel-heading  panel-inf">
									Invoice - ( ${invoice.invoiceCode } )<a href="invoiceHome.do"><span
										class="back-btn"><i class="fa fa-arrow-left"></i></span></a>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Customer
												</label>
												<div class="col-sm-9">
													<label>${invoice.customer }</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Account
												</label>
												<div class="col-sm-9">
													<label>${invoice.account }</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Date
												</label>
												<div class="col-sm-9">
													<label>${invoice.invoiceDate }</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Status
												</label>
												<div class="col-sm-9">
													<c:choose>
														<c:when test="${invoice.isSettled }">
															<label>Settled</label>
														</c:when>
														<c:otherwise>
															<label>Pending</label>
														</c:otherwise>
													</c:choose>

												</div>
											</div>
										</div>

									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Payment
													Term </label>
												<div class="col-sm-9">
													<label>${invoice.paymentTerm }</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Delivery
													Term </label>
												<div class="col-sm-9">
													<label>${invoice.deliveryTerm }</label>
												</div>
											</div>
										</div>
									</div>
									<div class=row>
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Currency
												</label>
												<div class="col-sm-9">
													<label>${invoice.currecny }</label>
												</div>
											</div>
										</div>
									</div>

									<c:if test="${! empty invoice.productVos  }">
										<div class="row">
											<div class="panel-body tbl_scrl_bdy">
												<div class="col-sm-12">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl prdct_tbl">
														<thead>
															<tr>
																<th width="10%">Product Code</th>
																<th width="20%">Item</th>
																<th width="10%">Quantity</th>
																<th width="10%">Unit Price</th>
																<th width="10%">Discount %</th>
																<th width="10%">Net Amount</th>
																<th width="10%">Tax</th>
																<th width="10%">Total</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${invoice.productVos }" var="p">
																<tr>
																	<td><label>${p.productCode }</label></td>
																	<td><label>${p.productName }</label></td>
																	<td><label>${p.quantity }</label></td>
																	<td><label>${p.unitPrice }</label></td>
																	<td><label>${p.discount }</label></td>
																	<td><label>${p.netAmount }</label></td>
																	<td><label> <c:choose>
																				<c:when test="${! empty p.tax }">
																	${p.tax }</c:when>
																				<c:otherwise>
																					<i>--</i>
																				</c:otherwise>
																			</c:choose>
																	</label></td>
																	<td><label>${p.netAmount }</label></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</c:if>
									<c:if test="${! empty invoice.chargeVos }">
										<div class="row">
											<div class="panel-body">
												<div class="col-sm-12">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl prdct_tbl">
														<thead>
															<tr>
																<th width="10%">Charge</th>
																<th width="20%">Amount</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${invoice.chargeVos }" var="p">
																<tr>
																	<td><label>${p.charge }</label></td>
																	<td><label>${p.amount }</label></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</c:if>
									<div class="row tbl_scrl_bdy">
										<div class="col-sm-8"></div>
										<div class="col-sm-4">
											<div class="form-group">
												<div class="col-sm-6">
													<label>Sub Total</label>
												</div>
												<div class="col-sm-6">
													<label id="subTotal">${invoice. subTotal}</label>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-6">
													<label>Discount Rate</label>
												</div>
												<div class="col-sm-6">
													<label id="subTotal">${invoice. discountRate}</label>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-6">
													<label>Discount Amount</label>
												</div>
												<div class="col-sm-6">
													<label id="subTotal">${invoice. discountTotal}</label>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-6">
													<label>Gross Total</label>
												</div>
												<div class="col-sm-6">
													<label id="subTotal">${invoice. netTotal}</label>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-6">
													<label>Tax: </label>
												</div>
												<div class="col-sm-6">
													<c:choose>
														<c:when test="${! empty invoice.taxes }">
															<c:forEach items="${invoice.taxes }" var="t">
																<label>${t }</label>
																<br>
															</c:forEach>
														</c:when>
														<c:otherwise>
															<label><i>None</i></label>
														</c:otherwise>
													</c:choose>

												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-6">
													<label>Tax Total</label>
												</div>
												<div class="col-sm-6">
													<label id="subTotal">${invoice. taxTotal}</label>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-6">
													<label>Grand Total</label>
												</div>
												<div class="col-sm-6">
													<label id="subTotal">${invoice. grandTotal}</label>
												</div>
											</div>
										</div>
									</div>

									<table cellpadding="0" cellspacing="0" border="0"
										class="table table-striped table-bordered erp-tbl instllmnt_tbl"
										id="">
										<thead>
											<tr>
												<th width="10%">Date</th>
												<th width="10%">Installment</th>
												<th width="20%">Amount</th>
												<th width="20%">Status</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${invoice.installmentVos }" var="inst">
												<tr>
													<td><label>${inst.date }</label></td>
													<td><label>${inst.percentage }</label>&nbsp;<b>%</b></td>
													<td><label><fmt:formatNumber type="number"
																pattern="0.00" maxFractionDigits="2"
																value="${inst.amount }" /></label></td>
													<td><c:choose>
															<c:when test="${inst.isPaid }">
																<label>Received</label>
															</c:when>
															<c:otherwise>
																<select class="form-control"
																	onchange="receiveBillInstallment(this.value,'${inst.id }')">
																	<option value="true"
																		label="Convert Installment To Receipt">
																	<option value="false" selected="selected"
																		label="Pending">
																</select>
															</c:otherwise>
														</c:choose></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<div class="row">
										<div class="col-sm-6">
											<div class="right">
												<a href="invoiceAdd.do">
													<button type="button" class="btn btn-primary erp-btn">New
														Invoice</button>
												</a>
												<c:if test="${! empty invoice.invoiceId }">
													<a href="generateInvoice.do?iid=${invoice.invoiceId }">
														<button type="button" class="btn btn-primary erp-btn">Generate
															Invoice</button>
													</a>
												</c:if>
											</div>
										</div>

									</div>
								</div>
							</c:when>
							<c:otherwise>
								<div class="panel-heading  panel-inf">
									Invoice
									<c:if test="${! empty invoice.invoiceCode }">- ( ${invoice.invoiceCode } ) </c:if>
									<a href="invoiceHome.do"><span class="back-btn"><i
											class="fa fa-arrow-left"></i></span></a>
								</div>
								<form:form action="#" commandName="invoice">
									<form:hidden path="invoiceId" id="invoiceId" />
									<form:hidden path="deliveryNoteIds" id="deliveryNoteIds" />
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Customer<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="customerId"
															id="bill_vendr">
															<option value="" label="--Select Customer--"></option>
															<c:forEach items="${customer }" var="v">
																<form:option value="${v.customerId }" label="${v.name }"></form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Account<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select path="ledgerId" class="form-control"
															id="bill_ledgr">
															<option value="" label="--Select Account--"></option>
															<c:forEach items="${accounts }" var="v">
																<form:option value="${v.ledgerId }"
																	label="${v.ledgerName }"></form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>

										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Date<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class='input-group date billDate' id="datepicker">
															<input type="text" readonly="true" name="periodStarts"
																id="bill-date" value="${date }"
																style="background-color: #ffffff !important;"
																data-date-format="DD/MM/YYYY" class="form-control" /> <span
																class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Status<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<select class="form-control" id="bill_sts">
															<option value="false" selected="selected" label="Pending"></option>
															<option value="true" label="Settled"></option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Payment
														Term<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select class="form-control" path="paymentTermId"
															onchange="getInstallmentsForInvoice()"
															id="paymentTermSelect">
															<option value="" label="--Select Payment Term--"></option>
															<c:forEach items="${terms }" var="t">
																<form:option value="${t.id }" label="${t.name }"></form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Delivery
														Term </label>
													<div class="col-sm-9">
														<form:select path="deliveryTermId" id="deliveryTermSelect"
															cssClass="form-control">
															<option value="" label="--Select Delivery Term--"></option>
															<c:forEach var="t" items="${deliveryTerms }">
																<form:option value="${t.deliveryMethodId }"
																	label="${t.method }"></form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Currency<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<select class="form-control" id="currencySelBill">
															<option value="" label="--Select Currency--"></option>
															<c:forEach items="${currencies }" var="t">
																<c:choose>
																	<c:when test="${t.erpCurrencyId eq currencyId }">
																		<option value="${t.erpCurrencyId }"
																			selected="selected" label="${t.currency }"></option>
																	</c:when>
																	<c:otherwise>
																		<option value="${t.erpCurrencyId }"
																			label="${t.currency }"></option>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>

										</div>
										<div class="row">
											<div class="panel-body tbl_scrl_bdy">
												<div class="col-sm-12">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl prdct_tbl">
														<thead>
															<tr>
																<th width="10%">Product Code</th>
																<th width="20%">Item</th>
																<th width="10%">Quantity</th>
																<th width="10%">Unit Price</th>
																<th width="10%">Discount %</th>
																<th width="10%">Net Amount</th>
																<th width="10%">Tax</th>
																<th width="10%">Total</th>
																<th width="10%">Delete</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${invoice.productVos }" var="p">
																<tr class="productTableRow" id="${p.productCode }">
																	<td style="padding: .5%;"><label
																		id="prod_code${p.productCode }">${p.productCode }</label>
																		<input type="hidden" id="godownId${p.productCode }"
																		value="${p.godownId }"> <input type="hidden"
																		class="prodctId" value="${p.productId }"> <input
																		type="hidden" id="itemId${p.productCode }"
																		value="${p.invoiceProductId }"></td>
																	<td style="padding: .5%;"><label
																		id="prod_name${p.productCode }">${p.productName }</label></td>
																	<td style="padding: .5%;"><c:choose>
																			<c:when test="${fromDN }">
																				<input type="text" value="${p.quantity }"
																					readonly="readonly"
																					onblur="findNetAmount('${p.productCode }')"
																					class="form-control double "
																					id="product_qty${p.productCode }" />
																				<input type="hidden" value="${p.deliveredQuantity }"
																					id="delivered_prod${p.productCode }">
																				<input type="hidden" value="${p.returnedQuantity }"
																					id="product_qty_hid${p.productCode }">
																			</c:when>
																			<c:otherwise>
																				<input type="text" value="${p.quantity }"
																					onblur="findNetAmount('${p.productCode }')"
																					class="form-control double "
																					id="product_qty${p.productCode }" />
																				<input type="hidden" value="${p.deliveredQuantity }"
																					id="delivered_prod${p.productCode }">
																				<input type="hidden" value="${p.returnedQuantity }"
																					id="product_qty_hid${p.productCode }">
																			</c:otherwise>
																		</c:choose></td>
																	<td style="padding: .5%;"><input type="text"
																		value="${p.unitPrice }"
																		onblur="findNetAmount('${p.productCode }')"
																		class="form-control double "
																		id="product_prc${p.productCode }" /></td>
																	<td style="padding: .5%;"><input type="text"
																		value="${p.discount }"
																		onblur="findNetAmount('${p.productCode }')"
																		class="form-control double"
																		id="product_disc${p.productCode }" /></td>
																	<td style="padding: .5%;"><label
																		id="prod_tot${p.productCode }">${p.amountExcludingTax }</label></td>
																	<td style="padding: .5%;"><select
																		class="form-control"
																		onchange="findNetAmount('${p.productCode }')"
																		id="product_tx${p.productCode }">
																			<option value="" label="--Select Tax--"></option>
																			<c:forEach items="${taxes }" var="tx">
																				<c:choose>
																					<c:when test="${p.tax eq tx.taxName }">
																						<option value="${tx.id }!!!${tx.rate}"
																							selected="selected" label="${tx.taxName }"></option>
																					</c:when>
																					<c:otherwise>
																						<option value="${tx.id }!!!${tx.rate}"
																							label="${tx.taxName }"></option>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																	</select></td>
																	<td style="padding: .5%;"><label
																		id="product_net${p.productCode }">${p.netAmount }</label></td>
																	<td style="padding: .5%;"><i
																		onclick="javascript:deleteProductRow(this)"
																		class="fa fa-times"></i></td>
																</tr>
															</c:forEach>
															<tr>
																<td colspan="9"><button type="button"
																		onclick="getProductDetails()"
																		class="btn btn-primary erp-btn right">Add
																		Item</button></td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="panel-body tbl_scrl_bdy">
												<div class="col-sm-12">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl chrg_tbl"
														id="">
														<thead>
															<tr>
																<th width="10%">Charges</th>
																<th width="20%">Amount</th>
																<th width="10%">Delete</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${invoice.chargeVos }" var="p">
																<tr class="productTableRow" id="${p.chargeId }">
																	<td style="padding: .5%;"><input type="hidden"
																		class="chargeId" value="${p.chargeId }"> <input
																		type="hidden" id="billChargeId${p.chargeId }"
																		value="${p.invoiceChargeId }"> <label>${p.charge }</label></td>
																	<td style="padding: .5%;"><input type="text"
																		value="${p.amount }" onblur="findTotalAmounts()"
																		class="form-control double "
																		id="chrge_price${p.chargeId }" /></td>
																	<td style="padding: .5%;"><i
																		onclick="javascript:deleteChargeRow(this)"
																		class="fa fa-times"></i></td>
																</tr>
															</c:forEach>
															<tr>
																<td colspan="3"><button type="button"
																		onclick="getChargeDetails()"
																		class="btn btn-primary erp-btn right">Add
																		Charge</button></td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>

										<div class="row">
											<input type="hidden" value="${invoice.netAmountHidden }"
												id="netAmountHidden"> <input
												value="${invoice.productsTaxAmount }" type="hidden"
												id="taxAmountHidden">
											<div class="col-sm-8"></div>
											<div class="col-sm-4">
												<div class="form-group">
													<div class="col-sm-6">
														<label>Sub Total</label>
													</div>
													<div class="col-sm-6">
														<label id="subTotal">${invoice.subTotal }</label>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Discount Rate</label>
													</div>
													<div class="col-sm-6">
														<input type="text" onblur="findTotalAmounts()"
															class="myText double" value="${invoice.discountRate }"
															id="discRate">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Discount Amount</label>
													</div>
													<div class="col-sm-6">
														<label id="disTotal">${invoice.discountTotal }</label>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Gross Total</label>
													</div>
													<div class="col-sm-6">
														<label id="netTotal">${invoice.netTotal }</label>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Tax: </label>
													</div>
													<div class="col-sm-6">
														<form:select multiple="multiple" path="taxesWithRate"
															data-placeHolder="-Select Taxes-"
															onchange="findTotalAmounts()" id="taxesMulti"
															class="form-control chosen-select">
															<c:forEach items="${taxes }" var="t">
																<form:option value="${t.id}!!!${t.rate }"
																	label="${t.taxName }" />
															</c:forEach>
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Tax Total</label>
													</div>
													<div class="col-sm-6">
														<label id="taxTotal">${invoice.taxTotal }</label>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Grand Total</label>
													</div>
													<div class="col-sm-6">
														<label id="grandTotal">${invoice.grandTotal }</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="panel-body tbl_scrl_bdy">
												<div class="col-sm-12" id="installment_table_bill">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl instllmnt_tbl"
														id="">
														<thead>
															<tr>
																<th width="10%">Date</th>
																<th width="10%">Installment</th>
																<th width="20%">Amount</th>
																<th width="20%">Status <span class="right"><i
																		id="spinr" class="fa fa-spinner "></i> </span>
																</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${invoice.installmentVos }" var="inst">
																<tr id="${inst.id }">
																	<td><input type="hidden"
																		value="${inst.installmentId }"
																		id="instllmntId${inst.id }" /> <label
																		id="instlmnt_date${inst.id }">${inst.date }</label></td>
																	<td><label id="instlmnt_perc${inst.id }">${inst.percentage }</label>&nbsp;<b>%</b></td>
																	<td><label id="instlmnt_amount${inst.id }"><fmt:formatNumber
																				type="number" pattern="0.00" maxFractionDigits="2"
																				value="${inst.amount }" /></label></td>
																	<td><c:choose>
																			<c:when test="${! empty invoice.invoiceId }">
																				<c:choose>
																					<c:when test="${inst.isPaid }">
																						<label>Received</label>
																					</c:when>
																					<c:otherwise>
																						<select class="form-control"
																							onchange="receiveBillInstallment(this.value,'${inst.id }')"
																							id="instlmnt_stts${inst.id }">
																							<option value="true"
																								label="Convert Installment To Receipt">
																							<option value="false" selected="selected"
																								label="Pending">
																						</select>
																					</c:otherwise>
																				</c:choose>
																			</c:when>
																			<c:otherwise>
																				<select class="form-control"
																					id="instlmnt_stts${inst.id }">
																					<option value="true"
																						label="Convert Installment To Receipt">
																					<option value="false" selected="selected"
																						label="Pending">
																				</select>
																			</c:otherwise>
																		</c:choose></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>

										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<button type="button" onclick="saveInvoice()"
														class="btn btn-primary erp-btn ">Save</button>


												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="right">
													<a href="invoiceAdd.do">
														<button type="button" class="btn btn-primary erp-btn">New
															Invoice</button>
													</a>
													<c:if test="${! empty invoice.invoiceId }">
														<a href="generateInvoice.do?iid=${invoice.invoiceId }">
															<button type="button" class="btn btn-primary erp-btn">Generate
																Invoice</button>
														</a>
													</c:if>
												</div>
											</div>
										</div>

									</div>
									<!-- </div> -->
								</form:form>
							</c:otherwise>
						</c:choose>



					</div>
				</div>
			</div>
		</div>

		<div id="pdtPop" style="display: none;"></div>

		<div id="errror-msg" style="display: none;">
			<div class="modal " data-backdrop="static" data-keyboard="false"
				tabindex="-1" aria-hidden="false" style="display: block;"
				role="dialog">
				<div class="modal-dialog ">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Alert</h4>
						</div>
						<div class="modal-body"></div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<button type="button" onclick="closeError()"
									class="btn btn-default">Ok</button>
								<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="confirm-delete" style="display: none;">
			<div class="modal " style="display: block;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Confirmation</h4>
						</div>
						<div class="modal-body">Do you want to convert this
							installment to receipt?</div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<input type="hidden" id="delete-item">
								<button type="button" onclick="cancel()" class="btn btn-default">No</button>
								<button type="button" onclick="confirmedInvoiceReceipt()"
									class="btn btn-primary">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
	<%-- <c:if test="${! empty success_msg }"> --%>
	<div class="notification-panl successMsg" style="display: none">
		<div class="notfctn-cntnnt">${success_msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
	<%-- </c:if> --%>
	<c:if test="${! empty msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${msg }</div>
			<span id="close-msg" onclick="closeMessage()"><i
				class="fa fa-times"></i></span>
		</div>
	</c:if>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script type="text/javascript">
		
	</script>
	<%-- 	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script> --%>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<script src="<c:url value='/resources/assets/sales/js/sales.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts-validation.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<script type="text/javascript">
		$("document").ready(function() {
			if ($("#deliveryNoteIds").val() != '') {
				findTotalAmounts();
			}
		});
	</script>
</body>
</html>