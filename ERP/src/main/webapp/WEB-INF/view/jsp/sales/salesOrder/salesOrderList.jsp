<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">Code</th>
			<th width="10%">Customer</th>
			<th width="10%">Delivery Note</th>
			<th width="10%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${quotes }" var="po">
			<tr>
				<%-- <c:choose>
					<c:when test="${! empty po.noteCode }">
						<td><a href="viewSalesOrder.do?id=${po.orderId }">
								${po.orderCode }</a></td>
						<td>${po.customer }</td>
						<td><a href="viewDeliveryNote.do?id=${po.noteId}"><span
								class="badge ">${po.noteCode }</span></a></td>
						<td><i>Delivered</i></td>
					</c:when>
					<c:otherwise>
						<td><a href="viewSalesOrder.do?id=${po.orderId }">
								${po.orderCode }</a></td>
						<td>${po.customer }</td>
						<td><a href="salesOrderToDeliveryNote.do?id=${po.orderId}"><button
									class="btn ">Delivery Note</button></a></td>
						<td><i class="fa fa-times"
							onclick="deleteSalesOrder('${po.orderId}')"></i></td>
					</c:otherwise>
				</c:choose> --%>
				<td><a href="viewSalesOrder.do?id=${po.orderId }">
						${po.orderCode }</a></td>
				<td>${po.customer }</td>
				<td><c:forEach items="${po.deliveryNoteIdCode}" var="r">
						<a href="viewDeliveryNote.do?id=${r.key}"><span class="badge ">${r.value }</span></a>
					</c:forEach> <c:if test="${! po.hasDelivered }">
						<a href="salesOrderToDeliveryNote.do?id=${po.orderId}"><button
								class="btn-sm btn-primary right">Create</button></a>
					</c:if></td>
				<td><c:choose>
						<c:when test="${ po.hasDelivered }">
							<i>Completely Delivered</i>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${! empty po.deliveryNoteIdCode}">
									<i>Cannot Delete</i>
								</c:when>
								<c:otherwise>
									<i class="fa fa-times"
										onclick="deleteSalesOrder('${po.orderId}')"></i>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
</c:if>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/purchase/js/purchase.js' />"></script>