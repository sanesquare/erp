<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">Code</th>
			<th width="10%">Date</th>
			<th width="10%">Customer</th>
			<th width="10%">Amount</th>
			<th width="10%">Outstanding</th>
			<th width="10%">Status</th>
			<th width="10%">Delivery Note</th>
			<th width="10%">Return</th>
			<th width="10%">Pack</th>
			<th width="5%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${invoice }" var="b">
			<tr>
				<td><a href="viewInvoice.do?id=${b.invoiceId }">${b.invoiceCode }</a></td>
				<td><a href="viewInvoice.do?id=${b.invoiceId }">${b.invoiceDate }</a></td>
				<td><a href="viewInvoice.do?id=${b.invoiceId }">${b.customer }</a></td>
				<td><a href="viewInvoice.do?id=${b.invoiceId }">${b.grandTotal }</a></td>
				<td><a href="viewInvoice.do?id=${b.invoiceId }">${b.outStanding }</a></td>
				<td><c:choose>
						<c:when test="${b.isSettled }">
							<a href="viewInvoice.do?id=${b.invoiceId }">Settled</a>
						</c:when>
						<c:otherwise>
							<a href="viewInvoice.do?id=${b.invoiceId }">Pending</a>
						</c:otherwise>
					</c:choose></td>
				<td><c:choose>
						<c:when test="${b.isDelivered }">
							<a href="viewInvoice.do?id=${b.invoiceId }"><i>Delivered</i></a>
						</c:when>
						<c:otherwise>
							<a href="convertInvoiceToDeliveryNote.do?id=${b.invoiceId }">Create
								Delivery Note</a>
						</c:otherwise>
					</c:choose></td>
				<td><c:choose>
						<c:when test="${b.isReturned }">
							<a href="viewInvoice.do?id=${b.invoiceId }"><i>Returned</i> </a>
						</c:when>
						<c:otherwise>
							<a href="returnThisInvoice.do?invoiceId=${b.invoiceId }">Return</a>
						</c:otherwise>
					</c:choose></td>
				<td><c:choose>
						<c:when test="${b.isPacked}">
							<a href="viewInvoice.do?id=${b.invoiceId }"><i>Packed</i> </a>
						</c:when>
						<c:otherwise>
							<a href="packThisInvoice.do?invoiceId=${b.invoiceId }">Pack</a>
						</c:otherwise>
					</c:choose></td>
				<td><a onclick="deleteThisInvoice('${b.invoiceId }')"><i
						class="fa fa-times"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script src="<c:url value='/resources/assets/sales/js/sales.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
