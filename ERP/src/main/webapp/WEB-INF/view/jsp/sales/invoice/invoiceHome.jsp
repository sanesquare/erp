<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}
</style>
</head>
<body>
	<div class="sales_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">
					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">Invoices</div>
						<div class="panel-body">
							<div id="bill_panelBody">
								<table cellpadding="0" cellspacing="0" border="0"
									class="table table-striped table-bordered erp-tbl"
									id="basic-datatable">
									<thead>
										<tr>
											<th width="10%">Code</th>
											<th width="10%">Date</th>
											<th width="10%">Customer</th>
											<th width="10%">Amount</th>
											<th width="10%">Outstanding</th>
											<th width="10%">Status</th>
											<th width="10%">Delivery Note</th>
											<th width="10%">Return</th>
											<th width="10%">Pack</th>
											<th width="5%">Delete</th>

										</tr>
									</thead>
									<tbody>
										<c:forEach items="${invoice }" var="b">
											<tr>
												<td><a href="viewInvoice.do?id=${b.invoiceId }">${b.invoiceCode }</a></td>
												<td><a href="viewInvoice.do?id=${b.invoiceId }">${b.invoiceDate }</a></td>
												<td><a href="viewInvoice.do?id=${b.invoiceId }">${b.customer }</a></td>
												<td><a href="viewInvoice.do?id=${b.invoiceId }">${b.grandTotal }</a></td>
												<td><a href="viewInvoice.do?id=${b.invoiceId }">${b.outStanding }</a></td>
												<td><c:choose>
														<c:when test="${b.isSettled }">
															<a href="viewInvoice.do?id=${b.invoiceId }">Settled</a>
														</c:when>
														<c:otherwise>
															<a href="viewInvoice.do?id=${b.invoiceId }">Pending</a>
														</c:otherwise>
													</c:choose></td>
												<td><c:choose>
														<c:when test="${b.isDelivered }">
															<a href="viewInvoice.do?id=${b.invoiceId }"><i>Delivered</i></a>
														</c:when>
														<c:otherwise>
															<a
																href="convertInvoiceToDeliveryNote.do?id=${b.invoiceId }">Create
																Delivery Note</a>
														</c:otherwise>
													</c:choose></td>
												<td><c:choose>
														<c:when test="${b.isReturned }">
															<a href="viewInvoice.do?id=${b.invoiceId }"><i>Returned</i>
															</a>
														</c:when>
														<c:otherwise>
															<a href="returnThisInvoice.do?invoiceId=${b.invoiceId }">Return</a>
														</c:otherwise>
													</c:choose></td>
													<td><c:choose>
														<c:when test="${b.isPacked}">
															<a href="viewInvoice.do?id=${b.invoiceId }"><i>Packed</i>
															</a>
														</c:when>
														<c:otherwise>
															<a href="packThisInvoice.do?invoiceId=${b.invoiceId }">Pack</a>
														</c:otherwise>
													</c:choose></td>
												<td><a onclick="deleteThisInvoice('${b.invoiceId }')"><i
														class="fa fa-times"></i></a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<a href="invoiceAdd.do">
								<button type="button" class="btn btn-primary erp-btn">New
									Invoice</button>
							</a>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="confirm-delete" style="display: none;">
			<div class="modal " style="display: block;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Confirmation</h4>
						</div>
						<div class="modal-body">Do you really want to delete this
							item?</div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<input type="hidden" id="delete-item">
								<button type="button" onclick="cancel()" class="btn btn-default">No</button>
								<button type="button" onclick="deleteItem()"
									class="btn btn-primary">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>




		<c:if test="${! empty success_msg }">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${success_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</c:if>

		<div id="success-msg" style="display: none">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt"></div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</div>


		<div id="load-erp">
			<div id="spinneeer-erp">
				<span class="loading-text-erp">Loading.. </span><span
					class="spinr-erp"><i class="fa fa-spinner fa-spin "></i></span>
			</div>
		</div>

		<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
		<script src="<c:url value='/resources/assets/sales/js/sales.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
		<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
		<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
		<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
		<script type="text/javascript">
			
		</script>
</body>
</html>