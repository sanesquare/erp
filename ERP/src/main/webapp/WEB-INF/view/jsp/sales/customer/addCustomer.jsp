<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<style type="text/css">
#name_reqd {
	display: none;
}
</style>
</head>
<body>
	<div class="sales_home">
		<div class="warper container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Customer's Information <a href="customer.do"><span
								class="back-btn"><i class="fa fa-arrow-left"></i></span></a>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Basic Information</div>
										<div class="panel-body">
											<form:form action="saveCustomer.do" id="custom-frm"
												modelAttribute="customerVo" method="POST">
												<form:hidden id="cusm_id" path="customerId" />
												<c:if test="${! empty customerVo.customerCode }">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Customer
															Code<span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<form:input type="text" class="form-control paste"
																path="customerCode" id="cust_code" readonly="true"
																cssStyle="background-color:#fff !important" />
														</div>
													</div>
												</c:if>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Name<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input type="text" class="form-control paste"
															path="name" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Address<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:textarea path="address" type="text"
															style="resize: none;" class="form-control paste" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Delivery
														Address </label>
													<div class="col-sm-9">
														<form:textarea path="deliveryAddress" type="text"
															style="resize: none;" class="form-control paste" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Country<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select path="countryId" cssClass="form-control">
															<form:option value="" label="--Select Country--" />
															<form:options items="${countries }" itemLabel="name"
																itemValue="countryId" />

														</form:select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Mobile<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input type="text" class="form-control numbersonly"
															path="mobile" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Email<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input type="text" class="form-control paste"
															path="email" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Land
														Phone </label>
													<div class="col-sm-9">
														<form:input type="text" class="form-control numbersonly"
															path="phone" />
													</div>
												</div>
												<div class="form-group">
													<button type="submit" class="btn btn-primary erp-btn">Save</button>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
							<c:if test="${! empty customerVo.customerId }">
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Contacts</div>
											<div class="panel-body">
												<div id="customerCnts">
													<c:if test="${! empty customerVo.contactVos }">
														<table cellpadding="0" cellspacing="0" border="0"
															class="table table-striped table-bordered erp-tbl"
															id="basic-datatable">
															<thead>
																<tr>
																	<th width="30%">Name</th>
																	<th width="30%">Email</th>
																	<th width="30%">Work Phone</th>
																	<th width="10%">Edit</th>
																	<th width="10%">Delete</th>
																</tr>
															</thead>
															<tbody>
																<c:forEach items="${customerVo.contactVos }" var="vo">
																	<tr>
																		<td>${vo.name }</td>
																		<td>${vo.email }</td>
																		<td>${vo.phone }</td>
																		<td><a
																			onclick="editCustomerContact('${vo.customerContactId }','${vo.name }','${vo.email }','${vo.phone }','${vo.address }')"><i
																				class="fa fa-pencil"></i> </a></td>
																		<td><a
																			onclick="deleteContact('${vo.customerContactId }')"><i
																				class="fa fa-times"></i></a></td>
																	</tr>
																</c:forEach>
															</tbody>
														</table>
													</c:if>
												</div>
												<div class="form-group" style="padding-top: 2%;">
													<label for="inputPassword3" class="col-sm-3 control-label">Name<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<input type="text" class="form-control paste"
															id="cnt_name">
														<div id="name_reqd">Name is Required</div>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Address
													</label>
													<div class="col-sm-9">
														<textarea type="text" style="resize: none;"
															id="cnt_address" class="form-control paste"></textarea>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Email
													</label>
													<div class="col-sm-9">
														<input type="text" class="form-control paste"
															id="cnt_email">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Work
														Phone </label>
													<div class="col-sm-9">
														<input type="text" class="form-control numbersonly"
															id="cnt_phone">
													</div>
												</div>
												<input type="hidden" id="cnt_id" />
												<div class="form-group">
													<button type="button" id="save_cnt_btn"
														onclick="saveCustomer()" class="btn btn-primary erp-btn">Save
													</button>


													<button type="button" onclick="clearCustomerCntForm()"
														class="btn btn-primary erp-btn">Clear</button>
												</div>

											</div>
										</div>
									</div>
								</div>
							</c:if>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${! empty success_msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>

	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script type="text/javascript"
		src='<c:url value="/resources/assets/sales/js/sales.js"/>'></script>
	<%-- <script type="text/javascript"
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script> --%>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script type="text/javascript"
		src='<c:url value="/resources/assets/sales/js/sales-validation.js"/>'></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {

		});
	</script>
</body>
</html>