<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">Code</th>
			<th width="20%">Name</th>
			<th width="20%">Address</th>
			<th width="20%">Email</th>
			<th width="20%">Phone</th>
			<th width="5%">Edit</th>
			<th width="5%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:if test="${! empty customers }">
			<c:forEach items="${customers }" var="cus">
				<tr>
					<td>${cus.customerCode }</td>
					<td>${cus.name }</td>
					<td>${cus.address }</td>
					<td>${cus.email }</td>
					<td>${cus.phone }</td>
					<td><a href="editCustomer.do?id=${cus.customerId }"><i
							class="fa fa-pencil"></i> </a></td>
					<td><a  onclick="cnfrmDelete('${cus.customerId }')"><i class="fa fa-times"></i></a></td>
				</tr>
			</c:forEach>
		</c:if>


	</tbody>
</table>
<script type="text/javascript" src='<c:url value="/resources/assets/sales/js/sales.js"/>'></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>