<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
</head>
<body>
	<div class="sales_home">
		<div class="warper container-fluid">

			<div class="row">

				<c:choose>
					<c:when test="${! quote.isQuote }">
						<div class="panel-body">
							<div class="panel panel-default erp-panle">
								<div class="panel-heading  panel-inf">
									Customer Quote - ( ${quote.quoteCode } )<a
										href="customerQuotesHome.do"><span class="back-btn"><i
											class="fa fa-arrow-left"></i></span></a>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Customer
												</label>
												<div class="col-sm-9">
													<label>${quote.customer }</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Expiry
													Date </label>
												<div class="col-sm-9">
													<div class='input-group date billDate' id="datepickers">
														<label>${quote.expiryDate }</label>
													</div>
												</div>
											</div>
										</div>

									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Date
												</label>
												<div class="col-sm-9">
													<label>${quote.orderDate }</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Currency
												</label>
												<div class="col-sm-9">
													<label>${quote.currency }</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Payment
													Term </label>
												<div class="col-sm-9">
													<label>${quote.paymentTerm }</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Delivery
													Term </label>
												<div class="col-sm-9">
													<label>${quote.deliveryTerm }</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="panel-body">
											<div class="col-sm-12">
												<c:if test="${! empty quote.orderCode }">
													<a href="viewSalesOrder.do?id=${quote.orderId }"
														class="badge">${quote.orderCode }</a>
												</c:if>
												<c:if test="${! empty quote.noteCode }">
													<a style="margin-left: 1%;"
														href="viewDeliveryNote.do?id=${quote.noteId }"
														class="badge">${quote.noteCode }</a>
												</c:if>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="panel-body tbl_scrl_bdy">
										<div class="col-sm-12">
											<table cellpadding="0" cellspacing="0" border="0"
												class="table table-striped table-bordered erp-tbl prdct_tbl">
												<thead>
													<tr>
														<th width="10%">Product Code</th>
														<th width="20%">Item</th>
														<th width="10%">Quantity</th>
														<th width="10%">Unit Price</th>
														<th width="10%">Discount %</th>
														<th width="10%">Net Amount</th>
														<th width="10%">Tax</th>
														<th width="10%">Total</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${quote.productVos }" var="p">
														<tr class="productTableRow" id="${p.productCode }">
															<td style="padding: .5%;">${p.productCode }</td>
															<td style="padding: .5%;">${p.productName }</td>
															<td style="padding: .5%;">${p.quantity }</td>
															<td style="padding: .5%;">${p.unitPrice }</td>
															<td style="padding: .5%;">${p.discount }</td>
															<td style="padding: .5%;">${p.amountExcludingTax }</td>
															<td style="padding: .5%;">${p.tax }</td>
															<td style="padding: .5%;">${p.netAmount }</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-8"></div>
									<div class="col-sm-4">
										<div class="form-group">
											<div class="col-sm-6">
												<label>Sub Total</label>
											</div>
											<div class="col-sm-6">
												<label id="subTotal">${quote.subTotal }</label>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-6">
												<label>Discount Rate</label>
											</div>
											<div class="col-sm-6">
												<label>${quote.discountRate }</label>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-6">
												<label>Discount Amount</label>
											</div>
											<div class="col-sm-6">
												<label id="disTotal">${quote.discountTotal }</label>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-6">
												<label>Gross Total</label>
											</div>
											<div class="col-sm-6">
												<label id="netTotal">${quote.netTotal }</label>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-6">
												<label>Tax: </label>
											</div>
											<div class="col-sm-6">
												<c:forEach var="tax" items="${quote.taxes }">
													<label>${t }</label>
												</c:forEach>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-6">
												<label>Tax Total</label>
											</div>
											<div class="col-sm-6">
												<label id="taxTotal">${quote.taxTotal }</label>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-6">
												<label>Grand Total</label>
											</div>
											<div class="col-sm-6">
												<label id="grandTotal">${quote.grandTotal }</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="right">
											<a href="addCustomerQuote.do">
												<button type="button" class="btn btn-primary erp-btn">New
													Customer Quote</button>
											</a>
											<c:if test="${! empty quote.quoteId }">
												<a href="generateCustomerQuotePdf.do?id=${quote.quoteId }">
													<button type="button" class="btn btn-primary erp-btn">Generate
														PDF</button>
												</a>
											</c:if>
										</div>
									</div>
								</div>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="panel-body">
							<div class="panel panel-default erp-panle">
								<div class="panel-heading  panel-inf">
									Customer Quote ${quote.quoteCode }<a
										href="customerQuotesHome.do"><span class="back-btn"><i
											class="fa fa-arrow-left"></i></span></a>
								</div>
								<form:form action="#" method="POST" commandName="quote">
									<div class="panel-body">
										<form:hidden path="orderId" id="vendorQuoteId" />
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Customer<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select class="form-control" id="bill_vendr"
															path="customerId">
															<option value="" label="--Select Customer--"></option>
															<c:forEach items="${customers }" var="v">
																<form:option value="${v.customerId }" label="${v.name }"></form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Expiry
														Date<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class='input-group date billDate' id="datepickers">
															<form:input type="text" readonly="true" path="expiryDate"
																id="vqte_expiry" value="${quote.expiryDate }"
																style="background-color: #ffffff !important;"
																data-date-format="DD/MM/YYYY" class="form-control" />
															<span class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>
												</div>
											</div>

										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Date<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class='input-group date billDate' id="datepicker">
															<form:input type="text" readonly="true" path="orderDate"
																id="bill-date" value="${quote.orderDate }"
																style="background-color: #ffffff !important;"
																data-date-format="DD/MM/YYYY" class="form-control" />
															<span class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Currency<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<select class="form-control" id="currencySelBill">
															<option value="" label="--Select Currency--"></option>
															<c:forEach items="${currencies }" var="t">
																<c:choose>
																	<c:when test="${t.erpCurrencyId eq currencyId }">
																		<option value="${t.erpCurrencyId }"
																			selected="selected" label="${t.currency }"></option>
																	</c:when>
																	<c:otherwise>
																		<option value="${t.erpCurrencyId }"
																			label="${t.currency }"></option>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Payment
														Term<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select path="paymentTermId" id="paymentTermSelect"
															cssClass="form-control">
															<c:forEach var="t" items="${terms }">
																<form:option value="${t.id }" label="${t.name }"></form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Delivery
														Term </label>
													<div class="col-sm-9">
														<form:select path="deliveryTermId" id="deliveryTermSelect"
															cssClass="form-control">
															<option value="" label="--Select Delivery Term--"></option>
															<c:forEach var="t" items="${deliveryTerms }">
																<form:option value="${t.deliveryMethodId }"
																	label="${t.method }"></form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="panel-body tbl_scrl_bdy">
												<div class="col-sm-12">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl prdct_tbl">
														<thead>
															<tr>
																<th width="10%">Product Code</th>
																<th width="20%">Item</th>
																<th width="10%">Quantity</th>
																<th width="10%">Unit Price</th>
																<th width="10%">Discount %</th>
																<th width="10%">Net Amount</th>
																<th width="10%">Tax</th>
																<th width="10%">Total</th>
																<th width="10%">Delete</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${quote.productVos }" var="p">
																<tr class="productTableRow" id="${p.productCode }">
																	<td style="padding: .5%;"><label
																		id="prod_code${p.productCode }">${p.productCode }</label>
																		<input type="hidden" class="prodctId"
																		value="${p.productId }"></td>
																	<td style="padding: .5%;"><label
																		id="prod_name${p.productCode }">${p.productName }</label></td>
																	<td style="padding: .5%;"><input type="text"
																		value="${p.quantity }"
																		onblur="findNetAmount('${p.productCode }')"
																		class="form-control double "
																		id="product_qty${p.productCode }" /></td>
																	<td style="padding: .5%;"><input type="text"
																		value="${p.unitPrice }"
																		onblur="findNetAmount('${p.productCode }')"
																		class="form-control double "
																		id="product_prc${p.productCode }" /></td>
																	<td style="padding: .5%;"><input type="text"
																		value="${p.discount }"
																		onblur="findNetAmount('${p.productCode }')"
																		class="form-control double"
																		id="product_disc${p.productCode }" /></td>
																	<td style="padding: .5%;"><label
																		id="prod_tot${p.productCode }">${p.amountExcludingTax }</label></td>
																	<td style="padding: .5%;"><select
																		class="form-control"
																		onchange="findNetAmount('${p.productCode }')"
																		id="product_tx${p.productCode }">
																			<option value="" label="--Select Tax--"></option>
																			<c:forEach items="${taxes }" var="tx">
																				<c:choose>
																					<c:when test="${p.tax eq tx.taxName }">
																						<option value="${tx.id }!!!${tx.rate}"
																							selected="selected" label="${tx.taxName }"></option>
																					</c:when>
																					<c:otherwise>
																						<option value="${tx.id }!!!${tx.rate}"
																							label="${tx.taxName }"></option>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																	</select></td>
																	<td style="padding: .5%;"><label
																		id="product_net${p.productCode }">${p.netAmount }</label></td>
																	<td style="padding: .5%;"><i
																		onclick="javascript:deleteProductRow(this)"
																		class="fa fa-times"></i></td>
																</tr>
															</c:forEach>
															<tr>
																<td colspan="9"><button type="button"
																		onclick="getProductDetails()"
																		class="btn btn-primary erp-btn right">Add
																		Item</button></td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>

										<div class="row">
											<input type="hidden" value="0.00" id="netAmountHidden">
											<input value="0.00" type="hidden" id="taxAmountHidden">
											<div class="col-sm-8"></div>
											<div class="col-sm-4">
												<div class="form-group">
													<div class="col-sm-6">
														<label>Sub Total</label>
													</div>
													<div class="col-sm-6">
														<label id="subTotal">${quote.subTotal }</label>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Discount Rate</label>
													</div>
													<div class="col-sm-6">
														<input type="text" onblur="findTotalAmounts()"
															class="myText double" value="${quote.discountRate }"
															id="discRate">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Discount Amount</label>
													</div>
													<div class="col-sm-6">
														<label id="disTotal">${quote.discountTotal }</label>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Gross Total</label>
													</div>
													<div class="col-sm-6">
														<label id="netTotal">${quote.netTotal }</label>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Tax: </label>
													</div>
													<div class="col-sm-6">
														<form:select multiple="multiple" path="taxesWithRate"
															data-placeHolder="-Select Taxes-"
															onchange="findTotalAmounts()" id="taxesMulti"
															class="form-control chosen-select">
															<c:forEach items="${taxes }" var="t">
																<form:option value="${t.id}!!!${t.rate }">${t.taxName }</form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Tax Total</label>
													</div>
													<div class="col-sm-6">
														<label id="taxTotal">${quote.taxTotal }</label>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-6">
														<label>Grand Total</label>
													</div>
													<div class="col-sm-6">
														<label id="grandTotal">${quote.grandTotal }</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<button type="button" onclick="saveCustomerQuote()"
														class="btn btn-primary erp-btn ">Save</button>


												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="right">
													<a href="addCustomerQuote.do">
														<button type="button" class="btn btn-primary erp-btn">New
															Customer Quote</button>
													</a>
													<c:if test="${! empty quote.quoteId }">
														<a href="generateCustomerQuotePdf.do?id=${quote.quoteId }">
															<button type="button" class="btn btn-primary erp-btn">Generate
																PDF</button>
														</a>
													</c:if>
												</div>
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>

		<div id="pdtPop" style="display: none;"></div>

		<div id="errror-msg" style="display: none;">
			<div class="modal " data-backdrop="static" data-keyboard="false"
				tabindex="-1" aria-hidden="false" style="display: block;"
				role="dialog">
				<div class="modal-dialog ">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Alert</h4>
						</div>
						<div class="modal-body"></div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<button type="button" onclick="closeError()"
									class="btn btn-default">Ok</button>
								<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
	<%-- <c:if test="${! empty success_msg }"> --%>
	<div class="notification-panl successMsg" style="display: none">
		<div class="notfctn-cntnnt">${success_msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
	<%-- </c:if> --%>
	<c:if test="${! empty msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${msg }</div>
			<span id="close-msg" onclick="closeMessage()"><i
				class="fa fa-times"></i></span>
		</div>
	</c:if>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script type="text/javascript">
		
	</script>
	<%-- <script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script> --%>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>

	<script src="<c:url value='/resources/assets/sales/js/sales.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts-validation.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>

	<script type="text/javascript">
		
	</script>
</body>
</html>