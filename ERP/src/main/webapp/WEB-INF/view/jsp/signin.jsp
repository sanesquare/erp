<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ERP Login</title>
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>
<!-- Bootstrap core CSS -->
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />
<!-- Fonts  -->
<link type="text/css" rel="stylesheet"
	href="<c:url value='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'/>" />
<!-- Base Styling  -->
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />
</head>
<body>
	<div class="logo_strip">
		<div class="col-md-3 col-md-offset-1">
			<div class="logo_imgs">
				<!-- <img src="resources/images/3g_logo.png" /> -->
			</div>
		</div>
	</div>
	<div class="containers">
		<div class="row">

			<div class="col-md-12">
				<div class="col-md-6  col-md-offset-1">
					<div class="col-xs-12">
						<img src="resources/assets/img/ERP1.png" class="main_pic" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="log_full_bg">
						<h3 class="text-center">
							<strong>LOGIN</strong>
						</h3>
						<p class="text-center">
							Sign in to get in touch <br> <br>
						</p>
						<hr class="clean">
						<form role="form" name='loginForm'
							action="<c:url value='/j_spring_security_check' />" method='POST'>
							<div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="text" class="form-control" placeholder="User name"
									name="username" id="username">
							</div>
							<div class="form-group input-group">
								<span class="input-group-addon"><i class="fa fa-key"></i></span>
								<input type="password" class="form-control"
									placeholder="Password" name="password" id="password">
							</div>
							<div class="form-group">
								<br> <label class="cr-styled"> <input
									type="checkbox" ng-model="todo.done"> <i class="fa"></i>
								</label> Remember me<br> <br>
							</div>
							<button type="submit" class="btn btn-info btn-block"
								style="float: left;">Sign in</button>
							<span class="login_status"><c:if
									test="${not empty status}">
									<div class="scs_ok">${status}</div>
								</c:if></span>
						</form>
						<!-- <hr>
						<p class="text-center text-gray">Dont have account yet!</p>
						<br>
						<button type="submit" class="btn btn-default btn-block">Create
							Account</button> -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<%-- <div class="container">
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4">
				<h3 class="text-center">HRMS</h3>
				<p class="text-center">Sign in to get in touch</p>
				<hr class="clean">
				<form role="form" name='loginForm'
					action="<c:url value='/j_spring_security_check' />" method='POST'>
					<div class="form-group input-group">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						<input type="text" class="form-control" placeholder="Email Adress"
							name="username" id="username">
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon"><i class="fa fa-key"></i></span> <input
							type="password" name="password" id="password"
							class="form-control" placeholder="Password">
					</div>
					<div class="form-group">
						<label class="cr-styled"> <input type="checkbox"
							ng-model="todo.done"> <i class="fa"></i>
						</label> Remember me
					</div>
					<button type="submit" class="btn btn-purple btn-block">Sign
						in</button>
					<span><c:if test="${not empty status}">
							<div class="scs_ok">${status}</div>
						</c:if></span>
				</form>
				<hr>
				<a href="Dashboard">No Thanks! Take me in.</a>
			</div>
		</div>
	</div> --%>
	<!-- <div id="notify-all">
		<div class="notification-panl">
			<div class="notfctn-cntnnt">Content</div>
			<span id="cloese"><i class="fa fa-times"></i></span>
		</div>
	</div> -->
	<!-- <div id="loading">
	</div> -->
	<!-- JQuery v1.9.1 -->
	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>
	<!-- Bootstrap -->
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>

	<!-- Globalize -->
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>

	<!-- NanoScroll -->
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		});
		$("#test").click(function() {
			$("#notify-all").css("display", "block");
			setTimeout(function() {
				$("#notify-all").fadeOut(600);
			}, 3500);
		});
		$("#cloese").click(function() {
			$("#notify-all").fadeOut(600);
		});
		
		$(window).load(function() {
		   $('#loading').hide();
		 });
	</script>
</body>

</html>


<%--   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
 <!DOCTYPE html>
<html>
<head>
    <title>Hello WebSocket</title>
    <script src="<c:url value='/resources/js/sockjs-0.3.4.js'/>"></script>
    <script src="<c:url value='/resources/js/stomp.js' />"></script>
    <script type="text/javascript">
        var stompClient = null;
        
        function setConnected(connected) {
            document.getElementById('connect').disabled = connected;
            document.getElementById('disconnect').disabled = !connected;
            document.getElementById('conversationDiv').style.visibility = connected ? 'visible' : 'hidden';
            document.getElementById('response').innerHTML = '';
        }
        
        function connect() {
            var socket = new SockJS('/HRMS/webs');
            stompClient = Stomp.over(socket);            
            stompClient.connect({}, function(frame) {
                setConnected(true);
                console.log('Connected: ' + frame);
                stompClient.subscribe('/HRMS/topic/greetings', function(greeting){
                    showGreeting(JSON.parse(greeting.body).content);
                });
            });
        }
        
        function disconnect() {
            if (stompClient != null) {
                stompClient.disconnect();
            }
            setConnected(false);
            console.log("Disconnected");
        }
        
        function sendName() {
            var name = document.getElementById('name').value;
            stompClient.send("/HRMS/webs", {}, JSON.stringify({ 'name': name }));
        }
        
        function showGreeting(message) {
            var response = document.getElementById('response');
            var p = document.createElement('p');
            p.style.wordWrap = 'break-word';
            p.appendChild(document.createTextNode(message));
            response.appendChild(p);
        }
    </script>
</head>
<body onload="disconnect()">
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being enabled. Please enable
    Javascript and reload this page!</h2></noscript>
<div>
    <div>
        <button id="connect" onclick="connect();">Connect</button>
        <button id="disconnect" disabled="disabled" onclick="disconnect();">Disconnect</button>
    </div>
    <div id="conversationDiv">
        <label>What is your name?</label><input type="text" id="name" />
        <button id="sendName" onclick="sendName();">Send</button>
        <p id="response"></p>
    </div>
</div>
</body>
</html>   --%>