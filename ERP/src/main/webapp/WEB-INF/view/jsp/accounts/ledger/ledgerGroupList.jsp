<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
<span onclick="closePop()" class="pull-right closebtn"></span>
<div class="panel panel-default erp-info" style="border-radius: 4px">
	<div class="panel-heading panel-inf">Ledger Group Information</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<%-- <form action="" id="ledger-group-form"> --%>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Ledger
						Group Name </label>
					<div class="col-sm-9">
						<i class="form-control-feedback fv-icon-no-label glyphicon glyphicon-remove nme-requed"
							style="display: none; color: #D01515;"></i> <input
							style="background-color: #fff !important" name="name" onfocus="closeNmeRqurd()"
							id="ledgerGroupName" class="form-control paste" /><small
							class="help-block nme-requed"
							style="display: none; color: #D01515;">Name is required</small>
					</div>
				</div>
				<input type="hidden" id="ledgerGroupIdHidden">
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Account
						Type <span class="stars">*</span>
					</label>
					<div class="col-sm-9">
					<i class="form-control-feedback fv-icon-no-label glyphicon glyphicon-remove type-requed"
							style="display: none; color: #D01515;"></i>
						<select class="form-control" name="accountType" onfocus="closeTypeRqurd()"
							id="ledgerGroupAcntTyp">
							<option label="--Select Account Type -- " value="" />
							<c:forEach items="${accountTypes }" var="a">
								<option value="${a.typeId }" label="${a.accountType }" />
							</c:forEach>
						</select>
						<small
							class="help-block type-requed"
							style="display: none; color: #D01515;">Account type is required</small>
					</div>
				</div>

				<div class="form-group">
					<button type="button" onclick="saveLedgerGroup()"
						class="btn btn-primary erp-btn">Save</button>
				</div>
				<%-- </form> --%>
			</div>
		</div>
		<div class="row"
			style="border-top: dotted #fff 1px; margin-top: 2%; padding-top: 2%;">
			<div class="col-md-12" style="height: 200px; overflow-x: scroll;"
				id="ledg-group-table">
				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered erp-tbl"
					id="basic-datatable">
					<thead>
						<tr>
							<th width="30%">Account Type</th>
							<th width="20%">Ledger Group Code</th>
							<th width="30%">Ledger Group Name</th>
							<th width="10%">Edit</th>
							<th width="10%">Delete</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${groups }" var="g">
							<tr>
								<td>${g.accounttype }</td>
								<td>${g.groupCode }</td>
								<td>${g.groupName }</td>
								<c:choose>
								<c:when test="${g.isEditable }">
										<td><span
									onclick='editThisLedgerGroup("${g.groupName }","${g.groupId }","${g.accountTypeId }")'><i
										class='fa fa-pencil'></i></span></td>
								<td><span onclick='deleteThisLedgerGroup("${g.groupId }")'><i
										class='fa fa-times'></i></span></td>								
								</c:when>
								<c:otherwise>
									<td><i>Default</i></td>
									<td><i>Default</i></td>
								</c:otherwise>
								</c:choose>
								
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>


	</div>
</div>
<div class="notification-panl successMsg" style="display: none;">
	<div class="notfctn-cntnnt">${success_msg }</div>
	<span id="close-msg" onclick="closeMessage()"><i
		class="fa fa-times"></i></span>
</div>
<script
	src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
