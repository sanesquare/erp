<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="modal prodModal" data-backdrop="static"
	data-keyboard="false" tabindex="-1" aria-hidden="false"
	style="display: block;" role="dialog">
	<div class="prodModal-dialogue">
		<div class="modal-content"
			style="max-height: 500px; min-width: 200%; overflow: scroll;"
			id="pop_body">
			<div class="modal-header">
				<h4 class="modal-title">
					Group Summary - ${group.ledgerGroup }<span class="back-btn"><i
						class="fa fa-times" onclick="closeMe()"></i></span>
				</h4>
			</div>
			<div class="modal-body">
				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered erp-tbl blnc_shet_tbl">
					<thead>
						<tr>
							<th width="55%" style="text-align: center;">Particulars</th>
							<th width="15%" style="text-align: center;">Debit</th>
							<th width="15%" style="text-align: center;">Credit</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${group.ledgerViewVos }" var="l">
							<tr>
								<td><a
									onclick="getLedgerMonthlyViewPop('${l.ledgerId}',false,'${startDate }','${endDate }')">${l.ledgerName }</a></td>
								<td style="text-align: right;"><c:if test="${l.closingDebit != zero}">${l.closingDebit }</c:if></td>
								<td style="text-align: right;"><c:if test="${l.closingCredit!= zero}">${l.closingCredit }</c:if></td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th style="text-align: right;"><b><c:if
										test="${group.totalDebit!= zero}">${group.totalDebit }</c:if></b></th>
							<th style="text-align: right;"><b><c:if
										test="${group.totalCredit!= zero}">${group.totalCredit }</c:if></b></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>