<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tr>
	<td style="padding: .5%;"><select class="form-control typesss" id="accTypePayment${count }">
			<option value="" label="-select account-" />
			<c:forEach items="${ledgers }" var="l" >
				<option value="${l.ledgerId }" label="${l.ledgerName }"></option>
			</c:forEach>
	</select></td>
	<td style="padding: .5%;">
	<input type="hidden" value="" id="idPayment${count }">
	<input type="text" onblur="addTotalDebitPayment('${count }')"
		class="form-control align-right double" value="${rowDebit }" id="debitPayment${count }"/><input type="hidden" value="0.00" id="debitPayment${count }hidden"> </td>
	<td style="padding: .5%;"><input type="text" onblur="addTotalCreditPayment('${count }')" 
		class="form-control align-right double" value="${rowCredit }" id="creditPayment${count }"/><input type="hidden" value="0.00" id="creditPayment${count }hidden"></td>
	<td style="padding: .5%;"><span class="deleteRow"><i onclick="javascript:deleteRowPayment(this,'${count}'); return false;"
			class="fa fa-times"></i></span></td>
</tr>
<tr>
	<td style="padding: .5%;">
		<button type="button" onclick="addRowPayment()"
			class="btn btn-primary erp-btn">Add Row</button>
			<span style="float: right"><strong>Total:</strong></span>
	</td>
	<td style="padding: .5%;"><input type="text" readonly="readonly" class="form-control align-right" id="totalDebitPayment" value="${debit }"></td>
	<td style="padding: .5%;"><input type="text" readonly="readonly" class="form-control align-right" id="totalCreditPayment" value="${credit }"></td>
	<td></td>
</tr>
<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
