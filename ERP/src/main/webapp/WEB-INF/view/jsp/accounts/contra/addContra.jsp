<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%-- <link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" /> --%>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />

<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />

</head>
<body>


<div class="accounts_home">
		<div class="warper container-fluid">

			<div class="row">
				<form:form action="saveJournal.do" modelAttribute="contraVo"
					method="POST" id="cntra_frm_new">
					<form:hidden path="contraId" id="contraId" />
					<input type="hidden" value="${startDate }" name="startDate">
					<input type="hidden" value="${endDate }" name="endDate">
					<div class="col-md-12">

						<div class="panel panel-default erp-panle">
							<div class="panel-heading  panel-inf">
								Contra<a href="contraHome.do"><span class="back-btn"><i
										class="fa fa-arrow-left"></i></span></a>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
									<c:if test="${! empty contraVo.voucherNumber }">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Contra
											</label>
											<div class="col-sm-9">
												<form:label for="inputPassword3"
													class="col-sm-3 control-label" path="voucherNumber"
													id="cntraVoucher">${contraVo.voucherNumber }</form:label>
											</div>
										</div></c:if>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Date
											</label>
											<div class="col-sm-9">
												<div class='input-group date cmp_fr jrnl-date'
													id="datepicker">
													<form:input type="text" path="date" readonly="true"
														name="periodStarts" id="cntraDate"
														style="background-color: #ffffff !important;"
														data-date-format="DD/MM/YYYY" class="form-control" />
													<span class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span> </span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="padding: 1% 1% 1% 1%;">
								<div class="col-md-12">
									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Entries</div>
										<div class="panel-body" id="contraTableBody">
											<table cellpadding="0" cellspacing="0" border="0"
												class="table table-striped table-bordered erp-tbl"
												id="contraEntries">
												<thead>
													<tr>
														<th width="30%">Account</th>
														<th width="30%">Debit</th>
														<th width="30%">Credit</th>
														<th width="10%"></th>
													</tr>
												</thead>
												<tbody>
													<c:choose>
														<c:when test="${contraVo.edit }">
															<c:forEach items="${contraVo.entries }" var="e"
																varStatus="s">
																<input type="hidden" value="${e.id }"
																	id="idContra${s.count }">
																<tr>
																	<td><select class="form-control typesss"
																		id="accTypeContra${s.count }">
																			<option value="" label="-select account-" />
																			<c:forEach items="${ledgers }" var="l">
																				<c:choose>
																					<c:when test="${l.ledgerId == e.accTypeId}">
																						<option value="${l.ledgerId }" selected="selected"
																							label="${l.ledgerName }"></option>
																					</c:when>
																					<c:otherwise>
																						<option value="${l.ledgerId }"
																							label="${l.ledgerName }"></option>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																	</select></td>
																	<td><input type="text"
																		onblur="addTotalDebitContra('${s.count }')"
																		class="form-control align-right double tt"
																		value="${e.debitAmnt }" id="debitContra${s.count }" /><input
																		type="hidden" value="${e.debitAmnt }"
																		id="debitContra${s.count }hidden"></td>
																	<td><input type="text"
																		onblur="addTotalCreditContra('${s.count }')"
																		class="form-control align-right double"
																		value="${e.creditAmnt }" id="creditContra${s.count }" /><input
																		type="hidden" value="${e.creditAmnt }"
																		id="creditContra${s.count }hidden"></td>
																	<td><span class="deleteRow"><i
																			onclick="javascript:deleteRowContra(this,'${s.count }'); return false;"
																			class="fa fa-times"></i></span></td>
																</tr>

															</c:forEach>
															<tr>
																<td>
																	<button type="button"
																		onclick="javascript:addRowContra(this); return false;"
																		class="btn btn-primary erp-btn">Add Row</button> <span
																	style="float: right"><strong>Total:</strong></span>
																</td>
																<td><input type="text" readonly="readonly"
																	class="form-control align-right" id="totalDebitContra"
																	value="${contraVo.amount }"></td>
																<td><input type="text" readonly="readonly"
																	class="form-control align-right"
																	id="totalCreditContra" value="${contraVo.amount }"></td>
																<td></td>
															</tr>
															<input type="hidden" id="totalCreditHiddenContra"
																value="${contraVo.amount }" />
															<input type="hidden" id="totalDebitHiddenContra"
																value="${contraVo.amount }" />
														</c:when>
														<c:otherwise>
															<tr>
																<td><select class="form-control typesss"
																	id="accTypeContra1">
																		<option value="" label="-select account-" />
																		<c:forEach items="${ledgers }" var="l">
																			<option value="${l.ledgerId }"
																				label="${l.ledgerName }"></option>
																		</c:forEach>
																</select></td>
																<td><input type="hidden" value="" id="idContra1"><input
																	type="text" onblur="addTotalDebitContra('1')"
																	class="form-control align-right double tt" value="0.00"
																	id="debitContra1" /><input type="hidden" value="0.00"
																	id="debitContra1hidden"></td>
																<td><input type="text" onblur="addTotalCreditContra('1')"
																	class="form-control align-right double" value="0.00"
																	id="creditContra1" /><input type="hidden" value="0.00"
																	id="creditContra1hidden"></td>
																<td><span class="deleteRow"><i
																		onclick="javascript:deleteRowContra(this,'1'); return false;"
																		class="fa fa-times"></i></span></td>
															</tr>
															<tr>
																<td>
																	<button type="button"
																		onclick="javascript:addRowContra(this); return false;"
																		class="btn btn-primary erp-btn">Add Row</button> <span
																	style="float: right"><strong>Total:</strong></span>
																</td>
																<td><input type="text" readonly="readonly"
																	class="form-control align-right" id="totalDebitContra"
																	value="0.00"></td>
																<td><input type="text" readonly="readonly"
																	class="form-control align-right" id="totalCreditContra"
																	value="0.00"></td>
																<td></td>
															</tr>
															<input type="hidden" id="totalCreditHiddenContra" value="0.00" />
															<input type="hidden" id="totalDebitHiddenContra" value="0.00" />
														</c:otherwise>
													</c:choose>
												</tbody>
											</table>
											<!-- <div class="form-group">
											<button type="button" onclick="addRow()" class="btn btn-primary erp-btn">Add
												Row</button>
										</div> -->
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Narration
												</label>
												<div class="col-sm-4">
													<form:textarea cssClass="form-control" id="cntraNrrtin"
														path="narration" style="resize: none;" />
												</div>
											</div>
										</div>
										<div class="form-group">
											<button type="button" onclick="saveContra()"
												class="btn btn-primary erp-btn">Save Contra</button>
										</div>
										<div class=" right">
											<a href="addLedger.do">
												<button type="button" class="btn btn-primary erp-btn">New
													Ledger</button>
											</a> <a href="addContra.do">
												<button type="button" class="btn btn-primary erp-btn">New
													Contra</button>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form:form>
			</div>

		</div>
	</div>
	<div id="errror-msg" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Alert</h4>
					</div>
					<div class="modal-body"></div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="closeError()"
								class="btn btn-default">ok</button>
							<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${! empty success_msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	
	<div class="notification-panl successMsg" style="display: none">
		<div class="notfctn-cntnnt">${success_msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>

<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>

	<div class="modal " data-backdrop="static" data-keyboard="false"
		tabindex="-1" aria-hidden="false" style="display: none;" role="dialog"
		id="ledgerGroupPop">
		<div class="pop-content"></div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts-validation.js' />"></script>


	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<!-- DateTime Picker -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
</body>
</html>