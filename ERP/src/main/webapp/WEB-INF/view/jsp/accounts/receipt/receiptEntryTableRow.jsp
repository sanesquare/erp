<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tr>
	<td style="padding: .5%;"><select class="form-control typesss" id="accTypeReceipt${count }">
			<option value="" label="-select account-" />
			<c:forEach items="${ledgers }" var="l" >
				<option value="${l.ledgerId }" label="${l.ledgerName }"></option>
			</c:forEach>
	</select></td>
	<td style="padding: .5%;">
	<input type="hidden" value="" id="idReceipt${count }">
	<input type="text" onblur="addTotalDebitReceipt('${count }')"
		class="form-control align-right double" value="${rowDebit }" id="debitReceipt${count }"/><input type="hidden" value="0.00" id="debitReceipt${count }hidden"> </td>
	<td style="padding: .5%;"><input type="text" onblur="addTotalCreditReceipt('${count }')" 
		class="form-control align-right double" value="${rowCredit }" id="creditReceipt${count }"/><input type="hidden" value="0.00" id="creditReceipt${count }hidden"></td>
	<td style="padding: .5%;"><span class="deleteRow"><i onclick="javascript:deleteRowReceipt(this,'${count}'); return false;"
			class="fa fa-times"></i></span></td>
</tr>
<tr>
	<td style="padding: .5%;">
		<button type="button" onclick="addRowReceipt()"
			class="btn btn-primary erp-btn">Add Row</button>
			<span style="float: right"><strong>Total:</strong></span>
	</td>
	<td style="padding: .5%;"><input type="text" readonly="readonly" class="form-control align-right" id="totalDebitReceipt" value="${debit }"></td>
	<td style="padding: .5%;"><input type="text" readonly="readonly" class="form-control align-right" id="totalCreditReceipt" value="${credit }"></td>
	<td></td>
</tr>
<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
