<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="modal-header">
	<h4 class="modal-title">
		Journal -${vo.voucherNumber } ( ${vo.date } )<span class="back-btn">
			<span
			onclick="getLedgerViewBack('${monthlyId}','month','${startDate }','${endDate }')"
			class="fa fa-arrow-left back"></span> <i class="fa fa-times"
			onclick="closeMe()"></i>
		</span>
	</h4>
</div>
<div class="modal-body">
	<div class="row" style="padding: 1% 1% 1% 1%;">
		<div class="col-md-12">
			<table cellpadding="0" cellspacing="0" border="0"
				class="table table-striped table-bordered erp-tbl"
				id="contraEntries">
				<thead>
					<tr>
						<th width="30%">Account</th>
						<th width="30%">Debit</th>
						<th width="30%">Credit</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${vo.entries }" var="e" varStatus="s">
						<tr>
							<td>${e.ledger }</td>
							<td>${e.debitAmnt }</td>
							<td>${e.creditAmnt }</td>
						</tr>

					</c:forEach>
					<tr>
						<td></td>
						<td><b>${vo.amount }</b></td>
						<td><b>${vo.amount }</b></td>
					</tr>
				</tbody>
			</table>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">Narration
				</label>

				<div class="col-sm-4">
					<label>${vo.narration }</label>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>