<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0" 
	class="table table-striped table-bordered erp-tbl" id="basic-datatableqqqqqq">
	<thead>
		<tr>
			<!-- <th width="10%">Code</th> -->
			<th width="20%">Ledger Name</th>
			<th width="20%">Ledger Group</th>
			<th width="20%">Account Type</th>
			<th width="20%">Created By</th>
			<th width="5%">Edit</th>
			<th width="5%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${ledgers }" var="v">
			<tr>
				<!-- <td></td> -->
				<td>${v.ledgerName }</td>
				<td>${v.ledgerGroup }</td>
				<td>${v.accountType }</td>
				<td>${v.createdBy }</td>
				<td><a href="editLedger.do?id=${v.ledgerId }"><i
						class="fa fa-pencil"></i> </a></td>
				<td><a href="#?id=${v.ledgerId }"
					onclick="cnfrmDelete('${v.ledgerId }')"><i class="fa fa-times"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
