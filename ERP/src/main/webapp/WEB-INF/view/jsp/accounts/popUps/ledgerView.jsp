<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="modal prodModal" data-backdrop="static"
	data-keyboard="false" tabindex="-1" aria-hidden="false"
	style="display: block;" role="dialog">
	<div class="prodModal-dialogue">
		<div class="modal-content"
			style="max-height: 500px; min-width: 200%; overflow: scroll;"
			id="pop_body">
			<div class="modal-header">
				<h4 class="modal-title">
					Ledger Summary - ${ledger.ledgerName } ( ${ledger.startDate } -
					${ledger.endDate }) <span class="back-btn"><i
						class="fa fa-times" onclick="closeMe()"></i></span>
				</h4>
			</div>
			<div class="modal-body">
				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered erp-tbl blnc_shet_tbl">
					<thead>
						<tr>
							<th width="55%" style="text-align: center;">Particulars</th>
							<th width="15%" style="text-align: center;">Debit</th>
							<th width="15%" style="text-align: center;">Credit</th>
							<th width="15%" style="text-align: center;">Closing Balance</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${ledger.monthlyVos }" var="l">
							<tr>
								<td><a
									onclick="getLedgerDetailedViewPop('${l.hashCode}','${startDate }','${endDate }')">${l.month }</a></td>
								<td style="text-align: right;"><c:if test="${l.currentTotalDebit != zero }">${l.currentTotalDebit }</c:if></td>
								<td style="text-align: right;"><c:if test="${l.currentTotalCredit != zero }">${l.currentTotalCredit }</c:if></td>
								<c:choose>
									<c:when test="${l.closingCredit ==zero }">
										<c:choose>
											<c:when test="${l.closingDebit != zero }">
												<td style="text-align: right;">${l.closingDebit }&nbsp;Dr.</td>
											</c:when>
											<c:otherwise>
												<td></td>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<td style="text-align: right;">${l.closingCredit }&nbsp;Cr.</td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th><b>Grand Total:&nbsp;</b></th>
							<th style="text-align: right;"><b><c:if test="${ledger.totalDebit != zero}">${ledger.totalDebit }</c:if> </b> </th>
							<th style="text-align: right;"><b><c:if test="${ledger.totalCredit != zero}">${ledger.totalCredit }</c:if> </b> </th>
							<th style="text-align: right;"><b><c:choose>
										<c:when test="${ledger.closingCredit ==zero }">
											<c:if test="${ledger.closingDebit != zero }">
												${ledger.closingDebit }&nbsp;Dr
											</c:if>
										</c:when>
										<c:otherwise>
										${ledger.closingCredit }&nbsp;Cr.
									</c:otherwise>
									</c:choose> </b></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>