<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="paymentEntries">
	<thead>
		<tr>
			<th width="30%">Account</th>
			<th width="30%">Debit</th>
			<th width="30%">Credit</th>
			<th width="10%"></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${paymentVo.entries }" var="e" varStatus="s">
			<input type="hidden" value="${e.id }" id="idPayment${s.count }">
			<tr>
				<td><select class="form-control typesss"
					id="accTypePayment${s.count }">
						<option value="" label="-select account-" />
						<c:forEach items="${ledgers }" var="l">
						<c:choose>
						<c:when test="${l.ledgerId == e.accTypeId}">
								<option value="${l.ledgerId }" selected="selected"
									label="${l.ledgerName }"></option>
							</c:when>
							<c:otherwise>
							<option value="${l.ledgerId }" 
									label="${l.ledgerName }"></option>
							</c:otherwise>
						</c:choose>
						</c:forEach>
				</select></td>
				<td><input type="text" onblur="addTotalDebitPayment('${s.count }')"
					class="form-control align-right double tt" value="${e.debitAmnt }"
					id="debitPayment${s.count }" /><input type="hidden"
					value="${e.debitAmnt }" id="debitPayment${s.count }hidden"></td>
				<td><input type="text" onblur="addTotalCreditPayment('${s.count }')"
					class="form-control align-right double" value="${e.creditAmnt }"
					id="creditPayment${s.count }" /><input type="hidden"
					value="${e.creditAmnt }" id="creditPayment${s.count }hidden"></td>
				<td><span class="deleteRow"><i
						onclick="javascript:deleteRowPayment(this,'${s.count }'); return false;"
						class="fa fa-times"></i></span></td>
			</tr>

		</c:forEach>
		<tr>
			<td>
				<button type="button"
					onclick="javascript:addRowPayment(this); return false;"
					class="btn btn-primary erp-btn">Add Row</button>
					<span style="float: right"><strong>Total:</strong></span>
			</td>
			<td><input type="text" readonly="readonly"
				class="form-control align-right" id="totalDebitPayment"
				value="${paymentVo.amount }"></td>
			<td><input type="text" readonly="readonly"
				class="form-control align-right" id="totalCreditPayment"
				value="${paymentVo.amount }"></td>
			<td></td>
		</tr>
		<input type="hidden" id="totalCreditHiddenPayment"
			value="${paymentVo.amount }" />
		<input type="hidden" id="totalDebitHiddenPayment"
			value="${paymentVo.amount }" />
	</tbody>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script type="text/javascript"
		src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>