<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}

.slideDownn {
	display: none;
}
.slideDownnPl {
	display: none;
}
</style>
</head>
<body>
	<div class="accounts_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Balance Sheet<label style="float: right" class="c-name">${title }</label>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Start
												Date<span class="stars">*</span>
											</label>
											<div class="col-sm-9">
												<div class='input-group date ledgDate' id="datepicker">
													<input type="text" readonly="true" value="${startDate }"
														data-date-format="DD/MM/YYYY" id="b_sStart"
														style="background-color: #ffffff !important;"
														class="form-control" /> <span class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span> </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">End
												Date<span class="stars">*</span>
											</label>
											<div class="col-sm-9">
												<div class='input-group date ledgDate' id="datepickers">
													<input type="text" readonly="true" value="${endDate }"
														data-date-format="DD/MM/YYYY" id="b_sEnd"
														style="background-color: #ffffff !important;"
														class="form-control" /> <span class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span> </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Company<span
												class="stars">*</span>
											</label>
											<div class="col-sm-9" id="cmpBsDiv">
												<select class="form-control" onchange="filterBalanceSheet()"
													id="selectedCompany">
													<c:forEach items="${companies }" var="c">
														<option value="${c.companyId }">${c.companyName }</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="overflow-x: scroll;">
								<div class="col-md-12">
									<div id="panelBodySM">
										<div id="load-erp">
											<div id="spinneeer-erp"></div>
										</div>
										<input type="checkbox" id="slide_btn">Details
										<table cellpadding="0" cellspacing="0" border="0"
											class="table table-striped table-bordered erp-tbl"
											id="basic-datatable!">
											<thead>
												<tr>
													<th width="20%">Liabilities</th>
													<th width="20%" class="amount">Amount ( ${currencyName }
														)</th>
													<th width="20%">Assets</th>
													<th width="20%" class="amount">Amount ( ${currencyName }
														)</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td colspan="2">
														<table class="pla-table ">
															<c:forEach items="${entries.currentLiabilities }"
																var="de">
																<c:set value="${de.key }" var="dek" />
																<c:if test="${dek.first }">
																	<tr>
																		<td><c:choose>
																				<c:when
																					test="${dek.groupName == 'Profit & Loss A/c' }">
																					<a
																						onclick="getProfitAndLoss('${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a>
																				</c:when>
																				<c:otherwise>
																					<a
																						onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a>
																				</c:otherwise>
																			</c:choose></td>
																		<td class="amount"></td>
																		<td class="amount"><strong>${dek.amount }</strong></td>
																	</tr>
																	<c:forEach items="${de.value }" var="dev">
																		<tr class="slideDownn">
																			<td><a
																				onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
																			<td class="amount">${dev.amount }</td>
																			<td class="amount"></td>
																		</tr>
																	</c:forEach>
																</c:if>
															</c:forEach>
															<c:forEach items="${entries.equity }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:choose>
																	<c:when test="${dek.first }"></c:when>
																	<c:otherwise>
																		<tr>
																			<td><a
																				onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
																			<td class="amount"></td>
																			<td class="amount"><strong>${dek.amount }</strong></td>
																		</tr>
																		<c:forEach items="${de.value }" var="dev">
																			<tr class="slideDownn">
																				<td><a
																					onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
																				<td class="amount">${dev.amount }</td>
																				<td class="amount"></td>
																			</tr>
																		</c:forEach>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
															<c:forEach items="${entries.currentLiabilities }"
																var="de">
																<c:set value="${de.key }" var="dek" />
																<c:choose>
																	<c:when test="${dek.first }"></c:when>
																	<c:otherwise>
																		<tr>
																			<td><a
																				onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
																			<td class="amount"></td>
																			<td class="amount"><strong>${dek.amount }</strong></td>
																		</tr>
																		<c:forEach items="${de.value }" var="dev">
																			<tr class="slideDownn">
																				<td><a
																					onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
																				<td class="amount">${dev.amount }</td>
																				<td class="amount"></td>
																			</tr>
																		</c:forEach>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</table>
													</td>
													<td colspan="2">
														<table class="pla-table">
															<c:forEach items="${entries.currentAssets }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:if test="${dek.first }">
																	<tr>
																		<td><a
																			onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
																		<td class="amount"></td>
																		<td class="amount"><strong>${dek.amount }</strong></td>
																	</tr>
																	<c:forEach items="${de.value }" var="dev">
																		<tr class="slideDownn">
																			<td><a
																				onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
																			<td class="amount">${dev.amount }</td>
																			<td class="amount"></td>
																		</tr>
																	</c:forEach>
																</c:if>
															</c:forEach>
															<c:forEach items="${entries.currentAssets }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:if test="${! dek.first }">
																	<c:if test="${! dek.isLast }">
																		<tr>
																			<td><a
																				onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
																			<td class="amount"></td>
																			<td class="amount"><strong>${dek.amount }</strong></td>
																		</tr>
																		<c:forEach items="${de.value }" var="dev">
																			<tr class="slideDownn">
																				<td><a
																					onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
																				<td class="amount">${dev.amount }</td>
																				<td class="amount"></td>
																			</tr>
																		</c:forEach>
																	</c:if>
																</c:if>
															</c:forEach>
															<c:forEach items="${entries.currentAssets }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:if test="${ dek.isLast }">
																	<tr>
																		<td><c:choose>
																				<c:when test="${dek.groupName =='Closing Stock' }">
																					<a
																						onclick="getStockSummary('${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a>
																				</c:when>
																				<c:otherwise>
																					<a
																						onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a>
																				</c:otherwise>
																			</c:choose></td>
																		<td class="amount"></td>
																		<td class="amount"><strong>${dek.amount }</strong></td>
																	</tr>
																	<c:forEach items="${de.value }" var="dev">
																		<tr class="slideDownn">
																			<td><a
																				onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
																			<td class="amount">${dev.amount }</td>
																			<td class="amount"></td>
																		</tr>
																	</c:forEach>
																</c:if>
															</c:forEach>
														</table>
													</td>

												</tr>
												<tr>

													<td colspan="2">
														<table class="pla-table ">
															<c:forEach items="${entries.nonCurrentLiabilities }"
																var="de">
																<c:set value="${de.key }" var="dek" />
																<tr>
																	<td><a
																		onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
																	<td class="amount"></td>
																	<td class="amount"><strong>${dek.amount }</strong></td>
																</tr>
																<c:forEach items="${de.value }" var="dev">
																	<tr class="slideDownn">
																		<td><a
																			onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
																		<td class="amount">${dev.amount }</td>
																		<td class="amount"></td>
																	</tr>
																</c:forEach>
															</c:forEach>
														</table>
													</td>
													<td colspan="2">
														<table class="pla-table">
															<c:forEach items="${entries.nonCurrentAssets }" var="de">
																<c:set value="${de.key }" var="dek" />
																<tr>
																	<td><a
																		onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
																	<td class="amount"></td>
																	<td class="amount"><strong>${dek.amount }</strong></td>
																</tr>
																<c:forEach items="${de.value }" var="dev">
																	<tr class="slideDownn">
																		<td><a
																			onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
																		<td class="amount">${dev.amount }</td>
																		<td class="amount"></td>
																	</tr>
																</c:forEach>
															</c:forEach>
														</table>
													</td>
												</tr>
												<tr>
													<td><strong>Total</strong></td>
													<td class="amount"><strong>${entries.totalLiability }</strong></td>
													<td><strong>Total</strong></td>
													<td class="amount"><strong>${entries.totalAsset }</strong></td>
												</tr>
											</tbody>
										</table>
									</div>
									<button type="button" onclick="getBalancesheetPdf()"
										class="btn btn-primary erp-btn">Generate PDF</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="newPop" style="display: none;"></div>
	<div id="plPOp" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false"
			style="display: block; overflow: scroll;" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content"
					style="margin-top: 10%; margin-right: -41%; margin-left: -41%;">
					<div class="modal-header">
						<h4 class="modal-title">
							Profit And Loss<span class="back-btn"> <i
								class="fa fa-times" onclick="closeMePL()"></i>
							</span>
						</h4>
					</div>
					<div class="modal-body"></div>
				</div>
			</div>
		</div>
	</div>
	<div id="SSPOp" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false"
			style="display: block; overflow: scroll; z-index: 999;" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content"
					style="margin-top: 10%; margin-right: -41%; margin-left: -41%;">
					<div class="modal-header">
						<h4 class="modal-title">
							Stock Summary<span class="back-btn"> <i
								class="fa fa-times" onclick="closeMePL()"></i>
							</span>
						</h4>
					</div>
					<div class="modal-body" id="SSPOpCntnt"></div>
				</div>
			</div>
		</div>
	</div>
	<div id="confirm-delete" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">Do you really want to delete this
						Ledger?</div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="deleteLedger('false')"
								class="btn btn-default">No</button>
							<button type="button" onclick="deleteLedger('true')"
								class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<c:if test="${! empty success_msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>

	<div id="success-msg">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>

	<div id="erro-msg">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>


	<div id="errror-msg" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Alert</h4>
					</div>
					<div class="modal-body"></div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="closeError()"
								class="btn btn-default">ok</button>
							<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script type="text/javascript">
		
	</script>
</body>
</html>