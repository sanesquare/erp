<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%-- <link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/bootstrap.css'/>" /> --%>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />

<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />

</head>
<body>
	<div class="accounts_home">
		<div class="warper container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Ledger Information<a href="ledgersHome.do"><span
								class="back-btn"><i class="fa fa-arrow-left"></i></span></a>
						</div>
						<div class="panel-body">
							<form:form method="POST" action="saveLedger.do"
								modelAttribute="ledgerVo" id="ledger_frm">
								<form:hidden path="ledgerId" id="ledg_id" />
								<input type="hidden" value="${startDate }" name="startDate">
								<input type="hidden" value="${endDate }" name="endDate">
								<div class="row">
									<div class="col-md-12">

										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Basic Information</div>
											<div class="panel-body">
												<%-- <div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Date<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class='input-group date ledgDate' id="datepicker">
															<form:input type="text" readonly="true"
																data-date-format="DD/MM/YYYY"
																style="background-color: #ffffff !important;"
																class="form-control" path="startDate" />
															<span class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>
												</div> --%>
												<c:choose>
													<c:when test="${! ledgerVo.isEditable }">
													<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Ledger Name </label>
															<div class="col-sm-9">
																<form:hidden path="ledgerName" />
																<label>${ledgerVo.ledgerName }</label>
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Ledger Group<span
																class="stars">*</span>
															</label>
															<div class="col-sm-8">
															<form:hidden path="ledgerGroupId" />
																<label>${ledgerVo.ledgerGroup }</label>
															</div>
														</div>
													</c:when>
													<c:otherwise>


														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Ledger Name </label>
															<div class="col-sm-9">
																<form:input style="background-color: #fff !important"
																	cssClass="form-control paste" path="ledgerName" />
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Ledger Group<span
																class="stars">*</span>
															</label>
															<div class="col-sm-8">
																<form:select path="ledgerGroupId"
																	cssClass="form-control" id="legers_selct_bx">
																	<form:options items="${groups }" itemLabel="groupName"
																		itemValue="groupId" />
																</form:select>
															</div>
															<div class="col-sm-1">
																<i onclick="addLedgerGroup()"
																	style="font-size: 20px; margin-top: 5%; float: right;"
																	class="fa fa-plus-circle"></i>
															</div>
														</div>
													</c:otherwise>
													</c:choose>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Opening Balance<span
																class="stars">*</span>
															</label>
															<div class="col-sm-9">
																<form:input type="text"
																	style="text-align : right; background-color: #fff !important;"
																	cssClass="form-control double" path="openingBalance" />
															</div>
														</div>
													
													<div class="form-group">
														<button type="submit" class="btn btn-primary erp-btn">Save</button>
													</div>
													<div class=" right">
														<a href="addLedger.do">
															<button type="button" class="btn btn-primary erp-btn">New
																Ledger</button>
														</a>
													</div>
											</div>
										</div>

									</div>
								</div>
							</form:form>
						</div>


					</div>
				</div>

				<!-- TEmplate -->

				<%-- <div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Ledger Information<a href="ledgersHome.do"><span
								class="back-btn"><i class="fa fa-arrow-left"></i></span></a>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">

									<div class="panel panel-default erp-info">
										<div class="panel-heading panel-inf">Ledger Group
											Information</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Ledger
															Group Name </label>
														<div class="col-sm-9">
															<input style="background-color: #fff !important"
																class="form-control paste" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-3 control-label">Account
															Type <span class="stars">*</span>
														</label>
														<div class="col-sm-9">
															<select class="form-control">
																<option label="--Select Account Type -- "></option>
															</select>
															<form:select path="ledgerGroupId" cssClass="form-control">
														<form:options items="${groups }" itemLabel="groupName" itemValue="groupId"/>
													</form:select>
														</div>
													</div>

													<div class="form-group">
														<button type="submit" class="btn btn-primary erp-btn">Save</button>
													</div>
												</div>
											</div>
											<div class="row"
												style="border-top: dotted #fff 1px; margin-top: 2%; padding-top: 2%;">
												<div class="col-md-12">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl"
														id="basic-datatable">
														<thead>
															<tr>
																<th width="40%">Account Type</th>
																<th width="40%">Ledger Group</th>
																<th width="10%">Edit</th>
																<th width="10%">Delete</th>
															</tr>
														</thead>
														<tbody>
														</tbody>
													</table>
												</div>
											</div>


										</div>
									</div> --%>
				<!--  -->

			</div>
		</div>
	</div>


	<c:if test="${! empty success_msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<div class="notification-panl successMsg" style="display: none">
		<div class="notfctn-cntnnt">${success_msg }</div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
	<!-- <div id="notify-all">
		<div class="notification-panl">
			<div class="notfctn-cntnnt">Content</div>
			<span id="cloese"><i class="fa fa-times"></i></span>
		</div>
	</div> -->


	<div class="modal " data-backdrop="static" data-keyboard="false"
		tabindex="-1" aria-hidden="false" style="display: none;" role="dialog"
		id="ledgerGroupPop">
		<div class="pop-content"></div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts-validation.js' />"></script>


	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>

	<!-- Custom JQuery -->
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<!-- DateTime Picker -->
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
</body>
</html>