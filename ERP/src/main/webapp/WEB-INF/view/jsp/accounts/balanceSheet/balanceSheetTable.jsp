<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="load-erp">
	<div id="spinneeer-erp"></div>
</div>
<input type="checkbox" id="slide_btn">
Details
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl"
	id="basic-datatable!">
	<thead>
		<tr>
			<th width="20%">Liabilities</th>
			<th width="20%" class="amount">Amount ( ${currencyName } )</th>
			<th width="20%">Assets</th>
			<th width="20%" class="amount">Amount ( ${currencyName } )</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="2">
				<table class="pla-table ">
					<c:forEach items="${entries.currentLiabilities }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:if test="${dek.first }">
							<tr>
								<td><c:choose>
										<c:when test="${dek.groupName == 'Profit & Loss A/c' }">
											<a onclick="getProfitAndLoss('${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a>
										</c:when>
										<c:otherwise>
											<a
												onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a>
										</c:otherwise>
									</c:choose></td>
								<td class="amount"></td>
								<td class="amount"><strong>${dek.amount }</strong></td>
							</tr>
							<c:forEach items="${de.value }" var="dev">
								<tr class="slideDownn">
									<td><a
										onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
									<td class="amount">${dev.amount }</td>
									<td class="amount"></td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
					<c:forEach items="${entries.equity }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:choose>
							<c:when test="${dek.first }"></c:when>
							<c:otherwise>
								<tr>
									<td><a
										onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
									<td class="amount"></td>
									<td class="amount"><strong>${dek.amount }</strong></td>
								</tr>
								<c:forEach items="${de.value }" var="dev">
									<tr class="slideDownn">
										<td><a
											onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
										<td class="amount">${dev.amount }</td>
										<td class="amount"></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<c:forEach items="${entries.currentLiabilities }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:choose>
							<c:when test="${dek.first }"></c:when>
							<c:otherwise>
								<tr>
									<td><a
										onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
									<td class="amount"></td>
									<td class="amount"><strong>${dek.amount }</strong></td>
								</tr>
								<c:forEach items="${de.value }" var="dev">
									<tr class="slideDownn">
										<td><a
											onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
										<td class="amount">${dev.amount }</td>
										<td class="amount"></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</table>
			</td>
			<td colspan="2">
				<table class="pla-table">
					<c:forEach items="${entries.currentAssets }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:if test="${dek.first }">
							<tr>
								<td><a
									onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
								<td class="amount"></td>
								<td class="amount"><strong>${dek.amount }</strong></td>
							</tr>
							<c:forEach items="${de.value }" var="dev">
								<tr class="slideDownn">
									<td><a
										onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
									<td class="amount">${dev.amount }</td>
									<td class="amount"></td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
					<c:forEach items="${entries.currentAssets }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:if test="${! dek.first }">
							<c:if test="${! dek.isLast }">
								<tr>
									<td><a
										onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
									<td class="amount"></td>
									<td class="amount"><strong>${dek.amount }</strong></td>
								</tr>
								<c:forEach items="${de.value }" var="dev">
									<tr class="slideDownn">
										<td><a
											onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
										<td class="amount">${dev.amount }</td>
										<td class="amount"></td>
									</tr>
								</c:forEach>
							</c:if>
						</c:if>
					</c:forEach>
					<c:forEach items="${entries.currentAssets }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:if test="${ dek.isLast }">
							<tr>
								<td><c:choose>
										<c:when test="${dek.groupName =='Closing Stock' }">
											<a onclick="getStockSummary('${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a>
										</c:when>
										<c:otherwise>
											<a
												onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a>
										</c:otherwise>
									</c:choose></td>
								<td class="amount"></td>
								<td class="amount"><strong>${dek.amount }</strong></td>
							</tr>
							<c:forEach items="${de.value }" var="dev">
								<tr class="slideDownn">
									<td><a
										onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
									<td class="amount">${dev.amount }</td>
									<td class="amount"></td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
				</table>
			</td>

		</tr>
		<tr>

			<td colspan="2">
				<table class="pla-table ">
					<c:forEach items="${entries.nonCurrentLiabilities }" var="de">
						<c:set value="${de.key }" var="dek" />
						<tr>
							<td><a
								onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
							<td class="amount"></td>
							<td class="amount"><strong>${dek.amount }</strong></td>
						</tr>
						<c:forEach items="${de.value }" var="dev">
							<tr class="slideDownn">
								<td><a
									onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
								<td class="amount">${dev.amount }</td>
								<td class="amount"></td>
							</tr>
						</c:forEach>
					</c:forEach>
				</table>
			</td>
			<td colspan="2">
				<table class="pla-table">
					<c:forEach items="${entries.nonCurrentAssets }" var="de">
						<c:set value="${de.key }" var="dek" />
						<tr>
							<td><a
								onclick="getLedgerViewPop('${dek.groupId }',true,'${startDate }','${endDate }')"><strong>${dek.groupName }</strong></a></td>
							<td class="amount"></td>
							<td class="amount"><strong>${dek.amount }</strong></td>
						</tr>
						<c:forEach items="${de.value }" var="dev">
							<tr class="slideDownn">
								<td><a
									onclick="getLedgerMonthlyViewPop('${dev.ledgerId}',true,'${startDate }','${endDate }')">${dev.ledgerName }</a></td>
								<td class="amount">${dev.amount }</td>
								<td class="amount"></td>
							</tr>
						</c:forEach>
					</c:forEach>
				</table>
			</td>
		</tr>
		<tr>
			<td><strong>Total</strong></td>
			<td class="amount"><strong>${entries.totalLiability }</strong></td>
			<td><strong>Total</strong></td>
			<td class="amount"><strong>${entries.totalAsset }</strong></td>
		</tr>
	</tbody>
</table>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
