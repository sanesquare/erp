<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="load-erp">
	<div id="spinneeer-erp"></div>
</div>
<input type="checkbox" id="slide_btn">
Details
<input type="checkbox" id="op_slide_btn">
Opening Balance
<input type="checkbox" id="ts_slide_btn">
Transactions
<!-- <input type="checkbox" id="cl_slide_btn">Closing Balance -->
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl"
	id="basic-datatable!">
	<thead>
		<tr>
			<!-- <th width="10%">Code</th> -->
			<th width="25%" rowspan="2"
				style="vertical-align: middle; text-align: center;">Particulars</th>
			<th width="25%" colspan="2"
				style="vertical-align: middle; text-align: center;" class="opSlide">Opening
				Balance</th>
			<th width="25%" colspan="2" style="text-align: center;"
				class="tsSlide">Transactions</th>
			<th width="25%" colspan="2" style="text-align: center;">Closing
				Balance</th>
			<%-- <th width="20%">Credit ( ${currencyName })</th> --%>
			<th width="5%" class="clSlide">Closing Balance ( ${currencyName })</th>

		</tr>
		<tr>
			<th width="10%" class="opSlide">Debit ( ${currencyName })</th>
			<th width="10%" class="opSlide">Credit ( ${currencyName })</th>
			<th width="10%" class="tsSlide">Debit ( ${currencyName })</th>
			<th width="10%" class="tsSlide">Credit ( ${currencyName })</th>
			<th width="10%">Debit ( ${currencyName })</th>
			<th width="10%">Credit ( ${currencyName })</th>
			<th width="5%" class="clSlide">Closing Balance ( ${currencyName })</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${entries }" var="group">
			<c:set var="key" value="${group.key }"></c:set>
			<c:choose>
				<c:when test="${key.group =='grand_total!@#'  }"></c:when>
				<c:when test="${key.group =='Closing Stock'  }"></c:when>
				<c:when test="${key.group =='Opening Stock'  }">

					<tr>
						<td><strong>${key.group }</strong></td>
						<td class="opSlide"><strong> <c:choose>
									<c:when test="${ key.openingDebit eq key.zero}"></c:when>
									<c:otherwise>
																${key.openingDebit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td class="opSlide"><strong> <c:choose>
									<c:when test="${ key.openingCredit eq key.zero}"></c:when>
									<c:otherwise>
																${key.openingCredit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td class="tsSlide"><strong> <c:choose>
									<c:when test="${ key.transactionDebit eq key.zero}"></c:when>
									<c:otherwise>
																${key.transactionDebit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td class="tsSlide"><strong> <c:choose>
									<c:when test="${ key.transactionCredit eq key.zero}"></c:when>
									<c:otherwise>
																${key.transactionCredit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td><strong><c:choose>
									<c:when test="${ key.debit eq key.zero}"></c:when>
									<c:otherwise>
																${key.debit }
																</c:otherwise>
								</c:choose> </strong></td>
						<td><strong><c:choose>
									<c:when test="${ key.credit eq key.zero}"></c:when>
									<c:otherwise>
																${key.credit }
																</c:otherwise>
								</c:choose></strong></td>
						<td class="clSlide"><strong> <c:choose>
									<c:when test="${ key.closing eq key.zero}"></c:when>
									<c:otherwise>
																${key.closing }
																</c:otherwise>
								</c:choose>
						</strong></td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr>
						<td><strong>${key.group }</strong></td>
						<td class="opSlide"><strong> <c:choose>
									<c:when test="${ key.openingDebit eq key.zero}"></c:when>
									<c:otherwise>
																${key.openingDebit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td class="opSlide"><strong> <c:choose>
									<c:when test="${ key.openingCredit eq key.zero}"></c:when>
									<c:otherwise>
																${key.openingCredit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td class="tsSlide"><strong> <c:choose>
									<c:when test="${ key.transactionDebit eq key.zero}"></c:when>
									<c:otherwise>
																${key.transactionDebit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td class="tsSlide"><strong> <c:choose>
									<c:when test="${ key.transactionCredit eq key.zero}"></c:when>
									<c:otherwise>
																${key.transactionCredit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td><strong><c:choose>
									<c:when test="${ key.debit eq key.zero}"></c:when>
									<c:otherwise>
																${key.debit }
																</c:otherwise>
								</c:choose> </strong></td>
						<td><strong><c:choose>
									<c:when test="${ key.credit eq key.zero}"></c:when>
									<c:otherwise>
																${key.credit }
																</c:otherwise>
								</c:choose></strong></td>
						<td class="clSlide"><strong> <c:choose>
									<c:when test="${ key.closing eq key.zero}"></c:when>
									<c:otherwise>
																${key.closing }
																</c:otherwise>
								</c:choose>
						</strong></td>
					</tr>
					<c:forEach items="${group.value }" var="l">
						<tr class="slideDownn">
							<td>${l.ledgerName }</td>
							<td class="opSlide"><c:choose>
									<c:when test="${ l.openingDebit eq l.zero}"></c:when>
									<c:otherwise>
																${l.openingDebit}
																</c:otherwise>
								</c:choose></td>
							<td class="opSlide"><c:choose>
									<c:when test="${ l.openingCredit eq l.zero}"></c:when>
									<c:otherwise>
																${l.openingCredit}
																</c:otherwise>
								</c:choose></td>
							<td class="tsSlide"><c:choose>
									<c:when test="${ l.transactionDebit eq l.zero}"></c:when>
									<c:otherwise>
																${l.transactionDebit}
																</c:otherwise>
								</c:choose></td>
							<td class="tsSlide"><c:choose>
									<c:when test="${ l.transactionCredit eq l.zero}"></c:when>
									<c:otherwise>
																${l.transactionCredit}
																</c:otherwise>
								</c:choose></td>
							<td><c:choose>
									<c:when test="${ l.debit eq l.zero}"></c:when>
									<c:otherwise>
																${l.debit}
																</c:otherwise>
								</c:choose></td>
							<td><c:choose>
									<c:when test="${ l.credit eq l.zero}"></c:when>
									<c:otherwise>
																${l.credit}
																</c:otherwise>
								</c:choose></td>
							<td class="clSlide"><c:choose>
									<c:when test="${ l.balance eq l.zero}"></c:when>
									<c:otherwise>
																${l.balance}
																</c:otherwise>
								</c:choose></td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		<c:forEach items="${entries }" var="group">
			<c:set var="key" value="${group.key }"></c:set>
			<c:if test="${key.group == 'grand_total!@#' }">
				<c:forEach items="${group.value }" var="l">
					<tr>
						<td><strong>Grand Total</strong></td>
						<td class="opSlide"><strong> <c:choose>
									<c:when test="${ l.totalOpeningDebit eq l.zero}"></c:when>
									<c:otherwise>
																${l.totalOpeningDebit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td class="opSlide"><strong> <c:choose>
									<c:when test="${ l.totalOpeningCredit eq l.zero}"></c:when>
									<c:otherwise>
																${l.totalOpeningCredit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td class="tsSlide"><strong> <c:choose>
									<c:when test="${ l.totalTransactionDebit eq l.zero}"></c:when>
									<c:otherwise>
																${l.totalTransactionDebit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td class="tsSlide"><strong> <c:choose>
									<c:when test="${ l.totalTransactionCredit eq l.zero}"></c:when>
									<c:otherwise>
																${l.totalTransactionCredit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td><strong> <c:choose>
									<c:when test="${ l.totalDebit eq l.zero}"></c:when>
									<c:otherwise>
																${l.totalDebit}
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td><strong> <c:choose>
									<c:when test="${ l.totalCredit eq l.zero}"></c:when>
									<c:otherwise>
																${l.totalCredit} 
																</c:otherwise>
								</c:choose>
						</strong></td>
						<td class="clSlide"><strong> <c:choose>
									<c:when test="${ l.totalClose eq l.zero}"></c:when>
									<c:otherwise>
																${l.totalClose}
																</c:otherwise>
								</c:choose>
						</strong></td>
					</tr>
				</c:forEach>
			</c:if>
		</c:forEach>
	</tbody>
</table>
<c:forEach items="${entries }" var="group">
	<c:set var="key" value="${group.key }"></c:set>
	<c:if test="${key.group == 'Closing Stock' }">
		<div class="row">
			<div class="col-md-12">
				<div class="panel-body">
					<div class="form-group">
						<label for="inputPassword3" class="col-md-4 control-label">Adjustments
							: </label>
					</div>
					<div class="form-group">
						<div class="col-md-1"></div>
						<label for="inputPassword3" class="col-md-2 control-label">Closing
							Stock: </label>
						<div class="col-md-2">
							<c:choose>
								<c:when test="${ key.credit eq key.zero}"></c:when>
								<c:otherwise>
									<label>${key.credit}</label>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${ key.debit eq key.zero}"></c:when>
								<c:otherwise>
									<label>${key.debit}</label>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
</c:forEach>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
