<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="load-erp">
	<div id="spinneeer-erp"></div>
</div>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="15%">Voucher Number</th>
			<th width="15%">Date</th>
			<th width="20%">From Account</th>
			<th width="20%">To Account</th>
			<th width="15%">Amount</th>
			<th width="5%">PDF</th>
			<th width="5%">Edit</th>
			<th width="5%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${contras }" var="p">
			<tr>
				<td>${p.voucherNumber }</td>
				<td>${p.date }</td>
				<td>${p.fromLedger }</td>
				<td>${p.toLedger }</td>
				<td>${p.amount }</td>
				<td><a href="contraPdf.do?id=${p.contraId}"><i
						class="fa fa-file-pdf-o"></i></a></td>
				<td><a href="editContra.do?id=${p.contraId}"><i
						class="fa fa-pencil"></i> </a></td>
				<td><a href="#" onclick="cnfrmDeleteContra('${p.contraId }')"><i
						class="fa fa-times"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>