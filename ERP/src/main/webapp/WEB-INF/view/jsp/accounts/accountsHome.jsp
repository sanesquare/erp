<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
</head>
<body>
	<div class="accounts_home">
		<div class="warper container-fluid">
			<div class="noti" id="notifications"></div>

			<div class="page-header">
				<div class="hdr">
					<div class="home-div">
						<a href="erpHome.do"> <span class="home"><i
								class="fa fa-home"></i></span>
						</a>
					</div>
					<div class="hdr-div">
						<span class="hdr-ico"><i class="fa fa-calculator"></i></span>
						Accounts <small>Let's get a quick overview...</small>
					</div>

				</div>
			</div>
			<div class="row"
				style="padding-top: 3%; margin-left: 4%; margin-right: 4%">
				<div class="col-md-4">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<a href="ledgersHome.do">
							<div class="panel-body  ">

								<span class="dashboard-title ">Ledgers</span> <span
									class="dashboard-icons "><i class="fa fa-book"></i></span>
								<div class="dashboard-label">Keep a track of your company.</div>
							</div>
						</a>
					</div> -->
					
					<div class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
							<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
								href="ledgersHome.do"> <span class="hover_effect dashboard-title">Ledgers
								</span></a> </span> <i class="fa fa-book bg-danger transit stats-icon"></i>
							<a href="ledgersHome.do">
								<h3 class="transit dashboard-title">
									Ledgers<br><small class="text-green"></small>
								</h3>
							</a>
						</div>
						
				</div>
				<div class="col-md-4">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<a href="paymentHome.do">
							<div class="panel-body  ">

								<span class="dashboard-title ">Payment</span> <span
									class="dashboard-icons "><i class="fa fa-credit-card"></i></span>
								<div class="dashboard-label">Close your payments..</div>
							</div>
						</a>
					</div> -->
					
					
						<div class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
							<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
								href="paymentHome.do"> <span class="hover_effect dashboard-title">Payment
								</span></a> </span> <i class="fa fa-credit-card bg-danger transit stats-icon"></i>
							<a href="paymentHome.do">
								<h3 class="transit dashboard-title">
									Payment<br><small class="text-green"></small>
								</h3>
							</a>
						</div>
					
				</div>
				<div class="col-md-4">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<a href="receiptHome.do">
							<div class="panel-body  ">

								<span class="dashboard-title ">Receipt</span> <span
									class="dashboard-icons "><i class="fa fa-file-text-o"></i></span>
								<div class="dashboard-label">Keep a track of your company.</div>
							</div>
						</a>
					</div> -->
					
					<div class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
							<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
								href="receiptHome.do"> <span class="hover_effect dashboard-title">Receipt
								</span></a> </span> <i class="fa fa-file-text-o bg-danger transit stats-icon"></i>
							<a href="receiptHome.do">
								<h3 class="transit dashboard-title">
									Receipt<br><small class="text-green"></small>
								</h3>
							</a>
						</div>
				</div>
			</div>

			<div class="row"
				style="padding-top: 3%; margin-left: 4%; margin-right: 4%">
				<div class="col-md-4">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<a href="trialBalance.do">
							<div class="panel-body  ">

								<span class="dashboard-title ">Trial Balance</span> <span
									class="dashboard-icons "><i class="fa fa-book"></i></span>
								<div class="dashboard-label">Keep a track of your company.</div>
							</div>
						</a>
					</div> -->
					
					<div class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
							<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
								href="trialBalance.do"> <span class="hover_effect dashboard-title">Trial<br>Balance
								</span></a> </span> <i class="fa fa-book bg-danger transit stats-icon"></i>
							<a href="trialBalance.do">
								<h3 class="transit dashboard-title">
									Trial<br>Balance<small class="text-green"></small>
								</h3>
							</a>
						</div>
				</div>
				<div class="col-md-4">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<a href="profitAndLoss.do">
						<div class="panel-body  ">

							<span class="dashboard-title ">Profit & Loss</span> <span
								class="dashboard-icons "><i class="fa fa-pie-chart"></i></span>
							<div class="dashboard-label">Close your payments..</div>
						</div></a>
					</div> -->
					
					<div class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
							<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
								href="profitAndLoss.do"> <span class="hover_effect dashboard-title">Profit &<br> Loss
								</span></a> </span> <i class="fa fa-pie-chart bg-danger transit stats-icon"></i>
							<a href="profitAndLoss.do">
								<h3 class="transit dashboard-title">
									Profit &<br> Loss<small class="text-green"></small>
								</h3>
							</a>
						</div>
				</div>
				<div class="col-md-4">
					<!-- <div class="panel panel-default dashboard-itmes"
						style="border-radius: 46px;">
						<a href="balanceSheet.do">
						<div class="panel-body  ">

							<span class="dashboard-title ">Balance Sheet</span> <span
								class="dashboard-icons "><i class="fa fa-bar-chart"></i></span>
							<div class="dashboard-label">Keep a track of your company.</div>
						</div></a>
					</div> -->
					
					<div class="panel panel-default clearfix dashboard-stats rounded dashboard-itmes">
							<span id="dashboard-stats-sparkline1" class="sparkline transit"><a
								href="balanceSheet.do"> <span class="hover_effect dashboard-title">Balance<br> Sheet
								</span></a> </span> <i class="fa fa-bar-chart bg-danger transit stats-icon"></i>
							<a href="balanceSheet.do">
								<h3 class="transit dashboard-title">
									Balance<br> Sheet<small class="text-green"></small>
								</h3>
							</a>
						</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>

	<script>
		
	</script>
	<script>
		
	</script>
	<!-- Warper Ends Here (working area) -->
</body>
</html>