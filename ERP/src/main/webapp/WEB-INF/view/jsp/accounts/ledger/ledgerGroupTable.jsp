<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered erp-tbl"
					id="basic-datatable">
					<thead>
						<tr>
							<th width="30%">Account Type</th>
							<th width="20%">Ledger Group Code</th>
							<th width="30%">Ledger Group Name</th>
							<th width="10%">Edit</th>
							<th width="10%">Delete</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${groups }" var="g">
							<tr>
								<td>${g.accounttype }</td>
								<td>${g.groupCode }</td>
								<td>${g.groupName }</td>
								<c:choose>
								<c:when test="${g.isEditable }">
										<td><span
									onclick='editThisLedgerGroup("${g.groupName }","${g.groupId }","${g.accountTypeId }")'><i
										class='fa fa-pencil'></i></span></td>
								<td><span onclick='deleteThisLedgerGroup("${g.groupId }")'><i
										class='fa fa-times'></i></span></td>								
								</c:when>
								<c:otherwise>
									<td><i>Default</i></td>
									<td><i>Default</i></td>
								</c:otherwise>
								</c:choose>
								
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
<script
	src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
