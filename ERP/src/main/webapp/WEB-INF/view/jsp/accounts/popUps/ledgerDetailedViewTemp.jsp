<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="modal-header">
	<h4 class="modal-title">
		Ledger Transactions - ${month.ledgerName } ( ${month.month } )<span
			class="back-btn"> <span
			onclick="getLedgerViewBack('${id}','ledger','${startDate }','${endDate }')"
			class="fa fa-arrow-left back"></span> <i class="fa fa-times"
			onclick="closeMe()"></i></span>
	</h4>
</div>
<div class="modal-body">
	<table cellpadding="0" cellspacing="0" border="0"
		class="table table-striped table-bordered erp-tbl blnc_shet_tbl">
		<thead>
			<tr>
				<th width="10%" style="text-align: center;">Date</th>
				<th width="30%" style="text-align: center;">Narration</th>
				<th width="15%" style="text-align: center;">Voucher</th>
				<th width="15%" style="text-align: center;">Voucher Number</th>
				<th width="15%" style="text-align: center;">Debit</th>
				<th width="15%" style="text-align: center;">Credit</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${month.transactionViewVos }" var="l">
				<tr>
					<td><a
						onclick="loadLedgerVoucher('${l.id}','${l.voucher }','${monthlyId }')">${l.date }</a></td>
					<td>${l.narration }</td>
					<td>${l.voucher }</td>
					<td>${l.voucherNumber }</td>
					<td style="text-align: right;"><c:if test="${l.debitAmount  != zero }">${l.debitAmount }</c:if></td>
					<td style="text-align: right;"><c:if test="${l.creditAmount != zero }">${l.creditAmount }</c:if></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="4" style="text-align: right;"><b>Opening
						Balance : </b></td>
				<td><b style="text-align: right;"><c:if test="${month.openingDebit != zero }">${month.openingDebit }</c:if></b></td>
				<td><b style="text-align: right;"><c:if test="${month.openingCredit != zero }">${month.openingCredit }</c:if></b></td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: right;"><b>Current Total
						: </b></td>
				<td style="text-align: right;"><b><c:if test="${month.currentTotalDebit != zero }">${month.currentTotalDebit }</c:if></b></td>
				<td style="text-align: right;"><b><c:if test="${month.currentTotalCredit != zero }">${month.currentTotalCredit }</c:if></b></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="4" style="text-align: right;"><b>Closing
						Balance : </b></th>
				<th style="text-align: right;"><b><c:if test="${month.closingDebit != zero }">${month.closingDebit }</c:if>
				</b></th>
				<th style="text-align: right;"><b><c:if test="${month.closingCredit != zero }">${month.closingCredit }</c:if></b></th>
			</tr>
		</tfoot>
	</table>
</div>
<script
	src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>