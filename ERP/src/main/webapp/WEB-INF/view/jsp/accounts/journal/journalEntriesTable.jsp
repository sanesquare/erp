<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="journalEntries">
	<thead>
		<tr>
			<th width="30%">Account</th>
			<th width="30%">Debit</th>
			<th width="30%">Credit</th>
			<th width="10%"></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${journalVo.entries }" var="e" varStatus="s">
			<input type="hidden" value="${e.id }" id="id${s.count }">
			<tr>
				<td><select class="form-control typesss"
					id="accType${s.count }">
						<option value="" label="-select account-" />
						<c:forEach items="${ledgers }" var="l">
						<c:choose>
						<c:when test="${l.ledgerId == e.accTypeId}">
								<option value="${l.ledgerId }" selected="selected"
									label="${l.ledgerName }"></option>
							</c:when>
							<c:otherwise>
							<option value="${l.ledgerId }" 
									label="${l.ledgerName }"></option>
							</c:otherwise>
						</c:choose>
						</c:forEach>
				</select></td>
				<td><input type="text" onblur="addTotalDebit('${s.count }')"
					class="form-control align-right double tt" value="${e.debitAmnt }"
					id="debit${s.count }" /><input type="hidden"
					value="${e.debitAmnt }" id="debit${s.count }hidden"></td>
				<td><input type="text" onblur="addTotalCredit('${s.count }')"
					class="form-control align-right double" value="${e.creditAmnt }"
					id="credit${s.count }" /><input type="hidden"
					value="${e.creditAmnt }" id="credit${s.count }hidden"></td>
				<td><span class="deleteRow"><i
						onclick="javascript:deleteRow(this,'${s.count }'); return false;"
						class="fa fa-times"></i></span></td>
			</tr>

		</c:forEach>
		<tr>
			<td>
				<button type="button"
					onclick="javascript:addRow(this); return false;"
					class="btn btn-primary erp-btn">Add Row</button>
					<span style="float: right"><strong>Total:</strong></span>
			</td>
			<td><input type="text" readonly="readonly"
				class="form-control align-right" id="totalDebit"
				value="${journalVo.amount }"></td>
			<td><input type="text" readonly="readonly"
				class="form-control align-right" id="totalCredit"
				value="${journalVo.amount }"></td>
			<td></td>
		</tr>
		<input type="hidden" id="totalCreditHidden"
			value="${journalVo.amount }" />
		<input type="hidden" id="totalDebitHidden"
			value="${journalVo.amount }" />
	</tbody>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script type="text/javascript"
		src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>