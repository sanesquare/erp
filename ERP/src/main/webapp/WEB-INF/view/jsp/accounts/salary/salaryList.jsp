<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered" id="basic-datatable">
	<thead>
		<tr>

			<th width="20%">Date</th>
			<th width="20%">Branch</th>
			<th width="20%">Salary Type</th>
			<th width="10%">Status</th>
			<!-- <th width="10%">Pay</th> -->
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${salaries }" var="salary">
			<tr>
				<td><a
					href="viewSalary.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }">${salary.date }</a></td>
				<td><a
					href="viewSalary.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }"><c:forEach
							items="${salary.branches }" var="branch">${branch } </c:forEach></a></td>
				<td><a
					href="viewSalary.do?id=${ salary.id}&tyPe=${salary.salaryTypeType }">${salary.salaryTypeType }</a></td>
				<td><select class="form-control"
					onchange="changeSalaryStatus('${salary.id }',this.value)">
						<c:forEach items="${status }" var="s">
							<c:choose>
								<c:when test="${s.status == salary.status }">
									<option value="${s.id }" selected="selected"
										label="${s.status }" />
								</c:when>
								<c:otherwise>
									<option value="${s.id }" label="${s.status }" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
				</select></td>
				<%-- 	<td>
										<button onclick="showPop('${salary.id }')" class="form-control">Pay Now</button>
										  </td> --%>

			</tr>
		</c:forEach>


	</tbody>
</table>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>