<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}

.slideDownnPl {
	display: none;
}
</style>
</head>
<body>
	<div class="accounts_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Profit And Loss<label style="float: right" class="c-name">${title }</label>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Start
												Date<span class="stars">*</span>
											</label>
											<div class="col-sm-9">
												<div class='input-group date ledgDate' id="datepicker">
													<input type="text" readonly="true" value="${startDate }"
														data-date-format="DD/MM/YYYY" id="p_lStart"
														style="background-color: #ffffff !important;"
														class="form-control" /> <span class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span> </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">End
												Date<span class="stars">*</span>
											</label>
											<div class="col-sm-9">
												<div class='input-group date ledgDate' id="datepickers">
													<input type="text" readonly="true" value="${endDate }"
														data-date-format="DD/MM/YYYY" id="p_lEnd"
														style="background-color: #ffffff !important;"
														class="form-control" /> <span class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span> </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Company<span
												class="stars">*</span>
											</label>
											<div class="col-sm-9" id="cmpPlDiv">
												<select class="form-control"
													onchange="filterProfitAndLoss()" id="selectedCompany">
													<c:forEach items="${companies }" var="c">
														<option value="${c.companyId }">${c.companyName }</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="overflow-x: scroll;">
								<div class="col-md-12">
									<div id="panelBody">
										<div id="load-erp">
											<div id="spinneeer-erp"></div>
										</div>
										<input type="checkbox" class="slide_btn_pl" >Details
										<table cellpadding="0" cellspacing="0" border="0"
											class="table table-striped table-bordered erp-tbl"
											id="basic-datatable!">
											<c:set value="${entries.zero }" var="zero" />
											<thead>
												<tr>
													<th width="20%">Expense</th>
													<th width="20%" class="amount">Amount ( ${currencyName }
														)</th>
													<th width="20%">Income</th>
													<th width="20%" class="amount">Amount ( ${currencyName }
														)</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td colspan="2">
														<table class="pla-table ">
															<c:forEach items="${entries.directExpenses }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:if test="${dek.first  }">
																	<tr>
																		<td><strong>${dek.groupName }</strong></td>
																		<td class="amount"></td>
																		<td class="amount"><strong>${dek.amount }</strong></td>
																	</tr>
																	<c:forEach items="${de.value }" var="dev">
																		<tr class="slideDownnPl">
																			<td>${dev.ledgerName }</td>
																			<td class="amount">${dev.amount }</td>
																			<td class="amount"></td>
																		</tr>
																	</c:forEach>
																</c:if>
															</c:forEach>
															<c:forEach items="${entries.directExpenses }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:if test="${! dek.first  }">
																	<tr>
																		<td><strong>${dek.groupName }</strong></td>
																		<td class="amount"></td>
																		<td class="amount"><strong>${dek.amount }</strong></td>
																	</tr>
																	<c:forEach items="${de.value }" var="dev">
																		<tr class="slideDownnPl">
																			<td>${dev.ledgerName }</td>
																			<td class="amount">${dev.amount }</td>
																			<td class="amount"></td>
																		</tr>
																	</c:forEach>
																</c:if>
															</c:forEach>
														</table>
													</td>
													<td colspan="2">
														<table class="pla-table">
															<c:forEach items="${entries.directIncomes }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:if test="${! dek.isLast  }">
																	<tr>
																		<td><strong>${dek.groupName }</strong></td>
																		<td class="amount"></td>
																		<td class="amount"><strong>${dek.amount }</strong></td>
																	</tr>
																	<c:forEach items="${de.value }" var="dev">
																		<tr class="slideDownnPl">
																			<td>${dev.ledgerName }</td>
																			<td class="amount">${dev.amount }</td>
																			<td class="amount"></td>
																		</tr>
																	</c:forEach>
																</c:if>
															</c:forEach>
															<c:forEach items="${entries.directIncomes }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:if test="${ dek.isLast  }">
																	<tr>
																		<td><strong>${dek.groupName }</strong></td>
																		<td class="amount"></td>
																		<td class="amount"><strong>${dek.amount }</strong></td>
																	</tr>
																	<c:forEach items="${de.value }" var="dev">
																		<tr class="slideDownnPl">
																			<td>${dev.ledgerName }</td>
																			<td class="amount">${dev.amount }</td>
																			<td class="amount"></td>
																		</tr>
																	</c:forEach>
																</c:if>
															</c:forEach>
														</table>
													</td>
												</tr>
												<tr>
													<td><strong>Gross Profit</strong></td>
													<td class="amount"><strong> <c:choose>
																<c:when
																	test="${entries.directExpenseGrossProfit == zero}"></c:when>
																<c:otherwise>${entries.directExpenseGrossProfit }</c:otherwise>
															</c:choose>
													</strong></td>
													<td><strong>Gross Loss</strong></td>
													<td class="amount"><strong> <c:choose>
																<c:when test="${entries.directIncomeGrossLoss == zero}"></c:when>
																<c:otherwise>${entries.directIncomeGrossLoss }</c:otherwise>
															</c:choose></strong></td>
												</tr>
												<tr>
													<td><strong>Total</strong></td>
													<td class="amount"><strong> <c:choose>
																<c:when test="${entries.directExpenseTotal == zero}"></c:when>
																<c:otherwise>${entries.directExpenseTotal }</c:otherwise>
															</c:choose>
													</strong></td>
													<td><strong>Total</strong></td>
													<td class="amount"><strong> <c:choose>
																<c:when test="${entries.directIncomeTotal == zero}"></c:when>
																<c:otherwise>${entries.directIncomeTotal }</c:otherwise>
															</c:choose>
													</strong></td>
												</tr>
												<tr>
													<td colspan="2">
														<table class="pla-table ">
															<c:forEach items="${entries.indirectExpenses }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:if test="${dek.first  }">
																	<tr>
																		<td><strong>${dek.groupName }</strong></td>
																		<td class="amount"></td>
																		<td class="amount"><strong>${dek.amount }</strong></td>
																	</tr>
																	<c:forEach items="${de.value }" var="dev">
																		<tr class="slideDownnPl">
																			<td>${dev.ledgerName }</td>
																			<td class="amount">${dev.amount }</td>
																			<td class="amount"></td>
																		</tr>
																	</c:forEach>
																</c:if>
															</c:forEach>
															<c:forEach items="${entries.indirectExpenses }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:choose>
																	<c:when test="${dek.first  }"></c:when>
																	<c:otherwise>
																		<tr>
																			<td><strong>${dek.groupName }</strong></td>
																			<td class="amount"></td>
																			<td class="amount"><strong>${dek.amount }</strong></td>
																		</tr>
																		<c:forEach items="${de.value }" var="dev">
																			<tr class="slideDownnPl">
																				<td>${dev.ledgerName }</td>
																				<td class="amount">${dev.amount }</td>
																				<td class="amount"></td>
																			</tr>
																		</c:forEach>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</table>
													</td>
													<td colspan="2">
														<table class="pla-table">
															<c:forEach items="${entries.indirectIncomes }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:if test="${dek.first }">
																	<tr>
																		<td><strong>${dek.groupName }</strong></td>
																		<td class="amount"></td>
																		<td class="amount"><strong>${dek.amount }</strong></td>
																	</tr>
																	<c:forEach items="${de.value }" var="dev">
																		<tr class="slideDownnPl">
																			<td>${dev.ledgerName }</td>
																			<td class="amount">${dev.amount }</td>
																			<td class="amount"></td>
																		</tr>
																	</c:forEach>
																</c:if>
															</c:forEach>
															<c:forEach items="${entries.indirectIncomes }" var="de">
																<c:set value="${de.key }" var="dek" />
																<c:choose>
																	<c:when test="${dek.first }"></c:when>
																	<c:otherwise>
																		<tr>
																			<td><strong>${dek.groupName }</strong></td>
																			<td class="amount"></td>
																			<td class="amount"><strong>${dek.amount }</strong></td>
																		</tr>
																		<c:forEach items="${de.value }" var="dev">
																			<tr class="slideDownn">
																				<td>${dev.ledgerName }</td>
																				<td class="amount">${dev.amount }</td>
																				<td class="amount"></td>
																			</tr>
																		</c:forEach>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</table>
													</td>
												</tr>
												<tr>
													<td><strong>Net Profit</strong></td>
													<td class="amount"><strong> <c:choose>
																<c:when
																	test="${entries.indirectExpenseNetProfit == zero}"></c:when>
																<c:otherwise>${entries.indirectExpenseNetProfit }</c:otherwise>
															</c:choose>
													</strong></td>
													<td><strong>Net Loss</strong></td>
													<td class="amount"><strong> <c:choose>
																<c:when test="${entries.indirectIncomeNetLoss == zero}"></c:when>
																<c:otherwise>${entries.indirectIncomeNetLoss }</c:otherwise>
															</c:choose>
													</strong></td>
												</tr>
												<tr>
													<td><strong>Total</strong></td>
													<td class="amount"><strong> <c:choose>
																<c:when test="${entries.inddirectExpenseTotal == zero}"></c:when>
																<c:otherwise>${entries.inddirectExpenseTotal }</c:otherwise>
															</c:choose>
													</strong></td>
													<td><strong>Total</strong></td>
													<td class="amount"><strong> <c:choose>
																<c:when test="${entries.indirectIncomeTotal == zero}"></c:when>
																<c:otherwise>${entries.indirectIncomeTotal }</c:otherwise>
															</c:choose>
													</strong></td>
												</tr>
											</tbody>
										</table>
									</div>
									<button type="button" onclick="getProfitAndLossPdf()"
										class="btn btn-primary erp-btn">Generate PDF</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="confirm-delete" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">Do you really want to delete this
						Ledger?</div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="deleteLedger('false')"
								class="btn btn-default">No</button>
							<button type="button" onclick="deleteLedger('true')"
								class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<c:if test="${! empty success_msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>

	<div id="success-msg">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>

	<div id="erro-msg">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>

	<div id="errror-msg" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Alert</h4>
					</div>
					<div class="modal-body"></div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="closeError()"
								class="btn btn-default">ok</button>
							<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script type="text/javascript">
		
	</script>
</body>
</html>