<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}
</style>
</head>
<body>
	<div class="accounts_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">Journals<label style="float: right" class="c-name">${title }</label></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Start
												Date<span class="stars">*</span>
											</label>
											<div class="col-sm-9">
												<div class='input-group date ledgDate' id="datepicker">
													<input type="text" readonly="true" value="${startDate }"
														data-date-format="DD/MM/YYYY" id="journalStart"
														style="background-color: #ffffff !important;"
														class="form-control" /> <span class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span> </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">End
												Date<span class="stars">*</span>
											</label>
											<div class="col-sm-9">
												<div class='input-group date ledgDate' id="datepickers">
													<input type="text" readonly="true" id="journalEnd"
														data-date-format="DD/MM/YYYY" value="${endDate }"
														style="background-color: #ffffff !important;"
														class="form-control" /> <span class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span> </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Company<span
												class="stars">*</span>
											</label>
											<div class="col-sm-9" id="cmpJrDiv">
												<select class="form-control" onchange="filterJournal()" id="selectedCompany">
													<c:forEach items="${companies }" var="c">
														<option value="${c.companyId }">${c.companyName }</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="panelBody">
										<div id="load-erp">
											<div id="spinneeer-erp"></div>
										</div>
										<table cellpadding="0" cellspacing="0" border="0"
											class="table table-striped table-bordered erp-tbl"
											id="basic-datatable">
											<thead>
												<tr>
													<th width="20%">Voucher Number</th>
													<th width="15%">Date</th>
													<th width="20%">From Account</th>
													<th width="20%">To Account</th>
													<th width="15%">Amount</th>
													<th width="15%">Narration</th>
													<th width="5%">PDF</th>
													<th width="5%">Edit</th>
													<th width="5%">Delete</th>

												</tr>
											</thead>
											<tbody>
												<c:forEach items="${journals }" var="j">
													<tr>
														<td>${j.voucherNumber }</td>
														<td>${j.date }</td>
														<td>${j.fromLedger }</td>
														<td>${j.toLedger }</td>
														<td>${j.amount }</td>
														<td>${j.narration }</td>
														<td><a href="journalPdf.do?id=${j.journalId }"><i
																class="fa fa-file-pdf-o"></i></a></td>
														<td><a href="editJournal.do?id=${j.journalId }"><i
																class="fa fa-pencil"></i> </a></td>
														<td><a href="#?id=${j.journalId }"
															onclick="cnfrmDelete('${j.journalId }')"><i
																class="fa fa-times"></i></a></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
									<a href="addJournal.do">
										<button type="button" class="btn btn-primary erp-btn">New
											Journal</button>
									</a>
									<c:if test="${! empty journals }">
										<button type="button" onclick="getJournalsHistoryPdf()"
											class="btn btn-primary erp-btn">Generate PDF</button>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="confirm-delete" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">Do you really want to delete this
						Journal?</div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="deleteJournal('false')"
								class="btn btn-default">No</button>
							<button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<c:if test="${! empty success_msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>

	<div id="success-msg">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>

	<div id="erro-msg">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>


	<div id="errror-msg" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Alert</h4>
					</div>
					<div class="modal-body"></div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="closeError()"
								class="btn btn-default">ok</button>
							<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script type="text/javascript">
		
	</script>
</body>
</html>