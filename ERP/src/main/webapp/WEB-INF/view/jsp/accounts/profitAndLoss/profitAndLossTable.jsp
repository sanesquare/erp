<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<input type="checkbox" class="slide_btn_pl" >
Details
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl"
	id="basic-datatable!">
	<c:set value="${entries.zero }" var="zero" />
	<thead>
		<tr>
			<th width="20%">Expense</th>
			<th width="20%" class="amount">Amount ( ${currencyName } )</th>
			<th width="20%">Income</th>
			<th width="20%" class="amount">Amount ( ${currencyName } )</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="2">
				<table class="pla-table ">
					<c:forEach items="${entries.directExpenses }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:if test="${dek.first  }">
							<tr>
								<td><strong>${dek.groupName }</strong></td>
								<td class="amount"></td>
								<td class="amount"><strong>${dek.amount }</strong></td>
							</tr>
							<c:forEach items="${de.value }" var="dev">
								<tr class="slideDownnPl">
									<td>${dev.ledgerName }</td>
									<td class="amount">${dev.amount }</td>
									<td class="amount"></td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
					<c:forEach items="${entries.directExpenses }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:if test="${! dek.first  }">
							<tr>
								<td><strong>${dek.groupName }</strong></td>
								<td class="amount"></td>
								<td class="amount"><strong>${dek.amount }</strong></td>
							</tr>
							<c:forEach items="${de.value }" var="dev">
								<tr class="slideDownnPl">
									<td>${dev.ledgerName }</td>
									<td class="amount">${dev.amount }</td>
									<td class="amount"></td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
				</table>
			</td>
			<td colspan="2">
				<table class="pla-table">
					<c:forEach items="${entries.directIncomes }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:if test="${! dek.isLast  }">
							<tr>
								<td><strong>${dek.groupName }</strong></td>
								<td class="amount"></td>
								<td class="amount"><strong>${dek.amount }</strong></td>
							</tr>
							<c:forEach items="${de.value }" var="dev">
								<tr class="slideDownnPl">
									<td>${dev.ledgerName }</td>
									<td class="amount">${dev.amount }</td>
									<td class="amount"></td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
					<c:forEach items="${entries.directIncomes }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:if test="${ dek.isLast  }">
							<tr>
								<td><strong>${dek.groupName }</strong></td>
								<td class="amount"></td>
								<td class="amount"><strong>${dek.amount }</strong></td>
							</tr>
							<c:forEach items="${de.value }" var="dev">
								<tr class="slideDownnPl">
									<td>${dev.ledgerName }</td>
									<td class="amount">${dev.amount }</td>
									<td class="amount"></td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
				</table>
			</td>
		</tr>
		<tr>
			<td><strong>Gross Profit</strong></td>
			<td class="amount"><strong> <c:choose>
						<c:when test="${entries.directExpenseGrossProfit == zero}"></c:when>
						<c:otherwise>${entries.directExpenseGrossProfit }</c:otherwise>
					</c:choose>
			</strong></td>
			<td><strong>Gross Loss</strong></td>
			<td class="amount"><strong> <c:choose>
						<c:when test="${entries.directIncomeGrossLoss == zero}"></c:when>
						<c:otherwise>${entries.directIncomeGrossLoss }</c:otherwise>
					</c:choose></strong></td>
		</tr>
		<tr>
			<td><strong>Total</strong></td>
			<td class="amount"><strong> <c:choose>
						<c:when test="${entries.directExpenseTotal == zero}"></c:when>
						<c:otherwise>${entries.directExpenseTotal }</c:otherwise>
					</c:choose>
			</strong></td>
			<td><strong>Total</strong></td>
			<td class="amount"><strong> <c:choose>
						<c:when test="${entries.directIncomeTotal == zero}"></c:when>
						<c:otherwise>${entries.directIncomeTotal }</c:otherwise>
					</c:choose>
			</strong></td>
		</tr>
		<tr>
			<td colspan="2">
				<table class="pla-table ">
					<c:forEach items="${entries.indirectExpenses }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:if test="${dek.first  }">
							<tr>
								<td><strong>${dek.groupName }</strong></td>
								<td class="amount"></td>
								<td class="amount"><strong>${dek.amount }</strong></td>
							</tr>
							<c:forEach items="${de.value }" var="dev">
								<tr class="slideDownnPl">
									<td>${dev.ledgerName }</td>
									<td class="amount">${dev.amount }</td>
									<td class="amount"></td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
					<c:forEach items="${entries.indirectExpenses }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:choose>
							<c:when test="${dek.first  }"></c:when>
							<c:otherwise>
								<tr>
									<td><strong>${dek.groupName }</strong></td>
									<td class="amount"></td>
									<td class="amount"><strong>${dek.amount }</strong></td>
								</tr>
								<c:forEach items="${de.value }" var="dev">
									<tr class="slideDownnPl">
										<td>${dev.ledgerName }</td>
										<td class="amount">${dev.amount }</td>
										<td class="amount"></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</table>
			</td>
			<td colspan="2">
				<table class="pla-table">
					<c:forEach items="${entries.indirectIncomes }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:if test="${dek.first }">
							<tr>
								<td><strong>${dek.groupName }</strong></td>
								<td class="amount"></td>
								<td class="amount"><strong>${dek.amount }</strong></td>
							</tr>
							<c:forEach items="${de.value }" var="dev">
								<tr class="slideDownnPl">
									<td>${dev.ledgerName }</td>
									<td class="amount">${dev.amount }</td>
									<td class="amount"></td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
					<c:forEach items="${entries.indirectIncomes }" var="de">
						<c:set value="${de.key }" var="dek" />
						<c:choose>
							<c:when test="${dek.first }"></c:when>
							<c:otherwise>
								<tr>
									<td><strong>${dek.groupName }</strong></td>
									<td class="amount"></td>
									<td class="amount"><strong>${dek.amount }</strong></td>
								</tr>
								<c:forEach items="${de.value }" var="dev">
									<tr class="slideDownnPl">
										<td>${dev.ledgerName }</td>
										<td class="amount">${dev.amount }</td>
										<td class="amount"></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</table>
			</td>
		</tr>
		<tr>
			<td><strong>Net Profit</strong></td>
			<td class="amount"><strong> <c:choose>
						<c:when test="${entries.indirectExpenseNetProfit == zero}"></c:when>
						<c:otherwise>${entries.indirectExpenseNetProfit }</c:otherwise>
					</c:choose>
			</strong></td>
			<td><strong>Net Loss</strong></td>
			<td class="amount"><strong> <c:choose>
						<c:when test="${entries.indirectIncomeNetLoss == zero}"></c:when>
						<c:otherwise>${entries.indirectIncomeNetLoss }</c:otherwise>
					</c:choose>
			</strong></td>
		</tr>
		<tr>
			<td><strong>Total</strong></td>
			<td class="amount"><strong> <c:choose>
						<c:when test="${entries.inddirectExpenseTotal == zero}"></c:when>
						<c:otherwise>${entries.inddirectExpenseTotal }</c:otherwise>
					</c:choose>
			</strong></td>
			<td><strong>Total</strong></td>
			<td class="amount"><strong> <c:choose>
						<c:when test="${entries.indirectIncomeTotal == zero}"></c:when>
						<c:otherwise>${entries.indirectIncomeTotal }</c:otherwise>
					</c:choose>
			</strong></td>
		</tr>
	</tbody>
</table>
<script type="text/javascript"
	src='<c:url value="/resources/assets/accounts/js/accounts.js"/>'></script>
