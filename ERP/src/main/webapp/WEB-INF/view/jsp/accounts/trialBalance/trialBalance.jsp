<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<style type="text/css">
#success-msg {
	display: none;
}

#erro-msg {
	display: none;
}

.slideDownn {
	display: none;
}

.clSlide {
	display: none;
}

.opSlide {
	display: none;
}

.tsSlide {
	display: none;
}
</style>
</head>
<body>
	<div class="accounts_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Trial Balance<label style="float: right" class="c-name">${title }</label>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Start
												Date<span class="stars">*</span>
											</label>
											<div class="col-sm-9">
												<div class='input-group date ledgDate' id="datepicker">
													<input type="text" readonly="true" value="${startDate }"
														data-date-format="DD/MM/YYYY" id="trialBalanceStart"
														style="background-color: #ffffff !important;"
														class="form-control" /> <span class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span> </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">End
												Date<span class="stars">*</span>
											</label>
											<div class="col-sm-9">
												<div class='input-group date ledgDate' id="datepickers">
													<input type="text" readonly="true" value="${endDate }"
														data-date-format="DD/MM/YYYY" id="trialBalanceEnd"
														style="background-color: #ffffff !important;"
														class="form-control" /> <span class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span> </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="panel-body">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Company<span
												class="stars">*</span>
											</label>
											<div class="col-sm-9" id="cmpTrDiv">
												<select class="form-control" onChange="filterTrialBalance()"
													id="selectedCompany">
													<c:forEach items="${companies }" var="c">
														<option value="${c.companyId }">${c.companyName }</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="row">
								<div></div>
							</div> -->
							<div class="row">
								<div class="col-md-12">
									<div id="panelBody">
										<div id="load-erp">
											<div id="spinneeer-erp"></div>
										</div>
										<input type="checkbox" id="slide_btn">Details <input
											type="checkbox" id="op_slide_btn">Opening Balance <input
											type="checkbox" id="ts_slide_btn">Transactions
										<!-- <input type="checkbox" id="cl_slide_btn">Closing Balance -->
										<table cellpadding="0" cellspacing="0" border="0"
											class="table table-striped table-bordered erp-tbl"
											id="basic-datatable!">
											<thead>
												<tr>
													<!-- <th width="10%">Code</th> -->
													<th width="25%" rowspan="2"
														style="vertical-align: middle; text-align: center;">Particulars</th>
													<th width="25%" colspan="2"
														style="vertical-align: middle; text-align: center;"
														class="opSlide">Opening Balance</th>
													<th width="25%" colspan="2" style="text-align: center;"
														class="tsSlide">Transactions</th>
													<th width="25%" colspan="2" style="text-align: center;">Closing
														Balance</th>
													<%-- <th width="20%">Credit ( ${currencyName })</th> --%>
													<th width="5%" class="clSlide">Closing Balance (
														${currencyName })</th>

												</tr>
												<tr>
													<th width="10%" class="opSlide">Debit ( ${currencyName })</th>
													<th width="10%" class="opSlide">Credit (
														${currencyName })</th>
													<th width="10%" class="tsSlide">Debit ( ${currencyName })</th>
													<th width="10%" class="tsSlide">Credit (
														${currencyName })</th>
													<th width="10%">Debit ( ${currencyName })</th>
													<th width="10%">Credit ( ${currencyName })</th>
													<th width="5%" class="clSlide">Closing Balance (
														${currencyName })</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${entries }" var="group">
													<c:set var="key" value="${group.key }"></c:set>
													<c:choose>
														<c:when test="${key.group =='grand_total!@#'  }"></c:when>
														<c:when test="${key.group =='Closing Stock'  }"></c:when>
														<c:when test="${key.group =='Opening Stock'  }">

															<tr>
																<td><strong>${key.group }</strong></td>
																<td class="opSlide"><strong> <c:choose>
																			<c:when test="${ key.openingDebit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.openingDebit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td class="opSlide"><strong> <c:choose>
																			<c:when test="${ key.openingCredit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.openingCredit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td class="tsSlide"><strong> <c:choose>
																			<c:when test="${ key.transactionDebit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.transactionDebit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td class="tsSlide"><strong> <c:choose>
																			<c:when test="${ key.transactionCredit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.transactionCredit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td><strong><c:choose>
																			<c:when test="${ key.debit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.debit }
																</c:otherwise>
																		</c:choose> </strong></td>
																<td><strong><c:choose>
																			<c:when test="${ key.credit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.credit }
																</c:otherwise>
																		</c:choose></strong></td>
																<td class="clSlide"><strong> <c:choose>
																			<c:when test="${ key.closing eq key.zero}"></c:when>
																			<c:otherwise>
																${key.closing }
																</c:otherwise>
																		</c:choose>
																</strong></td>
															</tr>
														</c:when>
														<c:otherwise>
															<tr>
																<td><strong>${key.group }</strong></td>
																<td class="opSlide"><strong> <c:choose>
																			<c:when test="${ key.openingDebit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.openingDebit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td class="opSlide"><strong> <c:choose>
																			<c:when test="${ key.openingCredit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.openingCredit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td class="tsSlide"><strong> <c:choose>
																			<c:when test="${ key.transactionDebit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.transactionDebit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td class="tsSlide"><strong> <c:choose>
																			<c:when test="${ key.transactionCredit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.transactionCredit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td><strong><c:choose>
																			<c:when test="${ key.debit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.debit }
																</c:otherwise>
																		</c:choose> </strong></td>
																<td><strong><c:choose>
																			<c:when test="${ key.credit eq key.zero}"></c:when>
																			<c:otherwise>
																${key.credit }
																</c:otherwise>
																		</c:choose></strong></td>
																<td class="clSlide"><strong> <c:choose>
																			<c:when test="${ key.closing eq key.zero}"></c:when>
																			<c:otherwise>
																${key.closing }
																</c:otherwise>
																		</c:choose>
																</strong></td>
															</tr>
															<c:forEach items="${group.value }" var="l">
																<tr class="slideDownn">
																	<td>${l.ledgerName }</td>
																	<td class="opSlide"><c:choose>
																			<c:when test="${ l.openingDebit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.openingDebit}
																</c:otherwise>
																		</c:choose></td>
																	<td class="opSlide"><c:choose>
																			<c:when test="${ l.openingCredit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.openingCredit}
																</c:otherwise>
																		</c:choose></td>
																	<td class="tsSlide"><c:choose>
																			<c:when test="${ l.transactionDebit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.transactionDebit}
																</c:otherwise>
																		</c:choose></td>
																	<td class="tsSlide"><c:choose>
																			<c:when test="${ l.transactionCredit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.transactionCredit}
																</c:otherwise>
																		</c:choose></td>
																	<td><c:choose>
																			<c:when test="${ l.debit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.debit}
																</c:otherwise>
																		</c:choose></td>
																	<td><c:choose>
																			<c:when test="${ l.credit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.credit}
																</c:otherwise>
																		</c:choose></td>
																	<td class="clSlide"><c:choose>
																			<c:when test="${ l.balance eq l.zero}"></c:when>
																			<c:otherwise>
																${l.balance}
																</c:otherwise>
																		</c:choose></td>
																</tr>
															</c:forEach>
														</c:otherwise>
													</c:choose>
												</c:forEach>
												<c:forEach items="${entries }" var="group">
													<c:set var="key" value="${group.key }"></c:set>
													<c:if test="${key.group == 'grand_total!@#' }">
														<c:forEach items="${group.value }" var="l">
															<tr>
																<td><strong>Grand Total</strong></td>
																<td class="opSlide"><strong> <c:choose>
																			<c:when test="${ l.totalOpeningDebit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.totalOpeningDebit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td class="opSlide"><strong> <c:choose>
																			<c:when test="${ l.totalOpeningCredit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.totalOpeningCredit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td class="tsSlide"><strong> <c:choose>
																			<c:when test="${ l.totalTransactionDebit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.totalTransactionDebit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td class="tsSlide"><strong> <c:choose>
																			<c:when test="${ l.totalTransactionCredit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.totalTransactionCredit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td><strong> <c:choose>
																			<c:when test="${ l.totalDebit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.totalDebit}
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td><strong> <c:choose>
																			<c:when test="${ l.totalCredit eq l.zero}"></c:when>
																			<c:otherwise>
																${l.totalCredit} 
																</c:otherwise>
																		</c:choose>
																</strong></td>
																<td class="clSlide"><strong> <c:choose>
																			<c:when test="${ l.totalClose eq l.zero}"></c:when>
																			<c:otherwise>
																${l.totalClose}
																</c:otherwise>
																		</c:choose>
																</strong></td>
															</tr>
														</c:forEach>
													</c:if>
												</c:forEach>
											</tbody>
										</table>
										<c:forEach items="${entries }" var="group">
											<c:set var="key" value="${group.key }"></c:set>
											<c:if test="${key.group == 'Closing Stock' }">
												<div class="row">
													<div class="col-md-12">
														<div class="panel-body">
															<div class="form-group">
																<label for="inputPassword3"
																	class="col-md-4 control-label">Adjustments : </label>
															</div>
															<div class="form-group">
															<div class="col-md-1"></div>
																<label for="inputPassword3"
																	class="col-md-2 control-label">Closing Stock: </label>
																<div class="col-md-2">
																	<c:choose>
																		<c:when test="${ key.credit eq key.zero}"></c:when>
																		<c:otherwise>
																			<label>${key.credit}</label>
																		</c:otherwise>
																	</c:choose>
																	<c:choose>
																		<c:when test="${ key.debit eq key.zero}"></c:when>
																		<c:otherwise>
																			<label>${key.debit}</label>
																		</c:otherwise>
																	</c:choose>
																</div>
															</div>
														</div>
													</div>
												</div>
											</c:if>
										</c:forEach>
									</div>
									<!-- <button type="button" onclick="getTrialBalancePdf()"
										class="btn btn-primary erp-btn">Generate PDF</button>  -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="confirm-delete" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">Do you really want to delete this
						Ledger?</div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="deleteLedger('false')"
								class="btn btn-default">No</button>
							<button type="button" onclick="deleteLedger('true')"
								class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<c:if test="${! empty success_msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<c:if test="${! empty error_msg }">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>

	<div id="success-msg">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>

	<div id="erro-msg">
		<div class="notification-panl er-msg">
			<div class="notfctn-cntnnt">${error_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>


	<div id="errror-msg" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Alert</h4>
					</div>
					<div class="modal-body"></div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="closeError()"
								class="btn btn-default">ok</button>
							<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/accounts/js/accounts.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/erp/js/erp.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script type="text/javascript">
		
	</script>
</body>
</html>