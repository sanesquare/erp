<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title><spring:message code="project.title" /></title>
</head>
<body>
	<section class="">
		<tiles:insertAttribute name="header" />
		
		<tiles:insertAttribute name="body" />
		
		<tiles:insertAttribute name="footer" />
	</section>
</body>
</html>
