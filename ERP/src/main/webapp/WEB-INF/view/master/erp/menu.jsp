
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<body>
	<aside class="left-panel collapsed">

		<div class="user text-center">
			<img src="resources/assets/images/logo1.png" class="img-circle">
		</div>



		<nav class="navigation">
			<ul class="list-unstyled">
				<li id="dashbordli"><a href="Dashboard" title="Dashboard"><i
						class="fa fa-home"></i><span class="nav-label">Home</span></a></li>
				<sec:authorize ifAllGranted="ADMINUSER">
					<li id="organzationli" class="has-submenu"><a href="#"
						title="Organisation"><i class="fa fa-building-o"></i> <span
							class="nav-label">Organisation</span></a>
						<ul class="list-unstyled">
							<c:if
								test="${ rolesMap.roles['Branch'].add || rolesMap.roles['Branch'].view || rolesMap.roles['Branch'].update || rolesMap.roles['Branch'].delete}">
								<li><a href="branches" title="Organisation"><i
										class="fa fa-building-o"></i>Branches</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Departments'].add || rolesMap.roles['Departments'].view || rolesMap.roles['Departments'].update || rolesMap.roles['Departments'].delete}">
								<li><a href="department"><i class="fa fa-sitemap"></i>Departments</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Projects'].add || rolesMap.roles['Projects'].view || rolesMap.roles['Projects'].update || rolesMap.roles['Projects'].delete}">
								<li><a href="projects"><i class="fa fa-file-o"></i>Projects</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Meetings'].add || rolesMap.roles['Meetings'].view || rolesMap.roles['Meetings'].update || rolesMap.roles['Meetings'].delete}">
								<li><a href="meetings"><i class="fa fa-users"></i>Meetings</a></li>
							</c:if>
							<!-- <li><a href="#"><i class="fa fa-newspaper-o"></i>Organization
								News</a></li>
						<li><a href="#"><i class="fa fa-book"></i> Organization
								Profile</a></li>
								News</a></li> -->
							<!-- <li><a href="#"><i class="fa fa-book"></i> Organization
								Profile</a></li> -->
							<c:if
								test="${ rolesMap.roles['Organization Policies'].add || rolesMap.roles['Organization Policies'].view || rolesMap.roles['Organization Policies'].update || rolesMap.roles['Organization Policies'].delete}">
								<li><a href="AddOrganizationPolicy"><i
										class="fa fa-bookmark-o"></i>Organization Policies</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Announcements'].add || rolesMap.roles['Announcements'].view || rolesMap.roles['Announcements'].update || rolesMap.roles['Announcements'].delete}">
								<li><a href="announcements"><i class="fa fa-bullhorn"></i>Announcements</a></li>
							</c:if>
							<sec:authorize access="hasRole('ADMINUSER')">
								<li><a href="settings"><i class="fa fa-cog"></i>System
										Settings</a></li>
							</sec:authorize>
							<c:if test="${ rolesMap.roles['System Log'].view}">
								<li><a href="userActivities"><i
										class="glyphicon glyphicon-log-in"></i>User Activities</a></li>
							</c:if>

						</ul></li>
				</sec:authorize>
				<sec:authorize ifNotGranted="ADMINUSER">
					<c:if
						test="${ rolesMap.roles['Branch'].add || rolesMap.roles['Branch'].view || rolesMap.roles['Branch'].update || rolesMap.roles['Branch'].delete || 
						rolesMap.roles['Branch'].add || rolesMap.roles['Branch'].view || rolesMap.roles['Branch'].update || rolesMap.roles['Branch'].delete || 
						rolesMap.roles['Departments'].add || rolesMap.roles['Departments'].view || rolesMap.roles['Departments'].update || rolesMap.roles['Departments'].delete ||
						 rolesMap.roles['Projects'].add || rolesMap.roles['Projects'].view || rolesMap.roles['Projects'].update || rolesMap.roles['Projects'].delete ||
						 rolesMap.roles['Meetings'].add || rolesMap.roles['Meetings'].view || rolesMap.roles['Meetings'].update || rolesMap.roles['Meetings'].delete ||
						  rolesMap.roles['Organization Policies'].add || rolesMap.roles['Organization Policies'].view || rolesMap.roles['Organization Policies'].update || rolesMap.roles['Organization Policies'].delete ||
						 rolesMap.roles['Announcements'].add || rolesMap.roles['Announcements'].view || rolesMap.roles['Announcements'].update || rolesMap.roles['Announcements'].delete || rolesMap.roles['System Log'].view}">
						<li class="has-submenu"><a href="#"><i
								class="fa fa-building-o"></i> <span class="nav-label">Organisation</span></a>
							<ul class="list-unstyled">
								<c:if
									test="${ rolesMap.roles['Branch'].add || rolesMap.roles['Branch'].view || rolesMap.roles['Branch'].update || rolesMap.roles['Branch'].delete}">
									<li><a href="branches"><i class="fa fa-building-o"></i>Branches</a></li>
								</c:if>
								<c:if
									test="${ rolesMap.roles['Departments'].add || rolesMap.roles['Departments'].view || rolesMap.roles['Departments'].update || rolesMap.roles['Departments'].delete}">
									<li><a href="department"><i class="fa fa-sitemap"></i>Departments</a></li>
								</c:if>
								<c:if
									test="${ rolesMap.roles['Projects'].add || rolesMap.roles['Projects'].view || rolesMap.roles['Projects'].update || rolesMap.roles['Projects'].delete}">
									<li><a href="projects"><i class="fa fa-file-o"></i>Projects</a></li>
								</c:if>
								<c:if
									test="${ rolesMap.roles['Meetings'].add || rolesMap.roles['Meetings'].view || rolesMap.roles['Meetings'].update || rolesMap.roles['Meetings'].delete}">
									<li><a href="meetings"><i class="fa fa-users"></i>Meetings</a></li>
								</c:if>
								<!-- <li><a href="#"><i class="fa fa-newspaper-o"></i>Organization
								News</a></li>-->
								<c:if
									test="${ rolesMap.roles['Organization Policies'].add || rolesMap.roles['Organization Policies'].view || rolesMap.roles['Organization Policies'].update || rolesMap.roles['Organization Policies'].delete}">
									<li><a href="OrganizationPolicy"><i class="fa fa-book"></i>
											Organization Policies</a></li>
								</c:if>
								<!-- <li><a href="#"><i class="fa fa-book"></i> Organization
								Profile</a></li> -->
								<!-- <li><a href="#"><i class="fa fa-bookmark-o"></i>Organization
								Policies</a></li>  -->
								<c:if
									test="${ rolesMap.roles['Announcements'].add || rolesMap.roles['Announcements'].view || rolesMap.roles['Announcements'].update || rolesMap.roles['Announcements'].delete}">
									<li><a href="announcements"><i class="fa fa-bullhorn"></i>Announcements</a></li>
								</c:if>
								<sec:authorize access="hasRole('ADMINUSER')">
									<li><a href="settings"><i class="fa fa-cog"></i>System
											Settings</a></li>
								</sec:authorize>
								<c:if test="${ rolesMap.roles['System Log'].view}">
									<li><a href="userActivities"><i
											class="glyphicon glyphicon-log-in"></i>User Activities</a></li>
								</c:if>

							</ul></li>
					</c:if>
				</sec:authorize>



				<c:if
					test="${ rolesMap.roles['Job Posts'].add || rolesMap.roles['Job Posts'].view || rolesMap.roles['Job Posts'].update || rolesMap.roles['Job Posts'].delete || 
			rolesMap.roles['Job Candidates'].add || rolesMap.roles['Job Candidates'].view || rolesMap.roles['Job Candidates'].update || rolesMap.roles['Job Candidates'].delete ||
			rolesMap.roles['Job Interviews'].add || rolesMap.roles['Job Interviews'].view || rolesMap.roles['Job Interviews'].update || rolesMap.roles['Job Interviews'].delete}">
					<li id="recruitmentli" class="has-submenu"><a href="#"
						title="Recruitment"><i class="fa fa-star-o"></i> <span
							class="nav-label">Recruitment</span></a>
						<ul class="list-unstyled">
							<c:if
								test="${ rolesMap.roles['Job Posts'].add || rolesMap.roles['Job Posts'].view || rolesMap.roles['Job Posts'].update || rolesMap.roles['Job Posts'].delete}">
								<li><a href="jobPosts"><i
										class="glyphicon glyphicon-lock"></i>Job Posts</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Job Candidates'].add || rolesMap.roles['Job Candidates'].view || rolesMap.roles['Job Candidates'].update || rolesMap.roles['Job Candidates'].delete}">
								<li><a href="jobCandidates"><i
										class="glyphicon glyphicon-user"></i>Job Candidates</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Job Interviews'].add || rolesMap.roles['Job Interviews'].view || rolesMap.roles['Job Interviews'].update || rolesMap.roles['Job Interviews'].delete}">
								<li><a href="jobInterviews"><i
										class="glyphicon glyphicon-comment"></i>Job Interviews</a></li>
							</c:if>
							<li><a href="addRecLeters"><i
									class="glyphicon glyphicon-comment"></i>Letters</a></li>
						</ul></li>
				</c:if>



				<c:if
					test="${ rolesMap.roles['Employees'].add || rolesMap.roles['Employees'].view || rolesMap.roles['Employees'].update || rolesMap.roles['Employees'].delete ||
 rolesMap.roles['Employee Joining'].add || rolesMap.roles['Employee Joining'].view || rolesMap.roles['Employee Joining'].update || rolesMap.roles['Employee Joining'].delete ||
  rolesMap.roles['Employee Contracts'].add || rolesMap.roles['Employee Contracts'].view || rolesMap.roles['Employee Contracts'].update || rolesMap.roles['Employee Contracts'].delete ||
   rolesMap.roles['Assignments'].add || rolesMap.roles['Assignments'].view || rolesMap.roles['Assignments'].update || rolesMap.roles['Assignments'].delete ||
   rolesMap.roles['Transfers'].add || rolesMap.roles['Transfers'].view || rolesMap.roles['Transfers'].update || rolesMap.roles['Transfers'].delete ||
  rolesMap.roles['Resignations'].add || rolesMap.roles['Resignations'].view || rolesMap.roles['Resignations'].update || rolesMap.roles['Resignations'].delete ||
   rolesMap.roles['Employee Travel'].add || rolesMap.roles['Employee Travel'].view || rolesMap.roles['Employee Travel'].update || rolesMap.roles['Employee Travel'].delete ||
    rolesMap.roles['Terminations'].add || rolesMap.roles['Terminations'].view || rolesMap.roles['Terminations'].update || rolesMap.roles['Terminations'].delete ||
    rolesMap.roles['Performance Evaluation'].add || rolesMap.roles['Performance Evaluation'].view || rolesMap.roles['Performance Evaluation'].update || rolesMap.roles['Performance Evaluation'].delete}">
					<li id="employeeli" class="has-submenu"><a href="#"
						title="Employees"><i class="fa fa-users"></i> <span
							class="nav-label">Employees</span></a>
						<ul class="list-unstyled">
							<c:if
								test="${ rolesMap.roles['Employees'].add || rolesMap.roles['Employees'].view || rolesMap.roles['Employees'].update || rolesMap.roles['Employees'].delete}">
								<li><a href="employees"><i class="fa fa-user"></i>Employee</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Employee Joining'].add || rolesMap.roles['Employee Joining'].view || rolesMap.roles['Employee Joining'].update || rolesMap.roles['Employee Joining'].delete}">
								<li><a href="employeeJoining"><i
										class="glyphicon glyphicon-log-in"></i>Employees Joining</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Employee Contracts'].add || rolesMap.roles['Employee Contracts'].view || rolesMap.roles['Employee Contracts'].update || rolesMap.roles['Employee Contracts'].delete}">
								<li><a href="contracts"><i class="fa fa-file-text"></i>Contracts</a></li>
							</c:if>
							<!-- <li><a href="#"><i class="glyphicon glyphicon-calendar"></i>Events</a></li> -->
							<c:if
								test="${ rolesMap.roles['Assignments'].add || rolesMap.roles['Assignments'].view || rolesMap.roles['Assignments'].update || rolesMap.roles['Assignments'].delete}">
								<li><a href="assignments"><i
										class="glyphicon glyphicon-briefcase"></i>Assignments</a></li>
							</c:if>
							<!-- <li><a href="#"><i class="fa fa-file-text-o"></i>Employee
								Forms</a></li>
						<li><a href="#"><i class="fa fa-table"></i>Requisitions</a></li> -->
							<c:if
								test="${ rolesMap.roles['Transfers'].add || rolesMap.roles['Transfers'].view || rolesMap.roles['Transfers'].update || rolesMap.roles['Transfers'].delete}">
								<li><a href="transfer"><i
										class="glyphicon glyphicon-refresh"></i>Transfers</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Resignations'].add || rolesMap.roles['Resignations'].view || rolesMap.roles['Resignations'].update || rolesMap.roles['Resignations'].delete}">
								<li><a href="resignation"><i class="fa fa-comment-o"></i>Resignations</a></li>
							</c:if>
							<!-- 	<li><a href="achievements"><i class="fa fa-trophy"></i>Achievements</a></li> -->
							<!-- <li><a href="#"><i class="fa fa-table"></i>Requisitions</a></li> -->
							<c:if
								test="${ rolesMap.roles['Employee Travel'].add || rolesMap.roles['Employee Travel'].view || rolesMap.roles['Employee Travel'].update || rolesMap.roles['Employee Travel'].delete}">
								<li><a href="travel"><i class="fa fa-fighter-jet"></i>Travels</a></li>
							</c:if>
							<!-- <li><a href="promotion"><i class="fa fa-star-o"></i>Promotions</a></li> -->
							<!-- <li><a href="complaints"><i
								class="glyphicon glyphicon-warning-sign"></i>Complaints</a></li> -->
							<!-- <li><a href="Warnings"><i
								class="glyphicon glyphicon-warning-sign"></i>Warnings</a></li> -->
							<!-- <li><a href="Memos"><i class="fa fa-list-alt"></i>Memos</a></li> -->
							<c:if
								test="${ rolesMap.roles['Terminations'].add || rolesMap.roles['Terminations'].view || rolesMap.roles['Terminations'].update || rolesMap.roles['Terminations'].delete}">
								<li><a href="Termination"><i class="fa fa-close"></i>Terminations</a></li>
							</c:if>
							<!-- <li><a href="#"><i class="fa fa-pie-chart"></i>Polls</a></li> -->
							<c:if
								test="${ rolesMap.roles['Performance Evaluation'].add || rolesMap.roles['Performance Evaluation'].view || rolesMap.roles['Performance Evaluation'].update || rolesMap.roles['Performance Evaluation'].delete}">
								<li><a href="PerformanceEvaluation"><i
										class="glyphicon glyphicon-star"></i>Performance Evaluations</a></li>
							</c:if>
							<!-- <li><a href="EmployeeExit"><i
						<li><a href="PerformanceEvaluation"><i
								class="glyphicon glyphicon-star"></i>Performance Evaluations</a></li>
						<!-- <li><a href="EmployeeExit"><i
								class="glyphicon glyphicon-log-out"></i>Employee Exit</a></li> -->

						</ul></li>
				</c:if>

				<c:if
					test="${ rolesMap.roles['Attendance'].add || rolesMap.roles['Attendance'].view || rolesMap.roles['Attendance'].update || rolesMap.roles['Attendance'].delete ||
	rolesMap.roles['Leaves'].add || rolesMap.roles['Leaves'].view || rolesMap.roles['Leaves'].update || rolesMap.roles['Leaves'].delete || rolesMap.roles['Work Shifts'].add ||
	rolesMap.roles['Work Sheets'].add || rolesMap.roles['Work Sheets'].view || rolesMap.roles['Work Sheets'].update || rolesMap.roles['Work Sheets'].delete || rolesMap.roles['Holidays'].add || rolesMap.roles['Holidays'].view || rolesMap.roles['Holidays'].update || rolesMap.roles['Holidays'].delete||
	 rolesMap.roles['Attendance Status'].add}">
					<li id="timesheetli" class="has-submenu"><a href="#"
						title="Timesheet"><i class="fa fa-clock-o"></i> <span
							class="nav-label">Timesheet</span></a>
						<ul class="list-unstyled">
							<c:if
								test="${ rolesMap.roles['Attendance'].add || rolesMap.roles['Attendance'].view || rolesMap.roles['Attendance'].update || rolesMap.roles['Attendance'].delete}">
								<li><a href="attendance"><i class="fa fa-clock-o"></i>Attendance
								</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Leaves'].add || rolesMap.roles['Leaves'].view || rolesMap.roles['Leaves'].update || rolesMap.roles['Leaves'].delete}">
								<li><a href="leaves"><i class="fa fa-envelope-o"></i>Leaves</a></li>
							</c:if>
							<c:if test="${ rolesMap.roles['Work Shifts'].add }">
								<li><a href="SaveWorkShift"><i
										class="glyphicon glyphicon-random"></i>Work Shifts</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Work Sheets'].add || rolesMap.roles['Work Sheets'].view || rolesMap.roles['Work Sheets'].update || rolesMap.roles['Work Sheets'].delete}">
								<li><a href="worksheets"><i class="fa fa-columns"></i>Worksheet
								</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Holidays'].add || rolesMap.roles['Holidays'].view || rolesMap.roles['Holidays'].update || rolesMap.roles['Holidays'].delete}">
								<li><a href="Holidays"><i
										class="glyphicon glyphicon-calendar"></i>Holidays </a></li>
							</c:if>
							<c:if test="${ rolesMap.roles['Attendance Status'].add}">
								<li><a href="addAttenStatus"><i
										class="glyphicon glyphicon-calendar"></i>Attendance Status </a></li>
							</c:if>
						</ul></li>
				</c:if>


				<c:if
					test="${ rolesMap.roles['Salary'].add || rolesMap.roles['Salary Payslips'].add || rolesMap.roles['Salary Payslips'].view || rolesMap.roles['Salary Payslips'].update || rolesMap.roles['Salary Payslips'].delete ||
			rolesMap.roles['Payroll Structure'].add || rolesMap.roles['Hourly Wages'].add || rolesMap.roles['Hourly Wages'].view || rolesMap.roles['Hourly Wages'].update || rolesMap.roles['Hourly Wages'].delete ||
			 rolesMap.roles['Daily Wages'].add || rolesMap.roles['Daily Wages'].view || rolesMap.roles['Daily Wages'].update || rolesMap.roles['Daily Wages'].delete ||
			 rolesMap.roles['Deductions'].add || rolesMap.roles['Deductions'].view || rolesMap.roles['Deductions'].update || rolesMap.roles['Deductions'].delete ||
			 rolesMap.roles['Bonuses'].add || rolesMap.roles['Bonuses'].view || rolesMap.roles['Bonuses'].update || rolesMap.roles['Bonuses'].delete || rolesMap.roles['Commissions'].add || rolesMap.roles['Commissions'].view || rolesMap.roles['Commissions'].update || rolesMap.roles['Commissions'].delete ||
			  rolesMap.roles['Pay Salary'].add || rolesMap.roles['Pay Salary'].view || rolesMap.roles['Pay Pay Salary'].update || rolesMap.roles['Pay Salary'].delete || 
			 rolesMap.roles['Adjustments'].add || rolesMap.roles['Adjustments'].view || rolesMap.roles['Adjustments'].update || rolesMap.roles['Adjustments'].delete ||
			 rolesMap.roles['ESI'].add || rolesMap.roles['ESI'].view || rolesMap.roles['ESI'].update || rolesMap.roles['ESI'].delete || rolesMap.roles['Reimbursements'].add || rolesMap.roles['Reimbursements'].view || rolesMap.roles['Reimbursements'].update || rolesMap.roles['Reimbursements'].delete ||
			rolesMap.roles['Overtimes'].add || rolesMap.roles['Overtimes'].view || rolesMap.roles['Overtimes'].update || rolesMap.roles['Overtimes'].delete || rolesMap.roles['Provident Fund'].add || rolesMap.roles['Provident Fund'].view || rolesMap.roles['Provident Fund'].update || rolesMap.roles['Provident Fund'].delete ||
			rolesMap.roles['Advance Salary'].add || rolesMap.roles['Advance Salary'].view || rolesMap.roles['Advance Salary'].update || rolesMap.roles['Advance Salary'].delete || rolesMap.roles['Loans'].add || rolesMap.roles['Loans'].view || rolesMap.roles['Loans'].update || rolesMap.roles['Loans'].delete}">
					<li id="payrollli" class="has-submenu"><a href="#"
						title="Payroll"><i class="fa fa-calculator"></i> <span
							class="nav-label">Payroll</span></a>
						<ul class="list-unstyled">
							<c:if test="${ rolesMap.roles['Salary'].add}">
								<li><a href="salary"><i class="fa fa-dollar"></i>Salary</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Salary Payslips'].add || rolesMap.roles['Salary Payslips'].view || rolesMap.roles['Salary Payslips'].update || rolesMap.roles['Salary Payslips'].delete}">
								<li><a href="payslip"><i class="fa fa-money"></i>Salary
										Payslips</a></li>
							</c:if>
							<c:if test="${ rolesMap.roles['Payroll Structure'].add}">
								<li><a href="PayrollStructure"><i
										class="fa fa-database"></i>Payroll Structure</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Hourly Wages'].add || rolesMap.roles['Hourly Wages'].view || rolesMap.roles['Hourly Wages'].update || rolesMap.roles['Hourly Wages'].delete}">
								<li><a href="hourlyWages"><i class="fa fa-reorder"></i>Hourly
										Wages</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Daily Wages'].add || rolesMap.roles['Daily Wages'].view || rolesMap.roles['Daily Wages'].update || rolesMap.roles['Daily Wages'].delete}">
								<li><a href="dailyWages"><i class="fa fa-reorder"></i>Daily
										Wages</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Deductions'].add || rolesMap.roles['Deductions'].view || rolesMap.roles['Deductions'].update || rolesMap.roles['Deductions'].delete}">
								<li><a href="deductions"><i
										class="fa fa-minus-square-o"></i>Deductions</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Bonuses'].add || rolesMap.roles['Bonuses'].view || rolesMap.roles['Bonuses'].update || rolesMap.roles['Bonuses'].delete}">
								<li><a href="bonuses"><i class="fa fa-plus-square-o"></i>Bonuses</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Commissions'].add || rolesMap.roles['Commissions'].view || rolesMap.roles['Commissions'].update || rolesMap.roles['Commissions'].delete}">
								<li><a href="commissions"><i class="fa fa-calculator"></i>Incentives</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Adjustments'].add || rolesMap.roles['Adjustments'].view || rolesMap.roles['Adjustments'].update || rolesMap.roles['Adjustments'].delete}">
								<li><a href="adjustments"><i class="fa fa-money"></i>Adjustments</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['ESI'].add || rolesMap.roles['ESI'].view || rolesMap.roles['ESI'].update || rolesMap.roles['ESI'].delete}">
								<li><a href="Esi"><i class="fa fa-clock-o"></i>ESI</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Reimbursements'].add || rolesMap.roles['Reimbursements'].view || rolesMap.roles['Reimbursements'].update || rolesMap.roles['Reimbursements'].delete}">
								<li><a href="reimbursementsHome"><i class="fa fa-money"></i>Reimbursements</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Overtimes'].add || rolesMap.roles['Overtimes'].view || rolesMap.roles['Overtimes'].update || rolesMap.roles['Overtimes'].delete}">
								<li><a href="Overtime"><i class="fa fa-clock-o"></i>Overtimes</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Provident Fund'].add || rolesMap.roles['Provident Fund'].view || rolesMap.roles['Provident Fund'].update || rolesMap.roles['Provident Fund'].delete}">
								<li><a href="ProfidentFunds"><i class="fa fa-cube"></i>Provident
										Fund</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Advance Salary'].add || rolesMap.roles['Advance Salary'].view || rolesMap.roles['Advance Salary'].update || rolesMap.roles['Advance Salary'].delete}">
								<li><a href="AdvanceSalary"><i class="fa fa-dollar"></i>Advance
										Salary</a></li>
							</c:if>
							<c:if
								test="${ rolesMap.roles['Loans'].add || rolesMap.roles['Loans'].view || rolesMap.roles['Loans'].update || rolesMap.roles['Loans'].delete}">
								<li><a href="Loan"><i class="fa fa-calculator"></i>Loans</a></li>
							</c:if>
							<c:if test="${ rolesMap.roles['Pay Salary'].add || rolesMap.roles['Pay Salary'].view || rolesMap.roles['Pay Salary'].update || rolesMap.roles['Pay Salary'].delete}">
							<li><a href="paysalary"><i class="fa fa-money"></i>
										Pay Salary</a></li></c:if>
							<!-- <li><a href="Insurance"><i class="fa fa-life-ring"></i>Insurance</a></li> -->
						</ul></li>
				</c:if>


				<c:if
					test="${ rolesMap.roles['Recruitment Reports'].view || rolesMap.roles['HR Report'].view
	|| rolesMap.roles['Employee Reports'].view || rolesMap.roles['Time Sheet Reports'].view ||
	 rolesMap.roles['Pay Roll Reports'].view }">
					<li id="reportsli" class="has-submenu"><a href="#"
						title="Reports"><i class="fa fa-pie-chart"></i> <span
							class="nav-label">Reports</span></a>
						<ul class="list-unstyled">
							<c:if test="${ rolesMap.roles['HR Report'].view}">
								<li><a href="hrReports"> <i class="fa fa-user"></i> HR
										Report
								</a></li>
							</c:if>
							<c:if test="${ rolesMap.roles['Recruitment Reports'].view}">
								<li><a href="recReports"><i
										class="glyphicon glyphicon-briefcase"></i>Recruitment Reports</a></li>
							</c:if>
							<c:if test="${ rolesMap.roles['Employee Reports'].view}">
								<li><a href="empReports"><i class="fa fa-user"></i>Employees
										Reports</a></li>
							</c:if>
							<c:if test="${ rolesMap.roles['Time Sheet Reports'].view}">
								<li><a href="timeReports"><i class="fa fa-clock-o"></i>Timesheet
										Reports</a></li>
							</c:if>
							<c:if test="${ rolesMap.roles['Pay Roll Reports'].view}">
								<li><a href="payReports"><i class="fa fa-money"></i>Payroll
										Reports</a></li>
							</c:if>
							<%-- 		<c:if test="${ rolesMap.roles['HR Report'].view}">
						<li><a href="#"><i class="fa fa-graduation-cap"></i>Training
								Reports</a></li>
								</c:if>
								<c:if test="${ rolesMap.roles['HR Report'].view}">
						<li><a href="#"><i class="fa fa-bar-chart"></i>Graphs</a></li>
						</c:if>
						<c:if test="${ rolesMap.roles['HR Report'].view}">
						<li><a href="#"><i class="fa fa-cogs"></i>Report
								Generator</a></li>
								</c:if> --%>


						</ul></li>
				</c:if>

				<!-- <li class="has-submenu"><a href="#"><i
						class="fa fa-question-circle"></i><span class="nav-label">Help</span></a>
					<ul class="list-unstyled">
						<li><a href="#"><i class="fa fa-list"></i>Help Topics</a></li>
						<li><a href="#"><i class="fa fa-comments-o"></i>FAQ's</a></li>
						<li><a href="#"><i class="fa fa-ticket"></i>Support
								Tickets</a></li>
						<li><a href="#"><i class="fa fa-mobile-phone"></i>Request
								a Call Bank</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i>Contact
								Us</a></li>
						<li><a href="#"><i class="fa fa-headphones"></i>Live
								Support</a></li>


					</ul></li> -->

			</ul>
		</nav>
	</aside>
	<!-- Aside Ends-->
</body>
</html>