<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>SNOGOL ERP</title>

<!-- Bootstrap core CSS -->
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />
<!-- Calendar Styling  -->
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/calendar/calendar.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>
<!-- Base Styling  -->
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/hrms/css/hrms-css.css'/>" />
<%-- <link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/RegalCalendar.css'/>" /> --%>

<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/jquery.growl.css'/>" />
<style type="text/css">
#new_pwd_ok {
	display: none
}

#new_pwd_err {
	display: none
}

#old_pwd_ok {
	display: none
}

#old_pwd_err {
	display: none
}

#pwd_success {
	display: none
}

#pwd_err {
	display: none
}

.logo_width {
	width: 57px;
	margin-top: -6px;
}
</style>
</head>
<body>



	<header class="top-head container-fluid"> <input type="hidden"
		value="${authEmpCode}" id="authEmpCode" />
	<button type="button" class="navbar-toggle pull-left">
		<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
		<span class="icon-bar"></span> <span class="icon-bar"></span>
	</button>

	<%-- <form role="search" class="navbar-left app-search pull-left hidden-xs">
		<input type="text" placeholder="Enter keywords..."
			class="form-control form-control-circle">
	</form>

	<nav class=" navbar-default hidden-xs" role="navigation">
	<ul class="nav navbar-nav">
		<li><a href="#" id="button_2">Link</a></li>
		<li class="dropdown"><a data-toggle="dropdown"
			class="dropdown-toggle" href="#">Dropdown <span class="caret"></span></a>
			<ul role="menu" class="dropdown-menu">
				<li><a href="#">Action</a></li>
				<li><a href="#">Another action</a></li>
				<li><a href="#">Something else here</a></li>
				<li class="divider"></li>
				<li><a href="#">Separated link</a></li>
				<li class="divider"></li>
				<li><a href="#">One more separated link</a></li>
			</ul></li>
	</ul>
	</nav> --%> <!-- <li class="dropdown"><a href="#" data-toggle="dropdown"><i
				class="fa fa-comments-o"></i> <span class="badge bg-warning">7</span></a>
			<div
				class="dropdown-menu md arrow pull-right panel panel-default arrow-top-right messages-dropdown">
				<div class="panel-heading">Messages</div>

				<div class="list-group">

					<a href="#" class="list-group-item">
						<div class="media">
							<div class="user-status busy pull-left">
								<img class="media-object img-circle pull-left"
									src="assets/images/avtar/user2.png" alt="user#1" width="40">
							</div>
							<div class="media-body">
								<h5 class="media-heading">Lorem ipsum dolor sit consect....</h5>
								<small class="text-muted">23 Sec ago</small>
							</div>
						</div>
					</a> <a href="#" class="list-group-item">
						<div class="media">
							<div class="user-status offline pull-left">
								<img class="media-object img-circle pull-left"
									src="assets/images/avtar/user3.png" alt="user#1" width="40">
							</div>
							<div class="media-body">
								<h5 class="media-heading">Nunc elementum, enim vitae</h5>
								<small class="text-muted">23 Sec ago</small>
							</div>
						</div>
					</a> <a href="#" class="list-group-item">
						<div class="media">
							<div class="user-status invisibled pull-left">
								<img class="media-object img-circle pull-left"
									src="assets/images/avtar/user4.png" alt="user#1" width="40">
							</div>
							<div class="media-body">
								<h5 class="media-heading">Praesent lacinia, arcu eget</h5>
								<small class="text-muted">23 Sec ago</small>
							</div>
						</div>
					</a> <a href="#" class="list-group-item">
						<div class="media">
							<div class="user-status online pull-left">
								<img class="media-object img-circle pull-left"
									src="assets/images/avtar/user5.png" alt="user#1" width="40">
							</div>
							<div class="media-body">
								<h5 class="media-heading">In mollis blandit tempor.</h5>
								<small class="text-muted">23 Sec ago</small>
							</div>
						</div>
					</a> <a href="#" class="btn btn-info btn-flat btn-block">View All
						Messages</a>

				</div>

			</div></li> --> <%-- <li class="dropdown"><a href="#" data-toggle="dropdown"><i
				class="fa fa-bell-o"></i><span class="badge bg-warning"
				id="notification${authEmpCode}"></span></a>
			<div
				class="dropdown-menu arrow pull-right md panel panel-default arrow-top-right notifications">
				<div class="panel-heading">Notification</div>
				<div class="list-group" id="notification_list${authEmpCode}">
					<!-- <a href="#" class="list-group-item"><div class="pro_pic"><img src="resources/images/img-1.jpg"/></div>
					  Fusce dapibus molestie
						tincidunt. Quisque facilisis libero eget justo iaculis </a> -->
				</div>
			</div></li> --%>




	<div style="float: right;">
		<div class="companyName-div">
			<span class="companyName">${companyName }</span>
		</div>
		<ul class="nav-toolbar">
			<li class="dropdown"><a href="#" data-toggle="dropdown"><i
					class="fa fa-user"></i></a>
				<div
					class="dropdown-menu arrow pull-right md panel panel-default arrow-top-right notifications">


					<div class="list-group">

						<a class="list-group-item" data-modal-id="pdtPop"> Profile :
							${authEmployeeDetails.name} </a> <a href="#" class="list-group-item"
							data-modal-id="popup1"> Change Password </a> <a href="logout"
							class="list-group-item"> Logout </a>

					</div>

				</div></li>




		</ul>
	</div>

	<div id="pdtPop" style="display: none;" class="modal-box">
		<header> <a href="#" class="js-modal-close closes">�</a><h3> My
		Information</h3> </header>
		<div class="modal-body">
			<div class="panel-body">
				<c:if test="${! empty authEmployeeDetails.profilePic }">
					<div class="form-group">
						<div class="photo_show">
							<img src='<c:url value="${authEmployeeDetails.profilePic }" />' />
						</div>
					</div>
				</c:if>
				<div class="form-group">
					<label class="col-sm-4 control-label">Name</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.name}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Code</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.employeeCode}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">User name</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.userName}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Branch</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.employeeBranch}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Department</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.employeeDepartment}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Designation</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.designation}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Join Date</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.joiningDate}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Shift</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.workShift}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- <li class="dropdown"><a href="#" data-toggle="dropdown"><i
				class="fa fa-ellipsis-v"></i></a>
			<div
				class="dropdown-menu lg pull-right arrow panel panel-default arrow-top-right">
				<div class="panel-heading">More Apps</div>
				<div class="panel-body text-center">
					<div class="row">
						<div class="col-xs-6 col-sm-4">
							<a href="#" class="text-green"><span class="h2"><i
									class="fa fa-envelope-o"></i></span>
								<p class="text-gray no-margn">Messages</p></a>
						</div>
						<div class="col-xs-6 col-sm-4">
							<a href="#" class="text-purple"><span class="h2"><i
									class="fa fa-calendar-o"></i></span>
								<p class="text-gray no-margn">Events</p></a>
						</div>

						<div class="col-xs-12 visible-xs-block">
							<hr>
						</div>

						<div class="col-xs-6 col-sm-4">
							<a href="#" class="text-red"><span class="h2"><i
									class="fa fa-comments-o"></i></span>
								<p class="text-gray no-margn">Chatting</p></a>
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12  hidden-xs">
							<hr>
						</div>

						<div class="col-xs-6 col-sm-4">
							<a href="#" class="text-yellow"><span class="h2"><i
									class="fa fa-folder-open-o"></i></span>
								<p class="text-gray">Folders</p></a>
						</div>

						<div class="col-xs-12 visible-xs-block">
							<hr>
						</div>

						<div class="col-xs-6 col-sm-4">
							<a href="#" class="text-primary"><span class="h2"><i
									class="fa fa-flag-o"></i></span>
								<p class="text-gray">Task</p></a>
						</div>
						<div class="col-xs-6 col-sm-4">
							<a href="#" class="text-info"><span class="h2"><i
									class="fa fa-star-o"></i></span>
								<p class="text-gray">Favorites</p></a>
						</div>
					</div>
				</div>
			</div></li> -->
	<div id="popup1" class="modal-box">
		<header> <a href="#" class="js-modal-close closes">�</a>
		<h3>Change Password</h3>
		</header>
		<div class="modal-body">
			<p>
			<div class="row" id="password">
				<%-- <form:form method="POST" action="changePassword"
					commandName="changePasswordVo"> --%>
				<div class="col-xs-12">
					<form action="#" id="form_pwd">
						<input type="hidden" id="userName_changpwd" name="userName"
							value="${thisUser }" />

						<div class="form-group">

							<div class="col-sm-12">
								<i class="fa fa-key password_icon"></i> <input type="password"
									id="old_pwd_changpwd" name="oldPassword" class="password_input"
									placeholder="Old Password" /><i class="fa fa-check valiyes"
									id="old_pwd_ok"></i> <i class="fa fa-times valino"
									id="old_pwd_err"></i>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<i class="fa fa-key password_icon"></i> <input type="password"
									name="newPassword" id="new_pwd_changpwd" class="password_input"
									placeholder="New Password" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<i class="fa fa-key password_icon"></i> <input type="password"
									id="confirm_pwd" name="confirmPassword" class="password_input"
									placeholder="Confirm Password"><i
									class="fa fa-check valiyes" id="new_pwd_ok"></i> <i
									class="fa fa-times valino" id="new_pwd_err"></i>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<input type="button" class="password_btns nwcl"
									id="changpwd_sbmt" value="Submit" /> <input type="reset"
									class="password_btns nwcls" value="Clear" /> <input
									id="chng_pwd_close_btn" type="button" class="password_btns"
									value="Cancel" /> <span class="msg_password_no" id="pwd_err"></span>
								<span class="msg_password_yes" id="pwd_success">Password
									Changed Successfully..</span>
							</div>
						</div>
					</form>
				</div>
			</div>
			</p>
		</div>
		<footer> </footer>
	</div>
	</header>
	<!-- Header Ends -->



	<!-- Content Block Ends Here (right box)-->

	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>
	<%-- <script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script> --%>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/DevExpressChartJS/dx.chartjs.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/DevExpressChartJS/world.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/DevExpressChartJS/demo-charts.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/sparkline/jquery.sparkline.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/sparkline/jquery.sparkline.demo.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.js' />"></script>

	<script src="<c:url value='/resources/js/sockjs-0.3.4.js'/>"></script>
	<script src="<c:url value='/resources/js/stomp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/pushnotification.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.confirmon.js' />"></script>

	<%-- <script
		src="<c:url value='/resources/js/validation/hrms-validator.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script> --%>

	<script type="text/javascript">
		function showMe() {
			$("#pdtPop").css("display", "block");
		}

		function handleNotification(messsage) {
			if ($.inArray("ALL", messsage['recipients']) > -1) {
				showAnnouncement("Announcement", messsage['content']);
			} else if ($.inArray("ONLINE", messsage['recipients']) > -1) {
				showAnnouncement("Online", messsage['content']);
			} else {
				if ($.inArray($("#authEmpCode").val(), messsage['recipients']) > -1) {
					$("#notification" + $("#authEmpCode").val()).show();
					if ($("#notification" + $("#authEmpCode").val()).text() != "") {
						var notifCount = $(
								"#notification" + $("#authEmpCode").val())
								.text();
						$("#notification" + $("#authEmpCode").val()).html(
								(parseInt(notifCount) + 1));
					} else {
						$("#notification" + $("#authEmpCode").val()).html("1");
					}
					$("#notification_list" + $("#authEmpCode").val())
							.append(
									"<a href='"+messsage['url']+"' class='list-group-item'><div class='pro_pic'><img src='"+messsage['picture']+"'/></div>"
											+ messsage['content'] + "</a>");
				}
			}
		}

		/* $('#button_2').confirmOn({
			questionText : 'This action cannot be undone, are you sure?',
			textYes : 'Yes, I\'m sure',
			textNo : 'No, I\'m not sure'
		}, 'click', function(e, confirmed) {
			/* if (confirmed)
				$(this).remove(); 
		}); */

		$(document).ready(function() {

			$(document).height(function() {
				if ($(this).height() > 775) {

					$(".footer").removeClass('bottomZeroLess');
					// $(".footer").( $(this).scrollTop() <= 600 );
				}
			});

			var stompClient = null;
			//connect(handleNotification);
			$("#notification" + $("#authEmpCode").val()).hide();

			$('#rCalendar').RegalCalendar({
				theme : 'cyan',
				base : 'white',
				modal : false,
				minDate : new Date(2012, 1 - 1, 1),
				maxDate : new Date(2014, 12 - 1, 31),
				tooltip : 'bootstrap',
				inputDate : '#inputDate',
				inputEvent : '#inputEvent',
				inputLocation : '#inputLocation',
				mapLink : 'map',

			});
			$('#mCalendar').RegalCalendar({
				theme : 'cyan',
				base : 'white',
				modal : true,
				minDate : new Date(2012, 1 - 1, 1),
				maxDate : new Date(2014, 12 - 1, 31),
				tooltip : 'bootstrap'
			});

			$("html").niceScroll();
			$('.bs-docs-sidenav a').click(function() {
				$('html, body').scrollTo($(this).attr('data-href'), 1000)
			});
		});
	</script>

	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.11.1.min.js' />"></script>
	<script>
		$(function() {

			var appendthis = ("<div class='modal-overlay js-modal-close'></div>");

			$('a[data-modal-id]').click(function(e) {
				$("#form_pwd").find("input[type=password]").val("");
				$("#new_pwd_err").css("display", "none");
				$("#old_pwd_err").css("display", "none");
				$("#new_pwd_ok").css("display", "none");
				$("#old_pwd_ok").css("display", "none");
				$("#pwd_success").css("display", "none");
				$("#pwd_err").css("display", "none");
				e.preventDefault();
				$("body").append(appendthis);
				$(".modal-overlay").fadeTo(500, 0.7);
				//$(".js-modalbox").fadeIn(500);
				var modalBox = $(this).attr('data-modal-id');
				$('#' + modalBox).fadeIn($(this).data());
			});

			$(".js-modal-close, .modal-overlay").click(function() {
				$(".modal-box, .modal-overlay").fadeOut(500, function() {
					$(".modal-overlay").remove();
				});

			});

			$(window).resize(
					function() {
						$(".modal-box").css(
								{
									top : ($(window).height() - $(".modal-box")
											.outerHeight()) / 2,
									left : ($(window).width() - $(".modal-box")
											.outerWidth()) / 2
								});
					});

			$(window).resize();

		});

		$("#changpwd_sbmt")
				.click(
						function() {
							var oldpassword = $("#old_pwd_changpwd").val();
							var newpassword = $("#new_pwd_changpwd").val();
							var confirmpassword = $("#confirm_pwd").val();
							if (oldpassword != '' && newpassword != ''
									&& confirmpassword != '') {
								if (newpassword == confirmpassword) {
									var vo = new Object();
									vo.userName = $("#userName_changpwd").val();
									vo.oldPassword = oldpassword;
									vo.newPassword = newpassword;
									$
											.ajax({
												type : "POST",
												url : 'changePassword',
												dataType : "json",
												contentType : "application/json; charset=utf-8",
												data : JSON.stringify(vo),
												cache : false,
												async : true,
												success : function(response) {
													if (response.status == "Invalid Password") {
														$("#old_pwd_err").css(
																"display",
																"block");
														$("#pwd_err").html("");
														$("#pwd_err")
																.append(
																		response.status);
														$("#pwd_err").css(
																"display",
																"block");
													} else if (response.status == "ok") {
														$("#new_pwd_ok").css(
																"display",
																"block");
														$("#old_pwd_ok").css(
																"display",
																"block");
														$("#pwd_success").css(
																"display",
																"block");
														$(
																".modal-box, .modal-overlay")
																.fadeOut(
																		2000,
																		function() {
																			$(
																					".modal-overlay")
																					.remove();
																		});
													}
												},
												error : function(requestObject,
														error, errorThrown) {
													alert(errorThrown);
												}
											});
								} else {
									$("#pwd_err").html("");
									$("#pwd_err").append(
											"Password Doesn't Match..");
									$("#pwd_err").css("display", "block");
									$("#new_pwd_err").css("display", "block");
								}
							}
						});

		$("#confirm_pwd").click(function() {
			$("#new_pwd_err").css("display", "none");
			$("#old_pwd_err").css("display", "none");
			$("#new_pwd_ok").css("display", "none");
			$("#old_pwd_ok").css("display", "none");
			$("#pwd_success").css("display", "none");
			$("#pwd_err").css("display", "none");
		});

		$("#old_pwd_changpwd").click(function() {
			$("#new_pwd_err").css("display", "none");
			$("#old_pwd_err").css("display", "none");
			$("#new_pwd_ok").css("display", "none");
			$("#old_pwd_ok").css("display", "none");
			$("#pwd_success").css("display", "none");
			$("#pwd_err").css("display", "none");
		});

		$("#new_pwd_changpwd").click(function() {
			$("#new_pwd_err").css("display", "none");
			$("#old_pwd_err").css("display", "none");
			$("#new_pwd_ok").css("display", "none");
			$("#old_pwd_ok").css("display", "none");
			$("#pwd_success").css("display", "none");
			$("#pwd_err").css("display", "none");
		});

		$("#chng_pwd_close_btn").click(function() {
			$("#new_pwd_err").css("display", "none");
			$("#old_pwd_err").css("display", "none");
			$("#new_pwd_ok").css("display", "none");
			$("#old_pwd_ok").css("display", "none");
			$("#pwd_success").css("display", "none");
			$("#pwd_err").css("display", "none");
			$(".modal-box, .modal-overlay").fadeOut(500, function() {
				$(".modal-overlay").remove();
			});
		});
	</script>

</body>
</html>