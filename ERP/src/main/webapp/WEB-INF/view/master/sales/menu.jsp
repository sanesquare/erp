<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<body>
	<aside class="left-panel ">

		<a href="erpHome.do">
			<div class="user text-center">
				<img src="resources/assets/images/snogol.png" class="img-circle">
			</div>
		</a>

		<nav class="navigation">
			<ul class="list-unstyled">
				<li id="dashbordli"><a href="salesHome.do" title="Dashboard"><i
						class="fa fa-home"></i><span class="nav-label">Home</span></a></li>
						
				<li id="organzationli" class="has-submenu"><a href="customer.do"
					title="Customer"><i class="fa fa-users"></i> <span
						class="nav-label">Customers</span></a>

				<li id="organzationli" class="has-submenu"><a href="customerQuotesHome.do"
					title="Customer Quote"><i class="fa fa-list-alt"></i> <span
						class="nav-label">Customer Quote</span></a>

				<li id="organzationli" class="has-submenu"><a href="salesOrderHome.do"
					title="Sales Order"><i class="fa fa-file-o"></i> <span
						class="nav-label">Sales Order</span></a>
					
					<li id="organzationli" class="has-submenu"><a href="invoiceHome.do"
					title="Invoice"><i class="fa fa-check-square-o"></i> <span
						class="nav-label">Invoice</span></a>
					
					<!-- <li id="organzationli" class="has-submenu"><a href="#"
					title="Credit Memo"><i class="fa fa-file-text-o"></i><span
						class="nav-label">Credit Memo</span></a> -->
					
					<li id="organzationli" class="has-submenu"><a href="salesReturnHome.do"
					title="Sales Return"><i class="fa fa-refresh"></i> <span
						class="nav-label">Sales Return</span></a>
			</ul>
		</nav>
	</aside>
	<!-- Aside Ends-->
</body>
</html>