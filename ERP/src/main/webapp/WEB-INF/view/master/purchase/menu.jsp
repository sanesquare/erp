<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<body>
	<aside class="left-panel ">

		<a href="erpHome.do">
			<div class="user text-center">
				<img src="resources/assets/images/snogol.png" class="img-circle">
			</div>
		</a>

		<nav class="navigation">
			<ul class="list-unstyled">
				<li id="dashbordli"><a href="purchaseHome.do" title="Dashboard"><i
						class="fa fa-home"></i><span class="nav-label">Home</span></a></li>
				<li id="organzationli" class="has-submenu"><a href="products.do"
					title="Products"><i class="fa fa-cubes"></i> <span
						class="nav-label">Products</span></a>

				<li id="organzationli" class="has-submenu"><a href="vendors.do"
					title="Vendors"><i class="fa fa-users"></i> <span
						class="nav-label">Vendors</span></a>

				<li id="organzationli" class="has-submenu"><a href="vendorQuotesHome.do"
					title="Vendor Quotes"><i class="fa fa-pencil-square-o"></i> <span
						class="nav-label">Vendor Quotes</span></a>

				<li id="organzationli" class="has-submenu"><a href="purchaseOrderHome.do"
					title="Purchase Order"><i class="fa fa-list-alt"></i> <span
						class="nav-label">Purchase Order</span></a>

				<li id="organzationli" class="has-submenu"><a href="billsHome.do"
					title="Bill"><i class="fa fa-inr"></i><span class="nav-label">Bill</span></a>

			<!-- 	<li id="organzationli" class="has-submenu"><a href="#"
					title="Vendor Credit Memo"><i class="fa fa-files-o"></i><span
						class="nav-label">Vendor Credit Memo</span></a> -->

				<li id="organzationli" class="has-submenu"><a href="purchaseReturnHome.do"
					title="Purchase Return"><i class="fa fa-refresh"></i> <span
						class="nav-label">Purchase Return</span></a>
			</ul>
		</nav>
	</aside>
	<!-- Aside Ends-->
</body>
</html>