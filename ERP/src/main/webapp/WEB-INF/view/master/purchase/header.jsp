<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>SNOGOL CRM</title>

<!-- Bootstrap core CSS -->
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/bootstrap/bootstrap.css'/>" />
<!-- Calendar Styling  -->
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/calendar/calendar.css'/>" />
<!-- Fonts  -->
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300'
	rel='stylesheet' type='text/css'>
<!-- Base Styling  -->
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/app/app.v1.css'/>" />
	<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/purchase/css/purchase-css.css'/>" />
<%-- <link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/RegalCalendar.css'/>" /> --%>

<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/jquery.growl.css'/>" />
<style type="text/css">
#new_pwd_ok {
	display: none
}

#new_pwd_err {
	display: none
}

#old_pwd_ok {
	display: none
}

#old_pwd_err {
	display: none
}

#pwd_success {
	display: none
}

#pwd_err {
	display: none
}
.logo_width{width: 57px;margin-top: -6px;}
</style>
</head>
<body>



	<header class="top-head container-fluid"> <input type="hidden"
		value="${authEmpCode}" id="authEmpCode" />
	<button type="button" class="navbar-toggle pull-left">
		<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
		<span class="icon-bar"></span> <span class="icon-bar"></span>
	</button>

	<div style="float: right;">
		<div class="companyName-div">
			<span class="companyName">${companyName }</span>
		</div>
		<ul class="nav-toolbar">
			<li class="dropdown"><a href="#" data-toggle="dropdown"><i
					class="fa fa-user"></i></a>
				<div
					class="dropdown-menu arrow pull-right md panel panel-default arrow-top-right notifications">


					<div class="list-group">

						<a class="list-group-item" data-modal-id="pdtPop"> Profile :
							${authEmployeeDetails.name} </a> <a href="#" class="list-group-item"
							data-modal-id="popup1"> Change Password </a> <a href="logout"
							class="list-group-item"> Logout </a>

					</div>

				</div></li>




		</ul>
	</div>

	<div id="pdtPop" style="display: none;" class="modal-box">
		<header> <a href="#" class="js-modal-close closes">�</a><h3> My
		Information</h3> </header>
		<div class="modal-body">
			<div class="panel-body">
				<c:if test="${! empty authEmployeeDetails.profilePic }">
					<div class="form-group">
						<div class="photo_show">
							<img src='<c:url value="${authEmployeeDetails.profilePic }" />' />
						</div>
					</div>
				</c:if>
				<div class="form-group">
					<label class="col-sm-4 control-label">Name</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.name}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Code</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.employeeCode}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">User name</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.userName}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Branch</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.employeeBranch}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Department</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.employeeDepartment}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Designation</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.designation}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Join Date</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.joiningDate}</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Shift</label>
					<div class="col-sm-8">
						<span>${authEmployeeDetails.workShift}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="popup1" class="modal-box">
		<header> <a href="#" class="js-modal-close closes">�</a>
		<h3>Change Password</h3>
		</header>
		<div class="modal-body">
			<p>
			<div class="row" id="password">
				<%-- <form:form method="POST" action="changePassword"
					commandName="changePasswordVo"> --%>
				<div class="col-xs-12">
					<form action="#" id="form_pwd">
						<input type="hidden" id="userName_changpwd" name="userName"
							value="${thisUser }" />

						<div class="form-group">

							<div class="col-sm-12">
								<i class="fa fa-key password_icon"></i> <input type="password"
									id="old_pwd_changpwd" name="oldPassword" class="password_input"
									placeholder="Old Password" /><i class="fa fa-check valiyes"
									id="old_pwd_ok"></i> <i class="fa fa-times valino"
									id="old_pwd_err"></i>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<i class="fa fa-key password_icon"></i> <input type="password"
									name="newPassword" id="new_pwd_changpwd" class="password_input"
									placeholder="New Password" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<i class="fa fa-key password_icon"></i> <input type="password"
									id="confirm_pwd" name="confirmPassword" class="password_input"
									placeholder="Confirm Password"><i
									class="fa fa-check valiyes" id="new_pwd_ok"></i> <i
									class="fa fa-times valino" id="new_pwd_err"></i>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<input type="button" class="password_btns nwcl"
									id="changpwd_sbmt" value="Submit" /> <input type="reset"
									class="password_btns nwcls" value="Clear" /> <input
									id="chng_pwd_close_btn" type="button" class="password_btns"
									value="Cancel" /> <span class="msg_password_no" id="pwd_err"></span>
								<span class="msg_password_yes" id="pwd_success">Password
									Changed Successfully..</span>
							</div>
						</div>
					</form>
				</div>
			</div>
			</p>
		</div>
		<footer> </footer>
	</div>
	</header>
	<!-- Header Ends -->



	<!-- Content Block Ends Here (right box)-->

	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.9.1.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/underscore/underscore-min.js' />"></script>
	<%-- <script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script> --%>
	<script
		src="<c:url value='/resources/assets/js/globalize/globalize.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/DevExpressChartJS/dx.chartjs.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/DevExpressChartJS/world.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/DevExpressChartJS/demo-charts.js' />"></script>

	<script
		src="<c:url value='/resources/assets/js/plugins/sparkline/jquery.sparkline.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/sparkline/jquery.sparkline.demo.js' />"></script>
	<script src="<c:url value='/resources/assets/js/app/custom.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.js' />"></script>

	<script src="<c:url value='/resources/js/sockjs-0.3.4.js'/>"></script>
	<script src="<c:url value='/resources/js/stomp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/pushnotification.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/jquery.confirmon.js' />"></script>


	<script type="text/javascript">
		function handleNotification(messsage) {
			if ($.inArray("ALL", messsage['recipients']) > -1) {
				showAnnouncement("Announcement", messsage['content']);
			} else if ($.inArray("ONLINE", messsage['recipients']) > -1) {
				showAnnouncement("Online", messsage['content']);
			} else {
				if ($.inArray($("#authEmpCode").val(), messsage['recipients']) > -1) {
					$("#notification" + $("#authEmpCode").val()).show();
					if ($("#notification" + $("#authEmpCode").val()).text() != "") {
						var notifCount = $(
								"#notification" + $("#authEmpCode").val())
								.text();
						$("#notification" + $("#authEmpCode").val()).html(
								(parseInt(notifCount) + 1));
					} else {
						$("#notification" + $("#authEmpCode").val()).html("1");
					}
					$("#notification_list" + $("#authEmpCode").val())
							.append(
									"<a href='"+messsage['url']+"' class='list-group-item'><div class='pro_pic'><img src='"+messsage['picture']+"'/></div>"
											+ messsage['content'] + "</a>");
				}
			}
		}

		/* $('#button_2').confirmOn({
			questionText : 'This action cannot be undone, are you sure?',
			textYes : 'Yes, I\'m sure',
			textNo : 'No, I\'m not sure'
		}, 'click', function(e, confirmed) {
			/* if (confirmed)
				$(this).remove(); 
		}); */

		$(document).ready(function() {

			$(document).height(function() {
				if ($(this).height() > 775) {

					$(".footer").removeClass('bottomZeroLess');
					// $(".footer").( $(this).scrollTop() <= 600 );
				}
			});

			var stompClient = null;
			//connect(handleNotification);
			$("#notification" + $("#authEmpCode").val()).hide();

			$('#rCalendar').RegalCalendar({
				theme : 'cyan',
				base : 'white',
				modal : false,
				minDate : new Date(2012, 1 - 1, 1),
				maxDate : new Date(2014, 12 - 1, 31),
				tooltip : 'bootstrap',
				inputDate : '#inputDate',
				inputEvent : '#inputEvent',
				inputLocation : '#inputLocation',
				mapLink : 'map',

			});
			$('#mCalendar').RegalCalendar({
				theme : 'cyan',
				base : 'white',
				modal : true,
				minDate : new Date(2012, 1 - 1, 1),
				maxDate : new Date(2014, 12 - 1, 31),
				tooltip : 'bootstrap'
			});

			$("html").niceScroll();
			$('.bs-docs-sidenav a').click(function() {
				$('html, body').scrollTo($(this).attr('data-href'), 1000)
			});
		});
	</script>

	<script
		src="<c:url value='/resources/assets/js/jquery/jquery-1.11.1.min.js' />"></script>
	<script>
		$(function() {

			var appendthis = ("<div class='modal-overlay js-modal-close'></div>");

			$('a[data-modal-id]').click(function(e) {
				$("#form_pwd").find("input[type=password]").val("");
				$("#new_pwd_err").css("display", "none");
				$("#old_pwd_err").css("display", "none");
				$("#new_pwd_ok").css("display", "none");
				$("#old_pwd_ok").css("display", "none");
				$("#pwd_success").css("display", "none");
				$("#pwd_err").css("display", "none");
				e.preventDefault();
				$("body").append(appendthis);
				$(".modal-overlay").fadeTo(500, 0.7);
				//$(".js-modalbox").fadeIn(500);
				var modalBox = $(this).attr('data-modal-id');
				$('#' + modalBox).fadeIn($(this).data());
			});

			$(".js-modal-close, .modal-overlay").click(function() {
				$(".modal-box, .modal-overlay").fadeOut(500, function() {
					$(".modal-overlay").remove();
				});

			});

			$(window).resize(
					function() {
						$(".modal-box").css(
								{
									top : ($(window).height() - $(".modal-box")
											.outerHeight()) / 2,
									left : ($(window).width() - $(".modal-box")
											.outerWidth()) / 2
								});
					});

			$(window).resize();

		});

		$("#changpwd_sbmt")
				.click(
						function() {
							var oldpassword = $("#old_pwd_changpwd").val();
							var newpassword = $("#new_pwd_changpwd").val();
							var confirmpassword = $("#confirm_pwd").val();
							if (oldpassword != '' && newpassword != ''
									&& confirmpassword != '') {
								if (newpassword == confirmpassword) {
									var vo = new Object();
									vo.userName = $("#userName_changpwd").val();
									vo.oldPassword = oldpassword;
									vo.newPassword = newpassword;
									$
											.ajax({
												type : "POST",
												url : 'changePassword',
												dataType : "json",
												contentType : "application/json; charset=utf-8",
												data : JSON.stringify(vo),
												cache : false,
												async : true,
												success : function(response) {
													if (response.status == "Invalid Password") {
														$("#old_pwd_err").css(
																"display",
																"block");
														$("#pwd_err").html("");
														$("#pwd_err")
																.append(
																		response.status);
														$("#pwd_err").css(
																"display",
																"block");
													} else if (response.status == "ok") {
														$("#new_pwd_ok").css(
																"display",
																"block");
														$("#old_pwd_ok").css(
																"display",
																"block");
														$("#pwd_success").css(
																"display",
																"block");
														$(
																".modal-box, .modal-overlay")
																.fadeOut(
																		2000,
																		function() {
																			$(
																					".modal-overlay")
																					.remove();
																		});
													}
												},
												error : function(requestObject,
														error, errorThrown) {
													alert(errorThrown);
												}
											});
								} else {
									$("#pwd_err").html("");
									$("#pwd_err").append(
											"Password Doesn't Match..");
									$("#pwd_err").css("display", "block");
									$("#new_pwd_err").css("display", "block");
								}
							}
						});

		$("#confirm_pwd").click(function() {
			$("#new_pwd_err").css("display", "none");
			$("#old_pwd_err").css("display", "none");
			$("#new_pwd_ok").css("display", "none");
			$("#old_pwd_ok").css("display", "none");
			$("#pwd_success").css("display", "none");
			$("#pwd_err").css("display", "none");
		});

		$("#old_pwd_changpwd").click(function() {
			$("#new_pwd_err").css("display", "none");
			$("#old_pwd_err").css("display", "none");
			$("#new_pwd_ok").css("display", "none");
			$("#old_pwd_ok").css("display", "none");
			$("#pwd_success").css("display", "none");
			$("#pwd_err").css("display", "none");
		});

		$("#new_pwd_changpwd").click(function() {
			$("#new_pwd_err").css("display", "none");
			$("#old_pwd_err").css("display", "none");
			$("#new_pwd_ok").css("display", "none");
			$("#old_pwd_ok").css("display", "none");
			$("#pwd_success").css("display", "none");
			$("#pwd_err").css("display", "none");
		});

		$("#chng_pwd_close_btn").click(function() {
			$("#new_pwd_err").css("display", "none");
			$("#old_pwd_err").css("display", "none");
			$("#new_pwd_ok").css("display", "none");
			$("#old_pwd_ok").css("display", "none");
			$("#pwd_success").css("display", "none");
			$("#pwd_err").css("display", "none");
			$(".modal-box, .modal-overlay").fadeOut(500, function() {
				$(".modal-overlay").remove();
			});
		});
	</script>

</body>
</html>