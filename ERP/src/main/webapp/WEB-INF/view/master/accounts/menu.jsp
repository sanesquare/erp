<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<body>
	<aside class="left-panel ">

		<a href="erpHome.do">
			<div class="user text-center">
				<img src="resources/assets/images/snogol.png" class="img-circle">
			</div>
		</a>

		<nav class="navigation">
			<ul class="list-unstyled">
				<li id="dashbordli"><a href="accountsHome.do" title="Dashboard"><i
						class="fa fa-home"></i><span class="nav-label">Home</span></a></li>
						
				<li id="organzationli" class="has-submenu"><a href="ledgersHome.do"
					title="Ledgers"><i class="fa fa-book"></i><span
						class="nav-label">Ledgers</span></a>

				<li id="organzationli" class="has-submenu"><a href="paymentHome.do"
					title="Payment"><i class="fa fa-credit-card"></i> <span
						class="nav-label">Payment</span></a>

				<li id="organzationli" class="has-submenu"><a href="receiptHome.do"
					title="Receipt"><i class="fa fa-file-text-o"></i><span
						class="nav-label">Receipt</span></a>
					
					<li id="organzationli" class="has-submenu"><a href="journalHome.do"
					title="Journal"><i class="fa fa-file-text"></i><span
						class="nav-label">Journal</span></a>
					
					<li id="organzationli" class="has-submenu"><a href="contraHome.do"
					title="Contra"><i class="fa fa-file-text"></i> <span
						class="nav-label">Contra</span></a>
					
					<li id="organzationli" class="has-submenu"><a href="trialBalance.do"
					title="Trial Balance"><i class="fa fa-book"></i> <span
						class="nav-label">Trial Balance</span></a>
					
					<li id="organzationli" class="has-submenu"><a href="profitAndLoss.do"
					title="Profit And Loss Account"><i class="fa fa-pie-chart"></i><span
						class="nav-label">Profit And Loss Account</span></a>
					
					<li id="organzationli" class="has-submenu"><a href="balanceSheet.do"
					title="Balance Sheet"><i class="fa fa-bar-chart"></i><span
						class="nav-label">Balance Sheet</span></a>
						<li id="organzationli" class="has-submenu"><a href="salaryAccounts.do"
					title="Salary"><i class="fa fa-money"></i><span
						class="nav-label">Salary</span></a>
			</ul>
		</nav>
	</aside>
	<!-- Aside Ends-->
</body>
</html>