<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<body>
	<aside class="left-panel ">

		<a href="erpHome.do">
			<div class="user text-center">
				<img src="resources/assets/images/snogol.png" class="img-circle">
			</div>
		</a>

		<nav class="navigation">
			<ul class="list-unstyled">
				<li id="dashbordli"><a href="inventoryHome.do"
					title="Dashboard"><i class="fa fa-home"></i><span
						class="nav-label">Home</span></a></li>
				<li id="organzationli" class="has-submenu"><a href="stockSummary.do"
					title="Stock"><i class="fa fa-building-o"></i> <span
						class="nav-label">Stock</span></a>
				<li id="organzationli" class="has-submenu"><a href="packingList.do"
					title="Packing List"><i class="fa fa-list-ol"></i> <span
						class="nav-label">Packing List</span></a>
				<li id="organzationli" class="has-submenu"><a href="deliveryNotes.do"
					title="Delivery Notes"><i class="fa fa-sign-out"></i><span
						class="nav-label">Delivery Notes</span></a>
				<li id="organzationli" class="has-submenu"><a
					href="receivingNotes.do" title="Receiving Notes"><i
						class="fa fa-sign-in flip-ico"></i> <span class="nav-label">Receiving
							Notes</span></a>
				<li id="organzationli" class="has-submenu"><a
					href="inventoryEntry.do" title="Inventory Entry"><i
						class="fa fa-sign-in flip-ico"></i> <span class="nav-label">Inventory
							Entry</span></a>
				<li id="organzationli" class="has-submenu"><a
					href="inventoryWithdrawal.do" title="Inventory Withdrawal"><i
						class="fa fa-sign-out"></i> <span class="nav-label">Inventory
							Withdrawal</span></a>
			</ul>
		</nav>
	</aside>
	<!-- Aside Ends-->
</body>
</html>