<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<body>
	<aside class="left-panel collapsed">

		<a href="erpHome.do">
			<div class="user text-center">
				<img src="resources/assets/images/snogol.png" class="img-circle">
			</div>
		</a>


		<nav class="navigation">
			<ul class="list-unstyled">
				<li id="dashbordli"><a href="crmHome.do" title="Dashboard"><i
						class="fa fa-home"></i><span class="nav-label">Home</span></a></li>
				<li id="organzationli" class="has-submenu"><a href="leads.do"
					title="Leads"><i class="fa fa-users"></i><span
						class="nav-label">Leads</span></a>

				<li id="organzationli" class="has-submenu"><a href="#"
					title="Opportunity"><i class="fa fa-comments-o"></i> <span
						class="nav-label">Opportunity</span></a>

				<li id="organzationli" class="has-submenu"><a href="#"
					title="Quotes"><i class="fa fa-pencil-square-o"></i> <span
						class="nav-label">Quotes</span></a>
			</ul>
		</nav>
	</aside>
	<!-- Aside Ends-->
</body>
</html>